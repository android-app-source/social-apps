.class public Lcom/twitter/android/media/imageeditor/EditImageFragment;
.super Lcom/twitter/app/common/abs/AbsFragment;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/media/imageeditor/EditImageFragment$a;,
        Lcom/twitter/android/media/imageeditor/EditImageFragment$d;,
        Lcom/twitter/android/media/imageeditor/EditImageFragment$e;,
        Lcom/twitter/android/media/imageeditor/EditImageFragment$c;,
        Lcom/twitter/android/media/imageeditor/EditImageFragment$b;,
        Lcom/twitter/android/media/imageeditor/EditImageFragment$f;,
        Lcom/twitter/android/media/imageeditor/EditImageFragment$h;,
        Lcom/twitter/android/media/imageeditor/EditImageFragment$g;
    }
.end annotation


# static fields
.field public static final a:Lcom/twitter/android/media/imageeditor/EditImageFragment$e;

.field private static final b:[I

.field private static final c:[I


# instance fields
.field private A:Lcom/twitter/android/media/imageeditor/EditImageFragment$b;

.field private B:Lcom/twitter/model/media/EditableImage;

.field private C:Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;

.field private D:Ljava/lang/String;

.field private E:I

.field private F:I

.field private G:F

.field private H:Z

.field private I:I

.field private J:I

.field private K:I

.field private L:I

.field private M:Z

.field private N:I

.field private O:Z

.field private P:Ljava/lang/String;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/media/filters/Filters;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/model/media/EditableImage;",
            "Lcom/twitter/android/media/imageeditor/EditImageFragment$d;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

.field private g:Lcom/twitter/android/media/imageeditor/CropMediaImageView;

.field private h:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

.field private i:Lcom/twitter/android/media/widget/FilterFilmstripView;

.field private j:Landroid/widget/ImageButton;

.field private k:Landroid/view/View;

.field private l:Lcom/twitter/media/ui/image/MediaImageView;

.field private m:Landroid/view/View;

.field private n:Landroid/view/View;

.field private o:Landroid/widget/ImageButton;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/view/View;

.field private s:Lcom/twitter/android/media/imageeditor/EditImageFragment$c;

.field private t:Lcom/twitter/model/media/EditableImage;

.field private u:Lcom/twitter/model/media/EditableImage;

.field private v:Lcom/twitter/android/media/imageeditor/a;

.field private w:Lcom/twitter/android/media/stickers/data/a;

.field private x:Lcom/twitter/media/filters/Filters;

.field private y:Lcom/twitter/android/composer/ComposerType;

.field private z:Lcom/twitter/android/media/imageeditor/EditImageFragment$g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 93
    new-instance v0, Lcom/twitter/android/media/imageeditor/EditImageFragment$e;

    invoke-direct {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$e;-><init>()V

    sput-object v0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment$e;

    .line 132
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->b:[I

    .line 142
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->c:[I

    return-void

    .line 132
    nop

    :array_0
    .array-data 4
        0x7f130352
        0x7f130351
        0x7f130353
        0x7f130354
        0x7f130359
        0x7f130356
        0x7f130357
        0x7f130358
    .end array-data

    .line 142
    :array_1
    .array-data 4
        0x7f130359
        0x7f130356
        0x7f130357
        0x7f130358
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/twitter/app/common/abs/AbsFragment;-><init>()V

    .line 149
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->d:Ljava/util/List;

    .line 150
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->e:Ljava/util/Map;

    .line 179
    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->F:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/media/imageeditor/EditImageFragment;)I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->E:I

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/media/imageeditor/EditImageFragment;I)I
    .locals 0

    .prologue
    .line 92
    iput p1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->F:I

    return p1
.end method

.method public static a(Lcom/twitter/model/media/EditableImage;Lcom/twitter/android/media/imageeditor/EditImageFragment$b;Landroid/view/View;Lcom/twitter/android/widget/RevealClipFrameLayout;Lcom/twitter/model/media/EditableImage;ILjava/lang/String;)Lcom/twitter/android/media/imageeditor/EditImageFragment;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 211
    invoke-virtual {p3, v0}, Lcom/twitter/android/widget/RevealClipFrameLayout;->setVisibility(I)V

    .line 213
    const/4 v1, 0x0

    invoke-virtual {p3, v1}, Lcom/twitter/android/widget/RevealClipFrameLayout;->setAlpha(F)V

    .line 214
    new-instance v1, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;

    invoke-direct {v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;-><init>()V

    .line 215
    if-eqz p6, :cond_0

    .line 216
    invoke-virtual {v1, p6}, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;->a(Ljava/lang/String;)Lcom/twitter/android/media/imageeditor/EditImageFragment$a;

    .line 220
    :cond_0
    invoke-static {v0}, Lbpt;->a(Z)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    if-ne p5, v2, :cond_1

    .line 221
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 223
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "sticker_selector_tooltip"

    .line 225
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    .line 222
    invoke-static {v3, v4, v6, v7}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v2

    .line 226
    invoke-virtual {v2}, Lcom/twitter/android/util/h;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    move p5, v0

    .line 232
    :cond_1
    invoke-virtual {v1, p5}, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;->a(I)Lcom/twitter/android/media/imageeditor/EditImageFragment$a;

    move-result-object v0

    .line 233
    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;->a()Lcom/twitter/android/media/imageeditor/EditImageFragment;

    move-result-object v0

    .line 235
    invoke-virtual {v0, p1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Lcom/twitter/android/media/imageeditor/EditImageFragment$b;)V

    .line 236
    invoke-virtual {v0, p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Lcom/twitter/model/media/EditableImage;)V

    .line 237
    invoke-direct {v0, p4}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->b(Lcom/twitter/model/media/EditableImage;)V

    .line 238
    new-instance v1, Lcom/twitter/android/media/imageeditor/EditImageFragment$1;

    invoke-direct {v1, p0, v0, p2, p3}, Lcom/twitter/android/media/imageeditor/EditImageFragment$1;-><init>(Lcom/twitter/model/media/EditableImage;Lcom/twitter/android/media/imageeditor/EditImageFragment;Landroid/view/View;Lcom/twitter/android/widget/RevealClipFrameLayout;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Lcom/twitter/android/media/imageeditor/EditImageFragment$c;)V

    .line 253
    return-object v0
.end method

.method public static a(Lcom/twitter/model/media/EditableImage;Lcom/twitter/android/media/imageeditor/EditImageFragment$b;Landroid/view/View;Lcom/twitter/android/widget/RevealClipFrameLayout;Lcom/twitter/model/media/EditableImage;Ljava/lang/String;)Lcom/twitter/android/media/imageeditor/EditImageFragment;
    .locals 7

    .prologue
    .line 199
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Lcom/twitter/model/media/EditableImage;Lcom/twitter/android/media/imageeditor/EditImageFragment$b;Landroid/view/View;Lcom/twitter/android/widget/RevealClipFrameLayout;Lcom/twitter/model/media/EditableImage;ILjava/lang/String;)Lcom/twitter/android/media/imageeditor/EditImageFragment;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/view/View;Lcom/twitter/model/media/EditableImage;Lcom/twitter/android/widget/RevealClipFrameLayout;Lcom/twitter/android/media/imageeditor/EditImageFragment;)V
    .locals 11

    .prologue
    .line 262
    const/4 v0, 0x2

    new-array v1, v0, [I

    fill-array-data v1, :array_0

    .line 263
    invoke-virtual {p0, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 264
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    .line 265
    invoke-virtual {p2, v0}, Lcom/twitter/android/widget/RevealClipFrameLayout;->getLocationInWindow([I)V

    .line 266
    const/4 v2, 0x0

    const/4 v3, 0x0

    aget v3, v1, v3

    const/4 v4, 0x0

    aget v4, v0, v4

    sub-int/2addr v3, v4

    aput v3, v1, v2

    .line 267
    const/4 v2, 0x1

    const/4 v3, 0x1

    aget v3, v1, v3

    const/4 v4, 0x1

    aget v0, v0, v4

    sub-int v0, v3, v0

    aput v0, v1, v2

    .line 270
    new-instance v2, Landroid/graphics/Rect;

    const/4 v0, 0x0

    const/4 v3, 0x0

    invoke-virtual {p2}, Lcom/twitter/android/widget/RevealClipFrameLayout;->getMeasuredWidth()I

    move-result v4

    invoke-virtual {p2}, Lcom/twitter/android/widget/RevealClipFrameLayout;->getMeasuredHeight()I

    move-result v5

    invoke-direct {v2, v0, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 271
    new-instance v3, Landroid/graphics/Rect;

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v0, p1, Lcom/twitter/model/media/EditableImage;->k:Lcom/twitter/media/model/MediaFile;

    check-cast v0, Lcom/twitter/media/model/ImageFile;

    iget-object v0, v0, Lcom/twitter/media/model/ImageFile;->f:Lcom/twitter/util/math/Size;

    invoke-virtual {v0}, Lcom/twitter/util/math/Size;->a()I

    move-result v6

    iget-object v0, p1, Lcom/twitter/model/media/EditableImage;->k:Lcom/twitter/media/model/MediaFile;

    check-cast v0, Lcom/twitter/media/model/ImageFile;

    iget-object v0, v0, Lcom/twitter/media/model/ImageFile;->f:Lcom/twitter/util/math/Size;

    .line 272
    invoke-virtual {v0}, Lcom/twitter/util/math/Size;->b()I

    move-result v0

    invoke-direct {v3, v4, v5, v6, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 273
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 274
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    sget-object v6, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v0, v4, v5, v6}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 276
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 277
    invoke-virtual {v0, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 279
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/twitter/android/widget/RevealClipFrameLayout;->setPivotX(F)V

    .line 280
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/twitter/android/widget/RevealClipFrameLayout;->setPivotY(F)V

    .line 283
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v3

    sub-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 284
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v2

    sub-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 286
    invoke-virtual {p1}, Lcom/twitter/model/media/EditableImage;->a()F

    move-result v0

    .line 287
    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v4

    if-lez v0, :cond_0

    .line 288
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Lcom/twitter/android/widget/RevealClipFrameLayout;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v4, v3

    int-to-float v4, v4

    div-float/2addr v0, v4

    .line 291
    :goto_0
    const-string/jumbo v4, "clipX"

    const/4 v5, 0x2

    new-array v5, v5, [I

    const/4 v6, 0x0

    aput v3, v5, v6

    const/4 v6, 0x1

    const/4 v7, 0x0

    aput v7, v5, v6

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    .line 292
    const-string/jumbo v5, "clipY"

    const/4 v6, 0x2

    new-array v6, v6, [I

    const/4 v7, 0x0

    aput v2, v6, v7

    const/4 v7, 0x1

    const/4 v8, 0x0

    aput v8, v6, v7

    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    .line 293
    const/4 v6, 0x2

    new-array v6, v6, [Landroid/animation/PropertyValuesHolder;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    const/4 v4, 0x1

    aput-object v5, v6, v4

    invoke-static {p2, v6}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 296
    sget-object v5, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/4 v6, 0x2

    new-array v6, v6, [F

    const/4 v7, 0x0

    aput v0, v6, v7

    const/4 v7, 0x1

    const/high16 v8, 0x3f800000    # 1.0f

    aput v8, v6, v7

    invoke-static {p2, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 297
    sget-object v6, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/4 v7, 0x2

    new-array v7, v7, [F

    const/4 v8, 0x0

    aput v0, v7, v8

    const/4 v8, 0x1

    const/high16 v9, 0x3f800000    # 1.0f

    aput v9, v7, v8

    invoke-static {p2, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 298
    sget-object v7, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    const/4 v8, 0x2

    new-array v8, v8, [F

    const/4 v9, 0x0

    const/4 v10, 0x0

    aget v10, v1, v10

    int-to-float v10, v10

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    mul-float/2addr v3, v0

    sub-float v3, v10, v3

    aput v3, v8, v9

    const/4 v3, 0x1

    const/4 v9, 0x0

    aput v9, v8, v3

    invoke-static {p2, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 300
    sget-object v7, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v8, 0x2

    new-array v8, v8, [F

    const/4 v9, 0x0

    const/4 v10, 0x1

    aget v1, v1, v10

    int-to-float v1, v1

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    sub-float v0, v1, v0

    aput v0, v8, v9

    const/4 v0, 0x1

    const/4 v1, 0x0

    aput v1, v8, v0

    invoke-static {p2, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 303
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 304
    const/4 v2, 0x5

    new-array v2, v2, [Landroid/animation/Animator;

    const/4 v7, 0x0

    aput-object v5, v2, v7

    const/4 v5, 0x1

    aput-object v6, v2, v5

    const/4 v5, 0x2

    aput-object v3, v2, v5

    const/4 v3, 0x3

    aput-object v0, v2, v3

    const/4 v0, 0x4

    aput-object v4, v2, v0

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 306
    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 307
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 309
    sget-object v0, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_2

    invoke-static {p2, v0, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 310
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 311
    new-instance v2, Lcom/twitter/android/media/imageeditor/EditImageFragment$8;

    invoke-direct {v2, p3}, Lcom/twitter/android/media/imageeditor/EditImageFragment$8;-><init>(Lcom/twitter/android/media/imageeditor/EditImageFragment;)V

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 319
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 320
    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    .line 321
    return-void

    .line 289
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Lcom/twitter/android/widget/RevealClipFrameLayout;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v4, v2

    int-to-float v4, v4

    div-float/2addr v0, v4

    goto/16 :goto_0

    .line 262
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 264
    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 309
    :array_2
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic a(Lcom/twitter/android/media/imageeditor/EditImageFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/media/imageeditor/a;)V
    .locals 0

    .prologue
    .line 92
    invoke-static {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->b(Lcom/twitter/android/media/imageeditor/a;)V

    return-void
.end method

.method private a(Lcom/twitter/model/media/EditableMedia;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1218
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->f()Lcom/twitter/media/model/MediaType;

    move-result-object v0

    sget-object v1, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    if-eq v0, v1, :cond_1

    .line 1302
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v4, p1

    .line 1221
    check-cast v4, Lcom/twitter/model/media/EditableImage;

    .line 1222
    new-instance v0, Lcom/twitter/android/media/imageeditor/a;

    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->f:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->g:Lcom/twitter/android/media/imageeditor/CropMediaImageView;

    iget-object v3, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->h:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    .line 1227
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 1228
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/media/imageeditor/a;-><init>(Lcom/twitter/android/media/stickers/StickerFilteredImageView;Lcom/twitter/android/media/imageeditor/CropMediaImageView;Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;Lcom/twitter/model/media/EditableImage;Landroid/content/Context;Landroid/support/v4/app/FragmentManager;)V

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    .line 1230
    iget-object v0, v4, Lcom/twitter/model/media/EditableImage;->h:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdy;

    .line 1231
    new-instance v2, Lcom/twitter/android/media/stickers/StickerView$a;

    invoke-direct {v2, v0}, Lcom/twitter/android/media/stickers/StickerView$a;-><init>(Lcdy;)V

    .line 1232
    new-instance v3, Lcom/twitter/android/media/stickers/StickerView;

    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v3, v5, v2}, Lcom/twitter/android/media/stickers/StickerView;-><init>(Landroid/content/Context;Lcom/twitter/android/media/stickers/StickerView$a;)V

    .line 1233
    iget-object v0, v0, Lcdy;->b:Lcdu;

    iget-object v0, v0, Lcdu;->j:Lcea;

    iget v0, v0, Lcea;->b:F

    invoke-virtual {v3, v0}, Lcom/twitter/android/media/stickers/StickerView;->setAspectRatio(F)V

    .line 1234
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->f:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    invoke-virtual {v0, v3}, Lcom/twitter/android/media/stickers/StickerFilteredImageView;->a(Lcom/twitter/android/media/stickers/StickerView;)V

    goto :goto_1

    .line 1237
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1238
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->d:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/filters/Filters;

    invoke-virtual {v1, v0}, Lcom/twitter/android/media/imageeditor/a;->a(Lcom/twitter/media/filters/Filters;)V

    .line 1243
    :goto_2
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->f:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/StickerFilteredImageView;->h()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1244
    iput-boolean v8, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->O:Z

    .line 1258
    :goto_3
    iget v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->E:I

    if-eq v0, v8, :cond_3

    iget v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->E:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 1259
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->f:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    new-instance v1, Lcom/twitter/android/media/imageeditor/EditImageFragment$7;

    invoke-direct {v1, p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$7;-><init>(Lcom/twitter/android/media/imageeditor/EditImageFragment;)V

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/media/stickers/StickerFilteredImageView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1268
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->C:Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->C:Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;

    .line 1271
    :goto_4
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->C:Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;

    .line 1272
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v1, v0}, Lcom/twitter/android/media/imageeditor/a;->a(Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;)V

    .line 1274
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->w:Lcom/twitter/android/media/stickers/data/a;

    .line 1275
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/stickers/data/a;

    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->y:Lcom/twitter/android/composer/ComposerType;

    iget v3, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->N:I

    .line 1274
    invoke-virtual {v1, v0, v2, v3}, Lcom/twitter/android/media/imageeditor/a;->a(Lcom/twitter/android/media/stickers/data/a;Lcom/twitter/android/composer/ComposerType;I)V

    .line 1279
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->o:Landroid/widget/ImageButton;

    iget-boolean v0, v4, Lcom/twitter/model/media/EditableImage;->b:Z

    if-eqz v0, :cond_9

    iget v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->K:I

    :goto_5
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1280
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->l()V

    .line 1281
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->e:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1282
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->e:Ljava/util/Map;

    new-instance v1, Lcom/twitter/android/media/imageeditor/EditImageFragment$d;

    invoke-direct {v1, p0, v4}, Lcom/twitter/android/media/imageeditor/EditImageFragment$d;-><init>(Lcom/twitter/android/media/imageeditor/EditImageFragment;Lcom/twitter/model/media/EditableImage;)V

    invoke-interface {v0, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1285
    :cond_5
    iget v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->E:I

    packed-switch v0, :pswitch_data_0

    .line 1298
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/a;->u()V

    goto/16 :goto_0

    .line 1240
    :cond_6
    new-instance v0, Lcom/twitter/android/media/imageeditor/EditImageFragment$f;

    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment$f;-><init>(Lcom/twitter/android/media/imageeditor/EditImageFragment;Landroid/content/Context;)V

    new-array v1, v7, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment$f;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_2

    .line 1246
    :cond_7
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->f:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    new-instance v1, Lcom/twitter/android/media/imageeditor/EditImageFragment$6;

    invoke-direct {v1, p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$6;-><init>(Lcom/twitter/android/media/imageeditor/EditImageFragment;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/stickers/StickerFilteredImageView;->setOnImageLoadedListener(Lcom/twitter/media/ui/image/BaseMediaImageView$b;)V

    goto :goto_3

    .line 1270
    :cond_8
    invoke-static {v4}, Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;->a(Lcom/twitter/model/media/EditableImage;)Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;

    move-result-object v0

    goto :goto_4

    .line 1279
    :cond_9
    iget v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->L:I

    goto :goto_5

    .line 1287
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->x:Lcom/twitter/media/filters/Filters;

    if-eqz v0, :cond_a

    .line 1288
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->x:Lcom/twitter/media/filters/Filters;

    invoke-direct {p0, v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->c(Lcom/twitter/media/filters/Filters;)V

    .line 1290
    :cond_a
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/a;->u()V

    goto/16 :goto_0

    .line 1294
    :pswitch_1
    invoke-virtual {p0, v7}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->d(Z)V

    goto/16 :goto_0

    .line 1285
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lcom/twitter/android/media/imageeditor/EditImageFragment;Z)Z
    .locals 0

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->O:Z

    return p1
.end method

.method private static b(Lcom/twitter/android/media/imageeditor/a;)V
    .locals 2

    .prologue
    .line 819
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/a;->a()Lcom/twitter/model/media/EditableImage;

    move-result-object v0

    .line 820
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/a;->g()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/model/media/EditableImage;->h:Ljava/util/List;

    .line 821
    return-void
.end method

.method private b(Lcom/twitter/model/media/EditableImage;)V
    .locals 0

    .prologue
    .line 726
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->B:Lcom/twitter/model/media/EditableImage;

    .line 727
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1098
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1099
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 1100
    const-string/jumbo v2, "sticker_edit_tooltip"

    .line 1101
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 1100
    invoke-static {v0, v2, v4, v5}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v1

    .line 1102
    invoke-virtual {v1}, Lcom/twitter/android/util/h;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1103
    invoke-static {v0, p1}, Lcom/twitter/ui/widget/Tooltip;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v2, 0x7f0a091a

    .line 1104
    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/Tooltip$a;->a(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v2, 0x7f0d0339

    .line 1105
    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/Tooltip$a;->b(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v2, 0x7f13034a

    .line 1106
    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/Tooltip$a;->d(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    .line 1107
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "sticker_edit_tooltip"

    invoke-virtual {v0, v2, v3}, Lcom/twitter/ui/widget/Tooltip$a;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)Lcom/twitter/ui/widget/Tooltip;

    .line 1108
    invoke-virtual {v1}, Lcom/twitter/android/util/h;->b()V

    .line 1110
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Z
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->O:Z

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Lcom/twitter/android/media/stickers/StickerFilteredImageView;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->f:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    return-object v0
.end method

.method private c(Lcom/twitter/media/filters/Filters;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1050
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    if-nez v0, :cond_0

    .line 1065
    :goto_0
    return-void

    .line 1053
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->y:Lcom/twitter/android/composer/ComposerType;

    sget-object v2, Lcom/twitter/android/composer/ComposerType;->c:Lcom/twitter/android/composer/ComposerType;

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lbpt;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1054
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1055
    invoke-virtual {p0, v1, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(ZZ)V

    .line 1058
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/a;->a()Lcom/twitter/model/media/EditableImage;

    move-result-object v0

    .line 1059
    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->i:Lcom/twitter/android/media/widget/FilterFilmstripView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/twitter/android/media/widget/FilterFilmstripView;->setFilterListener(Lcom/twitter/android/media/widget/FilterFilmstripView$a;)V

    .line 1060
    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->i:Lcom/twitter/android/media/widget/FilterFilmstripView;

    invoke-virtual {v0}, Lcom/twitter/model/media/EditableImage;->d()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    iget v4, v0, Lcom/twitter/model/media/EditableImage;->e:I

    invoke-virtual {v2, p1, v3, v4}, Lcom/twitter/android/media/widget/FilterFilmstripView;->a(Lcom/twitter/media/filters/Filters;Ljava/lang/String;I)V

    .line 1061
    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->i:Lcom/twitter/android/media/widget/FilterFilmstripView;

    iget v3, v0, Lcom/twitter/model/media/EditableImage;->c:I

    invoke-virtual {v2, v3}, Lcom/twitter/android/media/widget/FilterFilmstripView;->setSelectedFilter(I)V

    .line 1062
    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->i:Lcom/twitter/android/media/widget/FilterFilmstripView;

    iget v0, v0, Lcom/twitter/model/media/EditableImage;->d:F

    invoke-virtual {v2, v0}, Lcom/twitter/android/media/widget/FilterFilmstripView;->setIntensity(F)V

    .line 1063
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->i:Lcom/twitter/android/media/widget/FilterFilmstripView;

    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/FilterFilmstripView;->setFilterListener(Lcom/twitter/android/media/widget/FilterFilmstripView$a;)V

    .line 1064
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->i:Lcom/twitter/android/media/widget/FilterFilmstripView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/FilterFilmstripView;->a(Z)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1053
    goto :goto_1
.end method

.method private c(Lcom/twitter/model/media/EditableImage;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 824
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->A:Lcom/twitter/android/media/imageeditor/EditImageFragment$b;

    if-nez v0, :cond_0

    .line 841
    :goto_0
    return-void

    .line 827
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 828
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->A:Lcom/twitter/android/media/imageeditor/EditImageFragment$b;

    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->x:Lcom/twitter/media/filters/Filters;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->x:Lcom/twitter/media/filters/Filters;

    iget v2, p1, Lcom/twitter/model/media/EditableImage;->c:I

    .line 831
    invoke-virtual {v0, v2}, Lcom/twitter/media/filters/Filters;->b(I)Ljava/lang/String;

    move-result-object v0

    .line 828
    :goto_1
    invoke-interface {v1, p1, v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$b;->a(Lcom/twitter/model/media/EditableImage;Ljava/lang/String;)V

    goto :goto_0

    .line 831
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 834
    :cond_2
    new-instance v0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    .line 835
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0695

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 836
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/String;

    const-string/jumbo v4, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v4, v3, v6

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;-><init>(Ljava/lang/String;Landroid/content/Context;[Ljava/lang/String;)V

    const-string/jumbo v1, ":%s::"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->D:Ljava/lang/String;

    aput-object v3, v2, v6

    .line 837
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->f(Ljava/lang/String;)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    move-result-object v0

    .line 838
    invoke-virtual {v0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->a()Landroid/content/Intent;

    move-result-object v0

    .line 839
    invoke-virtual {p0, v0, v5}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Lcom/twitter/media/ui/image/MediaImageView;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->l:Lcom/twitter/media/ui/image/MediaImageView;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/media/imageeditor/EditImageFragment;)F
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->G:F

    return v0
.end method

.method static synthetic f(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Lcom/twitter/android/media/imageeditor/CropMediaImageView;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->g:Lcom/twitter/android/media/imageeditor/CropMediaImageView;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->k:Landroid/view/View;

    return-object v0
.end method

.method private g(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 952
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    if-nez v0, :cond_0

    .line 987
    :goto_0
    return-void

    .line 955
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->k:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 956
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/imageeditor/a;->a(Z)V

    .line 958
    if-nez p1, :cond_1

    .line 959
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, ""

    aput-object v2, v1, v4

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->D:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "image_attachment"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "crop"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "success"

    aput-object v3, v1, v2

    .line 960
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 961
    iget v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->F:I

    packed-switch v1, :pswitch_data_0

    .line 982
    :goto_1
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 985
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->m:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 986
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->l()V

    goto :goto_0

    .line 963
    :pswitch_0
    const-string/jumbo v1, "free_aspect"

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_1

    .line 967
    :pswitch_1
    const-string/jumbo v1, "original_aspect"

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_1

    .line 971
    :pswitch_2
    const-string/jumbo v1, "wide_aspect"

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_1

    .line 975
    :pswitch_3
    const-string/jumbo v1, "square_aspect"

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_1

    .line 961
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic h(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Lcom/twitter/android/media/imageeditor/EditImageFragment$c;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->s:Lcom/twitter/android/media/imageeditor/EditImageFragment$c;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Lcom/twitter/android/media/imageeditor/a;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Lcom/twitter/android/media/stickers/data/a;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->w:Lcom/twitter/android/media/stickers/data/a;

    return-object v0
.end method

.method private j()V
    .locals 4

    .prologue
    .line 844
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 845
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/media/EditableImage;

    .line 846
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/imageeditor/EditImageFragment$d;

    .line 847
    iget v3, v0, Lcom/twitter/android/media/imageeditor/EditImageFragment$d;->a:I

    iput v3, v1, Lcom/twitter/model/media/EditableImage;->c:I

    .line 848
    iget-object v3, v0, Lcom/twitter/android/media/imageeditor/EditImageFragment$d;->b:Lcom/twitter/util/math/c;

    iput-object v3, v1, Lcom/twitter/model/media/EditableImage;->f:Lcom/twitter/util/math/c;

    .line 849
    iget-boolean v3, v0, Lcom/twitter/android/media/imageeditor/EditImageFragment$d;->c:Z

    iput-boolean v3, v1, Lcom/twitter/model/media/EditableImage;->b:Z

    .line 850
    iget v0, v0, Lcom/twitter/android/media/imageeditor/EditImageFragment$d;->d:I

    iput v0, v1, Lcom/twitter/model/media/EditableImage;->e:I

    goto :goto_0

    .line 852
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->A:Lcom/twitter/android/media/imageeditor/EditImageFragment$b;

    if-eqz v0, :cond_1

    .line 853
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->A:Lcom/twitter/android/media/imageeditor/EditImageFragment$b;

    invoke-interface {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$b;->b()V

    .line 855
    :cond_1
    return-void
.end method

.method static synthetic k(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->D:Ljava/lang/String;

    return-object v0
.end method

.method private k()V
    .locals 4

    .prologue
    .line 859
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->D:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "editor"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "filters"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    .line 860
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 859
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 861
    return-void
.end method

.method static synthetic l(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->m:Landroid/view/View;

    return-object v0
.end method

.method private l()V
    .locals 3

    .prologue
    const v0, 0x7f0a07b3

    .line 990
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    if-nez v1, :cond_1

    .line 1018
    :cond_0
    :goto_0
    return-void

    .line 996
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v1}, Lcom/twitter/android/media/imageeditor/a;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 997
    const v1, 0x7f0a0693

    .line 998
    const v0, 0x7f0a031c

    .line 1010
    :goto_1
    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->p:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1011
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->q:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 1012
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->P:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 1013
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->q:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 999
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v1}, Lcom/twitter/android/media/imageeditor/a;->n()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1000
    const v1, 0x7f0a0691

    .line 1001
    const v0, 0x7f0a0077

    goto :goto_1

    .line 1002
    :cond_3
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->i:Lcom/twitter/android/media/widget/FilterFilmstripView;

    invoke-virtual {v1}, Lcom/twitter/android/media/widget/FilterFilmstripView;->b()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1003
    const v1, 0x7f0a0692

    .line 1004
    goto :goto_1

    .line 1006
    :cond_4
    const v1, 0x7f0a0349

    .line 1007
    goto :goto_1

    .line 1015
    :cond_5
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->q:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->P:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic m(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->n:Landroid/view/View;

    return-object v0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 1021
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    if-nez v0, :cond_0

    .line 1026
    :goto_0
    return-void

    .line 1024
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/a;->t()Z

    move-result v0

    .line 1025
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->o:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->K:I

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->L:I

    goto :goto_1
.end method

.method private n()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1069
    iget v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->E:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 1094
    :cond_0
    :goto_0
    return v0

    .line 1072
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1073
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 1074
    const-string/jumbo v3, "sticker_selector_tooltip"

    .line 1075
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 1074
    invoke-static {v1, v3, v4, v5}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v2

    .line 1076
    invoke-virtual {v2}, Lcom/twitter/android/util/h;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->E:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 1077
    const v0, 0x7f130354

    invoke-static {v1, v0}, Lcom/twitter/ui/widget/Tooltip;->a(Landroid/content/Context;I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v1, 0x7f0a091f

    .line 1078
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->a(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v1, 0x7f0d033a

    .line 1079
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->b(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v1, 0x7f13034a

    .line 1080
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->d(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/media/imageeditor/EditImageFragment$5;

    invoke-direct {v1, p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$5;-><init>(Lcom/twitter/android/media/imageeditor/EditImageFragment;)V

    .line 1081
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->a(Lcom/twitter/ui/widget/Tooltip$c;)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    .line 1090
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v3, "sticker_selector_tooltip"

    invoke-virtual {v0, v1, v3}, Lcom/twitter/ui/widget/Tooltip$a;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)Lcom/twitter/ui/widget/Tooltip;

    .line 1091
    invoke-virtual {v2}, Lcom/twitter/android/util/h;->b()V

    .line 1092
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 377
    const v0, 0x7f0400db

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 379
    const v0, 0x7f13034b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->f:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    .line 380
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->f:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    new-instance v2, Lcom/twitter/android/media/imageeditor/EditImageFragment$9;

    invoke-direct {v2, p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$9;-><init>(Lcom/twitter/android/media/imageeditor/EditImageFragment;)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/stickers/StickerFilteredImageView;->setStickerEditListener(Lcom/twitter/android/media/stickers/StickerFilteredImageView$a;)V

    .line 393
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->f:Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    new-instance v2, Lcom/twitter/android/media/imageeditor/EditImageFragment$10;

    invoke-direct {v2, p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$10;-><init>(Lcom/twitter/android/media/imageeditor/EditImageFragment;)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/stickers/StickerFilteredImageView;->setFilterRenderListener(Lcom/twitter/media/filters/b$a;)V

    .line 416
    const v0, 0x7f13034c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/imageeditor/CropMediaImageView;

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->g:Lcom/twitter/android/media/imageeditor/CropMediaImageView;

    .line 417
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->g:Lcom/twitter/android/media/imageeditor/CropMediaImageView;

    new-instance v2, Lcom/twitter/android/media/imageeditor/EditImageFragment$11;

    invoke-direct {v2, p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$11;-><init>(Lcom/twitter/android/media/imageeditor/EditImageFragment;)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->setOnImageLoadedListener(Lcom/twitter/media/ui/image/BaseMediaImageView$b;)V

    .line 445
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->g:Lcom/twitter/android/media/imageeditor/CropMediaImageView;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/CroppableImageView;

    .line 446
    new-instance v2, Lcom/twitter/android/media/imageeditor/EditImageFragment$12;

    invoke-direct {v2, p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$12;-><init>(Lcom/twitter/android/media/imageeditor/EditImageFragment;)V

    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/CroppableImageView;->setCropListener(Lcom/twitter/ui/widget/CroppableImageView$b;)V

    .line 452
    iget v2, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->G:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 453
    invoke-virtual {v0, v4}, Lcom/twitter/ui/widget/CroppableImageView;->setDraggableCorners(Z)V

    .line 454
    invoke-virtual {v0, v4}, Lcom/twitter/ui/widget/CroppableImageView;->setShowGrid(Z)V

    .line 457
    :cond_0
    const v0, 0x7f13034d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->h:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    .line 458
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->h:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    new-instance v2, Lcom/twitter/android/media/imageeditor/EditImageFragment$13;

    invoke-direct {v2, p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$13;-><init>(Lcom/twitter/android/media/imageeditor/EditImageFragment;)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;->setStickerSelectedListener(Lcom/twitter/android/media/imageeditor/stickers/c$b;)V

    .line 485
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->h:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->D:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;->setScribeSection(Ljava/lang/String;)V

    .line 487
    return-object v1
.end method

.method public a(Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;)V
    .locals 0

    .prologue
    .line 1341
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->C:Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;

    .line 1342
    return-void
.end method

.method public a(Lcom/twitter/android/media/imageeditor/EditImageFragment$b;)V
    .locals 0

    .prologue
    .line 568
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->A:Lcom/twitter/android/media/imageeditor/EditImageFragment$b;

    .line 569
    return-void
.end method

.method public a(Lcom/twitter/android/media/imageeditor/EditImageFragment$c;)V
    .locals 0

    .prologue
    .line 572
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->s:Lcom/twitter/android/media/imageeditor/EditImageFragment$c;

    .line 573
    return-void
.end method

.method a(Lcom/twitter/media/filters/Filters;)V
    .locals 1

    .prologue
    .line 1034
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->z:Lcom/twitter/android/media/imageeditor/EditImageFragment$g;

    .line 1035
    iget-boolean v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->H:Z

    if-eqz v0, :cond_1

    .line 1036
    if-eqz p1, :cond_0

    .line 1037
    invoke-virtual {p1}, Lcom/twitter/media/filters/Filters;->b()V

    .line 1047
    :cond_0
    :goto_0
    return-void

    .line 1041
    :cond_1
    if-nez p1, :cond_2

    .line 1042
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->g()V

    goto :goto_0

    .line 1045
    :cond_2
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->x:Lcom/twitter/media/filters/Filters;

    .line 1046
    invoke-direct {p0, p1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->c(Lcom/twitter/media/filters/Filters;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/media/EditableImage;)V
    .locals 1

    .prologue
    .line 324
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->t:Lcom/twitter/model/media/EditableImage;

    .line 325
    if-nez p1, :cond_1

    .line 333
    :cond_0
    :goto_0
    return-void

    .line 328
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 332
    invoke-direct {p0, p1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Lcom/twitter/model/media/EditableMedia;)V

    goto :goto_0
.end method

.method a(Z)V
    .locals 4

    .prologue
    .line 730
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(ZZ)V

    .line 731
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->m:Landroid/view/View;

    .line 732
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 733
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    .line 734
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/media/imageeditor/EditImageFragment$15;

    invoke-direct {v1, p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$15;-><init>(Lcom/twitter/android/media/imageeditor/EditImageFragment;)V

    .line 735
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 742
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 743
    return-void
.end method

.method public a(ZZ)V
    .locals 4

    .prologue
    const/16 v3, 0xfa

    .line 864
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 865
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->i:Lcom/twitter/android/media/widget/FilterFilmstripView;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/media/widget/FilterFilmstripView;->a(ZZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 866
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    .line 867
    if-eqz p1, :cond_2

    .line 868
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->j:Landroid/widget/ImageButton;

    iget v2, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->I:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 869
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    if-eqz v1, :cond_0

    .line 870
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v1}, Lcom/twitter/android/media/imageeditor/a;->e()V

    .line 872
    :cond_0
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 877
    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->l()V

    .line 879
    :cond_1
    return-void

    .line 874
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->j:Landroid/widget/ImageButton;

    iget v2, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->J:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 875
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/TransitionDrawable;->reverseTransition(I)V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 586
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->b()V

    .line 587
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    if-eqz v0, :cond_0

    .line 588
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/a;->q()V

    .line 590
    :cond_0
    return-void
.end method

.method b(Lcom/twitter/media/filters/Filters;)V
    .locals 2

    .prologue
    .line 1305
    iget-boolean v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->H:Z

    if-eqz v0, :cond_1

    .line 1306
    if-eqz p1, :cond_0

    .line 1307
    invoke-virtual {p1}, Lcom/twitter/media/filters/Filters;->b()V

    .line 1324
    :cond_0
    :goto_0
    return-void

    .line 1311
    :cond_1
    if-nez p1, :cond_2

    .line 1312
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->g()V

    goto :goto_0

    .line 1315
    :cond_2
    const/4 v0, 0x0

    .line 1316
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v1}, Lcom/twitter/android/media/imageeditor/a;->w()Lcom/twitter/media/filters/Filters;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1317
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/imageeditor/a;->a(Lcom/twitter/media/filters/Filters;)V

    .line 1318
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/a;->v()V

    .line 1319
    const/4 v0, 0x1

    .line 1321
    :cond_3
    if-nez v0, :cond_0

    .line 1322
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method b(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 746
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 747
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->m:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 748
    if-eqz p1, :cond_0

    .line 749
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->m:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 751
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->m:Landroid/view/View;

    new-instance v1, Lcom/twitter/android/media/imageeditor/EditImageFragment$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$2;-><init>(Lcom/twitter/android/media/imageeditor/EditImageFragment;)V

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->postOnAnimation(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 772
    :goto_0
    return-void

    .line 769
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->m:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 770
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->m:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method c(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 790
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->n:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 791
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->n:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 792
    if-eqz p1, :cond_0

    .line 793
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->n:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 795
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->n:Landroid/view/View;

    new-instance v1, Lcom/twitter/android/media/imageeditor/EditImageFragment$4;

    invoke-direct {v1, p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$4;-><init>(Lcom/twitter/android/media/imageeditor/EditImageFragment;)V

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->postOnAnimation(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 816
    :goto_0
    return-void

    .line 813
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->n:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 814
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->n:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method d()V
    .locals 4

    .prologue
    .line 775
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->n:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->n:Landroid/view/View;

    .line 776
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 777
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    .line 778
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/media/imageeditor/EditImageFragment$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$3;-><init>(Lcom/twitter/android/media/imageeditor/EditImageFragment;)V

    .line 779
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 786
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 787
    return-void
.end method

.method d(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 901
    invoke-virtual {p0, v2, p1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(ZZ)V

    .line 902
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    if-eqz v0, :cond_0

    .line 903
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->b(Lcom/twitter/android/media/imageeditor/a;)V

    .line 904
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/a;->i()V

    .line 906
    :cond_0
    iget v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->G:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 907
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->k:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 909
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->m:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 910
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->l()V

    .line 911
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 882
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/a;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 883
    iget-boolean v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->M:Z

    if-eqz v0, :cond_0

    .line 884
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->j()V

    .line 898
    :goto_0
    return-void

    .line 886
    :cond_0
    invoke-direct {p0, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->g(Z)V

    goto :goto_0

    .line 890
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/a;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 891
    invoke-virtual {p0, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->e(Z)V

    goto :goto_0

    .line 894
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->A:Lcom/twitter/android/media/imageeditor/EditImageFragment$b;

    if-eqz v0, :cond_3

    .line 895
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->j()V

    .line 897
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->r:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method e(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 938
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    if-nez v0, :cond_0

    .line 949
    :goto_0
    return-void

    .line 941
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->n:Landroid/view/View;

    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020064

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 942
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/a;->c()V

    .line 943
    invoke-virtual {p0, v3}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->b(Z)V

    .line 944
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->m:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 945
    if-eqz p1, :cond_1

    .line 946
    invoke-virtual {p0, v4, v4}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(ZZ)V

    .line 948
    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->l()V

    goto :goto_0
.end method

.method f()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 915
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    if-nez v0, :cond_0

    .line 935
    :goto_0
    return-void

    .line 918
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/a;->h()I

    move-result v0

    invoke-static {}, Lcom/twitter/android/media/stickers/b;->a()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 920
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0694

    .line 921
    invoke-virtual {p0, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 919
    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 923
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 927
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->n:Landroid/view/View;

    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f110030

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 928
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/a;->b()V

    .line 929
    invoke-virtual {p0, v3, v3}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(ZZ)V

    .line 930
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->m:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 931
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->l()V

    .line 933
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, ""

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->D:Ljava/lang/String;

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string/jumbo v3, "editor"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "sticker"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "show"

    aput-object v3, v1, v2

    .line 934
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 933
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0
.end method

.method f(Z)V
    .locals 5

    .prologue
    .line 1113
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getView()Landroid/view/View;

    move-result-object v1

    .line 1114
    if-eqz v1, :cond_0

    .line 1115
    sget-object v2, Lcom/twitter/android/media/imageeditor/EditImageFragment;->c:[I

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    .line 1116
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 1115
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1119
    :cond_0
    return-void
.end method

.method g()V
    .locals 3

    .prologue
    .line 1029
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Filters failed to load"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 1030
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0a0418

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1031
    return-void
.end method

.method h()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1327
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1338
    :cond_0
    :goto_0
    return-void

    .line 1330
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1331
    if-eqz v0, :cond_0

    .line 1334
    const v1, 0x7f0a0418

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 1335
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1336
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->j:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1337
    invoke-virtual {p0, v2, v3}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(ZZ)V

    goto :goto_0
.end method

.method public i()V
    .locals 2

    .prologue
    .line 1345
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/filters/Filters;

    .line 1346
    invoke-virtual {v0}, Lcom/twitter/media/filters/Filters;->b()V

    goto :goto_0

    .line 1348
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    if-eqz v0, :cond_1

    .line 1349
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/a;->r()V

    .line 1350
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/a;->s()V

    .line 1352
    :cond_1
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 554
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 559
    new-instance v0, Lcom/twitter/android/media/imageeditor/EditImageFragment$h;

    invoke-direct {v0, p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$h;-><init>(Lcom/twitter/android/media/imageeditor/EditImageFragment;)V

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->z:Lcom/twitter/android/media/imageeditor/EditImageFragment$g;

    .line 560
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->z:Lcom/twitter/android/media/imageeditor/EditImageFragment$g;

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment$g;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 562
    if-eqz p1, :cond_0

    const-string/jumbo v0, "is_cropping"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 563
    invoke-virtual {p0, v2}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->d(Z)V

    .line 565
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1123
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 1124
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    invoke-static {p3}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1125
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    if-eqz v0, :cond_0

    .line 1126
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/a;->a()Lcom/twitter/model/media/EditableImage;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->c(Lcom/twitter/model/media/EditableImage;)V

    .line 1130
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 646
    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    if-nez v2, :cond_1

    .line 723
    :cond_0
    :goto_0
    return-void

    .line 649
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 650
    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    .line 652
    :sswitch_0
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v1}, Lcom/twitter/android/media/imageeditor/a;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 653
    invoke-virtual {p0, v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->e(Z)V

    goto :goto_0

    .line 654
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v1}, Lcom/twitter/android/media/imageeditor/a;->m()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 655
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->b(Lcom/twitter/android/media/imageeditor/a;)V

    .line 656
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/a;->a()Lcom/twitter/model/media/EditableImage;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->c(Lcom/twitter/model/media/EditableImage;)V

    goto :goto_0

    .line 657
    :cond_3
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v1}, Lcom/twitter/android/media/imageeditor/a;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 658
    iget-boolean v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->M:Z

    if-eqz v1, :cond_4

    .line 659
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v1}, Lcom/twitter/android/media/imageeditor/a;->j()V

    .line 660
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-static {v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->b(Lcom/twitter/android/media/imageeditor/a;)V

    .line 661
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v1}, Lcom/twitter/android/media/imageeditor/a;->a()Lcom/twitter/model/media/EditableImage;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->c(Lcom/twitter/model/media/EditableImage;)V

    .line 665
    :goto_1
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->r:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 663
    :cond_4
    invoke-direct {p0, v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->g(Z)V

    goto :goto_1

    .line 670
    :sswitch_1
    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v2}, Lcom/twitter/android/media/imageeditor/a;->d()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 671
    invoke-virtual {p0, v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->e(Z)V

    goto :goto_0

    .line 672
    :cond_5
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/a;->n()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 673
    iget-boolean v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->M:Z

    if-eqz v0, :cond_6

    .line 674
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->j()V

    goto :goto_0

    .line 676
    :cond_6
    invoke-direct {p0, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->g(Z)V

    goto :goto_0

    .line 679
    :cond_7
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->j()V

    goto :goto_0

    .line 684
    :sswitch_2
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->m()V

    goto :goto_0

    .line 688
    :sswitch_3
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->f()V

    goto :goto_0

    .line 692
    :sswitch_4
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->k()V

    .line 693
    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->i:Lcom/twitter/android/media/widget/FilterFilmstripView;

    invoke-virtual {v2}, Lcom/twitter/android/media/widget/FilterFilmstripView;->b()Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    :cond_8
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(ZZ)V

    goto/16 :goto_0

    .line 697
    :sswitch_5
    invoke-virtual {p0, v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->d(Z)V

    goto/16 :goto_0

    .line 701
    :sswitch_6
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    const/16 v2, -0x5a

    invoke-virtual {v0, v2, v1}, Lcom/twitter/android/media/imageeditor/a;->a(IZ)V

    goto/16 :goto_0

    .line 705
    :sswitch_7
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/a;->k()V

    .line 706
    iput v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->F:I

    goto/16 :goto_0

    .line 710
    :sswitch_8
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    const v1, 0x3fe38e39

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/a;->a(F)V

    .line 711
    const/4 v0, 0x3

    iput v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->F:I

    goto/16 :goto_0

    .line 715
    :sswitch_9
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/a;->a(F)V

    .line 716
    const/4 v0, 0x4

    iput v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->F:I

    goto/16 :goto_0

    .line 650
    :sswitch_data_0
    .sparse-switch
        0x7f130164 -> :sswitch_0
        0x7f130351 -> :sswitch_2
        0x7f130352 -> :sswitch_4
        0x7f130353 -> :sswitch_5
        0x7f130354 -> :sswitch_3
        0x7f130356 -> :sswitch_7
        0x7f130357 -> :sswitch_8
        0x7f130358 -> :sswitch_9
        0x7f130359 -> :sswitch_6
        0x7f13035b -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 337
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 339
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->I()Lcom/twitter/app/common/base/b;

    move-result-object v1

    .line 340
    const-string/jumbo v0, "scribe_section"

    invoke-virtual {v1, v0}, Lcom/twitter/app/common/base/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->D:Ljava/lang/String;

    .line 341
    const-string/jumbo v0, "lock_to_initial"

    invoke-virtual {v1, v0, v6}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->M:Z

    .line 342
    const-string/jumbo v0, "composer_type"

    invoke-virtual {v1, v0}, Lcom/twitter/app/common/base/b;->h(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/composer/ComposerType;

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->y:Lcom/twitter/android/composer/ComposerType;

    .line 343
    const-string/jumbo v0, "force_crop_ratio"

    invoke-virtual {v1, v0}, Lcom/twitter/app/common/base/b;->d(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->G:F

    .line 344
    const-string/jumbo v0, "done_button_text"

    invoke-virtual {v1, v0}, Lcom/twitter/app/common/base/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->P:Ljava/lang/String;

    .line 346
    const-string/jumbo v0, "sticker_catalog_repo"

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->b_(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/stickers/data/a;

    .line 347
    if-eqz v0, :cond_0

    .line 351
    :goto_0
    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->w:Lcom/twitter/android/media/stickers/data/a;

    .line 353
    const-string/jumbo v0, "sticker_catalog_repo"

    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->w:Lcom/twitter/android/media/stickers/data/a;

    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    if-nez p1, :cond_1

    .line 357
    const-string/jumbo v0, "initial_type"

    invoke-virtual {v1, v0}, Lcom/twitter/app/common/base/b;->b(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->E:I

    .line 358
    iput v6, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->N:I

    .line 373
    :goto_1
    return-void

    .line 347
    :cond_0
    new-instance v0, Lcom/twitter/android/media/stickers/data/a;

    .line 350
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 351
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    new-instance v3, Lbrn;

    invoke-direct {v3, v7}, Lbrn;-><init>(I)V

    invoke-direct {v0, v2, v4, v5, v3}, Lcom/twitter/android/media/stickers/data/a;-><init>(Landroid/content/Context;JLbrn;)V

    goto :goto_0

    .line 360
    :cond_1
    const-string/jumbo v0, "sticker_tab_position"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->N:I

    .line 361
    const-string/jumbo v0, "image"

    sget-object v1, Lcom/twitter/model/media/EditableImage;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableImage;

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->u:Lcom/twitter/model/media/EditableImage;

    .line 362
    const-string/jumbo v0, "is_cropping"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "crop_rect"

    sget-object v1, Lcom/twitter/util/math/c;->a:Lcom/twitter/util/serialization/l;

    .line 363
    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/math/c;

    .line 365
    :goto_2
    if-eqz v0, :cond_3

    .line 366
    new-instance v1, Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;

    const-string/jumbo v2, "rotation"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-direct {v1, v2, v0}, Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;-><init>(ILcom/twitter/util/math/c;)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;)V

    .line 367
    iput v7, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->E:I

    goto :goto_1

    .line 363
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 370
    :cond_3
    const-string/jumbo v0, "editor_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->E:I

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 594
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->H:Z

    .line 595
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->z:Lcom/twitter/android/media/imageeditor/EditImageFragment$g;

    if-eqz v0, :cond_0

    .line 596
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->z:Lcom/twitter/android/media/imageeditor/EditImageFragment$g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment$g;->cancel(Z)Z

    .line 599
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->x:Lcom/twitter/media/filters/Filters;

    if-eqz v0, :cond_1

    .line 600
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->x:Lcom/twitter/media/filters/Filters;

    invoke-virtual {v0}, Lcom/twitter/media/filters/Filters;->b()V

    .line 602
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->i()V

    .line 603
    sget-object v0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment$e;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$e;->a()V

    .line 604
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->onDestroy()V

    .line 605
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 609
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 611
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    if-eqz v1, :cond_2

    .line 612
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-static {v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->b(Lcom/twitter/android/media/imageeditor/a;)V

    .line 613
    const-string/jumbo v1, "image"

    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    .line 616
    invoke-virtual {v2}, Lcom/twitter/android/media/imageeditor/a;->a()Lcom/twitter/model/media/EditableImage;

    move-result-object v2

    sget-object v3, Lcom/twitter/model/media/EditableImage;->a:Lcom/twitter/util/serialization/l;

    .line 613
    invoke-static {p1, v1, v2, v3}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/os/Bundle;

    .line 618
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v1}, Lcom/twitter/android/media/imageeditor/a;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 619
    const-string/jumbo v1, "is_cropping"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 620
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v1}, Lcom/twitter/android/media/imageeditor/a;->o()Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;

    move-result-object v1

    .line 621
    const-string/jumbo v2, "rotation"

    iget v3, v1, Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;->a:I

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 622
    const-string/jumbo v2, "crop_rect"

    iget-object v1, v1, Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;->b:Lcom/twitter/util/math/c;

    sget-object v3, Lcom/twitter/util/math/c;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v2, v1, v3}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/os/Bundle;

    .line 630
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v1}, Lcom/twitter/android/media/imageeditor/a;->n()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 631
    const/4 v0, 0x2

    .line 639
    :cond_1
    :goto_0
    const-string/jumbo v1, "sticker_tab_position"

    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v2}, Lcom/twitter/android/media/imageeditor/a;->f()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 640
    const-string/jumbo v1, "editor_type"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 642
    :cond_2
    return-void

    .line 632
    :cond_3
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v1}, Lcom/twitter/android/media/imageeditor/a;->d()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 633
    const/4 v0, 0x3

    goto :goto_0

    .line 634
    :cond_4
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->i:Lcom/twitter/android/media/widget/FilterFilmstripView;

    invoke-virtual {v1}, Lcom/twitter/android/media/widget/FilterFilmstripView;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 637
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 492
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/abs/AbsFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 494
    const v0, 0x7f02039b

    iput v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->L:I

    .line 495
    const v0, 0x7f02039c

    iput v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->K:I

    .line 496
    const v0, 0x7f020391

    iput v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->J:I

    .line 497
    const v0, 0x7f020392

    iput v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->I:I

    .line 499
    const v0, 0x7f130352

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->j:Landroid/widget/ImageButton;

    .line 500
    const v0, 0x7f130351

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->o:Landroid/widget/ImageButton;

    .line 501
    const v0, 0x7f13034e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->l:Lcom/twitter/media/ui/image/MediaImageView;

    .line 502
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->B:Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_1

    .line 503
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->l:Lcom/twitter/media/ui/image/MediaImageView;

    new-instance v2, Lcom/twitter/android/media/imageeditor/EditImageFragment$14;

    invoke-direct {v2, p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$14;-><init>(Lcom/twitter/android/media/imageeditor/EditImageFragment;)V

    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setOnImageLoadedListener(Lcom/twitter/media/ui/image/BaseMediaImageView$b;)V

    .line 511
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->l:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->B:Lcom/twitter/model/media/EditableImage;

    invoke-static {v2, v3}, Lbru;->a(Landroid/content/Context;Lcom/twitter/model/media/EditableMedia;)Lcom/twitter/media/request/a$a;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 516
    :cond_0
    :goto_0
    const v0, 0x7f13034f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/FilterFilmstripView;

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->i:Lcom/twitter/android/media/widget/FilterFilmstripView;

    .line 518
    const v0, 0x7f130166

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->p:Landroid/widget/TextView;

    .line 520
    const v0, 0x7f130355

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->k:Landroid/view/View;

    .line 522
    const v0, 0x7f130350

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->m:Landroid/view/View;

    .line 523
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    .line 524
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 526
    const v0, 0x7f13035a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->n:Landroid/view/View;

    .line 528
    sget-object v2, Lcom/twitter/android/media/imageeditor/EditImageFragment;->b:[I

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_2

    aget v4, v2, v0

    .line 529
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 528
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 512
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->s:Lcom/twitter/android/media/imageeditor/EditImageFragment$c;

    if-eqz v0, :cond_0

    .line 513
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->s:Lcom/twitter/android/media/imageeditor/EditImageFragment$c;

    invoke-interface {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$c;->a()V

    goto :goto_0

    .line 532
    :cond_2
    const v0, 0x7f130164

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->q:Landroid/widget/TextView;

    .line 533
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->q:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 534
    const v0, 0x7f13035b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->r:Landroid/view/View;

    .line 535
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->r:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 537
    invoke-virtual {p0, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->f(Z)V

    .line 539
    const v0, 0x7f13035c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 540
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->p:Landroid/widget/TextView;

    const-string/jumbo v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 542
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->y:Lcom/twitter/android/composer/ComposerType;

    sget-object v2, Lcom/twitter/android/composer/ComposerType;->c:Lcom/twitter/android/composer/ComposerType;

    if-ne v0, v2, :cond_5

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Lbpt;->a(Z)Z

    move-result v0

    if-nez v0, :cond_3

    .line 543
    const v0, 0x7f130354

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 546
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->t:Lcom/twitter/model/media/EditableImage;

    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->u:Lcom/twitter/model/media/EditableImage;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableImage;

    .line 547
    if-eqz v0, :cond_4

    .line 548
    invoke-direct {p0, v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Lcom/twitter/model/media/EditableMedia;)V

    .line 550
    :cond_4
    return-void

    :cond_5
    move v0, v1

    .line 542
    goto :goto_2
.end method

.method public q_()V
    .locals 1

    .prologue
    .line 577
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    if-eqz v0, :cond_0

    .line 578
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/a;->p()V

    .line 579
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment;->v:Lcom/twitter/android/media/imageeditor/a;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->b(Lcom/twitter/android/media/imageeditor/a;)V

    .line 581
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->q_()V

    .line 582
    return-void
.end method
