.class Lcom/twitter/android/media/imageeditor/EditImageFragment$11;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/media/ui/image/MediaImageView$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/imageeditor/EditImageFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/imageeditor/EditImageFragment;)V
    .locals 0

    .prologue
    .line 417
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$11;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/media/ui/image/BaseMediaImageView;Lcom/twitter/media/request/ImageResponse;)V
    .locals 0

    .prologue
    .line 417
    check-cast p1, Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/media/imageeditor/EditImageFragment$11;->a(Lcom/twitter/media/ui/image/MediaImageView;Lcom/twitter/media/request/ImageResponse;)V

    return-void
.end method

.method public a(Lcom/twitter/media/ui/image/MediaImageView;Lcom/twitter/media/request/ImageResponse;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 420
    invoke-virtual {p2}, Lcom/twitter/media/request/ImageResponse;->e()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 421
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$11;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->f(Z)V

    .line 423
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$11;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->e(Lcom/twitter/android/media/imageeditor/EditImageFragment;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 424
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$11;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->f(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Lcom/twitter/android/media/imageeditor/CropMediaImageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/CroppableImageView;

    .line 425
    new-instance v1, Lcom/twitter/android/media/imageeditor/EditImageFragment$11$1;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$11$1;-><init>(Lcom/twitter/android/media/imageeditor/EditImageFragment$11;Lcom/twitter/ui/widget/CroppableImageView;)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/CroppableImageView;->post(Ljava/lang/Runnable;)Z

    .line 434
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$11;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->g(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 436
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$11;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Lcom/twitter/android/media/imageeditor/EditImageFragment;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 437
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$11;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->d(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Lcom/twitter/media/ui/image/MediaImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setVisibility(I)V

    .line 438
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$11;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->d(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Lcom/twitter/media/ui/image/MediaImageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->h()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$11;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->h(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Lcom/twitter/android/media/imageeditor/EditImageFragment$c;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 440
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$11;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->h(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Lcom/twitter/android/media/imageeditor/EditImageFragment$c;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$c;->a()V

    .line 443
    :cond_2
    return-void
.end method
