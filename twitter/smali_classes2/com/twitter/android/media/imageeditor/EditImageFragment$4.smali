.class Lcom/twitter/android/media/imageeditor/EditImageFragment$4;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/media/imageeditor/EditImageFragment;->c(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/imageeditor/EditImageFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/imageeditor/EditImageFragment;)V
    .locals 0

    .prologue
    .line 795
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$4;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 798
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$4;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->m(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$4;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-static {v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->m(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 799
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$4;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->m(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 800
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 801
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    .line 802
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/media/imageeditor/EditImageFragment$4$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/media/imageeditor/EditImageFragment$4$1;-><init>(Lcom/twitter/android/media/imageeditor/EditImageFragment$4;)V

    .line 803
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 810
    return-void
.end method
