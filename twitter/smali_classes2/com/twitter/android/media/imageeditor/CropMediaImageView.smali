.class public Lcom/twitter/android/media/imageeditor/CropMediaImageView;
.super Lcom/twitter/media/ui/image/MediaImageView;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/ui/widget/CroppableImageView;

.field private k:Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/media/imageeditor/CropMediaImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    .line 35
    const v0, 0x7f040084

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/media/ui/image/MediaImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILandroid/widget/ImageView;Z)V

    .line 37
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/CroppableImageView;

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->a:Lcom/twitter/ui/widget/CroppableImageView;

    .line 38
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/media/request/ImageResponse;Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    .line 42
    invoke-super {p0, p1, p2}, Lcom/twitter/media/ui/image/MediaImageView;->a(Lcom/twitter/media/request/ImageResponse;Landroid/graphics/drawable/Drawable;)V

    .line 43
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->a:Lcom/twitter/ui/widget/CroppableImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/CroppableImageView;->setShowCrop(Z)V

    .line 44
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->k:Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->a:Lcom/twitter/ui/widget/CroppableImageView;

    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->k:Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;

    iget-object v1, v1, Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;->b:Lcom/twitter/util/math/c;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/CroppableImageView;->setImageSelection(Lcom/twitter/util/math/c;)V

    .line 46
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->a:Lcom/twitter/ui/widget/CroppableImageView;

    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->k:Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;

    iget v1, v1, Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;->a:I

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/CroppableImageView;->setRotation(I)V

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->a:Lcom/twitter/ui/widget/CroppableImageView;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/CroppableImageView;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 49
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 50
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 51
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->a:Lcom/twitter/ui/widget/CroppableImageView;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/CroppableImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 53
    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/media/request/a$a;Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 56
    iput-object p2, p0, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->k:Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;

    .line 57
    invoke-virtual {p0, p1, v1}, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->a(Lcom/twitter/media/request/a$a;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 59
    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->a:Lcom/twitter/ui/widget/CroppableImageView;

    invoke-virtual {v2, v1}, Lcom/twitter/ui/widget/CroppableImageView;->setShowCrop(Z)V

    .line 71
    :goto_0
    return v0

    .line 63
    :cond_0
    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->a:Lcom/twitter/ui/widget/CroppableImageView;

    invoke-virtual {v2, v0}, Lcom/twitter/ui/widget/CroppableImageView;->setShowCrop(Z)V

    .line 64
    if-nez p2, :cond_1

    .line 65
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->a:Lcom/twitter/ui/widget/CroppableImageView;

    sget-object v2, Lcom/twitter/util/math/c;->c:Lcom/twitter/util/math/c;

    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/CroppableImageView;->setImageSelection(Lcom/twitter/util/math/c;)V

    .line 66
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->a:Lcom/twitter/ui/widget/CroppableImageView;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/CroppableImageView;->setRotation(I)V

    :goto_1
    move v0, v1

    .line 71
    goto :goto_0

    .line 68
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->a:Lcom/twitter/ui/widget/CroppableImageView;

    iget-object v2, p2, Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;->b:Lcom/twitter/util/math/c;

    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/CroppableImageView;->setImageSelection(Lcom/twitter/util/math/c;)V

    .line 69
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->a:Lcom/twitter/ui/widget/CroppableImageView;

    iget v2, p2, Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;->a:I

    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/CroppableImageView;->setRotation(I)V

    goto :goto_1
.end method

.method public getCropState()Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;
    .locals 3

    .prologue
    .line 76
    new-instance v0, Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;

    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->a:Lcom/twitter/ui/widget/CroppableImageView;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/CroppableImageView;->getImageRotation()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/CropMediaImageView;->a:Lcom/twitter/ui/widget/CroppableImageView;

    invoke-virtual {v2}, Lcom/twitter/ui/widget/CroppableImageView;->getNormalizedImageSelection()Lcom/twitter/util/math/c;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/media/imageeditor/CropMediaImageView$a;-><init>(ILcom/twitter/util/math/c;)V

    return-object v0
.end method
