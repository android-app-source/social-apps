.class Lcom/twitter/android/media/imageeditor/a$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcpv;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/media/imageeditor/a;->g()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcpv",
        "<",
        "Lcdy;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:F

.field final synthetic b:Lcom/twitter/util/math/c;

.field final synthetic c:I

.field final synthetic d:F

.field final synthetic e:Lcom/twitter/android/media/imageeditor/a;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/imageeditor/a;FLcom/twitter/util/math/c;IF)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/a$3;->e:Lcom/twitter/android/media/imageeditor/a;

    iput p2, p0, Lcom/twitter/android/media/imageeditor/a$3;->a:F

    iput-object p3, p0, Lcom/twitter/android/media/imageeditor/a$3;->b:Lcom/twitter/util/math/c;

    iput p4, p0, Lcom/twitter/android/media/imageeditor/a$3;->c:I

    iput p5, p0, Lcom/twitter/android/media/imageeditor/a$3;->d:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcdy;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 178
    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdy;

    .line 179
    iget v1, p0, Lcom/twitter/android/media/imageeditor/a$3;->a:F

    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/a$3;->b:Lcom/twitter/util/math/c;

    iget v3, p0, Lcom/twitter/android/media/imageeditor/a$3;->c:I

    invoke-virtual {v0, v1, v2, v3}, Lcdy;->a(FLcom/twitter/util/math/c;I)Landroid/graphics/Matrix;

    move-result-object v1

    .line 181
    iget-object v0, v0, Lcdy;->b:Lcdu;

    iget-object v0, v0, Lcdu;->j:Lcea;

    iget v0, v0, Lcea;->b:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    .line 182
    new-instance v2, Landroid/graphics/RectF;

    const/high16 v3, -0x41000000    # -0.5f

    neg-float v4, v0

    const/high16 v5, 0x3f000000    # 0.5f

    invoke-direct {v2, v3, v4, v5, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 183
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 184
    iget v0, v2, Landroid/graphics/RectF;->left:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, v2, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/twitter/android/media/imageeditor/a$3;->d:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, v2, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v6

    if-ltz v0, :cond_0

    iget v0, v2, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v6

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 175
    check-cast p1, Lcdy;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/imageeditor/a$3;->a(Lcdy;)Z

    move-result v0

    return v0
.end method
