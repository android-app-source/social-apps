.class Lcom/twitter/android/media/imageeditor/EditImageFragment$13;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/imageeditor/stickers/c$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/imageeditor/EditImageFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/imageeditor/EditImageFragment;)V
    .locals 0

    .prologue
    .line 458
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$13;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcdu;ILandroid/graphics/drawable/Drawable;)V
    .locals 6

    .prologue
    .line 464
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$13;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->i(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Lcom/twitter/android/media/imageeditor/a;

    move-result-object v0

    if-nez v0, :cond_0

    .line 483
    :goto_0
    return-void

    .line 467
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$13;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->j(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Lcom/twitter/android/media/stickers/data/a;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/stickers/data/a;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/stickers/data/a;->a(Lcdu;)V

    .line 469
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$13;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->i(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Lcom/twitter/android/media/imageeditor/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/a;->a()Lcom/twitter/model/media/EditableImage;

    move-result-object v0

    .line 470
    new-instance v1, Lcom/twitter/android/media/stickers/StickerView$a;

    iget v2, v0, Lcom/twitter/model/media/EditableImage;->e:I

    neg-int v2, v2

    int-to-float v2, v2

    invoke-direct {v1, p1, v2}, Lcom/twitter/android/media/stickers/StickerView$a;-><init>(Lcdu;F)V

    .line 471
    new-instance v2, Lcom/twitter/android/media/stickers/StickerView;

    iget-object v3, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$13;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-virtual {v3}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, v1, p3}, Lcom/twitter/android/media/stickers/StickerView;-><init>(Landroid/content/Context;Lcom/twitter/android/media/stickers/StickerView$a;Landroid/graphics/drawable/Drawable;)V

    .line 472
    iget-object v1, p1, Lcdu;->j:Lcea;

    iget v1, v1, Lcea;->b:F

    invoke-virtual {v2, v1}, Lcom/twitter/android/media/stickers/StickerView;->setAspectRatio(F)V

    .line 473
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "STICKER:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p1, Lcdu;->h:J

    .line 474
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, v0, Lcom/twitter/model/media/EditableImage;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 475
    invoke-virtual {v2, v0}, Lcom/twitter/android/media/stickers/StickerView;->setTag(Ljava/lang/Object;)V

    .line 476
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$13;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-static {v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->c(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/twitter/android/media/stickers/StickerFilteredImageView;->a(Lcom/twitter/android/media/stickers/StickerView;)V

    .line 477
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$13;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-static {v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->i(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Lcom/twitter/android/media/imageeditor/a;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Lcom/twitter/android/media/imageeditor/a;)V

    .line 478
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$13;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->e(Z)V

    .line 480
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$13;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-static {v1, v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Lcom/twitter/android/media/imageeditor/EditImageFragment;Ljava/lang/String;)V

    .line 482
    iget-wide v0, p1, Lcdu;->h:J

    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$13;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-static {v2}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->k(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, p2, v2}, Lcom/twitter/android/media/imageeditor/stickers/b;->a(JILjava/lang/String;)V

    goto/16 :goto_0
.end method
