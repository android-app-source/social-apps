.class Lcom/twitter/android/media/imageeditor/EditImageFragment$h;
.super Lcom/twitter/android/media/imageeditor/EditImageFragment$g;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/imageeditor/EditImageFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "h"
.end annotation


# instance fields
.field final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/android/media/imageeditor/EditImageFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/twitter/android/media/imageeditor/EditImageFragment;)V
    .locals 2

    .prologue
    .line 1170
    invoke-virtual {p1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment$g;-><init>(Landroid/content/Context;Lcom/twitter/android/media/imageeditor/EditImageFragment$1;)V

    .line 1171
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$h;->a:Ljava/lang/ref/WeakReference;

    .line 1172
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/media/filters/Filters;)V
    .locals 2

    .prologue
    .line 1176
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$h;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/imageeditor/EditImageFragment;

    .line 1177
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1178
    :cond_0
    if-eqz p1, :cond_1

    .line 1179
    invoke-virtual {p1}, Lcom/twitter/media/filters/Filters;->b()V

    .line 1184
    :cond_1
    :goto_0
    return-void

    .line 1182
    :cond_2
    invoke-virtual {v0, p1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Lcom/twitter/media/filters/Filters;)V

    goto :goto_0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1166
    check-cast p1, Lcom/twitter/media/filters/Filters;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/imageeditor/EditImageFragment$h;->a(Lcom/twitter/media/filters/Filters;)V

    return-void
.end method
