.class public Lcom/twitter/android/media/imageeditor/EditImageFragment$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/imageeditor/EditImageFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/twitter/app/common/base/b$a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1383
    new-instance v0, Lcom/twitter/app/common/base/b$a;

    invoke-direct {v0}, Lcom/twitter/app/common/base/b$a;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;->a:Lcom/twitter/app/common/base/b$a;

    .line 1384
    return-void
.end method


# virtual methods
.method public a(F)Lcom/twitter/android/media/imageeditor/EditImageFragment$a;
    .locals 2

    .prologue
    .line 1417
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;->a:Lcom/twitter/app/common/base/b$a;

    const-string/jumbo v1, "force_crop_ratio"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/app/common/base/b$a;->a(Ljava/lang/String;F)Lcom/twitter/app/common/base/b$a;

    .line 1418
    return-object p0
.end method

.method public a(I)Lcom/twitter/android/media/imageeditor/EditImageFragment$a;
    .locals 2

    .prologue
    .line 1393
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;->a:Lcom/twitter/app/common/base/b$a;

    const-string/jumbo v1, "initial_type"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/app/common/base/b$a;->a(Ljava/lang/String;I)Lcom/twitter/app/common/base/b$a;

    .line 1394
    return-object p0
.end method

.method public a(Lcom/twitter/android/composer/ComposerType;)Lcom/twitter/android/media/imageeditor/EditImageFragment$a;
    .locals 2

    .prologue
    .line 1399
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;->a:Lcom/twitter/app/common/base/b$a;

    const-string/jumbo v1, "composer_type"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/app/common/base/b$a;->a(Ljava/lang/String;Landroid/os/Parcelable;)Lcom/twitter/app/common/base/b$a;

    .line 1400
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/android/media/imageeditor/EditImageFragment$a;
    .locals 2

    .prologue
    .line 1388
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;->a:Lcom/twitter/app/common/base/b$a;

    const-string/jumbo v1, "scribe_section"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/app/common/base/b$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/app/common/base/b$a;

    .line 1389
    return-object p0
.end method

.method public a(Z)Lcom/twitter/android/media/imageeditor/EditImageFragment$a;
    .locals 2

    .prologue
    .line 1412
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;->a:Lcom/twitter/app/common/base/b$a;

    const-string/jumbo v1, "lock_to_initial"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/app/common/base/b$a;->a(Ljava/lang/String;Z)Lcom/twitter/app/common/base/b$a;

    .line 1413
    return-object p0
.end method

.method public a()Lcom/twitter/android/media/imageeditor/EditImageFragment;
    .locals 2

    .prologue
    .line 1430
    new-instance v0, Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-direct {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;-><init>()V

    .line 1431
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;->a:Lcom/twitter/app/common/base/b$a;

    invoke-virtual {v1}, Lcom/twitter/app/common/base/b$a;->c()Lcom/twitter/app/common/base/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Lcom/twitter/app/common/base/b;)V

    .line 1432
    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/android/media/imageeditor/EditImageFragment$a;
    .locals 2

    .prologue
    .line 1422
    if-eqz p1, :cond_0

    .line 1423
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$a;->a:Lcom/twitter/app/common/base/b$a;

    const-string/jumbo v1, "done_button_text"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/app/common/base/b$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/app/common/base/b$a;

    .line 1425
    :cond_0
    return-object p0
.end method
