.class Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/client/x$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;)V
    .locals 1

    .prologue
    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity$a;->a:Ljava/lang/ref/WeakReference;

    .line 183
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;

    .line 188
    if-eqz v0, :cond_0

    .line 189
    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->c(Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;)V

    .line 191
    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;

    .line 196
    if-eqz v0, :cond_0

    .line 197
    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;->d(Lcom/twitter/android/media/imageeditor/ProfilePhotoEditImageActivity;)V

    .line 199
    :cond_0
    return-void
.end method
