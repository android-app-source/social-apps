.class Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView$4;
.super Landroid/support/v4/view/ViewPager$SimpleOnPageChangeListener;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;->setStickerFeaturedCategoryData(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView$4;->b:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    iput-object p2, p0, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView$4;->a:Ljava/util/List;

    invoke-direct {p0}, Landroid/support/v4/view/ViewPager$SimpleOnPageChangeListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageSelected(I)V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView$4;->b:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;->d(Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;)Lcom/twitter/android/media/imageeditor/stickers/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/imageeditor/stickers/a;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView$4;->b:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;->e(Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;)Lcom/twitter/ui/view/LockableViewPager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/ui/view/LockableViewPager;->removeOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 173
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView$4;->b:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;->d(Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;)Lcom/twitter/android/media/imageeditor/stickers/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/stickers/a;->a()Ljava/util/List;

    move-result-object v0

    .line 174
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView$4;->a:Ljava/util/List;

    invoke-static {v1, v0}, Lcpt;->b(Ljava/lang/Iterable;Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView$4;->b:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;->d(Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;)Lcom/twitter/android/media/imageeditor/stickers/a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView$4;->a:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/stickers/a;->a(Ljava/util/List;)V

    .line 176
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView$4;->b:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;->d(Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;)Lcom/twitter/android/media/imageeditor/stickers/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/stickers/a;->notifyDataSetChanged()V

    .line 177
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView$4;->b:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;->f(Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;)Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView$4;->b:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    invoke-static {v1}, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;->e(Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;)Lcom/twitter/ui/view/LockableViewPager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 180
    :cond_0
    return-void
.end method
