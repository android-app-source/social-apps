.class public Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;
.super Landroid/support/design/widget/TabLayout;
.source "Twttr"


# instance fields
.field private a:Z

.field private b:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/support/design/widget/TabLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    iput v0, p0, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->b:I

    .line 47
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 49
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x7f0100a0

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v0, v1, Landroid/util/TypedValue;->data:I

    :cond_0
    iput v0, p0, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->b:I

    .line 51
    return-void
.end method

.method private a()Landroid/view/View;
    .locals 3

    .prologue
    .line 121
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 122
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 124
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f02066d

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 125
    iget v2, p0, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->b:I

    invoke-static {v1, v2}, Lcnd;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 127
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a091b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 128
    return-object v0
.end method

.method private a(Ljava/lang/String;Lcdw;)Landroid/view/View;
    .locals 3

    .prologue
    .line 145
    new-instance v0, Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;-><init>(Landroid/content/Context;)V

    .line 146
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setScaleMode(I)V

    .line 147
    sget-object v1, Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;->c:Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setScaleType(Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;)V

    .line 148
    if-eqz p2, :cond_0

    .line 149
    iget-object v1, p2, Lcdw;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 153
    :goto_0
    invoke-virtual {v0, p1}, Lcom/twitter/media/ui/image/MediaImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 154
    return-object v0

    .line 151
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02039f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setDefaultDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private a(Lcom/twitter/android/media/imageeditor/stickers/a;IZZ)Lcec;
    .locals 2

    .prologue
    .line 108
    const/4 v0, 0x2

    new-array v0, v0, [Z

    const/4 v1, 0x0

    aput-boolean p3, v0, v1

    const/4 v1, 0x1

    aput-boolean p4, v0, v1

    invoke-static {v0}, Lcom/twitter/util/object/a;->a([Z)I

    move-result v0

    sub-int v0, p2, v0

    .line 111
    if-eqz p4, :cond_0

    .line 112
    invoke-virtual {p1}, Lcom/twitter/android/media/imageeditor/stickers/a;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcec;

    .line 116
    :goto_0
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcec;

    return-object v0

    .line 114
    :cond_0
    invoke-virtual {p1, v0}, Lcom/twitter/android/media/imageeditor/stickers/a;->a(I)Lcec;

    move-result-object v0

    goto :goto_0
.end method

.method private b()Landroid/view/View;
    .locals 3

    .prologue
    .line 133
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 134
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 136
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f02066c

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 137
    iget v2, p0, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->b:I

    invoke-static {v1, v2}, Lcnd;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 139
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a091e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 140
    return-object v0
.end method


# virtual methods
.method public setShouldShowRecentlyUsedFirstIfExists(Z)V
    .locals 0

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->a:Z

    .line 55
    return-void
.end method

.method public setupWithViewPager(Landroid/support/v4/view/ViewPager;)V
    .locals 11

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 59
    invoke-super {p0, p1}, Landroid/support/design/widget/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 60
    if-nez p1, :cond_1

    .line 102
    :cond_0
    return-void

    .line 63
    :cond_1
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    if-nez v0, :cond_2

    .line 64
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Set adapter before setting up tabs"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->removeAllTabs()V

    .line 67
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/imageeditor/stickers/a;

    .line 68
    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/stickers/a;->b()Z

    move-result v8

    .line 69
    iget-boolean v1, p0, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->a:Z

    if-eqz v1, :cond_3

    .line 70
    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/stickers/a;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    .line 71
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e04c5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    move v4, v3

    .line 73
    :goto_1
    invoke-virtual {v0}, Lcom/twitter/android/media/imageeditor/stickers/a;->getCount()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 76
    if-eqz v1, :cond_4

    if-nez v4, :cond_4

    .line 78
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->b()Landroid/view/View;

    move-result-object v5

    move-object v7, v5

    move-object v5, v6

    .line 97
    :goto_2
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->newTab()Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v10

    invoke-virtual {v10, v7}, Landroid/support/design/widget/TabLayout$Tab;->setCustomView(Landroid/view/View;)Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v10

    invoke-virtual {v10, v5}, Landroid/support/design/widget/TabLayout$Tab;->setTag(Ljava/lang/Object;)Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->addTab(Landroid/support/design/widget/TabLayout$Tab;)V

    .line 98
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 99
    iput v9, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 100
    iput v9, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 73
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_3
    move v1, v3

    .line 70
    goto :goto_0

    .line 79
    :cond_4
    if-eqz v1, :cond_5

    move v5, v2

    :goto_3
    if-ne v4, v5, :cond_6

    if-eqz v8, :cond_6

    .line 81
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->a()Landroid/view/View;

    move-result-object v5

    move-object v7, v5

    move-object v5, v6

    goto :goto_2

    :cond_5
    move v5, v3

    .line 79
    goto :goto_3

    .line 83
    :cond_6
    if-eqz v1, :cond_7

    .line 85
    invoke-direct {p0, v0, v4, v8, v2}, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->a(Lcom/twitter/android/media/imageeditor/stickers/a;IZZ)Lcec;

    move-result-object v5

    .line 86
    iget-object v7, v5, Lcec;->e:Ljava/lang/String;

    iget-object v10, v5, Lcec;->d:Lcdw;

    invoke-direct {p0, v7, v10}, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->a(Ljava/lang/String;Lcdw;)Landroid/view/View;

    move-result-object v7

    goto :goto_2

    .line 88
    :cond_7
    invoke-direct {p0, v0, v4, v8, v3}, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->a(Lcom/twitter/android/media/imageeditor/stickers/a;IZZ)Lcec;

    move-result-object v5

    .line 89
    iget-object v7, v5, Lcec;->e:Ljava/lang/String;

    const-string/jumbo v10, "recently_used"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 90
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->b()Landroid/view/View;

    move-result-object v7

    goto :goto_2

    .line 92
    :cond_8
    iget-object v7, v5, Lcec;->e:Ljava/lang/String;

    iget-object v10, v5, Lcec;->d:Lcdw;

    invoke-direct {p0, v7, v10}, Lcom/twitter/android/media/imageeditor/stickers/StickerTabLayout;->a(Ljava/lang/String;Lcdw;)Landroid/view/View;

    move-result-object v7

    goto :goto_2
.end method
