.class public Lcom/twitter/android/media/imageeditor/stickers/b;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method private static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    packed-switch p0, :pswitch_data_0

    .line 102
    const-string/jumbo v0, "sticker_category_other"

    :goto_0
    return-object v0

    .line 96
    :pswitch_0
    const-string/jumbo v0, "sticker_category_recent"

    goto :goto_0

    .line 99
    :pswitch_1
    const-string/jumbo v0, "sticker_category_featured"

    goto :goto_0

    .line 94
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a()V
    .locals 4

    .prologue
    .line 133
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "gallery::gallery:sticker:click"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 134
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 135
    return-void
.end method

.method public static a(JILjava/lang/String;)V
    .locals 4

    .prologue
    .line 78
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 79
    invoke-static {p3}, Lcom/twitter/android/media/imageeditor/stickers/b;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p2}, Lcom/twitter/android/media/imageeditor/stickers/b;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "sticker"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "select"

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>([Ljava/lang/String;)V

    .line 80
    invoke-virtual {v0, p0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 78
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 81
    return-void
.end method

.method public static a(JLcom/twitter/model/drafts/a;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 109
    .line 110
    iget-object v3, p2, Lcom/twitter/model/drafts/a;->d:Ljava/util/List;

    .line 111
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    .line 112
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, Lcom/twitter/model/drafts/DraftAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 113
    instance-of v5, v0, Lcom/twitter/model/media/EditableImage;

    if-eqz v5, :cond_2

    .line 114
    check-cast v0, Lcom/twitter/model/media/EditableImage;

    iget-object v0, v0, Lcom/twitter/model/media/EditableImage;->h:Ljava/util/List;

    .line 115
    if-eqz v0, :cond_2

    .line 116
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v1, v0

    move v0, v1

    :goto_1
    move v1, v0

    .line 119
    goto :goto_0

    .line 120
    :cond_0
    if-lez v1, :cond_1

    .line 121
    new-instance v0, Lcom/twitter/library/scribe/ScribeItemMediaDetails;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeItemMediaDetails;-><init>()V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/twitter/library/scribe/ScribeItemMediaDetails;->b(I)Lcom/twitter/library/scribe/ScribeItemMediaDetails;

    move-result-object v0

    .line 122
    new-instance v3, Lcom/twitter/library/scribe/ScribeItemSendStickerPhotoTweet;

    invoke-direct {v3}, Lcom/twitter/library/scribe/ScribeItemSendStickerPhotoTweet;-><init>()V

    .line 123
    invoke-virtual {v3, v0}, Lcom/twitter/library/scribe/ScribeItemSendStickerPhotoTweet;->a(Lcom/twitter/library/scribe/ScribeItemMediaDetails;)Lcom/twitter/library/scribe/ScribeItemSendStickerPhotoTweet;

    move-result-object v3

    .line 124
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, ":composition:::send_sticker_photo_tweet"

    aput-object v5, v4, v2

    invoke-direct {v0, p0, p1, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    int-to-long v4, v1

    .line 125
    invoke-virtual {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 126
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 127
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 129
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public static a(JLcom/twitter/model/media/EditableImage;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 45
    iget-object v0, p2, Lcom/twitter/model/media/EditableImage;->h:Ljava/util/List;

    .line 46
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 47
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, ""

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 48
    invoke-static {p3}, Lcom/twitter/android/media/imageeditor/stickers/b;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "image_attachment:sticker:add"

    aput-object v4, v2, v3

    invoke-direct {v1, p0, p1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    .line 49
    invoke-static {v0}, Lcom/twitter/library/scribe/b;->a(Ljava/util/List;)Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/util/Collection;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 47
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 51
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/ComponentName;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 168
    invoke-virtual {p0}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v3

    .line 170
    const/4 v0, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 180
    const-string/jumbo v0, "share"

    .line 185
    :goto_1
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, "sticker_timeline"

    aput-object v5, v4, v1

    aput-object p1, v4, v2

    const/4 v1, 0x2

    const-string/jumbo v2, "share_sheet:"

    aput-object v2, v4, v1

    const/4 v1, 0x3

    aput-object v0, v4, v1

    .line 186
    invoke-virtual {v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    new-instance v1, Lcom/twitter/analytics/model/ScribeItemShared;

    invoke-direct {v1, p0}, Lcom/twitter/analytics/model/ScribeItemShared;-><init>(Landroid/content/ComponentName;)V

    .line 187
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 185
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 188
    return-void

    .line 170
    :sswitch_0
    const-string/jumbo v4, "com.twitter.android/.UrlInterpreterActivity"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string/jumbo v4, "com.google.android.apps.docs/.drive.clipboard.SendTextToClipboardActivity"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v2

    goto :goto_0

    .line 172
    :pswitch_0
    const-string/jumbo v0, "share_via_dm"

    goto :goto_1

    .line 176
    :pswitch_1
    const-string/jumbo v0, "share_via_copy"

    goto :goto_1

    .line 170
    nop

    :sswitch_data_0
    .sparse-switch
        -0x6aa63f76 -> :sswitch_1
        0x57986100 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcec;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 63
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 64
    invoke-static {p1}, Lcom/twitter/android/media/imageeditor/stickers/b;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcec;->c:I

    invoke-static {v3}, Lcom/twitter/android/media/imageeditor/stickers/b;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "sticker"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "impression"

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>([Ljava/lang/String;)V

    .line 65
    invoke-static {p0}, Lcom/twitter/library/scribe/b;->a(Lcec;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 63
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 66
    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 142
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "sticker_timeline"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "::impression"

    aput-object v3, v1, v2

    .line 143
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 144
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 145
    return-void
.end method

.method public static b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 152
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "sticker_timeline"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "share_sheet::impression"

    aput-object v3, v1, v2

    .line 153
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 154
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 155
    return-void
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    const-string/jumbo v0, "dm_composition"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    const-string/jumbo v0, "dm_composition"

    .line 88
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "composition"

    goto :goto_0
.end method
