.class public Lcom/twitter/android/media/imageeditor/stickers/c;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/media/imageeditor/stickers/c$c;,
        Lcom/twitter/android/media/imageeditor/stickers/c$b;,
        Lcom/twitter/android/media/imageeditor/stickers/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/twitter/android/media/imageeditor/stickers/e$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcec;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private final d:Landroid/content/SharedPreferences;

.field private final e:Landroid/support/v4/util/SimpleArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/SimpleArrayMap",
            "<",
            "Lcdu;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/twitter/android/media/imageeditor/stickers/c$b;

.field private g:Lcom/twitter/android/media/imageeditor/stickers/c$c;

.field private final h:I

.field private final i:I

.field private j:I

.field private final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcec;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 64
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 61
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->k:Ljava/util/Set;

    .line 65
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->a:Landroid/content/Context;

    .line 66
    iput-object p2, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->b:Ljava/util/List;

    .line 67
    iput-object p3, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->c:Ljava/lang/String;

    .line 69
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcec;

    .line 70
    iget-object v0, v0, Lcec;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 71
    goto :goto_0

    .line 72
    :cond_0
    iput v1, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->h:I

    .line 73
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x1

    if-le v0, v3, :cond_1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    :cond_1
    iput v2, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->i:I

    .line 74
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->d:Landroid/content/SharedPreferences;

    .line 75
    new-instance v0, Landroid/support/v4/util/SimpleArrayMap;

    invoke-direct {v0, v1}, Landroid/support/v4/util/SimpleArrayMap;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->e:Landroid/support/v4/util/SimpleArrayMap;

    .line 76
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/media/imageeditor/stickers/c;)Lcom/twitter/android/media/imageeditor/stickers/c$b;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->f:Lcom/twitter/android/media/imageeditor/stickers/c$b;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/media/imageeditor/stickers/c;)Lcom/twitter/android/media/imageeditor/stickers/c$c;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->g:Lcom/twitter/android/media/imageeditor/stickers/c$c;

    return-object v0
.end method

.method private b(I)Lcom/twitter/util/collection/Pair;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 219
    .line 220
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    move v2, v0

    .line 221
    :goto_0
    if-ge v1, v3, :cond_1

    .line 222
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcec;

    iget-object v0, v0, Lcec;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 223
    add-int v4, v2, v0

    if-le p1, v4, :cond_0

    .line 225
    add-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    .line 221
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 227
    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sub-int v1, p1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    .line 230
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic c(Lcom/twitter/android/media/imageeditor/stickers/c;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->d:Landroid/content/SharedPreferences;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Lcom/twitter/android/media/imageeditor/stickers/e$a;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 82
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    .line 83
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->a:Landroid/content/Context;

    .line 84
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0403d4

    invoke-virtual {v0, v1, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 85
    iget v0, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->j:I

    if-eqz v0, :cond_0

    .line 87
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/GridLayoutManager$LayoutParams;

    .line 88
    iget v2, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->j:I

    neg-int v2, v2

    iget v3, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->j:I

    neg-int v3, v3

    invoke-virtual {v0, v2, v4, v3, v4}, Landroid/support/v7/widget/GridLayoutManager$LayoutParams;->setMargins(IIII)V

    .line 90
    :cond_0
    new-instance v0, Lcom/twitter/android/media/imageeditor/stickers/c$a;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/media/imageeditor/stickers/c$a;-><init>(Lcom/twitter/android/media/imageeditor/stickers/c;Landroid/view/View;)V

    .line 92
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/stickers/e;->a(Landroid/content/Context;)Lcom/twitter/android/media/imageeditor/stickers/e$b;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 105
    iput p1, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->j:I

    .line 106
    return-void
.end method

.method public a(Lcom/twitter/android/media/imageeditor/stickers/c$b;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->f:Lcom/twitter/android/media/imageeditor/stickers/c$b;

    .line 98
    return-void
.end method

.method public a(Lcom/twitter/android/media/imageeditor/stickers/c$c;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->g:Lcom/twitter/android/media/imageeditor/stickers/c$c;

    .line 102
    return-void
.end method

.method public a(Lcom/twitter/android/media/imageeditor/stickers/e$a;)V
    .locals 3

    .prologue
    .line 240
    instance-of v0, p1, Lcom/twitter/android/media/imageeditor/stickers/e$b;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 241
    check-cast v0, Lcom/twitter/android/media/imageeditor/stickers/e$b;

    .line 242
    iget-object v1, v0, Lcom/twitter/android/media/imageeditor/stickers/e$b;->a:Lcom/twitter/media/ui/image/MediaImageView;

    check-cast v1, Lcom/twitter/android/media/stickers/StickerCatalogMediaImageView;

    invoke-virtual {v1}, Lcom/twitter/android/media/stickers/StickerCatalogMediaImageView;->getSticker()Lcdu;

    move-result-object v2

    .line 243
    if-nez v2, :cond_0

    .line 253
    :goto_0
    return-void

    .line 247
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->e:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v1, v2}, Landroid/support/v4/util/SimpleArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 248
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_2

    .line 249
    :cond_1
    invoke-static {v2, v0}, Lcom/twitter/android/media/imageeditor/stickers/e;->a(Lcdu;Lcom/twitter/android/media/imageeditor/stickers/e$b;)V

    .line 252
    :cond_2
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$Adapter;->onViewAttachedToWindow(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/media/imageeditor/stickers/e$a;I)V
    .locals 9

    .prologue
    const/16 v3, 0x8

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 110
    instance-of v0, p1, Lcom/twitter/android/media/imageeditor/stickers/e$b;

    if-eqz v0, :cond_3

    .line 111
    check-cast p1, Lcom/twitter/android/media/imageeditor/stickers/e$b;

    .line 112
    iget-object v2, p1, Lcom/twitter/android/media/imageeditor/stickers/e$b;->a:Lcom/twitter/media/ui/image/MediaImageView;

    .line 113
    invoke-direct {p0, p2}, Lcom/twitter/android/media/imageeditor/stickers/c;->b(I)Lcom/twitter/util/collection/Pair;

    move-result-object v1

    .line 114
    if-nez v1, :cond_1

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    invoke-virtual {v1}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 118
    iget-object v3, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->b:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcec;

    .line 120
    iget-object v3, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v7, :cond_2

    invoke-virtual {v1}, Lcom/twitter/util/collection/Pair;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 p2, v1, -0x1

    .line 121
    :cond_2
    iget-object v1, v0, Lcec;->f:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcee;

    .line 122
    invoke-virtual {v1}, Lcee;->a()Lcdu;

    move-result-object v3

    .line 123
    iget-object v4, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->e:Landroid/support/v4/util/SimpleArrayMap;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/support/v4/util/SimpleArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    new-instance v4, Lcom/twitter/android/media/imageeditor/stickers/c$1;

    invoke-direct {v4, p0, v3, v0}, Lcom/twitter/android/media/imageeditor/stickers/c$1;-><init>(Lcom/twitter/android/media/imageeditor/stickers/c;Lcdu;Lcec;)V

    invoke-virtual {v2, v4}, Lcom/twitter/media/ui/image/MediaImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    invoke-static {v3, p1}, Lcom/twitter/android/media/imageeditor/stickers/e;->a(Lcdu;Lcom/twitter/android/media/imageeditor/stickers/e$b;)V

    .line 137
    new-instance v0, Lcom/twitter/android/media/imageeditor/stickers/c$2;

    invoke-direct {v0, p0, v1, p1}, Lcom/twitter/android/media/imageeditor/stickers/c$2;-><init>(Lcom/twitter/android/media/imageeditor/stickers/c;Lcee;Lcom/twitter/android/media/imageeditor/stickers/e$b;)V

    invoke-virtual {v2, v0}, Lcom/twitter/media/ui/image/MediaImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto :goto_0

    .line 169
    :cond_3
    check-cast p1, Lcom/twitter/android/media/imageeditor/stickers/c$a;

    .line 170
    invoke-direct {p0, p2}, Lcom/twitter/android/media/imageeditor/stickers/c;->b(I)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    .line 171
    if-eqz v0, :cond_0

    .line 174
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->b:Ljava/util/List;

    invoke-virtual {v0}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcec;

    .line 175
    iget-object v1, p1, Lcom/twitter/android/media/imageeditor/stickers/c$a;->a:Landroid/view/View;

    const v4, 0x7f130802

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 176
    iget-object v4, p1, Lcom/twitter/android/media/imageeditor/stickers/c$a;->a:Landroid/view/View;

    const v5, 0x7f130801

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 177
    iget-object v5, p1, Lcom/twitter/android/media/imageeditor/stickers/c$a;->a:Landroid/view/View;

    const v6, 0x7f130803

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 178
    iget-object v6, v0, Lcec;->e:Ljava/lang/String;

    invoke-static {v6}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 179
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 180
    iget-object v6, v0, Lcec;->e:Ljava/lang/String;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    :goto_1
    iget-boolean v1, v0, Lcec;->g:Z

    if-eqz v1, :cond_5

    move v1, v2

    :goto_2
    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    .line 185
    const v1, 0x7f130804

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 186
    iget-boolean v5, v0, Lcec;->g:Z

    if-eqz v5, :cond_6

    iget-object v5, v0, Lcec;->h:Ljava/lang/String;

    invoke-static {v5}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 187
    iget-object v5, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->a:Landroid/content/Context;

    const v6, 0x7f0a091d

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, v0, Lcec;->h:Ljava/lang/String;

    aput-object v8, v7, v2

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    :goto_3
    if-eqz p2, :cond_7

    .line 192
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 196
    :goto_4
    const/4 v1, 0x2

    iget v2, v0, Lcec;->c:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->k:Ljava/util/Set;

    iget-wide v2, v0, Lcec;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 197
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/android/media/imageeditor/stickers/b;->a(Lcec;Ljava/lang/String;)V

    .line 198
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->k:Ljava/util/Set;

    iget-wide v2, v0, Lcec;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 182
    :cond_4
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_5
    move v1, v3

    .line 184
    goto :goto_2

    .line 189
    :cond_6
    iget-object v5, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->a:Landroid/content/Context;

    const v6, 0x7f0a06f9

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 194
    :cond_7
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4
.end method

.method public b(Lcom/twitter/android/media/imageeditor/stickers/e$a;)V
    .locals 2

    .prologue
    .line 257
    instance-of v0, p1, Lcom/twitter/android/media/imageeditor/stickers/e$b;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 258
    check-cast v0, Lcom/twitter/android/media/imageeditor/stickers/e$b;

    iget-object v0, v0, Lcom/twitter/android/media/imageeditor/stickers/e$b;->a:Lcom/twitter/media/ui/image/MediaImageView;

    check-cast v0, Lcom/twitter/android/media/stickers/StickerCatalogMediaImageView;

    .line 259
    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/StickerCatalogMediaImageView;->getSticker()Lcdu;

    move-result-object v1

    .line 260
    if-nez v1, :cond_0

    .line 269
    :goto_0
    return-void

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->e:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0, v1}, Landroid/support/v4/util/SimpleArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 264
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 265
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->e:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0, v1}, Landroid/support/v4/util/SimpleArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$Adapter;->onViewDetachedFromWindow(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    goto :goto_0
.end method

.method public getItemCount()I
    .locals 2

    .prologue
    .line 235
    iget v0, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->h:I

    iget v1, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->i:I

    add-int/2addr v0, v1

    return v0
.end method

.method public getItemViewType(I)I
    .locals 3

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    .line 205
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/c;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_0

    move v0, v1

    .line 214
    :goto_0
    return v0

    .line 208
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/android/media/imageeditor/stickers/c;->b(I)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    .line 209
    if-nez v0, :cond_1

    .line 210
    const/4 v0, 0x0

    goto :goto_0

    .line 211
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/util/collection/Pair;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 212
    goto :goto_0

    :cond_2
    move v0, v1

    .line 214
    goto :goto_0
.end method

.method public synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 42
    check-cast p1, Lcom/twitter/android/media/imageeditor/stickers/e$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/media/imageeditor/stickers/c;->a(Lcom/twitter/android/media/imageeditor/stickers/e$a;I)V

    return-void
.end method

.method public synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/media/imageeditor/stickers/c;->a(Landroid/view/ViewGroup;I)Lcom/twitter/android/media/imageeditor/stickers/e$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic onViewAttachedToWindow(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 0

    .prologue
    .line 42
    check-cast p1, Lcom/twitter/android/media/imageeditor/stickers/e$a;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/imageeditor/stickers/c;->a(Lcom/twitter/android/media/imageeditor/stickers/e$a;)V

    return-void
.end method

.method public synthetic onViewDetachedFromWindow(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 0

    .prologue
    .line 42
    check-cast p1, Lcom/twitter/android/media/imageeditor/stickers/e$a;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/imageeditor/stickers/c;->b(Lcom/twitter/android/media/imageeditor/stickers/e$a;)V

    return-void
.end method
