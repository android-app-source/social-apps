.class Lcom/twitter/android/media/imageeditor/stickers/c$2$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/imageeditor/stickers/d$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/media/imageeditor/stickers/c$2;->onLongClick(Landroid/view/View;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/imageeditor/stickers/c$2;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/imageeditor/stickers/c$2;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/stickers/c$2$1;->a:Lcom/twitter/android/media/imageeditor/stickers/c$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcdu;Lcdu;)V
    .locals 7

    .prologue
    const/16 v6, 0x3a

    .line 146
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 147
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "stickers_primary_variant_list"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 149
    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/stickers/c$2$1;->a:Lcom/twitter/android/media/imageeditor/stickers/c$2;

    iget-object v2, v2, Lcom/twitter/android/media/imageeditor/stickers/c$2;->c:Lcom/twitter/android/media/imageeditor/stickers/c;

    invoke-static {v2}, Lcom/twitter/android/media/imageeditor/stickers/c;->c(Lcom/twitter/android/media/imageeditor/stickers/c;)Landroid/content/SharedPreferences;

    move-result-object v2

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v2, v1, v3}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    .line 151
    iget-wide v4, p1, Lcdu;->h:J

    const/16 v3, 0x24

    invoke-static {v4, v5, v3}, Ljava/lang/Long;->toString(JI)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 152
    iget-object v3, p0, Lcom/twitter/android/media/imageeditor/stickers/c$2$1;->a:Lcom/twitter/android/media/imageeditor/stickers/c$2;

    iget-object v3, v3, Lcom/twitter/android/media/imageeditor/stickers/c$2;->c:Lcom/twitter/android/media/imageeditor/stickers/c;

    invoke-static {v3}, Lcom/twitter/android/media/imageeditor/stickers/c;->c(Lcom/twitter/android/media/imageeditor/stickers/c;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 153
    invoke-interface {v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "stickers_primary_variant_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p1, Lcdu;->h:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p2, Lcdu;->h:J

    .line 154
    invoke-interface {v1, v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 156
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 157
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/c$2$1;->a:Lcom/twitter/android/media/imageeditor/stickers/c$2;

    iget-object v0, v0, Lcom/twitter/android/media/imageeditor/stickers/c$2;->c:Lcom/twitter/android/media/imageeditor/stickers/c;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/stickers/c;->b(Lcom/twitter/android/media/imageeditor/stickers/c;)Lcom/twitter/android/media/imageeditor/stickers/c$c;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/media/imageeditor/stickers/c$c;->a()V

    .line 158
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/c$2$1;->a:Lcom/twitter/android/media/imageeditor/stickers/c$2;

    iget-object v0, v0, Lcom/twitter/android/media/imageeditor/stickers/c$2;->a:Lcee;

    invoke-virtual {v0, p2}, Lcee;->a(Lcdu;)V

    .line 159
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/c$2$1;->a:Lcom/twitter/android/media/imageeditor/stickers/c$2;

    iget-object v0, v0, Lcom/twitter/android/media/imageeditor/stickers/c$2;->c:Lcom/twitter/android/media/imageeditor/stickers/c;

    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/stickers/c$2$1;->a:Lcom/twitter/android/media/imageeditor/stickers/c$2;

    iget-object v1, v1, Lcom/twitter/android/media/imageeditor/stickers/c$2;->b:Lcom/twitter/android/media/imageeditor/stickers/e$b;

    invoke-virtual {v1}, Lcom/twitter/android/media/imageeditor/stickers/e$b;->getAdapterPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/stickers/c;->notifyItemChanged(I)V

    .line 160
    return-void
.end method
