.class Lcom/twitter/android/media/imageeditor/stickers/a;
.super Landroid/support/v4/view/PagerAdapter;
.source "Twttr"


# static fields
.field private static final a:Lcpv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcpv",
            "<",
            "Lcec;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcpv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcpv",
            "<",
            "Lcec;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcec;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/twitter/android/media/imageeditor/stickers/c$c;

.field private final f:Ljava/lang/String;

.field private final g:Z

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcec;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcom/twitter/android/media/imageeditor/stickers/c$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/twitter/android/media/imageeditor/stickers/a$1;

    invoke-direct {v0}, Lcom/twitter/android/media/imageeditor/stickers/a$1;-><init>()V

    sput-object v0, Lcom/twitter/android/media/imageeditor/stickers/a;->a:Lcpv;

    .line 41
    new-instance v0, Lcom/twitter/android/media/imageeditor/stickers/a$2;

    invoke-direct {v0}, Lcom/twitter/android/media/imageeditor/stickers/a$2;-><init>()V

    sput-object v0, Lcom/twitter/android/media/imageeditor/stickers/a;->b:Lcpv;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/util/List;Ljava/util/List;Lcom/twitter/android/media/imageeditor/stickers/c$c;Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcec;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcec;",
            ">;",
            "Lcom/twitter/android/media/imageeditor/stickers/c$c;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->c:Landroid/content/Context;

    .line 67
    iput-object p2, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->d:Ljava/util/List;

    .line 68
    iput-object p3, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->h:Ljava/util/List;

    .line 69
    iput-object p4, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->e:Lcom/twitter/android/media/imageeditor/stickers/c$c;

    .line 70
    iput-object p5, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->f:Ljava/lang/String;

    .line 71
    if-eqz p6, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/stickers/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->g:Z

    .line 72
    return-void

    .line 71
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(IZ)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)",
            "Ljava/util/List",
            "<",
            "Lcec;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->g:Z

    if-eqz v0, :cond_0

    .line 118
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/stickers/a;->f()Lcec;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 131
    :goto_0
    return-object v0

    .line 119
    :cond_0
    if-eqz p2, :cond_1

    .line 120
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->h:Ljava/util/List;

    goto :goto_0

    .line 123
    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [Z

    const/4 v1, 0x0

    .line 124
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/stickers/a;->b()Z

    move-result v2

    aput-boolean v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->g:Z

    aput-boolean v2, v0, v1

    invoke-static {v0}, Lcom/twitter/util/object/a;->a([Z)I

    move-result v0

    sub-int v0, p1, v0

    .line 126
    iget-boolean v1, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->g:Z

    if-eqz v1, :cond_2

    .line 127
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/stickers/a;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcec;

    .line 131
    :goto_1
    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 129
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcec;

    goto :goto_1
.end method

.method private b(Ljava/util/List;)Lcom/twitter/android/media/imageeditor/stickers/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcec;",
            ">;)",
            "Lcom/twitter/android/media/imageeditor/stickers/c;"
        }
    .end annotation

    .prologue
    .line 109
    new-instance v0, Lcom/twitter/android/media/imageeditor/stickers/c;

    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->f:Ljava/lang/String;

    invoke-direct {v0, v1, p1, v2}, Lcom/twitter/android/media/imageeditor/stickers/c;-><init>(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;)V

    .line 111
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->e:Lcom/twitter/android/media/imageeditor/stickers/c$c;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/stickers/c;->a(Lcom/twitter/android/media/imageeditor/stickers/c$c;)V

    .line 112
    return-object v0
.end method

.method static synthetic e()Lcpv;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/twitter/android/media/imageeditor/stickers/a;->a:Lcpv;

    return-object v0
.end method

.method private f()Lcec;
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->d:Ljava/util/List;

    sget-object v1, Lcom/twitter/android/media/imageeditor/stickers/a;->a:Lcpv;

    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;Lcpv;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcec;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Landroid/view/ViewGroup;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 138
    new-instance v2, Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->c:Landroid/content/Context;

    invoke-direct {v2, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 139
    new-instance v3, Landroid/support/v7/widget/RecyclerView;

    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->c:Landroid/content/Context;

    invoke-direct {v3, v0}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    .line 141
    invoke-virtual {p0, p2}, Lcom/twitter/android/media/imageeditor/stickers/a;->b(I)Z

    move-result v0

    .line 142
    invoke-direct {p0, p2, v0}, Lcom/twitter/android/media/imageeditor/stickers/a;->a(IZ)Ljava/util/List;

    move-result-object v1

    .line 143
    if-eqz v0, :cond_1

    move-object v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->setTag(Ljava/lang/Object;)V

    .line 145
    invoke-direct {p0, v1}, Lcom/twitter/android/media/imageeditor/stickers/a;->b(Ljava/util/List;)Lcom/twitter/android/media/imageeditor/stickers/c;

    move-result-object v1

    .line 146
    invoke-virtual {v3, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 148
    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 149
    const v4, 0x7f0e04c9

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 150
    const v5, 0x7f0e04c8

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 152
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->c:Landroid/content/Context;

    const-string/jumbo v6, "window"

    .line 153
    invoke-virtual {v0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-static {v0}, Lcom/twitter/util/ui/k;->a(Landroid/view/WindowManager;)Landroid/graphics/Point;

    move-result-object v0

    .line 154
    mul-int/lit8 v6, v5, 0x2

    add-int/2addr v4, v6

    .line 155
    const/4 v6, 0x4

    iget v7, v0, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    int-to-float v8, v4

    div-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 158
    iget v0, v0, Landroid/graphics/Point;->x:I

    mul-int/2addr v4, v6

    sub-int/2addr v0, v4

    div-int/lit8 v0, v0, 0x2

    .line 160
    new-instance v4, Landroid/support/v7/widget/GridLayoutManager;

    iget-object v7, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->c:Landroid/content/Context;

    const/4 v8, 0x1

    invoke-direct {v4, v7, v6, v8, v9}, Landroid/support/v7/widget/GridLayoutManager;-><init>(Landroid/content/Context;IIZ)V

    .line 162
    new-instance v7, Lcom/twitter/android/media/imageeditor/stickers/a$3;

    invoke-direct {v7, p0, v1, v6}, Lcom/twitter/android/media/imageeditor/stickers/a$3;-><init>(Lcom/twitter/android/media/imageeditor/stickers/a;Lcom/twitter/android/media/imageeditor/stickers/c;I)V

    invoke-virtual {v4, v7}, Landroid/support/v7/widget/GridLayoutManager;->setSpanSizeLookup(Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;)V

    .line 172
    invoke-virtual {v3, v4}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 173
    if-lez v0, :cond_0

    .line 174
    invoke-virtual {v3, v5, v5, v5, v5}, Landroid/support/v7/widget/RecyclerView;->setPadding(IIII)V

    .line 175
    invoke-virtual {v3, v9}, Landroid/support/v7/widget/RecyclerView;->setClipToPadding(Z)V

    .line 176
    invoke-virtual {v1, v0}, Lcom/twitter/android/media/imageeditor/stickers/c;->a(I)V

    .line 178
    :cond_0
    invoke-static {v5}, Lcom/twitter/android/media/imageeditor/stickers/e;->a(I)Landroid/support/v7/widget/RecyclerView$ItemDecoration;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 180
    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 181
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->i:Lcom/twitter/android/media/imageeditor/stickers/c$b;

    invoke-virtual {v1, v0}, Lcom/twitter/android/media/imageeditor/stickers/c;->a(Lcom/twitter/android/media/imageeditor/stickers/c$b;)V

    .line 182
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 183
    return-object v2

    .line 143
    :cond_1
    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I)Lcec;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcec;

    return-object v0
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcec;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->h:Ljava/util/List;

    return-object v0
.end method

.method public a(Lcom/twitter/android/media/imageeditor/stickers/c$b;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->i:Lcom/twitter/android/media/imageeditor/stickers/c$b;

    .line 76
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcec;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 197
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->h:Ljava/util/List;

    .line 198
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 205
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/stickers/a;->b()Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 213
    :cond_0
    :goto_0
    return v0

    .line 209
    :cond_1
    iget-boolean v2, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->g:Z

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/stickers/a;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 210
    if-eq p1, v0, :cond_0

    move v0, v1

    goto :goto_0

    .line 213
    :cond_2
    if-eqz p1, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/twitter/android/media/imageeditor/stickers/a;->f()Lcec;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcec;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->d:Ljava/util/List;

    sget-object v1, Lcom/twitter/android/media/imageeditor/stickers/a;->b:Lcpv;

    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;Lcpv;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 232
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 233
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/stickers/a;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 90
    check-cast p1, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 92
    instance-of v2, v0, Ljava/util/List;

    if-eqz v2, :cond_2

    .line 94
    iget-boolean v0, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->g:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 102
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 94
    goto :goto_0

    .line 97
    :cond_2
    check-cast v0, Lcec;

    .line 98
    iget-boolean v2, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->g:Z

    if-eqz v2, :cond_3

    const-string/jumbo v2, "recently_used"

    iget-object v3, v0, Lcec;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    .line 99
    goto :goto_0

    .line 101
    :cond_3
    iget-object v1, p0, Lcom/twitter/android/media/imageeditor/stickers/a;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 102
    const/4 v1, -0x1

    if-eq v0, v1, :cond_4

    .line 103
    invoke-virtual {p0}, Lcom/twitter/android/media/imageeditor/stickers/a;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    const/4 v0, -0x2

    goto :goto_0
.end method

.method public synthetic instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/media/imageeditor/stickers/a;->a(Landroid/view/ViewGroup;I)Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
