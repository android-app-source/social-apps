.class Lcom/twitter/android/media/imageeditor/EditImageFragment$6;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/media/ui/image/BaseMediaImageView$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Lcom/twitter/model/media/EditableMedia;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/media/ui/image/BaseMediaImageView$b",
        "<",
        "Lcom/twitter/media/ui/image/FilteredImageView;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/imageeditor/EditImageFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/imageeditor/EditImageFragment;)V
    .locals 0

    .prologue
    .line 1246
    iput-object p1, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$6;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/media/ui/image/BaseMediaImageView;Lcom/twitter/media/request/ImageResponse;)V
    .locals 0

    .prologue
    .line 1246
    check-cast p1, Lcom/twitter/media/ui/image/FilteredImageView;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/media/imageeditor/EditImageFragment$6;->a(Lcom/twitter/media/ui/image/FilteredImageView;Lcom/twitter/media/request/ImageResponse;)V

    return-void
.end method

.method public a(Lcom/twitter/media/ui/image/FilteredImageView;Lcom/twitter/media/request/ImageResponse;)V
    .locals 2

    .prologue
    .line 1251
    invoke-virtual {p2}, Lcom/twitter/media/request/ImageResponse;->e()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1252
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$6;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->a(Lcom/twitter/android/media/imageeditor/EditImageFragment;Z)Z

    .line 1253
    iget-object v0, p0, Lcom/twitter/android/media/imageeditor/EditImageFragment$6;->a:Lcom/twitter/android/media/imageeditor/EditImageFragment;

    invoke-static {v0}, Lcom/twitter/android/media/imageeditor/EditImageFragment;->c(Lcom/twitter/android/media/imageeditor/EditImageFragment;)Lcom/twitter/android/media/stickers/StickerFilteredImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/stickers/StickerFilteredImageView;->setOnImageLoadedListener(Lcom/twitter/media/ui/image/BaseMediaImageView$b;)V

    .line 1255
    :cond_0
    return-void
.end method
