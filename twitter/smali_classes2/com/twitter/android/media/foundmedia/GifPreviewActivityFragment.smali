.class public Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;
.super Lcom/twitter/app/common/base/BaseFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/selection/a;


# static fields
.field static final synthetic c:Z


# instance fields
.field a:Lcom/twitter/android/media/selection/MediaAttachment;

.field b:Lcom/twitter/android/media/selection/c;

.field private d:Lcom/twitter/android/media/widget/AttachmentMediaView;

.field private e:Lcom/twitter/android/widget/FoundMediaAttributionView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->c:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/twitter/app/common/base/BaseFragment;-><init>()V

    .line 40
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->setRetainInstance(Z)V

    .line 41
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 45
    const v0, 0x7f0400ff

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/android/media/selection/b;)V
    .locals 4

    .prologue
    .line 122
    invoke-virtual {p1}, Lcom/twitter/android/media/selection/b;->c()Lcom/twitter/android/media/selection/MediaAttachment;

    move-result-object v1

    .line 123
    if-nez v1, :cond_1

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    iget v0, v1, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    packed-switch v0, :pswitch_data_0

    .line 141
    invoke-virtual {p0}, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0a04b2

    invoke-virtual {p0, v1}, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 129
    :pswitch_0
    iput-object v1, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->a:Lcom/twitter/android/media/selection/MediaAttachment;

    .line 130
    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Lcom/twitter/android/media/selection/MediaAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 131
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->d:Lcom/twitter/android/media/widget/AttachmentMediaView;

    if-eqz v2, :cond_0

    .line 132
    iget-object v2, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->d:Lcom/twitter/android/media/widget/AttachmentMediaView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setVisibility(I)V

    .line 133
    iget-object v2, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->d:Lcom/twitter/android/media/widget/AttachmentMediaView;

    invoke-virtual {v0}, Lcom/twitter/model/media/EditableMedia;->a()F

    move-result v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setAspectRatio(F)V

    .line 134
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->d:Lcom/twitter/android/media/widget/AttachmentMediaView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setMediaAttachment(Lcom/twitter/android/media/selection/MediaAttachment;)V

    .line 135
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->e:Lcom/twitter/android/widget/FoundMediaAttributionView;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/FoundMediaAttributionView;

    .line 136
    invoke-virtual {v1}, Lcom/twitter/android/media/selection/MediaAttachment;->c()Lcom/twitter/model/media/MediaSource;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/model/media/MediaSource;->c()Lcom/twitter/model/media/foundmedia/FoundMediaProvider;

    move-result-object v1

    .line 135
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/FoundMediaAttributionView;->setProvider(Lcom/twitter/model/media/foundmedia/FoundMediaProvider;)V

    goto :goto_0

    .line 126
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/twitter/android/media/selection/c;)V
    .locals 1

    .prologue
    .line 100
    iput-object p1, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->b:Lcom/twitter/android/media/selection/c;

    .line 101
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->a:Lcom/twitter/android/media/selection/MediaAttachment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->a:Lcom/twitter/android/media/selection/MediaAttachment;

    iget v0, v0, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->a:Lcom/twitter/android/media/selection/MediaAttachment;

    invoke-virtual {p1, v0, p0}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/media/selection/a;)V

    .line 104
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/android/media/selection/MediaAttachment;)Z
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x1

    return v0
.end method

.method public b(Lcom/twitter/android/media/selection/MediaAttachment;)V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->a:Lcom/twitter/android/media/selection/MediaAttachment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->a:Lcom/twitter/android/media/selection/MediaAttachment;

    invoke-virtual {v0}, Lcom/twitter/android/media/selection/MediaAttachment;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/android/media/selection/MediaAttachment;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 108
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->a:Lcom/twitter/android/media/selection/MediaAttachment;

    .line 109
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->d:Lcom/twitter/android/media/widget/AttachmentMediaView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->b:Lcom/twitter/android/media/selection/c;

    if-eqz v0, :cond_1

    .line 110
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->b:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v0, p1, p0}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/media/selection/a;)V

    .line 113
    :cond_1
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->b:Lcom/twitter/android/media/selection/c;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->b:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/selection/c;->a()V

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->a:Lcom/twitter/android/media/selection/MediaAttachment;

    if-eqz v0, :cond_1

    .line 158
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->a:Lcom/twitter/android/media/selection/MediaAttachment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/selection/MediaAttachment;->a(Lcom/twitter/android/media/selection/MediaAttachment;)V

    .line 160
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 161
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 162
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 163
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 149
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 150
    const-string/jumbo v0, "attachment"

    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->a:Lcom/twitter/android/media/selection/MediaAttachment;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 151
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const v3, 0x7f130393

    .line 50
    const v0, 0x7f130392

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/AttachmentMediaView;

    iput-object v0, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->d:Lcom/twitter/android/media/widget/AttachmentMediaView;

    .line 51
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/FoundMediaAttributionView;

    iput-object v0, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->e:Lcom/twitter/android/widget/FoundMediaAttributionView;

    .line 53
    if-eqz p2, :cond_0

    .line 54
    const-string/jumbo v0, "attachment"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/selection/MediaAttachment;

    iput-object v0, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->a:Lcom/twitter/android/media/selection/MediaAttachment;

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->a:Lcom/twitter/android/media/selection/MediaAttachment;

    if-eqz v0, :cond_2

    .line 57
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->a:Lcom/twitter/android/media/selection/MediaAttachment;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/selection/MediaAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 58
    if-eqz v0, :cond_2

    .line 59
    sget-boolean v1, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->c:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->d:Lcom/twitter/android/media/widget/AttachmentMediaView;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 60
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->d:Lcom/twitter/android/media/widget/AttachmentMediaView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setVisibility(I)V

    .line 61
    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->d:Lcom/twitter/android/media/widget/AttachmentMediaView;

    invoke-virtual {v0}, Lcom/twitter/model/media/EditableMedia;->a()F

    move-result v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setAspectRatio(F)V

    .line 62
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->d:Lcom/twitter/android/media/widget/AttachmentMediaView;

    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->a:Lcom/twitter/android/media/selection/MediaAttachment;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setMediaAttachment(Lcom/twitter/android/media/selection/MediaAttachment;)V

    .line 66
    :cond_2
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment$1;-><init>(Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    const v0, 0x7f130394

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment$2;-><init>(Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    return-void
.end method
