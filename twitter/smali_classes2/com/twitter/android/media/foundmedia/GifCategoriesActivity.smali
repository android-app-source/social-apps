.class public Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"


# instance fields
.field a:Lcom/twitter/android/composer/ComposerType;

.field private b:Lcom/twitter/android/media/widget/FoundMediaSearchView;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    .line 37
    sget-object v0, Lcom/twitter/android/composer/ComposerType;->a:Lcom/twitter/android/composer/ComposerType;

    iput-object v0, p0, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;->a:Lcom/twitter/android/composer/ComposerType;

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 46
    const v0, 0x7f040022

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 47
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(Z)V

    .line 48
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 49
    const/16 v0, 0x24

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(I)V

    .line 50
    if-eqz p1, :cond_0

    .line 51
    const-string/jumbo v0, "search_text"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;->c:Ljava/lang/String;

    .line 53
    :cond_0
    return-object p2
.end method

.method public a(Lcmm;)Z
    .locals 1

    .prologue
    .line 107
    invoke-interface {p1}, Lcmm;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 114
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmm;)Z

    move-result v0

    :goto_0
    return v0

    .line 109
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;->setResult(I)V

    .line 110
    invoke-virtual {p0}, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;->finish()V

    .line 111
    const/4 v0, 0x1

    goto :goto_0

    .line 107
    nop

    :pswitch_data_0
    .packed-switch 0x7f130043
        :pswitch_0
    .end packed-switch
.end method

.method public b(Lcmr;)I
    .locals 4

    .prologue
    .line 59
    invoke-interface {p1}, Lcmr;->k()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 61
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040119

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/media/widget/FoundMediaSearchView;

    iput-object v1, p0, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;->b:Lcom/twitter/android/media/widget/FoundMediaSearchView;

    .line 62
    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;->b:Lcom/twitter/android/media/widget/FoundMediaSearchView;

    new-instance v2, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity$1;

    invoke-direct {v2, p0}, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity$1;-><init>(Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/media/widget/FoundMediaSearchView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 78
    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;->b:Lcom/twitter/android/media/widget/FoundMediaSearchView;

    new-instance v2, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity$2;

    invoke-direct {v2, p0}, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity$2;-><init>(Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/media/widget/FoundMediaSearchView;->setOnClearClickListener(Lcom/twitter/android/media/widget/FoundMediaSearchView$a;)V

    .line 85
    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;->b:Lcom/twitter/android/media/widget/FoundMediaSearchView;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setCustomView(Landroid/view/View;)V

    .line 87
    const/4 v0, 0x2

    return v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;->b:Lcom/twitter/android/media/widget/FoundMediaSearchView;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    .line 138
    return-void
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 3

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 93
    invoke-virtual {p0}, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "composer_type"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/composer/ComposerType;

    iput-object v0, p0, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;->a:Lcom/twitter/android/composer/ComposerType;

    .line 95
    if-nez p1, :cond_0

    .line 96
    new-instance v0, Lcom/twitter/android/media/foundmedia/GifCategoriesFragment;

    invoke-direct {v0}, Lcom/twitter/android/media/foundmedia/GifCategoriesFragment;-><init>()V

    .line 97
    new-instance v1, Lcom/twitter/android/media/foundmedia/GifCategoriesFragment$a;

    iget-object v2, p0, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;->a:Lcom/twitter/android/composer/ComposerType;

    invoke-direct {v1, v2}, Lcom/twitter/android/media/foundmedia/GifCategoriesFragment$a;-><init>(Lcom/twitter/android/composer/ComposerType;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/foundmedia/GifCategoriesFragment;->a(Lcom/twitter/app/common/base/b;)V

    .line 98
    invoke-virtual {p0}, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 99
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f13014c

    .line 100
    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 101
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 103
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 121
    packed-switch p1, :pswitch_data_0

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 123
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;->b:Lcom/twitter/android/media/widget/FoundMediaSearchView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/FoundMediaSearchView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    if-ne p2, v2, :cond_0

    if-eqz p3, :cond_0

    .line 125
    invoke-virtual {p0, v2, p3}, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;->setResult(ILandroid/content/Intent;)V

    .line 126
    invoke-virtual {p0}, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;->finish()V

    goto :goto_0

    .line 121
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 142
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onResume()V

    .line 143
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;->b:Lcom/twitter/android/media/widget/FoundMediaSearchView;

    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/FoundMediaSearchView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;->c:Ljava/lang/String;

    .line 147
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 151
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 152
    const-string/jumbo v0, "search_text"

    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifCategoriesActivity;->b:Lcom/twitter/android/media/widget/FoundMediaSearchView;

    invoke-virtual {v1}, Lcom/twitter/android/media/widget/FoundMediaSearchView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    return-void
.end method
