.class public Lcom/twitter/android/media/foundmedia/GifGalleryFragment;
.super Lcom/twitter/app/common/base/BaseFragment;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/media/foundmedia/GifGalleryFragment$a;
    }
.end annotation


# instance fields
.field a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/android/media/foundmedia/GifGalleryFragment$a;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/lang/String;

.field c:Lcom/twitter/android/media/widget/GifGalleryView;

.field private final d:Lcom/twitter/android/media/widget/GifGalleryView$b;

.field private e:I

.field private f:Ljava/lang/String;

.field private g:Landroid/view/View;

.field private h:Landroid/view/View;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field private k:Landroid/widget/Switch;

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/media/foundmedia/d;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/lang/String;

.field private n:I

.field private o:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/twitter/app/common/base/BaseFragment;-><init>()V

    .line 57
    new-instance v0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$1;-><init>(Lcom/twitter/android/media/foundmedia/GifGalleryFragment;)V

    iput-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->d:Lcom/twitter/android/media/widget/GifGalleryView$b;

    .line 108
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->setRetainInstance(Z)V

    .line 109
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/media/foundmedia/GifGalleryFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->T:Landroid/content/Context;

    return-object v0
.end method

.method private h()Z
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->m:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 464
    invoke-virtual {p0}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 465
    if-nez v0, :cond_0

    .line 477
    :goto_0
    return-void

    .line 468
    :cond_0
    invoke-static {v0}, Lcom/twitter/android/media/foundmedia/b;->a(Landroid/content/Context;)Lcom/twitter/android/media/foundmedia/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/media/foundmedia/b;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 469
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/GifGalleryView;->setPlayAnimation(Z)V

    .line 470
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->j:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 472
    :cond_1
    invoke-static {v0}, Lcom/twitter/android/media/foundmedia/b;->a(Landroid/content/Context;)Lcom/twitter/android/media/foundmedia/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/media/foundmedia/b;->a()Z

    move-result v0

    .line 473
    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->k:Landroid/widget/Switch;

    invoke-virtual {v1, v0}, Landroid/widget/Switch;->setChecked(Z)V

    .line 474
    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    invoke-virtual {v1, v0}, Lcom/twitter/android/media/widget/GifGalleryView;->setPlayAnimation(Z)V

    .line 475
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->j:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 117
    const v0, 0x7f0400fe

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 118
    const v0, 0x7f130385

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->g:Landroid/view/View;

    .line 120
    const v0, 0x7f130391

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/GifGalleryView;

    iput-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    .line 121
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    new-instance v2, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$2;

    invoke-direct {v2, p0}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$2;-><init>(Lcom/twitter/android/media/foundmedia/GifGalleryFragment;)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/GifGalleryView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 134
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    iget-object v2, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->d:Lcom/twitter/android/media/widget/GifGalleryView$b;

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/GifGalleryView;->setItemClickListener(Lcom/twitter/android/media/widget/GifGalleryView$b;)V

    .line 136
    const v0, 0x7f13045c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->h:Landroid/view/View;

    .line 137
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->h:Landroid/view/View;

    const v2, 0x7f13045d

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v2, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$3;

    invoke-direct {v2, p0}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$3;-><init>(Lcom/twitter/android/media/foundmedia/GifGalleryFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    const v0, 0x7f13045a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->i:Landroid/view/View;

    .line 145
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->i:Landroid/view/View;

    const v2, 0x7f13045b

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v2, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$4;

    invoke-direct {v2, p0}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$4;-><init>(Lcom/twitter/android/media/foundmedia/GifGalleryFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    const v0, 0x7f1303b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->j:Landroid/view/View;

    .line 158
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->j:Landroid/view/View;

    const v2, 0x7f1303ba

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->k:Landroid/widget/Switch;

    .line 160
    return-object v1
.end method

.method a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 278
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    if-nez v0, :cond_0

    .line 330
    :goto_0
    return-void

    .line 283
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 286
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 287
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 288
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 289
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/GifGalleryView;->setVisibility(I)V

    .line 290
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/GifGalleryView;->e()V

    .line 291
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/GifGalleryView;->a(Z)Z

    .line 292
    invoke-direct {p0}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->i()V

    goto :goto_0

    .line 296
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 297
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 298
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 299
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/media/widget/GifGalleryView;->setVisibility(I)V

    goto :goto_0

    .line 303
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/GifGalleryView;->d()V

    goto :goto_0

    .line 307
    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/GifGalleryView;->a()V

    goto :goto_0

    .line 311
    :pswitch_4
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 312
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->h:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 313
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 314
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/GifGalleryView;->setVisibility(I)V

    .line 315
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 319
    :pswitch_5
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->i:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 320
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 321
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 322
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/GifGalleryView;->setVisibility(I)V

    .line 323
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 283
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public a(Landroid/content/Context;ILjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 184
    iget v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->e:I

    if-ne v0, p2, :cond_1

    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->f:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 185
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 233
    :goto_0
    return-void

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->l:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 191
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->l:Ljava/util/List;

    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->m:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->a(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0

    .line 195
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 196
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->b:Ljava/lang/String;

    .line 197
    iput-object v2, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->b:Ljava/lang/String;

    .line 198
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->b(Ljava/lang/String;)V

    .line 200
    :cond_2
    iput p2, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->e:I

    .line 201
    iput-object p3, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->f:Ljava/lang/String;

    .line 204
    :cond_3
    invoke-virtual {p0, v2, v2}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 205
    invoke-virtual {p0, v3}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->a(I)V

    .line 207
    const/4 v0, 0x2

    if-ne p2, v0, :cond_4

    new-instance v0, Lwh;

    invoke-direct {v0, p1, p3, v2, v3}, Lwh;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 210
    :goto_1
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$5;

    invoke-direct {v2, p0, p3}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$5;-><init>(Lcom/twitter/android/media/foundmedia/GifGalleryFragment;Ljava/lang/String;)V

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->b:Ljava/lang/String;

    goto :goto_0

    .line 207
    :cond_4
    new-instance v0, Lwj;

    invoke-direct {v0, p1, p3, v2, v3}, Lwj;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1
.end method

.method public a(Lcom/twitter/android/media/foundmedia/GifGalleryFragment$a;)V
    .locals 1

    .prologue
    .line 112
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->a:Ljava/lang/ref/WeakReference;

    .line 113
    return-void
.end method

.method a(Ljava/util/List;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/media/foundmedia/d;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 403
    iput-object p1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->l:Ljava/util/List;

    .line 404
    iput-object p2, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->m:Ljava/lang/String;

    .line 406
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    if-nez v0, :cond_0

    .line 421
    :goto_0
    return-void

    .line 411
    :cond_0
    if-nez p1, :cond_1

    .line 412
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/media/widget/GifGalleryView;->a(Ljava/lang/Iterable;Z)V

    .line 413
    invoke-virtual {p0, v2}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->a(I)V

    goto :goto_0

    .line 414
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 415
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/media/widget/GifGalleryView;->a(Ljava/lang/Iterable;Z)V

    .line 416
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->a(I)V

    goto :goto_0

    .line 418
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    invoke-direct {p0}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->h()Z

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/media/widget/GifGalleryView;->a(Ljava/lang/Iterable;Z)V

    .line 419
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->a(I)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 237
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseFragment;->b()V

    .line 238
    invoke-direct {p0}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->i()V

    .line 239
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->k:Landroid/widget/Switch;

    new-instance v1, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$6;

    invoke-direct {v1, p0}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$6;-><init>(Lcom/twitter/android/media/foundmedia/GifGalleryFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 248
    return-void
.end method

.method b(Ljava/util/List;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/media/foundmedia/d;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 424
    invoke-static {p1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 425
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->m:Ljava/lang/String;

    .line 433
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    if-eqz v0, :cond_0

    .line 434
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->l:Ljava/util/List;

    invoke-direct {p0}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->h()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/media/widget/GifGalleryView;->b(Ljava/lang/Iterable;Z)V

    .line 436
    :cond_0
    return-void

    .line 427
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->l:Ljava/util/List;

    .line 428
    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 429
    invoke-virtual {v0, p1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 430
    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->l:Ljava/util/List;

    .line 431
    iput-object p2, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->m:Ljava/lang/String;

    goto :goto_0
.end method

.method c(Ljava/util/List;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/media/foundmedia/d;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 439
    invoke-static {p1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 440
    iput-object p1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->l:Ljava/util/List;

    .line 441
    iput-object p2, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->m:Ljava/lang/String;

    .line 442
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    if-nez v0, :cond_1

    .line 448
    :cond_0
    :goto_0
    return-void

    .line 446
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->l:Ljava/util/List;

    invoke-direct {p0}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->h()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/media/widget/GifGalleryView;->b(Ljava/lang/Iterable;Z)V

    goto :goto_0
.end method

.method d()V
    .locals 3

    .prologue
    .line 333
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->T:Landroid/content/Context;

    iget v1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->e:I

    iget-object v2, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->f:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 336
    :cond_0
    return-void
.end method

.method e()V
    .locals 5

    .prologue
    const/4 v1, 0x2

    const/4 v4, 0x1

    .line 339
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 371
    :goto_0
    return-void

    .line 342
    :cond_0
    invoke-virtual {p0, v1}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->a(I)V

    .line 343
    iget v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->e:I

    if-ne v0, v1, :cond_1

    new-instance v0, Lwh;

    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->T:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->m:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lwh;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 346
    :goto_1
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$7;

    invoke-direct {v2, p0}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$7;-><init>(Lcom/twitter/android/media/foundmedia/GifGalleryFragment;)V

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->b:Ljava/lang/String;

    goto :goto_0

    .line 343
    :cond_1
    new-instance v0, Lwj;

    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->T:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->m:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lwj;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1
.end method

.method f()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 374
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 400
    :goto_0
    return-void

    .line 377
    :cond_0
    iput-object v3, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->m:Ljava/lang/String;

    .line 378
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->a(I)V

    .line 379
    iget v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->e:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    new-instance v0, Lwh;

    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->T:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->f:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lwh;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 382
    :goto_1
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$8;

    invoke-direct {v2, p0}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$8;-><init>(Lcom/twitter/android/media/foundmedia/GifGalleryFragment;)V

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->b:Ljava/lang/String;

    goto :goto_0

    .line 379
    :cond_1
    new-instance v0, Lwj;

    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->T:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->f:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lwj;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1
.end method

.method g()V
    .locals 3

    .prologue
    .line 451
    iget v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->n:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->l:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 452
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    iget v1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->n:I

    iget v2, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->o:I

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/media/widget/GifGalleryView;->a(II)V

    .line 454
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->n:I

    .line 456
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 259
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseFragment;->onDestroy()V

    .line 260
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 261
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->b(Ljava/lang/String;)V

    .line 263
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 267
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 268
    const-string/jumbo v0, "query"

    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    const-string/jumbo v0, "gallery_type"

    iget v1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->e:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 270
    const-string/jumbo v0, "cursor"

    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    const-string/jumbo v0, "images"

    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->l:Ljava/util/List;

    sget-object v2, Lcom/twitter/model/media/foundmedia/d;->a:Lcom/twitter/util/serialization/l;

    .line 272
    invoke-static {v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 271
    invoke-static {v1, v2}, Lcom/twitter/util/serialization/k;->b(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 273
    const-string/jumbo v0, "first_index"

    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    invoke-virtual {v1}, Lcom/twitter/android/media/widget/GifGalleryView;->getFirstVisibleItemIndex()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 274
    const-string/jumbo v0, "first_offset"

    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    invoke-virtual {v1}, Lcom/twitter/android/media/widget/GifGalleryView;->getFirstVisibleItemOffsetPixels()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 275
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 165
    if-nez p2, :cond_0

    .line 166
    invoke-virtual {p0, v0, v0}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 181
    :goto_0
    return-void

    .line 168
    :cond_0
    const-string/jumbo v0, "query"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->f:Ljava/lang/String;

    .line 170
    const-string/jumbo v0, "gallery_type"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->e:I

    .line 172
    const-string/jumbo v0, "first_index"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->n:I

    .line 173
    const-string/jumbo v0, "first_offset"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->o:I

    .line 175
    const-string/jumbo v0, "images"

    .line 177
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/media/foundmedia/d;->a:Lcom/twitter/util/serialization/l;

    .line 178
    invoke-static {v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    .line 176
    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->b([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const-string/jumbo v1, "cursor"

    .line 179
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 175
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->a(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public q_()V
    .locals 1

    .prologue
    .line 252
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseFragment;->q_()V

    .line 253
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/GifGalleryView;->getFirstVisibleItemIndex()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->n:I

    .line 254
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->c:Lcom/twitter/android/media/widget/GifGalleryView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/GifGalleryView;->getFirstVisibleItemOffsetPixels()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->o:I

    .line 255
    return-void
.end method
