.class Lcom/twitter/android/media/foundmedia/GifGalleryFragment$7;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->e()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/foundmedia/GifGalleryFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/foundmedia/GifGalleryFragment;)V
    .locals 0

    .prologue
    .line 348
    iput-object p1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$7;->a:Lcom/twitter/android/media/foundmedia/GifGalleryFragment;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 348
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$7;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 351
    iget-object v1, p1, Lcom/twitter/library/service/s;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$7;->a:Lcom/twitter/android/media/foundmedia/GifGalleryFragment;

    iget-object v2, v2, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 369
    :goto_0
    return-void

    .line 356
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$7;->a:Lcom/twitter/android/media/foundmedia/GifGalleryFragment;

    iput-object v0, v1, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->b:Ljava/lang/String;

    .line 357
    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$7;->a:Lcom/twitter/android/media/foundmedia/GifGalleryFragment;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->a(I)V

    .line 358
    check-cast p1, Lwi;

    invoke-virtual {p1}, Lwi;->h()Lcom/twitter/model/media/foundmedia/e;

    move-result-object v2

    .line 361
    if-nez v2, :cond_1

    .line 362
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 368
    :goto_1
    iget-object v2, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$7;->a:Lcom/twitter/android/media/foundmedia/GifGalleryFragment;

    invoke-virtual {v2, v1, v0}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->b(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0

    .line 365
    :cond_1
    iget-object v0, v2, Lcom/twitter/model/media/foundmedia/e;->a:Lcom/twitter/model/media/foundmedia/b;

    iget-object v1, v0, Lcom/twitter/model/media/foundmedia/b;->b:Ljava/util/List;

    .line 366
    iget-object v0, v2, Lcom/twitter/model/media/foundmedia/e;->b:Lcom/twitter/model/media/foundmedia/a;

    iget-object v0, v0, Lcom/twitter/model/media/foundmedia/a;->a:Ljava/lang/String;

    goto :goto_1
.end method
