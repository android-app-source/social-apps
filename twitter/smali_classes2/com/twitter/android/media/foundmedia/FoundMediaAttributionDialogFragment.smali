.class public Lcom/twitter/android/media/foundmedia/FoundMediaAttributionDialogFragment;
.super Lcom/twitter/app/common/dialog/BaseDialogFragment;
.source "Twttr"


# instance fields
.field private a:Ljava/lang/String;

.field private c:Lcom/twitter/model/media/foundmedia/FoundMediaProvider;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/twitter/model/media/foundmedia/FoundMediaProvider;)Lcom/twitter/android/media/foundmedia/FoundMediaAttributionDialogFragment;
    .locals 3

    .prologue
    .line 28
    new-instance v0, Lcom/twitter/android/media/foundmedia/FoundMediaAttributionDialogFragment;

    invoke-direct {v0}, Lcom/twitter/android/media/foundmedia/FoundMediaAttributionDialogFragment;-><init>()V

    .line 30
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 31
    const-string/jumbo v2, "uri"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    const-string/jumbo v2, "provider"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 33
    invoke-virtual {v0, v1}, Lcom/twitter/android/media/foundmedia/FoundMediaAttributionDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 34
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/media/foundmedia/FoundMediaAttributionDialogFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/FoundMediaAttributionDialogFragment;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/media/foundmedia/FoundMediaProvider;)V
    .locals 1

    .prologue
    .line 42
    invoke-static {p2, p3}, Lcom/twitter/android/media/foundmedia/FoundMediaAttributionDialogFragment;->a(Ljava/lang/String;Lcom/twitter/model/media/foundmedia/FoundMediaProvider;)Lcom/twitter/android/media/foundmedia/FoundMediaAttributionDialogFragment;

    move-result-object v0

    .line 43
    invoke-virtual {v0, p0, p1}, Lcom/twitter/android/media/foundmedia/FoundMediaAttributionDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 44
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 48
    invoke-super {p0, p1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 49
    invoke-virtual {p0}, Lcom/twitter/android/media/foundmedia/FoundMediaAttributionDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/media/foundmedia/FoundMediaAttributionDialogFragment;->a:Ljava/lang/String;

    .line 51
    invoke-virtual {p0}, Lcom/twitter/android/media/foundmedia/FoundMediaAttributionDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "provider"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 50
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/foundmedia/FoundMediaProvider;

    iput-object v0, p0, Lcom/twitter/android/media/foundmedia/FoundMediaAttributionDialogFragment;->c:Lcom/twitter/model/media/foundmedia/FoundMediaProvider;

    .line 52
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 57
    const v1, 0x7f0a03b6

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/FoundMediaAttributionDialogFragment;->c:Lcom/twitter/model/media/foundmedia/FoundMediaProvider;

    .line 58
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/foundmedia/FoundMediaProvider;

    iget-object v0, v0, Lcom/twitter/model/media/foundmedia/FoundMediaProvider;->c:Ljava/lang/String;

    aput-object v0, v2, v3

    .line 57
    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/media/foundmedia/FoundMediaAttributionDialogFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 60
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/twitter/android/media/foundmedia/FoundMediaAttributionDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f0d0395

    invoke-direct {v1, v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 61
    const v2, 0x7f0a021d

    new-instance v3, Lcom/twitter/android/media/foundmedia/FoundMediaAttributionDialogFragment$1;

    invoke-direct {v3, p0}, Lcom/twitter/android/media/foundmedia/FoundMediaAttributionDialogFragment$1;-><init>(Lcom/twitter/android/media/foundmedia/FoundMediaAttributionDialogFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 72
    const v2, 0x7f0a00f6

    new-instance v3, Lcom/twitter/android/media/foundmedia/FoundMediaAttributionDialogFragment$2;

    invoke-direct {v3, p0}, Lcom/twitter/android/media/foundmedia/FoundMediaAttributionDialogFragment$2;-><init>(Lcom/twitter/android/media/foundmedia/FoundMediaAttributionDialogFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 78
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 79
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
