.class Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment$2;->a:Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment$2;->a:Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;

    iget-object v0, v0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->b:Lcom/twitter/android/media/selection/c;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment$2;->a:Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;

    iget-object v0, v0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->b:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/selection/c;->a()V

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment$2;->a:Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;

    invoke-virtual {v0}, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 88
    iget-object v1, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment$2;->a:Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;

    iget-object v1, v1, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->a:Lcom/twitter/android/media/selection/MediaAttachment;

    if-nez v1, :cond_1

    .line 89
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 94
    :goto_0
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 95
    return-void

    .line 91
    :cond_1
    const/4 v1, -0x1

    iget-object v2, p0, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment$2;->a:Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;

    iget-object v2, v2, Lcom/twitter/android/media/foundmedia/GifPreviewActivityFragment;->a:Lcom/twitter/android/media/selection/MediaAttachment;

    .line 92
    invoke-virtual {v2}, Lcom/twitter/android/media/selection/MediaAttachment;->d()Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/twitter/android/util/j;->a(Lcom/twitter/model/drafts/DraftAttachment;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 91
    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0
.end method
