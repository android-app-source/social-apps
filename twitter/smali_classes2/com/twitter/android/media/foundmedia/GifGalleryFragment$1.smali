.class Lcom/twitter/android/media/foundmedia/GifGalleryFragment$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/widget/GifGalleryView$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/foundmedia/GifGalleryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/foundmedia/GifGalleryFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/foundmedia/GifGalleryFragment;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$1;->a:Lcom/twitter/android/media/foundmedia/GifGalleryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$1;->a:Lcom/twitter/android/media/foundmedia/GifGalleryFragment;

    invoke-virtual {v0}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->e()V

    .line 82
    return-void
.end method

.method public a(Lcom/twitter/model/media/foundmedia/d;)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$1;->a:Lcom/twitter/android/media/foundmedia/GifGalleryFragment;

    iget-object v0, v0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$1;->a:Lcom/twitter/android/media/foundmedia/GifGalleryFragment;

    iget-object v0, v0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$a;

    .line 73
    if-eqz v0, :cond_0

    .line 74
    invoke-interface {v0, p1}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$a;->a(Lcom/twitter/model/media/foundmedia/d;)V

    .line 77
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/model/media/foundmedia/d;Lcom/twitter/media/model/MediaFile;)V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$1;->a:Lcom/twitter/android/media/foundmedia/GifGalleryFragment;

    iget-object v0, v0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$1;->a:Lcom/twitter/android/media/foundmedia/GifGalleryFragment;

    iget-object v0, v0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$a;

    .line 62
    if-nez v0, :cond_1

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    invoke-interface {v0, p1, p2}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$a;->a(Lcom/twitter/model/media/foundmedia/d;Lcom/twitter/media/model/MediaFile;)V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$1;->a:Lcom/twitter/android/media/foundmedia/GifGalleryFragment;

    invoke-virtual {v0}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->f()V

    .line 87
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/twitter/android/media/foundmedia/GifGalleryFragment$1;->a:Lcom/twitter/android/media/foundmedia/GifGalleryFragment;

    invoke-virtual {v0}, Lcom/twitter/android/media/foundmedia/GifGalleryFragment;->g()V

    .line 92
    return-void
.end method
