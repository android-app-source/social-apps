.class Lcom/twitter/android/media/selection/c$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/selection/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/media/selection/c;->a(Ljava/util/List;Lcom/twitter/android/media/selection/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/selection/a;

.field final synthetic b:Ljava/util/Set;

.field final synthetic c:Lcom/twitter/android/media/selection/c;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/selection/c;Lcom/twitter/android/media/selection/a;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 464
    iput-object p1, p0, Lcom/twitter/android/media/selection/c$3;->c:Lcom/twitter/android/media/selection/c;

    iput-object p2, p0, Lcom/twitter/android/media/selection/c$3;->a:Lcom/twitter/android/media/selection/a;

    iput-object p3, p0, Lcom/twitter/android/media/selection/c$3;->b:Ljava/util/Set;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/media/selection/b;)V
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lcom/twitter/android/media/selection/c$3;->b:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 477
    iget-object v0, p0, Lcom/twitter/android/media/selection/c$3;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 479
    iget-object v0, p0, Lcom/twitter/android/media/selection/c$3;->a:Lcom/twitter/android/media/selection/a;

    invoke-interface {v0, p1}, Lcom/twitter/android/media/selection/a;->a(Lcom/twitter/android/media/selection/b;)V

    .line 481
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/android/media/selection/MediaAttachment;)Z
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lcom/twitter/android/media/selection/c$3;->a:Lcom/twitter/android/media/selection/a;

    invoke-interface {v0, p1}, Lcom/twitter/android/media/selection/a;->a(Lcom/twitter/android/media/selection/MediaAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 468
    const/4 v0, 0x1

    .line 471
    :goto_0
    return v0

    .line 470
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/selection/c$3;->b:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 471
    const/4 v0, 0x0

    goto :goto_0
.end method
