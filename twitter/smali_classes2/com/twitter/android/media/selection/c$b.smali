.class Lcom/twitter/android/media/selection/c$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/selection/c$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/media/selection/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/selection/c;

.field private final b:Lcom/twitter/model/drafts/DraftAttachment;

.field private final c:Lcom/twitter/android/media/selection/a;

.field private d:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/selection/c;Lcom/twitter/model/drafts/DraftAttachment;Lcom/twitter/android/media/selection/a;)V
    .locals 0

    .prologue
    .line 573
    iput-object p1, p0, Lcom/twitter/android/media/selection/c$b;->a:Lcom/twitter/android/media/selection/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 574
    iput-object p2, p0, Lcom/twitter/android/media/selection/c$b;->b:Lcom/twitter/model/drafts/DraftAttachment;

    .line 575
    iput-object p3, p0, Lcom/twitter/android/media/selection/c$b;->c:Lcom/twitter/android/media/selection/a;

    .line 576
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/media/selection/c$b;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lcom/twitter/android/media/selection/c$b;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/media/selection/c$b;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 565
    iput-object p1, p0, Lcom/twitter/android/media/selection/c$b;->d:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/twitter/android/media/selection/c$b;)Lcom/twitter/model/drafts/DraftAttachment;
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lcom/twitter/android/media/selection/c$b;->b:Lcom/twitter/model/drafts/DraftAttachment;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/media/selection/c$b;)Lcom/twitter/android/media/selection/a;
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lcom/twitter/android/media/selection/c$b;->c:Lcom/twitter/android/media/selection/a;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 581
    iget-object v0, p0, Lcom/twitter/android/media/selection/c$b;->b:Lcom/twitter/model/drafts/DraftAttachment;

    iget-object v0, v0, Lcom/twitter/model/drafts/DraftAttachment;->e:Landroid/net/Uri;

    return-object v0
.end method

.method public b()V
    .locals 5
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 587
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 589
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/media/manager/f;

    iget-object v2, p0, Lcom/twitter/android/media/selection/c$b;->a:Lcom/twitter/android/media/selection/c;

    .line 591
    invoke-static {v2}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/android/media/selection/c;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/media/selection/c$b;->b:Lcom/twitter/model/drafts/DraftAttachment;

    iget-object v3, v3, Lcom/twitter/model/drafts/DraftAttachment;->f:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/twitter/media/model/MediaType;->c:Lcom/twitter/media/model/MediaType;

    invoke-direct {v1, v2, v3, v4}, Lcom/twitter/library/media/manager/f;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/media/model/MediaType;)V

    new-instance v2, Lcom/twitter/android/media/selection/c$b$1;

    invoke-direct {v2, p0}, Lcom/twitter/android/media/selection/c$b$1;-><init>(Lcom/twitter/android/media/selection/c$b;)V

    .line 589
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/selection/c$b;->d:Ljava/lang/String;

    .line 623
    return-void
.end method

.method public c()V
    .locals 2
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 628
    iget-object v0, p0, Lcom/twitter/android/media/selection/c$b;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 629
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/media/selection/c$b;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->b(Ljava/lang/String;)V

    .line 630
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/media/selection/c$b;->d:Ljava/lang/String;

    .line 632
    :cond_0
    return-void
.end method
