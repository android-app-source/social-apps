.class public Lcom/twitter/android/media/selection/c;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/media/selection/c$b;,
        Lcom/twitter/android/media/selection/c$a;,
        Lcom/twitter/android/media/selection/c$c;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Lcom/twitter/android/media/selection/c$c;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/android/media/selection/b;

.field private final c:Landroid/content/Context;

.field private final d:Lbrp;

.field private final e:Lcom/twitter/library/client/Session;

.field private final f:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/twitter/media/model/MediaType;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/lang/String;

.field private final h:Lcom/twitter/android/composer/ComposerType;

.field private i:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;Lbrp;Ljava/lang/String;Ljava/util/EnumSet;ILcom/twitter/android/composer/ComposerType;Lcom/twitter/library/client/Session;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lbrp;",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/twitter/media/model/MediaType;",
            ">;I",
            "Lcom/twitter/android/composer/ComposerType;",
            "Lcom/twitter/library/client/Session;",
            ")V"
        }
    .end annotation

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/media/selection/c;->a:Ljava/util/Map;

    .line 140
    iput-object p1, p0, Lcom/twitter/android/media/selection/c;->c:Landroid/content/Context;

    .line 141
    iput-object p2, p0, Lcom/twitter/android/media/selection/c;->d:Lbrp;

    .line 142
    invoke-static {p3}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/media/selection/c;->g:Ljava/lang/String;

    .line 143
    iput-object p6, p0, Lcom/twitter/android/media/selection/c;->h:Lcom/twitter/android/composer/ComposerType;

    .line 144
    iput-object p4, p0, Lcom/twitter/android/media/selection/c;->f:Ljava/util/EnumSet;

    .line 145
    iput-object p7, p0, Lcom/twitter/android/media/selection/c;->e:Lcom/twitter/library/client/Session;

    .line 146
    new-instance v0, Lcom/twitter/android/media/selection/b;

    invoke-direct {v0, p5}, Lcom/twitter/android/media/selection/b;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/media/selection/c;->b:Lcom/twitter/android/media/selection/b;

    .line 147
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lbrp;Ljava/lang/String;Ljava/util/EnumSet;ILcom/twitter/android/composer/ComposerType;Lcom/twitter/library/client/Session;Lcom/twitter/app/common/util/j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lbrp;",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/twitter/media/model/MediaType;",
            ">;I",
            "Lcom/twitter/android/composer/ComposerType;",
            "Lcom/twitter/library/client/Session;",
            "Lcom/twitter/app/common/util/j;",
            ")V"
        }
    .end annotation

    .prologue
    .line 105
    invoke-direct/range {p0 .. p7}, Lcom/twitter/android/media/selection/c;-><init>(Landroid/content/Context;Lbrp;Ljava/lang/String;Ljava/util/EnumSet;ILcom/twitter/android/composer/ComposerType;Lcom/twitter/library/client/Session;)V

    .line 106
    new-instance v0, Lcom/twitter/android/media/selection/c$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/media/selection/c$1;-><init>(Lcom/twitter/android/media/selection/c;)V

    invoke-interface {p8, v0}, Lcom/twitter/app/common/util/j;->a(Lcom/twitter/app/common/util/b$a;)V

    .line 112
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lbrp;Ljava/lang/String;Ljava/util/EnumSet;ILcom/twitter/android/composer/ComposerType;Lcom/twitter/library/client/Session;Lcom/twitter/app/common/util/k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lbrp;",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/twitter/media/model/MediaType;",
            ">;I",
            "Lcom/twitter/android/composer/ComposerType;",
            "Lcom/twitter/library/client/Session;",
            "Lcom/twitter/app/common/util/k;",
            ")V"
        }
    .end annotation

    .prologue
    .line 123
    invoke-direct/range {p0 .. p7}, Lcom/twitter/android/media/selection/c;-><init>(Landroid/content/Context;Lbrp;Ljava/lang/String;Ljava/util/EnumSet;ILcom/twitter/android/composer/ComposerType;Lcom/twitter/library/client/Session;)V

    .line 124
    new-instance v0, Lcom/twitter/android/media/selection/c$2;

    invoke-direct {v0, p0}, Lcom/twitter/android/media/selection/c$2;-><init>(Lcom/twitter/android/media/selection/c;)V

    invoke-interface {p8, v0}, Lcom/twitter/app/common/util/k;->a(Lcom/twitter/app/common/util/d;)V

    .line 130
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/media/selection/c;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/android/media/selection/c;->c:Landroid/content/Context;

    return-object v0
.end method

.method private a(Lcom/twitter/android/media/selection/c$c;)V
    .locals 2

    .prologue
    .line 419
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 420
    iget-object v0, p0, Lcom/twitter/android/media/selection/c;->a:Ljava/util/Map;

    invoke-interface {p1}, Lcom/twitter/android/media/selection/c$c;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 421
    invoke-interface {p1}, Lcom/twitter/android/media/selection/c$c;->b()V

    .line 422
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/media/selection/c;Lcom/twitter/android/media/selection/a;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/twitter/android/media/selection/c;->b(Lcom/twitter/android/media/selection/a;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/media/selection/c;Lcom/twitter/android/media/selection/c$c;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/twitter/android/media/selection/c;->b(Lcom/twitter/android/media/selection/c$c;)V

    return-void
.end method

.method private a(Lcom/twitter/model/media/EditableImage;J)V
    .locals 4

    .prologue
    .line 350
    iget v0, p1, Lcom/twitter/model/media/EditableImage;->c:I

    if-eqz v0, :cond_0

    .line 351
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p2, p3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget v1, p1, Lcom/twitter/model/media/EditableImage;->c:I

    int-to-long v2, v1

    .line 352
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/android/media/selection/c;->g:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    .line 353
    invoke-virtual {p1}, Lcom/twitter/model/media/EditableImage;->g()Lcom/twitter/model/media/MediaSource;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/model/media/MediaSource;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "filters"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "filtered"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 354
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 356
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/media/selection/c;)Lcom/twitter/android/media/selection/b;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/android/media/selection/c;->b:Lcom/twitter/android/media/selection/b;

    return-object v0
.end method

.method private b(Lcom/twitter/android/media/selection/a;)V
    .locals 1

    .prologue
    .line 430
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/media/selection/c;->i:Z

    .line 431
    invoke-virtual {p0}, Lcom/twitter/android/media/selection/c;->d()Lcom/twitter/android/media/selection/b;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/twitter/android/media/selection/a;->a(Lcom/twitter/android/media/selection/b;)V

    .line 432
    iget-object v0, p0, Lcom/twitter/android/media/selection/c;->b:Lcom/twitter/android/media/selection/b;

    invoke-virtual {v0}, Lcom/twitter/android/media/selection/b;->d()V

    .line 433
    return-void
.end method

.method private b(Lcom/twitter/android/media/selection/c$c;)V
    .locals 2

    .prologue
    .line 426
    iget-object v0, p0, Lcom/twitter/android/media/selection/c;->a:Ljava/util/Map;

    invoke-interface {p1}, Lcom/twitter/android/media/selection/c$c;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 427
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 151
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 152
    iget-object v0, p0, Lcom/twitter/android/media/selection/c;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/selection/c$c;

    .line 153
    invoke-interface {v0}, Lcom/twitter/android/media/selection/c$c;->c()V

    goto :goto_0

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/selection/c;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 156
    return-void
.end method

.method public a(IILandroid/content/Intent;Lcom/twitter/android/media/selection/a;)V
    .locals 6

    .prologue
    const/4 v0, -0x1

    .line 270
    packed-switch p1, :pswitch_data_0

    .line 341
    :cond_0
    :goto_0
    return-void

    .line 272
    :pswitch_0
    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    .line 275
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 276
    if-nez v2, :cond_1

    .line 277
    iget-object v0, p0, Lcom/twitter/android/media/selection/c;->c:Landroid/content/Context;

    const v1, 0x7f0a04b2

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 280
    :cond_1
    new-instance v0, Lcom/twitter/android/media/selection/c$a;

    sget-object v3, Lcom/twitter/model/media/MediaSource;->c:Lcom/twitter/model/media/MediaSource;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/media/selection/c$a;-><init>(Lcom/twitter/android/media/selection/c;Landroid/net/Uri;Lcom/twitter/model/media/MediaSource;Lcom/twitter/android/media/selection/a;Z)V

    invoke-direct {p0, v0}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/android/media/selection/c$c;)V

    goto :goto_0

    .line 286
    :pswitch_1
    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    .line 287
    invoke-static {p3}, Lcom/twitter/android/VideoEditorActivity;->a(Landroid/content/Intent;)Lcom/twitter/model/media/EditableVideo;

    move-result-object v0

    .line 288
    if-eqz v0, :cond_0

    .line 289
    invoke-virtual {p0, v0, p4}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/selection/a;)V

    goto :goto_0

    .line 297
    :pswitch_2
    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    .line 301
    const-string/jumbo v0, "media_type"

    .line 302
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/MediaType;

    .line 303
    const-string/jumbo v1, "media_file"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/twitter/media/model/MediaFile;

    .line 305
    sget-object v2, Lcom/twitter/android/media/selection/c$4;->a:[I

    invoke-virtual {v0}, Lcom/twitter/media/model/MediaType;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_1

    .line 317
    sget-object v0, Lcom/twitter/model/media/MediaSource;->d:Lcom/twitter/model/media/MediaSource;

    invoke-static {v1, v0}, Lcom/twitter/model/media/EditableMedia;->a(Lcom/twitter/media/model/MediaFile;Lcom/twitter/model/media/MediaSource;)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 318
    new-instance v1, Lcom/twitter/android/media/selection/MediaAttachment;

    new-instance v2, Lcom/twitter/model/drafts/DraftAttachment;

    invoke-direct {v2, v0}, Lcom/twitter/model/drafts/DraftAttachment;-><init>(Lcom/twitter/model/media/EditableMedia;)V

    invoke-direct {v1, v2}, Lcom/twitter/android/media/selection/MediaAttachment;-><init>(Lcom/twitter/model/drafts/DraftAttachment;)V

    .line 319
    invoke-interface {p4, v1}, Lcom/twitter/android/media/selection/a;->a(Lcom/twitter/android/media/selection/MediaAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    invoke-virtual {p0, v1, p4}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/media/selection/a;)V

    goto :goto_0

    .line 307
    :pswitch_3
    sget-object v0, Lcom/twitter/model/media/MediaSource;->d:Lcom/twitter/model/media/MediaSource;

    invoke-static {v1, v0}, Lcom/twitter/model/media/EditableMedia;->a(Lcom/twitter/media/model/MediaFile;Lcom/twitter/model/media/MediaSource;)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p4}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;Lcom/twitter/android/media/selection/a;)V

    goto :goto_0

    .line 312
    :pswitch_4
    sget-object v0, Lcom/twitter/model/media/MediaSource;->d:Lcom/twitter/model/media/MediaSource;

    invoke-static {v1, v0}, Lcom/twitter/model/media/EditableMedia;->a(Lcom/twitter/media/model/MediaFile;Lcom/twitter/model/media/MediaSource;)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    invoke-virtual {p0, v0, p4}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/selection/a;)V

    goto/16 :goto_0

    .line 328
    :pswitch_5
    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    .line 330
    invoke-static {p3}, Lcom/twitter/android/media/imageeditor/EditImageActivity;->a(Landroid/content/Intent;)Lcom/twitter/model/media/EditableImage;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableMedia;

    .line 332
    invoke-static {p3}, Lcom/twitter/android/media/imageeditor/EditImageActivity;->b(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    .line 329
    invoke-virtual {p0, v0, p4, v1}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/selection/a;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 270
    nop

    :pswitch_data_0
    .packed-switch 0x101
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 305
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 384
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 385
    iget-object v0, p0, Lcom/twitter/android/media/selection/c;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/selection/c$c;

    .line 386
    if-eqz v0, :cond_0

    .line 387
    invoke-interface {v0}, Lcom/twitter/android/media/selection/c$c;->c()V

    .line 388
    iget-object v0, p0, Lcom/twitter/android/media/selection/c;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 390
    :cond_0
    return-void
.end method

.method public a(Landroid/net/Uri;Lcom/twitter/android/media/selection/a;)V
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Lcom/twitter/android/media/selection/c;->b:Lcom/twitter/android/media/selection/b;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/selection/b;->b(Landroid/net/Uri;)V

    .line 403
    invoke-direct {p0, p2}, Lcom/twitter/android/media/selection/c;->b(Lcom/twitter/android/media/selection/a;)V

    .line 404
    return-void
.end method

.method public a(Landroid/net/Uri;ZLcom/twitter/android/media/selection/a;)V
    .locals 6

    .prologue
    .line 160
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 161
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/media/selection/c;->i:Z

    .line 162
    new-instance v0, Lcom/twitter/android/media/selection/c$a;

    sget-object v3, Lcom/twitter/model/media/MediaSource;->b:Lcom/twitter/model/media/MediaSource;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/media/selection/c$a;-><init>(Lcom/twitter/android/media/selection/c;Landroid/net/Uri;Lcom/twitter/model/media/MediaSource;Lcom/twitter/android/media/selection/a;Z)V

    invoke-direct {p0, v0}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/android/media/selection/c$c;)V

    .line 163
    return-void
.end method

.method public a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/media/selection/a;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 436
    iget-object v0, p0, Lcom/twitter/android/media/selection/c;->b:Lcom/twitter/android/media/selection/b;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/selection/b;->a(Lcom/twitter/android/media/selection/MediaAttachment;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 449
    :cond_0
    :goto_0
    return-void

    .line 439
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/android/media/selection/MediaAttachment;->c()Lcom/twitter/model/media/MediaSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/media/MediaSource;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 440
    iget v0, p1, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/twitter/android/media/selection/MediaAttachment;->b()Lcom/twitter/media/model/MediaType;

    move-result-object v0

    sget-object v2, Lcom/twitter/media/model/MediaType;->c:Lcom/twitter/media/model/MediaType;

    if-ne v0, v2, :cond_3

    :cond_2
    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 441
    invoke-direct {p0, p2}, Lcom/twitter/android/media/selection/c;->b(Lcom/twitter/android/media/selection/a;)V

    .line 442
    iget v0, p1, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    if-ne v0, v1, :cond_0

    .line 443
    invoke-virtual {p1}, Lcom/twitter/android/media/selection/MediaAttachment;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/selection/c;->a(Landroid/net/Uri;)V

    .line 444
    new-instance v0, Lcom/twitter/android/media/selection/c$b;

    invoke-virtual {p1}, Lcom/twitter/android/media/selection/MediaAttachment;->d()Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v1

    invoke-direct {v0, p0, v1, p2}, Lcom/twitter/android/media/selection/c$b;-><init>(Lcom/twitter/android/media/selection/c;Lcom/twitter/model/drafts/DraftAttachment;Lcom/twitter/android/media/selection/a;)V

    invoke-direct {p0, v0}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/android/media/selection/c$c;)V

    goto :goto_0

    .line 440
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 448
    :cond_4
    invoke-direct {p0, p2}, Lcom/twitter/android/media/selection/c;->b(Lcom/twitter/android/media/selection/a;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/media/selection/a;)V
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/twitter/android/media/selection/c;->b:Lcom/twitter/android/media/selection/b;

    invoke-virtual {v0}, Lcom/twitter/android/media/selection/b;->a()V

    .line 412
    if-eqz p1, :cond_0

    .line 413
    invoke-direct {p0, p1}, Lcom/twitter/android/media/selection/c;->b(Lcom/twitter/android/media/selection/a;)V

    .line 415
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;Lcom/twitter/android/media/selection/a;)V
    .locals 1

    .prologue
    .line 212
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;Lcom/twitter/android/media/selection/a;Z)V

    .line 213
    return-void
.end method

.method public a(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;Lcom/twitter/android/media/selection/a;Z)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 218
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 219
    sget-object v1, Lcom/twitter/android/media/selection/c$4;->a:[I

    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->f()Lcom/twitter/media/model/MediaType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/media/model/MediaType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 245
    invoke-virtual {p0, p1, p3}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/selection/a;)V

    .line 248
    :goto_0
    return-void

    .line 221
    :pswitch_0
    invoke-virtual {p0, p1, p3, v3}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/selection/a;I)V

    goto :goto_0

    .line 225
    :pswitch_1
    iget-object v1, p0, Lcom/twitter/android/media/selection/c;->c:Landroid/content/Context;

    check-cast p1, Lcom/twitter/model/media/EditableVideo;

    invoke-static {v1, p1, p4}, Lcom/twitter/android/VideoEditorActivity;->a(Landroid/content/Context;Lcom/twitter/model/media/EditableVideo;Z)Landroid/content/Intent;

    move-result-object v1

    .line 227
    iget-object v2, p0, Lcom/twitter/android/media/selection/c;->d:Lbrp;

    const/16 v3, 0x104

    invoke-interface {v2, v1, v3, v0}, Lbrp;->a(Landroid/content/Intent;ILandroid/os/Bundle;)V

    goto :goto_0

    .line 231
    :pswitch_2
    iget-object v1, p0, Lcom/twitter/android/media/selection/c;->c:Landroid/content/Context;

    .line 232
    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->d()Landroid/net/Uri;

    move-result-object v2

    .line 231
    invoke-static {v1, v2}, Lcom/twitter/android/media/camera/CameraActivity;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 233
    if-nez p2, :cond_0

    .line 238
    :goto_1
    iget-object v2, p0, Lcom/twitter/android/media/selection/c;->d:Lbrp;

    const/16 v3, 0x105

    invoke-interface {v2, v1, v3, v0}, Lbrp;->a(Landroid/content/Intent;ILandroid/os/Bundle;)V

    goto :goto_0

    .line 236
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 237
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 235
    invoke-static {p2, v3, v3, v0, v2}, Landroid/support/v4/app/ActivityOptionsCompat;->makeScaleUpAnimation(Landroid/view/View;IIII)Landroid/support/v4/app/ActivityOptionsCompat;

    move-result-object v0

    .line 237
    invoke-virtual {v0}, Landroid/support/v4/app/ActivityOptionsCompat;->toBundle()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_1

    .line 219
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/selection/a;)V
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/selection/a;Ljava/lang/String;)V

    .line 168
    return-void
.end method

.method public a(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/selection/a;I)V
    .locals 4

    .prologue
    .line 253
    iget-object v0, p0, Lcom/twitter/android/media/selection/c;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/media/filters/c;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 254
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/selection/a;)V

    .line 264
    :goto_0
    return-void

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/selection/c;->c:Landroid/content/Context;

    check-cast p1, Lcom/twitter/model/media/EditableImage;

    iget-object v1, p0, Lcom/twitter/android/media/selection/c;->h:Lcom/twitter/android/composer/ComposerType;

    iget-object v2, p0, Lcom/twitter/android/media/selection/c;->g:Ljava/lang/String;

    invoke-static {v0, p1, v1, v2, p3}, Lcom/twitter/android/media/imageeditor/EditImageActivity;->a(Landroid/content/Context;Lcom/twitter/model/media/EditableImage;Lcom/twitter/android/composer/ComposerType;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 263
    iget-object v1, p0, Lcom/twitter/android/media/selection/c;->d:Lbrp;

    const/16 v2, 0x103

    const/4 v3, 0x0

    invoke-interface {v1, v0, v2, v3}, Lbrp;->a(Landroid/content/Intent;ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/selection/a;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 173
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 174
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/media/selection/c;->i:Z

    .line 175
    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->f()Lcom/twitter/media/model/MediaType;

    move-result-object v0

    .line 176
    new-instance v1, Lcom/twitter/model/drafts/DraftAttachment;

    invoke-direct {v1, p1}, Lcom/twitter/model/drafts/DraftAttachment;-><init>(Lcom/twitter/model/media/EditableMedia;)V

    .line 178
    iget-object v2, p0, Lcom/twitter/android/media/selection/c;->f:Ljava/util/EnumSet;

    invoke-virtual {v2, v0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 179
    sget-object v2, Lcom/twitter/media/model/MediaType;->c:Lcom/twitter/media/model/MediaType;

    if-ne v0, v2, :cond_0

    .line 180
    iget-object v0, p1, Lcom/twitter/model/media/EditableMedia;->k:Lcom/twitter/media/model/MediaFile;

    iget-object v0, v0, Lcom/twitter/media/model/MediaFile;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-static {}, Lwe;->a()I

    move-result v0

    int-to-long v4, v0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    .line 181
    new-instance v0, Lcom/twitter/android/media/selection/MediaAttachment;

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/media/selection/MediaAttachment;-><init>(Lcom/twitter/model/drafts/DraftAttachment;I)V

    .line 182
    invoke-virtual {p0, v0, p2}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/media/selection/a;)V

    .line 207
    :goto_0
    return-void

    .line 186
    :cond_0
    instance-of v0, p1, Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_1

    .line 187
    check-cast p1, Lcom/twitter/model/media/EditableImage;

    .line 188
    iget-object v0, p0, Lcom/twitter/android/media/selection/c;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/media/selection/c;->e:Lcom/twitter/library/client/Session;

    .line 192
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 188
    invoke-static {p1, v0, p3, v2, v3}, Lbrv;->a(Lcom/twitter/model/media/EditableImage;Ljava/lang/String;Ljava/lang/String;J)V

    .line 193
    iget-object v0, p0, Lcom/twitter/android/media/selection/c;->e:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {p0, p1, v2, v3}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/model/media/EditableImage;J)V

    .line 195
    :cond_1
    new-instance v0, Lcom/twitter/android/media/selection/MediaAttachment;

    invoke-direct {v0, v1}, Lcom/twitter/android/media/selection/MediaAttachment;-><init>(Lcom/twitter/model/drafts/DraftAttachment;)V

    .line 196
    invoke-virtual {p0, v0, p2}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/media/selection/a;)V

    goto :goto_0

    .line 197
    :cond_2
    sget-object v2, Lcom/twitter/media/model/MediaType;->c:Lcom/twitter/media/model/MediaType;

    if-ne v0, v2, :cond_3

    iget-object v0, p0, Lcom/twitter/android/media/selection/c;->f:Ljava/util/EnumSet;

    sget-object v2, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 198
    iget-object v0, p1, Lcom/twitter/model/media/EditableMedia;->k:Lcom/twitter/media/model/MediaFile;

    check-cast v0, Lcom/twitter/media/model/AnimatedGifFile;

    invoke-static {v0}, Lcom/twitter/media/model/ImageFile;->a(Lcom/twitter/media/model/AnimatedGifFile;)Lcom/twitter/media/model/ImageFile;

    move-result-object v0

    .line 200
    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->c()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->g()Lcom/twitter/model/media/MediaSource;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/twitter/model/media/EditableMedia;->a(Lcom/twitter/media/model/MediaFile;Landroid/net/Uri;Lcom/twitter/model/media/MediaSource;)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 201
    invoke-virtual {p0, v0, p2, p3}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/selection/a;Ljava/lang/String;)V

    goto :goto_0

    .line 203
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/media/selection/c;->c:Landroid/content/Context;

    const v2, 0x7f0a04b2

    invoke-static {v0, v2}, Lcom/twitter/util/ui/k;->a(Landroid/content/Context;I)V

    .line 204
    new-instance v0, Lcom/twitter/android/media/selection/MediaAttachment;

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/media/selection/MediaAttachment;-><init>(Lcom/twitter/model/drafts/DraftAttachment;I)V

    .line 205
    invoke-virtual {p0, v0, p2}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/media/selection/a;)V

    goto :goto_0
.end method

.method public a(Ljava/util/List;Lcom/twitter/android/media/selection/a;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;",
            "Lcom/twitter/android/media/selection/a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 461
    invoke-static {p1}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 462
    new-instance v1, Ljava/util/HashSet;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 463
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    .line 464
    new-instance v3, Lcom/twitter/android/media/selection/c$3;

    invoke-direct {v3, p0, p2, v1}, Lcom/twitter/android/media/selection/c$3;-><init>(Lcom/twitter/android/media/selection/c;Lcom/twitter/android/media/selection/a;Ljava/util/Set;)V

    .line 483
    new-instance v4, Lcom/twitter/android/media/selection/MediaAttachment;

    invoke-direct {v4, v0}, Lcom/twitter/android/media/selection/MediaAttachment;-><init>(Lcom/twitter/model/drafts/DraftAttachment;)V

    invoke-virtual {p0, v4, v3}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/media/selection/a;)V

    goto :goto_0

    .line 485
    :cond_0
    return-void
.end method

.method public a(ZI)V
    .locals 1

    .prologue
    .line 374
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/media/selection/c;->a(ZII)V

    .line 375
    return-void
.end method

.method public a(ZII)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 360
    if-nez p2, :cond_0

    .line 371
    :goto_0
    return-void

    .line 364
    :cond_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/media/selection/c;->e:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    aput-object v4, v1, v5

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/android/media/selection/c;->g:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object v4, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "twitter_camera"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    .line 365
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 366
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 368
    iget-object v0, p0, Lcom/twitter/android/media/selection/c;->d:Lbrp;

    iget-object v1, p0, Lcom/twitter/android/media/selection/c;->c:Landroid/content/Context;

    .line 369
    invoke-static {v1, p2, p1, v5, p3}, Lcom/twitter/android/media/camera/CameraActivity;->a(Landroid/content/Context;IZZI)Landroid/content/Intent;

    move-result-object v1

    const/16 v2, 0x102

    .line 368
    invoke-interface {v0, v1, v2, v4}, Lbrp;->a(Landroid/content/Intent;ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    .line 378
    iget-object v0, p0, Lcom/twitter/android/media/selection/c;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/media/selection/c;->d:Lbrp;

    const/16 v2, 0x101

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lbrv;->a(Landroid/content/Context;Lbrp;ILandroid/os/Bundle;)Z

    .line 380
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 393
    iget-boolean v0, p0, Lcom/twitter/android/media/selection/c;->i:Z

    return v0
.end method

.method public d()Lcom/twitter/android/media/selection/b;
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lcom/twitter/android/media/selection/c;->b:Lcom/twitter/android/media/selection/b;

    return-object v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 407
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/android/media/selection/a;)V

    .line 408
    return-void
.end method
