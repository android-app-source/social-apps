.class public Lcom/twitter/android/media/selection/MediaAttachment;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/android/media/selection/MediaAttachment;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field private final b:Lcom/twitter/model/drafts/DraftAttachment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/twitter/android/media/selection/MediaAttachment$1;

    invoke-direct {v0}, Lcom/twitter/android/media/selection/MediaAttachment$1;-><init>()V

    sput-object v0, Lcom/twitter/android/media/selection/MediaAttachment;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    .line 65
    const-class v0, Lcom/twitter/model/drafts/DraftAttachment;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    iput-object v0, p0, Lcom/twitter/android/media/selection/MediaAttachment;->b:Lcom/twitter/model/drafts/DraftAttachment;

    .line 66
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/drafts/DraftAttachment;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 54
    iget v1, p1, Lcom/twitter/model/drafts/DraftAttachment;->c:I

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/twitter/android/media/selection/MediaAttachment;-><init>(Lcom/twitter/model/drafts/DraftAttachment;I)V

    .line 55
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/drafts/DraftAttachment;I)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/twitter/android/media/selection/MediaAttachment;->b:Lcom/twitter/model/drafts/DraftAttachment;

    .line 59
    iput p2, p0, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    .line 60
    return-void
.end method


# virtual methods
.method public a()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/twitter/android/media/selection/MediaAttachment;->b:Lcom/twitter/model/drafts/DraftAttachment;

    iget-object v0, v0, Lcom/twitter/model/drafts/DraftAttachment;->e:Landroid/net/Uri;

    return-object v0
.end method

.method public a(I)Lcom/twitter/model/media/EditableMedia;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/android/media/selection/MediaAttachment;->b:Lcom/twitter/model/drafts/DraftAttachment;

    invoke-virtual {v0, p1}, Lcom/twitter/model/drafts/DraftAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/android/media/selection/MediaAttachment;)V
    .locals 2

    .prologue
    .line 108
    iget-object v1, p0, Lcom/twitter/android/media/selection/MediaAttachment;->b:Lcom/twitter/model/drafts/DraftAttachment;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/model/drafts/DraftAttachment;->b(Lcom/twitter/model/drafts/DraftAttachment;)Lrx/g;

    .line 109
    return-void

    .line 108
    :cond_0
    iget-object v0, p1, Lcom/twitter/android/media/selection/MediaAttachment;->b:Lcom/twitter/model/drafts/DraftAttachment;

    goto :goto_0
.end method

.method public b()Lcom/twitter/media/model/MediaType;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/media/selection/MediaAttachment;->b:Lcom/twitter/model/drafts/DraftAttachment;

    iget-object v0, v0, Lcom/twitter/model/drafts/DraftAttachment;->g:Lcom/twitter/media/model/MediaType;

    return-object v0
.end method

.method public b(I)Z
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/twitter/android/media/selection/MediaAttachment;->b:Lcom/twitter/model/drafts/DraftAttachment;

    invoke-virtual {v0, p1}, Lcom/twitter/model/drafts/DraftAttachment;->b(I)Z

    move-result v0

    return v0
.end method

.method public c()Lcom/twitter/model/media/MediaSource;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/twitter/android/media/selection/MediaAttachment;->b:Lcom/twitter/model/drafts/DraftAttachment;

    iget-object v0, v0, Lcom/twitter/model/drafts/DraftAttachment;->h:Lcom/twitter/model/media/MediaSource;

    return-object v0
.end method

.method public d()Lcom/twitter/model/drafts/DraftAttachment;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/twitter/android/media/selection/MediaAttachment;->b:Lcom/twitter/model/drafts/DraftAttachment;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 113
    if-ne p0, p1, :cond_1

    .line 120
    :cond_0
    :goto_0
    return v0

    .line 116
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 117
    goto :goto_0

    .line 119
    :cond_3
    check-cast p1, Lcom/twitter/android/media/selection/MediaAttachment;

    .line 120
    iget v2, p0, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    iget v3, p1, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/twitter/android/media/selection/MediaAttachment;->b:Lcom/twitter/model/drafts/DraftAttachment;

    iget-object v3, p1, Lcom/twitter/android/media/selection/MediaAttachment;->b:Lcom/twitter/model/drafts/DraftAttachment;

    invoke-virtual {v2, v3}, Lcom/twitter/model/drafts/DraftAttachment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/android/media/selection/MediaAttachment;->b:Lcom/twitter/model/drafts/DraftAttachment;

    invoke-virtual {v0}, Lcom/twitter/model/drafts/DraftAttachment;->hashCode()I

    move-result v0

    .line 126
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    add-int/2addr v0, v1

    .line 127
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 138
    iget-object v0, p0, Lcom/twitter/android/media/selection/MediaAttachment;->b:Lcom/twitter/model/drafts/DraftAttachment;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 139
    return-void
.end method
