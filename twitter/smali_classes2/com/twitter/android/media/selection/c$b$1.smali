.class Lcom/twitter/android/media/selection/c$b$1;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/media/selection/c$b;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/media/selection/c$b;


# direct methods
.method constructor <init>(Lcom/twitter/android/media/selection/c$b;)V
    .locals 0

    .prologue
    .line 592
    iput-object p1, p0, Lcom/twitter/android/media/selection/c$b$1;->a:Lcom/twitter/android/media/selection/c$b;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 592
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/android/media/selection/c$b$1;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 6
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 596
    iget-object v0, p0, Lcom/twitter/android/media/selection/c$b$1;->a:Lcom/twitter/android/media/selection/c$b;

    invoke-static {v0}, Lcom/twitter/android/media/selection/c$b;->a(Lcom/twitter/android/media/selection/c$b;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 621
    :goto_0
    return-void

    .line 600
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/media/selection/c$b$1;->a:Lcom/twitter/android/media/selection/c$b;

    iget-object v0, v0, Lcom/twitter/android/media/selection/c$b;->a:Lcom/twitter/android/media/selection/c;

    iget-object v1, p0, Lcom/twitter/android/media/selection/c$b$1;->a:Lcom/twitter/android/media/selection/c$b;

    invoke-static {v0, v1}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/android/media/selection/c;Lcom/twitter/android/media/selection/c$c;)V

    .line 601
    iget-object v0, p0, Lcom/twitter/android/media/selection/c$b$1;->a:Lcom/twitter/android/media/selection/c$b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/media/selection/c$b;->a(Lcom/twitter/android/media/selection/c$b;Ljava/lang/String;)Ljava/lang/String;

    .line 602
    check-cast p1, Lcom/twitter/library/media/manager/f;

    invoke-virtual {p1}, Lcom/twitter/library/media/manager/f;->a()Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    .line 603
    if-nez v0, :cond_1

    .line 604
    iget-object v0, p0, Lcom/twitter/android/media/selection/c$b$1;->a:Lcom/twitter/android/media/selection/c$b;

    iget-object v0, v0, Lcom/twitter/android/media/selection/c$b;->a:Lcom/twitter/android/media/selection/c;

    invoke-static {v0}, Lcom/twitter/android/media/selection/c;->b(Lcom/twitter/android/media/selection/c;)Lcom/twitter/android/media/selection/b;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/media/selection/MediaAttachment;

    iget-object v2, p0, Lcom/twitter/android/media/selection/c$b$1;->a:Lcom/twitter/android/media/selection/c$b;

    .line 605
    invoke-static {v2}, Lcom/twitter/android/media/selection/c$b;->b(Lcom/twitter/android/media/selection/c$b;)Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/twitter/android/media/selection/MediaAttachment;-><init>(Lcom/twitter/model/drafts/DraftAttachment;I)V

    .line 604
    invoke-virtual {v0, v1}, Lcom/twitter/android/media/selection/b;->b(Lcom/twitter/android/media/selection/MediaAttachment;)V

    .line 606
    iget-object v0, p0, Lcom/twitter/android/media/selection/c$b$1;->a:Lcom/twitter/android/media/selection/c$b;

    iget-object v0, v0, Lcom/twitter/android/media/selection/c$b;->a:Lcom/twitter/android/media/selection/c;

    iget-object v1, p0, Lcom/twitter/android/media/selection/c$b$1;->a:Lcom/twitter/android/media/selection/c$b;

    invoke-static {v1}, Lcom/twitter/android/media/selection/c$b;->c(Lcom/twitter/android/media/selection/c$b;)Lcom/twitter/android/media/selection/a;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/android/media/selection/c;Lcom/twitter/android/media/selection/a;)V

    goto :goto_0

    .line 610
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/media/selection/c$b$1;->a:Lcom/twitter/android/media/selection/c$b;

    .line 611
    invoke-static {v1}, Lcom/twitter/android/media/selection/c$b;->b(Lcom/twitter/android/media/selection/c$b;)Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/model/drafts/DraftAttachment;->e:Landroid/net/Uri;

    iget-object v2, p0, Lcom/twitter/android/media/selection/c$b$1;->a:Lcom/twitter/android/media/selection/c$b;

    invoke-static {v2}, Lcom/twitter/android/media/selection/c$b;->b(Lcom/twitter/android/media/selection/c$b;)Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/model/drafts/DraftAttachment;->h:Lcom/twitter/model/media/MediaSource;

    .line 610
    invoke-static {v0, v1, v2}, Lcom/twitter/model/media/EditableMedia;->a(Lcom/twitter/media/model/MediaFile;Landroid/net/Uri;Lcom/twitter/model/media/MediaSource;)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 612
    instance-of v1, v0, Lcom/twitter/model/media/EditableAnimatedGif;

    if-nez v1, :cond_2

    .line 613
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "found media downloaded non-gif media "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/media/selection/c$b$1;->a:Lcom/twitter/android/media/selection/c$b;

    .line 614
    invoke-static {v3}, Lcom/twitter/android/media/selection/c$b;->b(Lcom/twitter/android/media/selection/c$b;)Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v3

    iget-object v3, v3, Lcom/twitter/model/drafts/DraftAttachment;->f:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 613
    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 616
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/media/selection/c$b$1;->a:Lcom/twitter/android/media/selection/c$b;

    iget-object v1, v1, Lcom/twitter/android/media/selection/c$b;->a:Lcom/twitter/android/media/selection/c;

    invoke-static {v1}, Lcom/twitter/android/media/selection/c;->b(Lcom/twitter/android/media/selection/c;)Lcom/twitter/android/media/selection/b;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/media/selection/MediaAttachment;

    new-instance v3, Lcom/twitter/model/drafts/DraftAttachment;

    iget-object v4, p0, Lcom/twitter/android/media/selection/c$b$1;->a:Lcom/twitter/android/media/selection/c$b;

    .line 618
    invoke-static {v4}, Lcom/twitter/android/media/selection/c$b;->b(Lcom/twitter/android/media/selection/c$b;)Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v4

    iget-object v4, v4, Lcom/twitter/model/drafts/DraftAttachment;->f:Landroid/net/Uri;

    iget-object v5, p0, Lcom/twitter/android/media/selection/c$b$1;->a:Lcom/twitter/android/media/selection/c$b;

    .line 619
    invoke-static {v5}, Lcom/twitter/android/media/selection/c$b;->b(Lcom/twitter/android/media/selection/c$b;)Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v5

    iget v5, v5, Lcom/twitter/model/drafts/DraftAttachment;->d:I

    invoke-direct {v3, v0, v4, v5}, Lcom/twitter/model/drafts/DraftAttachment;-><init>(Lcom/twitter/model/media/EditableMedia;Landroid/net/Uri;I)V

    invoke-direct {v2, v3}, Lcom/twitter/android/media/selection/MediaAttachment;-><init>(Lcom/twitter/model/drafts/DraftAttachment;)V

    .line 616
    invoke-virtual {v1, v2}, Lcom/twitter/android/media/selection/b;->b(Lcom/twitter/android/media/selection/MediaAttachment;)V

    .line 620
    iget-object v0, p0, Lcom/twitter/android/media/selection/c$b$1;->a:Lcom/twitter/android/media/selection/c$b;

    iget-object v0, v0, Lcom/twitter/android/media/selection/c$b;->a:Lcom/twitter/android/media/selection/c;

    iget-object v1, p0, Lcom/twitter/android/media/selection/c$b$1;->a:Lcom/twitter/android/media/selection/c$b;

    invoke-static {v1}, Lcom/twitter/android/media/selection/c$b;->c(Lcom/twitter/android/media/selection/c$b;)Lcom/twitter/android/media/selection/a;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/android/media/selection/c;Lcom/twitter/android/media/selection/a;)V

    goto/16 :goto_0
.end method
