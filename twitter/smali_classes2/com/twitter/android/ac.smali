.class public Lcom/twitter/android/ac;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/ab;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/ac$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/timeline/k;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/library/widget/InlineDismissView;",
            ">;"
        }
    .end annotation
.end field

.field private final d:I

.field private final e:Landroid/content/Context;

.field private final f:Lcom/twitter/library/client/v;

.field private final g:Lcom/twitter/library/client/p;

.field private final h:Lbwm;

.field private final i:Lcom/twitter/library/widget/InlineDismissView$a;

.field private final j:Lcri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcri",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcpv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcpv",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/timeline/g;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Lajb;


# direct methods
.method public constructor <init>(ILandroid/content/Context;Lcom/twitter/library/client/v;Lcom/twitter/library/client/p;Lbwm;Lcom/twitter/library/widget/InlineDismissView$a;Landroid/os/Bundle;Lajb;)V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ac;->a:Ljava/util/Map;

    .line 69
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ac;->b:Ljava/util/Set;

    .line 71
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ac;->c:Ljava/util/Set;

    .line 83
    new-instance v0, Lcri;

    invoke-direct {v0}, Lcri;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ac;->j:Lcri;

    .line 85
    new-instance v0, Lcom/twitter/android/ac$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/ac$1;-><init>(Lcom/twitter/android/ac;)V

    iput-object v0, p0, Lcom/twitter/android/ac;->k:Lcpv;

    .line 93
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ac;->l:Ljava/util/Map;

    .line 103
    iput p1, p0, Lcom/twitter/android/ac;->d:I

    .line 104
    iput-object p2, p0, Lcom/twitter/android/ac;->e:Landroid/content/Context;

    .line 105
    iput-object p3, p0, Lcom/twitter/android/ac;->f:Lcom/twitter/library/client/v;

    .line 106
    iput-object p4, p0, Lcom/twitter/android/ac;->g:Lcom/twitter/library/client/p;

    .line 107
    iput-object p5, p0, Lcom/twitter/android/ac;->h:Lbwm;

    .line 108
    new-instance v0, Lcom/twitter/android/ac$a;

    invoke-direct {v0, p0, p6}, Lcom/twitter/android/ac$a;-><init>(Lcom/twitter/android/ac;Lcom/twitter/library/widget/InlineDismissView$a;)V

    iput-object v0, p0, Lcom/twitter/android/ac;->i:Lcom/twitter/library/widget/InlineDismissView$a;

    .line 109
    iput-object p8, p0, Lcom/twitter/android/ac;->m:Lajb;

    .line 110
    if-eqz p7, :cond_0

    .line 111
    invoke-virtual {p0, p7}, Lcom/twitter/android/ac;->b(Landroid/os/Bundle;)V

    .line 113
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/ac;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/android/ac;->e:Landroid/content/Context;

    return-object v0
.end method

.method private static a(Lcom/twitter/android/timeline/bk;J)Landroid/net/Uri;
    .locals 5

    .prologue
    .line 384
    sget-object v0, Lcom/twitter/library/provider/f$b;->a:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/android/timeline/bk;->d:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Context;Lcom/twitter/android/timeline/bk;Lcom/twitter/model/timeline/g;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 390
    invoke-virtual {p2, p1}, Lcom/twitter/android/timeline/bk;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 391
    invoke-virtual {p2}, Lcom/twitter/android/timeline/bk;->i()Ljava/lang/String;

    move-result-object v0

    .line 392
    if-nez v0, :cond_0

    instance-of v2, p2, Lcom/twitter/android/timeline/k;

    if-eqz v2, :cond_0

    .line 393
    const-string/jumbo v0, "tweet"

    .line 395
    :cond_0
    iget-object v2, p0, Lcom/twitter/android/ac;->h:Lbwm;

    invoke-virtual {v2, v1, v0, p3, p4}, Lbwm;->a(Ljava/util/List;Ljava/lang/String;Lcom/twitter/model/timeline/g;Ljava/lang/String;)V

    .line 396
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/ac;Landroid/content/Context;Lcom/twitter/android/timeline/bk;Lcom/twitter/model/timeline/g;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/ac;->a(Landroid/content/Context;Lcom/twitter/android/timeline/bk;Lcom/twitter/model/timeline/g;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/timeline/bk;ZI)Lcom/twitter/async/service/AsyncOperation;
    .locals 7
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/client/Session;",
            "Lcom/twitter/android/timeline/bk;",
            "ZI)",
            "Lcom/twitter/async/service/AsyncOperation",
            "<**>;"
        }
    .end annotation

    .prologue
    .line 335
    iget v3, p0, Lcom/twitter/android/ac;->d:I

    .line 336
    invoke-virtual {p3}, Lcom/twitter/android/timeline/bk;->j()I

    move-result v6

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move v4, p4

    move v5, p5

    .line 335
    invoke-static/range {v0 .. v6}, Lbwp;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/timeline/bk;IZII)Lbwp;

    move-result-object v0

    return-object v0
.end method

.method a(JLcom/twitter/model/timeline/g;)Lcom/twitter/model/timeline/g;
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 251
    iget-object v0, p0, Lcom/twitter/android/ac;->l:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/g;

    return-object v0
.end method

.method public a(JLcom/twitter/model/timeline/k;)Lcom/twitter/model/timeline/k;
    .locals 3

    .prologue
    .line 118
    iget-object v0, p0, Lcom/twitter/android/ac;->a:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/k;

    return-object v0
.end method

.method a(Lcom/twitter/android/timeline/bk;)Lrx/g;
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/timeline/bk;",
            ")",
            "Lrx/g",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 342
    iget-object v0, p0, Lcom/twitter/android/ac;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ac;->f:Lcom/twitter/library/client/v;

    .line 343
    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {p1, v2, v3}, Lcom/twitter/android/ac;->a(Lcom/twitter/android/timeline/bk;J)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/f$a;->a:[Ljava/lang/String;

    .line 342
    invoke-static {v0, v1, v2}, Lauk;->a(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 209
    iget-object v0, p0, Lcom/twitter/android/ac;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/twitter/android/ac;->c:Ljava/util/Set;

    iget-object v1, p0, Lcom/twitter/android/ac;->k:Lcpv;

    .line 211
    invoke-static {v0, v1}, Lcpt;->a(Ljava/lang/Iterable;Lcpv;)Ljava/lang/Iterable;

    move-result-object v0

    .line 210
    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 212
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/InlineDismissView;

    .line 213
    const v1, 0x7f13007e

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/InlineDismissView;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/timeline/bk;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ac;->d(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/android/timeline/bk;)V

    goto :goto_0

    .line 216
    :cond_0
    return-void
.end method

.method a(JLrx/j;)V
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 349
    iget-object v0, p0, Lcom/twitter/android/ac;->j:Lcri;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lcri;->a(Ljava/lang/Object;Lrx/j;)Lrx/j;

    .line 350
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 220
    const-string/jumbo v0, "fetched_dismiss_infos"

    iget-object v1, p0, Lcom/twitter/android/ac;->a:Ljava/util/Map;

    sget-object v2, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    sget-object v3, Lcom/twitter/model/timeline/k;->a:Lcom/twitter/util/serialization/l;

    .line 221
    invoke-static {v2, v3}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 220
    invoke-static {p1, v0, v1, v2}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/os/Bundle;

    .line 222
    const-string/jumbo v0, "saved_feedback_actions"

    iget-object v1, p0, Lcom/twitter/android/ac;->l:Ljava/util/Map;

    sget-object v2, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    sget-object v3, Lcom/twitter/model/timeline/g;->a:Lcom/twitter/util/serialization/l;

    .line 223
    invoke-static {v2, v3}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 222
    invoke-static {p1, v0, v1, v2}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/os/Bundle;

    .line 224
    const-string/jumbo v0, "dismissed_ids"

    iget-object v1, p0, Lcom/twitter/android/ac;->b:Ljava/util/Set;

    sget-object v2, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    .line 225
    invoke-static {v2}, Lcom/twitter/util/collection/d;->b(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 224
    invoke-static {p1, v0, v1, v2}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/os/Bundle;

    .line 226
    return-void
.end method

.method a(Lcom/twitter/android/timeline/bk;Lcom/twitter/model/timeline/k;)V
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 378
    iget-object v0, p0, Lcom/twitter/android/ac;->g:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/android/ac;->m:Lajb;

    iget-object v2, p2, Lcom/twitter/model/timeline/k;->b:Lcom/twitter/model/timeline/g;

    const/4 v3, 0x0

    .line 379
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 378
    invoke-virtual {v1, p1, v2, v3}, Lajb;->a(Lcom/twitter/android/timeline/bk;Lcom/twitter/model/timeline/g;Ljava/lang/Boolean;)Lcom/twitter/library/service/s;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 380
    return-void
.end method

.method public a(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/android/timeline/bk;)V
    .locals 4

    .prologue
    .line 152
    const v0, 0x7f13007e

    invoke-virtual {p1, v0, p2}, Lcom/twitter/library/widget/InlineDismissView;->setTag(ILjava/lang/Object;)V

    .line 153
    iget-object v0, p0, Lcom/twitter/android/ac;->i:Lcom/twitter/library/widget/InlineDismissView$a;

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/InlineDismissView;->setDismissListener(Lcom/twitter/library/widget/InlineDismissView$a;)V

    .line 154
    invoke-virtual {p0, p1}, Lcom/twitter/android/ac;->a(Lcom/twitter/library/widget/InlineDismissView;)Z

    .line 156
    iget-wide v2, p2, Lcom/twitter/android/timeline/bk;->d:J

    .line 163
    iget-object v0, p0, Lcom/twitter/android/ac;->e:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/twitter/android/p;->a(Landroid/content/Context;Lcom/twitter/android/timeline/bk;)Lcom/twitter/model/timeline/k;

    move-result-object v0

    .line 164
    if-nez v0, :cond_1

    .line 166
    invoke-virtual {p0, v2, v3}, Lcom/twitter/android/ac;->c(J)Lcom/twitter/model/timeline/k;

    move-result-object v0

    .line 171
    :goto_0
    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/InlineDismissView;->setDismissInfo(Lcom/twitter/model/timeline/k;)V

    .line 173
    if-nez v0, :cond_2

    .line 175
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/ac;->c(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/android/timeline/bk;)V

    .line 186
    :cond_0
    :goto_1
    return-void

    .line 169
    :cond_1
    invoke-virtual {p0, v2, v3, v0}, Lcom/twitter/android/ac;->a(JLcom/twitter/model/timeline/k;)Lcom/twitter/model/timeline/k;

    goto :goto_0

    .line 177
    :cond_2
    invoke-virtual {p0, v2, v3}, Lcom/twitter/android/ac;->g(J)Lcom/twitter/model/timeline/g;

    move-result-object v1

    .line 178
    if-eqz v1, :cond_3

    .line 179
    invoke-virtual {p1, v1}, Lcom/twitter/library/widget/InlineDismissView;->setCurrentFeedbackAction(Lcom/twitter/model/timeline/g;)V

    goto :goto_1

    .line 180
    :cond_3
    invoke-virtual {p0, v2, v3}, Lcom/twitter/android/ac;->e(J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 181
    invoke-virtual {p0, v2, v3}, Lcom/twitter/android/ac;->a(J)Z

    .line 182
    invoke-virtual {p0, p2, v0}, Lcom/twitter/android/ac;->a(Lcom/twitter/android/timeline/bk;Lcom/twitter/model/timeline/k;)V

    .line 183
    iget-object v1, p0, Lcom/twitter/android/ac;->e:Landroid/content/Context;

    iget-object v0, v0, Lcom/twitter/model/timeline/k;->b:Lcom/twitter/model/timeline/g;

    const-string/jumbo v2, "click"

    invoke-direct {p0, v1, p2, v0, v2}, Lcom/twitter/android/ac;->a(Landroid/content/Context;Lcom/twitter/android/timeline/bk;Lcom/twitter/model/timeline/g;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(J)Z
    .locals 3

    .prologue
    .line 135
    iget-object v0, p0, Lcom/twitter/android/ac;->b:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method a(Lcom/twitter/library/widget/InlineDismissView;)Z
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/android/ac;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b(J)Lcom/twitter/model/timeline/k;
    .locals 3

    .prologue
    .line 124
    iget-object v0, p0, Lcom/twitter/android/ac;->a:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/k;

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/twitter/android/ac;->j:Lcri;

    invoke-virtual {v0}, Lcri;->a()V

    .line 200
    iget-object v0, p0, Lcom/twitter/android/ac;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 201
    iget-object v0, p0, Lcom/twitter/android/ac;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 202
    return-void
.end method

.method b(Landroid/os/Bundle;)V
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 359
    const-string/jumbo v0, "fetched_dismiss_infos"

    sget-object v1, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    sget-object v2, Lcom/twitter/model/timeline/k;->a:Lcom/twitter/util/serialization/l;

    .line 361
    invoke-static {v1, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    .line 359
    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 362
    const-string/jumbo v1, "saved_feedback_actions"

    sget-object v2, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    sget-object v3, Lcom/twitter/model/timeline/g;->a:Lcom/twitter/util/serialization/l;

    .line 364
    invoke-static {v2, v3}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 362
    invoke-static {p1, v1, v2}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 365
    const-string/jumbo v2, "dismissed_ids"

    sget-object v3, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    .line 367
    invoke-static {v3}, Lcom/twitter/util/collection/d;->b(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v3

    .line 365
    invoke-static {p1, v2, v3}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    .line 368
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 369
    iget-object v3, p0, Lcom/twitter/android/ac;->a:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 370
    iget-object v0, p0, Lcom/twitter/android/ac;->l:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 371
    iget-object v0, p0, Lcom/twitter/android/ac;->b:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 373
    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/android/timeline/bk;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 190
    invoke-virtual {p0, p1}, Lcom/twitter/android/ac;->b(Lcom/twitter/library/widget/InlineDismissView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    iget-object v6, p0, Lcom/twitter/android/ac;->g:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/android/ac;->e:Landroid/content/Context;

    iget-object v0, p0, Lcom/twitter/android/ac;->f:Lcom/twitter/library/client/v;

    .line 192
    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    move-object v0, p0

    move-object v3, p2

    move v5, v4

    .line 191
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/ac;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/timeline/bk;ZI)Lcom/twitter/async/service/AsyncOperation;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 195
    :cond_0
    return-void
.end method

.method b(Lcom/twitter/library/widget/InlineDismissView;)Z
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 240
    iget-object v0, p0, Lcom/twitter/android/ac;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c(J)Lcom/twitter/model/timeline/k;
    .locals 3

    .prologue
    .line 130
    iget-object v0, p0, Lcom/twitter/android/ac;->a:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/k;

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/twitter/android/ac;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 231
    return-void
.end method

.method c(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/android/timeline/bk;)V
    .locals 7
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 269
    iget-wide v4, p2, Lcom/twitter/android/timeline/bk;->d:J

    .line 270
    invoke-virtual {p0, p2}, Lcom/twitter/android/ac;->a(Lcom/twitter/android/timeline/bk;)Lrx/g;

    move-result-object v0

    .line 271
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/f;)Lrx/g;

    move-result-object v6

    new-instance v0, Lcom/twitter/android/ac$2;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/ac$2;-><init>(Lcom/twitter/android/ac;Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/android/timeline/bk;J)V

    .line 272
    invoke-virtual {v6, v0}, Lrx/g;->a(Lrx/i;)Lrx/j;

    move-result-object v0

    .line 294
    invoke-virtual {p0, v4, v5, v0}, Lcom/twitter/android/ac;->a(JLrx/j;)V

    .line 295
    return-void
.end method

.method d(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/android/timeline/bk;)V
    .locals 6
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 299
    invoke-virtual {p0, p1}, Lcom/twitter/android/ac;->b(Lcom/twitter/library/widget/InlineDismissView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 303
    iget-wide v0, p2, Lcom/twitter/android/timeline/bk;->d:J

    .line 304
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ac;->h(J)V

    .line 305
    iget-object v2, p0, Lcom/twitter/android/ac;->g:Lcom/twitter/library/client/p;

    new-instance v3, Lbfo;

    iget-object v4, p0, Lcom/twitter/android/ac;->e:Landroid/content/Context;

    iget-object v5, p0, Lcom/twitter/android/ac;->f:Lcom/twitter/library/client/v;

    .line 306
    invoke-virtual {v5}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-direct {v3, v4, v5, p2}, Lbfo;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/timeline/bk;)V

    new-instance v4, Lcom/twitter/android/ac$3;

    invoke-direct {v4, p0, v0, v1, p2}, Lcom/twitter/android/ac$3;-><init>(Lcom/twitter/android/ac;JLcom/twitter/android/timeline/bk;)V

    .line 305
    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 327
    :cond_0
    return-void
.end method

.method public d(J)Z
    .locals 3

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/android/ac;->b:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public e(J)Z
    .locals 3

    .prologue
    .line 145
    iget-object v0, p0, Lcom/twitter/android/ac;->b:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method f(J)Lcom/twitter/model/timeline/g;
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 257
    iget-object v0, p0, Lcom/twitter/android/ac;->l:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/g;

    return-object v0
.end method

.method g(J)Lcom/twitter/model/timeline/g;
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 263
    iget-object v0, p0, Lcom/twitter/android/ac;->l:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/g;

    return-object v0
.end method

.method h(J)V
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 354
    iget-object v0, p0, Lcom/twitter/android/ac;->j:Lcri;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcri;->a(Ljava/lang/Object;)Lrx/j;

    .line 355
    return-void
.end method
