.class public abstract Lcom/twitter/android/e;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected final a:Lpv;

.field protected b:Lcom/twitter/model/av/h;

.field protected c:Lrx/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/c",
            "<",
            "Lcom/twitter/android/e$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lpv;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/twitter/android/e;->a:Lpv;

    .line 31
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/av/h;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/android/e;->b:Lcom/twitter/model/av/h;

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/Object;)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/client/Session;",
            "TT;)",
            "Lrx/c",
            "<",
            "Lcom/twitter/android/e$a;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 51
    iget-object v0, p0, Lcom/twitter/android/e;->b:Lcom/twitter/model/av/h;

    if-eqz v0, :cond_0

    .line 52
    new-instance v0, Lcom/twitter/android/e$a;

    iget-object v1, p0, Lcom/twitter/android/e;->b:Lcom/twitter/model/av/h;

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/e$a;-><init>(Lcom/twitter/model/av/h;Z)V

    invoke-static {v0}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    .line 71
    :goto_0
    return-object v0

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/e;->c:Lrx/c;

    if-eqz v0, :cond_1

    .line 56
    iget-object v0, p0, Lcom/twitter/android/e;->c:Lrx/c;

    goto :goto_0

    .line 59
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/e;->b(Lcom/twitter/library/client/Session;Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    .line 60
    if-eqz v0, :cond_2

    .line 61
    new-instance v1, Lcom/twitter/android/e$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/e$1;-><init>(Lcom/twitter/android/e;)V

    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/e;->c:Lrx/c;

    .line 71
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/e;->c:Lrx/c;

    goto :goto_0

    .line 68
    :cond_2
    new-instance v0, Lcom/twitter/android/e$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/e$a;-><init>(Lcom/twitter/model/av/h;Z)V

    invoke-static {v0}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/e;->c:Lrx/c;

    goto :goto_1
.end method

.method public a(Lcom/twitter/model/av/h;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/twitter/android/e;->b:Lcom/twitter/model/av/h;

    .line 92
    return-void
.end method

.method protected abstract b(Lcom/twitter/library/client/Session;Ljava/lang/Object;)Lrx/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/client/Session;",
            "TT;)",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/av/h;",
            ">;"
        }
    .end annotation
.end method
