.class public Lcom/twitter/android/PasswordEntryFragment;
.super Lcom/twitter/app/common/abs/AbsFragment;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/twitter/app/onboarding/flowstep/common/a;
.implements Lcom/twitter/app/onboarding/flowstep/common/h;
.implements Lcom/twitter/ui/widget/TwitterEditText$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/PasswordEntryFragment$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/abs/AbsFragment;",
        "Landroid/text/TextWatcher;",
        "Landroid/widget/TextView$OnEditorActionListener;",
        "Lcom/twitter/app/onboarding/flowstep/common/a;",
        "Lcom/twitter/app/onboarding/flowstep/common/h",
        "<",
        "Lcom/twitter/model/onboarding/a;",
        ">;",
        "Lcom/twitter/ui/widget/TwitterEditText$a;"
    }
.end annotation


# static fields
.field private static final b:[I

.field private static final c:[I

.field private static final d:[I

.field private static final e:[I

.field private static final f:[[I


# instance fields
.field a:Lcom/twitter/ui/widget/TwitterEditText;

.field private g:Lcom/twitter/android/ValidationState$State;

.field private h:I

.field private i:I

.field private j:Z

.field private k:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

.field private l:Lcom/twitter/android/PasswordEntryFragment$a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 49
    const/4 v0, 0x0

    sput-object v0, Lcom/twitter/android/PasswordEntryFragment;->b:[I

    .line 50
    new-array v0, v3, [I

    const v1, 0x7f010471

    aput v1, v0, v2

    sput-object v0, Lcom/twitter/android/PasswordEntryFragment;->c:[I

    .line 51
    new-array v0, v3, [I

    const v1, 0x7f010454

    aput v1, v0, v2

    sput-object v0, Lcom/twitter/android/PasswordEntryFragment;->d:[I

    .line 52
    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/twitter/android/PasswordEntryFragment;->e:[I

    .line 54
    const/4 v0, 0x4

    new-array v0, v0, [[I

    sget-object v1, Lcom/twitter/android/PasswordEntryFragment;->b:[I

    aput-object v1, v0, v2

    sget-object v1, Lcom/twitter/android/PasswordEntryFragment;->c:[I

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/android/PasswordEntryFragment;->d:[I

    aput-object v1, v0, v4

    const/4 v1, 0x3

    sget-object v2, Lcom/twitter/android/PasswordEntryFragment;->e:[I

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/PasswordEntryFragment;->f:[[I

    return-void

    .line 52
    :array_0
    .array-data 4
        0x7f010454
        0x7f010471
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/twitter/app/common/abs/AbsFragment;-><init>()V

    .line 74
    const/16 v0, 0x81

    iput v0, p0, Lcom/twitter/android/PasswordEntryFragment;->h:I

    .line 80
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/PasswordEntryFragment;->i:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/PasswordEntryFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->T:Landroid/content/Context;

    return-object v0
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->T:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/FlowData;->c(Landroid/content/Context;)V

    .line 236
    invoke-direct {p0}, Lcom/twitter/android/PasswordEntryFragment;->g()Larv;

    move-result-object v0

    .line 237
    if-eqz v0, :cond_1

    .line 238
    if-eqz p2, :cond_2

    .line 239
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Larv;->h(Ljava/lang/String;)V

    .line 243
    :cond_0
    :goto_0
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Larv;->g(Z)V

    .line 245
    :cond_1
    return-void

    .line 240
    :cond_2
    if-eqz p1, :cond_0

    .line 241
    invoke-interface {v0, p1}, Larv;->h(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 299
    if-eqz p1, :cond_0

    .line 300
    iget v0, p0, Lcom/twitter/android/PasswordEntryFragment;->i:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/android/PasswordEntryFragment;->i:I

    .line 304
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    sget-object v1, Lcom/twitter/android/PasswordEntryFragment;->f:[[I

    iget v2, p0, Lcom/twitter/android/PasswordEntryFragment;->i:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setExtraState([I)V

    .line 305
    return-void

    .line 302
    :cond_0
    iget v0, p0, Lcom/twitter/android/PasswordEntryFragment;->i:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/twitter/android/PasswordEntryFragment;->i:I

    goto :goto_0
.end method

.method private b(I)Lcom/twitter/android/ValidationState$State;
    .locals 1

    .prologue
    .line 286
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/PasswordEntryFragment;->a(Z)V

    .line 287
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->e()V

    .line 288
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->length()I

    move-result v0

    .line 289
    if-lt v0, p1, :cond_0

    .line 290
    sget-object v0, Lcom/twitter/android/ValidationState$State;->b:Lcom/twitter/android/ValidationState$State;

    .line 294
    :goto_0
    return-object v0

    .line 291
    :cond_0
    if-ge v0, p1, :cond_1

    .line 292
    sget-object v0, Lcom/twitter/android/ValidationState$State;->a:Lcom/twitter/android/ValidationState$State;

    goto :goto_0

    .line 294
    :cond_1
    sget-object v0, Lcom/twitter/android/ValidationState$State;->d:Lcom/twitter/android/ValidationState$State;

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/PasswordEntryFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/twitter/android/PasswordEntryFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/PasswordEntryFragment;)Lcom/twitter/android/ValidationState$a;
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/twitter/android/PasswordEntryFragment;->h()Lcom/twitter/android/ValidationState$a;

    move-result-object v0

    return-object v0
.end method

.method private c(I)V
    .locals 3

    .prologue
    .line 343
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->k:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    if-nez v0, :cond_0

    .line 344
    invoke-static {p1}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->a(I)Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->k:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    .line 345
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->k:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->setRetainInstance(Z)V

    .line 346
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->k:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    invoke-virtual {p0}, Lcom/twitter/android/PasswordEntryFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 348
    :cond_0
    return-void
.end method

.method private g()Larv;
    .locals 1

    .prologue
    .line 143
    const-class v0, Larv;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PasswordEntryFragment;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larv;

    return-object v0
.end method

.method private h()Lcom/twitter/android/ValidationState$a;
    .locals 1

    .prologue
    .line 148
    const-class v0, Lcom/twitter/android/ValidationState$a;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PasswordEntryFragment;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ValidationState$a;

    return-object v0
.end method

.method private i()Lcom/twitter/android/au;
    .locals 1

    .prologue
    .line 152
    const-class v0, Lcom/twitter/android/au;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PasswordEntryFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/au;

    return-object v0
.end method

.method private j()V
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->k:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->k:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->dismissAllowingStateLoss()V

    .line 353
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->k:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    .line 355
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 96
    const v0, 0x7f040286

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 98
    const v0, 0x7f1303a0

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 99
    const v1, 0x7f1307f4

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 100
    const v3, 0x7f0a0631

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 101
    const v0, 0x7f0a0630

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 102
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 104
    const v0, 0x7f13061e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterEditText;

    iput-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    .line 105
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    iget v1, p0, Lcom/twitter/android/PasswordEntryFragment;->h:I

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setInputType(I)V

    .line 106
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 107
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 108
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->setOnStatusIconClickListener(Lcom/twitter/ui/widget/TwitterEditText$a;)V

    .line 109
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->requestFocus()Z

    .line 111
    invoke-virtual {p0}, Lcom/twitter/android/PasswordEntryFragment;->I()Lcom/twitter/app/common/base/b;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/bw;->a(Lcom/twitter/app/common/base/b;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/PasswordEntryFragment;->j:Z

    .line 113
    return-object v2
.end method

.method protected a(Lbbb;)V
    .locals 2

    .prologue
    .line 335
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/android/PasswordEntryFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 336
    return-void
.end method

.method protected a(Lbif;)V
    .locals 2

    .prologue
    .line 339
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/android/PasswordEntryFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 340
    return-void
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 9
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 160
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/abs/AbsFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 161
    packed-switch p2, :pswitch_data_0

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 163
    :pswitch_0
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v0

    if-eqz v0, :cond_1

    check-cast p1, Lbif;

    .line 164
    invoke-virtual {p1}, Lbif;->e()Lcom/twitter/library/api/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/api/m;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v2

    .line 166
    :goto_1
    invoke-direct {p0, v0}, Lcom/twitter/android/PasswordEntryFragment;->a(Z)V

    .line 168
    if-eqz v0, :cond_3

    .line 169
    sget-object v0, Lcom/twitter/android/ValidationState$State;->c:Lcom/twitter/android/ValidationState$State;

    iput-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->g:Lcom/twitter/android/ValidationState$State;

    .line 170
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->e()V

    .line 175
    :goto_2
    invoke-direct {p0}, Lcom/twitter/android/PasswordEntryFragment;->h()Lcom/twitter/android/ValidationState$a;

    move-result-object v0

    .line 176
    if-eqz v0, :cond_0

    .line 177
    new-instance v1, Lcom/twitter/android/ValidationState;

    iget-object v2, p0, Lcom/twitter/android/PasswordEntryFragment;->g:Lcom/twitter/android/ValidationState$State;

    sget-object v3, Lcom/twitter/android/ValidationState$Level;->b:Lcom/twitter/android/ValidationState$Level;

    invoke-direct {v1, v2, v3}, Lcom/twitter/android/ValidationState;-><init>(Lcom/twitter/android/ValidationState$State;Lcom/twitter/android/ValidationState$Level;)V

    invoke-interface {v0, v1}, Lcom/twitter/android/ValidationState$a;->a(Lcom/twitter/android/ValidationState;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 164
    goto :goto_1

    .line 172
    :cond_3
    sget-object v0, Lcom/twitter/android/ValidationState$State;->d:Lcom/twitter/android/ValidationState$State;

    iput-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->g:Lcom/twitter/android/ValidationState$State;

    .line 173
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    const v1, 0x7f0a08c2

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setError(I)V

    goto :goto_2

    .line 183
    :pswitch_1
    invoke-direct {p0}, Lcom/twitter/android/PasswordEntryFragment;->j()V

    .line 184
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 185
    check-cast p1, Lbbb;

    iget-object v0, p1, Lbbb;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/PasswordEntryFragment;->a(Ljava/lang/String;Z)V

    .line 186
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/PasswordEntryFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "phone100_change_password"

    aput-object v4, v3, v1

    const-string/jumbo v1, "change_password"

    aput-object v1, v3, v2

    iget-boolean v1, p0, Lcom/twitter/android/PasswordEntryFragment;->j:Z

    .line 188
    invoke-static {v1}, Lcom/twitter/android/bw;->a(Z)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v7

    aput-object v6, v3, v8

    const/4 v1, 0x4

    const-string/jumbo v2, "success"

    aput-object v2, v3, v1

    .line 187
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 186
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 190
    :cond_4
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 191
    iget-object v0, v0, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    invoke-static {v0}, Lcom/twitter/library/network/ab;->a(Landroid/os/Bundle;)[I

    move-result-object v0

    .line 192
    if-eqz v0, :cond_5

    array-length v3, v0

    if-nez v3, :cond_6

    :cond_5
    move v0, v1

    .line 196
    :goto_3
    sparse-switch v0, :sswitch_data_0

    .line 215
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->T:Landroid/content/Context;

    const v3, 0x7f0a062d

    invoke-static {v0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 216
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 217
    invoke-direct {p0}, Lcom/twitter/android/PasswordEntryFragment;->i()Lcom/twitter/android/au;

    move-result-object v0

    const v2, 0x7f0a08d6

    invoke-interface {v0, v2}, Lcom/twitter/android/au;->a(I)V

    .line 218
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    const-string/jumbo v2, ""

    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/TwitterEditText;->setText(Ljava/lang/CharSequence;)V

    .line 222
    :goto_4
    if-eqz v1, :cond_0

    .line 223
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setError(I)V

    goto/16 :goto_0

    .line 192
    :cond_6
    aget v0, v0, v1

    goto :goto_3

    .line 198
    :sswitch_0
    invoke-direct {p0, v6, v2}, Lcom/twitter/android/PasswordEntryFragment;->a(Ljava/lang/String;Z)V

    .line 199
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/PasswordEntryFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "phone100_change_password"

    aput-object v4, v3, v1

    const-string/jumbo v4, "change_password"

    aput-object v4, v3, v2

    iget-boolean v2, p0, Lcom/twitter/android/PasswordEntryFragment;->j:Z

    .line 201
    invoke-static {v2}, Lcom/twitter/android/bw;->a(Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v7

    aput-object v6, v3, v8

    const/4 v2, 0x4

    const-string/jumbo v4, "wrong_current"

    aput-object v4, v3, v2

    .line 200
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 199
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_4

    .line 206
    :sswitch_1
    const v1, 0x7f0a08c1

    .line 207
    goto :goto_4

    .line 210
    :sswitch_2
    const v1, 0x7f0a062c

    .line 211
    goto :goto_4

    .line 161
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 196
    :sswitch_data_0
    .sparse-switch
        0x3e -> :sswitch_1
        0x72 -> :sswitch_0
        0xee -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 308
    invoke-virtual {p0}, Lcom/twitter/android/PasswordEntryFragment;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309
    new-instance v0, Lbbb;

    invoke-virtual {p0}, Lcom/twitter/android/PasswordEntryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 310
    invoke-virtual {p0}, Lcom/twitter/android/PasswordEntryFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v3}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lbbb;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbbb;->g(I)Lcom/twitter/library/service/s;

    .line 312
    const v1, 0x7f0a0a35

    invoke-direct {p0, v1}, Lcom/twitter/android/PasswordEntryFragment;->c(I)V

    .line 313
    invoke-virtual {p0, v0}, Lcom/twitter/android/PasswordEntryFragment;->a(Lbbb;)V

    .line 315
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/ui/widget/TwitterEditText;)Z
    .locals 4

    .prologue
    const/16 v3, 0x91

    .line 359
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    if-ne v0, p1, :cond_1

    .line 360
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 361
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getSelectionStart()I

    move-result v1

    .line 362
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getSelectionEnd()I

    move-result v2

    .line 364
    iget v0, p0, Lcom/twitter/android/PasswordEntryFragment;->h:I

    if-eq v0, v3, :cond_0

    .line 365
    iput v3, p0, Lcom/twitter/android/PasswordEntryFragment;->h:I

    .line 366
    iget v0, p0, Lcom/twitter/android/PasswordEntryFragment;->i:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/twitter/android/PasswordEntryFragment;->i:I

    .line 371
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    iget v3, p0, Lcom/twitter/android/PasswordEntryFragment;->h:I

    invoke-virtual {v0, v3}, Lcom/twitter/ui/widget/TwitterEditText;->setInputType(I)V

    .line 372
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Lcom/twitter/ui/widget/TwitterEditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 373
    invoke-direct {p0}, Lcom/twitter/android/PasswordEntryFragment;->h()Lcom/twitter/android/ValidationState$a;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ValidationState$a;

    .line 374
    invoke-interface {v0}, Lcom/twitter/android/ValidationState$a;->e()Lcom/twitter/android/ValidationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/ValidationState;->a()Z

    move-result v0

    .line 373
    invoke-direct {p0, v0}, Lcom/twitter/android/PasswordEntryFragment;->a(Z)V

    .line 376
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/ui/widget/TwitterEditText;->setSelection(II)V

    .line 377
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 378
    const/4 v0, 0x1

    .line 380
    :goto_1
    return v0

    .line 368
    :cond_0
    const/16 v0, 0x81

    iput v0, p0, Lcom/twitter/android/PasswordEntryFragment;->h:I

    .line 369
    iget v0, p0, Lcom/twitter/android/PasswordEntryFragment;->i:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/twitter/android/PasswordEntryFragment;->i:I

    goto :goto_0

    .line 380
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 268
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 269
    invoke-direct {p0, v2}, Lcom/twitter/android/PasswordEntryFragment;->b(I)Lcom/twitter/android/ValidationState$State;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->g:Lcom/twitter/android/ValidationState$State;

    .line 271
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->g:Lcom/twitter/android/ValidationState$State;

    sget-object v1, Lcom/twitter/android/ValidationState$State;->b:Lcom/twitter/android/ValidationState$State;

    if-ne v0, v1, :cond_1

    .line 272
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->l:Lcom/twitter/android/PasswordEntryFragment$a;

    invoke-virtual {v0, v2}, Lcom/twitter/android/PasswordEntryFragment$a;->a(I)V

    .line 278
    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/PasswordEntryFragment;->h()Lcom/twitter/android/ValidationState$a;

    move-result-object v0

    .line 279
    if-eqz v0, :cond_0

    .line 280
    new-instance v1, Lcom/twitter/android/ValidationState;

    iget-object v2, p0, Lcom/twitter/android/PasswordEntryFragment;->g:Lcom/twitter/android/ValidationState$State;

    sget-object v3, Lcom/twitter/android/ValidationState$Level;->a:Lcom/twitter/android/ValidationState$Level;

    invoke-direct {v1, v2, v3}, Lcom/twitter/android/ValidationState;-><init>(Lcom/twitter/android/ValidationState$State;Lcom/twitter/android/ValidationState$Level;)V

    invoke-interface {v0, v1}, Lcom/twitter/android/ValidationState$a;->a(Lcom/twitter/android/ValidationState;)V

    .line 283
    :cond_0
    return-void

    .line 275
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->l:Lcom/twitter/android/PasswordEntryFragment$a;

    invoke-virtual {v0, v2}, Lcom/twitter/android/PasswordEntryFragment$a;->removeMessages(I)V

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 118
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->b()V

    .line 119
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 120
    invoke-direct {p0}, Lcom/twitter/android/PasswordEntryFragment;->h()Lcom/twitter/android/ValidationState$a;

    move-result-object v0

    .line 121
    if-eqz v0, :cond_2

    .line 122
    invoke-interface {v0}, Lcom/twitter/android/ValidationState$a;->e()Lcom/twitter/android/ValidationState;

    move-result-object v0

    .line 123
    invoke-virtual {v0}, Lcom/twitter/android/ValidationState;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 124
    invoke-direct {p0, v2}, Lcom/twitter/android/PasswordEntryFragment;->a(Z)V

    .line 132
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/android/ValidationState;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v2}, Lcom/twitter/android/PasswordEntryFragment;->b(I)Lcom/twitter/android/ValidationState$State;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/ValidationState$State;->b:Lcom/twitter/android/ValidationState$State;

    if-ne v0, v1, :cond_0

    .line 127
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->l:Lcom/twitter/android/PasswordEntryFragment$a;

    invoke-virtual {v0, v2}, Lcom/twitter/android/PasswordEntryFragment$a;->a(I)V

    goto :goto_0

    .line 129
    :cond_2
    invoke-direct {p0, v2}, Lcom/twitter/android/PasswordEntryFragment;->b(I)Lcom/twitter/android/ValidationState$State;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/ValidationState$State;->b:Lcom/twitter/android/ValidationState$State;

    if-ne v0, v1, :cond_0

    .line 130
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->l:Lcom/twitter/android/PasswordEntryFragment$a;

    invoke-virtual {v0, v2}, Lcom/twitter/android/PasswordEntryFragment$a;->a(I)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 260
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 384
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/PasswordEntryFragment;->a(Ljava/lang/String;Z)V

    .line 385
    return-void
.end method

.method public e()Lcom/twitter/model/onboarding/a;
    .locals 2

    .prologue
    .line 331
    new-instance v0, Lcom/twitter/model/onboarding/g;

    iget-object v1, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/model/onboarding/g;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 319
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 320
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x6

    if-lt v0, v1, :cond_0

    .line 321
    const/4 v0, 0x1

    .line 324
    :goto_0
    return v0

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    const v1, 0x7f0a08c1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setError(I)V

    .line 324
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 90
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 91
    new-instance v0, Lcom/twitter/android/PasswordEntryFragment$a;

    invoke-direct {v0, p0}, Lcom/twitter/android/PasswordEntryFragment$a;-><init>(Lcom/twitter/android/PasswordEntryFragment;)V

    iput-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->l:Lcom/twitter/android/PasswordEntryFragment$a;

    .line 92
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 249
    invoke-virtual {p1}, Landroid/widget/TextView;->getId()I

    move-result v0

    .line 250
    const v1, 0x7f13061e

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 251
    invoke-direct {p0}, Lcom/twitter/android/PasswordEntryFragment;->i()Lcom/twitter/android/au;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/au;->n_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    invoke-direct {p0}, Lcom/twitter/android/PasswordEntryFragment;->i()Lcom/twitter/android/au;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/au;->b()V

    .line 253
    const/4 v0, 0x1

    .line 255
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 264
    return-void
.end method

.method public q_()V
    .locals 3

    .prologue
    .line 136
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->T:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    .line 137
    iget-object v0, p0, Lcom/twitter/android/PasswordEntryFragment;->a:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 138
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->q_()V

    .line 139
    return-void
.end method
