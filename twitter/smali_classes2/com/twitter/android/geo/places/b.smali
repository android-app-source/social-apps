.class public Lcom/twitter/android/geo/places/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/client/s;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/geo/places/b$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/v;

.field private final c:Lcom/twitter/library/client/p;

.field private final d:J

.field private e:Lcom/twitter/android/geo/places/b$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/twitter/android/geo/places/b;->a:Landroid/content/Context;

    .line 37
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/geo/places/b;->b:Lcom/twitter/library/client/v;

    .line 38
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/geo/places/b;->c:Lcom/twitter/library/client/p;

    .line 39
    iget-object v0, p0, Lcom/twitter/android/geo/places/b;->b:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/geo/places/b;->d:J

    .line 40
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/geo/places/b;->e:Lcom/twitter/android/geo/places/b$a;

    .line 80
    return-void
.end method

.method public a(ILandroid/os/Bundle;Lcom/twitter/library/service/s;)V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public a(ILcom/twitter/library/service/s;)V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/twitter/android/geo/places/b$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/geo/places/b;->e:Lcom/twitter/android/geo/places/b$a;

    if-nez v0, :cond_0

    .line 46
    iput-object p2, p0, Lcom/twitter/android/geo/places/b;->e:Lcom/twitter/android/geo/places/b$a;

    .line 47
    iget-object v0, p0, Lcom/twitter/android/geo/places/b;->c:Lcom/twitter/library/client/p;

    new-instance v1, Lcom/twitter/library/api/geo/c$a;

    invoke-direct {v1}, Lcom/twitter/library/api/geo/c$a;-><init>()V

    iget-object v2, p0, Lcom/twitter/android/geo/places/b;->a:Landroid/content/Context;

    .line 48
    invoke-virtual {v1, v2}, Lcom/twitter/library/api/geo/c$a;->a(Landroid/content/Context;)Lcom/twitter/library/api/geo/c$a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/geo/places/b;->b:Lcom/twitter/library/client/v;

    .line 49
    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/api/geo/c$a;->a(Lcom/twitter/library/client/Session;)Lcom/twitter/library/api/geo/c$a;

    move-result-object v1

    .line 50
    invoke-virtual {v1, p1}, Lcom/twitter/library/api/geo/c$a;->a(Ljava/lang/String;)Lcom/twitter/library/api/geo/c$a;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/geo/places/b;->d:J

    .line 51
    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/api/geo/c$a;->a(J)Lcom/twitter/library/api/geo/c$a;

    move-result-object v1

    .line 52
    invoke-virtual {v1, p3}, Lcom/twitter/library/api/geo/c$a;->b(Ljava/lang/String;)Lcom/twitter/library/api/geo/c$a;

    move-result-object v1

    .line 53
    invoke-virtual {v1, p5}, Lcom/twitter/library/api/geo/c$a;->d(Ljava/lang/String;)Lcom/twitter/library/api/geo/c$a;

    move-result-object v1

    .line 54
    invoke-virtual {v1, p6}, Lcom/twitter/library/api/geo/c$a;->a(Z)Lcom/twitter/library/api/geo/c$a;

    move-result-object v1

    .line 55
    invoke-virtual {v1, p4}, Lcom/twitter/library/api/geo/c$a;->c(Ljava/lang/String;)Lcom/twitter/library/api/geo/c$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/api/geo/c$a;->a()Lcom/twitter/library/api/geo/c;

    move-result-object v1

    const/4 v2, 0x0

    .line 47
    invoke-virtual {v0, v1, v2, p0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;ILcom/twitter/library/client/s;)Z

    .line 59
    :cond_0
    return-void
.end method

.method public b(ILcom/twitter/library/service/s;)V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/android/geo/places/b;->e:Lcom/twitter/android/geo/places/b$a;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/twitter/android/geo/places/b;->e:Lcom/twitter/android/geo/places/b$a;

    check-cast p2, Lcom/twitter/library/api/geo/c;

    invoke-virtual {p2}, Lcom/twitter/library/api/geo/c;->e()Lcom/twitter/model/geo/d;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/geo/places/b$a;->a(Lcom/twitter/model/geo/d;)V

    .line 75
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/geo/places/b;->e:Lcom/twitter/android/geo/places/b$a;

    .line 76
    return-void
.end method
