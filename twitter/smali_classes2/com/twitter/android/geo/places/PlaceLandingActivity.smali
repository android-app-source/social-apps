.class public Lcom/twitter/android/geo/places/PlaceLandingActivity;
.super Lcom/twitter/android/ScrollingHeaderActivity;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/bv;
.implements Lcom/twitter/android/geo/places/PlaceTimelineFragment$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/geo/places/PlaceLandingActivity$a;,
        Lcom/twitter/android/geo/places/PlaceLandingActivity$b;,
        Lcom/twitter/android/geo/places/PlaceLandingActivity$PageType;
    }
.end annotation


# static fields
.field private static final a:Landroid/net/Uri;

.field private static final b:Landroid/net/Uri;


# instance fields
.field private c:Lcom/twitter/android/geo/places/d;

.field private d:Lcom/twitter/android/geo/places/PlaceLandingActivity$a;

.field private e:Lcom/twitter/android/at;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const-string/jumbo v0, "twitter://place/tweets"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->a:Landroid/net/Uri;

    .line 67
    const-string/jumbo v0, "twitter://place/media"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->b:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/twitter/android/ScrollingHeaderActivity;-><init>()V

    return-void
.end method

.method private a(Lcom/twitter/android/geo/places/PlaceLandingActivity$PageType;I)Lcom/twitter/library/client/m;
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 248
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->c:Lcom/twitter/android/geo/places/d;

    invoke-virtual {v0}, Lcom/twitter/android/geo/places/d;->a()Lcom/twitter/model/geo/TwitterPlace;

    move-result-object v7

    .line 249
    sget-object v0, Lcom/twitter/android/geo/places/PlaceLandingActivity$1;->a:[I

    invoke-virtual {p1}, Lcom/twitter/android/geo/places/PlaceLandingActivity$PageType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 282
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown page type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 251
    :pswitch_0
    const-class v2, Lcom/twitter/android/geo/places/PlaceTimelineFragment;

    .line 252
    const v1, 0x7f0a0991

    .line 253
    sget-object v3, Lcom/twitter/android/geo/places/PlaceLandingActivity;->a:Landroid/net/Uri;

    .line 254
    new-instance v0, Lcom/twitter/android/timeline/ai$a;

    invoke-virtual {p0}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/twitter/android/timeline/ai$a;-><init>(Landroid/os/Bundle;)V

    const v4, 0x7f0a05ea

    .line 255
    invoke-virtual {v0, v4}, Lcom/twitter/android/timeline/ai$a;->b(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/ai$a;

    const-string/jumbo v4, "fragment_page_number"

    .line 256
    invoke-virtual {v0, v4, p2}, Lcom/twitter/android/timeline/ai$a;->a(Ljava/lang/String;I)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/ai$a;

    iget-object v4, v7, Lcom/twitter/model/geo/TwitterPlace;->b:Ljava/lang/String;

    .line 257
    invoke-virtual {v0, v4}, Lcom/twitter/android/timeline/ai$a;->c(Ljava/lang/String;)Lcom/twitter/app/common/timeline/c$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/ai$a;

    .line 258
    invoke-virtual {v0}, Lcom/twitter/android/timeline/ai$a;->a()Lcom/twitter/android/timeline/ai;

    move-result-object v0

    move v10, v1

    move-object v1, v2

    move-object v2, v3

    move-object v3, v0

    move v0, v10

    .line 286
    :goto_0
    new-instance v4, Lcom/twitter/library/client/m$a;

    invoke-direct {v4, v2, v1}, Lcom/twitter/library/client/m$a;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    .line 287
    invoke-virtual {p0, v0}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/twitter/library/client/m$a;->a(Ljava/lang/CharSequence;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 288
    invoke-virtual {v0, v3}, Lcom/twitter/library/client/m$a;->a(Lcom/twitter/app/common/base/b;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 289
    invoke-virtual {v0}, Lcom/twitter/library/client/m$a;->a()Lcom/twitter/library/client/m;

    move-result-object v0

    .line 286
    return-object v0

    .line 262
    :pswitch_1
    const-class v5, Lcom/twitter/android/EventGridFragment;

    .line 263
    const v4, 0x7f0a06ec

    .line 264
    sget-object v6, Lcom/twitter/android/geo/places/PlaceLandingActivity;->b:Landroid/net/Uri;

    .line 265
    invoke-virtual {p0}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/bs$a;->a(Landroid/content/Intent;)Lcom/twitter/android/bs$a;

    move-result-object v0

    const v1, 0x7f0a05e7

    .line 266
    invoke-virtual {v0, v1}, Lcom/twitter/android/bs$a;->b(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bs$a;

    iget-object v1, v7, Lcom/twitter/model/geo/TwitterPlace;->c:Lcom/twitter/model/geo/TwitterPlace$PlaceType;

    sget-object v8, Lcom/twitter/model/geo/TwitterPlace$PlaceType;->a:Lcom/twitter/model/geo/TwitterPlace$PlaceType;

    if-ne v1, v8, :cond_0

    move v1, v2

    .line 267
    :goto_1
    invoke-virtual {v0, v1}, Lcom/twitter/android/bs$a;->a(Z)Lcom/twitter/android/bs$a;

    move-result-object v0

    const-string/jumbo v1, "fragment_page_number"

    .line 268
    invoke-virtual {v0, v1, p2}, Lcom/twitter/android/bs$a;->a(Ljava/lang/String;I)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bs$a;

    const-string/jumbo v1, "query"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "place:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v7, Lcom/twitter/model/geo/TwitterPlace;->b:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 269
    invoke-virtual {v0, v1, v8}, Lcom/twitter/android/bs$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bs$a;

    const-string/jumbo v1, "search_id"

    sget-object v8, Lcom/twitter/util/y;->a:Ljava/security/SecureRandom;

    .line 270
    invoke-virtual {v8}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v8

    invoke-virtual {v0, v1, v8, v9}, Lcom/twitter/android/bs$a;->a(Ljava/lang/String;J)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bs$a;

    const-string/jumbo v1, "query_name"

    iget-object v7, v7, Lcom/twitter/model/geo/TwitterPlace;->d:Ljava/lang/String;

    .line 271
    invoke-virtual {v0, v1, v7}, Lcom/twitter/android/bs$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bs$a;

    const-string/jumbo v1, "search_button"

    .line 272
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/bs$a;->a(Ljava/lang/String;Z)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bs$a;

    const-string/jumbo v1, "terminal"

    .line 273
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/bs$a;->a(Ljava/lang/String;Z)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bs$a;

    const-string/jumbo v1, "should_shim"

    .line 274
    invoke-virtual {v0, v1, v3}, Lcom/twitter/android/bs$a;->a(Ljava/lang/String;Z)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bs$a;

    const-string/jumbo v1, "should_refresh"

    .line 275
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/bs$a;->a(Ljava/lang/String;Z)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bs$a;

    const-string/jumbo v1, "search_type"

    .line 276
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/bs$a;->a(Ljava/lang/String;I)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bs$a;

    .line 277
    invoke-virtual {v0}, Lcom/twitter/android/bs$a;->a()Lcom/twitter/android/bs;

    move-result-object v0

    move-object v1, v5

    move-object v2, v6

    move-object v3, v0

    move v0, v4

    .line 278
    goto/16 :goto_0

    :cond_0
    move v1, v3

    .line 266
    goto :goto_1

    .line 249
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lcom/twitter/android/geo/places/PlaceLandingActivity;)Ljava/util/List;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->m:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/geo/places/PlaceLandingActivity;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 59
    invoke-virtual {p0, p1}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/geo/places/PlaceLandingActivity;Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 59
    invoke-virtual {p0, p1}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->a(Landroid/support/v4/app/Fragment;)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/geo/places/PlaceLandingActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->n:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/geo/places/PlaceLandingActivity;Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 59
    invoke-virtual {p0, p1}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->a(Landroid/support/v4/app/Fragment;)V

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/geo/places/PlaceLandingActivity;)Lcom/twitter/android/at;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->e:Lcom/twitter/android/at;

    return-object v0
.end method

.method private i()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 125
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 128
    :cond_0
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/res/Resources;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 168
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 169
    iget-object v1, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->d:Lcom/twitter/android/geo/places/PlaceLandingActivity$a;

    invoke-virtual {v1}, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->d()Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    move-result-object v1

    .line 170
    const/high16 v2, 0x40000000    # 2.0f

    .line 171
    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 172
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 170
    invoke-virtual {v1, v0, v2}, Landroid/view/View;->measure(II)V

    .line 173
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method protected a(Ljava/util/List;Landroid/support/v4/view/ViewPager;)Landroid/support/v4/view/PagerAdapter;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;",
            "Landroid/support/v4/view/ViewPager;",
            ")",
            "Landroid/support/v4/view/PagerAdapter;"
        }
    .end annotation

    .prologue
    .line 209
    new-instance v0, Lcom/twitter/android/geo/places/PlaceLandingActivity$b;

    iget-object v5, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->p:Lcom/twitter/internal/android/widget/HorizontalListView;

    iget-object v6, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->e:Lcom/twitter/android/at;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/geo/places/PlaceLandingActivity$b;-><init>(Lcom/twitter/android/geo/places/PlaceLandingActivity;Landroid/support/v4/app/FragmentActivity;Ljava/util/List;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/HorizontalListView;Lcom/twitter/android/at;)V

    return-object v0
.end method

.method protected a(Ljava/util/List;)Landroid/widget/BaseAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;)",
            "Landroid/widget/BaseAdapter;"
        }
    .end annotation

    .prologue
    .line 202
    new-instance v0, Lcom/twitter/android/at;

    invoke-direct {v0, p1}, Lcom/twitter/android/at;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->e:Lcom/twitter/android/at;

    .line 203
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->e:Lcom/twitter/android/at;

    return-object v0
.end method

.method protected a(I)V
    .locals 2

    .prologue
    .line 294
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderActivity;->a(I)V

    .line 295
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->c:Lcom/twitter/android/geo/places/d;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/geo/places/d;->a(IZ)V

    .line 296
    return-void
.end method

.method protected a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->d:Lcom/twitter/android/geo/places/PlaceLandingActivity$a;

    invoke-virtual {v0, p1}, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->a(Landroid/graphics/drawable/Drawable;)V

    .line 240
    return-void
.end method

.method public a(ZLandroid/widget/ListView;Lcom/twitter/android/SearchFragment;)V
    .locals 0

    .prologue
    .line 184
    return-void
.end method

.method public a(Ljava/lang/String;JLcom/twitter/model/topic/TwitterTopic;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x0

    return v0
.end method

.method protected ab_()I
    .locals 2

    .prologue
    .line 219
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->d:Lcom/twitter/android/geo/places/PlaceLandingActivity$a;

    invoke-virtual {v0}, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->d()Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->r:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->c:Lcom/twitter/android/geo/places/d;

    invoke-virtual {v0}, Lcom/twitter/android/geo/places/d;->c()V

    .line 301
    return-void
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 10

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_place"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    invoke-virtual {p0}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_place"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    if-nez v0, :cond_1

    .line 87
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "PlaceLandingActivity expects a value for extra_place in its intent extras!"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_place"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/model/geo/TwitterPlace;->a([B)Lcom/twitter/model/geo/TwitterPlace;

    move-result-object v4

    .line 93
    const v0, 0x7f0402ac

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/widget/LinearLayout;

    .line 96
    new-instance v0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;

    invoke-direct {v0, p0, v8}, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;-><init>(Lcom/twitter/android/geo/places/PlaceLandingActivity;Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->d:Lcom/twitter/android/geo/places/PlaceLandingActivity$a;

    .line 97
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->d:Lcom/twitter/android/geo/places/PlaceLandingActivity$a;

    .line 98
    invoke-virtual {v0}, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->d()Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    move-result-object v9

    .line 99
    const v0, 0x7f130651

    .line 101
    invoke-virtual {v9, v0}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 100
    invoke-static {p0, v0}, Lbqt;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Lbqs;

    move-result-object v1

    .line 103
    invoke-virtual {p0}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v6

    .line 104
    new-instance v0, Lcom/twitter/android/geo/places/d;

    new-instance v2, Lcom/twitter/android/geo/places/b;

    invoke-direct {v2, p0}, Lcom/twitter/android/geo/places/b;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/twitter/android/geo/places/g;

    .line 107
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v5

    invoke-direct {v3, v5, p0, v6}, Lcom/twitter/android/geo/places/g;-><init>(Lcom/twitter/library/client/p;Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    iget-object v5, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->d:Lcom/twitter/android/geo/places/PlaceLandingActivity$a;

    .line 108
    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/geo/places/d;-><init>(Lbqs;Lcom/twitter/android/geo/places/b;Lcom/twitter/android/geo/places/g;Lcom/twitter/model/geo/TwitterPlace;Lcom/twitter/android/geo/places/d$a;J)V

    iput-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->c:Lcom/twitter/android/geo/places/d;

    .line 111
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ScrollingHeaderActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 113
    invoke-virtual {p0, v8}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->setHeaderView(Landroid/view/View;)V

    .line 114
    invoke-virtual {v9}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;->getAspectRatio()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->c(F)V

    .line 117
    invoke-virtual {v8, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    invoke-direct {p0}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->i()V

    .line 120
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->c:Lcom/twitter/android/geo/places/d;

    invoke-virtual {v0, p1}, Lcom/twitter/android/geo/places/d;->a(Landroid/os/Bundle;)V

    .line 121
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 229
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderActivity;->b(Z)V

    .line 232
    if-eqz p1, :cond_0

    .line 233
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->c:Lcom/twitter/android/geo/places/d;

    invoke-virtual {v0}, Lcom/twitter/android/geo/places/d;->b()V

    .line 235
    :cond_0
    return-void
.end method

.method protected d()V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->c:Lcom/twitter/android/geo/places/d;

    invoke-virtual {v0}, Lcom/twitter/android/geo/places/d;->i()V

    .line 157
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderActivity;->d()V

    .line 158
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 188
    return-void
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 162
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderActivity;->onLowMemory()V

    .line 163
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->c:Lcom/twitter/android/geo/places/d;

    invoke-virtual {v0}, Lcom/twitter/android/geo/places/d;->h()V

    .line 164
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->c:Lcom/twitter/android/geo/places/d;

    invoke-virtual {v0}, Lcom/twitter/android/geo/places/d;->g()V

    .line 145
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderActivity;->onPause()V

    .line 146
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->c:Lcom/twitter/android/geo/places/d;

    invoke-virtual {v0}, Lcom/twitter/android/geo/places/d;->f()V

    .line 133
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderActivity;->onResume()V

    .line 134
    return-void
.end method

.method protected onResumeFragments()V
    .locals 1

    .prologue
    .line 138
    invoke-super {p0}, Lcom/twitter/android/ScrollingHeaderActivity;->onResumeFragments()V

    .line 139
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->c:Lcom/twitter/android/geo/places/d;

    invoke-virtual {v0}, Lcom/twitter/android/geo/places/d;->d()V

    .line 140
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->c:Lcom/twitter/android/geo/places/d;

    invoke-virtual {v0, p1}, Lcom/twitter/android/geo/places/d;->b(Landroid/os/Bundle;)V

    .line 151
    invoke-super {p0, p1}, Lcom/twitter/android/ScrollingHeaderActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 152
    return-void
.end method

.method protected r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->c:Lcom/twitter/android/geo/places/d;

    invoke-virtual {v0}, Lcom/twitter/android/geo/places/d;->a()Lcom/twitter/model/geo/TwitterPlace;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/geo/TwitterPlace;->d:Ljava/lang/String;

    return-object v0
.end method

.method protected s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x0

    return-object v0
.end method

.method protected t()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 193
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity;->c:Lcom/twitter/android/geo/places/d;

    invoke-virtual {v0, v3, v3}, Lcom/twitter/android/geo/places/d;->a(IZ)V

    .line 194
    sget-object v0, Lcom/twitter/android/geo/places/PlaceLandingActivity$PageType;->a:Lcom/twitter/android/geo/places/PlaceLandingActivity$PageType;

    .line 195
    invoke-direct {p0, v0, v3}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->a(Lcom/twitter/android/geo/places/PlaceLandingActivity$PageType;I)Lcom/twitter/library/client/m;

    move-result-object v0

    new-array v1, v4, [Lcom/twitter/library/client/m;

    sget-object v2, Lcom/twitter/android/geo/places/PlaceLandingActivity$PageType;->b:Lcom/twitter/android/geo/places/PlaceLandingActivity$PageType;

    .line 196
    invoke-direct {p0, v2, v4}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->a(Lcom/twitter/android/geo/places/PlaceLandingActivity$PageType;I)Lcom/twitter/library/client/m;

    move-result-object v2

    aput-object v2, v1, v3

    .line 194
    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
