.class Lcom/twitter/android/geo/places/PlaceLandingActivity$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/geo/places/d$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/geo/places/PlaceLandingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/geo/places/PlaceLandingActivity;

.field private final b:Landroid/view/ViewGroup;

.field private final c:Landroid/widget/ImageView;

.field private final d:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

.field private final e:Landroid/view/ViewGroup;

.field private f:Lcom/twitter/android/geo/places/f;

.field private g:Lcom/twitter/android/geo/places/c;


# direct methods
.method constructor <init>(Lcom/twitter/android/geo/places/PlaceLandingActivity;Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 356
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 357
    iput-object p1, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->a:Lcom/twitter/android/geo/places/PlaceLandingActivity;

    .line 358
    iput-object p2, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->b:Landroid/view/ViewGroup;

    .line 360
    const v0, 0x7f130167

    .line 361
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    iput-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->d:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    .line 362
    const v0, 0x7f130652

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->c:Landroid/widget/ImageView;

    .line 363
    const v0, 0x7f130152

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->e:Landroid/view/ViewGroup;

    .line 364
    return-void
.end method

.method private a(ILjava/lang/CharSequence;)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param

    .prologue
    .line 433
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 434
    invoke-static {p2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 435
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 436
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 441
    :goto_0
    return-void

    .line 438
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 439
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 408
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->a:Lcom/twitter/android/geo/places/PlaceLandingActivity;

    const v1, 0x7f0a069d

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 409
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 410
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->a:Lcom/twitter/android/geo/places/PlaceLandingActivity;

    invoke-static {v0}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->b(Lcom/twitter/android/geo/places/PlaceLandingActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 403
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->a:Lcom/twitter/android/geo/places/PlaceLandingActivity;

    invoke-static {v0}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->c(Lcom/twitter/android/geo/places/PlaceLandingActivity;)Lcom/twitter/android/at;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/at;->a(I)V

    .line 404
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->a:Lcom/twitter/android/geo/places/PlaceLandingActivity;

    invoke-static {v0, p1}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->a(Lcom/twitter/android/geo/places/PlaceLandingActivity;Landroid/graphics/Bitmap;)V

    .line 369
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 449
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 373
    const v0, 0x7f130653

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->a(ILjava/lang/CharSequence;)V

    .line 374
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 388
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->a:Lcom/twitter/android/geo/places/PlaceLandingActivity;

    invoke-virtual {v0, p1}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->b(Z)V

    .line 391
    if-nez p1, :cond_0

    .line 392
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->a:Lcom/twitter/android/geo/places/PlaceLandingActivity;

    invoke-static {v0}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->a(Lcom/twitter/android/geo/places/PlaceLandingActivity;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/m;

    .line 393
    iget-object v1, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->a:Lcom/twitter/android/geo/places/PlaceLandingActivity;

    .line 395
    invoke-virtual {v1}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 394
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/m;->a(Landroid/support/v4/app/FragmentManager;)Lcom/twitter/app/common/base/BaseFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/geo/places/PlaceTimelineFragment;

    .line 396
    invoke-virtual {v0}, Lcom/twitter/android/geo/places/PlaceTimelineFragment;->aA_()V

    .line 398
    :cond_0
    return-void
.end method

.method public b()Lcom/twitter/android/geo/places/f;
    .locals 2

    .prologue
    .line 415
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->f:Lcom/twitter/android/geo/places/f;

    if-nez v0, :cond_0

    .line 416
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->e:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/twitter/android/geo/places/f;->a(Landroid/view/ViewGroup;)Lcom/twitter/android/geo/places/f;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->f:Lcom/twitter/android/geo/places/f;

    .line 417
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->e:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->f:Lcom/twitter/android/geo/places/f;

    invoke-virtual {v1}, Lcom/twitter/android/geo/places/f;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 419
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->f:Lcom/twitter/android/geo/places/f;

    return-object v0
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 378
    const v0, 0x7f130654

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->a(ILjava/lang/CharSequence;)V

    .line 379
    return-void
.end method

.method public c()Lcom/twitter/android/geo/places/c;
    .locals 2

    .prologue
    .line 425
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->g:Lcom/twitter/android/geo/places/c;

    if-nez v0, :cond_0

    .line 426
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->e:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/twitter/android/geo/places/c;->a(Landroid/view/ViewGroup;)Lcom/twitter/android/geo/places/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->g:Lcom/twitter/android/geo/places/c;

    .line 427
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->e:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->g:Lcom/twitter/android/geo/places/c;

    invoke-virtual {v1}, Lcom/twitter/android/geo/places/c;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 429
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->g:Lcom/twitter/android/geo/places/c;

    return-object v0
.end method

.method public c(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 383
    const v0, 0x7f130655

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->a(ILjava/lang/CharSequence;)V

    .line 384
    return-void
.end method

.method public d()Lcom/twitter/media/ui/image/AspectRatioFrameLayout;
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$a;->d:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    return-object v0
.end method
