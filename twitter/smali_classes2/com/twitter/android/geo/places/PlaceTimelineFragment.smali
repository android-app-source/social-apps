.class public Lcom/twitter/android/geo/places/PlaceTimelineFragment;
.super Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/geo/places/PlaceTimelineFragment$a;
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/android/geo/places/PlaceTimelineFragment$a;

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic H()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/twitter/android/geo/places/PlaceTimelineFragment;->h()Lcom/twitter/android/timeline/ai;

    move-result-object v0

    return-object v0
.end method

.method protected H_()V
    .locals 0

    .prologue
    .line 32
    return-void
.end method

.method public synthetic I()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/twitter/android/geo/places/PlaceTimelineFragment;->h()Lcom/twitter/android/timeline/ai;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/android/geo/places/PlaceTimelineFragment$a;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/twitter/android/geo/places/PlaceTimelineFragment;->a:Lcom/twitter/android/geo/places/PlaceTimelineFragment$a;

    .line 36
    return-void
.end method

.method public aA_()V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/geo/places/PlaceTimelineFragment;->b:Z

    .line 74
    return-void
.end method

.method protected c(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 63
    if-ne p1, v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/geo/places/PlaceTimelineFragment;->a:Lcom/twitter/android/geo/places/PlaceTimelineFragment$a;

    if-eqz v1, :cond_0

    .line 64
    iput-boolean v0, p0, Lcom/twitter/android/geo/places/PlaceTimelineFragment;->b:Z

    .line 65
    iget-object v1, p0, Lcom/twitter/android/geo/places/PlaceTimelineFragment;->a:Lcom/twitter/android/geo/places/PlaceTimelineFragment$a;

    invoke-interface {v1}, Lcom/twitter/android/geo/places/PlaceTimelineFragment$a;->b()V

    .line 69
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected c_(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 50
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;->c_(I)Z

    move-result v1

    if-eqz v1, :cond_0

    if-ne p1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 78
    if-ne p1, v0, :cond_0

    iget-boolean v1, p0, Lcom/twitter/android/geo/places/PlaceTimelineFragment;->b:Z

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Lcom/twitter/android/timeline/ai;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/twitter/android/geo/places/PlaceTimelineFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/timeline/ai;->a(Landroid/os/Bundle;)Lcom/twitter/android/timeline/ai;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lcom/twitter/app/common/timeline/c;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/twitter/android/geo/places/PlaceTimelineFragment;->h()Lcom/twitter/android/timeline/ai;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 21
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;->onCreate(Landroid/os/Bundle;)V

    .line 23
    new-instance v0, Lcom/twitter/ui/view/h$a;

    invoke-direct {v0}, Lcom/twitter/ui/view/h$a;-><init>()V

    const/4 v1, 0x1

    .line 24
    invoke-virtual {v0, v1}, Lcom/twitter/ui/view/h$a;->i(Z)Lcom/twitter/ui/view/h$a;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lcom/twitter/ui/view/h$a;->a()Lcom/twitter/ui/view/h;

    move-result-object v0

    .line 26
    invoke-virtual {p0, v0}, Lcom/twitter/android/geo/places/PlaceTimelineFragment;->a(Lcom/twitter/ui/view/h;)V

    .line 27
    return-void
.end method
