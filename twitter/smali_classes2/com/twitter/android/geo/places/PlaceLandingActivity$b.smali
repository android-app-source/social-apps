.class Lcom/twitter/android/geo/places/PlaceLandingActivity$b;
.super Lcom/twitter/android/AbsPagesAdapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/geo/places/PlaceLandingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "b"
.end annotation


# instance fields
.field final synthetic g:Lcom/twitter/android/geo/places/PlaceLandingActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/geo/places/PlaceLandingActivity;Landroid/support/v4/app/FragmentActivity;Ljava/util/List;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/HorizontalListView;Lcom/twitter/android/at;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentActivity;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;",
            "Landroid/support/v4/view/ViewPager;",
            "Lcom/twitter/internal/android/widget/HorizontalListView;",
            "Lcom/twitter/android/at;",
            ")V"
        }
    .end annotation

    .prologue
    .line 306
    iput-object p1, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$b;->g:Lcom/twitter/android/geo/places/PlaceLandingActivity;

    .line 307
    invoke-virtual {p2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    move-object v0, p0

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/AbsPagesAdapter;-><init>(Landroid/support/v4/app/FragmentActivity;Landroid/support/v4/app/FragmentManager;Ljava/util/List;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/HorizontalListView;Lcom/twitter/android/at;)V

    .line 308
    invoke-virtual {p4}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$b;->f:I

    .line 309
    return-void
.end method


# virtual methods
.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 313
    invoke-super {p0, p1, p2}, Lcom/twitter/android/AbsPagesAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/TweetListFragment;

    .line 314
    invoke-virtual {p0, v0, p2}, Lcom/twitter/android/geo/places/PlaceLandingActivity$b;->a(Lcom/twitter/app/common/base/BaseFragment;I)V

    .line 316
    if-nez p2, :cond_0

    instance-of v1, v0, Lcom/twitter/android/geo/places/PlaceTimelineFragment;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 317
    check-cast v1, Lcom/twitter/android/geo/places/PlaceTimelineFragment;

    iget-object v2, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$b;->g:Lcom/twitter/android/geo/places/PlaceLandingActivity;

    invoke-virtual {v1, v2}, Lcom/twitter/android/geo/places/PlaceTimelineFragment;->a(Lcom/twitter/android/geo/places/PlaceTimelineFragment$a;)V

    .line 320
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$b;->g:Lcom/twitter/android/geo/places/PlaceLandingActivity;

    invoke-static {v1, v0}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->a(Lcom/twitter/android/geo/places/PlaceLandingActivity;Landroid/support/v4/app/Fragment;)V

    .line 321
    return-object v0
.end method

.method public onPageScrollStateChanged(I)V
    .locals 3

    .prologue
    .line 336
    invoke-super {p0, p1}, Lcom/twitter/android/AbsPagesAdapter;->onPageScrollStateChanged(I)V

    .line 337
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$b;->e:Lcom/twitter/android/at;

    invoke-virtual {v0}, Lcom/twitter/android/at;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/m;

    .line 339
    iget-object v2, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$b;->g:Lcom/twitter/android/geo/places/PlaceLandingActivity;

    invoke-virtual {p0, v0}, Lcom/twitter/android/geo/places/PlaceLandingActivity$b;->c(Lcom/twitter/library/client/m;)Lcom/twitter/app/common/base/BaseFragment;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/twitter/android/geo/places/PlaceLandingActivity;->b(Lcom/twitter/android/geo/places/PlaceLandingActivity;Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 342
    :cond_0
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1

    .prologue
    .line 326
    invoke-super {p0, p1}, Lcom/twitter/android/AbsPagesAdapter;->onPageSelected(I)V

    .line 328
    iget v0, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$b;->f:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/geo/places/PlaceLandingActivity$b;->c(I)Lcom/twitter/library/client/m;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/geo/places/PlaceLandingActivity$b;->a(Lcom/twitter/library/client/m;)Z

    .line 329
    invoke-virtual {p0, p1}, Lcom/twitter/android/geo/places/PlaceLandingActivity$b;->a(I)Lcom/twitter/library/client/m;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/geo/places/PlaceLandingActivity$b;->b(Lcom/twitter/library/client/m;)Z

    .line 331
    iput p1, p0, Lcom/twitter/android/geo/places/PlaceLandingActivity$b;->f:I

    .line 332
    return-void
.end method
