.class public Lcom/twitter/android/geo/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/dialog/b$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/geo/a$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private b:I

.field private final c:Landroid/support/v4/app/FragmentActivity;

.field private final d:Landroid/support/v4/app/FragmentManager;

.field private final e:Lcom/twitter/util/android/f;

.field private final f:I

.field private g:Lcom/twitter/android/geo/a$a;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Lcom/twitter/util/android/f;I)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/geo/a;->d:Landroid/support/v4/app/FragmentManager;

    .line 70
    iput-object p1, p0, Lcom/twitter/android/geo/a;->c:Landroid/support/v4/app/FragmentActivity;

    .line 71
    iput-object p2, p0, Lcom/twitter/android/geo/a;->a:Ljava/lang/String;

    .line 72
    iput-object p3, p0, Lcom/twitter/android/geo/a;->e:Lcom/twitter/util/android/f;

    .line 73
    iput p4, p0, Lcom/twitter/android/geo/a;->f:I

    .line 76
    iget-object v0, p0, Lcom/twitter/android/geo/a;->d:Landroid/support/v4/app/FragmentManager;

    .line 77
    invoke-virtual {v0, p2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PromptDialogFragment;

    .line 78
    if-eqz v0, :cond_0

    .line 79
    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    .line 80
    invoke-virtual {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->c()Lcom/twitter/android/widget/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/widget/aj;->D()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/geo/a;->b:I

    .line 82
    :cond_0
    return-void
.end method

.method private a()V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 257
    iget-object v0, p0, Lcom/twitter/android/geo/a;->c:Landroid/support/v4/app/FragmentActivity;

    iget v1, p0, Lcom/twitter/android/geo/a;->f:I

    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v3, v2, v5

    new-array v3, v4, [I

    const/4 v4, -0x1

    aput v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentActivity;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    .line 260
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 121
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-static {p0, v0}, Lcom/twitter/android/geo/a;->a(Landroid/content/Context;Landroid/app/AlertDialog$Builder;)V

    .line 122
    return-void
.end method

.method static a(Landroid/content/Context;Landroid/app/AlertDialog$Builder;)V
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 130
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/android/f;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    const v0, 0x7f0a026f

    .line 132
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a03d2

    new-instance v2, Lcom/twitter/android/geo/a$1;

    invoke-direct {v2, p0}, Lcom/twitter/android/geo/a$1;-><init>(Landroid/content/Context;)V

    .line 133
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00f6

    const/4 v2, 0x0

    .line 139
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 140
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 141
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 142
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 144
    :cond_0
    return-void
.end method

.method private b()J
    .locals 2

    .prologue
    .line 263
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    return-wide v0
.end method

.method private b(I)V
    .locals 5

    .prologue
    const v4, 0x7f0a00f6

    const/4 v3, 0x0

    .line 169
    iget-object v0, p0, Lcom/twitter/android/geo/a;->c:Landroid/support/v4/app/FragmentActivity;

    .line 171
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    .line 173
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    invoke-direct {v0, p1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    iget v1, p0, Lcom/twitter/android/geo/a;->b:I

    .line 174
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->h(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 175
    packed-switch p1, :pswitch_data_0

    .line 206
    :goto_0
    return-void

    .line 177
    :pswitch_0
    const v1, 0x7f0a026d

    .line 178
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/aj$b;

    const v2, 0x7f0a0616

    .line 179
    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/aj$b;

    const v2, 0x7f0a03d6

    .line 180
    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    .line 202
    :goto_1
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PromptDialogFragment;

    .line 203
    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v1

    .line 204
    invoke-virtual {v1, v3}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->setCancelable(Z)V

    .line 205
    iget-object v1, p0, Lcom/twitter/android/geo/a;->d:Landroid/support/v4/app/FragmentManager;

    iget-object v2, p0, Lcom/twitter/android/geo/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 184
    :pswitch_1
    const v1, 0x7f0a0987

    .line 185
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/aj$b;

    const v2, 0x7f0a0986

    .line 186
    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/aj$b;

    const v2, 0x7f0a037c

    .line 187
    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/aj$b;

    .line 188
    invoke-virtual {v1, v4}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    goto :goto_1

    .line 192
    :pswitch_2
    const v1, 0x7f0a0270

    .line 193
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/aj$b;

    const v2, 0x7f0a0800

    .line 194
    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/aj$b;

    .line 195
    invoke-virtual {v1, v4}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    goto :goto_1

    .line 175
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 165
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-static {p0, v0}, Lcom/twitter/android/geo/a;->b(Landroid/content/Context;Landroid/app/AlertDialog$Builder;)V

    .line 166
    return-void
.end method

.method static b(Landroid/content/Context;Landroid/app/AlertDialog$Builder;)V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 152
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v0

    .line 153
    invoke-virtual {v0}, Lbqg;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lbqg;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 154
    invoke-virtual {v0}, Lbqg;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 155
    invoke-static {p0, p1}, Lcom/twitter/android/geo/a;->a(Landroid/content/Context;Landroid/app/AlertDialog$Builder;)V

    .line 157
    :cond_0
    return-void
.end method

.method public static c(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 251
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10000000

    .line 252
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 251
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 253
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 94
    iput p1, p0, Lcom/twitter/android/geo/a;->b:I

    .line 95
    and-int/lit8 v0, p1, 0x1

    if-lez v0, :cond_0

    move v4, v1

    .line 96
    :goto_0
    and-int/lit8 v0, p1, 0x2

    if-lez v0, :cond_1

    move v3, v1

    .line 97
    :goto_1
    and-int/lit8 v0, p1, 0x4

    if-lez v0, :cond_2

    move v0, v1

    .line 99
    :goto_2
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v5

    .line 100
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v6

    .line 101
    if-eqz v3, :cond_3

    invoke-virtual {v6, v5}, Lbqg;->b(Lcom/twitter/library/client/Session;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 102
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/twitter/android/geo/a;->b(I)V

    .line 113
    :goto_3
    return-void

    :cond_0
    move v4, v2

    .line 95
    goto :goto_0

    :cond_1
    move v3, v2

    .line 96
    goto :goto_1

    :cond_2
    move v0, v2

    .line 97
    goto :goto_2

    .line 104
    :cond_3
    if-eqz v4, :cond_4

    invoke-virtual {v6}, Lbqg;->e()Z

    move-result v3

    if-nez v3, :cond_4

    .line 105
    invoke-direct {p0, v1}, Lcom/twitter/android/geo/a;->b(I)V

    .line 106
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {p0}, Lcom/twitter/android/geo/a;->b()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v3, "location_prompt::::impression"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_3

    .line 108
    :cond_4
    if-eqz v0, :cond_5

    invoke-virtual {v6}, Lbqg;->f()Z

    move-result v0

    if-nez v0, :cond_5

    .line 109
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/twitter/android/geo/a;->b(I)V

    goto :goto_3

    .line 112
    :cond_5
    iget-object v0, p0, Lcom/twitter/android/geo/a;->e:Lcom/twitter/util/android/f;

    iget v3, p0, Lcom/twitter/android/geo/a;->f:I

    iget-object v4, p0, Lcom/twitter/android/geo/a;->c:Landroid/support/v4/app/FragmentActivity;

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v5, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v5, v1, v2

    invoke-virtual {v0, v3, v4, v1}, Lcom/twitter/util/android/f;->a(ILandroid/app/Activity;[Ljava/lang/String;)V

    goto :goto_3
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, -0x1

    const/4 v4, 0x1

    .line 211
    packed-switch p2, :pswitch_data_0

    .line 248
    :cond_0
    :goto_0
    return-void

    .line 213
    :pswitch_0
    if-ne v0, p3, :cond_1

    .line 214
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v0

    invoke-virtual {v0, v4}, Lbqg;->a(Z)V

    .line 215
    iget v0, p0, Lcom/twitter/android/geo/a;->b:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/geo/a;->a(I)V

    .line 216
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {p0}, Lcom/twitter/android/geo/a;->b()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "location_prompt:::allow:click"

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 217
    :cond_1
    const/4 v0, -0x2

    if-ne v0, p3, :cond_0

    .line 218
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {p0}, Lcom/twitter/android/geo/a;->b()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "location_prompt:::deny:click"

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 219
    invoke-direct {p0}, Lcom/twitter/android/geo/a;->a()V

    goto :goto_0

    .line 224
    :pswitch_1
    if-ne v0, p3, :cond_2

    .line 225
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v0

    .line 226
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 225
    invoke-virtual {v0, v1, v4}, Lbqg;->b(Lcom/twitter/library/client/Session;Z)Z

    .line 227
    iget v0, p0, Lcom/twitter/android/geo/a;->b:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/geo/a;->a(I)V

    goto :goto_0

    .line 229
    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/geo/a;->a()V

    goto :goto_0

    .line 234
    :pswitch_2
    if-ne v0, p3, :cond_4

    .line 235
    iget-object v0, p0, Lcom/twitter/android/geo/a;->g:Lcom/twitter/android/geo/a$a;

    if-eqz v0, :cond_3

    .line 236
    iget-object v0, p0, Lcom/twitter/android/geo/a;->g:Lcom/twitter/android/geo/a$a;

    invoke-interface {v0}, Lcom/twitter/android/geo/a$a;->a()V

    .line 238
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/geo/a;->c:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0}, Lcom/twitter/android/geo/a;->c(Landroid/content/Context;)V

    goto :goto_0

    .line 240
    :cond_4
    invoke-direct {p0}, Lcom/twitter/android/geo/a;->a()V

    goto :goto_0

    .line 211
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lcom/twitter/android/geo/a$a;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/twitter/android/geo/a;->g:Lcom/twitter/android/geo/a$a;

    .line 86
    return-void
.end method
