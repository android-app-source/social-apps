.class public final Lcom/twitter/android/GalleryGridActivity$b;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/GalleryGridActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 168
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/twitter/android/GalleryGridActivity$b;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/GalleryGridActivity$b;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/GalleryGridActivity$b;->c:Ljava/lang/String;

    .line 210
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 209
    :goto_0
    return v0

    .line 210
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Lcom/twitter/android/GalleryGridActivity$b;
    .locals 0

    .prologue
    .line 203
    iput p1, p0, Lcom/twitter/android/GalleryGridActivity$b;->e:I

    .line 204
    return-object p0
.end method

.method public a(Landroid/content/Context;)Lcom/twitter/android/GalleryGridActivity$b;
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/twitter/android/GalleryGridActivity$b;->a:Landroid/content/Context;

    .line 180
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/android/GalleryGridActivity$b;
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/twitter/android/GalleryGridActivity$b;->b:Ljava/lang/String;

    .line 186
    return-object p0
.end method

.method public a(Z)Lcom/twitter/android/GalleryGridActivity$b;
    .locals 0

    .prologue
    .line 197
    iput-boolean p1, p0, Lcom/twitter/android/GalleryGridActivity$b;->d:Z

    .line 198
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/android/GalleryGridActivity$b;
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/twitter/android/GalleryGridActivity$b;->c:Ljava/lang/String;

    .line 192
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/twitter/android/GalleryGridActivity$b;->d()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected d()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 216
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/GalleryGridActivity$b;->a:Landroid/content/Context;

    const-class v2, Lcom/twitter/android/GalleryGridActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "scribe_section"

    iget-object v2, p0, Lcom/twitter/android/GalleryGridActivity$b;->b:Ljava/lang/String;

    .line 217
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "permissions_event_prefix"

    iget-object v2, p0, Lcom/twitter/android/GalleryGridActivity$b;->c:Ljava/lang/String;

    .line 218
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "is_video_allowed"

    iget-boolean v2, p0, Lcom/twitter/android/GalleryGridActivity$b;->d:Z

    .line 219
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "camera_initiator"

    iget v2, p0, Lcom/twitter/android/GalleryGridActivity$b;->e:I

    .line 220
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 216
    return-object v0
.end method
