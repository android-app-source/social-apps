.class public Lcom/twitter/android/dogfood/BugReporterActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/dogfood/BugReporterActivity$a;
    }
.end annotation


# static fields
.field private static final a:[Lcom/twitter/android/dogfood/BugReporterActivity$a;


# instance fields
.field private b:Landroid/view/View;

.field private c:Landroid/widget/EditText;

.field private d:I

.field private e:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 58
    const/16 v0, 0x1d

    new-array v0, v0, [Lcom/twitter/android/dogfood/BugReporterActivity$a;

    new-instance v1, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v2, "Where did we go wrong?"

    invoke-direct {v1, v2}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "** New Bugs / Triage **"

    aput-object v3, v2, v5

    .line 59
    invoke-virtual {v1, v2}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v1

    aput-object v1, v0, v5

    new-instance v1, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v2, "Tweet Anatomy"

    invoke-direct {v1, v2}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "Tweet Anatomy"

    aput-object v3, v2, v5

    .line 60
    invoke-virtual {v1, v2}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v1

    aput-object v1, v0, v6

    const/4 v1, 0x2

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Tweet Detail"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "Tweet Detail"

    aput-object v4, v3, v5

    .line 61
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "DMs"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "DM"

    aput-object v4, v3, v5

    .line 62
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Profiles"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "Profiles"

    aput-object v4, v3, v5

    .line 63
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Guide"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "Guide"

    aput-object v4, v3, v5

    .line 64
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Find People/Connect"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "People Discovery"

    aput-object v4, v3, v5

    .line 65
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Search & Trends"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "Search & Trends"

    aput-object v4, v3, v5

    .line 66
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Notifications & Badging"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "Notifications"

    aput-object v4, v3, v5

    .line 67
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Notifications Tab"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "Notifications"

    aput-object v4, v3, v5

    .line 68
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Photos"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "Expression"

    aput-object v4, v3, v5

    .line 69
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Composer"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "Composer"

    aput-object v4, v3, v5

    .line 70
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Cards"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "Cards"

    aput-object v4, v3, v5

    .line 71
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Translation/Localization"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "** New Bugs / Triage **"

    aput-object v4, v3, v5

    .line 72
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Geo/Geotagging"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "Geo"

    aput-object v4, v3, v5

    .line 73
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Moments"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "Moments"

    aput-object v4, v3, v5

    .line 74
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Onboarding"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "Onboarding"

    aput-object v4, v3, v5

    .line 75
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Lists"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "Lists & Collections"

    aput-object v4, v3, v5

    .line 76
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Product Safety"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "CUP"

    .line 77
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a(Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Promoted"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "REVFMTS"

    .line 78
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a(Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Video"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "Media Consumption"

    aput-object v4, v3, v5

    .line 79
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Audio"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "Media Consumption"

    aput-object v4, v3, v5

    .line 80
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Home Timeline"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "Home Timeline"

    aput-object v4, v3, v5

    .line 81
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Highlights"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "Highlights"

    aput-object v4, v3, v5

    .line 82
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Navigation"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "Modern Android"

    aput-object v4, v3, v5

    .line 83
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Design Feedback"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "Modern Android"

    aput-object v4, v3, v5

    .line 84
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Periscope Integration"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "PSCP"

    .line 85
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a(Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Live Video"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "LV"

    .line 86
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a(Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "Android"

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    new-instance v2, Lcom/twitter/android/dogfood/BugReporterActivity$a;

    const-string/jumbo v3, "Crash/Other"

    invoke-direct {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "** New Bugs / Triage **"

    aput-object v4, v3, v5

    .line 87
    invoke-virtual {v2, v3}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/dogfood/BugReporterActivity;->a:[Lcom/twitter/android/dogfood/BugReporterActivity$a;

    .line 58
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    .line 92
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/dogfood/BugReporterActivity;->d:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/dogfood/BugReporterActivity;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/twitter/android/dogfood/BugReporterActivity;->i()V

    return-void
.end method

.method private i()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 160
    const-string/jumbo v0, "Your bug report has been generated. Make sure to use your @twitter.com email address when sending!"

    .line 162
    const-string/jumbo v0, "Your bug report has been generated. Make sure to use your @twitter.com email address when sending!"

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 164
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/dogfood/BugReporterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 165
    invoke-virtual {v0, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 168
    sget-object v1, Lcom/twitter/android/dogfood/BugReporterActivity;->a:[Lcom/twitter/android/dogfood/BugReporterActivity$a;

    iget v2, p0, Lcom/twitter/android/dogfood/BugReporterActivity;->d:I

    aget-object v1, v1, v2

    .line 169
    iget-object v2, p0, Lcom/twitter/android/dogfood/BugReporterActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 170
    const-string/jumbo v2, "%s #build=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-direct {p0}, Lcom/twitter/android/dogfood/BugReporterActivity;->j()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 172
    const-string/jumbo v2, "android.intent.extra.SUBJECT"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 174
    invoke-static {v0, v6}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x1f40

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/dogfood/BugReporterActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 175
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 3

    .prologue
    .line 181
    invoke-static {p0}, Lcom/twitter/util/d;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 182
    const/16 v1, 0x2d

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 183
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(Z)V

    .line 99
    const v0, 0x7f040052

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 100
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 101
    return-object p2
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 4

    .prologue
    .line 106
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V

    .line 107
    const-string/jumbo v0, "File a bug"

    invoke-virtual {p0, v0}, Lcom/twitter/android/dogfood/BugReporterActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 110
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 111
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "BugReporterActivity should not be created in release builds."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 115
    :cond_0
    const v0, 0x7f1301b6

    invoke-virtual {p0, v0}, Lcom/twitter/android/dogfood/BugReporterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 116
    const v1, 0x7f1301b5

    invoke-virtual {p0, v1}, Lcom/twitter/android/dogfood/BugReporterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/twitter/android/dogfood/BugReporterActivity;->c:Landroid/widget/EditText;

    .line 117
    const v1, 0x7f1301b7

    invoke-virtual {p0, v1}, Lcom/twitter/android/dogfood/BugReporterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/dogfood/BugReporterActivity;->b:Landroid/view/View;

    .line 118
    const v1, 0x7f1301b8

    invoke-virtual {p0, v1}, Lcom/twitter/android/dogfood/BugReporterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/twitter/android/dogfood/BugReporterActivity;->e:Landroid/widget/ImageView;

    .line 120
    iget-object v1, p0, Lcom/twitter/android/dogfood/BugReporterActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 122
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v2, 0x1090008

    sget-object v3, Lcom/twitter/android/dogfood/BugReporterActivity;->a:[Lcom/twitter/android/dogfood/BugReporterActivity$a;

    invoke-direct {v1, p0, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 124
    const v2, 0x1090009

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 125
    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 126
    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 127
    return-void
.end method

.method public a(Lcmm;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 188
    invoke-interface {p1}, Lcmm;->a()I

    move-result v1

    .line 189
    const v2, 0x7f1308d1

    if-ne v1, v2, :cond_1

    .line 190
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "android.permission.GET_ACCOUNTS"

    aput-object v4, v2, v3

    invoke-virtual {v1, p0, v2}, Lcom/twitter/util/android/f;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 191
    invoke-static {p0}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 192
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string/jumbo v2, "Twitter bug reporter"

    .line 193
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string/jumbo v2, "Setup your phone with your @twitter.com email address so we can follow up on your bug report."

    .line 194
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 196
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string/jumbo v2, "Skip"

    new-instance v3, Lcom/twitter/android/dogfood/BugReporterActivity$2;

    invoke-direct {v3, p0}, Lcom/twitter/android/dogfood/BugReporterActivity$2;-><init>(Lcom/twitter/android/dogfood/BugReporterActivity;)V

    .line 197
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string/jumbo v2, "Setup"

    new-instance v3, Lcom/twitter/android/dogfood/BugReporterActivity$1;

    invoke-direct {v3, p0}, Lcom/twitter/android/dogfood/BugReporterActivity$1;-><init>(Lcom/twitter/android/dogfood/BugReporterActivity;)V

    .line 203
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 209
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 210
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 217
    :goto_0
    return v0

    .line 212
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/dogfood/BugReporterActivity;->i()V

    goto :goto_0

    .line 217
    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmm;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 147
    const v0, 0x7f14002d

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 148
    const/4 v0, 0x1

    return v0
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .prologue
    .line 236
    invoke-virtual {p0}, Lcom/twitter/android/dogfood/BugReporterActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->h()V

    .line 237
    return-void
.end method

.method public b(Lcmr;)I
    .locals 2

    .prologue
    .line 154
    invoke-interface {p1}, Lcmr;->k()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 155
    const v1, 0x7f1308d1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/dogfood/BugReporterActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lazv;->c(Z)Lazv;

    .line 156
    const/4 v0, 0x2

    return v0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 229
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 222
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 223
    const/16 v0, 0x1f40

    if-ne p1, v0, :cond_0

    .line 224
    invoke-virtual {p0}, Lcom/twitter/android/dogfood/BugReporterActivity;->finish()V

    .line 226
    :cond_0
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 241
    iput p3, p0, Lcom/twitter/android/dogfood/BugReporterActivity;->d:I

    .line 242
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 245
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 131
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onResume()V

    .line 133
    invoke-virtual {p0}, Lcom/twitter/android/dogfood/BugReporterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 134
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 135
    iget-object v0, p0, Lcom/twitter/android/dogfood/BugReporterActivity;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 136
    iget-object v0, p0, Lcom/twitter/android/dogfood/BugReporterActivity;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 143
    :goto_0
    return-void

    .line 139
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/dogfood/BugReporterActivity;->e:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 140
    iget-object v1, p0, Lcom/twitter/android/dogfood/BugReporterActivity;->e:Landroid/widget/ImageView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    .line 141
    iget-object v1, p0, Lcom/twitter/android/dogfood/BugReporterActivity;->e:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 232
    return-void
.end method
