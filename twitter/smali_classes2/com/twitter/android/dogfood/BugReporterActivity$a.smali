.class public Lcom/twitter/android/dogfood/BugReporterActivity$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/dogfood/BugReporterActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:[Ljava/lang/String;

.field private d:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 262
    const-string/jumbo v0, "ANDROID"

    const/4 v1, 0x0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "fileanandroidbug-email"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "triage"

    aput-object v4, v2, v3

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/twitter/android/dogfood/BugReporterActivity$a;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 264
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 255
    iput-object p1, p0, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a:Ljava/lang/String;

    .line 256
    iput-object p2, p0, Lcom/twitter/android/dogfood/BugReporterActivity$a;->b:Ljava/lang/String;

    .line 257
    iput-object p3, p0, Lcom/twitter/android/dogfood/BugReporterActivity$a;->c:[Ljava/lang/String;

    .line 258
    iput-object p4, p0, Lcom/twitter/android/dogfood/BugReporterActivity$a;->d:[Ljava/lang/String;

    .line 259
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 291
    iget-object v0, p0, Lcom/twitter/android/dogfood/BugReporterActivity$a;->c:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 292
    const-string/jumbo v0, ""

    .line 294
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "#components=\"%s\""

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, ","

    iget-object v4, p0, Lcom/twitter/android/dogfood/BugReporterActivity$a;->c:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b()Ljava/lang/String;
    .locals 5

    .prologue
    .line 298
    iget-object v0, p0, Lcom/twitter/android/dogfood/BugReporterActivity$a;->d:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 299
    const-string/jumbo v0, ""

    .line 301
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "#labels=\"%s\""

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, ","

    iget-object v4, p0, Lcom/twitter/android/dogfood/BugReporterActivity$a;->d:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Lcom/twitter/android/dogfood/BugReporterActivity$a;->b:Ljava/lang/String;

    .line 268
    return-object p0
.end method

.method public varargs a([Ljava/lang/String;)Lcom/twitter/android/dogfood/BugReporterActivity$a;
    .locals 0

    .prologue
    .line 272
    iput-object p1, p0, Lcom/twitter/android/dogfood/BugReporterActivity$a;->c:[Ljava/lang/String;

    .line 273
    return-object p0
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 282
    const-string/jumbo v0, "%s in [%s] %s %s %s #issueType=\"Bug\""

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v6

    iget-object v2, p0, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a:Ljava/lang/String;

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string/jumbo v3, "#project=\"%s\""

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/twitter/android/dogfood/BugReporterActivity$a;->b:Ljava/lang/String;

    aput-object v5, v4, v6

    .line 285
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    .line 286
    invoke-direct {p0}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    .line 287
    invoke-direct {p0}, Lcom/twitter/android/dogfood/BugReporterActivity$a;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 282
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/twitter/android/dogfood/BugReporterActivity$a;->a:Ljava/lang/String;

    return-object v0
.end method
