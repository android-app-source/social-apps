.class Lcom/twitter/android/dogfood/a$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/dogfood/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lrx/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Z

.field final synthetic e:Landroid/graphics/Bitmap;

.field final synthetic f:Ljava/io/File;

.field final synthetic g:Lcom/twitter/android/dogfood/a;


# direct methods
.method constructor <init>(Lcom/twitter/android/dogfood/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/graphics/Bitmap;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lcom/twitter/android/dogfood/a$2;->g:Lcom/twitter/android/dogfood/a;

    iput-object p2, p0, Lcom/twitter/android/dogfood/a$2;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/twitter/android/dogfood/a$2;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/android/dogfood/a$2;->c:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/twitter/android/dogfood/a$2;->d:Z

    iput-object p6, p0, Lcom/twitter/android/dogfood/a$2;->e:Landroid/graphics/Bitmap;

    iput-object p7, p0, Lcom/twitter/android/dogfood/a$2;->f:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 302
    iget-object v0, p0, Lcom/twitter/android/dogfood/a$2;->g:Lcom/twitter/android/dogfood/a;

    iget-object v1, p0, Lcom/twitter/android/dogfood/a$2;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/dogfood/a$2;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/dogfood/a$2;->c:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/twitter/android/dogfood/a$2;->d:Z

    iget-object v5, p0, Lcom/twitter/android/dogfood/a$2;->e:Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/twitter/android/dogfood/a$2;->f:Ljava/io/File;

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/dogfood/a;->a(Lcom/twitter/android/dogfood/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/graphics/Bitmap;Ljava/io/File;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 299
    invoke-virtual {p0}, Lcom/twitter/android/dogfood/a$2;->a()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
