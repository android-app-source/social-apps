.class public Lcom/twitter/android/dogfood/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/android/b$a;
.implements Lcpc;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/dogfood/a$b;,
        Lcom/twitter/android/dogfood/a$a;
    }
.end annotation


# static fields
.field private static a:Lcom/twitter/android/dogfood/a;

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Ljava/lang/Long;",
            "Lcpb;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/twitter/android/dogfood/b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcog;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".fileprovider"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/dogfood/a;->b:Ljava/lang/String;

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcog;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".bug"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/dogfood/a;->c:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/dogfood/a;->d:Landroid/content/Context;

    .line 131
    new-instance v0, Landroid/support/v4/util/LruCache;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/support/v4/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/dogfood/a;->e:Landroid/support/v4/util/LruCache;

    .line 133
    invoke-static {}, Lcom/twitter/android/dogfood/a;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    new-instance v0, Lcom/twitter/android/dogfood/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/dogfood/a$a;-><init>(Lcom/twitter/android/dogfood/a$1;)V

    .line 135
    new-instance v1, Landroid/content/IntentFilter;

    sget-object v2, Lcom/twitter/android/dogfood/a;->c:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 136
    invoke-static {}, Lcom/twitter/util/android/b;->a()Lcom/twitter/util/android/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/util/android/b;->a(Lcom/twitter/util/android/b$a;)V

    .line 138
    :cond_0
    new-instance v0, Lcom/twitter/android/dogfood/b;

    invoke-direct {v0, p1}, Lcom/twitter/android/dogfood/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/dogfood/a;->f:Lcom/twitter/android/dogfood/b;

    .line 139
    return-void
.end method

.method public static a()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 143
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    sget-object v1, Lcom/twitter/android/dogfood/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/dogfood/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/graphics/Bitmap;Ljava/io/File;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 77
    invoke-direct/range {p0 .. p6}, Lcom/twitter/android/dogfood/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/graphics/Bitmap;Ljava/io/File;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/graphics/Bitmap;Ljava/io/File;)Landroid/content/Intent;
    .locals 14

    .prologue
    .line 311
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 313
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.SEND_MULTIPLE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "text/xml"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 314
    const-string/jumbo v2, "android.intent.extra.EMAIL"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v3, v5

    invoke-virtual {v4, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 315
    const-string/jumbo v2, "android.intent.extra.SUBJECT"

    move-object/from16 v0, p2

    invoke-virtual {v4, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 316
    const-string/jumbo v2, "android.intent.extra.TEXT"

    move-object/from16 v0, p3

    invoke-virtual {v4, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 318
    iget-object v2, p0, Lcom/twitter/android/dogfood/a;->d:Landroid/content/Context;

    invoke-static {v2}, Lcqc;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v5

    .line 319
    if-eqz p4, :cond_e

    if-eqz v5, :cond_e

    .line 321
    invoke-direct {p0}, Lcom/twitter/android/dogfood/a;->j()V

    .line 323
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 326
    if-eqz p5, :cond_1

    .line 328
    :try_start_0
    new-instance v2, Ljava/io/File;

    const-string/jumbo v3, "bug_report.jpg"

    invoke-direct {v2, v5, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 329
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v7, 0x46

    move-object/from16 v0, p5

    invoke-static {v0, v2, v3, v7}, Lcom/twitter/media/util/a;->a(Landroid/graphics/Bitmap;Ljava/io/File;Landroid/graphics/Bitmap$CompressFormat;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 330
    invoke-direct {p0, v2}, Lcom/twitter/android/dogfood/a;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 333
    :cond_0
    invoke-virtual/range {p5 .. p5}, Landroid/graphics/Bitmap;->recycle()V

    .line 338
    :cond_1
    if-eqz p6, :cond_3

    .line 339
    new-instance v2, Ljava/io/File;

    const-string/jumbo v3, "activity_state.txt"

    invoke-direct {v2, v5, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 340
    move-object/from16 v0, p6

    invoke-static {v0, v2}, Lcqc;->c(Ljava/io/File;Ljava/io/File;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 341
    invoke-direct {p0, v2}, Lcom/twitter/android/dogfood/a;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 343
    :cond_2
    invoke-static/range {p6 .. p6}, Lcqc;->d(Ljava/io/File;)V

    .line 347
    :cond_3
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v2

    invoke-virtual {v2}, Lcof;->p()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 349
    iget-object v2, p0, Lcom/twitter/android/dogfood/a;->d:Landroid/content/Context;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/twitter/library/network/narc/i;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v2

    .line 351
    if-eqz v2, :cond_4

    .line 352
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/twitter/android/dogfood/a;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 357
    :cond_4
    new-instance v7, Ljava/io/File;

    const-string/jumbo v2, "stack_traces.txt"

    invoke-direct {v7, v5, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 358
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 359
    iget-object v2, p0, Lcom/twitter/android/dogfood/a;->e:Landroid/support/v4/util/LruCache;

    invoke-virtual {v2}, Landroid/support/v4/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 360
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_6

    .line 361
    const-string/jumbo v3, "\n\n"

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 364
    :cond_6
    const-string/jumbo v3, "Time: "

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    new-instance v11, Ljava/util/Date;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-direct {v11, v12, v13}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v10, "\n"

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 365
    const-string/jumbo v3, "----------------------------------------\n"

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 367
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcpb;

    .line 368
    invoke-virtual {v2}, Lcpb;->c()Ljava/lang/Throwable;

    move-result-object v3

    invoke-static {v3}, Lcqj;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370
    invoke-virtual {v2}, Lcpb;->a()Ljava/util/Map;

    move-result-object v2

    .line 371
    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    .line 372
    const-string/jumbo v3, "----------------------------------------\n"

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 374
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v11, " : "

    .line 375
    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 376
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\n"

    .line 377
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 333
    :catchall_0
    move-exception v2

    invoke-virtual/range {p5 .. p5}, Landroid/graphics/Bitmap;->recycle()V

    throw v2

    .line 383
    :cond_7
    const/4 v2, 0x1

    invoke-static {v2}, Lcqi;->a(Z)Ljava/util/Collection;

    move-result-object v2

    .line 384
    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_8

    .line 385
    new-instance v3, Ljava/io/File;

    const-string/jumbo v9, "logcat.txt"

    invoke-direct {v3, v5, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 386
    const-string/jumbo v9, "\n"

    invoke-static {v9, v2}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v3}, Lcqc;->a(Ljava/lang/String;Ljava/io/File;)Z

    .line 387
    invoke-direct {p0, v3}, Lcom/twitter/android/dogfood/a;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 391
    :cond_8
    iget-object v2, p0, Lcom/twitter/android/dogfood/a;->d:Landroid/content/Context;

    invoke-static {v2}, Lcoq;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    .line 392
    if-eqz v2, :cond_a

    .line 393
    new-instance v3, Ljava/io/File;

    const-string/jumbo v9, "thread_dump.txt"

    invoke-direct {v3, v5, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 394
    invoke-static {v2, v3}, Lcqc;->c(Ljava/io/File;Ljava/io/File;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 395
    invoke-direct {p0, v3}, Lcom/twitter/android/dogfood/a;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 397
    :cond_9
    invoke-static {v2}, Lcqc;->d(Ljava/io/File;)V

    .line 401
    :cond_a
    iget-object v2, p0, Lcom/twitter/android/dogfood/a;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/android/settings/developer/d;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    .line 402
    if-eqz v2, :cond_c

    .line 403
    new-instance v3, Ljava/io/File;

    const-string/jumbo v9, "feature_switches.txt"

    invoke-direct {v3, v5, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 404
    invoke-static {v2, v3}, Lcqc;->c(Ljava/io/File;Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 405
    invoke-direct {p0, v3}, Lcom/twitter/android/dogfood/a;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 407
    :cond_b
    iget-object v2, p0, Lcom/twitter/android/dogfood/a;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/android/settings/developer/d;->b(Landroid/content/Context;)V

    .line 410
    :cond_c
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 411
    invoke-static {v2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-static {v2, v7}, Lcqc;->a(Ljava/lang/String;Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 412
    invoke-direct {p0, v7}, Lcom/twitter/android/dogfood/a;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 415
    :cond_d
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_e

    .line 416
    const-string/jumbo v2, "android.intent.extra.STREAM"

    invoke-virtual {v4, v2, v6}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 420
    :cond_e
    return-object v4
.end method

.method private a(Ljava/io/File;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 457
    iget-object v0, p0, Lcom/twitter/android/dogfood/a;->d:Landroid/content/Context;

    sget-object v1, Lcom/twitter/android/dogfood/a;->b:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Landroid/support/v4/content/FileProvider;->getUriForFile(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/android/dogfood/a;
    .locals 2

    .prologue
    .line 117
    const-class v1, Lcom/twitter/android/dogfood/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/android/dogfood/a;->a:Lcom/twitter/android/dogfood/a;

    if-nez v0, :cond_0

    .line 118
    new-instance v0, Lcom/twitter/android/dogfood/a;

    invoke-direct {v0, p0}, Lcom/twitter/android/dogfood/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/android/dogfood/a;->a:Lcom/twitter/android/dogfood/a;

    .line 119
    const-class v0, Lcom/twitter/android/dogfood/a;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 121
    :cond_0
    sget-object v0, Lcom/twitter/android/dogfood/a;->a:Lcom/twitter/android/dogfood/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/twitter/android/dogfood/a;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/twitter/android/dogfood/a;->j()V

    return-void
.end method

.method public static g()Z
    .locals 2

    .prologue
    .line 461
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    .line 462
    invoke-virtual {v0}, Lcof;->p()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcof;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h()Z
    .locals 2

    .prologue
    .line 466
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    .line 467
    invoke-virtual {v0}, Lcof;->p()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcof;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i()Z
    .locals 1

    .prologue
    .line 471
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->c()Z

    move-result v0

    return v0
.end method

.method private declared-synchronized j()V
    .locals 3

    .prologue
    .line 438
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 440
    iget-object v0, p0, Lcom/twitter/android/dogfood/a;->d:Landroid/content/Context;

    invoke-static {v0}, Lcqc;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 441
    if-eqz v0, :cond_0

    .line 443
    new-instance v1, Ljava/io/File;

    const-string/jumbo v2, "bug_reports"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 444
    invoke-static {v1}, Lcqc;->b(Ljava/io/File;)V

    .line 446
    new-instance v1, Ljava/io/File;

    const-string/jumbo v2, "bug_report.jpg"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v1}, Lcqc;->d(Ljava/io/File;)V

    .line 447
    new-instance v1, Ljava/io/File;

    const-string/jumbo v2, "stack_traces.txt"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v1}, Lcqc;->d(Ljava/io/File;)V

    .line 448
    new-instance v1, Ljava/io/File;

    const-string/jumbo v2, "activity_state.txt"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v1}, Lcqc;->d(Ljava/io/File;)V

    .line 449
    new-instance v1, Ljava/io/File;

    const-string/jumbo v2, "logcat.txt"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v1}, Lcqc;->d(Ljava/io/File;)V

    .line 450
    new-instance v1, Ljava/io/File;

    const-string/jumbo v2, "thread_dump.txt"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v1}, Lcqc;->d(Ljava/io/File;)V

    .line 451
    new-instance v1, Ljava/io/File;

    const-string/jumbo v2, "feature_switches.txt"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v1}, Lcqc;->d(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 453
    :cond_0
    monitor-exit p0

    return-void

    .line 438
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lrx/g;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "Lrx/g",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 280
    invoke-static {}, Lcom/twitter/app/common/util/b;->a()Lcom/twitter/app/common/util/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/util/b;->b()Landroid/app/Activity;

    move-result-object v0

    .line 283
    if-eqz p4, :cond_0

    if-eqz v0, :cond_0

    .line 285
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    .line 286
    invoke-static {v1}, Lcom/twitter/util/ui/k;->d(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 288
    new-instance v1, Lcom/twitter/android/dogfood/a$1;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/dogfood/a$1;-><init>(Lcom/twitter/android/dogfood/a;Landroid/app/Activity;)V

    invoke-static {v1}, Lcom/twitter/util/f;->a(Lcom/twitter/util/concurrent/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    move-object v7, v0

    .line 299
    :goto_0
    new-instance v0, Lcom/twitter/android/dogfood/a$2;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/dogfood/a$2;-><init>(Lcom/twitter/android/dogfood/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/graphics/Bitmap;Ljava/io/File;)V

    invoke-static {v0}, Lcre;->a(Ljava/util/concurrent/Callable;)Lrx/g;

    move-result-object v0

    .line 304
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/f;)Lrx/g;

    move-result-object v0

    .line 299
    return-object v0

    :cond_0
    move-object v6, v7

    .line 297
    goto :goto_0
.end method

.method public a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/twitter/android/dogfood/a;->f:Lcom/twitter/android/dogfood/b;

    invoke-virtual {v0}, Lcom/twitter/android/dogfood/b;->a()V

    .line 149
    return-void
.end method

.method public a(Lcpb;Lcpa$b;)V
    .locals 4

    .prologue
    .line 162
    iget-object v0, p0, Lcom/twitter/android/dogfood/a;->e:Landroid/support/v4/util/LruCache;

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 158
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 4

    .prologue
    .line 172
    iget-object v1, p0, Lcom/twitter/android/dogfood/a;->d:Landroid/content/Context;

    .line 173
    invoke-static {}, Lcom/twitter/app/common/util/b;->a()Lcom/twitter/app/common/util/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/util/b;->b()Landroid/app/Activity;

    move-result-object v0

    .line 174
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 175
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Reporting bug in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " with v"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 176
    invoke-static {v1}, Lcom/twitter/util/d;->d(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 175
    return-object v0

    .line 174
    :cond_0
    const-string/jumbo v0, "Unknown"

    goto :goto_0
.end method

.method public b(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/twitter/android/dogfood/a;->f:Lcom/twitter/android/dogfood/b;

    invoke-static {}, Lcom/twitter/android/dogfood/a;->a()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/dogfood/b;->a(Landroid/content/Intent;)V

    .line 154
    return-void
.end method

.method public b(Lcpb;Lcpa$b;)V
    .locals 0

    .prologue
    .line 167
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/dogfood/a;->a(Lcpb;Lcpa$b;)V

    .line 168
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 7

    .prologue
    .line 181
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 182
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    .line 183
    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 185
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 186
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 187
    const-string/jumbo v2, "\nuserId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 188
    const-string/jumbo v2, "\nusername: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    const-string/jumbo v2, "\nprotected: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, v0, Lcom/twitter/model/core/TwitterUser;->l:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 190
    const-string/jumbo v2, "\nsuspended: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, v0, Lcom/twitter/model/core/TwitterUser;->k:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 191
    const-string/jumbo v2, "\ncrash url: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/dogfood/a;->d:Landroid/content/Context;

    const v4, 0x7f0a0ba2

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 193
    :cond_0
    const-string/jumbo v0, "\nusername: <none -- logged out>"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 201
    iget-object v1, p0, Lcom/twitter/android/dogfood/a;->d:Landroid/content/Context;

    .line 202
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 204
    invoke-static {}, Lcom/twitter/app/common/util/b;->a()Lcom/twitter/app/common/util/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/util/b;->b()Landroid/app/Activity;

    move-result-object v0

    .line 205
    instance-of v3, v0, Lcom/twitter/android/dogfood/a$b;

    if-eqz v3, :cond_0

    .line 206
    check-cast v0, Lcom/twitter/android/dogfood/a$b;

    invoke-interface {v0}, Lcom/twitter/android/dogfood/a$b;->x()Ljava/lang/String;

    move-result-object v0

    .line 207
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 208
    const-string/jumbo v3, "\n\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    :cond_0
    const-string/jumbo v0, "\n\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    const-string/jumbo v0, "package: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    const-string/jumbo v0, "\nversion: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Lcom/twitter/util/d;->d(Landroid/content/Context;)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 217
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v0

    invoke-virtual {v0}, Lcrr;->g()Z

    move-result v0

    .line 218
    const-string/jumbo v3, "\nconnectivity: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 220
    if-eqz v0, :cond_1

    .line 221
    const-string/jumbo v0, "\nconnectivityType: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v3

    invoke-virtual {v3}, Lcrr;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/dogfood/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    invoke-static {v1}, Lcom/twitter/library/network/ab;->a(Landroid/content/Context;)Lcom/twitter/library/network/ab;

    move-result-object v0

    .line 227
    const-string/jumbo v1, "\n"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, v0, Lcom/twitter/library/network/ab;->c:Lcom/twitter/library/network/ae;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 229
    invoke-virtual {v0}, Lcom/twitter/library/network/ab;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 230
    invoke-virtual {v0}, Lcom/twitter/library/network/ab;->g()Ljava/util/List;

    move-result-object v0

    .line 232
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v3, 0x4

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 233
    if-lez v1, :cond_2

    .line 237
    const-string/jumbo v3, "\nRecent traces (only work if requests sent from Dodo):"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    invoke-interface {v0, v6, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 239
    const-string/jumbo v3, "http://go/zipkin/%1$s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 240
    const-string/jumbo v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 246
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 6

    .prologue
    .line 250
    iget-object v1, p0, Lcom/twitter/android/dogfood/a;->d:Landroid/content/Context;

    .line 251
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 252
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v0

    .line 253
    const-string/jumbo v3, "isLocationEnabled [geoTag, system, app]: ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 255
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v4

    .line 254
    invoke-virtual {v0, v4}, Lbqg;->b(Lcom/twitter/library/client/Session;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", "

    .line 256
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 257
    invoke-virtual {v0}, Lbqg;->f()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 258
    invoke-virtual {v0}, Lbqg;->e()Z

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    const-string/jumbo v0, "\nisGooglePlayServicesEnabled: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 260
    invoke-static {v1}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "geo_data_provider_google_play_services_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 263
    invoke-static {v1}, Lbqn;->a(Landroid/content/Context;)Lbqn;

    move-result-object v0

    invoke-virtual {v0}, Lbqn;->a()Landroid/location/Location;

    move-result-object v0

    .line 264
    if-eqz v0, :cond_1

    .line 265
    const-string/jumbo v1, "\nlocation [lat,lng]: ["

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 266
    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 267
    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "]"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    const-string/jumbo v1, "\nlocationAccuracy: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 269
    const-string/jumbo v1, "\nlocationProvider: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    :goto_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 260
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 271
    :cond_1
    const-string/jumbo v0, "\nlocation: null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public f()Lrx/a;
    .locals 1

    .prologue
    .line 425
    new-instance v0, Lcom/twitter/android/dogfood/a$3;

    invoke-direct {v0, p0}, Lcom/twitter/android/dogfood/a$3;-><init>(Lcom/twitter/android/dogfood/a;)V

    invoke-static {v0}, Lcre;->a(Lrx/functions/a;)Lrx/a;

    move-result-object v0

    return-object v0
.end method
