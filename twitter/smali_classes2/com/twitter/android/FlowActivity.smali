.class public Lcom/twitter/android/FlowActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/FlowPresenter$a;
.implements Lcom/twitter/android/PhoneEntryFragment$a;
.implements Lcom/twitter/android/ValidationState$a;
.implements Lcom/twitter/android/au;
.implements Lcom/twitter/android/bd;
.implements Lcom/twitter/android/by;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/FlowActivity$a;,
        Lcom/twitter/android/FlowActivity$b;
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/android/FlowPresenter;

.field private b:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

.field private c:Lcom/twitter/android/util/s;

.field private d:Lcom/twitter/ui/widget/TwitterButton;

.field private e:Lcom/twitter/ui/widget/TwitterButton;

.field private f:Lcom/twitter/android/util/l;

.field private g:Landroid/view/View;

.field private h:Landroid/view/View;

.field private i:Lcom/twitter/app/common/abs/AbsFragment;

.field private final j:Lcom/twitter/app/common/dialog/b$d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    .line 91
    new-instance v0, Lcom/twitter/android/FlowActivity$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/FlowActivity$1;-><init>(Lcom/twitter/android/FlowActivity;)V

    iput-object v0, p0, Lcom/twitter/android/FlowActivity;->j:Lcom/twitter/app/common/dialog/b$d;

    return-void
.end method

.method private B()V
    .locals 6

    .prologue
    .line 214
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v0

    .line 215
    invoke-virtual {v0}, Lcom/twitter/android/FlowData;->j()Lcom/twitter/android/FlowData$SignupState;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/FlowData$SignupState;->d:Lcom/twitter/android/FlowData$SignupState;

    if-ne v1, v2, :cond_0

    .line 216
    invoke-virtual {v0}, Lcom/twitter/android/FlowData;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 217
    invoke-virtual {v0}, Lcom/twitter/android/FlowData;->i()Z

    move-result v1

    if-nez v1, :cond_0

    .line 218
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 219
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v2

    .line 220
    new-instance v3, Lbbh;

    new-instance v4, Lcom/twitter/library/network/t;

    .line 221
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->h()Lcom/twitter/model/account/OAuthToken;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/twitter/library/network/t;-><init>(Lcom/twitter/model/account/OAuthToken;)V

    invoke-direct {v3, p0, v1, v4}, Lbbh;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/network/t;)V

    .line 222
    new-instance v1, Lcom/twitter/android/FlowActivity$b;

    invoke-direct {v1, v0}, Lcom/twitter/android/FlowActivity$b;-><init>(Lcom/twitter/android/FlowData;)V

    invoke-virtual {v2, v3, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 224
    :cond_0
    return-void
.end method

.method private C()Ljava/lang/String;
    .locals 1

    .prologue
    .line 489
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/FlowData;->h()Z

    move-result v0

    invoke-static {v0}, Lcom/twitter/android/bw;->a(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 583
    const v0, 0x7f0a08ba

    invoke-virtual {p0, v0}, Lcom/twitter/android/FlowActivity;->b(I)V

    .line 584
    invoke-static {}, Lcom/twitter/android/bw;->a()Ljava/lang/String;

    move-result-object v4

    .line 585
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v3

    .line 586
    invoke-virtual {v3, v4}, Lcom/twitter/android/FlowData;->d(Ljava/lang/String;)V

    .line 588
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    .line 589
    invoke-virtual {v3}, Lcom/twitter/android/FlowData;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const-string/jumbo v5, ""

    const-string/jumbo v6, ""

    new-instance v7, Lcom/twitter/android/bx;

    .line 595
    invoke-virtual {v3}, Lcom/twitter/android/FlowData;->h()Z

    move-result v3

    invoke-direct {v7, p0, v3}, Lcom/twitter/android/bx;-><init>(Lcom/twitter/android/FlowActivity;Z)V

    iget-object v3, p0, Lcom/twitter/android/FlowActivity;->f:Lcom/twitter/android/util/l;

    .line 597
    invoke-virtual {v3}, Lcom/twitter/android/util/l;->a()Ljava/lang/String;

    move-result-object v9

    move-object v3, p2

    move-object v8, p1

    .line 588
    invoke-virtual/range {v0 .. v9}, Lcom/twitter/library/client/v;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/v$h;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 598
    return-void
.end method

.method private b(Lcom/twitter/android/ValidationState;)V
    .locals 2

    .prologue
    .line 735
    invoke-static {p0}, Lcom/twitter/android/util/ac;->a(Landroid/content/Context;)Lcom/twitter/android/util/ab;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/util/ab;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/twitter/android/ValidationState;->c:Lcom/twitter/android/ValidationState$Level;

    sget-object v1, Lcom/twitter/android/ValidationState$Level;->a:Lcom/twitter/android/ValidationState$Level;

    if-ne v0, v1, :cond_1

    .line 737
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->d:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {p1}, Lcom/twitter/android/ValidationState;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setEnabled(Z)V

    .line 741
    :goto_0
    return-void

    .line 739
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->d:Lcom/twitter/ui/widget/TwitterButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method private i(Z)V
    .locals 2

    .prologue
    .line 301
    iget-object v1, p0, Lcom/twitter/android/FlowActivity;->e:Lcom/twitter/ui/widget/TwitterButton;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 302
    return-void

    .line 301
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public O_()V
    .locals 3

    .prologue
    .line 315
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    new-instance v1, Lcom/twitter/android/Flow$EmailSignupStep;

    invoke-direct {v1}, Lcom/twitter/android/Flow$EmailSignupStep;-><init>()V

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/FlowPresenter;->a(Lcom/twitter/android/Flow$Step;Lcom/twitter/android/w;)V

    .line 316
    return-void
.end method

.method public P_()V
    .locals 4

    .prologue
    .line 378
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 379
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->l()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "form"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-direct {p0}, Lcom/twitter/android/FlowActivity;->C()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const/4 v3, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "skip"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 378
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 381
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->e()V

    .line 382
    return-void
.end method

.method public Q_()Z
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/FlowData;->g()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 116
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 117
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(I)V

    .line 118
    const v0, 0x7f0400f9

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 119
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(Z)V

    .line 120
    return-object p2
.end method

.method public a(I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 288
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->e:Lcom/twitter/ui/widget/TwitterButton;

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->e:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/TwitterButton;->setText(I)V

    .line 290
    invoke-direct {p0, v1}, Lcom/twitter/android/FlowActivity;->i(Z)V

    .line 291
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0, p1, v1}, Lcom/twitter/android/FlowPresenter;->a(IZ)V

    .line 293
    :cond_0
    return-void
.end method

.method public final a(Lcom/twitter/android/Flow$Options;)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 254
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 255
    iget-boolean v0, p1, Lcom/twitter/android/Flow$Options;->e:Z

    if-nez v0, :cond_2

    .line 256
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 275
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->h:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 276
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->h:Landroid/view/View;

    iget-boolean v3, p1, Lcom/twitter/android/Flow$Options;->f:Z

    if-eqz v3, :cond_6

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 278
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->f()Lcom/twitter/android/ValidationState;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/FlowActivity;->b(Lcom/twitter/android/ValidationState;)V

    .line 279
    return-void

    .line 258
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 260
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->e:Lcom/twitter/ui/widget/TwitterButton;

    if-eqz v0, :cond_3

    .line 261
    iget-boolean v0, p1, Lcom/twitter/android/Flow$Options;->b:Z

    if-eqz v0, :cond_4

    .line 262
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->e:Lcom/twitter/ui/widget/TwitterButton;

    iget v3, p1, Lcom/twitter/android/Flow$Options;->c:I

    invoke-virtual {v0, v3}, Lcom/twitter/ui/widget/TwitterButton;->setText(I)V

    .line 263
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/FlowActivity;->i(Z)V

    .line 269
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->d:Lcom/twitter/ui/widget/TwitterButton;

    if-eqz v0, :cond_0

    .line 270
    iget-object v3, p0, Lcom/twitter/android/FlowActivity;->d:Lcom/twitter/ui/widget/TwitterButton;

    iget-boolean v0, p1, Lcom/twitter/android/Flow$Options;->a:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    goto :goto_0

    .line 265
    :cond_4
    invoke-direct {p0, v1}, Lcom/twitter/android/FlowActivity;->i(Z)V

    goto :goto_2

    :cond_5
    move v0, v2

    .line 270
    goto :goto_3

    :cond_6
    move v1, v2

    .line 276
    goto :goto_1
.end method

.method public a(Lcom/twitter/android/Flow$Step;Lcom/twitter/android/FlowPresenter$Direction;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 408
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 410
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->i:Lcom/twitter/app/common/abs/AbsFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->i:Lcom/twitter/app/common/abs/AbsFragment;

    invoke-virtual {v0}, Lcom/twitter/app/common/abs/AbsFragment;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/android/Flow$Step;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 411
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->i:Lcom/twitter/app/common/abs/AbsFragment;

    invoke-virtual {p1, v0}, Lcom/twitter/android/Flow$Step;->a(Lcom/twitter/app/common/abs/AbsFragment;)V

    .line 442
    :goto_0
    return-void

    .line 415
    :cond_0
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 417
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->i:Lcom/twitter/app/common/abs/AbsFragment;

    if-eqz v0, :cond_1

    .line 418
    sget-object v0, Lcom/twitter/android/FlowPresenter$Direction;->a:Lcom/twitter/android/FlowPresenter$Direction;

    if-ne p2, v0, :cond_2

    .line 419
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->i:Lcom/twitter/app/common/abs/AbsFragment;

    invoke-virtual {v3, v0}, Landroid/support/v4/app/FragmentTransaction;->detach(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 425
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/android/Flow$Step;->a(Landroid/support/v4/app/FragmentManager;)Lcom/twitter/app/common/abs/AbsFragment;

    move-result-object v0

    .line 427
    if-eqz v0, :cond_3

    .line 428
    invoke-virtual {v3, v0}, Landroid/support/v4/app/FragmentTransaction;->attach(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 435
    :goto_2
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 436
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 438
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/twitter/android/Flow$Step;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "form"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 439
    invoke-direct {p0}, Lcom/twitter/android/FlowActivity;->C()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const/4 v4, 0x0

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "impression"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    .line 438
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 441
    check-cast v0, Lcom/twitter/app/common/abs/AbsFragment;

    iput-object v0, p0, Lcom/twitter/android/FlowActivity;->i:Lcom/twitter/app/common/abs/AbsFragment;

    goto :goto_0

    .line 421
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->i:Lcom/twitter/app/common/abs/AbsFragment;

    invoke-virtual {v3, v0}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_1

    .line 430
    :cond_3
    new-instance v0, Lcom/twitter/app/common/base/b$a;

    invoke-direct {v0, p3}, Lcom/twitter/app/common/base/b$a;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {p1, v0}, Lcom/twitter/android/Flow$Step;->a(Lcom/twitter/app/common/base/b$a;)Lcom/twitter/app/common/abs/AbsFragment;

    move-result-object v1

    .line 431
    const v0, 0x7f1302e4

    invoke-virtual {p1}, Lcom/twitter/android/Flow$Step;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v1, v4}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-object v0, v1

    .line 432
    check-cast v0, Lcom/twitter/app/common/abs/AbsFragment;

    invoke-virtual {p1, v0}, Lcom/twitter/android/Flow$Step;->a(Lcom/twitter/app/common/abs/AbsFragment;)V

    move-object v0, v1

    goto :goto_2
.end method

.method public a(Lcom/twitter/android/FlowData$SignupState;)V
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/FlowData;->a(Lcom/twitter/android/FlowData$SignupState;)V

    .line 476
    return-void
.end method

.method public a(Lcom/twitter/android/ValidationState;)V
    .locals 1

    .prologue
    .line 724
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0, p1}, Lcom/twitter/android/FlowPresenter;->a(Lcom/twitter/android/ValidationState;)V

    .line 725
    invoke-direct {p0, p1}, Lcom/twitter/android/FlowActivity;->b(Lcom/twitter/android/ValidationState;)V

    .line 726
    return-void
.end method

.method public a(Lcom/twitter/android/w;)V
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0, p1}, Lcom/twitter/android/FlowPresenter;->a(Lcom/twitter/android/w;)V

    .line 451
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/FlowData;->b(Ljava/lang/String;)V

    .line 332
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 528
    if-nez p2, :cond_0

    .line 529
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/FlowData;->f(Ljava/lang/String;)V

    .line 530
    invoke-static {p0}, Lcom/twitter/android/client/u;->a(Landroid/content/Context;)Lcom/twitter/android/client/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/u;->a()V

    .line 531
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    sget-object v1, Lcom/twitter/android/FlowPresenter$Direction;->a:Lcom/twitter/android/FlowPresenter$Direction;

    invoke-interface {v0, v1}, Lcom/twitter/android/FlowPresenter;->b(Lcom/twitter/android/FlowPresenter$Direction;)V

    .line 533
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 284
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    .line 307
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 308
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->l()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "form"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-direct {p0}, Lcom/twitter/android/FlowActivity;->C()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const/4 v3, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "submit"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 307
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 310
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->d()V

    .line 311
    return-void
.end method

.method public b(I)V
    .locals 3

    .prologue
    .line 621
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->b:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    if-nez v0, :cond_0

    .line 622
    invoke-static {p1}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->a(I)Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/FlowActivity;->b:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    .line 623
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->b:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->setRetainInstance(Z)V

    .line 624
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->b:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 626
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 125
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 127
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    if-nez v0, :cond_0

    .line 128
    new-instance v0, Lcom/twitter/android/x;

    invoke-direct {v0, p0}, Lcom/twitter/android/x;-><init>(Lcom/twitter/android/FlowPresenter$a;)V

    iput-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    .line 131
    :cond_0
    new-instance v0, Lcom/twitter/android/util/l;

    invoke-direct {v0, p0}, Lcom/twitter/android/util/l;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/FlowActivity;->f:Lcom/twitter/android/util/l;

    .line 132
    const-string/jumbo v0, "signup_js_instrumentation_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 133
    const v0, 0x7f130438

    invoke-virtual {p0, v0}, Lcom/twitter/android/FlowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 134
    iget-object v1, p0, Lcom/twitter/android/FlowActivity;->f:Lcom/twitter/android/util/l;

    invoke-virtual {v1, v0, p1}, Lcom/twitter/android/util/l;->a(Landroid/webkit/WebView;Landroid/os/Bundle;)V

    .line 137
    :cond_1
    const v0, 0x7f13038c

    invoke-virtual {p0, v0}, Lcom/twitter/android/FlowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/FlowActivity;->g:Landroid/view/View;

    .line 138
    const v0, 0x7f1301b3

    invoke-virtual {p0, v0}, Lcom/twitter/android/FlowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/FlowActivity;->h:Landroid/view/View;

    .line 140
    if-eqz p1, :cond_3

    .line 141
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 142
    iget-object v1, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v1, p1}, Lcom/twitter/android/FlowPresenter;->b(Landroid/os/Bundle;)V

    .line 143
    const v1, 0x7f1302e4

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/AbsFragment;

    iput-object v0, p0, Lcom/twitter/android/FlowActivity;->i:Lcom/twitter/app/common/abs/AbsFragment;

    .line 172
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->g:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 173
    const v0, 0x7f1301ad

    invoke-virtual {p0, v0}, Lcom/twitter/android/FlowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/FlowActivity;->d:Lcom/twitter/ui/widget/TwitterButton;

    .line 174
    const v0, 0x7f1301a7

    invoke-virtual {p0, v0}, Lcom/twitter/android/FlowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/FlowActivity;->e:Lcom/twitter/ui/widget/TwitterButton;

    .line 175
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->e:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 176
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->d:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    :cond_2
    invoke-static {p0}, Lcom/twitter/android/util/t;->a(Landroid/content/Context;)Lcom/twitter/android/util/s;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/FlowActivity;->c:Lcom/twitter/android/util/s;

    .line 180
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->c:Lcom/twitter/android/util/s;

    invoke-interface {v0}, Lcom/twitter/android/util/s;->i()V

    .line 181
    invoke-static {p0}, Lcom/twitter/android/bw;->a(Landroid/app/Activity;)V

    .line 182
    return-void

    .line 145
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 146
    const-string/jumbo v1, "phone100_signup_first_step_password"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 147
    const-string/jumbo v2, "phone100_signup_first_step_add_phone"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 151
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 152
    const-string/jumbo v4, "flow_data"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/FlowData;

    .line 156
    if-eqz v0, :cond_4

    .line 162
    :goto_1
    if-eqz v1, :cond_5

    .line 163
    new-instance v1, Lcom/twitter/android/Flow$PasswordEntryStep;

    invoke-direct {v1}, Lcom/twitter/android/Flow$PasswordEntryStep;-><init>()V

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    :goto_2
    iget-object v1, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v1, v3, v0}, Lcom/twitter/android/FlowPresenter;->a(Ljava/util/List;Lcom/twitter/android/FlowData;)V

    goto :goto_0

    .line 159
    :cond_4
    new-instance v0, Lcom/twitter/android/FlowData;

    invoke-direct {v0}, Lcom/twitter/android/FlowData;-><init>()V

    goto :goto_1

    .line 164
    :cond_5
    if-eqz v2, :cond_6

    .line 165
    new-instance v1, Lcom/twitter/android/Flow$AddPhoneStep;

    invoke-direct {v1}, Lcom/twitter/android/Flow$AddPhoneStep;-><init>()V

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 167
    :cond_6
    new-instance v1, Lcom/twitter/android/Flow$NameEntryStep;

    invoke-direct {v1}, Lcom/twitter/android/Flow$NameEntryStep;-><init>()V

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public b(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 365
    const v0, 0x7f0a0687

    invoke-virtual {p0, v0}, Lcom/twitter/android/FlowActivity;->b(I)V

    .line 366
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v0

    .line 367
    new-instance v1, Lbfh;

    .line 368
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v0}, Lcom/twitter/android/FlowData;->b()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4, p1}, Lbfh;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v2

    new-instance v3, Lcom/twitter/android/bx;

    .line 370
    invoke-virtual {v0}, Lcom/twitter/android/FlowData;->h()Z

    move-result v0

    invoke-direct {v3, p0, v0}, Lcom/twitter/android/bx;-><init>(Lcom/twitter/android/FlowActivity;Z)V

    .line 369
    invoke-virtual {v2, v1, v5, v3}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;ILcom/twitter/library/client/s;)Z

    .line 372
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 373
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->l()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "phone_verification"

    aput-object v3, v1, v2

    invoke-direct {p0}, Lcom/twitter/android/FlowActivity;->C()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    const/4 v2, 0x3

    const-string/jumbo v3, "complete"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "attempt"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 372
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 374
    return-void
.end method

.method public b(Z)V
    .locals 4

    .prologue
    .line 322
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 323
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->l()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "form"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-direct {p0}, Lcom/twitter/android/FlowActivity;->C()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "settings"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 322
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 325
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/AdvancedDiscoSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "extra_is_signup_process"

    .line 326
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 325
    invoke-virtual {p0, v0}, Lcom/twitter/android/FlowActivity;->startActivity(Landroid/content/Intent;)V

    .line 327
    return-void
.end method

.method public bl_()V
    .locals 2

    .prologue
    .line 386
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    sget-object v1, Lcom/twitter/android/FlowPresenter$Direction;->a:Lcom/twitter/android/FlowPresenter$Direction;

    invoke-interface {v0, v1}, Lcom/twitter/android/FlowPresenter;->b(Lcom/twitter/android/FlowPresenter$Direction;)V

    .line 387
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/FlowData;->a(Ljava/lang/String;)V

    .line 456
    return-void
.end method

.method public c(Z)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 340
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v2

    .line 342
    invoke-virtual {v2, v1}, Lcom/twitter/android/FlowData;->f(Ljava/lang/String;)V

    .line 344
    invoke-virtual {v2}, Lcom/twitter/android/FlowData;->b()Ljava/lang/String;

    move-result-object v0

    .line 345
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 346
    const v1, 0x7f0a08ce

    invoke-virtual {p0, v1}, Lcom/twitter/android/FlowActivity;->b(I)V

    .line 347
    new-instance v1, Lbfg;

    .line 348
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-direct {v1, v3, v4, v0}, Lbfg;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    .line 350
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v3

    if-eqz p1, :cond_0

    const/4 v0, 0x3

    :goto_0
    new-instance v4, Lcom/twitter/android/bx;

    .line 352
    invoke-virtual {v2}, Lcom/twitter/android/FlowData;->h()Z

    move-result v2

    invoke-direct {v4, p0, v2}, Lcom/twitter/android/bx;-><init>(Lcom/twitter/android/FlowActivity;Z)V

    .line 350
    invoke-virtual {v3, v1, v0, v4}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;ILcom/twitter/library/client/s;)Z

    .line 359
    :goto_1
    return-void

    .line 350
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 354
    :cond_1
    new-instance v0, Lcom/twitter/android/w;

    const v2, 0x7f0a08c3

    .line 355
    invoke-virtual {p0, v2}, Lcom/twitter/android/FlowActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v2, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/w;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    iget-object v1, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v1, v0}, Lcom/twitter/android/FlowPresenter;->a(Lcom/twitter/android/w;)V

    goto :goto_1
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 524
    return-void
.end method

.method public d(Z)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 637
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v2

    .line 638
    invoke-virtual {v2}, Lcom/twitter/android/FlowData;->b()Ljava/lang/String;

    move-result-object v0

    .line 639
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 640
    const v1, 0x7f0a08ce

    invoke-virtual {p0, v1}, Lcom/twitter/android/FlowActivity;->b(I)V

    .line 642
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 641
    invoke-static {p0, v1, v0}, Lbci;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)Lbci;

    move-result-object v1

    .line 644
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->c:Lcom/twitter/android/util/s;

    invoke-interface {v0}, Lcom/twitter/android/util/s;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 645
    invoke-virtual {v1}, Lbci;->g()Lbci;

    .line 648
    :cond_0
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v3

    if-eqz p1, :cond_1

    const/4 v0, 0x5

    :goto_0
    new-instance v4, Lcom/twitter/android/bx;

    .line 650
    invoke-virtual {v2}, Lcom/twitter/android/FlowData;->h()Z

    move-result v2

    invoke-direct {v4, p0, v2}, Lcom/twitter/android/bx;-><init>(Lcom/twitter/android/FlowActivity;Z)V

    .line 648
    invoke-virtual {v3, v1, v0, v4}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;ILcom/twitter/library/client/s;)Z

    .line 655
    :goto_1
    return-void

    .line 648
    :cond_1
    const/4 v0, 0x4

    goto :goto_0

    .line 652
    :cond_2
    new-instance v0, Lcom/twitter/android/w;

    const v2, 0x7f0a08c3

    invoke-virtual {p0, v2}, Lcom/twitter/android/FlowActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v2, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/w;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    iget-object v1, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v1, v0}, Lcom/twitter/android/FlowPresenter;->a(Lcom/twitter/android/w;)V

    goto :goto_1
.end method

.method public e()Lcom/twitter/android/ValidationState;
    .locals 1

    .prologue
    .line 731
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->f()Lcom/twitter/android/ValidationState;

    move-result-object v0

    return-object v0
.end method

.method public e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/FlowData;->c(Ljava/lang/String;)V

    .line 551
    return-void
.end method

.method public e(Z)V
    .locals 1

    .prologue
    .line 704
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/FlowData;->d(Z)V

    .line 705
    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 660
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v0

    .line 661
    invoke-virtual {v0}, Lcom/twitter/android/FlowData;->b()Ljava/lang/String;

    move-result-object v1

    .line 662
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 663
    const v2, 0x7f0a08ce

    invoke-virtual {p0, v2}, Lcom/twitter/android/FlowActivity;->b(I)V

    .line 665
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-static {p0, v2, v1, p1, v5}, Lbcj;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Z)Lbcj;

    move-result-object v1

    .line 667
    iget-object v2, p0, Lcom/twitter/android/FlowActivity;->c:Lcom/twitter/android/util/s;

    invoke-interface {v2}, Lcom/twitter/android/util/s;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 668
    invoke-virtual {v1}, Lbcj;->g()Lbcj;

    .line 671
    :cond_0
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v2

    const/4 v3, 0x6

    new-instance v4, Lcom/twitter/android/bx;

    .line 672
    invoke-virtual {v0}, Lcom/twitter/android/FlowData;->h()Z

    move-result v0

    invoke-direct {v4, p0, v0}, Lcom/twitter/android/bx;-><init>(Lcom/twitter/android/FlowActivity;Z)V

    .line 671
    invoke-virtual {v2, v1, v3, v4}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;ILcom/twitter/library/client/s;)Z

    .line 675
    :cond_1
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    .line 676
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->l()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    const/4 v2, 0x1

    const-string/jumbo v3, "phone_verification"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-direct {p0}, Lcom/twitter/android/FlowActivity;->C()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "complete"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "attempt"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 675
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 677
    return-void
.end method

.method public f(Z)V
    .locals 1

    .prologue
    .line 709
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/FlowData;->e(Z)V

    .line 710
    return-void
.end method

.method public g(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 699
    const/4 v0, -0x1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v2, "AbsFragmentActivity_account_name"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/FlowActivity;->setResult(ILandroid/content/Intent;)V

    .line 700
    return-void
.end method

.method public g(Z)V
    .locals 1

    .prologue
    .line 714
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0, p1}, Lcom/twitter/android/FlowPresenter;->a(Z)V

    .line 715
    return-void
.end method

.method public h(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 719
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/FlowData;->d(Ljava/lang/String;)V

    .line 720
    return-void
.end method

.method public i()V
    .locals 0

    .prologue
    .line 446
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->finish()V

    .line 447
    return-void
.end method

.method public i(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 754
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/FlowData;->e(Ljava/lang/String;)V

    .line 755
    return-void
.end method

.method public j()V
    .locals 2

    .prologue
    .line 460
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/FlowData;->k()Z

    move-result v0

    .line 461
    iget-object v1, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v1}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/FlowData;->l()Z

    move-result v1

    .line 462
    if-eqz v0, :cond_1

    .line 463
    const-string/jumbo v0, "welcome"

    invoke-static {p0, v0, v1}, Lcom/twitter/android/ContactsUploadService;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 465
    invoke-static {p0}, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/FlowActivity;->startActivity(Landroid/content/Intent;)V

    .line 472
    :goto_0
    return-void

    .line 467
    :cond_0
    invoke-static {p0}, Lcom/twitter/android/SmartNuxContactsUploadHelperActivity;->a(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/FlowActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 470
    :cond_1
    invoke-static {p0}, Lcom/twitter/android/smartfollow/SmartFollowFlowActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/FlowActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 484
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()V
    .locals 0

    .prologue
    .line 495
    return-void
.end method

.method public n()V
    .locals 0

    .prologue
    .line 500
    return-void
.end method

.method public n_()Z
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->d:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterButton;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public o()V
    .locals 4

    .prologue
    .line 505
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/FlowData;->f(Ljava/lang/String;)V

    .line 506
    invoke-static {p0}, Lcom/twitter/android/client/u;->a(Landroid/content/Context;)Lcom/twitter/android/client/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/u;->a()V

    .line 507
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 508
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->l()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "phone_verification"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-direct {p0}, Lcom/twitter/android/FlowActivity;->C()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "manual_entry"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 507
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 509
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    sget-object v1, Lcom/twitter/android/FlowPresenter$Direction;->a:Lcom/twitter/android/FlowPresenter$Direction;

    invoke-interface {v0, v1}, Lcom/twitter/android/FlowPresenter;->b(Lcom/twitter/android/FlowPresenter$Direction;)V

    .line 510
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 760
    if-ne p1, v4, :cond_1

    .line 761
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 762
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v0

    .line 763
    invoke-virtual {v0, v4}, Lcom/twitter/android/FlowData;->f(Z)V

    .line 764
    invoke-virtual {v0, p0}, Lcom/twitter/android/FlowData;->a(Landroid/content/Context;)V

    .line 765
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "signup"

    aput-object v3, v1, v2

    const-string/jumbo v2, "phone100"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string/jumbo v3, "sspc"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const/4 v3, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "finish"

    aput-object v3, v1, v2

    .line 766
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 765
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 771
    :cond_0
    :goto_0
    return-void

    .line 769
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    .line 392
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 393
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->l()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "form"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-direct {p0}, Lcom/twitter/android/FlowActivity;->C()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const/4 v3, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "back"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 392
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 395
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->a()V

    .line 396
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 201
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 202
    const v1, 0x7f1301ad

    if-ne v0, v1, :cond_1

    .line 203
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->b()V

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 204
    :cond_1
    const v1, 0x7f1301a7

    if-ne v0, v1, :cond_0

    .line 205
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->P_()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 194
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 195
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0, p1}, Lcom/twitter/android/FlowPresenter;->a(Landroid/os/Bundle;)V

    .line 196
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->f:Lcom/twitter/android/util/l;

    invoke-virtual {v0, p1}, Lcom/twitter/android/util/l;->a(Landroid/os/Bundle;)V

    .line 197
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 186
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onStart()V

    .line 188
    invoke-direct {p0}, Lcom/twitter/android/FlowActivity;->B()V

    .line 189
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    sget-object v1, Lcom/twitter/android/FlowPresenter$Direction;->a:Lcom/twitter/android/FlowPresenter$Direction;

    invoke-interface {v0, v1}, Lcom/twitter/android/FlowPresenter;->a(Lcom/twitter/android/FlowPresenter$Direction;)V

    .line 190
    return-void
.end method

.method public p()V
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->a()V

    .line 401
    return-void
.end method

.method public q()V
    .locals 4

    .prologue
    .line 516
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 517
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->l()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "phone_verification"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-direct {p0}, Lcom/twitter/android/FlowActivity;->C()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "resend"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 516
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 519
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->x()V

    .line 520
    return-void
.end method

.method public r()Ljava/lang/String;
    .locals 2

    .prologue
    .line 537
    invoke-static {p0}, Lcom/twitter/android/util/t;->a(Landroid/content/Context;)Lcom/twitter/android/util/s;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v1}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/FlowData;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/util/s;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public s()V
    .locals 4

    .prologue
    .line 543
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 544
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->l()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "form"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-direct {p0}, Lcom/twitter/android/FlowActivity;->C()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "use_phone_instead"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 543
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 545
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    new-instance v1, Lcom/twitter/android/Flow$PhoneSignupStep;

    invoke-direct {v1}, Lcom/twitter/android/Flow$PhoneSignupStep;-><init>()V

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/FlowPresenter;->a(Lcom/twitter/android/Flow$Step;Lcom/twitter/android/w;)V

    .line 546
    return-void
.end method

.method public t()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 555
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v7

    .line 556
    invoke-virtual {v7}, Lcom/twitter/android/FlowData;->d()Ljava/lang/String;

    move-result-object v0

    .line 558
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 559
    const/4 v2, 0x1

    invoke-virtual {v7, v2}, Lcom/twitter/android/FlowData;->b(Z)V

    .line 560
    invoke-direct {p0, v1, v0}, Lcom/twitter/android/FlowActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    :goto_0
    return-void

    .line 562
    :cond_0
    new-instance v0, Lcom/twitter/android/w;

    const v2, 0x7f0a08bd

    invoke-virtual {p0, v2}, Lcom/twitter/android/FlowActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/w;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    invoke-virtual {v7, v1}, Lcom/twitter/android/FlowData;->c(Ljava/lang/String;)V

    .line 564
    iget-object v1, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v1, v0}, Lcom/twitter/android/FlowPresenter;->a(Lcom/twitter/android/w;)V

    goto :goto_0
.end method

.method public u()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 569
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v7

    .line 570
    invoke-virtual {v7}, Lcom/twitter/android/FlowData;->b()Ljava/lang/String;

    move-result-object v0

    .line 572
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 573
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Lcom/twitter/android/FlowData;->b(Z)V

    .line 574
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/FlowActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    :goto_0
    return-void

    .line 576
    :cond_0
    new-instance v0, Lcom/twitter/android/w;

    const v2, 0x7f0a08c3

    invoke-virtual {p0, v2}, Lcom/twitter/android/FlowActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v2, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/w;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    invoke-virtual {v7, v1}, Lcom/twitter/android/FlowData;->b(Ljava/lang/String;)V

    .line 578
    iget-object v1, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v1, v0}, Lcom/twitter/android/FlowPresenter;->a(Lcom/twitter/android/w;)V

    goto :goto_0
.end method

.method protected v()V
    .locals 4

    .prologue
    .line 601
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v0

    .line 602
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/LoginActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "password"

    .line 603
    invoke-virtual {v0}, Lcom/twitter/android/FlowData;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 605
    invoke-virtual {v0}, Lcom/twitter/android/FlowData;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 606
    const-string/jumbo v2, "screen_name"

    invoke-virtual {v0}, Lcom/twitter/android/FlowData;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 611
    :goto_0
    invoke-virtual {p0, v1}, Lcom/twitter/android/FlowActivity;->startActivity(Landroid/content/Intent;)V

    .line 612
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->finish()V

    .line 613
    return-void

    .line 608
    :cond_0
    const-string/jumbo v2, "screen_name"

    invoke-virtual {v0}, Lcom/twitter/android/FlowData;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public w()V
    .locals 1

    .prologue
    .line 629
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->b:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    if-eqz v0, :cond_0

    .line 630
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->b:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->dismissAllowingStateLoss()V

    .line 631
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/FlowActivity;->b:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    .line 633
    :cond_0
    return-void
.end method

.method public x()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 680
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    invoke-direct {v0, v2}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a067d

    .line 682
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0b0013

    .line 683
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->c(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 684
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/FlowActivity;->j:Lcom/twitter/app/common/dialog/b$d;

    .line 685
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PromptDialogFragment;

    .line 686
    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->setRetainInstance(Z)V

    .line 687
    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 688
    return-void
.end method

.method public y()V
    .locals 1

    .prologue
    .line 692
    iget-object v0, p0, Lcom/twitter/android/FlowActivity;->a:Lcom/twitter/android/FlowPresenter;

    invoke-interface {v0}, Lcom/twitter/android/FlowPresenter;->b()Lcom/twitter/android/FlowData;

    move-result-object v0

    .line 693
    invoke-virtual {v0, p0}, Lcom/twitter/android/FlowData;->b(Landroid/content/Context;)V

    .line 694
    invoke-virtual {v0, p0}, Lcom/twitter/android/FlowData;->a(Landroid/content/Context;)V

    .line 695
    return-void
.end method

.method public z()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 745
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/dialog/PhoneVerificationDialogFragmentActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "extra_is_blocking"

    .line 746
    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_forward_result"

    .line 747
    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 748
    invoke-virtual {p0, v0, v4}, Lcom/twitter/android/FlowActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 749
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/FlowActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "signup"

    aput-object v3, v1, v2

    const-string/jumbo v2, "phone100"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string/jumbo v3, "sspc"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const/4 v3, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "begin"

    aput-object v3, v1, v2

    .line 750
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 749
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 751
    return-void
.end method
