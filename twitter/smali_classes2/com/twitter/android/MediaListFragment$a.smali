.class public Lcom/twitter/android/MediaListFragment$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/av;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/MediaListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/android/av",
        "<",
        "Landroid/view/View;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/MediaListFragment;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/twitter/android/MediaListFragment;)V
    .locals 1

    .prologue
    .line 400
    iput-object p1, p0, Lcom/twitter/android/MediaListFragment$a;->a:Lcom/twitter/android/MediaListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 401
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MediaListFragment$a;->b:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 405
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/MediaListFragment$c;

    iget-object v0, v0, Lcom/twitter/android/MediaListFragment$c;->a:Lcom/twitter/android/MediaListFragment$b;

    .line 406
    iget-object v1, p0, Lcom/twitter/android/MediaListFragment$a;->a:Lcom/twitter/android/MediaListFragment;

    invoke-static {v1}, Lcom/twitter/android/MediaListFragment;->a(Lcom/twitter/android/MediaListFragment;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0}, Lcom/twitter/android/MediaListFragment$b;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 407
    invoke-interface {v0}, Lcom/twitter/android/MediaListFragment$b;->d()Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v1

    .line 408
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/twitter/android/MediaListFragment$a;->b:Ljava/util/Set;

    iget-object v3, v1, Lcgi;->c:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 409
    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->a:Lcom/twitter/library/api/PromotedEvent;

    invoke-static {v2, v1}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v1

    invoke-virtual {v1}, Lbsq$a;->a()Lbsq;

    move-result-object v1

    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 411
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/MediaListFragment$a;->a:Lcom/twitter/android/MediaListFragment;

    invoke-virtual {v1}, Lcom/twitter/android/MediaListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 412
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0}, Lcom/twitter/android/MediaListFragment$b;->d()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    const/4 v2, 0x0

    .line 411
    invoke-static {v1, v0, v2}, Lcom/twitter/library/scribe/b;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 413
    iget-object v1, p0, Lcom/twitter/android/MediaListFragment$a;->a:Lcom/twitter/android/MediaListFragment;

    invoke-static {v1}, Lcom/twitter/android/MediaListFragment;->b(Lcom/twitter/android/MediaListFragment;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 415
    :cond_1
    instance-of v0, p1, Lcom/twitter/media/ui/image/a;

    if-eqz v0, :cond_2

    .line 416
    iget-object v0, p0, Lcom/twitter/android/MediaListFragment$a;->a:Lcom/twitter/android/MediaListFragment;

    invoke-static {v0}, Lcom/twitter/android/MediaListFragment;->c(Lcom/twitter/android/MediaListFragment;)Lcom/twitter/media/ui/image/b;

    move-result-object v0

    check-cast p1, Lcom/twitter/media/ui/image/a;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/twitter/media/ui/image/b;->a(Lcom/twitter/media/ui/image/a;Z)Z

    .line 418
    :cond_2
    return-void
.end method
