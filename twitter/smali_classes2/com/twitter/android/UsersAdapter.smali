.class public Lcom/twitter/android/UsersAdapter;
.super Lbab;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/UsersAdapter$CheckboxConfig;,
        Lcom/twitter/android/UsersAdapter$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbab",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private a:J

.field private final b:Lcom/twitter/ui/user/BaseUserView$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserView;",
            ">;"
        }
    .end annotation
.end field

.field protected c:Z

.field private final d:Lcom/twitter/model/util/FriendshipCache;

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/view/animation/Animation;

.field private final g:Lcom/twitter/android/UsersAdapter$a;

.field private final h:Lcom/twitter/library/client/v;

.field private final i:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field private j:Lcom/twitter/android/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/av",
            "<",
            "Lcom/twitter/ui/user/BaseUserView;",
            "Lcgi;",
            ">;"
        }
    .end annotation
.end field

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private final o:Lcom/twitter/android/UsersAdapter$CheckboxConfig;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/android/UsersAdapter$CheckboxConfig;)V
    .locals 2
    .param p2    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserView;",
            ">;",
            "Lcom/twitter/model/util/FriendshipCache;",
            "Lcom/twitter/android/UsersAdapter$CheckboxConfig;",
            ")V"
        }
    .end annotation

    .prologue
    .line 99
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lbab;-><init>(Landroid/content/Context;I)V

    .line 100
    iput p2, p0, Lcom/twitter/android/UsersAdapter;->i:I

    .line 101
    iput-object p3, p0, Lcom/twitter/android/UsersAdapter;->b:Lcom/twitter/ui/user/BaseUserView$a;

    .line 102
    iput-object p4, p0, Lcom/twitter/android/UsersAdapter;->d:Lcom/twitter/model/util/FriendshipCache;

    .line 103
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/UsersAdapter;->h:Lcom/twitter/library/client/v;

    .line 104
    iget-object v0, p0, Lcom/twitter/android/UsersAdapter;->h:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/UsersAdapter;->a:J

    .line 105
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/UsersAdapter;->k:Z

    .line 106
    iput-object p5, p0, Lcom/twitter/android/UsersAdapter;->o:Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/UsersAdapter;->c:Z

    .line 109
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/UsersAdapter;->e:Ljava/util/Map;

    .line 110
    const v0, 0x7f05002f

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/UsersAdapter;->f:Landroid/view/animation/Animation;

    .line 111
    new-instance v0, Lcom/twitter/android/UsersAdapter$a;

    invoke-direct {v0, p0}, Lcom/twitter/android/UsersAdapter$a;-><init>(Lcom/twitter/android/UsersAdapter;)V

    iput-object v0, p0, Lcom/twitter/android/UsersAdapter;->g:Lcom/twitter/android/UsersAdapter$a;

    .line 112
    return-void
.end method

.method private a(Landroid/view/View;JJ)V
    .locals 6

    .prologue
    .line 360
    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    .line 362
    iget-object v0, p0, Lcom/twitter/android/UsersAdapter;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 363
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v1, p4, v4

    if-nez v1, :cond_0

    .line 364
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 365
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v1, p2, v4

    if-eqz v1, :cond_0

    .line 366
    iget-object v1, p0, Lcom/twitter/android/UsersAdapter;->e:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 367
    iget-object v0, p0, Lcom/twitter/android/UsersAdapter;->f:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 372
    :cond_1
    return-void
.end method

.method private a(Lcom/twitter/ui/user/BaseUserView;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/v;Lcgi;JLjava/lang/String;IZZII)V
    .locals 4

    .prologue
    .line 194
    invoke-virtual {p1, p2, p3}, Lcom/twitter/ui/user/BaseUserView;->setUserId(J)V

    .line 195
    invoke-virtual {p1, p5, p6}, Lcom/twitter/ui/user/BaseUserView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    move/from16 v0, p14

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/BaseUserView;->setProtected(Z)V

    .line 197
    move/from16 v0, p15

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/BaseUserView;->setVerified(Z)V

    .line 198
    invoke-virtual {p1, p4}, Lcom/twitter/ui/user/BaseUserView;->setUserImageUrl(Ljava/lang/String;)V

    .line 200
    invoke-virtual {p1, p7, p8}, Lcom/twitter/ui/user/BaseUserView;->a(Ljava/lang/String;Lcom/twitter/model/core/v;)V

    .line 201
    iget-boolean v1, p0, Lcom/twitter/android/UsersAdapter;->c:Z

    invoke-virtual {p1, v1}, Lcom/twitter/ui/user/BaseUserView;->a(Z)V

    .line 203
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v1

    invoke-virtual {p1, p9, v1}, Lcom/twitter/ui/user/BaseUserView;->a(Lcgi;Z)V

    .line 205
    invoke-virtual {p1}, Lcom/twitter/ui/user/BaseUserView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/db;

    .line 206
    if-eqz p6, :cond_1

    :goto_0
    iput-object p6, v1, Lcom/twitter/android/db;->h:Ljava/lang/String;

    .line 207
    iput-wide p10, v1, Lcom/twitter/android/db;->d:J

    .line 208
    move-object/from16 v0, p12

    iput-object v0, v1, Lcom/twitter/android/db;->g:Ljava/lang/String;

    .line 209
    iget-object v2, p0, Lcom/twitter/android/UsersAdapter;->j:Lcom/twitter/android/av;

    if-eqz v2, :cond_0

    .line 210
    new-instance v2, Landroid/os/Bundle;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 211
    const-string/jumbo v3, "position"

    move/from16 v0, p16

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 212
    iget-object v3, p0, Lcom/twitter/android/UsersAdapter;->j:Lcom/twitter/android/av;

    invoke-interface {v3, p1, p9, v2}, Lcom/twitter/android/av;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 214
    :cond_0
    move/from16 v0, p13

    iput v0, v1, Lcom/twitter/android/db;->f:I

    .line 215
    move/from16 v0, p17

    iput v0, v1, Lcom/twitter/android/db;->i:I

    .line 216
    iput-wide p2, v1, Lcom/twitter/android/db;->e:J

    .line 217
    return-void

    :cond_1
    move-object p6, p5

    .line 206
    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 131
    invoke-virtual {p0, p1, p3}, Lcom/twitter/android/UsersAdapter;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/twitter/ui/user/UserView;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 64
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/UsersAdapter;->a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method a(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/twitter/ui/user/UserView;
    .locals 3

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/twitter/android/UsersAdapter;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f040063

    .line 137
    :goto_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/user/UserView;

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersAdapter;->a(Lcom/twitter/ui/user/UserView;)Lcom/twitter/ui/user/UserView;

    move-result-object v0

    return-object v0

    .line 136
    :cond_0
    const v0, 0x7f04042d

    goto :goto_0
.end method

.method public a(Landroid/view/View;)Lcom/twitter/ui/user/UserView;
    .locals 0

    .prologue
    .line 294
    check-cast p1, Lcom/twitter/ui/user/UserView;

    return-object p1
.end method

.method protected a(Lcom/twitter/ui/user/UserView;)Lcom/twitter/ui/user/UserView;
    .locals 2

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/twitter/android/UsersAdapter;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 143
    invoke-virtual {p0}, Lcom/twitter/android/UsersAdapter;->b()Lcom/twitter/ui/user/BaseUserView$a;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserView;->setCheckBoxClickListener(Lcom/twitter/ui/user/BaseUserView$a;)V

    .line 152
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/UsersAdapter;->b()Lcom/twitter/ui/user/BaseUserView$a;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserView;->setProfileClickListener(Lcom/twitter/ui/user/BaseUserView$a;)V

    .line 153
    invoke-virtual {p0}, Lcom/twitter/android/UsersAdapter;->b()Lcom/twitter/ui/user/BaseUserView$a;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserView;->setBlockButtonClickListener(Lcom/twitter/ui/user/BaseUserView$a;)V

    .line 154
    new-instance v0, Lcom/twitter/android/db;

    invoke-direct {v0, p1}, Lcom/twitter/android/db;-><init>(Lcom/twitter/ui/user/BaseUserView;)V

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserView;->setTag(Ljava/lang/Object;)V

    .line 156
    invoke-virtual {p0}, Lcom/twitter/android/UsersAdapter;->j()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a03a5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/twitter/ui/user/UserView;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    return-object p1

    .line 144
    :cond_1
    iget v0, p0, Lcom/twitter/android/UsersAdapter;->i:I

    if-lez v0, :cond_0

    .line 145
    iget v0, p0, Lcom/twitter/android/UsersAdapter;->i:I

    iget-object v1, p0, Lcom/twitter/android/UsersAdapter;->g:Lcom/twitter/android/UsersAdapter$a;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/ui/user/UserView;->a(ILcom/twitter/ui/user/BaseUserView$a;)V

    .line 146
    const v0, 0x7f0200b0

    iget v1, p0, Lcom/twitter/android/UsersAdapter;->i:I

    if-ne v0, v1, :cond_2

    .line 147
    const v0, 0x7f0200b1

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserView;->setFollowBackgroundResource(I)V

    goto :goto_0

    .line 148
    :cond_2
    const v0, 0x7f0200b5

    iget v1, p0, Lcom/twitter/android/UsersAdapter;->i:I

    if-ne v0, v1, :cond_0

    .line 149
    const v0, 0x7f0200b6

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserView;->setFollowBackgroundResource(I)V

    goto :goto_0
.end method

.method public a(J)Ljava/lang/Long;
    .locals 3

    .prologue
    .line 339
    iget-object v0, p0, Lcom/twitter/android/UsersAdapter;->e:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public a(JJ)V
    .locals 3

    .prologue
    .line 334
    iget-object v0, p0, Lcom/twitter/android/UsersAdapter;->e:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    return-void
.end method

.method protected a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 221
    return-void
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;I)V
    .locals 11

    .prologue
    .line 225
    const/4 v0, 0x2

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 226
    const/4 v0, 0x0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    .line 227
    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/UsersAdapter;->a(Landroid/view/View;JJ)V

    .line 229
    invoke-virtual {p0, p1}, Lcom/twitter/android/UsersAdapter;->a(Landroid/view/View;)Lcom/twitter/ui/user/UserView;

    move-result-object v6

    .line 230
    invoke-virtual {v6}, Lcom/twitter/ui/user/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/db;

    move-object v5, p0

    move-object v7, p3

    move-wide v8, v2

    move v10, p4

    .line 232
    invoke-virtual/range {v5 .. v10}, Lcom/twitter/android/UsersAdapter;->a(Lcom/twitter/ui/user/BaseUserView;Landroid/database/Cursor;JI)V

    .line 234
    invoke-virtual {p0}, Lcom/twitter/android/UsersAdapter;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 236
    iget-object v1, p0, Lcom/twitter/android/UsersAdapter;->o:Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    iget-object v1, v1, Lcom/twitter/android/UsersAdapter$CheckboxConfig;->b:Ljava/util/List;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 237
    iget-object v1, p0, Lcom/twitter/android/UsersAdapter;->o:Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    iget-boolean v1, v1, Lcom/twitter/android/UsersAdapter$CheckboxConfig;->a:Z

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    move v4, v1

    .line 241
    :goto_1
    iget-object v1, v6, Lcom/twitter/ui/user/UserView;->s:Landroid/widget/CheckBox;

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 243
    iget-object v1, p0, Lcom/twitter/android/UsersAdapter;->o:Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    iget-object v1, v1, Lcom/twitter/android/UsersAdapter$CheckboxConfig;->c:Ljava/util/List;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 244
    iget-object v1, v6, Lcom/twitter/ui/user/UserView;->s:Landroid/widget/CheckBox;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 248
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/UsersAdapter;->c()Lcom/twitter/model/util/FriendshipCache;

    move-result-object v1

    .line 249
    if-eqz v1, :cond_1

    invoke-virtual {v1, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->a(J)Z

    move-result v4

    if-nez v4, :cond_1

    .line 250
    iget v0, v0, Lcom/twitter/android/db;->f:I

    invoke-virtual {v1, v2, v3, v0}, Lcom/twitter/model/util/FriendshipCache;->b(JI)V

    .line 253
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/UsersAdapter;->n:Z

    if-eqz v0, :cond_6

    .line 254
    const/16 v0, 0x8

    invoke-virtual {v6, v0}, Lcom/twitter/ui/user/UserView;->setFollowVisibility(I)V

    .line 255
    iget-object v4, v6, Lcom/twitter/ui/user/UserView;->r:Lcom/twitter/ui/widget/ActionButton;

    if-eqz v1, :cond_5

    invoke-virtual {v1, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->l(J)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v4, v0}, Lcom/twitter/ui/widget/ActionButton;->setChecked(Z)V

    .line 256
    iget-object v0, v6, Lcom/twitter/ui/user/UserView;->r:Lcom/twitter/ui/widget/ActionButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/ActionButton;->setVisibility(I)V

    .line 279
    :cond_2
    :goto_3
    return-void

    .line 237
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 239
    :cond_4
    iget-object v1, p0, Lcom/twitter/android/UsersAdapter;->o:Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    iget-boolean v1, v1, Lcom/twitter/android/UsersAdapter$CheckboxConfig;->a:Z

    move v4, v1

    goto :goto_1

    .line 255
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 257
    :cond_6
    iget v0, p0, Lcom/twitter/android/UsersAdapter;->i:I

    if-lez v0, :cond_2

    .line 258
    iget-boolean v0, p0, Lcom/twitter/android/UsersAdapter;->k:Z

    if-nez v0, :cond_7

    invoke-virtual {p0}, Lcom/twitter/android/UsersAdapter;->d()J

    move-result-wide v4

    cmp-long v0, v4, v2

    if-nez v0, :cond_7

    .line 259
    const/4 v0, 0x4

    invoke-virtual {v6, v0}, Lcom/twitter/ui/user/UserView;->setFollowVisibility(I)V

    goto :goto_3

    .line 261
    :cond_7
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Lcom/twitter/ui/user/UserView;->setFollowVisibility(I)V

    .line 262
    if-eqz v1, :cond_2

    .line 263
    invoke-virtual {v1, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->l(J)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 264
    const/16 v0, 0x8

    invoke-virtual {v6, v0}, Lcom/twitter/ui/user/UserView;->setFollowVisibility(I)V

    .line 265
    iget-object v0, v6, Lcom/twitter/ui/user/UserView;->r:Lcom/twitter/ui/widget/ActionButton;

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/twitter/ui/widget/ActionButton;->setChecked(Z)V

    .line 266
    iget-object v0, v6, Lcom/twitter/ui/user/UserView;->r:Lcom/twitter/ui/widget/ActionButton;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/twitter/ui/widget/ActionButton;->setVisibility(I)V

    .line 271
    :goto_4
    iget-boolean v0, p0, Lcom/twitter/android/UsersAdapter;->l:Z

    if-eqz v0, :cond_2

    .line 273
    invoke-virtual {v1, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->j(J)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/util/y;->a(Ljava/lang/Integer;)Z

    move-result v0

    iget-boolean v1, p0, Lcom/twitter/android/UsersAdapter;->m:Z

    invoke-virtual {v6, v0, v1}, Lcom/twitter/ui/user/UserView;->a(ZZ)V

    .line 274
    invoke-virtual {p0}, Lcom/twitter/android/UsersAdapter;->b()Lcom/twitter/ui/user/BaseUserView$a;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/twitter/ui/user/UserView;->setMutedViewClickListener(Lcom/twitter/ui/user/BaseUserView$a;)V

    goto :goto_3

    .line 268
    :cond_8
    iget-object v0, v6, Lcom/twitter/ui/user/UserView;->r:Lcom/twitter/ui/widget/ActionButton;

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Lcom/twitter/ui/widget/ActionButton;->setVisibility(I)V

    .line 269
    invoke-virtual {v1, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->k(J)Z

    move-result v0

    invoke-virtual {v6, v0}, Lcom/twitter/ui/user/UserView;->setIsFollowing(Z)V

    goto :goto_4
.end method

.method protected bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 64
    check-cast p3, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/UsersAdapter;->a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 64
    check-cast p3, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/android/UsersAdapter;->a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;I)V

    return-void
.end method

.method public a(Lcom/twitter/android/av;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/av",
            "<",
            "Lcom/twitter/ui/user/BaseUserView;",
            "Lcgi;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 298
    iput-object p1, p0, Lcom/twitter/android/UsersAdapter;->j:Lcom/twitter/android/av;

    .line 299
    return-void
.end method

.method protected a(Lcom/twitter/ui/user/BaseUserView;Landroid/database/Cursor;JI)V
    .locals 21

    .prologue
    .line 163
    const-string/jumbo v2, "user_metadata_token"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 164
    const-string/jumbo v3, "user_groups_type"

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 165
    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 166
    :goto_0
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 167
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 172
    :goto_1
    const/4 v2, 0x6

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 173
    const/4 v3, 0x5

    .line 174
    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v3, 0x4

    .line 175
    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v3, 0x3

    .line 176
    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/16 v3, 0x8

    .line 177
    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/16 v3, 0x9

    .line 178
    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/model/core/v;->a([B)Lcom/twitter/model/core/v;

    move-result-object v10

    const/16 v3, 0xa

    .line 179
    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    sget-object v4, Lcgi;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v3, v4}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcgi;

    const/4 v3, 0x0

    .line 181
    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-long v12, v3

    const/4 v3, 0x7

    .line 183
    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    and-int/lit8 v3, v2, 0x1

    if-eqz v3, :cond_2

    const/16 v16, 0x1

    :goto_2
    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_3

    const/16 v17, 0x1

    :goto_3
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-wide/from16 v4, p3

    move/from16 v18, p5

    .line 173
    invoke-direct/range {v2 .. v19}, Lcom/twitter/android/UsersAdapter;->a(Lcom/twitter/ui/user/BaseUserView;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/v;Lcgi;JLjava/lang/String;IZZII)V

    .line 188
    return-void

    .line 165
    :cond_0
    const/16 v19, 0x0

    goto :goto_0

    .line 169
    :cond_1
    const/4 v14, 0x0

    goto :goto_1

    .line 183
    :cond_2
    const/16 v16, 0x0

    goto :goto_2

    :cond_3
    const/16 v17, 0x0

    goto :goto_3
.end method

.method public a(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 375
    new-instance v5, Lcom/twitter/library/provider/ParcelableMatrixCursor;

    sget-object v0, Lbun;->a:[Ljava/lang/String;

    invoke-direct {v5, v0}, Lcom/twitter/library/provider/ParcelableMatrixCursor;-><init>([Ljava/lang/String;)V

    .line 377
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 378
    invoke-virtual {v5}, Lcom/twitter/library/provider/ParcelableMatrixCursor;->a()Lcom/twitter/library/provider/ParcelableMatrixCursor$a;

    move-result-object v7

    .line 379
    add-int/lit8 v3, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/twitter/library/provider/ParcelableMatrixCursor$a;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/ParcelableMatrixCursor$a;

    .line 380
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/twitter/library/provider/ParcelableMatrixCursor$a;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/ParcelableMatrixCursor$a;

    .line 381
    iget-wide v8, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/twitter/library/provider/ParcelableMatrixCursor$a;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/ParcelableMatrixCursor$a;

    .line 382
    iget-object v1, v0, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    invoke-virtual {v7, v1}, Lcom/twitter/library/provider/ParcelableMatrixCursor$a;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/ParcelableMatrixCursor$a;

    .line 383
    iget-object v1, v0, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    invoke-virtual {v7, v1}, Lcom/twitter/library/provider/ParcelableMatrixCursor$a;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/ParcelableMatrixCursor$a;

    .line 384
    iget-object v1, v0, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    invoke-virtual {v7, v1}, Lcom/twitter/library/provider/ParcelableMatrixCursor$a;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/ParcelableMatrixCursor$a;

    .line 385
    invoke-static {v0}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/model/core/TwitterUser;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/twitter/library/provider/ParcelableMatrixCursor$a;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/ParcelableMatrixCursor$a;

    .line 386
    iget v1, v0, Lcom/twitter/model/core/TwitterUser;->U:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/twitter/library/provider/ParcelableMatrixCursor$a;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/ParcelableMatrixCursor$a;

    .line 387
    iget-object v1, v0, Lcom/twitter/model/core/TwitterUser;->f:Ljava/lang/String;

    invoke-virtual {v7, v1}, Lcom/twitter/library/provider/ParcelableMatrixCursor$a;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/ParcelableMatrixCursor$a;

    .line 388
    iget-object v1, v0, Lcom/twitter/model/core/TwitterUser;->C:Lcom/twitter/model/core/v;

    if-nez v1, :cond_0

    move-object v1, v4

    :goto_1
    invoke-virtual {v7, v1}, Lcom/twitter/library/provider/ParcelableMatrixCursor$a;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/ParcelableMatrixCursor$a;

    .line 390
    iget-object v1, v0, Lcom/twitter/model/core/TwitterUser;->A:Lcgi;

    if-nez v1, :cond_1

    move-object v0, v4

    :goto_2
    invoke-virtual {v7, v0}, Lcom/twitter/library/provider/ParcelableMatrixCursor$a;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/ParcelableMatrixCursor$a;

    .line 391
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/twitter/library/provider/ParcelableMatrixCursor$a;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/ParcelableMatrixCursor$a;

    move v1, v3

    .line 392
    goto :goto_0

    .line 388
    :cond_0
    iget-object v1, v0, Lcom/twitter/model/core/TwitterUser;->C:Lcom/twitter/model/core/v;

    sget-object v8, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    .line 389
    invoke-static {v1, v8}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v1

    goto :goto_1

    .line 390
    :cond_1
    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->A:Lcgi;

    invoke-virtual {v0}, Lcgi;->g()[B

    move-result-object v0

    goto :goto_2

    .line 394
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/UsersAdapter;->k()Lcjt;

    move-result-object v0

    new-instance v1, Lcbe;

    invoke-direct {v1, v5}, Lcbe;-><init>(Landroid/database/Cursor;)V

    invoke-interface {v0, v1}, Lcjt;->a(Lcbi;)Lcbi;

    .line 395
    return-void
.end method

.method public a(ZZ)V
    .locals 0

    .prologue
    .line 310
    iput-boolean p1, p0, Lcom/twitter/android/UsersAdapter;->l:Z

    .line 311
    iput-boolean p2, p0, Lcom/twitter/android/UsersAdapter;->m:Z

    .line 312
    return-void
.end method

.method public b()Lcom/twitter/ui/user/BaseUserView$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Lcom/twitter/android/UsersAdapter;->b:Lcom/twitter/ui/user/BaseUserView$a;

    return-object v0
.end method

.method public b(J)Ljava/lang/Long;
    .locals 3

    .prologue
    .line 344
    iget-object v0, p0, Lcom/twitter/android/UsersAdapter;->e:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public c()Lcom/twitter/model/util/FriendshipCache;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/twitter/android/UsersAdapter;->d:Lcom/twitter/model/util/FriendshipCache;

    return-object v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 125
    iget-wide v0, p0, Lcom/twitter/android/UsersAdapter;->a:J

    return-wide v0
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 302
    iput-boolean p1, p0, Lcom/twitter/android/UsersAdapter;->k:Z

    .line 303
    return-void
.end method

.method public e(Z)V
    .locals 0

    .prologue
    .line 306
    iput-boolean p1, p0, Lcom/twitter/android/UsersAdapter;->c:Z

    .line 307
    return-void
.end method

.method protected e()Z
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/twitter/android/UsersAdapter;->o:Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/twitter/android/UsersAdapter;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 349
    return-void
.end method

.method public f(Z)V
    .locals 0

    .prologue
    .line 319
    iput-boolean p1, p0, Lcom/twitter/android/UsersAdapter;->n:Z

    .line 320
    return-void
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 283
    const/4 v0, 0x2

    return v0
.end method

.method public notifyDataSetChanged()V
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/twitter/android/UsersAdapter;->h:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/UsersAdapter;->a:J

    .line 289
    invoke-super {p0}, Lbab;->notifyDataSetChanged()V

    .line 290
    return-void
.end method
