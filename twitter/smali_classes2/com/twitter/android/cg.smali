.class public Lcom/twitter/android/cg;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/InlineDismissView$a;


# instance fields
.field private final a:Lcom/twitter/util/object/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/j",
            "<",
            "Lcom/twitter/android/ab;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/library/client/v;

.field private final c:Lcom/twitter/library/client/p;

.field private final d:Landroid/content/Context;

.field private final e:Lbwm;

.field private final f:Lcom/twitter/android/ck;

.field private final g:Lcom/twitter/model/util/FriendshipCache;

.field private final h:Lajb;


# direct methods
.method public constructor <init>(Lcom/twitter/util/object/j;Lcom/twitter/library/client/v;Lcom/twitter/library/client/p;Landroid/content/Context;Lbwm;Lcom/twitter/android/ck;Lcom/twitter/model/util/FriendshipCache;Lajb;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/object/j",
            "<",
            "Lcom/twitter/android/ab;",
            ">;",
            "Lcom/twitter/library/client/v;",
            "Lcom/twitter/library/client/p;",
            "Landroid/content/Context;",
            "Lbwm;",
            "Lcom/twitter/android/ck;",
            "Lcom/twitter/model/util/FriendshipCache;",
            "Lajb;",
            ")V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/twitter/android/cg;->a:Lcom/twitter/util/object/j;

    .line 47
    iput-object p2, p0, Lcom/twitter/android/cg;->b:Lcom/twitter/library/client/v;

    .line 48
    iput-object p3, p0, Lcom/twitter/android/cg;->c:Lcom/twitter/library/client/p;

    .line 49
    invoke-virtual {p4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/cg;->d:Landroid/content/Context;

    .line 50
    iput-object p5, p0, Lcom/twitter/android/cg;->e:Lbwm;

    .line 51
    iput-object p6, p0, Lcom/twitter/android/cg;->f:Lcom/twitter/android/ck;

    .line 52
    iput-object p7, p0, Lcom/twitter/android/cg;->g:Lcom/twitter/model/util/FriendshipCache;

    .line 53
    iput-object p8, p0, Lcom/twitter/android/cg;->h:Lajb;

    .line 54
    return-void
.end method

.method private a(Lcom/twitter/android/timeline/bk;Lcom/twitter/model/timeline/g;Z)V
    .locals 2

    .prologue
    .line 94
    invoke-virtual {p0, p2, p1, p3}, Lcom/twitter/android/cg;->a(Lcom/twitter/model/timeline/g;Lcom/twitter/android/timeline/bk;Z)Lcom/twitter/library/service/s;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/twitter/android/cg;->c:Lcom/twitter/library/client/p;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 97
    return-void
.end method


# virtual methods
.method a(Lcom/twitter/model/timeline/g;Lcom/twitter/android/timeline/bk;Z)Lcom/twitter/library/service/s;
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/twitter/android/cg;->h:Lajb;

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p2, p1, v1}, Lajb;->a(Lcom/twitter/android/timeline/bk;Lcom/twitter/model/timeline/g;Ljava/lang/Boolean;)Lcom/twitter/library/service/s;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/model/timeline/g;)V
    .locals 4

    .prologue
    .line 60
    const v0, 0x7f13007e

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/InlineDismissView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk;

    .line 61
    const/4 v1, 0x0

    invoke-direct {p0, v0, p2, v1}, Lcom/twitter/android/cg;->a(Lcom/twitter/android/timeline/bk;Lcom/twitter/model/timeline/g;Z)V

    .line 62
    iget-object v1, p0, Lcom/twitter/android/cg;->e:Lbwm;

    iget-object v2, p0, Lcom/twitter/android/cg;->d:Landroid/content/Context;

    invoke-virtual {v0, v2}, Lcom/twitter/android/timeline/bk;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    .line 63
    invoke-virtual {v0}, Lcom/twitter/android/timeline/bk;->i()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "click"

    .line 62
    invoke-virtual {v1, v2, v0, p2, v3}, Lbwm;->a(Ljava/util/List;Ljava/lang/String;Lcom/twitter/model/timeline/g;Ljava/lang/String;)V

    .line 64
    return-void
.end method

.method public b(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/model/timeline/g;)V
    .locals 4

    .prologue
    .line 70
    const v0, 0x7f13007e

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/InlineDismissView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk;

    .line 71
    const/4 v1, 0x1

    invoke-direct {p0, v0, p2, v1}, Lcom/twitter/android/cg;->a(Lcom/twitter/android/timeline/bk;Lcom/twitter/model/timeline/g;Z)V

    .line 72
    iget-object v1, p0, Lcom/twitter/android/cg;->e:Lbwm;

    iget-object v2, p0, Lcom/twitter/android/cg;->d:Landroid/content/Context;

    invoke-virtual {v0, v2}, Lcom/twitter/android/timeline/bk;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    .line 73
    invoke-virtual {v0}, Lcom/twitter/android/timeline/bk;->i()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "undo"

    .line 72
    invoke-virtual {v1, v2, v0, p2, v3}, Lbwm;->a(Ljava/util/List;Ljava/lang/String;Lcom/twitter/model/timeline/g;Ljava/lang/String;)V

    .line 74
    return-void
.end method

.method public c(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/model/timeline/g;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 79
    const v0, 0x7f13007e

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/InlineDismissView;->getTag(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/android/timeline/bk;

    .line 80
    instance-of v0, v6, Lcom/twitter/android/timeline/k;

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {v6}, Lcom/twitter/android/timeline/bk;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v0

    iget v0, v0, Lcom/twitter/android/timeline/bg;->r:I

    if-ne v0, v8, :cond_0

    .line 82
    iget-object v0, p0, Lcom/twitter/android/cg;->f:Lcom/twitter/android/ck;

    sget-object v1, Lcom/twitter/model/core/TweetActionType;->f:Lcom/twitter/model/core/TweetActionType;

    move-object v2, v6

    check-cast v2, Lcom/twitter/android/timeline/k;

    .line 83
    invoke-interface {v2}, Lcom/twitter/android/timeline/k;->d()Lcom/twitter/model/core/Tweet;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/cg;->g:Lcom/twitter/model/util/FriendshipCache;

    move-object v5, v4

    move-object v7, v4

    .line 82
    invoke-virtual/range {v0 .. v7}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/TweetActionType;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/library/widget/h;Lcom/twitter/android/timeline/bk;Ljava/lang/String;)V

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/cg;->a:Lcom/twitter/util/object/j;

    invoke-interface {v0}, Lcom/twitter/util/object/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ab;

    invoke-interface {v0, p1, v6}, Lcom/twitter/android/ab;->b(Lcom/twitter/library/widget/InlineDismissView;Lcom/twitter/android/timeline/bk;)V

    .line 87
    invoke-direct {p0, v6, p2, v8}, Lcom/twitter/android/cg;->a(Lcom/twitter/android/timeline/bk;Lcom/twitter/model/timeline/g;Z)V

    .line 88
    iget-object v0, p0, Lcom/twitter/android/cg;->e:Lbwm;

    iget-object v1, p0, Lcom/twitter/android/cg;->d:Landroid/content/Context;

    invoke-virtual {v6, v1}, Lcom/twitter/android/timeline/bk;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 89
    invoke-virtual {v6}, Lcom/twitter/android/timeline/bk;->i()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "undo"

    .line 88
    invoke-virtual {v0, v1, v2, p2, v3}, Lbwm;->a(Ljava/util/List;Ljava/lang/String;Lcom/twitter/model/timeline/g;Ljava/lang/String;)V

    .line 90
    return-void
.end method
