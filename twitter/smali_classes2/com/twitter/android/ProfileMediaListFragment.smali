.class public Lcom/twitter/android/ProfileMediaListFragment;
.super Lcom/twitter/android/widget/ScrollingHeaderMediaListFragment;
.source "Twttr"


# instance fields
.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/twitter/android/widget/ScrollingHeaderMediaListFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 34
    invoke-super {p0, p1, p2}, Lcom/twitter/android/widget/ScrollingHeaderMediaListFragment;->a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 35
    if-eqz v1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/ProfileMediaListFragment;->c:Z

    if-eqz v0, :cond_0

    .line 36
    const v0, 0x7f130030

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    .line 37
    const v2, 0x7f0a0374

    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(I)V

    .line 39
    :cond_0
    return-object v1
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 2

    .prologue
    .line 44
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ScrollingHeaderMediaListFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 45
    iget-boolean v0, p0, Lcom/twitter/android/ProfileMediaListFragment;->c:Z

    if-eqz v0, :cond_0

    .line 46
    const v0, 0x7f040396

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    move-result-object v0

    const v1, 0x7f0402c9

    .line 47
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l$d;->f(I)Lcom/twitter/app/common/list/l$d;

    .line 48
    invoke-static {}, Lbpu;->a()Lbpu;

    move-result-object v0

    invoke-virtual {v0}, Lbpu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    const v0, 0x7f0403e0

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    .line 50
    const v0, 0x7f040127

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->h(I)V

    .line 53
    :cond_0
    return-void
.end method

.method protected f()V
    .locals 5

    .prologue
    .line 57
    new-instance v0, Lcom/twitter/android/metrics/b;

    const-string/jumbo v1, "timeline:bellbird_profile_photogrid"

    const-string/jumbo v2, "timeline:bellbird_profile_photogrid"

    sget-object v3, Lcom/twitter/metrics/g;->n:Lcom/twitter/metrics/g$b;

    .line 58
    invoke-virtual {p0}, Lcom/twitter/android/ProfileMediaListFragment;->aJ()Lcom/twitter/metrics/j;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/metrics/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/metrics/g$b;Lcom/twitter/metrics/h;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileMediaListFragment;->b:Lcom/twitter/android/metrics/b;

    .line 59
    iget-object v0, p0, Lcom/twitter/android/ProfileMediaListFragment;->b:Lcom/twitter/android/metrics/b;

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/metrics/b;->b(J)V

    .line 60
    iget-object v0, p0, Lcom/twitter/android/ProfileMediaListFragment;->b:Lcom/twitter/android/metrics/b;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/b;->i()V

    .line 61
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 28
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ScrollingHeaderMediaListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 29
    invoke-virtual {p0}, Lcom/twitter/android/ProfileMediaListFragment;->H()Lcom/twitter/app/common/list/i;

    move-result-object v0

    const-string/jumbo v1, "is_me"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ProfileMediaListFragment;->c:Z

    .line 30
    return-void
.end method
