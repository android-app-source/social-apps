.class public Lcom/twitter/android/SearchResultsFragment;
.super Lcom/twitter/android/SearchFragment;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/twitter/android/cl$b;
.implements Lcom/twitter/android/cx$c;
.implements Lcom/twitter/app/common/dialog/b$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/SearchResultsFragment$b;,
        Lcom/twitter/android/SearchResultsFragment$c;,
        Lcom/twitter/android/SearchResultsFragment$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/SearchFragment",
        "<",
        "Lcom/twitter/android/bu;",
        "Lcom/twitter/android/bt;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lcom/twitter/android/cl$b;",
        "Lcom/twitter/android/cx$c;",
        "Lcom/twitter/app/common/dialog/b$d;"
    }
.end annotation


# static fields
.field private static final aa:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private ab:J

.field private ac:J

.field private ad:Z

.field private ae:Z

.field private af:Lcom/twitter/model/util/FriendshipCache;

.field private ag:Lcom/twitter/android/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/av",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private ah:Ljava/lang/String;

.field private ai:Z

.field private aj:Z

.field private ak:Ljava/lang/String;

.field private al:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private am:Lcom/twitter/android/ct;

.field private an:Lcom/twitter/android/ct;

.field private ao:Lcom/twitter/android/ct;

.field private ap:Lcom/twitter/android/ct;

.field private aq:Lcom/twitter/android/ct;

.field private ar:Lcom/twitter/android/SearchResultsFragment$c;

.field private final as:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation
.end field

.field private at:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation
.end field

.field private final au:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private av:Lcom/twitter/android/bt;

.field private aw:Lagg;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 119
    new-instance v0, Landroid/util/SparseArray;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    sput-object v0, Lcom/twitter/android/SearchResultsFragment;->aa:Landroid/util/SparseArray;

    .line 120
    sget-object v0, Lcom/twitter/android/SearchResultsFragment;->aa:Landroid/util/SparseArray;

    const/4 v1, 0x1

    const-string/jumbo v2, "universal_all"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 121
    sget-object v0, Lcom/twitter/android/SearchResultsFragment;->aa:Landroid/util/SparseArray;

    const/4 v1, 0x2

    const-string/jumbo v2, "users"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 122
    sget-object v0, Lcom/twitter/android/SearchResultsFragment;->aa:Landroid/util/SparseArray;

    const/4 v1, 0x3

    const-string/jumbo v2, "photo_tweets"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 123
    sget-object v0, Lcom/twitter/android/SearchResultsFragment;->aa:Landroid/util/SparseArray;

    const/4 v1, 0x4

    const-string/jumbo v2, "videos_vines"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 124
    sget-object v0, Lcom/twitter/android/SearchResultsFragment;->aa:Landroid/util/SparseArray;

    const/4 v1, 0x5

    const-string/jumbo v2, "videos_all"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 125
    sget-object v0, Lcom/twitter/android/SearchResultsFragment;->aa:Landroid/util/SparseArray;

    const/4 v1, 0x6

    const-string/jumbo v2, "news"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 126
    sget-object v0, Lcom/twitter/android/SearchResultsFragment;->aa:Landroid/util/SparseArray;

    const/16 v1, 0x9

    const-string/jumbo v2, "geo"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 127
    sget-object v0, Lcom/twitter/android/SearchResultsFragment;->aa:Landroid/util/SparseArray;

    const/16 v1, 0xd

    const-string/jumbo v2, "periscopes_recent"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 128
    sget-object v0, Lcom/twitter/android/SearchResultsFragment;->aa:Landroid/util/SparseArray;

    const/16 v1, 0xc

    const-string/jumbo v2, "periscopes_top"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 129
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/twitter/android/SearchFragment;-><init>()V

    .line 133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->ad:Z

    .line 134
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->ae:Z

    .line 149
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->as:Ljava/util/List;

    .line 151
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->au:Ljava/util/Set;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/SearchResultsFragment;)J
    .locals 2

    .prologue
    .line 93
    iget-wide v0, p0, Lcom/twitter/android/SearchResultsFragment;->a_:J

    return-wide v0
.end method

.method private a(Lcom/twitter/android/ct;Ljava/lang/String;ZLcom/twitter/analytics/model/ScribeLog$SearchDetails;)Lcom/twitter/android/ct;
    .locals 1

    .prologue
    .line 336
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/android/ct;->a()Z

    move-result v0

    if-eq v0, p3, :cond_1

    .line 337
    :cond_0
    invoke-direct {p0, p2, p3, p4}, Lcom/twitter/android/SearchResultsFragment;->a(Ljava/lang/String;ZLcom/twitter/analytics/model/ScribeLog$SearchDetails;)Lcom/twitter/android/ct;

    move-result-object p1

    .line 339
    :cond_1
    return-object p1
.end method

.method private a(Ljava/lang/String;ZLcom/twitter/analytics/model/ScribeLog$SearchDetails;)Lcom/twitter/android/ct;
    .locals 9

    .prologue
    .line 349
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 350
    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    .line 351
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v3

    .line 353
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v2

    .line 354
    const-string/jumbo v4, "avatar"

    const-string/jumbo v5, "profile_click"

    invoke-static {v2, p1, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 356
    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    const/4 v6, 0x1

    aput-object v3, v5, v6

    const/4 v6, 0x2

    aput-object p1, v5, v6

    const/4 v6, 0x3

    const-string/jumbo v7, "link"

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const-string/jumbo v7, "open_link"

    aput-object v7, v5, v6

    invoke-static {v0, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 358
    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    const/4 v7, 0x1

    aput-object v3, v6, v7

    const/4 v7, 0x2

    aput-object p1, v6, v7

    const/4 v7, 0x3

    const-string/jumbo v8, "platform_photo_card"

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const-string/jumbo v8, "click"

    aput-object v8, v6, v7

    invoke-static {v0, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 360
    const/4 v7, 0x5

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    const/4 v1, 0x1

    aput-object v3, v7, v1

    const/4 v1, 0x2

    aput-object p1, v7, v1

    const/4 v1, 0x3

    const-string/jumbo v3, "platform_player_card"

    aput-object v3, v7, v1

    const/4 v1, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v7, v1

    invoke-static {v0, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 363
    new-instance v1, Lbxc$a;

    invoke-direct {v1}, Lbxc$a;-><init>()V

    .line 364
    invoke-virtual {v1, v4}, Lbxc$a;->a(Ljava/lang/String;)Lbxc$a;

    move-result-object v1

    .line 365
    invoke-virtual {v1, v5}, Lbxc$a;->b(Ljava/lang/String;)Lbxc$a;

    move-result-object v1

    .line 366
    invoke-virtual {v1, v6}, Lbxc$a;->c(Ljava/lang/String;)Lbxc$a;

    move-result-object v1

    .line 367
    invoke-virtual {v1, v0}, Lbxc$a;->d(Ljava/lang/String;)Lbxc$a;

    move-result-object v0

    .line 368
    invoke-virtual {v0}, Lbxc$a;->a()Lbxc;

    move-result-object v4

    .line 370
    if-eqz p2, :cond_0

    .line 371
    new-instance v0, Lapb$a;

    invoke-direct {v0}, Lapb$a;-><init>()V

    sget-object v1, Lcom/twitter/database/schema/a$t;->a:Landroid/net/Uri;

    iget-wide v6, p0, Lcom/twitter/android/SearchResultsFragment;->a_:J

    .line 372
    invoke-static {v1, v6, v7}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lapb$a;->a(Landroid/net/Uri;)Lapb$a;

    move-result-object v0

    sget-object v1, Lbtv;->a:[Ljava/lang/String;

    .line 374
    invoke-virtual {v0, v1}, Lapb$a;->b([Ljava/lang/String;)Lapb$a;

    move-result-object v0

    const-string/jumbo v1, "statuses_flags&1537 !=0 AND search_id=?"

    .line 375
    invoke-virtual {v0, v1}, Lapb$a;->a(Ljava/lang/String;)Laop$a;

    move-result-object v0

    check-cast v0, Lapb$a;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-wide v6, p0, Lcom/twitter/android/SearchResultsFragment;->r:J

    .line 376
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v3

    invoke-virtual {v0, v1}, Lapb$a;->a([Ljava/lang/String;)Laop$a;

    move-result-object v0

    check-cast v0, Lapb$a;

    const-string/jumbo v1, "type_id ASC, _id ASC"

    .line 377
    invoke-virtual {v0, v1}, Lapb$a;->b(Ljava/lang/String;)Laop$a;

    move-result-object v0

    check-cast v0, Lapb$a;

    .line 378
    invoke-virtual {v0}, Lapb$a;->q()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lapb;

    .line 379
    new-instance v0, Lcom/twitter/android/ct;

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/ct;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Lbxc;Lapb;)V

    .line 381
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/android/ct;

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    new-instance v5, Lcom/twitter/android/SearchResultsFragment$b;

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->at:Ljava/util/List;

    invoke-direct {v5, p0, v2, p3, v1}, Lcom/twitter/android/SearchResultsFragment$b;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/model/ScribeLog$SearchDetails;Ljava/util/List;)V

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/ct;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Lbxc;Lcom/twitter/android/ck;)V

    goto :goto_0
.end method

.method private a(IJ)Z
    .locals 18

    .prologue
    .line 526
    invoke-virtual/range {p0 .. p1}, Lcom/twitter/android/SearchResultsFragment;->c_(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 527
    const/4 v2, 0x0

    .line 582
    :goto_0
    return v2

    .line 530
    :cond_0
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/twitter/android/SearchResultsFragment;->k:I

    .line 535
    packed-switch p1, :pswitch_data_0

    .line 558
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Invalid fetch type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 537
    :pswitch_0
    const/4 v12, 0x0

    .line 539
    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/SearchResultsFragment;->l:I

    if-nez v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    move/from16 v16, v2

    .line 562
    :goto_2
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/twitter/android/SearchResultsFragment;->a_:J

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    .line 563
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v6

    move/from16 v0, p1

    invoke-static {v5, v6, v0}, Lcom/twitter/android/SearchResultsFragment;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/android/SearchResultsFragment;->l:I

    .line 564
    invoke-static {v4}, Lcom/twitter/android/SearchResultsFragment;->c(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/twitter/android/SearchResultsFragment;->c:Z

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/twitter/android/SearchResultsFragment;->b:Z

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 566
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    .line 567
    new-instance v3, Lcom/twitter/library/api/search/d;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/SearchResultsFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    .line 568
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/SearchResultsFragment;->B()I

    move-result v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/android/SearchResultsFragment;->u:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/SearchResultsFragment;->s:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/twitter/android/SearchResultsFragment;->z:Ljava/lang/String;

    const/4 v14, 0x0

    .line 569
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v15

    move-wide/from16 v6, p2

    invoke-direct/range {v3 .. v15}, Lcom/twitter/library/api/search/d;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;ZZ)V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/android/SearchResultsFragment;->l:I

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/twitter/android/SearchResultsFragment;->c:Z

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/twitter/android/SearchResultsFragment;->d:Z

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/twitter/android/SearchResultsFragment;->e:Z

    .line 570
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/twitter/library/api/search/d;->a(IZZZ)Lcom/twitter/library/api/search/d;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/twitter/android/SearchResultsFragment;->ab:J

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/twitter/android/SearchResultsFragment;->ac:J

    .line 571
    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/twitter/library/api/search/d;->a(JJ)Lcom/twitter/library/api/search/d;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/SearchResultsFragment;->C:Ljava/lang/String;

    const/4 v5, 0x0

    .line 572
    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/api/search/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/api/search/d;

    move-result-object v3

    .line 573
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/twitter/android/SearchResultsFragment;->a(Lcom/twitter/library/api/search/d;)V

    .line 574
    if-eqz v16, :cond_1

    .line 575
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/SearchResultsFragment;->al:Ljava/util/List;

    invoke-virtual {v3, v4}, Lcom/twitter/library/api/search/d;->a(Ljava/util/List;)Lcom/twitter/library/api/search/d;

    .line 577
    :cond_1
    const-string/jumbo v4, "scribe_log"

    invoke-virtual {v3, v4, v2}, Lcom/twitter/library/api/search/d;->a(Ljava/lang/String;Landroid/os/Parcelable;)Lcom/twitter/library/service/s;

    .line 578
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/twitter/android/SearchResultsFragment;->b:Z

    if-eqz v2, :cond_2

    .line 579
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/SearchResultsFragment;->F:Lbqn;

    invoke-virtual {v2}, Lbqn;->a()Landroid/location/Location;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/twitter/library/api/search/d;->a(Landroid/location/Location;)Lcom/twitter/library/api/search/d;

    .line 581
    :cond_2
    const/4 v2, 0x2

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v3, v2, v1}, Lcom/twitter/android/SearchResultsFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 582
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 539
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 543
    :pswitch_1
    const/4 v12, 0x1

    .line 544
    const/4 v2, 0x0

    move/from16 v16, v2

    .line 545
    goto/16 :goto_2

    .line 548
    :pswitch_2
    const/4 v12, 0x1

    .line 549
    const/4 v2, 0x0

    move/from16 v16, v2

    .line 550
    goto/16 :goto_2

    .line 553
    :pswitch_3
    const/4 v12, 0x2

    .line 554
    const/4 v2, 0x0

    move/from16 v16, v2

    .line 555
    goto/16 :goto_2

    .line 535
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lcom/twitter/android/SearchResultsFragment;Lcom/twitter/library/api/search/d;)Z
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/twitter/android/SearchResultsFragment;->b(Lcom/twitter/library/api/search/d;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/SearchResultsFragment;Z)Z
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/twitter/android/SearchResultsFragment;->ad:Z

    return p1
.end method

.method private aO()V
    .locals 4

    .prologue
    .line 456
    iget v0, p0, Lcom/twitter/android/SearchResultsFragment;->l:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 457
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const/4 v1, 0x5

    .line 458
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-wide v2, p0, Lcom/twitter/android/SearchResultsFragment;->a_:J

    .line 459
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    .line 460
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v1, "people"

    .line 461
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 457
    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 468
    :goto_0
    return-void

    .line 463
    :cond_0
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const/4 v1, 0x6

    .line 464
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    .line 465
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 466
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 463
    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    goto :goto_0
.end method

.method private aP()Z
    .locals 1

    .prologue
    .line 1255
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->C:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private b(Ljava/lang/String;)Lcom/twitter/android/ct;
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 304
    new-instance v0, Lcom/twitter/analytics/model/ScribeLog$SearchDetails;

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    iget v2, p0, Lcom/twitter/android/SearchResultsFragment;->l:I

    .line 305
    invoke-static {v2}, Lcom/twitter/android/SearchResultsFragment;->c(I)Ljava/lang/String;

    move-result-object v2

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->c:Z

    iget-boolean v5, p0, Lcom/twitter/android/SearchResultsFragment;->b:Z

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/analytics/model/ScribeLog$SearchDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 306
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    move-object v0, v8

    .line 329
    :goto_1
    return-object v0

    .line 306
    :sswitch_0
    const-string/jumbo v2, "tweet"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v7

    goto :goto_0

    :sswitch_1
    const-string/jumbo v2, "news"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v6

    goto :goto_0

    :sswitch_2
    const-string/jumbo v2, "highlight"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string/jumbo v2, "tweet_list_glance"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v9

    goto :goto_0

    :sswitch_4
    const-string/jumbo v2, "tweet_list_popular"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    .line 308
    :pswitch_0
    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->am:Lcom/twitter/android/ct;

    iget v0, p0, Lcom/twitter/android/SearchResultsFragment;->l:I

    if-ne v0, v9, :cond_1

    .line 309
    invoke-static {}, Lcom/twitter/android/search/d;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v6

    .line 308
    :goto_2
    invoke-direct {p0, v1, p1, v0, v8}, Lcom/twitter/android/SearchResultsFragment;->a(Lcom/twitter/android/ct;Ljava/lang/String;ZLcom/twitter/analytics/model/ScribeLog$SearchDetails;)Lcom/twitter/android/ct;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->am:Lcom/twitter/android/ct;

    goto :goto_1

    :cond_1
    move v0, v7

    .line 309
    goto :goto_2

    .line 313
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->an:Lcom/twitter/android/ct;

    invoke-direct {p0, v0, p1, v7, v8}, Lcom/twitter/android/SearchResultsFragment;->a(Lcom/twitter/android/ct;Ljava/lang/String;ZLcom/twitter/analytics/model/ScribeLog$SearchDetails;)Lcom/twitter/android/ct;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->an:Lcom/twitter/android/ct;

    goto :goto_1

    .line 317
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ao:Lcom/twitter/android/ct;

    invoke-direct {p0, v0, p1, v7, v8}, Lcom/twitter/android/SearchResultsFragment;->a(Lcom/twitter/android/ct;Ljava/lang/String;ZLcom/twitter/analytics/model/ScribeLog$SearchDetails;)Lcom/twitter/android/ct;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ao:Lcom/twitter/android/ct;

    goto :goto_1

    .line 321
    :pswitch_3
    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->ap:Lcom/twitter/android/ct;

    invoke-direct {p0, v1, p1, v7, v0}, Lcom/twitter/android/SearchResultsFragment;->a(Lcom/twitter/android/ct;Ljava/lang/String;ZLcom/twitter/analytics/model/ScribeLog$SearchDetails;)Lcom/twitter/android/ct;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ap:Lcom/twitter/android/ct;

    goto :goto_1

    .line 325
    :pswitch_4
    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->aq:Lcom/twitter/android/ct;

    invoke-direct {p0, v1, p1, v7, v0}, Lcom/twitter/android/SearchResultsFragment;->a(Lcom/twitter/android/ct;Ljava/lang/String;ZLcom/twitter/analytics/model/ScribeLog$SearchDetails;)Lcom/twitter/android/ct;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->aq:Lcom/twitter/android/ct;

    goto :goto_1

    .line 306
    :sswitch_data_0
    .sparse-switch
        -0x6fdf50ba -> :sswitch_4
        -0x289a734c -> :sswitch_2
        -0x1328aa59 -> :sswitch_3
        0x338ad3 -> :sswitch_1
        0x69a4671 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method static synthetic b(Lcom/twitter/android/SearchResultsFragment;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->au:Ljava/util/Set;

    return-object v0
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 388
    iget-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->aj:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->C()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 395
    :cond_0
    :goto_0
    return-void

    .line 391
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/android/bv;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 392
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bv;

    .line 393
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-interface {v0, p1, v1, p0}, Lcom/twitter/android/bv;->a(ZLandroid/widget/ListView;Lcom/twitter/android/SearchFragment;)V

    goto :goto_0
.end method

.method private b(Lcom/twitter/library/api/search/d;)Z
    .locals 24

    .prologue
    .line 662
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    instance-of v2, v2, Lcom/twitter/android/bv;

    if-nez v2, :cond_0

    .line 663
    const/4 v2, 0x0

    .line 692
    :goto_0
    return v2

    .line 666
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/twitter/android/SearchResultsFragment;->ad:Z

    .line 667
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/api/search/d;->y()Ljava/lang/String;

    move-result-object v4

    .line 668
    if-eqz v4, :cond_2

    .line 669
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 670
    if-eqz v2, :cond_2

    .line 671
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/api/search/d;->C()Lcom/twitter/model/topic/TwitterTopic;

    move-result-object v6

    .line 672
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/api/search/d;->z()Ljava/lang/String;

    move-result-object v23

    .line 673
    invoke-static {v4}, Lcom/twitter/android/events/a;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 674
    if-nez v6, :cond_1

    .line 675
    new-instance v3, Lcom/twitter/model/topic/TwitterTopic;

    new-instance v4, Lcom/twitter/model/topic/TwitterTopic$a;

    const/4 v5, 0x2

    .line 678
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/api/search/d;->x()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct {v4, v5, v6, v7}, Lcom/twitter/model/topic/TwitterTopic$a;-><init>(ILjava/lang/String;Z)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/SearchResultsFragment;->s:Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    const-wide/16 v16, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    invoke-direct/range {v3 .. v22}, Lcom/twitter/model/topic/TwitterTopic;-><init>(Lcom/twitter/model/topic/TwitterTopic$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLcgi;Ljava/util/List;Lcom/twitter/model/topic/b;Lcom/twitter/model/core/TwitterUser;Ljava/lang/String;)V

    move-object v6, v3

    .line 688
    :cond_1
    check-cast v2, Lcom/twitter/android/bv;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/twitter/android/SearchResultsFragment;->r:J

    move-object/from16 v7, v23

    invoke-interface/range {v2 .. v7}, Lcom/twitter/android/bv;->a(Ljava/lang/String;JLcom/twitter/model/topic/TwitterTopic;Ljava/lang/String;)Z

    move-result v2

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/SearchResultsFragment;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/SearchResultsFragment;)J
    .locals 2

    .prologue
    .line 93
    iget-wide v0, p0, Lcom/twitter/android/SearchResultsFragment;->a_:J

    return-wide v0
.end method

.method static synthetic e(Lcom/twitter/android/SearchResultsFragment;)J
    .locals 2

    .prologue
    .line 93
    iget-wide v0, p0, Lcom/twitter/android/SearchResultsFragment;->a_:J

    return-wide v0
.end method

.method static synthetic f(Lcom/twitter/android/SearchResultsFragment;)J
    .locals 2

    .prologue
    .line 93
    iget-wide v0, p0, Lcom/twitter/android/SearchResultsFragment;->a_:J

    return-wide v0
.end method

.method static synthetic g(Lcom/twitter/android/SearchResultsFragment;)Z
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aP()Z

    move-result v0

    return v0
.end method

.method static synthetic h(Lcom/twitter/android/SearchResultsFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->as:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method protected E()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1232
    sget-object v0, Lcom/twitter/android/SearchResultsFragment;->aa:Landroid/util/SparseArray;

    iget v1, p0, Lcom/twitter/android/SearchResultsFragment;->l:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1233
    if-eqz v0, :cond_0

    .line 1236
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "universal_top"

    goto :goto_0
.end method

.method protected F()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 1271
    iget v0, p0, Lcom/twitter/android/SearchResultsFragment;->l:I

    if-ne v0, v3, :cond_0

    .line 1272
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":people:users::impression"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->a(Ljava/lang/String;)V

    .line 1277
    :goto_0
    return-void

    .line 1274
    :cond_0
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    aput-object v4, v0, v3

    const/4 v1, 0x3

    aput-object v4, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "impression"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected G()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1282
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->as:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1284
    iget v0, p0, Lcom/twitter/android/SearchResultsFragment;->p:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aP()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1285
    :cond_0
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    aput-object v1, v0, v4

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    const-string/jumbo v1, "stream"

    aput-object v1, v0, v2

    const/4 v1, 0x0

    aput-object v1, v0, v3

    const-string/jumbo v1, "results"

    aput-object v1, v0, v6

    invoke-static {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1292
    :goto_0
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/SearchResultsFragment;->a_:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v5, [Ljava/lang/String;

    aput-object v0, v2, v4

    .line 1293
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 1294
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->N()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/twitter/android/SearchResultsFragment;->l:I

    invoke-static {v2}, Lcom/twitter/android/SearchResultsFragment;->c(I)Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lcom/twitter/android/SearchResultsFragment;->c:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->b:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->as:Ljava/util/List;

    .line 1295
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(Ljava/util/List;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1292
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1296
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->as:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1298
    :cond_1
    return-void

    .line 1288
    :cond_2
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    aput-object v1, v0, v4

    const-string/jumbo v1, "universal_top"

    aput-object v1, v0, v5

    iget v1, p0, Lcom/twitter/android/SearchResultsFragment;->p:I

    .line 1289
    invoke-static {v1}, Lcom/twitter/model/topic/TwitterTopic;->c(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    const-string/jumbo v1, "event"

    aput-object v1, v0, v3

    const-string/jumbo v1, "results"

    aput-object v1, v0, v6

    .line 1288
    invoke-static {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected H_()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 257
    iget v0, p0, Lcom/twitter/android/SearchResultsFragment;->l:I

    if-ne v0, v4, :cond_0

    iget-wide v0, p0, Lcom/twitter/android/SearchResultsFragment;->ab:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 267
    :goto_0
    return-void

    .line 261
    :cond_0
    iget v0, p0, Lcom/twitter/android/SearchResultsFragment;->m:I

    if-lez v0, :cond_1

    .line 263
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->s()V

    goto :goto_0

    .line 265
    :cond_1
    iget-wide v0, p0, Lcom/twitter/android/SearchResultsFragment;->r:J

    invoke-direct {p0, v4, v0, v1}, Lcom/twitter/android/SearchResultsFragment;->a(IJ)Z

    goto :goto_0
.end method

.method protected K()Laoy;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laoy",
            "<",
            "Lcbi",
            "<",
            "Lcom/twitter/android/bu;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 400
    invoke-direct {p0, v5}, Lcom/twitter/android/SearchResultsFragment;->b(Z)V

    .line 401
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aK()Lcom/twitter/app/common/base/TwitterFragmentActivity;

    move-result-object v1

    .line 402
    iget-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->ad:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->C:Ljava/lang/String;

    if-eqz v0, :cond_1

    instance-of v0, v1, Lcom/twitter/android/u;

    if-eqz v0, :cond_1

    .line 404
    :cond_0
    :goto_0
    new-instance v0, Lcom/twitter/android/search/e;

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->C:Ljava/lang/String;

    iget v3, p0, Lcom/twitter/android/SearchResultsFragment;->l:I

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->ad:Z

    iget-boolean v6, p0, Lcom/twitter/android/SearchResultsFragment;->ae:Z

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/search/e;-><init>(Landroid/content/Context;Ljava/lang/String;IZZZ)V

    .line 407
    new-instance v1, Lcom/twitter/android/SearchResultsFragment$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/SearchResultsFragment$1;-><init>(Lcom/twitter/android/SearchResultsFragment;)V

    .line 417
    new-instance v2, Laos;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    new-instance v4, Lcom/twitter/android/SearchResultsFragment$2;

    invoke-direct {v4, p0, v0}, Lcom/twitter/android/SearchResultsFragment$2;-><init>(Lcom/twitter/android/SearchResultsFragment;Lcom/twitter/android/search/e;)V

    invoke-direct {v2, v3, v7, v1, v4}, Laos;-><init>(Landroid/support/v4/app/LoaderManager;ILcom/twitter/util/object/j;Lcbq;)V

    return-object v2

    :cond_1
    move v5, v7

    .line 402
    goto :goto_0
.end method

.method public L()V
    .locals 1

    .prologue
    .line 429
    invoke-super {p0}, Lcom/twitter/android/SearchFragment;->L()V

    .line 431
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->b(Z)V

    .line 432
    return-void
.end method

.method public M()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 1041
    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aP()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1042
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v1

    .line 1043
    const-string/jumbo v0, "user_rail"

    .line 1048
    :goto_0
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    aput-object v4, v2, v3

    aput-object v1, v2, v5

    aput-object v0, v2, v6

    const/4 v0, 0x3

    const-string/jumbo v1, "more"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string/jumbo v1, "search"

    aput-object v1, v2, v0

    invoke-static {v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->a(Ljava/lang/String;)V

    .line 1049
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->aw:Lagg;

    new-instance v1, Lage$a;

    .line 1050
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lage$a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->s:Ljava/lang/String;

    .line 1051
    invoke-virtual {v1, v2}, Lage$a;->a(Ljava/lang/String;)Lage$a;

    move-result-object v1

    .line 1052
    invoke-virtual {v1, v6}, Lage$a;->a(I)Lage$a;

    move-result-object v1

    .line 1053
    invoke-virtual {v1, v5}, Lage$a;->a(Z)Lage$a;

    move-result-object v1

    .line 1054
    invoke-virtual {v1}, Lage$a;->a()Lage;

    move-result-object v1

    .line 1049
    invoke-virtual {v0, v1}, Lagg;->a(Lage;)V

    .line 1055
    return-void

    .line 1045
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v1

    .line 1046
    const-string/jumbo v0, "user_gallery"

    goto :goto_0
.end method

.method protected N()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1241
    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aP()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1242
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->C:Ljava/lang/String;

    .line 1244
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    goto :goto_0
.end method

.method protected N_()Z
    .locals 1

    .prologue
    .line 300
    invoke-static {}, Lcom/twitter/android/revenue/k;->a()Z

    move-result v0

    return v0
.end method

.method public a(JLcgi;ILcom/twitter/android/cx$e;)V
    .locals 13

    .prologue
    .line 943
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "user_id"

    .line 944
    invoke-virtual {v2, v3, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "screen_name"

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/twitter/android/cx$e;->a:Ljava/lang/String;

    .line 945
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 946
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v11

    .line 947
    if-eqz v11, :cond_0

    .line 948
    const-string/jumbo v4, "association"

    new-instance v2, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v2, v11}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    const/4 v5, 0x5

    .line 950
    invoke-virtual {v2, v5}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-wide v6, p0, Lcom/twitter/android/SearchResultsFragment;->a_:J

    .line 951
    invoke-virtual {v2, v6, v7}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v2

    .line 948
    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 954
    :cond_0
    if-eqz p3, :cond_1

    .line 955
    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->d:Lcom/twitter/library/api/PromotedEvent;

    move-object/from16 v0, p3

    invoke-static {v2, v0}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v2

    invoke-virtual {v2}, Lbsq$a;->a()Lbsq;

    move-result-object v2

    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 956
    const-string/jumbo v2, "pc"

    invoke-static/range {p3 .. p3}, Lcgi;->a(Lcgi;)[B

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 958
    :cond_1
    invoke-virtual {p0, v3}, Lcom/twitter/android/SearchResultsFragment;->startActivity(Landroid/content/Intent;)V

    .line 962
    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aP()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 963
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v4

    .line 964
    const-string/jumbo v3, "user_rail"

    .line 965
    const-string/jumbo v2, "avatar"

    move-object v9, v3

    move-object v10, v4

    .line 971
    :goto_0
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, p0, Lcom/twitter/android/SearchResultsFragment;->a_:J

    invoke-direct {v3, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 972
    const/4 v7, 0x0

    move-wide v4, p1

    move-object/from16 v6, p3

    move/from16 v8, p4

    invoke-static/range {v3 .. v8}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;JLcgi;Ljava/lang/String;I)V

    .line 973
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v10, v6, v7

    const/4 v7, 0x2

    aput-object v9, v6, v7

    const/4 v7, 0x3

    aput-object v2, v6, v7

    const/4 v2, 0x4

    const-string/jumbo v7, "profile_click"

    aput-object v7, v6, v2

    invoke-static {v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-virtual {v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 975
    invoke-virtual {v2, v11}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    iget v4, p0, Lcom/twitter/android/SearchResultsFragment;->l:I

    .line 976
    invoke-static {v4}, Lcom/twitter/android/SearchResultsFragment;->c(I)Ljava/lang/String;

    move-result-object v4

    iget-boolean v5, p0, Lcom/twitter/android/SearchResultsFragment;->c:Z

    iget-boolean v6, p0, Lcom/twitter/android/SearchResultsFragment;->b:Z

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 973
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 977
    return-void

    .line 967
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v4

    .line 968
    const-string/jumbo v3, "user_gallery"

    .line 969
    const-string/jumbo v2, "user"

    move-object v9, v3

    move-object v10, v4

    goto :goto_0
.end method

.method protected a(Landroid/content/Context;)V
    .locals 14

    .prologue
    const/4 v10, 0x1

    .line 441
    new-instance v1, Lcom/twitter/library/api/search/d;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/android/SearchResultsFragment;->r:J

    iget-object v6, p0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    .line 442
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->B()I

    move-result v7

    iget-object v8, p0, Lcom/twitter/android/SearchResultsFragment;->u:Ljava/lang/String;

    iget-object v9, p0, Lcom/twitter/android/SearchResultsFragment;->s:Ljava/lang/String;

    iget-object v11, p0, Lcom/twitter/android/SearchResultsFragment;->z:Ljava/lang/String;

    const/4 v12, 0x0

    .line 443
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v13

    move-object v2, p1

    invoke-direct/range {v1 .. v13}, Lcom/twitter/library/api/search/d;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;ZZ)V

    iget v0, p0, Lcom/twitter/android/SearchResultsFragment;->l:I

    iget-boolean v2, p0, Lcom/twitter/android/SearchResultsFragment;->c:Z

    iget-boolean v3, p0, Lcom/twitter/android/SearchResultsFragment;->d:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->e:Z

    .line 444
    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/twitter/library/api/search/d;->a(IZZZ)Lcom/twitter/library/api/search/d;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/SearchResultsFragment;->ab:J

    iget-wide v4, p0, Lcom/twitter/android/SearchResultsFragment;->ac:J

    .line 445
    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/twitter/library/api/search/d;->a(JJ)Lcom/twitter/library/api/search/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->C:Ljava/lang/String;

    const/4 v2, 0x0

    .line 446
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/api/search/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/api/search/d;

    move-result-object v0

    .line 447
    const-string/jumbo v1, "Not triggered by a user action."

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/search/d;->l(Ljava/lang/String;)Lcom/twitter/library/service/s;

    .line 448
    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->a(Lcom/twitter/library/api/search/d;)V

    .line 449
    iget-boolean v1, p0, Lcom/twitter/android/SearchResultsFragment;->b:Z

    if-eqz v1, :cond_0

    .line 450
    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->F:Lbqn;

    invoke-virtual {v1}, Lbqn;->a()Landroid/location/Location;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/search/d;->a(Landroid/location/Location;)Lcom/twitter/library/api/search/d;

    .line 452
    :cond_0
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v10, v1}, Lcom/twitter/android/SearchResultsFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 453
    return-void
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1082
    if-ne p2, v5, :cond_1

    .line 1083
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v0

    .line 1084
    const/4 v1, -0x1

    if-ne p3, v1, :cond_2

    .line 1085
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/SearchResultsFragment;->a_:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    aput-object v3, v2, v4

    aput-object v0, v2, v5

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ak:Ljava/lang/String;

    aput-object v0, v2, v6

    const-string/jumbo v0, "feedback"

    aput-object v0, v2, v7

    const-string/jumbo v0, "accept"

    aput-object v0, v2, v8

    .line 1086
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1085
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1091
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0a07ee

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 1092
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1094
    :cond_1
    return-void

    .line 1087
    :cond_2
    const/4 v1, -0x2

    if-ne p3, v1, :cond_0

    .line 1088
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/SearchResultsFragment;->a_:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    aput-object v3, v2, v4

    aput-object v0, v2, v5

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ak:Ljava/lang/String;

    aput-object v0, v2, v6

    const-string/jumbo v0, "feedback"

    aput-object v0, v2, v7

    const-string/jumbo v0, "deny"

    aput-object v0, v2, v8

    .line 1089
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1088
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0
.end method

.method a(Landroid/view/View;)V
    .locals 4

    .prologue
    const v3, 0x7f0a07e9

    const/4 v2, 0x0

    .line 646
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->T:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, v3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 647
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 648
    invoke-direct {p0, v2}, Lcom/twitter/android/SearchResultsFragment;->b(Z)V

    .line 649
    if-eqz p1, :cond_0

    .line 650
    const v0, 0x7f13002f

    .line 651
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 652
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 653
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 655
    :cond_0
    return-void
.end method

.method public a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 15

    .prologue
    .line 717
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v2

    move/from16 v0, p3

    if-ge v0, v2, :cond_1

    .line 916
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 721
    :cond_1
    iget v2, p0, Lcom/twitter/android/SearchResultsFragment;->l:I

    invoke-static {v2}, Lcom/twitter/android/SearchResultsFragment;->c(I)Ljava/lang/String;

    move-result-object v10

    .line 723
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/bt$a;

    .line 724
    if-nez v2, :cond_2

    .line 728
    new-instance v3, Lcpb;

    invoke-direct {v3}, Lcpb;-><init>()V

    const-string/jumbo v4, "view"

    .line 729
    move-object/from16 v0, p2

    invoke-virtual {v3, v4, v0}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v3

    const-string/jumbo v4, "position"

    .line 730
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v4

    const-string/jumbo v5, "view type"

    .line 731
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->az()Lcjr;

    move-result-object v3

    check-cast v3, Lcom/twitter/android/bt;

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/twitter/android/bt;->getItemViewType(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v3

    .line 728
    invoke-static {v3}, Lcpd;->c(Lcpb;)V

    .line 733
    :cond_2
    iget-object v3, v2, Lcom/twitter/android/bt$a;->m:Lcom/twitter/android/bu;

    .line 734
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 735
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v11

    .line 736
    iget v4, v3, Lcom/twitter/android/bu;->b:I

    packed-switch v4, :pswitch_data_0

    :pswitch_1
    goto :goto_0

    .line 741
    :pswitch_2
    iget-object v2, v2, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    iget-object v2, v2, Lcom/twitter/android/cu;->d:Lcom/twitter/library/widget/TweetView;

    .line 742
    invoke-virtual {v2}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/model/core/Tweet;

    move-result-object v4

    .line 743
    invoke-virtual {v2}, Lcom/twitter/library/widget/TweetView;->getReason()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 744
    invoke-virtual {v2}, Lcom/twitter/library/widget/TweetView;->getReasonIconResId()I

    move-result v2

    .line 745
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    .line 746
    const/4 v8, 0x0

    invoke-static {v5, v2, v8}, Laji;->a(Ljava/lang/String;IZ)Laji;

    move-result-object v2

    .line 748
    new-instance v5, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    const-class v9, Lcom/twitter/android/TweetActivity;

    invoke-direct {v5, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v8, "tw"

    .line 749
    invoke-virtual {v5, v8, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v5

    const-string/jumbo v8, "association"

    .line 750
    invoke-virtual {v5, v8, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v5

    .line 751
    const-string/jumbo v8, "social_proof_override"

    sget-object v9, Laji;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v5, v8, v2, v9}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/content/Intent;

    .line 753
    invoke-virtual {p0, v5}, Lcom/twitter/android/SearchResultsFragment;->startActivity(Landroid/content/Intent;)V

    .line 754
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v8, p0, Lcom/twitter/android/SearchResultsFragment;->a_:J

    invoke-direct {v2, v8, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 755
    const/4 v5, 0x0

    invoke-static {v2, v7, v4, v5}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 756
    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aP()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 757
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    aput-object v8, v5, v7

    const/4 v7, 0x1

    .line 758
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    const/4 v7, 0x2

    const-string/jumbo v8, "tweet"

    aput-object v8, v5, v7

    const/4 v7, 0x3

    const-string/jumbo v8, "tweet"

    aput-object v8, v5, v7

    const/4 v7, 0x4

    const-string/jumbo v8, "click"

    aput-object v8, v5, v7

    .line 757
    invoke-static {v6, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 759
    invoke-virtual {v2, v11}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->at:Ljava/util/List;

    .line 760
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/util/Collection;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->c:Z

    iget-boolean v5, p0, Lcom/twitter/android/SearchResultsFragment;->b:Z

    .line 761
    invoke-virtual {v2, v3, v10, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 757
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 762
    :cond_3
    iget v4, v3, Lcom/twitter/android/bu;->b:I

    if-eqz v4, :cond_4

    iget v4, v3, Lcom/twitter/android/bu;->b:I

    const/16 v5, 0x12

    if-ne v4, v5, :cond_5

    .line 764
    :cond_4
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "tweet"

    const-string/jumbo v6, "tweet"

    const-string/jumbo v7, "click"

    invoke-static {v11, v5, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 766
    invoke-virtual {v2, v11}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->at:Ljava/util/List;

    .line 767
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/util/Collection;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->c:Z

    iget-boolean v5, p0, Lcom/twitter/android/SearchResultsFragment;->b:Z

    .line 768
    invoke-virtual {v2, v3, v10, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 764
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 769
    :cond_5
    iget v3, v3, Lcom/twitter/android/bu;->b:I

    const/16 v4, 0x14

    if-ne v3, v4, :cond_6

    .line 770
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ":cluster:tweet::click"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 771
    invoke-virtual {v2, v11}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->at:Ljava/util/List;

    .line 772
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/util/Collection;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->c:Z

    iget-boolean v5, p0, Lcom/twitter/android/SearchResultsFragment;->b:Z

    .line 773
    invoke-virtual {v2, v3, v10, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 770
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 775
    :cond_6
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    aput-object v8, v5, v7

    const/4 v7, 0x1

    .line 776
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    const/4 v7, 0x2

    const-string/jumbo v8, "news"

    aput-object v8, v5, v7

    const/4 v7, 0x3

    const-string/jumbo v8, "tweet"

    aput-object v8, v5, v7

    const/4 v7, 0x4

    const-string/jumbo v8, "click"

    aput-object v8, v5, v7

    .line 775
    invoke-static {v6, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 777
    invoke-virtual {v2, v11}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->at:Ljava/util/List;

    .line 778
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/util/Collection;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->c:Z

    iget-boolean v5, p0, Lcom/twitter/android/SearchResultsFragment;->b:Z

    .line 779
    invoke-virtual {v2, v3, v10, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 775
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 784
    :pswitch_3
    check-cast p2, Lcom/twitter/internal/android/widget/GroupedRowView;

    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/GroupedRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/twitter/ui/user/UserView;

    .line 785
    invoke-virtual {v2}, Lcom/twitter/ui/user/UserView;->getUserId()J

    move-result-wide v4

    .line 786
    new-instance v3, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    const-class v8, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v3, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v7, "user_id"

    .line 787
    invoke-virtual {v3, v7, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v7, "screen_name"

    .line 788
    invoke-virtual {v2}, Lcom/twitter/ui/user/UserView;->getUserName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    .line 789
    if-eqz v11, :cond_7

    .line 790
    const-string/jumbo v8, "association"

    new-instance v3, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v3, v11}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    const/4 v9, 0x5

    .line 792
    invoke-virtual {v3, v9}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v3

    check-cast v3, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-wide v12, p0, Lcom/twitter/android/SearchResultsFragment;->a_:J

    .line 793
    invoke-virtual {v3, v12, v13}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v3

    .line 790
    invoke-virtual {v7, v8, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 796
    :cond_7
    invoke-virtual {v2}, Lcom/twitter/ui/user/UserView;->getPromotedContent()Lcgi;

    move-result-object v3

    .line 797
    if-eqz v3, :cond_8

    .line 798
    sget-object v8, Lcom/twitter/library/api/PromotedEvent;->d:Lcom/twitter/library/api/PromotedEvent;

    invoke-static {v8, v3}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v8

    invoke-virtual {v8}, Lbsq$a;->a()Lbsq;

    move-result-object v8

    invoke-static {v8}, Lcpm;->a(Lcpk;)V

    .line 799
    const-string/jumbo v8, "pc"

    invoke-static {v3}, Lcgi;->a(Lcgi;)[B

    move-result-object v3

    invoke-virtual {v7, v8, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 801
    :cond_8
    invoke-virtual {p0, v7}, Lcom/twitter/android/SearchResultsFragment;->startActivity(Landroid/content/Intent;)V

    .line 803
    iget v3, p0, Lcom/twitter/android/SearchResultsFragment;->l:I

    const/4 v7, 0x2

    if-ne v3, v7, :cond_9

    .line 804
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v6, ":people:users:user:profile_click"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v9, v3

    .line 809
    :goto_1
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v6, p0, Lcom/twitter/android/SearchResultsFragment;->a_:J

    invoke-direct {v3, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 810
    invoke-virtual {v2}, Lcom/twitter/ui/user/UserView;->getPromotedContent()Lcgi;

    move-result-object v6

    const/4 v7, 0x0

    move/from16 v8, p3

    invoke-static/range {v3 .. v8}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;JLcgi;Ljava/lang/String;I)V

    .line 812
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v9, v2, v4

    invoke-virtual {v3, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 813
    invoke-virtual {v2, v11}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->c:Z

    iget-boolean v5, p0, Lcom/twitter/android/SearchResultsFragment;->b:Z

    .line 814
    invoke-virtual {v2, v3, v10, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->at:Ljava/util/List;

    .line 815
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/util/Collection;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 812
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 806
    :cond_9
    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    aput-object v8, v3, v7

    const/4 v7, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v3, v7

    const/4 v7, 0x2

    const/4 v8, 0x0

    aput-object v8, v3, v7

    const/4 v7, 0x3

    const-string/jumbo v8, "user"

    aput-object v8, v3, v7

    const/4 v7, 0x4

    const-string/jumbo v8, "profile_click"

    aput-object v8, v3, v7

    invoke-static {v6, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v9, v3

    goto :goto_1

    .line 820
    :pswitch_4
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "user"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "more"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "search"

    aput-object v4, v2, v3

    invoke-static {v6, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/twitter/android/SearchResultsFragment;->a(Ljava/lang/String;)V

    .line 822
    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->aw:Lagg;

    new-instance v3, Lage$a;

    .line 823
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Lage$a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->s:Ljava/lang/String;

    .line 824
    invoke-virtual {v3, v4}, Lage$a;->a(Ljava/lang/String;)Lage$a;

    move-result-object v3

    const/4 v4, 0x2

    .line 825
    invoke-virtual {v3, v4}, Lage$a;->a(I)Lage$a;

    move-result-object v3

    const/4 v4, 0x1

    .line 826
    invoke-virtual {v3, v4}, Lage$a;->a(Z)Lage$a;

    move-result-object v3

    .line 827
    invoke-virtual {v3}, Lage$a;->a()Lage;

    move-result-object v3

    .line 822
    invoke-virtual {v2, v3}, Lagg;->a(Lage;)V

    goto/16 :goto_0

    .line 835
    :pswitch_5
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "media_gallery"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "more"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "search"

    aput-object v4, v2, v3

    invoke-static {v6, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/twitter/android/SearchResultsFragment;->a(Ljava/lang/String;)V

    .line 837
    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->aw:Lagg;

    new-instance v3, Lage$a;

    .line 838
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Lage$a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->s:Ljava/lang/String;

    .line 839
    invoke-virtual {v3, v4}, Lage$a;->a(Ljava/lang/String;)Lage$a;

    move-result-object v3

    const/16 v4, 0xd

    .line 840
    invoke-virtual {v3, v4}, Lage$a;->a(I)Lage$a;

    move-result-object v3

    const/4 v4, 0x1

    .line 841
    invoke-virtual {v3, v4}, Lage$a;->a(Z)Lage$a;

    move-result-object v3

    .line 842
    invoke-virtual {v3}, Lage$a;->a()Lage;

    move-result-object v3

    .line 837
    invoke-virtual {v2, v3}, Lagg;->a(Lage;)V

    goto/16 :goto_0

    .line 846
    :pswitch_6
    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->aw:Lagg;

    new-instance v4, Lage$a;

    .line 847
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    iget-object v7, v3, Lcom/twitter/android/bu;->h:Ljava/lang/String;

    invoke-direct {v4, v5, v7}, Lage$a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v3, v3, Lcom/twitter/android/bu;->h:Ljava/lang/String;

    .line 848
    invoke-virtual {v4, v3}, Lage$a;->a(Ljava/lang/String;)Lage$a;

    move-result-object v3

    const-string/jumbo v4, "related_query_click"

    .line 849
    invoke-virtual {v3, v4}, Lage$a;->b(Ljava/lang/String;)Lage$a;

    move-result-object v3

    .line 850
    invoke-virtual {v3}, Lage$a;->a()Lage;

    move-result-object v3

    .line 846
    invoke-virtual {v2, v3}, Lagg;->b(Lage;)V

    .line 851
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "related_queries"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const/4 v4, 0x0

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "search"

    aput-object v4, v2, v3

    invoke-static {v6, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/twitter/android/SearchResultsFragment;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 856
    :pswitch_7
    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->aw:Lagg;

    new-instance v3, Lage$a;

    .line 857
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Lage$a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->s:Ljava/lang/String;

    .line 858
    invoke-virtual {v3, v4}, Lage$a;->a(Ljava/lang/String;)Lage$a;

    move-result-object v3

    const-string/jumbo v4, "auto_spell_correct_revert_click"

    .line 859
    invoke-virtual {v3, v4}, Lage$a;->b(Ljava/lang/String;)Lage$a;

    move-result-object v3

    .line 860
    invoke-virtual {v3}, Lage$a;->a()Lage;

    move-result-object v3

    .line 856
    invoke-virtual {v2, v3}, Lagg;->b(Lage;)V

    .line 861
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "spelling_corrections"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const/4 v4, 0x0

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "revert_click"

    aput-object v4, v2, v3

    invoke-static {v6, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/twitter/android/SearchResultsFragment;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 867
    :pswitch_8
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const-class v5, Lcom/twitter/android/SearchTerminalActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v4, "query"

    iget-object v5, p0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    .line 868
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v4, "terminal"

    const/4 v5, 0x1

    .line 869
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v4, "q_type"

    const/4 v5, 0x1

    .line 870
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    .line 871
    iget-object v4, v3, Lcom/twitter/android/bu;->i:Lcom/twitter/model/search/a;

    if-eqz v4, :cond_a

    .line 872
    const-string/jumbo v4, "query_name"

    iget-object v5, p0, Lcom/twitter/android/SearchResultsFragment;->s:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    const-string/jumbo v5, "follows"

    iget-object v7, v3, Lcom/twitter/android/bu;->i:Lcom/twitter/model/search/a;

    iget-boolean v7, v7, Lcom/twitter/model/search/a;->b:Z

    .line 873
    invoke-virtual {v4, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v4

    const-string/jumbo v5, "near"

    iget-object v7, v3, Lcom/twitter/android/bu;->i:Lcom/twitter/model/search/a;

    iget-boolean v7, v7, Lcom/twitter/model/search/a;->c:Z

    .line 874
    invoke-virtual {v4, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 875
    iget-object v4, v3, Lcom/twitter/android/bu;->i:Lcom/twitter/model/search/a;

    iget-boolean v4, v4, Lcom/twitter/model/search/a;->b:Z

    if-eqz v4, :cond_b

    .line 876
    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "follows_pivot"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string/jumbo v5, "more"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string/jumbo v5, "search"

    aput-object v5, v3, v4

    invoke-static {v6, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/twitter/android/SearchResultsFragment;->a(Ljava/lang/String;)V

    .line 883
    :cond_a
    :goto_2
    invoke-virtual {p0, v2}, Lcom/twitter/android/SearchResultsFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 878
    :cond_b
    iget-object v3, v3, Lcom/twitter/android/bu;->i:Lcom/twitter/model/search/a;

    iget-boolean v3, v3, Lcom/twitter/model/search/a;->c:Z

    if-eqz v3, :cond_a

    .line 879
    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "nearby_pivot"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string/jumbo v5, "more"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string/jumbo v5, "search"

    aput-object v5, v3, v4

    invoke-static {v6, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/twitter/android/SearchResultsFragment;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 889
    :pswitch_9
    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aP()Z

    move-result v3

    if-nez v3, :cond_0

    .line 890
    iget-object v8, v2, Lcom/twitter/android/bt$a;->j:Lcom/twitter/android/widget/TopicView;

    .line 891
    invoke-virtual {v8}, Lcom/twitter/android/widget/TopicView;->getTopicType()I

    move-result v4

    .line 892
    invoke-virtual {v8}, Lcom/twitter/android/widget/TopicView;->getTopicId()Ljava/lang/String;

    move-result-object v3

    .line 893
    invoke-virtual {v8}, Lcom/twitter/android/widget/TopicView;->getSeedHashtag()Ljava/lang/String;

    move-result-object v7

    .line 895
    invoke-static {v4}, Lcom/twitter/model/topic/TwitterTopic;->c(I)Ljava/lang/String;

    move-result-object v2

    .line 896
    new-instance v5, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v5}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 897
    iput-object v3, v5, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 898
    const/16 v6, 0x10

    iput v6, v5, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->c:I

    .line 899
    iput-object v2, v5, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->x:Ljava/lang/String;

    .line 900
    new-instance v6, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v12, p0, Lcom/twitter/android/SearchResultsFragment;->a_:J

    invoke-direct {v6, v12, v13}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 901
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    const/4 v12, 0x5

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    iget-object v14, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    aput-object v14, v12, v13

    const/4 v13, 0x1

    const-string/jumbo v14, "universal_top"

    aput-object v14, v12, v13

    const/4 v13, 0x2

    aput-object v2, v12, v13

    const/4 v2, 0x3

    const-string/jumbo v13, "event"

    aput-object v13, v12, v2

    const/4 v2, 0x4

    const-string/jumbo v13, "click"

    aput-object v13, v12, v2

    invoke-static {v12}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v11

    invoke-virtual {v6, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 903
    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    iget-boolean v9, p0, Lcom/twitter/android/SearchResultsFragment;->c:Z

    iget-boolean v11, p0, Lcom/twitter/android/SearchResultsFragment;->b:Z

    .line 904
    invoke-virtual {v6, v2, v10, v9, v11}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 905
    invoke-virtual {v2, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 903
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 906
    new-instance v2, Lcom/twitter/android/cj;

    invoke-direct {v2, p0}, Lcom/twitter/android/cj;-><init>(Landroid/support/v4/app/Fragment;)V

    .line 907
    iget-object v5, p0, Lcom/twitter/android/SearchResultsFragment;->s:Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    .line 908
    invoke-virtual {v8}, Lcom/twitter/android/widget/TopicView;->getTopicData()Lcom/twitter/android/widget/TopicView$TopicData;

    move-result-object v8

    .line 907
    invoke-virtual/range {v2 .. v8}, Lcom/twitter/android/cj;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/android/widget/TopicView$TopicData;)V

    goto/16 :goto_0

    .line 736
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_7
        :pswitch_6
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_9
        :pswitch_1
        :pswitch_1
        :pswitch_4
        :pswitch_1
        :pswitch_5
        :pswitch_9
        :pswitch_1
        :pswitch_2
        :pswitch_8
        :pswitch_2
    .end packed-switch
.end method

.method protected a(Lcbi;)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingSuperCall"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/android/bu;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x3

    .line 473
    iget v1, p0, Lcom/twitter/android/SearchResultsFragment;->k:I

    .line 474
    iget-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->ai:Z

    if-eqz v0, :cond_1

    .line 475
    if-ne v1, v6, :cond_0

    .line 476
    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aO()V

    .line 477
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bt;

    const-string/jumbo v2, "tweet"

    .line 478
    invoke-direct {p0, v2}, Lcom/twitter/android/SearchResultsFragment;->b(Ljava/lang/String;)Lcom/twitter/android/ct;

    move-result-object v2

    const-string/jumbo v3, "news"

    .line 479
    invoke-direct {p0, v3}, Lcom/twitter/android/SearchResultsFragment;->b(Ljava/lang/String;)Lcom/twitter/android/ct;

    move-result-object v3

    const-string/jumbo v4, "tweet_list_glance"

    .line 480
    invoke-direct {p0, v4}, Lcom/twitter/android/SearchResultsFragment;->b(Ljava/lang/String;)Lcom/twitter/android/ct;

    move-result-object v4

    const-string/jumbo v5, "tweet_list_popular"

    .line 481
    invoke-direct {p0, v5}, Lcom/twitter/android/SearchResultsFragment;->b(Ljava/lang/String;)Lcom/twitter/android/ct;

    move-result-object v5

    .line 477
    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/twitter/android/bt;->a(Lcom/twitter/library/view/d;Lcom/twitter/library/view/d;Lcom/twitter/library/view/d;Lcom/twitter/library/view/d;)V

    .line 483
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->I_()V

    .line 487
    :cond_1
    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->av:Lcom/twitter/android/bt;

    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbf;

    invoke-virtual {v0}, Lcbf;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/bt;->a(Landroid/database/Cursor;)V

    .line 488
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->z()Lcnk;

    move-result-object v0

    .line 489
    invoke-virtual {p0, p1}, Lcom/twitter/android/SearchResultsFragment;->b(Lcbi;)V

    .line 490
    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->b(Lcnk;)V

    .line 492
    iget-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->ai:Z

    if-eqz v0, :cond_4

    .line 493
    const/4 v0, 0x2

    if-ne v1, v0, :cond_2

    .line 494
    iput v6, p0, Lcom/twitter/android/SearchResultsFragment;->k:I

    .line 500
    :cond_2
    :goto_0
    iget-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->ad:Z

    invoke-direct {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->b(Z)V

    .line 502
    iget-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->V:Z

    if-eqz v0, :cond_3

    .line 503
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->ax()V

    .line 505
    :cond_3
    return-void

    .line 496
    :cond_4
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bt;

    invoke-virtual {v0}, Lcom/twitter/android/bt;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 497
    invoke-virtual {p0, v6}, Lcom/twitter/android/SearchResultsFragment;->a(I)Z

    goto :goto_0
.end method

.method a(Lcom/twitter/library/api/search/d;Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 628
    invoke-virtual {p1}, Lcom/twitter/library/api/search/d;->x()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->C:Ljava/lang/String;

    .line 629
    invoke-virtual {p1}, Lcom/twitter/library/api/search/d;->B()Lcom/twitter/model/topic/TwitterTopic;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->a(Lcom/twitter/model/topic/TwitterTopic;)V

    .line 630
    invoke-virtual {p1}, Lcom/twitter/library/api/search/d;->h()I

    move-result v0

    if-nez v0, :cond_1

    .line 631
    if-eqz p2, :cond_0

    .line 632
    invoke-virtual {p2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 634
    :cond_0
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    aput-object v1, v0, v4

    .line 635
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    const/4 v1, 0x2

    const-string/jumbo v2, "stream"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const/4 v2, 0x0

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "no_results"

    aput-object v2, v0, v1

    .line 634
    invoke-static {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 636
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/SearchResultsFragment;->a_:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v5, [Ljava/lang/String;

    aput-object v0, v2, v4

    .line 637
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    iget v2, p0, Lcom/twitter/android/SearchResultsFragment;->l:I

    .line 638
    invoke-static {v2}, Lcom/twitter/android/SearchResultsFragment;->c(I)Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lcom/twitter/android/SearchResultsFragment;->c:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->b:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 636
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 640
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->i:Z

    if-eqz v0, :cond_2

    .line 641
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->J_()V

    .line 643
    :cond_2
    return-void
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 593
    iput-boolean v2, p0, Lcom/twitter/android/SearchResultsFragment;->ai:Z

    .line 594
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getEmptyView()Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 595
    :goto_0
    if-eqz v1, :cond_0

    .line 596
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 599
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/SearchFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 601
    const/4 v0, 0x2

    if-ne p2, v0, :cond_3

    .line 602
    check-cast p1, Lcom/twitter/library/api/search/d;

    .line 603
    invoke-virtual {p1}, Lcom/twitter/library/api/search/d;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 604
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 605
    :cond_1
    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchResultsFragment;->a(Landroid/view/View;)V

    .line 622
    :cond_2
    :goto_1
    invoke-virtual {p1}, Lcom/twitter/library/api/search/d;->g()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->j:Z

    .line 624
    :cond_3
    return-void

    .line 594
    :cond_4
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    .line 606
    :cond_5
    const/4 v0, 0x3

    if-ne p3, v0, :cond_6

    .line 607
    invoke-virtual {p0, p1, v1}, Lcom/twitter/android/SearchResultsFragment;->a(Lcom/twitter/library/api/search/d;Landroid/view/View;)V

    .line 609
    invoke-direct {p0, p1}, Lcom/twitter/android/SearchResultsFragment;->b(Lcom/twitter/library/api/search/d;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->ad:Z

    .line 610
    iget-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->ad:Z

    if-nez v0, :cond_3

    .line 615
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->b(Z)V

    goto :goto_1

    .line 616
    :cond_6
    invoke-virtual {p1}, Lcom/twitter/library/api/search/d;->h()I

    move-result v0

    if-nez v0, :cond_2

    .line 617
    if-ne p3, v2, :cond_2

    .line 618
    iput-boolean v2, p0, Lcom/twitter/android/SearchResultsFragment;->g:Z

    goto :goto_1
.end method

.method protected a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1260
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/SearchResultsFragment;->a_:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->ah:Ljava/lang/String;

    .line 1261
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->k(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 1262
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->at:Ljava/util/List;

    .line 1263
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/util/Collection;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 1264
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->N()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/twitter/android/SearchResultsFragment;->l:I

    invoke-static {v2}, Lcom/twitter/android/SearchResultsFragment;->c(I)Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lcom/twitter/android/SearchResultsFragment;->c:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->b:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 1265
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1260
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1266
    return-void
.end method

.method protected a(I)Z
    .locals 2

    .prologue
    .line 520
    iget-wide v0, p0, Lcom/twitter/android/SearchResultsFragment;->r:J

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/SearchResultsFragment;->a(IJ)Z

    move-result v0

    return v0
.end method

.method protected ar_()V
    .locals 1

    .prologue
    .line 248
    invoke-super {p0}, Lcom/twitter/android/SearchFragment;->ar_()V

    .line 249
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->ak()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bt;

    invoke-virtual {v0}, Lcom/twitter/android/bt;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 250
    :cond_0
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->a(I)Z

    .line 252
    :cond_1
    return-void
.end method

.method protected b(J)I
    .locals 1

    .prologue
    .line 697
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bt;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/bt;->a(J)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(JLcgi;ILcom/twitter/android/cx$e;)I
    .locals 13

    .prologue
    .line 986
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    .line 987
    iget-object v12, p0, Lcom/twitter/android/SearchResultsFragment;->af:Lcom/twitter/model/util/FriendshipCache;

    .line 989
    move-object/from16 v0, p5

    iget v9, v0, Lcom/twitter/android/cx$e;->b:I

    .line 991
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v2

    if-nez v2, :cond_3

    .line 992
    invoke-virtual {v12, p1, p2}, Lcom/twitter/model/util/FriendshipCache;->a(J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 993
    invoke-virtual {v12, p1, p2}, Lcom/twitter/model/util/FriendshipCache;->k(J)Z

    move-result v2

    .line 1000
    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aP()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1001
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v5

    .line 1002
    const-string/jumbo v3, "user_rail"

    move-object v10, v3

    move-object v11, v5

    .line 1008
    :goto_1
    if-eqz v2, :cond_2

    .line 1009
    const/4 v2, 0x1

    invoke-static {v9, v2}, Lcom/twitter/model/core/g;->b(II)I

    move-result v9

    .line 1010
    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/client/p;

    new-instance v3, Lbhs;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v5

    move-wide v6, p1

    move-object/from16 v8, p3

    invoke-direct/range {v3 .. v8}, Lbhs;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    invoke-virtual {v2, v3}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 1012
    invoke-virtual {v12, p1, p2}, Lcom/twitter/model/util/FriendshipCache;->c(J)V

    .line 1013
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v11, v2, v3

    const/4 v3, 0x2

    aput-object v10, v2, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "user"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "unfollow"

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1023
    :goto_2
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, p0, Lcom/twitter/android/SearchResultsFragment;->a_:J

    invoke-direct {v3, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 1024
    const/4 v7, 0x0

    move-wide v4, p1

    move-object/from16 v6, p3

    move/from16 v8, p4

    invoke-static/range {v3 .. v8}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;JLcgi;Ljava/lang/String;I)V

    .line 1025
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-virtual {v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 1026
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    check-cast v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->t:Ljava/lang/String;

    iget v4, p0, Lcom/twitter/android/SearchResultsFragment;->l:I

    .line 1027
    invoke-static {v4}, Lcom/twitter/android/SearchResultsFragment;->c(I)Ljava/lang/String;

    move-result-object v4

    iget-boolean v5, p0, Lcom/twitter/android/SearchResultsFragment;->c:Z

    iget-boolean v6, p0, Lcom/twitter/android/SearchResultsFragment;->b:Z

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 1025
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 1032
    :goto_3
    return v9

    .line 995
    :cond_0
    invoke-static {v9}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v2

    goto/16 :goto_0

    .line 1004
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v5

    .line 1005
    const-string/jumbo v3, "user_gallery"

    move-object v10, v3

    move-object v11, v5

    goto/16 :goto_1

    .line 1016
    :cond_2
    const/4 v2, 0x1

    invoke-static {v9, v2}, Lcom/twitter/model/core/g;->a(II)I

    move-result v9

    .line 1017
    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/client/p;

    new-instance v3, Lbhq;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v5

    move-wide v6, p1

    move-object/from16 v8, p3

    invoke-direct/range {v3 .. v8}, Lbhq;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    invoke-virtual {v2, v3}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 1019
    invoke-virtual {v12, p1, p2}, Lcom/twitter/model/util/FriendshipCache;->b(J)V

    .line 1020
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v11, v2, v3

    const/4 v3, 0x2

    aput-object v10, v2, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "user"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "follow"

    aput-object v4, v2, v3

    .line 1021
    invoke-static {v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 1029
    :cond_3
    const/4 v2, 0x4

    move-object/from16 v0, p5

    iget-object v3, v0, Lcom/twitter/android/cx$e;->a:Ljava/lang/String;

    invoke-static {v4, v2, v3}, Lcom/twitter/android/al;->a(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)V

    goto :goto_3
.end method

.method public b(Landroid/view/View;)Lcom/twitter/android/cu;
    .locals 1

    .prologue
    .line 702
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/android/bt$a;

    if-eqz v0, :cond_0

    .line 703
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bt$a;

    iget-object v0, v0, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    .line 705
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 198
    invoke-super {p0}, Lcom/twitter/android/SearchFragment;->b()V

    .line 200
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v1, "summary_dialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PromptDialogFragment;

    .line 201
    if-eqz v0, :cond_0

    .line 202
    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    .line 204
    :cond_0
    return-void
.end method

.method protected b(Lcom/twitter/library/service/s;II)V
    .locals 1

    .prologue
    .line 587
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/SearchFragment;->b(Lcom/twitter/library/service/s;II)V

    .line 588
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->ai:Z

    .line 589
    return-void
.end method

.method public e()I
    .locals 1

    .prologue
    .line 1251
    iget v0, p0, Lcom/twitter/android/SearchResultsFragment;->l:I

    return v0
.end method

.method protected f()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 290
    iget-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->f:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aP()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 291
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->C:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->B:Ljava/lang/String;

    iget v2, p0, Lcom/twitter/android/SearchResultsFragment;->p:I

    .line 292
    invoke-static {v0, v1, v2}, Lcom/twitter/library/scribe/b;->a(Ljava/lang/String;Ljava/lang/String;I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 291
    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 294
    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 271
    invoke-super {p0, p1}, Lcom/twitter/android/SearchFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 272
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->k()Lcom/twitter/android/bs;

    move-result-object v0

    const-string/jumbo v1, "scribe_context"

    invoke-virtual {v0, v1}, Lcom/twitter/android/bs;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ah:Ljava/lang/String;

    .line 273
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->av:Lcom/twitter/android/bt;

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->a(Lcom/twitter/android/client/j;)Lcom/twitter/app/common/list/TwitterListFragment;

    .line 274
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->I:Lcom/twitter/android/cl;

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->I:Lcom/twitter/android/cl;

    invoke-virtual {v0, p0}, Lcom/twitter/android/cl;->a(Lcom/twitter/android/cl$b;)V

    .line 277
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->f()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->at:Ljava/util/List;

    .line 278
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->av:Lcom/twitter/android/bt;

    const-string/jumbo v1, "tweet"

    .line 279
    invoke-direct {p0, v1}, Lcom/twitter/android/SearchResultsFragment;->b(Ljava/lang/String;)Lcom/twitter/android/ct;

    move-result-object v1

    const-string/jumbo v2, "news"

    .line 280
    invoke-direct {p0, v2}, Lcom/twitter/android/SearchResultsFragment;->b(Ljava/lang/String;)Lcom/twitter/android/ct;

    move-result-object v2

    const-string/jumbo v3, "tweet_list_glance"

    .line 281
    invoke-direct {p0, v3}, Lcom/twitter/android/SearchResultsFragment;->b(Ljava/lang/String;)Lcom/twitter/android/ct;

    move-result-object v3

    const-string/jumbo v4, "tweet_list_popular"

    .line 282
    invoke-direct {p0, v4}, Lcom/twitter/android/SearchResultsFragment;->b(Ljava/lang/String;)Lcom/twitter/android/ct;

    move-result-object v4

    .line 278
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/bt;->a(Lcom/twitter/library/view/d;Lcom/twitter/library/view/d;Lcom/twitter/library/view/d;Lcom/twitter/library/view/d;)V

    .line 283
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    .line 284
    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->av:Lcom/twitter/android/bt;

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l;->a(Lcjr;)V

    .line 285
    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    const v1, 0x7f0a07f2

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchResultsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 286
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1060
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f1307ea

    if-ne v0, v1, :cond_0

    .line 1061
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1062
    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ak:Ljava/lang/String;

    .line 1063
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/SearchResultsFragment;->a_:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 1064
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x2

    aput-object v0, v2, v3

    const/4 v0, 0x3

    const-string/jumbo v3, "feedback"

    aput-object v3, v2, v0

    const/4 v0, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v2, v0

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1063
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1065
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    invoke-direct {v0, v5}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a07ed

    .line 1066
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0a40

    .line 1067
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a05e0

    .line 1068
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a07ef

    .line 1069
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 1070
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 1071
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 1072
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "summary_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1074
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const-wide/16 v4, 0x0

    const/4 v10, 0x0

    .line 157
    invoke-super {p0, p1}, Lcom/twitter/android/SearchFragment;->onCreate(Landroid/os/Bundle;)V

    .line 158
    if-eqz p1, :cond_2

    .line 159
    const-string/jumbo v0, "friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 160
    const-string/jumbo v0, "friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/util/FriendshipCache;

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->af:Lcom/twitter/model/util/FriendshipCache;

    .line 165
    :goto_0
    const-string/jumbo v0, "since"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/SearchResultsFragment;->ab:J

    .line 166
    const-string/jumbo v0, "until"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/SearchResultsFragment;->ac:J

    .line 167
    const-string/jumbo v0, "in_back_stack"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->aj:Z

    .line 168
    const-string/jumbo v0, "search_takeover"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->ad:Z

    .line 169
    const-string/jumbo v0, "event_header_available"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->ae:Z

    .line 181
    :goto_1
    new-instance v0, Lcom/twitter/android/SearchResultsFragment$a;

    invoke-direct {v0, p0}, Lcom/twitter/android/SearchResultsFragment$a;-><init>(Lcom/twitter/android/SearchResultsFragment;)V

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ag:Lcom/twitter/android/av;

    .line 182
    new-instance v0, Lcom/twitter/android/SearchResultsFragment$c;

    invoke-direct {v0, p0}, Lcom/twitter/android/SearchResultsFragment$c;-><init>(Lcom/twitter/android/SearchResultsFragment;)V

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ar:Lcom/twitter/android/SearchResultsFragment$c;

    .line 184
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    .line 185
    new-instance v0, Lcom/twitter/android/bt;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aK()Lcom/twitter/app/common/base/TwitterFragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->s:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->af:Lcom/twitter/model/util/FriendshipCache;

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->ag:Lcom/twitter/android/av;

    iget v7, p0, Lcom/twitter/android/SearchResultsFragment;->l:I

    .line 186
    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aP()Z

    move-result v8

    iget-object v9, p0, Lcom/twitter/android/SearchResultsFragment;->C:Ljava/lang/String;

    if-eqz p1, :cond_0

    const/4 v10, 0x1

    :cond_0
    iget v11, p0, Lcom/twitter/android/SearchResultsFragment;->p:I

    move-object v5, p0

    move-object v6, p0

    move-object v12, p0

    invoke-direct/range {v0 .. v12}, Lcom/twitter/android/bt;-><init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;Ljava/lang/String;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/android/av;Landroid/widget/AdapterView$OnItemClickListener;Lcom/twitter/android/cx$c;IZLjava/lang/String;ZILandroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->av:Lcom/twitter/android/bt;

    .line 191
    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aO()V

    .line 193
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lagg;->a(Landroid/app/Activity;)Lagg;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->aw:Lagg;

    .line 194
    return-void

    .line 163
    :cond_1
    new-instance v0, Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/model/util/FriendshipCache;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->af:Lcom/twitter/model/util/FriendshipCache;

    goto :goto_0

    .line 171
    :cond_2
    new-instance v0, Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/model/util/FriendshipCache;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->af:Lcom/twitter/model/util/FriendshipCache;

    .line 172
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->k()Lcom/twitter/android/bs;

    move-result-object v0

    .line 173
    const-string/jumbo v1, "since"

    invoke-virtual {v0, v1, v4, v5}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/SearchResultsFragment;->ab:J

    .line 174
    const-string/jumbo v1, "until"

    invoke-virtual {v0, v1, v4, v5}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/SearchResultsFragment;->ac:J

    .line 175
    const-string/jumbo v1, "in_back_stack"

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/SearchResultsFragment;->aj:Z

    .line 176
    const-string/jumbo v1, "search_takeover"

    invoke-virtual {v0, v1, v10}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/SearchResultsFragment;->ad:Z

    .line 177
    const-string/jumbo v1, "event_header_available"

    invoke-virtual {v0, v1, v10}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/SearchResultsFragment;->ae:Z

    .line 178
    const-string/jumbo v1, "pinnedTweetIds"

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/i;->i(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->al:Ljava/util/List;

    goto/16 :goto_1
.end method

.method public onDestroy()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 234
    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aP()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 236
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/SearchResultsFragment;->a_:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v5, [Ljava/lang/String;

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    aput-object v4, v3, v6

    iget v4, p0, Lcom/twitter/android/SearchResultsFragment;->p:I

    .line 238
    invoke-static {v4}, Lcom/twitter/model/topic/TwitterTopic;->c(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x2

    const-string/jumbo v5, "time_nav"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const/4 v5, 0x0

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string/jumbo v5, "close"

    aput-object v5, v3, v4

    .line 237
    invoke-static {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->C:Ljava/lang/String;

    .line 239
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->i(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 236
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 242
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->ar:Lcom/twitter/android/SearchResultsFragment$c;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->b(Lcom/twitter/library/service/t;)V

    .line 243
    invoke-super {p0}, Lcom/twitter/android/SearchFragment;->onDestroy()V

    .line 244
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 921
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bu;

    .line 923
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/twitter/android/GalleryActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v3, Lcom/twitter/database/schema/a$t;->a:Landroid/net/Uri;

    iget-wide v4, p0, Lcom/twitter/android/SearchResultsFragment;->a_:J

    .line 924
    invoke-static {v3, v4, v5}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "prj"

    sget-object v4, Lbtv;->a:[Ljava/lang/String;

    .line 926
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "sel"

    const-string/jumbo v4, "statuses_flags&1537 !=0 AND search_id=? AND type_id=?"

    .line 927
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "selArgs"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-wide v6, p0, Lcom/twitter/android/SearchResultsFragment;->r:J

    .line 930
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget v0, v0, Lcom/twitter/android/bu;->c:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 929
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "orderBy"

    const-string/jumbo v3, "type_id ASC, _id ASC"

    .line 931
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "id"

    .line 932
    invoke-virtual {v0, v2, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "context"

    const/4 v3, 0x2

    .line 933
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 923
    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 934
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->A:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->E()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "media_gallery"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "photo"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "click"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->a(Ljava/lang/String;)V

    .line 936
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 208
    invoke-super {p0, p1}, Lcom/twitter/android/SearchFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 209
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->af:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0}, Lcom/twitter/model/util/FriendshipCache;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    const-string/jumbo v0, "friendship_cache"

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->af:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 212
    :cond_0
    const-string/jumbo v0, "since"

    iget-wide v2, p0, Lcom/twitter/android/SearchResultsFragment;->ab:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 213
    const-string/jumbo v0, "until"

    iget-wide v2, p0, Lcom/twitter/android/SearchResultsFragment;->ac:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 214
    const-string/jumbo v0, "in_back_stack"

    iget-boolean v1, p0, Lcom/twitter/android/SearchResultsFragment;->aj:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 215
    const-string/jumbo v0, "search_takeover"

    iget-boolean v1, p0, Lcom/twitter/android/SearchResultsFragment;->ad:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 216
    const-string/jumbo v0, "event_header_available"

    iget-boolean v1, p0, Lcom/twitter/android/SearchResultsFragment;->ae:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 225
    iget-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->ai:Z

    if-nez v0, :cond_1

    .line 226
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->ar:Lcom/twitter/android/SearchResultsFragment$c;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/t;)V

    .line 228
    :cond_1
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 509
    invoke-super {p0, p1, p2}, Lcom/twitter/android/SearchFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 510
    if-eqz p2, :cond_0

    .line 514
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->b(Z)V

    .line 516
    :cond_0
    return-void
.end method

.method protected r()Z
    .locals 4

    .prologue
    .line 436
    iget v0, p0, Lcom/twitter/android/SearchResultsFragment;->l:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget-wide v0, p0, Lcom/twitter/android/SearchResultsFragment;->ab:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
