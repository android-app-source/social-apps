.class public Lcom/twitter/android/cc;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Lcom/twitter/model/core/Tweet;

.field private b:Laji;


# direct methods
.method public constructor <init>(Lcom/twitter/model/core/Tweet;Laji;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/twitter/android/cc;->a:Lcom/twitter/model/core/Tweet;

    .line 31
    iput-object p2, p0, Lcom/twitter/android/cc;->b:Laji;

    .line 32
    return-void
.end method

.method private a(Ljava/util/List;)[J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)[J"
        }
    .end annotation

    .prologue
    .line 71
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [J

    .line 72
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 73
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v2, v1

    .line 72
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 75
    :cond_0
    return-object v2
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/Tweet;Laji;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/twitter/android/cc;->a:Lcom/twitter/model/core/Tweet;

    .line 36
    iput-object p2, p0, Lcom/twitter/android/cc;->b:Laji;

    .line 37
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 41
    iget-object v0, p0, Lcom/twitter/android/cc;->a:Lcom/twitter/model/core/Tweet;

    .line 42
    if-eqz v0, :cond_0

    .line 43
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 44
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->aa()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 45
    iget-wide v2, v0, Lcom/twitter/model/core/Tweet;->b:J

    iget-object v4, v0, Lcom/twitter/model/core/Tweet;->p:Ljava/lang/String;

    move-object v6, v5

    move-object v7, v5

    invoke-static/range {v1 .. v7}, Lcom/twitter/android/ProfileActivity;->a(Landroid/content/Context;JLjava/lang/String;Lcgi;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/model/timeline/r;)V

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 46
    :cond_1
    const-string/jumbo v0, "tappable_likes_social_proof_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/twitter/android/cc;->b:Laji;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/cc;->b:Laji;

    iget-object v0, v0, Laji;->e:Ljava/util/List;

    .line 49
    :goto_1
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 52
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v7, :cond_3

    .line 54
    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move-object v4, v5

    move-object v6, v5

    move-object v7, v5

    invoke-static/range {v1 .. v7}, Lcom/twitter/android/ProfileActivity;->a(Landroid/content/Context;JLjava/lang/String;Lcgi;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/model/timeline/r;)V

    goto :goto_0

    :cond_2
    move-object v0, v5

    .line 48
    goto :goto_1

    .line 57
    :cond_3
    new-instance v2, Lcom/twitter/app/users/f;

    invoke-direct {v2}, Lcom/twitter/app/users/f;-><init>()V

    .line 58
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/twitter/app/users/f;->c(J)Lcom/twitter/app/users/f;

    move-result-object v2

    .line 59
    invoke-direct {p0, v0}, Lcom/twitter/android/cc;->a(Ljava/util/List;)[J

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/app/users/f;->a([J)Lcom/twitter/app/users/f;

    move-result-object v0

    const/16 v2, 0x2d

    .line 60
    invoke-virtual {v0, v2}, Lcom/twitter/app/users/f;->a(I)Lcom/twitter/app/users/f;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/cc;->a:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v2, Lcom/twitter/model/core/Tweet;->t:J

    .line 61
    invoke-virtual {v0, v2, v3}, Lcom/twitter/app/users/f;->b(J)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 62
    invoke-virtual {v0, v6}, Lcom/twitter/app/users/f;->c(Z)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 63
    invoke-virtual {v0, v7}, Lcom/twitter/app/users/f;->a(Z)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 64
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/f;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 57
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
