.class public Lcom/twitter/android/moments/viewmodels/g;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/viewmodels/e;


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/g;->a:Landroid/content/res/Resources;

    .line 26
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/viewmodels/MomentGuide;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/viewmodels/MomentGuide;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    const v8, 0x7f0a0960

    .line 50
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v3

    .line 51
    iget-object v0, p1, Lcom/twitter/android/moments/viewmodels/MomentGuide;->d:Ljava/util/List;

    .line 52
    iget-object v1, p1, Lcom/twitter/android/moments/viewmodels/MomentGuide;->e:Lcej;

    if-eqz v1, :cond_0

    invoke-static {}, Lwk;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 53
    new-instance v1, Lcom/twitter/android/moments/viewmodels/u;

    iget-object v2, p1, Lcom/twitter/android/moments/viewmodels/MomentGuide;->e:Lcej;

    invoke-direct {v1, v2}, Lcom/twitter/android/moments/viewmodels/u;-><init>(Lcej;)V

    invoke-virtual {v3, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 54
    new-instance v1, Lcom/twitter/android/moments/viewmodels/q;

    invoke-direct {v1}, Lcom/twitter/android/moments/viewmodels/q;-><init>()V

    invoke-virtual {v3, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 56
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 58
    invoke-virtual {p0, v0}, Lcom/twitter/android/moments/viewmodels/g;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    .line 60
    :cond_1
    iget-object v0, p1, Lcom/twitter/android/moments/viewmodels/MomentGuide;->c:Ljava/util/List;

    .line 61
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    .line 63
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/MomentGuideSection;

    .line 66
    invoke-virtual {v0}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection;->c()Lcom/twitter/model/moments/MomentGuideSectionType;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/moments/MomentGuideSectionType;->b:Lcom/twitter/model/moments/MomentGuideSectionType;

    if-ne v1, v2, :cond_6

    .line 67
    invoke-virtual {v0}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection;->a()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 68
    invoke-virtual {v0}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    .line 69
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_5

    .line 70
    invoke-virtual {v0}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/moments/viewmodels/MomentModule;

    .line 71
    invoke-virtual {v1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->e()Lcom/twitter/model/moments/DisplayStyle;

    move-result-object v6

    sget-object v7, Lcom/twitter/model/moments/DisplayStyle;->b:Lcom/twitter/model/moments/DisplayStyle;

    if-ne v6, v7, :cond_4

    .line 72
    invoke-virtual {v1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->f()Lceg;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-static {}, Lwk;->b()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 77
    new-instance v6, Lcom/twitter/android/moments/viewmodels/v;

    invoke-direct {v6, v1}, Lcom/twitter/android/moments/viewmodels/v;-><init>(Lcom/twitter/android/moments/viewmodels/MomentModule;)V

    invoke-virtual {v3, v2, v6}, Lcom/twitter/util/collection/h;->a(ILjava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 69
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 79
    :cond_3
    new-instance v6, Lcom/twitter/android/moments/viewmodels/s;

    invoke-direct {v6, v1}, Lcom/twitter/android/moments/viewmodels/s;-><init>(Lcom/twitter/android/moments/viewmodels/MomentModule;)V

    invoke-virtual {v3, v2, v6}, Lcom/twitter/util/collection/h;->a(ILjava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_2

    .line 82
    :cond_4
    new-instance v6, Lcom/twitter/android/moments/viewmodels/w;

    invoke-direct {v6, v1}, Lcom/twitter/android/moments/viewmodels/w;-><init>(Lcom/twitter/android/moments/viewmodels/MomentModule;)V

    invoke-virtual {v3, v2, v6}, Lcom/twitter/util/collection/h;->a(ILjava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_2

    .line 85
    :cond_5
    new-instance v0, Lcom/twitter/android/moments/viewmodels/q;

    invoke-direct {v0}, Lcom/twitter/android/moments/viewmodels/q;-><init>()V

    invoke-virtual {v3, v5, v0}, Lcom/twitter/util/collection/h;->a(ILjava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 90
    :cond_6
    new-instance v1, Lcom/twitter/android/moments/viewmodels/x;

    iget-object v2, p0, Lcom/twitter/android/moments/viewmodels/g;->a:Landroid/content/res/Resources;

    const v5, 0x7f0a03d9

    .line 91
    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v5, "0"

    iget-object v6, p0, Lcom/twitter/android/moments/viewmodels/g;->a:Landroid/content/res/Resources;

    .line 92
    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v2, v5, v6, v9}, Lcom/twitter/android/moments/viewmodels/x;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    new-instance v2, Lcom/twitter/android/moments/viewmodels/r;

    iget-object v5, p0, Lcom/twitter/android/moments/viewmodels/g;->a:Landroid/content/res/Resources;

    .line 94
    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5, v1}, Lcom/twitter/android/moments/viewmodels/r;-><init>(Ljava/lang/String;Lcom/twitter/android/moments/viewmodels/x;)V

    invoke-virtual {v3, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 95
    invoke-virtual {v0}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/MomentModule;

    .line 96
    new-instance v2, Lcom/twitter/android/moments/viewmodels/w;

    invoke-direct {v2, v0}, Lcom/twitter/android/moments/viewmodels/w;-><init>(Lcom/twitter/android/moments/viewmodels/MomentModule;)V

    invoke-virtual {v3, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_3

    .line 98
    :cond_7
    new-instance v0, Lcom/twitter/android/moments/viewmodels/q;

    invoke-direct {v0}, Lcom/twitter/android/moments/viewmodels/q;-><init>()V

    invoke-virtual {v3, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto/16 :goto_0

    .line 103
    :cond_8
    new-instance v0, Lcom/twitter/android/moments/viewmodels/r;

    iget-object v1, p0, Lcom/twitter/android/moments/viewmodels/g;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a03d8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v9}, Lcom/twitter/android/moments/viewmodels/r;-><init>(Ljava/lang/String;Lcom/twitter/android/moments/viewmodels/x;)V

    invoke-virtual {v3, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 104
    new-instance v0, Lcom/twitter/android/moments/viewmodels/n;

    invoke-direct {v0}, Lcom/twitter/android/moments/viewmodels/n;-><init>()V

    invoke-virtual {v3, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 106
    :cond_9
    invoke-virtual {v3}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public a(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/i;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 31
    new-instance v0, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;

    invoke-direct {v0}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/moments/viewmodels/g;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a03dd

    .line 32
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;->a(Ljava/lang/String;)Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/viewmodels/g;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a03d9

    .line 33
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;->c(Ljava/lang/String;)Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;

    move-result-object v0

    const-string/jumbo v1, "twitter://trends"

    .line 34
    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;->d(Ljava/lang/String;)Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/MomentGuideSection;

    .line 36
    new-instance v1, Lcom/twitter/android/moments/viewmodels/x;

    .line 37
    invoke-virtual {v0}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection;->f()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v4, v4, v3}, Lcom/twitter/android/moments/viewmodels/x;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 39
    new-instance v3, Lcom/twitter/android/moments/viewmodels/r;

    invoke-virtual {v0}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0, v1}, Lcom/twitter/android/moments/viewmodels/r;-><init>(Ljava/lang/String;Lcom/twitter/android/moments/viewmodels/x;)V

    invoke-virtual {v2, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 40
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/i;

    .line 41
    new-instance v3, Lcom/twitter/android/moments/viewmodels/y;

    invoke-direct {v3, v0}, Lcom/twitter/android/moments/viewmodels/y;-><init>(Lcom/twitter/model/moments/i;)V

    invoke-virtual {v2, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 43
    :cond_0
    new-instance v0, Lcom/twitter/android/moments/viewmodels/q;

    invoke-direct {v0}, Lcom/twitter/android/moments/viewmodels/q;-><init>()V

    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 44
    invoke-virtual {v2}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method
