.class Lcom/twitter/android/moments/viewmodels/b$h;
.super Lcom/twitter/android/moments/viewmodels/b$g;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/viewmodels/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "h"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/viewmodels/b;

.field private final d:Lcff;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/viewmodels/b;Lcff;)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/b$h;->a:Lcom/twitter/android/moments/viewmodels/b;

    .line 238
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/moments/viewmodels/b$g;-><init>(Lcom/twitter/android/moments/viewmodels/b;Lcfd;)V

    .line 239
    iput-object p2, p0, Lcom/twitter/android/moments/viewmodels/b$h;->d:Lcff;

    .line 240
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/viewmodels/b$j;)V
    .locals 6

    .prologue
    .line 249
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/b$j;->b()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 250
    iget-object v1, p1, Lcom/twitter/android/moments/viewmodels/b$j;->b:Ljava/util/List;

    .line 251
    iget-object v2, p0, Lcom/twitter/android/moments/viewmodels/b$h;->d:Lcff;

    iget-object v2, v2, Lcff;->a:Lcom/twitter/model/moments/r;

    iget-object v2, v2, Lcom/twitter/model/moments/r;->c:Lcom/twitter/model/moments/MomentPageDisplayMode;

    sget-object v3, Lcom/twitter/model/moments/MomentPageDisplayMode;->a:Lcom/twitter/model/moments/MomentPageDisplayMode;

    if-ne v2, v3, :cond_1

    instance-of v2, v0, Lcom/twitter/model/moments/viewmodels/c;

    if-eqz v2, :cond_1

    instance-of v2, v0, Lcom/twitter/model/moments/viewmodels/f;

    if-eqz v2, :cond_1

    .line 254
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/moments/viewmodels/b$h;->d:Lcff;

    iget-object v3, v3, Lcff;->a:Lcom/twitter/model/moments/r;

    invoke-virtual {v2, v3}, Lcom/twitter/model/moments/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 255
    check-cast v0, Lcom/twitter/model/moments/viewmodels/c;

    iget-object v1, p0, Lcom/twitter/android/moments/viewmodels/b$h;->d:Lcff;

    iget-object v1, v1, Lcff;->b:Lcom/twitter/model/moments/e;

    .line 256
    invoke-interface {v0, v1}, Lcom/twitter/model/moments/viewmodels/c;->a(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 257
    invoke-virtual {p1, v0}, Lcom/twitter/android/moments/viewmodels/b$j;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    .line 258
    instance-of v1, v0, Lcom/twitter/model/moments/viewmodels/f;

    const-string/jumbo v2, "Unable to crop the end page. Cropping the cover page should maintain its type."

    invoke-static {v1, v2}, Lcom/twitter/util/g;->a(ZLjava/lang/String;)Z

    .line 260
    check-cast v0, Lcom/twitter/model/moments/viewmodels/f;

    sget-object v1, Lcom/twitter/model/moments/MomentPageDisplayMode;->c:Lcom/twitter/model/moments/MomentPageDisplayMode;

    .line 261
    invoke-interface {v0, v1}, Lcom/twitter/model/moments/viewmodels/f;->a(Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 262
    invoke-virtual {p1, v0}, Lcom/twitter/android/moments/viewmodels/b$j;->b(Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    .line 282
    :cond_0
    :goto_0
    return-void

    .line 263
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$h;->d:Lcff;

    iget-object v0, v0, Lcff;->a:Lcom/twitter/model/moments/r;

    iget-object v0, v0, Lcom/twitter/model/moments/r;->c:Lcom/twitter/model/moments/MomentPageDisplayMode;

    sget-object v2, Lcom/twitter/model/moments/MomentPageDisplayMode;->b:Lcom/twitter/model/moments/MomentPageDisplayMode;

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$h;->d:Lcff;

    iget-object v0, v0, Lcff;->a:Lcom/twitter/model/moments/r;

    iget-object v0, v0, Lcom/twitter/model/moments/r;->e:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$h;->a:Lcom/twitter/android/moments/viewmodels/b;

    iget-object v2, p0, Lcom/twitter/android/moments/viewmodels/b$h;->d:Lcff;

    iget-object v2, v2, Lcff;->a:Lcom/twitter/model/moments/r;

    iget-object v2, v2, Lcom/twitter/model/moments/r;->e:Ljava/lang/Long;

    .line 266
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v4, Lcom/twitter/model/moments/MomentPageDisplayMode;->b:Lcom/twitter/model/moments/MomentPageDisplayMode;

    invoke-static {v0, p1, v2, v3, v4}, Lcom/twitter/android/moments/viewmodels/b;->a(Lcom/twitter/android/moments/viewmodels/b;Lcom/twitter/android/moments/viewmodels/b$j;JLcom/twitter/model/moments/MomentPageDisplayMode;)Ljava/lang/Integer;

    move-result-object v2

    .line 268
    if-eqz v2, :cond_0

    .line 269
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v3

    .line 270
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/g;

    iget-object v0, v0, Lcom/twitter/model/moments/viewmodels/g;->a:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 271
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$h;->d:Lcff;

    iget-object v0, v0, Lcff;->a:Lcom/twitter/model/moments/r;

    .line 272
    invoke-static {v3, v0}, Lcom/twitter/android/moments/viewmodels/b;->a(Ljava/util/List;Lcom/twitter/model/moments/r;)Ljava/lang/Integer;

    move-result-object v4

    .line 273
    if-eqz v4, :cond_0

    .line 275
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage;

    iget-object v5, p0, Lcom/twitter/android/moments/viewmodels/b$h;->d:Lcff;

    iget-object v5, v5, Lcff;->b:Lcom/twitter/model/moments/e;

    invoke-static {v0, v5}, Lcom/twitter/model/moments/viewmodels/h;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 277
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v3, v4, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 278
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    new-instance v2, Lcom/twitter/model/moments/viewmodels/g;

    invoke-direct {v2, v3}, Lcom/twitter/model/moments/viewmodels/g;-><init>(Ljava/util/List;)V

    invoke-interface {v1, v0, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
