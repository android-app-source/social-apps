.class public Lcom/twitter/android/moments/viewmodels/f;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/viewmodels/e;


# instance fields
.field private final a:Lcom/twitter/android/moments/viewmodels/e;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/viewmodels/e;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/f;->a:Lcom/twitter/android/moments/viewmodels/e;

    .line 25
    return-void
.end method

.method private static a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-static {p0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;

    .line 48
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;->c()Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;->a:Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;

    if-ne v1, v2, :cond_0

    .line 49
    const/4 v1, 0x0

    new-instance v2, Lcom/twitter/android/moments/viewmodels/t;

    check-cast v0, Lcom/twitter/android/moments/viewmodels/s;

    invoke-virtual {v0}, Lcom/twitter/android/moments/viewmodels/s;->a()Lcom/twitter/android/moments/viewmodels/MomentModule;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/twitter/android/moments/viewmodels/t;-><init>(Lcom/twitter/android/moments/viewmodels/MomentModule;)V

    invoke-interface {p0, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 51
    :cond_0
    return-void
.end method

.method private static b(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-static {p0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;

    .line 58
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;->c()Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;->e:Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;

    if-ne v0, v1, :cond_0

    .line 59
    const/4 v0, 0x0

    new-instance v1, Lcom/twitter/android/moments/viewmodels/o;

    invoke-direct {v1}, Lcom/twitter/android/moments/viewmodels/o;-><init>()V

    invoke-interface {p0, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 61
    :cond_0
    return-void
.end method

.method private static c(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-static {p0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;

    .line 68
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;->c()Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;->f:Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;

    if-ne v1, v2, :cond_0

    .line 69
    invoke-interface {p0, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 71
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/viewmodels/MomentGuide;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/viewmodels/MomentGuide;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/f;->a:Lcom/twitter/android/moments/viewmodels/e;

    invoke-interface {v0, p1}, Lcom/twitter/android/moments/viewmodels/e;->a(Lcom/twitter/android/moments/viewmodels/MomentGuide;)Ljava/util/List;

    move-result-object v0

    .line 31
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 32
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v1

    .line 33
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 34
    invoke-static {v1}, Lcom/twitter/android/moments/viewmodels/f;->c(Ljava/util/List;)V

    .line 35
    invoke-static {v1}, Lcom/twitter/android/moments/viewmodels/f;->a(Ljava/util/List;)V

    .line 36
    invoke-static {v1}, Lcom/twitter/android/moments/viewmodels/f;->b(Ljava/util/List;)V

    .line 37
    invoke-static {v1}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 39
    :cond_0
    return-object v0
.end method
