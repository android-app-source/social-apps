.class Lcom/twitter/android/moments/viewmodels/b$d;
.super Lcom/twitter/android/moments/viewmodels/b$g;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/viewmodels/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/viewmodels/b;

.field private final d:Lcew;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/viewmodels/b;Lcew;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/b$d;->a:Lcom/twitter/android/moments/viewmodels/b;

    .line 217
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/moments/viewmodels/b$g;-><init>(Lcom/twitter/android/moments/viewmodels/b;Lcfd;)V

    .line 218
    iput-object p2, p0, Lcom/twitter/android/moments/viewmodels/b$d;->d:Lcew;

    .line 219
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/viewmodels/b$j;)V
    .locals 4

    .prologue
    .line 223
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$d;->a:Lcom/twitter/android/moments/viewmodels/b;

    iget-object v1, p0, Lcom/twitter/android/moments/viewmodels/b$d;->d:Lcew;

    iget-wide v2, v1, Lcew;->c:J

    sget-object v1, Lcom/twitter/model/moments/MomentPageDisplayMode;->b:Lcom/twitter/model/moments/MomentPageDisplayMode;

    invoke-static {v0, p1, v2, v3, v1}, Lcom/twitter/android/moments/viewmodels/b;->a(Lcom/twitter/android/moments/viewmodels/b;Lcom/twitter/android/moments/viewmodels/b$j;JLcom/twitter/model/moments/MomentPageDisplayMode;)Ljava/lang/Integer;

    move-result-object v1

    .line 225
    iget-object v2, p1, Lcom/twitter/android/moments/viewmodels/b$j;->b:Ljava/util/List;

    .line 226
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/g;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/g;->a()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 227
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/g;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/g;->a()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 228
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 230
    :cond_0
    return-void
.end method
