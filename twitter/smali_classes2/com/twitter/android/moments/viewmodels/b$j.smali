.class Lcom/twitter/android/moments/viewmodels/b$j;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/viewmodels/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "j"
.end annotation


# instance fields
.field public final a:Lcom/twitter/model/moments/Moment$a;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcfn;

.field private d:Lcom/twitter/model/moments/viewmodels/MomentPage;

.field private e:Lcom/twitter/model/moments/viewmodels/MomentPage;


# direct methods
.method constructor <init>(Lcom/twitter/model/moments/viewmodels/a;)V
    .locals 2

    .prologue
    .line 472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 466
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$j;->b:Ljava/util/List;

    .line 473
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/a;->a()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/model/moments/Moment$a;->a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$j;->a:Lcom/twitter/model/moments/Moment$a;

    .line 474
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/a;->c()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$j;->d:Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 475
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/a;->d()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$j;->e:Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 476
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/a;->e()Lcfn;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$j;->c:Lcfn;

    .line 477
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$j;->b:Ljava/util/List;

    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/a;->h()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 478
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/moments/viewmodels/a;
    .locals 3

    .prologue
    .line 482
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$j;->b:Ljava/util/List;

    invoke-static {v0}, Lcpt;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    .line 483
    new-instance v2, Lcom/twitter/model/moments/viewmodels/a$a;

    invoke-direct {v2}, Lcom/twitter/model/moments/viewmodels/a$a;-><init>()V

    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$j;->a:Lcom/twitter/model/moments/Moment$a;

    .line 484
    invoke-virtual {v0}, Lcom/twitter/model/moments/Moment$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/Moment;

    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/a$a;->a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/model/moments/viewmodels/a$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/moments/viewmodels/b$j;->d:Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 485
    invoke-virtual {v0, v2}, Lcom/twitter/model/moments/viewmodels/a$a;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/model/moments/viewmodels/a$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/moments/viewmodels/b$j;->e:Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 486
    invoke-virtual {v0, v2}, Lcom/twitter/model/moments/viewmodels/a$a;->b(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/model/moments/viewmodels/a$a;

    move-result-object v0

    .line 487
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/viewmodels/a$a;->a(Ljava/util/List;)Lcom/twitter/model/moments/viewmodels/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/viewmodels/b$j;->c:Lcfn;

    .line 488
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/viewmodels/a$a;->a(Lcfn;)Lcom/twitter/model/moments/viewmodels/a$a;

    move-result-object v0

    .line 489
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/a$a;->a()Lcom/twitter/model/moments/viewmodels/a;

    move-result-object v0

    .line 483
    return-object v0
.end method

.method public a(Lcom/twitter/model/moments/viewmodels/MomentPage;)V
    .locals 0

    .prologue
    .line 503
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/b$j;->d:Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 504
    return-void
.end method

.method public b()Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$j;->d:Lcom/twitter/model/moments/viewmodels/MomentPage;

    return-object v0
.end method

.method public b(Lcom/twitter/model/moments/viewmodels/MomentPage;)V
    .locals 0

    .prologue
    .line 507
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/b$j;->e:Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 508
    return-void
.end method

.method public c()Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$j;->e:Lcom/twitter/model/moments/viewmodels/MomentPage;

    return-object v0
.end method
