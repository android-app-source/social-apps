.class public Lcom/twitter/android/moments/viewmodels/b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/viewmodels/b$j;,
        Lcom/twitter/android/moments/viewmodels/b$k;,
        Lcom/twitter/android/moments/viewmodels/b$g;,
        Lcom/twitter/android/moments/viewmodels/b$i;,
        Lcom/twitter/android/moments/viewmodels/b$b;,
        Lcom/twitter/android/moments/viewmodels/b$c;,
        Lcom/twitter/android/moments/viewmodels/b$h;,
        Lcom/twitter/android/moments/viewmodels/b$d;,
        Lcom/twitter/android/moments/viewmodels/b$f;,
        Lcom/twitter/android/moments/viewmodels/b$e;,
        Lcom/twitter/android/moments/viewmodels/b$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/moments/viewmodels/b$g;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/twitter/model/moments/viewmodels/a;


# direct methods
.method public constructor <init>(Lcom/twitter/model/moments/viewmodels/a;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/viewmodels/b;->a:Ljava/util/List;

    .line 47
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/b;->b:Lcom/twitter/model/moments/viewmodels/a;

    .line 48
    return-void
.end method

.method private a(Lces;Lcom/twitter/model/moments/viewmodels/g;)Lcom/twitter/android/moments/viewmodels/b$g;
    .locals 1

    .prologue
    .line 90
    if-eqz p2, :cond_0

    .line 91
    new-instance v0, Lcom/twitter/android/moments/viewmodels/b$a;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/android/moments/viewmodels/b$a;-><init>(Lcom/twitter/android/moments/viewmodels/b;Lces;Lcom/twitter/model/moments/viewmodels/g;)V

    .line 93
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcex;Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/android/moments/viewmodels/b$g;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 120
    .line 121
    iget-object v0, p1, Lcex;->d:Lcez;

    if-eqz v0, :cond_2

    if-nez p2, :cond_2

    .line 123
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b;->b:Lcom/twitter/model/moments/viewmodels/a;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/a;->h()Ljava/util/List;

    move-result-object v0

    .line 124
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/g;

    .line 125
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/g;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 126
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v4

    iget-object v4, v4, Lcom/twitter/model/moments/r;->e:Ljava/lang/Long;

    iget-object v5, p1, Lcex;->d:Lcez;

    iget-object v5, v5, Lcez;->b:Lcom/twitter/model/moments/r;

    iget-object v5, v5, Lcom/twitter/model/moments/r;->e:Ljava/lang/Long;

    invoke-static {v4, v5}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 127
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v4

    iget-object v4, v4, Lcom/twitter/model/moments/r;->f:Ljava/lang/Long;

    iget-object v5, p1, Lcex;->d:Lcez;

    iget-object v5, v5, Lcez;->b:Lcom/twitter/model/moments/r;

    iget-object v5, v5, Lcom/twitter/model/moments/r;->f:Ljava/lang/Long;

    invoke-static {v4, v5}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object p2, v0

    .line 129
    goto :goto_0

    :cond_2
    move-object v0, p2

    .line 134
    if-eqz v0, :cond_3

    instance-of v2, v0, Lcom/twitter/model/moments/viewmodels/f;

    if-eqz v2, :cond_3

    .line 135
    new-instance v1, Lcom/twitter/android/moments/viewmodels/b$e;

    check-cast v0, Lcom/twitter/model/moments/viewmodels/f;

    invoke-direct {v1, p0, p1, v0}, Lcom/twitter/android/moments/viewmodels/b$e;-><init>(Lcom/twitter/android/moments/viewmodels/b;Lcex;Lcom/twitter/model/moments/viewmodels/f;)V

    move-object v0, v1

    .line 139
    :goto_1
    return-object v0

    .line 136
    :cond_3
    iget-object v0, p1, Lcex;->d:Lcez;

    if-nez v0, :cond_4

    .line 137
    new-instance v0, Lcom/twitter/android/moments/viewmodels/b$e;

    invoke-direct {v0, p0, p1, v1}, Lcom/twitter/android/moments/viewmodels/b$e;-><init>(Lcom/twitter/android/moments/viewmodels/b;Lcex;Lcom/twitter/model/moments/viewmodels/f;)V

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 139
    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/android/moments/viewmodels/b;)Lcom/twitter/model/moments/viewmodels/a;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b;->b:Lcom/twitter/model/moments/viewmodels/a;

    return-object v0
.end method

.method private a(Lcom/twitter/android/moments/viewmodels/b$j;JLcom/twitter/model/moments/MomentPageDisplayMode;)Ljava/lang/Integer;
    .locals 6

    .prologue
    .line 373
    iget-object v2, p1, Lcom/twitter/android/moments/viewmodels/b$j;->b:Ljava/util/List;

    .line 374
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 375
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/g;

    .line 376
    invoke-static {v0}, Lcom/twitter/android/moments/viewmodels/z;->a(Lcom/twitter/model/moments/viewmodels/g;)Ljava/lang/Long;

    move-result-object v3

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 377
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/g;->b()Lcom/twitter/model/moments/MomentPageDisplayMode;

    move-result-object v0

    if-ne v0, p4, :cond_0

    .line 378
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 381
    :goto_1
    return-object v0

    .line 374
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 381
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Lcom/twitter/android/moments/viewmodels/b$j;Lcfi;)Ljava/lang/Integer;
    .locals 6

    .prologue
    .line 409
    iget-object v2, p1, Lcom/twitter/android/moments/viewmodels/b$j;->b:Ljava/util/List;

    .line 410
    iget v0, p2, Lcfi;->c:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 411
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 422
    :goto_0
    return-object v0

    .line 413
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 414
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/g;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/g;->a()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 415
    iget-wide v4, p2, Lcfi;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/twitter/android/moments/viewmodels/b;->b(Lcom/twitter/model/moments/viewmodels/MomentPage;Ljava/lang/Long;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 416
    iget v0, p2, Lcfi;->c:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 417
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 419
    :cond_1
    add-int/lit8 v0, v1, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 413
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 422
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/moments/viewmodels/b;Lcom/twitter/android/moments/viewmodels/b$j;JLcom/twitter/model/moments/MomentPageDisplayMode;)Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/moments/viewmodels/b;->a(Lcom/twitter/android/moments/viewmodels/b$j;JLcom/twitter/model/moments/MomentPageDisplayMode;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/viewmodels/b;Lcom/twitter/android/moments/viewmodels/b$j;Lcfi;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/moments/viewmodels/b;->a(Lcom/twitter/android/moments/viewmodels/b$j;Lcfi;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ljava/util/List;Lcom/twitter/model/moments/r;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 41
    invoke-static {p0, p1}, Lcom/twitter/android/moments/viewmodels/b;->b(Ljava/util/List;Lcom/twitter/model/moments/r;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/twitter/model/moments/viewmodels/MomentPage;Ljava/lang/Long;)Z
    .locals 1

    .prologue
    .line 397
    invoke-static {p0}, Lcom/twitter/model/moments/viewmodels/h;->b(Lcom/twitter/model/moments/viewmodels/MomentPage;)Ljava/lang/Long;

    move-result-object v0

    .line 398
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/util/List;Lcom/twitter/model/moments/r;)Ljava/lang/Integer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ">;",
            "Lcom/twitter/model/moments/r;",
            ")",
            "Ljava/lang/Integer;"
        }
    .end annotation

    .prologue
    .line 387
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 388
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 389
    iget-object v2, p1, Lcom/twitter/model/moments/r;->e:Ljava/lang/Long;

    invoke-static {v0, v2}, Lcom/twitter/android/moments/viewmodels/b;->b(Lcom/twitter/model/moments/viewmodels/MomentPage;Ljava/lang/Long;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/twitter/model/moments/r;->f:Ljava/lang/Long;

    invoke-static {v0, v2}, Lcom/twitter/android/moments/viewmodels/b;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;Ljava/lang/Long;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 393
    :goto_1
    return-object v0

    .line 387
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 393
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static b(Lcom/twitter/model/moments/viewmodels/MomentPage;Ljava/lang/Long;)Z
    .locals 2

    .prologue
    .line 402
    instance-of v0, p0, Lcom/twitter/model/moments/viewmodels/n;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/twitter/model/moments/viewmodels/n;

    .line 403
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/n;->t()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 402
    :goto_0
    return v0

    .line 403
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcfd;Lcom/twitter/model/moments/viewmodels/g;)Lcom/twitter/android/moments/viewmodels/b;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 61
    .line 62
    instance-of v1, p1, Lcex;

    if-eqz v1, :cond_3

    .line 63
    check-cast p1, Lcex;

    .line 64
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/twitter/model/moments/viewmodels/g;->a()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 65
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/twitter/android/moments/viewmodels/b;->a(Lcex;Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/android/moments/viewmodels/b$g;

    move-result-object v0

    .line 81
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    .line 82
    iget-object v1, p0, Lcom/twitter/android/moments/viewmodels/b;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    :cond_2
    return-object p0

    .line 66
    :cond_3
    instance-of v1, p1, Lces;

    if-eqz v1, :cond_4

    .line 67
    check-cast p1, Lces;

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/moments/viewmodels/b;->a(Lces;Lcom/twitter/model/moments/viewmodels/g;)Lcom/twitter/android/moments/viewmodels/b$g;

    move-result-object v0

    goto :goto_0

    .line 68
    :cond_4
    instance-of v1, p1, Lcfc;

    if-eqz v1, :cond_5

    .line 69
    new-instance v0, Lcom/twitter/android/moments/viewmodels/b$f;

    check-cast p1, Lcfc;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/moments/viewmodels/b$f;-><init>(Lcom/twitter/android/moments/viewmodels/b;Lcfc;)V

    goto :goto_0

    .line 70
    :cond_5
    instance-of v1, p1, Lcew;

    if-eqz v1, :cond_6

    .line 71
    new-instance v0, Lcom/twitter/android/moments/viewmodels/b$d;

    check-cast p1, Lcew;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/moments/viewmodels/b$d;-><init>(Lcom/twitter/android/moments/viewmodels/b;Lcew;)V

    goto :goto_0

    .line 72
    :cond_6
    instance-of v1, p1, Lcfj;

    if-eqz v1, :cond_7

    .line 73
    new-instance v0, Lcom/twitter/android/moments/viewmodels/b$i;

    check-cast p1, Lcfj;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/moments/viewmodels/b$i;-><init>(Lcom/twitter/android/moments/viewmodels/b;Lcfj;)V

    goto :goto_0

    .line 74
    :cond_7
    instance-of v1, p1, Lcff;

    if-eqz v1, :cond_8

    .line 75
    new-instance v0, Lcom/twitter/android/moments/viewmodels/b$h;

    check-cast p1, Lcff;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/moments/viewmodels/b$h;-><init>(Lcom/twitter/android/moments/viewmodels/b;Lcff;)V

    goto :goto_0

    .line 76
    :cond_8
    instance-of v1, p1, Lcet;

    if-eqz v1, :cond_9

    .line 77
    new-instance v0, Lcom/twitter/android/moments/viewmodels/b$b;

    check-cast p1, Lcet;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/moments/viewmodels/b$b;-><init>(Lcom/twitter/android/moments/viewmodels/b;Lcet;)V

    goto :goto_0

    .line 78
    :cond_9
    instance-of v1, p1, Lceu;

    if-eqz v1, :cond_1

    .line 79
    new-instance v0, Lcom/twitter/android/moments/viewmodels/b$c;

    check-cast p1, Lceu;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/moments/viewmodels/b$c;-><init>(Lcom/twitter/android/moments/viewmodels/b;Lceu;)V

    goto :goto_0
.end method

.method public a()Lcom/twitter/model/moments/viewmodels/a;
    .locals 3

    .prologue
    .line 427
    new-instance v1, Lcom/twitter/android/moments/viewmodels/b$j;

    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b;->b:Lcom/twitter/model/moments/viewmodels/a;

    invoke-direct {v1, v0}, Lcom/twitter/android/moments/viewmodels/b$j;-><init>(Lcom/twitter/model/moments/viewmodels/a;)V

    .line 428
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/b$g;

    .line 429
    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/viewmodels/b$g;->a(Lcom/twitter/android/moments/viewmodels/b$j;)V

    goto :goto_0

    .line 431
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/android/moments/viewmodels/b$j;->a()Lcom/twitter/model/moments/viewmodels/a;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/model/moments/viewmodels/a;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/b;->b:Lcom/twitter/model/moments/viewmodels/a;

    .line 52
    return-void
.end method

.method public b()Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcfd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 436
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b;->a:Ljava/util/List;

    new-instance v1, Lcom/twitter/android/moments/viewmodels/b$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/viewmodels/b$1;-><init>(Lcom/twitter/android/moments/viewmodels/b;)V

    invoke-static {v0, v1}, Lcpt;->a(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method
