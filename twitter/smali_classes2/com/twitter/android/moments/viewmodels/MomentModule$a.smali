.class public abstract Lcom/twitter/android/moments/viewmodels/MomentModule$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/viewmodels/MomentModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40c
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/android/moments/viewmodels/MomentModule;",
        "B:",
        "Lcom/twitter/android/moments/viewmodels/MomentModule$a;",
        ">",
        "Lcom/twitter/util/object/i",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/model/moments/Moment;

.field private b:Lcfn;

.field private c:Lcom/twitter/model/moments/DisplayStyle;

.field private d:Ljava/lang/String;

.field private e:Lcom/twitter/model/core/Tweet;

.field private f:Lcom/twitter/model/moments/u;

.field private g:Lceg;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/viewmodels/MomentModule$a;)Lcom/twitter/model/moments/Moment;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/MomentModule$a;->a:Lcom/twitter/model/moments/Moment;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/viewmodels/MomentModule$a;)Lcfn;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/MomentModule$a;->b:Lcfn;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/viewmodels/MomentModule$a;)Lcom/twitter/model/moments/DisplayStyle;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/MomentModule$a;->c:Lcom/twitter/model/moments/DisplayStyle;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/moments/viewmodels/MomentModule$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/MomentModule$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/moments/viewmodels/MomentModule$a;)Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/MomentModule$a;->e:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/moments/viewmodels/MomentModule$a;)Lcom/twitter/model/moments/u;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/MomentModule$a;->f:Lcom/twitter/model/moments/u;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/moments/viewmodels/MomentModule$a;)Lceg;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/MomentModule$a;->g:Lceg;

    return-object v0
.end method


# virtual methods
.method public a(Lceg;)Lcom/twitter/android/moments/viewmodels/MomentModule$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lceg;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 133
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/MomentModule$a;->g:Lceg;

    .line 134
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/MomentModule$a;

    return-object v0
.end method

.method public a(Lcfn;)Lcom/twitter/android/moments/viewmodels/MomentModule$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcfn;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 103
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/MomentModule$a;->b:Lcfn;

    .line 104
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/MomentModule$a;

    return-object v0
.end method

.method public a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/moments/viewmodels/MomentModule$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/Tweet;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 121
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/MomentModule$a;->e:Lcom/twitter/model/core/Tweet;

    .line 122
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/MomentModule$a;

    return-object v0
.end method

.method public a(Lcom/twitter/model/moments/DisplayStyle;)Lcom/twitter/android/moments/viewmodels/MomentModule$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/DisplayStyle;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 109
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/MomentModule$a;->c:Lcom/twitter/model/moments/DisplayStyle;

    .line 110
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/MomentModule$a;

    return-object v0
.end method

.method public a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/android/moments/viewmodels/MomentModule$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/Moment;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 97
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/MomentModule$a;->a:Lcom/twitter/model/moments/Moment;

    .line 98
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/MomentModule$a;

    return-object v0
.end method

.method public a(Lcom/twitter/model/moments/u;)Lcom/twitter/android/moments/viewmodels/MomentModule$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/u;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 127
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/MomentModule$a;->f:Lcom/twitter/model/moments/u;

    .line 128
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/MomentModule$a;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/android/moments/viewmodels/MomentModule$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 115
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/MomentModule$a;->d:Ljava/lang/String;

    .line 116
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/MomentModule$a;

    return-object v0
.end method
