.class public Lcom/twitter/android/moments/viewmodels/ag;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/viewmodels/d;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 18
    invoke-static {}, Lbrz;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "android_fab_new_list_moment_collection_5378"

    .line 19
    invoke-static {v1}, Lcoi;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 20
    new-instance v1, Lcom/twitter/android/moments/viewmodels/p;

    invoke-direct {v1}, Lcom/twitter/android/moments/viewmodels/p;-><init>()V

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 22
    :cond_0
    invoke-virtual {v0, p1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method
