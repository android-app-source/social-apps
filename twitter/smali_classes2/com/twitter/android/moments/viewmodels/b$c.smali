.class Lcom/twitter/android/moments/viewmodels/b$c;
.super Lcom/twitter/android/moments/viewmodels/b$g;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/viewmodels/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/viewmodels/b;

.field private final d:Lceu;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/viewmodels/b;Lceu;)V
    .locals 0

    .prologue
    .line 289
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/b$c;->a:Lcom/twitter/android/moments/viewmodels/b;

    .line 290
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/moments/viewmodels/b$g;-><init>(Lcom/twitter/android/moments/viewmodels/b;Lcfd;)V

    .line 291
    iput-object p2, p0, Lcom/twitter/android/moments/viewmodels/b$c;->d:Lceu;

    .line 292
    return-void
.end method


# virtual methods
.method a(Lcom/twitter/android/moments/viewmodels/b$j;)V
    .locals 6

    .prologue
    .line 296
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$c;->d:Lceu;

    iget-object v0, v0, Lceu;->a:Lcom/twitter/model/moments/r;

    iget-object v0, v0, Lcom/twitter/model/moments/r;->e:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 297
    iget-object v1, p1, Lcom/twitter/android/moments/viewmodels/b$j;->b:Ljava/util/List;

    .line 298
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$c;->a:Lcom/twitter/android/moments/viewmodels/b;

    iget-object v2, p0, Lcom/twitter/android/moments/viewmodels/b$c;->d:Lceu;

    iget-object v2, v2, Lceu;->a:Lcom/twitter/model/moments/r;

    iget-object v2, v2, Lcom/twitter/model/moments/r;->e:Ljava/lang/Long;

    .line 299
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v4, Lcom/twitter/model/moments/MomentPageDisplayMode;->b:Lcom/twitter/model/moments/MomentPageDisplayMode;

    invoke-static {v0, p1, v2, v3, v4}, Lcom/twitter/android/moments/viewmodels/b;->a(Lcom/twitter/android/moments/viewmodels/b;Lcom/twitter/android/moments/viewmodels/b$j;JLcom/twitter/model/moments/MomentPageDisplayMode;)Ljava/lang/Integer;

    move-result-object v2

    .line 301
    if-eqz v2, :cond_0

    .line 302
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v3

    .line 303
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/g;

    iget-object v0, v0, Lcom/twitter/model/moments/viewmodels/g;->a:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 304
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$c;->d:Lceu;

    iget-object v0, v0, Lceu;->a:Lcom/twitter/model/moments/r;

    .line 305
    invoke-static {v3, v0}, Lcom/twitter/android/moments/viewmodels/b;->a(Ljava/util/List;Lcom/twitter/model/moments/r;)Ljava/lang/Integer;

    move-result-object v4

    .line 306
    if-eqz v4, :cond_0

    .line 308
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage;

    iget-object v5, p0, Lcom/twitter/android/moments/viewmodels/b$c;->d:Lceu;

    iget-object v5, v5, Lceu;->b:Lcom/twitter/model/moments/w;

    invoke-static {v0, v5}, Lcom/twitter/model/moments/viewmodels/h;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/moments/w;)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 310
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v3, v4, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 311
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    new-instance v2, Lcom/twitter/model/moments/viewmodels/g;

    invoke-direct {v2, v3}, Lcom/twitter/model/moments/viewmodels/g;-><init>(Ljava/util/List;)V

    invoke-interface {v1, v0, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 315
    :cond_0
    return-void
.end method
