.class public Lcom/twitter/android/moments/viewmodels/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/viewmodels/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/twitter/android/moments/viewmodels/MomentGuideSection;)Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;
    .locals 5

    .prologue
    .line 87
    new-instance v0, Lcom/twitter/android/moments/viewmodels/x;

    invoke-virtual {p0}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 88
    invoke-virtual {p0}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection;->f()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/moments/viewmodels/x;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    return-object v0
.end method

.method private static a()Lcom/twitter/android/moments/viewmodels/q;
    .locals 1

    .prologue
    .line 93
    new-instance v0, Lcom/twitter/android/moments/viewmodels/q;

    invoke-direct {v0}, Lcom/twitter/android/moments/viewmodels/q;-><init>()V

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Lcom/twitter/android/moments/viewmodels/r;
    .locals 2

    .prologue
    .line 72
    new-instance v0, Lcom/twitter/android/moments/viewmodels/r;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/moments/viewmodels/r;-><init>(Ljava/lang/String;Lcom/twitter/android/moments/viewmodels/x;)V

    return-object v0
.end method

.method private static a(Lcom/twitter/android/moments/viewmodels/MomentGuideSection;I)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    invoke-virtual {p0}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/MomentModule;

    invoke-static {v0}, Lcom/twitter/android/moments/viewmodels/c;->a(Lcom/twitter/android/moments/viewmodels/MomentModule;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 62
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 63
    goto :goto_0
.end method

.method private static a(Lcom/twitter/android/moments/viewmodels/MomentModule;)Z
    .locals 2

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/twitter/android/moments/viewmodels/MomentModule;->e()Lcom/twitter/model/moments/DisplayStyle;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/moments/DisplayStyle;->b:Lcom/twitter/model/moments/DisplayStyle;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/twitter/android/moments/viewmodels/MomentModule;)Lcom/twitter/android/moments/viewmodels/w;
    .locals 1

    .prologue
    .line 77
    new-instance v0, Lcom/twitter/android/moments/viewmodels/w;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/viewmodels/w;-><init>(Lcom/twitter/android/moments/viewmodels/MomentModule;)V

    return-object v0
.end method

.method private static c(Lcom/twitter/android/moments/viewmodels/MomentModule;)Lcom/twitter/android/moments/viewmodels/s;
    .locals 1

    .prologue
    .line 82
    new-instance v0, Lcom/twitter/android/moments/viewmodels/s;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/viewmodels/s;-><init>(Lcom/twitter/android/moments/viewmodels/MomentModule;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/viewmodels/MomentGuide;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/viewmodels/MomentGuide;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 53
    iget-object v3, p1, Lcom/twitter/android/moments/viewmodels/MomentGuide;->c:Ljava/util/List;

    .line 54
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 55
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/MomentGuideSection;

    .line 56
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {p0, v0, v1, v4}, Lcom/twitter/android/moments/viewmodels/c;->a(Lcom/twitter/android/moments/viewmodels/MomentGuideSection;II)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    .line 54
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 58
    :cond_0
    invoke-virtual {v2}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/MomentGuideSection;II)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/viewmodels/MomentGuideSection;",
            "II)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection;->b()Ljava/lang/String;

    move-result-object v0

    .line 27
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 29
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1, p2}, Lcom/twitter/android/moments/viewmodels/c;->a(Lcom/twitter/android/moments/viewmodels/MomentGuideSection;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 30
    invoke-static {v0}, Lcom/twitter/android/moments/viewmodels/c;->a(Ljava/lang/String;)Lcom/twitter/android/moments/viewmodels/r;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 32
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/MomentModule;

    .line 33
    invoke-static {v0}, Lcom/twitter/android/moments/viewmodels/c;->a(Lcom/twitter/android/moments/viewmodels/MomentModule;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 34
    invoke-static {v0}, Lcom/twitter/android/moments/viewmodels/c;->c(Lcom/twitter/android/moments/viewmodels/MomentModule;)Lcom/twitter/android/moments/viewmodels/s;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 36
    :cond_1
    invoke-static {v0}, Lcom/twitter/android/moments/viewmodels/c;->b(Lcom/twitter/android/moments/viewmodels/MomentModule;)Lcom/twitter/android/moments/viewmodels/w;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 39
    :cond_2
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 40
    invoke-static {p1}, Lcom/twitter/android/moments/viewmodels/c;->a(Lcom/twitter/android/moments/viewmodels/MomentGuideSection;)Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 43
    :cond_3
    add-int/lit8 v0, p3, -0x1

    if-ge p2, v0, :cond_4

    .line 44
    invoke-static {}, Lcom/twitter/android/moments/viewmodels/c;->a()Lcom/twitter/android/moments/viewmodels/q;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 46
    :cond_4
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method
