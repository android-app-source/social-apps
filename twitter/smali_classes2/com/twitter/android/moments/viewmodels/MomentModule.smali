.class public abstract Lcom/twitter/android/moments/viewmodels/MomentModule;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/viewmodels/MomentModule$a;,
        Lcom/twitter/android/moments/viewmodels/MomentModule$Type;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/model/moments/Moment;

.field private final b:Lcfn;

.field private final c:Lcom/twitter/model/moments/DisplayStyle;

.field protected final d:Lcom/twitter/model/core/Tweet;

.field private final e:Ljava/lang/String;

.field private final f:Lcom/twitter/model/moments/u;

.field private final g:Lceg;


# direct methods
.method protected constructor <init>(Lcom/twitter/android/moments/viewmodels/MomentModule$a;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule$a;->a(Lcom/twitter/android/moments/viewmodels/MomentModule$a;)Lcom/twitter/model/moments/Moment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/viewmodels/MomentModule;->a:Lcom/twitter/model/moments/Moment;

    .line 34
    invoke-static {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule$a;->b(Lcom/twitter/android/moments/viewmodels/MomentModule$a;)Lcfn;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/viewmodels/MomentModule;->b:Lcfn;

    .line 35
    invoke-static {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule$a;->c(Lcom/twitter/android/moments/viewmodels/MomentModule$a;)Lcom/twitter/model/moments/DisplayStyle;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/viewmodels/MomentModule;->c:Lcom/twitter/model/moments/DisplayStyle;

    .line 36
    invoke-static {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule$a;->d(Lcom/twitter/android/moments/viewmodels/MomentModule$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/viewmodels/MomentModule;->e:Ljava/lang/String;

    .line 37
    invoke-static {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule$a;->e(Lcom/twitter/android/moments/viewmodels/MomentModule$a;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/viewmodels/MomentModule;->d:Lcom/twitter/model/core/Tweet;

    .line 38
    invoke-static {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule$a;->f(Lcom/twitter/android/moments/viewmodels/MomentModule$a;)Lcom/twitter/model/moments/u;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/viewmodels/MomentModule;->f:Lcom/twitter/model/moments/u;

    .line 39
    invoke-static {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule$a;->g(Lcom/twitter/android/moments/viewmodels/MomentModule$a;)Lceg;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/viewmodels/MomentModule;->g:Lceg;

    .line 40
    return-void
.end method


# virtual methods
.method public abstract a()Lcom/twitter/android/moments/viewmodels/MomentModule$Type;
.end method

.method public b()Lcom/twitter/model/moments/Moment;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/MomentModule;->a:Lcom/twitter/model/moments/Moment;

    return-object v0
.end method

.method public d()Lcfn;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/MomentModule;->b:Lcfn;

    return-object v0
.end method

.method public e()Lcom/twitter/model/moments/DisplayStyle;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/MomentModule;->c:Lcom/twitter/model/moments/DisplayStyle;

    return-object v0
.end method

.method public f()Lceg;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/MomentModule;->g:Lceg;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/MomentModule;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/moments/viewmodels/MomentModule;->a:Lcom/twitter/model/moments/Moment;

    iget-object v1, v1, Lcom/twitter/model/moments/Moment;->i:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public h()Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/MomentModule;->d:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method public i()Lcom/twitter/model/moments/u;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/MomentModule;->f:Lcom/twitter/model/moments/u;

    return-object v0
.end method
