.class public Lcom/twitter/android/moments/viewmodels/y;
.super Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;
.source "Twttr"


# instance fields
.field private final b:Lcom/twitter/model/moments/i;


# direct methods
.method public constructor <init>(Lcom/twitter/model/moments/i;)V
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;->j:Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;-><init>(Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;)V

    .line 17
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/y;->b:Lcom/twitter/model/moments/i;

    .line 18
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/moments/i;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/y;->b:Lcom/twitter/model/moments/i;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/y;->b:Lcom/twitter/model/moments/i;

    iget-object v0, v0, Lcom/twitter/model/moments/i;->b:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/y;->b:Lcom/twitter/model/moments/i;

    iget-object v0, v0, Lcom/twitter/model/moments/i;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/y;->b:Lcom/twitter/model/moments/i;

    iget-object v0, v0, Lcom/twitter/model/moments/i;->c:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/topic/trends/TrendBadge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/y;->b:Lcom/twitter/model/moments/i;

    iget-object v0, v0, Lcom/twitter/model/moments/i;->i:Ljava/util/List;

    return-object v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/y;->b:Lcom/twitter/model/moments/i;

    iget-boolean v0, v0, Lcom/twitter/model/moments/i;->f:Z

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/y;->b:Lcom/twitter/model/moments/i;

    iget-object v0, v0, Lcom/twitter/model/moments/i;->g:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/y;->b:Lcom/twitter/model/moments/i;

    iget-object v0, v0, Lcom/twitter/model/moments/i;->j:Ljava/util/List;

    return-object v0
.end method
