.class Lcom/twitter/android/moments/viewmodels/b$e;
.super Lcom/twitter/android/moments/viewmodels/b$g;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/viewmodels/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "e"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/viewmodels/b;

.field private final d:Lcex;

.field private final e:Lcom/twitter/model/moments/viewmodels/f;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/viewmodels/b;Lcex;Lcom/twitter/model/moments/viewmodels/f;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/b$e;->a:Lcom/twitter/android/moments/viewmodels/b;

    .line 149
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/moments/viewmodels/b$g;-><init>(Lcom/twitter/android/moments/viewmodels/b;Lcfd;)V

    .line 150
    iput-object p2, p0, Lcom/twitter/android/moments/viewmodels/b$e;->d:Lcex;

    .line 151
    iput-object p3, p0, Lcom/twitter/android/moments/viewmodels/b$e;->e:Lcom/twitter/model/moments/viewmodels/f;

    .line 152
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/viewmodels/b$j;)V
    .locals 5

    .prologue
    .line 156
    iget-object v1, p1, Lcom/twitter/android/moments/viewmodels/b$j;->a:Lcom/twitter/model/moments/Moment$a;

    .line 157
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$e;->d:Lcex;

    iget-object v0, v0, Lcex;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$e;->d:Lcex;

    iget-object v0, v0, Lcex;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/twitter/model/moments/Moment$a;->a(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$e;->d:Lcex;

    iget-object v0, v0, Lcex;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 161
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$e;->d:Lcex;

    iget-object v0, v0, Lcex;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/twitter/model/moments/Moment$a;->f(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$e;->d:Lcex;

    iget-object v0, v0, Lcex;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 164
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$e;->d:Lcex;

    iget-object v0, v0, Lcex;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/twitter/model/moments/Moment$a;->c(Z)Lcom/twitter/model/moments/Moment$a;

    .line 166
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$e;->e:Lcom/twitter/model/moments/viewmodels/f;

    if-eqz v0, :cond_3

    .line 167
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$e;->e:Lcom/twitter/model/moments/viewmodels/f;

    sget-object v2, Lcom/twitter/model/moments/MomentPageDisplayMode;->a:Lcom/twitter/model/moments/MomentPageDisplayMode;

    invoke-interface {v0, v2}, Lcom/twitter/model/moments/viewmodels/f;->a(Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/android/moments/viewmodels/b$j;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    .line 168
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$e;->e:Lcom/twitter/model/moments/viewmodels/f;

    sget-object v2, Lcom/twitter/model/moments/MomentPageDisplayMode;->c:Lcom/twitter/model/moments/MomentPageDisplayMode;

    invoke-interface {v0, v2}, Lcom/twitter/model/moments/viewmodels/f;->a(Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/android/moments/viewmodels/b$j;->b(Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    .line 170
    :cond_3
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/b$j;->b()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/b$j;->c()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 172
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/b$j;->b()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->p()Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    invoke-virtual {v1}, Lcom/twitter/model/moments/Moment$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/Moment;

    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 171
    invoke-virtual {p1, v0}, Lcom/twitter/android/moments/viewmodels/b$j;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    .line 174
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/b$j;->c()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->p()Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    invoke-virtual {v1}, Lcom/twitter/model/moments/Moment$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/Moment;

    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 173
    invoke-virtual {p1, v0}, Lcom/twitter/android/moments/viewmodels/b$j;->b(Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    .line 176
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$e;->d:Lcex;

    iget-object v0, v0, Lcex;->f:Lcom/twitter/model/moments/MomentVisibilityMode;

    if-eqz v0, :cond_5

    .line 177
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$e;->a:Lcom/twitter/android/moments/viewmodels/b;

    .line 178
    invoke-static {v0}, Lcom/twitter/android/moments/viewmodels/b;->a(Lcom/twitter/android/moments/viewmodels/b;)Lcom/twitter/model/moments/viewmodels/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/a;->a()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/moments/Moment;->r:Lcom/twitter/model/moments/f;

    sget-object v2, Lcom/twitter/model/moments/f;->b:Lcom/twitter/model/moments/f;

    invoke-static {v0, v2}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/f;

    .line 179
    new-instance v2, Lcom/twitter/model/moments/f;

    iget-object v3, p0, Lcom/twitter/android/moments/viewmodels/b$e;->d:Lcex;

    iget-object v3, v3, Lcex;->f:Lcom/twitter/model/moments/MomentVisibilityMode;

    iget-object v4, v0, Lcom/twitter/model/moments/f;->d:Ljava/lang/Boolean;

    iget-boolean v0, v0, Lcom/twitter/model/moments/f;->e:Z

    invoke-direct {v2, v3, v4, v0}, Lcom/twitter/model/moments/f;-><init>(Lcom/twitter/model/moments/MomentVisibilityMode;Ljava/lang/Boolean;Z)V

    invoke-virtual {v1, v2}, Lcom/twitter/model/moments/Moment$a;->a(Lcom/twitter/model/moments/f;)Lcom/twitter/model/moments/Moment$a;

    .line 183
    :cond_5
    return-void
.end method
