.class public final Lcom/twitter/android/moments/viewmodels/ah$a;
.super Lcom/twitter/android/moments/viewmodels/MomentModule$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/viewmodels/ah;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/moments/viewmodels/MomentModule$a",
        "<",
        "Lcom/twitter/android/moments/viewmodels/ah;",
        "Lcom/twitter/android/moments/viewmodels/ah$a;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/util/math/Size;

.field private b:Lcom/twitter/model/moments/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/twitter/android/moments/viewmodels/MomentModule$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/viewmodels/ah$a;)Lcom/twitter/util/math/Size;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/ah$a;->a:Lcom/twitter/util/math/Size;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/viewmodels/ah$a;)Lcom/twitter/model/moments/e;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/ah$a;->b:Lcom/twitter/model/moments/e;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/e;)Lcom/twitter/android/moments/viewmodels/ah$a;
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/ah$a;->b:Lcom/twitter/model/moments/e;

    .line 72
    return-object p0
.end method

.method public a(Lcom/twitter/util/math/Size;)Lcom/twitter/android/moments/viewmodels/ah$a;
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/ah$a;->a:Lcom/twitter/util/math/Size;

    .line 66
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/twitter/android/moments/viewmodels/ah$a;->d()Lcom/twitter/android/moments/viewmodels/ah;

    move-result-object v0

    return-object v0
.end method

.method protected d()Lcom/twitter/android/moments/viewmodels/ah;
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lcom/twitter/android/moments/viewmodels/ah;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/viewmodels/ah;-><init>(Lcom/twitter/android/moments/viewmodels/ah$a;)V

    return-object v0
.end method
