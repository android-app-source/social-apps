.class Lcom/twitter/android/moments/viewmodels/b$b;
.super Lcom/twitter/android/moments/viewmodels/b$g;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/viewmodels/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/viewmodels/b;

.field private final d:Lcet;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/viewmodels/b;Lcet;)V
    .locals 0

    .prologue
    .line 322
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/b$b;->a:Lcom/twitter/android/moments/viewmodels/b;

    .line 323
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/moments/viewmodels/b$g;-><init>(Lcom/twitter/android/moments/viewmodels/b;Lcfd;)V

    .line 324
    iput-object p2, p0, Lcom/twitter/android/moments/viewmodels/b$b;->d:Lcet;

    .line 325
    return-void
.end method


# virtual methods
.method a(Lcom/twitter/android/moments/viewmodels/b$j;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 329
    iget-object v4, p1, Lcom/twitter/android/moments/viewmodels/b$j;->b:Ljava/util/List;

    move v1, v2

    .line 330
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 331
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v5

    .line 332
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/g;

    iget-object v0, v0, Lcom/twitter/model/moments/viewmodels/g;->a:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move v3, v2

    .line 333
    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_0

    .line 335
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage;

    iget-object v6, p0, Lcom/twitter/android/moments/viewmodels/b$b;->d:Lcet;

    iget-object v6, v6, Lcet;->a:Lcom/twitter/model/moments/w;

    invoke-static {v0, v6}, Lcom/twitter/model/moments/viewmodels/h;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/moments/w;)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 337
    invoke-interface {v5, v3, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 333
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 339
    :cond_0
    new-instance v0, Lcom/twitter/model/moments/viewmodels/g;

    invoke-direct {v0, v5}, Lcom/twitter/model/moments/viewmodels/g;-><init>(Ljava/util/List;)V

    invoke-interface {v4, v1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 330
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 341
    :cond_1
    return-void
.end method
