.class Lcom/twitter/android/moments/viewmodels/b$i;
.super Lcom/twitter/android/moments/viewmodels/b$g;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/viewmodels/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "i"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/viewmodels/b;

.field private final d:Lcfj;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/viewmodels/b;Lcfj;)V
    .locals 0

    .prologue
    .line 348
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/b$i;->a:Lcom/twitter/android/moments/viewmodels/b;

    .line 349
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/moments/viewmodels/b$g;-><init>(Lcom/twitter/android/moments/viewmodels/b;Lcfd;)V

    .line 350
    iput-object p2, p0, Lcom/twitter/android/moments/viewmodels/b$i;->d:Lcfj;

    .line 351
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/viewmodels/b$j;)V
    .locals 3

    .prologue
    .line 355
    new-instance v1, Lcom/twitter/model/moments/viewmodels/k$a;

    invoke-direct {v1}, Lcom/twitter/model/moments/viewmodels/k$a;-><init>()V

    new-instance v0, Lcom/twitter/model/moments/r$a;

    invoke-direct {v0}, Lcom/twitter/model/moments/r$a;-><init>()V

    iget-object v2, p0, Lcom/twitter/android/moments/viewmodels/b$i;->d:Lcfj;

    iget-object v2, v2, Lcfj;->a:Landroid/net/Uri;

    .line 357
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/model/moments/r$a;->a(Ljava/lang/String;)Lcom/twitter/model/moments/r$a;

    move-result-object v0

    sget-object v2, Lcom/twitter/model/moments/MomentPageDisplayMode;->a:Lcom/twitter/model/moments/MomentPageDisplayMode;

    .line 358
    invoke-virtual {v0, v2}, Lcom/twitter/model/moments/r$a;->a(Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/r$a;

    move-result-object v0

    .line 359
    invoke-virtual {v0}, Lcom/twitter/model/moments/r$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/r;

    .line 356
    invoke-virtual {v1, v0}, Lcom/twitter/model/moments/viewmodels/k$a;->a(Lcom/twitter/model/moments/r;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/k$a;

    sget-object v1, Lcom/twitter/model/moments/MomentPageDisplayMode;->a:Lcom/twitter/model/moments/MomentPageDisplayMode;

    .line 360
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/viewmodels/k$a;->a(Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/k$a;

    sget-object v1, Lcom/twitter/model/moments/e;->b:Lcom/twitter/model/moments/e;

    .line 361
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/viewmodels/k$a;->a(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/j$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/k$a;

    iget-object v1, p1, Lcom/twitter/android/moments/viewmodels/b$j;->a:Lcom/twitter/model/moments/Moment$a;

    .line 362
    invoke-virtual {v1}, Lcom/twitter/model/moments/Moment$a;->q()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/moments/Moment;

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/viewmodels/k$a;->a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/k$a;

    iget-object v1, p0, Lcom/twitter/android/moments/viewmodels/b$i;->d:Lcfj;

    iget-object v1, v1, Lcfj;->a:Landroid/net/Uri;

    .line 363
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/viewmodels/k$a;->a(Ljava/lang/String;)Lcom/twitter/model/moments/viewmodels/j$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/k$a;

    iget-object v1, p0, Lcom/twitter/android/moments/viewmodels/b$i;->d:Lcfj;

    iget-object v1, v1, Lcfj;->c:Lcom/twitter/util/math/Size;

    .line 364
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/viewmodels/k$a;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/model/moments/viewmodels/j$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/k$a;

    .line 365
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/k$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 366
    invoke-virtual {p1, v0}, Lcom/twitter/android/moments/viewmodels/b$j;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    .line 367
    return-void
.end method
