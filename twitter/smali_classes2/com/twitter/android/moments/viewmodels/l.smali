.class public Lcom/twitter/android/moments/viewmodels/l;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/object/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/object/d",
        "<",
        "Lcom/twitter/model/moments/viewmodels/MomentPage;",
        "Lcom/twitter/android/moments/viewmodels/k;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/util/object/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            "Lcom/twitter/media/request/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/util/object/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            "Lcom/twitter/util/math/Size;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lbrd;


# direct methods
.method public constructor <init>(Lcom/twitter/util/object/d;Lcom/twitter/util/object/d;Lbrd;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            "Lcom/twitter/media/request/a$a;",
            ">;",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            "Lcom/twitter/util/math/Size;",
            ">;",
            "Lbrd;",
            ")V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/l;->a:Lcom/twitter/util/object/d;

    .line 54
    iput-object p2, p0, Lcom/twitter/android/moments/viewmodels/l;->b:Lcom/twitter/util/object/d;

    .line 55
    iput-object p3, p0, Lcom/twitter/android/moments/viewmodels/l;->c:Lbrd;

    .line 56
    return-void
.end method

.method public static a()Lcom/twitter/android/moments/viewmodels/l;
    .locals 4

    .prologue
    .line 31
    new-instance v0, Lcom/twitter/android/moments/viewmodels/l;

    new-instance v1, Lcom/twitter/android/moments/viewmodels/l$1;

    invoke-direct {v1}, Lcom/twitter/android/moments/viewmodels/l$1;-><init>()V

    new-instance v2, Lcom/twitter/android/moments/viewmodels/l$2;

    invoke-direct {v2}, Lcom/twitter/android/moments/viewmodels/l$2;-><init>()V

    new-instance v3, Lbrd;

    invoke-direct {v3}, Lbrd;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/moments/viewmodels/l;-><init>(Lcom/twitter/util/object/d;Lcom/twitter/util/object/d;Lbrd;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/android/moments/viewmodels/k;
    .locals 4

    .prologue
    .line 62
    instance-of v0, p1, Lcom/twitter/model/moments/viewmodels/o;

    if-eqz v0, :cond_0

    .line 63
    new-instance v0, Lcom/twitter/android/moments/viewmodels/af;

    check-cast p1, Lcom/twitter/model/moments/viewmodels/o;

    invoke-direct {v0, p1}, Lcom/twitter/android/moments/viewmodels/af;-><init>(Lcom/twitter/model/moments/viewmodels/o;)V

    .line 83
    :goto_0
    return-object v0

    .line 64
    :cond_0
    instance-of v0, p1, Lcom/twitter/model/moments/viewmodels/j;

    if-eqz v0, :cond_1

    .line 65
    new-instance v0, Lcom/twitter/android/moments/viewmodels/ac;

    check-cast p1, Lcom/twitter/model/moments/viewmodels/j;

    invoke-direct {v0, p1}, Lcom/twitter/android/moments/viewmodels/ac;-><init>(Lcom/twitter/model/moments/viewmodels/j;)V

    goto :goto_0

    .line 66
    :cond_1
    instance-of v0, p1, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 67
    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    .line 68
    iget-object v1, v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->a:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    sget-object v2, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;->e:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    if-ne v1, v2, :cond_2

    .line 69
    new-instance v2, Lcom/twitter/android/moments/viewmodels/j;

    iget-object v3, p0, Lcom/twitter/android/moments/viewmodels/l;->c:Lbrd;

    .line 71
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/core/Tweet;

    .line 70
    invoke-virtual {v3, v1}, Lbrd;->a(Lcom/twitter/model/core/Tweet;)Lbrc;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Lcom/twitter/android/moments/viewmodels/j;-><init>(Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;Lbrc;)V

    move-object v0, v2

    goto :goto_0

    .line 73
    :cond_2
    new-instance v0, Lcom/twitter/android/moments/viewmodels/a;

    check-cast p1, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    iget-object v1, p0, Lcom/twitter/android/moments/viewmodels/l;->a:Lcom/twitter/util/object/d;

    iget-object v2, p0, Lcom/twitter/android/moments/viewmodels/l;->b:Lcom/twitter/util/object/d;

    invoke-direct {v0, p1, v1, v2}, Lcom/twitter/android/moments/viewmodels/a;-><init>(Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;Lcom/twitter/util/object/d;Lcom/twitter/util/object/d;)V

    goto :goto_0

    .line 77
    :cond_3
    instance-of v0, p1, Lcom/twitter/model/moments/viewmodels/p;

    if-eqz v0, :cond_4

    .line 78
    new-instance v0, Lcom/twitter/android/moments/viewmodels/a;

    check-cast p1, Lcom/twitter/model/moments/viewmodels/p;

    iget-object v1, p0, Lcom/twitter/android/moments/viewmodels/l;->a:Lcom/twitter/util/object/d;

    iget-object v2, p0, Lcom/twitter/android/moments/viewmodels/l;->b:Lcom/twitter/util/object/d;

    invoke-direct {v0, p1, v1, v2}, Lcom/twitter/android/moments/viewmodels/a;-><init>(Lcom/twitter/model/moments/viewmodels/p;Lcom/twitter/util/object/d;Lcom/twitter/util/object/d;)V

    goto :goto_0

    .line 81
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unrecognized moment page type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    check-cast p1, Lcom/twitter/model/moments/viewmodels/MomentPage;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/viewmodels/l;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/android/moments/viewmodels/k;

    move-result-object v0

    return-object v0
.end method
