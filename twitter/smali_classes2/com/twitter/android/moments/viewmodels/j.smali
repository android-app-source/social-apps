.class public Lcom/twitter/android/moments/viewmodels/j;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/viewmodels/k;


# instance fields
.field private final a:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

.field private final b:Lbrc;


# direct methods
.method public constructor <init>(Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;Lbrc;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/j;->a:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    .line 20
    iput-object p2, p0, Lcom/twitter/android/moments/viewmodels/j;->b:Lbrc;

    .line 21
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/media/request/a$a;
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/j;->b:Lbrc;

    invoke-virtual {v0}, Lbrc;->e()Lcas;

    move-result-object v1

    .line 27
    if-eqz v1, :cond_0

    .line 28
    new-instance v0, Lcom/twitter/media/request/a$a;

    iget-object v1, v1, Lcas;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/twitter/media/request/a$a;-><init>(Ljava/lang/String;)V

    .line 30
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/media/request/a$a;

    const-string/jumbo v1, ""

    invoke-direct {v0, v1}, Lcom/twitter/media/request/a$a;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b()Lcom/twitter/util/math/Size;
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/j;->b:Lbrc;

    invoke-virtual {v0}, Lbrc;->e()Lcas;

    move-result-object v0

    .line 37
    if-eqz v0, :cond_0

    .line 38
    iget v1, v0, Lcas;->b:I

    iget v0, v0, Lcas;->c:I

    invoke-static {v1, v0}, Lcom/twitter/util/math/Size;->a(II)Lcom/twitter/util/math/Size;

    move-result-object v0

    .line 40
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    goto :goto_0
.end method

.method public c()Lcom/twitter/model/moments/e;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/j;->a:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    iget-object v0, v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->b:Lcom/twitter/model/moments/e;

    return-object v0
.end method
