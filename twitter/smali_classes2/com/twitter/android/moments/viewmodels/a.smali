.class public Lcom/twitter/android/moments/viewmodels/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/viewmodels/k;


# instance fields
.field private final a:Lcom/twitter/model/moments/e;

.field private final b:Lcom/twitter/media/request/a$a;

.field private final c:Lcom/twitter/util/math/Size;


# direct methods
.method public constructor <init>(Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;Lcom/twitter/util/object/d;Lcom/twitter/util/object/d;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            "Lcom/twitter/media/request/a$a;",
            ">;",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            "Lcom/twitter/util/math/Size;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    .line 23
    iget-object v1, p1, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->b:Lcom/twitter/model/moments/e;

    iput-object v1, p0, Lcom/twitter/android/moments/viewmodels/a;->a:Lcom/twitter/model/moments/e;

    .line 24
    invoke-interface {p2, v0}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/media/request/a$a;

    iput-object v1, p0, Lcom/twitter/android/moments/viewmodels/a;->b:Lcom/twitter/media/request/a$a;

    .line 25
    invoke-interface {p3, v0}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/math/Size;

    iput-object v0, p0, Lcom/twitter/android/moments/viewmodels/a;->c:Lcom/twitter/util/math/Size;

    .line 26
    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/moments/viewmodels/p;Lcom/twitter/util/object/d;Lcom/twitter/util/object/d;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/viewmodels/p;",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            "Lcom/twitter/media/request/a$a;",
            ">;",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            "Lcom/twitter/util/math/Size;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/p;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    .line 32
    iget-object v1, p1, Lcom/twitter/model/moments/viewmodels/p;->a:Lcom/twitter/model/moments/e;

    iput-object v1, p0, Lcom/twitter/android/moments/viewmodels/a;->a:Lcom/twitter/model/moments/e;

    .line 33
    invoke-interface {p2, v0}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/media/request/a$a;

    iput-object v1, p0, Lcom/twitter/android/moments/viewmodels/a;->b:Lcom/twitter/media/request/a$a;

    .line 34
    invoke-interface {p3, v0}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/math/Size;

    iput-object v0, p0, Lcom/twitter/android/moments/viewmodels/a;->c:Lcom/twitter/util/math/Size;

    .line 35
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/media/request/a$a;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/a;->b:Lcom/twitter/media/request/a$a;

    return-object v0
.end method

.method public b()Lcom/twitter/util/math/Size;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/a;->c:Lcom/twitter/util/math/Size;

    return-object v0
.end method

.method public c()Lcom/twitter/model/moments/e;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/a;->a:Lcom/twitter/model/moments/e;

    return-object v0
.end method
