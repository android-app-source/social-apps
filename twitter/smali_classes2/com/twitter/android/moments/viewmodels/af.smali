.class public Lcom/twitter/android/moments/viewmodels/af;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/viewmodels/k;


# static fields
.field private static final a:Lcom/twitter/util/math/Size;


# instance fields
.field private final b:Lcom/twitter/model/moments/viewmodels/o;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/16 v0, 0xc8

    invoke-static {v0}, Lcom/twitter/util/math/Size;->a(I)Lcom/twitter/util/math/Size;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/moments/viewmodels/af;->a:Lcom/twitter/util/math/Size;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/model/moments/viewmodels/o;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/af;->b:Lcom/twitter/model/moments/viewmodels/o;

    .line 27
    return-void
.end method

.method private static a(Lcom/twitter/model/moments/viewmodels/o;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/o;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_0

    .line 65
    invoke-static {v0}, Lcom/twitter/library/av/playback/ab;->m(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/card/property/ImageSpec;

    move-result-object v0

    .line 66
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    sget-object v2, Lcom/twitter/model/card/property/Vector2F;->b:Lcom/twitter/model/card/property/Vector2F;

    invoke-virtual {v1, v2}, Lcom/twitter/model/card/property/Vector2F;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 67
    iget-object v0, v0, Lcom/twitter/model/card/property/ImageSpec;->b:Ljava/lang/String;

    .line 70
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/twitter/model/moments/viewmodels/o;)Lcom/twitter/util/math/Size;
    .locals 2

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/o;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 76
    if-eqz v0, :cond_0

    .line 77
    invoke-static {v0}, Lcom/twitter/library/av/playback/ab;->m(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/card/property/ImageSpec;

    move-result-object v0

    .line 78
    if-eqz v0, :cond_0

    .line 79
    iget-object v1, v0, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    iget v1, v1, Lcom/twitter/model/card/property/Vector2F;->x:F

    iget-object v0, v0, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    iget v0, v0, Lcom/twitter/model/card/property/Vector2F;->y:F

    invoke-static {v1, v0}, Lcom/twitter/util/math/Size;->a(FF)Lcom/twitter/util/math/Size;

    move-result-object v0

    .line 82
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/twitter/media/request/a$a;
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/af;->b:Lcom/twitter/model/moments/viewmodels/o;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/o;->s()Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    .line 34
    if-eqz v0, :cond_0

    .line 35
    invoke-static {v0}, Lcom/twitter/media/util/k;->a(Lcom/twitter/model/core/MediaEntity;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    .line 39
    :goto_0
    return-object v0

    .line 37
    :cond_0
    new-instance v0, Lcom/twitter/media/request/a$a;

    iget-object v1, p0, Lcom/twitter/android/moments/viewmodels/af;->b:Lcom/twitter/model/moments/viewmodels/o;

    invoke-static {v1}, Lcom/twitter/android/moments/viewmodels/af;->a(Lcom/twitter/model/moments/viewmodels/o;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/media/request/a$a;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b()Lcom/twitter/util/math/Size;
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/af;->b:Lcom/twitter/model/moments/viewmodels/o;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/o;->s()Lcom/twitter/model/core/MediaEntity;

    move-result-object v0

    .line 47
    if-eqz v0, :cond_0

    .line 48
    iget-object v0, v0, Lcom/twitter/model/core/MediaEntity;->o:Lcom/twitter/util/math/Size;

    .line 52
    :goto_0
    return-object v0

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/af;->b:Lcom/twitter/model/moments/viewmodels/o;

    invoke-static {v0}, Lcom/twitter/android/moments/viewmodels/af;->b(Lcom/twitter/model/moments/viewmodels/o;)Lcom/twitter/util/math/Size;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/moments/viewmodels/af;->a:Lcom/twitter/util/math/Size;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/math/Size;

    goto :goto_0
.end method

.method public c()Lcom/twitter/model/moments/e;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/af;->b:Lcom/twitter/model/moments/viewmodels/o;

    iget-object v0, v0, Lcom/twitter/model/moments/viewmodels/o;->b:Lcom/twitter/model/moments/e;

    return-object v0
.end method
