.class Lcom/twitter/android/moments/viewmodels/b$f;
.super Lcom/twitter/android/moments/viewmodels/b$g;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/viewmodels/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "f"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/viewmodels/b;

.field private final d:Lcfc;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/viewmodels/b;Lcfc;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/twitter/android/moments/viewmodels/b$f;->a:Lcom/twitter/android/moments/viewmodels/b;

    .line 191
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/moments/viewmodels/b$g;-><init>(Lcom/twitter/android/moments/viewmodels/b;Lcfd;)V

    .line 192
    iput-object p2, p0, Lcom/twitter/android/moments/viewmodels/b$f;->d:Lcfc;

    .line 193
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/viewmodels/b$j;)V
    .locals 4

    .prologue
    .line 197
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$f;->a:Lcom/twitter/android/moments/viewmodels/b;

    iget-object v1, p0, Lcom/twitter/android/moments/viewmodels/b$f;->d:Lcfc;

    iget-wide v2, v1, Lcfc;->c:J

    sget-object v1, Lcom/twitter/model/moments/MomentPageDisplayMode;->b:Lcom/twitter/model/moments/MomentPageDisplayMode;

    .line 198
    invoke-static {v0, p1, v2, v3, v1}, Lcom/twitter/android/moments/viewmodels/b;->a(Lcom/twitter/android/moments/viewmodels/b;Lcom/twitter/android/moments/viewmodels/b$j;JLcom/twitter/model/moments/MomentPageDisplayMode;)Ljava/lang/Integer;

    move-result-object v1

    .line 199
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$f;->d:Lcfc;

    iget-object v2, v0, Lcfc;->b:Lcfi;

    .line 200
    iget-object v0, p0, Lcom/twitter/android/moments/viewmodels/b$f;->a:Lcom/twitter/android/moments/viewmodels/b;

    .line 201
    invoke-static {v0, p1, v2}, Lcom/twitter/android/moments/viewmodels/b;->a(Lcom/twitter/android/moments/viewmodels/b;Lcom/twitter/android/moments/viewmodels/b$j;Lcfi;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 202
    :goto_0
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 203
    iget-object v3, p1, Lcom/twitter/android/moments/viewmodels/b$j;->b:Ljava/util/List;

    .line 204
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v3, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/g;

    .line 205
    iget-object v1, p0, Lcom/twitter/android/moments/viewmodels/b$f;->a:Lcom/twitter/android/moments/viewmodels/b;

    .line 206
    invoke-static {v1, p1, v2}, Lcom/twitter/android/moments/viewmodels/b;->a(Lcom/twitter/android/moments/viewmodels/b;Lcom/twitter/android/moments/viewmodels/b$j;Lcfi;)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 205
    invoke-interface {v3, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 209
    :cond_0
    return-void

    .line 201
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
