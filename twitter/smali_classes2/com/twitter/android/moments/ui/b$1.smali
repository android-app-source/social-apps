.class Lcom/twitter/android/moments/ui/b$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/b;->a(JLjava/lang/String;Lcgi;)Lrx/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/b",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Lcgi;

.field final synthetic c:Lcom/twitter/android/moments/ui/b;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/b;JLcgi;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/twitter/android/moments/ui/b$1;->c:Lcom/twitter/android/moments/ui/b;

    iput-wide p2, p0, Lcom/twitter/android/moments/ui/b$1;->a:J

    iput-object p4, p0, Lcom/twitter/android/moments/ui/b$1;->b:Lcgi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    .line 66
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/twitter/android/moments/ui/b$1;->c:Lcom/twitter/android/moments/ui/b;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/b;->b(Lcom/twitter/android/moments/ui/b;)Lcqu;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/b$1$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/b$1$1;-><init>(Lcom/twitter/android/moments/ui/b$1;)V

    invoke-virtual {v0, v1}, Lcqu;->a(Ljava/util/concurrent/Callable;)Lrx/a;

    move-result-object v0

    .line 73
    invoke-static {}, Lcqv;->b()Lcqv;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/a;->b(Lrx/b;)V

    .line 74
    iget-object v0, p0, Lcom/twitter/android/moments/ui/b$1;->c:Lcom/twitter/android/moments/ui/b;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/b;->c(Lcom/twitter/android/moments/ui/b;)Lcom/twitter/android/moments/ui/b$a;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/moments/ui/b$1;->a:J

    iget-object v1, p0, Lcom/twitter/android/moments/ui/b$1;->b:Lcgi;

    invoke-virtual {v0, v2, v3, v1}, Lcom/twitter/android/moments/ui/b$a;->a(JLcgi;)Lrx/g;

    move-result-object v0

    invoke-virtual {v0}, Lrx/g;->b()Lrx/j;

    .line 76
    :cond_0
    return-void
.end method

.method public synthetic call(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 63
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/b$1;->a(Ljava/lang/Boolean;)V

    return-void
.end method
