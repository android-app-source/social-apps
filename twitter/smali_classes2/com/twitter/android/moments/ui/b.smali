.class public Lcom/twitter/android/moments/ui/b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/b$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/b$a;

.field private final b:Lbsb;

.field private final c:Lcqu;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/b$a;Lbsb;Lcqu;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/twitter/android/moments/ui/b;->a:Lcom/twitter/android/moments/ui/b$a;

    .line 44
    iput-object p2, p0, Lcom/twitter/android/moments/ui/b;->b:Lbsb;

    .line 45
    iput-object p3, p0, Lcom/twitter/android/moments/ui/b;->c:Lcqu;

    .line 46
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/b;)Lbsb;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/android/moments/ui/b;->b:Lbsb;

    return-object v0
.end method

.method public static a(Landroid/support/v4/app/FragmentActivity;Lbsb;)Lcom/twitter/android/moments/ui/b;
    .locals 4

    .prologue
    .line 36
    new-instance v0, Lcom/twitter/android/moments/ui/b;

    new-instance v1, Lcom/twitter/android/moments/ui/b$a;

    .line 37
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3}, Lcom/twitter/android/moments/ui/b$a;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Lcom/twitter/library/client/v;)V

    new-instance v2, Lcqu;

    .line 38
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v3

    invoke-direct {v2, v3}, Lcqu;-><init>(Lrx/f;)V

    invoke-direct {v0, v1, p1, v2}, Lcom/twitter/android/moments/ui/b;-><init>(Lcom/twitter/android/moments/ui/b$a;Lbsb;Lcqu;)V

    .line 36
    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/b;)Lcqu;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/android/moments/ui/b;->c:Lcqu;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/b;)Lcom/twitter/android/moments/ui/b$a;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/android/moments/ui/b;->a:Lcom/twitter/android/moments/ui/b$a;

    return-object v0
.end method


# virtual methods
.method public a(JLjava/lang/String;Lcgi;)Lrx/g;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Lcgi;",
            ")",
            "Lrx/g",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/android/moments/ui/b;->a:Lcom/twitter/android/moments/ui/b$a;

    invoke-virtual {v0, p3}, Lcom/twitter/android/moments/ui/b$a;->a(Ljava/lang/String;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/b$1;

    invoke-direct {v1, p0, p1, p2, p4}, Lcom/twitter/android/moments/ui/b$1;-><init>(Lcom/twitter/android/moments/ui/b;JLcgi;)V

    .line 63
    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/b;)Lrx/g;

    move-result-object v0

    .line 62
    return-object v0
.end method
