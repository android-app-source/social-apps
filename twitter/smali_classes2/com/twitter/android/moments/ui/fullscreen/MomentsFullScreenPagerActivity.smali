.class public Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/fullscreen/co;


# instance fields
.field private a:Lcom/twitter/android/moments/ui/fullscreen/ah;

.field private b:Landroid/view/ViewGroup;

.field private c:Landroid/view/ViewGroup;

.field private d:Lcom/twitter/ui/anim/p;

.field private e:Lcom/twitter/android/av/k;

.field private f:Lcom/twitter/android/moments/ui/fullscreen/r;

.field private g:Lcom/twitter/android/moments/ui/fullscreen/b;

.field private h:Lcom/twitter/android/moments/data/ag;

.field private i:Lcom/twitter/android/moments/data/an;

.field private j:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;J)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 83
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 84
    const-string/jumbo v1, "extra_moment_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 85
    return-object v0
.end method

.method public static b(Landroid/content/Context;J)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 90
    invoke-static {p0, p1, p2}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->a(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_should_resume_user"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static c(Landroid/content/Context;J)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 95
    invoke-static {p0, p1, p2}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->b(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_should_force_capsule_refresh"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private i()Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 208
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f130514

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    .line 102
    const v0, 0x7f0401eb

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 103
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->b(Z)V

    .line 104
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 31

    .prologue
    .line 118
    invoke-super/range {p0 .. p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V

    .line 119
    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/r;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/twitter/android/moments/ui/fullscreen/r;-><init>(Landroid/app/Activity;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->f:Lcom/twitter/android/moments/ui/fullscreen/r;

    .line 122
    if-nez p1, :cond_0

    .line 123
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->setRequestedOrientation(I)V

    .line 126
    :cond_0
    const v2, 0x7f13050c

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->b:Landroid/view/ViewGroup;

    .line 127
    const v2, 0x7f13050b

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->c:Landroid/view/ViewGroup;

    .line 129
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/ui/anim/b;->a(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-nez p1, :cond_1

    .line 131
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->c:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->a(Landroid/support/v4/app/FragmentActivity;Landroid/content/Intent;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;

    move-result-object v2

    .line 132
    new-instance v3, Lcom/twitter/ui/anim/p;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->b:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4, v2}, Lcom/twitter/ui/anim/p;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/twitter/ui/anim/a;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->d:Lcom/twitter/ui/anim/p;

    .line 135
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v11

    .line 136
    invoke-virtual {v11}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    .line 137
    invoke-static {v8, v9}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v10

    .line 138
    new-instance v5, Lbsb;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v10, v8, v9}, Lbsb;-><init>(Landroid/content/Context;Lcom/twitter/library/provider/t;J)V

    .line 139
    new-instance v2, Lcom/twitter/android/moments/data/ad;

    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->i()Landroid/support/v4/app/Fragment;

    move-result-object v3

    const/4 v4, 0x0

    new-instance v6, Lcqu;

    .line 140
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v7

    invoke-direct {v6, v7}, Lcqu;-><init>(Lrx/f;)V

    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v7

    invoke-direct/range {v2 .. v7}, Lcom/twitter/android/moments/data/ad;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lbsb;Lcqu;Lrx/f;)V

    .line 141
    const v3, 0x7f13043b

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Lcom/twitter/ui/widget/touchintercept/TouchInterceptingFrameLayout;

    .line 142
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v30

    .line 144
    const-string/jumbo v3, "extra_moment_id"

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 145
    const-string/jumbo v3, "extra_moment_id"

    const-wide/16 v12, -0x1

    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v12

    .line 151
    new-instance v3, Lbds;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->getApplication()Landroid/app/Application;

    move-result-object v4

    invoke-direct {v3, v4, v11}, Lbds;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 152
    invoke-static {v3}, Lcom/twitter/android/moments/data/ag;->a(Lbds;)Lcom/twitter/android/moments/data/ag;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->h:Lcom/twitter/android/moments/data/ag;

    .line 153
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->h:Lcom/twitter/android/moments/data/ag;

    .line 154
    invoke-static {v4, v3, v10}, Lcom/twitter/android/moments/data/an;->a(Lcom/twitter/android/moments/data/ag;Lbds;Lcom/twitter/library/provider/t;)Lcom/twitter/android/moments/data/an;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->i:Lcom/twitter/android/moments/data/an;

    .line 157
    invoke-static {v5, v12, v13}, Lcom/twitter/android/moments/data/v;->a(Lbsb;J)V

    .line 158
    invoke-static {v5, v12, v13}, Lcom/twitter/android/moments/data/aa;->a(Lbsb;J)V

    .line 160
    new-instance v16, Lcom/twitter/android/moments/data/i;

    invoke-direct/range {v16 .. v16}, Lcom/twitter/android/moments/data/i;-><init>()V

    .line 161
    invoke-static/range {p0 .. p0}, Lcom/twitter/android/moments/ui/fullscreen/b;->a(Landroid/app/Activity;)Lcom/twitter/android/moments/ui/fullscreen/b;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->g:Lcom/twitter/android/moments/ui/fullscreen/b;

    .line 162
    new-instance v14, Lcom/twitter/android/moments/ui/fullscreen/ad;

    invoke-direct {v14}, Lcom/twitter/android/moments/ui/fullscreen/ad;-><init>()V

    .line 164
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->X()Lann;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lys;

    .line 165
    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/twitter/android/moments/ui/b;->a(Landroid/support/v4/app/FragmentActivity;Lbsb;)Lcom/twitter/android/moments/ui/b;

    move-result-object v22

    .line 166
    const-string/jumbo v3, "extra_should_force_capsule_refresh"

    const/4 v5, 0x0

    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 167
    const-string/jumbo v5, "extra_preview_mode"

    const/4 v7, 0x0

    move-object/from16 v0, v30

    invoke-virtual {v0, v5, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->j:Z

    .line 169
    if-eqz v3, :cond_5

    .line 170
    invoke-interface {v4}, Lys;->f()Lwq;

    move-result-object v20

    .line 175
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->j:Z

    if-eqz v3, :cond_6

    .line 176
    new-instance v27, Lcom/twitter/android/moments/ui/fullscreen/cm;

    invoke-direct/range {v27 .. v27}, Lcom/twitter/android/moments/ui/fullscreen/cm;-><init>()V

    .line 180
    :goto_1
    new-instance v3, Lcom/twitter/android/av/k;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/twitter/android/av/k;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->e:Lcom/twitter/android/av/k;

    .line 181
    new-instance v3, Lcom/twitter/android/moments/ui/fullscreen/ah;

    .line 182
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->g:Lcom/twitter/android/moments/ui/fullscreen/b;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->d:Lcom/twitter/ui/anim/p;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->i:Lcom/twitter/android/moments/data/an;

    move-object/from16 v19, v0

    new-instance v21, Labg;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Labg;-><init>(Landroid/app/Activity;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->f:Lcom/twitter/android/moments/ui/fullscreen/r;

    move-object/from16 v23, v0

    .line 185
    invoke-static/range {p0 .. p0}, Lcom/twitter/android/moments/ui/fullscreen/t;->a(Landroid/support/v4/app/FragmentActivity;)Lcom/twitter/android/moments/ui/fullscreen/t;

    move-result-object v24

    invoke-interface {v4}, Lys;->e()Lyf;

    move-result-object v25

    .line 186
    invoke-interface {v4}, Lys;->g()Lcif;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->e:Lcom/twitter/android/av/k;

    move-object/from16 v29, v0

    move-object/from16 v4, p0

    move-object v5, v6

    move-object v6, v2

    move-object/from16 v11, p0

    move-object/from16 v17, p1

    move-object/from16 v26, p0

    invoke-direct/range {v3 .. v29}, Lcom/twitter/android/moments/ui/fullscreen/ah;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/ui/widget/touchintercept/TouchInterceptingFrameLayout;Lcom/twitter/android/ck;Lcom/twitter/library/client/v;JLcom/twitter/library/provider/t;Lcom/twitter/app/common/util/j;JLcom/twitter/android/moments/ui/fullscreen/ad;Lcom/twitter/android/moments/ui/fullscreen/bz;Lcom/twitter/android/moments/data/i;Landroid/os/Bundle;Lcom/twitter/ui/anim/p;Lcom/twitter/android/moments/data/am;Lxm;Labg;Lcom/twitter/android/moments/ui/b;Lcom/twitter/android/moments/ui/fullscreen/r;Lcom/twitter/android/moments/ui/fullscreen/t;Lyf;Lcom/twitter/android/moments/ui/fullscreen/co;Lcom/twitter/android/moments/ui/fullscreen/x;Lcif;Lcom/twitter/android/av/k;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    .line 187
    const-string/jumbo v2, "extra_initial_page_number"

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "extra_should_resume_user"

    const/4 v3, 0x0

    move-object/from16 v0, v30

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_7

    .line 188
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    const-string/jumbo v3, "extra_initial_page_number"

    const/4 v4, 0x0

    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/util/collection/k;->a(Ljava/lang/Object;)Lcom/twitter/util/collection/k;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Lcom/twitter/util/collection/k;)V

    .line 197
    :cond_3
    :goto_2
    invoke-static/range {p0 .. p0}, Lcom/twitter/android/moments/ui/animation/e;->a(Landroid/content/Context;)Lcom/twitter/android/moments/ui/animation/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/animation/e;->a()V

    .line 198
    return-void

    .line 147
    :cond_4
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Starting full screen pager requires a capsule"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 172
    :cond_5
    invoke-interface {v4}, Lys;->d()Lxn;

    move-result-object v20

    goto/16 :goto_0

    .line 178
    :cond_6
    sget-object v27, Lcom/twitter/android/moments/ui/fullscreen/x;->a:Lcom/twitter/android/moments/ui/fullscreen/x;

    goto/16 :goto_1

    .line 189
    :cond_7
    const-string/jumbo v2, "extra_initial_page_id"

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 190
    const-string/jumbo v2, "extra_initial_page_id"

    sget-object v3, Lcom/twitter/model/moments/r;->a:Lcom/twitter/util/serialization/l;

    move-object/from16 v0, v30

    invoke-static {v0, v2, v3}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/r;

    .line 191
    if-eqz v2, :cond_3

    .line 192
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-virtual {v3, v2}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Lcom/twitter/model/moments/r;)V

    goto :goto_2

    .line 195
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-static {}, Lcom/twitter/util/collection/k;->a()Lcom/twitter/util/collection/k;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Lcom/twitter/util/collection/k;)V

    goto :goto_2
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->j:Z

    return v0
.end method

.method protected d(Lank;)Lys;
    .locals 2

    .prologue
    .line 110
    invoke-static {}, Lyq;->a()Lyq$a;

    move-result-object v0

    .line 111
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lyq$a;->a(Lamu;)Lyq$a;

    move-result-object v0

    .line 112
    invoke-virtual {v0}, Lyq$a;->a()Lys;

    move-result-object v0

    .line 110
    return-object v0
.end method

.method protected d()V
    .locals 2

    .prologue
    .line 249
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->isChangingConfigurations()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Z)V

    .line 250
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->i:Lcom/twitter/android/moments/data/an;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 251
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->e:Lcom/twitter/android/av/k;

    invoke-virtual {v0}, Lcom/twitter/android/av/k;->b()V

    .line 252
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d()V

    .line 253
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Landroid/view/KeyEvent;)V

    .line 238
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected synthetic e(Lank;)Lcom/twitter/app/common/base/i;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->d(Lank;)Lys;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f(Lank;)Lcom/twitter/app/common/abs/a;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->d(Lank;)Lys;

    move-result-object v0

    return-object v0
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 275
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->finish()V

    .line 277
    const/4 v0, 0x0

    const v1, 0x7f05003d

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->overridePendingTransition(II)V

    .line 278
    return-void
.end method

.method protected synthetic g(Lank;)Lann;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->d(Lank;)Lys;

    move-result-object v0

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    .line 257
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 258
    const/16 v0, 0xbad

    if-ne p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p2, v0, :cond_0

    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ah;->c()Lcom/twitter/util/collection/k;

    move-result-object v0

    .line 262
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "extra_moment_id"

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/k;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 263
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->f:Lcom/twitter/android/moments/ui/fullscreen/r;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/fullscreen/r;

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/moments/ui/fullscreen/r;->a(J)V

    .line 265
    :cond_1
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 203
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onBackPressed()V

    .line 205
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 269
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 270
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Landroid/content/res/Configuration;)V

    .line 271
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 228
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onPause()V

    .line 229
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ah;->e()V

    .line 230
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->h:Lcom/twitter/android/moments/data/ag;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->h:Lcom/twitter/android/moments/data/ag;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/ag;->a()V

    .line 233
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 219
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onResume()V

    .line 220
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ah;->d()V

    .line 221
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->h:Lcom/twitter/android/moments/data/ag;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->h:Lcom/twitter/android/moments/data/ag;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/ag;->b()V

    .line 224
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 243
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 244
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Landroid/os/Bundle;)V

    .line 245
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 213
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onWindowFocusChanged(Z)V

    .line 214
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ah;->b(Z)V

    .line 215
    return-void
.end method
