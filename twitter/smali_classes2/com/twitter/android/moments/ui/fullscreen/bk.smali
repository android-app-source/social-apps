.class Lcom/twitter/android/moments/ui/fullscreen/bk;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method constructor <init>(Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/bk;->a:Landroid/content/res/Resources;

    .line 21
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/ui/fullscreen/bg;Lcom/twitter/model/moments/Moment;)Lcom/twitter/android/dialog/d;
    .locals 3

    .prologue
    .line 83
    new-instance v0, Lcom/twitter/android/dialog/d;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bk;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a0599

    .line 84
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/bk$5;

    invoke-direct {v2, p0, p1, p2}, Lcom/twitter/android/moments/ui/fullscreen/bk$5;-><init>(Lcom/twitter/android/moments/ui/fullscreen/bk;Lcom/twitter/android/moments/ui/fullscreen/bg;Lcom/twitter/model/moments/Moment;)V

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/dialog/d;-><init>(Ljava/lang/String;Lcom/twitter/android/dialog/d$a;)V

    .line 83
    return-object v0
.end method

.method public a(Lcom/twitter/android/moments/ui/fullscreen/bl;Lcom/twitter/model/moments/Moment;Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/dialog/d;
    .locals 3

    .prologue
    .line 126
    new-instance v0, Lcom/twitter/android/dialog/d;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bk;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a058d

    .line 127
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/bk$8;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/twitter/android/moments/ui/fullscreen/bk$8;-><init>(Lcom/twitter/android/moments/ui/fullscreen/bk;Lcom/twitter/android/moments/ui/fullscreen/bl;Lcom/twitter/model/moments/Moment;Lcom/twitter/model/core/Tweet;)V

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/dialog/d;-><init>(Ljava/lang/String;Lcom/twitter/android/dialog/d$a;)V

    .line 126
    return-object v0
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/android/moments/ui/fullscreen/bl;)Lcom/twitter/android/dialog/d;
    .locals 3

    .prologue
    .line 26
    new-instance v0, Lcom/twitter/android/dialog/d;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bk;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a059c

    .line 27
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/bk$1;

    invoke-direct {v2, p0, p2, p1}, Lcom/twitter/android/moments/ui/fullscreen/bk$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/bk;Lcom/twitter/android/moments/ui/fullscreen/bl;Lcom/twitter/model/core/Tweet;)V

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/dialog/d;-><init>(Ljava/lang/String;Lcom/twitter/android/dialog/d$a;)V

    .line 26
    return-object v0
.end method

.method public a(Lcom/twitter/model/moments/Moment;Lcom/twitter/model/moments/a;Lcom/twitter/android/moments/ui/fullscreen/bl;)Lcom/twitter/android/dialog/d;
    .locals 6

    .prologue
    .line 55
    new-instance v0, Lcom/twitter/android/dialog/d;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bk;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a0573

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p2, Lcom/twitter/model/moments/a;->e:Ljava/lang/String;

    aput-object v5, v3, v4

    .line 56
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/bk$3;

    invoke-direct {v2, p0, p3, p1, p2}, Lcom/twitter/android/moments/ui/fullscreen/bk$3;-><init>(Lcom/twitter/android/moments/ui/fullscreen/bk;Lcom/twitter/android/moments/ui/fullscreen/bl;Lcom/twitter/model/moments/Moment;Lcom/twitter/model/moments/a;)V

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/dialog/d;-><init>(Ljava/lang/String;Lcom/twitter/android/dialog/d$a;)V

    .line 55
    return-object v0
.end method

.method public b(Lcom/twitter/android/moments/ui/fullscreen/bg;Lcom/twitter/model/moments/Moment;)Lcom/twitter/android/dialog/d;
    .locals 3

    .prologue
    .line 97
    new-instance v0, Lcom/twitter/android/dialog/d;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bk;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a0592

    .line 98
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/bk$6;

    invoke-direct {v2, p0, p1, p2}, Lcom/twitter/android/moments/ui/fullscreen/bk$6;-><init>(Lcom/twitter/android/moments/ui/fullscreen/bk;Lcom/twitter/android/moments/ui/fullscreen/bg;Lcom/twitter/model/moments/Moment;)V

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/dialog/d;-><init>(Ljava/lang/String;Lcom/twitter/android/dialog/d$a;)V

    .line 97
    return-object v0
.end method

.method public b(Lcom/twitter/model/core/Tweet;Lcom/twitter/android/moments/ui/fullscreen/bl;)Lcom/twitter/android/dialog/d;
    .locals 3

    .prologue
    .line 40
    new-instance v0, Lcom/twitter/android/dialog/d;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bk;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a00a6

    .line 41
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/bk$2;

    invoke-direct {v2, p0, p2, p1}, Lcom/twitter/android/moments/ui/fullscreen/bk$2;-><init>(Lcom/twitter/android/moments/ui/fullscreen/bk;Lcom/twitter/android/moments/ui/fullscreen/bl;Lcom/twitter/model/core/Tweet;)V

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/dialog/d;-><init>(Ljava/lang/String;Lcom/twitter/android/dialog/d$a;)V

    .line 40
    return-object v0
.end method

.method public c(Lcom/twitter/android/moments/ui/fullscreen/bg;Lcom/twitter/model/moments/Moment;)Lcom/twitter/android/dialog/d;
    .locals 3

    .prologue
    .line 111
    new-instance v0, Lcom/twitter/android/dialog/d;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bk;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a08a6

    .line 112
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/bk$7;

    invoke-direct {v2, p0, p1, p2}, Lcom/twitter/android/moments/ui/fullscreen/bk$7;-><init>(Lcom/twitter/android/moments/ui/fullscreen/bk;Lcom/twitter/android/moments/ui/fullscreen/bg;Lcom/twitter/model/moments/Moment;)V

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/dialog/d;-><init>(Ljava/lang/String;Lcom/twitter/android/dialog/d$a;)V

    .line 111
    return-object v0
.end method

.method public c(Lcom/twitter/model/core/Tweet;Lcom/twitter/android/moments/ui/fullscreen/bl;)Lcom/twitter/android/dialog/d;
    .locals 3

    .prologue
    .line 69
    new-instance v0, Lcom/twitter/android/dialog/d;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bk;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a058e

    .line 70
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/bk$4;

    invoke-direct {v2, p0, p2, p1}, Lcom/twitter/android/moments/ui/fullscreen/bk$4;-><init>(Lcom/twitter/android/moments/ui/fullscreen/bk;Lcom/twitter/android/moments/ui/fullscreen/bl;Lcom/twitter/model/core/Tweet;)V

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/dialog/d;-><init>(Ljava/lang/String;Lcom/twitter/android/dialog/d$a;)V

    .line 69
    return-object v0
.end method
