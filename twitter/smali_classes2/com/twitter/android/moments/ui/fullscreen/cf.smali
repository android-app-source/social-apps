.class Lcom/twitter/android/moments/ui/fullscreen/cf;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:Lrx/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/c",
            "<",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentModule;",
            "Lcom/twitter/model/moments/viewmodels/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/android/moments/ui/fullscreen/br;

.field private final d:Lzn;

.field private final e:Lrx/f;

.field private final f:Lcom/twitter/android/moments/ui/fullscreen/co;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentModule;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lrx/j;

.field private i:Z


# direct methods
.method constructor <init>(Landroid/view/LayoutInflater;Lrx/c;Lcom/twitter/android/moments/ui/fullscreen/br;Lzn;Lcom/twitter/android/moments/ui/fullscreen/co;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/LayoutInflater;",
            "Lrx/c",
            "<",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentModule;",
            "Lcom/twitter/model/moments/viewmodels/a;",
            ">;>;",
            "Lcom/twitter/android/moments/ui/fullscreen/br;",
            "Lzn;",
            "Lcom/twitter/android/moments/ui/fullscreen/co;",
            ")V"
        }
    .end annotation

    .prologue
    .line 58
    .line 59
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    .line 58
    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/moments/ui/fullscreen/cf;-><init>(Landroid/view/LayoutInflater;Lrx/c;Lcom/twitter/android/moments/ui/fullscreen/br;Lzn;Lrx/f;Lcom/twitter/android/moments/ui/fullscreen/co;)V

    .line 60
    return-void
.end method

.method constructor <init>(Landroid/view/LayoutInflater;Lrx/c;Lcom/twitter/android/moments/ui/fullscreen/br;Lzn;Lrx/f;Lcom/twitter/android/moments/ui/fullscreen/co;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/LayoutInflater;",
            "Lrx/c",
            "<",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentModule;",
            "Lcom/twitter/model/moments/viewmodels/a;",
            ">;>;",
            "Lcom/twitter/android/moments/ui/fullscreen/br;",
            "Lzn;",
            "Lrx/f;",
            "Lcom/twitter/android/moments/ui/fullscreen/co;",
            ")V"
        }
    .end annotation

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->g:Ljava/util/List;

    .line 69
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->a:Landroid/view/LayoutInflater;

    .line 70
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->b:Lrx/c;

    .line 71
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->c:Lcom/twitter/android/moments/ui/fullscreen/br;

    .line 72
    iput-object p4, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->d:Lzn;

    .line 73
    iput-object p5, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->e:Lrx/f;

    .line 74
    iput-object p6, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->f:Lcom/twitter/android/moments/ui/fullscreen/co;

    .line 75
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/cf;)Lcom/twitter/android/moments/ui/fullscreen/br;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->c:Lcom/twitter/android/moments/ui/fullscreen/br;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 179
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->i:Z

    if-eqz v0, :cond_1

    .line 180
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/MomentModule;

    .line 181
    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->d:Lzn;

    invoke-virtual {v0}, Lcom/twitter/android/moments/viewmodels/MomentModule;->b()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    invoke-virtual {v2, v0}, Lzn;->a(Lcom/twitter/model/moments/Moment;)V

    goto :goto_0

    .line 183
    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->g:Ljava/util/List;

    .line 185
    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->i:Z

    .line 94
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/cf;->d()V

    .line 95
    return-void
.end method

.method public a(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->h:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 79
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->f:Lcom/twitter/android/moments/ui/fullscreen/co;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/fullscreen/co;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    :goto_0
    return-void

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->b:Lrx/c;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->e:Lrx/f;

    .line 83
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/cf$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/cf$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/cf;Landroid/view/ViewGroup;)V

    .line 84
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->h:Lrx/j;

    goto :goto_0
.end method

.method a(Landroid/view/ViewGroup;Ljava/util/Map;)V
    .locals 11
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentModule;",
            "Lcom/twitter/model/moments/viewmodels/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 111
    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 112
    const/4 v1, 0x0

    .line 115
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v5

    .line 116
    const/4 v0, 0x0

    .line 117
    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->g:Ljava/util/List;

    .line 118
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v0

    move v2, v1

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 119
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/moments/viewmodels/MomentModule;

    .line 120
    invoke-virtual {v1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->b()Lcom/twitter/model/moments/Moment;

    move-result-object v7

    .line 122
    iget-wide v8, v7, Lcom/twitter/model/moments/Moment;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 123
    if-nez v3, :cond_0

    .line 124
    const/4 v0, 0x1

    .line 125
    new-instance v1, Lcpb;

    new-instance v3, Ljava/lang/IllegalStateException;

    const-string/jumbo v4, "Got duplicate moment in suggestions"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v3}, Lcpb;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1}, Lcpd;->c(Lcpb;)V

    move v3, v0

    goto :goto_0

    .line 129
    :cond_1
    iget-wide v8, v7, Lcom/twitter/model/moments/Moment;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 132
    if-nez v2, :cond_4

    .line 133
    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->c:Lcom/twitter/android/moments/ui/fullscreen/br;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/a;

    invoke-interface {v4, v2}, Lcom/twitter/android/moments/ui/fullscreen/br;->a(Lcom/twitter/model/moments/viewmodels/a;)V

    .line 134
    const/4 v4, 0x1

    .line 137
    :goto_1
    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->a:Landroid/view/LayoutInflater;

    const v8, 0x7f04020a

    const/4 v9, 0x0

    .line 138
    invoke-virtual {v2, v8, p1, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    .line 139
    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->a:Landroid/view/LayoutInflater;

    const v9, 0x7f0400aa

    const/4 v10, 0x0

    invoke-virtual {v2, v9, p1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 141
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f1100f8

    .line 142
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 141
    invoke-virtual {v2, v9}, Landroid/view/View;->setBackgroundColor(I)V

    .line 143
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 144
    const v2, 0x7f13052a

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/twitter/media/ui/image/MediaImageView;

    .line 145
    invoke-static {v1, v2}, Lcom/twitter/android/moments/data/r;->a(Lcom/twitter/android/moments/viewmodels/MomentModule;Lcom/twitter/media/ui/image/MediaImageView;)V

    .line 147
    const v2, 0x7f1304f2

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v9, v7, Lcom/twitter/model/moments/Moment;->c:Ljava/lang/String;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 148
    const v2, 0x7f1304f4

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 151
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-static {v9, v1}, Lcom/twitter/android/moments/data/x;->a(Landroid/content/res/Resources;Lcom/twitter/android/moments/viewmodels/MomentModule;)Ljava/lang/String;

    move-result-object v1

    .line 150
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    invoke-static {v8}, Lcom/twitter/android/moments/ui/guide/f;->a(Landroid/view/View;)Lcom/twitter/android/moments/ui/guide/f;

    move-result-object v1

    .line 154
    invoke-interface {v1, v7}, Lcom/twitter/android/moments/ui/guide/y;->a(Lcom/twitter/model/moments/Moment;)V

    .line 156
    const v1, 0x7f130527

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/ui/widget/BadgeView;

    .line 157
    invoke-virtual {v7}, Lcom/twitter/model/moments/Moment;->a()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 158
    const v9, 0x7f020842

    const/4 v10, 0x1

    invoke-static {v7, v1, v9, v10}, Lcom/twitter/android/moments/ui/d;->a(Lcom/twitter/model/moments/Moment;Lcom/twitter/ui/widget/BadgeView;IZ)V

    .line 160
    const/16 v1, 0x8

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 166
    :goto_2
    invoke-virtual {p1, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 167
    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/cf$2;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/moments/ui/fullscreen/cf$2;-><init>(Lcom/twitter/android/moments/ui/fullscreen/cf;Ljava/util/Map$Entry;)V

    invoke-virtual {v8, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/cf;->d()V

    move v2, v4

    .line 175
    goto/16 :goto_0

    .line 162
    :cond_2
    const/16 v7, 0x8

    invoke-virtual {v1, v7}, Lcom/twitter/ui/widget/BadgeView;->setVisibility(I)V

    .line 163
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 176
    :cond_3
    return-void

    :cond_4
    move v4, v2

    goto/16 :goto_1
.end method

.method public b()V
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->i:Z

    .line 99
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->h:Lrx/j;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->h:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 104
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cf;->h:Lrx/j;

    .line 106
    :cond_0
    return-void
.end method
