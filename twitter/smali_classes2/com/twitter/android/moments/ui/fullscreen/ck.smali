.class public Lcom/twitter/android/moments/ui/fullscreen/ck;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/fullscreen/cc;


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field private final b:Lcom/twitter/library/widget/renderablecontent/d;


# direct methods
.method constructor <init>(Landroid/view/ViewGroup;Lcom/twitter/library/widget/renderablecontent/d;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/ck;->b:Lcom/twitter/library/widget/renderablecontent/d;

    .line 28
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/ck;->a:Landroid/view/ViewGroup;

    .line 29
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ck;->a:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup;Lcom/twitter/model/core/Tweet;)V
    .locals 1

    .prologue
    .line 23
    invoke-static {p1, p2}, Lcom/twitter/android/moments/ui/fullscreen/ck;->a(Landroid/view/ViewGroup;Lcom/twitter/model/core/Tweet;)Lcom/twitter/library/widget/renderablecontent/d;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/moments/ui/fullscreen/ck;-><init>(Landroid/view/ViewGroup;Lcom/twitter/library/widget/renderablecontent/d;)V

    .line 24
    return-void
.end method

.method static a(Landroid/view/ViewGroup;Lcom/twitter/model/core/Tweet;)Lcom/twitter/library/widget/renderablecontent/d;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 57
    new-instance v0, Lbxy;

    const/4 v1, 0x1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->h:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    move-object v3, p1

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lbxy;-><init>(ZLandroid/app/Activity;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 58
    invoke-virtual {v0}, Lbxy;->a()Lcom/twitter/library/widget/renderablecontent/d;

    move-result-object v0

    .line 57
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ck;->b:Lcom/twitter/library/widget/renderablecontent/d;

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ck;->b:Lcom/twitter/library/widget/renderablecontent/d;

    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->bg_()V

    .line 36
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ck;->b:Lcom/twitter/library/widget/renderablecontent/d;

    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->c()V

    .line 37
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ck;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ck;->b:Lcom/twitter/library/widget/renderablecontent/d;

    invoke-interface {v1}, Lcom/twitter/library/widget/renderablecontent/d;->d()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 39
    :cond_0
    return-void
.end method

.method public b()Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/cc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    invoke-static {p0}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ck;->b:Lcom/twitter/library/widget/renderablecontent/d;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ck;->b:Lcom/twitter/library/widget/renderablecontent/d;

    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->bh_()V

    .line 52
    :cond_0
    return-void
.end method
