.class Lcom/twitter/android/moments/ui/fullscreen/cv;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/twitter/model/moments/viewmodels/a;

.field private final c:Landroid/view/LayoutInflater;

.field private final d:Lcom/twitter/android/moments/ui/fullscreen/bg;

.field private final e:Lcom/twitter/android/moments/ui/fullscreen/bl;

.field private final f:Lcom/twitter/android/moments/ui/fullscreen/cs;

.field private final g:Lcom/twitter/android/moments/data/w;

.field private final h:Lcom/twitter/android/moments/data/am;

.field private final i:Lcom/twitter/util/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lzn;

.field private final k:Lcom/twitter/android/moments/ui/fullscreen/cf;

.field private final l:Lcom/twitter/android/moments/ui/fullscreen/m;

.field private final m:Lcom/twitter/android/moments/ui/fullscreen/cp;

.field private final n:Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;

.field private final o:Lcom/twitter/android/moments/ui/fullscreen/bp;

.field private final p:Lbxo;

.field private final q:Lcom/twitter/android/moments/ui/fullscreen/as;

.field private final r:Lcom/twitter/android/moments/data/h;

.field private final s:Lrx/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/g",
            "<",
            "Lcom/twitter/model/moments/p;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Lcom/twitter/android/moments/data/n;

.field private u:Lcom/twitter/android/moments/ui/fullscreen/co;


# direct methods
.method constructor <init>(Landroid/app/Activity;Landroid/view/LayoutInflater;Lcom/twitter/model/moments/viewmodels/a;Lcom/twitter/android/moments/ui/fullscreen/bg;Lcom/twitter/android/moments/data/w;Lcom/twitter/android/moments/data/am;Lcom/twitter/android/moments/ui/fullscreen/bl;Lcom/twitter/android/moments/ui/fullscreen/cs;Lcom/twitter/util/p;Lzn;Lcom/twitter/android/moments/ui/fullscreen/cf;Lcom/twitter/android/moments/ui/fullscreen/m;Lcom/twitter/android/moments/ui/fullscreen/cp;Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;Lcom/twitter/android/moments/ui/fullscreen/bp;Lbxo;Lcom/twitter/android/moments/ui/fullscreen/as;Lcom/twitter/android/moments/data/h;Lrx/g;Lcom/twitter/android/moments/data/n;Lcom/twitter/android/moments/ui/fullscreen/co;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/view/LayoutInflater;",
            "Lcom/twitter/model/moments/viewmodels/a;",
            "Lcom/twitter/android/moments/ui/fullscreen/bg;",
            "Lcom/twitter/android/moments/data/w;",
            "Lcom/twitter/android/moments/data/am;",
            "Lcom/twitter/android/moments/ui/fullscreen/bl;",
            "Lcom/twitter/android/moments/ui/fullscreen/cs;",
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;",
            "Lzn;",
            "Lcom/twitter/android/moments/ui/fullscreen/cf;",
            "Lcom/twitter/android/moments/ui/fullscreen/m;",
            "Lcom/twitter/android/moments/ui/fullscreen/cp;",
            "Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;",
            "Lcom/twitter/android/moments/ui/fullscreen/bp;",
            "Lbxo;",
            "Lcom/twitter/android/moments/ui/fullscreen/as;",
            "Lcom/twitter/android/moments/data/h;",
            "Lrx/g",
            "<",
            "Lcom/twitter/model/moments/p;",
            ">;",
            "Lcom/twitter/android/moments/data/n;",
            "Lcom/twitter/android/moments/ui/fullscreen/co;",
            ")V"
        }
    .end annotation

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->a:Landroid/app/Activity;

    .line 75
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->c:Landroid/view/LayoutInflater;

    .line 76
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->b:Lcom/twitter/model/moments/viewmodels/a;

    .line 77
    iput-object p4, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->d:Lcom/twitter/android/moments/ui/fullscreen/bg;

    .line 78
    iput-object p5, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->g:Lcom/twitter/android/moments/data/w;

    .line 79
    iput-object p7, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->e:Lcom/twitter/android/moments/ui/fullscreen/bl;

    .line 80
    iput-object p8, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->f:Lcom/twitter/android/moments/ui/fullscreen/cs;

    .line 81
    iput-object p9, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->i:Lcom/twitter/util/p;

    .line 82
    iput-object p10, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->j:Lzn;

    .line 83
    iput-object p11, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->k:Lcom/twitter/android/moments/ui/fullscreen/cf;

    .line 84
    iput-object p12, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->l:Lcom/twitter/android/moments/ui/fullscreen/m;

    .line 85
    iput-object p13, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->m:Lcom/twitter/android/moments/ui/fullscreen/cp;

    .line 86
    iput-object p14, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->n:Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;

    .line 87
    iput-object p6, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->h:Lcom/twitter/android/moments/data/am;

    .line 88
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->o:Lcom/twitter/android/moments/ui/fullscreen/bp;

    .line 89
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->p:Lbxo;

    .line 90
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->q:Lcom/twitter/android/moments/ui/fullscreen/as;

    .line 91
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->r:Lcom/twitter/android/moments/data/h;

    .line 92
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->s:Lrx/g;

    .line 93
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->t:Lcom/twitter/android/moments/data/n;

    .line 94
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->u:Lcom/twitter/android/moments/ui/fullscreen/co;

    .line 95
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/moments/core/ui/widget/sectionpager/d;
    .locals 11

    .prologue
    .line 103
    instance-of v0, p1, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 104
    check-cast v0, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;

    .line 105
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 106
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;->e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->c:Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    if-ne v1, v2, :cond_0

    .line 107
    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/ch;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->a:Landroid/app/Activity;

    invoke-direct {v1, v2, v0, p0}, Lcom/twitter/android/moments/ui/fullscreen/ch;-><init>(Landroid/content/Context;Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;Lcom/twitter/android/moments/ui/fullscreen/cv;)V

    move-object v0, v1

    .line 167
    :goto_0
    return-object v0

    .line 109
    :cond_0
    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/cg;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->a:Landroid/app/Activity;

    invoke-direct {v1, v2, v0, p0}, Lcom/twitter/android/moments/ui/fullscreen/cg;-><init>(Landroid/content/Context;Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;Lcom/twitter/android/moments/ui/fullscreen/cv;)V

    move-object v0, v1

    goto :goto_0

    .line 112
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->k()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->l()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 113
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->b:Lcom/twitter/model/moments/viewmodels/a;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/a;->a()Lcom/twitter/model/moments/Moment;

    move-result-object v1

    .line 115
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->k()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 116
    const/4 v2, 0x0

    .line 117
    instance-of v0, p1, Lcom/twitter/model/moments/viewmodels/n;

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 118
    check-cast v0, Lcom/twitter/model/moments/viewmodels/n;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/n;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v2

    .line 120
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->a:Landroid/app/Activity;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->g:Lcom/twitter/android/moments/data/w;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->h:Lcom/twitter/android/moments/data/am;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->j:Lzn;

    .line 121
    invoke-virtual {v5}, Lzn;->f()Lzq;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->e:Lcom/twitter/android/moments/ui/fullscreen/bl;

    iget-object v8, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->m:Lcom/twitter/android/moments/ui/fullscreen/cp;

    iget-object v7, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->b:Lcom/twitter/model/moments/viewmodels/a;

    .line 122
    invoke-virtual {v7}, Lcom/twitter/model/moments/viewmodels/a;->e()Lcfn;

    move-result-object v9

    iget-object v10, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->s:Lrx/g;

    move-object v7, p1

    .line 120
    invoke-static/range {v0 .. v10}, Lcom/twitter/android/moments/ui/fullscreen/ab;->a(Landroid/app/Activity;Lcom/twitter/model/moments/Moment;Lcom/twitter/model/core/Tweet;Lcom/twitter/android/moments/data/w;Lcom/twitter/android/moments/data/am;Lzq;Lcom/twitter/android/moments/ui/fullscreen/bl;Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/android/moments/ui/fullscreen/cp;Lcfn;Lrx/g;)Lcom/twitter/android/moments/ui/fullscreen/ab;

    move-result-object v0

    goto :goto_0

    .line 123
    :cond_4
    invoke-static {v1}, Lbsd;->a(Lcom/twitter/model/moments/Moment;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 124
    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->c:Landroid/view/LayoutInflater;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->g:Lcom/twitter/android/moments/data/w;

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->j:Lzn;

    .line 125
    invoke-virtual {v0}, Lzn;->g()Lzq;

    move-result-object v6

    iget-object v7, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->k:Lcom/twitter/android/moments/ui/fullscreen/cf;

    iget-object v8, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->d:Lcom/twitter/android/moments/ui/fullscreen/bg;

    move-object v3, p1

    move-object v4, v1

    .line 124
    invoke-static/range {v2 .. v8}, Lcom/twitter/android/moments/ui/fullscreen/ae;->a(Landroid/view/LayoutInflater;Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/moments/Moment;Lcom/twitter/android/moments/data/w;Lzq;Lcom/twitter/android/moments/ui/fullscreen/cf;Lcom/twitter/android/moments/ui/fullscreen/bg;)Lcom/twitter/android/moments/ui/fullscreen/ae;

    move-result-object v0

    goto :goto_0

    .line 128
    :cond_5
    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->c:Landroid/view/LayoutInflater;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->k:Lcom/twitter/android/moments/ui/fullscreen/cf;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->d:Lcom/twitter/android/moments/ui/fullscreen/bg;

    iget-object v7, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->a:Landroid/app/Activity;

    iget-object v8, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->s:Lrx/g;

    iget-object v9, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->t:Lcom/twitter/android/moments/data/n;

    iget-object v10, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->u:Lcom/twitter/android/moments/ui/fullscreen/co;

    move-object v5, p1

    move-object v6, v1

    invoke-static/range {v2 .. v10}, Lcom/twitter/android/moments/ui/fullscreen/bx;->a(Landroid/view/LayoutInflater;Lcom/twitter/android/moments/ui/fullscreen/cf;Lcom/twitter/android/moments/ui/fullscreen/bg;Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/moments/Moment;Landroid/app/Activity;Lrx/g;Lcom/twitter/android/moments/data/n;Lcom/twitter/android/moments/ui/fullscreen/co;)Lcom/twitter/android/moments/ui/fullscreen/bx;

    move-result-object v0

    goto :goto_0

    :cond_6
    move-object v2, p1

    .line 133
    check-cast v2, Lcom/twitter/model/moments/viewmodels/n;

    .line 134
    sget-object v0, Lcom/twitter/android/moments/ui/fullscreen/cv$1;->a:[I

    invoke-virtual {v2}, Lcom/twitter/model/moments/viewmodels/n;->e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 167
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/db;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->a:Landroid/app/Activity;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->e:Lcom/twitter/android/moments/ui/fullscreen/bl;

    new-instance v4, Labz;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->c:Landroid/view/LayoutInflater;

    invoke-direct {v4, v5}, Labz;-><init>(Landroid/view/LayoutInflater;)V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/moments/ui/fullscreen/db;-><init>(Landroid/content/Context;Lcom/twitter/model/moments/viewmodels/n;Lcom/twitter/android/moments/ui/fullscreen/bl;Labz;)V

    goto/16 :goto_0

    .line 137
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->c:Landroid/view/LayoutInflater;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->e:Lcom/twitter/android/moments/ui/fullscreen/bl;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->f:Lcom/twitter/android/moments/ui/fullscreen/cs;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->i:Lcom/twitter/util/p;

    iget-object v6, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->o:Lcom/twitter/android/moments/ui/fullscreen/bp;

    iget-object v7, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->p:Lbxo;

    iget-object v8, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->l:Lcom/twitter/android/moments/ui/fullscreen/m;

    iget-object v9, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->q:Lcom/twitter/android/moments/ui/fullscreen/as;

    .line 138
    invoke-static/range {v0 .. v9}, Lcom/twitter/android/moments/ui/fullscreen/av;->a(Landroid/app/Activity;Landroid/view/LayoutInflater;Lcom/twitter/model/moments/viewmodels/n;Lcom/twitter/android/moments/ui/fullscreen/bl;Lcom/twitter/android/moments/ui/fullscreen/cs;Lcom/twitter/util/p;Lcom/twitter/android/moments/ui/fullscreen/bp;Lbxo;Lcom/twitter/android/moments/ui/fullscreen/m;Lcom/twitter/android/moments/ui/fullscreen/as;)Lcom/twitter/android/moments/ui/fullscreen/av;

    move-result-object v3

    .line 142
    invoke-virtual {v2}, Lcom/twitter/model/moments/viewmodels/n;->e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->c:Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    if-ne v0, v1, :cond_9

    move-object v0, v2

    .line 143
    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    .line 144
    iget-object v1, v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->a:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    sget-object v4, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;->e:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    if-ne v1, v4, :cond_8

    .line 145
    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->r:Lcom/twitter/android/moments/data/h;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/core/Tweet;

    invoke-virtual {v2, v1}, Lcom/twitter/android/moments/data/h;->b(Lcom/twitter/model/core/Tweet;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 146
    const v0, 0x7f0401dc

    invoke-virtual {v3, v0}, Lcom/twitter/android/moments/ui/fullscreen/av;->a(I)V

    move-object v0, v3

    .line 147
    goto/16 :goto_0

    .line 149
    :cond_7
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->a:Landroid/app/Activity;

    .line 150
    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->r:Lcom/twitter/android/moments/data/h;

    .line 149
    invoke-static {v3, v1, v2, v0}, Lcom/twitter/android/moments/ui/fullscreen/cl;->a(Lcom/twitter/android/moments/ui/fullscreen/av;Landroid/content/res/Resources;Lcom/twitter/android/moments/data/h;Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;)Lcom/twitter/android/moments/ui/fullscreen/cl;

    move-result-object v0

    goto/16 :goto_0

    .line 153
    :cond_8
    check-cast v2, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    invoke-static {v2, v3}, Lcom/twitter/android/moments/ui/fullscreen/dl;->a(Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;Lcom/twitter/android/moments/ui/fullscreen/av;)Lcom/twitter/android/moments/ui/fullscreen/dl;

    move-result-object v0

    goto/16 :goto_0

    .line 156
    :cond_9
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->n:Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->a:Landroid/app/Activity;

    .line 157
    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 156
    invoke-static {v3, v0, v2, v1}, Lcom/twitter/android/moments/ui/fullscreen/k;->a(Lcom/twitter/android/moments/ui/fullscreen/av;Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;Lcom/twitter/model/moments/viewmodels/n;Landroid/content/res/Resources;)Lcom/twitter/android/moments/ui/fullscreen/k;

    move-result-object v0

    goto/16 :goto_0

    .line 161
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->c:Landroid/view/LayoutInflater;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->e:Lcom/twitter/android/moments/ui/fullscreen/bl;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->f:Lcom/twitter/android/moments/ui/fullscreen/cs;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->i:Lcom/twitter/util/p;

    iget-object v6, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->o:Lcom/twitter/android/moments/ui/fullscreen/bp;

    iget-object v7, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->p:Lbxo;

    iget-object v8, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->l:Lcom/twitter/android/moments/ui/fullscreen/m;

    iget-object v9, p0, Lcom/twitter/android/moments/ui/fullscreen/cv;->q:Lcom/twitter/android/moments/ui/fullscreen/as;

    invoke-static/range {v0 .. v9}, Lcom/twitter/android/moments/ui/fullscreen/av;->a(Landroid/app/Activity;Landroid/view/LayoutInflater;Lcom/twitter/model/moments/viewmodels/n;Lcom/twitter/android/moments/ui/fullscreen/bl;Lcom/twitter/android/moments/ui/fullscreen/cs;Lcom/twitter/util/p;Lcom/twitter/android/moments/ui/fullscreen/bp;Lbxo;Lcom/twitter/android/moments/ui/fullscreen/m;Lcom/twitter/android/moments/ui/fullscreen/as;)Lcom/twitter/android/moments/ui/fullscreen/av;

    move-result-object v0

    goto/16 :goto_0

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
