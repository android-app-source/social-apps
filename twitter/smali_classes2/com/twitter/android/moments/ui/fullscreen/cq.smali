.class public Lcom/twitter/android/moments/ui/fullscreen/cq;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/fullscreen/cc;


# instance fields
.field private final a:Lcom/twitter/model/core/r;

.field private final b:Lcom/twitter/android/moments/ui/fullscreen/cr;

.field private final c:Lbxo;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/cr;Lcom/twitter/model/core/r;Lbxo;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/cq;->a:Lcom/twitter/model/core/r;

    .line 18
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/cq;->b:Lcom/twitter/android/moments/ui/fullscreen/cr;

    .line 19
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/cq;->c:Lbxo;

    .line 20
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 24
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cq;->c:Lbxo;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/cq;->a:Lcom/twitter/model/core/r;

    invoke-virtual {v0, v1}, Lbxo;->a(Lcom/twitter/model/core/r;)Ljava/lang/String;

    move-result-object v0

    .line 25
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/cq;->b:Lcom/twitter/android/moments/ui/fullscreen/cr;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/cq;->a:Lcom/twitter/model/core/r;

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/moments/ui/fullscreen/cr;->a(Lcom/twitter/model/core/r;Ljava/lang/CharSequence;)V

    .line 26
    return-void
.end method

.method public b()Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/cc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    invoke-static {p0}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 36
    return-void
.end method
