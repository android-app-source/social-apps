.class Lcom/twitter/android/moments/ui/fullscreen/dg$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/fullscreen/dg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/q",
        "<",
        "Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$AudioFailInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/ui/fullscreen/dg;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/dg;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/dg$2;->a:Lcom/twitter/android/moments/ui/fullscreen/dg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEvent(Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$AudioFailInfo;)V
    .locals 3

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dg$2;->a:Lcom/twitter/android/moments/ui/fullscreen/dg;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dg;->a(Lcom/twitter/android/moments/ui/fullscreen/dg;)Lcom/twitter/model/moments/viewmodels/p;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$AudioFailInfo;->a:Lcom/twitter/model/moments/viewmodels/p;

    if-ne v0, v1, :cond_0

    .line 67
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dg$2;->a:Lcom/twitter/android/moments/ui/fullscreen/dg;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dg;->b(Lcom/twitter/android/moments/ui/fullscreen/dg;)Lcom/twitter/android/moments/ui/fullscreen/ad;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dg$2;->a:Lcom/twitter/android/moments/ui/fullscreen/dg;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/fullscreen/dg;->a(Lcom/twitter/android/moments/ui/fullscreen/dg;)Lcom/twitter/model/moments/viewmodels/p;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/p;->j()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/g;

    invoke-direct {v2, p1}, Lcom/twitter/android/moments/ui/fullscreen/g;-><init>(Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$AudioFailInfo;)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 69
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dg$2;->a:Lcom/twitter/android/moments/ui/fullscreen/dg;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/dg;->b(Lcom/twitter/android/moments/ui/fullscreen/dg;Z)Z

    .line 71
    :cond_0
    return-void
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 63
    check-cast p1, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$AudioFailInfo;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/dg$2;->onEvent(Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$AudioFailInfo;)V

    return-void
.end method
