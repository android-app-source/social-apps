.class public Lcom/twitter/android/moments/ui/fullscreen/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/fullscreen/bz;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/twitter/android/moments/ui/fullscreen/bt;

.field private final c:Lcom/twitter/android/moments/ui/fullscreen/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/moments/ui/fullscreen/d",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/twitter/android/moments/ui/fullscreen/bt;Lcom/twitter/android/moments/ui/fullscreen/d;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/twitter/android/moments/ui/fullscreen/bt;",
            "Lcom/twitter/android/moments/ui/fullscreen/d",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/b;->a:Landroid/app/Activity;

    .line 38
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/b;->b:Lcom/twitter/android/moments/ui/fullscreen/bt;

    .line 39
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/b;->c:Lcom/twitter/android/moments/ui/fullscreen/d;

    .line 40
    return-void
.end method

.method public static a(Landroid/app/Activity;)Lcom/twitter/android/moments/ui/fullscreen/b;
    .locals 5

    .prologue
    .line 23
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/b;

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/bt;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/fullscreen/bt;-><init>(Landroid/app/Activity;)V

    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/d;

    new-instance v3, Lcom/twitter/android/moments/ui/fullscreen/b$1;

    invoke-direct {v3, p0}, Lcom/twitter/android/moments/ui/fullscreen/b$1;-><init>(Landroid/app/Activity;)V

    .line 30
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/twitter/android/moments/ui/fullscreen/d;-><init>(Ljava/util/concurrent/Callable;Lrx/f;)V

    invoke-direct {v0, p0, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/b;-><init>(Landroid/app/Activity;Lcom/twitter/android/moments/ui/fullscreen/bt;Lcom/twitter/android/moments/ui/fullscreen/d;)V

    .line 23
    return-object v0
.end method


# virtual methods
.method public a(Z)V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/b;->c:Lcom/twitter/android/moments/ui/fullscreen/d;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 53
    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/b;->a:Landroid/app/Activity;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 58
    :goto_0
    return-void

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/b;->a:Landroid/app/Activity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/b;->b:Lcom/twitter/android/moments/ui/fullscreen/bt;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/bt;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/b;->a:Landroid/app/Activity;

    .line 47
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 46
    :goto_0
    return v0

    .line 47
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
