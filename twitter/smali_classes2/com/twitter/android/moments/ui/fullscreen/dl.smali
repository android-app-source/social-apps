.class public Lcom/twitter/android/moments/ui/fullscreen/dl;
.super Lcom/twitter/android/moments/ui/fullscreen/af;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/fullscreen/cj;


# instance fields
.field private final b:Lacf;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/av;Lacf;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/af;-><init>(Lcom/twitter/android/moments/ui/fullscreen/av;)V

    .line 30
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/dl;->b:Lacf;

    .line 31
    return-void
.end method

.method public static a(Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;Lcom/twitter/android/moments/ui/fullscreen/av;)Lcom/twitter/android/moments/ui/fullscreen/dl;
    .locals 3

    .prologue
    .line 20
    invoke-virtual {p1}, Lcom/twitter/android/moments/ui/fullscreen/av;->f()Labq;

    move-result-object v1

    .line 21
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    .line 22
    new-instance v2, Lacf;

    invoke-direct {v2, v1, v0}, Lacf;-><init>(Labq;Lcom/twitter/model/core/Tweet;)V

    .line 24
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/dl;

    invoke-direct {v0, p1, v2}, Lcom/twitter/android/moments/ui/fullscreen/dl;-><init>(Lcom/twitter/android/moments/ui/fullscreen/av;Lacf;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/library/av/playback/aa;)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dl;->b:Lacf;

    invoke-virtual {v0, p1}, Lacf;->a(Lcom/twitter/library/av/playback/aa;)V

    .line 46
    return-void
.end method

.method public a(Lcom/twitter/model/av/AVMedia;)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dl;->b:Lacf;

    invoke-virtual {v0}, Lacf;->b()V

    .line 41
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dl;->b:Lacf;

    invoke-virtual {v0}, Lacf;->a()V

    .line 36
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dl;->b:Lacf;

    invoke-virtual {v0}, Lacf;->c()V

    .line 51
    return-void
.end method
