.class public Lcom/twitter/android/moments/ui/fullscreen/h;
.super Lcom/twitter/android/moments/ui/fullscreen/o;
.source "Twttr"


# instance fields
.field private final c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/widget/ProgressBar;Landroid/widget/TextView;Landroid/view/View;Landroid/content/res/Resources;Landroid/widget/ProgressBar;Lcom/twitter/model/moments/MomentPageDisplayMode;Ljava/lang/String;Lcom/twitter/android/moments/ui/fullscreen/ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/widget/ProgressBar;",
            "Landroid/widget/TextView;",
            "Landroid/view/View;",
            "Landroid/content/res/Resources;",
            "Landroid/widget/ProgressBar;",
            "Lcom/twitter/model/moments/MomentPageDisplayMode;",
            "Ljava/lang/String;",
            "Lcom/twitter/android/moments/ui/fullscreen/ad",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct/range {p0 .. p9}, Lcom/twitter/android/moments/ui/fullscreen/o;-><init>(Landroid/view/View;Landroid/widget/ProgressBar;Landroid/widget/TextView;Landroid/view/View;Landroid/content/res/Resources;Landroid/widget/ProgressBar;Lcom/twitter/model/moments/MomentPageDisplayMode;Ljava/lang/String;Lcom/twitter/android/moments/ui/fullscreen/ad;)V

    .line 26
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/h;->c:Landroid/widget/TextView;

    .line 27
    return-void
.end method


# virtual methods
.method a(Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;)V
    .locals 2

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/o;->a(Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;)V

    .line 32
    instance-of v0, p1, Lcom/twitter/android/moments/ui/fullscreen/g;

    if-eqz v0, :cond_1

    .line 33
    check-cast p1, Lcom/twitter/android/moments/ui/fullscreen/g;

    iget-object v0, p1, Lcom/twitter/android/moments/ui/fullscreen/g;->a:Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$AudioFailInfo;

    .line 34
    sget-object v1, Lcom/twitter/android/av/audio/d;->a:Lcom/twitter/android/av/audio/d;

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$AudioFailInfo;->b:Lcom/twitter/android/av/audio/AudioCardError;

    invoke-interface {v1, v0}, Lcom/twitter/android/av/audio/d;->b(Lcom/twitter/android/av/audio/AudioCardError;)I

    move-result v0

    .line 36
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/h;->c:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const v0, 0x7f0a0572

    :cond_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 39
    :cond_1
    return-void
.end method
