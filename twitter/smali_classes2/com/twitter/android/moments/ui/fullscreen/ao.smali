.class public Lcom/twitter/android/moments/ui/fullscreen/ao;
.super Landroid/support/v4/view/ViewPager$SimpleOnPageChangeListener;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/view/ViewPager$SimpleOnPageChangeListener;",
        "Lcom/twitter/util/q",
        "<",
        "Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/moments/core/ui/widget/capsule/a;

.field private final c:Lcom/twitter/android/moments/data/h;

.field private final d:Lcom/twitter/android/moments/ui/fullscreen/ap;

.field private final e:Lcom/twitter/android/moments/ui/fullscreen/s;

.field private f:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/moments/core/ui/widget/capsule/a;Lcom/twitter/android/moments/data/h;Lcom/twitter/android/moments/ui/fullscreen/ap;Lcom/twitter/android/moments/ui/fullscreen/s;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager$SimpleOnPageChangeListener;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/ao;->a:Landroid/content/Context;

    .line 36
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/ao;->c:Lcom/twitter/android/moments/data/h;

    .line 37
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/ao;->b:Lcom/twitter/moments/core/ui/widget/capsule/a;

    .line 38
    iput-object p4, p0, Lcom/twitter/android/moments/ui/fullscreen/ao;->d:Lcom/twitter/android/moments/ui/fullscreen/ap;

    .line 39
    iput-object p5, p0, Lcom/twitter/android/moments/ui/fullscreen/ao;->e:Lcom/twitter/android/moments/ui/fullscreen/s;

    .line 40
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/ao;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ao;->a:Landroid/content/Context;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ao;->b:Lcom/twitter/moments/core/ui/widget/capsule/a;

    invoke-interface {v0}, Lcom/twitter/moments/core/ui/widget/capsule/a;->c()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 57
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ao;->f:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ao;->f:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    if-eq v1, v0, :cond_0

    .line 58
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ao;->f:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    invoke-virtual {v1, p0}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->b(Lcom/twitter/util/q;)V

    .line 60
    :cond_0
    instance-of v1, v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    if-nez v1, :cond_1

    .line 61
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ao;->d:Lcom/twitter/android/moments/ui/fullscreen/ap;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/fullscreen/ap;->a()V

    .line 88
    :goto_0
    return-void

    .line 65
    :cond_1
    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    .line 66
    iget-object v1, v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->a:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    sget-object v2, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;->e:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    if-eq v1, v2, :cond_2

    .line 67
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ao;->d:Lcom/twitter/android/moments/ui/fullscreen/ap;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/fullscreen/ap;->a()V

    goto :goto_0

    .line 71
    :cond_2
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ao;->c:Lcom/twitter/android/moments/data/h;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/moments/data/h;->b(Lcom/twitter/model/core/Tweet;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 72
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ao;->d:Lcom/twitter/android/moments/ui/fullscreen/ap;

    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/ao$1;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/moments/ui/fullscreen/ao$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ao;Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;)V

    invoke-interface {v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/ap;->a(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 81
    :cond_3
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v1

    if-nez v1, :cond_4

    .line 82
    invoke-virtual {v0, p0}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->a(Lcom/twitter/util/q;)V

    .line 83
    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ao;->f:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    .line 84
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ao;->d:Lcom/twitter/android/moments/ui/fullscreen/ap;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/fullscreen/ap;->a()V

    goto :goto_0

    .line 86
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ao;->d:Lcom/twitter/android/moments/ui/fullscreen/ap;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/fullscreen/ap;->a()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/fullscreen/ao;)Lcom/twitter/android/moments/data/h;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ao;->c:Lcom/twitter/android/moments/data/h;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/fullscreen/ao;)Lcom/twitter/android/moments/ui/fullscreen/s;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ao;->e:Lcom/twitter/android/moments/ui/fullscreen/s;

    return-object v0
.end method


# virtual methods
.method public onEvent(Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;)V
    .locals 0

    .prologue
    .line 49
    if-eqz p1, :cond_0

    .line 50
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/ao;->a()V

    .line 51
    invoke-virtual {p1, p0}, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;->b(Lcom/twitter/util/q;)V

    .line 53
    :cond_0
    return-void
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ao;->onEvent(Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;)V

    return-void
.end method

.method public onPageSelected(I)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/ao;->a()V

    .line 45
    return-void
.end method
