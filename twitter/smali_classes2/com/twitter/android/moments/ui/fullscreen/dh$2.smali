.class Lcom/twitter/android/moments/ui/fullscreen/dh$2;
.super Lcom/twitter/library/av/l;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/fullscreen/dh;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field final synthetic b:Lcom/twitter/android/moments/ui/fullscreen/dh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/dh;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-direct {p0}, Lcom/twitter/library/av/l;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 119
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->c(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/android/moments/ui/fullscreen/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ci;->h()V

    .line 120
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->e(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/android/moments/ui/fullscreen/ad;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/fullscreen/dh;->d(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->j()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->g:Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;

    .line 121
    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->a()Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;

    move-result-object v2

    .line 120
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 122
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/dh;->a(Lcom/twitter/android/moments/ui/fullscreen/dh;Z)Z

    .line 123
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/dh;->b(Lcom/twitter/android/moments/ui/fullscreen/dh;Z)Z

    .line 124
    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v0, p1, p2}, Lcom/twitter/android/moments/ui/fullscreen/dh;->a(Lcom/twitter/android/moments/ui/fullscreen/dh;II)V

    .line 97
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->a()V

    .line 82
    return-void
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V
    .locals 3

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->b(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/android/moments/ui/video/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/video/a;->f()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v0

    .line 61
    if-eqz v0, :cond_0

    .line 62
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/fullscreen/dh;->c(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/android/moments/ui/fullscreen/ci;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/fullscreen/ci;->a(Lcom/twitter/model/av/AVMedia;)V

    .line 63
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->e(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/android/moments/ui/fullscreen/ad;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/fullscreen/dh;->d(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->j()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->f:Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;

    .line 64
    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->a()Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;

    move-result-object v2

    .line 63
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 65
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/dh;->a(Lcom/twitter/android/moments/ui/fullscreen/dh;Z)Z

    .line 67
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/av/playback/aa;)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->c(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/android/moments/ui/fullscreen/ci;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ci;->a(Lcom/twitter/library/av/playback/aa;)V

    .line 72
    return-void
.end method

.method public a(Lcom/twitter/model/av/c;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->a()V

    .line 87
    return-void
.end method

.method public b(II)V
    .locals 3

    .prologue
    .line 101
    const/16 v0, 0x2bd

    if-ne p1, v0, :cond_1

    .line 102
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->c(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/android/moments/ui/fullscreen/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ci;->g()V

    .line 103
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->e(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/android/moments/ui/fullscreen/ad;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/fullscreen/dh;->d(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->j()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->b:Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;

    .line 104
    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->a()Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;

    move-result-object v2

    .line 103
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 105
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/dh;->a(Lcom/twitter/android/moments/ui/fullscreen/dh;Z)Z

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    const/16 v0, 0x2be

    if-ne p1, v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->b(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/android/moments/ui/video/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/video/a;->f()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->v()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 108
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->b(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/android/moments/ui/video/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/video/a;->f()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v0

    .line 109
    sget-boolean v1, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->a:Z

    if-nez v1, :cond_2

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 110
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/fullscreen/dh;->c(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/android/moments/ui/fullscreen/ci;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/fullscreen/ci;->a(Lcom/twitter/model/av/AVMedia;)V

    .line 112
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->e(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/android/moments/ui/fullscreen/ad;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/fullscreen/dh;->d(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->j()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->f:Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;

    .line 113
    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->a()Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;

    move-result-object v2

    .line 112
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 114
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/dh;->a(Lcom/twitter/android/moments/ui/fullscreen/dh;Z)Z

    goto :goto_0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->b(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/android/moments/ui/video/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/video/a;->c()V

    .line 92
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$2;->b:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->c(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/android/moments/ui/fullscreen/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ci;->h()V

    .line 77
    return-void
.end method
