.class public Lcom/twitter/android/moments/ui/fullscreen/j;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Labd;

.field private final b:Lcom/twitter/android/moments/ui/fullscreen/ak;

.field private final c:Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;

.field private final d:Lcom/twitter/model/moments/viewmodels/p;

.field private final e:Lcom/twitter/util/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/q",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Labd;Lcom/twitter/android/moments/ui/fullscreen/ak;Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;Lcom/twitter/model/moments/viewmodels/p;)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/j$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/fullscreen/j$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/j;)V

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/j;->e:Lcom/twitter/util/q;

    .line 28
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/j;->a:Labd;

    .line 29
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/j;->b:Lcom/twitter/android/moments/ui/fullscreen/ak;

    .line 30
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/j;->c:Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;

    .line 31
    iput-object p4, p0, Lcom/twitter/android/moments/ui/fullscreen/j;->d:Lcom/twitter/model/moments/viewmodels/p;

    .line 32
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/j;->a:Labd;

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/j$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/fullscreen/j$2;-><init>(Lcom/twitter/android/moments/ui/fullscreen/j;)V

    invoke-virtual {v0, v1}, Labd;->a(Landroid/view/View$OnClickListener;)V

    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/j;->a:Labd;

    invoke-virtual {v0}, Labd;->b()V

    .line 40
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/j;)Labd;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/j;->a:Labd;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/j;Z)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/j;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 64
    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/j;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/j;->a:Labd;

    invoke-virtual {v0}, Labd;->a()V

    .line 69
    :goto_0
    return-void

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/j;->a:Labd;

    invoke-virtual {v0}, Labd;->b()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/fullscreen/j;)Lcom/twitter/android/moments/ui/fullscreen/ak;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/j;->b:Lcom/twitter/android/moments/ui/fullscreen/ak;

    return-object v0
.end method

.method private d()Z
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/j;->c:Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/j;->d:Lcom/twitter/model/moments/viewmodels/p;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;->a(Lcom/twitter/model/moments/viewmodels/p;)Lcom/twitter/model/av/Audio;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/j;->b:Lcom/twitter/android/moments/ui/fullscreen/ak;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ak;->c()Lcom/twitter/util/p;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/j;->e:Lcom/twitter/util/q;

    invoke-virtual {v0, v1}, Lcom/twitter/util/p;->a(Lcom/twitter/util/q;)Z

    .line 47
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/j;->b:Lcom/twitter/android/moments/ui/fullscreen/ak;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ak;->c()Lcom/twitter/util/p;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/j;->e:Lcom/twitter/util/q;

    invoke-virtual {v0, v1}, Lcom/twitter/util/p;->b(Lcom/twitter/util/q;)Z

    .line 54
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/j;->b:Lcom/twitter/android/moments/ui/fullscreen/ak;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ak;->a()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/fullscreen/j;->a(Z)V

    .line 61
    return-void
.end method
