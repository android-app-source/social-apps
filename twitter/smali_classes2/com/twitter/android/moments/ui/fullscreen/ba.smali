.class public Lcom/twitter/android/moments/ui/fullscreen/ba;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field private final b:Landroid/app/Activity;

.field private final c:Lcom/twitter/util/object/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/d",
            "<",
            "Landroid/view/ViewGroup;",
            "Lcom/twitter/android/moments/ui/fullscreen/bb;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/twitter/util/object/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/d",
            "<",
            "Landroid/view/ViewGroup;",
            "Lcom/twitter/android/moments/ui/guide/y;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:Lcom/twitter/android/moments/ui/fullscreen/bb;

.field private h:Lcom/twitter/android/moments/ui/guide/y;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/twitter/util/object/d;Lcom/twitter/util/object/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/view/ViewGroup;",
            "Lcom/twitter/util/object/d",
            "<",
            "Landroid/view/ViewGroup;",
            "Lcom/twitter/android/moments/ui/fullscreen/bb;",
            ">;",
            "Lcom/twitter/util/object/d",
            "<",
            "Landroid/view/ViewGroup;",
            "Lcom/twitter/android/moments/ui/guide/y;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->a:Landroid/view/ViewGroup;

    .line 48
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->b:Landroid/app/Activity;

    .line 49
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->c:Lcom/twitter/util/object/d;

    .line 50
    iput-object p4, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->d:Lcom/twitter/util/object/d;

    .line 51
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/ba;Lcom/twitter/model/moments/Moment;Lcom/twitter/model/moments/o;Ljava/lang/String;Lcom/twitter/media/ui/image/MediaImageView;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/moments/ui/fullscreen/ba;->a(Lcom/twitter/model/moments/Moment;Lcom/twitter/model/moments/o;Ljava/lang/String;Lcom/twitter/media/ui/image/MediaImageView;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/ba;Lcom/twitter/model/moments/o;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ba;->b(Lcom/twitter/model/moments/o;)V

    return-void
.end method

.method private a(Lcom/twitter/model/moments/Moment;Lcom/twitter/model/moments/o;Ljava/lang/String;Lcom/twitter/media/ui/image/MediaImageView;)V
    .locals 6

    .prologue
    .line 104
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->b:Landroid/app/Activity;

    iget-object v1, p2, Lcom/twitter/model/moments/o;->b:Lcom/twitter/model/moments/Moment;

    iget-wide v2, v1, Lcom/twitter/model/moments/Moment;->b:J

    invoke-static {v0, v2, v3}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->c(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v0

    .line 106
    invoke-static {p4}, Lcom/twitter/util/math/Size;->a(Landroid/view/View;)Lcom/twitter/util/math/Size;

    move-result-object v2

    iget-object v1, p2, Lcom/twitter/model/moments/o;->c:Lceo;

    iget-object v4, v1, Lceo;->e:Lcom/twitter/model/moments/e;

    sget-object v5, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$Type;->b:Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$Type;

    move-object v1, p1

    move-object v3, p3

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->a(Landroid/content/Intent;Lcom/twitter/model/moments/Moment;Lcom/twitter/util/math/Size;Ljava/lang/String;Lcom/twitter/model/moments/e;Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$Type;)Landroid/content/Intent;

    .line 108
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->b:Landroid/app/Activity;

    invoke-static {v1, v0, p4}, Lcom/twitter/ui/anim/b;->a(Landroid/app/Activity;Landroid/content/Intent;Landroid/view/View;)V

    .line 110
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/ba;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->e:Z

    return v0
.end method

.method private b(Lcom/twitter/model/moments/o;)V
    .locals 4

    .prologue
    .line 113
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->b:Landroid/app/Activity;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->b:Landroid/app/Activity;

    iget-object v2, p1, Lcom/twitter/model/moments/o;->b:Lcom/twitter/model/moments/Moment;

    iget-wide v2, v2, Lcom/twitter/model/moments/Moment;->b:J

    .line 114
    invoke-static {v1, v2, v3}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->c(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v1

    .line 113
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 116
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->b:Landroid/app/Activity;

    const v1, 0x7f050041

    const v2, 0x7f05003d

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 118
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/fullscreen/ba;)Z
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/ba;->c()Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/fullscreen/ba;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->f:Ljava/lang/String;

    return-object v0
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->g:Lcom/twitter/android/moments/ui/fullscreen/bb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->g:Lcom/twitter/android/moments/ui/fullscreen/bb;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/bb;->b()Lcom/twitter/media/ui/image/MediaImageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Lcom/twitter/media/request/ImageResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->g:Lcom/twitter/android/moments/ui/fullscreen/bb;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/fullscreen/bb;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/bb;->c()Lrx/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lrx/c;->d(I)Lrx/c;

    move-result-object v0

    invoke-virtual {v0}, Lrx/c;->b()Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/model/moments/o;)V
    .locals 3

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/fullscreen/ba;->b()V

    .line 55
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->g:Lcom/twitter/android/moments/ui/fullscreen/bb;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/fullscreen/bb;

    .line 56
    iget-object v2, p1, Lcom/twitter/model/moments/o;->b:Lcom/twitter/model/moments/Moment;

    .line 57
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->h:Lcom/twitter/android/moments/ui/guide/y;

    .line 58
    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/moments/ui/guide/y;

    .line 59
    invoke-interface {v1, v2}, Lcom/twitter/android/moments/ui/guide/y;->a(Lcom/twitter/model/moments/Moment;)V

    .line 60
    iget-object v1, v2, Lcom/twitter/model/moments/Moment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/bb;->a(Ljava/lang/CharSequence;)V

    .line 61
    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/ba$1;

    invoke-direct {v1, p0, v2, p1, v0}, Lcom/twitter/android/moments/ui/fullscreen/ba$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ba;Lcom/twitter/model/moments/Moment;Lcom/twitter/model/moments/o;Lcom/twitter/android/moments/ui/fullscreen/bb;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/bb;->a(Landroid/view/View$OnClickListener;)V

    .line 76
    return-void
.end method

.method public a(Lcom/twitter/model/moments/o;Lcom/twitter/model/core/Tweet;)V
    .locals 6

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/fullscreen/ba;->b()V

    .line 80
    iget-object v0, p1, Lcom/twitter/model/moments/o;->c:Lceo;

    iget-object v1, v0, Lceo;->g:Lcem;

    .line 81
    if-eqz v1, :cond_0

    .line 82
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->g:Lcom/twitter/android/moments/ui/fullscreen/bb;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/fullscreen/bb;

    .line 83
    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/bb;->b()Lcom/twitter/media/ui/image/MediaImageView;

    move-result-object v0

    .line 84
    iget-object v2, p1, Lcom/twitter/model/moments/o;->c:Lceo;

    iget-object v2, v2, Lceo;->e:Lcom/twitter/model/moments/e;

    .line 85
    invoke-static {v0}, Lcom/twitter/util/math/Size;->a(Landroid/view/View;)Lcom/twitter/util/math/Size;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/util/math/Size;->g()F

    move-result v3

    .line 84
    invoke-static {v2, v3}, Lcom/twitter/model/moments/e;->a(Lcom/twitter/model/moments/e;F)Lcom/twitter/model/moments/d;

    move-result-object v2

    .line 86
    iget-wide v4, v1, Lcem;->b:J

    iget-object v3, v1, Lcem;->d:Ljava/lang/String;

    invoke-static {p2, v4, v5, v3}, Lcom/twitter/android/moments/data/o;->a(Lcom/twitter/model/core/Tweet;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 87
    iget-object v1, v1, Lcem;->c:Lcom/twitter/util/math/Size;

    invoke-static {v3, v0, v2, v1}, Lcom/twitter/android/moments/data/r;->a(Ljava/lang/String;Lcom/twitter/media/ui/image/MediaImageView;Lcom/twitter/model/moments/d;Lcom/twitter/util/math/Size;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 89
    iput-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->f:Ljava/lang/String;

    .line 91
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 133
    iput-boolean p1, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->e:Z

    .line 134
    return-void
.end method

.method b()V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->g:Lcom/twitter/android/moments/ui/fullscreen/bb;

    if-nez v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->c:Lcom/twitter/util/object/d;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->a:Landroid/view/ViewGroup;

    invoke-interface {v0, v1}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/fullscreen/bb;

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->g:Lcom/twitter/android/moments/ui/fullscreen/bb;

    .line 124
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->g:Lcom/twitter/android/moments/ui/fullscreen/bb;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/fullscreen/bb;->aN_()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->a:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 127
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->h:Lcom/twitter/android/moments/ui/guide/y;

    if-nez v0, :cond_1

    .line 128
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->d:Lcom/twitter/util/object/d;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->a:Landroid/view/ViewGroup;

    invoke-interface {v0, v1}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/guide/y;

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba;->h:Lcom/twitter/android/moments/ui/guide/y;

    .line 130
    :cond_1
    return-void
.end method
