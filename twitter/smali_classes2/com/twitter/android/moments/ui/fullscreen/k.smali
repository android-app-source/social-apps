.class public Lcom/twitter/android/moments/ui/fullscreen/k;
.super Lcom/twitter/android/moments/ui/fullscreen/af;
.source "Twttr"


# instance fields
.field private final b:Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;

.field private final c:Lcom/twitter/model/moments/viewmodels/n;

.field private final d:Lcom/twitter/util/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$AudioStartInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/twitter/util/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$AudioFailInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/content/res/Resources;

.field private final g:Labe;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/av;Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;Lcom/twitter/model/moments/viewmodels/n;Landroid/content/res/Resources;Labe;)V
    .locals 3

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/af;-><init>(Lcom/twitter/android/moments/ui/fullscreen/av;)V

    .line 20
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/k$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/fullscreen/k$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/k;)V

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/k;->d:Lcom/twitter/util/q;

    .line 31
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/k$2;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/fullscreen/k$2;-><init>(Lcom/twitter/android/moments/ui/fullscreen/k;)V

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/k;->e:Lcom/twitter/util/q;

    .line 59
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/k;->b:Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;

    .line 60
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/k;->c:Lcom/twitter/model/moments/viewmodels/n;

    .line 61
    iput-object p4, p0, Lcom/twitter/android/moments/ui/fullscreen/k;->f:Landroid/content/res/Resources;

    .line 62
    iput-object p5, p0, Lcom/twitter/android/moments/ui/fullscreen/k;->g:Labe;

    .line 63
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/k;->b:Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/k;->d:Lcom/twitter/util/q;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/k;->e:Lcom/twitter/util/q;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;->a(Lcom/twitter/util/q;Lcom/twitter/util/q;)V

    .line 64
    return-void
.end method

.method public static a(Lcom/twitter/android/moments/ui/fullscreen/av;Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;Lcom/twitter/model/moments/viewmodels/n;Landroid/content/res/Resources;)Lcom/twitter/android/moments/ui/fullscreen/k;
    .locals 6

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/fullscreen/av;->f()Labq;

    move-result-object v0

    .line 48
    new-instance v5, Labe;

    invoke-direct {v5, v0}, Labe;-><init>(Labq;)V

    .line 50
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/k;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/moments/ui/fullscreen/k;-><init>(Lcom/twitter/android/moments/ui/fullscreen/av;Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;Lcom/twitter/model/moments/viewmodels/n;Landroid/content/res/Resources;Labe;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/k;)Lcom/twitter/model/moments/viewmodels/n;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/k;->c:Lcom/twitter/model/moments/viewmodels/n;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/fullscreen/k;)Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/k;->f:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/fullscreen/k;)Labe;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/k;->g:Labe;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/k;->b:Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/k;->d:Lcom/twitter/util/q;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/k;->e:Lcom/twitter/util/q;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;->b(Lcom/twitter/util/q;Lcom/twitter/util/q;)V

    .line 69
    invoke-super {p0}, Lcom/twitter/android/moments/ui/fullscreen/af;->a()V

    .line 70
    return-void
.end method
