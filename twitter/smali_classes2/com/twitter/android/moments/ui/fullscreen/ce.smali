.class public Lcom/twitter/android/moments/ui/fullscreen/ce;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/moments/data/s;

.field private final b:Lxv;

.field private final c:Lrx/subjects/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/d",
            "<",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentModule;",
            "Lcom/twitter/model/moments/viewmodels/a;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentModule;",
            "Lcom/twitter/model/moments/viewmodels/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lrx/j;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/data/s;Lxv;Lrx/subjects/d;J)V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/data/s;",
            "Lxv;",
            "Lrx/subjects/d",
            "<",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentModule;",
            "Lcom/twitter/model/moments/viewmodels/a;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentModule;",
            "Lcom/twitter/model/moments/viewmodels/a;",
            ">;>;J)V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/ce;->a:Lcom/twitter/android/moments/data/s;

    .line 43
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/ce;->b:Lxv;

    .line 44
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/ce;->c:Lrx/subjects/d;

    .line 46
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ce;->a:Lcom/twitter/android/moments/data/s;

    invoke-virtual {v0, p4, p5}, Lcom/twitter/android/moments/data/s;->a(J)Lrx/c;

    move-result-object v0

    .line 47
    invoke-static {}, Lcre;->b()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->k(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 48
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/ce;->e()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->f(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ce;->c:Lrx/subjects/d;

    .line 49
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/d;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ce;->d:Lrx/j;

    .line 50
    return-void
.end method

.method public static a(Lcom/twitter/android/moments/data/s;Lxv;J)Lcom/twitter/android/moments/ui/fullscreen/ce;
    .locals 6

    .prologue
    .line 55
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/ce;

    .line 56
    invoke-static {}, Lrx/subjects/b;->r()Lrx/subjects/b;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/moments/ui/fullscreen/ce;-><init>(Lcom/twitter/android/moments/data/s;Lxv;Lrx/subjects/d;J)V

    .line 55
    return-object v0
.end method

.method static synthetic a(Ljava/util/List;)Lrx/functions/d;
    .locals 1

    .prologue
    .line 30
    invoke-static {p0}, Lcom/twitter/android/moments/ui/fullscreen/ce;->b(Ljava/util/List;)Lrx/functions/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/ce;)Lxv;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ce;->b:Lxv;

    return-object v0
.end method

.method private static b(Ljava/util/List;)Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentModule;",
            ">;)",
            "Lrx/functions/d",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/viewmodels/a;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentModule;",
            "Lcom/twitter/model/moments/viewmodels/a;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 101
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/ce$2;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/fullscreen/ce$2;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method static synthetic c()Lrx/functions/h;
    .locals 1

    .prologue
    .line 30
    invoke-static {}, Lcom/twitter/android/moments/ui/fullscreen/ce;->d()Lrx/functions/h;

    move-result-object v0

    return-object v0
.end method

.method private static d()Lrx/functions/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/h",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/viewmodels/a;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 76
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/ce$1;

    invoke-direct {v0}, Lcom/twitter/android/moments/ui/fullscreen/ce$1;-><init>()V

    return-object v0
.end method

.method private e()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<",
            "Lcbi",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentModule;",
            ">;",
            "Lrx/c",
            "<",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentModule;",
            "Lcom/twitter/model/moments/viewmodels/a;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 122
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/ce$3;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/fullscreen/ce$3;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ce;)V

    return-object v0
.end method


# virtual methods
.method public a()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentModule;",
            "Lcom/twitter/model/moments/viewmodels/a;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ce;->c:Lrx/subjects/d;

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ce;->d:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 69
    return-void
.end method
