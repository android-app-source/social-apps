.class Lcom/twitter/android/moments/ui/fullscreen/de$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/fullscreen/de;->b(Lcom/twitter/model/core/Tweet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/model/core/Tweet;

.field final synthetic b:Lcom/twitter/android/moments/ui/fullscreen/de;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/de;Lcom/twitter/model/core/Tweet;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/de$2;->b:Lcom/twitter/android/moments/ui/fullscreen/de;

    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/de$2;->a:Lcom/twitter/model/core/Tweet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/de$2;->b:Lcom/twitter/android/moments/ui/fullscreen/de;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/de;->b(Lcom/twitter/android/moments/ui/fullscreen/de;)Lcom/twitter/android/widget/ToggleImageButton;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/widget/ToggleImageButton;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 96
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/de$2;->b:Lcom/twitter/android/moments/ui/fullscreen/de;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/de;->c(Lcom/twitter/android/moments/ui/fullscreen/de;)Lbpl;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/de$2;->b:Lcom/twitter/android/moments/ui/fullscreen/de;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/de;->d(Lcom/twitter/android/moments/ui/fullscreen/de;)Lcom/twitter/android/moments/ui/animation/e;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/de$2;->b:Lcom/twitter/android/moments/ui/fullscreen/de;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/fullscreen/de;->c(Lcom/twitter/android/moments/ui/fullscreen/de;)Lbpl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/animation/e;->a(Lbpl;)V

    .line 98
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/de$2;->b:Lcom/twitter/android/moments/ui/fullscreen/de;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/de;->c(Lcom/twitter/android/moments/ui/fullscreen/de;)Lbpl;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/de$2;->b:Lcom/twitter/android/moments/ui/fullscreen/de;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/fullscreen/de;->b(Lcom/twitter/android/moments/ui/fullscreen/de;)Lcom/twitter/android/widget/ToggleImageButton;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbpl;->b(Landroid/view/View;)V

    .line 105
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/de$2;->b:Lcom/twitter/android/moments/ui/fullscreen/de;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/de;->a(Lcom/twitter/android/moments/ui/fullscreen/de;)Lcom/twitter/android/moments/ui/fullscreen/bl;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/TweetActionType;->b:Lcom/twitter/model/core/TweetActionType;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/de$2;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/bl;->a(Lcom/twitter/model/core/TweetActionType;Lcom/twitter/model/core/Tweet;)V

    .line 106
    return-void

    .line 101
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/de$2;->b:Lcom/twitter/android/moments/ui/fullscreen/de;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/de;->c(Lcom/twitter/android/moments/ui/fullscreen/de;)Lbpl;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/de$2;->b:Lcom/twitter/android/moments/ui/fullscreen/de;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/de;->c(Lcom/twitter/android/moments/ui/fullscreen/de;)Lbpl;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/de$2;->b:Lcom/twitter/android/moments/ui/fullscreen/de;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/fullscreen/de;->b(Lcom/twitter/android/moments/ui/fullscreen/de;)Lcom/twitter/android/widget/ToggleImageButton;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbpl;->c(Landroid/view/View;)V

    goto :goto_0
.end method
