.class public Lcom/twitter/android/moments/ui/fullscreen/am;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/media/request/b$b;
.implements Lcom/twitter/moments/core/ui/widget/sectionpager/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/media/request/b$b",
        "<",
        "Lcom/twitter/media/request/ImageResponse;",
        ">;",
        "Lcom/twitter/moments/core/ui/widget/sectionpager/a;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/model/moments/viewmodels/MomentPage;

.field private final b:Lcom/twitter/android/moments/ui/fullscreen/at;

.field private final c:Lcom/twitter/android/moments/ui/fullscreen/cd;

.field private final d:Lcom/twitter/util/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/twitter/util/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/twitter/android/moments/ui/fullscreen/ad;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/moments/ui/fullscreen/ad",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Labn;

.field private h:Z

.field private i:Z


# direct methods
.method constructor <init>(Lcom/twitter/model/moments/viewmodels/MomentPage;Labn;Labo;Lcom/twitter/android/moments/ui/fullscreen/at;Lcom/twitter/android/moments/ui/fullscreen/cd;Lcom/twitter/util/p;Lcom/twitter/android/moments/ui/fullscreen/ad;Lzn;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            "Labn;",
            "Labo;",
            "Lcom/twitter/android/moments/ui/fullscreen/at;",
            "Lcom/twitter/android/moments/ui/fullscreen/cd;",
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;",
            "Lcom/twitter/android/moments/ui/fullscreen/ad",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;",
            ">;",
            "Lzn;",
            ")V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->a:Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 48
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->g:Labn;

    .line 49
    iput-object p5, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->c:Lcom/twitter/android/moments/ui/fullscreen/cd;

    .line 50
    iput-object p6, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->d:Lcom/twitter/util/p;

    .line 51
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/au;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->g:Labn;

    invoke-direct {v0, v1, p5, p8}, Lcom/twitter/android/moments/ui/fullscreen/au;-><init>(Laci;Lcom/twitter/android/moments/ui/fullscreen/cd;Lzn;)V

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->e:Lcom/twitter/util/q;

    .line 53
    iput-object p4, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->b:Lcom/twitter/android/moments/ui/fullscreen/at;

    .line 54
    iput-object p7, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->f:Lcom/twitter/android/moments/ui/fullscreen/ad;

    .line 56
    invoke-virtual {p3, p0}, Labo;->a(Lcom/twitter/media/request/b$b;)V

    .line 57
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->a:Lcom/twitter/model/moments/viewmodels/MomentPage;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->a:Lcom/twitter/model/moments/viewmodels/MomentPage;

    invoke-static {v1}, Lcom/twitter/model/moments/viewmodels/h;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Labo;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/core/Tweet;)V

    .line 59
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->f:Lcom/twitter/android/moments/ui/fullscreen/ad;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->a:Lcom/twitter/model/moments/viewmodels/MomentPage;

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->j()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->a:Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;

    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->a()Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 60
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/android/moments/viewmodels/k;Lcom/twitter/util/p;Lcom/twitter/android/moments/ui/fullscreen/ad;Lzn;Lcom/twitter/android/moments/ui/fullscreen/cu;)Lcom/twitter/android/moments/ui/fullscreen/am;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            "Lcom/twitter/android/moments/viewmodels/k;",
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;",
            "Lcom/twitter/android/moments/ui/fullscreen/ad",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;",
            ">;",
            "Lzn;",
            "Lcom/twitter/android/moments/ui/fullscreen/cu;",
            ")",
            "Lcom/twitter/android/moments/ui/fullscreen/am;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 68
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 69
    invoke-interface {p2}, Lcom/twitter/android/moments/viewmodels/k;->c()Lcom/twitter/model/moments/e;

    move-result-object v1

    .line 68
    invoke-static {v0, v1, p6, v3}, Labn;->a(Landroid/view/LayoutInflater;Lcom/twitter/model/moments/e;Lcom/twitter/android/moments/ui/fullscreen/cu;Z)Labn;

    move-result-object v2

    .line 71
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f100009

    invoke-virtual {v0, v1, v3, v3}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    .line 73
    new-instance v5, Lcom/twitter/android/moments/ui/fullscreen/cd;

    invoke-virtual {v2}, Labn;->b()Lcom/twitter/media/ui/image/MediaImageView;

    move-result-object v1

    invoke-direct {v5, v1, v0, v3}, Lcom/twitter/android/moments/ui/fullscreen/cd;-><init>(Landroid/view/View;FI)V

    .line 76
    new-instance v4, Lcom/twitter/android/moments/ui/fullscreen/at;

    .line 77
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 78
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->j()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v2, v0, v1, p4}, Lcom/twitter/android/moments/ui/fullscreen/at;-><init>(Labp;Landroid/content/res/Resources;Ljava/lang/String;Lcom/twitter/android/moments/ui/fullscreen/ad;)V

    .line 79
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/am;

    .line 80
    invoke-static {v2, p2}, Labo;->a(Labn;Lcom/twitter/android/moments/viewmodels/k;)Labo;

    move-result-object v3

    move-object v1, p1

    move-object v6, p3

    move-object v7, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/moments/ui/fullscreen/am;-><init>(Lcom/twitter/model/moments/viewmodels/MomentPage;Labn;Labo;Lcom/twitter/android/moments/ui/fullscreen/at;Lcom/twitter/android/moments/ui/fullscreen/cd;Lcom/twitter/util/p;Lcom/twitter/android/moments/ui/fullscreen/ad;Lzn;)V

    .line 79
    return-object v0
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->g:Labn;

    invoke-virtual {v0}, Labn;->a()Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method public a(F)V
    .locals 3

    .prologue
    .line 113
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->c:Lcom/twitter/android/moments/ui/fullscreen/cd;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/cd;->a(F)V

    .line 114
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->i:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3a83126f    # 0.001f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->f:Lcom/twitter/android/moments/ui/fullscreen/ad;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->a:Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 116
    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->j()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->d:Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;

    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->a()Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;

    move-result-object v2

    .line 115
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 118
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/media/request/ImageResponse;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 122
    invoke-virtual {p1}, Lcom/twitter/media/request/ImageResponse;->e()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->f:Lcom/twitter/android/moments/ui/fullscreen/ad;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->a:Lcom/twitter/model/moments/viewmodels/MomentPage;

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->j()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->f:Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;

    .line 124
    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->a()Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;

    move-result-object v2

    .line 123
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 125
    iput-boolean v3, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->i:Z

    .line 131
    :goto_0
    return-void

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->f:Lcom/twitter/android/moments/ui/fullscreen/ad;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->a:Lcom/twitter/model/moments/viewmodels/MomentPage;

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->j()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->g:Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;

    .line 128
    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->a()Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;

    move-result-object v2

    .line 127
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 129
    iput-boolean v3, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->h:Z

    goto :goto_0
.end method

.method public bridge synthetic a(Lcom/twitter/media/request/ResourceResponse;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lcom/twitter/media/request/ImageResponse;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/am;->a(Lcom/twitter/media/request/ImageResponse;)V

    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->h:Z

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->f:Lcom/twitter/android/moments/ui/fullscreen/ad;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->a:Lcom/twitter/model/moments/viewmodels/MomentPage;

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->j()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->c:Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;

    .line 95
    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->a()Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;

    move-result-object v2

    .line 94
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 96
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->d:Lcom/twitter/util/p;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->e:Lcom/twitter/util/q;

    invoke-virtual {v0, v1}, Lcom/twitter/util/p;->a(Lcom/twitter/util/q;)Z

    .line 98
    :cond_0
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->d:Lcom/twitter/util/p;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->e:Lcom/twitter/util/q;

    invoke-virtual {v0, v1}, Lcom/twitter/util/p;->b(Lcom/twitter/util/q;)Z

    .line 103
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->d:Lcom/twitter/util/p;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->e:Lcom/twitter/util/q;

    invoke-virtual {v0, v1}, Lcom/twitter/util/p;->b(Lcom/twitter/util/q;)Z

    .line 108
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/am;->b:Lcom/twitter/android/moments/ui/fullscreen/at;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/at;->g()V

    .line 109
    return-void
.end method
