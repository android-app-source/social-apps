.class public Lcom/twitter/android/moments/ui/fullscreen/bg;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/android/moments/ui/fullscreen/bh;

.field private final c:Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/twitter/android/moments/ui/fullscreen/bh;Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/bg;->a:Landroid/content/Context;

    .line 33
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/bg;->b:Lcom/twitter/android/moments/ui/fullscreen/bh;

    .line 34
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/bg;->c:Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;

    .line 35
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/Moment;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 48
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bg;->c:Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;

    invoke-virtual {v0}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bg;->c:Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;

    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;->a(Lcom/twitter/model/moments/Moment;)V

    .line 59
    :goto_0
    return-void

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bg;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0be2

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string/jumbo v4, "\u26a1"

    aput-object v4, v2, v3

    iget-object v3, p1, Lcom/twitter/model/moments/Moment;->c:Ljava/lang/String;

    aput-object v3, v2, v5

    const/4 v3, 0x2

    iget-object v4, p1, Lcom/twitter/model/moments/Moment;->n:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 54
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bg;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/bg;->a:Landroid/content/Context;

    new-instance v3, Lcom/twitter/app/dm/h$a;

    invoke-direct {v3}, Lcom/twitter/app/dm/h$a;-><init>()V

    .line 55
    invoke-virtual {v3, v0}, Lcom/twitter/app/dm/h$a;->a(Ljava/lang/String;)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/h$a;

    .line 56
    invoke-virtual {v0, v5}, Lcom/twitter/app/dm/h$a;->i(Z)Lcom/twitter/app/dm/h$a;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Lcom/twitter/app/dm/h$a;->e()Lcom/twitter/app/dm/h;

    move-result-object v0

    .line 54
    invoke-static {v2, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/h;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public b(Lcom/twitter/model/moments/Moment;)V
    .locals 4

    .prologue
    .line 66
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bg;->a:Landroid/content/Context;

    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/bg;->b:Lcom/twitter/android/moments/ui/fullscreen/bh;

    .line 67
    invoke-virtual {v2, p1}, Lcom/twitter/android/moments/ui/fullscreen/bh;->a(Lcom/twitter/model/moments/Moment;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/composer/a;->a(Ljava/lang/String;I)Lcom/twitter/android/composer/a;

    move-result-object v0

    const/4 v2, 0x1

    .line 68
    invoke-virtual {v0, v2}, Lcom/twitter/android/composer/a;->d(Z)Lcom/twitter/app/common/base/h;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/composer/a;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/bg;->a:Landroid/content/Context;

    .line 69
    invoke-virtual {v0, v2}, Lcom/twitter/android/composer/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 66
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 70
    return-void
.end method

.method public c(Lcom/twitter/model/moments/Moment;)V
    .locals 6

    .prologue
    .line 38
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/bk;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bg;->a:Landroid/content/Context;

    .line 39
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/bk;-><init>(Landroid/content/res/Resources;)V

    .line 40
    new-instance v1, Lcom/twitter/android/dialog/e;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/bg;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/twitter/android/dialog/e;-><init>(Landroid/content/Context;)V

    .line 41
    invoke-virtual {v0, p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/bk;->a(Lcom/twitter/android/moments/ui/fullscreen/bg;Lcom/twitter/model/moments/Moment;)Lcom/twitter/android/dialog/d;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/twitter/android/dialog/d;

    const/4 v4, 0x0

    .line 42
    invoke-virtual {v0, p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/bk;->b(Lcom/twitter/android/moments/ui/fullscreen/bg;Lcom/twitter/model/moments/Moment;)Lcom/twitter/android/dialog/d;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 43
    invoke-virtual {v0, p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/bk;->c(Lcom/twitter/android/moments/ui/fullscreen/bg;Lcom/twitter/model/moments/Moment;)Lcom/twitter/android/dialog/d;

    move-result-object v0

    aput-object v0, v3, v4

    .line 40
    invoke-static {v2, v3}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/dialog/e;->a(Ljava/util/List;)Landroid/app/Dialog;

    .line 45
    return-void
.end method

.method public d(Lcom/twitter/model/moments/Moment;)V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bg;->a:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Lcom/twitter/model/moments/Moment;Z)V

    .line 63
    return-void
.end method
