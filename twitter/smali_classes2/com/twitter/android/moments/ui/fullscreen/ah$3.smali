.class Lcom/twitter/android/moments/ui/fullscreen/ah$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/fullscreen/ah;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/ui/widget/touchintercept/TouchInterceptingFrameLayout;Lcom/twitter/android/ck;Lcom/twitter/library/client/v;JLcom/twitter/library/provider/t;Lcom/twitter/app/common/util/j;JLcom/twitter/android/moments/ui/fullscreen/ad;Lcom/twitter/android/moments/ui/fullscreen/bz;Lcom/twitter/android/moments/data/i;Landroid/os/Bundle;Lcom/twitter/ui/anim/p;Lcom/twitter/android/moments/data/am;Lxm;Labg;Lcom/twitter/android/moments/ui/b;Lcom/twitter/android/moments/ui/fullscreen/r;Lcom/twitter/android/moments/ui/fullscreen/t;Lyf;Lcom/twitter/android/moments/ui/fullscreen/co;Lcom/twitter/android/moments/ui/fullscreen/x;Lcif;Lcom/twitter/android/av/k;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/q",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/ToggleImageButton;

.field final synthetic b:Lcom/twitter/android/moments/ui/fullscreen/ah;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/ah;Lcom/twitter/android/widget/ToggleImageButton;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$3;->b:Lcom/twitter/android/moments/ui/fullscreen/ah;

    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$3;->a:Lcom/twitter/android/widget/ToggleImageButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEvent(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 219
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$3;->a:Lcom/twitter/android/widget/ToggleImageButton;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/ToggleImageButton;->setToggledOn(Z)V

    .line 220
    return-void

    .line 219
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 216
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ah$3;->onEvent(Ljava/lang/Boolean;)V

    return-void
.end method
