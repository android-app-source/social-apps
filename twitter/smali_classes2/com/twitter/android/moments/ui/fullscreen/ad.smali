.class public Lcom/twitter/android/moments/ui/fullscreen/ad;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/util/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/util/collection/Pair",
            "<TK;TT;>;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/util/q",
            "<TT;>;",
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/util/collection/Pair",
            "<TK;TT;>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Lcom/twitter/util/p;

    invoke-direct {v0}, Lcom/twitter/util/p;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ad;->a:Lcom/twitter/util/p;

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ad;->b:Ljava/util/Map;

    .line 29
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/util/q;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/util/collection/Pair",
            "<TK;TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ad;->a:Lcom/twitter/util/p;

    invoke-virtual {v0, p1}, Lcom/twitter/util/p;->a(Lcom/twitter/util/q;)Z

    .line 36
    return-void
.end method

.method public a(Ljava/lang/Object;Lcom/twitter/util/q;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Lcom/twitter/util/q",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ad;->b:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/ad$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/android/moments/ui/fullscreen/ad$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ad;Ljava/lang/Object;Lcom/twitter/util/q;)V

    .line 53
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ad;->a:Lcom/twitter/util/p;

    invoke-virtual {v1, v0}, Lcom/twitter/util/p;->a(Lcom/twitter/util/q;)Z

    .line 54
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ad;->b:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TT;)V"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ad;->a:Lcom/twitter/util/p;

    invoke-static {p1, p2}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/p;->a(Ljava/lang/Object;)V

    .line 68
    return-void
.end method

.method public b(Lcom/twitter/util/q;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/q",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ad;->a:Lcom/twitter/util/p;

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ad;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/q;

    invoke-virtual {v1, v0}, Lcom/twitter/util/p;->b(Lcom/twitter/util/q;)Z

    .line 60
    return-void
.end method

.method public c(Lcom/twitter/util/q;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/util/collection/Pair",
            "<TK;TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ad;->a:Lcom/twitter/util/p;

    invoke-virtual {v0, p1}, Lcom/twitter/util/p;->b(Lcom/twitter/util/q;)Z

    .line 64
    return-void
.end method
