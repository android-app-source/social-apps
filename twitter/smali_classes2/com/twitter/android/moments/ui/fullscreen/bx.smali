.class public Lcom/twitter/android/moments/ui/fullscreen/bx;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/moments/core/ui/widget/sectionpager/d;


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/fullscreen/cf;

.field private final b:Landroid/view/ViewGroup;

.field private final c:Lcom/twitter/android/moments/ui/fullscreen/ax;


# direct methods
.method constructor <init>(Labl;Lcom/twitter/android/moments/ui/fullscreen/cf;Lcom/twitter/android/moments/ui/fullscreen/bg;Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/moments/Moment;Lcom/twitter/android/moments/ui/fullscreen/ax;)V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/bx;->a:Lcom/twitter/android/moments/ui/fullscreen/cf;

    .line 60
    invoke-virtual {p1}, Labl;->e()Labk;

    move-result-object v0

    invoke-virtual {v0}, Labk;->a()Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bx;->b:Landroid/view/ViewGroup;

    .line 61
    iput-object p6, p0, Lcom/twitter/android/moments/ui/fullscreen/bx;->c:Lcom/twitter/android/moments/ui/fullscreen/ax;

    .line 62
    invoke-virtual {p1}, Labl;->e()Labk;

    move-result-object v0

    invoke-virtual {v0}, Labk;->h()Landroid/view/ViewGroup;

    move-result-object v0

    .line 63
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bx;->a:Lcom/twitter/android/moments/ui/fullscreen/cf;

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/fullscreen/cf;->a(Landroid/view/ViewGroup;)V

    .line 64
    invoke-virtual {p1}, Labl;->e()Labk;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/bx$1;

    invoke-direct {v1, p0, p3, p5}, Lcom/twitter/android/moments/ui/fullscreen/bx$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/bx;Lcom/twitter/android/moments/ui/fullscreen/bg;Lcom/twitter/model/moments/Moment;)V

    invoke-virtual {v0, v1}, Labk;->c(Landroid/view/View$OnClickListener;)V

    .line 70
    invoke-static {p4}, Lcom/twitter/model/moments/viewmodels/h;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-virtual {p1, p4, v0}, Labl;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/core/Tweet;)V

    .line 71
    return-void
.end method

.method static a(Landroid/view/LayoutInflater;Lcom/twitter/android/moments/ui/fullscreen/cf;Lcom/twitter/android/moments/ui/fullscreen/bg;Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/moments/Moment;Landroid/app/Activity;Lrx/g;Lcom/twitter/android/moments/data/n;Lcom/twitter/android/moments/ui/fullscreen/co;)Lcom/twitter/android/moments/ui/fullscreen/bx;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/LayoutInflater;",
            "Lcom/twitter/android/moments/ui/fullscreen/cf;",
            "Lcom/twitter/android/moments/ui/fullscreen/bg;",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            "Lcom/twitter/model/moments/Moment;",
            "Landroid/app/Activity;",
            "Lrx/g",
            "<",
            "Lcom/twitter/model/moments/p;",
            ">;",
            "Lcom/twitter/android/moments/data/n;",
            "Lcom/twitter/android/moments/ui/fullscreen/co;",
            ")",
            "Lcom/twitter/android/moments/ui/fullscreen/bx;"
        }
    .end annotation

    .prologue
    .line 42
    .line 43
    invoke-static {p0, p3}, Labl;->a(Landroid/view/LayoutInflater;Lcom/twitter/model/moments/viewmodels/MomentPage;)Labl;

    move-result-object v7

    .line 44
    iget-wide v0, p4, Lcom/twitter/model/moments/Moment;->b:J

    invoke-static {v0, v1}, Lzr;->a(J)Lzr;

    move-result-object v5

    .line 46
    invoke-virtual {v7}, Labl;->e()Labk;

    move-result-object v1

    move-object v0, p5

    move-object v2, p7

    move-object/from16 v3, p8

    move-object v4, p6

    .line 45
    invoke-static/range {v0 .. v5}, Lcom/twitter/android/moments/ui/fullscreen/ax;->a(Landroid/app/Activity;Labk;Lcom/twitter/android/moments/data/n;Lcom/twitter/android/moments/ui/fullscreen/co;Lrx/g;Lzr;)Lcom/twitter/android/moments/ui/fullscreen/ax;

    move-result-object v6

    .line 48
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/bx;

    move-object v1, v7

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/moments/ui/fullscreen/bx;-><init>(Labl;Lcom/twitter/android/moments/ui/fullscreen/cf;Lcom/twitter/android/moments/ui/fullscreen/bg;Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/moments/Moment;Lcom/twitter/android/moments/ui/fullscreen/ax;)V

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bx;->a:Lcom/twitter/android/moments/ui/fullscreen/cf;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/cf;->c()V

    .line 86
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bx;->c:Lcom/twitter/android/moments/ui/fullscreen/ax;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ax;->a()V

    .line 87
    return-void
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bx;->b:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bx;->a:Lcom/twitter/android/moments/ui/fullscreen/cf;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/cf;->a()V

    .line 76
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bx;->a:Lcom/twitter/android/moments/ui/fullscreen/cf;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/cf;->b()V

    .line 81
    return-void
.end method
