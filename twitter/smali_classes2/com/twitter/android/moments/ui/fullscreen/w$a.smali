.class Lcom/twitter/android/moments/ui/fullscreen/w$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/object/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/fullscreen/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/object/d",
        "<",
        "Lcom/twitter/moments/core/ui/widget/sectionpager/d;",
        "Lcom/twitter/moments/core/ui/widget/sectionpager/a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/android/moments/ui/fullscreen/w;

.field private final c:Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/android/moments/ui/fullscreen/w;Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;)V
    .locals 0

    .prologue
    .line 238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 239
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/w$a;->a:Landroid/content/Context;

    .line 240
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/w$a;->b:Lcom/twitter/android/moments/ui/fullscreen/w;

    .line 241
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/w$a;->c:Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;

    .line 242
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/moments/core/ui/widget/sectionpager/d;)Lcom/twitter/moments/core/ui/widget/sectionpager/a;
    .locals 3

    .prologue
    .line 247
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/w$a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/w$a;->b:Lcom/twitter/android/moments/ui/fullscreen/w;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/w$a;->c:Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;

    invoke-static {v0, v1, v2, p1}, Lcom/twitter/android/moments/ui/fullscreen/ar;->a(Landroid/content/Context;Lcom/twitter/android/moments/ui/fullscreen/w;Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;Lcom/twitter/moments/core/ui/widget/sectionpager/d;)Lcom/twitter/android/moments/ui/fullscreen/ar;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 230
    check-cast p1, Lcom/twitter/moments/core/ui/widget/sectionpager/d;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/w$a;->a(Lcom/twitter/moments/core/ui/widget/sectionpager/d;)Lcom/twitter/moments/core/ui/widget/sectionpager/a;

    move-result-object v0

    return-object v0
.end method
