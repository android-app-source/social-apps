.class public Lcom/twitter/android/moments/ui/fullscreen/bb;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Lcom/twitter/media/ui/image/MediaImageView;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/bb;->a:Landroid/content/res/Resources;

    .line 44
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/bb;->b:Landroid/view/View;

    .line 45
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bb;->b:Landroid/view/View;

    const v1, 0x7f1304f2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bb;->c:Landroid/widget/TextView;

    .line 46
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bb;->b:Landroid/view/View;

    const v1, 0x7f1304e5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bb;->d:Lcom/twitter/media/ui/image/MediaImageView;

    .line 47
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bb;->d:Lcom/twitter/media/ui/image/MediaImageView;

    sget-object v1, Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;->b:Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setScaleType(Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;)V

    .line 48
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/bb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bb;->b:Landroid/view/View;

    return-object v0
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/fullscreen/bb;
    .locals 3

    .prologue
    .line 32
    const v0, 0x7f0401e0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 33
    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/bb;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/twitter/android/moments/ui/fullscreen/bb;-><init>(Landroid/content/res/Resources;Landroid/view/View;)V

    return-object v1
.end method

.method public static a(Landroid/view/View;)Lcom/twitter/android/moments/ui/fullscreen/bb;
    .locals 2

    .prologue
    .line 38
    const v0, 0x7f0206ef

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 39
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/bb;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/twitter/android/moments/ui/fullscreen/bb;-><init>(Landroid/content/res/Resources;Landroid/view/View;)V

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bb;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bb;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bb;->b:Landroid/view/View;

    return-object v0
.end method

.method public b()Lcom/twitter/media/ui/image/MediaImageView;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bb;->d:Lcom/twitter/media/ui/image/MediaImageView;

    return-object v0
.end method

.method public c()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcom/twitter/media/request/ImageResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bb;->d:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->i()Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 4

    .prologue
    .line 75
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bb;->a:Landroid/content/res/Resources;

    const v1, 0x7f0f000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 76
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bb;->a:Landroid/content/res/Resources;

    const v2, 0x7f0e00f4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 77
    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/bb;->b:Landroid/view/View;

    invoke-static {v2}, Landroid/support/v4/view/ViewCompat;->animate(Landroid/view/View;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v2

    neg-float v1, v1

    .line 78
    invoke-virtual {v2, v1}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->translationX(F)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v1

    new-instance v2, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v2}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    .line 79
    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setInterpolator(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v1

    int-to-long v2, v0

    .line 80
    invoke-virtual {v1, v2, v3}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setDuration(J)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/bb$1;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/moments/ui/fullscreen/bb$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/bb;I)V

    .line 81
    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->withEndAction(Ljava/lang/Runnable;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->start()V

    .line 92
    return-void
.end method
