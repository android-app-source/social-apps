.class Lcom/twitter/android/moments/ui/fullscreen/ab$5;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/fullscreen/ab;->g()Lcqw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Lcom/twitter/model/moments/Moment;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/ui/fullscreen/ab;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/ab;)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/ab$5;->a:Lcom/twitter/android/moments/ui/fullscreen/ab;

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/Moment;)V
    .locals 4

    .prologue
    .line 203
    invoke-static {}, Lbsd;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p1, Lcom/twitter/model/moments/Moment;->u:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ab$5;->a:Lcom/twitter/android/moments/ui/fullscreen/ab;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/ab;->d(Lcom/twitter/android/moments/ui/fullscreen/ab;)Labh;

    move-result-object v0

    invoke-virtual {v0}, Labh;->b()V

    .line 205
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ab$5;->a:Lcom/twitter/android/moments/ui/fullscreen/ab;

    invoke-static {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ab;->a(Lcom/twitter/android/moments/ui/fullscreen/ab;Lcom/twitter/model/moments/Moment;)V

    .line 209
    :goto_0
    return-void

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ab$5;->a:Lcom/twitter/android/moments/ui/fullscreen/ab;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/ab;->d(Lcom/twitter/android/moments/ui/fullscreen/ab;)Labh;

    move-result-object v0

    invoke-virtual {v0}, Labh;->c()V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 200
    check-cast p1, Lcom/twitter/model/moments/Moment;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ab$5;->a(Lcom/twitter/model/moments/Moment;)V

    return-void
.end method
