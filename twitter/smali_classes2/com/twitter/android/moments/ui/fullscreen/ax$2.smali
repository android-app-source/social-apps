.class Lcom/twitter/android/moments/ui/fullscreen/ax$2;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/fullscreen/ax;->c()Lcqw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Lcom/twitter/model/moments/Moment;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/ui/fullscreen/ax;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/ax;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/ax$2;->a:Lcom/twitter/android/moments/ui/fullscreen/ax;

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/Moment;)V
    .locals 4

    .prologue
    .line 94
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ax$2;->a:Lcom/twitter/android/moments/ui/fullscreen/ax;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/ax;->a(Lcom/twitter/android/moments/ui/fullscreen/ax;)Labk;

    move-result-object v0

    iget-boolean v1, p1, Lcom/twitter/model/moments/Moment;->t:Z

    invoke-virtual {v0, v1}, Labk;->a(Z)V

    .line 95
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ax$2;->a:Lcom/twitter/android/moments/ui/fullscreen/ax;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/ax;->a(Lcom/twitter/android/moments/ui/fullscreen/ax;)Labk;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ax$2;->a:Lcom/twitter/android/moments/ui/fullscreen/ax;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/ax;->b(Lcom/twitter/android/moments/ui/fullscreen/ax;)Lcom/twitter/android/moments/ui/fullscreen/co;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/fullscreen/co;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Labk;->b(Z)V

    .line 96
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ax$2;->a:Lcom/twitter/android/moments/ui/fullscreen/ax;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/ax;->a(Lcom/twitter/android/moments/ui/fullscreen/ax;)Labk;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/ax$2$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ax$2$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ax$2;Lcom/twitter/model/moments/Moment;)V

    invoke-virtual {v0, v1}, Labk;->a(Landroid/view/View$OnClickListener;)V

    .line 109
    invoke-static {}, Lbsd;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 110
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ax$2;->a:Lcom/twitter/android/moments/ui/fullscreen/ax;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/ax;->a(Lcom/twitter/android/moments/ui/fullscreen/ax;)Labk;

    move-result-object v0

    invoke-virtual {v0}, Labk;->f()V

    .line 117
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ax$2;->a:Lcom/twitter/android/moments/ui/fullscreen/ax;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/ax;->a(Lcom/twitter/android/moments/ui/fullscreen/ax;)Labk;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/ax$2$2;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ax$2$2;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ax$2;Lcom/twitter/model/moments/Moment;)V

    invoke-virtual {v0, v1}, Labk;->b(Landroid/view/View$OnClickListener;)V

    .line 124
    return-void

    .line 95
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 111
    :cond_1
    iget-wide v0, p1, Lcom/twitter/model/moments/Moment;->u:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 112
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ax$2;->a:Lcom/twitter/android/moments/ui/fullscreen/ax;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/ax;->a(Lcom/twitter/android/moments/ui/fullscreen/ax;)Labk;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ax$2;->a:Lcom/twitter/android/moments/ui/fullscreen/ax;

    iget-wide v2, p1, Lcom/twitter/model/moments/Moment;->u:J

    invoke-static {v1, v2, v3}, Lcom/twitter/android/moments/ui/fullscreen/ax;->a(Lcom/twitter/android/moments/ui/fullscreen/ax;J)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Labk;->a(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ax$2;->a:Lcom/twitter/android/moments/ui/fullscreen/ax;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/ax;->a(Lcom/twitter/android/moments/ui/fullscreen/ax;)Labk;

    move-result-object v0

    invoke-virtual {v0}, Labk;->d()V

    goto :goto_1

    .line 115
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ax$2;->a:Lcom/twitter/android/moments/ui/fullscreen/ax;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/ax;->a(Lcom/twitter/android/moments/ui/fullscreen/ax;)Labk;

    move-result-object v0

    invoke-virtual {v0}, Labk;->e()V

    goto :goto_1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 91
    check-cast p1, Lcom/twitter/model/moments/Moment;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ax$2;->a(Lcom/twitter/model/moments/Moment;)V

    return-void
.end method
