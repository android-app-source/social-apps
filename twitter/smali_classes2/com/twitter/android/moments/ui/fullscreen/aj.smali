.class public Lcom/twitter/android/moments/ui/fullscreen/aj;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/q",
        "<",
        "Lcom/twitter/android/moments/ui/fullscreen/Event;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/moments/core/ui/widget/sectionpager/e;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/util/o;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/android/moments/ui/fullscreen/bt;


# direct methods
.method public constructor <init>(Lcom/twitter/moments/core/ui/widget/sectionpager/e;Ljava/util/Set;Lcom/twitter/android/moments/ui/fullscreen/bt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/moments/core/ui/widget/sectionpager/e;",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/util/o;",
            ">;",
            "Lcom/twitter/android/moments/ui/fullscreen/bt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/aj;->a:Lcom/twitter/moments/core/ui/widget/sectionpager/e;

    .line 23
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/aj;->c:Lcom/twitter/android/moments/ui/fullscreen/bt;

    .line 24
    invoke-static {p2}, Lcom/twitter/util/collection/o;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aj;->b:Ljava/util/Set;

    .line 25
    return-void
.end method


# virtual methods
.method public onEvent(Lcom/twitter/android/moments/ui/fullscreen/Event;)V
    .locals 2

    .prologue
    .line 29
    if-nez p1, :cond_1

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 32
    :cond_1
    sget-object v0, Lcom/twitter/android/moments/ui/fullscreen/aj$1;->a:[I

    invoke-virtual {p1}, Lcom/twitter/android/moments/ui/fullscreen/Event;->a()Lcom/twitter/android/moments/ui/fullscreen/Event$EventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/fullscreen/Event$EventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 35
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aj;->c:Lcom/twitter/android/moments/ui/fullscreen/bt;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/bt;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aj;->a:Lcom/twitter/moments/core/ui/widget/sectionpager/e;

    invoke-virtual {v0}, Lcom/twitter/moments/core/ui/widget/sectionpager/e;->a()V

    .line 37
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aj;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/o;

    .line 38
    invoke-interface {v0}, Lcom/twitter/util/o;->a()V

    goto :goto_1

    .line 44
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aj;->a:Lcom/twitter/moments/core/ui/widget/sectionpager/e;

    invoke-virtual {v0}, Lcom/twitter/moments/core/ui/widget/sectionpager/e;->b()V

    .line 45
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aj;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/o;

    .line 46
    invoke-interface {v0}, Lcom/twitter/util/o;->b()V

    goto :goto_2

    .line 32
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 13
    check-cast p1, Lcom/twitter/android/moments/ui/fullscreen/Event;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/aj;->onEvent(Lcom/twitter/android/moments/ui/fullscreen/Event;)V

    return-void
.end method
