.class public Lcom/twitter/android/moments/ui/fullscreen/ch;
.super Lcom/twitter/android/moments/ui/fullscreen/cg;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/fullscreen/cj;


# instance fields
.field private final b:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;Lcom/twitter/android/moments/ui/fullscreen/cv;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/moments/ui/fullscreen/cg;-><init>(Landroid/content/Context;Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;Lcom/twitter/android/moments/ui/fullscreen/cv;)V

    .line 27
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ch;->b:Lrx/subjects/PublishSubject;

    .line 28
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/av/playback/aa;)V
    .locals 2

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/fullscreen/ch;->f()Lcom/twitter/moments/core/ui/widget/sectionpager/d;

    move-result-object v0

    .line 49
    instance-of v1, v0, Lcom/twitter/android/moments/ui/fullscreen/cj;

    if-eqz v1, :cond_0

    .line 50
    check-cast v0, Lcom/twitter/android/moments/ui/fullscreen/cj;

    invoke-interface {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/cj;->a(Lcom/twitter/library/av/playback/aa;)V

    .line 52
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/model/av/AVMedia;)V
    .locals 2

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/fullscreen/ch;->f()Lcom/twitter/moments/core/ui/widget/sectionpager/d;

    move-result-object v0

    .line 41
    instance-of v1, v0, Lcom/twitter/android/moments/ui/fullscreen/cj;

    if-eqz v1, :cond_0

    .line 42
    check-cast v0, Lcom/twitter/android/moments/ui/fullscreen/cj;

    invoke-interface {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/cj;->a(Lcom/twitter/model/av/AVMedia;)V

    .line 44
    :cond_0
    return-void
.end method

.method public e()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ch;->b:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/fullscreen/ch;->f()Lcom/twitter/moments/core/ui/widget/sectionpager/d;

    move-result-object v0

    .line 33
    instance-of v1, v0, Lcom/twitter/android/moments/ui/fullscreen/cj;

    if-eqz v1, :cond_0

    .line 34
    check-cast v0, Lcom/twitter/android/moments/ui/fullscreen/cj;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/fullscreen/cj;->g()V

    .line 36
    :cond_0
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/fullscreen/ch;->f()Lcom/twitter/moments/core/ui/widget/sectionpager/d;

    move-result-object v0

    .line 57
    instance-of v1, v0, Lcom/twitter/android/moments/ui/fullscreen/cj;

    if-eqz v1, :cond_0

    .line 58
    check-cast v0, Lcom/twitter/android/moments/ui/fullscreen/cj;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/fullscreen/cj;->h()V

    .line 60
    :cond_0
    return-void
.end method

.method public onEvent(Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;)V
    .locals 2

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/cg;->onEvent(Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;)V

    .line 65
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/fullscreen/ch;->f()Lcom/twitter/moments/core/ui/widget/sectionpager/d;

    move-result-object v0

    .line 66
    instance-of v1, v0, Lcom/twitter/android/moments/ui/fullscreen/cj;

    if-eqz v1, :cond_0

    .line 67
    check-cast v0, Lcom/twitter/android/moments/ui/fullscreen/cj;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/fullscreen/cj;->e()Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ch;->b:Lrx/subjects/PublishSubject;

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/d;)Lrx/j;

    .line 69
    :cond_0
    return-void
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ch;->onEvent(Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;)V

    return-void
.end method
