.class public Lcom/twitter/android/moments/ui/fullscreen/cm;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/fullscreen/x;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/viewmodels/a;)Lcom/twitter/model/moments/viewmodels/a;
    .locals 4

    .prologue
    .line 17
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/a;->c()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 20
    :goto_0
    return-object p1

    :cond_0
    invoke-static {p1}, Lcom/twitter/model/moments/viewmodels/a$a;->a(Lcom/twitter/model/moments/viewmodels/a;)Lcom/twitter/model/moments/viewmodels/a$a;

    move-result-object v1

    new-instance v2, Lcom/twitter/model/moments/viewmodels/l$a;

    invoke-direct {v2}, Lcom/twitter/model/moments/viewmodels/l$a;-><init>()V

    new-instance v0, Lcom/twitter/model/moments/r$a;

    invoke-direct {v0}, Lcom/twitter/model/moments/r$a;-><init>()V

    const-string/jumbo v3, "preview_mode_placeholder_cover"

    .line 23
    invoke-virtual {v0, v3}, Lcom/twitter/model/moments/r$a;->a(Ljava/lang/String;)Lcom/twitter/model/moments/r$a;

    move-result-object v0

    sget-object v3, Lcom/twitter/model/moments/MomentPageDisplayMode;->a:Lcom/twitter/model/moments/MomentPageDisplayMode;

    .line 24
    invoke-virtual {v0, v3}, Lcom/twitter/model/moments/r$a;->a(Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/r$a;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lcom/twitter/model/moments/r$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/r;

    .line 22
    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/l$a;->a(Lcom/twitter/model/moments/r;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/l$a;

    sget-object v2, Lcom/twitter/model/moments/MomentPageDisplayMode;->a:Lcom/twitter/model/moments/MomentPageDisplayMode;

    .line 26
    invoke-virtual {v0, v2}, Lcom/twitter/model/moments/viewmodels/l$a;->a(Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/l$a;

    .line 27
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/a;->a()Lcom/twitter/model/moments/Moment;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/model/moments/viewmodels/l$a;->a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/l$a;

    .line 28
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/l$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 21
    invoke-virtual {v1, v0}, Lcom/twitter/model/moments/viewmodels/a$a;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/model/moments/viewmodels/a$a;

    move-result-object v0

    .line 29
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/a$a;->a()Lcom/twitter/model/moments/viewmodels/a;

    move-result-object p1

    goto :goto_0
.end method
