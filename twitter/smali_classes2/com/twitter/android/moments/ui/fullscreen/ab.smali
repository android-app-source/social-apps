.class Lcom/twitter/android/moments/ui/fullscreen/ab;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/moments/core/ui/widget/sectionpager/d;


# instance fields
.field private final a:Lcom/twitter/android/moments/data/e;

.field private final b:Lcom/twitter/android/moments/ui/fullscreen/bl;

.field private final c:Labh;

.field private final d:Lcom/twitter/android/moments/data/ai;

.field private final e:Lcom/twitter/android/moments/ui/fullscreen/cp;

.field private final f:Landroid/content/res/Resources;

.field private final g:Lrx/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/g",
            "<",
            "Lcom/twitter/model/moments/p;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lxj;

.field private final i:Lcom/twitter/model/moments/Moment;

.field private final j:Lcnz;

.field private final k:Lcom/twitter/model/core/Tweet;

.field private final l:Labi;

.field private final m:Lcom/twitter/app/users/URTUsersActivity$b;

.field private final n:Lzr;

.field private o:Lrx/j;


# direct methods
.method constructor <init>(Lcom/twitter/model/moments/Moment;Lcom/twitter/model/core/Tweet;Lcnz;Lcom/twitter/android/moments/ui/fullscreen/bl;Lcom/twitter/model/moments/viewmodels/MomentPage;Lcfn;Labh;Labi;Lcom/twitter/android/moments/data/e;Lcom/twitter/android/moments/data/ai;Lcom/twitter/android/moments/ui/fullscreen/cp;Landroid/content/res/Resources;Lrx/g;Lxj;Lcom/twitter/app/users/URTUsersActivity$b;Lzr;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/Moment;",
            "Lcom/twitter/model/core/Tweet;",
            "Lcnz;",
            "Lcom/twitter/android/moments/ui/fullscreen/bl;",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            "Lcfn;",
            "Labh;",
            "Labi;",
            "Lcom/twitter/android/moments/data/e;",
            "Lcom/twitter/android/moments/data/ai;",
            "Lcom/twitter/android/moments/ui/fullscreen/cp;",
            "Landroid/content/res/Resources;",
            "Lrx/g",
            "<",
            "Lcom/twitter/model/moments/p;",
            ">;",
            "Lxj;",
            "Lcom/twitter/app/users/URTUsersActivity$b;",
            "Lzr;",
            ")V"
        }
    .end annotation

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->i:Lcom/twitter/model/moments/Moment;

    .line 105
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->k:Lcom/twitter/model/core/Tweet;

    .line 106
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->j:Lcnz;

    .line 107
    iput-object p4, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->b:Lcom/twitter/android/moments/ui/fullscreen/bl;

    .line 109
    iput-object p8, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->l:Labi;

    .line 110
    iput-object p7, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->c:Labh;

    .line 111
    iput-object p9, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->a:Lcom/twitter/android/moments/data/e;

    .line 112
    iput-object p10, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->d:Lcom/twitter/android/moments/data/ai;

    .line 113
    iput-object p11, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->e:Lcom/twitter/android/moments/ui/fullscreen/cp;

    .line 114
    iput-object p12, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->f:Landroid/content/res/Resources;

    .line 115
    iput-object p13, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->g:Lrx/g;

    .line 116
    iput-object p14, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->h:Lxj;

    .line 117
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->m:Lcom/twitter/app/users/URTUsersActivity$b;

    .line 118
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->n:Lzr;

    .line 119
    invoke-direct {p0, p5, p6}, Lcom/twitter/android/moments/ui/fullscreen/ab;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcfn;)V

    .line 120
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/ab;->e()V

    .line 121
    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/twitter/model/moments/Moment;Lcom/twitter/model/core/Tweet;Lcom/twitter/android/moments/data/w;Lcom/twitter/android/moments/data/am;Lzq;Lcom/twitter/android/moments/ui/fullscreen/bl;Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/android/moments/ui/fullscreen/cp;Lcfn;Lrx/g;)Lcom/twitter/android/moments/ui/fullscreen/ab;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/twitter/model/moments/Moment;",
            "Lcom/twitter/model/core/Tweet;",
            "Lcom/twitter/android/moments/data/w;",
            "Lcom/twitter/android/moments/data/am;",
            "Lzq;",
            "Lcom/twitter/android/moments/ui/fullscreen/bl;",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            "Lcom/twitter/android/moments/ui/fullscreen/cp;",
            "Lcfn;",
            "Lrx/g",
            "<",
            "Lcom/twitter/model/moments/p;",
            ">;)",
            "Lcom/twitter/android/moments/ui/fullscreen/ab;"
        }
    .end annotation

    .prologue
    .line 70
    .line 71
    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-static {v2}, Labh;->a(Landroid/view/LayoutInflater;)Labh;

    move-result-object v9

    .line 72
    new-instance v10, Labi;

    .line 73
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v10, v2, v9}, Labi;-><init>(Landroid/content/res/Resources;Labh;)V

    .line 74
    invoke-virtual {v9}, Labh;->i()Landroid/view/View;

    move-result-object v2

    .line 75
    new-instance v3, Laae;

    invoke-direct {v3, v2}, Laae;-><init>(Landroid/view/View;)V

    .line 76
    new-instance v11, Lcom/twitter/android/moments/data/e;

    move-object/from16 v0, p3

    move-object/from16 v1, p5

    invoke-direct {v11, v3, v0, v1}, Lcom/twitter/android/moments/data/e;-><init>(Laae;Lcom/twitter/android/moments/data/w;Lzq;)V

    .line 79
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f130502

    const v5, 0x7f130503

    .line 78
    invoke-static {v3, v2, v4, v5}, Laah;->a(Landroid/content/res/Resources;Landroid/view/View;II)Laaf;

    move-result-object v2

    .line 80
    new-instance v12, Lcom/twitter/android/moments/data/ai;

    new-instance v3, Laag;

    invoke-direct {v3, v2}, Laag;-><init>(Laaf;)V

    move-object/from16 v0, p4

    invoke-direct {v12, v3, v0}, Lcom/twitter/android/moments/data/ai;-><init>(Laag;Lcom/twitter/android/moments/data/am;)V

    .line 82
    new-instance v5, Lcnz;

    .line 83
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v5, v2, v3}, Lcnz;-><init>(J)V

    .line 84
    invoke-static {}, Lxj;->a()Lxj;

    move-result-object v16

    .line 85
    new-instance v17, Lcom/twitter/app/users/URTUsersActivity$b;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/twitter/app/users/URTUsersActivity$b;-><init>(Landroid/app/Activity;)V

    .line 86
    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/twitter/model/moments/Moment;->b:J

    invoke-static {v2, v3}, Lzr;->a(J)Lzr;

    move-result-object v18

    .line 87
    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/ab;

    .line 89
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p9

    move-object/from16 v13, p8

    move-object/from16 v15, p10

    invoke-direct/range {v2 .. v18}, Lcom/twitter/android/moments/ui/fullscreen/ab;-><init>(Lcom/twitter/model/moments/Moment;Lcom/twitter/model/core/Tweet;Lcnz;Lcom/twitter/android/moments/ui/fullscreen/bl;Lcom/twitter/model/moments/viewmodels/MomentPage;Lcfn;Labh;Labi;Lcom/twitter/android/moments/data/e;Lcom/twitter/android/moments/data/ai;Lcom/twitter/android/moments/ui/fullscreen/cp;Landroid/content/res/Resources;Lrx/g;Lxj;Lcom/twitter/app/users/URTUsersActivity$b;Lzr;)V

    .line 87
    return-object v2
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/ab;)Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->k:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/ab;Lcom/twitter/model/moments/Moment;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ab;->a(Lcom/twitter/model/moments/Moment;)V

    return-void
.end method

.method private a(Lcom/twitter/model/moments/Moment;)V
    .locals 8

    .prologue
    .line 214
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->f:Landroid/content/res/Resources;

    const v1, 0x7f0c0011

    iget-wide v2, p1, Lcom/twitter/model/moments/Moment;->u:J

    long-to-int v2, v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->f:Landroid/content/res/Resources;

    iget-wide v6, p1, Lcom/twitter/model/moments/Moment;->u:J

    .line 215
    invoke-static {v5, v6, v7}, Lcom/twitter/util/r;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 214
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 216
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->c:Labh;

    invoke-virtual {v1, v0}, Labh;->a(Ljava/lang/String;)V

    .line 217
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->c:Labh;

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/ab$6;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ab$6;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ab;Lcom/twitter/model/moments/Moment;)V

    invoke-virtual {v0, v1}, Labh;->a(Landroid/view/View$OnClickListener;)V

    .line 224
    return-void
.end method

.method private a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcfn;)V
    .locals 6

    .prologue
    .line 131
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->l:Labi;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->k:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, p1, v1}, Labi;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/core/Tweet;)V

    .line 134
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->i:Lcom/twitter/model/moments/Moment;

    .line 135
    iget-object v1, v0, Lcom/twitter/model/moments/Moment;->l:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->k:Lcom/twitter/model/core/Tweet;

    if-eqz v1, :cond_0

    .line 136
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->c:Labh;

    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/ab$1;

    invoke-direct {v2, p0}, Lcom/twitter/android/moments/ui/fullscreen/ab$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ab;)V

    invoke-virtual {v1, v2}, Labh;->c(Landroid/view/View$OnClickListener;)V

    .line 145
    :cond_0
    iget-object v1, v0, Lcom/twitter/model/moments/Moment;->o:Lcom/twitter/model/moments/a;

    invoke-static {v1}, Lcom/twitter/model/moments/a;->a(Lcom/twitter/model/moments/a;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 146
    iget-object v1, v0, Lcom/twitter/model/moments/Moment;->o:Lcom/twitter/model/moments/a;

    .line 147
    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/ab$2;

    invoke-direct {v2, p0, v1}, Lcom/twitter/android/moments/ui/fullscreen/ab$2;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ab;Lcom/twitter/model/moments/a;)V

    .line 153
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->c:Labh;

    invoke-virtual {v1, v2}, Labh;->b(Landroid/view/View$OnClickListener;)V

    .line 157
    :cond_1
    if-eqz p2, :cond_2

    .line 158
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->d:Lcom/twitter/android/moments/data/ai;

    invoke-virtual {v1, p2}, Lcom/twitter/android/moments/data/ai;->a(Lcfn;)V

    .line 164
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->a:Lcom/twitter/android/moments/data/e;

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/data/e;->a(Lcom/twitter/model/moments/Moment;)V

    .line 166
    iget-object v1, v0, Lcom/twitter/model/moments/Moment;->o:Lcom/twitter/model/moments/a;

    .line 167
    invoke-static {}, Lbsd;->h()Z

    move-result v2

    if-eqz v2, :cond_3

    if-eqz v1, :cond_3

    iget-wide v2, v1, Lcom/twitter/model/moments/a;->b:J

    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->j:Lcnz;

    invoke-virtual {v4}, Lcnz;->b()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 168
    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->c:Labh;

    invoke-virtual {v2}, Labh;->e()V

    .line 169
    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->c:Labh;

    new-instance v3, Lcom/twitter/android/moments/ui/fullscreen/ab$3;

    invoke-direct {v3, p0, v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/ab$3;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ab;Lcom/twitter/model/moments/Moment;Lcom/twitter/model/moments/a;)V

    invoke-virtual {v2, v3}, Labh;->d(Landroid/view/View$OnClickListener;)V

    .line 178
    :goto_1
    return-void

    .line 160
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->d:Lcom/twitter/android/moments/data/ai;

    invoke-virtual {v1}, Lcom/twitter/android/moments/data/ai;->a()V

    goto :goto_0

    .line 176
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->c:Labh;

    invoke-virtual {v0}, Labh;->f()V

    goto :goto_1
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/fullscreen/ab;)Lcom/twitter/android/moments/ui/fullscreen/bl;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->b:Lcom/twitter/android/moments/ui/fullscreen/bl;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/fullscreen/ab;)Lcom/twitter/android/moments/ui/fullscreen/cp;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->e:Lcom/twitter/android/moments/ui/fullscreen/cp;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/moments/ui/fullscreen/ab;)Labh;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->c:Labh;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/moments/ui/fullscreen/ab;)Lcom/twitter/app/users/URTUsersActivity$b;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->m:Lcom/twitter/app/users/URTUsersActivity$b;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->g:Lrx/g;

    .line 182
    invoke-static {}, Lcom/twitter/android/moments/ui/fullscreen/ab;->f()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->b(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->h:Lxj;

    iget-object v1, v1, Lxj;->a:Lrx/f;

    .line 183
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->h:Lxj;

    iget-object v1, v1, Lxj;->b:Lrx/f;

    .line 184
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 185
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/ab;->g()Lcqw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->o:Lrx/j;

    .line 186
    return-void
.end method

.method private static f()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<",
            "Lcom/twitter/model/moments/p;",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 190
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/ab$4;

    invoke-direct {v0}, Lcom/twitter/android/moments/ui/fullscreen/ab$4;-><init>()V

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/moments/ui/fullscreen/ab;)Lzr;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->n:Lzr;

    return-object v0
.end method

.method private g()Lcqw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcqw",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/ab$5;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/fullscreen/ab$5;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ab;)V

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->a:Lcom/twitter/android/moments/data/e;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/e;->a()V

    .line 237
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->d:Lcom/twitter/android/moments/data/ai;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/ai;->a()V

    .line 238
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->l:Labi;

    invoke-virtual {v0}, Labi;->c()V

    .line 239
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->o:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 240
    return-void
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ab;->c:Labh;

    invoke-virtual {v0}, Labh;->i()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 228
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 232
    return-void
.end method
