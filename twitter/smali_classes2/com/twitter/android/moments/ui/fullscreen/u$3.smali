.class Lcom/twitter/android/moments/ui/fullscreen/u$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/object/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/fullscreen/u;-><init>(Landroid/support/v4/app/FragmentActivity;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/fullscreen/bs;Lbsb;Lcom/twitter/util/collection/k;Lcom/twitter/library/client/v;Lcom/twitter/library/client/p;Landroid/support/v4/app/LoaderManager;Lcom/twitter/android/moments/data/i;Lcom/twitter/android/moments/ui/fullscreen/bd;Landroid/support/v4/view/ViewPager;Lcom/twitter/moments/core/ui/widget/sectionpager/e;Lcom/twitter/android/ck;Lcom/twitter/android/moments/ui/b;Lcom/twitter/android/moments/ui/fullscreen/br;Labf;Landroid/view/View;Lcom/twitter/util/p;Lcom/twitter/android/moments/ui/fullscreen/ad;Lcom/twitter/android/moments/ui/fullscreen/ak;Lcom/twitter/android/moments/ui/fullscreen/bz;Labm;Lyj;Lcom/twitter/app/common/util/j;Lcom/twitter/model/moments/viewmodels/a;Landroid/os/Bundle;Lcom/twitter/android/moments/data/am;Lcom/twitter/android/moments/ui/fullscreen/r;Lyf;Lcom/twitter/android/moments/ui/fullscreen/co;Lcom/twitter/android/av/k;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/object/d",
        "<",
        "Lcom/twitter/model/core/Tweet;",
        "Lcne;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Lcom/twitter/android/moments/ui/fullscreen/u;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/u;J)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/u$3;->b:Lcom/twitter/android/moments/ui/fullscreen/u;

    iput-wide p2, p0, Lcom/twitter/android/moments/ui/fullscreen/u$3;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/Tweet;)Lcne;
    .locals 7

    .prologue
    .line 283
    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/y;

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u$3;->b:Lcom/twitter/android/moments/ui/fullscreen/u;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/u;->a(Lcom/twitter/android/moments/ui/fullscreen/u;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-wide v4, p0, Lcom/twitter/android/moments/ui/fullscreen/u$3;->a:J

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u$3;->b:Lcom/twitter/android/moments/ui/fullscreen/u;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/u;->b(Lcom/twitter/android/moments/ui/fullscreen/u;)Lcom/twitter/android/moments/ui/fullscreen/q;

    move-result-object v6

    move-object v3, p1

    invoke-direct/range {v1 .. v6}, Lcom/twitter/android/moments/ui/fullscreen/y;-><init>(Landroid/content/Context;Lcom/twitter/model/core/Tweet;JLcom/twitter/android/moments/ui/fullscreen/cp;)V

    return-object v1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 279
    check-cast p1, Lcom/twitter/model/core/Tweet;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/u$3;->a(Lcom/twitter/model/core/Tweet;)Lcne;

    move-result-object v0

    return-object v0
.end method
