.class public Lcom/twitter/android/moments/ui/fullscreen/w;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/fullscreen/w$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/twitter/util/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/android/moments/ui/fullscreen/cv;

.field private final d:Lcom/twitter/android/moments/ui/fullscreen/bd;

.field private final e:Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;

.field private final f:Lcom/twitter/android/moments/ui/fullscreen/ak;

.field private final g:Lcom/twitter/android/moments/ui/fullscreen/bz;

.field private final h:Lcom/twitter/app/common/util/j;

.field private final i:Lzn;

.field private final j:Lcom/twitter/android/moments/ui/fullscreen/ad;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/moments/ui/fullscreen/ad",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcom/twitter/android/moments/ui/fullscreen/m;

.field private final l:Lcom/twitter/android/moments/ui/fullscreen/cs;

.field private final m:Lcom/twitter/android/moments/ui/fullscreen/bp;

.field private final n:Lbxo;

.field private final o:Lcom/twitter/android/moments/ui/fullscreen/as;

.field private final p:Lcom/twitter/android/av/k;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/util/p;Lcom/twitter/android/moments/ui/fullscreen/cv;Lcom/twitter/android/moments/ui/fullscreen/bd;Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;Lcom/twitter/android/moments/ui/fullscreen/ak;Lcom/twitter/app/common/util/j;Lcom/twitter/android/moments/ui/fullscreen/bz;Lzn;Lcom/twitter/android/moments/ui/fullscreen/ad;Lcom/twitter/android/moments/ui/fullscreen/m;Lcom/twitter/android/moments/ui/fullscreen/cs;Lcom/twitter/android/moments/ui/fullscreen/bp;Lbxo;Lcom/twitter/android/moments/ui/fullscreen/as;Lcom/twitter/android/av/k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;",
            "Lcom/twitter/android/moments/ui/fullscreen/cv;",
            "Lcom/twitter/android/moments/ui/fullscreen/bd;",
            "Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;",
            "Lcom/twitter/android/moments/ui/fullscreen/ak;",
            "Lcom/twitter/app/common/util/j;",
            "Lcom/twitter/android/moments/ui/fullscreen/bz;",
            "Lzn;",
            "Lcom/twitter/android/moments/ui/fullscreen/ad",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;",
            ">;",
            "Lcom/twitter/android/moments/ui/fullscreen/m;",
            "Lcom/twitter/android/moments/ui/fullscreen/cs;",
            "Lcom/twitter/android/moments/ui/fullscreen/bp;",
            "Lbxo;",
            "Lcom/twitter/android/moments/ui/fullscreen/as;",
            "Lcom/twitter/android/av/k;",
            ")V"
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->b:Lcom/twitter/util/p;

    .line 74
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->c:Lcom/twitter/android/moments/ui/fullscreen/cv;

    .line 75
    iput-object p4, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->d:Lcom/twitter/android/moments/ui/fullscreen/bd;

    .line 76
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->a:Landroid/app/Activity;

    .line 77
    iput-object p5, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->e:Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;

    .line 78
    iput-object p6, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->f:Lcom/twitter/android/moments/ui/fullscreen/ak;

    .line 79
    iput-object p8, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->g:Lcom/twitter/android/moments/ui/fullscreen/bz;

    .line 80
    iput-object p7, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->h:Lcom/twitter/app/common/util/j;

    .line 81
    iput-object p9, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->i:Lzn;

    .line 82
    iput-object p10, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->j:Lcom/twitter/android/moments/ui/fullscreen/ad;

    .line 83
    iput-object p11, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->k:Lcom/twitter/android/moments/ui/fullscreen/m;

    .line 84
    iput-object p12, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->l:Lcom/twitter/android/moments/ui/fullscreen/cs;

    .line 85
    iput-object p13, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->m:Lcom/twitter/android/moments/ui/fullscreen/bp;

    .line 86
    iput-object p14, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->n:Lbxo;

    .line 87
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->o:Lcom/twitter/android/moments/ui/fullscreen/as;

    .line 88
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->p:Lcom/twitter/android/av/k;

    .line 89
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/w;)Lcom/twitter/android/moments/ui/fullscreen/cv;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->c:Lcom/twitter/android/moments/ui/fullscreen/cv;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/android/moments/viewmodels/k;Lcom/twitter/android/moments/ui/fullscreen/cu;)Lcom/twitter/moments/core/ui/widget/sectionpager/a;
    .locals 7

    .prologue
    .line 153
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->a:Landroid/app/Activity;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->b:Lcom/twitter/util/p;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->j:Lcom/twitter/android/moments/ui/fullscreen/ad;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->i:Lzn;

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/moments/ui/fullscreen/am;->a(Landroid/content/Context;Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/android/moments/viewmodels/k;Lcom/twitter/util/p;Lcom/twitter/android/moments/ui/fullscreen/ad;Lzn;Lcom/twitter/android/moments/ui/fullscreen/cu;)Lcom/twitter/android/moments/ui/fullscreen/am;

    move-result-object v0

    return-object v0
.end method

.method a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/moments/core/ui/widget/sectionpager/d;)Lcom/twitter/moments/core/ui/widget/sectionpager/a;
    .locals 8

    .prologue
    .line 169
    const-class v0, Lcom/twitter/android/moments/ui/fullscreen/cu;

    sget-object v1, Lcom/twitter/android/moments/ui/fullscreen/cu;->a:Lcom/twitter/android/moments/ui/fullscreen/cu;

    .line 170
    invoke-static {p2, v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/twitter/android/moments/ui/fullscreen/cu;

    .line 172
    sget-object v0, Lcom/twitter/android/moments/ui/fullscreen/w$3;->a:[I

    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 219
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unrecognized moment page type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 220
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    move-object v1, p1

    .line 174
    check-cast v1, Lcom/twitter/model/moments/viewmodels/p;

    .line 175
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->e:Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->b:Lcom/twitter/util/p;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->j:Lcom/twitter/android/moments/ui/fullscreen/ad;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->i:Lzn;

    iget-object v6, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->f:Lcom/twitter/android/moments/ui/fullscreen/ak;

    invoke-static/range {v0 .. v7}, Lcom/twitter/android/moments/ui/fullscreen/dg;->a(Landroid/content/Context;Lcom/twitter/model/moments/viewmodels/p;Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;Lcom/twitter/util/p;Lcom/twitter/android/moments/ui/fullscreen/ad;Lzn;Lcom/twitter/android/moments/ui/fullscreen/ak;Lcom/twitter/android/moments/ui/fullscreen/cu;)Lcom/twitter/android/moments/ui/fullscreen/dg;

    move-result-object v0

    .line 216
    :goto_0
    return-object v0

    .line 180
    :pswitch_1
    new-instance v1, Lcom/twitter/android/moments/viewmodels/af;

    move-object v0, p1

    check-cast v0, Lcom/twitter/model/moments/viewmodels/o;

    invoke-direct {v1, v0}, Lcom/twitter/android/moments/viewmodels/af;-><init>(Lcom/twitter/model/moments/viewmodels/o;)V

    .line 182
    invoke-virtual {p0, p1, v1, v7}, Lcom/twitter/android/moments/ui/fullscreen/w;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/android/moments/viewmodels/k;Lcom/twitter/android/moments/ui/fullscreen/cu;)Lcom/twitter/moments/core/ui/widget/sectionpager/a;

    move-result-object v0

    goto :goto_0

    .line 186
    :pswitch_2
    new-instance v3, Lcom/twitter/android/moments/viewmodels/ac;

    move-object v0, p1

    check-cast v0, Lcom/twitter/model/moments/viewmodels/j;

    invoke-direct {v3, v0}, Lcom/twitter/android/moments/viewmodels/ac;-><init>(Lcom/twitter/model/moments/viewmodels/j;)V

    .line 187
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->a:Landroid/app/Activity;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->b:Lcom/twitter/util/p;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->j:Lcom/twitter/android/moments/ui/fullscreen/ad;

    iget-object v6, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->i:Lzn;

    move-object v2, p1

    invoke-static/range {v1 .. v7}, Lcom/twitter/android/moments/ui/fullscreen/am;->a(Landroid/content/Context;Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/android/moments/viewmodels/k;Lcom/twitter/util/p;Lcom/twitter/android/moments/ui/fullscreen/ad;Lzn;Lcom/twitter/android/moments/ui/fullscreen/cu;)Lcom/twitter/android/moments/ui/fullscreen/am;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    move-object v1, p1

    .line 191
    check-cast v1, Lcom/twitter/model/moments/viewmodels/m;

    .line 192
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->j:Lcom/twitter/android/moments/ui/fullscreen/ad;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->k:Lcom/twitter/android/moments/ui/fullscreen/m;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->l:Lcom/twitter/android/moments/ui/fullscreen/cs;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->m:Lcom/twitter/android/moments/ui/fullscreen/bp;

    iget-object v6, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->n:Lbxo;

    iget-object v7, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->o:Lcom/twitter/android/moments/ui/fullscreen/as;

    invoke-static/range {v0 .. v7}, Lcom/twitter/android/moments/ui/fullscreen/di;->a(Landroid/app/Activity;Lcom/twitter/model/moments/viewmodels/m;Lcom/twitter/android/moments/ui/fullscreen/ad;Lcom/twitter/android/moments/ui/fullscreen/m;Lcom/twitter/android/moments/ui/fullscreen/cs;Lcom/twitter/android/moments/ui/fullscreen/bp;Lbxo;Lcom/twitter/android/moments/ui/fullscreen/as;)Lcom/twitter/android/moments/ui/fullscreen/di;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    move-object v1, p1

    .line 197
    check-cast v1, Lcom/twitter/model/moments/viewmodels/q;

    .line 198
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->j:Lcom/twitter/android/moments/ui/fullscreen/ad;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->k:Lcom/twitter/android/moments/ui/fullscreen/m;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->l:Lcom/twitter/android/moments/ui/fullscreen/cs;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->m:Lcom/twitter/android/moments/ui/fullscreen/bp;

    iget-object v6, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->n:Lbxo;

    iget-object v7, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->o:Lcom/twitter/android/moments/ui/fullscreen/as;

    invoke-static/range {v0 .. v7}, Lcom/twitter/android/moments/ui/fullscreen/di;->a(Landroid/app/Activity;Lcom/twitter/model/moments/viewmodels/q;Lcom/twitter/android/moments/ui/fullscreen/ad;Lcom/twitter/android/moments/ui/fullscreen/m;Lcom/twitter/android/moments/ui/fullscreen/cs;Lcom/twitter/android/moments/ui/fullscreen/bp;Lbxo;Lcom/twitter/android/moments/ui/fullscreen/as;)Lcom/twitter/android/moments/ui/fullscreen/di;

    move-result-object v0

    goto :goto_0

    .line 203
    :pswitch_5
    check-cast p1, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    .line 205
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/ci;

    invoke-direct {v0, p2}, Lcom/twitter/android/moments/ui/fullscreen/ci;-><init>(Lcom/twitter/moments/core/ui/widget/sectionpager/d;)V

    .line 207
    iget-object v1, p1, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->a:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    sget-object v2, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;->e:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    if-ne v1, v2, :cond_0

    .line 208
    invoke-static {p1, v0, v7, p0}, Lcom/twitter/android/moments/ui/fullscreen/aq;->a(Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;Lcom/twitter/android/moments/ui/fullscreen/ci;Lcom/twitter/android/moments/ui/fullscreen/cu;Lcom/twitter/android/moments/ui/fullscreen/w;)Lcom/twitter/android/moments/ui/fullscreen/aq;

    move-result-object v0

    goto :goto_0

    .line 211
    :cond_0
    invoke-virtual {p0, p1, v0, v7}, Lcom/twitter/android/moments/ui/fullscreen/w;->a(Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;Lcom/twitter/android/moments/ui/fullscreen/ci;Lcom/twitter/android/moments/ui/fullscreen/cu;)Lcom/twitter/moments/core/ui/widget/sectionpager/a;

    move-result-object v0

    goto :goto_0

    .line 216
    :pswitch_6
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/cn;->a(Landroid/view/LayoutInflater;)Lcom/twitter/android/moments/ui/fullscreen/cn;

    move-result-object v0

    goto :goto_0

    .line 172
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_6
    .end packed-switch
.end method

.method public a(Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;Lcom/twitter/android/moments/ui/fullscreen/ci;Lcom/twitter/android/moments/ui/fullscreen/cu;)Lcom/twitter/moments/core/ui/widget/sectionpager/a;
    .locals 12

    .prologue
    .line 161
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->b:Lcom/twitter/util/p;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->j:Lcom/twitter/android/moments/ui/fullscreen/ad;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->d:Lcom/twitter/android/moments/ui/fullscreen/bd;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->f:Lcom/twitter/android/moments/ui/fullscreen/ak;

    iget-object v6, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->h:Lcom/twitter/app/common/util/j;

    iget-object v8, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->g:Lcom/twitter/android/moments/ui/fullscreen/bz;

    iget-object v9, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->i:Lzn;

    iget-object v11, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->p:Lcom/twitter/android/av/k;

    move-object v1, p1

    move-object v7, p2

    move-object v10, p3

    invoke-static/range {v0 .. v11}, Lcom/twitter/android/moments/ui/fullscreen/dh;->a(Landroid/app/Activity;Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;Lcom/twitter/util/p;Lcom/twitter/android/moments/ui/fullscreen/ad;Lcom/twitter/android/moments/ui/fullscreen/bd;Lcom/twitter/android/moments/ui/fullscreen/ak;Lcom/twitter/app/common/util/j;Lcom/twitter/android/moments/ui/fullscreen/ci;Lcom/twitter/android/moments/ui/fullscreen/bz;Lzn;Lcom/twitter/android/moments/ui/fullscreen/cu;Lcom/twitter/android/av/k;)Lcom/twitter/android/moments/ui/fullscreen/dh;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/lang/String;)Lcom/twitter/moments/core/ui/widget/sectionpager/a;
    .locals 3

    .prologue
    .line 227
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/df;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->j:Lcom/twitter/android/moments/ui/fullscreen/ad;

    invoke-direct {v0, v1, v2, p1}, Lcom/twitter/android/moments/ui/fullscreen/df;-><init>(Landroid/content/Context;Lcom/twitter/android/moments/ui/fullscreen/ad;Ljava/lang/String;)V

    return-object v0
.end method

.method a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/moments/core/ui/widget/sectionpager/b;
    .locals 3

    .prologue
    .line 122
    sget-object v0, Lcom/twitter/android/moments/ui/fullscreen/w$3;->a:[I

    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 143
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unrecognized moment page type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 144
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :pswitch_0
    new-instance v0, Lcom/twitter/moments/core/ui/widget/sectionpager/b;

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/w$a;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/w;->a:Landroid/app/Activity;

    check-cast p1, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;

    invoke-direct {v1, v2, p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/w$a;-><init>(Landroid/content/Context;Lcom/twitter/android/moments/ui/fullscreen/w;Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;)V

    invoke-direct {v0, v1}, Lcom/twitter/moments/core/ui/widget/sectionpager/b;-><init>(Lcom/twitter/util/object/d;)V

    .line 134
    :goto_0
    return-object v0

    :pswitch_1
    new-instance v0, Lcom/twitter/moments/core/ui/widget/sectionpager/b;

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/w$2;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/w$2;-><init>(Lcom/twitter/android/moments/ui/fullscreen/w;Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    invoke-direct {v0, v1}, Lcom/twitter/moments/core/ui/widget/sectionpager/b;-><init>(Lcom/twitter/util/object/d;)V

    goto :goto_0

    .line 122
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/twitter/model/moments/viewmodels/g;)Lcom/twitter/moments/core/ui/widget/sectionpager/c;
    .locals 3

    .prologue
    .line 97
    iget-object v0, p1, Lcom/twitter/model/moments/viewmodels/g;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/collection/MutableList;->a(I)Ljava/util/List;

    move-result-object v1

    .line 98
    iget-object v0, p1, Lcom/twitter/model/moments/viewmodels/g;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 99
    invoke-virtual {p0, v0}, Lcom/twitter/android/moments/ui/fullscreen/w;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/moments/core/ui/widget/sectionpager/b;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 101
    :cond_0
    new-instance v0, Lcom/twitter/moments/core/ui/widget/sectionpager/c;

    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/w$1;

    invoke-direct {v2, p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/w$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/w;Lcom/twitter/model/moments/viewmodels/g;)V

    invoke-direct {v0, v1, v2}, Lcom/twitter/moments/core/ui/widget/sectionpager/c;-><init>(Ljava/util/List;Lcom/twitter/util/object/j;)V

    return-object v0
.end method

.method public a(Lcom/twitter/model/moments/viewmodels/a;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/viewmodels/a;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/moments/core/ui/widget/sectionpager/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 113
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/a;->g()Ljava/util/List;

    move-result-object v0

    .line 114
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/g;

    .line 115
    invoke-virtual {p0, v0}, Lcom/twitter/android/moments/ui/fullscreen/w;->a(Lcom/twitter/model/moments/viewmodels/g;)Lcom/twitter/moments/core/ui/widget/sectionpager/c;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 117
    :cond_0
    return-object v1
.end method
