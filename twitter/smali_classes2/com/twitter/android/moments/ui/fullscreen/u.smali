.class public Lcom/twitter/android/moments/ui/fullscreen/u;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/fullscreen/u$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/support/v4/app/FragmentActivity;

.field private final b:Lcom/twitter/android/moments/ui/fullscreen/bs;

.field private final c:Landroid/support/v4/view/ViewPager;

.field private final d:Landroid/view/View;

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/support/v4/view/ViewPager$OnPageChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/twitter/moments/core/ui/widget/sectionpager/e;

.field private final g:Lcom/twitter/android/moments/data/w;

.field private final h:Lcom/twitter/android/moments/data/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/moments/data/ae",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/Moment;",
            "Lcom/twitter/model/moments/p;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lxv;

.field private final j:Lyj;

.field private final k:Lcom/twitter/android/moments/data/s;

.field private final l:Lcom/twitter/android/moments/ui/fullscreen/bo;

.field private final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/moments/core/ui/widget/sectionpager/c;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lcom/twitter/android/moments/ui/fullscreen/q;

.field private final o:Lcom/twitter/moments/core/ui/widget/capsule/a;

.field private final p:Lzn;

.field private final q:Lzo;

.field private final r:Lcom/twitter/android/moments/ui/fullscreen/e;

.field private final s:Lcom/twitter/model/moments/viewmodels/a;

.field private final t:Lcom/twitter/android/moments/ui/fullscreen/aw;

.field private final u:Lcom/twitter/android/moments/ui/fullscreen/ce;

.field private v:Lcom/twitter/android/moments/ui/fullscreen/u$a;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/fullscreen/bs;Lbsb;Lcom/twitter/util/collection/k;Lcom/twitter/library/client/v;Lcom/twitter/library/client/p;Landroid/support/v4/app/LoaderManager;Lcom/twitter/android/moments/data/i;Lcom/twitter/android/moments/ui/fullscreen/bd;Landroid/support/v4/view/ViewPager;Lcom/twitter/moments/core/ui/widget/sectionpager/e;Lcom/twitter/android/ck;Lcom/twitter/android/moments/ui/b;Lcom/twitter/android/moments/ui/fullscreen/br;Labf;Landroid/view/View;Lcom/twitter/util/p;Lcom/twitter/android/moments/ui/fullscreen/ad;Lcom/twitter/android/moments/ui/fullscreen/ak;Lcom/twitter/android/moments/ui/fullscreen/bz;Labm;Lyj;Lcom/twitter/app/common/util/j;Lcom/twitter/model/moments/viewmodels/a;Landroid/os/Bundle;Lcom/twitter/android/moments/data/am;Lcom/twitter/android/moments/ui/fullscreen/r;Lyf;Lcom/twitter/android/moments/ui/fullscreen/co;Lcom/twitter/android/av/k;)V
    .locals 74
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentActivity;",
            "Landroid/view/ViewGroup;",
            "Lcom/twitter/android/moments/ui/fullscreen/bs;",
            "Lbsb;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/twitter/library/client/v;",
            "Lcom/twitter/library/client/p;",
            "Landroid/support/v4/app/LoaderManager;",
            "Lcom/twitter/android/moments/data/i;",
            "Lcom/twitter/android/moments/ui/fullscreen/bd;",
            "Landroid/support/v4/view/ViewPager;",
            "Lcom/twitter/moments/core/ui/widget/sectionpager/e;",
            "Lcom/twitter/android/ck;",
            "Lcom/twitter/android/moments/ui/b;",
            "Lcom/twitter/android/moments/ui/fullscreen/br;",
            "Labf;",
            "Landroid/view/View;",
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;",
            "Lcom/twitter/android/moments/ui/fullscreen/ad",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;",
            ">;",
            "Lcom/twitter/android/moments/ui/fullscreen/ak;",
            "Lcom/twitter/android/moments/ui/fullscreen/bz;",
            "Labm;",
            "Lyj;",
            "Lcom/twitter/app/common/util/j;",
            "Lcom/twitter/model/moments/viewmodels/a;",
            "Landroid/os/Bundle;",
            "Lcom/twitter/android/moments/data/am;",
            "Lcom/twitter/android/moments/ui/fullscreen/r;",
            "Lyf;",
            "Lcom/twitter/android/moments/ui/fullscreen/co;",
            "Lcom/twitter/android/av/k;",
            ")V"
        }
    .end annotation

    .prologue
    .line 140
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 141
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    .line 142
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/fullscreen/u;->b:Lcom/twitter/android/moments/ui/fullscreen/bs;

    .line 143
    move-object/from16 v0, p11

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/fullscreen/u;->c:Landroid/support/v4/view/ViewPager;

    .line 144
    move-object/from16 v0, p12

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/fullscreen/u;->f:Lcom/twitter/moments/core/ui/widget/sectionpager/e;

    .line 145
    move-object/from16 v0, p17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/fullscreen/u;->d:Landroid/view/View;

    .line 146
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->e:Ljava/util/Set;

    .line 147
    move-object/from16 v0, p23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/fullscreen/u;->j:Lyj;

    .line 148
    move-object/from16 v0, p25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/fullscreen/u;->s:Lcom/twitter/model/moments/viewmodels/a;

    .line 150
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v42

    .line 153
    new-instance v43, Lcom/twitter/android/moments/ui/fullscreen/i;

    move-object/from16 v0, v43

    move-object/from16 v1, p25

    invoke-direct {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/i;-><init>(Lcom/twitter/model/moments/viewmodels/i;)V

    .line 154
    new-instance v73, Lcom/twitter/android/moments/ui/fullscreen/bu;

    move-object/from16 v0, v73

    move-object/from16 v1, p22

    move-object/from16 v2, v43

    move-object/from16 v3, p25

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/moments/ui/fullscreen/bu;-><init>(Labm;Lcom/twitter/android/moments/ui/fullscreen/i;Lcom/twitter/model/moments/viewmodels/i;)V

    .line 156
    invoke-virtual/range {p25 .. p25}, Lcom/twitter/model/moments/viewmodels/a;->a()Lcom/twitter/model/moments/Moment;

    move-result-object v6

    iget-wide v0, v6, Lcom/twitter/model/moments/Moment;->b:J

    move-wide/from16 v18, v0

    .line 157
    invoke-virtual/range {p6 .. p6}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v24

    .line 158
    invoke-static {}, Lcom/twitter/library/provider/t;->c()Lcom/twitter/library/provider/t;

    move-result-object v6

    .line 159
    new-instance v7, Lcom/twitter/android/moments/ui/fullscreen/u$1;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Lcom/twitter/android/moments/ui/fullscreen/u$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/u;)V

    .line 169
    new-instance v8, Lcom/twitter/android/moments/data/l;

    move-object/from16 v0, p1

    move-object/from16 v1, p6

    move-object/from16 v2, p7

    move-object/from16 v3, p4

    invoke-direct {v8, v0, v1, v2, v3}, Lcom/twitter/android/moments/data/l;-><init>(Landroid/content/Context;Lcom/twitter/library/client/v;Lcom/twitter/library/client/p;Lbsb;)V

    .line 171
    new-instance v9, Lcom/twitter/android/moments/data/w;

    new-instance v10, Lcom/twitter/android/moments/data/c;

    invoke-direct {v10, v7}, Lcom/twitter/android/moments/data/c;-><init>(Lcom/twitter/util/object/j;)V

    move-wide/from16 v0, v24

    invoke-direct {v9, v0, v1, v10, v8}, Lcom/twitter/android/moments/data/w;-><init>(JLcom/twitter/android/moments/data/c;Lcom/twitter/android/moments/data/l;)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->g:Lcom/twitter/android/moments/data/w;

    .line 173
    new-instance v15, Lcom/twitter/android/moments/data/n;

    .line 174
    invoke-virtual/range {p6 .. p6}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v7

    move-object/from16 v0, p1

    move-object/from16 v1, p7

    move-object/from16 v2, p4

    invoke-direct {v15, v0, v7, v1, v2}, Lcom/twitter/android/moments/data/n;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/p;Lbsb;)V

    .line 176
    invoke-static {v6}, Lbse;->a(Lcom/twitter/library/provider/t;)Lbse;

    move-result-object v10

    .line 177
    new-instance v6, Lzn;

    invoke-virtual/range {p25 .. p25}, Lcom/twitter/model/moments/viewmodels/a;->a()Lcom/twitter/model/moments/Moment;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->g:Lcom/twitter/android/moments/data/w;

    .line 178
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/twitter/android/moments/data/w;->a(Ljava/lang/Long;)Lrx/c;

    move-result-object v8

    move-object/from16 v9, p25

    move-object/from16 v11, p26

    invoke-direct/range {v6 .. v11}, Lzn;-><init>(Lcom/twitter/model/moments/Moment;Lrx/c;Lcom/twitter/model/moments/viewmodels/a;Lbse;Landroid/os/Bundle;)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->p:Lzn;

    .line 179
    new-instance v33, Laaa;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    const/16 v7, 0xbad

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->p:Lzn;

    move-object/from16 v0, v33

    invoke-direct {v0, v6, v7, v8}, Laaa;-><init>(Landroid/app/Activity;ILzn;)V

    .line 181
    const v6, 0x7f13041b

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v44

    .line 182
    const v6, 0x7f130511

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    move-object/from16 v41, v6

    check-cast v41, Landroid/view/ViewGroup;

    .line 183
    new-instance v6, Lcom/twitter/android/moments/ui/fullscreen/dk;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    move-wide/from16 v8, v18

    move-object/from16 v10, p25

    move-object/from16 v11, p4

    move-object/from16 v12, p7

    invoke-direct/range {v6 .. v12}, Lcom/twitter/android/moments/ui/fullscreen/dk;-><init>(Landroid/content/Context;JLcom/twitter/model/moments/viewmodels/i;Lbsb;Lcom/twitter/library/client/p;)V

    .line 185
    new-instance v32, Lcom/twitter/android/moments/data/MomentsFriendshipCache;

    move-object/from16 v0, v32

    move-object/from16 v1, p25

    invoke-direct {v0, v1}, Lcom/twitter/android/moments/data/MomentsFriendshipCache;-><init>(Lcom/twitter/model/moments/viewmodels/a;)V

    .line 186
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->p:Lzn;

    .line 187
    invoke-static {v7, v8}, Laab;->a(Landroid/support/v4/app/FragmentActivity;Lzn;)Laab;

    move-result-object v10

    .line 188
    new-instance v7, Lcom/twitter/android/moments/ui/fullscreen/cy;

    .line 189
    invoke-virtual/range {p25 .. p25}, Lcom/twitter/model/moments/viewmodels/a;->a()Lcom/twitter/model/moments/Moment;

    move-result-object v12

    move-object/from16 v8, p22

    move-object/from16 v9, p25

    move-object/from16 v11, p30

    invoke-direct/range {v7 .. v12}, Lcom/twitter/android/moments/ui/fullscreen/cy;-><init>(Labm;Lcom/twitter/model/moments/viewmodels/i;Lcom/twitter/android/moments/ui/fullscreen/bg;Lcom/twitter/android/moments/ui/fullscreen/co;Lcom/twitter/model/moments/Moment;)V

    .line 190
    new-instance v8, Lcom/twitter/android/moments/data/ae;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->g:Lcom/twitter/android/moments/data/w;

    sget-object v11, Lcom/twitter/model/moments/p;->a:Lcom/twitter/util/object/f;

    invoke-direct {v8, v9, v11}, Lcom/twitter/android/moments/data/ae;-><init>(Lauj;Lcom/twitter/util/object/f;)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->h:Lcom/twitter/android/moments/data/ae;

    .line 191
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->h:Lcom/twitter/android/moments/data/ae;

    .line 192
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/twitter/android/moments/data/ae;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v16

    move-object/from16 v12, p1

    move-object/from16 v13, p22

    move-object/from16 v14, p25

    move-object/from16 v17, p30

    .line 193
    invoke-static/range {v12 .. v19}, Lcom/twitter/android/moments/ui/fullscreen/aw;->a(Landroid/support/v4/app/FragmentActivity;Labm;Lcom/twitter/model/moments/viewmodels/a;Lcom/twitter/android/moments/data/n;Lrx/g;Lcom/twitter/android/moments/ui/fullscreen/co;J)Lcom/twitter/android/moments/ui/fullscreen/aw;

    move-result-object v8

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->t:Lcom/twitter/android/moments/ui/fullscreen/aw;

    .line 195
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    move-object/from16 v22, v0

    move-object/from16 v20, p22

    move-object/from16 v21, p25

    move-object/from16 v23, p30

    .line 196
    invoke-static/range {v20 .. v25}, Lcom/twitter/android/moments/ui/fullscreen/bc;->a(Labm;Lcom/twitter/model/moments/viewmodels/a;Landroid/app/Activity;Lcom/twitter/android/moments/ui/fullscreen/co;J)Lcom/twitter/android/moments/ui/fullscreen/bc;

    move-result-object v8

    .line 197
    new-instance v26, Lcom/twitter/android/moments/ui/fullscreen/bl;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    move-object/from16 v27, v0

    new-instance v28, Lcnz;

    move-object/from16 v0, v28

    move-wide/from16 v1, v24

    invoke-direct {v0, v1, v2}, Lcnz;-><init>(J)V

    new-instance v29, Lcom/twitter/android/dialog/e;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    move-object/from16 v0, v29

    invoke-direct {v0, v9}, Lcom/twitter/android/dialog/e;-><init>(Landroid/content/Context;)V

    new-instance v30, Lcom/twitter/android/moments/ui/fullscreen/bk;

    move-object/from16 v0, v30

    move-object/from16 v1, v42

    invoke-direct {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/bk;-><init>(Landroid/content/res/Resources;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->p:Lzn;

    move-object/from16 v34, v0

    new-instance v40, Lcom/twitter/util/android/g;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    move-object/from16 v0, v40

    invoke-direct {v0, v9}, Lcom/twitter/util/android/g;-><init>(Landroid/content/Context;)V

    move-object/from16 v31, p13

    move-object/from16 v35, p14

    move-object/from16 v36, p28

    move-object/from16 v37, p24

    move-wide/from16 v38, v18

    invoke-direct/range {v26 .. v40}, Lcom/twitter/android/moments/ui/fullscreen/bl;-><init>(Landroid/content/Context;Lcnz;Lcom/twitter/android/dialog/e;Lcom/twitter/android/moments/ui/fullscreen/bk;Lcom/twitter/android/ck;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/android/moments/ui/fullscreen/a;Lzn;Lcom/twitter/android/moments/ui/b;Lcom/twitter/android/moments/ui/fullscreen/r;Lcom/twitter/app/common/util/j;JLcom/twitter/util/android/g;)V

    .line 203
    new-instance v9, Lcom/twitter/android/moments/data/s;

    new-instance v11, Lcom/twitter/android/moments/data/q;

    new-instance v12, Lauh;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    .line 204
    invoke-virtual {v13}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    invoke-direct {v12, v13}, Lauh;-><init>(Landroid/content/ContentResolver;)V

    invoke-direct {v11, v12}, Lcom/twitter/android/moments/data/q;-><init>(Lauj;)V

    move-wide/from16 v0, v24

    invoke-direct {v9, v0, v1, v11}, Lcom/twitter/android/moments/data/s;-><init>(JLcom/twitter/android/moments/data/q;)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->k:Lcom/twitter/android/moments/data/s;

    .line 205
    new-instance v9, Lcom/twitter/android/moments/ui/fullscreen/u$2;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcom/twitter/android/moments/ui/fullscreen/u$2;-><init>(Lcom/twitter/android/moments/ui/fullscreen/u;)V

    .line 216
    new-instance v11, Lxv;

    new-instance v12, Lcnz;

    move-wide/from16 v0, v24

    invoke-direct {v12, v0, v1}, Lcnz;-><init>(J)V

    new-instance v13, Lcom/twitter/android/moments/data/c;

    invoke-direct {v13, v9}, Lcom/twitter/android/moments/data/c;-><init>(Lcom/twitter/util/object/j;)V

    invoke-direct {v11, v12, v13}, Lxv;-><init>(Lcnz;Lcom/twitter/android/moments/data/c;)V

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->i:Lxv;

    .line 220
    new-instance v34, Lbdj;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    move-object/from16 v35, v0

    .line 221
    invoke-virtual/range {p6 .. p6}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v36

    move-object/from16 v37, p4

    move-wide/from16 v38, v18

    invoke-direct/range {v34 .. v39}, Lbdj;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lbsb;J)V

    .line 223
    const/4 v9, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p7

    move-object/from16 v1, v34

    invoke-virtual {v0, v1, v9, v11}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;ILcom/twitter/library/client/s;)Z

    .line 225
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->k:Lcom/twitter/android/moments/data/s;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->i:Lxv;

    move-wide/from16 v0, v18

    invoke-static {v9, v11, v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/ce;->a(Lcom/twitter/android/moments/data/s;Lxv;J)Lcom/twitter/android/moments/ui/fullscreen/ce;

    move-result-object v9

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->u:Lcom/twitter/android/moments/ui/fullscreen/ce;

    .line 227
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->u:Lcom/twitter/android/moments/ui/fullscreen/ce;

    .line 228
    invoke-virtual {v9}, Lcom/twitter/android/moments/ui/fullscreen/ce;->a()Lrx/c;

    move-result-object v9

    .line 230
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v11}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v11

    .line 231
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    .line 232
    invoke-virtual {v12}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0f0034

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v12

    .line 233
    new-instance v30, Labt;

    move-object/from16 v0, v30

    move-object/from16 v1, v44

    move-object/from16 v2, v41

    invoke-direct {v0, v1, v2, v11, v12}, Labt;-><init>(Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;I)V

    .line 235
    new-instance v31, Lcom/twitter/android/moments/ui/fullscreen/bi;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    move-object/from16 v0, v31

    move-object/from16 v1, v33

    move-wide/from16 v2, v18

    invoke-direct {v0, v11, v1, v2, v3}, Lcom/twitter/android/moments/ui/fullscreen/bi;-><init>(Landroid/content/Context;Lcom/twitter/android/moments/ui/fullscreen/a;J)V

    .line 237
    new-instance v27, Lcom/twitter/android/moments/ui/fullscreen/bo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    move-object/from16 v28, v0

    const/16 v32, 0x1

    const/16 v33, 0x0

    move-object/from16 v29, p7

    invoke-direct/range {v27 .. v33}, Lcom/twitter/android/moments/ui/fullscreen/bo;-><init>(Landroid/content/Context;Lcom/twitter/library/client/p;Labt;Lcom/twitter/android/moments/ui/fullscreen/ca;ZZ)V

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/fullscreen/u;->l:Lcom/twitter/android/moments/ui/fullscreen/bo;

    .line 240
    new-instance v27, Lcom/twitter/android/moments/ui/fullscreen/cf;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    .line 241
    invoke-static {v11}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->p:Lzn;

    move-object/from16 v31, v0

    move-object/from16 v29, v9

    move-object/from16 v30, p15

    move-object/from16 v32, p30

    invoke-direct/range {v27 .. v32}, Lcom/twitter/android/moments/ui/fullscreen/cf;-><init>(Landroid/view/LayoutInflater;Lrx/c;Lcom/twitter/android/moments/ui/fullscreen/br;Lzn;Lcom/twitter/android/moments/ui/fullscreen/co;)V

    .line 244
    new-instance v9, Lzo;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->p:Lzn;

    move-object/from16 v0, p20

    invoke-direct {v9, v0, v11}, Lzo;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ak;Lzn;)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->q:Lzo;

    .line 247
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->b:Lcom/twitter/android/moments/ui/fullscreen/bs;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->s:Lcom/twitter/model/moments/viewmodels/a;

    invoke-virtual {v9, v11}, Lcom/twitter/android/moments/ui/fullscreen/bs;->a(Lcom/twitter/model/moments/viewmodels/a;)V

    .line 248
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->b:Lcom/twitter/android/moments/ui/fullscreen/bs;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->s:Lcom/twitter/model/moments/viewmodels/a;

    .line 249
    invoke-virtual {v9, v11}, Lcom/twitter/android/moments/ui/fullscreen/bs;->c(Lcom/twitter/model/moments/viewmodels/a;)Lcom/twitter/moments/core/ui/widget/capsule/a;

    move-result-object v30

    .line 250
    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/fullscreen/u;->o:Lcom/twitter/moments/core/ui/widget/capsule/a;

    .line 254
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->d:Landroid/view/View;

    const v11, 0x7f1304f6

    .line 256
    invoke-virtual {v9, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v32

    check-cast v32, Lcom/twitter/ui/widget/DragOnlySeekBar;

    .line 257
    const v9, 0x7f0206f0

    move-object/from16 v0, v42

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v31

    .line 258
    const v9, 0x7f100008

    const/4 v11, 0x1

    const/4 v12, 0x1

    .line 259
    move-object/from16 v0, v42

    invoke-virtual {v0, v9, v11, v12}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v33

    .line 260
    new-instance v28, Lcom/twitter/moments/core/ui/widget/capsule/b;

    .line 261
    invoke-virtual/range {p25 .. p25}, Lcom/twitter/model/moments/viewmodels/a;->f()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v29

    invoke-direct/range {v28 .. v33}, Lcom/twitter/moments/core/ui/widget/capsule/b;-><init>(ILcom/twitter/moments/core/ui/widget/capsule/a;Landroid/graphics/drawable/Drawable;Landroid/widget/SeekBar;F)V

    .line 263
    new-instance v31, Lcom/twitter/android/moments/ui/fullscreen/q;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->l:Lcom/twitter/android/moments/ui/fullscreen/bo;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->j:Lyj;

    move-object/from16 v38, v0

    move-object/from16 v32, p16

    move-object/from16 v33, p18

    move-object/from16 v34, p25

    move-object/from16 v35, v30

    move-object/from16 v36, v26

    invoke-direct/range {v31 .. v38}, Lcom/twitter/android/moments/ui/fullscreen/q;-><init>(Labf;Lcom/twitter/util/p;Lcom/twitter/model/moments/viewmodels/i;Lcom/twitter/moments/core/ui/widget/capsule/a;Lcom/twitter/android/moments/ui/fullscreen/bl;Lcom/twitter/android/moments/ui/fullscreen/bo;Lyj;)V

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/fullscreen/u;->n:Lcom/twitter/android/moments/ui/fullscreen/q;

    .line 267
    new-instance v36, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;

    move-object/from16 v0, v36

    move-object/from16 v1, v43

    move-object/from16 v2, v30

    move-object/from16 v3, p10

    move-object/from16 v4, p20

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;-><init>(Lcom/twitter/android/moments/ui/fullscreen/i;Lcom/twitter/moments/core/ui/widget/capsule/a;Lcom/twitter/android/moments/ui/fullscreen/bd;Lcom/twitter/android/moments/ui/fullscreen/ak;)V

    .line 269
    new-instance v31, Lcom/twitter/android/moments/ui/fullscreen/e;

    move-object/from16 v32, p16

    move-object/from16 v33, p20

    move-object/from16 v34, v43

    move-object/from16 v35, v30

    move-object/from16 v37, p30

    invoke-direct/range {v31 .. v37}, Lcom/twitter/android/moments/ui/fullscreen/e;-><init>(Labf;Lcom/twitter/android/moments/ui/fullscreen/ak;Lcom/twitter/android/moments/ui/fullscreen/i;Lcom/twitter/moments/core/ui/widget/capsule/a;Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;Lcom/twitter/android/moments/ui/fullscreen/co;)V

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/fullscreen/u;->r:Lcom/twitter/android/moments/ui/fullscreen/e;

    .line 273
    new-instance v45, Lcom/twitter/android/moments/ui/fullscreen/ct;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->p:Lzn;

    move-object/from16 v0, v45

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v9}, Lcom/twitter/android/moments/ui/fullscreen/ct;-><init>(Landroid/content/Context;Lzn;)V

    .line 275
    new-instance v9, Lcom/twitter/android/card/c;

    move-object/from16 v0, p1

    invoke-direct {v9, v0}, Lcom/twitter/android/card/c;-><init>(Landroid/app/Activity;)V

    .line 276
    new-instance v53, Lbxo;

    move-object/from16 v0, v53

    move-object/from16 v1, v42

    invoke-direct {v0, v1}, Lbxo;-><init>(Landroid/content/res/Resources;)V

    .line 278
    new-instance v11, Lcom/twitter/android/moments/ui/fullscreen/u$3;

    move-object/from16 v0, p0

    move-wide/from16 v1, v24

    invoke-direct {v11, v0, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/u$3;-><init>(Lcom/twitter/android/moments/ui/fullscreen/u;J)V

    .line 286
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v12, v11}, Lcom/twitter/android/moments/ui/fullscreen/bp;->a(Landroid/content/Context;Lcom/twitter/util/object/d;)Lcom/twitter/android/moments/ui/fullscreen/bp;

    move-result-object v52

    .line 287
    new-instance v11, Lcom/twitter/android/moments/ui/fullscreen/cb;

    move-object/from16 v0, p1

    move-object/from16 v1, p29

    move-object/from16 v2, v53

    invoke-direct {v11, v0, v9, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/cb;-><init>(Landroid/app/Activity;Lcom/twitter/android/card/b;Lyf;Lbxo;)V

    .line 289
    new-instance v54, Lcom/twitter/android/moments/ui/fullscreen/as;

    .line 290
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v9

    new-instance v12, Lcom/twitter/android/moments/ui/maker/an;

    invoke-direct {v12}, Lcom/twitter/android/moments/ui/maker/an;-><init>()V

    move-object/from16 v0, v54

    invoke-direct {v0, v11, v9, v12}, Lcom/twitter/android/moments/ui/fullscreen/as;-><init>(Lcom/twitter/android/moments/ui/fullscreen/cb;Lrx/f;Lcom/twitter/android/moments/ui/maker/an;)V

    .line 291
    new-instance v55, Lcom/twitter/android/moments/data/h;

    new-instance v9, Lbrd;

    invoke-direct {v9}, Lbrd;-><init>()V

    move-object/from16 v0, v55

    invoke-direct {v0, v9}, Lcom/twitter/android/moments/data/h;-><init>(Lbrd;)V

    .line 293
    new-instance v37, Lcom/twitter/android/moments/ui/fullscreen/cv;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    .line 294
    invoke-static {v9}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->s:Lcom/twitter/model/moments/viewmodels/a;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->g:Lcom/twitter/android/moments/data/w;

    move-object/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->p:Lzn;

    move-object/from16 v47, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->n:Lcom/twitter/android/moments/ui/fullscreen/q;

    move-object/from16 v49, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->n:Lcom/twitter/android/moments/ui/fullscreen/q;

    move-object/from16 v50, v0

    move-object/from16 v41, v10

    move-object/from16 v43, p27

    move-object/from16 v44, v26

    move-object/from16 v46, p18

    move-object/from16 v48, v27

    move-object/from16 v51, v36

    move-object/from16 v56, v16

    move-object/from16 v57, v15

    move-object/from16 v58, p30

    invoke-direct/range {v37 .. v58}, Lcom/twitter/android/moments/ui/fullscreen/cv;-><init>(Landroid/app/Activity;Landroid/view/LayoutInflater;Lcom/twitter/model/moments/viewmodels/a;Lcom/twitter/android/moments/ui/fullscreen/bg;Lcom/twitter/android/moments/data/w;Lcom/twitter/android/moments/data/am;Lcom/twitter/android/moments/ui/fullscreen/bl;Lcom/twitter/android/moments/ui/fullscreen/cs;Lcom/twitter/util/p;Lzn;Lcom/twitter/android/moments/ui/fullscreen/cf;Lcom/twitter/android/moments/ui/fullscreen/m;Lcom/twitter/android/moments/ui/fullscreen/cp;Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;Lcom/twitter/android/moments/ui/fullscreen/bp;Lbxo;Lcom/twitter/android/moments/ui/fullscreen/as;Lcom/twitter/android/moments/data/h;Lrx/g;Lcom/twitter/android/moments/data/n;Lcom/twitter/android/moments/ui/fullscreen/co;)V

    .line 301
    new-instance v56, Lcom/twitter/android/moments/ui/fullscreen/w;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    move-object/from16 v57, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->p:Lzn;

    move-object/from16 v65, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->n:Lcom/twitter/android/moments/ui/fullscreen/q;

    move-object/from16 v67, v0

    move-object/from16 v58, p18

    move-object/from16 v59, v37

    move-object/from16 v60, p10

    move-object/from16 v61, v36

    move-object/from16 v62, p20

    move-object/from16 v63, p24

    move-object/from16 v64, p21

    move-object/from16 v66, p19

    move-object/from16 v68, v45

    move-object/from16 v69, v52

    move-object/from16 v70, v53

    move-object/from16 v71, v54

    move-object/from16 v72, p31

    invoke-direct/range {v56 .. v72}, Lcom/twitter/android/moments/ui/fullscreen/w;-><init>(Landroid/app/Activity;Lcom/twitter/util/p;Lcom/twitter/android/moments/ui/fullscreen/cv;Lcom/twitter/android/moments/ui/fullscreen/bd;Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;Lcom/twitter/android/moments/ui/fullscreen/ak;Lcom/twitter/app/common/util/j;Lcom/twitter/android/moments/ui/fullscreen/bz;Lzn;Lcom/twitter/android/moments/ui/fullscreen/ad;Lcom/twitter/android/moments/ui/fullscreen/m;Lcom/twitter/android/moments/ui/fullscreen/cs;Lcom/twitter/android/moments/ui/fullscreen/bp;Lbxo;Lcom/twitter/android/moments/ui/fullscreen/as;Lcom/twitter/android/av/k;)V

    .line 308
    move-object/from16 v0, v56

    move-object/from16 v1, p25

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/w;->a(Lcom/twitter/model/moments/viewmodels/a;)Ljava/util/List;

    move-result-object v9

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->m:Ljava/util/List;

    .line 309
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->f:Lcom/twitter/moments/core/ui/widget/sectionpager/e;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->m:Ljava/util/List;

    invoke-virtual {v9, v10}, Lcom/twitter/moments/core/ui/widget/sectionpager/e;->a(Ljava/util/List;)V

    .line 311
    new-instance v52, Lcom/twitter/android/moments/ui/fullscreen/ao;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    move-object/from16 v53, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->p:Lzn;

    move-object/from16 v57, v0

    move-object/from16 v54, v30

    move-object/from16 v56, p16

    invoke-direct/range {v52 .. v57}, Lcom/twitter/android/moments/ui/fullscreen/ao;-><init>(Landroid/content/Context;Lcom/twitter/moments/core/ui/widget/capsule/a;Lcom/twitter/android/moments/data/h;Lcom/twitter/android/moments/ui/fullscreen/ap;Lcom/twitter/android/moments/ui/fullscreen/s;)V

    .line 315
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move-object/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/u;->a(Lcom/twitter/moments/core/ui/widget/capsule/a;Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 316
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move-object/from16 v2, v36

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/u;->a(Lcom/twitter/moments/core/ui/widget/capsule/a;Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 317
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1, v6}, Lcom/twitter/android/moments/ui/fullscreen/u;->a(Lcom/twitter/moments/core/ui/widget/capsule/a;Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 318
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->p:Lzn;

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1, v6}, Lcom/twitter/android/moments/ui/fullscreen/u;->a(Lcom/twitter/moments/core/ui/widget/capsule/a;Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 319
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->n:Lcom/twitter/android/moments/ui/fullscreen/q;

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1, v6}, Lcom/twitter/android/moments/ui/fullscreen/u;->a(Lcom/twitter/moments/core/ui/widget/capsule/a;Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 320
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->r:Lcom/twitter/android/moments/ui/fullscreen/e;

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1, v6}, Lcom/twitter/android/moments/ui/fullscreen/u;->a(Lcom/twitter/moments/core/ui/widget/capsule/a;Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 321
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move-object/from16 v2, v73

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/u;->a(Lcom/twitter/moments/core/ui/widget/capsule/a;Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 322
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1, v7}, Lcom/twitter/android/moments/ui/fullscreen/u;->a(Lcom/twitter/moments/core/ui/widget/capsule/a;Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 323
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->t:Lcom/twitter/android/moments/ui/fullscreen/aw;

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1, v6}, Lcom/twitter/android/moments/ui/fullscreen/u;->a(Lcom/twitter/moments/core/ui/widget/capsule/a;Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 324
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1, v8}, Lcom/twitter/android/moments/ui/fullscreen/u;->a(Lcom/twitter/moments/core/ui/widget/capsule/a;Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 325
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move-object/from16 v2, v52

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/u;->a(Lcom/twitter/moments/core/ui/widget/capsule/a;Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 327
    new-instance v14, Lbsa;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    move-wide/from16 v0, v24

    invoke-direct {v14, v6, v0, v1}, Lbsa;-><init>(Landroid/content/Context;J)V

    .line 330
    new-instance v6, Lcom/twitter/android/moments/data/z;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    .line 331
    invoke-virtual/range {p25 .. p25}, Lcom/twitter/model/moments/viewmodels/a;->a()Lcom/twitter/model/moments/Moment;

    move-result-object v8

    iget-wide v10, v8, Lcom/twitter/model/moments/Moment;->b:J

    const-class v8, Lcom/twitter/android/moments/data/z;

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    move-object/from16 v0, p9

    invoke-virtual {v0, v8, v9}, Lcom/twitter/android/moments/data/i;->a(Ljava/lang/Class;Ljava/lang/Object;)I

    move-result v12

    move-object/from16 v8, p4

    move-object/from16 v9, p8

    move-object/from16 v13, p7

    invoke-direct/range {v6 .. v14}, Lcom/twitter/android/moments/data/z;-><init>(Landroid/content/Context;Lbsb;Landroid/support/v4/app/LoaderManager;JILcom/twitter/library/client/p;Lbsa;)V

    .line 333
    new-instance v7, Lcom/twitter/android/moments/data/y;

    move-object/from16 v0, p6

    invoke-direct {v7, v0, v6}, Lcom/twitter/android/moments/data/y;-><init>(Lcom/twitter/library/client/v;Lcom/twitter/android/moments/data/z;)V

    .line 335
    invoke-virtual/range {p25 .. p25}, Lcom/twitter/model/moments/viewmodels/a;->f()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v7, v6}, Lcom/twitter/android/moments/data/y;->a(Ljava/util/List;)V

    .line 336
    invoke-virtual/range {p5 .. p5}, Lcom/twitter/util/collection/k;->c()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 337
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual/range {p5 .. p5}, Lcom/twitter/util/collection/k;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/4 v8, 0x0

    invoke-virtual {v7, v6, v8}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 340
    :cond_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/moments/ui/fullscreen/u;->c:Landroid/support/v4/view/ViewPager;

    new-instance v7, Lcom/twitter/android/moments/ui/fullscreen/u$4;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Lcom/twitter/android/moments/ui/fullscreen/u$4;-><init>(Lcom/twitter/android/moments/ui/fullscreen/u;)V

    invoke-virtual {v6, v7}, Landroid/support/v4/view/ViewPager;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 350
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/u;)Landroid/support/v4/app/FragmentActivity;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->a:Landroid/support/v4/app/FragmentActivity;

    return-object v0
.end method

.method private a(Lcom/twitter/moments/core/ui/widget/capsule/a;Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V
    .locals 1

    .prologue
    .line 358
    invoke-interface {p1, p2}, Lcom/twitter/moments/core/ui/widget/capsule/a;->a(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 359
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->e:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 360
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/fullscreen/u;)Lcom/twitter/android/moments/ui/fullscreen/q;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->n:Lcom/twitter/android/moments/ui/fullscreen/q;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/fullscreen/u;)Lcom/twitter/android/moments/ui/fullscreen/u$a;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->v:Lcom/twitter/android/moments/ui/fullscreen/u$a;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/moments/ui/fullscreen/u;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->c:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->p:Lzn;

    invoke-virtual {v0, p1}, Lzn;->a(Landroid/os/Bundle;)V

    .line 368
    return-void
.end method

.method public a(Lcom/twitter/android/moments/ui/fullscreen/u$a;)V
    .locals 0

    .prologue
    .line 353
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->v:Lcom/twitter/android/moments/ui/fullscreen/u$a;

    .line 354
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->l:Lcom/twitter/android/moments/ui/fullscreen/bo;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/bo;->b()Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 371
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 372
    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->o:Lcom/twitter/moments/core/ui/widget/capsule/a;

    invoke-interface {v2, v0}, Lcom/twitter/moments/core/ui/widget/capsule/a;->b(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    goto :goto_0

    .line 374
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->r:Lcom/twitter/android/moments/ui/fullscreen/e;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/e;->a()V

    .line 375
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->n:Lcom/twitter/android/moments/ui/fullscreen/q;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/q;->a()V

    .line 376
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 377
    if-eqz v0, :cond_1

    .line 378
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 380
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->b:Lcom/twitter/android/moments/ui/fullscreen/bs;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->s:Lcom/twitter/model/moments/viewmodels/a;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/bs;->b(Lcom/twitter/model/moments/viewmodels/a;)V

    .line 381
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->f:Lcom/twitter/moments/core/ui/widget/sectionpager/e;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->m:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/twitter/moments/core/ui/widget/sectionpager/e;->b(Ljava/util/List;)V

    .line 382
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->u:Lcom/twitter/android/moments/ui/fullscreen/ce;

    if-eqz v0, :cond_2

    .line 383
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->u:Lcom/twitter/android/moments/ui/fullscreen/ce;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ce;->b()V

    .line 385
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->i:Lxv;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 386
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->k:Lcom/twitter/android/moments/data/s;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 387
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->g:Lcom/twitter/android/moments/data/w;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 388
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->h:Lcom/twitter/android/moments/data/ae;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 389
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->t:Lcom/twitter/android/moments/ui/fullscreen/aw;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/aw;->a()V

    .line 390
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->q:Lzo;

    invoke-virtual {v0}, Lzo;->a()V

    .line 391
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->p:Lzn;

    invoke-virtual {v0}, Lzn;->h()V

    .line 392
    return-void
.end method

.method public c()Lcom/twitter/model/moments/viewmodels/a;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/u;->s:Lcom/twitter/model/moments/viewmodels/a;

    return-object v0
.end method
