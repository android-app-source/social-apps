.class Lcom/twitter/android/moments/ui/fullscreen/ba$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/fullscreen/ba;->a(Lcom/twitter/model/moments/o;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/model/moments/Moment;

.field final synthetic b:Lcom/twitter/model/moments/o;

.field final synthetic c:Lcom/twitter/android/moments/ui/fullscreen/bb;

.field final synthetic d:Lcom/twitter/android/moments/ui/fullscreen/ba;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/ba;Lcom/twitter/model/moments/Moment;Lcom/twitter/model/moments/o;Lcom/twitter/android/moments/ui/fullscreen/bb;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/ba$1;->d:Lcom/twitter/android/moments/ui/fullscreen/ba;

    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/ba$1;->a:Lcom/twitter/model/moments/Moment;

    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/ba$1;->b:Lcom/twitter/model/moments/o;

    iput-object p4, p0, Lcom/twitter/android/moments/ui/fullscreen/ba$1;->c:Lcom/twitter/android/moments/ui/fullscreen/bb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba$1;->d:Lcom/twitter/android/moments/ui/fullscreen/ba;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/ba;->a(Lcom/twitter/android/moments/ui/fullscreen/ba;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba$1;->d:Lcom/twitter/android/moments/ui/fullscreen/ba;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/ba;->b(Lcom/twitter/android/moments/ui/fullscreen/ba;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ba$1;->d:Lcom/twitter/android/moments/ui/fullscreen/ba;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/ba$1;->a:Lcom/twitter/model/moments/Moment;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/ba$1;->b:Lcom/twitter/model/moments/o;

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba$1;->d:Lcom/twitter/android/moments/ui/fullscreen/ba;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/ba;->c(Lcom/twitter/android/moments/ui/fullscreen/ba;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ba$1;->c:Lcom/twitter/android/moments/ui/fullscreen/bb;

    .line 67
    invoke-virtual {v4}, Lcom/twitter/android/moments/ui/fullscreen/bb;->b()Lcom/twitter/media/ui/image/MediaImageView;

    move-result-object v4

    .line 66
    invoke-static {v1, v2, v3, v0, v4}, Lcom/twitter/android/moments/ui/fullscreen/ba;->a(Lcom/twitter/android/moments/ui/fullscreen/ba;Lcom/twitter/model/moments/Moment;Lcom/twitter/model/moments/o;Ljava/lang/String;Lcom/twitter/media/ui/image/MediaImageView;)V

    .line 74
    :goto_0
    return-void

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba$1;->d:Lcom/twitter/android/moments/ui/fullscreen/ba;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ba$1;->b:Lcom/twitter/model/moments/o;

    invoke-static {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/ba;->a(Lcom/twitter/android/moments/ui/fullscreen/ba;Lcom/twitter/model/moments/o;)V

    goto :goto_0

    .line 72
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ba$1;->c:Lcom/twitter/android/moments/ui/fullscreen/bb;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/bb;->d()V

    goto :goto_0
.end method
