.class Lcom/twitter/android/moments/ui/fullscreen/av;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/fullscreen/cu;
.implements Lcom/twitter/moments/core/ui/widget/sectionpager/d;


# instance fields
.field private final b:Lcom/twitter/model/moments/viewmodels/n;

.field private final c:Lcom/twitter/util/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/twitter/android/moments/ui/fullscreen/m;

.field private final e:Lrx/f;

.field private final f:Labq;

.field private final g:Labr;

.field private final h:Lcom/twitter/util/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/twitter/util/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/twitter/android/moments/ui/fullscreen/dd;


# direct methods
.method constructor <init>(Lcom/twitter/model/moments/viewmodels/n;Lcom/twitter/util/p;Lcom/twitter/android/moments/ui/fullscreen/m;Lrx/f;Labq;Labr;Lcom/twitter/util/q;Lcom/twitter/android/moments/ui/fullscreen/dd;)V
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/viewmodels/n;",
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;",
            "Lcom/twitter/android/moments/ui/fullscreen/m;",
            "Lrx/f;",
            "Labq;",
            "Labr;",
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;",
            "Lcom/twitter/android/moments/ui/fullscreen/dd;",
            ")V"
        }
    .end annotation

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/av$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/fullscreen/av$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/av;)V

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->i:Lcom/twitter/util/q;

    .line 85
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->b:Lcom/twitter/model/moments/viewmodels/n;

    .line 86
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->c:Lcom/twitter/util/p;

    .line 87
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->d:Lcom/twitter/android/moments/ui/fullscreen/m;

    .line 88
    iput-object p4, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->e:Lrx/f;

    .line 89
    iput-object p6, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->g:Labr;

    .line 90
    iput-object p5, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->f:Labq;

    .line 91
    iput-object p7, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->h:Lcom/twitter/util/q;

    .line 92
    iput-object p8, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->j:Lcom/twitter/android/moments/ui/fullscreen/dd;

    .line 94
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->g:Labr;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->b:Lcom/twitter/model/moments/viewmodels/n;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->b:Lcom/twitter/model/moments/viewmodels/n;

    invoke-virtual {v2}, Lcom/twitter/model/moments/viewmodels/n;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Labr;->a(Lcom/twitter/model/moments/viewmodels/n;Lcom/twitter/model/core/Tweet;)V

    .line 96
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->c:Lcom/twitter/util/p;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->h:Lcom/twitter/util/q;

    invoke-virtual {v0, v1}, Lcom/twitter/util/p;->a(Lcom/twitter/util/q;)Z

    .line 97
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->c:Lcom/twitter/util/p;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->i:Lcom/twitter/util/q;

    invoke-virtual {v0, v1}, Lcom/twitter/util/p;->a(Lcom/twitter/util/q;)Z

    .line 99
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->f:Labq;

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/av$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/fullscreen/av$2;-><init>(Lcom/twitter/android/moments/ui/fullscreen/av;)V

    invoke-virtual {v0, v1}, Labq;->a(Landroid/view/View$OnClickListener;)V

    .line 105
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/av;)Labq;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->f:Labq;

    return-object v0
.end method

.method public static a(Landroid/app/Activity;Landroid/view/LayoutInflater;Lcom/twitter/model/moments/viewmodels/n;Lcom/twitter/android/moments/ui/fullscreen/bl;Lcom/twitter/android/moments/ui/fullscreen/cs;Lcom/twitter/util/p;Lcom/twitter/android/moments/ui/fullscreen/bp;Lbxo;Lcom/twitter/android/moments/ui/fullscreen/m;Lcom/twitter/android/moments/ui/fullscreen/as;)Lcom/twitter/android/moments/ui/fullscreen/av;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/view/LayoutInflater;",
            "Lcom/twitter/model/moments/viewmodels/n;",
            "Lcom/twitter/android/moments/ui/fullscreen/bl;",
            "Lcom/twitter/android/moments/ui/fullscreen/cs;",
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;",
            "Lcom/twitter/android/moments/ui/fullscreen/bp;",
            "Lbxo;",
            "Lcom/twitter/android/moments/ui/fullscreen/m;",
            "Lcom/twitter/android/moments/ui/fullscreen/as;",
            ")",
            "Lcom/twitter/android/moments/ui/fullscreen/av;"
        }
    .end annotation

    .prologue
    .line 59
    .line 60
    invoke-static {p1}, Labq;->a(Landroid/view/LayoutInflater;)Labq;

    move-result-object v1

    .line 61
    invoke-virtual {p2}, Lcom/twitter/model/moments/viewmodels/n;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/twitter/model/core/Tweet;

    .line 62
    new-instance v0, Labr;

    .line 64
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    move-object/from16 v2, p6

    move-object/from16 v3, p7

    move-object v4, p4

    move-object/from16 v6, p9

    invoke-direct/range {v0 .. v6}, Labr;-><init>(Labq;Lcom/twitter/android/moments/ui/fullscreen/bp;Lbxo;Lcom/twitter/android/moments/ui/fullscreen/cs;Landroid/content/res/Resources;Lcom/twitter/android/moments/ui/fullscreen/as;)V

    .line 67
    invoke-virtual {v1}, Labq;->d()Landroid/widget/FrameLayout;

    move-result-object v2

    .line 68
    invoke-virtual {v1}, Labq;->f()Lbpl;

    move-result-object v3

    .line 66
    invoke-static {p0, v2, v7, p3, v3}, Lcom/twitter/android/moments/ui/fullscreen/dd;->a(Landroid/content/Context;Landroid/widget/FrameLayout;Lcom/twitter/model/core/Tweet;Lcom/twitter/android/moments/ui/fullscreen/bl;Lbpl;)Lcom/twitter/android/moments/ui/fullscreen/dd;

    move-result-object v10

    .line 69
    new-instance v9, Lcom/twitter/android/moments/ui/fullscreen/aa;

    .line 70
    invoke-virtual {v1}, Labq;->b()Lcom/twitter/android/moments/ui/fullscreen/z;

    move-result-object v2

    invoke-direct {v9, v2}, Lcom/twitter/android/moments/ui/fullscreen/aa;-><init>(Lcom/twitter/android/moments/ui/fullscreen/z;)V

    .line 71
    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/av;

    .line 72
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v6

    move-object v3, p2

    move-object/from16 v4, p5

    move-object/from16 v5, p8

    move-object v7, v1

    move-object v8, v0

    invoke-direct/range {v2 .. v10}, Lcom/twitter/android/moments/ui/fullscreen/av;-><init>(Lcom/twitter/model/moments/viewmodels/n;Lcom/twitter/util/p;Lcom/twitter/android/moments/ui/fullscreen/m;Lrx/f;Labq;Labr;Lcom/twitter/util/q;Lcom/twitter/android/moments/ui/fullscreen/dd;)V

    .line 71
    return-object v2
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/fullscreen/av;)Lcom/twitter/android/moments/ui/fullscreen/m;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->d:Lcom/twitter/android/moments/ui/fullscreen/m;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->c:Lcom/twitter/util/p;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->h:Lcom/twitter/util/q;

    invoke-virtual {v0, v1}, Lcom/twitter/util/p;->b(Lcom/twitter/util/q;)Z

    .line 129
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->c:Lcom/twitter/util/p;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->i:Lcom/twitter/util/q;

    invoke-virtual {v0, v1}, Lcom/twitter/util/p;->b(Lcom/twitter/util/q;)Z

    .line 130
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->j:Lcom/twitter/android/moments/ui/fullscreen/dd;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/dd;->a()V

    .line 131
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->g:Labr;

    invoke-virtual {v0}, Labr;->c()V

    .line 132
    return-void
.end method

.method public a(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    .line 141
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->f:Labq;

    invoke-virtual {v0, p1}, Labq;->a(I)V

    .line 142
    return-void
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->f:Labq;

    invoke-virtual {v0}, Labq;->d()Landroid/widget/FrameLayout;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 120
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 124
    return-void
.end method

.method public e()Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->f:Labq;

    invoke-virtual {v0}, Labq;->e()Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->e:Lrx/f;

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public f()Labq;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/av;->f:Labq;

    return-object v0
.end method
