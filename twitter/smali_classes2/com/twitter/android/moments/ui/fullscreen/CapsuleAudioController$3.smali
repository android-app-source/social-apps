.class Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$3;
.super Lcom/twitter/library/av/l;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;->c(Lcom/twitter/model/moments/viewmodels/p;)Lcom/twitter/android/moments/ui/video/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/ui/video/a;

.field final synthetic b:Lcom/twitter/model/moments/viewmodels/p;

.field final synthetic c:Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;Lcom/twitter/android/moments/ui/video/a;Lcom/twitter/model/moments/viewmodels/p;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$3;->c:Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;

    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$3;->a:Lcom/twitter/android/moments/ui/video/a;

    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$3;->b:Lcom/twitter/model/moments/viewmodels/p;

    invoke-direct {p0}, Lcom/twitter/library/av/l;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 131
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$AudioFailInfo;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$3;->b:Lcom/twitter/model/moments/viewmodels/p;

    sget-object v2, Lcom/twitter/android/av/audio/AudioCardError;->h:Lcom/twitter/android/av/audio/AudioCardError;

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$AudioFailInfo;-><init>(Lcom/twitter/model/moments/viewmodels/p;Lcom/twitter/android/av/audio/AudioCardError;)V

    .line 132
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$3;->c:Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;->e(Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;)Lcom/twitter/util/p;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/util/p;->a(Ljava/lang/Object;)V

    .line 133
    return-void
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V
    .locals 4

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$3;->c:Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;->d(Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;)Lcom/twitter/util/p;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$AudioStartInfo;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$3;->a:Lcom/twitter/android/moments/ui/video/a;

    .line 126
    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/video/a;->f()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/av/playback/AVPlayer;->F()Lcom/twitter/model/av/AVMedia;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$3;->b:Lcom/twitter/model/moments/viewmodels/p;

    invoke-direct {v1, v2, v3}, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$AudioStartInfo;-><init>(Lcom/twitter/model/av/AVMedia;Lcom/twitter/model/moments/viewmodels/p;)V

    .line 125
    invoke-virtual {v0, v1}, Lcom/twitter/util/p;->a(Ljava/lang/Object;)V

    .line 127
    return-void
.end method

.method public a(Lcom/twitter/model/av/c;)V
    .locals 3

    .prologue
    .line 137
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$AudioFailInfo;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$3;->b:Lcom/twitter/model/moments/viewmodels/p;

    .line 138
    invoke-static {p1}, Lcom/twitter/android/av/audio/AudioCardError;->a(Lcom/twitter/model/av/c;)Lcom/twitter/android/av/audio/AudioCardError;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$AudioFailInfo;-><init>(Lcom/twitter/model/moments/viewmodels/p;Lcom/twitter/android/av/audio/AudioCardError;)V

    .line 139
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$3;->c:Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;->e(Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController;)Lcom/twitter/util/p;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/util/p;->a(Ljava/lang/Object;)V

    .line 140
    return-void
.end method
