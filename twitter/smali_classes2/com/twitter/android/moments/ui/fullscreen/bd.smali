.class public Lcom/twitter/android/moments/ui/fullscreen/bd;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/android/moments/ui/video/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/android/moments/ui/video/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/android/moments/ui/video/a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/content/Context;

.field private final e:Lcom/twitter/library/av/playback/q;

.field private final f:Lcom/twitter/android/moments/ui/fullscreen/da;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/av/playback/q;)V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/da;

    invoke-direct {v0}, Lcom/twitter/android/moments/ui/fullscreen/da;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/moments/ui/fullscreen/bd;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/q;Lcom/twitter/android/moments/ui/fullscreen/da;)V

    .line 29
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/twitter/library/av/playback/q;Lcom/twitter/android/moments/ui/fullscreen/da;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->f:Lcom/twitter/android/moments/ui/fullscreen/da;

    .line 35
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->b:Ljava/util/Set;

    .line 36
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->c:Ljava/util/Set;

    .line 37
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->a:Ljava/util/Set;

    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->d:Landroid/content/Context;

    .line 39
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->e:Lcom/twitter/library/av/playback/q;

    .line 40
    return-void
.end method

.method private c(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/moments/ui/video/a;
    .locals 4

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->f:Lcom/twitter/android/moments/ui/fullscreen/da;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->e:Lcom/twitter/library/av/playback/q;

    sget-object v3, Lbyo;->d:Lbyf;

    .line 54
    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/twitter/android/moments/ui/fullscreen/da;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/av/playback/q;Lbyf;)Lcom/twitter/android/moments/ui/video/a;

    move-result-object v0

    .line 56
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->c:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 57
    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/moments/ui/video/a;
    .locals 4

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->f:Lcom/twitter/android/moments/ui/fullscreen/da;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->e:Lcom/twitter/library/av/playback/q;

    sget-object v3, Lbyo;->d:Lbyf;

    .line 45
    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/twitter/android/moments/ui/fullscreen/da;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/av/playback/q;Lbyf;)Lcom/twitter/android/moments/ui/video/a;

    move-result-object v0

    .line 47
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 48
    return-object v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 114
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 115
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/video/a;

    .line 116
    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/video/a;->f()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/av/playback/AVPlayer;->v()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 117
    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/video/a;->e()V

    .line 118
    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->a:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 121
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/video/a;

    .line 122
    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/video/a;->f()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/av/playback/AVPlayer;->v()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 123
    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/video/a;->e()V

    .line 124
    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->a:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 127
    :cond_3
    return-void
.end method

.method public a(Lcom/twitter/android/moments/ui/video/a;)V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/moments/ui/fullscreen/bd;->a(Lcom/twitter/android/moments/ui/video/a;Z)V

    .line 72
    return-void
.end method

.method public a(Lcom/twitter/android/moments/ui/video/a;Z)V
    .locals 2

    .prologue
    .line 83
    invoke-virtual {p1}, Lcom/twitter/android/moments/ui/video/a;->e()V

    .line 84
    invoke-virtual {p1}, Lcom/twitter/android/moments/ui/video/a;->g()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->e:Lcom/twitter/library/av/playback/q;

    invoke-virtual {v1, v0}, Lcom/twitter/library/av/playback/q;->a(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V

    .line 86
    if-eqz p2, :cond_0

    .line 87
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->e:Lcom/twitter/library/av/playback/q;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->h()Lcom/twitter/library/av/playback/u;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/av/playback/q;->b(Lcom/twitter/library/av/playback/u;)V

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 90
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 91
    return-void
.end method

.method public a(Z)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 100
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->b:Ljava/util/Set;

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/video/a;

    .line 101
    if-nez p1, :cond_0

    move v1, v2

    :goto_1
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/bd;->a(Lcom/twitter/android/moments/ui/video/a;Z)V

    goto :goto_0

    :cond_0
    move v1, v3

    goto :goto_1

    .line 103
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->c:Ljava/util/Set;

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/video/a;

    .line 104
    if-nez p1, :cond_2

    move v1, v2

    :goto_3
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/bd;->a(Lcom/twitter/android/moments/ui/video/a;Z)V

    goto :goto_2

    :cond_2
    move v1, v3

    goto :goto_3

    .line 106
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 107
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 108
    return-void
.end method

.method public b(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/moments/ui/video/a;
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/bd;->c(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/moments/ui/video/a;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/video/a;->a()V

    .line 64
    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 149
    invoke-static {}, Lcob;->a()Lcob;

    move-result-object v0

    invoke-virtual {v0}, Lcob;->b()I

    move-result v0

    const/16 v1, 0x7dd

    if-gt v0, v1, :cond_0

    .line 150
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->e:Lcom/twitter/library/av/playback/q;

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/q;->c()V

    .line 152
    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/video/a;

    .line 134
    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/video/a;->a(Z)V

    goto :goto_0

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bd;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 137
    return-void
.end method
