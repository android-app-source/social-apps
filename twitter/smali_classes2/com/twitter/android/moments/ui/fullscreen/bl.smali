.class public Lcom/twitter/android/moments/ui/fullscreen/bl;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/fullscreen/bl$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcnz;

.field private final c:Lcom/twitter/android/dialog/e;

.field private final d:Lcom/twitter/android/moments/ui/fullscreen/bk;

.field private final e:Lcom/twitter/android/ck;

.field private final f:Lcom/twitter/util/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lzn;

.field private final h:Lcom/twitter/model/util/FriendshipCache;

.field private final i:Lcom/twitter/android/moments/ui/fullscreen/a;

.field private final j:Lcom/twitter/android/moments/ui/fullscreen/r;

.field private final k:Lcom/twitter/android/moments/ui/b;

.field private final l:Lcom/twitter/util/android/g;

.field private final m:J

.field private n:Landroid/app/Dialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcnz;Lcom/twitter/android/dialog/e;Lcom/twitter/android/moments/ui/fullscreen/bk;Lcom/twitter/android/ck;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/android/moments/ui/fullscreen/a;Lzn;Lcom/twitter/android/moments/ui/b;Lcom/twitter/android/moments/ui/fullscreen/r;Lcom/twitter/app/common/util/j;JLcom/twitter/util/android/g;)V
    .locals 6

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->a:Landroid/content/Context;

    .line 66
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->b:Lcnz;

    .line 67
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->c:Lcom/twitter/android/dialog/e;

    .line 68
    iput-object p4, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->d:Lcom/twitter/android/moments/ui/fullscreen/bk;

    .line 69
    iput-object p5, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->e:Lcom/twitter/android/ck;

    .line 70
    iput-object p6, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->h:Lcom/twitter/model/util/FriendshipCache;

    .line 71
    new-instance v2, Lcom/twitter/util/p;

    invoke-direct {v2}, Lcom/twitter/util/p;-><init>()V

    iput-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->f:Lcom/twitter/util/p;

    .line 72
    iput-object p8, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->g:Lzn;

    .line 73
    iput-object p7, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->i:Lcom/twitter/android/moments/ui/fullscreen/a;

    .line 74
    iput-object p9, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->k:Lcom/twitter/android/moments/ui/b;

    .line 75
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->j:Lcom/twitter/android/moments/ui/fullscreen/r;

    .line 76
    move-wide/from16 v0, p12

    iput-wide v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->m:J

    .line 77
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->l:Lcom/twitter/util/android/g;

    .line 78
    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/bl$1;

    invoke-direct {v2, p0}, Lcom/twitter/android/moments/ui/fullscreen/bl$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/bl;)V

    move-object/from16 v0, p11

    invoke-interface {v0, v2}, Lcom/twitter/app/common/util/j;->a(Lcom/twitter/app/common/util/b$a;)V

    .line 84
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/bl;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/bl;->b()V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/fullscreen/bl;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->a:Landroid/content/Context;

    return-object v0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->n:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->n:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 186
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->n:Landroid/app/Dialog;

    .line 188
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/fullscreen/bl;)Lcom/twitter/util/android/g;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->l:Lcom/twitter/util/android/g;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/moments/ui/fullscreen/bl;)Lcom/twitter/android/moments/ui/fullscreen/r;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->j:Lcom/twitter/android/moments/ui/fullscreen/r;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/moments/ui/fullscreen/bl;)J
    .locals 2

    .prologue
    .line 36
    iget-wide v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->m:J

    return-wide v0
.end method


# virtual methods
.method public a()Lcom/twitter/util/p;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->f:Lcom/twitter/util/p;

    return-object v0
.end method

.method public a(Lcom/twitter/model/core/Tweet;)V
    .locals 4

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->i:Lcom/twitter/android/moments/ui/fullscreen/a;

    iget-wide v2, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->m:J

    invoke-virtual {v0, p1, v2, v3}, Lcom/twitter/android/moments/ui/fullscreen/a;->a(Lcom/twitter/model/core/Tweet;J)V

    .line 93
    return-void
.end method

.method public a(Lcom/twitter/model/core/TweetActionType;Lcom/twitter/model/core/Tweet;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 108
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->b:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v0, :cond_0

    .line 110
    iget-boolean v0, p2, Lcom/twitter/model/core/Tweet;->a:Z

    if-eqz v0, :cond_3

    .line 111
    const/4 v0, 0x3

    .line 115
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->g:Lzn;

    invoke-virtual {v1, v0}, Lzn;->a(I)V

    .line 118
    :cond_0
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->c:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v0, :cond_1

    .line 120
    iget-boolean v0, p2, Lcom/twitter/model/core/Tweet;->c:Z

    if-eqz v0, :cond_4

    .line 121
    const/4 v0, 0x5

    .line 125
    :goto_1
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->g:Lzn;

    invoke-virtual {v1, v0}, Lzn;->a(I)V

    .line 128
    :cond_1
    sget-object v0, Lcom/twitter/model/core/TweetActionType;->n:Lcom/twitter/model/core/TweetActionType;

    if-ne p1, v0, :cond_2

    .line 129
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->g:Lzn;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lzn;->a(I)V

    .line 134
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->e:Lcom/twitter/android/ck;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->h:Lcom/twitter/model/util/FriendshipCache;

    new-instance v5, Lcom/twitter/android/moments/ui/fullscreen/bl$a;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->f:Lcom/twitter/util/p;

    invoke-direct {v5, p0, p2, v1}, Lcom/twitter/android/moments/ui/fullscreen/bl$a;-><init>(Lcom/twitter/android/moments/ui/fullscreen/bl;Lcom/twitter/model/core/Tweet;Lcom/twitter/util/p;)V

    move-object v1, p1

    move-object v2, p2

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/TweetActionType;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/library/widget/h;Lcom/twitter/android/timeline/bk;Ljava/lang/String;)V

    .line 136
    return-void

    .line 113
    :cond_3
    const/4 v0, 0x2

    goto :goto_0

    .line 123
    :cond_4
    const/4 v0, 0x4

    goto :goto_1
.end method

.method public a(Lcom/twitter/model/moments/Moment;Lcom/twitter/model/core/Tweet;)V
    .locals 2

    .prologue
    .line 96
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->i:Lcom/twitter/android/moments/ui/fullscreen/a;

    iget-object v0, p1, Lcom/twitter/model/moments/Moment;->o:Lcom/twitter/model/moments/a;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/a;

    invoke-virtual {v1, p1, p2, v0}, Lcom/twitter/android/moments/ui/fullscreen/a;->a(Lcom/twitter/model/moments/Moment;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/moments/a;)V

    .line 97
    return-void
.end method

.method public a(Lcom/twitter/model/moments/Moment;Lcom/twitter/model/moments/a;)V
    .locals 5

    .prologue
    .line 170
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->k:Lcom/twitter/android/moments/ui/b;

    iget-wide v2, p2, Lcom/twitter/model/moments/a;->b:J

    iget-object v0, p2, Lcom/twitter/model/moments/a;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p1, Lcom/twitter/model/moments/Moment;->p:Lcgi;

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/twitter/android/moments/ui/b;->a(JLjava/lang/String;Lcgi;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/bl$2;

    invoke-direct {v1, p0, p2}, Lcom/twitter/android/moments/ui/fullscreen/bl$2;-><init>(Lcom/twitter/android/moments/ui/fullscreen/bl;Lcom/twitter/model/moments/a;)V

    .line 171
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/functions/b;)Lrx/j;

    .line 181
    return-void
.end method

.method public a(Lcom/twitter/model/moments/Moment;Lcom/twitter/model/moments/a;Lcom/twitter/model/core/Tweet;)V
    .locals 4

    .prologue
    .line 156
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/bl;->b()V

    .line 157
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 158
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->b:Lcnz;

    iget-wide v2, p2, Lcom/twitter/model/moments/a;->b:J

    invoke-virtual {v1, v2, v3}, Lcnz;->a(J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 159
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->d:Lcom/twitter/android/moments/ui/fullscreen/bk;

    invoke-virtual {v1, p1, p2, p0}, Lcom/twitter/android/moments/ui/fullscreen/bk;->a(Lcom/twitter/model/moments/Moment;Lcom/twitter/model/moments/a;Lcom/twitter/android/moments/ui/fullscreen/bl;)Lcom/twitter/android/dialog/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 161
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->d:Lcom/twitter/android/moments/ui/fullscreen/bk;

    invoke-virtual {v1, p0, p1, p3}, Lcom/twitter/android/moments/ui/fullscreen/bk;->a(Lcom/twitter/android/moments/ui/fullscreen/bl;Lcom/twitter/model/moments/Moment;Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/dialog/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 164
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->h()Z

    move-result v1

    if-nez v1, :cond_1

    .line 165
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->c:Lcom/twitter/android/dialog/e;

    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/twitter/android/dialog/e;->a(Ljava/util/List;)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->n:Landroid/app/Dialog;

    .line 167
    :cond_1
    return-void
.end method

.method public b(Lcom/twitter/model/core/Tweet;)V
    .locals 4

    .prologue
    .line 100
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->a:Landroid/content/Context;

    new-instance v1, Lcom/twitter/app/common/base/h;

    invoke-direct {v1}, Lcom/twitter/app/common/base/h;-><init>()V

    const/4 v2, 0x1

    .line 101
    invoke-virtual {v1, v2}, Lcom/twitter/app/common/base/h;->d(Z)Lcom/twitter/app/common/base/h;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->a:Landroid/content/Context;

    const-class v3, Lcom/twitter/android/TweetActivity;

    .line 102
    invoke-virtual {v1, v2, v3}, Lcom/twitter/app/common/base/h;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "tw"

    .line 103
    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    .line 100
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 104
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->g:Lzn;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lzn;->a(I)V

    .line 105
    return-void
.end method

.method public c(Lcom/twitter/model/core/Tweet;)V
    .locals 4

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/bl;->b()V

    .line 144
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 145
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->d:Lcom/twitter/android/moments/ui/fullscreen/bk;

    invoke-virtual {v1, p1, p0}, Lcom/twitter/android/moments/ui/fullscreen/bk;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/android/moments/ui/fullscreen/bl;)Lcom/twitter/android/dialog/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 146
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->b:Lcnz;

    iget-wide v2, p1, Lcom/twitter/model/core/Tweet;->b:J

    invoke-virtual {v1, v2, v3}, Lcnz;->a(J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 147
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->d:Lcom/twitter/android/moments/ui/fullscreen/bk;

    invoke-virtual {v1, p1, p0}, Lcom/twitter/android/moments/ui/fullscreen/bk;->c(Lcom/twitter/model/core/Tweet;Lcom/twitter/android/moments/ui/fullscreen/bl;)Lcom/twitter/android/dialog/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 148
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->d:Lcom/twitter/android/moments/ui/fullscreen/bk;

    .line 149
    invoke-virtual {v1, p1, p0}, Lcom/twitter/android/moments/ui/fullscreen/bk;->b(Lcom/twitter/model/core/Tweet;Lcom/twitter/android/moments/ui/fullscreen/bl;)Lcom/twitter/android/dialog/d;

    move-result-object v1

    .line 148
    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 151
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->c:Lcom/twitter/android/dialog/e;

    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/twitter/android/dialog/e;->a(Ljava/util/List;)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl;->n:Landroid/app/Dialog;

    .line 152
    return-void
.end method
