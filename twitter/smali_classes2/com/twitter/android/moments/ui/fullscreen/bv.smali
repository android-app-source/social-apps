.class public Lcom/twitter/android/moments/ui/fullscreen/bv;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/fullscreen/bv$a;
    }
.end annotation


# static fields
.field private static a:Lcom/twitter/android/moments/ui/fullscreen/bv;


# instance fields
.field private b:Lcom/twitter/android/moments/ui/fullscreen/bv$a;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    return-void
.end method

.method public static a()Lcom/twitter/android/moments/ui/fullscreen/bv;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/twitter/android/moments/ui/fullscreen/bv;->a:Lcom/twitter/android/moments/ui/fullscreen/bv;

    if-nez v0, :cond_0

    .line 28
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/bv;

    invoke-direct {v0}, Lcom/twitter/android/moments/ui/fullscreen/bv;-><init>()V

    sput-object v0, Lcom/twitter/android/moments/ui/fullscreen/bv;->a:Lcom/twitter/android/moments/ui/fullscreen/bv;

    .line 29
    const-class v0, Lcom/twitter/android/moments/ui/fullscreen/bv;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 32
    :cond_0
    sget-object v0, Lcom/twitter/android/moments/ui/fullscreen/bv;->a:Lcom/twitter/android/moments/ui/fullscreen/bv;

    return-object v0
.end method


# virtual methods
.method public a(Z)V
    .locals 4

    .prologue
    .line 44
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/bv$a;

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-direct {v0, p1, v2, v3}, Lcom/twitter/android/moments/ui/fullscreen/bv$a;-><init>(ZJ)V

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bv;->b:Lcom/twitter/android/moments/ui/fullscreen/bv$a;

    .line 45
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/fullscreen/bv;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/fullscreen/bv;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c()Z
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bv;->b:Lcom/twitter/android/moments/ui/fullscreen/bv$a;

    if-eqz v0, :cond_0

    .line 54
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/bv;->b:Lcom/twitter/android/moments/ui/fullscreen/bv$a;

    iget-wide v2, v2, Lcom/twitter/android/moments/ui/fullscreen/bv$a;->b:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    .line 53
    :goto_0
    return v0

    .line 54
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method d()Z
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bv;->b:Lcom/twitter/android/moments/ui/fullscreen/bv$a;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bv;->b:Lcom/twitter/android/moments/ui/fullscreen/bv$a;

    iget-boolean v0, v0, Lcom/twitter/android/moments/ui/fullscreen/bv$a;->a:Z

    goto :goto_0
.end method
