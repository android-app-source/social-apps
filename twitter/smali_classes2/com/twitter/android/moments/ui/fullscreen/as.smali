.class public Lcom/twitter/android/moments/ui/fullscreen/as;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/fullscreen/cb;

.field private final b:Lrx/f;

.field private final c:Lrx/subjects/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/a",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/cc;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/twitter/android/moments/ui/maker/an;

.field private e:Lcom/twitter/android/moments/ui/fullscreen/cc;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/cb;Lrx/f;Lcom/twitter/android/moments/ui/maker/an;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/as;->a:Lcom/twitter/android/moments/ui/fullscreen/cb;

    .line 35
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/as;->b:Lrx/f;

    .line 36
    invoke-static {}, Lrx/subjects/a;->r()Lrx/subjects/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/as;->c:Lrx/subjects/a;

    .line 37
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/as;->d:Lcom/twitter/android/moments/ui/maker/an;

    .line 38
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/as;)Lcom/twitter/android/moments/ui/fullscreen/cb;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/as;->a:Lcom/twitter/android/moments/ui/fullscreen/cb;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/as;Lcom/twitter/android/moments/ui/fullscreen/cc;)Lcom/twitter/android/moments/ui/fullscreen/cc;
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/as;->e:Lcom/twitter/android/moments/ui/fullscreen/cc;

    return-object p1
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/fullscreen/as;)Lcom/twitter/android/moments/ui/fullscreen/cc;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/as;->e:Lcom/twitter/android/moments/ui/fullscreen/cc;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/fullscreen/as;)Lrx/subjects/a;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/as;->c:Lrx/subjects/a;

    return-object v0
.end method


# virtual methods
.method public a()Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/cc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/as;->d:Lcom/twitter/android/moments/ui/maker/an;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/an;->a()Lrx/f;

    move-result-object v0

    .line 57
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/as;->c:Lrx/subjects/a;

    invoke-virtual {v1}, Lrx/subjects/a;->b()Lrx/g;

    move-result-object v1

    invoke-virtual {v1, v0}, Lrx/g;->a(Lrx/f;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/ViewGroup;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/moments/viewmodels/MomentPage;)V
    .locals 2

    .prologue
    .line 42
    invoke-static {p2}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/as;->b:Lrx/f;

    .line 43
    invoke-virtual {v0, v1}, Lrx/g;->b(Lrx/f;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/as$1;

    invoke-direct {v1, p0, p1, p3}, Lcom/twitter/android/moments/ui/fullscreen/as$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/as;Landroid/view/ViewGroup;Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    .line 44
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    .line 52
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/as;->e:Lcom/twitter/android/moments/ui/fullscreen/cc;

    if-eqz v0, :cond_0

    .line 62
    invoke-static {p0}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/as;->b:Lrx/f;

    .line 63
    invoke-virtual {v0, v1}, Lrx/g;->b(Lrx/f;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/as$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/fullscreen/as$2;-><init>(Lcom/twitter/android/moments/ui/fullscreen/as;)V

    .line 64
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    .line 71
    :cond_0
    return-void
.end method
