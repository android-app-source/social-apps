.class public Lcom/twitter/android/moments/ui/fullscreen/aw;
.super Landroid/support/v4/view/ViewPager$SimpleOnPageChangeListener;
.source "Twttr"


# instance fields
.field private final a:Labm;

.field private final b:Lcom/twitter/model/moments/viewmodels/a;

.field private final c:Lcom/twitter/android/moments/data/n;

.field private final d:Lrx/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/g",
            "<",
            "Lcom/twitter/model/moments/p;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lxj;

.field private final f:Lcom/twitter/android/moments/ui/fullscreen/ay;

.field private final g:Lzr;

.field private final h:Z

.field private i:Lrx/j;

.field private j:Lcom/twitter/model/moments/viewmodels/MomentPage;

.field private k:I


# direct methods
.method constructor <init>(Labm;Lcom/twitter/model/moments/viewmodels/a;Lcom/twitter/android/moments/data/n;Lrx/g;ZLxj;Lcom/twitter/android/moments/ui/fullscreen/ay;Lzr;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Labm;",
            "Lcom/twitter/model/moments/viewmodels/a;",
            "Lcom/twitter/android/moments/data/n;",
            "Lrx/g",
            "<",
            "Lcom/twitter/model/moments/p;",
            ">;Z",
            "Lxj;",
            "Lcom/twitter/android/moments/ui/fullscreen/ay;",
            "Lzr;",
            ")V"
        }
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager$SimpleOnPageChangeListener;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->a:Labm;

    .line 67
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->b:Lcom/twitter/model/moments/viewmodels/a;

    .line 68
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->c:Lcom/twitter/android/moments/data/n;

    .line 69
    iput-object p4, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->d:Lrx/g;

    .line 70
    iput-boolean p5, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->h:Z

    .line 71
    iput-object p6, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->e:Lxj;

    .line 72
    iput-object p7, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->f:Lcom/twitter/android/moments/ui/fullscreen/ay;

    .line 73
    iput-object p8, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->g:Lzr;

    .line 74
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/aw;->b()V

    .line 75
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/aw;)Labm;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->a:Labm;

    return-object v0
.end method

.method static a(Landroid/support/v4/app/FragmentActivity;Labm;Lcom/twitter/model/moments/viewmodels/a;Lcom/twitter/android/moments/data/n;Lrx/g;Lcom/twitter/android/moments/ui/fullscreen/co;J)Lcom/twitter/android/moments/ui/fullscreen/aw;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentActivity;",
            "Labm;",
            "Lcom/twitter/model/moments/viewmodels/a;",
            "Lcom/twitter/android/moments/data/n;",
            "Lrx/g",
            "<",
            "Lcom/twitter/model/moments/p;",
            ">;",
            "Lcom/twitter/android/moments/ui/fullscreen/co;",
            "J)",
            "Lcom/twitter/android/moments/ui/fullscreen/aw;"
        }
    .end annotation

    .prologue
    .line 50
    invoke-static {}, Lxj;->a()Lxj;

    move-result-object v6

    .line 51
    invoke-static {p0, p5}, Lcom/twitter/android/moments/ui/fullscreen/ay;->a(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/android/moments/ui/fullscreen/co;)Lcom/twitter/android/moments/ui/fullscreen/ay;

    move-result-object v7

    .line 52
    invoke-static/range {p6 .. p7}, Lzr;->a(J)Lzr;

    move-result-object v8

    .line 53
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/aw;

    .line 54
    invoke-interface {p5}, Lcom/twitter/android/moments/ui/fullscreen/co;->b()Z

    move-result v5

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/moments/ui/fullscreen/aw;-><init>(Labm;Lcom/twitter/model/moments/viewmodels/a;Lcom/twitter/android/moments/data/n;Lrx/g;ZLxj;Lcom/twitter/android/moments/ui/fullscreen/ay;Lzr;)V

    .line 53
    return-object v0
.end method

.method private a(Lcom/twitter/model/moments/viewmodels/MomentPage;)V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/aw;->b(Lcom/twitter/model/moments/viewmodels/MomentPage;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->a:Labm;

    invoke-virtual {v0}, Labm;->a()V

    .line 90
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->f:Lcom/twitter/android/moments/ui/fullscreen/ay;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ay;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    .line 94
    :goto_0
    return-void

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->a:Labm;

    invoke-virtual {v0}, Labm;->b()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/fullscreen/aw;)Lcom/twitter/android/moments/data/n;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->c:Lcom/twitter/android/moments/data/n;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->d:Lrx/g;

    .line 99
    invoke-static {}, Lcom/twitter/android/moments/ui/fullscreen/aw;->c()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->b(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->e:Lxj;

    iget-object v1, v1, Lxj;->a:Lrx/f;

    .line 100
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->e:Lxj;

    iget-object v1, v1, Lxj;->b:Lrx/f;

    .line 101
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 102
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/aw;->d()Lcqw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->i:Lrx/j;

    .line 103
    return-void
.end method

.method private b(Lcom/twitter/model/moments/viewmodels/MomentPage;)Z
    .locals 1

    .prologue
    .line 165
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->h:Z

    if-nez v0, :cond_2

    invoke-static {}, Lbsd;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 166
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->f()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/Moment;

    iget-boolean v0, v0, Lcom/twitter/model/moments/Moment;->d:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->l()Z

    move-result v0

    if-nez v0, :cond_1

    .line 167
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 165
    :goto_0
    return v0

    .line 167
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<",
            "Lcom/twitter/model/moments/p;",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 107
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/aw$1;

    invoke-direct {v0}, Lcom/twitter/android/moments/ui/fullscreen/aw$1;-><init>()V

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/fullscreen/aw;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/aw;->f()V

    return-void
.end method

.method private d()Lcqw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcqw",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/aw$2;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/fullscreen/aw$2;-><init>(Lcom/twitter/android/moments/ui/fullscreen/aw;)V

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/moments/ui/fullscreen/aw;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/aw;->e()V

    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->j:Lcom/twitter/model/moments/viewmodels/MomentPage;

    if-nez v0, :cond_0

    .line 148
    :goto_0
    return-void

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->j:Lcom/twitter/model/moments/viewmodels/MomentPage;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 142
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->g:Lzr;

    invoke-virtual {v0}, Lzr;->a()V

    goto :goto_0

    .line 143
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->j:Lcom/twitter/model/moments/viewmodels/MomentPage;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 144
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->g:Lzr;

    invoke-virtual {v0}, Lzr;->c()V

    goto :goto_0

    .line 146
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->g:Lzr;

    iget v1, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->k:I

    invoke-virtual {v0, v1}, Lzr;->a(I)V

    goto :goto_0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->j:Lcom/twitter/model/moments/viewmodels/MomentPage;

    if-nez v0, :cond_0

    .line 161
    :goto_0
    return-void

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->j:Lcom/twitter/model/moments/viewmodels/MomentPage;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 155
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->g:Lzr;

    invoke-virtual {v0}, Lzr;->b()V

    goto :goto_0

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->j:Lcom/twitter/model/moments/viewmodels/MomentPage;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 157
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->g:Lzr;

    invoke-virtual {v0}, Lzr;->d()V

    goto :goto_0

    .line 159
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->g:Lzr;

    iget v1, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->k:I

    invoke-virtual {v0, v1}, Lzr;->b(I)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->i:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 172
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->b:Lcom/twitter/model/moments/viewmodels/a;

    invoke-virtual {v0, p1}, Lcom/twitter/model/moments/viewmodels/a;->c(I)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 80
    if-eqz v0, :cond_0

    .line 81
    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/fullscreen/aw;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    .line 82
    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->j:Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 83
    iput p1, p0, Lcom/twitter/android/moments/ui/fullscreen/aw;->k:I

    .line 85
    :cond_0
    return-void
.end method
