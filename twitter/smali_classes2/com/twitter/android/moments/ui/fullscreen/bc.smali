.class public Lcom/twitter/android/moments/ui/fullscreen/bc;
.super Landroid/support/v4/view/ViewPager$SimpleOnPageChangeListener;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/model/moments/viewmodels/a;

.field private final b:Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;

.field private final c:Labm;

.field private final d:Z

.field private final e:Z

.field private final f:Z

.field private final g:Z


# direct methods
.method public constructor <init>(Labm;Lcom/twitter/android/moments/ui/maker/af;Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;Lcom/twitter/android/moments/ui/fullscreen/co;Lcom/twitter/model/moments/viewmodels/a;J)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager$SimpleOnPageChangeListener;-><init>()V

    .line 41
    iput-object p5, p0, Lcom/twitter/android/moments/ui/fullscreen/bc;->a:Lcom/twitter/model/moments/viewmodels/a;

    .line 42
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/bc;->b:Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;

    .line 43
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/bc;->c:Labm;

    .line 44
    invoke-direct {p0, p6, p7}, Lcom/twitter/android/moments/ui/fullscreen/bc;->a(J)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bc;->d:Z

    .line 45
    invoke-interface {p4}, Lcom/twitter/android/moments/ui/fullscreen/co;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bc;->f:Z

    .line 46
    invoke-virtual {p2}, Lcom/twitter/android/moments/ui/maker/af;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bc;->e:Z

    .line 47
    invoke-static {}, Lbrz;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bc;->g:Z

    .line 48
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bc;->f:Z

    if-eqz v0, :cond_0

    .line 49
    invoke-virtual {p1}, Labm;->c()V

    .line 51
    invoke-virtual {p1}, Labm;->d()V

    .line 53
    :cond_0
    return-void
.end method

.method public static a(Labm;Lcom/twitter/model/moments/viewmodels/a;Landroid/app/Activity;Lcom/twitter/android/moments/ui/fullscreen/co;J)Lcom/twitter/android/moments/ui/fullscreen/bc;
    .locals 8

    .prologue
    .line 32
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/bc;

    .line 33
    invoke-static {p2}, Lcom/twitter/android/moments/ui/maker/af;->a(Landroid/content/Context;)Lcom/twitter/android/moments/ui/maker/af;

    move-result-object v2

    new-instance v3, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;

    invoke-direct {v3, p2}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;-><init>(Landroid/app/Activity;)V

    move-object v1, p0

    move-object v4, p3

    move-object v5, p1

    move-wide v6, p4

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/moments/ui/fullscreen/bc;-><init>(Labm;Lcom/twitter/android/moments/ui/maker/af;Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;Lcom/twitter/android/moments/ui/fullscreen/co;Lcom/twitter/model/moments/viewmodels/a;J)V

    .line 32
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/bc;Lcom/twitter/model/moments/viewmodels/MomentPage;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/bc;->b(Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    return-void
.end method

.method private a(Lcom/twitter/model/moments/viewmodels/MomentPage;)V
    .locals 2

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/bc;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bc;->c:Labm;

    invoke-virtual {v0}, Labm;->e()V

    .line 66
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bc;->c:Labm;

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/bc$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/bc$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/bc;Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    invoke-virtual {v0, v1}, Labm;->c(Landroid/view/View$OnClickListener;)V

    .line 75
    :goto_0
    return-void

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bc;->c:Labm;

    invoke-virtual {v0}, Labm;->f()V

    goto :goto_0
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bc;->f:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bc;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bc;->g:Z

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bc;->e:Z

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(J)Z
    .locals 3

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bc;->a:Lcom/twitter/model/moments/viewmodels/a;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/a;->a()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/moments/Moment;->o:Lcom/twitter/model/moments/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bc;->a:Lcom/twitter/model/moments/viewmodels/a;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/a;->a()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/moments/Moment;->o:Lcom/twitter/model/moments/a;

    iget-wide v0, v0, Lcom/twitter/model/moments/a;->b:J

    :goto_0
    cmp-long v0, v0, p1

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(Lcom/twitter/model/moments/viewmodels/MomentPage;)V
    .locals 5

    .prologue
    .line 78
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bc;->b:Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;

    .line 79
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->g()Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    new-instance v0, Lcil;

    .line 80
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v4

    invoke-direct {v0, v4}, Lcil;-><init>(Lcom/twitter/model/moments/r;)V

    .line 78
    invoke-static {v2, v3, v0}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;->a(JLcim;)Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;->a(Lako;)V

    .line 81
    return-void
.end method


# virtual methods
.method public onPageSelected(I)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bc;->a:Lcom/twitter/model/moments/viewmodels/a;

    invoke-virtual {v0, p1}, Lcom/twitter/model/moments/viewmodels/a;->c(I)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 58
    if-eqz v0, :cond_0

    .line 59
    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/fullscreen/bc;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    .line 61
    :cond_0
    return-void
.end method
