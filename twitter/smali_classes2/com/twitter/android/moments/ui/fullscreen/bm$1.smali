.class Lcom/twitter/android/moments/ui/fullscreen/bm$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/fullscreen/bm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/q",
        "<",
        "Lcom/twitter/util/collection/Pair",
        "<",
        "Ljava/lang/String;",
        "Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/ui/fullscreen/bm;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/bm;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/bm$1;->a:Lcom/twitter/android/moments/ui/fullscreen/bm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEvent(Lcom/twitter/util/collection/Pair;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-virtual {p1}, Lcom/twitter/util/collection/Pair;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;->b:Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;

    sget-object v1, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->f:Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;

    if-eq v0, v1, :cond_0

    .line 36
    invoke-virtual {p1}, Lcom/twitter/util/collection/Pair;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;->b:Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;

    sget-object v1, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->d:Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;

    if-ne v0, v1, :cond_1

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bm$1;->a:Lcom/twitter/android/moments/ui/fullscreen/bm;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/bm;->a(Lcom/twitter/android/moments/ui/fullscreen/bm;)Lcom/twitter/moments/core/ui/widget/capsule/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/moments/core/ui/widget/capsule/a;->c()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v1

    .line 38
    if-eqz v1, :cond_1

    instance-of v0, v1, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;

    if-eqz v0, :cond_1

    .line 39
    invoke-virtual {p1}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;

    .line 40
    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/bm;->a(Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bm$1;->a:Lcom/twitter/android/moments/ui/fullscreen/bm;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/bm;->b(Lcom/twitter/android/moments/ui/fullscreen/bm;)V

    .line 44
    :cond_1
    return-void
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 32
    check-cast p1, Lcom/twitter/util/collection/Pair;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/bm$1;->onEvent(Lcom/twitter/util/collection/Pair;)V

    return-void
.end method
