.class public Lcom/twitter/android/moments/ui/fullscreen/ac;
.super Lcom/twitter/android/moments/ui/fullscreen/cz;
.source "Twttr"


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/view/View;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/view/View;

.field private final f:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/cz;-><init>(Landroid/view/ViewGroup;)V

    .line 22
    const v0, 0x7f1304ff

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ac;->a:Landroid/view/View;

    .line 23
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ac;->a:Landroid/view/View;

    const v1, 0x7f13009a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ac;->b:Landroid/widget/TextView;

    .line 24
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ac;->a:Landroid/view/View;

    const v1, 0x7f130300

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ac;->c:Landroid/view/View;

    .line 25
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ac;->a:Landroid/view/View;

    const v1, 0x7f13040f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ac;->d:Landroid/widget/TextView;

    .line 26
    const v0, 0x7f1304fe

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ac;->e:Landroid/view/View;

    .line 27
    const v0, 0x7f1304fc

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ac;->f:Landroid/view/View;

    .line 28
    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ac;->f:Landroid/view/View;

    return-object v0
.end method
