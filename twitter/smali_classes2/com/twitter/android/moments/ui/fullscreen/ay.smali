.class Lcom/twitter/android/moments/ui/fullscreen/ay;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/ui/widget/Tooltip$c;


# instance fields
.field private final a:Landroid/support/v4/app/FragmentManager;

.field private final b:Lcom/twitter/android/util/h;

.field private final c:Lcom/twitter/android/moments/ui/fullscreen/co;

.field private d:Lcom/twitter/ui/widget/Tooltip$a;


# direct methods
.method constructor <init>(Landroid/support/v4/app/FragmentManager;Lcom/twitter/ui/widget/Tooltip$a;Lcom/twitter/android/util/h;Lcom/twitter/android/moments/ui/fullscreen/co;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/ay;->a:Landroid/support/v4/app/FragmentManager;

    .line 44
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/ay;->d:Lcom/twitter/ui/widget/Tooltip$a;

    .line 45
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/ay;->b:Lcom/twitter/android/util/h;

    .line 46
    iput-object p4, p0, Lcom/twitter/android/moments/ui/fullscreen/ay;->c:Lcom/twitter/android/moments/ui/fullscreen/co;

    .line 47
    return-void
.end method

.method static a(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/android/moments/ui/fullscreen/co;)Lcom/twitter/android/moments/ui/fullscreen/ay;
    .locals 5

    .prologue
    .line 28
    .line 29
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f130506

    invoke-static {v0, v1}, Lcom/twitter/ui/widget/Tooltip;->a(Landroid/content/Context;I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v1, 0x7f0a0556

    .line 30
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->a(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v1, 0x7f0d03d1

    .line 31
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->b(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const/4 v1, 0x1

    .line 32
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->c(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v1, 0x7f13043b

    .line 33
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->d(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    .line 34
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 36
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v4, "moment_like_button_tooltip"

    .line 35
    invoke-static {v1, v4, v2, v3}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v1

    .line 37
    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/ay;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-direct {v2, v3, v0, v1, p1}, Lcom/twitter/android/moments/ui/fullscreen/ay;-><init>(Landroid/support/v4/app/FragmentManager;Lcom/twitter/ui/widget/Tooltip$a;Lcom/twitter/android/util/h;Lcom/twitter/android/moments/ui/fullscreen/co;)V

    return-object v2
.end method

.method private b(Lcom/twitter/model/moments/viewmodels/MomentPage;)Z
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lbsd;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ay;->d:Lcom/twitter/ui/widget/Tooltip$a;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ay;->c:Lcom/twitter/android/moments/ui/fullscreen/co;

    .line 69
    invoke-interface {v0}, Lcom/twitter/android/moments/ui/fullscreen/co;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ay;->b:Lcom/twitter/android/util/h;

    invoke-virtual {v0}, Lcom/twitter/android/util/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 68
    :goto_0
    return v0

    .line 69
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(Lcom/twitter/model/moments/viewmodels/MomentPage;)V
    .locals 3

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ay;->b(Lcom/twitter/model/moments/viewmodels/MomentPage;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ay;->d:Lcom/twitter/ui/widget/Tooltip$a;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/Tooltip$a;

    .line 61
    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/Tooltip$a;->a(Lcom/twitter/ui/widget/Tooltip$c;)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ay;->a:Landroid/support/v4/app/FragmentManager;

    const-string/jumbo v2, "moment_like_button_tooltip"

    .line 62
    invoke-virtual {v0, v1, v2}, Lcom/twitter/ui/widget/Tooltip$a;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)Lcom/twitter/ui/widget/Tooltip;

    .line 63
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ay;->b:Lcom/twitter/android/util/h;

    invoke-virtual {v0}, Lcom/twitter/android/util/h;->b()V

    .line 65
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/ui/widget/Tooltip;I)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 51
    if-ne p2, v0, :cond_1

    .line 52
    invoke-virtual {p1, v0}, Lcom/twitter/ui/widget/Tooltip;->a(Z)V

    .line 56
    :cond_0
    :goto_0
    return-void

    .line 53
    :cond_1
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ay;->d:Lcom/twitter/ui/widget/Tooltip$a;

    goto :goto_0
.end method
