.class public Lcom/twitter/android/moments/ui/fullscreen/bn;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/fullscreen/bn$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/p;

.field private final c:Labs;

.field private d:Lcom/twitter/android/moments/ui/fullscreen/bn$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/p;Labs;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/bn;->a:Landroid/content/Context;

    .line 38
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/bn;->b:Lcom/twitter/library/client/p;

    .line 39
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/bn;->c:Labs;

    .line 40
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/bn;)Labs;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bn;->c:Labs;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/fullscreen/bn;)Lcom/twitter/library/client/p;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bn;->b:Lcom/twitter/library/client/p;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/fullscreen/bn;)Lcom/twitter/android/moments/ui/fullscreen/bn$a;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bn;->d:Lcom/twitter/android/moments/ui/fullscreen/bn$a;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/ui/fullscreen/bn$a;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/bn;->d:Lcom/twitter/android/moments/ui/fullscreen/bn$a;

    .line 75
    return-void
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;)V
    .locals 6

    .prologue
    .line 44
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 45
    iget-wide v2, p1, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-object v0, p1, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    invoke-static {v2, v3, v0, v1}, Lcom/twitter/android/profiles/v;->a(JLjava/lang/String;Lcom/twitter/library/client/Session;)Z

    move-result v2

    .line 46
    iget v0, p1, Lcom/twitter/model/core/TwitterUser;->U:I

    invoke-static {v0}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v3

    .line 47
    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/bn;->c:Labs;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/bn;->a:Landroid/content/Context;

    if-eqz v3, :cond_0

    const v0, 0x7f0a09ac

    .line 48
    :goto_0
    invoke-virtual {v5, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 47
    invoke-virtual {v4, v2, v3, v0}, Labs;->a(ZZLjava/lang/CharSequence;)V

    .line 49
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bn;->c:Labs;

    new-instance v3, Lcom/twitter/android/moments/ui/fullscreen/bn$1;

    invoke-direct {v3, p0, v1, p1, v2}, Lcom/twitter/android/moments/ui/fullscreen/bn$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/bn;Lcom/twitter/library/client/Session;Lcom/twitter/model/core/TwitterUser;Z)V

    invoke-virtual {v0, v3}, Labs;->a(Landroid/view/View$OnClickListener;)V

    .line 71
    return-void

    .line 47
    :cond_0
    const v0, 0x7f0a03a5

    goto :goto_0
.end method
