.class public Lcom/twitter/android/moments/ui/fullscreen/bh;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/bh;->a:Landroid/content/res/Resources;

    .line 20
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/Moment;)Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 24
    iget-object v0, p1, Lcom/twitter/model/moments/Moment;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 25
    iget-object v0, p1, Lcom/twitter/model/moments/Moment;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 26
    iget-object v7, p1, Lcom/twitter/model/moments/Moment;->o:Lcom/twitter/model/moments/a;

    .line 27
    iget-object v0, p1, Lcom/twitter/model/moments/Moment;->o:Lcom/twitter/model/moments/a;

    if-eqz v0, :cond_1

    move v2, v3

    .line 30
    :goto_0
    if-eqz v2, :cond_2

    .line 31
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bh;->a:Landroid/content/res/Resources;

    const v1, 0x7f0a0591

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 32
    new-array v0, v11, [Ljava/lang/Object;

    const-string/jumbo v8, "\u26a1"

    aput-object v8, v0, v4

    aput-object v5, v0, v3

    iget-object v8, v7, Lcom/twitter/model/moments/a;->e:Ljava/lang/String;

    aput-object v8, v0, v9

    aput-object v6, v0, v10

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 37
    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit16 v8, v8, -0x8c

    .line 38
    if-lez v8, :cond_0

    .line 39
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    sub-int/2addr v0, v8

    .line 40
    if-lez v0, :cond_3

    invoke-static {v5, v0}, Lcom/twitter/util/y;->e(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 41
    :goto_2
    if-eqz v2, :cond_4

    .line 42
    new-array v2, v11, [Ljava/lang/Object;

    const-string/jumbo v5, "\u26a1"

    aput-object v5, v2, v4

    aput-object v0, v2, v3

    iget-object v0, v7, Lcom/twitter/model/moments/a;->e:Ljava/lang/String;

    aput-object v0, v2, v9

    aput-object v6, v2, v10

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 47
    :cond_0
    :goto_3
    return-object v0

    :cond_1
    move v2, v4

    .line 27
    goto :goto_0

    .line 34
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bh;->a:Landroid/content/res/Resources;

    const v1, 0x7f0a0be1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 35
    new-array v0, v10, [Ljava/lang/Object;

    const-string/jumbo v8, "\u26a1"

    aput-object v8, v0, v4

    aput-object v5, v0, v3

    aput-object v6, v0, v9

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 40
    :cond_3
    const-string/jumbo v0, ""

    goto :goto_2

    .line 44
    :cond_4
    new-array v2, v10, [Ljava/lang/Object;

    const-string/jumbo v5, "\u26a1"

    aput-object v5, v2, v4

    aput-object v0, v2, v3

    aput-object v6, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method
