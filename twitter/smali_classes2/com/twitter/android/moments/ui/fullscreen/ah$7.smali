.class Lcom/twitter/android/moments/ui/fullscreen/ah$7;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/ui/anim/p$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Lcom/twitter/util/collection/k;Lcom/twitter/model/moments/r;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/ui/fullscreen/ah;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/ah;)V
    .locals 0

    .prologue
    .line 334
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$7;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 337
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 338
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$7;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Lcom/twitter/android/moments/ui/fullscreen/ah;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/d;->a(Landroid/view/View;)V

    .line 340
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$7;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Lcom/twitter/android/moments/ui/fullscreen/ah;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_2

    .line 341
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$7;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/ah;->d(Lcom/twitter/android/moments/ui/fullscreen/ah;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 342
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$7;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/ah;->d(Lcom/twitter/android/moments/ui/fullscreen/ah;)Landroid/view/View;

    move-result-object v0

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 344
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$7;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/ah;->e(Lcom/twitter/android/moments/ui/fullscreen/ah;)Lcom/twitter/model/moments/viewmodels/a;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 345
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$7;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$7;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/fullscreen/ah;->e(Lcom/twitter/android/moments/ui/fullscreen/ah;)Lcom/twitter/model/moments/viewmodels/a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$7;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-static {v2}, Lcom/twitter/android/moments/ui/fullscreen/ah;->f(Lcom/twitter/android/moments/ui/fullscreen/ah;)Lyj;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$7;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    .line 346
    invoke-static {v3}, Lcom/twitter/android/moments/ui/fullscreen/ah;->g(Lcom/twitter/android/moments/ui/fullscreen/ah;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/util/collection/k;->a(Ljava/lang/Object;)Lcom/twitter/util/collection/k;

    move-result-object v3

    .line 345
    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Lcom/twitter/android/moments/ui/fullscreen/ah;Lcom/twitter/model/moments/viewmodels/a;Lyj;Lcom/twitter/util/collection/k;)V

    .line 347
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$7;->a:Lcom/twitter/android/moments/ui/fullscreen/ah;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Lcom/twitter/android/moments/ui/fullscreen/ah;Lcom/twitter/model/moments/viewmodels/a;)Lcom/twitter/model/moments/viewmodels/a;

    .line 350
    :cond_2
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 353
    return-void
.end method
