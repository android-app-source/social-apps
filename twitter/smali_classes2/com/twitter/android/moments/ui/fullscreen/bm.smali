.class public Lcom/twitter/android/moments/ui/fullscreen/bm;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Lcom/twitter/util/o;
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/view/ViewPager$OnPageChangeListener;",
        "Lcom/twitter/util/o;",
        "Lcom/twitter/util/q",
        "<",
        "Lcom/twitter/android/moments/ui/fullscreen/Event;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/util/h;

.field private final b:Lcom/twitter/moments/core/ui/widget/capsule/a;

.field private final c:Lcom/twitter/android/moments/ui/fullscreen/TapHintView;

.field private final d:Lcom/twitter/util/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/twitter/android/moments/ui/fullscreen/ad;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/moments/ui/fullscreen/ad",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;",
            ">;"
        }
    .end annotation
.end field

.field private f:Z

.field private final g:Lcom/twitter/util/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private h:Z


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/TapHintView;Lcom/twitter/android/util/h;Lcom/twitter/moments/core/ui/widget/capsule/a;Lcom/twitter/util/p;Lcom/twitter/android/moments/ui/fullscreen/ad;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/ui/fullscreen/TapHintView;",
            "Lcom/twitter/android/util/h;",
            "Lcom/twitter/moments/core/ui/widget/capsule/a;",
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;",
            "Lcom/twitter/android/moments/ui/fullscreen/ad",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/bm$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/fullscreen/bm$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/bm;)V

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->g:Lcom/twitter/util/q;

    .line 52
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->c:Lcom/twitter/android/moments/ui/fullscreen/TapHintView;

    .line 53
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->a:Lcom/twitter/android/util/h;

    .line 54
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->b:Lcom/twitter/moments/core/ui/widget/capsule/a;

    .line 55
    iput-object p4, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->d:Lcom/twitter/util/p;

    .line 56
    iput-object p5, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->e:Lcom/twitter/android/moments/ui/fullscreen/ad;

    .line 57
    invoke-virtual {p4, p0}, Lcom/twitter/util/p;->a(Lcom/twitter/util/q;)Z

    .line 58
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->g:Lcom/twitter/util/q;

    invoke-virtual {p5, v0}, Lcom/twitter/android/moments/ui/fullscreen/ad;->a(Lcom/twitter/util/q;)V

    .line 59
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/bm;)Lcom/twitter/moments/core/ui/widget/capsule/a;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->b:Lcom/twitter/moments/core/ui/widget/capsule/a;

    return-object v0
.end method

.method static a(Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;)Z
    .locals 2

    .prologue
    .line 94
    invoke-static {}, Lbsd;->c()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;->k()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;->l()Z

    move-result v0

    if-nez v0, :cond_1

    .line 95
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 96
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;->e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->b:Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/HydratableMomentPage;->e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->c:Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 94
    :goto_0
    return v0

    .line 96
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/fullscreen/bm;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/bm;->d()V

    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->a:Lcom/twitter/android/util/h;

    invoke-virtual {v0}, Lcom/twitter/android/util/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->f:Z

    if-nez v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->c:Lcom/twitter/android/moments/ui/fullscreen/TapHintView;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/TapHintView;->a()V

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->h:Z

    .line 81
    :cond_0
    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->h:Z

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->c:Lcom/twitter/android/moments/ui/fullscreen/TapHintView;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/TapHintView;->b()V

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->h:Z

    .line 91
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->f:Z

    .line 121
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/bm;->e()V

    .line 122
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->f:Z

    .line 116
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->c:Lcom/twitter/android/moments/ui/fullscreen/TapHintView;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/TapHintView;->c()V

    .line 109
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->d:Lcom/twitter/util/p;

    invoke-virtual {v0, p0}, Lcom/twitter/util/p;->b(Lcom/twitter/util/q;)Z

    .line 110
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->e:Lcom/twitter/android/moments/ui/fullscreen/ad;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->g:Lcom/twitter/util/q;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/ad;->c(Lcom/twitter/util/q;)V

    .line 111
    return-void
.end method

.method public onEvent(Lcom/twitter/android/moments/ui/fullscreen/Event;)V
    .locals 2

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->h:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/android/moments/ui/fullscreen/Event;->a()Lcom/twitter/android/moments/ui/fullscreen/Event$EventType;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/moments/ui/fullscreen/Event$EventType;->b:Lcom/twitter/android/moments/ui/fullscreen/Event$EventType;

    if-ne v0, v1, :cond_0

    .line 102
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/bm;->e()V

    .line 103
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bm;->a:Lcom/twitter/android/util/h;

    invoke-virtual {v0}, Lcom/twitter/android/util/h;->b()V

    .line 105
    :cond_0
    return-void
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lcom/twitter/android/moments/ui/fullscreen/Event;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/bm;->onEvent(Lcom/twitter/android/moments/ui/fullscreen/Event;)V

    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0

    .prologue
    .line 71
    if-eqz p1, :cond_0

    .line 72
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/bm;->e()V

    .line 74
    :cond_0
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method public onPageSelected(I)V
    .locals 0

    .prologue
    .line 67
    return-void
.end method
