.class public Lcom/twitter/android/moments/ui/fullscreen/cl;
.super Lcom/twitter/android/moments/ui/fullscreen/af;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/livevideo/landing/i$a;


# instance fields
.field private final b:Labw;

.field private final c:Lcom/twitter/android/moments/data/h;

.field private final d:Lcom/twitter/android/livevideo/landing/i;

.field private final e:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/av;Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;Labw;Lcom/twitter/android/livevideo/landing/i;Lcom/twitter/android/moments/data/h;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/af;-><init>(Lcom/twitter/android/moments/ui/fullscreen/av;)V

    .line 42
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/cl;->e:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    .line 43
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/cl;->b:Labw;

    .line 44
    iput-object p4, p0, Lcom/twitter/android/moments/ui/fullscreen/cl;->d:Lcom/twitter/android/livevideo/landing/i;

    .line 45
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cl;->d:Lcom/twitter/android/livevideo/landing/i;

    invoke-virtual {v0, p0}, Lcom/twitter/android/livevideo/landing/i;->a(Lcom/twitter/android/livevideo/landing/i$a;)V

    .line 46
    iput-object p5, p0, Lcom/twitter/android/moments/ui/fullscreen/cl;->c:Lcom/twitter/android/moments/data/h;

    .line 47
    return-void
.end method

.method public static a(Lcom/twitter/android/moments/ui/fullscreen/av;Landroid/content/res/Resources;Lcom/twitter/android/moments/data/h;Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;)Lcom/twitter/android/moments/ui/fullscreen/cl;
    .locals 6

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/fullscreen/av;->f()Labq;

    move-result-object v0

    .line 30
    new-instance v3, Labw;

    invoke-direct {v3, v0}, Labw;-><init>(Labq;)V

    .line 32
    new-instance v4, Lcom/twitter/android/livevideo/landing/i;

    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v0

    invoke-direct {v4, p1, v0}, Lcom/twitter/android/livevideo/landing/i;-><init>(Landroid/content/res/Resources;Lrx/f;)V

    .line 33
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/cl;

    move-object v1, p0

    move-object v2, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/moments/ui/fullscreen/cl;-><init>(Lcom/twitter/android/moments/ui/fullscreen/av;Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;Labw;Lcom/twitter/android/livevideo/landing/i;Lcom/twitter/android/moments/data/h;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cl;->b:Labw;

    invoke-virtual {v0, p1}, Labw;->a(Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    .line 60
    invoke-super {p0}, Lcom/twitter/android/moments/ui/fullscreen/af;->c()V

    .line 61
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cl;->e:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    .line 62
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/cl;->d:Lcom/twitter/android/livevideo/landing/i;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/cl;->c:Lcom/twitter/android/moments/data/h;

    invoke-virtual {v2, v0}, Lcom/twitter/android/moments/data/h;->c(Lcom/twitter/model/core/Tweet;)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/livevideo/landing/i;->d(J)V

    .line 63
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cl;->d:Lcom/twitter/android/livevideo/landing/i;

    invoke-virtual {v0}, Lcom/twitter/android/livevideo/landing/i;->a()V

    .line 68
    invoke-super {p0}, Lcom/twitter/android/moments/ui/fullscreen/af;->d()V

    .line 69
    return-void
.end method

.method public f()V
    .locals 0

    .prologue
    .line 56
    return-void
.end method
