.class public Lcom/twitter/android/moments/ui/fullscreen/dm;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/fullscreen/cc;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/fullscreen/dm$a;
    }
.end annotation


# instance fields
.field private final a:Lach;

.field private final b:Lcom/twitter/android/card/b;

.field private final c:Lcom/twitter/model/moments/n;


# direct methods
.method public constructor <init>(Lach;Lcom/twitter/android/card/b;Lcom/twitter/model/moments/n;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/dm;->a:Lach;

    .line 21
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/dm;->b:Lcom/twitter/android/card/b;

    .line 22
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/dm;->c:Lcom/twitter/model/moments/n;

    .line 23
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/dm;)Lcom/twitter/android/card/b;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dm;->b:Lcom/twitter/android/card/b;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dm;->c:Lcom/twitter/model/moments/n;

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dm;->a:Lach;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dm;->c:Lcom/twitter/model/moments/n;

    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/dm$1;

    invoke-direct {v2, p0}, Lcom/twitter/android/moments/ui/fullscreen/dm$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/dm;)V

    invoke-virtual {v0, v1, v2}, Lach;->a(Lcom/twitter/model/moments/n;Lcom/twitter/android/moments/ui/fullscreen/dm$a;)V

    .line 35
    :cond_0
    return-void
.end method

.method public b()Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/cc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    invoke-static {p0}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 45
    return-void
.end method
