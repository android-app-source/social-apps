.class public Lcom/twitter/android/moments/ui/fullscreen/ax;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Labk;

.field private final c:Lcom/twitter/android/moments/data/n;

.field private final d:Lxj;

.field private final e:Lcom/twitter/app/users/URTUsersActivity$b;

.field private final f:Lzr;

.field private final g:Lcom/twitter/android/moments/ui/fullscreen/co;

.field private h:Lrx/j;


# direct methods
.method constructor <init>(Landroid/content/res/Resources;Labk;Lcom/twitter/android/moments/data/n;Lcom/twitter/android/moments/ui/fullscreen/co;Lxj;Lrx/g;Lcom/twitter/app/users/URTUsersActivity$b;Lzr;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Labk;",
            "Lcom/twitter/android/moments/data/n;",
            "Lcom/twitter/android/moments/ui/fullscreen/co;",
            "Lxj;",
            "Lrx/g",
            "<",
            "Lcom/twitter/model/moments/p;",
            ">;",
            "Lcom/twitter/app/users/URTUsersActivity$b;",
            "Lzr;",
            ")V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/ax;->a:Landroid/content/res/Resources;

    .line 62
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/ax;->b:Labk;

    .line 63
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/ax;->c:Lcom/twitter/android/moments/data/n;

    .line 64
    iput-object p4, p0, Lcom/twitter/android/moments/ui/fullscreen/ax;->g:Lcom/twitter/android/moments/ui/fullscreen/co;

    .line 65
    iput-object p5, p0, Lcom/twitter/android/moments/ui/fullscreen/ax;->d:Lxj;

    .line 66
    iput-object p7, p0, Lcom/twitter/android/moments/ui/fullscreen/ax;->e:Lcom/twitter/app/users/URTUsersActivity$b;

    .line 67
    iput-object p8, p0, Lcom/twitter/android/moments/ui/fullscreen/ax;->f:Lzr;

    .line 68
    invoke-direct {p0, p6}, Lcom/twitter/android/moments/ui/fullscreen/ax;->a(Lrx/g;)V

    .line 69
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/ax;)Labk;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ax;->b:Labk;

    return-object v0
.end method

.method private a(J)Landroid/text/SpannableString;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 130
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ax;->a:Landroid/content/res/Resources;

    const v1, 0x7f0c0011

    .line 131
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->intValue()I

    move-result v2

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ax;->a:Landroid/content/res/Resources;

    invoke-static {v4, p1, p2}, Lcom/twitter/util/r;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    .line 130
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 132
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 135
    const-string/jumbo v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 136
    new-instance v2, Landroid/text/style/StyleSpan;

    invoke-direct {v2, v6}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v3, 0x21

    invoke-virtual {v1, v2, v5, v0, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 137
    return-object v1
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/ax;J)Landroid/text/SpannableString;
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/moments/ui/fullscreen/ax;->a(J)Landroid/text/SpannableString;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/app/Activity;Labk;Lcom/twitter/android/moments/data/n;Lcom/twitter/android/moments/ui/fullscreen/co;Lrx/g;Lzr;)Lcom/twitter/android/moments/ui/fullscreen/ax;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Labk;",
            "Lcom/twitter/android/moments/data/n;",
            "Lcom/twitter/android/moments/ui/fullscreen/co;",
            "Lrx/g",
            "<",
            "Lcom/twitter/model/moments/p;",
            ">;",
            "Lzr;",
            ")",
            "Lcom/twitter/android/moments/ui/fullscreen/ax;"
        }
    .end annotation

    .prologue
    .line 49
    new-instance v7, Lcom/twitter/app/users/URTUsersActivity$b;

    invoke-direct {v7, p0}, Lcom/twitter/app/users/URTUsersActivity$b;-><init>(Landroid/app/Activity;)V

    .line 50
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/ax;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 51
    invoke-static {}, Lxj;->a()Lxj;

    move-result-object v5

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v6, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/moments/ui/fullscreen/ax;-><init>(Landroid/content/res/Resources;Labk;Lcom/twitter/android/moments/data/n;Lcom/twitter/android/moments/ui/fullscreen/co;Lxj;Lrx/g;Lcom/twitter/app/users/URTUsersActivity$b;Lzr;)V

    .line 50
    return-object v0
.end method

.method private a(Lrx/g;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/g",
            "<",
            "Lcom/twitter/model/moments/p;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 72
    .line 73
    invoke-static {}, Lcom/twitter/android/moments/ui/fullscreen/ax;->b()Lrx/functions/d;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/g;->b(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ax;->d:Lxj;

    iget-object v1, v1, Lxj;->a:Lrx/f;

    .line 74
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ax;->d:Lxj;

    iget-object v1, v1, Lxj;->b:Lrx/f;

    .line 75
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 76
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/ax;->c()Lcqw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ax;->h:Lrx/j;

    .line 77
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/fullscreen/ax;)Lcom/twitter/android/moments/ui/fullscreen/co;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ax;->g:Lcom/twitter/android/moments/ui/fullscreen/co;

    return-object v0
.end method

.method private static b()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<",
            "Lcom/twitter/model/moments/p;",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 81
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/ax$1;

    invoke-direct {v0}, Lcom/twitter/android/moments/ui/fullscreen/ax$1;-><init>()V

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/fullscreen/ax;)Lcom/twitter/android/moments/data/n;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ax;->c:Lcom/twitter/android/moments/data/n;

    return-object v0
.end method

.method private c()Lcqw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcqw",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/ax$2;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/fullscreen/ax$2;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ax;)V

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/moments/ui/fullscreen/ax;)Lzr;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ax;->f:Lzr;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/moments/ui/fullscreen/ax;)Lcom/twitter/app/users/URTUsersActivity$b;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ax;->e:Lcom/twitter/app/users/URTUsersActivity$b;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ax;->h:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 142
    return-void
.end method
