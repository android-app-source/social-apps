.class public Lcom/twitter/android/moments/ui/fullscreen/dj;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

.field private final b:Lcom/twitter/android/moments/ui/fullscreen/ScaleToFitFrameLayout;

.field private c:F

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/twitter/media/ui/image/AspectRatioFrameLayout;Lcom/twitter/android/moments/ui/fullscreen/ScaleToFitFrameLayout;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/dj;->a:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    .line 32
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/dj;->b:Lcom/twitter/android/moments/ui/fullscreen/ScaleToFitFrameLayout;

    .line 33
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/dj;)F
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dj;->c:F

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/dj;F)F
    .locals 0

    .prologue
    .line 20
    iput p1, p0, Lcom/twitter/android/moments/ui/fullscreen/dj;->c:F

    return p1
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/dj;I)I
    .locals 0

    .prologue
    .line 20
    iput p1, p0, Lcom/twitter/android/moments/ui/fullscreen/dj;->d:I

    return p1
.end method

.method private a(Lace;)V
    .locals 5

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 88
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dj;->e:Z

    if-eqz v0, :cond_0

    .line 89
    const/4 v0, 0x0

    .line 96
    :goto_0
    invoke-interface {p1, v0}, Lace;->a(I)V

    .line 97
    new-instance v2, Laai;

    invoke-direct {v2}, Laai;-><init>()V

    .line 98
    iget-object v3, v2, Laai;->a:Laan;

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {v3, v1, v4, v0}, Laan;->a(FFF)V

    .line 99
    iget-object v0, v2, Laai;->b:Laan;

    invoke-virtual {v0, v1, v4, v4}, Laan;->a(FFF)V

    .line 100
    invoke-interface {p1, v2}, Lace;->a(Laai;)V

    .line 101
    return-void

    .line 91
    :cond_0
    iget v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dj;->c:F

    cmpl-float v0, v0, v4

    if-lez v0, :cond_1

    iget v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dj;->c:F

    .line 92
    :goto_1
    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/dj;->b:Lcom/twitter/android/moments/ui/fullscreen/ScaleToFitFrameLayout;

    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/fullscreen/ScaleToFitFrameLayout;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/dj;->b:Lcom/twitter/android/moments/ui/fullscreen/ScaleToFitFrameLayout;

    .line 93
    invoke-virtual {v3}, Lcom/twitter/android/moments/ui/fullscreen/ScaleToFitFrameLayout;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v0, v3, v0

    float-to-int v0, v0

    sub-int v0, v2, v0

    .line 94
    iget v2, p0, Lcom/twitter/android/moments/ui/fullscreen/dj;->d:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 91
    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/dj;Lace;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/dj;->a(Lace;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/dj;Z)Z
    .locals 0

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/twitter/android/moments/ui/fullscreen/dj;->e:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/fullscreen/dj;)Lcom/twitter/media/ui/image/AspectRatioFrameLayout;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dj;->a:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/fullscreen/dj;)Lcom/twitter/android/moments/ui/fullscreen/ScaleToFitFrameLayout;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dj;->b:Lcom/twitter/android/moments/ui/fullscreen/ScaleToFitFrameLayout;

    return-object v0
.end method


# virtual methods
.method public a()Labj;
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/dj$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/fullscreen/dj$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/dj;)V

    return-object v0
.end method
