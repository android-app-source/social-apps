.class public Lcom/twitter/android/moments/ui/fullscreen/aq;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/moments/core/ui/widget/sectionpager/a;


# instance fields
.field private final a:Lcom/twitter/moments/core/ui/widget/sectionpager/a;


# direct methods
.method public constructor <init>(Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;Lcom/twitter/android/moments/ui/fullscreen/ci;Lcom/twitter/android/moments/data/h;Lcom/twitter/android/moments/ui/fullscreen/cu;Lcom/twitter/android/moments/ui/fullscreen/w;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iget-object v0, p1, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->a:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    sget-object v1, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;->e:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 37
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    .line 38
    invoke-virtual {p3, v0}, Lcom/twitter/android/moments/data/h;->b(Lcom/twitter/model/core/Tweet;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 39
    new-instance v1, Lcom/twitter/android/moments/viewmodels/j;

    .line 40
    invoke-virtual {p3, v0}, Lcom/twitter/android/moments/data/h;->a(Lcom/twitter/model/core/Tweet;)Lbrc;

    move-result-object v0

    invoke-direct {v1, p1, v0}, Lcom/twitter/android/moments/viewmodels/j;-><init>(Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;Lbrc;)V

    .line 41
    invoke-virtual {p5, p1, v1, p4}, Lcom/twitter/android/moments/ui/fullscreen/w;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/android/moments/viewmodels/k;Lcom/twitter/android/moments/ui/fullscreen/cu;)Lcom/twitter/moments/core/ui/widget/sectionpager/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aq;->a:Lcom/twitter/moments/core/ui/widget/sectionpager/a;

    .line 47
    :goto_1
    return-void

    .line 36
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 44
    :cond_1
    invoke-virtual {p5, p1, p2, p4}, Lcom/twitter/android/moments/ui/fullscreen/w;->a(Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;Lcom/twitter/android/moments/ui/fullscreen/ci;Lcom/twitter/android/moments/ui/fullscreen/cu;)Lcom/twitter/moments/core/ui/widget/sectionpager/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aq;->a:Lcom/twitter/moments/core/ui/widget/sectionpager/a;

    goto :goto_1
.end method

.method public static a(Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;Lcom/twitter/android/moments/ui/fullscreen/ci;Lcom/twitter/android/moments/ui/fullscreen/cu;Lcom/twitter/android/moments/ui/fullscreen/w;)Lcom/twitter/android/moments/ui/fullscreen/aq;
    .locals 6

    .prologue
    .line 26
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/aq;

    new-instance v3, Lcom/twitter/android/moments/data/h;

    new-instance v1, Lbrd;

    invoke-direct {v1}, Lbrd;-><init>()V

    invoke-direct {v3, v1}, Lcom/twitter/android/moments/data/h;-><init>(Lbrd;)V

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/moments/ui/fullscreen/aq;-><init>(Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;Lcom/twitter/android/moments/ui/fullscreen/ci;Lcom/twitter/android/moments/data/h;Lcom/twitter/android/moments/ui/fullscreen/cu;Lcom/twitter/android/moments/ui/fullscreen/w;)V

    return-object v0
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aq;->a:Lcom/twitter/moments/core/ui/widget/sectionpager/a;

    invoke-interface {v0}, Lcom/twitter/moments/core/ui/widget/sectionpager/a;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aq;->a:Lcom/twitter/moments/core/ui/widget/sectionpager/a;

    invoke-interface {v0, p1}, Lcom/twitter/moments/core/ui/widget/sectionpager/a;->a(F)V

    .line 73
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aq;->a:Lcom/twitter/moments/core/ui/widget/sectionpager/a;

    invoke-interface {v0}, Lcom/twitter/moments/core/ui/widget/sectionpager/a;->b()V

    .line 58
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aq;->a:Lcom/twitter/moments/core/ui/widget/sectionpager/a;

    invoke-interface {v0}, Lcom/twitter/moments/core/ui/widget/sectionpager/a;->c()V

    .line 63
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/aq;->a:Lcom/twitter/moments/core/ui/widget/sectionpager/a;

    invoke-interface {v0}, Lcom/twitter/moments/core/ui/widget/sectionpager/a;->d()V

    .line 68
    return-void
.end method
