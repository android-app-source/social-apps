.class public Lcom/twitter/android/moments/ui/fullscreen/cb;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Lcom/twitter/android/moments/ui/fullscreen/cc;


# instance fields
.field private final b:Landroid/app/Activity;

.field private final c:Lcom/twitter/android/card/b;

.field private final d:Lyf;

.field private final e:Lbxo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/cc$a;

    invoke-direct {v0}, Lcom/twitter/android/moments/ui/fullscreen/cc$a;-><init>()V

    sput-object v0, Lcom/twitter/android/moments/ui/fullscreen/cb;->a:Lcom/twitter/android/moments/ui/fullscreen/cc;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/android/card/b;Lyf;Lbxo;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/cb;->b:Landroid/app/Activity;

    .line 33
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/cb;->c:Lcom/twitter/android/card/b;

    .line 34
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/cb;->d:Lyf;

    .line 35
    iput-object p4, p0, Lcom/twitter/android/moments/ui/fullscreen/cb;->e:Lbxo;

    .line 36
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/cb;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cb;->b:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/android/moments/ui/fullscreen/cc;
    .locals 6

    .prologue
    const v4, 0x7f130518

    .line 41
    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/twitter/model/core/Tweet;->w:Lcom/twitter/model/core/r;

    if-eqz v0, :cond_0

    .line 42
    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/cq;

    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/cr;

    .line 43
    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {v2, v0}, Lcom/twitter/android/moments/ui/fullscreen/cr;-><init>(Landroid/view/ViewGroup;)V

    iget-object v0, p2, Lcom/twitter/model/core/Tweet;->w:Lcom/twitter/model/core/r;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/cb;->e:Lbxo;

    invoke-direct {v1, v2, v0, v3}, Lcom/twitter/android/moments/ui/fullscreen/cq;-><init>(Lcom/twitter/android/moments/ui/fullscreen/cr;Lcom/twitter/model/core/r;Lbxo;)V

    move-object v0, v1

    .line 79
    :goto_0
    return-object v0

    .line 46
    :cond_0
    instance-of v0, p3, Lcom/twitter/model/moments/viewmodels/m;

    if-eqz v0, :cond_1

    .line 47
    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/ck;

    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {v1, v0, p2}, Lcom/twitter/android/moments/ui/fullscreen/ck;-><init>(Landroid/view/ViewGroup;Lcom/twitter/model/core/Tweet;)V

    move-object v0, v1

    goto :goto_0

    .line 50
    :cond_1
    invoke-static {}, Lbsd;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p3}, Lcom/twitter/model/moments/viewmodels/MomentPage;->r()Lcom/twitter/model/moments/o;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 51
    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/az;

    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/ba;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/cb;->b:Landroid/app/Activity;

    .line 53
    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    new-instance v4, Lcom/twitter/android/moments/ui/fullscreen/cb$1;

    invoke-direct {v4, p0}, Lcom/twitter/android/moments/ui/fullscreen/cb$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/cb;)V

    new-instance v5, Lcom/twitter/android/moments/ui/fullscreen/cb$2;

    invoke-direct {v5, p0}, Lcom/twitter/android/moments/ui/fullscreen/cb$2;-><init>(Lcom/twitter/android/moments/ui/fullscreen/cb;)V

    invoke-direct {v2, v3, v0, v4, v5}, Lcom/twitter/android/moments/ui/fullscreen/ba;-><init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/twitter/util/object/d;Lcom/twitter/util/object/d;)V

    .line 70
    invoke-virtual {p3}, Lcom/twitter/model/moments/viewmodels/MomentPage;->g()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p3}, Lcom/twitter/model/moments/viewmodels/MomentPage;->r()Lcom/twitter/model/moments/o;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/cb;->d:Lyf;

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/twitter/android/moments/ui/fullscreen/az;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ba;Ljava/lang/Long;Lcom/twitter/model/moments/o;Lyf;)V

    move-object v0, v1

    .line 51
    goto :goto_0

    .line 73
    :cond_2
    invoke-virtual {p3}, Lcom/twitter/model/moments/viewmodels/MomentPage;->q()Lcom/twitter/model/moments/n;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 74
    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/dm;

    new-instance v2, Lach;

    .line 76
    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {v2, v0}, Lach;-><init>(Landroid/view/ViewGroup;)V

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cb;->c:Lcom/twitter/android/card/b;

    .line 77
    invoke-virtual {p3}, Lcom/twitter/model/moments/viewmodels/MomentPage;->q()Lcom/twitter/model/moments/n;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lcom/twitter/android/moments/ui/fullscreen/dm;-><init>(Lach;Lcom/twitter/android/card/b;Lcom/twitter/model/moments/n;)V

    move-object v0, v1

    .line 74
    goto :goto_0

    .line 79
    :cond_3
    sget-object v0, Lcom/twitter/android/moments/ui/fullscreen/cb;->a:Lcom/twitter/android/moments/ui/fullscreen/cc;

    goto :goto_0
.end method
