.class Lcom/twitter/android/moments/ui/fullscreen/ah$8;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Lcom/twitter/util/collection/k;Lcom/twitter/model/moments/r;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Lcom/twitter/util/collection/m",
        "<",
        "Lcom/twitter/model/moments/viewmodels/b;",
        "Lcom/twitter/model/moments/b;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/model/moments/r;

.field final synthetic b:Lcom/twitter/util/collection/k;

.field final synthetic c:Lcom/twitter/android/moments/ui/fullscreen/ah;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/ah;Lcom/twitter/model/moments/r;Lcom/twitter/util/collection/k;)V
    .locals 0

    .prologue
    .line 370
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$8;->c:Lcom/twitter/android/moments/ui/fullscreen/ah;

    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$8;->a:Lcom/twitter/model/moments/r;

    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$8;->b:Lcom/twitter/util/collection/k;

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/util/collection/m;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 374
    invoke-virtual {p1}, Lcom/twitter/util/collection/m;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 375
    invoke-virtual {p1}, Lcom/twitter/util/collection/m;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/b;

    .line 376
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$8;->c:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/fullscreen/ah;->h(Lcom/twitter/android/moments/ui/fullscreen/ah;)Lcom/twitter/android/moments/ui/fullscreen/x;

    move-result-object v1

    iget-object v2, v0, Lcom/twitter/model/moments/viewmodels/b;->a:Lcom/twitter/model/moments/viewmodels/a;

    invoke-interface {v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/x;->a(Lcom/twitter/model/moments/viewmodels/a;)Lcom/twitter/model/moments/viewmodels/a;

    move-result-object v2

    .line 377
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$8;->a:Lcom/twitter/model/moments/r;

    if-nez v1, :cond_1

    .line 378
    invoke-static {}, Lcom/twitter/util/collection/k;->a()Lcom/twitter/util/collection/k;

    move-result-object v1

    .line 380
    :goto_0
    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$8;->b:Lcom/twitter/util/collection/k;

    .line 383
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/b;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/k;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 381
    invoke-virtual {v3, v0}, Lcom/twitter/util/collection/k;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 384
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$8;->c:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/fullscreen/ah;->i(Lcom/twitter/android/moments/ui/fullscreen/ah;)Lcom/twitter/ui/anim/p;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$8;->c:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/fullscreen/ah;->i(Lcom/twitter/android/moments/ui/fullscreen/ah;)Lcom/twitter/ui/anim/p;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/ui/anim/p;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 386
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$8;->c:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-static {v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Lcom/twitter/android/moments/ui/fullscreen/ah;Lcom/twitter/model/moments/viewmodels/a;)Lcom/twitter/model/moments/viewmodels/a;

    .line 387
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$8;->c:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-static {v1, v0}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Lcom/twitter/android/moments/ui/fullscreen/ah;I)I

    .line 397
    :cond_0
    :goto_1
    return-void

    .line 378
    :cond_1
    invoke-virtual {v2}, Lcom/twitter/model/moments/viewmodels/a;->f()Ljava/util/List;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$8;->a:Lcom/twitter/model/moments/r;

    invoke-static {v1, v3}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Ljava/util/List;Lcom/twitter/model/moments/r;)Lcom/twitter/util/collection/k;

    move-result-object v1

    goto :goto_0

    .line 388
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$8;->c:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Lcom/twitter/android/moments/ui/fullscreen/ah;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 389
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$8;->c:Lcom/twitter/android/moments/ui/fullscreen/ah;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$8;->c:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-static {v3}, Lcom/twitter/android/moments/ui/fullscreen/ah;->f(Lcom/twitter/android/moments/ui/fullscreen/ah;)Lyj;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/k;->a(Ljava/lang/Object;)Lcom/twitter/util/collection/k;

    move-result-object v0

    invoke-static {v1, v2, v3, v0}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Lcom/twitter/android/moments/ui/fullscreen/ah;Lcom/twitter/model/moments/viewmodels/a;Lyj;Lcom/twitter/util/collection/k;)V

    goto :goto_1

    .line 391
    :cond_3
    invoke-virtual {p1}, Lcom/twitter/util/collection/m;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/b;

    invoke-virtual {v0}, Lcom/twitter/model/moments/b;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 392
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$8;->c:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/ah;->j(Lcom/twitter/android/moments/ui/fullscreen/ah;)Lcom/twitter/android/moments/ui/fullscreen/t;

    move-result-object v1

    .line 393
    invoke-virtual {p1}, Lcom/twitter/util/collection/m;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/b;

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/fullscreen/t;->b(Lcom/twitter/model/moments/b;)V

    goto :goto_1

    .line 395
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$8;->c:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/ah;->k(Lcom/twitter/android/moments/ui/fullscreen/ah;)Labg;

    move-result-object v0

    const v1, 0x7f0a0588

    invoke-virtual {v0, v1}, Labg;->a(I)V

    goto :goto_1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 370
    check-cast p1, Lcom/twitter/util/collection/m;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ah$8;->a(Lcom/twitter/util/collection/m;)V

    return-void
.end method
