.class public Lcom/twitter/android/moments/ui/fullscreen/ae;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/moments/core/ui/widget/sectionpager/d;


# instance fields
.field private final a:Lcom/twitter/android/moments/data/e;

.field private final b:Labl;

.field private final c:Lcom/twitter/android/moments/ui/fullscreen/cf;

.field private final d:Lcom/twitter/android/moments/ui/fullscreen/bg;


# direct methods
.method constructor <init>(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/moments/Moment;Labl;Lcom/twitter/android/moments/data/e;Lcom/twitter/android/moments/ui/fullscreen/cf;Lcom/twitter/android/moments/ui/fullscreen/bg;)V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/ae;->b:Labl;

    .line 58
    iput-object p4, p0, Lcom/twitter/android/moments/ui/fullscreen/ae;->a:Lcom/twitter/android/moments/data/e;

    .line 59
    iput-object p5, p0, Lcom/twitter/android/moments/ui/fullscreen/ae;->c:Lcom/twitter/android/moments/ui/fullscreen/cf;

    .line 60
    iput-object p6, p0, Lcom/twitter/android/moments/ui/fullscreen/ae;->d:Lcom/twitter/android/moments/ui/fullscreen/bg;

    .line 62
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ae;->b:Labl;

    .line 63
    invoke-virtual {v0}, Labl;->e()Labk;

    move-result-object v0

    invoke-virtual {v0}, Labk;->h()Landroid/view/ViewGroup;

    move-result-object v0

    .line 64
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ae;->c:Lcom/twitter/android/moments/ui/fullscreen/cf;

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/fullscreen/cf;->a(Landroid/view/ViewGroup;)V

    .line 65
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/moments/ui/fullscreen/ae;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/moments/Moment;)V

    .line 66
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/moments/Moment;Lcom/twitter/android/moments/data/w;Lzq;Lcom/twitter/android/moments/ui/fullscreen/cf;Lcom/twitter/android/moments/ui/fullscreen/bg;)Lcom/twitter/android/moments/ui/fullscreen/ae;
    .locals 7

    .prologue
    .line 35
    .line 36
    invoke-static {p0, p1}, Labl;->a(Landroid/view/LayoutInflater;Lcom/twitter/model/moments/viewmodels/MomentPage;)Labl;

    move-result-object v3

    .line 37
    new-instance v0, Laae;

    .line 38
    invoke-virtual {v3}, Labl;->e()Labk;

    move-result-object v1

    invoke-virtual {v1}, Labk;->a()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-direct {v0, v1}, Laae;-><init>(Landroid/view/View;)V

    .line 39
    new-instance v4, Lcom/twitter/android/moments/data/e;

    invoke-direct {v4, v0, p3, p4}, Lcom/twitter/android/moments/data/e;-><init>(Laae;Lcom/twitter/android/moments/data/w;Lzq;)V

    .line 41
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/ae;

    move-object v1, p1

    move-object v2, p2

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/moments/ui/fullscreen/ae;-><init>(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/moments/Moment;Labl;Lcom/twitter/android/moments/data/e;Lcom/twitter/android/moments/ui/fullscreen/cf;Lcom/twitter/android/moments/ui/fullscreen/bg;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/ae;)Lcom/twitter/android/moments/ui/fullscreen/bg;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ae;->d:Lcom/twitter/android/moments/ui/fullscreen/bg;

    return-object v0
.end method

.method private a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/moments/Moment;)V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ae;->b:Labl;

    invoke-virtual {v0}, Labl;->e()Labk;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/ae$1;

    invoke-direct {v1, p0, p2}, Lcom/twitter/android/moments/ui/fullscreen/ae$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ae;Lcom/twitter/model/moments/Moment;)V

    invoke-virtual {v0, v1}, Labk;->c(Landroid/view/View$OnClickListener;)V

    .line 81
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ae;->b:Labl;

    invoke-static {p1}, Lcom/twitter/model/moments/viewmodels/h;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Labl;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/core/Tweet;)V

    .line 82
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ae;->a:Lcom/twitter/android/moments/data/e;

    invoke-virtual {v0, p2}, Lcom/twitter/android/moments/data/e;->a(Lcom/twitter/model/moments/Moment;)V

    .line 83
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ae;->a:Lcom/twitter/android/moments/data/e;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/e;->a()V

    .line 98
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ae;->c:Lcom/twitter/android/moments/ui/fullscreen/cf;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/cf;->c()V

    .line 99
    return-void
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ae;->b:Labl;

    invoke-virtual {v0}, Labl;->d()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ae;->c:Lcom/twitter/android/moments/ui/fullscreen/cf;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/cf;->a()V

    .line 88
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ae;->c:Lcom/twitter/android/moments/ui/fullscreen/cf;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/cf;->b()V

    .line 93
    return-void
.end method
