.class public Lcom/twitter/android/moments/ui/fullscreen/az;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/fullscreen/cc;


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/fullscreen/ba;

.field private final b:Ljava/lang/Long;

.field private final c:Lcom/twitter/model/moments/o;

.field private final d:Lyf;

.field private final e:Lrx/subjects/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/a",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/cc;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lrx/j;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/ba;Ljava/lang/Long;Lcom/twitter/model/moments/o;Lyf;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/az;->a:Lcom/twitter/android/moments/ui/fullscreen/ba;

    .line 30
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/az;->b:Ljava/lang/Long;

    .line 31
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/az;->c:Lcom/twitter/model/moments/o;

    .line 32
    iput-object p4, p0, Lcom/twitter/android/moments/ui/fullscreen/az;->d:Lyf;

    .line 33
    invoke-static {}, Lrx/subjects/a;->r()Lrx/subjects/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/az;->e:Lrx/subjects/a;

    .line 34
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/az;)Lrx/subjects/a;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/az;->e:Lrx/subjects/a;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/fullscreen/az;)Lcom/twitter/android/moments/ui/fullscreen/ba;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/az;->a:Lcom/twitter/android/moments/ui/fullscreen/ba;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/fullscreen/az;)Lcom/twitter/model/moments/o;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/az;->c:Lcom/twitter/model/moments/o;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 38
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/az;->a:Lcom/twitter/android/moments/ui/fullscreen/ba;

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/az;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/az;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/az;->c:Lcom/twitter/model/moments/o;

    iget-object v0, v0, Lcom/twitter/model/moments/o;->b:Lcom/twitter/model/moments/Moment;

    iget-wide v4, v0, Lcom/twitter/model/moments/Moment;->b:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/fullscreen/ba;->a(Z)V

    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/az;->a:Lcom/twitter/android/moments/ui/fullscreen/ba;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/az;->c:Lcom/twitter/model/moments/o;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/ba;->a(Lcom/twitter/model/moments/o;)V

    .line 40
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/az;->d:Lyf;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/az;->c:Lcom/twitter/model/moments/o;

    iget-object v1, v1, Lcom/twitter/model/moments/o;->c:Lceo;

    iget-wide v2, v1, Lceo;->k:J

    invoke-virtual {v0, v2, v3}, Lyf;->a(J)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/az$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/fullscreen/az$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/az;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/az;->f:Lrx/j;

    .line 52
    return-void

    .line 38
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/cc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/az;->e:Lrx/subjects/a;

    invoke-virtual {v0}, Lrx/subjects/a;->b()Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/az;->f:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 63
    return-void
.end method
