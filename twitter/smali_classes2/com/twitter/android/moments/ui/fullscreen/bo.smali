.class public Lcom/twitter/android/moments/ui/fullscreen/bo;
.super Lcom/twitter/android/moments/ui/fullscreen/bj;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/p;

.field private final c:Lcne;

.field private final d:Labt;

.field private final e:Lcom/twitter/android/moments/ui/fullscreen/ca;

.field private f:Lcom/twitter/model/core/TwitterUser;

.field private g:Lain;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/p;Labt;Lcom/twitter/android/moments/ui/fullscreen/ca;ZZ)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p3}, Lcom/twitter/android/moments/ui/fullscreen/bj;-><init>(Labu;)V

    .line 48
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->a:Landroid/content/Context;

    .line 49
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->b:Lcom/twitter/library/client/p;

    .line 50
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    .line 51
    iput-object p4, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->e:Lcom/twitter/android/moments/ui/fullscreen/ca;

    .line 52
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    invoke-virtual {v0, p5}, Labt;->f(Z)V

    .line 53
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    invoke-virtual {v0, p6}, Labt;->g(Z)V

    .line 55
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/bo$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/fullscreen/bo$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/bo;)V

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->c:Lcne;

    .line 67
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/bo;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->a:Landroid/content/Context;

    return-object v0
.end method

.method private a(J)V
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 180
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->a:Landroid/content/Context;

    const/4 v7, -0x1

    move-wide v2, p1

    move-object v5, v4

    move-object v6, v4

    move-object v8, v4

    move-object v9, v4

    invoke-static/range {v1 .. v9}, Lcom/twitter/android/ProfileActivity;->a(Landroid/content/Context;JLjava/lang/String;Lcgi;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ILcom/twitter/library/api/PromotedEvent;Lcom/twitter/model/timeline/r;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/fullscreen/bo;->a(Landroid/content/Intent;)V

    .line 182
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->a:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {p1, v1}, Lcom/twitter/app/common/base/h;->a(Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 186
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/bo;J)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/moments/ui/fullscreen/bo;->a(J)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/bo;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/bo;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 175
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->a:Landroid/content/Context;

    const-wide/16 v2, 0x0

    const/4 v7, -0x1

    move-object v4, p1

    move-object v6, v5

    move-object v8, v5

    move-object v9, v5

    invoke-static/range {v1 .. v9}, Lcom/twitter/android/ProfileActivity;->a(Landroid/content/Context;JLjava/lang/String;Lcgi;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ILcom/twitter/library/api/PromotedEvent;Lcom/twitter/model/timeline/r;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/fullscreen/bo;->a(Landroid/content/Intent;)V

    .line 177
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/fullscreen/bo;)Lcom/twitter/model/core/TwitterUser;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->f:Lcom/twitter/model/core/TwitterUser;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/fullscreen/bo;)Lcom/twitter/android/moments/ui/fullscreen/ca;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->e:Lcom/twitter/android/moments/ui/fullscreen/ca;

    return-object v0
.end method


# virtual methods
.method public a(Lain;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->g:Lain;

    .line 78
    return-void
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;)V
    .locals 1

    .prologue
    .line 70
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->f:Lcom/twitter/model/core/TwitterUser;

    .line 71
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    invoke-virtual {v0}, Labt;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/fullscreen/bo;->c()V

    .line 74
    :cond_0
    return-void
.end method

.method public c()V
    .locals 9

    .prologue
    const/4 v8, -0x1

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 82
    invoke-super {p0}, Lcom/twitter/android/moments/ui/fullscreen/bj;->c()V

    .line 83
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->f:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_6

    .line 84
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    invoke-virtual {v0, v7}, Labt;->a(Z)V

    .line 85
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    invoke-virtual {v0, v6}, Labt;->b(Z)V

    .line 86
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->f:Lcom/twitter/model/core/TwitterUser;

    iget-object v1, v1, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Labt;->a(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->f:Lcom/twitter/model/core/TwitterUser;

    iget-object v1, v1, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Labt;->a(Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->a:Landroid/content/Context;

    const v2, 0x7f0a0b92

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->f:Lcom/twitter/model/core/TwitterUser;

    iget-object v4, v4, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Labt;->b(Ljava/lang/CharSequence;)V

    .line 89
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f110104

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Labt;->a(I)V

    .line 90
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->f:Lcom/twitter/model/core/TwitterUser;

    iget v1, v1, Lcom/twitter/model/core/TwitterUser;->u:I

    int-to-long v2, v1

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->f:Lcom/twitter/model/core/TwitterUser;

    iget v1, v1, Lcom/twitter/model/core/TwitterUser;->R:I

    int-to-long v4, v1

    invoke-virtual {v0, v2, v3, v4, v5}, Labt;->a(JJ)V

    .line 92
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->f:Lcom/twitter/model/core/TwitterUser;

    iget-boolean v1, v1, Lcom/twitter/model/core/TwitterUser;->m:Z

    invoke-virtual {v0, v1}, Labt;->c(Z)V

    .line 94
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->f:Lcom/twitter/model/core/TwitterUser;

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 95
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    invoke-virtual {v0, v7}, Labt;->d(Z)V

    .line 96
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->f:Lcom/twitter/model/core/TwitterUser;

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->C:Lcom/twitter/model/core/v;

    .line 97
    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    invoke-virtual {v1}, Lcom/twitter/model/core/f;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/twitter/model/core/v;->e:Lcom/twitter/model/core/f;

    invoke-virtual {v0}, Lcom/twitter/model/core/f;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->f:Lcom/twitter/model/core/TwitterUser;

    iget-object v1, v1, Lcom/twitter/model/core/TwitterUser;->f:Ljava/lang/String;

    .line 99
    invoke-static {v1}, Lcnf;->a(Ljava/lang/CharSequence;)Lcnf;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->f:Lcom/twitter/model/core/TwitterUser;

    iget-object v2, v2, Lcom/twitter/model/core/TwitterUser;->C:Lcom/twitter/model/core/v;

    .line 100
    invoke-virtual {v1, v2}, Lcnf;->a(Lcom/twitter/model/core/v;)Lcnf;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->c:Lcne;

    .line 101
    invoke-virtual {v1, v2}, Lcnf;->a(Lcne;)Lcnf;

    move-result-object v1

    .line 102
    invoke-virtual {v1, v8}, Lcnf;->a(I)Lcnf;

    move-result-object v1

    .line 103
    invoke-virtual {v1}, Lcnf;->a()Landroid/text/SpannableStringBuilder;

    move-result-object v1

    .line 98
    invoke-virtual {v0, v1}, Labt;->c(Ljava/lang/CharSequence;)V

    .line 104
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    invoke-virtual {v0}, Labt;->a()V

    .line 112
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->f:Lcom/twitter/model/core/TwitterUser;

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->p:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 113
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    invoke-virtual {v0, v7}, Labt;->e(Z)V

    .line 114
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->f:Lcom/twitter/model/core/TwitterUser;

    iget-object v1, v1, Lcom/twitter/model/core/TwitterUser;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Labt;->d(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    invoke-virtual {v0, v8}, Labt;->b(I)V

    .line 120
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/bo$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/fullscreen/bo$2;-><init>(Lcom/twitter/android/moments/ui/fullscreen/bo;)V

    invoke-virtual {v0, v1}, Labt;->a(Landroid/view/View$OnClickListener;)V

    .line 128
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    .line 129
    invoke-virtual {v0}, Labt;->b()Labs;

    move-result-object v1

    .line 131
    if-eqz v1, :cond_5

    .line 132
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/bn;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->b:Lcom/twitter/library/client/p;

    invoke-direct {v0, v2, v3, v1}, Lcom/twitter/android/moments/ui/fullscreen/bn;-><init>(Landroid/content/Context;Lcom/twitter/library/client/p;Labs;)V

    .line 134
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->f:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/bn;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 139
    :goto_2
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    .line 140
    invoke-virtual {v1}, Labt;->c()Lcom/twitter/android/periscope/l;

    move-result-object v1

    .line 141
    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->g:Lain;

    if-eqz v2, :cond_1

    .line 142
    new-instance v2, Lcom/twitter/android/periscope/p;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->a:Landroid/content/Context;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->g:Lain;

    invoke-direct {v2, v3, v4, v1}, Lcom/twitter/android/periscope/p;-><init>(Landroid/content/Context;Lain;Lcom/twitter/android/periscope/l;)V

    .line 145
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->f:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v2, v1}, Lcom/twitter/android/periscope/p;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 147
    if-eqz v0, :cond_1

    .line 148
    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/bo$3;

    invoke-direct {v1, p0, v2}, Lcom/twitter/android/moments/ui/fullscreen/bo$3;-><init>(Lcom/twitter/android/moments/ui/fullscreen/bo;Lcom/twitter/android/periscope/p;)V

    .line 149
    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/bn;->a(Lcom/twitter/android/moments/ui/fullscreen/bn$a;)V

    .line 162
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/bo$4;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/fullscreen/bo$4;-><init>(Lcom/twitter/android/moments/ui/fullscreen/bo;)V

    invoke-virtual {v0, v1}, Labt;->b(Landroid/view/View$OnClickListener;)V

    .line 172
    :goto_3
    return-void

    .line 106
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->f:Lcom/twitter/model/core/TwitterUser;

    iget-object v1, v1, Lcom/twitter/model/core/TwitterUser;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Labt;->c(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 109
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    invoke-virtual {v0, v6}, Labt;->d(Z)V

    goto :goto_0

    .line 117
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    invoke-virtual {v0, v6}, Labt;->e(Z)V

    goto :goto_1

    .line 136
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 169
    :cond_6
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    invoke-virtual {v0, v6}, Labt;->a(Z)V

    .line 170
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bo;->d:Labt;

    invoke-virtual {v0, v7}, Labt;->b(Z)V

    goto :goto_3
.end method

.method protected d()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 191
    const v0, 0x7f040219

    return v0
.end method
