.class public Lcom/twitter/android/moments/ui/fullscreen/ah;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/fullscreen/ah$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/q",
        "<",
        "Lcom/twitter/util/collection/Pair",
        "<",
        "Ljava/lang/String;",
        "Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final A:Labg;

.field private final B:Lcom/twitter/android/moments/ui/b;

.field private final C:Lcom/twitter/android/moments/ui/fullscreen/r;

.field private final D:Lcom/twitter/android/moments/ui/fullscreen/t;

.field private final E:Lyf;

.field private final F:Lcom/twitter/android/moments/ui/fullscreen/co;

.field private final G:Lcom/twitter/android/moments/ui/fullscreen/x;

.field private final H:Lrx/j;

.field private final I:Lcom/twitter/android/av/k;

.field private final J:Lcom/twitter/android/moments/ui/fullscreen/bm;

.field private final K:Lcom/twitter/android/moments/ui/fullscreen/InternalFeedbackDialogsController;

.field private final L:Lcom/twitter/ui/anim/p;

.field private final M:Landroid/os/Bundle;

.field private final N:Lcom/twitter/android/moments/ui/fullscreen/by;

.field private final O:Landroid/view/View;

.field private P:Lcom/twitter/model/moments/viewmodels/a;

.field private Q:I

.field private final R:J

.field private final a:Lcom/twitter/android/ck;

.field private final b:Lcom/twitter/android/moments/ui/fullscreen/bd;

.field private final c:Landroid/view/ViewGroup;

.field private final d:Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;

.field private final e:Lcom/twitter/android/moments/ui/fullscreen/bs;

.field private final f:Lcom/twitter/android/moments/ui/fullscreen/ak;

.field private final g:Lcom/twitter/util/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/twitter/android/moments/ui/DoubleTapFavoriteHud;

.field private final i:Lcom/twitter/android/moments/data/i;

.field private final j:Landroid/support/v4/app/FragmentActivity;

.field private final k:Lcom/twitter/library/client/v;

.field private final l:Lcom/twitter/app/common/util/j;

.field private final m:Lcom/twitter/android/moments/ui/fullscreen/be;

.field private final n:Lcom/twitter/android/moments/ui/fullscreen/v;

.field private final o:Lcom/twitter/android/moments/ui/fullscreen/bz;

.field private final p:Lcom/twitter/android/moments/ui/fullscreen/ad;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/moments/ui/fullscreen/ad",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final q:Lbsb;

.field private final r:Lcom/twitter/moments/core/ui/widget/sectionpager/e;

.field private final s:Labm;

.field private final t:Lzz;

.field private final u:Lcom/twitter/android/moments/ui/fullscreen/ai;

.field private final v:Lyj;

.field private final w:Lcom/twitter/util/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;"
        }
    .end annotation
.end field

.field private final x:Lcom/twitter/android/moments/data/am;

.field private final y:Lcom/twitter/android/moments/ui/fullscreen/aj;

.field private final z:Lxm;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/ui/widget/touchintercept/TouchInterceptingFrameLayout;Lcom/twitter/android/ck;Lcom/twitter/library/client/v;JLcom/twitter/library/provider/t;Lcom/twitter/app/common/util/j;JLcom/twitter/android/moments/ui/fullscreen/ad;Lcom/twitter/android/moments/ui/fullscreen/bz;Lcom/twitter/android/moments/data/i;Landroid/os/Bundle;Lcom/twitter/ui/anim/p;Lcom/twitter/android/moments/data/am;Lxm;Labg;Lcom/twitter/android/moments/ui/b;Lcom/twitter/android/moments/ui/fullscreen/r;Lcom/twitter/android/moments/ui/fullscreen/t;Lyf;Lcom/twitter/android/moments/ui/fullscreen/co;Lcom/twitter/android/moments/ui/fullscreen/x;Lcif;Lcom/twitter/android/av/k;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentActivity;",
            "Lcom/twitter/ui/widget/touchintercept/TouchInterceptingFrameLayout;",
            "Lcom/twitter/android/ck;",
            "Lcom/twitter/library/client/v;",
            "J",
            "Lcom/twitter/library/provider/t;",
            "Lcom/twitter/app/common/util/j;",
            "J",
            "Lcom/twitter/android/moments/ui/fullscreen/ad",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;",
            ">;",
            "Lcom/twitter/android/moments/ui/fullscreen/bz;",
            "Lcom/twitter/android/moments/data/i;",
            "Landroid/os/Bundle;",
            "Lcom/twitter/ui/anim/p;",
            "Lcom/twitter/android/moments/data/am;",
            "Lxm;",
            "Labg;",
            "Lcom/twitter/android/moments/ui/b;",
            "Lcom/twitter/android/moments/ui/fullscreen/r;",
            "Lcom/twitter/android/moments/ui/fullscreen/t;",
            "Lyf;",
            "Lcom/twitter/android/moments/ui/fullscreen/co;",
            "Lcom/twitter/android/moments/ui/fullscreen/x;",
            "Lcif;",
            "Lcom/twitter/android/av/k;",
            ")V"
        }
    .end annotation

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->j:Landroid/support/v4/app/FragmentActivity;

    .line 149
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->a:Lcom/twitter/android/ck;

    .line 150
    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->k:Lcom/twitter/library/client/v;

    .line 151
    const v4, 0x7f13050c

    invoke-virtual {p2, v4}, Lcom/twitter/ui/widget/touchintercept/TouchInterceptingFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    iput-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->c:Landroid/view/ViewGroup;

    .line 152
    const v4, 0x7f13050e

    invoke-virtual {p2, v4}, Lcom/twitter/ui/widget/touchintercept/TouchInterceptingFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;

    iput-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->d:Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;

    .line 153
    new-instance v4, Lcom/twitter/android/moments/ui/fullscreen/ak;

    invoke-static {}, Lcom/twitter/android/moments/ui/fullscreen/bv;->a()Lcom/twitter/android/moments/ui/fullscreen/bv;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/twitter/android/moments/ui/fullscreen/ak;-><init>(Lcom/twitter/android/moments/ui/fullscreen/bv;)V

    iput-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->f:Lcom/twitter/android/moments/ui/fullscreen/ak;

    .line 154
    new-instance v4, Lcom/twitter/android/moments/ui/fullscreen/bd;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->j:Landroid/support/v4/app/FragmentActivity;

    invoke-static {}, Lcom/twitter/library/av/playback/q;->a()Lcom/twitter/library/av/playback/q;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/twitter/android/moments/ui/fullscreen/bd;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/q;)V

    iput-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->b:Lcom/twitter/android/moments/ui/fullscreen/bd;

    .line 155
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->i:Lcom/twitter/android/moments/data/i;

    .line 156
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->l:Lcom/twitter/app/common/util/j;

    .line 157
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->o:Lcom/twitter/android/moments/ui/fullscreen/bz;

    .line 158
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->p:Lcom/twitter/android/moments/ui/fullscreen/ad;

    .line 159
    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->p:Lcom/twitter/android/moments/ui/fullscreen/ad;

    invoke-virtual {v4, p0}, Lcom/twitter/android/moments/ui/fullscreen/ad;->a(Lcom/twitter/util/q;)V

    .line 160
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->L:Lcom/twitter/ui/anim/p;

    .line 161
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->x:Lcom/twitter/android/moments/data/am;

    .line 162
    move-wide/from16 v0, p9

    iput-wide v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->R:J

    .line 163
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->z:Lxm;

    .line 164
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->A:Labg;

    .line 165
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->D:Lcom/twitter/android/moments/ui/fullscreen/t;

    .line 166
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->E:Lyf;

    .line 167
    new-instance v4, Lbsb;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->j:Landroid/support/v4/app/FragmentActivity;

    move-object/from16 v0, p7

    move-wide/from16 v1, p5

    invoke-direct {v4, v5, v0, v1, v2}, Lbsb;-><init>(Landroid/content/Context;Lcom/twitter/library/provider/t;J)V

    iput-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->q:Lbsb;

    .line 168
    const v4, 0x7f13050d

    .line 169
    invoke-virtual {p2, v4}, Lcom/twitter/ui/widget/touchintercept/TouchInterceptingFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/twitter/moments/core/ui/widget/sectionpager/SectionsView;

    .line 170
    new-instance v5, Lcom/twitter/moments/core/ui/widget/sectionpager/e;

    iget-object v6, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->d:Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;

    .line 171
    invoke-static {}, Lcob;->a()Lcob;

    move-result-object v7

    invoke-direct {v5, v6, v4, v7}, Lcom/twitter/moments/core/ui/widget/sectionpager/e;-><init>(Lcom/twitter/ui/widget/ScrollEventsFilteringViewPager;Lcom/twitter/moments/core/ui/widget/sectionpager/SectionsView;Lcob;)V

    iput-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->r:Lcom/twitter/moments/core/ui/widget/sectionpager/e;

    .line 172
    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->r:Lcom/twitter/moments/core/ui/widget/sectionpager/e;

    const v5, 0x7f0e0362

    invoke-virtual {v4, v5}, Lcom/twitter/moments/core/ui/widget/sectionpager/e;->a(I)V

    .line 173
    new-instance v4, Lcom/twitter/android/moments/ui/fullscreen/bs;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->r:Lcom/twitter/moments/core/ui/widget/sectionpager/e;

    iget-object v6, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->d:Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;

    invoke-direct {v4, v5, v6}, Lcom/twitter/android/moments/ui/fullscreen/bs;-><init>(Lcom/twitter/moments/core/ui/widget/sectionpager/e;Landroid/support/v4/view/ViewPager;)V

    iput-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->e:Lcom/twitter/android/moments/ui/fullscreen/bs;

    .line 174
    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->d:Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->r:Lcom/twitter/moments/core/ui/widget/sectionpager/e;

    invoke-virtual {v5}, Lcom/twitter/moments/core/ui/widget/sectionpager/e;->c()Lcom/twitter/moments/core/ui/widget/sectionpager/e$a;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 175
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->B:Lcom/twitter/android/moments/ui/b;

    .line 176
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->C:Lcom/twitter/android/moments/ui/fullscreen/r;

    .line 177
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->F:Lcom/twitter/android/moments/ui/fullscreen/co;

    .line 178
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->G:Lcom/twitter/android/moments/ui/fullscreen/x;

    .line 179
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->I:Lcom/twitter/android/av/k;

    .line 181
    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->k:Lcom/twitter/library/client/v;

    invoke-virtual {v4}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v6

    .line 182
    new-instance v4, Lyj;

    new-instance v5, Laug;

    new-instance v7, Lwx;

    iget-object v8, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->j:Landroid/support/v4/app/FragmentActivity;

    invoke-direct {v7, v8, v6}, Lwx;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-direct {v5, v7}, Laug;-><init>(Lauj;)V

    new-instance v7, Laug;

    new-instance v8, Lww;

    .line 186
    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v9

    invoke-direct {v8, v9}, Lww;-><init>(Lcom/twitter/library/provider/t;)V

    invoke-direct {v7, v8}, Laug;-><init>(Lauj;)V

    invoke-direct {v4, v5, v7}, Lyj;-><init>(Lauj;Lauj;)V

    iput-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->v:Lyj;

    .line 188
    new-instance v7, Lcom/twitter/android/moments/ui/fullscreen/ag;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->d:Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->e:Lcom/twitter/android/moments/ui/fullscreen/bs;

    invoke-direct {v7, v4, v5}, Lcom/twitter/android/moments/ui/fullscreen/ag;-><init>(Landroid/support/v4/view/ViewPager;Lcom/twitter/model/moments/viewmodels/i;)V

    .line 191
    new-instance v4, Lcom/twitter/android/moments/ui/fullscreen/by;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->o:Lcom/twitter/android/moments/ui/fullscreen/bz;

    invoke-direct {v4, v5, v7}, Lcom/twitter/android/moments/ui/fullscreen/by;-><init>(Lcom/twitter/android/moments/ui/fullscreen/bz;Lcom/twitter/moments/core/ui/widget/capsule/a;)V

    iput-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->N:Lcom/twitter/android/moments/ui/fullscreen/by;

    .line 193
    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->d:Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->N:Lcom/twitter/android/moments/ui/fullscreen/by;

    invoke-virtual {v4, v5}, Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 195
    const v4, 0x7f130512

    invoke-virtual {p2, v4}, Lcom/twitter/ui/widget/touchintercept/TouchInterceptingFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/twitter/android/moments/ui/DoubleTapFavoriteHud;

    iput-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->h:Lcom/twitter/android/moments/ui/DoubleTapFavoriteHud;

    .line 197
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v4

    invoke-virtual {v4}, Lcof;->p()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 198
    new-instance v4, Lcom/twitter/android/moments/ui/fullscreen/InternalFeedbackDialogsController;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->j:Landroid/support/v4/app/FragmentActivity;

    invoke-direct {v4, v5, v7}, Lcom/twitter/android/moments/ui/fullscreen/InternalFeedbackDialogsController;-><init>(Landroid/content/Context;Lcom/twitter/moments/core/ui/widget/capsule/a;)V

    iput-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->K:Lcom/twitter/android/moments/ui/fullscreen/InternalFeedbackDialogsController;

    .line 203
    :goto_0
    const v4, 0x7f13050f

    invoke-virtual {p2, v4}, Lcom/twitter/ui/widget/touchintercept/TouchInterceptingFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 204
    new-instance v5, Lcom/twitter/android/moments/ui/fullscreen/ah$1;

    invoke-direct {v5, p0}, Lcom/twitter/android/moments/ui/fullscreen/ah$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ah;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    const v4, 0x7f130402

    invoke-virtual {p2, v4}, Lcom/twitter/ui/widget/touchintercept/TouchInterceptingFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/twitter/android/widget/ToggleImageButton;

    .line 214
    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->f:Lcom/twitter/android/moments/ui/fullscreen/ak;

    invoke-virtual {v5}, Lcom/twitter/android/moments/ui/fullscreen/ak;->a()Z

    move-result v5

    if-nez v5, :cond_1

    const/4 v5, 0x1

    :goto_1
    invoke-virtual {v4, v5}, Lcom/twitter/android/widget/ToggleImageButton;->setToggledOn(Z)V

    .line 215
    new-instance v5, Lcom/twitter/android/moments/ui/fullscreen/ah$a;

    iget-object v8, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->f:Lcom/twitter/android/moments/ui/fullscreen/ak;

    invoke-direct {v5, v8, v4}, Lcom/twitter/android/moments/ui/fullscreen/ah$a;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ak;Lcom/twitter/android/widget/ToggleImageButton;)V

    invoke-virtual {v4, v5}, Lcom/twitter/android/widget/ToggleImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 216
    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->f:Lcom/twitter/android/moments/ui/fullscreen/ak;

    invoke-virtual {v5}, Lcom/twitter/android/moments/ui/fullscreen/ak;->c()Lcom/twitter/util/p;

    move-result-object v5

    new-instance v8, Lcom/twitter/android/moments/ui/fullscreen/ah$3;

    invoke-direct {v8, p0, v4}, Lcom/twitter/android/moments/ui/fullscreen/ah$3;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ah;Lcom/twitter/android/widget/ToggleImageButton;)V

    invoke-virtual {v5, v8}, Lcom/twitter/util/p;->a(Lcom/twitter/util/q;)Z

    .line 225
    invoke-static {p2}, Labm;->a(Landroid/view/ViewGroup;)Labm;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->s:Labm;

    .line 226
    new-instance v4, Lcom/twitter/util/p;

    invoke-direct {v4}, Lcom/twitter/util/p;-><init>()V

    iput-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->g:Lcom/twitter/util/p;

    .line 228
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v4

    .line 229
    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->d:Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 231
    const v5, 0x7f13050a

    invoke-virtual {p2, v5}, Lcom/twitter/ui/widget/touchintercept/TouchInterceptingFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->O:Landroid/view/View;

    .line 232
    const v5, 0x7f13050b

    invoke-virtual {p2, v5}, Lcom/twitter/ui/widget/touchintercept/TouchInterceptingFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 233
    new-instance v8, Lcom/twitter/android/moments/ui/fullscreen/ah$4;

    invoke-direct {v8, p0}, Lcom/twitter/android/moments/ui/fullscreen/ah$4;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ah;)V

    .line 245
    new-instance v9, Lcom/twitter/ui/widget/touchintercept/b;

    new-instance v10, Lcom/twitter/ui/widget/touchintercept/a;

    iget-object v11, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->j:Landroid/support/v4/app/FragmentActivity;

    iget-object v12, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->O:Landroid/view/View;

    invoke-direct {v10, v11, v5, v12, v8}, Lcom/twitter/ui/widget/touchintercept/a;-><init>(Landroid/app/Activity;Landroid/view/View;Landroid/view/View;Lcom/twitter/ui/widget/touchintercept/a$a;)V

    invoke-direct {v9, p1, v10}, Lcom/twitter/ui/widget/touchintercept/b;-><init>(Landroid/content/Context;Lcom/twitter/ui/widget/touchintercept/d$a;)V

    .line 249
    invoke-virtual {p2, v9}, Lcom/twitter/ui/widget/touchintercept/TouchInterceptingFrameLayout;->setTouchInterceptListener(Lcom/twitter/ui/widget/touchintercept/c;)V

    .line 250
    invoke-interface {v4, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 252
    new-instance v5, Lcom/twitter/android/moments/ui/fullscreen/ai;

    invoke-direct {v5, v4}, Lcom/twitter/android/moments/ui/fullscreen/ai;-><init>(Ljava/util/Set;)V

    iput-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->u:Lcom/twitter/android/moments/ui/fullscreen/ai;

    .line 253
    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->j:Landroid/support/v4/app/FragmentActivity;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->e:Lcom/twitter/android/moments/ui/fullscreen/bs;

    iget-object v8, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->g:Lcom/twitter/util/p;

    iget-object v10, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->u:Lcom/twitter/android/moments/ui/fullscreen/ai;

    iget-object v11, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->o:Lcom/twitter/android/moments/ui/fullscreen/bz;

    invoke-static {v4, v5, v8, v10, v11}, Lcom/twitter/android/moments/ui/fullscreen/be;->a(Landroid/content/Context;Lcom/twitter/model/moments/viewmodels/i;Lcom/twitter/util/p;Lcom/twitter/android/moments/ui/fullscreen/ai;Lcom/twitter/android/moments/ui/fullscreen/bz;)Lcom/twitter/android/moments/ui/fullscreen/be;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->m:Lcom/twitter/android/moments/ui/fullscreen/be;

    .line 257
    invoke-static/range {p5 .. p6}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v4

    .line 256
    invoke-static {v4}, Lbse;->a(Lcom/twitter/library/provider/t;)Lbse;

    move-result-object v4

    .line 258
    new-instance v5, Lzz;

    iget-object v8, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->e:Lcom/twitter/android/moments/ui/fullscreen/bs;

    move-wide/from16 v0, p5

    invoke-direct {v5, v0, v1, v8, v4}, Lzz;-><init>(JLcom/twitter/android/moments/ui/fullscreen/bs;Lbse;)V

    iput-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->t:Lzz;

    .line 261
    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->d:Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->t:Lzz;

    invoke-virtual {v4, v5}, Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 263
    new-instance v4, Lcom/twitter/android/moments/ui/fullscreen/ah$5;

    invoke-direct {v4, p0}, Lcom/twitter/android/moments/ui/fullscreen/ah$5;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ah;)V

    .line 271
    new-instance v5, Lcom/twitter/android/moments/ui/fullscreen/v;

    iget-object v8, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->d:Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;

    iget-object v10, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->e:Lcom/twitter/android/moments/ui/fullscreen/bs;

    invoke-direct {v5, v8, v10, v4}, Lcom/twitter/android/moments/ui/fullscreen/v;-><init>(Landroid/support/v4/view/ViewPager;Lcom/twitter/android/moments/ui/fullscreen/bs;Lcom/twitter/util/object/d;)V

    iput-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->n:Lcom/twitter/android/moments/ui/fullscreen/v;

    .line 274
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v10

    .line 275
    invoke-interface {v10, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 277
    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->j:Landroid/support/v4/app/FragmentActivity;

    const-string/jumbo v5, "tap_to_fit_tutorial_fatigue"

    .line 279
    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    .line 277
    invoke-static {v4, v5, v8, v9}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v6

    .line 280
    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->j:Landroid/support/v4/app/FragmentActivity;

    const v5, 0x7f130513

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewStub;

    .line 281
    invoke-virtual {v6}, Lcom/twitter/android/util/h;->a()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 282
    invoke-virtual {v4}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/twitter/android/moments/ui/fullscreen/TapHintView;

    .line 283
    new-instance v4, Lcom/twitter/android/moments/ui/fullscreen/bm;

    iget-object v8, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->g:Lcom/twitter/util/p;

    iget-object v9, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->p:Lcom/twitter/android/moments/ui/fullscreen/ad;

    invoke-direct/range {v4 .. v9}, Lcom/twitter/android/moments/ui/fullscreen/bm;-><init>(Lcom/twitter/android/moments/ui/fullscreen/TapHintView;Lcom/twitter/android/util/h;Lcom/twitter/moments/core/ui/widget/capsule/a;Lcom/twitter/util/p;Lcom/twitter/android/moments/ui/fullscreen/ad;)V

    iput-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->J:Lcom/twitter/android/moments/ui/fullscreen/bm;

    .line 286
    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->d:Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->J:Lcom/twitter/android/moments/ui/fullscreen/bm;

    invoke-virtual {v4, v5}, Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 287
    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->J:Lcom/twitter/android/moments/ui/fullscreen/bm;

    invoke-interface {v10, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 292
    :goto_2
    new-instance v4, Lcom/twitter/android/moments/ui/fullscreen/aj;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->r:Lcom/twitter/moments/core/ui/widget/sectionpager/e;

    new-instance v6, Lcom/twitter/android/moments/ui/fullscreen/bt;

    iget-object v7, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->j:Landroid/support/v4/app/FragmentActivity;

    invoke-direct {v6, v7}, Lcom/twitter/android/moments/ui/fullscreen/bt;-><init>(Landroid/app/Activity;)V

    invoke-direct {v4, v5, v10, v6}, Lcom/twitter/android/moments/ui/fullscreen/aj;-><init>(Lcom/twitter/moments/core/ui/widget/sectionpager/e;Ljava/util/Set;Lcom/twitter/android/moments/ui/fullscreen/bt;)V

    iput-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->y:Lcom/twitter/android/moments/ui/fullscreen/aj;

    .line 294
    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->g:Lcom/twitter/util/p;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->y:Lcom/twitter/android/moments/ui/fullscreen/aj;

    invoke-virtual {v4, v5}, Lcom/twitter/util/p;->a(Lcom/twitter/util/q;)Z

    .line 296
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->M:Landroid/os/Bundle;

    .line 298
    new-instance v4, Lcom/twitter/android/moments/ui/fullscreen/bw;

    new-instance v5, Lcom/twitter/android/moments/ui/e;

    iget-object v6, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->d:Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;

    invoke-direct {v5, v6}, Lcom/twitter/android/moments/ui/e;-><init>(Landroid/support/v4/view/ViewPager;)V

    invoke-direct {v4, v5}, Lcom/twitter/android/moments/ui/fullscreen/bw;-><init>(Lcom/twitter/android/moments/ui/e;)V

    iput-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->w:Lcom/twitter/util/q;

    .line 299
    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->g:Lcom/twitter/util/p;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->w:Lcom/twitter/util/q;

    invoke-virtual {v4, v5}, Lcom/twitter/util/p;->a(Lcom/twitter/util/q;)Z

    .line 302
    move-object/from16 v0, p25

    move-wide/from16 v1, p9

    invoke-virtual {v0, v1, v2}, Lcif;->e(J)Lrx/c;

    move-result-object v4

    new-instance v5, Lcom/twitter/android/moments/ui/fullscreen/ah$6;

    invoke-direct {v5, p0}, Lcom/twitter/android/moments/ui/fullscreen/ah$6;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ah;)V

    .line 303
    invoke-virtual {v4, v5}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->H:Lrx/j;

    .line 309
    return-void

    .line 200
    :cond_0
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->K:Lcom/twitter/android/moments/ui/fullscreen/InternalFeedbackDialogsController;

    goto/16 :goto_0

    .line 214
    :cond_1
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 289
    :cond_2
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->J:Lcom/twitter/android/moments/ui/fullscreen/bm;

    goto :goto_2
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/ah;I)I
    .locals 0

    .prologue
    .line 83
    iput p1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->Q:I

    return p1
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/ah;)Landroid/support/v4/app/FragmentActivity;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->j:Landroid/support/v4/app/FragmentActivity;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/ah;Lcom/twitter/model/moments/viewmodels/a;Lcom/twitter/util/collection/k;Landroid/os/Bundle;)Lcom/twitter/android/moments/ui/fullscreen/u;
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Lcom/twitter/model/moments/viewmodels/a;Lcom/twitter/util/collection/k;Landroid/os/Bundle;)Lcom/twitter/android/moments/ui/fullscreen/u;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/ah;Lcom/twitter/model/moments/viewmodels/a;Lcom/twitter/util/collection/k;Lcom/twitter/android/moments/ui/fullscreen/bs;Landroid/os/Bundle;)Lcom/twitter/android/moments/ui/fullscreen/u;
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Lcom/twitter/model/moments/viewmodels/a;Lcom/twitter/util/collection/k;Lcom/twitter/android/moments/ui/fullscreen/bs;Landroid/os/Bundle;)Lcom/twitter/android/moments/ui/fullscreen/u;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/twitter/model/moments/viewmodels/a;Lcom/twitter/util/collection/k;Landroid/os/Bundle;)Lcom/twitter/android/moments/ui/fullscreen/u;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/viewmodels/a;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/os/Bundle;",
            ")",
            "Lcom/twitter/android/moments/ui/fullscreen/u;"
        }
    .end annotation

    .prologue
    .line 543
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->M:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->M:Landroid/os/Bundle;

    const-string/jumbo v1, "state_main_pager_current_item"

    .line 544
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/k;->a(Ljava/lang/Object;)Lcom/twitter/util/collection/k;

    move-result-object p2

    .line 545
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->e:Lcom/twitter/android/moments/ui/fullscreen/bs;

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Lcom/twitter/model/moments/viewmodels/a;Lcom/twitter/util/collection/k;Lcom/twitter/android/moments/ui/fullscreen/bs;Landroid/os/Bundle;)Lcom/twitter/android/moments/ui/fullscreen/u;

    move-result-object v0

    .line 547
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->M:Landroid/os/Bundle;

    if-eqz v1, :cond_1

    .line 548
    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/ah$2;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/moments/ui/fullscreen/ah$2;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ah;Lcom/twitter/android/moments/ui/fullscreen/u;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/u;->a(Lcom/twitter/android/moments/ui/fullscreen/u$a;)V

    .line 557
    :cond_1
    return-object v0
.end method

.method private a(Lcom/twitter/model/moments/viewmodels/a;Lcom/twitter/util/collection/k;Lcom/twitter/android/moments/ui/fullscreen/bs;Landroid/os/Bundle;)Lcom/twitter/android/moments/ui/fullscreen/u;
    .locals 33
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/viewmodels/a;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/twitter/android/moments/ui/fullscreen/bs;",
            "Landroid/os/Bundle;",
            ")",
            "Lcom/twitter/android/moments/ui/fullscreen/u;"
        }
    .end annotation

    .prologue
    .line 438
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->j:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 439
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->c:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->h:Lcom/twitter/android/moments/ui/DoubleTapFavoriteHud;

    .line 440
    invoke-static {v1, v2, v3}, Labf;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/DoubleTapFavoriteHud;)Labf;

    move-result-object v17

    .line 441
    const/16 v1, 0x8

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Labf;->a(I)V

    .line 442
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->c:Landroid/view/ViewGroup;

    invoke-virtual/range {v17 .. v17}, Labf;->h()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 443
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->c:Landroid/view/ViewGroup;

    invoke-virtual/range {v17 .. v17}, Labf;->g()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 444
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->c:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->j:Landroid/support/v4/app/FragmentActivity;

    const v3, 0x7f130511

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->bringChildToFront(Landroid/view/View;)V

    .line 445
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v8

    .line 446
    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/u;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->j:Landroid/support/v4/app/FragmentActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->c:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->q:Lbsb;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->k:Lcom/twitter/library/client/v;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->j:Landroid/support/v4/app/FragmentActivity;

    .line 448
    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->i:Lcom/twitter/android/moments/data/i;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->b:Lcom/twitter/android/moments/ui/fullscreen/bd;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->d:Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->r:Lcom/twitter/moments/core/ui/widget/sectionpager/e;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->a:Lcom/twitter/android/ck;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->B:Lcom/twitter/android/moments/ui/b;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->n:Lcom/twitter/android/moments/ui/fullscreen/v;

    move-object/from16 v16, v0

    .line 450
    invoke-virtual/range {v17 .. v17}, Labf;->h()Landroid/view/View;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->g:Lcom/twitter/util/p;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->p:Lcom/twitter/android/moments/ui/fullscreen/ad;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->f:Lcom/twitter/android/moments/ui/fullscreen/ak;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->o:Lcom/twitter/android/moments/ui/fullscreen/bz;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->s:Labm;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->v:Lyj;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->l:Lcom/twitter/app/common/util/j;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->x:Lcom/twitter/android/moments/data/am;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->C:Lcom/twitter/android/moments/ui/fullscreen/r;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->E:Lyf;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->F:Lcom/twitter/android/moments/ui/fullscreen/co;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->I:Lcom/twitter/android/av/k;

    move-object/from16 v32, v0

    move-object/from16 v4, p3

    move-object/from16 v6, p2

    move-object/from16 v26, p1

    move-object/from16 v27, p4

    invoke-direct/range {v1 .. v32}, Lcom/twitter/android/moments/ui/fullscreen/u;-><init>(Landroid/support/v4/app/FragmentActivity;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/fullscreen/bs;Lbsb;Lcom/twitter/util/collection/k;Lcom/twitter/library/client/v;Lcom/twitter/library/client/p;Landroid/support/v4/app/LoaderManager;Lcom/twitter/android/moments/data/i;Lcom/twitter/android/moments/ui/fullscreen/bd;Landroid/support/v4/view/ViewPager;Lcom/twitter/moments/core/ui/widget/sectionpager/e;Lcom/twitter/android/ck;Lcom/twitter/android/moments/ui/b;Lcom/twitter/android/moments/ui/fullscreen/br;Labf;Landroid/view/View;Lcom/twitter/util/p;Lcom/twitter/android/moments/ui/fullscreen/ad;Lcom/twitter/android/moments/ui/fullscreen/ak;Lcom/twitter/android/moments/ui/fullscreen/bz;Labm;Lyj;Lcom/twitter/app/common/util/j;Lcom/twitter/model/moments/viewmodels/a;Landroid/os/Bundle;Lcom/twitter/android/moments/data/am;Lcom/twitter/android/moments/ui/fullscreen/r;Lyf;Lcom/twitter/android/moments/ui/fullscreen/co;Lcom/twitter/android/av/k;)V

    .line 454
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/fullscreen/ah;->N:Lcom/twitter/android/moments/ui/fullscreen/by;

    invoke-virtual {v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/u;->a(Lcom/twitter/android/moments/ui/fullscreen/u$a;)V

    .line 455
    return-object v1
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/ah;Lcom/twitter/model/moments/viewmodels/a;)Lcom/twitter/model/moments/viewmodels/a;
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->P:Lcom/twitter/model/moments/viewmodels/a;

    return-object p1
.end method

.method static synthetic a(Ljava/util/List;Lcom/twitter/model/moments/r;)Lcom/twitter/util/collection/k;
    .locals 1

    .prologue
    .line 83
    invoke-static {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ah;->b(Ljava/util/List;Lcom/twitter/model/moments/r;)Lcom/twitter/util/collection/k;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/ah;Lcom/twitter/model/moments/viewmodels/a;Lyj;Lcom/twitter/util/collection/k;)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Lcom/twitter/model/moments/viewmodels/a;Lyj;Lcom/twitter/util/collection/k;)V

    return-void
.end method

.method private a(Lcom/twitter/model/moments/viewmodels/a;Lyj;Lcom/twitter/util/collection/k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/viewmodels/a;",
            "Lyj;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 418
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->D:Lcom/twitter/android/moments/ui/fullscreen/t;

    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/a;->a()Lcom/twitter/model/moments/Moment;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/model/moments/Moment;->o:Lcom/twitter/model/moments/a;

    invoke-virtual {v0, p2, v1}, Lcom/twitter/android/moments/ui/fullscreen/t;->a(Lyj;Lcom/twitter/model/moments/a;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/ah$10;

    invoke-direct {v1, p0, p1, p3}, Lcom/twitter/android/moments/ui/fullscreen/ah$10;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ah;Lcom/twitter/model/moments/viewmodels/a;Lcom/twitter/util/collection/k;)V

    .line 419
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    .line 433
    return-void
.end method

.method private a(Lcom/twitter/util/collection/k;Lcom/twitter/model/moments/r;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/collection/k",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/twitter/model/moments/r;",
            ")V"
        }
    .end annotation

    .prologue
    .line 325
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->M:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->f:Lcom/twitter/android/moments/ui/fullscreen/ak;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->M:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/ak;->b(Landroid/os/Bundle;)V

    .line 333
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->L:Lcom/twitter/ui/anim/p;

    if-eqz v0, :cond_1

    .line 334
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->L:Lcom/twitter/ui/anim/p;

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/ah$7;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/fullscreen/ah$7;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ah;)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/anim/p;->a(Lcom/twitter/ui/anim/p$a;)V

    .line 355
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->L:Lcom/twitter/ui/anim/p;

    invoke-virtual {v0}, Lcom/twitter/ui/anim/p;->a()V

    .line 361
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->M:Landroid/os/Bundle;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->M:Landroid/os/Bundle;

    const-string/jumbo v1, "state_current_moment_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 362
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->M:Landroid/os/Bundle;

    const-string/jumbo v1, "state_current_moment_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 366
    :goto_2
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_3

    .line 367
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Moment ID must be non-negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 330
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->b:Lcom/twitter/android/moments/ui/fullscreen/bd;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/bd;->b()V

    goto :goto_0

    .line 357
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->c:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1

    .line 364
    :cond_2
    iget-wide v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->R:J

    goto :goto_2

    .line 369
    :cond_3
    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->z:Lxm;

    invoke-interface {v2, v0, v1}, Lxm;->a(J)Lrx/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lrx/c;->d(I)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/ah$8;

    invoke-direct {v1, p0, p2, p1}, Lcom/twitter/android/moments/ui/fullscreen/ah$8;-><init>(Lcom/twitter/android/moments/ui/fullscreen/ah;Lcom/twitter/model/moments/r;Lcom/twitter/util/collection/k;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 399
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->d:Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->m:Lcom/twitter/android/moments/ui/fullscreen/be;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 400
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->d:Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->m:Lcom/twitter/android/moments/ui/fullscreen/be;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 401
    return-void
.end method

.method static a(Landroid/view/KeyEvent;Lcom/twitter/android/moments/ui/fullscreen/ak;)Z
    .locals 2

    .prologue
    .line 518
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x18

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/android/moments/ui/fullscreen/ak;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 536
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->e:Lcom/twitter/android/moments/ui/fullscreen/bs;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->d:Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/bs;->c(I)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 537
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/util/List;Lcom/twitter/model/moments/r;)Lcom/twitter/util/collection/k;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ">;",
            "Lcom/twitter/model/moments/r;",
            ")",
            "Lcom/twitter/util/collection/k",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 406
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/ah$9;

    invoke-direct {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ah$9;-><init>(Lcom/twitter/model/moments/r;)V

    invoke-static {p0, v0}, Lcpt;->c(Ljava/lang/Iterable;Lcpv;)I

    move-result v0

    .line 412
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/twitter/util/collection/k;->a()Lcom/twitter/util/collection/k;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/k;->a(Ljava/lang/Object;)Lcom/twitter/util/collection/k;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/fullscreen/ah;)Lzz;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->t:Lzz;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/fullscreen/ah;)Lcom/twitter/android/moments/ui/fullscreen/bs;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->e:Lcom/twitter/android/moments/ui/fullscreen/bs;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/moments/ui/fullscreen/ah;)Landroid/view/View;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->O:Landroid/view/View;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/moments/ui/fullscreen/ah;)Lcom/twitter/model/moments/viewmodels/a;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->P:Lcom/twitter/model/moments/viewmodels/a;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/moments/ui/fullscreen/ah;)Lyj;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->v:Lyj;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/moments/ui/fullscreen/ah;)I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->Q:I

    return v0
.end method

.method static synthetic h(Lcom/twitter/android/moments/ui/fullscreen/ah;)Lcom/twitter/android/moments/ui/fullscreen/x;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->G:Lcom/twitter/android/moments/ui/fullscreen/x;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/moments/ui/fullscreen/ah;)Lcom/twitter/ui/anim/p;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->L:Lcom/twitter/ui/anim/p;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/moments/ui/fullscreen/ah;)Lcom/twitter/android/moments/ui/fullscreen/t;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->D:Lcom/twitter/android/moments/ui/fullscreen/t;

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/moments/ui/fullscreen/ah;)Labg;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->A:Labg;

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/android/moments/ui/fullscreen/ah;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->M:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic m(Lcom/twitter/android/moments/ui/fullscreen/ah;)Lcom/twitter/android/moments/ui/fullscreen/v;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->n:Lcom/twitter/android/moments/ui/fullscreen/v;

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/android/moments/ui/fullscreen/ah;)Lcom/twitter/android/moments/ui/fullscreen/be;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->m:Lcom/twitter/android/moments/ui/fullscreen/be;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 588
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 589
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->g:Lcom/twitter/util/p;

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/Event;

    sget-object v2, Lcom/twitter/android/moments/ui/fullscreen/Event$EventType;->l:Lcom/twitter/android/moments/ui/fullscreen/Event$EventType;

    invoke-direct {v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/Event;-><init>(Lcom/twitter/android/moments/ui/fullscreen/Event$EventType;)V

    invoke-virtual {v0, v1}, Lcom/twitter/util/p;->a(Ljava/lang/Object;)V

    .line 593
    :goto_0
    return-void

    .line 591
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->g:Lcom/twitter/util/p;

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/Event;

    sget-object v2, Lcom/twitter/android/moments/ui/fullscreen/Event$EventType;->m:Lcom/twitter/android/moments/ui/fullscreen/Event$EventType;

    invoke-direct {v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/Event;-><init>(Lcom/twitter/android/moments/ui/fullscreen/Event$EventType;)V

    invoke-virtual {v0, v1}, Lcom/twitter/util/p;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 490
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->n:Lcom/twitter/android/moments/ui/fullscreen/v;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/v;->c()Lcom/twitter/android/moments/ui/fullscreen/u;

    move-result-object v0

    .line 491
    if-eqz v0, :cond_0

    .line 492
    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/u;->a(Landroid/os/Bundle;)V

    .line 493
    const-string/jumbo v1, "state_current_moment_id"

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/u;->c()Lcom/twitter/model/moments/viewmodels/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/a;->a()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    iget-wide v2, v0, Lcom/twitter/model/moments/Moment;->b:J

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 495
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->m:Lcom/twitter/android/moments/ui/fullscreen/be;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/be;->a(Landroid/os/Bundle;)V

    .line 496
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->f:Lcom/twitter/android/moments/ui/fullscreen/ak;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ak;->a(Landroid/os/Bundle;)V

    .line 497
    const-string/jumbo v0, "state_main_pager_current_item"

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->d:Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/fullscreen/MomentsViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 498
    return-void
.end method

.method public a(Landroid/view/KeyEvent;)V
    .locals 2

    .prologue
    .line 474
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->f:Lcom/twitter/android/moments/ui/fullscreen/ak;

    invoke-static {p1, v0}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Landroid/view/KeyEvent;Lcom/twitter/android/moments/ui/fullscreen/ak;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 475
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->f:Lcom/twitter/android/moments/ui/fullscreen/ak;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/ak;->a(Z)V

    .line 477
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/model/moments/r;)V
    .locals 1

    .prologue
    .line 320
    invoke-static {}, Lcom/twitter/util/collection/k;->a()Lcom/twitter/util/collection/k;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Lcom/twitter/util/collection/k;Lcom/twitter/model/moments/r;)V

    .line 321
    return-void
.end method

.method public a(Lcom/twitter/util/collection/k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/collection/k",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 316
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Lcom/twitter/util/collection/k;Lcom/twitter/model/moments/r;)V

    .line 317
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 501
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->g:Lcom/twitter/util/p;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->w:Lcom/twitter/util/q;

    invoke-virtual {v0, v1}, Lcom/twitter/util/p;->b(Lcom/twitter/util/q;)Z

    .line 502
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->n:Lcom/twitter/android/moments/ui/fullscreen/v;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/v;->e()V

    .line 503
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->b:Lcom/twitter/android/moments/ui/fullscreen/bd;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/bd;->a(Z)V

    .line 504
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->p:Lcom/twitter/android/moments/ui/fullscreen/ad;

    invoke-virtual {v0, p0}, Lcom/twitter/android/moments/ui/fullscreen/ad;->c(Lcom/twitter/util/q;)V

    .line 505
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->H:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 506
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->v:Lyj;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 507
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->J:Lcom/twitter/android/moments/ui/fullscreen/bm;

    if-eqz v0, :cond_0

    .line 508
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->J:Lcom/twitter/android/moments/ui/fullscreen/bm;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/bm;->c()V

    .line 510
    :cond_0
    if-nez p1, :cond_1

    .line 511
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->f:Lcom/twitter/android/moments/ui/fullscreen/ak;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ak;->b()V

    .line 513
    :cond_1
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 459
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/fullscreen/ah;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 460
    const/4 v0, 0x0

    .line 466
    :cond_0
    :goto_0
    return v0

    .line 462
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->n:Lcom/twitter/android/moments/ui/fullscreen/v;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/v;->a()Z

    move-result v0

    .line 463
    if-nez v0, :cond_0

    .line 464
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->t:Lzz;

    invoke-virtual {v1}, Lzz;->b()V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 561
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/fullscreen/ah;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 562
    if-eqz p1, :cond_0

    .line 563
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->j:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/d;->a(Landroid/view/View;)V

    .line 566
    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 470
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->L:Lcom/twitter/ui/anim/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->L:Lcom/twitter/ui/anim/p;

    invoke-virtual {v0}, Lcom/twitter/ui/anim/p;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Lcom/twitter/util/collection/k;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/collection/k",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 481
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->n:Lcom/twitter/android/moments/ui/fullscreen/v;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/v;->d()Lcom/twitter/model/moments/viewmodels/a;

    move-result-object v0

    .line 482
    if-eqz v0, :cond_0

    .line 483
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/a;->a()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    iget-wide v0, v0, Lcom/twitter/model/moments/Moment;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/k;->a(Ljava/lang/Object;)Lcom/twitter/util/collection/k;

    move-result-object v0

    .line 485
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/k;->a()Lcom/twitter/util/collection/k;

    move-result-object v0

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 571
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/fullscreen/ah;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 572
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->j:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/d;->a(Landroid/view/View;)V

    .line 574
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->b:Lcom/twitter/android/moments/ui/fullscreen/bd;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->f:Lcom/twitter/android/moments/ui/fullscreen/ak;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/fullscreen/ak;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/bd;->b(Z)V

    .line 575
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->K:Lcom/twitter/android/moments/ui/fullscreen/InternalFeedbackDialogsController;

    if-eqz v0, :cond_1

    .line 576
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->K:Lcom/twitter/android/moments/ui/fullscreen/InternalFeedbackDialogsController;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/InternalFeedbackDialogsController;->a()V

    .line 578
    :cond_1
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 581
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->b:Lcom/twitter/android/moments/ui/fullscreen/bd;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/bd;->a()V

    .line 582
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->K:Lcom/twitter/android/moments/ui/fullscreen/InternalFeedbackDialogsController;

    if-eqz v0, :cond_0

    .line 583
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->K:Lcom/twitter/android/moments/ui/fullscreen/InternalFeedbackDialogsController;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/InternalFeedbackDialogsController;->b()V

    .line 585
    :cond_0
    return-void
.end method

.method public onEvent(Lcom/twitter/util/collection/Pair;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 523
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->L:Lcom/twitter/ui/anim/p;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 524
    invoke-virtual {p1}, Lcom/twitter/util/collection/Pair;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;

    iget-object v0, v0, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;->b:Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;

    .line 525
    sget-object v1, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->f:Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->g:Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->e:Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;

    if-ne v0, v1, :cond_2

    .line 528
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->L:Lcom/twitter/ui/anim/p;

    invoke-virtual {v0}, Lcom/twitter/ui/anim/p;->b()V

    .line 533
    :cond_1
    :goto_0
    return-void

    .line 529
    :cond_2
    sget-object v1, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->c:Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;

    if-ne v0, v1, :cond_1

    .line 530
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah;->L:Lcom/twitter/ui/anim/p;

    invoke-virtual {v0}, Lcom/twitter/ui/anim/p;->c()V

    goto :goto_0
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 83
    check-cast p1, Lcom/twitter/util/collection/Pair;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ah;->onEvent(Lcom/twitter/util/collection/Pair;)V

    return-void
.end method
