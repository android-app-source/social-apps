.class Lcom/twitter/android/moments/ui/fullscreen/da;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/av/playback/q;Lbyf;)Lcom/twitter/android/moments/ui/video/a;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 24
    new-instance v0, Lcom/twitter/library/av/playback/v;

    invoke-direct {v0}, Lcom/twitter/library/av/playback/v;-><init>()V

    invoke-virtual {v0, p2}, Lcom/twitter/library/av/playback/v;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/library/av/playback/u;

    move-result-object v1

    .line 26
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const-string/jumbo v2, "moments"

    .line 27
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v2, "capsule"

    .line 28
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 29
    new-instance v2, Lcom/twitter/library/av/playback/r;

    invoke-direct {v2, p3}, Lcom/twitter/library/av/playback/r;-><init>(Lcom/twitter/library/av/playback/q;)V

    .line 30
    invoke-virtual {v2, v0}, Lcom/twitter/library/av/playback/r;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    .line 31
    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/r;->a(Lcom/twitter/library/av/playback/u;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    .line 32
    invoke-virtual {v0, v3}, Lcom/twitter/library/av/playback/r;->b(Z)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    .line 33
    invoke-virtual {v0, p4}, Lcom/twitter/library/av/playback/r;->a(Lbyf;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    .line 34
    invoke-virtual {v0, v3}, Lcom/twitter/library/av/playback/r;->a(Z)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    .line 35
    invoke-virtual {v0, p1}, Lcom/twitter/library/av/playback/r;->a(Landroid/content/Context;)Lcom/twitter/library/av/playback/r;

    move-result-object v0

    .line 36
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/r;->a()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v0

    .line 37
    new-instance v2, Lcom/twitter/android/moments/ui/video/a;

    invoke-virtual {v1}, Lcom/twitter/library/av/playback/u;->c()Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Lcom/twitter/android/moments/ui/video/a;-><init>(Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/playback/AVDataSource;)V

    return-object v2
.end method
