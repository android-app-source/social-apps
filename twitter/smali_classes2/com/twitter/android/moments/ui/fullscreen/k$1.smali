.class Lcom/twitter/android/moments/ui/fullscreen/k$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/fullscreen/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/q",
        "<",
        "Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$AudioStartInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/ui/fullscreen/k;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/k;)V
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/k$1;->a:Lcom/twitter/android/moments/ui/fullscreen/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEvent(Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$AudioStartInfo;)V
    .locals 7

    .prologue
    .line 24
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/k$1;->a:Lcom/twitter/android/moments/ui/fullscreen/k;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/k;->a(Lcom/twitter/android/moments/ui/fullscreen/k;)Lcom/twitter/model/moments/viewmodels/n;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$AudioStartInfo;->b:Lcom/twitter/model/moments/viewmodels/p;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p1, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$AudioStartInfo;->a:Lcom/twitter/model/av/AVMedia;

    check-cast v0, Lcom/twitter/model/av/Audio;

    .line 26
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/k$1;->a:Lcom/twitter/android/moments/ui/fullscreen/k;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/fullscreen/k;->c(Lcom/twitter/android/moments/ui/fullscreen/k;)Labe;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/k$1;->a:Lcom/twitter/android/moments/ui/fullscreen/k;

    invoke-static {v2}, Lcom/twitter/android/moments/ui/fullscreen/k;->b(Lcom/twitter/android/moments/ui/fullscreen/k;)Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0ba4

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 27
    invoke-virtual {v0}, Lcom/twitter/model/av/Audio;->n()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0}, Lcom/twitter/model/av/Audio;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 26
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Labe;->a(Ljava/lang/String;)V

    .line 29
    :cond_0
    return-void
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$AudioStartInfo;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/k$1;->onEvent(Lcom/twitter/android/moments/ui/fullscreen/CapsuleAudioController$AudioStartInfo;)V

    return-void
.end method
