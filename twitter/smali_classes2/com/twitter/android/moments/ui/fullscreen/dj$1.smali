.class Lcom/twitter/android/moments/ui/fullscreen/dj$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Labj;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/fullscreen/dj;->a()Labj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/ui/fullscreen/dj;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/dj;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/dj$1;->a:Lcom/twitter/android/moments/ui/fullscreen/dj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lace;)V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dj$1;->a:Lcom/twitter/android/moments/ui/fullscreen/dj;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dj;->c(Lcom/twitter/android/moments/ui/fullscreen/dj;)Lcom/twitter/android/moments/ui/fullscreen/ScaleToFitFrameLayout;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/ViewCompat;->isAttachedToWindow(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dj$1;->a:Lcom/twitter/android/moments/ui/fullscreen/dj;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dj;->c(Lcom/twitter/android/moments/ui/fullscreen/dj;)Lcom/twitter/android/moments/ui/fullscreen/ScaleToFitFrameLayout;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/dj$1$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/dj$1$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/dj$1;Lace;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/ScaleToFitFrameLayout;->post(Ljava/lang/Runnable;)Z

    .line 63
    :goto_0
    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dj$1;->a:Lcom/twitter/android/moments/ui/fullscreen/dj;

    invoke-static {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/dj;->a(Lcom/twitter/android/moments/ui/fullscreen/dj;Lace;)V

    goto :goto_0
.end method

.method public a(Lace;I)V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dj$1;->a:Lcom/twitter/android/moments/ui/fullscreen/dj;

    invoke-static {v0, p2}, Lcom/twitter/android/moments/ui/fullscreen/dj;->a(Lcom/twitter/android/moments/ui/fullscreen/dj;I)I

    .line 81
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dj$1;->a:Lcom/twitter/android/moments/ui/fullscreen/dj;

    invoke-static {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/dj;->a(Lcom/twitter/android/moments/ui/fullscreen/dj;Lace;)V

    .line 82
    return-void
.end method

.method public a(Lace;Lcom/twitter/util/math/Size;Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dj$1;->a:Lcom/twitter/android/moments/ui/fullscreen/dj;

    invoke-virtual {p2}, Lcom/twitter/util/math/Size;->g()F

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/dj;->a(Lcom/twitter/android/moments/ui/fullscreen/dj;F)F

    .line 42
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dj$1;->a:Lcom/twitter/android/moments/ui/fullscreen/dj;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dj;->b(Lcom/twitter/android/moments/ui/fullscreen/dj;)Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dj$1;->a:Lcom/twitter/android/moments/ui/fullscreen/dj;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/fullscreen/dj;->a(Lcom/twitter/android/moments/ui/fullscreen/dj;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;->setAspectRatio(F)V

    .line 43
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dj$1;->a:Lcom/twitter/android/moments/ui/fullscreen/dj;

    invoke-static {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/dj;->a(Lcom/twitter/android/moments/ui/fullscreen/dj;Lace;)V

    .line 44
    return-void
.end method

.method public a(Lace;Z)V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dj$1;->a:Lcom/twitter/android/moments/ui/fullscreen/dj;

    invoke-static {v0, p2}, Lcom/twitter/android/moments/ui/fullscreen/dj;->a(Lcom/twitter/android/moments/ui/fullscreen/dj;Z)Z

    .line 69
    if-eqz p2, :cond_0

    .line 70
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dj$1;->a:Lcom/twitter/android/moments/ui/fullscreen/dj;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dj;->c(Lcom/twitter/android/moments/ui/fullscreen/dj;)Lcom/twitter/android/moments/ui/fullscreen/ScaleToFitFrameLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dj$1;->a:Lcom/twitter/android/moments/ui/fullscreen/dj;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/fullscreen/dj;->a(Lcom/twitter/android/moments/ui/fullscreen/dj;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/ScaleToFitFrameLayout;->setAspectRatio(F)V

    .line 74
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dj$1;->a:Lcom/twitter/android/moments/ui/fullscreen/dj;

    invoke-static {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/dj;->a(Lcom/twitter/android/moments/ui/fullscreen/dj;Lace;)V

    .line 75
    return-void

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dj$1;->a:Lcom/twitter/android/moments/ui/fullscreen/dj;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dj;->c(Lcom/twitter/android/moments/ui/fullscreen/dj;)Lcom/twitter/android/moments/ui/fullscreen/ScaleToFitFrameLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ScaleToFitFrameLayout;->a()V

    goto :goto_0
.end method
