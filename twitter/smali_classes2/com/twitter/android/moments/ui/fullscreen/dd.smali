.class public Lcom/twitter/android/moments/ui/fullscreen/dd;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/model/core/Tweet;

.field private final b:Lcom/twitter/android/moments/ui/fullscreen/bl;

.field private final c:Lcom/twitter/util/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/twitter/android/moments/ui/fullscreen/de;


# direct methods
.method constructor <init>(Lcom/twitter/model/core/Tweet;Lcom/twitter/android/moments/ui/fullscreen/bl;Lcom/twitter/android/moments/ui/fullscreen/de;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/dd$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/fullscreen/dd$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/dd;)V

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dd;->c:Lcom/twitter/util/q;

    .line 30
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/dd;->b:Lcom/twitter/android/moments/ui/fullscreen/bl;

    .line 31
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/dd;->d:Lcom/twitter/android/moments/ui/fullscreen/de;

    .line 32
    invoke-virtual {p2}, Lcom/twitter/android/moments/ui/fullscreen/bl;->a()Lcom/twitter/util/p;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dd;->c:Lcom/twitter/util/q;

    invoke-virtual {v0, v1}, Lcom/twitter/util/p;->a(Lcom/twitter/util/q;)Z

    .line 33
    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/dd;->a(Lcom/twitter/model/core/Tweet;)V

    .line 34
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/widget/FrameLayout;Lcom/twitter/model/core/Tweet;Lcom/twitter/android/moments/ui/fullscreen/bl;Lbpl;)Lcom/twitter/android/moments/ui/fullscreen/dd;
    .locals 3

    .prologue
    .line 40
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/dd;

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/de;

    .line 42
    invoke-static {p0}, Lcom/twitter/android/moments/ui/animation/e;->a(Landroid/content/Context;)Lcom/twitter/android/moments/ui/animation/e;

    move-result-object v2

    invoke-direct {v1, p1, p3, v2, p4}, Lcom/twitter/android/moments/ui/fullscreen/de;-><init>(Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/fullscreen/bl;Lcom/twitter/android/moments/ui/animation/e;Lbpl;)V

    invoke-direct {v0, p2, p3, v1}, Lcom/twitter/android/moments/ui/fullscreen/dd;-><init>(Lcom/twitter/model/core/Tweet;Lcom/twitter/android/moments/ui/fullscreen/bl;Lcom/twitter/android/moments/ui/fullscreen/de;)V

    .line 40
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/dd;)Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dd;->a:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dd;->d:Lcom/twitter/android/moments/ui/fullscreen/de;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/de;->a()V

    .line 53
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dd;->b:Lcom/twitter/android/moments/ui/fullscreen/bl;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/bl;->a()Lcom/twitter/util/p;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dd;->c:Lcom/twitter/util/q;

    invoke-virtual {v0, v1}, Lcom/twitter/util/p;->b(Lcom/twitter/util/q;)Z

    .line 54
    return-void
.end method

.method protected a(Lcom/twitter/model/core/Tweet;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 47
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/dd;->a:Lcom/twitter/model/core/Tweet;

    .line 48
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dd;->d:Lcom/twitter/android/moments/ui/fullscreen/de;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/de;->a(Lcom/twitter/model/core/Tweet;)V

    .line 49
    return-void
.end method
