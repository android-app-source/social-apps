.class Lcom/twitter/android/moments/ui/fullscreen/ah$10;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Lcom/twitter/model/moments/viewmodels/a;Lyj;Lcom/twitter/util/collection/k;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/model/moments/viewmodels/a;

.field final synthetic b:Lcom/twitter/util/collection/k;

.field final synthetic c:Lcom/twitter/android/moments/ui/fullscreen/ah;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/ah;Lcom/twitter/model/moments/viewmodels/a;Lcom/twitter/util/collection/k;)V
    .locals 0

    .prologue
    .line 419
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$10;->c:Lcom/twitter/android/moments/ui/fullscreen/ah;

    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$10;->a:Lcom/twitter/model/moments/viewmodels/a;

    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$10;->b:Lcom/twitter/util/collection/k;

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Boolean;)V
    .locals 5

    .prologue
    .line 422
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$10;->c:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/ah;->m(Lcom/twitter/android/moments/ui/fullscreen/ah;)Lcom/twitter/android/moments/ui/fullscreen/v;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$10;->c:Lcom/twitter/android/moments/ui/fullscreen/ah;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$10;->a:Lcom/twitter/model/moments/viewmodels/a;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$10;->b:Lcom/twitter/util/collection/k;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$10;->c:Lcom/twitter/android/moments/ui/fullscreen/ah;

    .line 427
    invoke-static {v4}, Lcom/twitter/android/moments/ui/fullscreen/ah;->l(Lcom/twitter/android/moments/ui/fullscreen/ah;)Landroid/os/Bundle;

    move-result-object v4

    .line 424
    invoke-static {v1, v2, v3, v4}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Lcom/twitter/android/moments/ui/fullscreen/ah;Lcom/twitter/model/moments/viewmodels/a;Lcom/twitter/util/collection/k;Landroid/os/Bundle;)Lcom/twitter/android/moments/ui/fullscreen/u;

    move-result-object v1

    .line 423
    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/v;->a(Lcom/twitter/android/moments/ui/fullscreen/u;)V

    .line 431
    :goto_0
    return-void

    .line 429
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/ah$10;->c:Lcom/twitter/android/moments/ui/fullscreen/ah;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/ah;->a(Lcom/twitter/android/moments/ui/fullscreen/ah;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 419
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ah$10;->a(Ljava/lang/Boolean;)V

    return-void
.end method
