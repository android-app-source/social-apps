.class Lcom/twitter/android/moments/ui/fullscreen/bl$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/fullscreen/bl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/ui/fullscreen/bl;

.field private final b:Lcom/twitter/model/core/Tweet;

.field private final c:Lcom/twitter/util/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/bl;Lcom/twitter/model/core/Tweet;Lcom/twitter/util/p;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/Tweet;",
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 195
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/bl$a;->a:Lcom/twitter/android/moments/ui/fullscreen/bl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 196
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/bl$a;->b:Lcom/twitter/model/core/Tweet;

    .line 197
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/bl$a;->c:Lcom/twitter/util/p;

    .line 198
    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl$a;->b:Lcom/twitter/model/core/Tweet;

    iput-boolean p1, v0, Lcom/twitter/model/core/Tweet;->a:Z

    .line 203
    if-eqz p1, :cond_0

    .line 204
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl$a;->b:Lcom/twitter/model/core/Tweet;

    iget v1, v0, Lcom/twitter/model/core/Tweet;->n:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/twitter/model/core/Tweet;->n:I

    .line 208
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl$a;->c:Lcom/twitter/util/p;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bl$a;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, v1}, Lcom/twitter/util/p;->a(Ljava/lang/Object;)V

    .line 209
    return-void

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl$a;->b:Lcom/twitter/model/core/Tweet;

    iget v1, v0, Lcom/twitter/model/core/Tweet;->n:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/twitter/model/core/Tweet;->n:I

    goto :goto_0
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 213
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bl$a;->b:Lcom/twitter/model/core/Tweet;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v1, Lcom/twitter/model/core/Tweet;->c:Z

    .line 214
    if-nez p1, :cond_1

    .line 215
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl$a;->b:Lcom/twitter/model/core/Tweet;

    iget v1, v0, Lcom/twitter/model/core/Tweet;->k:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/twitter/model/core/Tweet;->k:I

    .line 219
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl$a;->c:Lcom/twitter/util/p;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bl$a;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, v1}, Lcom/twitter/util/p;->a(Ljava/lang/Object;)V

    .line 220
    return-void

    .line 213
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl$a;->b:Lcom/twitter/model/core/Tweet;

    iget v1, v0, Lcom/twitter/model/core/Tweet;->k:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/twitter/model/core/Tweet;->k:I

    goto :goto_1
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 224
    return-void
.end method

.method public d(Z)V
    .locals 4

    .prologue
    .line 228
    if-eqz p1, :cond_0

    .line 229
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/bl$a;->a:Lcom/twitter/android/moments/ui/fullscreen/bl;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/bl;->d(Lcom/twitter/android/moments/ui/fullscreen/bl;)Lcom/twitter/android/moments/ui/fullscreen/r;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/bl$a;->a:Lcom/twitter/android/moments/ui/fullscreen/bl;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/fullscreen/bl;->e(Lcom/twitter/android/moments/ui/fullscreen/bl;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/moments/ui/fullscreen/r;->a(J)V

    .line 231
    :cond_0
    return-void
.end method
