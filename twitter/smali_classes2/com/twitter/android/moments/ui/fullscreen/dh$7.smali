.class Lcom/twitter/android/moments/ui/fullscreen/dh$7;
.super Lcom/twitter/android/moments/ui/fullscreen/au;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/fullscreen/dh;->e()Lcom/twitter/util/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/ui/fullscreen/dh;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/dh;Laci;Lcom/twitter/android/moments/ui/fullscreen/cd;Lzn;)V
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$7;->a:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-direct {p0, p2, p3, p4}, Lcom/twitter/android/moments/ui/fullscreen/au;-><init>(Laci;Lcom/twitter/android/moments/ui/fullscreen/cd;Lzn;)V

    return-void
.end method


# virtual methods
.method public onEvent(Lcom/twitter/android/moments/ui/fullscreen/Event;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 230
    invoke-super {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/au;->onEvent(Lcom/twitter/android/moments/ui/fullscreen/Event;)V

    .line 231
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$7;->a:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->d(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->w()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$7;->a:Lcom/twitter/android/moments/ui/fullscreen/dh;

    .line 232
    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->g(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/android/moments/ui/fullscreen/bz;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/fullscreen/bz;->a()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 233
    :goto_0
    sget-object v3, Lcom/twitter/android/moments/ui/fullscreen/dh$8;->a:[I

    invoke-virtual {p1}, Lcom/twitter/android/moments/ui/fullscreen/Event;->a()Lcom/twitter/android/moments/ui/fullscreen/Event$EventType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/android/moments/ui/fullscreen/Event$EventType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 266
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 232
    goto :goto_0

    .line 235
    :pswitch_0
    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$7;->a:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->f(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lacb;

    move-result-object v0

    invoke-virtual {v0, v2}, Lacb;->d(Z)V

    goto :goto_1

    .line 241
    :pswitch_1
    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$7;->a:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->f(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lacb;

    move-result-object v0

    invoke-virtual {v0, v1}, Lacb;->d(Z)V

    goto :goto_1

    .line 247
    :pswitch_2
    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$7;->a:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->f(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lacb;

    move-result-object v0

    invoke-virtual {v0}, Lacb;->h()V

    goto :goto_1

    .line 253
    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$7;->a:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->g(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/android/moments/ui/fullscreen/bz;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/fullscreen/bz;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$7;->a:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->d(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$7;->a:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->f(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lacb;

    move-result-object v0

    invoke-virtual {v0}, Lacb;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 255
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$7;->a:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->f(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lacb;

    move-result-object v0

    invoke-virtual {v0}, Lacb;->h()V

    goto :goto_1

    .line 257
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh$7;->a:Lcom/twitter/android/moments/ui/fullscreen/dh;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->f(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lacb;

    move-result-object v0

    invoke-virtual {v0, v1}, Lacb;->d(Z)V

    goto :goto_1

    .line 233
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 227
    check-cast p1, Lcom/twitter/android/moments/ui/fullscreen/Event;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/dh$7;->onEvent(Lcom/twitter/android/moments/ui/fullscreen/Event;)V

    return-void
.end method
