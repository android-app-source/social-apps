.class public Lcom/twitter/android/moments/ui/fullscreen/t;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/client/v;

.field private final b:Lcom/twitter/android/moments/ui/fullscreen/p;

.field private final c:Landroid/support/v4/app/FragmentActivity;


# direct methods
.method public constructor <init>(Lcom/twitter/android/client/v;Lcom/twitter/android/moments/ui/fullscreen/p;Landroid/support/v4/app/FragmentActivity;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/t;->a:Lcom/twitter/android/client/v;

    .line 42
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/t;->b:Lcom/twitter/android/moments/ui/fullscreen/p;

    .line 43
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/t;->c:Landroid/support/v4/app/FragmentActivity;

    .line 44
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/t;)Landroid/support/v4/app/FragmentActivity;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/t;->c:Landroid/support/v4/app/FragmentActivity;

    return-object v0
.end method

.method public static a(Landroid/support/v4/app/FragmentActivity;)Lcom/twitter/android/moments/ui/fullscreen/t;
    .locals 3

    .prologue
    .line 48
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/t;

    invoke-static {p0}, Lcom/twitter/android/client/v;->a(Landroid/app/Activity;)Lcom/twitter/android/client/v;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/p;

    invoke-direct {v2}, Lcom/twitter/android/moments/ui/fullscreen/p;-><init>()V

    invoke-direct {v0, v1, v2, p0}, Lcom/twitter/android/moments/ui/fullscreen/t;-><init>(Lcom/twitter/android/client/v;Lcom/twitter/android/moments/ui/fullscreen/p;Landroid/support/v4/app/FragmentActivity;)V

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/fullscreen/t;)Lcom/twitter/android/moments/ui/fullscreen/p;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/t;->b:Lcom/twitter/android/moments/ui/fullscreen/p;

    return-object v0
.end method

.method static synthetic b(Lyj;Lcom/twitter/model/moments/a;)Lrx/g;
    .locals 1

    .prologue
    .line 33
    invoke-static {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/t;->c(Lyj;Lcom/twitter/model/moments/a;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/fullscreen/t;)Lcom/twitter/android/client/v;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/t;->a:Lcom/twitter/android/client/v;

    return-object v0
.end method

.method private static c(Lyj;Lcom/twitter/model/moments/a;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyj;",
            "Lcom/twitter/model/moments/a;",
            ")",
            "Lrx/g",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    if-eqz p1, :cond_0

    .line 92
    iget-wide v0, p1, Lcom/twitter/model/moments/a;->b:J

    invoke-virtual {p0, v0, v1}, Lyj;->a(J)Lrx/c;

    move-result-object v0

    invoke-static {}, Lcom/twitter/util/collection/k;->a()Lcom/twitter/util/collection/k;

    move-result-object v1

    invoke-static {v0, v1}, Lcre;->a(Lrx/c;Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/t$2;

    invoke-direct {v1}, Lcom/twitter/android/moments/ui/fullscreen/t$2;-><init>()V

    .line 93
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    .line 104
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method a(Lcom/twitter/model/moments/b;)Lcta;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/b;",
            ")",
            "Lcta",
            "<",
            "Lcom/twitter/app/common/dialog/BaseDialogFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 130
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/t$4;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/t$4;-><init>(Lcom/twitter/android/moments/ui/fullscreen/t;Lcom/twitter/model/moments/b;)V

    return-object v0
.end method

.method a()Lrx/functions/d;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<",
            "Ljava/lang/Boolean;",
            "Lrx/g",
            "<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 116
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/t$3;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/fullscreen/t$3;-><init>(Lcom/twitter/android/moments/ui/fullscreen/t;)V

    return-object v0
.end method

.method a(Lyj;)Lrx/functions/d;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyj;",
            ")",
            "Lrx/functions/d",
            "<",
            "Lcom/twitter/model/moments/a;",
            "Lrx/g",
            "<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 73
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/t$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/t$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/t;Lyj;)V

    return-object v0
.end method

.method public a(Lyj;Lcom/twitter/model/moments/a;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyj;",
            "Lcom/twitter/model/moments/a;",
            ")",
            "Lrx/g",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    invoke-static {p2}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    .line 61
    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/t;->a(Lyj;)Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    .line 62
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/fullscreen/t;->a()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    .line 60
    return-object v0
.end method

.method public b(Lcom/twitter/model/moments/b;)V
    .locals 2

    .prologue
    .line 146
    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/t;->a(Lcom/twitter/model/moments/b;)Lcta;

    move-result-object v0

    .line 147
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/t;->c:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/util/ae;->a(Lcta;Landroid/support/v4/app/FragmentManager;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/t$5;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/t$5;-><init>(Lcom/twitter/android/moments/ui/fullscreen/t;Lcom/twitter/model/moments/b;)V

    .line 148
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/functions/b;)Lrx/j;

    .line 157
    return-void
.end method
