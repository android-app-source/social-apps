.class public Lcom/twitter/android/moments/ui/fullscreen/dh;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/moments/core/ui/widget/sectionpager/a;


# instance fields
.field final a:Lcom/twitter/app/common/util/b$a;

.field final b:Lcom/twitter/library/av/k;

.field private final c:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

.field private final d:Lacb;

.field private final e:Lcom/twitter/android/moments/ui/fullscreen/cd;

.field private final f:Lcom/twitter/android/moments/ui/video/a;

.field private final g:Lcom/twitter/android/moments/ui/fullscreen/o;

.field private final h:Lcom/twitter/android/moments/ui/fullscreen/bd;

.field private final i:Lcom/twitter/android/moments/ui/fullscreen/ak;

.field private final j:Lcom/twitter/util/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcom/twitter/android/moments/ui/fullscreen/ad;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/moments/ui/fullscreen/ad",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Lcom/twitter/app/common/util/j;

.field private final m:Lcom/twitter/util/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lcom/twitter/util/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/q",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Lcom/twitter/android/moments/ui/fullscreen/ci;

.field private final p:Lcom/twitter/android/moments/ui/fullscreen/bz;

.field private final q:Lzn;

.field private final r:Lcom/twitter/android/av/k;

.field private final s:F

.field private t:Z

.field private u:Z

.field private v:Z


# direct methods
.method constructor <init>(Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;Lacb;Lcom/twitter/android/moments/ui/fullscreen/cd;Lcom/twitter/android/moments/ui/video/a;Lcom/twitter/android/moments/ui/fullscreen/o;Lcom/twitter/android/moments/ui/fullscreen/bd;Lcom/twitter/android/moments/ui/fullscreen/ak;Lcom/twitter/util/p;Lcom/twitter/android/moments/ui/fullscreen/ad;Lcom/twitter/app/common/util/j;Lcom/twitter/android/moments/ui/fullscreen/ci;Lcom/twitter/android/moments/ui/fullscreen/bz;Lzn;Lcom/twitter/android/av/k;F)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;",
            "Lacb;",
            "Lcom/twitter/android/moments/ui/fullscreen/cd;",
            "Lcom/twitter/android/moments/ui/video/a;",
            "Lcom/twitter/android/moments/ui/fullscreen/o;",
            "Lcom/twitter/android/moments/ui/fullscreen/bd;",
            "Lcom/twitter/android/moments/ui/fullscreen/ak;",
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;",
            "Lcom/twitter/android/moments/ui/fullscreen/ad",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;",
            ">;",
            "Lcom/twitter/app/common/util/j;",
            "Lcom/twitter/android/moments/ui/fullscreen/ci;",
            "Lcom/twitter/android/moments/ui/fullscreen/bz;",
            "Lzn;",
            "Lcom/twitter/android/av/k;",
            "F)V"
        }
    .end annotation

    .prologue
    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/dh$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/fullscreen/dh$1;-><init>(Lcom/twitter/android/moments/ui/fullscreen/dh;)V

    iput-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->a:Lcom/twitter/app/common/util/b$a;

    .line 57
    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/dh$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/fullscreen/dh$2;-><init>(Lcom/twitter/android/moments/ui/fullscreen/dh;)V

    iput-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->b:Lcom/twitter/library/av/k;

    .line 138
    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/dh$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/fullscreen/dh$3;-><init>(Lcom/twitter/android/moments/ui/fullscreen/dh;)V

    iput-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->n:Lcom/twitter/util/q;

    .line 168
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->c:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    .line 169
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->d:Lacb;

    .line 170
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->e:Lcom/twitter/android/moments/ui/fullscreen/cd;

    .line 171
    iput-object p4, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->f:Lcom/twitter/android/moments/ui/video/a;

    .line 172
    iput-object p5, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->g:Lcom/twitter/android/moments/ui/fullscreen/o;

    .line 173
    iput-object p6, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->h:Lcom/twitter/android/moments/ui/fullscreen/bd;

    .line 174
    iput-object p7, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->i:Lcom/twitter/android/moments/ui/fullscreen/ak;

    .line 175
    iput-object p8, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->j:Lcom/twitter/util/p;

    .line 176
    iput-object p10, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->l:Lcom/twitter/app/common/util/j;

    .line 177
    iput-object p11, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->o:Lcom/twitter/android/moments/ui/fullscreen/ci;

    .line 178
    iput-object p12, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->p:Lcom/twitter/android/moments/ui/fullscreen/bz;

    .line 179
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->q:Lzn;

    .line 180
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->r:Lcom/twitter/android/av/k;

    .line 181
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->e()Lcom/twitter/util/q;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->m:Lcom/twitter/util/q;

    .line 182
    iput-object p9, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->k:Lcom/twitter/android/moments/ui/fullscreen/ad;

    .line 183
    move/from16 v0, p15

    iput v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->s:F

    .line 185
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->d:Lacb;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->b:Lcom/twitter/library/av/k;

    invoke-virtual {v1, v2}, Lacb;->a(Lcom/twitter/library/av/k;)V

    .line 186
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->d:Lacb;

    invoke-virtual {p4}, Lcom/twitter/android/moments/ui/video/a;->g()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v2

    invoke-virtual {v1, v2}, Lacb;->a(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V

    .line 187
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->f:Lcom/twitter/android/moments/ui/video/a;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/video/a;->b()V

    .line 188
    invoke-virtual {p11}, Lcom/twitter/android/moments/ui/fullscreen/ci;->g()V

    .line 190
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->a:Lcom/twitter/app/common/util/b$a;

    invoke-interface {p10, v1}, Lcom/twitter/app/common/util/j;->a(Lcom/twitter/app/common/util/b$a;)V

    .line 191
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/av/playback/ab;->m(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/card/property/ImageSpec;

    move-result-object v1

    .line 192
    if-eqz v1, :cond_0

    .line 193
    iget-object v2, v1, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    iget v2, v2, Lcom/twitter/model/card/property/Vector2F;->x:F

    float-to-int v2, v2

    iget-object v1, v1, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    iget v1, v1, Lcom/twitter/model/card/property/Vector2F;->y:F

    float-to-int v1, v1

    invoke-direct {p0, v2, v1}, Lcom/twitter/android/moments/ui/fullscreen/dh;->a(II)V

    .line 196
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->d:Lacb;

    invoke-virtual {v1}, Lacb;->k()Lrx/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/dh$4;

    invoke-direct {v2, p0}, Lcom/twitter/android/moments/ui/fullscreen/dh$4;-><init>(Lcom/twitter/android/moments/ui/fullscreen/dh;)V

    invoke-virtual {v1, v2}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 203
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->c:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    iget-object v1, v1, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->b:Lcom/twitter/model/moments/e;

    iget-boolean v1, v1, Lcom/twitter/model/moments/e;->h:Z

    if-eqz v1, :cond_1

    .line 204
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->d:Lacb;

    invoke-virtual {v1}, Lacb;->k()Lrx/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/moments/ui/fullscreen/dh$5;

    invoke-direct {v2, p0}, Lcom/twitter/android/moments/ui/fullscreen/dh$5;-><init>(Lcom/twitter/android/moments/ui/fullscreen/dh;)V

    invoke-virtual {v1, v2}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 211
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->c:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->l()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->c:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->k()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->r:Lcom/twitter/android/av/k;

    .line 212
    invoke-virtual {v1}, Lcom/twitter/android/av/k;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 213
    invoke-virtual {p2}, Lacb;->i()V

    .line 214
    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/dh$6;

    invoke-direct {v1, p0, p2}, Lcom/twitter/android/moments/ui/fullscreen/dh$6;-><init>(Lcom/twitter/android/moments/ui/fullscreen/dh;Lacb;)V

    invoke-virtual {p2, v1}, Lacb;->a(Landroid/view/View$OnClickListener;)V

    .line 222
    :cond_2
    return-void
.end method

.method static a(Landroid/content/Context;Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/android/moments/ui/fullscreen/bq;Lcom/twitter/android/moments/ui/fullscreen/cu;)Lacb;
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 311
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 312
    iget-object v1, p1, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->b:Lcom/twitter/model/moments/e;

    iget-boolean v1, v1, Lcom/twitter/model/moments/e;->h:Z

    if-eqz v1, :cond_0

    .line 313
    new-instance v1, Lcom/twitter/library/av/VideoPlayerView;

    sget-object v2, Lcom/twitter/library/av/VideoPlayerView$Mode;->e:Lcom/twitter/library/av/VideoPlayerView$Mode;

    invoke-direct {v1, p0, p2, p3, v2}, Lcom/twitter/library/av/VideoPlayerView;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/ag;Lcom/twitter/library/av/VideoPlayerView$Mode;)V

    .line 317
    invoke-interface {p4}, Lcom/twitter/android/moments/ui/fullscreen/cu;->e()Lrx/c;

    move-result-object v2

    .line 316
    invoke-static {v0, v1, v2}, Lacb;->a(Landroid/view/LayoutInflater;Lcom/twitter/library/av/VideoPlayerView;Lrx/c;)Lacb;

    move-result-object v0

    .line 322
    :goto_0
    return-object v0

    .line 320
    :cond_0
    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/MomentsVideoPlayerView;

    sget-object v2, Lcom/twitter/library/av/VideoPlayerView$Mode;->e:Lcom/twitter/library/av/VideoPlayerView$Mode;

    invoke-direct {v1, p0, p2, p3, v2}, Lcom/twitter/android/moments/ui/fullscreen/MomentsVideoPlayerView;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/ag;Lcom/twitter/library/av/VideoPlayerView$Mode;)V

    .line 322
    invoke-static {v0, v1}, Lacb;->a(Landroid/view/LayoutInflater;Lcom/twitter/library/av/VideoPlayerView;)Lacb;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;Lcom/twitter/util/p;Lcom/twitter/android/moments/ui/fullscreen/ad;Lcom/twitter/android/moments/ui/fullscreen/bd;Lcom/twitter/android/moments/ui/fullscreen/ak;Lcom/twitter/app/common/util/j;Lcom/twitter/android/moments/ui/fullscreen/ci;Lcom/twitter/android/moments/ui/fullscreen/bz;Lzn;Lcom/twitter/android/moments/ui/fullscreen/cu;Lcom/twitter/android/av/k;)Lcom/twitter/android/moments/ui/fullscreen/dh;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;",
            "Lcom/twitter/util/p",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;",
            "Lcom/twitter/android/moments/ui/fullscreen/ad",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;",
            ">;",
            "Lcom/twitter/android/moments/ui/fullscreen/bd;",
            "Lcom/twitter/android/moments/ui/fullscreen/ak;",
            "Lcom/twitter/app/common/util/j;",
            "Lcom/twitter/android/moments/ui/fullscreen/ci;",
            "Lcom/twitter/android/moments/ui/fullscreen/bz;",
            "Lzn;",
            "Lcom/twitter/android/moments/ui/fullscreen/cu;",
            "Lcom/twitter/android/av/k;",
            ")",
            "Lcom/twitter/android/moments/ui/fullscreen/dh;"
        }
    .end annotation

    .prologue
    .line 282
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v3

    .line 283
    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Lcom/twitter/android/moments/ui/fullscreen/bd;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/moments/ui/video/a;

    move-result-object v13

    .line 285
    invoke-virtual {v13}, Lcom/twitter/android/moments/ui/video/a;->g()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v3

    new-instance v4, Lcom/twitter/android/moments/ui/fullscreen/bq;

    invoke-direct {v4}, Lcom/twitter/android/moments/ui/fullscreen/bq;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p10

    invoke-static {v0, v1, v3, v4, v2}, Lcom/twitter/android/moments/ui/fullscreen/dh;->a(Landroid/content/Context;Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/android/moments/ui/fullscreen/bq;Lcom/twitter/android/moments/ui/fullscreen/cu;)Lacb;

    move-result-object v14

    .line 287
    new-instance v3, Lcom/twitter/android/moments/ui/fullscreen/o;

    .line 288
    invoke-virtual {v14}, Lacb;->f()Landroid/view/ViewGroup;

    move-result-object v4

    .line 289
    invoke-virtual {v14}, Lacb;->c()Landroid/widget/ProgressBar;

    move-result-object v5

    invoke-virtual {v14}, Lacb;->e()Landroid/widget/TextView;

    move-result-object v6

    .line 290
    invoke-virtual {v14}, Lacb;->d()Landroid/view/View;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 291
    invoke-virtual {v14}, Lacb;->b()Landroid/widget/ProgressBar;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->h()Lcom/twitter/model/moments/MomentPageDisplayMode;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->j()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v12, p3

    invoke-direct/range {v3 .. v12}, Lcom/twitter/android/moments/ui/fullscreen/o;-><init>(Landroid/view/View;Landroid/widget/ProgressBar;Landroid/widget/TextView;Landroid/view/View;Landroid/content/res/Resources;Landroid/widget/ProgressBar;Lcom/twitter/model/moments/MomentPageDisplayMode;Ljava/lang/String;Lcom/twitter/android/moments/ui/fullscreen/ad;)V

    .line 293
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f100009

    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v4

    .line 295
    new-instance v7, Lcom/twitter/android/moments/ui/fullscreen/cd;

    invoke-virtual {v14}, Lacb;->f()Landroid/view/ViewGroup;

    move-result-object v5

    const/4 v6, 0x1

    invoke-direct {v7, v5, v4, v6}, Lcom/twitter/android/moments/ui/fullscreen/cd;-><init>(Landroid/view/View;FI)V

    .line 297
    invoke-static/range {p0 .. p0}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;)Lcom/twitter/util/math/Size;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/util/math/Size;->g()F

    move-result v19

    .line 298
    new-instance v4, Lcom/twitter/android/moments/ui/fullscreen/dh;

    move-object/from16 v5, p1

    move-object v6, v14

    move-object v8, v13

    move-object v9, v3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move-object/from16 v12, p2

    move-object/from16 v13, p3

    move-object/from16 v14, p6

    move-object/from16 v15, p7

    move-object/from16 v16, p8

    move-object/from16 v17, p9

    move-object/from16 v18, p11

    invoke-direct/range {v4 .. v19}, Lcom/twitter/android/moments/ui/fullscreen/dh;-><init>(Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;Lacb;Lcom/twitter/android/moments/ui/fullscreen/cd;Lcom/twitter/android/moments/ui/video/a;Lcom/twitter/android/moments/ui/fullscreen/o;Lcom/twitter/android/moments/ui/fullscreen/bd;Lcom/twitter/android/moments/ui/fullscreen/ak;Lcom/twitter/util/p;Lcom/twitter/android/moments/ui/fullscreen/ad;Lcom/twitter/app/common/util/j;Lcom/twitter/android/moments/ui/fullscreen/ci;Lcom/twitter/android/moments/ui/fullscreen/bz;Lzn;Lcom/twitter/android/av/k;F)V

    return-object v4
.end method

.method private a(II)V
    .locals 3

    .prologue
    .line 401
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->c:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    iget-object v0, v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->b:Lcom/twitter/model/moments/e;

    iget v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->s:F

    invoke-static {v0, v1}, Lcom/twitter/model/moments/e;->a(Lcom/twitter/model/moments/e;F)Lcom/twitter/model/moments/d;

    move-result-object v0

    .line 404
    if-nez v0, :cond_0

    .line 405
    invoke-static {p1, p2}, Lcom/twitter/util/math/Size;->a(II)Lcom/twitter/util/math/Size;

    move-result-object v1

    .line 406
    const/4 v0, 0x0

    .line 411
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->d:Lacb;

    invoke-virtual {v2, v1, v0}, Lacb;->a(Lcom/twitter/util/math/Size;Landroid/graphics/Rect;)V

    .line 412
    return-void

    .line 408
    :cond_0
    iget-object v1, v0, Lcom/twitter/model/moments/d;->f:Lcom/twitter/util/math/Size;

    .line 409
    invoke-virtual {v0}, Lcom/twitter/model/moments/d;->a()Landroid/graphics/Rect;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/dh;II)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/moments/ui/fullscreen/dh;->a(II)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/dh;)Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->u:Z

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/fullscreen/dh;Z)Z
    .locals 0

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->v:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/android/moments/ui/video/a;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->f:Lcom/twitter/android/moments/ui/video/a;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/fullscreen/dh;Z)Z
    .locals 0

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->t:Z

    return p1
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/android/moments/ui/fullscreen/ci;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->o:Lcom/twitter/android/moments/ui/fullscreen/ci;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->c:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/android/moments/ui/fullscreen/ad;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->k:Lcom/twitter/android/moments/ui/fullscreen/ad;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lacb;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->d:Lacb;

    return-object v0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->r:Lcom/twitter/android/av/k;

    invoke-virtual {v0}, Lcom/twitter/android/av/k;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->f:Lcom/twitter/android/moments/ui/video/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/video/a;->c()V

    .line 398
    :cond_0
    return-void
.end method

.method static synthetic g(Lcom/twitter/android/moments/ui/fullscreen/dh;)Lcom/twitter/android/moments/ui/fullscreen/bz;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->p:Lcom/twitter/android/moments/ui/fullscreen/bz;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->d:Lacb;

    invoke-virtual {v0}, Lacb;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(F)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 365
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->e:Lcom/twitter/android/moments/ui/fullscreen/cd;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/fullscreen/cd;->a(F)V

    .line 373
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->f:Lcom/twitter/android/moments/ui/video/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/video/a;->f()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    .line 374
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    sub-float v1, v2, v1

    invoke-static {v1}, Lcom/twitter/android/moments/ui/fullscreen/f;->a(F)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->a(F)V

    .line 377
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    sub-float v1, v2, v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->b(F)V

    .line 379
    iget-boolean v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->v:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayer;->V()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 380
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->k:Lcom/twitter/android/moments/ui/fullscreen/ad;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->c:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->j()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->d:Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;

    .line 381
    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->a()Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;

    move-result-object v2

    .line 380
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 384
    :cond_0
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 385
    :goto_0
    if-eqz v0, :cond_1

    .line 389
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->f:Lcom/twitter/android/moments/ui/video/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/video/a;->g()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->i()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 390
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->f()V

    .line 392
    :cond_1
    return-void

    .line 384
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 333
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->t:Z

    if-nez v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->k:Lcom/twitter/android/moments/ui/fullscreen/ad;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->c:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->j()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->c:Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;

    .line 335
    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent$PageLoadingEventType;->a()Lcom/twitter/android/moments/ui/fullscreen/PageLoadingEvent;

    move-result-object v2

    .line 334
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 337
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->f:Lcom/twitter/android/moments/ui/video/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/video/a;->g()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->i()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 338
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->f:Lcom/twitter/android/moments/ui/video/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/video/a;->f()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->i:Lcom/twitter/android/moments/ui/fullscreen/ak;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/fullscreen/ak;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->d(Z)V

    .line 339
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/fullscreen/dh;->f()V

    .line 340
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->u:Z

    .line 341
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->j:Lcom/twitter/util/p;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->m:Lcom/twitter/util/q;

    invoke-virtual {v0, v1}, Lcom/twitter/util/p;->a(Lcom/twitter/util/q;)Z

    .line 342
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->i:Lcom/twitter/android/moments/ui/fullscreen/ak;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ak;->c()Lcom/twitter/util/p;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->n:Lcom/twitter/util/q;

    invoke-virtual {v0, v1}, Lcom/twitter/util/p;->a(Lcom/twitter/util/q;)Z

    .line 343
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    .line 347
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->u:Z

    .line 348
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->f:Lcom/twitter/android/moments/ui/video/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/video/a;->f()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/av/playback/AVPlayer;->a(J)V

    .line 349
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->f:Lcom/twitter/android/moments/ui/video/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/video/a;->e()V

    .line 350
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->f:Lcom/twitter/android/moments/ui/video/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/video/a;->g()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->j()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 351
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->j:Lcom/twitter/util/p;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->m:Lcom/twitter/util/q;

    invoke-virtual {v0, v1}, Lcom/twitter/util/p;->b(Lcom/twitter/util/q;)Z

    .line 352
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->i:Lcom/twitter/android/moments/ui/fullscreen/ak;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ak;->c()Lcom/twitter/util/p;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->n:Lcom/twitter/util/q;

    invoke-virtual {v0, v1}, Lcom/twitter/util/p;->b(Lcom/twitter/util/q;)Z

    .line 353
    return-void
.end method

.method public d()V
    .locals 3

    .prologue
    .line 357
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->l:Lcom/twitter/app/common/util/j;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->a:Lcom/twitter/app/common/util/b$a;

    invoke-interface {v0, v1}, Lcom/twitter/app/common/util/j;->b(Lcom/twitter/app/common/util/b$a;)V

    .line 358
    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->h:Lcom/twitter/android/moments/ui/fullscreen/bd;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->f:Lcom/twitter/android/moments/ui/video/a;

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->l:Lcom/twitter/app/common/util/j;

    .line 359
    invoke-interface {v0}, Lcom/twitter/app/common/util/j;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 358
    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/moments/ui/fullscreen/bd;->a(Lcom/twitter/android/moments/ui/video/a;Z)V

    .line 360
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->g:Lcom/twitter/android/moments/ui/fullscreen/o;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/o;->g()V

    .line 361
    return-void

    .line 359
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected e()Lcom/twitter/util/q;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/android/moments/ui/fullscreen/Event;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226
    new-instance v0, Lcom/twitter/android/moments/ui/fullscreen/dh$7;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->d:Lacb;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->e:Lcom/twitter/android/moments/ui/fullscreen/cd;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/fullscreen/dh;->q:Lzn;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/twitter/android/moments/ui/fullscreen/dh$7;-><init>(Lcom/twitter/android/moments/ui/fullscreen/dh;Laci;Lcom/twitter/android/moments/ui/fullscreen/cd;Lzn;)V

    return-object v0
.end method
