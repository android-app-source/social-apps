.class Lcom/twitter/android/moments/ui/fullscreen/ce$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/fullscreen/ce;->e()Lrx/functions/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/d",
        "<",
        "Lcbi",
        "<",
        "Lcom/twitter/android/moments/viewmodels/MomentModule;",
        ">;",
        "Lrx/c",
        "<",
        "Ljava/util/Map",
        "<",
        "Lcom/twitter/android/moments/viewmodels/MomentModule;",
        "Lcom/twitter/model/moments/viewmodels/a;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/ui/fullscreen/ce;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/ce;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/ce$3;->a:Lcom/twitter/android/moments/ui/fullscreen/ce;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 122
    check-cast p1, Lcbi;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/ce$3;->a(Lcbi;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcbi;)Lrx/c;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentModule;",
            ">;)",
            "Lrx/c",
            "<",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentModule;",
            "Lcom/twitter/model/moments/viewmodels/a;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 126
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 129
    invoke-static {p1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v2

    .line 130
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/MomentModule;

    .line 131
    iget-object v4, p0, Lcom/twitter/android/moments/ui/fullscreen/ce$3;->a:Lcom/twitter/android/moments/ui/fullscreen/ce;

    invoke-static {v4}, Lcom/twitter/android/moments/ui/fullscreen/ce;->a(Lcom/twitter/android/moments/ui/fullscreen/ce;)Lxv;

    move-result-object v4

    invoke-virtual {v0}, Lcom/twitter/android/moments/viewmodels/MomentModule;->b()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    iget-wide v6, v0, Lcom/twitter/model/moments/Moment;->b:J

    invoke-virtual {v4, v6, v7}, Lxv;->a(J)Lrx/c;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 133
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {}, Lcom/twitter/android/moments/ui/fullscreen/ce;->c()Lrx/functions/h;

    move-result-object v1

    invoke-static {v0, v1}, Lrx/c;->a(Ljava/lang/Iterable;Lrx/functions/h;)Lrx/c;

    move-result-object v0

    invoke-static {v2}, Lcom/twitter/android/moments/ui/fullscreen/ce;->a(Ljava/util/List;)Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    return-object v0
.end method
