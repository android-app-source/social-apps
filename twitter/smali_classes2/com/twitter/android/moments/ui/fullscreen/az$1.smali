.class Lcom/twitter/android/moments/ui/fullscreen/az$1;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/fullscreen/az;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Lcom/twitter/util/collection/k",
        "<",
        "Lcom/twitter/model/core/Tweet;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/ui/fullscreen/az;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/az;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/az$1;->a:Lcom/twitter/android/moments/ui/fullscreen/az;

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/util/collection/k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p1}, Lcom/twitter/util/collection/k;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/az$1;->a:Lcom/twitter/android/moments/ui/fullscreen/az;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/az;->b(Lcom/twitter/android/moments/ui/fullscreen/az;)Lcom/twitter/android/moments/ui/fullscreen/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/ba;->a()Lrx/g;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/az$1;->a:Lcom/twitter/android/moments/ui/fullscreen/az;

    .line 46
    invoke-static {v1}, Lcre;->a(Ljava/lang/Object;)Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/az$1;->a:Lcom/twitter/android/moments/ui/fullscreen/az;

    .line 47
    invoke-static {v1}, Lcom/twitter/android/moments/ui/fullscreen/az;->a(Lcom/twitter/android/moments/ui/fullscreen/az;)Lrx/subjects/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/d;)Lrx/j;

    .line 48
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/az$1;->a:Lcom/twitter/android/moments/ui/fullscreen/az;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/az;->b(Lcom/twitter/android/moments/ui/fullscreen/az;)Lcom/twitter/android/moments/ui/fullscreen/ba;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/az$1;->a:Lcom/twitter/android/moments/ui/fullscreen/az;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/az;->c(Lcom/twitter/android/moments/ui/fullscreen/az;)Lcom/twitter/model/moments/o;

    move-result-object v2

    invoke-virtual {p1}, Lcom/twitter/util/collection/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/moments/ui/fullscreen/ba;->a(Lcom/twitter/model/moments/o;Lcom/twitter/model/core/Tweet;)V

    .line 50
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 41
    check-cast p1, Lcom/twitter/util/collection/k;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/fullscreen/az$1;->a(Lcom/twitter/util/collection/k;)V

    return-void
.end method
