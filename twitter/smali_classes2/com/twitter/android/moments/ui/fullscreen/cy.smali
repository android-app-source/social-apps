.class public Lcom/twitter/android/moments/ui/fullscreen/cy;
.super Landroid/support/v4/view/ViewPager$SimpleOnPageChangeListener;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Labm;

.field private final b:Lcom/twitter/model/moments/viewmodels/i;

.field private final c:Lcom/twitter/android/moments/ui/fullscreen/bg;

.field private final d:Lcom/twitter/model/moments/Moment;


# direct methods
.method public constructor <init>(Labm;Lcom/twitter/model/moments/viewmodels/i;Lcom/twitter/android/moments/ui/fullscreen/bg;Lcom/twitter/android/moments/ui/fullscreen/co;Lcom/twitter/model/moments/Moment;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager$SimpleOnPageChangeListener;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/twitter/android/moments/ui/fullscreen/cy;->a:Labm;

    .line 24
    iput-object p2, p0, Lcom/twitter/android/moments/ui/fullscreen/cy;->b:Lcom/twitter/model/moments/viewmodels/i;

    .line 25
    iput-object p3, p0, Lcom/twitter/android/moments/ui/fullscreen/cy;->c:Lcom/twitter/android/moments/ui/fullscreen/bg;

    .line 26
    iput-object p5, p0, Lcom/twitter/android/moments/ui/fullscreen/cy;->d:Lcom/twitter/model/moments/Moment;

    .line 27
    invoke-interface {p4}, Lcom/twitter/android/moments/ui/fullscreen/co;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cy;->a:Labm;

    invoke-virtual {v0}, Labm;->g()V

    .line 30
    :cond_0
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cy;->c:Lcom/twitter/android/moments/ui/fullscreen/bg;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/fullscreen/cy;->d:Lcom/twitter/model/moments/Moment;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/bg;->c(Lcom/twitter/model/moments/Moment;)V

    .line 43
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cy;->b:Lcom/twitter/model/moments/viewmodels/i;

    invoke-interface {v0, p1}, Lcom/twitter/model/moments/viewmodels/i;->c(I)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 35
    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/twitter/android/moments/ui/fullscreen/cy;->a:Labm;

    invoke-virtual {v0, p0}, Labm;->b(Landroid/view/View$OnClickListener;)V

    .line 38
    :cond_0
    return-void
.end method
