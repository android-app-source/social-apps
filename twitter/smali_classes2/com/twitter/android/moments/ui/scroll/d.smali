.class public Lcom/twitter/android/moments/ui/scroll/d;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Lcom/twitter/util/object/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/d",
            "<",
            "Landroid/support/v7/widget/RecyclerView;",
            "Lcom/twitter/android/moments/ui/scroll/d;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/twitter/android/moments/ui/scroll/b;

.field private final c:Lcom/twitter/ui/anim/f$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/twitter/android/moments/ui/scroll/d$1;

    invoke-direct {v0}, Lcom/twitter/android/moments/ui/scroll/d$1;-><init>()V

    sput-object v0, Lcom/twitter/android/moments/ui/scroll/d;->a:Lcom/twitter/util/object/d;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/android/moments/ui/scroll/b;Lcom/twitter/ui/anim/f$a;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/twitter/android/moments/ui/scroll/d;->b:Lcom/twitter/android/moments/ui/scroll/b;

    .line 43
    iput-object p2, p0, Lcom/twitter/android/moments/ui/scroll/d;->c:Lcom/twitter/ui/anim/f$a;

    .line 44
    return-void
.end method

.method private a(F)F
    .locals 2

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/scroll/d;->b(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/d;->b:Lcom/twitter/android/moments/ui/scroll/b;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/scroll/b;->a()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40a00000    # 5.0f

    mul-float/2addr v0, v1

    .line 79
    invoke-static {p1}, Ljava/lang/Math;->signum(F)F

    move-result v1

    mul-float p1, v0, v1

    .line 81
    :cond_0
    return p1
.end method

.method public static a(Landroid/support/v7/widget/RecyclerView;)Lcom/twitter/android/moments/ui/scroll/d;
    .locals 3

    .prologue
    .line 35
    new-instance v0, Lcom/twitter/android/moments/ui/scroll/d;

    new-instance v1, Lcom/twitter/android/moments/ui/scroll/b;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/scroll/b;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    sget-object v2, Lcom/twitter/ui/anim/f;->a:Lcom/twitter/ui/anim/f$a;

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/moments/ui/scroll/d;-><init>(Lcom/twitter/android/moments/ui/scroll/b;Lcom/twitter/ui/anim/f$a;)V

    return-object v0
.end method

.method private b(F)Z
    .locals 3

    .prologue
    .line 86
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/scroll/d;->b:Lcom/twitter/android/moments/ui/scroll/b;

    .line 87
    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/scroll/b;->a()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40a00000    # 5.0f

    mul-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    .line 86
    :goto_0
    return v0

    .line 87
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(FF)I
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 47
    invoke-direct {p0, p2}, Lcom/twitter/android/moments/ui/scroll/d;->a(F)F

    move-result v0

    .line 48
    iget-object v3, p0, Lcom/twitter/android/moments/ui/scroll/d;->c:Lcom/twitter/ui/anim/f$a;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcom/twitter/ui/anim/f$a;->a(Ljava/lang/Float;Ljava/lang/Float;)Lcom/twitter/ui/anim/f;

    move-result-object v0

    .line 49
    iget-object v3, p0, Lcom/twitter/android/moments/ui/scroll/d;->b:Lcom/twitter/android/moments/ui/scroll/b;

    invoke-virtual {v3}, Lcom/twitter/android/moments/ui/scroll/b;->c()I

    move-result v4

    .line 50
    invoke-virtual {v0}, Lcom/twitter/ui/anim/f;->a()F

    move-result v0

    int-to-float v3, v4

    div-float/2addr v0, v3

    .line 51
    iget-object v3, p0, Lcom/twitter/android/moments/ui/scroll/d;->b:Lcom/twitter/android/moments/ui/scroll/b;

    sget-object v5, Lcom/twitter/android/moments/ui/scroll/Direction;->b:Lcom/twitter/android/moments/ui/scroll/Direction;

    invoke-virtual {v3, v5}, Lcom/twitter/android/moments/ui/scroll/b;->b(Lcom/twitter/android/moments/ui/scroll/Direction;)I

    move-result v3

    .line 52
    iget-object v5, p0, Lcom/twitter/android/moments/ui/scroll/d;->b:Lcom/twitter/android/moments/ui/scroll/b;

    sget-object v6, Lcom/twitter/android/moments/ui/scroll/Direction;->b:Lcom/twitter/android/moments/ui/scroll/Direction;

    invoke-virtual {v5, v6}, Lcom/twitter/android/moments/ui/scroll/b;->a(Lcom/twitter/android/moments/ui/scroll/Direction;)I

    move-result v5

    .line 53
    int-to-float v5, v5

    int-to-float v4, v4

    div-float v4, v5, v4

    .line 54
    int-to-float v5, v3

    sub-float v4, v5, v4

    .line 55
    add-float v5, v4, v0

    .line 58
    invoke-static {p2}, Ljava/lang/Math;->signum(F)F

    move-result v0

    cmpg-float v0, v0, v7

    if-gez v0, :cond_2

    add-int/lit8 v0, v3, 0x1

    int-to-float v0, v0

    sub-float v0, v5, v0

    const/high16 v4, -0x41000000    # -0.5f

    cmpl-float v0, v0, v4

    if-lez v0, :cond_2

    add-int/lit8 v0, v3, 0x1

    int-to-float v0, v0

    sub-float v0, v5, v0

    cmpg-float v0, v0, v7

    if-gtz v0, :cond_2

    move v0, v1

    .line 61
    :goto_0
    invoke-static {p2}, Ljava/lang/Math;->signum(F)F

    move-result v4

    cmpl-float v4, v4, v7

    if-ltz v4, :cond_3

    int-to-float v4, v3

    sub-float v4, v5, v4

    .line 62
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v6, 0x3f000000    # 0.5f

    cmpg-float v4, v4, v6

    if-gez v4, :cond_3

    move v4, v1

    .line 63
    :goto_1
    if-nez v0, :cond_0

    if-eqz v4, :cond_1

    :cond_0
    move v2, v1

    .line 64
    :cond_1
    if-eqz v2, :cond_5

    .line 65
    invoke-static {p2}, Ljava/lang/Math;->signum(F)F

    move-result v0

    cmpl-float v0, v0, v7

    if-lez v0, :cond_4

    add-int/lit8 v0, v3, 0x1

    .line 64
    :goto_2
    return v0

    :cond_2
    move v0, v2

    .line 58
    goto :goto_0

    :cond_3
    move v4, v2

    .line 62
    goto :goto_1

    :cond_4
    move v0, v3

    .line 65
    goto :goto_2

    .line 66
    :cond_5
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_2
.end method
