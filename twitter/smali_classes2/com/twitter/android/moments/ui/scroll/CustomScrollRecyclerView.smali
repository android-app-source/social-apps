.class public Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;
.super Landroid/support/v7/widget/RecyclerView;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView$a;
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method private a(II)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 46
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 47
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 48
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;->getMinFlingVelocity()I

    move-result v4

    if-lt v0, v4, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;->getMaxFlingVelocity()I

    move-result v4

    if-gt v0, v4, :cond_2

    move v0, v1

    .line 49
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;->getMinFlingVelocity()I

    move-result v4

    if-lt v3, v4, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;->getMaxFlingVelocity()I

    move-result v4

    if-gt v3, v4, :cond_3

    move v3, v1

    .line 50
    :goto_1
    if-nez v0, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    return v2

    :cond_2
    move v0, v2

    .line 48
    goto :goto_0

    :cond_3
    move v3, v2

    .line 49
    goto :goto_1
.end method


# virtual methods
.method public fling(II)Z
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;->a:Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView$a;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;->a:Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView$a;

    .line 39
    invoke-interface {v0, p0, p1, p2}, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView$a;->a(Landroid/support/v7/widget/RecyclerView;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    const/4 v0, 0x1

    .line 42
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->fling(II)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 55
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 56
    iget-object v1, p0, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;->a:Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView$a;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;->a:Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView$a;

    invoke-interface {v1, p0, p1}, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView$a;->a(Landroid/support/v7/widget/RecyclerView;Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    const/4 v0, 0x1

    .line 59
    :cond_0
    return v0
.end method

.method public setOnScrollTouchListener(Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView$a;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;->a:Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView$a;

    .line 34
    return-void
.end method
