.class public Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;
.super Landroid/support/v7/widget/LinearLayoutManager;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 18
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;->getChildCount()I

    move-result v1

    if-lez v1, :cond_3

    .line 23
    const/4 v2, 0x0

    .line 24
    const v1, 0x7fffffff

    move v3, v0

    move v0, v1

    .line 25
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;->getChildCount()I

    move-result v1

    if-ge v3, v1, :cond_1

    .line 26
    invoke-virtual {p0, v3}, Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 27
    invoke-virtual {p0, v1}, Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;->getPosition(Landroid/view/View;)I

    move-result v4

    .line 28
    if-ne v4, p1, :cond_0

    .line 25
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    goto :goto_0

    .line 30
    :cond_0
    if-nez v2, :cond_4

    .line 34
    invoke-virtual {p0, v1}, Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;->getDecoratedMeasuredWidth(Landroid/view/View;)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    move-object v1, v2

    goto :goto_1

    .line 37
    :cond_1
    if-eqz v2, :cond_2

    .line 38
    invoke-virtual {p0, v2}, Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;->getDecoratedMeasuredWidth(Landroid/view/View;)I

    move-result v0

    .line 40
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;->getWidth()I

    move-result v1

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    .line 42
    :cond_3
    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;->scrollToPositionWithOffset(II)V

    .line 43
    return-void

    :cond_4
    move-object v1, v2

    goto :goto_1
.end method
