.class public Lcom/twitter/android/moments/ui/scroll/b;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/twitter/android/moments/ui/scroll/b;->a:Landroid/support/v7/widget/RecyclerView;

    .line 20
    return-void
.end method

.method private a(I)Landroid/view/View;
    .locals 5

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/b;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v2

    .line 79
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    .line 80
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/b;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 82
    iget-object v3, p0, Lcom/twitter/android/moments/ui/scroll/b;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/RecyclerView;->getChildLayoutPosition(Landroid/view/View;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    .line 79
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 85
    :cond_1
    iget-object v3, p0, Lcom/twitter/android/moments/ui/scroll/b;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/RecyclerView;->getChildLayoutPosition(Landroid/view/View;)I

    move-result v3

    .line 86
    sub-int v3, p1, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 90
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private c(Lcom/twitter/android/moments/ui/scroll/Direction;)Landroid/view/View;
    .locals 9

    .prologue
    .line 101
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/b;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v5, v0

    .line 102
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/b;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v6

    .line 103
    const/4 v2, 0x0

    .line 104
    const v1, 0x7fffffff

    .line 105
    const/4 v0, 0x0

    move v4, v0

    move v0, v1

    :goto_0
    if-ge v4, v6, :cond_3

    .line 106
    iget-object v1, p0, Lcom/twitter/android/moments/ui/scroll/b;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v4}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 107
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v1, v7

    .line 109
    iget-object v7, p0, Lcom/twitter/android/moments/ui/scroll/b;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v7, v3}, Landroid/support/v7/widget/RecyclerView;->getChildLayoutPosition(Landroid/view/View;)I

    move-result v7

    const/4 v8, -0x1

    if-ne v7, v8, :cond_0

    move-object v1, v2

    .line 105
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v2, v1

    goto :goto_0

    .line 112
    :cond_0
    int-to-float v7, v1

    cmpl-float v7, v7, v5

    if-lez v7, :cond_1

    sget-object v7, Lcom/twitter/android/moments/ui/scroll/Direction;->b:Lcom/twitter/android/moments/ui/scroll/Direction;

    if-ne p1, v7, :cond_1

    move-object v1, v2

    .line 113
    goto :goto_1

    .line 115
    :cond_1
    int-to-float v7, v1

    cmpg-float v7, v7, v5

    if-gez v7, :cond_2

    sget-object v7, Lcom/twitter/android/moments/ui/scroll/Direction;->c:Lcom/twitter/android/moments/ui/scroll/Direction;

    if-ne p1, v7, :cond_2

    move-object v1, v2

    .line 116
    goto :goto_1

    .line 118
    :cond_2
    int-to-float v1, v1

    sub-float/2addr v1, v5

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    float-to-int v1, v1

    .line 119
    if-ge v1, v0, :cond_4

    move v0, v1

    move-object v1, v3

    .line 121
    goto :goto_1

    .line 124
    :cond_3
    return-object v2

    :cond_4
    move-object v1, v2

    goto :goto_1
.end method

.method private d()Landroid/view/View;
    .locals 4

    .prologue
    .line 67
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/moments/ui/scroll/b;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 68
    iget-object v1, p0, Lcom/twitter/android/moments/ui/scroll/b;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 69
    iget-object v2, p0, Lcom/twitter/android/moments/ui/scroll/b;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/RecyclerView;->getChildLayoutPosition(Landroid/view/View;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    move-object v0, v1

    .line 73
    :goto_1
    return-object v0

    .line 67
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 73
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/b;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getMinFlingVelocity()I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/b;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    neg-int v0, v0

    return v0
.end method

.method public a(Lcom/twitter/android/moments/ui/scroll/Direction;)I
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/scroll/b;->c(Lcom/twitter/android/moments/ui/scroll/Direction;)Landroid/view/View;

    move-result-object v0

    .line 36
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/android/moments/ui/scroll/b;->a(Landroid/view/View;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/b;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    .line 28
    return-void
.end method

.method public b(Lcom/twitter/android/moments/ui/scroll/Direction;)I
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/scroll/b;->c(Lcom/twitter/android/moments/ui/scroll/Direction;)Landroid/view/View;

    move-result-object v0

    .line 41
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/scroll/b;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->getChildLayoutPosition(Landroid/view/View;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/b;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->stopScroll()V

    .line 32
    return-void
.end method

.method public c()I
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/scroll/b;->d()Landroid/view/View;

    move-result-object v0

    .line 57
    iget-object v1, p0, Lcom/twitter/android/moments/ui/scroll/b;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->getChildLayoutPosition(Landroid/view/View;)I

    move-result v1

    .line 58
    invoke-direct {p0, v1}, Lcom/twitter/android/moments/ui/scroll/b;->a(I)Landroid/view/View;

    move-result-object v1

    .line 59
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/b;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    .line 62
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    goto :goto_0
.end method
