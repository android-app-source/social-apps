.class public Lcom/twitter/android/moments/ui/scroll/e;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView$a;


# instance fields
.field private final a:Lcom/twitter/util/object/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/g",
            "<",
            "Landroid/support/v7/widget/RecyclerView;",
            "Ljava/lang/Float;",
            "Lcom/twitter/android/moments/ui/scroll/c;",
            "Lcom/twitter/android/moments/ui/scroll/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/android/moments/ui/scroll/f;

.field private c:Lcom/twitter/android/moments/ui/scroll/a;

.field private d:Z

.field private e:I


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/scroll/f;Lcom/twitter/util/object/g;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/ui/scroll/f;",
            "Lcom/twitter/util/object/g",
            "<",
            "Landroid/support/v7/widget/RecyclerView;",
            "Ljava/lang/Float;",
            "Lcom/twitter/android/moments/ui/scroll/c;",
            "Lcom/twitter/android/moments/ui/scroll/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x3

    iput v0, p0, Lcom/twitter/android/moments/ui/scroll/e;->e:I

    .line 41
    iput-object p2, p0, Lcom/twitter/android/moments/ui/scroll/e;->a:Lcom/twitter/util/object/g;

    .line 42
    iput-object p1, p0, Lcom/twitter/android/moments/ui/scroll/e;->b:Lcom/twitter/android/moments/ui/scroll/f;

    .line 43
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/twitter/android/moments/ui/scroll/e;
    .locals 3

    .prologue
    .line 33
    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/ViewConfigurationCompat;->getScaledPagingTouchSlop(Landroid/view/ViewConfiguration;)I

    move-result v0

    .line 34
    new-instance v1, Lcom/twitter/android/moments/ui/scroll/e;

    new-instance v2, Lcom/twitter/android/moments/ui/scroll/f;

    int-to-float v0, v0

    invoke-direct {v2, v0}, Lcom/twitter/android/moments/ui/scroll/f;-><init>(F)V

    sget-object v0, Lcom/twitter/android/moments/ui/scroll/a;->a:Lcom/twitter/util/object/g;

    invoke-direct {v1, v2, v0}, Lcom/twitter/android/moments/ui/scroll/e;-><init>(Lcom/twitter/android/moments/ui/scroll/f;Lcom/twitter/util/object/g;)V

    return-object v1
.end method

.method private a()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/e;->c:Lcom/twitter/android/moments/ui/scroll/a;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/e;->c:Lcom/twitter/android/moments/ui/scroll/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/scroll/a;->c()V

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/scroll/e;->c:Lcom/twitter/android/moments/ui/scroll/a;

    .line 85
    :cond_0
    return-void
.end method

.method private a(Landroid/support/v7/widget/RecyclerView;I)Z
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/scroll/e;->a()V

    .line 92
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/moments/ui/scroll/e;->c(Landroid/support/v7/widget/RecyclerView;I)Lcom/twitter/android/moments/ui/scroll/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/scroll/e;->c:Lcom/twitter/android/moments/ui/scroll/a;

    .line 93
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/e;->c:Lcom/twitter/android/moments/ui/scroll/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/scroll/a;->a()I

    move-result v0

    .line 94
    invoke-direct {p0, p1, v0}, Lcom/twitter/android/moments/ui/scroll/e;->b(Landroid/support/v7/widget/RecyclerView;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/e;->c:Lcom/twitter/android/moments/ui/scroll/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/scroll/a;->b()V

    .line 96
    const/4 v0, 0x1

    .line 100
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/support/v7/widget/RecyclerView;I)Z
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    if-eqz v0, :cond_0

    if-ltz p2, :cond_0

    .line 110
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$Adapter;->getItemCount()I

    move-result v0

    if-ge p2, v0, :cond_0

    const/4 v0, 0x1

    .line 109
    :goto_0
    return v0

    .line 110
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Landroid/support/v7/widget/RecyclerView;I)Lcom/twitter/android/moments/ui/scroll/a;
    .locals 3

    .prologue
    .line 116
    new-instance v0, Lcom/twitter/android/moments/ui/scroll/c;

    invoke-static {}, Lcom/twitter/util/collection/k;->a()Lcom/twitter/util/collection/k;

    move-result-object v1

    int-to-float v2, p2

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/moments/ui/scroll/c;-><init>(Lcom/twitter/util/collection/k;F)V

    .line 117
    iget-object v1, p0, Lcom/twitter/android/moments/ui/scroll/e;->a:Lcom/twitter/util/object/g;

    const/high16 v2, 0x40e00000    # 7.0f

    .line 118
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    .line 117
    invoke-interface {v1, p1, v2, v0}, Lcom/twitter/util/object/g;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/scroll/a;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/support/v7/widget/RecyclerView;II)Z
    .locals 4

    .prologue
    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/scroll/e;->d:Z

    .line 48
    int-to-double v0, p2

    const-wide v2, 0x3ff4cccccccccccdL    # 1.3

    mul-double/2addr v0, v2

    double-to-int v0, v0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/moments/ui/scroll/e;->a(Landroid/support/v7/widget/RecyclerView;I)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/support/v7/widget/RecyclerView;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 55
    invoke-static {p2}, Landroid/support/v4/view/MotionEventCompat;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result v5

    .line 56
    iget v0, p0, Lcom/twitter/android/moments/ui/scroll/e;->e:I

    if-ne v0, v1, :cond_6

    if-eqz v5, :cond_0

    const/4 v0, 0x2

    if-ne v5, v0, :cond_6

    :cond_0
    move v4, v1

    .line 58
    :goto_0
    if-nez v4, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/scroll/e;->d:Z

    if-nez v0, :cond_1

    if-eq v5, v1, :cond_2

    :cond_1
    const/4 v0, 0x3

    if-ne v5, v0, :cond_7

    :cond_2
    move v3, v1

    .line 62
    :goto_1
    if-ne v5, v1, :cond_8

    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/scroll/e;->d:Z

    if-nez v0, :cond_8

    move v0, v1

    .line 63
    :goto_2
    if-eqz v4, :cond_3

    .line 64
    iget-object v4, p0, Lcom/twitter/android/moments/ui/scroll/e;->b:Lcom/twitter/android/moments/ui/scroll/f;

    invoke-virtual {v4}, Lcom/twitter/android/moments/ui/scroll/f;->a()V

    .line 66
    :cond_3
    iget-object v4, p0, Lcom/twitter/android/moments/ui/scroll/e;->b:Lcom/twitter/android/moments/ui/scroll/f;

    invoke-virtual {v4, p2}, Lcom/twitter/android/moments/ui/scroll/f;->a(Landroid/view/MotionEvent;)V

    .line 67
    if-eqz v3, :cond_4

    .line 68
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/scroll/e;->a()V

    .line 70
    :cond_4
    if-eqz v0, :cond_5

    .line 71
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/e;->b:Lcom/twitter/android/moments/ui/scroll/f;

    .line 72
    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/scroll/f;->b()Lcom/twitter/android/moments/ui/scroll/Direction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/scroll/Direction;->a()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/moments/ui/scroll/e;->c(Landroid/support/v7/widget/RecyclerView;I)Lcom/twitter/android/moments/ui/scroll/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/scroll/e;->c:Lcom/twitter/android/moments/ui/scroll/a;

    .line 73
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/e;->c:Lcom/twitter/android/moments/ui/scroll/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/scroll/a;->b()V

    .line 75
    :cond_5
    iput-boolean v2, p0, Lcom/twitter/android/moments/ui/scroll/e;->d:Z

    .line 76
    iput v5, p0, Lcom/twitter/android/moments/ui/scroll/e;->e:I

    .line 77
    return v1

    :cond_6
    move v4, v2

    .line 56
    goto :goto_0

    :cond_7
    move v3, v2

    .line 58
    goto :goto_1

    :cond_8
    move v0, v2

    .line 62
    goto :goto_2
.end method
