.class public Lcom/twitter/android/moments/ui/scroll/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/animation/TimeAnimator$TimeListener;


# static fields
.field public static final a:Lcom/twitter/util/object/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/g",
            "<",
            "Landroid/support/v7/widget/RecyclerView;",
            "Ljava/lang/Float;",
            "Lcom/twitter/android/moments/ui/scroll/c;",
            "Lcom/twitter/android/moments/ui/scroll/a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/twitter/android/moments/ui/scroll/b;

.field private final c:Lcom/twitter/ui/anim/f;

.field private final d:Landroid/animation/TimeAnimator;

.field private final e:Lcom/twitter/ui/anim/f$a;

.field private final f:I

.field private final g:I

.field private final h:I

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/twitter/android/moments/ui/scroll/a$1;

    invoke-direct {v0}, Lcom/twitter/android/moments/ui/scroll/a$1;-><init>()V

    sput-object v0, Lcom/twitter/android/moments/ui/scroll/a;->a:Lcom/twitter/util/object/g;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/android/moments/ui/scroll/c;Lcom/twitter/android/moments/ui/scroll/d;Landroid/animation/TimeAnimator;Lcom/twitter/ui/anim/f$a;Lcom/twitter/android/moments/ui/scroll/b;F)V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p4, p0, Lcom/twitter/android/moments/ui/scroll/a;->e:Lcom/twitter/ui/anim/f$a;

    .line 80
    iput-object p5, p0, Lcom/twitter/android/moments/ui/scroll/a;->b:Lcom/twitter/android/moments/ui/scroll/b;

    .line 81
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/a;->b:Lcom/twitter/android/moments/ui/scroll/b;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/scroll/b;->a()I

    move-result v0

    mul-int/lit8 v0, v0, 0x19

    iput v0, p0, Lcom/twitter/android/moments/ui/scroll/a;->f:I

    .line 82
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/a;->b:Lcom/twitter/android/moments/ui/scroll/b;

    .line 83
    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/scroll/b;->a()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/twitter/android/moments/ui/scroll/a;->g:I

    .line 84
    iput-object p3, p0, Lcom/twitter/android/moments/ui/scroll/a;->d:Landroid/animation/TimeAnimator;

    .line 85
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/a;->d:Landroid/animation/TimeAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/TimeAnimator;->setTimeListener(Landroid/animation/TimeAnimator$TimeListener;)V

    .line 86
    iget-object v0, p1, Lcom/twitter/android/moments/ui/scroll/c;->a:Lcom/twitter/util/collection/k;

    invoke-virtual {v0}, Lcom/twitter/util/collection/k;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/twitter/android/moments/ui/scroll/c;->a:Lcom/twitter/util/collection/k;

    .line 87
    invoke-virtual {v0}, Lcom/twitter/util/collection/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 88
    :goto_0
    iput v0, p0, Lcom/twitter/android/moments/ui/scroll/a;->h:I

    .line 90
    iget v0, p0, Lcom/twitter/android/moments/ui/scroll/a;->h:I

    invoke-direct {p0, v0, p6}, Lcom/twitter/android/moments/ui/scroll/a;->a(IF)Lcom/twitter/ui/anim/f;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/scroll/a;->c:Lcom/twitter/ui/anim/f;

    .line 91
    return-void

    .line 87
    :cond_0
    iget v0, p1, Lcom/twitter/android/moments/ui/scroll/c;->b:F

    .line 88
    invoke-virtual {p2, p6, v0}, Lcom/twitter/android/moments/ui/scroll/d;->a(FF)I

    move-result v0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/android/moments/ui/scroll/c;Landroid/support/v7/widget/RecyclerView;F)Lcom/twitter/android/moments/ui/scroll/a;
    .locals 7

    .prologue
    .line 67
    new-instance v0, Lcom/twitter/android/moments/ui/scroll/a;

    .line 68
    invoke-static {p1}, Lcom/twitter/android/moments/ui/scroll/d;->a(Landroid/support/v7/widget/RecyclerView;)Lcom/twitter/android/moments/ui/scroll/d;

    move-result-object v2

    new-instance v3, Landroid/animation/TimeAnimator;

    invoke-direct {v3}, Landroid/animation/TimeAnimator;-><init>()V

    sget-object v4, Lcom/twitter/ui/anim/f;->a:Lcom/twitter/ui/anim/f$a;

    new-instance v5, Lcom/twitter/android/moments/ui/scroll/b;

    invoke-direct {v5, p1}, Lcom/twitter/android/moments/ui/scroll/b;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    move-object v1, p0

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/moments/ui/scroll/a;-><init>(Lcom/twitter/android/moments/ui/scroll/c;Lcom/twitter/android/moments/ui/scroll/d;Landroid/animation/TimeAnimator;Lcom/twitter/ui/anim/f$a;Lcom/twitter/android/moments/ui/scroll/b;F)V

    .line 67
    return-object v0
.end method

.method private a(IF)Lcom/twitter/ui/anim/f;
    .locals 4

    .prologue
    .line 105
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/a;->b:Lcom/twitter/android/moments/ui/scroll/b;

    sget-object v1, Lcom/twitter/android/moments/ui/scroll/Direction;->b:Lcom/twitter/android/moments/ui/scroll/Direction;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/scroll/b;->b(Lcom/twitter/android/moments/ui/scroll/Direction;)I

    move-result v0

    .line 106
    iget-object v1, p0, Lcom/twitter/android/moments/ui/scroll/a;->b:Lcom/twitter/android/moments/ui/scroll/b;

    sget-object v2, Lcom/twitter/android/moments/ui/scroll/Direction;->b:Lcom/twitter/android/moments/ui/scroll/Direction;

    invoke-virtual {v1, v2}, Lcom/twitter/android/moments/ui/scroll/b;->a(Lcom/twitter/android/moments/ui/scroll/Direction;)I

    move-result v1

    .line 107
    iget-object v2, p0, Lcom/twitter/android/moments/ui/scroll/a;->b:Lcom/twitter/android/moments/ui/scroll/b;

    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/scroll/b;->c()I

    move-result v2

    .line 108
    sub-int v0, p1, v0

    .line 109
    mul-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 112
    invoke-static {p2, v1}, Lcom/twitter/ui/anim/g;->a(FI)F

    move-result v0

    .line 113
    if-eqz v1, :cond_0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/twitter/android/moments/ui/scroll/a;->f:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 115
    int-to-float v0, v1

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    iget v2, p0, Lcom/twitter/android/moments/ui/scroll/a;->f:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    .line 116
    int-to-float v1, v1

    invoke-static {v0, v1}, Lcom/twitter/ui/anim/g;->a(FF)F

    move-result p2

    .line 118
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/moments/ui/scroll/a;->e:Lcom/twitter/ui/anim/f$a;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/twitter/ui/anim/f$a;->a(Ljava/lang/Float;Ljava/lang/Float;)Lcom/twitter/ui/anim/f;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/twitter/android/moments/ui/scroll/a;->h:I

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/a;->d:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->start()V

    .line 123
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/a;->b:Lcom/twitter/android/moments/ui/scroll/b;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/scroll/b;->b()V

    .line 127
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/a;->d:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->cancel()V

    .line 128
    return-void
.end method

.method public onTimeUpdate(Landroid/animation/TimeAnimator;JJ)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 132
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/a;->c:Lcom/twitter/ui/anim/f;

    long-to-float v1, p2

    invoke-virtual {v0, v1}, Lcom/twitter/ui/anim/f;->a(F)F

    move-result v0

    iget v1, p0, Lcom/twitter/android/moments/ui/scroll/a;->i:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    .line 133
    float-to-int v1, v0

    .line 134
    iget-object v2, p0, Lcom/twitter/android/moments/ui/scroll/a;->b:Lcom/twitter/android/moments/ui/scroll/b;

    invoke-virtual {v2, v1, v3}, Lcom/twitter/android/moments/ui/scroll/b;->a(II)V

    .line 135
    iget-object v1, p0, Lcom/twitter/android/moments/ui/scroll/a;->c:Lcom/twitter/ui/anim/f;

    long-to-float v2, p2

    invoke-virtual {v1, v2}, Lcom/twitter/ui/anim/f;->b(F)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/twitter/android/moments/ui/scroll/a;->g:I

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 136
    iget-object v1, p0, Lcom/twitter/android/moments/ui/scroll/a;->b:Lcom/twitter/android/moments/ui/scroll/b;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/scroll/b;->b()V

    .line 138
    :cond_0
    iget v1, p0, Lcom/twitter/android/moments/ui/scroll/a;->i:I

    int-to-float v1, v1

    add-float/2addr v1, v0

    iget-object v2, p0, Lcom/twitter/android/moments/ui/scroll/a;->c:Lcom/twitter/ui/anim/f;

    invoke-virtual {v2}, Lcom/twitter/ui/anim/f;->a()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v2, 0x3f000000    # 0.5f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    .line 140
    iget-object v1, p0, Lcom/twitter/android/moments/ui/scroll/a;->b:Lcom/twitter/android/moments/ui/scroll/b;

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/moments/ui/scroll/b;->a(II)V

    .line 141
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/scroll/a;->c()V

    .line 143
    :cond_1
    iget v1, p0, Lcom/twitter/android/moments/ui/scroll/a;->i:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/twitter/android/moments/ui/scroll/a;->i:I

    .line 144
    return-void
.end method
