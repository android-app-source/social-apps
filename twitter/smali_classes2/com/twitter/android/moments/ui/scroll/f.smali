.class public Lcom/twitter/android/moments/ui/scroll/f;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:F

.field private b:F

.field private c:Lcom/twitter/android/moments/ui/scroll/Direction;

.field private d:Z


# direct methods
.method public constructor <init>(F)V
    .locals 2

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lcom/twitter/android/moments/ui/scroll/f;->b:F

    .line 10
    sget-object v0, Lcom/twitter/android/moments/ui/scroll/Direction;->a:Lcom/twitter/android/moments/ui/scroll/Direction;

    iput-object v0, p0, Lcom/twitter/android/moments/ui/scroll/f;->c:Lcom/twitter/android/moments/ui/scroll/Direction;

    .line 14
    const/high16 v0, 0x40400000    # 3.0f

    div-float v0, p1, v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/twitter/android/moments/ui/scroll/f;->a:F

    .line 15
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/twitter/android/moments/ui/scroll/Direction;->a:Lcom/twitter/android/moments/ui/scroll/Direction;

    iput-object v0, p0, Lcom/twitter/android/moments/ui/scroll/f;->c:Lcom/twitter/android/moments/ui/scroll/Direction;

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/scroll/f;->d:Z

    .line 38
    return-void
.end method

.method public a(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 18
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 19
    iget-boolean v1, p0, Lcom/twitter/android/moments/ui/scroll/f;->d:Z

    if-nez v1, :cond_0

    .line 20
    iput v0, p0, Lcom/twitter/android/moments/ui/scroll/f;->b:F

    .line 21
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/twitter/android/moments/ui/scroll/f;->d:Z

    .line 23
    :cond_0
    iget v1, p0, Lcom/twitter/android/moments/ui/scroll/f;->b:F

    iget v2, p0, Lcom/twitter/android/moments/ui/scroll/f;->a:F

    sub-float/2addr v1, v2

    cmpg-float v1, v0, v1

    if-gez v1, :cond_3

    .line 24
    sget-object v1, Lcom/twitter/android/moments/ui/scroll/Direction;->c:Lcom/twitter/android/moments/ui/scroll/Direction;

    iput-object v1, p0, Lcom/twitter/android/moments/ui/scroll/f;->c:Lcom/twitter/android/moments/ui/scroll/Direction;

    .line 28
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/moments/ui/scroll/f;->c:Lcom/twitter/android/moments/ui/scroll/Direction;

    sget-object v2, Lcom/twitter/android/moments/ui/scroll/Direction;->c:Lcom/twitter/android/moments/ui/scroll/Direction;

    if-ne v1, v2, :cond_4

    iget v1, p0, Lcom/twitter/android/moments/ui/scroll/f;->b:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_4

    .line 29
    iput v0, p0, Lcom/twitter/android/moments/ui/scroll/f;->b:F

    .line 33
    :cond_2
    :goto_1
    return-void

    .line 25
    :cond_3
    iget v1, p0, Lcom/twitter/android/moments/ui/scroll/f;->b:F

    iget v2, p0, Lcom/twitter/android/moments/ui/scroll/f;->a:F

    add-float/2addr v1, v2

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    .line 26
    sget-object v1, Lcom/twitter/android/moments/ui/scroll/Direction;->b:Lcom/twitter/android/moments/ui/scroll/Direction;

    iput-object v1, p0, Lcom/twitter/android/moments/ui/scroll/f;->c:Lcom/twitter/android/moments/ui/scroll/Direction;

    goto :goto_0

    .line 30
    :cond_4
    iget-object v1, p0, Lcom/twitter/android/moments/ui/scroll/f;->c:Lcom/twitter/android/moments/ui/scroll/Direction;

    sget-object v2, Lcom/twitter/android/moments/ui/scroll/Direction;->b:Lcom/twitter/android/moments/ui/scroll/Direction;

    if-ne v1, v2, :cond_2

    iget v1, p0, Lcom/twitter/android/moments/ui/scroll/f;->b:F

    cmpl-float v1, v0, v1

    if-lez v1, :cond_2

    .line 31
    iput v0, p0, Lcom/twitter/android/moments/ui/scroll/f;->b:F

    goto :goto_1
.end method

.method public b()Lcom/twitter/android/moments/ui/scroll/Direction;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/moments/ui/scroll/f;->c:Lcom/twitter/android/moments/ui/scroll/Direction;

    return-object v0
.end method
