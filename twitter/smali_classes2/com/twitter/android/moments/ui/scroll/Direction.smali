.class public final enum Lcom/twitter/android/moments/ui/scroll/Direction;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/twitter/android/moments/ui/scroll/Direction;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/twitter/android/moments/ui/scroll/Direction;

.field public static final enum b:Lcom/twitter/android/moments/ui/scroll/Direction;

.field public static final enum c:Lcom/twitter/android/moments/ui/scroll/Direction;

.field private static final synthetic d:[Lcom/twitter/android/moments/ui/scroll/Direction;


# instance fields
.field private final mDirectionInteger:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4
    new-instance v0, Lcom/twitter/android/moments/ui/scroll/Direction;

    const-string/jumbo v1, "NO_DIRECTION"

    invoke-direct {v0, v1, v3, v3}, Lcom/twitter/android/moments/ui/scroll/Direction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/twitter/android/moments/ui/scroll/Direction;->a:Lcom/twitter/android/moments/ui/scroll/Direction;

    .line 5
    new-instance v0, Lcom/twitter/android/moments/ui/scroll/Direction;

    const-string/jumbo v1, "LEFT"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/twitter/android/moments/ui/scroll/Direction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/twitter/android/moments/ui/scroll/Direction;->b:Lcom/twitter/android/moments/ui/scroll/Direction;

    .line 6
    new-instance v0, Lcom/twitter/android/moments/ui/scroll/Direction;

    const-string/jumbo v1, "RIGHT"

    invoke-direct {v0, v1, v5, v4}, Lcom/twitter/android/moments/ui/scroll/Direction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/twitter/android/moments/ui/scroll/Direction;->c:Lcom/twitter/android/moments/ui/scroll/Direction;

    .line 3
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/twitter/android/moments/ui/scroll/Direction;

    sget-object v1, Lcom/twitter/android/moments/ui/scroll/Direction;->a:Lcom/twitter/android/moments/ui/scroll/Direction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/android/moments/ui/scroll/Direction;->b:Lcom/twitter/android/moments/ui/scroll/Direction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/android/moments/ui/scroll/Direction;->c:Lcom/twitter/android/moments/ui/scroll/Direction;

    aput-object v1, v0, v5

    sput-object v0, Lcom/twitter/android/moments/ui/scroll/Direction;->d:[Lcom/twitter/android/moments/ui/scroll/Direction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 11
    iput p3, p0, Lcom/twitter/android/moments/ui/scroll/Direction;->mDirectionInteger:I

    .line 12
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/android/moments/ui/scroll/Direction;
    .locals 1

    .prologue
    .line 3
    const-class v0, Lcom/twitter/android/moments/ui/scroll/Direction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/scroll/Direction;

    return-object v0
.end method

.method public static values()[Lcom/twitter/android/moments/ui/scroll/Direction;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/twitter/android/moments/ui/scroll/Direction;->d:[Lcom/twitter/android/moments/ui/scroll/Direction;

    invoke-virtual {v0}, [Lcom/twitter/android/moments/ui/scroll/Direction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/moments/ui/scroll/Direction;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lcom/twitter/android/moments/ui/scroll/Direction;->mDirectionInteger:I

    return v0
.end method
