.class public Lcom/twitter/android/moments/ui/b$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/support/v4/app/FragmentManager;

.field private final c:Lcom/twitter/library/client/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Lcom/twitter/library/client/v;)V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object p1, p0, Lcom/twitter/android/moments/ui/b$a;->a:Landroid/content/Context;

    .line 91
    iput-object p2, p0, Lcom/twitter/android/moments/ui/b$a;->b:Landroid/support/v4/app/FragmentManager;

    .line 92
    iput-object p3, p0, Lcom/twitter/android/moments/ui/b$a;->c:Lcom/twitter/library/client/v;

    .line 93
    return-void
.end method


# virtual methods
.method public a(JLcgi;)Lrx/g;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcgi;",
            ")",
            "Lrx/g",
            "<",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Lbes;",
            "Lcom/twitter/library/service/u;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 104
    new-instance v1, Lbes;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/b$a;->a:Landroid/content/Context;

    iget-object v0, p0, Lcom/twitter/android/moments/ui/b$a;->c:Lcom/twitter/library/client/v;

    .line 105
    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    const/4 v7, 0x1

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v1 .. v7}, Lbes;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;I)V

    .line 104
    invoke-static {v1}, Lcom/twitter/library/util/ae;->b(Lcom/twitter/library/service/s;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lrx/g;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/g",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lcom/twitter/android/moments/ui/b$a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/twitter/android/moments/ui/b$a;->b:Landroid/support/v4/app/FragmentManager;

    invoke-static {v0, p1, v1, v2}, Lcom/twitter/android/util/y;->a(Landroid/content/res/Resources;Ljava/lang/String;ILandroid/support/v4/app/FragmentManager;)Lrx/g;

    move-result-object v0

    return-object v0
.end method
