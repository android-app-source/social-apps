.class public Lcom/twitter/android/moments/ui/maker/MomentMakerAddTweetsActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/maker/MomentMakerAddTweetsActivity$a;
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/android/moments/ui/maker/ai;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Laog;
    .locals 6

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/MomentMakerAddTweetsActivity;->X()Lann;

    move-result-object v0

    check-cast v0, Lzd;

    .line 47
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/MomentMakerAddTweetsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "moment_id"

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 48
    invoke-interface {v0}, Lzd;->d()Lyf;

    move-result-object v1

    .line 49
    invoke-interface {v0}, Lzd;->e()Lya;

    move-result-object v2

    .line 50
    invoke-interface {v0}, Lzd;->f()Lxp;

    move-result-object v3

    move-object v0, p0

    .line 48
    invoke-static/range {v0 .. v5}, Lcom/twitter/android/moments/ui/maker/ai;->a(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lyf;Lya;Lxp;J)Lcom/twitter/android/moments/ui/maker/ai;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/MomentMakerAddTweetsActivity;->a:Lcom/twitter/android/moments/ui/maker/ai;

    .line 51
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/MomentMakerAddTweetsActivity;->a:Lcom/twitter/android/moments/ui/maker/ai;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 30
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    return-object v0
.end method

.method protected d(Lank;)Lzd;
    .locals 5

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/MomentMakerAddTweetsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "moment_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 37
    invoke-static {}, Lyz;->a()Lyz$a;

    move-result-object v2

    .line 38
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v3

    invoke-virtual {v2, v3}, Lyz$a;->a(Lamu;)Lyz$a;

    move-result-object v2

    new-instance v3, Lano;

    new-instance v4, Lzj;

    invoke-direct {v4, v0, v1}, Lzj;-><init>(J)V

    invoke-direct {v3, v4}, Lano;-><init>(Lanr;)V

    .line 39
    invoke-virtual {v2, v3}, Lyz$a;->a(Lano;)Lyz$a;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lyz$a;->a()Lzd;

    move-result-object v0

    .line 37
    return-object v0
.end method

.method protected synthetic e(Lank;)Lcom/twitter/app/common/base/i;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/MomentMakerAddTweetsActivity;->d(Lank;)Lzd;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f(Lank;)Lcom/twitter/app/common/abs/a;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/MomentMakerAddTweetsActivity;->d(Lank;)Lzd;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic g(Lank;)Lann;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/MomentMakerAddTweetsActivity;->d(Lank;)Lzd;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/MomentMakerAddTweetsActivity;->a:Lcom/twitter/android/moments/ui/maker/ai;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/MomentMakerAddTweetsActivity;->a:Lcom/twitter/android/moments/ui/maker/ai;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/ai;->e()V

    .line 61
    :goto_0
    return-void

    .line 59
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method
