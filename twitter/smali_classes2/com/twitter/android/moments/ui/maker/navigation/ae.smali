.class public Lcom/twitter/android/moments/ui/maker/navigation/ae;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/navigation/d;

.field private final b:Lcom/twitter/android/moments/ui/maker/navigation/y;

.field private final c:Lcom/twitter/android/moments/ui/maker/navigation/r;

.field private final d:Lcom/twitter/android/moments/ui/maker/navigation/l;

.field private final e:Lcom/twitter/android/moments/ui/maker/navigation/o;

.field private final f:Lcom/twitter/android/moments/ui/maker/navigation/i;

.field private final g:Lcom/twitter/android/moments/viewmodels/l;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/navigation/y;Lcom/twitter/android/moments/ui/maker/navigation/d;Lcom/twitter/android/moments/ui/maker/navigation/r;Lcom/twitter/android/moments/ui/maker/navigation/l;Lcom/twitter/android/moments/ui/maker/navigation/o;Lcom/twitter/android/moments/ui/maker/navigation/i;Lcom/twitter/android/moments/viewmodels/l;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/navigation/ae;->a:Lcom/twitter/android/moments/ui/maker/navigation/d;

    .line 59
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/navigation/ae;->b:Lcom/twitter/android/moments/ui/maker/navigation/y;

    .line 60
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/navigation/ae;->c:Lcom/twitter/android/moments/ui/maker/navigation/r;

    .line 61
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/navigation/ae;->d:Lcom/twitter/android/moments/ui/maker/navigation/l;

    .line 62
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/navigation/ae;->e:Lcom/twitter/android/moments/ui/maker/navigation/o;

    .line 63
    iput-object p6, p0, Lcom/twitter/android/moments/ui/maker/navigation/ae;->f:Lcom/twitter/android/moments/ui/maker/navigation/i;

    .line 64
    iput-object p7, p0, Lcom/twitter/android/moments/ui/maker/navigation/ae;->g:Lcom/twitter/android/moments/viewmodels/l;

    .line 65
    return-void
.end method

.method public static a(Lcom/twitter/app/common/base/BaseFragmentActivity;Lwy;Lcom/twitter/android/moments/ui/maker/ap$a;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/maker/a;Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;Lakt;J)Lcom/twitter/android/moments/ui/maker/navigation/ae;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/app/common/base/BaseFragmentActivity;",
            "Lwy;",
            "Lcom/twitter/android/moments/ui/maker/ap$a;",
            "Landroid/view/ViewGroup;",
            "Lcom/twitter/android/moments/ui/maker/a;",
            "Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;",
            "Lakt",
            "<",
            "Lcom/twitter/android/moments/ui/maker/y$a;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Landroid/net/Uri;",
            ">;>;J)",
            "Lcom/twitter/android/moments/ui/maker/navigation/ae;"
        }
    .end annotation

    .prologue
    .line 40
    new-instance v4, Lcom/twitter/android/moments/ui/maker/navigation/ae;

    new-instance v5, Lcom/twitter/android/moments/ui/maker/navigation/y;

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-wide/from16 v10, p7

    invoke-direct/range {v5 .. v11}, Lcom/twitter/android/moments/ui/maker/navigation/y;-><init>(Landroid/app/Activity;Lwy;Lcom/twitter/android/moments/ui/maker/ap$a;Landroid/view/ViewGroup;J)V

    new-instance v7, Lcom/twitter/android/moments/ui/maker/navigation/d;

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    move-object/from16 v13, p5

    move-wide/from16 v14, p7

    invoke-direct/range {v7 .. v15}, Lcom/twitter/android/moments/ui/maker/navigation/d;-><init>(Lcom/twitter/app/common/base/BaseFragmentActivity;Lwy;Lcom/twitter/android/moments/ui/maker/ap$a;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/maker/a;Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;J)V

    new-instance v8, Lcom/twitter/android/moments/ui/maker/navigation/r;

    move-object/from16 v9, p0

    move-object/from16 v10, p3

    move-object/from16 v11, p1

    move-wide/from16 v12, p7

    invoke-direct/range {v8 .. v13}, Lcom/twitter/android/moments/ui/maker/navigation/r;-><init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lwy;J)V

    new-instance v9, Lcom/twitter/android/moments/ui/maker/navigation/l;

    move-object/from16 v10, p0

    move-object/from16 v11, p3

    move-object/from16 v12, p1

    move-object/from16 v13, p6

    move-wide/from16 v14, p7

    invoke-direct/range {v9 .. v15}, Lcom/twitter/android/moments/ui/maker/navigation/l;-><init>(Lcom/twitter/app/common/base/BaseFragmentActivity;Landroid/view/ViewGroup;Lwy;Lakt;J)V

    new-instance v16, Lcom/twitter/android/moments/ui/maker/navigation/o;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, p3

    move-object/from16 v3, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/moments/ui/maker/navigation/o;-><init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lwy;)V

    new-instance v10, Lcom/twitter/android/moments/ui/maker/navigation/i;

    move-object/from16 v11, p0

    move-object/from16 v12, p3

    move-object/from16 v13, p1

    move-wide/from16 v14, p7

    invoke-direct/range {v10 .. v15}, Lcom/twitter/android/moments/ui/maker/navigation/i;-><init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lwy;J)V

    .line 49
    invoke-static {}, Lcom/twitter/android/moments/viewmodels/l;->a()Lcom/twitter/android/moments/viewmodels/l;

    move-result-object v18

    move-object v11, v4

    move-object v12, v5

    move-object v13, v7

    move-object v14, v8

    move-object v15, v9

    move-object/from16 v17, v10

    invoke-direct/range {v11 .. v18}, Lcom/twitter/android/moments/ui/maker/navigation/ae;-><init>(Lcom/twitter/android/moments/ui/maker/navigation/y;Lcom/twitter/android/moments/ui/maker/navigation/d;Lcom/twitter/android/moments/ui/maker/navigation/r;Lcom/twitter/android/moments/ui/maker/navigation/l;Lcom/twitter/android/moments/ui/maker/navigation/o;Lcom/twitter/android/moments/ui/maker/navigation/i;Lcom/twitter/android/moments/viewmodels/l;)V

    .line 40
    return-object v4
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/ui/maker/navigation/NavigationKey;Lcom/twitter/android/moments/ui/maker/navigation/ag;Lcom/twitter/android/moments/ui/maker/navigation/ac;)Lcom/twitter/android/moments/ui/maker/navigation/ai;
    .locals 3

    .prologue
    .line 71
    sget-object v0, Lcom/twitter/android/moments/ui/maker/navigation/ae$1;->a:[I

    invoke-virtual {p2}, Lcom/twitter/android/moments/ui/maker/navigation/NavigationKey;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 126
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "key must have a Screen type defined"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :pswitch_0
    instance-of v0, p3, Lcom/twitter/android/moments/ui/maker/navigation/c;

    if-eqz v0, :cond_0

    .line 74
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/navigation/ae;->a:Lcom/twitter/android/moments/ui/maker/navigation/d;

    .line 75
    invoke-static {p3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/maker/navigation/c;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/navigation/c;->a()Lcim;

    move-result-object v0

    .line 74
    invoke-virtual {v1, p1, v0}, Lcom/twitter/android/moments/ui/maker/navigation/d;->a(Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcim;)Lcom/twitter/android/moments/ui/maker/navigation/b;

    move-result-object v0

    .line 129
    :goto_0
    return-object v0

    .line 78
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Canvas Screen must have CanvasScreenArgs"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :pswitch_1
    instance-of v0, p3, Lcom/twitter/android/moments/ui/maker/navigation/x;

    if-eqz v0, :cond_1

    .line 83
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/navigation/ae;->b:Lcom/twitter/android/moments/ui/maker/navigation/y;

    .line 84
    invoke-static {p3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/maker/navigation/x;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/navigation/x;->a()Lcom/twitter/model/moments/r;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Lcom/twitter/android/moments/ui/maker/navigation/y;->a(Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/model/moments/r;)Lcom/twitter/android/moments/ui/maker/navigation/w;

    move-result-object v0

    goto :goto_0

    .line 87
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Grid Screen must have GridScreenArgs"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :pswitch_2
    instance-of v0, p3, Lcom/twitter/android/moments/ui/maker/navigation/q;

    if-eqz v0, :cond_2

    .line 92
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/navigation/ae;->c:Lcom/twitter/android/moments/ui/maker/navigation/r;

    .line 93
    invoke-static {p3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/maker/navigation/q;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/navigation/q;->a()Lcom/twitter/model/moments/viewmodels/g;

    move-result-object v0

    .line 92
    invoke-virtual {v1, p1, v0}, Lcom/twitter/android/moments/ui/maker/navigation/r;->a(Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/model/moments/viewmodels/g;)Lcom/twitter/android/moments/ui/maker/navigation/p;

    move-result-object v0

    goto :goto_0

    .line 96
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Cropper Screen must have CropperScreenArgs"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/ae;->d:Lcom/twitter/android/moments/ui/maker/navigation/l;

    sget-object v1, Lcom/twitter/android/moments/ui/maker/navigation/ag;->a:Lcom/twitter/android/moments/ui/maker/navigation/ag;

    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/moments/ui/maker/navigation/l;->a(Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/ui/maker/navigation/ag;)Lcom/twitter/android/moments/ui/maker/navigation/k;

    move-result-object v0

    goto :goto_0

    .line 104
    :pswitch_4
    instance-of v0, p3, Lcom/twitter/android/moments/ui/maker/navigation/n;

    if-eqz v0, :cond_4

    .line 105
    invoke-static {p3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/maker/navigation/n;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/navigation/n;->a()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 106
    if-nez v0, :cond_3

    const/4 v0, 0x0

    move-object v1, v0

    .line 108
    :goto_1
    invoke-static {p3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/maker/navigation/n;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/navigation/n;->b()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    .line 109
    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/navigation/ae;->e:Lcom/twitter/android/moments/ui/maker/navigation/o;

    invoke-virtual {v2, p1, v1, v0}, Lcom/twitter/android/moments/ui/maker/navigation/o;->a(Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/viewmodels/k;Lcom/twitter/model/moments/Moment;)Lcom/twitter/android/moments/ui/maker/navigation/m;

    move-result-object v0

    goto :goto_0

    .line 106
    :cond_3
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/navigation/ae;->g:Lcom/twitter/android/moments/viewmodels/l;

    .line 107
    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/viewmodels/l;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/android/moments/viewmodels/k;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 111
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Cover Screen must have CoverScreenArgs"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 116
    :pswitch_5
    instance-of v0, p3, Lcom/twitter/android/moments/ui/maker/navigation/h;

    if-eqz v0, :cond_5

    .line 117
    check-cast p3, Lcom/twitter/android/moments/ui/maker/navigation/h;

    .line 118
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/ae;->f:Lcom/twitter/android/moments/ui/maker/navigation/i;

    invoke-virtual {p3}, Lcom/twitter/android/moments/ui/maker/navigation/h;->a()Lcom/twitter/model/moments/r;

    move-result-object v1

    .line 119
    invoke-virtual {p3}, Lcom/twitter/android/moments/ui/maker/navigation/h;->b()Lcom/twitter/model/moments/w;

    move-result-object v2

    .line 118
    invoke-virtual {v0, v1, v2, p1}, Lcom/twitter/android/moments/ui/maker/navigation/i;->a(Lcom/twitter/model/moments/r;Lcom/twitter/model/moments/w;Lcom/twitter/android/moments/ui/maker/navigation/ah;)Lcom/twitter/android/moments/ui/maker/navigation/g;

    move-result-object v0

    goto/16 :goto_0

    .line 121
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Color Picker Screen must have ColorPickerArgs"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
