.class public Lcom/twitter/android/moments/ui/maker/b;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/model/core/TwitterUser;

.field private final c:Z


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/twitter/model/core/TwitterUser;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;",
            ">;",
            "Lcom/twitter/model/core/TwitterUser;",
            ")V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {}, Lbrz;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/maker/b;->c:Z

    .line 20
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/b;->a:Ljava/util/List;

    .line 21
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/b;->b:Lcom/twitter/model/core/TwitterUser;

    .line 22
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/b;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 27
    invoke-static {}, Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;->values()[Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 28
    invoke-virtual {p0, v4}, Lcom/twitter/android/moments/ui/maker/b;->b(Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0, v4}, Lcom/twitter/android/moments/ui/maker/b;->a(Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 29
    invoke-virtual {v1, v4}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 27
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 32
    :cond_1
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public a(Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;)Z
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;->a:Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/b;->b:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/b;->b:Lcom/twitter/model/core/TwitterUser;

    iget-boolean v0, v0, Lcom/twitter/model/core/TwitterUser;->l:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;)Z
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;->c:Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/maker/b;->c:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
