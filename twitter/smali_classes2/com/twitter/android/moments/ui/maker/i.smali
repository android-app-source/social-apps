.class public Lcom/twitter/android/moments/ui/maker/i;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lakv;
.implements Lcom/twitter/android/moments/ui/maker/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lakv",
        "<",
        "Lcom/twitter/android/client/r;",
        ">;",
        "Lcom/twitter/android/moments/ui/maker/e;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/viewdelegate/c;

.field private final b:Lcom/twitter/util/object/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/library/api/moments/maker/q;",
            "Lcom/twitter/android/moments/ui/maker/c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$b;

.field private final d:Lzt;

.field private final e:J

.field private f:Lcom/twitter/android/moments/ui/maker/c;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/c;Lcom/twitter/util/object/d;Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$b;JLzt;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/c;",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/library/api/moments/maker/q;",
            "Lcom/twitter/android/moments/ui/maker/c;",
            ">;",
            "Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$b;",
            "J",
            "Lzt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-wide p4, p0, Lcom/twitter/android/moments/ui/maker/i;->e:J

    .line 77
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/i;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/c;

    .line 78
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/i;->b:Lcom/twitter/util/object/d;

    .line 79
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/i;->c:Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$b;

    .line 80
    iput-object p6, p0, Lcom/twitter/android/moments/ui/maker/i;->d:Lzt;

    .line 81
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/i;->c:Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$b;

    invoke-virtual {v0, p0}, Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$b;->a(Lakv;)V

    .line 82
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/i;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/c;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/i$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/i$2;-><init>(Lcom/twitter/android/moments/ui/maker/i;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->a(Landroid/view/View$OnClickListener;)V

    .line 88
    return-void
.end method

.method public static a(Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/maker/ap$a;Lcom/twitter/android/moments/data/af;Lanh;Lya;Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$b;Lzt;Lxm;J)Lcom/twitter/android/moments/ui/maker/i;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;",
            "Landroid/app/Activity;",
            "Landroid/view/ViewGroup;",
            "Lcom/twitter/android/moments/ui/maker/ap$a;",
            "Lcom/twitter/android/moments/data/af",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;",
            "Lanh;",
            "Lya;",
            "Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$b;",
            "Lzt;",
            "Lxm;",
            "J)",
            "Lcom/twitter/android/moments/ui/maker/i;"
        }
    .end annotation

    .prologue
    .line 50
    .line 51
    invoke-static/range {p1 .. p2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/c;

    move-result-object v12

    .line 54
    invoke-virtual {v12}, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->aN_()Landroid/view/View;

    move-result-object v0

    .line 53
    invoke-static {p1, v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->a(Landroid/content/Context;Landroid/view/View;)Lcom/twitter/android/moments/ui/maker/viewdelegate/b;

    move-result-object v3

    .line 56
    new-instance v0, Lcom/twitter/android/moments/ui/maker/i$1;

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p8

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p9

    move-wide/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/twitter/android/moments/ui/maker/i$1;-><init>(Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;Landroid/app/Activity;Lcom/twitter/android/moments/ui/maker/viewdelegate/b;Lcom/twitter/android/moments/ui/maker/ap$a;Lcom/twitter/android/moments/data/af;Lzt;Lanh;Lya;Lxm;J)V

    .line 68
    new-instance v2, Lcom/twitter/android/moments/ui/maker/i;

    .line 69
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    move-object v3, v12

    move-object v4, v0

    move-object/from16 v5, p7

    move-object/from16 v8, p8

    invoke-direct/range {v2 .. v8}, Lcom/twitter/android/moments/ui/maker/i;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/c;Lcom/twitter/util/object/d;Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$b;JLzt;)V

    .line 68
    return-object v2
.end method

.method private a(Lcom/twitter/android/client/r;)V
    .locals 5

    .prologue
    .line 100
    iget-object v0, p1, Lcom/twitter/android/client/r;->b:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    sget-object v1, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->b:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    if-ne v0, v1, :cond_1

    iget-object v0, p1, Lcom/twitter/android/client/r;->d:Lcom/twitter/model/search/viewmodel/g;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 102
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/i;->d:Lzt;

    invoke-virtual {v0, v1}, Lzt;->a(Z)V

    .line 103
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/i;->d:Lzt;

    invoke-virtual {v0}, Lzt;->a()V

    .line 104
    if-eqz v1, :cond_2

    sget-object v0, Lcom/twitter/library/api/moments/maker/RecommendationType;->a:Lcom/twitter/library/api/moments/maker/RecommendationType;

    iget-object v2, p1, Lcom/twitter/android/client/r;->d:Lcom/twitter/model/search/viewmodel/g;

    iget-wide v2, v2, Lcom/twitter/model/search/viewmodel/g;->b:J

    .line 105
    invoke-static {v0, v2, v3}, Lcom/twitter/library/api/moments/maker/q;->a(Lcom/twitter/library/api/moments/maker/RecommendationType;J)Lcom/twitter/library/api/moments/maker/q;

    move-result-object v0

    .line 108
    :goto_1
    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/i;->f:Lcom/twitter/android/moments/ui/maker/c;

    if-eqz v2, :cond_0

    .line 109
    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/i;->f:Lcom/twitter/android/moments/ui/maker/c;

    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/maker/c;->c()V

    .line 111
    :cond_0
    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/i;->b:Lcom/twitter/util/object/d;

    invoke-interface {v2, v0}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/maker/c;

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/i;->f:Lcom/twitter/android/moments/ui/maker/c;

    .line 112
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/i;->f:Lcom/twitter/android/moments/ui/maker/c;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/c;->b()V

    .line 113
    if-eqz v1, :cond_3

    .line 114
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/i;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/c;

    iget-object v1, p1, Lcom/twitter/android/client/r;->d:Lcom/twitter/model/search/viewmodel/g;

    iget-object v1, v1, Lcom/twitter/model/search/viewmodel/g;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/android/client/r;->d:Lcom/twitter/model/search/viewmodel/g;

    iget-object v2, v2, Lcom/twitter/model/search/viewmodel/g;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/android/client/r;->d:Lcom/twitter/model/search/viewmodel/g;

    iget-object v3, v3, Lcom/twitter/model/search/viewmodel/g;->e:Ljava/lang/String;

    iget-object v4, p1, Lcom/twitter/android/client/r;->d:Lcom/twitter/model/search/viewmodel/g;

    iget-boolean v4, v4, Lcom/twitter/model/search/viewmodel/g;->f:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 122
    :goto_2
    return-void

    .line 100
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 105
    :cond_2
    iget-wide v2, p0, Lcom/twitter/android/moments/ui/maker/i;->e:J

    iget-object v0, p1, Lcom/twitter/android/client/r;->c:Ljava/lang/String;

    .line 107
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v3, v0}, Lcom/twitter/library/api/moments/maker/q;->a(JLjava/lang/String;)Lcom/twitter/library/api/moments/maker/q;

    move-result-object v0

    goto :goto_1

    .line 120
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/i;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/c;

    iget-object v1, p1, Lcom/twitter/android/client/r;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->a(Ljava/lang/String;)V

    goto :goto_2
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/i;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/i;->d()V

    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/i;->c:Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$b;

    new-instance v1, Lcom/twitter/app/common/base/h;

    invoke-direct {v1}, Lcom/twitter/app/common/base/h;-><init>()V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$b;->c(Lako;)V

    .line 126
    return-void
.end method


# virtual methods
.method public a(ILcom/twitter/android/client/r;)V
    .locals 1

    .prologue
    .line 92
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    .line 93
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/i;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/c;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->b()V

    .line 94
    invoke-direct {p0, p2}, Lcom/twitter/android/moments/ui/maker/i;->a(Lcom/twitter/android/client/r;)V

    .line 96
    :cond_0
    return-void
.end method

.method public bridge synthetic a(ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 29
    check-cast p2, Lcom/twitter/android/client/r;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/moments/ui/maker/i;->a(ILcom/twitter/android/client/r;)V

    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/i;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/c;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->aN_()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 140
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/i;->c:Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$b;

    invoke-virtual {v0, p0}, Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$b;->b(Lakv;)V

    .line 148
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/i;->f:Lcom/twitter/android/moments/ui/maker/c;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/i;->f:Lcom/twitter/android/moments/ui/maker/c;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/c;->c()V

    .line 151
    :cond_0
    return-void
.end method
