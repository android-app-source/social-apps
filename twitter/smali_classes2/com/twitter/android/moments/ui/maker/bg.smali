.class public Lcom/twitter/android/moments/ui/maker/bg;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/maker/bl;


# instance fields
.field private final a:Lcom/twitter/util/object/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/f",
            "<",
            "Lcom/twitter/android/moments/viewmodels/k;",
            "Lcom/twitter/android/moments/ui/fullscreen/cu;",
            "Labo;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/util/object/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/e",
            "<",
            "Lacd;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/util/object/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/e",
            "<",
            "Labi;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/twitter/util/object/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            "Labl;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/twitter/util/object/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/e",
            "<",
            "Labr;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/twitter/util/object/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/e",
            "<",
            "Laca;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/twitter/android/moments/viewmodels/l;


# direct methods
.method public constructor <init>(Lcom/twitter/util/object/f;Lcom/twitter/util/object/e;Lcom/twitter/util/object/e;Lcom/twitter/util/object/d;Lcom/twitter/util/object/e;Lcom/twitter/util/object/e;Lcom/twitter/android/moments/viewmodels/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/object/f",
            "<",
            "Lcom/twitter/android/moments/viewmodels/k;",
            "Lcom/twitter/android/moments/ui/fullscreen/cu;",
            "Labo;",
            ">;",
            "Lcom/twitter/util/object/e",
            "<",
            "Lacd;",
            ">;",
            "Lcom/twitter/util/object/e",
            "<",
            "Labi;",
            ">;",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            "Labl;",
            ">;",
            "Lcom/twitter/util/object/e",
            "<",
            "Labr;",
            ">;",
            "Lcom/twitter/util/object/e",
            "<",
            "Laca;",
            ">;",
            "Lcom/twitter/android/moments/viewmodels/l;",
            ")V"
        }
    .end annotation

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/bg;->a:Lcom/twitter/util/object/f;

    .line 141
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/bg;->b:Lcom/twitter/util/object/e;

    .line 142
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/bg;->c:Lcom/twitter/util/object/e;

    .line 143
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/bg;->d:Lcom/twitter/util/object/d;

    .line 144
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/bg;->e:Lcom/twitter/util/object/e;

    .line 145
    iput-object p6, p0, Lcom/twitter/android/moments/ui/maker/bg;->f:Lcom/twitter/util/object/e;

    .line 146
    iput-object p7, p0, Lcom/twitter/android/moments/ui/maker/bg;->g:Lcom/twitter/android/moments/viewmodels/l;

    .line 147
    return-void
.end method

.method private a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/android/moments/ui/fullscreen/cu;)Lacg;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            "Lcom/twitter/android/moments/ui/fullscreen/cu;",
            ")",
            "Lacg",
            "<+",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 169
    sget-object v0, Lcom/twitter/android/moments/ui/maker/bg$7;->a:[I

    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 190
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unrecognized moment page type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 191
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 175
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/bg;->g:Lcom/twitter/android/moments/viewmodels/l;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/viewmodels/l;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/android/moments/viewmodels/k;

    move-result-object v0

    .line 176
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/bg;->a:Lcom/twitter/util/object/f;

    invoke-interface {v1, v0, p2}, Lcom/twitter/util/object/f;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Labo;

    .line 178
    invoke-static {p1}, Lcom/twitter/model/moments/viewmodels/h;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Labo;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/core/Tweet;)V

    .line 187
    :goto_0
    return-object v0

    .line 183
    :pswitch_1
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/q;

    .line 184
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/bg;->b:Lcom/twitter/util/object/e;

    .line 185
    invoke-interface {v1}, Lcom/twitter/util/object/e;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lacd;

    .line 186
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/q;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lacd;->a(Lcom/twitter/model/moments/viewmodels/q;Lcom/twitter/model/core/Tweet;)V

    move-object v0, v1

    .line 187
    goto :goto_0

    .line 169
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Landroid/content/res/Resources;Landroid/view/LayoutInflater;Landroid/app/Activity;Lcom/twitter/android/moments/ui/fullscreen/as;Lbxo;)Lcom/twitter/android/moments/ui/maker/bg;
    .locals 12

    .prologue
    .line 62
    new-instance v7, Lcom/twitter/android/moments/ui/maker/bg$1;

    invoke-direct {v7, p1}, Lcom/twitter/android/moments/ui/maker/bg$1;-><init>(Landroid/view/LayoutInflater;)V

    .line 72
    new-instance v0, Lcom/twitter/android/moments/ui/maker/bg$2;

    move-object v1, p2

    move-object v2, p1

    move-object v3, p0

    move-object v4, p3

    move-object/from16 v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/moments/ui/maker/bg$2;-><init>(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/content/res/Resources;Lcom/twitter/android/moments/ui/fullscreen/as;Lbxo;)V

    .line 82
    new-instance v10, Lcom/twitter/android/moments/ui/maker/bg$3;

    invoke-direct {v10, p0, p1}, Lcom/twitter/android/moments/ui/maker/bg$3;-><init>(Landroid/content/res/Resources;Landroid/view/LayoutInflater;)V

    .line 91
    new-instance v11, Lcom/twitter/android/moments/ui/maker/bg$4;

    invoke-direct {v11, p1}, Lcom/twitter/android/moments/ui/maker/bg$4;-><init>(Landroid/view/LayoutInflater;)V

    .line 100
    new-instance v1, Lcom/twitter/android/moments/ui/maker/bg$5;

    move-object v2, p2

    move-object v3, p1

    move-object v4, p0

    move-object v5, p3

    move-object/from16 v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/twitter/android/moments/ui/maker/bg$5;-><init>(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/content/res/Resources;Lcom/twitter/android/moments/ui/fullscreen/as;Lbxo;)V

    .line 110
    new-instance v8, Lcom/twitter/android/moments/ui/maker/bg$6;

    invoke-direct {v8, p1}, Lcom/twitter/android/moments/ui/maker/bg$6;-><init>(Landroid/view/LayoutInflater;)V

    .line 119
    new-instance v2, Lcom/twitter/android/moments/ui/maker/bg;

    .line 126
    invoke-static {}, Lcom/twitter/android/moments/viewmodels/l;->a()Lcom/twitter/android/moments/viewmodels/l;

    move-result-object v9

    move-object v3, v7

    move-object v4, v0

    move-object v5, v10

    move-object v6, v11

    move-object v7, v1

    invoke-direct/range {v2 .. v9}, Lcom/twitter/android/moments/ui/maker/bg;-><init>(Lcom/twitter/util/object/f;Lcom/twitter/util/object/e;Lcom/twitter/util/object/e;Lcom/twitter/util/object/d;Lcom/twitter/util/object/e;Lcom/twitter/util/object/e;Lcom/twitter/android/moments/viewmodels/l;)V

    .line 119
    return-object v2
.end method

.method private b(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lacg;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ")",
            "Lacg",
            "<+",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 199
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/bg;->c:Lcom/twitter/util/object/e;

    invoke-interface {v0}, Lcom/twitter/util/object/e;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Labi;

    .line 201
    invoke-static {p1}, Lcom/twitter/model/moments/viewmodels/h;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Labi;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/core/Tweet;)V

    .line 227
    :goto_0
    return-object v0

    .line 203
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 204
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/bg;->d:Lcom/twitter/util/object/d;

    invoke-interface {v0, p1}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Labl;

    .line 205
    invoke-static {p1}, Lcom/twitter/model/moments/viewmodels/h;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Labl;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/core/Tweet;)V

    goto :goto_0

    .line 210
    :cond_1
    instance-of v0, p1, Lcom/twitter/model/moments/viewmodels/n;

    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 212
    sget-object v0, Lcom/twitter/android/moments/ui/maker/bg$7;->a:[I

    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 224
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/bg;->f:Lcom/twitter/util/object/e;

    .line 225
    invoke-interface {v0}, Lcom/twitter/util/object/e;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laca;

    .line 226
    invoke-static {p1}, Lcom/twitter/model/moments/viewmodels/h;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/model/core/Tweet;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Laca;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/core/Tweet;)V

    goto :goto_0

    .line 216
    :pswitch_0
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/n;

    .line 217
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/bg;->e:Lcom/twitter/util/object/e;

    .line 218
    invoke-interface {v1}, Lcom/twitter/util/object/e;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Labr;

    .line 219
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/n;->u()Lcom/twitter/model/core/Tweet;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Labr;->a(Lcom/twitter/model/moments/viewmodels/n;Lcom/twitter/model/core/Tweet;)V

    move-object v0, v1

    .line 220
    goto :goto_0

    .line 212
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    check-cast p1, Lcom/twitter/model/moments/viewmodels/MomentPage;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/bg;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ")",
            "Ljava/util/List",
            "<",
            "Lacg",
            "<+",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 155
    .line 156
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/maker/bg;->b(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lacg;

    move-result-object v1

    .line 157
    instance-of v0, v1, Lcom/twitter/android/moments/ui/fullscreen/cu;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lcom/twitter/android/moments/ui/fullscreen/cu;

    .line 162
    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/twitter/android/moments/ui/maker/bg;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/android/moments/ui/fullscreen/cu;)Lacg;

    move-result-object v0

    .line 163
    const/4 v2, 0x1

    new-array v2, v2, [Lacg;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 157
    :cond_0
    sget-object v0, Lcom/twitter/android/moments/ui/fullscreen/cu;->a:Lcom/twitter/android/moments/ui/fullscreen/cu;

    goto :goto_0
.end method
