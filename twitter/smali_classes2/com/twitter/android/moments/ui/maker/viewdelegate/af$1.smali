.class Lcom/twitter/android/moments/ui/maker/viewdelegate/af$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a(Lcom/twitter/android/moments/ui/maker/viewdelegate/y;)Lrx/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/ui/maker/viewdelegate/y;

.field final synthetic b:Lcom/twitter/android/moments/ui/maker/viewdelegate/af;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/af;Lcom/twitter/android/moments/ui/maker/viewdelegate/y;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af$1;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/af;

    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af$1;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af$1;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/af;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af$1;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/y;

    invoke-interface {v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/y;->b()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a(Lcom/twitter/android/moments/ui/maker/viewdelegate/af;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af$1;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/af;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a(Lcom/twitter/android/moments/ui/maker/viewdelegate/af;)Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;->b()Lcom/twitter/android/moments/ui/maker/viewdelegate/ak;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ak;->a(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 57
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af$1;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/af;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af$1;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/y;

    invoke-static {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a(Lcom/twitter/android/moments/ui/maker/viewdelegate/af;Lcom/twitter/android/moments/ui/maker/viewdelegate/y;)V

    .line 58
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af$1;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/af;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a(Lcom/twitter/android/moments/ui/maker/viewdelegate/af;)Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;->d()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 59
    const/4 v0, 0x0

    .line 65
    :goto_0
    return v0

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af$1;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/af;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a(Lcom/twitter/android/moments/ui/maker/viewdelegate/af;)Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;->b()Lcom/twitter/android/moments/ui/maker/viewdelegate/ak;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ak;->a(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 62
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af$1;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/af;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->b(Lcom/twitter/android/moments/ui/maker/viewdelegate/af;)Lrx/subjects/PublishSubject;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject;->a(Ljava/lang/Object;)V

    .line 63
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af$1;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/af;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->b(Lcom/twitter/android/moments/ui/maker/viewdelegate/af;)Lrx/subjects/PublishSubject;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject;->a(Ljava/lang/Object;)V

    .line 64
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af$1;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/af;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->b(Lcom/twitter/android/moments/ui/maker/viewdelegate/af;)Lrx/subjects/PublishSubject;

    move-result-object v0

    invoke-virtual {v0}, Lrx/subjects/PublishSubject;->by_()V

    .line 65
    const/4 v0, 0x1

    goto :goto_0
.end method
