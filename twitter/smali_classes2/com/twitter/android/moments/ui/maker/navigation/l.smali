.class public Lcom/twitter/android/moments/ui/maker/navigation/l;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/object/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/object/f",
        "<",
        "Lcom/twitter/android/moments/ui/maker/navigation/ah;",
        "Lcom/twitter/android/moments/ui/maker/navigation/ag;",
        "Lcom/twitter/android/moments/ui/maker/navigation/k;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/app/common/base/BaseFragmentActivity;

.field private final b:Landroid/view/ViewGroup;

.field private final c:Lwy;

.field private final d:Lakt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lakt",
            "<",
            "Lcom/twitter/android/moments/ui/maker/y$a;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Landroid/net/Uri;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:J


# direct methods
.method public constructor <init>(Lcom/twitter/app/common/base/BaseFragmentActivity;Landroid/view/ViewGroup;Lwy;Lakt;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/app/common/base/BaseFragmentActivity;",
            "Landroid/view/ViewGroup;",
            "Lwy;",
            "Lakt",
            "<",
            "Lcom/twitter/android/moments/ui/maker/y$a;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Landroid/net/Uri;",
            ">;>;J)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/navigation/l;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    .line 29
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/navigation/l;->b:Landroid/view/ViewGroup;

    .line 30
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/navigation/l;->c:Lwy;

    .line 31
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/navigation/l;->d:Lakt;

    .line 32
    iput-wide p5, p0, Lcom/twitter/android/moments/ui/maker/navigation/l;->e:J

    .line 33
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/ui/maker/navigation/ag;)Lcom/twitter/android/moments/ui/maker/navigation/k;
    .locals 12

    .prologue
    .line 38
    new-instance v8, Lcom/twitter/android/moments/ui/maker/navigation/k;

    iget-object v9, p0, Lcom/twitter/android/moments/ui/maker/navigation/l;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    iget-object v10, p0, Lcom/twitter/android/moments/ui/maker/navigation/l;->b:Landroid/view/ViewGroup;

    iget-object v11, p0, Lcom/twitter/android/moments/ui/maker/navigation/l;->c:Lwy;

    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/l;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/navigation/l;->d:Lakt;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/navigation/l;->c:Lwy;

    iget-wide v4, p0, Lcom/twitter/android/moments/ui/maker/navigation/l;->e:J

    move-object v3, p1

    .line 40
    invoke-static/range {v0 .. v5}, Lcom/twitter/android/moments/ui/maker/q;->a(Landroid/app/Activity;Lakt;Lwy;Lcom/twitter/android/moments/ui/maker/navigation/ah;J)Lcom/twitter/android/moments/ui/maker/q;

    move-result-object v5

    iget-wide v6, p0, Lcom/twitter/android/moments/ui/maker/navigation/l;->e:J

    move-object v1, v9

    move-object v2, v10

    move-object v3, p1

    move-object v4, v11

    .line 39
    invoke-static/range {v1 .. v7}, Lacw;->a(Lcom/twitter/app/common/base/BaseFragmentActivity;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lwy;Lcom/twitter/android/moments/ui/maker/q;J)Lacw;

    move-result-object v0

    invoke-direct {v8, v0}, Lcom/twitter/android/moments/ui/maker/navigation/k;-><init>(Lacw;)V

    .line 38
    return-object v8
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    check-cast p1, Lcom/twitter/android/moments/ui/maker/navigation/ah;

    check-cast p2, Lcom/twitter/android/moments/ui/maker/navigation/ag;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/moments/ui/maker/navigation/l;->a(Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/ui/maker/navigation/ag;)Lcom/twitter/android/moments/ui/maker/navigation/k;

    move-result-object v0

    return-object v0
.end method
