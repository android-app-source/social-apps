.class public Lcom/twitter/android/moments/ui/maker/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lajo;
.implements Lanj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lajo",
        "<",
        "Lcom/twitter/model/moments/viewmodels/g;",
        ">;",
        "Lanj;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/viewdelegate/a;

.field private final b:Lcom/twitter/android/moments/ui/maker/be;

.field private final c:Lcom/twitter/android/moments/data/af;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/moments/data/af",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lrx/j;

.field private final e:Lacz;

.field private final f:Lzt;

.field private g:Lcom/twitter/model/moments/viewmodels/g;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/a;Lcom/twitter/android/moments/ui/maker/be;Lcom/twitter/android/moments/data/af;Lacz;Lzt;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/a;",
            "Lcom/twitter/android/moments/ui/maker/be;",
            "Lcom/twitter/android/moments/data/af",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;",
            "Lacz;",
            "Lzt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/d;->b:Lcom/twitter/android/moments/ui/maker/be;

    .line 41
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/d;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/a;

    .line 42
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/d;->c:Lcom/twitter/android/moments/data/af;

    .line 43
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/d;->e:Lacz;

    .line 44
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/d;->f:Lzt;

    .line 45
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/d;->c:Lcom/twitter/android/moments/data/af;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/af;->a()Lrx/c;

    move-result-object v0

    .line 46
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/d;->d()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/d$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/d$1;-><init>(Lcom/twitter/android/moments/ui/maker/d;)V

    .line 47
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/d;->d:Lrx/j;

    .line 53
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/d;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/a;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/d$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/d$2;-><init>(Lcom/twitter/android/moments/ui/maker/d;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/a;->a(Landroid/view/View$OnClickListener;)V

    .line 59
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/d;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/d;->c()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/d;Z)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/maker/d;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/d;->g:Lcom/twitter/model/moments/viewmodels/g;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/d;->c:Lcom/twitter/android/moments/data/af;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/d;->g:Lcom/twitter/model/moments/viewmodels/g;

    .line 99
    invoke-static {v1}, Lcom/twitter/android/moments/viewmodels/z;->a(Lcom/twitter/model/moments/viewmodels/g;)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 98
    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/data/af;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    .line 100
    if-eqz p1, :cond_0

    .line 101
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/d;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/a;

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/a;->b(Z)V

    .line 105
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/d;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/a;

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/a;->a(Z)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/maker/d;)Lcom/twitter/model/moments/viewmodels/g;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/d;->g:Lcom/twitter/model/moments/viewmodels/g;

    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/d;->g:Lcom/twitter/model/moments/viewmodels/g;

    if-nez v0, :cond_0

    .line 94
    :goto_0
    return-void

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/d;->g:Lcom/twitter/model/moments/viewmodels/g;

    invoke-static {v0}, Lcom/twitter/android/moments/viewmodels/z;->a(Lcom/twitter/model/moments/viewmodels/g;)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 86
    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/d;->c:Lcom/twitter/android/moments/data/af;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/moments/data/af;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 87
    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/d;->c:Lcom/twitter/android/moments/data/af;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/moments/data/af;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/d;->f:Lzt;

    invoke-virtual {v2, v0, v1}, Lzt;->b(J)V

    goto :goto_0

    .line 90
    :cond_1
    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/d;->c:Lcom/twitter/android/moments/data/af;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/moments/ui/maker/d;->g:Lcom/twitter/model/moments/viewmodels/g;

    invoke-virtual {v2, v3, v4}, Lcom/twitter/android/moments/data/af;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/d;->f:Lzt;

    invoke-virtual {v2, v0, v1}, Lzt;->a(J)V

    goto :goto_0
.end method

.method private d()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<-",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    new-instance v0, Lcom/twitter/android/moments/ui/maker/d$3;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/maker/d$3;-><init>(Lcom/twitter/android/moments/ui/maker/d;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/viewmodels/g;)V
    .locals 1

    .prologue
    .line 69
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/d;->g:Lcom/twitter/model/moments/viewmodels/g;

    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/maker/d;->a(Z)V

    .line 71
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/d;->b:Lcom/twitter/android/moments/ui/maker/be;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/maker/be;->a(Lcom/twitter/model/moments/viewmodels/g;)V

    .line 72
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/d;->e:Lacz;

    invoke-virtual {v0, p1}, Lacz;->a(Lcom/twitter/model/moments/viewmodels/g;)V

    .line 73
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Lcom/twitter/model/moments/viewmodels/g;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/d;->a(Lcom/twitter/model/moments/viewmodels/g;)V

    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/d;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/a;->aN_()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public am_()V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/d;->d:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 110
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/d;->g:Lcom/twitter/model/moments/viewmodels/g;

    .line 78
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/d;->b:Lcom/twitter/android/moments/ui/maker/be;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/be;->c()V

    .line 79
    return-void
.end method
