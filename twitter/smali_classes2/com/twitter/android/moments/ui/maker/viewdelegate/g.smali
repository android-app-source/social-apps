.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/g;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/maker/viewdelegate/j;


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/viewdelegate/j;

.field private final b:Lcom/twitter/android/moments/ui/maker/viewdelegate/f;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/j;Lcom/twitter/android/moments/ui/maker/viewdelegate/f;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/g;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/j;

    .line 33
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/g;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/f;

    .line 34
    return-void
.end method

.method public static a(Lcom/twitter/android/moments/ui/maker/viewdelegate/h;Lcom/twitter/android/moments/ui/maker/viewdelegate/f;)Lcom/twitter/android/moments/ui/maker/viewdelegate/g;
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/g;

    .line 26
    invoke-static {p0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/q;->a(Lcom/twitter/android/moments/ui/maker/viewdelegate/z;)Lcom/twitter/android/moments/ui/maker/viewdelegate/q;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/g;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/j;Lcom/twitter/android/moments/ui/maker/viewdelegate/f;)V

    .line 25
    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/g;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/j;

    invoke-interface {v0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/j;->a(I)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public a(ILandroid/graphics/Rect;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/graphics/Rect;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/g;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/j;

    invoke-interface {v0, p1, p2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/j;->a(ILandroid/graphics/Rect;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/g;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/j;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/j;->a()V

    .line 59
    return-void
.end method

.method public b(ILandroid/graphics/Rect;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/graphics/Rect;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/g;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/j;

    invoke-interface {v0, p1, p2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/j;->b(ILandroid/graphics/Rect;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/g;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/f;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->f()V

    .line 63
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/g;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/f;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->k()V

    .line 64
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/g;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/f;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->b(I)V

    .line 73
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/g;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/f;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->g()V

    .line 68
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/g;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/f;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->j()V

    .line 69
    return-void
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/g;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/f;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->a(I)V

    .line 77
    return-void
.end method
