.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;

.field private final c:Landroid/view/View;

.field private final d:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;Landroid/view/View;Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/a;->a:Landroid/view/View;

    .line 39
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/a;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;

    .line 40
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/a;->c:Landroid/view/View;

    .line 41
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/a;->d:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    .line 42
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/a;
    .locals 5

    .prologue
    .line 23
    const v0, 0x7f0401c6

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 24
    const v0, 0x7f1304d2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 26
    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;->a(Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;

    move-result-object v2

    .line 27
    const v0, 0x7f1304d3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 28
    const v0, 0x7f130010

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 30
    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->b(Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    move-result-object v0

    .line 31
    new-instance v4, Lcom/twitter/android/moments/ui/maker/viewdelegate/a;

    invoke-direct {v4, v1, v2, v3, v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/a;-><init>(Landroid/view/View;Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;Landroid/view/View;Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;)V

    return-object v4
.end method


# virtual methods
.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/a;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 65
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/a;->c:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 66
    return-void

    .line 65
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/a;->a:Landroid/view/View;

    return-object v0
.end method

.method public b()Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/a;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;

    return-object v0
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 69
    if-eqz p1, :cond_0

    .line 70
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/a;->c:Landroid/view/View;

    invoke-static {v0}, Lcom/twitter/util/e;->c(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    .line 74
    :goto_0
    return-void

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/a;->c:Landroid/view/View;

    invoke-static {v0}, Lcom/twitter/util/e;->a(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method public c()Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/a;->d:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    return-object v0
.end method
