.class public Lcom/twitter/android/moments/ui/maker/n;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/maker/n$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/viewdelegate/h;

.field private final b:Lwy;

.field private final c:Lajt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajt",
            "<",
            "Lada;",
            "Lajq",
            "<",
            "Lada;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/twitter/android/moments/ui/maker/l;

.field private final e:Lcom/twitter/android/moments/ui/maker/navigation/ah;

.field private final f:Lcom/twitter/android/moments/ui/maker/viewdelegate/f;

.field private final g:Lcom/twitter/android/moments/ui/maker/a;

.field private final h:Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;

.field private final i:Lcom/twitter/android/moments/ui/maker/au;

.field private final j:Lcim;

.field private final k:Lcom/twitter/android/moments/ui/maker/z;

.field private final l:Lcom/twitter/android/moments/ui/maker/x;

.field private final m:Lzv;

.field private n:Lcom/twitter/model/moments/r;

.field private o:Lrx/j;

.field private p:Lrx/j;

.field private q:Lrx/j;

.field private r:Z


# direct methods
.method constructor <init>(Lcim;Lcom/twitter/android/moments/ui/maker/viewdelegate/h;Lwy;Lajt;Lcom/twitter/android/moments/ui/maker/l;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/ui/maker/viewdelegate/f;Lcom/twitter/android/moments/ui/maker/a;Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;Lcom/twitter/android/moments/ui/maker/au;Lcom/twitter/android/moments/ui/maker/z;Lcom/twitter/android/moments/ui/maker/x;Lzv;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcim;",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/h;",
            "Lwy;",
            "Lajt",
            "<",
            "Lada;",
            "Lajq",
            "<",
            "Lada;",
            ">;>;",
            "Lcom/twitter/android/moments/ui/maker/l;",
            "Lcom/twitter/android/moments/ui/maker/navigation/ah;",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/f;",
            "Lcom/twitter/android/moments/ui/maker/a;",
            "Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;",
            "Lcom/twitter/android/moments/ui/maker/au;",
            "Lcom/twitter/android/moments/ui/maker/z;",
            "Lcom/twitter/android/moments/ui/maker/x;",
            "Lzv;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/n;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/h;

    .line 111
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/n;->b:Lwy;

    .line 112
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/n;->d:Lcom/twitter/android/moments/ui/maker/l;

    .line 113
    iput-object p6, p0, Lcom/twitter/android/moments/ui/maker/n;->e:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    .line 114
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/n;->c:Lajt;

    .line 115
    iput-object p9, p0, Lcom/twitter/android/moments/ui/maker/n;->h:Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;

    .line 116
    iput-object p10, p0, Lcom/twitter/android/moments/ui/maker/n;->i:Lcom/twitter/android/moments/ui/maker/au;

    .line 117
    iput-object p11, p0, Lcom/twitter/android/moments/ui/maker/n;->k:Lcom/twitter/android/moments/ui/maker/z;

    .line 118
    iput-object p13, p0, Lcom/twitter/android/moments/ui/maker/n;->m:Lzv;

    .line 119
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->c:Lajt;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lajt;->a(Z)V

    .line 120
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/h;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/n;->c:Lajt;

    invoke-virtual {v1}, Lajt;->b()Lajs;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->a(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 121
    iput-object p7, p0, Lcom/twitter/android/moments/ui/maker/n;->f:Lcom/twitter/android/moments/ui/maker/viewdelegate/f;

    .line 122
    iput-object p8, p0, Lcom/twitter/android/moments/ui/maker/n;->g:Lcom/twitter/android/moments/ui/maker/a;

    .line 123
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/n;->f()V

    .line 124
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->f:Lcom/twitter/android/moments/ui/maker/viewdelegate/f;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->a()V

    .line 125
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/n;->j:Lcim;

    .line 126
    iput-object p12, p0, Lcom/twitter/android/moments/ui/maker/n;->l:Lcom/twitter/android/moments/ui/maker/x;

    .line 127
    return-void
.end method

.method public static a(Lcim;Lcom/twitter/app/common/base/BaseFragmentActivity;Lcom/twitter/android/moments/ui/maker/viewdelegate/h;Lcom/twitter/android/moments/ui/maker/viewdelegate/f;Lwy;Lcom/twitter/android/moments/ui/maker/ap$a;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/ui/maker/a;Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;J)Lcom/twitter/android/moments/ui/maker/n;
    .locals 23

    .prologue
    .line 79
    new-instance v6, Lcom/twitter/android/moments/ui/maker/n$a;

    .line 80
    invoke-static/range {p1 .. p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    move-object/from16 v7, p1

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    move-object/from16 v11, p4

    move-wide/from16 v12, p9

    invoke-direct/range {v6 .. v13}, Lcom/twitter/android/moments/ui/maker/n$a;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/twitter/android/moments/ui/maker/ap$a;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lwy;J)V

    .line 83
    invoke-static {}, Lcom/twitter/android/moments/ui/maker/l;->a()Lcom/twitter/android/moments/ui/maker/l;

    move-result-object v11

    .line 84
    new-instance v10, Lajt;

    new-instance v7, Lajs;

    invoke-direct {v7, v11, v6}, Lajs;-><init>(Lajv;Lajr;)V

    invoke-direct {v10, v7}, Lajt;-><init>(Lajs;)V

    .line 88
    invoke-static/range {p1 .. p1}, Lcom/twitter/android/moments/ui/maker/x;->a(Landroid/app/Activity;)Lcom/twitter/android/moments/ui/maker/x;

    move-result-object v18

    .line 89
    invoke-static/range {p9 .. p10}, Lzv;->a(J)Lzv;

    move-result-object v19

    .line 90
    new-instance v6, Lcom/twitter/android/moments/ui/maker/n;

    .line 93
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    move-object/from16 v2, p7

    move-wide/from16 v3, p9

    invoke-static {v0, v1, v2, v3, v4}, Lcom/twitter/android/moments/ui/maker/au;->a(Landroid/app/Activity;Lwy;Lcom/twitter/android/moments/ui/maker/a;J)Lcom/twitter/android/moments/ui/maker/au;

    move-result-object v16

    .line 94
    invoke-static/range {p1 .. p1}, Lcom/twitter/android/moments/ui/maker/z;->a(Lcom/twitter/app/common/base/BaseFragmentActivity;)Lcom/twitter/android/moments/ui/maker/z;

    move-result-object v17

    move-object/from16 v7, p0

    move-object/from16 v8, p2

    move-object/from16 v9, p4

    move-object/from16 v12, p6

    move-object/from16 v13, p3

    move-object/from16 v14, p7

    move-object/from16 v15, p8

    move-wide/from16 v20, p9

    invoke-direct/range {v6 .. v21}, Lcom/twitter/android/moments/ui/maker/n;-><init>(Lcim;Lcom/twitter/android/moments/ui/maker/viewdelegate/h;Lwy;Lajt;Lcom/twitter/android/moments/ui/maker/l;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/ui/maker/viewdelegate/f;Lcom/twitter/android/moments/ui/maker/a;Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;Lcom/twitter/android/moments/ui/maker/au;Lcom/twitter/android/moments/ui/maker/z;Lcom/twitter/android/moments/ui/maker/x;Lzv;J)V

    .line 90
    return-object v6
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/n;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/n;->i()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/n;Lcom/twitter/model/moments/viewmodels/a;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/maker/n;->a(Lcom/twitter/model/moments/viewmodels/a;)V

    return-void
.end method

.method private a(Lcom/twitter/model/moments/MomentVisibilityMode;)V
    .locals 2

    .prologue
    .line 262
    sget-object v0, Lcom/twitter/model/moments/MomentVisibilityMode;->b:Lcom/twitter/model/moments/MomentVisibilityMode;

    if-ne v0, p1, :cond_0

    .line 263
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->f:Lcom/twitter/android/moments/ui/maker/viewdelegate/f;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->d()V

    .line 264
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->f:Lcom/twitter/android/moments/ui/maker/viewdelegate/f;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->h()V

    .line 265
    new-instance v0, Lcom/twitter/android/moments/ui/maker/n$8;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/maker/n$8;-><init>(Lcom/twitter/android/moments/ui/maker/n;)V

    .line 282
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/n;->f:Lcom/twitter/android/moments/ui/maker/viewdelegate/f;

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->d(Landroid/view/View$OnClickListener;)V

    .line 283
    return-void

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->f:Lcom/twitter/android/moments/ui/maker/viewdelegate/f;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->e()V

    .line 273
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->f:Lcom/twitter/android/moments/ui/maker/viewdelegate/f;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->i()V

    .line 274
    new-instance v0, Lcom/twitter/android/moments/ui/maker/n$9;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/maker/n$9;-><init>(Lcom/twitter/android/moments/ui/maker/n;)V

    goto :goto_0
.end method

.method private a(Lcom/twitter/model/moments/viewmodels/a;)V
    .locals 2

    .prologue
    .line 229
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/a;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 230
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->f:Lcom/twitter/android/moments/ui/maker/viewdelegate/f;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->c()V

    .line 236
    :goto_0
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/a;->a()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/moments/Moment;->r:Lcom/twitter/model/moments/f;

    sget-object v1, Lcom/twitter/model/moments/f;->b:Lcom/twitter/model/moments/f;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/f;

    .line 237
    iget-object v0, v0, Lcom/twitter/model/moments/f;->c:Lcom/twitter/model/moments/MomentVisibilityMode;

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/maker/n;->a(Lcom/twitter/model/moments/MomentVisibilityMode;)V

    .line 238
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->d:Lcom/twitter/android/moments/ui/maker/l;

    invoke-static {p1}, Lada;->a(Lcom/twitter/model/moments/viewmodels/a;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/l;->a(Ljava/util/List;)V

    .line 239
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->c:Lajt;

    invoke-virtual {v0}, Lajt;->a()V

    .line 240
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/maker/n;->r:Z

    if-nez v0, :cond_0

    .line 241
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/maker/n;->r:Z

    .line 242
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/n;->k()V

    .line 244
    :cond_0
    return-void

    .line 232
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->f:Lcom/twitter/android/moments/ui/maker/viewdelegate/f;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->b()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/maker/n;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/n;->g()V

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/maker/n;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/n;->n()V

    return-void
.end method

.method static synthetic d(Lcom/twitter/android/moments/ui/maker/n;)Lcom/twitter/android/moments/ui/maker/navigation/ah;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->e:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/moments/ui/maker/n;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/n;->h()V

    return-void
.end method

.method static synthetic f(Lcom/twitter/android/moments/ui/maker/n;)Lcom/twitter/android/moments/ui/maker/x;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->l:Lcom/twitter/android/moments/ui/maker/x;

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->f:Lcom/twitter/android/moments/ui/maker/viewdelegate/f;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/n$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/n$1;-><init>(Lcom/twitter/android/moments/ui/maker/n;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->a(Landroid/view/View$OnClickListener;)V

    .line 149
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->f:Lcom/twitter/android/moments/ui/maker/viewdelegate/f;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/n$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/n$3;-><init>(Lcom/twitter/android/moments/ui/maker/n;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->b(Landroid/view/View$OnClickListener;)V

    .line 155
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->f:Lcom/twitter/android/moments/ui/maker/viewdelegate/f;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/n$4;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/n$4;-><init>(Lcom/twitter/android/moments/ui/maker/n;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->c(Landroid/view/View$OnClickListener;)V

    .line 162
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->f:Lcom/twitter/android/moments/ui/maker/viewdelegate/f;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/n$5;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/n$5;-><init>(Lcom/twitter/android/moments/ui/maker/n;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->e(Landroid/view/View$OnClickListener;)V

    .line 168
    return-void
.end method

.method static synthetic g(Lcom/twitter/android/moments/ui/maker/n;)Lcom/twitter/android/moments/ui/maker/au;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->i:Lcom/twitter/android/moments/ui/maker/au;

    return-object v0
.end method

.method private g()V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->g:Lcom/twitter/android/moments/ui/maker/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/a;->a()V

    .line 172
    return-void
.end method

.method static synthetic h(Lcom/twitter/android/moments/ui/maker/n;)Lzv;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->m:Lzv;

    return-object v0
.end method

.method private h()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->h:Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;->a()V

    .line 176
    return-void
.end method

.method static synthetic i(Lcom/twitter/android/moments/ui/maker/n;)Lcom/twitter/android/moments/ui/maker/z;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->k:Lcom/twitter/android/moments/ui/maker/z;

    return-object v0
.end method

.method private i()V
    .locals 4

    .prologue
    .line 179
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->e:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    sget-object v1, Lcom/twitter/android/moments/ui/maker/navigation/NavigationKey;->b:Lcom/twitter/android/moments/ui/maker/navigation/NavigationKey;

    new-instance v2, Lcom/twitter/android/moments/ui/maker/navigation/x;

    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/n;->e()Lcom/twitter/model/moments/r;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/twitter/android/moments/ui/maker/navigation/x;-><init>(Lcom/twitter/model/moments/r;)V

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/moments/ui/maker/navigation/ah;->a(Lcom/twitter/android/moments/ui/maker/navigation/NavigationKey;Lcom/twitter/android/moments/ui/maker/navigation/ag;)V

    .line 180
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->b:Lwy;

    invoke-virtual {v0}, Lwy;->a()Lrx/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lrx/c;->d(I)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/n$7;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/n$7;-><init>(Lcom/twitter/android/moments/ui/maker/n;)V

    .line 248
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->p:Lrx/j;

    .line 258
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->d:Lcom/twitter/android/moments/ui/maker/l;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/l;->b()I

    move-result v0

    if-nez v0, :cond_0

    .line 301
    :goto_0
    return-void

    .line 289
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->d:Lcom/twitter/android/moments/ui/maker/l;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/n;->j:Lcim;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/l;->a(Lcim;)I

    move-result v1

    .line 290
    if-ltz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->d:Lcom/twitter/android/moments/ui/maker/l;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/l;->b()I

    move-result v0

    if-lt v1, v0, :cond_2

    .line 292
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->d:Lcom/twitter/android/moments/ui/maker/l;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/l;->b()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    .line 293
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->d:Lcom/twitter/android/moments/ui/maker/l;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/l;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lada;

    .line 294
    invoke-static {v0}, Ladb;->a(Lada;)Lcom/twitter/model/moments/r;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->n:Lcom/twitter/model/moments/r;

    .line 295
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/h;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->a(I)V

    goto :goto_0

    .line 297
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->d:Lcom/twitter/android/moments/ui/maker/l;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/l;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lada;

    .line 298
    invoke-static {v0}, Ladb;->a(Lada;)Lcom/twitter/model/moments/r;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->n:Lcom/twitter/model/moments/r;

    .line 299
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/h;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->a(I)V

    goto :goto_0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 305
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->b:Lwy;

    invoke-virtual {v0}, Lwy;->b()Lrx/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lrx/c;->d(I)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/n$10;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/n$10;-><init>(Lcom/twitter/android/moments/ui/maker/n;)V

    .line 306
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->q:Lrx/j;

    .line 312
    return-void
.end method

.method private m()I
    .locals 2

    .prologue
    .line 315
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->d:Lcom/twitter/android/moments/ui/maker/l;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/n;->n:Lcom/twitter/model/moments/r;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/l;->a(Lcom/twitter/model/moments/r;)I

    move-result v0

    .line 316
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 320
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->b:Lwy;

    invoke-virtual {v0}, Lwy;->a()Lrx/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lrx/c;->d(I)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/n$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/n$2;-><init>(Lcom/twitter/android/moments/ui/maker/n;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 329
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/r;)V
    .locals 2

    .prologue
    .line 213
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/n;->n:Lcom/twitter/model/moments/r;

    .line 214
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/h;

    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/n;->m()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->a(I)V

    .line 215
    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/h;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->aN_()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 130
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->n:Lcom/twitter/model/moments/r;

    if-nez v0, :cond_1

    .line 139
    :cond_0
    :goto_0
    return v1

    .line 133
    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/n;->m()I

    move-result v0

    .line 134
    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/n;->d:Lcom/twitter/android/moments/ui/maker/l;

    invoke-virtual {v2, v0}, Lcom/twitter/android/moments/ui/maker/l;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lada;

    .line 135
    invoke-virtual {v0}, Lada;->b()Lcom/twitter/model/moments/viewmodels/g;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 138
    invoke-virtual {v0}, Lada;->b()Lcom/twitter/model/moments/viewmodels/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/g;->a()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 139
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->k()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public c()V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->o:Lrx/j;

    if-eqz v0, :cond_0

    .line 203
    :goto_0
    return-void

    .line 193
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->m:Lzv;

    invoke-virtual {v0}, Lzv;->a()V

    .line 194
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->b:Lwy;

    invoke-virtual {v0}, Lwy;->a()Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/n$6;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/n$6;-><init>(Lcom/twitter/android/moments/ui/maker/n;)V

    .line 195
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->o:Lrx/j;

    .line 201
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/n;->j()V

    .line 202
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/n;->l()V

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->o:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 207
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->p:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 208
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->q:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 209
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/h;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->d()V

    .line 210
    return-void
.end method

.method public e()Lcom/twitter/model/moments/r;
    .locals 2

    .prologue
    .line 219
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/h;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->c()I

    move-result v0

    .line 220
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 221
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/n;->d:Lcom/twitter/android/moments/ui/maker/l;

    .line 222
    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/maker/l;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lada;

    .line 223
    invoke-static {v0}, Ladb;->a(Lada;)Lcom/twitter/model/moments/r;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->n:Lcom/twitter/model/moments/r;

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n;->n:Lcom/twitter/model/moments/r;

    return-object v0
.end method
