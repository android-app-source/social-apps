.class public Lcom/twitter/android/moments/ui/maker/h;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/object/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/object/f",
        "<",
        "Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;",
        "Landroid/view/ViewGroup;",
        "Lcom/twitter/android/moments/ui/maker/e;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Landroid/view/ViewGroup;

.field private final c:Lcom/twitter/android/moments/ui/maker/ap$a;

.field private final d:Lcom/twitter/android/moments/data/af;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/moments/data/af",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lanh;

.field private final f:Lya;

.field private final g:Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$b;

.field private final h:Lxm;

.field private final i:J


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/maker/ap$a;Lcom/twitter/android/moments/data/af;Lanh;Lya;Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$b;Lxm;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/view/ViewGroup;",
            "Lcom/twitter/android/moments/ui/maker/ap$a;",
            "Lcom/twitter/android/moments/data/af",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;",
            "Lanh;",
            "Lya;",
            "Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$b;",
            "Lxm;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/h;->a:Landroid/app/Activity;

    .line 43
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/h;->b:Landroid/view/ViewGroup;

    .line 44
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/h;->c:Lcom/twitter/android/moments/ui/maker/ap$a;

    .line 45
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/h;->d:Lcom/twitter/android/moments/data/af;

    .line 46
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/h;->e:Lanh;

    .line 47
    iput-object p6, p0, Lcom/twitter/android/moments/ui/maker/h;->f:Lya;

    .line 48
    iput-object p7, p0, Lcom/twitter/android/moments/ui/maker/h;->g:Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$b;

    .line 49
    iput-object p8, p0, Lcom/twitter/android/moments/ui/maker/h;->h:Lxm;

    .line 50
    iput-wide p9, p0, Lcom/twitter/android/moments/ui/maker/h;->i:J

    .line 51
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/e;
    .locals 14

    .prologue
    .line 56
    iget-wide v0, p0, Lcom/twitter/android/moments/ui/maker/h;->i:J

    .line 57
    invoke-static {p1, v0, v1}, Lzt;->a(Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;J)Lzt;

    move-result-object v8

    .line 58
    sget-object v0, Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;->d:Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;

    if-ne p1, v0, :cond_0

    .line 59
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/h;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/h;->b:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/h;->c:Lcom/twitter/android/moments/ui/maker/ap$a;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/maker/h;->d:Lcom/twitter/android/moments/data/af;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/maker/h;->e:Lanh;

    iget-object v6, p0, Lcom/twitter/android/moments/ui/maker/h;->f:Lya;

    iget-object v7, p0, Lcom/twitter/android/moments/ui/maker/h;->g:Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$b;

    iget-object v9, p0, Lcom/twitter/android/moments/ui/maker/h;->h:Lxm;

    iget-wide v10, p0, Lcom/twitter/android/moments/ui/maker/h;->i:J

    move-object v0, p1

    invoke-static/range {v0 .. v11}, Lcom/twitter/android/moments/ui/maker/i;->a(Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/maker/ap$a;Lcom/twitter/android/moments/data/af;Lanh;Lya;Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$b;Lzt;Lxm;J)Lcom/twitter/android/moments/ui/maker/i;

    move-result-object v0

    .line 68
    :goto_0
    return-object v0

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/h;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/h;->b:Landroid/view/ViewGroup;

    .line 64
    invoke-static {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/b;

    move-result-object v5

    .line 65
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/h;->f:Lya;

    .line 66
    invoke-static {p1, v0}, Lxz;->a(Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;Lya;)Lrx/g;

    move-result-object v10

    .line 68
    iget-object v4, p0, Lcom/twitter/android/moments/ui/maker/h;->a:Landroid/app/Activity;

    iget-object v6, p0, Lcom/twitter/android/moments/ui/maker/h;->c:Lcom/twitter/android/moments/ui/maker/ap$a;

    iget-object v7, p0, Lcom/twitter/android/moments/ui/maker/h;->d:Lcom/twitter/android/moments/data/af;

    iget-object v9, p0, Lcom/twitter/android/moments/ui/maker/h;->e:Lanh;

    iget-object v11, p0, Lcom/twitter/android/moments/ui/maker/h;->h:Lxm;

    iget-wide v12, p0, Lcom/twitter/android/moments/ui/maker/h;->i:J

    move-object v3, p1

    invoke-static/range {v3 .. v13}, Lcom/twitter/android/moments/ui/maker/c;->a(Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;Landroid/content/Context;Lcom/twitter/android/moments/ui/maker/viewdelegate/b;Lcom/twitter/android/moments/ui/maker/ap$a;Lcom/twitter/android/moments/data/af;Lzt;Lanh;Lrx/g;Lxm;J)Lcom/twitter/android/moments/ui/maker/c;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    check-cast p1, Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;

    check-cast p2, Landroid/view/ViewGroup;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/moments/ui/maker/h;->a(Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/e;

    move-result-object v0

    return-object v0
.end method
