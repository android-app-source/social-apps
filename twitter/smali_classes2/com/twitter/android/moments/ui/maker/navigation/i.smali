.class public Lcom/twitter/android/moments/ui/maker/navigation/i;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/object/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/object/g",
        "<",
        "Lcom/twitter/model/moments/r;",
        "Lcom/twitter/model/moments/w;",
        "Lcom/twitter/android/moments/ui/maker/navigation/ah;",
        "Lcom/twitter/android/moments/ui/maker/navigation/g;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Landroid/view/ViewGroup;

.field private final c:Lwy;

.field private final d:J


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lwy;J)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/navigation/i;->a:Landroid/app/Activity;

    .line 25
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/navigation/i;->b:Landroid/view/ViewGroup;

    .line 26
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/navigation/i;->c:Lwy;

    .line 27
    iput-wide p4, p0, Lcom/twitter/android/moments/ui/maker/navigation/i;->d:J

    .line 28
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/r;Lcom/twitter/model/moments/w;Lcom/twitter/android/moments/ui/maker/navigation/ah;)Lcom/twitter/android/moments/ui/maker/navigation/g;
    .locals 10

    .prologue
    .line 34
    new-instance v0, Lcom/twitter/android/moments/ui/maker/navigation/g;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/navigation/i;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/navigation/i;->b:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/navigation/i;->c:Lwy;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/maker/navigation/i;->a:Landroid/app/Activity;

    .line 35
    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    iget-wide v8, p0, Lcom/twitter/android/moments/ui/maker/navigation/i;->d:J

    move-object v4, p3

    move-object v5, p1

    move-object v6, p2

    .line 34
    invoke-static/range {v1 .. v9}, Lact;->a(Landroid/content/Context;Landroid/view/ViewGroup;Lwy;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/model/moments/r;Lcom/twitter/model/moments/w;Landroid/content/res/Resources;J)Lact;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/moments/ui/maker/navigation/g;-><init>(Lact;)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    check-cast p1, Lcom/twitter/model/moments/r;

    check-cast p2, Lcom/twitter/model/moments/w;

    check-cast p3, Lcom/twitter/android/moments/ui/maker/navigation/ah;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/moments/ui/maker/navigation/i;->a(Lcom/twitter/model/moments/r;Lcom/twitter/model/moments/w;Lcom/twitter/android/moments/ui/maker/navigation/ah;)Lcom/twitter/android/moments/ui/maker/navigation/g;

    move-result-object v0

    return-object v0
.end method
