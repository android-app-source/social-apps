.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/o;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field private final b:Landroid/support/v7/widget/RecyclerView;

.field private final c:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/ViewGroup;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/GridLayoutManager;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/o;->a:Landroid/view/ViewGroup;

    .line 37
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/o;->b:Landroid/support/v7/widget/RecyclerView;

    .line 38
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/o;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 39
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/o;->c:Landroid/view/View;

    .line 40
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/o;
    .locals 6

    .prologue
    .line 26
    .line 27
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401d0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 28
    const v1, 0x7f1304e7

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    .line 29
    const v2, 0x7f130164

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 30
    new-instance v3, Lcom/twitter/android/moments/ui/maker/viewdelegate/o;

    new-instance v4, Landroid/support/v7/widget/GridLayoutManager;

    const/4 v5, 0x3

    invoke-direct {v4, p0, v5}, Landroid/support/v7/widget/GridLayoutManager;-><init>(Landroid/content/Context;I)V

    invoke-direct {v3, v0, v1, v4, v2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/o;-><init>(Landroid/view/ViewGroup;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/GridLayoutManager;Landroid/view/View;)V

    return-object v3
.end method


# virtual methods
.method public a(Lajt;)V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/o;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p1}, Lajt;->b()Lajs;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 50
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/o;->c:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/o;->a:Landroid/view/ViewGroup;

    return-object v0
.end method
