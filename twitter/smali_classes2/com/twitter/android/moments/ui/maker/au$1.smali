.class Lcom/twitter/android/moments/ui/maker/au$1;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/maker/au;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Lcom/twitter/model/moments/viewmodels/a;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/ui/maker/au;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/maker/au;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/au$1;->a:Lcom/twitter/android/moments/ui/maker/au;

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/viewmodels/a;)V
    .locals 3

    .prologue
    .line 72
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/a;->a()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/au$1;->a:Lcom/twitter/android/moments/ui/maker/au;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/maker/au;->a(Lcom/twitter/android/moments/ui/maker/au;)Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    .line 74
    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/au$1;->a:Lcom/twitter/android/moments/ui/maker/au;

    invoke-static {v2, p1}, Lcom/twitter/android/moments/ui/maker/au;->a(Lcom/twitter/android/moments/ui/maker/au;Lcom/twitter/model/moments/viewmodels/a;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 75
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/au$1;->a:Lcom/twitter/android/moments/ui/maker/au;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/au;->c(Lcom/twitter/android/moments/ui/maker/au;)Lcom/twitter/android/moments/ui/maker/av;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/au$1;->a:Lcom/twitter/android/moments/ui/maker/au;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/maker/au;->b(Lcom/twitter/android/moments/ui/maker/au;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/av;->b(Landroid/content/DialogInterface$OnClickListener;)V

    .line 88
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/au$1;->B_()V

    .line 89
    return-void

    .line 76
    :cond_0
    iget-object v2, v0, Lcom/twitter/model/moments/Moment;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v0, Lcom/twitter/model/moments/Moment;->l:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 77
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/au$1;->a:Lcom/twitter/android/moments/ui/maker/au;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/au;->c(Lcom/twitter/android/moments/ui/maker/au;)Lcom/twitter/android/moments/ui/maker/av;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/av;->a()V

    goto :goto_0

    .line 78
    :cond_2
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/a;->c()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v2

    if-nez v2, :cond_3

    .line 79
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/au$1;->a:Lcom/twitter/android/moments/ui/maker/au;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/au;->c(Lcom/twitter/android/moments/ui/maker/au;)Lcom/twitter/android/moments/ui/maker/av;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/av;->b()V

    goto :goto_0

    .line 80
    :cond_3
    if-eqz v1, :cond_4

    iget-boolean v1, v1, Lcom/twitter/model/core/TwitterUser;->l:Z

    if-eqz v1, :cond_4

    .line 81
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/au$1;->a:Lcom/twitter/android/moments/ui/maker/au;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/au;->c(Lcom/twitter/android/moments/ui/maker/au;)Lcom/twitter/android/moments/ui/maker/av;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/av;->c()V

    goto :goto_0

    .line 82
    :cond_4
    iget-object v1, v0, Lcom/twitter/model/moments/Moment;->r:Lcom/twitter/model/moments/f;

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/twitter/model/moments/Moment;->r:Lcom/twitter/model/moments/f;

    iget-boolean v1, v1, Lcom/twitter/model/moments/f;->e:Z

    if-eqz v1, :cond_5

    .line 83
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/au$1;->a:Lcom/twitter/android/moments/ui/maker/au;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/au;->c(Lcom/twitter/android/moments/ui/maker/au;)Lcom/twitter/android/moments/ui/maker/av;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/av;->d()V

    goto :goto_0

    .line 85
    :cond_5
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/au$1;->a:Lcom/twitter/android/moments/ui/maker/au;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/maker/au;->c(Lcom/twitter/android/moments/ui/maker/au;)Lcom/twitter/android/moments/ui/maker/av;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/au$1;->a:Lcom/twitter/android/moments/ui/maker/au;

    invoke-static {v2, v0}, Lcom/twitter/android/moments/ui/maker/au;->a(Lcom/twitter/android/moments/ui/maker/au;Lcom/twitter/model/moments/Moment;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/maker/av;->a(Landroid/content/DialogInterface$OnClickListener;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 69
    check-cast p1, Lcom/twitter/model/moments/viewmodels/a;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/au$1;->a(Lcom/twitter/model/moments/viewmodels/a;)V

    return-void
.end method
