.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/f;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/view/View;

.field private final c:Landroid/view/View;

.field private final d:Landroid/view/View;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/view/View;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Landroid/view/View;

.field private final k:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const v0, 0x7f0a06ff

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->h:Ljava/lang/String;

    .line 46
    const v0, 0x7f0a031c

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->i:Ljava/lang/String;

    .line 47
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->a:Landroid/view/View;

    .line 48
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->b:Landroid/view/View;

    .line 49
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->c:Landroid/view/View;

    .line 50
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->d:Landroid/view/View;

    .line 51
    iput-object p6, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->e:Landroid/widget/TextView;

    .line 52
    iput-object p7, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->f:Landroid/widget/TextView;

    .line 53
    iput-object p8, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->g:Landroid/view/View;

    .line 54
    iput-object p9, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->j:Landroid/view/View;

    .line 55
    iput-object p10, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->k:Landroid/view/View;

    .line 56
    return-void
.end method

.method public static a(Landroid/view/View;)Lcom/twitter/android/moments/ui/maker/viewdelegate/f;
    .locals 11

    .prologue
    .line 27
    const v0, 0x7f13047b

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 28
    const v0, 0x7f1304d8

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 29
    const v0, 0x7f1304d9

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 30
    const v0, 0x7f130114

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 31
    const v0, 0x7f1304d7

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 32
    const v0, 0x7f1304da

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 33
    const v0, 0x7f1304ac

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 34
    const v0, 0x7f1304d6

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    .line 35
    const v0, 0x7f13035b

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 36
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct/range {v0 .. v10}, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;-><init>(Landroid/content/res/Resources;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 60
    return-void
.end method

.method public a(I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 117
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 118
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 119
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->a:Landroid/view/View;

    .line 120
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 121
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 122
    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v2, p1

    .line 123
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 124
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 125
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 64
    return-void
.end method

.method public b(I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 128
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->a:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 129
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 130
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->a:Landroid/view/View;

    .line 131
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 132
    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->a:Landroid/view/View;

    .line 133
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v2, p1

    .line 134
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 135
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 136
    return-void
.end method

.method public b(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->c:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->b:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 68
    return-void
.end method

.method public c(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->d:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 72
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    return-void
.end method

.method public d(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 77
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    return-void
.end method

.method public e(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->g:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 82
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->f:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 86
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 90
    return-void
.end method

.method public i()V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->e:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 94
    return-void
.end method

.method public j()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 139
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 140
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 141
    return-void
.end method

.method public k()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 144
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 145
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 146
    return-void
.end method
