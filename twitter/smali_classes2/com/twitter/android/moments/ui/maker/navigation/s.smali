.class public Lcom/twitter/android/moments/ui/maker/navigation/s;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Lcom/twitter/android/moments/ui/maker/navigation/s;


# instance fields
.field private final b:I

.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 9
    new-instance v0, Lcom/twitter/android/moments/ui/maker/navigation/s;

    const/16 v1, 0x15e

    const/16 v2, 0xe1

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/moments/ui/maker/navigation/s;-><init>(II)V

    sput-object v0, Lcom/twitter/android/moments/ui/maker/navigation/s;->a:Lcom/twitter/android/moments/ui/maker/navigation/s;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput p1, p0, Lcom/twitter/android/moments/ui/maker/navigation/s;->b:I

    .line 19
    iput p2, p0, Lcom/twitter/android/moments/ui/maker/navigation/s;->c:I

    .line 20
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/ui/maker/navigation/aj;)Lcom/twitter/android/moments/ui/maker/navigation/ak;
    .locals 6

    .prologue
    .line 24
    iget-object v1, p1, Lcom/twitter/android/moments/ui/maker/navigation/aj;->a:Lcom/twitter/android/moments/ui/maker/navigation/ai;

    .line 25
    if-eqz v1, :cond_0

    new-instance v0, Lcom/twitter/android/moments/ui/maker/navigation/v;

    .line 26
    invoke-interface {v1}, Lcom/twitter/android/moments/ui/maker/navigation/ai;->a()Landroid/view/View;

    move-result-object v1

    iget v2, p0, Lcom/twitter/android/moments/ui/maker/navigation/s;->c:I

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/moments/ui/maker/navigation/v;-><init>(Landroid/view/View;II)V

    .line 28
    :goto_0
    new-instance v1, Lcom/twitter/android/moments/ui/maker/navigation/u;

    iget-object v2, p1, Lcom/twitter/android/moments/ui/maker/navigation/aj;->b:Lcom/twitter/android/moments/ui/maker/navigation/ai;

    invoke-interface {v2}, Lcom/twitter/android/moments/ui/maker/navigation/ai;->a()Landroid/view/View;

    move-result-object v2

    iget v3, p0, Lcom/twitter/android/moments/ui/maker/navigation/s;->c:I

    iget v4, p0, Lcom/twitter/android/moments/ui/maker/navigation/s;->b:I

    iget v5, p0, Lcom/twitter/android/moments/ui/maker/navigation/s;->c:I

    sub-int/2addr v4, v5

    invoke-direct {v1, v2, v3, v4}, Lcom/twitter/android/moments/ui/maker/navigation/u;-><init>(Landroid/view/View;II)V

    .line 30
    new-instance v2, Lcom/twitter/android/moments/ui/maker/navigation/j;

    invoke-direct {v2, v0, v1}, Lcom/twitter/android/moments/ui/maker/navigation/j;-><init>(Lcom/twitter/android/moments/ui/maker/navigation/ak;Lcom/twitter/android/moments/ui/maker/navigation/ak;)V

    return-object v2

    .line 26
    :cond_0
    sget-object v0, Lcom/twitter/android/moments/ui/maker/navigation/ak;->a:Lcom/twitter/android/moments/ui/maker/navigation/ak;

    goto :goto_0
.end method
