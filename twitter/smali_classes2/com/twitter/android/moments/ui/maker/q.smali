.class public Lcom/twitter/android/moments/ui/maker/q;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lwy;

.field private final b:Lcom/twitter/android/moments/ui/maker/y;

.field private final c:Lakv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lakv",
            "<",
            "Lcom/twitter/util/collection/k",
            "<",
            "Landroid/net/Uri;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lzw;


# direct methods
.method public constructor <init>(Lwy;Lcom/twitter/android/moments/ui/maker/y;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lzw;)V
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/q;->a:Lwy;

    .line 48
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/q;->b:Lcom/twitter/android/moments/ui/maker/y;

    .line 49
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/q;->d:Lzw;

    .line 50
    new-instance v0, Lcom/twitter/android/moments/ui/maker/q$1;

    invoke-direct {v0, p0, p3}, Lcom/twitter/android/moments/ui/maker/q$1;-><init>(Lcom/twitter/android/moments/ui/maker/q;Lcom/twitter/android/moments/ui/maker/navigation/ah;)V

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/q;->c:Lakv;

    .line 62
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/q;->b:Lcom/twitter/android/moments/ui/maker/y;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/q;->c:Lakv;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/y;->b(Lakv;)V

    .line 63
    return-void
.end method

.method public static a(Landroid/app/Activity;Lakt;Lwy;Lcom/twitter/android/moments/ui/maker/navigation/ah;J)Lcom/twitter/android/moments/ui/maker/q;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lakt",
            "<",
            "Lcom/twitter/android/moments/ui/maker/y$a;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Landroid/net/Uri;",
            ">;>;",
            "Lwy;",
            "Lcom/twitter/android/moments/ui/maker/navigation/ah;",
            "J)",
            "Lcom/twitter/android/moments/ui/maker/q;"
        }
    .end annotation

    .prologue
    .line 38
    .line 39
    invoke-static {p0, p4, p5}, Lzw;->a(Landroid/content/Context;J)Lzw;

    move-result-object v0

    .line 40
    new-instance v1, Lcom/twitter/android/moments/ui/maker/q;

    new-instance v2, Lcom/twitter/android/moments/ui/maker/y;

    invoke-direct {v2, p1}, Lcom/twitter/android/moments/ui/maker/y;-><init>(Lakt;)V

    invoke-direct {v1, p2, v2, p3, v0}, Lcom/twitter/android/moments/ui/maker/q;-><init>(Lwy;Lcom/twitter/android/moments/ui/maker/y;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lzw;)V

    return-object v1
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/q;)Lzw;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/q;->d:Lzw;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/maker/q;)Lwy;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/q;->a:Lwy;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/q;->b:Lcom/twitter/android/moments/ui/maker/y;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/q;->b:Lcom/twitter/android/moments/ui/maker/y;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/y;->a()Lcom/twitter/android/moments/ui/maker/y$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/y;->a(Lcom/twitter/android/moments/ui/maker/y$a;)V

    .line 67
    return-void
.end method

.method public a(Lcom/twitter/model/moments/viewmodels/MomentPage;)V
    .locals 4

    .prologue
    .line 70
    new-instance v0, Lcez$a;

    invoke-direct {v0}, Lcez$a;-><init>()V

    .line 71
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcez$a;->a(Lcom/twitter/model/moments/r;)Lcez$a;

    move-result-object v1

    .line 72
    instance-of v0, p1, Lcom/twitter/model/moments/viewmodels/e;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 73
    check-cast v0, Lcom/twitter/model/moments/viewmodels/e;

    invoke-interface {v0}, Lcom/twitter/model/moments/viewmodels/e;->c()Lcom/twitter/model/moments/e;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcez$a;->a(Lcom/twitter/model/moments/e;)Lcez$a;

    .line 75
    :cond_0
    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/q;->a:Lwy;

    new-instance v3, Lcex$a;

    invoke-direct {v3}, Lcex$a;-><init>()V

    .line 76
    invoke-virtual {v1}, Lcez$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcez;

    invoke-virtual {v3, v0}, Lcex$a;->a(Lcez;)Lcex$a;

    move-result-object v0

    invoke-virtual {v0}, Lcex$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfd;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/twitter/model/moments/viewmodels/MomentPage;

    const/4 v3, 0x0

    aput-object p1, v1, v3

    .line 77
    invoke-static {v1}, Lcom/twitter/model/moments/viewmodels/g;->a([Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/model/moments/viewmodels/g;

    move-result-object v1

    .line 76
    invoke-virtual {v2, v0, v1}, Lwy;->a(Lcfd;Lcom/twitter/model/moments/viewmodels/g;)V

    .line 78
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/q;->a:Lwy;

    invoke-virtual {v0}, Lwy;->c()Lrx/g;

    move-result-object v0

    invoke-static {}, Lcqw;->d()Lcqw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    .line 79
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/q;->b:Lcom/twitter/android/moments/ui/maker/y;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/q;->c:Lakv;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/y;->a(Lakv;)V

    .line 83
    return-void
.end method
