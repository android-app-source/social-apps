.class public Lcom/twitter/android/moments/ui/maker/c$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lajr;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/maker/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lajr",
        "<",
        "Lcom/twitter/model/moments/viewmodels/g;",
        "Lcom/twitter/android/moments/ui/maker/d;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Landroid/view/LayoutInflater;

.field final b:Lcom/twitter/android/moments/ui/maker/ap$a;

.field private final c:Lcom/twitter/android/moments/data/af;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/moments/data/af",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lzt;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Lcom/twitter/android/moments/ui/maker/ap$a;Lcom/twitter/android/moments/data/af;Lzt;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/LayoutInflater;",
            "Lcom/twitter/android/moments/ui/maker/ap$a;",
            "Lcom/twitter/android/moments/data/af",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;",
            "Lzt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/c$a;->a:Landroid/view/LayoutInflater;

    .line 218
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/c$a;->b:Lcom/twitter/android/moments/ui/maker/ap$a;

    .line 219
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/c$a;->c:Lcom/twitter/android/moments/data/af;

    .line 220
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/c$a;->d:Lzt;

    .line 221
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/viewmodels/g;)I
    .locals 1

    .prologue
    .line 239
    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 205
    check-cast p1, Lcom/twitter/model/moments/viewmodels/g;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/c$a;->a(Lcom/twitter/model/moments/viewmodels/g;)I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/ViewGroup;Lcjs;I)Lcom/twitter/android/moments/ui/maker/d;
    .locals 6

    .prologue
    .line 227
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/c$a;->a:Landroid/view/LayoutInflater;

    .line 228
    invoke-static {v0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/a;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/a;

    move-result-object v1

    .line 230
    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/a;->c()Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    move-result-object v0

    invoke-static {v0}, Lacz;->a(Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;)Lacz;

    move-result-object v4

    .line 231
    new-instance v2, Lcom/twitter/android/moments/ui/maker/be;

    .line 232
    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/a;->b()Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/c$a;->b:Lcom/twitter/android/moments/ui/maker/ap$a;

    invoke-direct {v2, v0, v3}, Lcom/twitter/android/moments/ui/maker/be;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;Lcom/twitter/android/moments/ui/maker/ap$a;)V

    .line 233
    new-instance v0, Lcom/twitter/android/moments/ui/maker/d;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/c$a;->c:Lcom/twitter/android/moments/data/af;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/maker/c$a;->d:Lzt;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/moments/ui/maker/d;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/a;Lcom/twitter/android/moments/ui/maker/be;Lcom/twitter/android/moments/data/af;Lacz;Lzt;)V

    return-object v0
.end method

.method public synthetic b(Landroid/view/ViewGroup;Lcjs;I)Lajq;
    .locals 1

    .prologue
    .line 205
    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/moments/ui/maker/c$a;->a(Landroid/view/ViewGroup;Lcjs;I)Lcom/twitter/android/moments/ui/maker/d;

    move-result-object v0

    return-object v0
.end method
