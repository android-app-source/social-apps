.class public Lcom/twitter/android/moments/ui/maker/m;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lajn;
.implements Lajq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lajn",
        "<",
        "Lada;",
        ">;",
        "Lajq",
        "<",
        "Lada;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

.field private final c:Lcom/twitter/android/moments/ui/maker/be;

.field private final d:Lacz;

.field private final e:Lcom/twitter/android/moments/ui/maker/navigation/ah;

.field private f:Lcom/twitter/model/moments/viewmodels/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/moments/ui/maker/viewdelegate/e;Lcom/twitter/android/moments/ui/maker/be;Lacz;Lcom/twitter/android/moments/ui/maker/navigation/ah;)V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/m;->a:Landroid/content/Context;

    .line 43
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/m;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

    .line 44
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/m;->c:Lcom/twitter/android/moments/ui/maker/be;

    .line 45
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/m;->d:Lacz;

    .line 46
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/m;->e:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    .line 47
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/m;->c:Lcom/twitter/android/moments/ui/maker/be;

    .line 48
    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/be;->b()Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/m$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/m$1;-><init>(Lcom/twitter/android/moments/ui/maker/m;)V

    .line 49
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 55
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/m;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/m;->d()V

    return-void
.end method

.method private a(Lcom/twitter/model/moments/viewmodels/g;)V
    .locals 2

    .prologue
    .line 112
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/g;->a()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/maker/m;->b(Lcom/twitter/model/moments/viewmodels/MomentPage;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/m;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->d()V

    .line 114
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/m;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/m$3;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/maker/m$3;-><init>(Lcom/twitter/android/moments/ui/maker/m;Lcom/twitter/model/moments/viewmodels/g;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->a(Landroid/view/View$OnClickListener;)V

    .line 123
    :goto_0
    return-void

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/m;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->f()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/maker/m;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/m;->a:Landroid/content/Context;

    return-object v0
.end method

.method private b(Lcom/twitter/model/moments/viewmodels/g;)V
    .locals 3

    .prologue
    .line 126
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/g;->a()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 127
    instance-of v1, v0, Lcom/twitter/model/moments/viewmodels/q;

    if-eqz v1, :cond_0

    .line 128
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/m;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->g()V

    .line 129
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/m;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

    new-instance v2, Lcom/twitter/android/moments/ui/maker/m$4;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/moments/ui/maker/m$4;-><init>(Lcom/twitter/android/moments/ui/maker/m;Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->c(Landroid/view/View$OnClickListener;)V

    .line 140
    :goto_0
    return-void

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/m;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->h()V

    goto :goto_0
.end method

.method private b(Lcom/twitter/model/moments/viewmodels/MomentPage;)Z
    .locals 1

    .prologue
    .line 143
    instance-of v0, p1, Lcom/twitter/model/moments/viewmodels/o;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/maker/m;)Lcom/twitter/android/moments/ui/maker/navigation/ah;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/m;->e:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 100
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/m;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->f()V

    .line 101
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/m;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->h()V

    .line 102
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/m;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->a(Landroid/view/View$OnClickListener;)V

    .line 103
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/m;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->c(Landroid/view/View$OnClickListener;)V

    .line 104
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/m;->f:Lcom/twitter/model/moments/viewmodels/g;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/g;

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/maker/m;->a(Lcom/twitter/model/moments/viewmodels/g;)V

    .line 108
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/m;->f:Lcom/twitter/model/moments/viewmodels/g;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/g;

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/maker/m;->b(Lcom/twitter/model/moments/viewmodels/g;)V

    .line 109
    return-void
.end method


# virtual methods
.method public a(Lada;)V
    .locals 2

    .prologue
    .line 80
    invoke-virtual {p1}, Lada;->b()Lcom/twitter/model/moments/viewmodels/g;

    move-result-object v0

    .line 81
    if-nez v0, :cond_0

    .line 82
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/m;->b()V

    .line 90
    :goto_0
    return-void

    .line 85
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/m;->c()V

    .line 86
    invoke-virtual {p1}, Lada;->b()Lcom/twitter/model/moments/viewmodels/g;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/m;->f:Lcom/twitter/model/moments/viewmodels/g;

    .line 87
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/m;->c:Lcom/twitter/android/moments/ui/maker/be;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/m;->f:Lcom/twitter/model/moments/viewmodels/g;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/be;->a(Lcom/twitter/model/moments/viewmodels/g;)V

    .line 88
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/m;->d:Lacz;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/m;->f:Lcom/twitter/model/moments/viewmodels/g;

    invoke-virtual {v0, v1}, Lacz;->a(Lcom/twitter/model/moments/viewmodels/g;)V

    .line 89
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/m;->f:Lcom/twitter/model/moments/viewmodels/g;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/g;->a()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/moments/ui/maker/m;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/moments/viewmodels/MomentPage;)V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/m;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/m$2;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/maker/m$2;-><init>(Lcom/twitter/android/moments/ui/maker/m;Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->d(Landroid/view/View$OnClickListener;)V

    .line 76
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lada;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/m;->a(Lada;)V

    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/m;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->aN_()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/m;->c()V

    .line 95
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/m;->c:Lcom/twitter/android/moments/ui/maker/be;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/be;->c()V

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/m;->f:Lcom/twitter/model/moments/viewmodels/g;

    .line 97
    return-void
.end method
