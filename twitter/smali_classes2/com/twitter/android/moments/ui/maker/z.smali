.class Lcom/twitter/android/moments/ui/maker/z;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/ui/widget/Tooltip$c;


# instance fields
.field private final a:Landroid/support/v4/app/FragmentManager;

.field private final b:Lcom/twitter/android/util/h;

.field private c:Lcom/twitter/ui/widget/Tooltip$a;


# direct methods
.method constructor <init>(Landroid/support/v4/app/FragmentManager;Lcom/twitter/ui/widget/Tooltip$a;Lcom/twitter/android/util/h;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/z;->a:Landroid/support/v4/app/FragmentManager;

    .line 40
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/z;->c:Lcom/twitter/ui/widget/Tooltip$a;

    .line 41
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/z;->b:Lcom/twitter/android/util/h;

    .line 42
    return-void
.end method

.method static a(Lcom/twitter/app/common/base/BaseFragmentActivity;)Lcom/twitter/android/moments/ui/maker/z;
    .locals 5

    .prologue
    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f1304da

    invoke-static {v0, v1}, Lcom/twitter/ui/widget/Tooltip;->a(Landroid/content/Context;I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v1, 0x7f0a056e

    .line 28
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->a(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v1, 0x7f0d03d1

    .line 29
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->b(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v1, 0x7f130155

    .line 30
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->d(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    .line 31
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 33
    invoke-virtual {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v4, "geo_setting_tooltip"

    invoke-static {v1, v4, v2, v3}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v1

    .line 34
    new-instance v2, Lcom/twitter/android/moments/ui/maker/z;

    invoke-virtual {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-direct {v2, v3, v0, v1}, Lcom/twitter/android/moments/ui/maker/z;-><init>(Landroid/support/v4/app/FragmentManager;Lcom/twitter/ui/widget/Tooltip$a;Lcom/twitter/android/util/h;)V

    return-object v2
.end method


# virtual methods
.method a()V
    .locals 3

    .prologue
    .line 54
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/z;->c:Lcom/twitter/ui/widget/Tooltip$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/z;->b:Lcom/twitter/android/util/h;

    invoke-virtual {v0}, Lcom/twitter/android/util/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/z;->c:Lcom/twitter/ui/widget/Tooltip$a;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/Tooltip$a;->a(Lcom/twitter/ui/widget/Tooltip$c;)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/z;->a:Landroid/support/v4/app/FragmentManager;

    const-string/jumbo v2, "geo_setting_tooltip"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/ui/widget/Tooltip$a;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)Lcom/twitter/ui/widget/Tooltip;

    .line 56
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/z;->b:Lcom/twitter/android/util/h;

    invoke-virtual {v0}, Lcom/twitter/android/util/h;->b()V

    .line 58
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/ui/widget/Tooltip;I)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 46
    if-ne p2, v0, :cond_1

    .line 47
    invoke-virtual {p1, v0}, Lcom/twitter/ui/widget/Tooltip;->a(Z)V

    .line 51
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/z;->c:Lcom/twitter/ui/widget/Tooltip$a;

    goto :goto_0
.end method
