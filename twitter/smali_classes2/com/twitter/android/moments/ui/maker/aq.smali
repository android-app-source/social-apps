.class Lcom/twitter/android/moments/ui/maker/aq;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/preference/PreferenceActivity;

.field private final b:J

.field private final c:Lxp;

.field private final d:Lcom/twitter/util/object/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/moments/viewmodels/a;",
            "Lwy;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/twitter/android/moments/ui/maker/ar;


# direct methods
.method constructor <init>(Lcom/twitter/app/common/inject/InjectedPreferenceActivity;JLxp;Lcom/twitter/util/object/d;Lcom/twitter/android/moments/ui/maker/ar;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/app/common/inject/InjectedPreferenceActivity;",
            "J",
            "Lxp;",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/moments/viewmodels/a;",
            "Lwy;",
            ">;",
            "Lcom/twitter/android/moments/ui/maker/ar;",
            ")V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/aq;->a:Landroid/preference/PreferenceActivity;

    .line 64
    iput-wide p2, p0, Lcom/twitter/android/moments/ui/maker/aq;->b:J

    .line 65
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/aq;->c:Lxp;

    .line 66
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/aq;->d:Lcom/twitter/util/object/d;

    .line 67
    iput-object p6, p0, Lcom/twitter/android/moments/ui/maker/aq;->e:Lcom/twitter/android/moments/ui/maker/ar;

    .line 69
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/aq;->a()V

    .line 70
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/aq;->b()V

    .line 71
    return-void
.end method

.method static a(Lcom/twitter/app/common/inject/InjectedPreferenceActivity;Lzg;J)Lcom/twitter/android/moments/ui/maker/aq;
    .locals 8

    .prologue
    .line 39
    .line 40
    invoke-interface {p1}, Lzg;->c()Lxp;

    move-result-object v4

    .line 41
    new-instance v5, Lcom/twitter/android/moments/ui/maker/aq$1;

    invoke-direct {v5, p1, v4}, Lcom/twitter/android/moments/ui/maker/aq$1;-><init>(Lzg;Lxp;)V

    .line 52
    invoke-interface {p1}, Lzg;->e()Lcom/twitter/library/api/moments/maker/j;

    move-result-object v0

    .line 51
    invoke-static {p0, p2, p3, v0}, Lxa;->a(Landroid/content/Context;JLcom/twitter/library/api/moments/maker/j;)Lxa;

    move-result-object v0

    .line 53
    new-instance v6, Lcom/twitter/android/moments/ui/maker/ar;

    invoke-direct {v6, p0, v0, p2, p3}, Lcom/twitter/android/moments/ui/maker/ar;-><init>(Landroid/preference/PreferenceActivity;Lxa;J)V

    .line 55
    new-instance v0, Lcom/twitter/android/moments/ui/maker/aq;

    move-object v1, p0

    move-wide v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/moments/ui/maker/aq;-><init>(Lcom/twitter/app/common/inject/InjectedPreferenceActivity;JLxp;Lcom/twitter/util/object/d;Lcom/twitter/android/moments/ui/maker/ar;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/aq;)Lcom/twitter/util/object/d;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aq;->d:Lcom/twitter/util/object/d;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aq;->a:Landroid/preference/PreferenceActivity;

    const v1, 0x7f0402c0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceActivity;->setContentView(I)V

    .line 75
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aq;->e:Lcom/twitter/android/moments/ui/maker/ar;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/ar;->b()Lcom/twitter/android/moments/ui/maker/ba;

    .line 76
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/aq;Lcom/twitter/model/moments/Moment;Lwy;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/moments/ui/maker/aq;->a(Lcom/twitter/model/moments/Moment;Lwy;)V

    return-void
.end method

.method private a(Lcom/twitter/model/moments/Moment;Lwy;)V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aq;->a:Landroid/preference/PreferenceActivity;

    const v1, 0x7f080012

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceActivity;->addPreferencesFromResource(I)V

    .line 111
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aq;->e:Lcom/twitter/android/moments/ui/maker/ar;

    iget-boolean v1, p1, Lcom/twitter/model/moments/Moment;->f:Z

    invoke-virtual {v0, p2, v1}, Lcom/twitter/android/moments/ui/maker/ar;->a(Lwy;Z)Lcom/twitter/android/moments/ui/maker/az;

    .line 112
    iget-object v0, p1, Lcom/twitter/model/moments/Moment;->r:Lcom/twitter/model/moments/f;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/twitter/model/moments/Moment;->r:Lcom/twitter/model/moments/f;

    iget-object v0, v0, Lcom/twitter/model/moments/f;->d:Ljava/lang/Boolean;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 115
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/aq;->e:Lcom/twitter/android/moments/ui/maker/ar;

    invoke-virtual {v1, p2, v0}, Lcom/twitter/android/moments/ui/maker/ar;->b(Lwy;Z)Lcom/twitter/android/moments/ui/maker/ah;

    .line 116
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aq;->e:Lcom/twitter/android/moments/ui/maker/ar;

    iget-object v1, p1, Lcom/twitter/model/moments/Moment;->r:Lcom/twitter/model/moments/f;

    invoke-virtual {v0, p2, v1}, Lcom/twitter/android/moments/ui/maker/ar;->a(Lwy;Lcom/twitter/model/moments/f;)Lcom/twitter/android/moments/ui/maker/bh;

    .line 117
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aq;->e:Lcom/twitter/android/moments/ui/maker/ar;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/ar;->a()Lcom/twitter/android/moments/ui/maker/w;

    .line 118
    return-void

    .line 112
    :cond_0
    sget-object v0, Lcom/twitter/model/moments/f;->b:Lcom/twitter/model/moments/f;

    iget-object v0, v0, Lcom/twitter/model/moments/f;->d:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aq;->c:Lxp;

    iget-wide v2, p0, Lcom/twitter/android/moments/ui/maker/aq;->b:J

    invoke-virtual {v0, v2, v3}, Lxp;->a(J)Lrx/c;

    move-result-object v0

    .line 80
    invoke-static {}, Lcom/twitter/android/moments/ui/maker/aq;->c()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    const/4 v1, 0x1

    .line 81
    invoke-virtual {v0, v1}, Lrx/c;->d(I)Lrx/c;

    move-result-object v0

    .line 82
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/aq;->d()Lcqw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 83
    return-void
.end method

.method private static c()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<-",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    new-instance v0, Lcom/twitter/android/moments/ui/maker/aq$2;

    invoke-direct {v0}, Lcom/twitter/android/moments/ui/maker/aq$2;-><init>()V

    return-object v0
.end method

.method private d()Lcqw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcqw",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 97
    new-instance v0, Lcom/twitter/android/moments/ui/maker/aq$3;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/maker/aq$3;-><init>(Lcom/twitter/android/moments/ui/maker/aq;)V

    return-object v0
.end method
