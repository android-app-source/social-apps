.class Lcom/twitter/android/moments/ui/maker/x;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/maker/x$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/x$a;

.field private final b:Lcom/twitter/android/util/h;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/maker/x$a;Lcom/twitter/android/util/h;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/x;->a:Lcom/twitter/android/moments/ui/maker/x$a;

    .line 33
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/x;->b:Lcom/twitter/android/util/h;

    .line 34
    return-void
.end method

.method static a(Landroid/app/Activity;)Lcom/twitter/android/moments/ui/maker/x;
    .locals 9

    .prologue
    .line 24
    new-instance v8, Lcom/twitter/android/moments/ui/maker/x$a;

    invoke-direct {v8, p0}, Lcom/twitter/android/moments/ui/maker/x$a;-><init>(Landroid/app/Activity;)V

    .line 25
    new-instance v0, Lcom/twitter/android/util/h;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "editing_published_moment"

    const v3, 0x7fffffff

    const-wide/32 v4, 0x240c8400

    .line 26
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/util/h;-><init>(Landroid/content/Context;Ljava/lang/String;IJJ)V

    .line 27
    new-instance v1, Lcom/twitter/android/moments/ui/maker/x;

    invoke-direct {v1, v8, v0}, Lcom/twitter/android/moments/ui/maker/x;-><init>(Lcom/twitter/android/moments/ui/maker/x$a;Lcom/twitter/android/util/h;)V

    return-object v1
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/x;->b:Lcom/twitter/android/util/h;

    invoke-virtual {v0}, Lcom/twitter/android/util/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/x;->a:Lcom/twitter/android/moments/ui/maker/x$a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/x$a;->a()V

    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/x;->b:Lcom/twitter/android/util/h;

    invoke-virtual {v0}, Lcom/twitter/android/util/h;->b()V

    .line 41
    :cond_0
    return-void
.end method
