.class public Lcom/twitter/android/moments/ui/maker/au;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/twitter/android/moments/ui/maker/av;

.field private final c:Lwy;

.field private final d:Lcom/twitter/android/moments/ui/maker/a;

.field private final e:Landroid/app/TaskStackBuilder;

.field private final f:Lcom/twitter/android/moments/ui/fullscreen/bh;

.field private final g:Lcom/twitter/library/client/v;

.field private h:Lzv;


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/twitter/android/moments/ui/maker/av;Lwy;Lcom/twitter/android/moments/ui/maker/a;Landroid/app/TaskStackBuilder;Lcom/twitter/android/moments/ui/fullscreen/bh;Lcom/twitter/library/client/v;Lzv;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/au;->a:Landroid/app/Activity;

    .line 59
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/au;->b:Lcom/twitter/android/moments/ui/maker/av;

    .line 60
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/au;->c:Lwy;

    .line 61
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/au;->d:Lcom/twitter/android/moments/ui/maker/a;

    .line 62
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/au;->e:Landroid/app/TaskStackBuilder;

    .line 63
    iput-object p6, p0, Lcom/twitter/android/moments/ui/maker/au;->f:Lcom/twitter/android/moments/ui/fullscreen/bh;

    .line 64
    iput-object p7, p0, Lcom/twitter/android/moments/ui/maker/au;->g:Lcom/twitter/library/client/v;

    .line 65
    iput-object p8, p0, Lcom/twitter/android/moments/ui/maker/au;->h:Lzv;

    .line 66
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/au;Lcom/twitter/model/moments/Moment;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/maker/au;->a(Lcom/twitter/model/moments/Moment;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/twitter/model/moments/Moment;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 105
    new-instance v0, Lcom/twitter/android/moments/ui/maker/au$2;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/moments/ui/maker/au$2;-><init>(Lcom/twitter/android/moments/ui/maker/au;Lcom/twitter/model/moments/Moment;)V

    return-object v0
.end method

.method public static a(Landroid/app/Activity;Lwy;Lcom/twitter/android/moments/ui/maker/a;J)Lcom/twitter/android/moments/ui/maker/au;
    .locals 9

    .prologue
    .line 45
    invoke-static {p3, p4}, Lzv;->a(J)Lzv;

    move-result-object v8

    .line 46
    new-instance v0, Lcom/twitter/android/moments/ui/maker/au;

    new-instance v2, Lcom/twitter/android/moments/ui/maker/av;

    invoke-direct {v2, p0}, Lcom/twitter/android/moments/ui/maker/av;-><init>(Landroid/app/Activity;)V

    .line 47
    invoke-static {p0}, Landroid/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/app/TaskStackBuilder;

    move-result-object v5

    new-instance v6, Lcom/twitter/android/moments/ui/fullscreen/bh;

    .line 48
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v6, v1}, Lcom/twitter/android/moments/ui/fullscreen/bh;-><init>(Landroid/content/res/Resources;)V

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v7

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/moments/ui/maker/au;-><init>(Landroid/app/Activity;Lcom/twitter/android/moments/ui/maker/av;Lwy;Lcom/twitter/android/moments/ui/maker/a;Landroid/app/TaskStackBuilder;Lcom/twitter/android/moments/ui/fullscreen/bh;Lcom/twitter/library/client/v;Lzv;)V

    .line 46
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/au;)Lcom/twitter/library/client/v;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/au;->g:Lcom/twitter/library/client/v;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/au;Lcom/twitter/model/moments/viewmodels/a;)Z
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/maker/au;->a(Lcom/twitter/model/moments/viewmodels/a;)Z

    move-result v0

    return v0
.end method

.method private a(Lcom/twitter/model/moments/viewmodels/a;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 94
    move v1, v2

    :goto_0
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/a;->b()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 95
    invoke-virtual {p1, v1}, Lcom/twitter/model/moments/viewmodels/a;->c(I)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->h()Lcom/twitter/model/moments/MomentPageDisplayMode;

    move-result-object v0

    sget-object v3, Lcom/twitter/model/moments/MomentPageDisplayMode;->b:Lcom/twitter/model/moments/MomentPageDisplayMode;

    if-ne v0, v3, :cond_0

    .line 100
    :goto_1
    return v2

    .line 94
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 100
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method private b()Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 117
    new-instance v0, Lcom/twitter/android/moments/ui/maker/au$3;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/maker/au$3;-><init>(Lcom/twitter/android/moments/ui/maker/au;)V

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/maker/au;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/au;->b()Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/maker/au;Lcom/twitter/model/moments/Moment;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/maker/au;->b(Lcom/twitter/model/moments/Moment;)V

    return-void
.end method

.method private b(Lcom/twitter/model/moments/Moment;)V
    .locals 3

    .prologue
    .line 128
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/au;->h:Lzv;

    invoke-virtual {v0}, Lzv;->c()V

    .line 129
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/au;->c:Lwy;

    new-instance v0, Lcex$a;

    invoke-direct {v0}, Lcex$a;-><init>()V

    sget-object v2, Lcom/twitter/model/moments/MomentVisibilityMode;->a:Lcom/twitter/model/moments/MomentVisibilityMode;

    .line 130
    invoke-virtual {v0, v2}, Lcex$a;->a(Lcom/twitter/model/moments/MomentVisibilityMode;)Lcex$a;

    move-result-object v0

    invoke-virtual {v0}, Lcex$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfd;

    .line 129
    invoke-virtual {v1, v0}, Lwy;->a(Lcfd;)V

    .line 131
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/au;->c:Lwy;

    invoke-virtual {v0}, Lwy;->c()Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/au$4;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/maker/au$4;-><init>(Lcom/twitter/android/moments/ui/maker/au;Lcom/twitter/model/moments/Moment;)V

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    .line 137
    return-void
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/maker/au;)Lcom/twitter/android/moments/ui/maker/av;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/au;->b:Lcom/twitter/android/moments/ui/maker/av;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/maker/au;Lcom/twitter/model/moments/Moment;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/maker/au;->c(Lcom/twitter/model/moments/Moment;)V

    return-void
.end method

.method private c(Lcom/twitter/model/moments/Moment;)V
    .locals 6

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/au;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 141
    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/au;->f:Lcom/twitter/android/moments/ui/fullscreen/bh;

    .line 142
    invoke-virtual {v1, p1}, Lcom/twitter/android/moments/ui/fullscreen/bh;->a(Lcom/twitter/model/moments/Moment;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/composer/a;->a(Ljava/lang/String;I)Lcom/twitter/android/composer/a;

    move-result-object v0

    const/4 v1, 0x1

    .line 143
    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->d(Z)Lcom/twitter/app/common/base/h;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/composer/a;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/au;->a:Landroid/app/Activity;

    .line 144
    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 146
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/au;->e:Landroid/app/TaskStackBuilder;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/au;->a:Landroid/app/Activity;

    const-class v4, Lcom/twitter/app/main/MainActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 147
    invoke-virtual {v1, v2}, Landroid/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/au;->a:Landroid/app/Activity;

    iget-wide v4, p1, Lcom/twitter/model/moments/Moment;->b:J

    .line 148
    invoke-static {v2, v4, v5}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->a(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    move-result-object v1

    .line 149
    invoke-virtual {v1, v0}, Landroid/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    move-result-object v0

    .line 150
    invoke-virtual {v0}, Landroid/app/TaskStackBuilder;->startActivities()V

    .line 151
    return-void
.end method

.method static synthetic d(Lcom/twitter/android/moments/ui/maker/au;)Lcom/twitter/android/moments/ui/maker/a;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/au;->d:Lcom/twitter/android/moments/ui/maker/a;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/au;->c:Lwy;

    invoke-virtual {v0}, Lwy;->a()Lrx/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lrx/c;->d(I)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/au$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/au$1;-><init>(Lcom/twitter/android/moments/ui/maker/au;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 91
    return-void
.end method
