.class public Lcom/twitter/android/moments/ui/maker/ad$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lajr;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/maker/ad;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lajr",
        "<",
        "Lcom/twitter/model/moments/viewmodels/g;",
        "Lcom/twitter/android/moments/ui/maker/ab;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Landroid/view/LayoutInflater;

.field final b:Lcom/twitter/android/moments/ui/maker/ap$a;

.field private final c:Lcom/twitter/android/moments/ui/maker/ax;

.field private final d:Lcom/twitter/android/moments/ui/maker/navigation/ah;

.field private final e:Lwy;

.field private final f:Lzx;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Lcom/twitter/android/moments/ui/maker/ap$a;Lcom/twitter/android/moments/ui/maker/ax;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lwy;Lzx;)V
    .locals 0

    .prologue
    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/ad$a;->a:Landroid/view/LayoutInflater;

    .line 177
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/ad$a;->b:Lcom/twitter/android/moments/ui/maker/ap$a;

    .line 178
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/ad$a;->c:Lcom/twitter/android/moments/ui/maker/ax;

    .line 179
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/ad$a;->d:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    .line 180
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/ad$a;->e:Lwy;

    .line 181
    iput-object p6, p0, Lcom/twitter/android/moments/ui/maker/ad$a;->f:Lzx;

    .line 182
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/viewmodels/g;)I
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 161
    check-cast p1, Lcom/twitter/model/moments/viewmodels/g;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/ad$a;->a(Lcom/twitter/model/moments/viewmodels/g;)I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/ViewGroup;Lcjs;I)Lcom/twitter/android/moments/ui/maker/ab;
    .locals 8

    .prologue
    .line 188
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ad$a;->a:Landroid/view/LayoutInflater;

    .line 189
    invoke-static {v0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/s;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/s;

    move-result-object v1

    .line 191
    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/s;->c()Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    move-result-object v0

    invoke-static {v0}, Lacz;->a(Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;)Lacz;

    move-result-object v6

    .line 192
    new-instance v2, Lcom/twitter/android/moments/ui/maker/be;

    .line 193
    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/s;->b()Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/ad$a;->b:Lcom/twitter/android/moments/ui/maker/ap$a;

    invoke-direct {v2, v0, v3}, Lcom/twitter/android/moments/ui/maker/be;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;Lcom/twitter/android/moments/ui/maker/ap$a;)V

    .line 194
    new-instance v0, Lcom/twitter/android/moments/ui/maker/ab;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/ad$a;->c:Lcom/twitter/android/moments/ui/maker/ax;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/maker/ad$a;->d:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/maker/ad$a;->e:Lwy;

    iget-object v7, p0, Lcom/twitter/android/moments/ui/maker/ad$a;->f:Lzx;

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/moments/ui/maker/ab;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/s;Lcom/twitter/android/moments/ui/maker/be;Lcom/twitter/android/moments/ui/maker/ax;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lwy;Lacz;Lzx;)V

    return-object v0
.end method

.method public synthetic b(Landroid/view/ViewGroup;Lcjs;I)Lajq;
    .locals 1

    .prologue
    .line 161
    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/moments/ui/maker/ad$a;->a(Landroid/view/ViewGroup;Lcjs;I)Lcom/twitter/android/moments/ui/maker/ab;

    move-result-object v0

    return-object v0
.end method
