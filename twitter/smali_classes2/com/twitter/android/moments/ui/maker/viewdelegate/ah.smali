.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field private final b:Lcom/twitter/android/moments/ui/maker/viewdelegate/aj;

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/maker/viewdelegate/aj;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;->a:Landroid/view/ViewGroup;

    .line 25
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/aj;

    .line 26
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/viewdelegate/aj;

    invoke-direct {v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/aj;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;-><init>(Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/maker/viewdelegate/aj;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/ui/maker/bb;Z)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 35
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 36
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;->b()V

    .line 38
    :cond_0
    invoke-interface {p1}, Lcom/twitter/android/moments/ui/maker/bb;->a()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;->c:Landroid/view/View;

    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;->c:Landroid/view/View;

    const/4 v2, 0x0

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 41
    if-eqz p2, :cond_1

    .line 42
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/aj;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;->c:Landroid/view/View;

    const/16 v2, 0x12c

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/aj;->a(Landroid/view/View;I)V

    .line 44
    :cond_1
    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;->a:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;->c:Landroid/view/View;

    .line 49
    return-void
.end method
