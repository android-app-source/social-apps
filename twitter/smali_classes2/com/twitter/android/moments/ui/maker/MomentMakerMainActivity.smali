.class public Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;,
        Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/android/moments/ui/maker/aj;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Laog;
    .locals 7

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity;->X()Lann;

    move-result-object v0

    check-cast v0, Lze;

    .line 55
    invoke-interface {v0}, Lze;->d()Lxn;

    move-result-object v1

    .line 56
    new-instance v2, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$1;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$1;-><init>(Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity;Lze;)V

    .line 67
    invoke-interface {v0}, Lze;->g()Lyf;

    move-result-object v3

    .line 68
    invoke-interface {v0}, Lze;->h()Lyg;

    move-result-object v4

    .line 69
    invoke-interface {v0}, Lze;->i()Lcif;

    move-result-object v5

    .line 70
    invoke-interface {v0}, Lze;->j()Lcii;

    move-result-object v6

    move-object v0, p0

    .line 56
    invoke-static/range {v0 .. v6}, Lcom/twitter/android/moments/ui/maker/aj;->a(Lcom/twitter/app/common/inject/InjectedFragmentActivity;Lxm;Lcom/twitter/util/object/d;Lyf;Lyg;Lcif;Lcii;)Lcom/twitter/android/moments/ui/maker/aj;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity;->a:Lcom/twitter/android/moments/ui/maker/aj;

    .line 71
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity;->a:Lcom/twitter/android/moments/ui/maker/aj;

    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;->a(Landroid/content/Intent;)Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/aj;->a(Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;)V

    .line 72
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity;->a:Lcom/twitter/android/moments/ui/maker/aj;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/aj;->aN_()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x100

    invoke-static {v0, v1}, Lcom/twitter/util/d;->b(Landroid/view/View;I)V

    .line 73
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity;->a:Lcom/twitter/android/moments/ui/maker/aj;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->b(Z)V

    .line 47
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 48
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    return-object v0
.end method

.method protected d(Lank;)Lze;
    .locals 2

    .prologue
    .line 79
    invoke-static {}, Lza;->a()Lza$a;

    move-result-object v0

    .line 80
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lza$a;->a(Lamu;)Lza$a;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Lza$a;->a()Lze;

    move-result-object v0

    .line 79
    return-object v0
.end method

.method protected synthetic e(Lank;)Lcom/twitter/app/common/base/i;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity;->d(Lank;)Lze;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f(Lank;)Lcom/twitter/app/common/abs/a;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity;->d(Lank;)Lze;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic g(Lank;)Lann;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity;->d(Lank;)Lze;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity;->a:Lcom/twitter/android/moments/ui/maker/aj;

    if-nez v0, :cond_0

    .line 87
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onBackPressed()V

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity;->a:Lcom/twitter/android/moments/ui/maker/aj;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/aj;->e()V

    goto :goto_0
.end method
