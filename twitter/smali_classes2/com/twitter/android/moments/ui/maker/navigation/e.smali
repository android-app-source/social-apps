.class public Lcom/twitter/android/moments/ui/maker/navigation/e;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/maker/navigation/ak;


# instance fields
.field private final b:Lcom/twitter/android/moments/ui/maker/navigation/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/moments/ui/maker/navigation/f",
            "<",
            "Lcom/twitter/model/moments/r;",
            "Lcom/twitter/model/moments/r;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/android/moments/ui/maker/navigation/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/moments/ui/maker/navigation/aj",
            "<",
            "Lcom/twitter/android/moments/ui/maker/navigation/b;",
            "Lcom/twitter/android/moments/ui/maker/navigation/w;",
            "Lcom/twitter/android/moments/ui/maker/navigation/am;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/twitter/android/moments/ui/maker/navigation/s;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/navigation/aj;Lcom/twitter/android/moments/ui/maker/navigation/f;Lcom/twitter/android/moments/ui/maker/navigation/s;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/ui/maker/navigation/aj",
            "<",
            "Lcom/twitter/android/moments/ui/maker/navigation/b;",
            "Lcom/twitter/android/moments/ui/maker/navigation/w;",
            "Lcom/twitter/android/moments/ui/maker/navigation/am;",
            ">;",
            "Lcom/twitter/android/moments/ui/maker/navigation/f",
            "<",
            "Lcom/twitter/model/moments/r;",
            "Lcom/twitter/model/moments/r;",
            ">;",
            "Lcom/twitter/android/moments/ui/maker/navigation/s;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/navigation/e;->c:Lcom/twitter/android/moments/ui/maker/navigation/aj;

    .line 40
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/navigation/e;->b:Lcom/twitter/android/moments/ui/maker/navigation/f;

    .line 41
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/navigation/e;->d:Lcom/twitter/android/moments/ui/maker/navigation/s;

    .line 42
    return-void
.end method

.method public static a(Lcom/twitter/android/moments/ui/maker/navigation/aj;)Lcom/twitter/android/moments/ui/maker/navigation/ak;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/ui/maker/navigation/aj",
            "<",
            "Lcom/twitter/android/moments/ui/maker/navigation/b;",
            "Lcom/twitter/android/moments/ui/maker/navigation/w;",
            "Lcom/twitter/android/moments/ui/maker/navigation/am;",
            ">;)",
            "Lcom/twitter/android/moments/ui/maker/navigation/ak;"
        }
    .end annotation

    .prologue
    .line 27
    new-instance v1, Lcom/twitter/android/moments/ui/maker/navigation/e;

    new-instance v2, Lcom/twitter/android/moments/ui/maker/navigation/f;

    .line 31
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/maker/navigation/aj;

    invoke-direct {v2, v0}, Lcom/twitter/android/moments/ui/maker/navigation/f;-><init>(Lcom/twitter/android/moments/ui/maker/navigation/aj;)V

    sget-object v0, Lcom/twitter/android/moments/ui/maker/navigation/s;->a:Lcom/twitter/android/moments/ui/maker/navigation/s;

    invoke-direct {v1, p0, v2, v0}, Lcom/twitter/android/moments/ui/maker/navigation/e;-><init>(Lcom/twitter/android/moments/ui/maker/navigation/aj;Lcom/twitter/android/moments/ui/maker/navigation/f;Lcom/twitter/android/moments/ui/maker/navigation/s;)V

    .line 27
    return-object v1
.end method


# virtual methods
.method public a()Lrx/g;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/e;->c:Lcom/twitter/android/moments/ui/maker/navigation/aj;

    iget-object v0, v0, Lcom/twitter/android/moments/ui/maker/navigation/aj;->a:Lcom/twitter/android/moments/ui/maker/navigation/ai;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/maker/navigation/b;

    .line 48
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/navigation/e;->c:Lcom/twitter/android/moments/ui/maker/navigation/aj;

    iget-object v1, v1, Lcom/twitter/android/moments/ui/maker/navigation/aj;->b:Lcom/twitter/android/moments/ui/maker/navigation/ai;

    check-cast v1, Lcom/twitter/android/moments/ui/maker/navigation/w;

    .line 49
    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/navigation/b;->e()Lcom/twitter/android/moments/ui/maker/viewdelegate/g;

    move-result-object v2

    .line 50
    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/navigation/w;->e()Lcom/twitter/android/moments/ui/maker/viewdelegate/u;

    move-result-object v1

    .line 51
    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/g;->c()V

    .line 52
    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/u;->b()V

    .line 53
    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/u;->a()V

    .line 54
    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/navigation/b;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    const/16 v0, 0x14d

    invoke-virtual {v2, v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/g;->b(I)V

    .line 56
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/e;->b:Lcom/twitter/android/moments/ui/maker/navigation/f;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/navigation/f;->a()Lrx/g;

    move-result-object v0

    .line 58
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/e;->d:Lcom/twitter/android/moments/ui/maker/navigation/s;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/navigation/e;->c:Lcom/twitter/android/moments/ui/maker/navigation/aj;

    .line 59
    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/navigation/s;->a(Lcom/twitter/android/moments/ui/maker/navigation/aj;)Lcom/twitter/android/moments/ui/maker/navigation/ak;

    move-result-object v0

    .line 60
    invoke-interface {v0}, Lcom/twitter/android/moments/ui/maker/navigation/ak;->a()Lrx/g;

    move-result-object v0

    goto :goto_0
.end method
