.class public Lcom/twitter/android/moments/ui/maker/be;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# instance fields
.field final a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;

.field private final b:Lcom/twitter/android/moments/ui/maker/ap$a;

.field private final c:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject",
            "<",
            "Lcom/twitter/android/moments/ui/maker/be;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lrx/j;

.field private e:Lcom/twitter/android/moments/ui/maker/bb;

.field private f:Lcom/twitter/model/moments/viewmodels/MomentPage;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;Lcom/twitter/android/moments/ui/maker/ap$a;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/be;->c:Lrx/subjects/PublishSubject;

    .line 30
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/be;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;

    .line 31
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/be;->b:Lcom/twitter/android/moments/ui/maker/ap$a;

    .line 32
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/be;)Lrx/subjects/PublishSubject;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/be;->c:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method private a(Lcom/twitter/android/moments/ui/maker/bb;Z)V
    .locals 1

    .prologue
    .line 78
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/be;->e:Lcom/twitter/android/moments/ui/maker/bb;

    .line 79
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/be;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;->a(Lcom/twitter/android/moments/ui/maker/bb;Z)V

    .line 80
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/be;Lcom/twitter/android/moments/ui/maker/bb;Z)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/moments/ui/maker/be;->a(Lcom/twitter/android/moments/ui/maker/bb;Z)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/viewmodels/g;)V
    .locals 3

    .prologue
    .line 51
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/g;->a()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 52
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/be;->f:Lcom/twitter/model/moments/viewmodels/MomentPage;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/be;->f:Lcom/twitter/model/moments/viewmodels/MomentPage;

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/model/moments/r;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/be;->f:Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 53
    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->m()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->m()Ljava/util/Set;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/be;->c:Lrx/subjects/PublishSubject;

    invoke-virtual {v0, p0}, Lrx/subjects/PublishSubject;->a(Ljava/lang/Object;)V

    .line 75
    :goto_0
    return-void

    .line 57
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/be;->f:Lcom/twitter/model/moments/viewmodels/MomentPage;

    if-eqz v1, :cond_1

    .line 58
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/be;->c()V

    .line 60
    :cond_1
    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/be;->f:Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 61
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/be;->b:Lcom/twitter/android/moments/ui/maker/ap$a;

    .line 62
    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/maker/ap$a;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/android/moments/ui/maker/ap;

    move-result-object v1

    .line 63
    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/ap;->b()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 65
    :goto_1
    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/ap;->a()Lrx/g;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/moments/ui/maker/be$1;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/moments/ui/maker/be$1;-><init>(Lcom/twitter/android/moments/ui/maker/be;Z)V

    .line 66
    invoke-virtual {v1, v2}, Lrx/g;->a(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/be;->d:Lrx/j;

    goto :goto_0

    .line 63
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/be;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;->aN_()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcom/twitter/android/moments/ui/maker/be;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/be;->c:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method public c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 83
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/be;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;->b()V

    .line 84
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/be;->d:Lrx/j;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/be;->d:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 87
    :cond_0
    iput-object v1, p0, Lcom/twitter/android/moments/ui/maker/be;->d:Lrx/j;

    .line 88
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/be;->e:Lcom/twitter/android/moments/ui/maker/bb;

    if-eqz v0, :cond_1

    .line 89
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/be;->e:Lcom/twitter/android/moments/ui/maker/bb;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/maker/bb;->b()V

    .line 91
    :cond_1
    iput-object v1, p0, Lcom/twitter/android/moments/ui/maker/be;->e:Lcom/twitter/android/moments/ui/maker/bb;

    .line 92
    iput-object v1, p0, Lcom/twitter/android/moments/ui/maker/be;->f:Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 93
    return-void
.end method
