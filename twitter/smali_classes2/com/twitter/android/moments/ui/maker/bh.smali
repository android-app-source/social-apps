.class public Lcom/twitter/android/moments/ui/maker/bh;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/maker/bh$a;
    }
.end annotation


# instance fields
.field private final a:Lwy;

.field private final b:Lcom/twitter/android/moments/ui/maker/bh$a;

.field private final c:Lzv;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/maker/bh$a;Lcom/twitter/android/moments/ui/maker/bi;Lwy;Lcom/twitter/model/moments/f;Lzv;)V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/bh;->a:Lwy;

    .line 51
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/bh;->b:Lcom/twitter/android/moments/ui/maker/bh$a;

    .line 52
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/bh;->c:Lzv;

    .line 53
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/bh;->b:Lcom/twitter/android/moments/ui/maker/bh$a;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/bh$1;

    invoke-direct {v1, p0, p2}, Lcom/twitter/android/moments/ui/maker/bh$1;-><init>(Lcom/twitter/android/moments/ui/maker/bh;Lcom/twitter/android/moments/ui/maker/bi;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/bh$a;->a(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 68
    iget-object v0, p4, Lcom/twitter/model/moments/f;->c:Lcom/twitter/model/moments/MomentVisibilityMode;

    sget-object v1, Lcom/twitter/model/moments/MomentVisibilityMode;->b:Lcom/twitter/model/moments/MomentVisibilityMode;

    if-ne v0, v1, :cond_0

    .line 69
    invoke-virtual {p1}, Lcom/twitter/android/moments/ui/maker/bh$a;->a()V

    .line 71
    :cond_0
    return-void
.end method

.method public static a(Landroid/preference/PreferenceActivity;Lwy;Lcom/twitter/model/moments/f;J)Lcom/twitter/android/moments/ui/maker/bh;
    .locals 7

    .prologue
    .line 34
    const-string/jumbo v0, "pref_unpublish_moment"

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 35
    new-instance v2, Lcom/twitter/android/moments/ui/maker/bi;

    invoke-direct {v2, p0}, Lcom/twitter/android/moments/ui/maker/bi;-><init>(Landroid/app/Activity;)V

    .line 37
    new-instance v1, Lcom/twitter/android/moments/ui/maker/bh$a;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/moments/ui/maker/bh$a;-><init>(Landroid/preference/PreferenceActivity;Landroid/preference/Preference;)V

    .line 39
    invoke-static {p3, p4}, Lzv;->a(J)Lzv;

    move-result-object v5

    .line 40
    new-instance v0, Lcom/twitter/android/moments/ui/maker/bh;

    sget-object v3, Lcom/twitter/model/moments/f;->b:Lcom/twitter/model/moments/f;

    .line 41
    invoke-static {p2, v3}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/model/moments/f;

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/moments/ui/maker/bh;-><init>(Lcom/twitter/android/moments/ui/maker/bh$a;Lcom/twitter/android/moments/ui/maker/bi;Lwy;Lcom/twitter/model/moments/f;Lzv;)V

    .line 40
    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 74
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/bh;->a:Lwy;

    new-instance v0, Lcex$a;

    invoke-direct {v0}, Lcex$a;-><init>()V

    sget-object v2, Lcom/twitter/model/moments/MomentVisibilityMode;->b:Lcom/twitter/model/moments/MomentVisibilityMode;

    .line 75
    invoke-virtual {v0, v2}, Lcex$a;->a(Lcom/twitter/model/moments/MomentVisibilityMode;)Lcex$a;

    move-result-object v0

    invoke-virtual {v0}, Lcex$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfd;

    .line 74
    invoke-virtual {v1, v0}, Lwy;->a(Lcfd;)V

    .line 76
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/bh;->a:Lwy;

    invoke-virtual {v0}, Lwy;->c()Lrx/g;

    move-result-object v0

    invoke-static {}, Lcqw;->d()Lcqw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    .line 77
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/bh;->b:Lcom/twitter/android/moments/ui/maker/bh$a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/bh$a;->b()V

    .line 78
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/bh;->b:Lcom/twitter/android/moments/ui/maker/bh$a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/bh$a;->a()V

    .line 79
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/bh;->c:Lzv;

    invoke-virtual {v0}, Lzv;->d()V

    .line 80
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/bh;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/bh;->a()V

    return-void
.end method
