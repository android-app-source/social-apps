.class public Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity;
.super Lcom/twitter/app/common/inject/InjectedPreferenceActivity;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;,
        Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lank;)Lzg;
    .locals 2

    .prologue
    .line 32
    invoke-static {}, Lzc;->a()Lzc$a;

    move-result-object v0

    .line 33
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lzc$a;->a(Lamu;)Lzc$a;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lzc$a;->a()Lzg;

    move-result-object v0

    .line 32
    return-object v0
.end method

.method protected synthetic b(Lank;)Lann;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity;->a(Lank;)Lzg;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 24
    invoke-super {p0, p1}, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 25
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$a;->a(Landroid/content/Intent;)Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$a;

    move-result-object v0

    move-object v1, v0

    .line 26
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity;->o()Lann;

    move-result-object v0

    check-cast v0, Lzg;

    .line 27
    iget-wide v2, v1, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$a;->a:J

    invoke-static {p0, v0, v2, v3}, Lcom/twitter/android/moments/ui/maker/aq;->a(Lcom/twitter/app/common/inject/InjectedPreferenceActivity;Lzg;J)Lcom/twitter/android/moments/ui/maker/aq;

    .line 28
    return-void

    .line 25
    :cond_0
    invoke-static {}, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$a;->a()Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$a;

    move-result-object v0

    move-object v1, v0

    goto :goto_0
.end method
