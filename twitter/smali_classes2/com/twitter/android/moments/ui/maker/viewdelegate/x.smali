.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/x;
.super Landroid/support/v7/widget/RecyclerView$ItemDecoration;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/internal/android/widget/c;

.field private final b:F


# direct methods
.method public constructor <init>(Lcom/twitter/internal/android/widget/c;F)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$ItemDecoration;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/x;->a:Lcom/twitter/internal/android/widget/c;

    .line 33
    iput p2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/x;->b:F

    .line 34
    return-void
.end method

.method public static a(II)Lcom/twitter/android/moments/ui/maker/viewdelegate/x;
    .locals 3

    .prologue
    .line 26
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/x;

    new-instance v1, Lcom/twitter/internal/android/widget/c;

    invoke-direct {v1, p0, p1}, Lcom/twitter/internal/android/widget/c;-><init>(II)V

    const/high16 v2, 0x3f100000    # 0.5625f

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/x;-><init>(Lcom/twitter/internal/android/widget/c;F)V

    return-object v0
.end method


# virtual methods
.method public getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$State;)V
    .locals 3

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/x;->a:Lcom/twitter/internal/android/widget/c;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/twitter/internal/android/widget/c;->getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$State;)V

    .line 42
    invoke-virtual {p3, p2}, Landroid/support/v7/widget/RecyclerView;->getChildLayoutPosition(Landroid/view/View;)I

    move-result v0

    .line 44
    invoke-virtual {p3}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p3}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p3}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/x;->b:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 46
    if-nez v0, :cond_1

    .line 47
    invoke-virtual {p3}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v0

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 51
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    invoke-virtual {p4}, Landroid/support/v7/widget/RecyclerView$State;->getItemCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v0, v2, :cond_0

    .line 49
    invoke-virtual {p3}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v0

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p1, Landroid/graphics/Rect;->right:I

    goto :goto_0
.end method
