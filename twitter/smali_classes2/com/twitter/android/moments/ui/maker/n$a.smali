.class public Lcom/twitter/android/moments/ui/maker/n$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lajr;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/maker/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lajr",
        "<",
        "Lada;",
        "Lajq",
        "<",
        "Lada;",
        ">;>;"
    }
.end annotation


# instance fields
.field final a:Landroid/view/LayoutInflater;

.field final b:Lcom/twitter/android/moments/ui/maker/ap$a;

.field private final c:Lcom/twitter/android/moments/ui/maker/navigation/ah;

.field private final d:Landroid/content/Context;

.field private final e:Lwy;

.field private final f:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/twitter/android/moments/ui/maker/ap$a;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lwy;J)V
    .locals 0

    .prologue
    .line 347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 348
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/n$a;->a:Landroid/view/LayoutInflater;

    .line 349
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/n$a;->b:Lcom/twitter/android/moments/ui/maker/ap$a;

    .line 350
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/n$a;->c:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    .line 351
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/n$a;->d:Landroid/content/Context;

    .line 352
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/n$a;->e:Lwy;

    .line 353
    iput-wide p6, p0, Lcom/twitter/android/moments/ui/maker/n$a;->f:J

    .line 354
    return-void
.end method


# virtual methods
.method public a(Lada;)I
    .locals 1

    .prologue
    .line 394
    instance-of v0, p1, Ladi;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lade;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 331
    check-cast p1, Lada;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/n$a;->a(Lada;)I

    move-result v0

    return v0
.end method

.method public b(Landroid/view/ViewGroup;Lcjs;I)Lajq;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Lcjs;",
            "I)",
            "Lajq",
            "<",
            "Lada;",
            ">;"
        }
    .end annotation

    .prologue
    .line 361
    packed-switch p3, :pswitch_data_0

    .line 372
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n$a;->a:Landroid/view/LayoutInflater;

    .line 373
    invoke-static {v0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

    move-result-object v2

    .line 374
    new-instance v4, Lcom/twitter/android/moments/ui/maker/be;

    .line 375
    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->c()Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/n$a;->b:Lcom/twitter/android/moments/ui/maker/ap$a;

    invoke-direct {v4, v0, v1}, Lcom/twitter/android/moments/ui/maker/be;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;Lcom/twitter/android/moments/ui/maker/ap$a;)V

    .line 378
    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->b()Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    move-result-object v0

    invoke-static {v0}, Lacz;->a(Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;)Lacz;

    move-result-object v5

    .line 379
    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    .line 380
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/n$a;->d:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/n$a;->c:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    iget-wide v6, p0, Lcom/twitter/android/moments/ui/maker/n$a;->f:J

    invoke-static/range {v1 .. v7}, Lcom/twitter/android/moments/ui/maker/p;->a(Landroid/content/Context;Lcom/twitter/android/moments/ui/maker/viewdelegate/e;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/ui/maker/be;Lacz;J)Lcom/twitter/android/moments/ui/maker/p;

    move-result-object v6

    .line 389
    :goto_0
    return-object v6

    .line 363
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n$a;->a:Landroid/view/LayoutInflater;

    .line 365
    invoke-static {v0, p1}, Lcom/twitter/android/moments/ui/maker/s;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/s;

    move-result-object v1

    .line 366
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/n$a;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/n$a;->c:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/n$a;->e:Lwy;

    iget-wide v4, p0, Lcom/twitter/android/moments/ui/maker/n$a;->f:J

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/moments/ui/maker/r;->a(Landroid/content/Context;Lcom/twitter/android/moments/ui/maker/s;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lwy;J)Lcom/twitter/android/moments/ui/maker/r;

    move-result-object v6

    goto :goto_0

    .line 383
    :cond_0
    new-instance v6, Lcom/twitter/android/moments/ui/maker/m;

    iget-object v7, p0, Lcom/twitter/android/moments/ui/maker/n$a;->d:Landroid/content/Context;

    iget-object v11, p0, Lcom/twitter/android/moments/ui/maker/n$a;->c:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    move-object v8, v2

    move-object v9, v4

    move-object v10, v5

    invoke-direct/range {v6 .. v11}, Lcom/twitter/android/moments/ui/maker/m;-><init>(Landroid/content/Context;Lcom/twitter/android/moments/ui/maker/viewdelegate/e;Lcom/twitter/android/moments/ui/maker/be;Lacz;Lcom/twitter/android/moments/ui/maker/navigation/ah;)V

    goto :goto_0

    .line 361
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
