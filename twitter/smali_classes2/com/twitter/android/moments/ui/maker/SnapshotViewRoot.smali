.class public Lcom/twitter/android/moments/ui/maker/SnapshotViewRoot;
.super Landroid/widget/FrameLayout;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/util/math/Size;

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/util/math/Size;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 24
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/SnapshotViewRoot;->a:Lcom/twitter/util/math/Size;

    .line 25
    return-void
.end method


# virtual methods
.method public requestLayout()V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingSuperCall"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 30
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/maker/SnapshotViewRoot;->b:Z

    if-nez v0, :cond_1

    .line 31
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/maker/SnapshotViewRoot;->b:Z

    move v0, v1

    .line 32
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/SnapshotViewRoot;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 33
    invoke-virtual {p0, v0}, Lcom/twitter/android/moments/ui/maker/SnapshotViewRoot;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/SnapshotViewRoot;->a:Lcom/twitter/util/math/Size;

    invoke-static {v2, v3}, Lcom/twitter/android/moments/ui/maker/bm;->a(Landroid/view/View;Lcom/twitter/util/math/Size;)V

    .line 32
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 35
    :cond_0
    iput-boolean v1, p0, Lcom/twitter/android/moments/ui/maker/SnapshotViewRoot;->b:Z

    .line 37
    :cond_1
    return-void
.end method
