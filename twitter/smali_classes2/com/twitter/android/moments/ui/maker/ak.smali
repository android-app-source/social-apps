.class public Lcom/twitter/android/moments/ui/maker/ak;
.super Laog;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/maker/ak$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/navigation/ah;

.field private final b:Lcom/twitter/android/moments/ui/maker/bd;


# direct methods
.method public constructor <init>(Lcim;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/ui/maker/bd;)V
    .locals 3

    .prologue
    .line 68
    invoke-direct {p0}, Laog;-><init>()V

    .line 69
    invoke-virtual {p0, p2}, Lcom/twitter/android/moments/ui/maker/ak;->a(Landroid/view/View;)V

    .line 70
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/ak;->b:Lcom/twitter/android/moments/ui/maker/bd;

    .line 71
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/ak;->a:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    .line 72
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ak;->a:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    sget-object v1, Lcom/twitter/android/moments/ui/maker/navigation/NavigationKey;->a:Lcom/twitter/android/moments/ui/maker/navigation/NavigationKey;

    new-instance v2, Lcom/twitter/android/moments/ui/maker/navigation/c;

    invoke-direct {v2, p1}, Lcom/twitter/android/moments/ui/maker/navigation/c;-><init>(Lcim;)V

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/moments/ui/maker/navigation/ah;->a(Lcom/twitter/android/moments/ui/maker/navigation/NavigationKey;Lcom/twitter/android/moments/ui/maker/navigation/ag;)V

    .line 73
    return-void
.end method

.method public static a(Lcom/twitter/app/common/inject/InjectedFragmentActivity;Lwy;JLcim;Lyf;Lyg;)Lcom/twitter/android/moments/ui/maker/ak;
    .locals 18

    .prologue
    .line 44
    new-instance v4, Lcom/twitter/android/moments/ui/maker/bc;

    new-instance v5, Landroid/util/LruCache;

    const/16 v6, 0xc

    invoke-direct {v5, v6}, Landroid/util/LruCache;-><init>(I)V

    invoke-direct {v4, v5}, Lcom/twitter/android/moments/ui/maker/bc;-><init>(Landroid/util/LruCache;)V

    .line 45
    new-instance v15, Lcom/twitter/android/moments/ui/maker/bd;

    .line 46
    invoke-virtual/range {p1 .. p1}, Lwy;->b()Lrx/c;

    move-result-object v5

    invoke-direct {v15, v4, v5}, Lcom/twitter/android/moments/ui/maker/bd;-><init>(Lcom/twitter/android/moments/ui/maker/bc;Lrx/c;)V

    .line 48
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-static {v0, v4, v1}, Lcom/twitter/android/moments/ui/maker/ap$a;->a(Landroid/app/Activity;Lcom/twitter/android/moments/ui/maker/bc;Lyf;)Lcom/twitter/android/moments/ui/maker/ap$a;

    move-result-object v13

    .line 49
    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f0401d8

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    move-object v14, v4

    check-cast v14, Landroid/view/ViewGroup;

    .line 51
    new-instance v4, Lcom/twitter/android/moments/ui/maker/a;

    const/16 v8, 0x270f

    new-instance v11, Lcom/twitter/util/android/g;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lcom/twitter/util/android/g;-><init>(Landroid/content/Context;)V

    .line 53
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    move-object/from16 v5, p0

    move-wide/from16 v6, p2

    move-object/from16 v9, p1

    move-object/from16 v10, p6

    invoke-direct/range {v4 .. v12}, Lcom/twitter/android/moments/ui/maker/a;-><init>(Landroid/app/Activity;JILwy;Lyg;Lcom/twitter/util/android/g;Landroid/content/res/Resources;)V

    .line 55
    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-static {v0, v1, v2}, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;->a(Landroid/app/Activity;J)Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;

    move-result-object v10

    .line 56
    new-instance v11, Lcom/twitter/android/moments/ui/maker/y$b;

    const/16 v5, 0x1b1

    move-object/from16 v0, p0

    invoke-direct {v11, v0, v5}, Lcom/twitter/android/moments/ui/maker/y$b;-><init>(Landroid/app/Activity;I)V

    .line 59
    new-instance v16, Lcom/twitter/android/moments/ui/maker/navigation/ad;

    new-instance v17, Lcom/twitter/android/moments/ui/maker/navigation/ac;

    move-object/from16 v0, v17

    move-wide/from16 v1, p2

    move-object/from16 v3, p4

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/moments/ui/maker/navigation/ac;-><init>(JLcim;)V

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object v7, v13

    move-object v8, v14

    move-object v9, v4

    move-wide/from16 v12, p2

    .line 61
    invoke-static/range {v5 .. v13}, Lcom/twitter/android/moments/ui/maker/navigation/ae;->a(Lcom/twitter/app/common/base/BaseFragmentActivity;Lwy;Lcom/twitter/android/moments/ui/maker/ap$a;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/maker/a;Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;Lakt;J)Lcom/twitter/android/moments/ui/maker/navigation/ae;

    move-result-object v8

    new-instance v9, Lcom/twitter/android/moments/ui/maker/navigation/t;

    new-instance v4, Lcom/twitter/android/moments/ui/maker/navigation/af;

    invoke-direct {v4}, Lcom/twitter/android/moments/ui/maker/navigation/af;-><init>()V

    invoke-direct {v9, v4}, Lcom/twitter/android/moments/ui/maker/navigation/t;-><init>(Lcom/twitter/util/object/d;)V

    move-object/from16 v4, v16

    move-object/from16 v5, p0

    move-object v6, v14

    move-object/from16 v7, v17

    invoke-direct/range {v4 .. v9}, Lcom/twitter/android/moments/ui/maker/navigation/ad;-><init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/maker/navigation/ac;Lcom/twitter/android/moments/ui/maker/navigation/ae;Lcom/twitter/android/moments/ui/maker/navigation/al;)V

    .line 64
    new-instance v4, Lcom/twitter/android/moments/ui/maker/ak;

    move-object/from16 v0, p4

    move-object/from16 v1, v16

    invoke-direct {v4, v0, v14, v1, v15}, Lcom/twitter/android/moments/ui/maker/ak;-><init>(Lcim;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/ui/maker/bd;)V

    return-object v4
.end method


# virtual methods
.method public am_()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ak;->b:Lcom/twitter/android/moments/ui/maker/bd;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/bd;->a()V

    .line 82
    invoke-super {p0}, Laog;->am_()V

    .line 83
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ak;->a:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/maker/navigation/ah;->a()V

    .line 77
    return-void
.end method
