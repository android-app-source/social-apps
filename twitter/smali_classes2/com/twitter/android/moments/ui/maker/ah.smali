.class public Lcom/twitter/android/moments/ui/maker/ah;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/maker/ah$a;,
        Lcom/twitter/android/moments/ui/maker/ah$b;
    }
.end annotation


# instance fields
.field private final a:Lwy;

.field private final b:Lzv;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/maker/ah$b;Lwy;ZLzv;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/ah;->a:Lwy;

    .line 36
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/ah;->b:Lzv;

    .line 37
    invoke-virtual {p1, p3}, Lcom/twitter/android/moments/ui/maker/ah$b;->a(Z)V

    .line 38
    new-instance v0, Lcom/twitter/android/moments/ui/maker/ah$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/maker/ah$1;-><init>(Lcom/twitter/android/moments/ui/maker/ah;)V

    invoke-virtual {p1, v0}, Lcom/twitter/android/moments/ui/maker/ah$b;->a(Lcom/twitter/android/moments/ui/maker/ah$a;)V

    .line 46
    return-void
.end method

.method static a(Landroid/preference/PreferenceActivity;Lwy;ZJ)Lcom/twitter/android/moments/ui/maker/ah;
    .locals 3

    .prologue
    .line 23
    const-string/jumbo v0, "pref_publish_location"

    .line 24
    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 25
    new-instance v1, Lcom/twitter/android/moments/ui/maker/ah$b;

    invoke-direct {v1, v0}, Lcom/twitter/android/moments/ui/maker/ah$b;-><init>(Landroid/preference/CheckBoxPreference;)V

    .line 27
    invoke-static {p3, p4}, Lzv;->a(J)Lzv;

    move-result-object v0

    .line 28
    new-instance v2, Lcom/twitter/android/moments/ui/maker/ah;

    invoke-direct {v2, v1, p1, p2, v0}, Lcom/twitter/android/moments/ui/maker/ah;-><init>(Lcom/twitter/android/moments/ui/maker/ah$b;Lwy;ZLzv;)V

    return-object v2
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/ah;Z)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/maker/ah;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 49
    if-nez p1, :cond_0

    .line 50
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ah;->b:Lzv;

    invoke-virtual {v0}, Lzv;->g()V

    .line 52
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/ah;->a:Lwy;

    new-instance v0, Lcex$a;

    invoke-direct {v0}, Lcex$a;-><init>()V

    .line 53
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcex$a;->b(Ljava/lang/Boolean;)Lcex$a;

    move-result-object v0

    invoke-virtual {v0}, Lcex$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfd;

    .line 52
    invoke-virtual {v1, v0}, Lwy;->a(Lcfd;)V

    .line 54
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ah;->a:Lwy;

    invoke-virtual {v0}, Lwy;->c()Lrx/g;

    move-result-object v0

    invoke-static {}, Lcqw;->d()Lcqw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    .line 55
    return-void
.end method
