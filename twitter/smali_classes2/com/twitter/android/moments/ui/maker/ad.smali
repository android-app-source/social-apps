.class public Lcom/twitter/android/moments/ui/maker/ad;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/maker/ad$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/viewdelegate/v;

.field private final b:Lwy;

.field private final c:Lcom/twitter/android/moments/ui/maker/al;

.field private final d:Lajt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajt",
            "<",
            "Lcom/twitter/model/moments/viewmodels/g;",
            "Lcom/twitter/android/moments/ui/maker/ab;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lzx;

.field private final f:Lcom/twitter/android/moments/ui/maker/navigation/ah;

.field private final g:Lcom/twitter/android/moments/ui/maker/ax;

.field private h:Lrx/j;

.field private i:Z


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/ax;Lwy;Lcom/twitter/android/moments/ui/maker/viewdelegate/v;Lajt;Lcom/twitter/android/moments/ui/maker/al;Lcom/twitter/android/moments/ui/maker/aa;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lzx;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/ui/maker/ax;",
            "Lwy;",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/v;",
            "Lajt",
            "<",
            "Lcom/twitter/model/moments/viewmodels/g;",
            "Lcom/twitter/android/moments/ui/maker/ab;",
            ">;",
            "Lcom/twitter/android/moments/ui/maker/al;",
            "Lcom/twitter/android/moments/ui/maker/aa;",
            "Lcom/twitter/android/moments/ui/maker/navigation/ah;",
            "Lzx;",
            ")V"
        }
    .end annotation

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/ad;->b:Lwy;

    .line 91
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/ad;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/v;

    .line 92
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/ad;->c:Lcom/twitter/android/moments/ui/maker/al;

    .line 93
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/ad;->d:Lajt;

    .line 94
    iput-object p8, p0, Lcom/twitter/android/moments/ui/maker/ad;->e:Lzx;

    .line 95
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ad;->d:Lajt;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lajt;->a(Z)V

    .line 96
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ad;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/v;

    invoke-virtual {p4}, Lajt;->b()Lajs;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/v;->a(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 97
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ad;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/v;

    invoke-virtual {v0, p6}, Lcom/twitter/android/moments/ui/maker/viewdelegate/v;->a(Lcom/twitter/android/moments/ui/maker/viewdelegate/r$b;)V

    .line 98
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/ad;->g:Lcom/twitter/android/moments/ui/maker/ax;

    .line 99
    iput-object p7, p0, Lcom/twitter/android/moments/ui/maker/ad;->f:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    .line 100
    return-void
.end method

.method public static a(Lcom/twitter/model/moments/r;Landroid/content/Context;Lcom/twitter/android/moments/ui/maker/viewdelegate/v;Lcom/twitter/android/moments/ui/maker/viewdelegate/t;Lwy;Lcom/twitter/android/moments/ui/maker/ap$a;Lcom/twitter/android/moments/ui/maker/navigation/ah;J)Lcom/twitter/android/moments/ui/maker/ad;
    .locals 19

    .prologue
    .line 61
    invoke-static {}, Lcom/twitter/android/moments/ui/maker/al;->a()Lcom/twitter/android/moments/ui/maker/al;

    move-result-object v14

    .line 63
    new-instance v5, Lcom/twitter/android/moments/ui/maker/ax;

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    invoke-direct {v5, v0, v1, v14}, Lcom/twitter/android/moments/ui/maker/ax;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/v;Lcom/twitter/model/moments/r;Lcom/twitter/android/moments/ui/maker/al;)V

    .line 64
    invoke-static/range {p7 .. p8}, Lzx;->a(J)Lzx;

    move-result-object v8

    .line 65
    new-instance v2, Lcom/twitter/android/moments/ui/maker/ad$a;

    .line 66
    invoke-static/range {p1 .. p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    move-object/from16 v4, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p4

    invoke-direct/range {v2 .. v8}, Lcom/twitter/android/moments/ui/maker/ad$a;-><init>(Landroid/view/LayoutInflater;Lcom/twitter/android/moments/ui/maker/ap$a;Lcom/twitter/android/moments/ui/maker/ax;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lwy;Lzx;)V

    .line 68
    new-instance v13, Lajt;

    new-instance v3, Lajs;

    invoke-direct {v3, v14, v2}, Lajs;-><init>(Lajv;Lajr;)V

    invoke-direct {v13, v3}, Lajt;-><init>(Lajs;)V

    .line 73
    new-instance v2, Lcom/twitter/android/moments/ui/maker/ac;

    move-object/from16 v0, p3

    move-object/from16 v1, p6

    invoke-direct {v2, v0, v1}, Lcom/twitter/android/moments/ui/maker/ac;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/t;Lcom/twitter/android/moments/ui/maker/navigation/ah;)V

    .line 75
    new-instance v15, Lcom/twitter/android/moments/ui/maker/aa;

    move-object/from16 v0, p4

    invoke-direct {v15, v13, v14, v0, v8}, Lcom/twitter/android/moments/ui/maker/aa;-><init>(Lajt;Lajx;Lwy;Lzx;)V

    .line 78
    new-instance v9, Lcom/twitter/android/moments/ui/maker/ad;

    move-object v10, v5

    move-object/from16 v11, p4

    move-object/from16 v12, p2

    move-object/from16 v16, p6

    move-object/from16 v17, v8

    invoke-direct/range {v9 .. v17}, Lcom/twitter/android/moments/ui/maker/ad;-><init>(Lcom/twitter/android/moments/ui/maker/ax;Lwy;Lcom/twitter/android/moments/ui/maker/viewdelegate/v;Lajt;Lcom/twitter/android/moments/ui/maker/al;Lcom/twitter/android/moments/ui/maker/aa;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lzx;)V

    return-object v9
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/ad;Lcom/twitter/model/moments/viewmodels/a;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/maker/ad;->a(Lcom/twitter/model/moments/viewmodels/a;)V

    return-void
.end method

.method private a(Lcom/twitter/model/moments/viewmodels/a;)V
    .locals 3

    .prologue
    .line 118
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/a;->h()Ljava/util/List;

    move-result-object v0

    .line 119
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/ad;->c:Lcom/twitter/android/moments/ui/maker/al;

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/maker/al;->a(Ljava/util/List;)V

    .line 120
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/ad;->d:Lajt;

    invoke-virtual {v1}, Lajt;->a()V

    .line 121
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 123
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ad;->g:Lcom/twitter/android/moments/ui/maker/ax;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/ax;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    .line 124
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ad;->f:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/maker/navigation/ah;->a()V

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/maker/ad;->i:Z

    if-nez v0, :cond_0

    .line 128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/maker/ad;->i:Z

    .line 129
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ad;->g:Lcom/twitter/android/moments/ui/maker/ax;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/ax;->a()Lcom/twitter/model/moments/r;

    move-result-object v0

    .line 130
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/ad;->c:Lcom/twitter/android/moments/ui/maker/al;

    .line 131
    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/maker/al;->a(Lcom/twitter/model/moments/r;)I

    move-result v0

    .line 132
    :goto_1
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/ad;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/v;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/v;->a(I)V

    goto :goto_0

    .line 131
    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method


# virtual methods
.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ad;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/v;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/v;->aN_()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ad;->h:Lrx/j;

    if-eqz v0, :cond_0

    .line 115
    :goto_0
    return-void

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ad;->e:Lzx;

    invoke-virtual {v0}, Lzx;->a()V

    .line 108
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ad;->b:Lwy;

    invoke-virtual {v0}, Lwy;->a()Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/ad$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/ad$1;-><init>(Lcom/twitter/android/moments/ui/maker/ad;)V

    .line 109
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/ad;->h:Lrx/j;

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ad;->h:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 138
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ad;->b:Lwy;

    invoke-virtual {v0}, Lwy;->c()Lrx/g;

    move-result-object v0

    invoke-static {}, Lcqw;->d()Lcqw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    .line 139
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ad;->e:Lzx;

    invoke-virtual {v0}, Lzx;->b()V

    .line 140
    return-void
.end method

.method public d()Lcom/twitter/model/moments/r;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ad;->g:Lcom/twitter/android/moments/ui/maker/ax;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/ax;->a()Lcom/twitter/model/moments/r;

    move-result-object v0

    return-object v0
.end method
