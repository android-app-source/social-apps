.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;

.field private final c:Landroid/widget/LinearLayout;

.field private final d:Landroid/widget/TextView;

.field private final e:Lcom/twitter/ui/widget/TwitterButton;

.field private final f:Landroid/widget/ProgressBar;


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;Landroid/support/v7/widget/LinearLayoutManager;Lcom/twitter/android/moments/ui/scroll/e;Landroid/support/v7/widget/RecyclerView$ItemDecoration;Landroid/widget/LinearLayout;Landroid/widget/TextView;Lcom/twitter/ui/widget/TwitterButton;Landroid/widget/ProgressBar;)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->a:Landroid/view/View;

    .line 66
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->b:Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;

    .line 67
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->b:Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;

    invoke-virtual {v0, p4}, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;->setOnScrollTouchListener(Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView$a;)V

    .line 68
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->b:Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;

    invoke-virtual {v0, p3}, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 69
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->b:Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;

    invoke-virtual {v0, p5}, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 70
    iput-object p6, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->c:Landroid/widget/LinearLayout;

    .line 71
    iput-object p7, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->d:Landroid/widget/TextView;

    .line 72
    iput-object p8, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->e:Lcom/twitter/ui/widget/TwitterButton;

    .line 73
    iput-object p9, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->f:Landroid/widget/ProgressBar;

    .line 74
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/View;)Lcom/twitter/android/moments/ui/maker/viewdelegate/b;
    .locals 10

    .prologue
    .line 42
    const v0, 0x7f1304c9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;

    .line 43
    new-instance v3, Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;

    const/4 v0, 0x0

    invoke-direct {v3, p0, v0}, Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;-><init>(Landroid/content/Context;Z)V

    .line 46
    invoke-static {p0}, Lcom/twitter/android/moments/ui/scroll/e;->a(Landroid/content/Context;)Lcom/twitter/android/moments/ui/scroll/e;

    move-result-object v4

    .line 47
    const v0, 0x7f1304ca

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    .line 48
    const v0, 0x7f1304cb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 49
    const v0, 0x7f1304cc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/twitter/ui/widget/TwitterButton;

    .line 50
    const v0, 0x7f130535

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ProgressBar;

    .line 52
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0315

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 54
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f0e0316

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 55
    new-instance v5, Lcom/twitter/internal/android/widget/c;

    invoke-direct {v5, v0, v1}, Lcom/twitter/internal/android/widget/c;-><init>(II)V

    .line 56
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;

    move-object v1, p1

    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;-><init>(Landroid/view/View;Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;Landroid/support/v7/widget/LinearLayoutManager;Lcom/twitter/android/moments/ui/scroll/e;Landroid/support/v7/widget/RecyclerView$ItemDecoration;Landroid/widget/LinearLayout;Landroid/widget/TextView;Lcom/twitter/ui/widget/TwitterButton;Landroid/widget/ProgressBar;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/b;
    .locals 3

    .prologue
    .line 35
    .line 36
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401c3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 37
    invoke-static {p0, v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->a(Landroid/content/Context;Landroid/view/View;)Lcom/twitter/android/moments/ui/maker/viewdelegate/b;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/support/v7/widget/RecyclerView$Adapter;)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->b:Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 84
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->e:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 97
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->c:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->b:Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->f:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 101
    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->a:Landroid/view/View;

    return-object v0
.end method

.method public b()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 91
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->f:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 92
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->b:Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;->setVisibility(I)V

    .line 93
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 94
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 104
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->b:Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;->setVisibility(I)V

    .line 105
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 106
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->f:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 107
    return-void
.end method
