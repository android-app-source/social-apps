.class public Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;
.super Lcom/twitter/app/common/base/h;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/base/h",
        "<",
        "Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Long;

.field public final b:Lcim;

.field public final c:Lcev;


# direct methods
.method private constructor <init>(Ljava/lang/Long;Lcim;Lcev;)V
    .locals 3

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/twitter/app/common/base/h;-><init>()V

    .line 111
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;->a:Ljava/lang/Long;

    .line 112
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;->b:Lcim;

    .line 113
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;->c:Lcev;

    .line 114
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "moment_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 115
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "page_matcher"

    sget-object v2, Lcim;->b:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1, p2, v2}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/content/Intent;

    .line 116
    if-eqz p3, :cond_0

    .line 117
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "create_moment_operation"

    sget-object v2, Lcev;->h:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1, p3, v2}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/content/Intent;

    .line 120
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;->d(Z)Lcom/twitter/app/common/base/h;

    .line 121
    return-void
.end method

.method public static a(JLcim;)Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;
    .locals 4

    .prologue
    .line 125
    new-instance v0, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p2, v2}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;-><init>(Ljava/lang/Long;Lcim;Lcev;)V

    return-object v0
.end method

.method public static a(Landroid/content/Intent;)Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 136
    const-string/jumbo v0, "moment_id"

    invoke-virtual {p0, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 137
    const-string/jumbo v0, "page_matcher"

    sget-object v1, Lcim;->b:Lcom/twitter/util/serialization/l;

    invoke-static {p0, v0, v1}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcim;

    .line 139
    const-string/jumbo v1, "create_moment_operation"

    sget-object v4, Lcev;->h:Lcom/twitter/util/serialization/l;

    .line 140
    invoke-static {p0, v1, v4}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcev;

    .line 141
    new-instance v4, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;

    cmp-long v5, v2, v6

    if-nez v5, :cond_0

    const/4 v2, 0x0

    .line 142
    :goto_0
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcim;

    invoke-direct {v4, v2, v0, v1}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;-><init>(Ljava/lang/Long;Lcim;Lcev;)V

    .line 141
    return-object v4

    :cond_0
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_0
.end method

.method public static a(Lcev;)Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;
    .locals 3

    .prologue
    .line 130
    new-instance v0, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;

    const/4 v1, 0x0

    new-instance v2, Lcik;

    invoke-direct {v2}, Lcik;-><init>()V

    invoke-direct {v0, v1, v2, p0}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;-><init>(Ljava/lang/Long;Lcim;Lcev;)V

    return-object v0
.end method
