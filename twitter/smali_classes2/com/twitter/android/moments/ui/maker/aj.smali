.class public Lcom/twitter/android/moments/ui/maker/aj;
.super Laog;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/maker/aj$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/aj$a;

.field private final b:Lxm;

.field private final c:Labg;

.field private final d:Lcom/twitter/android/moments/ui/maker/ak$a;

.field private final e:Lcom/twitter/util/object/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/moments/viewmodels/a;",
            "Lwy;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcif;

.field private final g:Lakp;

.field private final h:Lwz;

.field private final i:Lcii;

.field private final j:Lacy;

.field private k:Lcom/twitter/android/moments/ui/maker/ak;

.field private l:Lwy;

.field private m:Lrx/j;

.field private n:Lrx/j;


# direct methods
.method public constructor <init>(Lakp;Lcom/twitter/android/moments/ui/maker/aj$a;Lxm;Labg;Lcom/twitter/android/moments/ui/maker/ak$a;Lcom/twitter/util/object/d;Lcif;Lwz;Lcii;Lacy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lakp;",
            "Lcom/twitter/android/moments/ui/maker/aj$a;",
            "Lxm;",
            "Labg;",
            "Lcom/twitter/android/moments/ui/maker/ak$a;",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/moments/viewmodels/a;",
            "Lwy;",
            ">;",
            "Lcif;",
            "Lwz;",
            "Lcii;",
            "Lacy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 82
    invoke-direct {p0}, Laog;-><init>()V

    .line 83
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/aj;->a:Lcom/twitter/android/moments/ui/maker/aj$a;

    .line 84
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/aj;->b:Lxm;

    .line 85
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/aj;->c:Labg;

    .line 86
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/aj;->d:Lcom/twitter/android/moments/ui/maker/ak$a;

    .line 87
    iput-object p6, p0, Lcom/twitter/android/moments/ui/maker/aj;->e:Lcom/twitter/util/object/d;

    .line 88
    iput-object p7, p0, Lcom/twitter/android/moments/ui/maker/aj;->f:Lcif;

    .line 89
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/aj;->g:Lakp;

    .line 90
    iput-object p8, p0, Lcom/twitter/android/moments/ui/maker/aj;->h:Lwz;

    .line 91
    iput-object p9, p0, Lcom/twitter/android/moments/ui/maker/aj;->i:Lcii;

    .line 92
    iput-object p10, p0, Lcom/twitter/android/moments/ui/maker/aj;->j:Lacy;

    .line 93
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj;->a:Lcom/twitter/android/moments/ui/maker/aj$a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/aj$a;->aN_()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/moments/ui/maker/aj;->a(Landroid/view/View;)V

    .line 94
    return-void
.end method

.method public static a(Lcom/twitter/app/common/inject/InjectedFragmentActivity;Lxm;Lcom/twitter/util/object/d;Lyf;Lyg;Lcif;Lcii;)Lcom/twitter/android/moments/ui/maker/aj;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/app/common/inject/InjectedFragmentActivity;",
            "Lxm;",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/moments/viewmodels/a;",
            "Lwy;",
            ">;",
            "Lyf;",
            "Lyg;",
            "Lcif;",
            "Lcii;",
            ")",
            "Lcom/twitter/android/moments/ui/maker/aj;"
        }
    .end annotation

    .prologue
    .line 67
    new-instance v2, Lcom/twitter/android/moments/ui/maker/aj$a;

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/twitter/android/moments/ui/maker/aj$a;-><init>(Landroid/view/LayoutInflater;)V

    .line 68
    new-instance v4, Labg;

    invoke-direct {v4, p0}, Labg;-><init>(Landroid/app/Activity;)V

    .line 69
    new-instance v5, Lcom/twitter/android/moments/ui/maker/ak$a;

    invoke-direct {v5, p0, p3, p4}, Lcom/twitter/android/moments/ui/maker/ak$a;-><init>(Lcom/twitter/app/common/inject/InjectedFragmentActivity;Lyf;Lyg;)V

    .line 71
    invoke-static {p0}, Lwz;->a(Landroid/app/Activity;)Lwz;

    move-result-object v8

    .line 72
    new-instance v0, Lcom/twitter/android/moments/ui/maker/aj;

    new-instance v1, Lakp;

    invoke-direct {v1, p0}, Lakp;-><init>(Landroid/app/Activity;)V

    new-instance v10, Lacy;

    new-instance v3, Lcom/twitter/util/android/g;

    invoke-direct {v3, p0}, Lcom/twitter/util/android/g;-><init>(Landroid/content/Context;)V

    invoke-direct {v10, v3}, Lacy;-><init>(Lcom/twitter/util/android/g;)V

    move-object v3, p1

    move-object v6, p2

    move-object/from16 v7, p5

    move-object/from16 v9, p6

    invoke-direct/range {v0 .. v10}, Lcom/twitter/android/moments/ui/maker/aj;-><init>(Lakp;Lcom/twitter/android/moments/ui/maker/aj$a;Lxm;Labg;Lcom/twitter/android/moments/ui/maker/ak$a;Lcom/twitter/util/object/d;Lcif;Lwz;Lcii;Lacy;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/aj;Lcom/twitter/android/moments/ui/maker/ak;)Lcom/twitter/android/moments/ui/maker/ak;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/aj;->k:Lcom/twitter/android/moments/ui/maker/ak;

    return-object p1
.end method

.method private a(Lcim;)Lcqw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcim;",
            ")",
            "Lcqw",
            "<",
            "Lcom/twitter/util/collection/k",
            "<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 132
    new-instance v0, Lcom/twitter/android/moments/ui/maker/aj$3;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/moments/ui/maker/aj$3;-><init>(Lcom/twitter/android/moments/ui/maker/aj;Lcim;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/aj;Lwy;)Lwy;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/aj;->l:Lwy;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/aj;)Lwz;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj;->h:Lwz;

    return-object v0
.end method

.method private a(JLcim;)V
    .locals 3

    .prologue
    .line 145
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj;->b:Lxm;

    invoke-interface {v0, p1, p2}, Lxm;->a(J)Lrx/c;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/moments/ui/maker/aj;->b(JLcim;)Lcqw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 147
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj;->f:Lcif;

    .line 148
    invoke-virtual {v0, p1, p2}, Lcif;->e(J)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/aj$4;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/aj$4;-><init>(Lcom/twitter/android/moments/ui/maker/aj;)V

    .line 149
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj;->m:Lrx/j;

    .line 155
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj;->i:Lcii;

    invoke-virtual {v0, p1, p2}, Lcii;->a(J)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/aj$5;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/aj$5;-><init>(Lcom/twitter/android/moments/ui/maker/aj;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj;->n:Lrx/j;

    .line 162
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/aj;JLcim;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/moments/ui/maker/aj;->a(JLcim;)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/maker/aj;)Labg;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj;->c:Labg;

    return-object v0
.end method

.method private b(JLcim;)Lcqw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcim;",
            ")",
            "Lcqw",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 167
    new-instance v0, Lcom/twitter/android/moments/ui/maker/aj$6;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/twitter/android/moments/ui/maker/aj$6;-><init>(Lcom/twitter/android/moments/ui/maker/aj;JLcim;)V

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/maker/aj;)Lakp;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj;->g:Lakp;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/moments/ui/maker/aj;)Lacy;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj;->j:Lacy;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/moments/ui/maker/aj;)Lcom/twitter/util/object/d;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj;->e:Lcom/twitter/util/object/d;

    return-object v0
.end method

.method static synthetic f()Lrx/functions/d;
    .locals 1

    .prologue
    .line 42
    invoke-static {}, Lcom/twitter/android/moments/ui/maker/aj;->h()Lrx/functions/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/moments/ui/maker/aj;)Lwy;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj;->l:Lwy;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/moments/ui/maker/aj;)Lcom/twitter/android/moments/ui/maker/ak$a;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj;->d:Lcom/twitter/android/moments/ui/maker/ak$a;

    return-object v0
.end method

.method private g()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<",
            "Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;",
            "Lrx/c",
            "<",
            "Lcom/twitter/util/collection/k",
            "<",
            "Ljava/lang/Long;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 106
    new-instance v0, Lcom/twitter/android/moments/ui/maker/aj$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/maker/aj$1;-><init>(Lcom/twitter/android/moments/ui/maker/aj;)V

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/moments/ui/maker/aj;)Lcom/twitter/android/moments/ui/maker/ak;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj;->k:Lcom/twitter/android/moments/ui/maker/ak;

    return-object v0
.end method

.method private static h()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 119
    new-instance v0, Lcom/twitter/android/moments/ui/maker/aj$2;

    invoke-direct {v0}, Lcom/twitter/android/moments/ui/maker/aj$2;-><init>()V

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/moments/ui/maker/aj;)Lcom/twitter/android/moments/ui/maker/aj$a;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj;->a:Lcom/twitter/android/moments/ui/maker/aj$a;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;)V
    .locals 2

    .prologue
    .line 99
    invoke-static {p1}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    .line 100
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/aj;->g()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->f(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;->b:Lcim;

    .line 101
    invoke-direct {p0, v1}, Lcom/twitter/android/moments/ui/maker/aj;->a(Lcim;)Lcqw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 102
    return-void
.end method

.method public am_()V
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj;->l:Lwy;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj;->l:Lwy;

    invoke-virtual {v0}, Lwy;->d()V

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj;->m:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 191
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj;->n:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 192
    invoke-super {p0}, Laog;->am_()V

    .line 193
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj;->k:Lcom/twitter/android/moments/ui/maker/ak;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj;->k:Lcom/twitter/android/moments/ui/maker/ak;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/ak;->e()V

    .line 201
    :goto_0
    return-void

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj;->g:Lakp;

    invoke-virtual {v0}, Lakp;->a()V

    goto :goto_0
.end method
