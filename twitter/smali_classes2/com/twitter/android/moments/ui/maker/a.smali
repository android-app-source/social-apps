.class public Lcom/twitter/android/moments/ui/maker/a;
.super Lakt;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lakt",
        "<",
        "Lcom/twitter/android/moments/ui/maker/MomentMakerAddTweetsActivity$a;",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Long;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final c:J

.field private final d:Landroid/content/res/Resources;

.field private final e:Lwy;

.field private final f:Lyg;

.field private final g:Lcom/twitter/util/android/g;


# direct methods
.method public constructor <init>(Landroid/app/Activity;JILwy;Lyg;Lcom/twitter/util/android/g;Landroid/content/res/Resources;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Landroid/app/Activity;",
            ":",
            "Lcom/twitter/app/common/util/j;",
            ">(TC;JI",
            "Lwy;",
            "Lyg;",
            "Lcom/twitter/util/android/g;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .prologue
    .line 43
    const-class v0, Lcom/twitter/android/moments/ui/maker/MomentMakerAddTweetsActivity;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/ay;

    invoke-direct {v1}, Lcom/twitter/android/moments/ui/maker/ay;-><init>()V

    invoke-direct {p0, p1, v0, p4, v1}, Lakt;-><init>(Landroid/app/Activity;Ljava/lang/Class;ILaku;)V

    .line 45
    iput-object p8, p0, Lcom/twitter/android/moments/ui/maker/a;->d:Landroid/content/res/Resources;

    .line 46
    iput-wide p2, p0, Lcom/twitter/android/moments/ui/maker/a;->c:J

    .line 47
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/a;->e:Lwy;

    .line 48
    iput-object p6, p0, Lcom/twitter/android/moments/ui/maker/a;->f:Lyg;

    .line 49
    iput-object p7, p0, Lcom/twitter/android/moments/ui/maker/a;->g:Lcom/twitter/util/android/g;

    .line 50
    new-instance v0, Lcom/twitter/android/moments/ui/maker/a$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/maker/a$1;-><init>(Lcom/twitter/android/moments/ui/maker/a;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/moments/ui/maker/a;->a(Lakv;)V

    .line 56
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/a;Ljava/util/List;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/moments/ui/maker/a;->a(Ljava/util/List;Ljava/util/Map;)V

    return-void
.end method

.method private a(Ljava/util/List;Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 88
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 89
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/moments/viewmodels/g;

    .line 90
    new-instance v3, Lces$b;

    invoke-direct {v3}, Lces$b;-><init>()V

    .line 91
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lces$b;->a(J)Lcfl$a;

    move-result-object v0

    check-cast v0, Lces$b;

    .line 92
    invoke-static {}, Lcfi;->a()Lcfi;

    move-result-object v3

    invoke-virtual {v0, v3}, Lces$b;->a(Lcfi;)Lcfg$a;

    move-result-object v0

    check-cast v0, Lces$b;

    .line 93
    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/a;->e:Lwy;

    invoke-virtual {v0}, Lces$b;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfd;

    invoke-virtual {v3, v0, v1}, Lwy;->a(Lcfd;Lcom/twitter/model/moments/viewmodels/g;)V

    goto :goto_0

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/a;->e:Lwy;

    .line 96
    invoke-virtual {v0}, Lwy;->c()Lrx/g;

    move-result-object v0

    new-instance v1, Lcqw;

    invoke-direct {v1}, Lcqw;-><init>()V

    .line 97
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    .line 98
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/a;->g:Lcom/twitter/util/android/g;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/a;->d:Landroid/content/res/Resources;

    const v2, 0x7f0c0013

    .line 99
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    .line 98
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Lcom/twitter/util/android/g;->a(Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 100
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 63
    new-instance v0, Lcom/twitter/android/moments/ui/maker/MomentMakerAddTweetsActivity$a;

    iget-wide v2, p0, Lcom/twitter/android/moments/ui/maker/a;->c:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/android/moments/ui/maker/MomentMakerAddTweetsActivity$a;-><init>(J)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/moments/ui/maker/a;->c(Lako;)V

    .line 64
    return-void
.end method

.method a(Ljava/util/List;)V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 84
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/a;->f:Lyg;

    invoke-interface {v0, p1}, Lyg;->a(Ljava/util/Collection;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/a$2;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/maker/a$2;-><init>(Lcom/twitter/android/moments/ui/maker/a;Ljava/util/List;)V

    .line 78
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/functions/b;)Lrx/j;

    goto :goto_0
.end method
