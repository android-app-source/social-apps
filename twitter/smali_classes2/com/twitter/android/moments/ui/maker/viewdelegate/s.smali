.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/s;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/view/View;

.field private final c:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

.field private final d:Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;Landroid/view/View;Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/s;->a:Landroid/view/View;

    .line 42
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/s;->b:Landroid/view/View;

    .line 43
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/s;->c:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    .line 44
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/s;->d:Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;

    .line 45
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/s;
    .locals 5

    .prologue
    .line 26
    const v0, 0x7f0401d6

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 27
    const v0, 0x7f1304ea

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 28
    const v0, 0x7f1304d2

    .line 30
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 29
    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;->a(Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;

    move-result-object v3

    .line 31
    const v0, 0x7f130010

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 33
    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->a(Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    move-result-object v0

    .line 34
    new-instance v4, Lcom/twitter/android/moments/ui/maker/viewdelegate/s;

    invoke-direct {v4, v1, v3, v2, v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/s;-><init>(Landroid/view/View;Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;Landroid/view/View;Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;)V

    return-object v4
.end method


# virtual methods
.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/s;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/s;->a:Landroid/view/View;

    return-object v0
.end method

.method public b()Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/s;->d:Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;

    return-object v0
.end method

.method public b(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/s;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    return-void
.end method

.method public c()Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/s;->c:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/s;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 72
    return-void
.end method
