.class Lcom/twitter/android/moments/ui/maker/t;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Landroid/view/animation/Interpolator;


# instance fields
.field private final b:Landroid/view/ViewGroup;

.field private final c:Landroid/view/View;

.field private final d:Landroid/view/View;

.field private final e:Landroid/animation/ObjectAnimator;

.field private final f:Landroid/animation/ObjectAnimator;

.field private g:I

.field private final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/twitter/android/moments/ui/maker/t;->a:Landroid/view/animation/Interpolator;

    return-void
.end method

.method constructor <init>(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;I)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/moments/ui/maker/t;->g:I

    .line 46
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/t;->b:Landroid/view/ViewGroup;

    .line 47
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/t;->c:Landroid/view/View;

    .line 48
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/t;->d:Landroid/view/View;

    .line 49
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/t;->e:Landroid/animation/ObjectAnimator;

    .line 50
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/t;->f:Landroid/animation/ObjectAnimator;

    .line 51
    iput p6, p0, Lcom/twitter/android/moments/ui/maker/t;->h:I

    .line 52
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/t;->a()V

    .line 53
    return-void
.end method

.method static a(Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/t;
    .locals 7

    .prologue
    .line 35
    const v0, 0x7f130480

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 36
    const v0, 0x7f130481

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 37
    const v0, 0x7f130482

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 38
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0f0032

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    .line 39
    new-instance v0, Lcom/twitter/android/moments/ui/maker/t;

    new-instance v4, Landroid/animation/ObjectAnimator;

    invoke-direct {v4}, Landroid/animation/ObjectAnimator;-><init>()V

    new-instance v5, Landroid/animation/ObjectAnimator;

    invoke-direct {v5}, Landroid/animation/ObjectAnimator;-><init>()V

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/moments/ui/maker/t;-><init>(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;I)V

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/t;->e:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/t;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 57
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/t;->e:Landroid/animation/ObjectAnimator;

    sget-object v1, Landroid/view/View;->ROTATION:Landroid/util/Property;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setProperty(Landroid/util/Property;)V

    .line 58
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/t;->e:Landroid/animation/ObjectAnimator;

    sget-object v1, Lcom/twitter/android/moments/ui/maker/t;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 60
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/t;->f:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/t;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 61
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/t;->f:Landroid/animation/ObjectAnimator;

    sget-object v1, Landroid/view/View;->ROTATION:Landroid/util/Property;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setProperty(Landroid/util/Property;)V

    .line 62
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/t;->f:Landroid/animation/ObjectAnimator;

    sget-object v1, Lcom/twitter/android/moments/ui/maker/t;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 63
    return-void
.end method

.method private a(FI)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 88
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/t;->e:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 89
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/t;->e:Landroid/animation/ObjectAnimator;

    new-array v1, v5, [F

    aput p1, v1, v4

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 90
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/t;->e:Landroid/animation/ObjectAnimator;

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 91
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/t;->e:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 93
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/t;->f:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 94
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/t;->f:Landroid/animation/ObjectAnimator;

    new-array v1, v5, [F

    aput p1, v1, v4

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 95
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/t;->f:Landroid/animation/ObjectAnimator;

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 96
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/t;->f:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 97
    return-void
.end method


# virtual methods
.method a(I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 70
    iget v0, p0, Lcom/twitter/android/moments/ui/maker/t;->g:I

    const/4 v3, -0x1

    if-eq v0, v3, :cond_2

    move v0, v1

    .line 71
    :goto_0
    iget v3, p0, Lcom/twitter/android/moments/ui/maker/t;->g:I

    if-eq p1, v3, :cond_1

    .line 72
    iput p1, p0, Lcom/twitter/android/moments/ui/maker/t;->g:I

    .line 73
    if-ne p1, v1, :cond_3

    const/4 v1, 0x0

    .line 74
    :goto_1
    if-eqz v0, :cond_0

    iget v2, p0, Lcom/twitter/android/moments/ui/maker/t;->h:I

    .line 75
    :cond_0
    invoke-direct {p0, v1, v2}, Lcom/twitter/android/moments/ui/maker/t;->a(FI)V

    .line 77
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 70
    goto :goto_0

    .line 73
    :cond_3
    const/high16 v1, 0x43340000    # 180.0f

    goto :goto_1
.end method

.method a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/t;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    return-void
.end method

.method a(Z)V
    .locals 3

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/t;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setEnabled(Z)V

    .line 81
    if-eqz p1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    .line 82
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/t;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 83
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/t;->c:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 84
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/t;->d:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 85
    return-void

    .line 81
    :cond_0
    const/high16 v0, 0x3f000000    # 0.5f

    goto :goto_0
.end method
