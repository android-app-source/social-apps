.class public final enum Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;
.super Ljava/lang/Enum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AnimationStateType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;

.field public static final enum b:Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;

.field private static final synthetic c:[Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;

    const-string/jumbo v1, "START"

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;

    .line 25
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;

    const-string/jumbo v1, "FINISH"

    invoke-direct {v0, v1, v3}, Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;

    .line 17
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;

    sget-object v1, Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;->c:[Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;

    return-object v0
.end method

.method public static values()[Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;->c:[Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;

    invoke-virtual {v0}, [Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;

    return-object v0
.end method
