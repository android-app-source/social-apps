.class public Lcom/twitter/android/moments/ui/maker/navigation/o;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/object/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/object/g",
        "<",
        "Lcom/twitter/android/moments/ui/maker/navigation/ah;",
        "Lcom/twitter/android/moments/viewmodels/k;",
        "Lcom/twitter/model/moments/Moment;",
        "Lcom/twitter/android/moments/ui/maker/navigation/m;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lwy;

.field private final c:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lwy;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/navigation/o;->a:Landroid/app/Activity;

    .line 23
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/navigation/o;->c:Landroid/view/ViewGroup;

    .line 24
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/navigation/o;->b:Lwy;

    .line 25
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/viewmodels/k;Lcom/twitter/model/moments/Moment;)Lcom/twitter/android/moments/ui/maker/navigation/m;
    .locals 7

    .prologue
    .line 31
    new-instance v6, Lcom/twitter/android/moments/ui/maker/navigation/m;

    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/o;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/navigation/o;->c:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/navigation/o;->b:Lwy;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lacx;->a(Landroid/app/Activity;Landroid/view/ViewGroup;Lwy;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/viewmodels/k;Lcom/twitter/model/moments/Moment;)Lacx;

    move-result-object v0

    invoke-direct {v6, v0}, Lcom/twitter/android/moments/ui/maker/navigation/m;-><init>(Lacx;)V

    return-object v6
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    check-cast p1, Lcom/twitter/android/moments/ui/maker/navigation/ah;

    check-cast p2, Lcom/twitter/android/moments/viewmodels/k;

    check-cast p3, Lcom/twitter/model/moments/Moment;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/moments/ui/maker/navigation/o;->a(Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/viewmodels/k;Lcom/twitter/model/moments/Moment;)Lcom/twitter/android/moments/ui/maker/navigation/m;

    move-result-object v0

    return-object v0
.end method
