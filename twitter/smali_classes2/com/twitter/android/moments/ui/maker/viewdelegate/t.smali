.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/t;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Landroid/view/View;

.field private final c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/widget/TextView;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/t;->a:Landroid/widget/TextView;

    .line 28
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/t;->b:Landroid/view/View;

    .line 29
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/t;->c:Landroid/view/View;

    .line 30
    return-void
.end method

.method public static a(Landroid/view/View;)Lcom/twitter/android/moments/ui/maker/viewdelegate/t;
    .locals 4

    .prologue
    .line 19
    const v0, 0x7f13029f

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 20
    const v1, 0x7f1304ac

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 21
    const v2, 0x7f1304d6

    invoke-virtual {p0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 22
    new-instance v3, Lcom/twitter/android/moments/ui/maker/viewdelegate/t;

    invoke-direct {v3, v0, v1, v2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/t;-><init>(Landroid/widget/TextView;Landroid/view/View;Landroid/view/View;)V

    return-object v3
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/t;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 34
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/t;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 35
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/t;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 36
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/t;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/t;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 40
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/t;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 41
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/t;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 42
    return-void
.end method
