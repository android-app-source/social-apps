.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/e;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/view/View;

.field private final c:Landroid/view/View;

.field private final d:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

.field private final e:Landroid/view/View;

.field private final f:Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;Landroid/view/View;Landroid/view/View;Landroid/view/View;Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->a:Landroid/view/View;

    .line 44
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->b:Landroid/view/View;

    .line 45
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->c:Landroid/view/View;

    .line 46
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->e:Landroid/view/View;

    .line 47
    iput-object p6, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->d:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    .line 48
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->f:Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;

    .line 49
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/e;
    .locals 7

    .prologue
    .line 26
    const v0, 0x7f0401cb

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 27
    const v0, 0x7f1304db

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 28
    const v0, 0x7f1304dc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 29
    const v0, 0x7f1304dd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 30
    const v0, 0x7f1304d2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 31
    const v2, 0x7f130010

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v6, v2

    check-cast v6, Landroid/view/ViewGroup;

    .line 33
    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;->a(Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;

    move-result-object v2

    .line 35
    invoke-static {v6}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;->b(Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    move-result-object v6

    .line 36
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;-><init>(Landroid/view/View;Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;Landroid/view/View;Landroid/view/View;Landroid/view/View;Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;)V

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->a:Landroid/view/View;

    return-object v0
.end method

.method public b()Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->d:Lcom/twitter/android/moments/ui/maker/viewdelegate/ac;

    return-object v0
.end method

.method public b(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->e:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    return-void
.end method

.method public c()Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->f:Lcom/twitter/android/moments/ui/maker/viewdelegate/ah;

    return-object v0
.end method

.method public c(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->c:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 77
    return-void
.end method

.method public d(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->a:Landroid/view/View;

    invoke-static {v0}, Lcrf;->d(Landroid/view/View;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/viewdelegate/e$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e$1;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/e;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 106
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 81
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->b:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 85
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 93
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 97
    return-void
.end method
