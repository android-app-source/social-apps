.class public Lcom/twitter/android/moments/ui/maker/k;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/ao;

.field private final b:Lcom/twitter/android/moments/ui/maker/bc;

.field private final c:Lcom/twitter/model/moments/viewmodels/MomentPage;


# direct methods
.method public constructor <init>(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/android/moments/ui/maker/ao;Lcom/twitter/android/moments/ui/maker/bc;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/k;->c:Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 26
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/k;->a:Lcom/twitter/android/moments/ui/maker/ao;

    .line 27
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/k;->b:Lcom/twitter/android/moments/ui/maker/bc;

    .line 28
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/k;)Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/k;->c:Lcom/twitter/model/moments/viewmodels/MomentPage;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/maker/k;)Lcom/twitter/android/moments/ui/maker/bc;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/k;->b:Lcom/twitter/android/moments/ui/maker/bc;

    return-object v0
.end method

.method private c()Lrx/functions/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/b",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Lcom/twitter/android/moments/ui/maker/k$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/maker/k$1;-><init>(Lcom/twitter/android/moments/ui/maker/k;)V

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/k;->b:Lcom/twitter/android/moments/ui/maker/bc;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/k;->c:Lcom/twitter/model/moments/viewmodels/MomentPage;

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/bc;->a(Lcom/twitter/model/moments/r;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/k;->b:Lcom/twitter/android/moments/ui/maker/bc;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/k;->c:Lcom/twitter/model/moments/viewmodels/MomentPage;

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/bc;->a(Lcom/twitter/model/moments/r;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 37
    if-eqz v0, :cond_0

    .line 38
    invoke-static {v0}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    .line 40
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/k;->a:Lcom/twitter/android/moments/ui/maker/ao;

    .line 41
    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/ao;->a()Lrx/g;

    move-result-object v0

    .line 42
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/k;->c()Lrx/functions/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/b;)Lrx/g;

    move-result-object v0

    goto :goto_0
.end method
