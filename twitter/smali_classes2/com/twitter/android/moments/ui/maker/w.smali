.class public Lcom/twitter/android/moments/ui/maker/w;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/maker/w$a;,
        Lcom/twitter/android/moments/ui/maker/w$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/w$b;

.field private final b:Lcom/twitter/android/moments/ui/maker/w$a;

.field private final c:Lxa;

.field private final d:Lakp;

.field private final e:Lrx/f;

.field private final f:Lzv;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/w$b;Lcom/twitter/android/moments/ui/maker/w$a;Lxa;Lakp;Lrx/f;Lzv;)V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/w;->a:Lcom/twitter/android/moments/ui/maker/w$b;

    .line 54
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/w;->b:Lcom/twitter/android/moments/ui/maker/w$a;

    .line 55
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/w;->c:Lxa;

    .line 56
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/w;->d:Lakp;

    .line 57
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/w;->e:Lrx/f;

    .line 58
    iput-object p6, p0, Lcom/twitter/android/moments/ui/maker/w;->f:Lzv;

    .line 59
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/w;->a:Lcom/twitter/android/moments/ui/maker/w$b;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/w$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/w$1;-><init>(Lcom/twitter/android/moments/ui/maker/w;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/w$b;->a(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 66
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/w;->b:Lcom/twitter/android/moments/ui/maker/w$a;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/w$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/w$2;-><init>(Lcom/twitter/android/moments/ui/maker/w;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/w$a;->a(Landroid/content/DialogInterface$OnClickListener;)V

    .line 74
    return-void
.end method

.method public static a(Landroid/preference/PreferenceActivity;Lxa;J)Lcom/twitter/android/moments/ui/maker/w;
    .locals 8

    .prologue
    .line 39
    const-string/jumbo v0, "pref_delete_moment"

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 40
    new-instance v1, Lcom/twitter/android/moments/ui/maker/w$b;

    invoke-direct {v1, v0}, Lcom/twitter/android/moments/ui/maker/w$b;-><init>(Landroid/preference/Preference;)V

    .line 42
    new-instance v2, Lcom/twitter/android/moments/ui/maker/w$a;

    const/4 v0, 0x0

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/moments/ui/maker/w$a;-><init>(Landroid/content/Context;Lcom/twitter/android/moments/ui/maker/w$1;)V

    .line 44
    invoke-static {p2, p3}, Lzv;->a(J)Lzv;

    move-result-object v6

    .line 45
    new-instance v0, Lcom/twitter/android/moments/ui/maker/w;

    new-instance v4, Lakp;

    invoke-direct {v4, p0}, Lakp;-><init>(Landroid/app/Activity;)V

    .line 46
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v5

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/moments/ui/maker/w;-><init>(Lcom/twitter/android/moments/ui/maker/w$b;Lcom/twitter/android/moments/ui/maker/w$a;Lxa;Lakp;Lrx/f;Lzv;)V

    .line 45
    return-object v0
.end method

.method private a()V
    .locals 5

    .prologue
    .line 77
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/w;->f:Lzv;

    invoke-virtual {v0}, Lzv;->e()V

    .line 78
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/w;->c:Lxa;

    .line 79
    invoke-virtual {v0}, Lxa;->a()Lrx/c;

    move-result-object v0

    const-wide/16 v2, 0x190

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/maker/w;->e:Lrx/f;

    .line 80
    invoke-virtual {v0, v2, v3, v1, v4}, Lrx/c;->c(JLjava/util/concurrent/TimeUnit;Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/w$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/w$3;-><init>(Lcom/twitter/android/moments/ui/maker/w;)V

    .line 81
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 89
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/w;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/w;->b()V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/w;->b:Lcom/twitter/android/moments/ui/maker/w$a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/w$a;->a()V

    .line 93
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/maker/w;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/w;->a()V

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/maker/w;)Lakp;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/w;->d:Lakp;

    return-object v0
.end method
