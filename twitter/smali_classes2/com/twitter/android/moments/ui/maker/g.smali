.class public Lcom/twitter/android/moments/ui/maker/g;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/viewdelegate/d;

.field private final b:Lcom/twitter/android/moments/ui/maker/f;

.field private final c:Lzs;

.field private final d:Lakq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lakq",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/twitter/android/moments/data/af;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/moments/data/af",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lrx/j;


# direct methods
.method constructor <init>(Landroid/content/res/Resources;Lakq;Lcom/twitter/android/moments/ui/maker/viewdelegate/d;Lcom/twitter/android/moments/ui/maker/f;Lcom/twitter/android/moments/data/af;Lzs;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Lakq",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;>;",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/d;",
            "Lcom/twitter/android/moments/ui/maker/f;",
            "Lcom/twitter/android/moments/data/af",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;",
            "Lzs;",
            ")V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/g;->d:Lakq;

    .line 49
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/g;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/d;

    .line 50
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/g;->b:Lcom/twitter/android/moments/ui/maker/f;

    .line 51
    iput-object p6, p0, Lcom/twitter/android/moments/ui/maker/g;->c:Lzs;

    .line 52
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/g;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/d;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/g;->b:Lcom/twitter/android/moments/ui/maker/f;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->a(Landroid/support/v4/view/PagerAdapter;)V

    .line 53
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/g;->e:Lcom/twitter/android/moments/data/af;

    .line 54
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/g;->e()V

    .line 55
    invoke-virtual {p5}, Lcom/twitter/android/moments/data/af;->a()Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/g$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/g$2;-><init>(Lcom/twitter/android/moments/ui/maker/g;)V

    .line 56
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lrx/c;->j()Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/g$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/maker/g$1;-><init>(Lcom/twitter/android/moments/ui/maker/g;Landroid/content/res/Resources;)V

    .line 64
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/g;->f:Lrx/j;

    .line 77
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/g;->c:Lzs;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/g;->b:Lcom/twitter/android/moments/ui/maker/f;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/twitter/android/moments/ui/maker/f;->a(I)Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lzs;->a(Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;)V

    .line 78
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/g;->f()V

    .line 79
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/g;)Lcom/twitter/android/moments/data/af;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/g;->e:Lcom/twitter/android/moments/data/af;

    return-object v0
.end method

.method public static a(Lcom/twitter/app/common/base/TwitterFragmentActivity;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/maker/ap$a;Lcom/twitter/android/moments/data/af;Lanh;Lya;JLxp;)Lcom/twitter/android/moments/ui/maker/g;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/app/common/base/TwitterFragmentActivity;",
            "Landroid/view/ViewGroup;",
            "Lcom/twitter/android/moments/ui/maker/ap$a;",
            "Lcom/twitter/android/moments/data/af",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;",
            "Lanh;",
            "Lya;",
            "J",
            "Lxp;",
            ")",
            "Lcom/twitter/android/moments/ui/maker/g;"
        }
    .end annotation

    .prologue
    .line 96
    new-instance v3, Lcom/twitter/android/moments/ui/maker/h;

    .line 98
    invoke-static {p0}, Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$b;->a(Lcom/twitter/app/common/base/TwitterFragmentActivity;)Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$b;

    move-result-object v10

    move-object v4, p0

    move-object v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move-object/from16 v11, p8

    move-wide/from16 v12, p6

    invoke-direct/range {v3 .. v13}, Lcom/twitter/android/moments/ui/maker/h;-><init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/maker/ap$a;Lcom/twitter/android/moments/data/af;Lanh;Lya;Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$b;Lxm;J)V

    .line 100
    new-instance v2, Lcom/twitter/android/moments/ui/maker/b;

    .line 101
    invoke-static {}, Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;->values()[Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;

    move-result-object v4

    invoke-static {v4}, Lcom/twitter/util/collection/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    .line 102
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Lcom/twitter/android/moments/ui/maker/b;-><init>(Ljava/util/List;Lcom/twitter/model/core/TwitterUser;)V

    .line 103
    new-instance v6, Lcom/twitter/android/moments/ui/maker/f;

    .line 104
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/maker/b;->a()Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, p8

    invoke-direct {v6, v4, v2, v3, v0}, Lcom/twitter/android/moments/ui/maker/f;-><init>(Landroid/content/res/Resources;Ljava/util/List;Lcom/twitter/android/moments/ui/maker/h;Lxp;)V

    .line 107
    invoke-static {p0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/d;

    move-result-object v5

    .line 108
    new-instance v4, Lakq;

    new-instance v2, Lcom/twitter/android/moments/ui/maker/ay;

    invoke-direct {v2}, Lcom/twitter/android/moments/ui/maker/ay;-><init>()V

    invoke-direct {v4, p0, v2}, Lakq;-><init>(Landroid/app/Activity;Lakw;)V

    .line 111
    invoke-static/range {p6 .. p7}, Lzs;->a(J)Lzs;

    move-result-object v8

    .line 112
    new-instance v2, Lcom/twitter/android/moments/ui/maker/g;

    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    move-object/from16 v7, p3

    invoke-direct/range {v2 .. v8}, Lcom/twitter/android/moments/ui/maker/g;-><init>(Landroid/content/res/Resources;Lakq;Lcom/twitter/android/moments/ui/maker/viewdelegate/d;Lcom/twitter/android/moments/ui/maker/f;Lcom/twitter/android/moments/data/af;Lzs;)V

    return-object v2
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/maker/g;)Lcom/twitter/android/moments/ui/maker/viewdelegate/d;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/g;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/d;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/maker/g;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/g;->d()V

    return-void
.end method

.method static synthetic d(Lcom/twitter/android/moments/ui/maker/g;)Lcom/twitter/android/moments/ui/maker/f;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/g;->b:Lcom/twitter/android/moments/ui/maker/f;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 137
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/g;->c:Lzs;

    invoke-virtual {v0}, Lzs;->a()V

    .line 138
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/g;->d:Lakq;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/g;->e:Lcom/twitter/android/moments/data/af;

    .line 140
    invoke-virtual {v2}, Lcom/twitter/android/moments/data/af;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v2

    .line 138
    invoke-virtual {v0, v1, v2}, Lakq;->a(ILjava/lang/Object;)V

    .line 141
    return-void
.end method

.method static synthetic e(Lcom/twitter/android/moments/ui/maker/g;)Lzs;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/g;->c:Lzs;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/g;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/d;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/g$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/g$3;-><init>(Lcom/twitter/android/moments/ui/maker/g;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->a(Landroid/view/View$OnClickListener;)V

    .line 150
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/g;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/d;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/g$4;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/g$4;-><init>(Lcom/twitter/android/moments/ui/maker/g;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->b(Landroid/view/View$OnClickListener;)V

    .line 156
    return-void
.end method

.method static synthetic f(Lcom/twitter/android/moments/ui/maker/g;)Lakq;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/g;->d:Lakq;

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/g;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/d;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/g$5;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/g$5;-><init>(Lcom/twitter/android/moments/ui/maker/g;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->a(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 173
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 176
    new-instance v0, Lcom/twitter/android/moments/ui/maker/g$6;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/maker/g$6;-><init>(Lcom/twitter/android/moments/ui/maker/g;)V

    .line 190
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/g;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/d;

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->a(Landroid/content/DialogInterface$OnClickListener;)V

    .line 191
    return-void
.end method


# virtual methods
.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/g;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/d;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->aN_()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 121
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/g;->e:Lcom/twitter/android/moments/data/af;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/af;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 122
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/g;->g()V

    .line 126
    :goto_0
    return-void

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/g;->d:Lakq;

    const/4 v1, 0x0

    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lakq;->a(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/g;->f:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 130
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/g;->b:Lcom/twitter/android/moments/ui/maker/f;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/f;->a()V

    .line 131
    return-void
.end method
