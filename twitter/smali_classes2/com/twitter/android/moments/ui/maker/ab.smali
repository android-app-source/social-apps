.class public Lcom/twitter/android/moments/ui/maker/ab;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lajo;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lajo",
        "<",
        "Lcom/twitter/model/moments/viewmodels/g;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/viewdelegate/s;

.field private final b:Lcom/twitter/android/moments/ui/maker/ax;

.field private final c:Lcom/twitter/android/moments/ui/maker/navigation/ah;

.field private final d:Lwy;

.field private final e:Lacz;

.field private final f:Lcom/twitter/android/moments/ui/maker/be;

.field private final g:Lzx;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/s;Lcom/twitter/android/moments/ui/maker/be;Lcom/twitter/android/moments/ui/maker/ax;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lwy;Lacz;Lzx;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/ab;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/s;

    .line 40
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/ab;->b:Lcom/twitter/android/moments/ui/maker/ax;

    .line 41
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/ab;->c:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    .line 42
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/ab;->d:Lwy;

    .line 43
    iput-object p6, p0, Lcom/twitter/android/moments/ui/maker/ab;->e:Lacz;

    .line 44
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/ab;->f:Lcom/twitter/android/moments/ui/maker/be;

    .line 45
    iput-object p7, p0, Lcom/twitter/android/moments/ui/maker/ab;->g:Lzx;

    .line 46
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/ab;)Lwy;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ab;->d:Lwy;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/maker/ab;)Lzx;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ab;->g:Lzx;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/maker/ab;)Lcom/twitter/android/moments/ui/maker/ax;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ab;->b:Lcom/twitter/android/moments/ui/maker/ax;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/moments/ui/maker/ab;)Lcom/twitter/android/moments/ui/maker/navigation/ah;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ab;->c:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/viewmodels/g;)V
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ab;->f:Lcom/twitter/android/moments/ui/maker/be;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/maker/be;->a(Lcom/twitter/model/moments/viewmodels/g;)V

    .line 57
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ab;->e:Lacz;

    invoke-virtual {v0, p1}, Lacz;->a(Lcom/twitter/model/moments/viewmodels/g;)V

    .line 58
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/g;->a()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 59
    instance-of v1, v0, Lcom/twitter/model/moments/viewmodels/n;

    if-eqz v1, :cond_0

    .line 60
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/ab;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/s;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/s;->d()V

    .line 61
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/ab;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/s;

    new-instance v2, Lcom/twitter/android/moments/ui/maker/ab$1;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/moments/ui/maker/ab$1;-><init>(Lcom/twitter/android/moments/ui/maker/ab;Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/s;->b(Landroid/view/View$OnClickListener;)V

    .line 74
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/ab;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/s;

    new-instance v2, Lcom/twitter/android/moments/ui/maker/ab$2;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/moments/ui/maker/ab$2;-><init>(Lcom/twitter/android/moments/ui/maker/ab;Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/s;->a(Landroid/view/View$OnClickListener;)V

    .line 81
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lcom/twitter/model/moments/viewmodels/g;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/ab;->a(Lcom/twitter/model/moments/viewmodels/g;)V

    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ab;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/s;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/s;->aN_()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ab;->f:Lcom/twitter/android/moments/ui/maker/be;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/be;->c()V

    .line 89
    return-void
.end method
