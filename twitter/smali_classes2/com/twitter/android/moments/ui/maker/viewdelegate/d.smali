.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/support/v4/view/ViewPager;

.field private final c:Lcom/twitter/android/moments/ui/maker/viewdelegate/ai;

.field private final d:Landroid/view/View;

.field private final e:Landroid/widget/ImageView;

.field private final f:Landroid/app/AlertDialog$Builder;

.field private final g:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/twitter/android/moments/ui/maker/viewdelegate/ai;Landroid/support/v4/view/ViewPager;Landroid/view/View;Landroid/widget/ImageView;Landroid/app/AlertDialog$Builder;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->a:Landroid/view/View;

    .line 48
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->c:Lcom/twitter/android/moments/ui/maker/viewdelegate/ai;

    .line 49
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->b:Landroid/support/v4/view/ViewPager;

    .line 50
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->d:Landroid/view/View;

    .line 51
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->e:Landroid/widget/ImageView;

    .line 52
    iput-object p7, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->g:Landroid/widget/TextView;

    .line 53
    iput-object p6, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->f:Landroid/app/AlertDialog$Builder;

    .line 54
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/d;
    .locals 8

    .prologue
    .line 33
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401c1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 34
    new-instance v2, Lcom/twitter/android/moments/ui/maker/viewdelegate/ai;

    const v0, 0x7f13037a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout;

    invoke-direct {v2, v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ai;-><init>(Landroid/support/design/widget/TabLayout;)V

    .line 35
    const v0, 0x7f1304c7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v4/view/ViewPager;

    .line 36
    const v0, 0x7f130164

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 37
    const v0, 0x7f13035b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 38
    const v0, 0x7f1304c8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 39
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;

    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;-><init>(Landroid/view/View;Lcom/twitter/android/moments/ui/maker/viewdelegate/ai;Landroid/support/v4/view/ViewPager;Landroid/view/View;Landroid/widget/ImageView;Landroid/app/AlertDialog$Builder;Landroid/widget/TextView;)V

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface$OnClickListener;)V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->f:Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0a00f7

    .line 81
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0278

    .line 82
    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00f6

    .line 83
    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 84
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 86
    return-void
.end method

.method public a(Landroid/support/v4/view/PagerAdapter;)V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 64
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->c:Lcom/twitter/android/moments/ui/maker/viewdelegate/ai;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ai;->a(Landroid/support/v4/view/ViewPager;)V

    .line 65
    return-void
.end method

.method public a(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 69
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->d:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->a:Landroid/view/View;

    return-object v0
.end method

.method public b(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    return-void
.end method
