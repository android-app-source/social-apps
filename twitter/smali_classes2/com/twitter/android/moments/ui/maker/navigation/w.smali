.class public Lcom/twitter/android/moments/ui/maker/navigation/w;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/maker/navigation/ab;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/android/moments/ui/maker/navigation/ab",
        "<",
        "Lcom/twitter/model/moments/r;",
        "Lcom/twitter/android/moments/ui/maker/viewdelegate/u;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/ad;

.field private final b:Lcom/twitter/android/moments/ui/maker/viewdelegate/u;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/ad;Lcom/twitter/android/moments/ui/maker/viewdelegate/u;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/navigation/w;->a:Lcom/twitter/android/moments/ui/maker/ad;

    .line 19
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/navigation/w;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/u;

    .line 20
    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/w;->a:Lcom/twitter/android/moments/ui/maker/ad;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/ad;->b()V

    .line 26
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/w;->a:Lcom/twitter/android/moments/ui/maker/ad;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/ad;->aN_()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/w;->a:Lcom/twitter/android/moments/ui/maker/ad;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/ad;->c()V

    .line 32
    return-void
.end method

.method public c()Lcom/twitter/android/moments/ui/maker/navigation/am;
    .locals 2

    .prologue
    .line 37
    new-instance v0, Lcom/twitter/android/moments/ui/maker/navigation/z;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/navigation/w;->a:Lcom/twitter/android/moments/ui/maker/ad;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/ad;->d()Lcom/twitter/model/moments/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/moments/ui/maker/navigation/z;-><init>(Lcom/twitter/model/moments/r;)V

    return-object v0
.end method

.method public synthetic d()Lcom/twitter/android/moments/ui/maker/navigation/an;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/navigation/w;->e()Lcom/twitter/android/moments/ui/maker/viewdelegate/u;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/twitter/android/moments/ui/maker/viewdelegate/u;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/w;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/u;

    return-object v0
.end method

.method public f()Lcom/twitter/model/moments/r;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/w;->a:Lcom/twitter/android/moments/ui/maker/ad;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/ad;->d()Lcom/twitter/model/moments/r;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/navigation/w;->f()Lcom/twitter/model/moments/r;

    move-result-object v0

    return-object v0
.end method
