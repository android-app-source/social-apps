.class public Lcom/twitter/android/moments/ui/maker/u;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/v;

.field private final b:Lcom/twitter/android/moments/ui/maker/at;

.field private final c:Lcom/twitter/android/moments/ui/maker/navigation/ah;

.field private final d:Lwy;

.field private final e:Lzv;

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/model/moments/r;",
            "Lcff;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/model/moments/r;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/twitter/model/moments/viewmodels/g;

.field private i:Lcom/twitter/model/moments/r;

.field private j:I

.field private k:Lrx/j;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/maker/v;Lcom/twitter/android/moments/ui/maker/at;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lwy;Lzv;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->f:Ljava/util/Map;

    .line 46
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->g:Ljava/util/Map;

    .line 70
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/u;->a:Lcom/twitter/android/moments/ui/maker/v;

    .line 71
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/u;->b:Lcom/twitter/android/moments/ui/maker/at;

    .line 72
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/u;->c:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    .line 73
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/u;->d:Lwy;

    .line 74
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/u;->e:Lzv;

    .line 75
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/u;I)I
    .locals 0

    .prologue
    .line 39
    iput p1, p0, Lcom/twitter/android/moments/ui/maker/u;->j:I

    return p1
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/u;)Lcom/twitter/android/moments/ui/maker/at;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->b:Lcom/twitter/android/moments/ui/maker/at;

    return-object v0
.end method

.method public static a(Landroid/app/Activity;Landroid/view/ViewGroup;Lwy;Lcom/twitter/android/moments/ui/maker/navigation/ah;J)Lcom/twitter/android/moments/ui/maker/u;
    .locals 6

    .prologue
    .line 56
    invoke-static {p0, p1}, Lcom/twitter/android/moments/ui/maker/v;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/v;

    move-result-object v1

    .line 57
    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/v;->b()Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;

    move-result-object v0

    .line 59
    invoke-static {p0, v0}, Lcom/twitter/android/moments/ui/maker/at;->a(Landroid/app/Activity;Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;)Lcom/twitter/android/moments/ui/maker/at;

    move-result-object v2

    .line 60
    invoke-static {p4, p5}, Lzv;->a(J)Lzv;

    move-result-object v5

    .line 61
    new-instance v0, Lcom/twitter/android/moments/ui/maker/u;

    move-object v3, p3

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/moments/ui/maker/u;-><init>(Lcom/twitter/android/moments/ui/maker/v;Lcom/twitter/android/moments/ui/maker/at;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lwy;Lzv;)V

    return-object v0
.end method

.method private a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->f:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcff;

    .line 157
    if-eqz v0, :cond_0

    instance-of v1, p1, Lcom/twitter/model/moments/viewmodels/c;

    if-eqz v1, :cond_0

    .line 158
    check-cast p1, Lcom/twitter/model/moments/viewmodels/c;

    .line 159
    iget-object v0, v0, Lcff;->b:Lcom/twitter/model/moments/e;

    .line 160
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/e;

    invoke-interface {p1, v0}, Lcom/twitter/model/moments/viewmodels/c;->a(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 159
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 162
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method private a(Lcom/twitter/model/moments/r;)V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->h:Lcom/twitter/model/moments/viewmodels/g;

    if-nez v0, :cond_0

    .line 101
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Presenter is not bound to a group of pages yet"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 115
    :goto_0
    return-void

    .line 104
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/u;->i:Lcom/twitter/model/moments/r;

    .line 105
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->h:Lcom/twitter/model/moments/viewmodels/g;

    invoke-virtual {v0, p1}, Lcom/twitter/model/moments/viewmodels/g;->a(Lcom/twitter/model/moments/r;)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 106
    if-eqz v0, :cond_1

    .line 107
    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/maker/u;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 108
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/u;->b:Lcom/twitter/android/moments/ui/maker/at;

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/maker/at;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    .line 109
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/u;->f()V

    .line 110
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/u;->e()V

    .line 111
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/u;->d()V

    goto :goto_0

    .line 113
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Moment page was not found or is not a tweet photo page"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/maker/u;)Lcom/twitter/android/moments/ui/maker/v;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->a:Lcom/twitter/android/moments/ui/maker/v;

    return-object v0
.end method

.method private b(Lcom/twitter/model/moments/r;)V
    .locals 3

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->b:Lcom/twitter/android/moments/ui/maker/at;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/at;->b()Lcom/twitter/model/moments/e;

    move-result-object v0

    .line 218
    if-eqz v0, :cond_0

    .line 219
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/u;->f:Ljava/util/Map;

    new-instance v2, Lcff;

    invoke-direct {v2, p1, v0}, Lcff;-><init>(Lcom/twitter/model/moments/r;Lcom/twitter/model/moments/e;)V

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->g:Ljava/util/Map;

    iget v1, p0, Lcom/twitter/android/moments/ui/maker/u;->j:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    :cond_0
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->b:Lcom/twitter/android/moments/ui/maker/at;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/at;->e()Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/u$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/u$1;-><init>(Lcom/twitter/android/moments/ui/maker/u;)V

    .line 119
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->k:Lrx/j;

    .line 127
    return-void
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/maker/u;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/u;->h()V

    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->h:Lcom/twitter/model/moments/viewmodels/g;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/g;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/u;->i:Lcom/twitter/model/moments/r;

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/viewmodels/g;->b(Lcom/twitter/model/moments/r;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->a:Lcom/twitter/android/moments/ui/maker/v;

    const v1, 0x7f0a00f6

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/v;->b(I)V

    .line 135
    :goto_0
    return-void

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->a:Lcom/twitter/android/moments/ui/maker/v;

    const v1, 0x7f0a00a0

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/v;->b(I)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/twitter/android/moments/ui/maker/u;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/u;->i()V

    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->h:Lcom/twitter/model/moments/viewmodels/g;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/g;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/u;->i:Lcom/twitter/model/moments/r;

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/viewmodels/g;->c(Lcom/twitter/model/moments/r;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->a:Lcom/twitter/android/moments/ui/maker/v;

    const v1, 0x7f0a031c

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/v;->a(I)V

    .line 143
    :goto_0
    return-void

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->a:Lcom/twitter/android/moments/ui/maker/v;

    const v1, 0x7f0a05dd

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/v;->a(I)V

    goto :goto_0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 146
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->h:Lcom/twitter/model/moments/viewmodels/g;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/g;

    iget-object v0, v0, Lcom/twitter/model/moments/viewmodels/g;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 147
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->h:Lcom/twitter/model/moments/viewmodels/g;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/u;->i:Lcom/twitter/model/moments/r;

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/viewmodels/g;->f(Lcom/twitter/model/moments/r;)I

    move-result v0

    .line 148
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/u;->a:Lcom/twitter/android/moments/ui/maker/v;

    add-int/lit8 v0, v0, 0x1

    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/u;->h:Lcom/twitter/model/moments/viewmodels/g;

    iget-object v2, v2, Lcom/twitter/model/moments/viewmodels/g;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/moments/ui/maker/v;->a(II)V

    .line 152
    :goto_0
    return-void

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->a:Lcom/twitter/android/moments/ui/maker/v;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/v;->c()V

    goto :goto_0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->a:Lcom/twitter/android/moments/ui/maker/v;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/u$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/u$2;-><init>(Lcom/twitter/android/moments/ui/maker/u;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/v;->b(Landroid/view/View$OnClickListener;)V

    .line 173
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->a:Lcom/twitter/android/moments/ui/maker/v;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/u$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/u$3;-><init>(Lcom/twitter/android/moments/ui/maker/u;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/v;->a(Landroid/view/View$OnClickListener;)V

    .line 179
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->a:Lcom/twitter/android/moments/ui/maker/v;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/u$4;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/u$4;-><init>(Lcom/twitter/android/moments/ui/maker/u;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/v;->c(Landroid/view/View$OnClickListener;)V

    .line 185
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->i:Lcom/twitter/model/moments/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->h:Lcom/twitter/model/moments/viewmodels/g;

    if-nez v0, :cond_1

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 191
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->h:Lcom/twitter/model/moments/viewmodels/g;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/u;->i:Lcom/twitter/model/moments/r;

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/viewmodels/g;->b(Lcom/twitter/model/moments/r;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 192
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->c:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/maker/navigation/ah;->a()V

    goto :goto_0

    .line 194
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->f:Ljava/util/Map;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/u;->i:Lcom/twitter/model/moments/r;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->g:Ljava/util/Map;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/u;->i:Lcom/twitter/model/moments/r;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->h:Lcom/twitter/model/moments/viewmodels/g;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/u;->i:Lcom/twitter/model/moments/r;

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/viewmodels/g;->e(Lcom/twitter/model/moments/r;)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 197
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/maker/u;->a(Lcom/twitter/model/moments/r;)V

    goto :goto_0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->i:Lcom/twitter/model/moments/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->h:Lcom/twitter/model/moments/viewmodels/g;

    if-nez v0, :cond_1

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->i:Lcom/twitter/model/moments/r;

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/maker/u;->b(Lcom/twitter/model/moments/r;)V

    .line 206
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->h:Lcom/twitter/model/moments/viewmodels/g;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/u;->i:Lcom/twitter/model/moments/r;

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/viewmodels/g;->c(Lcom/twitter/model/moments/r;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 207
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/u;->j()V

    .line 208
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/u;->k()V

    .line 209
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->c:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/maker/navigation/ah;->a()V

    goto :goto_0

    .line 211
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->h:Lcom/twitter/model/moments/viewmodels/g;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/u;->i:Lcom/twitter/model/moments/r;

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/viewmodels/g;->d(Lcom/twitter/model/moments/r;)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 212
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/maker/u;->a(Lcom/twitter/model/moments/r;)V

    goto :goto_0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 225
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcff;

    .line 226
    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/u;->d:Lwy;

    invoke-virtual {v2, v0}, Lwy;->a(Lcfd;)V

    goto :goto_0

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->d:Lwy;

    invoke-virtual {v0}, Lwy;->c()Lrx/g;

    move-result-object v0

    invoke-static {}, Lcqw;->d()Lcqw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    .line 229
    return-void
.end method

.method private k()V
    .locals 4

    .prologue
    .line 232
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 233
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 234
    if-nez v1, :cond_1

    .line 235
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/u;->e:Lzv;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/r;

    iget-object v0, v0, Lcom/twitter/model/moments/r;->e:Ljava/lang/Long;

    invoke-virtual {v1, v0}, Lzv;->a(Ljava/lang/Long;)V

    goto :goto_0

    .line 236
    :cond_1
    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    .line 237
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/u;->e:Lzv;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/r;

    iget-object v0, v0, Lcom/twitter/model/moments/r;->e:Ljava/lang/Long;

    invoke-virtual {v1, v0}, Lzv;->b(Ljava/lang/Long;)V

    goto :goto_0

    .line 240
    :cond_2
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/viewmodels/g;)V
    .locals 1

    .prologue
    .line 84
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/u;->h:Lcom/twitter/model/moments/viewmodels/g;

    .line 85
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/g;->a()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/maker/u;->a(Lcom/twitter/model/moments/r;)V

    .line 86
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/u;->g()V

    .line 87
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/u;->c()V

    .line 88
    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->a:Lcom/twitter/android/moments/ui/maker/v;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/v;->aN_()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/u;->k:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 244
    return-void
.end method
