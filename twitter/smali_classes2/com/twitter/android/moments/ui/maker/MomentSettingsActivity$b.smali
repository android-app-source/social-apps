.class public Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;
.super Lakr;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lakr",
        "<",
        "Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:J

.field private final d:Lcom/twitter/library/client/Session;


# direct methods
.method protected constructor <init>(Landroid/app/Activity;Lcom/twitter/library/client/Session;J)V
    .locals 1

    .prologue
    .line 74
    const-class v0, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity;

    invoke-direct {p0, p1, v0}, Lakr;-><init>(Landroid/app/Activity;Ljava/lang/Class;)V

    .line 75
    iput-wide p3, p0, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;->c:J

    .line 76
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;->d:Lcom/twitter/library/client/Session;

    .line 77
    return-void
.end method

.method public static a(Landroid/app/Activity;J)Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;
    .locals 3

    .prologue
    .line 70
    new-instance v0, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;-><init>(Landroid/app/Activity;Lcom/twitter/library/client/Session;J)V

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 80
    new-instance v0, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$a;

    iget-wide v2, p0, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;->c:J

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;->d:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$a;-><init>(JJ)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;->a(Lako;)V

    .line 81
    return-void
.end method
