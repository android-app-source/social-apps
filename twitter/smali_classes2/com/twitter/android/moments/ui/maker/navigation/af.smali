.class public Lcom/twitter/android/moments/ui/maker/navigation/af;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/object/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/object/d",
        "<",
        "Lcom/twitter/android/moments/ui/maker/navigation/aj;",
        "Lcom/twitter/android/moments/ui/maker/navigation/ak;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/ui/maker/navigation/aj;)Lcom/twitter/android/moments/ui/maker/navigation/ak;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p1, Lcom/twitter/android/moments/ui/maker/navigation/aj;->a:Lcom/twitter/android/moments/ui/maker/navigation/ai;

    instance-of v0, v0, Lcom/twitter/android/moments/ui/maker/navigation/b;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/twitter/android/moments/ui/maker/navigation/aj;->b:Lcom/twitter/android/moments/ui/maker/navigation/ai;

    instance-of v0, v0, Lcom/twitter/android/moments/ui/maker/navigation/w;

    if-eqz v0, :cond_0

    .line 17
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/maker/navigation/aj;

    .line 15
    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/navigation/e;->a(Lcom/twitter/android/moments/ui/maker/navigation/aj;)Lcom/twitter/android/moments/ui/maker/navigation/ak;

    move-result-object v0

    .line 32
    :goto_0
    return-object v0

    .line 19
    :cond_0
    iget-object v0, p1, Lcom/twitter/android/moments/ui/maker/navigation/aj;->a:Lcom/twitter/android/moments/ui/maker/navigation/ai;

    instance-of v0, v0, Lcom/twitter/android/moments/ui/maker/navigation/w;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/twitter/android/moments/ui/maker/navigation/aj;->b:Lcom/twitter/android/moments/ui/maker/navigation/ai;

    instance-of v0, v0, Lcom/twitter/android/moments/ui/maker/navigation/b;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/twitter/android/moments/ui/maker/navigation/aj;->c:Lcom/twitter/android/moments/ui/maker/navigation/am;

    instance-of v0, v0, Lcom/twitter/android/moments/ui/maker/navigation/z;

    if-eqz v0, :cond_1

    .line 24
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/maker/navigation/aj;

    .line 22
    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/navigation/aa;->a(Lcom/twitter/android/moments/ui/maker/navigation/aj;)Lcom/twitter/android/moments/ui/maker/navigation/ak;

    move-result-object v0

    goto :goto_0

    .line 26
    :cond_1
    iget-object v0, p1, Lcom/twitter/android/moments/ui/maker/navigation/aj;->b:Lcom/twitter/android/moments/ui/maker/navigation/ai;

    instance-of v0, v0, Lcom/twitter/android/moments/ui/maker/navigation/g;

    if-eqz v0, :cond_2

    .line 27
    iget-object v0, p1, Lcom/twitter/android/moments/ui/maker/navigation/aj;->b:Lcom/twitter/android/moments/ui/maker/navigation/ai;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/navigation/u;->a(Lcom/twitter/android/moments/ui/maker/navigation/ai;)Lcom/twitter/android/moments/ui/maker/navigation/ak;

    move-result-object v0

    goto :goto_0

    .line 29
    :cond_2
    iget-object v0, p1, Lcom/twitter/android/moments/ui/maker/navigation/aj;->a:Lcom/twitter/android/moments/ui/maker/navigation/ai;

    instance-of v0, v0, Lcom/twitter/android/moments/ui/maker/navigation/g;

    if-eqz v0, :cond_3

    .line 30
    iget-object v0, p1, Lcom/twitter/android/moments/ui/maker/navigation/aj;->a:Lcom/twitter/android/moments/ui/maker/navigation/ai;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/navigation/v;->a(Lcom/twitter/android/moments/ui/maker/navigation/ai;)Lcom/twitter/android/moments/ui/maker/navigation/ak;

    move-result-object v0

    goto :goto_0

    .line 32
    :cond_3
    sget-object v0, Lcom/twitter/android/moments/ui/maker/navigation/s;->a:Lcom/twitter/android/moments/ui/maker/navigation/s;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/maker/navigation/s;->a(Lcom/twitter/android/moments/ui/maker/navigation/aj;)Lcom/twitter/android/moments/ui/maker/navigation/ak;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    check-cast p1, Lcom/twitter/android/moments/ui/maker/navigation/aj;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/navigation/af;->a(Lcom/twitter/android/moments/ui/maker/navigation/aj;)Lcom/twitter/android/moments/ui/maker/navigation/ak;

    move-result-object v0

    return-object v0
.end method
