.class public Lcom/twitter/android/moments/ui/maker/ai;
.super Laog;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/navigation/a;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Landroid/view/ViewGroup;Lcom/twitter/util/object/d;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Landroid/view/ViewGroup;",
            "Lcom/twitter/util/object/d",
            "<",
            "Lanh;",
            "Lcom/twitter/android/moments/ui/maker/navigation/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0}, Laog;-><init>()V

    .line 57
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/ai;->o()Lanh;

    move-result-object v0

    invoke-interface {p3, v0}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/maker/navigation/a;

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/ai;->a:Lcom/twitter/android/moments/ui/maker/navigation/a;

    .line 58
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ai;->a:Lcom/twitter/android/moments/ui/maker/navigation/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/navigation/a;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 59
    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/ai;->a(Landroid/view/View;)V

    .line 60
    return-void
.end method

.method public static a(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lyf;Lya;Lxp;J)Lcom/twitter/android/moments/ui/maker/ai;
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 34
    new-instance v0, Lcom/twitter/android/moments/ui/maker/bc;

    new-instance v1, Landroid/util/LruCache;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Landroid/util/LruCache;-><init>(I)V

    invoke-direct {v0, v1}, Lcom/twitter/android/moments/ui/maker/bc;-><init>(Landroid/util/LruCache;)V

    .line 37
    invoke-static {p0, v0, p1}, Lcom/twitter/android/moments/ui/maker/ap$a;->a(Landroid/app/Activity;Lcom/twitter/android/moments/ui/maker/bc;Lyf;)Lcom/twitter/android/moments/ui/maker/ap$a;

    move-result-object v3

    .line 39
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401c4

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/view/ViewGroup;

    .line 40
    const v0, 0x7f1304cd

    invoke-virtual {v9, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 41
    new-instance v4, Lcom/twitter/android/moments/data/af;

    invoke-direct {v4, v5}, Lcom/twitter/android/moments/data/af;-><init>(Ljava/lang/Object;)V

    .line 42
    new-instance v0, Lcom/twitter/android/moments/ui/maker/ai$1;

    move-object v1, p0

    move-object v5, p2

    move-wide v6, p4

    move-object v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/moments/ui/maker/ai$1;-><init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/maker/ap$a;Lcom/twitter/android/moments/data/af;Lya;JLxp;)V

    .line 52
    new-instance v1, Lcom/twitter/android/moments/ui/maker/ai;

    invoke-direct {v1, v9, v2, v0}, Lcom/twitter/android/moments/ui/maker/ai;-><init>(Landroid/view/ViewGroup;Landroid/view/ViewGroup;Lcom/twitter/util/object/d;)V

    return-object v1
.end method


# virtual methods
.method public am_()V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Laog;->am_()V

    .line 69
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ai;->a:Lcom/twitter/android/moments/ui/maker/navigation/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/navigation/a;->b()V

    .line 70
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ai;->a:Lcom/twitter/android/moments/ui/maker/navigation/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/navigation/a;->e()V

    .line 64
    return-void
.end method
