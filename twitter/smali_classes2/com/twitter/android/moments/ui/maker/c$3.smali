.class Lcom/twitter/android/moments/ui/maker/c$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/maker/c;->d()Lrx/functions/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/e",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/twitter/model/moments/viewmodels/g;",
        ">;",
        "Lcom/twitter/util/collection/m",
        "<",
        "Lcom/twitter/model/moments/viewmodels/b;",
        "Lcom/twitter/model/moments/b;",
        ">;",
        "Ljava/util/List",
        "<",
        "Lcom/twitter/model/moments/viewmodels/g;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/ui/maker/c;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/maker/c;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/c$3;->a:Lcom/twitter/android/moments/ui/maker/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 138
    check-cast p1, Ljava/util/List;

    check-cast p2, Lcom/twitter/util/collection/m;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/moments/ui/maker/c$3;->a(Ljava/util/List;Lcom/twitter/util/collection/m;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/List;Lcom/twitter/util/collection/m;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 142
    invoke-virtual {p2}, Lcom/twitter/util/collection/m;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/c$3;->a:Lcom/twitter/android/moments/ui/maker/c;

    .line 144
    invoke-virtual {p2}, Lcom/twitter/util/collection/m;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/b;

    iget-object v0, v0, Lcom/twitter/model/moments/viewmodels/b;->a:Lcom/twitter/model/moments/viewmodels/a;

    .line 143
    invoke-static {v1, p1, v0}, Lcom/twitter/android/moments/ui/maker/c;->a(Lcom/twitter/android/moments/ui/maker/c;Ljava/util/List;Lcom/twitter/model/moments/viewmodels/a;)Ljava/util/List;

    move-result-object p1

    .line 146
    :cond_0
    return-object p1
.end method
