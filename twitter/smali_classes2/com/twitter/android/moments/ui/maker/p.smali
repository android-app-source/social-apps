.class public Lcom/twitter/android/moments/ui/maker/p;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lajn;
.implements Lajq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lajn",
        "<",
        "Lada;",
        ">;",
        "Lajq",
        "<",
        "Lada;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

.field private final b:Lcom/twitter/android/moments/ui/maker/navigation/ah;

.field private final c:Lcom/twitter/android/moments/ui/maker/be;

.field private final d:Lacz;

.field private final e:Lzw;

.field private f:Lcom/twitter/model/moments/viewmodels/g;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/e;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/ui/maker/be;Lacz;Lzw;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/p;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

    .line 51
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/p;->b:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    .line 52
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/p;->c:Lcom/twitter/android/moments/ui/maker/be;

    .line 53
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/p;->d:Lacz;

    .line 54
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/p;->e:Lzw;

    .line 55
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/p;)Lcom/twitter/android/moments/ui/maker/navigation/ah;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/p;->b:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/android/moments/ui/maker/viewdelegate/e;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/ui/maker/be;Lacz;J)Lcom/twitter/android/moments/ui/maker/p;
    .locals 7

    .prologue
    .line 39
    .line 40
    invoke-static {p0, p5, p6}, Lzw;->a(Landroid/content/Context;J)Lzw;

    move-result-object v5

    .line 41
    new-instance v0, Lcom/twitter/android/moments/ui/maker/p;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/moments/ui/maker/p;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/e;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/ui/maker/be;Lacz;Lzw;)V

    return-object v0
.end method

.method private a(Lcom/twitter/model/moments/viewmodels/g;)V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/p;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/p$2;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/maker/p$2;-><init>(Lcom/twitter/android/moments/ui/maker/p;Lcom/twitter/model/moments/viewmodels/g;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->a(Landroid/view/View$OnClickListener;)V

    .line 113
    return-void
.end method

.method private a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Z
    .locals 1

    .prologue
    .line 87
    instance-of v0, p1, Lcom/twitter/model/moments/viewmodels/j;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/twitter/model/moments/viewmodels/o;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/maker/p;)Lzw;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/p;->e:Lzw;

    return-object v0
.end method

.method private b(Lcom/twitter/model/moments/viewmodels/MomentPage;)V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/p;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/p$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/maker/p$1;-><init>(Lcom/twitter/android/moments/ui/maker/p;Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->d(Landroid/view/View$OnClickListener;)V

    .line 104
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/p;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/p$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/p$3;-><init>(Lcom/twitter/android/moments/ui/maker/p;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->b(Landroid/view/View$OnClickListener;)V

    .line 123
    return-void
.end method


# virtual methods
.method public a(Lada;)V
    .locals 2

    .prologue
    .line 65
    invoke-virtual {p1}, Lada;->b()Lcom/twitter/model/moments/viewmodels/g;

    move-result-object v0

    .line 66
    if-nez v0, :cond_1

    .line 67
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/p;->f:Lcom/twitter/model/moments/viewmodels/g;

    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/p;->b()V

    .line 84
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/p;->f:Lcom/twitter/model/moments/viewmodels/g;

    .line 73
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/p;->c:Lcom/twitter/android/moments/ui/maker/be;

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/maker/be;->a(Lcom/twitter/model/moments/viewmodels/g;)V

    .line 74
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/p;->d:Lacz;

    invoke-virtual {v1, v0}, Lacz;->a(Lcom/twitter/model/moments/viewmodels/g;)V

    .line 75
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/g;->a()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/twitter/android/moments/ui/maker/p;->b(Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    .line 76
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/p;->c()V

    .line 77
    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/maker/p;->a(Lcom/twitter/model/moments/viewmodels/g;)V

    .line 78
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/p;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->e()V

    .line 79
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/g;->a()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/maker/p;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 80
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/p;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->d()V

    goto :goto_0

    .line 82
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/p;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->f()V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lada;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/p;->a(Lada;)V

    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/p;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/e;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/e;->aN_()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/p;->c:Lcom/twitter/android/moments/ui/maker/be;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/be;->c()V

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/p;->f:Lcom/twitter/model/moments/viewmodels/g;

    .line 94
    return-void
.end method
