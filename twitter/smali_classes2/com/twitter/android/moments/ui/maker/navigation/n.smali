.class public Lcom/twitter/android/moments/ui/maker/navigation/n;
.super Lcom/twitter/android/moments/ui/maker/navigation/ag;
.source "Twttr"


# instance fields
.field private final b:Lcom/twitter/model/moments/viewmodels/MomentPage;

.field private final c:Lcom/twitter/model/moments/Moment;


# direct methods
.method public constructor <init>(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/moments/Moment;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/navigation/ag;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/navigation/n;->b:Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 15
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/navigation/n;->c:Lcom/twitter/model/moments/Moment;

    .line 16
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/n;->b:Lcom/twitter/model/moments/viewmodels/MomentPage;

    return-object v0
.end method

.method public b()Lcom/twitter/model/moments/Moment;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/n;->c:Lcom/twitter/model/moments/Moment;

    return-object v0
.end method
