.class public Lcom/twitter/android/moments/ui/maker/ax;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/viewdelegate/v;

.field private final b:Lcom/twitter/android/moments/ui/maker/al;

.field private c:Lcom/twitter/model/moments/r;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/v;Lcom/twitter/model/moments/r;Lcom/twitter/android/moments/ui/maker/al;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/ax;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/v;

    .line 27
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/ax;->c:Lcom/twitter/model/moments/r;

    .line 28
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/ax;->b:Lcom/twitter/android/moments/ui/maker/al;

    .line 29
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ax;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/v;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/v;->c()I

    move-result v0

    .line 45
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/ax;->b:Lcom/twitter/android/moments/ui/maker/al;

    .line 46
    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/al;->b()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 47
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/ax;->b:Lcom/twitter/android/moments/ui/maker/al;

    .line 48
    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/maker/al;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/g;

    .line 49
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/g;->a()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/ax;->c:Lcom/twitter/model/moments/r;

    .line 51
    :cond_0
    return-void
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 54
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/ax;->b:Lcom/twitter/android/moments/ui/maker/al;

    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ax;->c:Lcom/twitter/model/moments/r;

    .line 55
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/r;

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/maker/al;->a(Lcom/twitter/model/moments/r;)I

    move-result v0

    .line 56
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/ax;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/v;

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/v;->c(I)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()Lcom/twitter/model/moments/r;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ax;->c:Lcom/twitter/model/moments/r;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/ax;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 34
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/ax;->b()V

    .line 36
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ax;->c:Lcom/twitter/model/moments/r;

    return-object v0
.end method

.method public a(Lcom/twitter/model/moments/viewmodels/MomentPage;)V
    .locals 1

    .prologue
    .line 40
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/ax;->c:Lcom/twitter/model/moments/r;

    .line 41
    return-void

    .line 40
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
