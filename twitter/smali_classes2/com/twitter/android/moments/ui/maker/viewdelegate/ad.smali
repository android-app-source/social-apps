.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;->a:Landroid/view/View;

    .line 31
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;->b:Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;

    .line 32
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;
    .locals 2

    .prologue
    .line 25
    const v0, 0x7f13047c

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;

    .line 26
    new-instance v1, Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;

    invoke-direct {v1, v0, v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;-><init>(Landroid/view/View;Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;)V

    return-object v1
.end method


# virtual methods
.method public a(Landroid/graphics/Bitmap;Lcom/twitter/util/math/c;ZZ)V
    .locals 4

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;->aN_()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04019a

    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;->b:Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;

    const/4 v3, 0x0

    .line 43
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 44
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 45
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;->b:Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;

    invoke-virtual {v1, v0, p2, p3, p4}, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->a(Landroid/view/View;Lcom/twitter/util/math/c;ZZ)V

    .line 46
    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;->a:Landroid/view/View;

    return-object v0
.end method

.method public b()Lcom/twitter/util/math/c;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;->b:Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;

    invoke-virtual {v0}, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->getCurrentCropRect()Lcom/twitter/util/math/c;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;->b:Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;

    invoke-virtual {v0}, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->a()V

    .line 55
    return-void
.end method

.method public d()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;->b:Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;

    invoke-virtual {v0}, Lcom/twitter/moments/maker/ui/cropper/view/CropContainerView;->b()Lrx/c;

    move-result-object v0

    return-object v0
.end method
