.class Lcom/twitter/android/moments/ui/maker/ah$b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/maker/ah;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation


# instance fields
.field private final a:Landroid/preference/CheckBoxPreference;


# direct methods
.method constructor <init>(Landroid/preference/CheckBoxPreference;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/ah$b;->a:Landroid/preference/CheckBoxPreference;

    .line 62
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/ah$b;)Landroid/preference/CheckBoxPreference;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ah$b;->a:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/ui/maker/ah$a;)V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ah$b;->a:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/ah$b$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/maker/ah$b$1;-><init>(Lcom/twitter/android/moments/ui/maker/ah$b;Lcom/twitter/android/moments/ui/maker/ah$a;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 76
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ah$b;->a:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 66
    return-void
.end method
