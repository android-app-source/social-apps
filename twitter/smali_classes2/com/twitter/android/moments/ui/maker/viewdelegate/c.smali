.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;

.field private final e:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;Landroid/view/View;Landroid/widget/TextView;Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->a:Landroid/view/View;

    .line 44
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->b:Landroid/view/View;

    .line 45
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->c:Landroid/widget/TextView;

    .line 46
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->d:Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;

    .line 47
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->e:Landroid/view/View;

    .line 48
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/c;
    .locals 6

    .prologue
    .line 29
    .line 30
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401c5

    const/4 v2, 0x0

    .line 31
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 32
    const v0, 0x7f1304ce

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 33
    const v0, 0x7f1304cf

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 34
    const v0, 0x7f1304d1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 35
    const v0, 0x7f1304d0

    .line 36
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 35
    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;->a(Landroid/view/View;)Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;

    move-result-object v4

    .line 37
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;-><init>(Landroid/view/View;Landroid/view/View;Landroid/widget/TextView;Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;Landroid/view/View;)V

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 66
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->d:Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;->c()V

    .line 67
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 73
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->d:Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;->b()V

    .line 74
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->d:Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;

    invoke-virtual {v0, p4}, Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;->a(Z)V

    .line 75
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->d:Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;->a(Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->d:Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;

    invoke-virtual {v0, p2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;->b(Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->d:Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;

    invoke-virtual {v0, p3}, Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;->c(Ljava/lang/String;)V

    .line 78
    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->a:Landroid/view/View;

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 62
    return-void
.end method
