.class public Lcom/twitter/android/moments/ui/maker/navigation/u;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/maker/navigation/ak;


# instance fields
.field private final b:Landroid/view/View;

.field private final c:Landroid/animation/ObjectAnimator;

.field private final d:I

.field private final e:I

.field private f:Lrx/subjects/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/a",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;II)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/navigation/u;->b:Landroid/view/View;

    .line 32
    iput p2, p0, Lcom/twitter/android/moments/ui/maker/navigation/u;->d:I

    .line 33
    iput p3, p0, Lcom/twitter/android/moments/ui/maker/navigation/u;->e:I

    .line 34
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/maker/navigation/u;->a(Landroid/view/View;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/u;->c:Landroid/animation/ObjectAnimator;

    .line 35
    return-void
.end method

.method private a(Landroid/view/View;)Landroid/animation/ObjectAnimator;
    .locals 4

    .prologue
    .line 40
    sget-object v0, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 41
    iget v1, p0, Lcom/twitter/android/moments/ui/maker/navigation/u;->e:I

    if-eqz v1, :cond_0

    .line 42
    iget v1, p0, Lcom/twitter/android/moments/ui/maker/navigation/u;->e:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 44
    :cond_0
    iget v1, p0, Lcom/twitter/android/moments/ui/maker/navigation/u;->d:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 45
    new-instance v1, Lcom/twitter/android/moments/ui/maker/navigation/u$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/maker/navigation/u$1;-><init>(Lcom/twitter/android/moments/ui/maker/navigation/u;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 58
    return-object v0

    .line 40
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static a(Lcom/twitter/android/moments/ui/maker/navigation/ai;)Lcom/twitter/android/moments/ui/maker/navigation/ak;
    .locals 4

    .prologue
    .line 26
    new-instance v0, Lcom/twitter/android/moments/ui/maker/navigation/u;

    invoke-interface {p0}, Lcom/twitter/android/moments/ui/maker/navigation/ai;->a()Landroid/view/View;

    move-result-object v1

    const/16 v2, 0xe1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/moments/ui/maker/navigation/u;-><init>(Landroid/view/View;II)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/navigation/u;)Lrx/subjects/a;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/u;->f:Lrx/subjects/a;

    return-object v0
.end method


# virtual methods
.method public a()Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    invoke-static {}, Lrx/subjects/a;->r()Lrx/subjects/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/u;->f:Lrx/subjects/a;

    .line 65
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/u;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 66
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/u;->c:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 67
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/u;->f:Lrx/subjects/a;

    invoke-virtual {v0}, Lrx/subjects/a;->b()Lrx/g;

    move-result-object v0

    return-object v0
.end method
