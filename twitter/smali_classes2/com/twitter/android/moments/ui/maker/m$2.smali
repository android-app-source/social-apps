.class Lcom/twitter/android/moments/ui/maker/m$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/maker/m;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/model/moments/viewmodels/MomentPage;

.field final synthetic b:Lcom/twitter/android/moments/ui/maker/m;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/maker/m;Lcom/twitter/model/moments/viewmodels/MomentPage;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/m$2;->b:Lcom/twitter/android/moments/ui/maker/m;

    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/m$2;->a:Lcom/twitter/model/moments/viewmodels/MomentPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/m$2;->b:Lcom/twitter/android/moments/ui/maker/m;

    .line 69
    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/m;->b(Lcom/twitter/android/moments/ui/maker/m;)Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/m$2;->a:Lcom/twitter/model/moments/viewmodels/MomentPage;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->g()Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/twitter/android/moments/ui/fullscreen/MomentsFullScreenPagerActivity;->b(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v0

    .line 70
    const-string/jumbo v1, "extra_initial_page_id"

    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/m$2;->a:Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 71
    invoke-virtual {v2}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v2

    sget-object v3, Lcom/twitter/model/moments/r;->a:Lcom/twitter/util/serialization/l;

    .line 70
    invoke-static {v0, v1, v2, v3}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "extra_preview_mode"

    const/4 v3, 0x1

    .line 72
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 73
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/m$2;->b:Lcom/twitter/android/moments/ui/maker/m;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/maker/m;->b(Lcom/twitter/android/moments/ui/maker/m;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 74
    return-void
.end method
