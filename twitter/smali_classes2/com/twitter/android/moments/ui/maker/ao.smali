.class public Lcom/twitter/android/moments/ui/maker/ao;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/model/moments/viewmodels/MomentPage;

.field private final b:Lcom/twitter/android/moments/ui/maker/bl;

.field private final c:Lcom/twitter/android/moments/ui/maker/bj;

.field private final d:Lcom/twitter/util/math/Size;

.field private final e:Lcom/twitter/util/math/Size;

.field private final f:Lcom/twitter/util/object/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/util/math/Size;",
            "Lcom/twitter/android/moments/ui/maker/SnapshotViewRoot;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/util/math/Size;Lcom/twitter/util/math/Size;Lcom/twitter/android/moments/ui/maker/bl;Lcom/twitter/android/moments/ui/maker/bj;Lcom/twitter/util/object/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            "Lcom/twitter/util/math/Size;",
            "Lcom/twitter/util/math/Size;",
            "Lcom/twitter/android/moments/ui/maker/bl;",
            "Lcom/twitter/android/moments/ui/maker/bj;",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/util/math/Size;",
            "Lcom/twitter/android/moments/ui/maker/SnapshotViewRoot;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/ao;->a:Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 43
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/ao;->b:Lcom/twitter/android/moments/ui/maker/bl;

    .line 44
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/ao;->c:Lcom/twitter/android/moments/ui/maker/bj;

    .line 45
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/ao;->d:Lcom/twitter/util/math/Size;

    .line 46
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/ao;->e:Lcom/twitter/util/math/Size;

    .line 47
    iput-object p6, p0, Lcom/twitter/android/moments/ui/maker/ao;->f:Lcom/twitter/util/object/d;

    .line 48
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/ao;)Lcom/twitter/android/moments/ui/maker/bj;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ao;->c:Lcom/twitter/android/moments/ui/maker/bj;

    return-object v0
.end method

.method private a(Landroid/widget/FrameLayout;)Lrx/functions/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/FrameLayout;",
            ")",
            "Lrx/functions/b",
            "<",
            "Lacg",
            "<+",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 71
    new-instance v0, Lcom/twitter/android/moments/ui/maker/ao$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/moments/ui/maker/ao$1;-><init>(Lcom/twitter/android/moments/ui/maker/ao;Landroid/widget/FrameLayout;)V

    return-object v0
.end method

.method private a(Landroid/widget/FrameLayout;Lcom/twitter/util/math/Size;Lcom/twitter/util/math/Size;)Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/FrameLayout;",
            "Lcom/twitter/util/math/Size;",
            "Lcom/twitter/util/math/Size;",
            ")",
            "Lrx/functions/d",
            "<",
            "Ljava/util/List",
            "<",
            "Lacg",
            "<+",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ">;>;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129
    new-instance v0, Lcom/twitter/android/moments/ui/maker/ao$4;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/twitter/android/moments/ui/maker/ao$4;-><init>(Lcom/twitter/android/moments/ui/maker/ao;Landroid/widget/FrameLayout;Lcom/twitter/util/math/Size;Lcom/twitter/util/math/Size;)V

    return-object v0
.end method

.method private a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ")",
            "Lrx/functions/d",
            "<",
            "Lcom/twitter/android/moments/ui/maker/bl;",
            "Lrx/c",
            "<",
            "Lacg",
            "<+",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 107
    new-instance v0, Lcom/twitter/android/moments/ui/maker/ao$3;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/moments/ui/maker/ao$3;-><init>(Lcom/twitter/android/moments/ui/maker/ao;Lcom/twitter/model/moments/viewmodels/MomentPage;)V

    return-object v0
.end method

.method private b()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<",
            "Lacg",
            "<+",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ">;",
            "Lrx/c",
            "<+",
            "Lacg",
            "<+",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 88
    new-instance v0, Lcom/twitter/android/moments/ui/maker/ao$2;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/maker/ao$2;-><init>(Lcom/twitter/android/moments/ui/maker/ao;)V

    return-object v0
.end method


# virtual methods
.method public a()Lrx/g;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ao;->f:Lcom/twitter/util/object/d;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/ao;->d:Lcom/twitter/util/math/Size;

    invoke-interface {v0, v1}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/maker/SnapshotViewRoot;

    .line 53
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/ao;->b:Lcom/twitter/android/moments/ui/maker/bl;

    .line 54
    invoke-static {v1}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/ao;->a:Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 55
    invoke-direct {p0, v2}, Lcom/twitter/android/moments/ui/maker/ao;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lrx/functions/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/g;->b(Lrx/functions/d;)Lrx/c;

    move-result-object v1

    .line 56
    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/maker/ao;->a(Landroid/widget/FrameLayout;)Lrx/functions/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/c;->b(Lrx/functions/b;)Lrx/c;

    move-result-object v1

    .line 57
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/ao;->b()Lrx/functions/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/c;->c(Lrx/functions/d;)Lrx/c;

    move-result-object v1

    .line 58
    invoke-virtual {v1}, Lrx/c;->q()Lrx/c;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/ao;->d:Lcom/twitter/util/math/Size;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/ao;->e:Lcom/twitter/util/math/Size;

    .line 59
    invoke-direct {p0, v0, v2, v3}, Lcom/twitter/android/moments/ui/maker/ao;->a(Landroid/widget/FrameLayout;Lcom/twitter/util/math/Size;Lcom/twitter/util/math/Size;)Lrx/functions/d;

    move-result-object v0

    invoke-virtual {v1, v0}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 61
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcre;->a(Lrx/c;Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    return-object v0
.end method
