.class Lcom/twitter/android/moments/ui/maker/g$1;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/maker/g;-><init>(Landroid/content/res/Resources;Lakq;Lcom/twitter/android/moments/ui/maker/viewdelegate/d;Lcom/twitter/android/moments/ui/maker/f;Lcom/twitter/android/moments/data/af;Lzs;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/content/res/Resources;

.field final synthetic b:Lcom/twitter/android/moments/ui/maker/g;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/maker/g;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/g$1;->b:Lcom/twitter/android/moments/ui/maker/g;

    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/g$1;->a:Landroid/content/res/Resources;

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Integer;)V
    .locals 7

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/g$1;->b:Lcom/twitter/android/moments/ui/maker/g;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/g;->a(Lcom/twitter/android/moments/ui/maker/g;)Lcom/twitter/android/moments/data/af;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/af;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/g$1;->b:Lcom/twitter/android/moments/ui/maker/g;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/g;->b(Lcom/twitter/android/moments/ui/maker/g;)Lcom/twitter/android/moments/ui/maker/viewdelegate/d;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->a(Ljava/lang/String;)V

    .line 74
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/g$1;->b:Lcom/twitter/android/moments/ui/maker/g;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/g;->b(Lcom/twitter/android/moments/ui/maker/g;)Lcom/twitter/android/moments/ui/maker/viewdelegate/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/g$1;->a:Landroid/content/res/Resources;

    const v2, 0x7f0c0014

    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/g$1;->b:Lcom/twitter/android/moments/ui/maker/g;

    .line 71
    invoke-static {v3}, Lcom/twitter/android/moments/ui/maker/g;->a(Lcom/twitter/android/moments/ui/maker/g;)Lcom/twitter/android/moments/data/af;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/moments/data/af;->size()I

    move-result v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/twitter/android/moments/ui/maker/g$1;->b:Lcom/twitter/android/moments/ui/maker/g;

    .line 72
    invoke-static {v6}, Lcom/twitter/android/moments/ui/maker/g;->a(Lcom/twitter/android/moments/ui/maker/g;)Lcom/twitter/android/moments/data/af;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/android/moments/data/af;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 70
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/d;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 64
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/g$1;->a(Ljava/lang/Integer;)V

    return-void
.end method
