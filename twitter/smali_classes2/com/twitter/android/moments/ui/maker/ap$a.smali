.class public Lcom/twitter/android/moments/ui/maker/ap$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/object/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/maker/ap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/object/d",
        "<",
        "Lcom/twitter/model/moments/viewmodels/MomentPage;",
        "Lcom/twitter/android/moments/ui/maker/ap;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Landroid/app/Activity;

.field final b:Lcom/twitter/util/math/Size;

.field final c:Lcom/twitter/util/math/Size;

.field final d:Lcom/twitter/android/moments/ui/maker/bc;

.field private final e:Lyf;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/util/math/Size;Lcom/twitter/util/math/Size;Lcom/twitter/android/moments/ui/maker/bc;Lyf;)V
    .locals 0

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/ap$a;->a:Landroid/app/Activity;

    .line 157
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/ap$a;->b:Lcom/twitter/util/math/Size;

    .line 158
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/ap$a;->c:Lcom/twitter/util/math/Size;

    .line 159
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/ap$a;->d:Lcom/twitter/android/moments/ui/maker/bc;

    .line 160
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/ap$a;->e:Lyf;

    .line 161
    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/twitter/android/moments/ui/maker/bc;Lyf;)Lcom/twitter/android/moments/ui/maker/ap$a;
    .locals 6

    .prologue
    .line 148
    invoke-static {p0}, Lcom/twitter/android/moments/ui/maker/bf;->a(Landroid/content/Context;)Lcom/twitter/android/moments/ui/maker/bf;

    move-result-object v1

    .line 149
    new-instance v0, Lcom/twitter/android/moments/ui/maker/ap$a;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/bf;->a()Lcom/twitter/util/math/Size;

    move-result-object v2

    .line 150
    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/bf;->b()Lcom/twitter/util/math/Size;

    move-result-object v3

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/moments/ui/maker/ap$a;-><init>(Landroid/app/Activity;Lcom/twitter/util/math/Size;Lcom/twitter/util/math/Size;Lcom/twitter/android/moments/ui/maker/bc;Lyf;)V

    .line 149
    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/android/moments/ui/maker/ap;
    .locals 6

    .prologue
    .line 166
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ap$a;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/ap$a;->b:Lcom/twitter/util/math/Size;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/ap$a;->c:Lcom/twitter/util/math/Size;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/maker/ap$a;->d:Lcom/twitter/android/moments/ui/maker/bc;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/maker/ap$a;->e:Lyf;

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/moments/ui/maker/ap;->a(Landroid/app/Activity;Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/util/math/Size;Lcom/twitter/util/math/Size;Lcom/twitter/android/moments/ui/maker/bc;Lyf;)Lcom/twitter/android/moments/ui/maker/ap;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 137
    check-cast p1, Lcom/twitter/model/moments/viewmodels/MomentPage;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/ap$a;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/android/moments/ui/maker/ap;

    move-result-object v0

    return-object v0
.end method
