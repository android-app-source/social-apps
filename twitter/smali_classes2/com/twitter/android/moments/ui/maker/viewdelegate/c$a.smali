.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/maker/viewdelegate/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Lcom/twitter/media/ui/image/UserImageView;

.field private final c:Lcom/twitter/ui/widget/TypefacesTextView;

.field private final d:Lcom/twitter/ui/widget/TypefacesTextView;

.field private final e:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/twitter/media/ui/image/UserImageView;Lcom/twitter/ui/widget/TypefacesTextView;Lcom/twitter/ui/widget/TypefacesTextView;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;->a:Landroid/view/View;

    .line 104
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;->b:Lcom/twitter/media/ui/image/UserImageView;

    .line 105
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;->c:Lcom/twitter/ui/widget/TypefacesTextView;

    .line 106
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;->d:Lcom/twitter/ui/widget/TypefacesTextView;

    .line 107
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;->e:Landroid/widget/ImageView;

    .line 108
    return-void
.end method

.method public static a(Landroid/view/View;)Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;
    .locals 6

    .prologue
    .line 90
    const v0, 0x7f130097

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/twitter/media/ui/image/UserImageView;

    .line 91
    const v0, 0x7f130485

    .line 92
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/twitter/ui/widget/TypefacesTextView;

    .line 93
    const v0, 0x7f130486

    .line 94
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/twitter/ui/widget/TypefacesTextView;

    .line 95
    const v0, 0x7f130300

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 96
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;-><init>(Landroid/view/View;Lcom/twitter/media/ui/image/UserImageView;Lcom/twitter/ui/widget/TypefacesTextView;Lcom/twitter/ui/widget/TypefacesTextView;Landroid/widget/ImageView;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;->c:Lcom/twitter/ui/widget/TypefacesTextView;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 138
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;->e:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 139
    return-void

    .line 138
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;->a:Landroid/view/View;

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 118
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 129
    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;->d:Lcom/twitter/ui/widget/TypefacesTextView;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 122
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/c$a;->b:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0, p1}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;)Z

    .line 135
    return-void
.end method
