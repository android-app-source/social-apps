.class public Lcom/twitter/android/moments/ui/maker/r;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lajn;
.implements Lajq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lajn",
        "<",
        "Lada;",
        ">;",
        "Lajq",
        "<",
        "Lada;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/s;

.field private final b:Lcom/twitter/android/moments/ui/maker/navigation/ah;

.field private final c:Lwy;

.field private final d:Lzw;

.field private e:Lrx/j;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/s;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lwy;Lzw;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/r;->a:Lcom/twitter/android/moments/ui/maker/s;

    .line 50
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/r;->b:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    .line 51
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/r;->c:Lwy;

    .line 52
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/r;->d:Lzw;

    .line 53
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/r;)Lcom/twitter/android/moments/ui/maker/navigation/ah;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/r;->b:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/android/moments/ui/maker/s;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lwy;J)Lcom/twitter/android/moments/ui/maker/r;
    .locals 2

    .prologue
    .line 39
    .line 40
    invoke-static {p0, p4, p5}, Lzw;->a(Landroid/content/Context;J)Lzw;

    move-result-object v0

    .line 41
    new-instance v1, Lcom/twitter/android/moments/ui/maker/r;

    invoke-direct {v1, p1, p2, p3, v0}, Lcom/twitter/android/moments/ui/maker/r;-><init>(Lcom/twitter/android/moments/ui/maker/s;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lwy;Lzw;)V

    return-object v1
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/r;Lcom/twitter/model/moments/viewmodels/a;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/maker/r;->a(Lcom/twitter/model/moments/viewmodels/a;)V

    return-void
.end method

.method private a(Lcom/twitter/model/moments/viewmodels/a;)V
    .locals 3

    .prologue
    .line 56
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/a;->a()Lcom/twitter/model/moments/Moment;

    move-result-object v1

    .line 57
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/r;->a:Lcom/twitter/android/moments/ui/maker/s;

    iget-object v2, v1, Lcom/twitter/model/moments/Moment;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/twitter/android/moments/ui/maker/s;->a(Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/r;->a:Lcom/twitter/android/moments/ui/maker/s;

    iget-object v2, v1, Lcom/twitter/model/moments/Moment;->l:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/twitter/android/moments/ui/maker/s;->b(Ljava/lang/String;)V

    .line 59
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/a;->c()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 60
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/a;->c()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    move-result-object v0

    sget-object v2, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->g:Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    if-ne v0, v2, :cond_0

    .line 61
    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/r;->a:Lcom/twitter/android/moments/ui/maker/s;

    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/a;->c()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/j;

    iget-object v0, v0, Lcom/twitter/model/moments/viewmodels/j;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/twitter/android/moments/ui/maker/s;->c(Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/r;->a:Lcom/twitter/android/moments/ui/maker/s;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/s;->b()V

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/r;->a:Lcom/twitter/android/moments/ui/maker/s;

    new-instance v2, Lcom/twitter/android/moments/ui/maker/r$1;

    invoke-direct {v2, p0, v1}, Lcom/twitter/android/moments/ui/maker/r$1;-><init>(Lcom/twitter/android/moments/ui/maker/r;Lcom/twitter/model/moments/Moment;)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/moments/ui/maker/s;->a(Landroid/view/View$OnClickListener;)V

    .line 71
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/maker/r;)Lzw;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/r;->d:Lzw;

    return-object v0
.end method


# virtual methods
.method public a(Lada;)V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/r;->e:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 82
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/r;->c:Lwy;

    invoke-virtual {v0}, Lwy;->a()Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/r$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/r$2;-><init>(Lcom/twitter/android/moments/ui/maker/r;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/r;->e:Lrx/j;

    .line 88
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/r;->a:Lcom/twitter/android/moments/ui/maker/s;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/r$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/r$3;-><init>(Lcom/twitter/android/moments/ui/maker/r;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/s;->b(Landroid/view/View$OnClickListener;)V

    .line 95
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lada;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/r;->a(Lada;)V

    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/r;->a:Lcom/twitter/android/moments/ui/maker/s;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/s;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/r;->e:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 100
    return-void
.end method
