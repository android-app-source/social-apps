.class Lcom/twitter/android/moments/ui/maker/ao$4;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/maker/ao;->a(Landroid/widget/FrameLayout;Lcom/twitter/util/math/Size;Lcom/twitter/util/math/Size;)Lrx/functions/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/d",
        "<",
        "Ljava/util/List",
        "<",
        "Lacg",
        "<+",
        "Lcom/twitter/model/moments/viewmodels/MomentPage;",
        ">;>;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/widget/FrameLayout;

.field final synthetic b:Lcom/twitter/util/math/Size;

.field final synthetic c:Lcom/twitter/util/math/Size;

.field final synthetic d:Lcom/twitter/android/moments/ui/maker/ao;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/maker/ao;Landroid/widget/FrameLayout;Lcom/twitter/util/math/Size;Lcom/twitter/util/math/Size;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/ao$4;->d:Lcom/twitter/android/moments/ui/maker/ao;

    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/ao$4;->a:Landroid/widget/FrameLayout;

    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/ao$4;->b:Lcom/twitter/util/math/Size;

    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/ao$4;->c:Lcom/twitter/util/math/Size;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;)Landroid/graphics/Bitmap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lacg",
            "<+",
            "Lcom/twitter/model/moments/viewmodels/MomentPage;",
            ">;>;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 133
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 134
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ao$4;->d:Lcom/twitter/android/moments/ui/maker/ao;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/ao;->a(Lcom/twitter/android/moments/ui/maker/ao;)Lcom/twitter/android/moments/ui/maker/bj;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/ao$4;->a:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/ao$4;->b:Lcom/twitter/util/math/Size;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/ao$4;->c:Lcom/twitter/util/math/Size;

    invoke-interface {v0, v1, v2, v3}, Lcom/twitter/android/moments/ui/maker/bj;->a(Landroid/view/View;Lcom/twitter/util/math/Size;Lcom/twitter/util/math/Size;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 136
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacg;

    .line 137
    invoke-interface {v0}, Lacg;->c()V

    goto :goto_0

    .line 139
    :cond_0
    return-object v1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 129
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/ao$4;->a(Ljava/util/List;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
