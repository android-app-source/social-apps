.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/h;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;
.implements Lcom/twitter/android/moments/ui/maker/viewdelegate/z;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;

.field private final c:Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;

.field private final d:Lcom/twitter/android/moments/ui/maker/viewdelegate/z;

.field private e:Lrx/j;


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;Landroid/support/v7/widget/RecyclerView$ItemDecoration;Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;Lcom/twitter/android/moments/ui/scroll/e;Lcom/twitter/android/moments/ui/maker/viewdelegate/z;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->a:Landroid/view/View;

    .line 60
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->b:Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;

    .line 61
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->c:Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;

    .line 62
    iput-object p6, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->d:Lcom/twitter/android/moments/ui/maker/viewdelegate/z;

    .line 63
    invoke-virtual {p2, p3}, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 64
    invoke-virtual {p2, p5}, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;->setOnScrollTouchListener(Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView$a;)V

    .line 65
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->c:Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;

    invoke-virtual {p2, v0}, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 66
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/h;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 35
    .line 36
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0315

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 38
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0316

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 40
    invoke-static {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/x;->a(II)Lcom/twitter/android/moments/ui/maker/viewdelegate/x;

    move-result-object v3

    .line 41
    new-instance v4, Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;

    invoke-direct {v4, p0, v5}, Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;-><init>(Landroid/content/Context;Z)V

    .line 43
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040196

    invoke-virtual {v0, v1, p1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 44
    const v0, 0x7f13047a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;

    .line 47
    invoke-static {p0}, Lcom/twitter/android/moments/ui/scroll/e;->a(Landroid/content/Context;)Lcom/twitter/android/moments/ui/scroll/e;

    move-result-object v5

    .line 49
    invoke-static {v2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a(Landroid/support/v7/widget/RecyclerView;)Lcom/twitter/android/moments/ui/maker/viewdelegate/af;

    move-result-object v6

    .line 50
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;-><init>(Landroid/view/View;Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;Landroid/support/v7/widget/RecyclerView$ItemDecoration;Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;Lcom/twitter/android/moments/ui/scroll/e;Lcom/twitter/android/moments/ui/maker/viewdelegate/z;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/viewdelegate/h;)Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->b:Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/maker/viewdelegate/h;)Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->c:Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;

    return-object v0
.end method

.method private c(I)Lcqw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcqw",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/h$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/h$1;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/h;I)V

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/maker/viewdelegate/h;)Lrx/j;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->e:Lrx/j;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/ui/maker/viewdelegate/y;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/y;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->d:Lcom/twitter/android/moments/ui/maker/viewdelegate/z;

    invoke-interface {v0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/z;->a(Lcom/twitter/android/moments/ui/maker/viewdelegate/y;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->b:Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->c:Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;->a(I)V

    .line 86
    :goto_0
    return-void

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->e:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 83
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->b:Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;

    .line 84
    invoke-static {v0}, Lcrf;->b(Landroid/view/View;)Lrx/c;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->c(I)Lcqw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->e:Lrx/j;

    goto :goto_0
.end method

.method public a(Landroid/support/v7/widget/RecyclerView$Adapter;)V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->b:Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/scroll/CustomScrollRecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 76
    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->a:Landroid/view/View;

    return-object v0
.end method

.method public b(I)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->d:Lcom/twitter/android/moments/ui/maker/viewdelegate/z;

    invoke-interface {v0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/z;->b(I)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->d:Lcom/twitter/android/moments/ui/maker/viewdelegate/z;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/z;->b()V

    .line 98
    return-void
.end method

.method public c()I
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->c:Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;->findFirstCompletelyVisibleItemPosition()I

    move-result v0

    .line 121
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->c:Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/scroll/CenteredHorizontalLinearLayoutManager;->findLastCompletelyVisibleItemPosition()I

    move-result v1

    .line 122
    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->e:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 127
    return-void
.end method
