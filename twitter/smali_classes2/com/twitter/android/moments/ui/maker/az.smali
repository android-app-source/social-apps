.class Lcom/twitter/android/moments/ui/maker/az;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/maker/az$a;
    }
.end annotation


# instance fields
.field private final a:Lwy;

.field private final b:Lzv;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/maker/az$a;Lwy;ZLzv;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/az;->a:Lwy;

    .line 35
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/az;->b:Lzv;

    .line 36
    invoke-virtual {p1, p3}, Lcom/twitter/android/moments/ui/maker/az$a;->a(Z)V

    .line 37
    new-instance v0, Lcom/twitter/android/moments/ui/maker/az$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/maker/az$1;-><init>(Lcom/twitter/android/moments/ui/maker/az;)V

    invoke-virtual {p1, v0}, Lcom/twitter/android/moments/ui/maker/az$a;->a(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 45
    return-void
.end method

.method static a(Landroid/preference/PreferenceActivity;Lwy;ZJ)Lcom/twitter/android/moments/ui/maker/az;
    .locals 3

    .prologue
    .line 23
    const-string/jumbo v0, "pref_nsfw_flag"

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 24
    new-instance v1, Lcom/twitter/android/moments/ui/maker/az$a;

    invoke-direct {v1, v0}, Lcom/twitter/android/moments/ui/maker/az$a;-><init>(Landroid/preference/CheckBoxPreference;)V

    .line 26
    invoke-static {p3, p4}, Lzv;->a(J)Lzv;

    move-result-object v0

    .line 27
    new-instance v2, Lcom/twitter/android/moments/ui/maker/az;

    invoke-direct {v2, v1, p1, p2, v0}, Lcom/twitter/android/moments/ui/maker/az;-><init>(Lcom/twitter/android/moments/ui/maker/az$a;Lwy;ZLzv;)V

    return-object v2
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/az;Z)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/maker/az;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 49
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/az;->a:Lwy;

    new-instance v0, Lcex$a;

    invoke-direct {v0}, Lcex$a;-><init>()V

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcex$a;->a(Ljava/lang/Boolean;)Lcex$a;

    move-result-object v0

    invoke-virtual {v0}, Lcex$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfd;

    invoke-virtual {v1, v0}, Lwy;->a(Lcfd;)V

    .line 50
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/az;->a:Lwy;

    invoke-virtual {v0}, Lwy;->c()Lrx/g;

    move-result-object v0

    invoke-static {}, Lcqw;->d()Lcqw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    .line 51
    if-eqz p1, :cond_0

    .line 52
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/az;->b:Lzv;

    invoke-virtual {v0}, Lzv;->h()V

    .line 54
    :cond_0
    return-void
.end method
