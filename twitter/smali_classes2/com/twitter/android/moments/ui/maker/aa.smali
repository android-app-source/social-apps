.class public Lcom/twitter/android/moments/ui/maker/aa;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/maker/viewdelegate/r$b;


# instance fields
.field private final a:Lajt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajt",
            "<",
            "Lcom/twitter/model/moments/viewmodels/g;",
            "Lcom/twitter/android/moments/ui/maker/ab;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lajx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajx",
            "<",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lwy;

.field private final d:Lzx;


# direct methods
.method public constructor <init>(Lajt;Lajx;Lwy;Lzx;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lajt",
            "<",
            "Lcom/twitter/model/moments/viewmodels/g;",
            "Lcom/twitter/android/moments/ui/maker/ab;",
            ">;",
            "Lajx",
            "<",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;",
            "Lwy;",
            "Lzx;",
            ")V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/aa;->a:Lajt;

    .line 37
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/aa;->b:Lajx;

    .line 38
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/aa;->c:Lwy;

    .line 39
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/aa;->d:Lzx;

    .line 40
    return-void
.end method


# virtual methods
.method public a(Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 3

    .prologue
    .line 45
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v0

    .line 46
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v1

    .line 47
    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/aa;->b:Lajx;

    invoke-virtual {v2, v0, v1}, Lajx;->a(II)V

    .line 48
    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/aa;->a:Lajt;

    invoke-virtual {v2, v0, v1}, Lajt;->a(II)V

    .line 49
    return-void
.end method

.method public b(Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 5

    .prologue
    .line 55
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v1

    .line 59
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aa;->b:Lajx;

    invoke-virtual {v0, v1}, Lajx;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/g;

    .line 60
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/g;->a()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/n;

    .line 61
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/n;->t()J

    move-result-wide v2

    .line 63
    if-lez v1, :cond_2

    .line 64
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aa;->b:Lajx;

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lajx;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/g;

    .line 65
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/g;->a()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/n;

    .line 66
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/n;->t()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcfi;->b(J)Lcfi;

    move-result-object v0

    move-object v1, v0

    .line 72
    :goto_1
    iget-object v4, p0, Lcom/twitter/android/moments/ui/maker/aa;->c:Lwy;

    new-instance v0, Lcfc$a;

    invoke-direct {v0}, Lcfc$a;-><init>()V

    .line 73
    invoke-virtual {v0, v2, v3}, Lcfc$a;->a(J)Lcfl$a;

    move-result-object v0

    check-cast v0, Lcfc$a;

    .line 74
    invoke-virtual {v0, v1}, Lcfc$a;->a(Lcfi;)Lcfg$a;

    move-result-object v0

    check-cast v0, Lcfc$a;

    .line 75
    invoke-virtual {v0}, Lcfc$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfd;

    .line 72
    invoke-virtual {v4, v0}, Lwy;->a(Lcfd;)V

    .line 76
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aa;->d:Lzx;

    invoke-virtual {v0, v2, v3}, Lzx;->c(J)V

    goto :goto_0

    .line 68
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aa;->b:Lajx;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lajx;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/g;

    .line 69
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/g;->a()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/n;

    .line 70
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/n;->t()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcfi;->a(J)Lcfi;

    move-result-object v0

    move-object v1, v0

    goto :goto_1
.end method
