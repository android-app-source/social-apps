.class public Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$a;
.super Lcom/twitter/app/common/base/h;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/base/h",
        "<",
        "Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$a;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:J

.field public final b:J


# direct methods
.method public constructor <init>(JJ)V
    .locals 3

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/twitter/app/common/base/h;-><init>()V

    .line 45
    iput-wide p1, p0, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$a;->a:J

    .line 46
    iput-wide p3, p0, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$a;->b:J

    .line 47
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "extra_moment_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 48
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "extra_account_id"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 49
    return-void
.end method

.method public static a()Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$a;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 60
    new-instance v0, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$a;

    invoke-direct {v0, v2, v3, v2, v3}, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$a;-><init>(JJ)V

    return-object v0
.end method

.method public static a(Landroid/content/Intent;)Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$a;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 53
    const-string/jumbo v0, "extra_moment_id"

    invoke-virtual {p0, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 54
    const-string/jumbo v2, "extra_account_id"

    invoke-virtual {p0, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 55
    new-instance v4, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$a;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$a;-><init>(JJ)V

    return-object v4
.end method
