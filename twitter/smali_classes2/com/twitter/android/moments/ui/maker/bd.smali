.class public Lcom/twitter/android/moments/ui/maker/bd;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lrx/j;

.field private final b:Lrx/j;

.field private final c:Lrx/j;

.field private final d:Lrx/j;

.field private final e:Lrx/j;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/bc;Lrx/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/ui/maker/bc;",
            "Lrx/c",
            "<",
            "Lcfd;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-class v0, Lcfa;

    invoke-static {p2, v0}, Lcre;->a(Lrx/c;Ljava/lang/Class;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/bd$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/maker/bd$1;-><init>(Lcom/twitter/android/moments/ui/maker/bd;Lcom/twitter/android/moments/ui/maker/bc;)V

    .line 28
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/bd;->a:Lrx/j;

    .line 34
    const-class v0, Lcex;

    invoke-static {p2, v0}, Lcre;->a(Lrx/c;Ljava/lang/Class;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/bd$2;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/maker/bd$2;-><init>(Lcom/twitter/android/moments/ui/maker/bd;Lcom/twitter/android/moments/ui/maker/bc;)V

    .line 35
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/bd;->b:Lrx/j;

    .line 41
    const-class v0, Lcff;

    invoke-static {p2, v0}, Lcre;->a(Lrx/c;Ljava/lang/Class;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/bd$3;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/maker/bd$3;-><init>(Lcom/twitter/android/moments/ui/maker/bd;Lcom/twitter/android/moments/ui/maker/bc;)V

    .line 42
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/bd;->c:Lrx/j;

    .line 48
    const-class v0, Lceu;

    invoke-static {p2, v0}, Lcre;->a(Lrx/c;Ljava/lang/Class;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/bd$4;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/maker/bd$4;-><init>(Lcom/twitter/android/moments/ui/maker/bd;Lcom/twitter/android/moments/ui/maker/bc;)V

    .line 49
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/bd;->d:Lrx/j;

    .line 55
    const-class v0, Lcet;

    invoke-static {p2, v0}, Lcre;->a(Lrx/c;Ljava/lang/Class;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/bd$5;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/maker/bd$5;-><init>(Lcom/twitter/android/moments/ui/maker/bd;Lcom/twitter/android/moments/ui/maker/bc;)V

    .line 56
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/bd;->e:Lrx/j;

    .line 63
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/bd;->a:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 67
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/bd;->c:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 68
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/bd;->d:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 69
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/bd;->b:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 70
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/bd;->e:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 71
    return-void
.end method
