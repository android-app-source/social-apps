.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/i;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/maker/viewdelegate/y;


# static fields
.field private static final a:Landroid/view/animation/Interpolator;


# instance fields
.field private final b:Lcom/twitter/android/moments/ui/maker/viewdelegate/aa;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/graphics/Rect;

.field private final e:Lrx/subjects/ReplaySubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/ReplaySubject",
            "<",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lcom/twitter/ui/anim/l;

    const v1, 0x3f0ccccd    # 0.55f

    invoke-direct {v0, v1}, Lcom/twitter/ui/anim/l;-><init>(F)V

    sput-object v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/i;->a:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(ILandroid/graphics/Rect;Lcom/twitter/android/moments/ui/maker/viewdelegate/aa;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {}, Lrx/subjects/ReplaySubject;->r()Lrx/subjects/ReplaySubject;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/i;->e:Lrx/subjects/ReplaySubject;

    .line 34
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/i;->f:Landroid/graphics/Rect;

    .line 38
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/i;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/aa;

    .line 39
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/i;->c:Ljava/util/List;

    .line 40
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/i;->d:Landroid/graphics/Rect;

    .line 41
    return-void
.end method


# virtual methods
.method public a()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/i;->e:Lrx/subjects/ReplaySubject;

    return-object v0
.end method

.method public a(Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;)V
    .locals 0

    .prologue
    .line 72
    return-void
.end method

.method public a(Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;Landroid/view/View;)V
    .locals 6

    .prologue
    const/16 v3, 0x1f3

    .line 53
    invoke-interface {p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;->a()Landroid/view/View;

    move-result-object v1

    .line 54
    invoke-interface {p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;->b()I

    move-result v2

    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/i;->c:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v2, v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/i;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/aa;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/i;->d:Landroid/graphics/Rect;

    sget-object v4, Lcom/twitter/android/moments/ui/maker/viewdelegate/i;->a:Landroid/view/animation/Interpolator;

    .line 56
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/moments/ui/maker/viewdelegate/aa;->a(Landroid/view/View;Landroid/graphics/Rect;ILandroid/view/animation/Interpolator;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/i;->e:Lrx/subjects/ReplaySubject;

    .line 57
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/d;)Lrx/j;

    .line 67
    :goto_0
    return-void

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/i;->f:Landroid/graphics/Rect;

    invoke-virtual {p3, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 60
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/i;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/aa;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/i;->f:Landroid/graphics/Rect;

    const/16 v4, 0x29a

    sget-object v5, Lcom/twitter/android/moments/ui/maker/viewdelegate/i;->a:Landroid/view/animation/Interpolator;

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/moments/ui/maker/viewdelegate/aa;->a(Landroid/view/View;Landroid/graphics/Rect;IILandroid/view/animation/Interpolator;)V

    goto :goto_0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/i;->c:Ljava/util/List;

    return-object v0
.end method
