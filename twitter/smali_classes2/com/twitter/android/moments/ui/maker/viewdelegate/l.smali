.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/l;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field private final b:Landroid/view/View;

.field private final c:Landroid/view/View;

.field private final d:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/l;->a:Landroid/view/ViewGroup;

    .line 52
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/l;->b:Landroid/view/View;

    .line 53
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/l;->c:Landroid/view/View;

    .line 54
    invoke-static {}, Lbsd;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/l;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 57
    :cond_0
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/l;->d:Landroid/support/v7/widget/RecyclerView;

    .line 58
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/l;
    .locals 6

    .prologue
    .line 29
    const v0, 0x7f0401cc

    const/4 v1, 0x0

    .line 30
    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 31
    const v1, 0x7f1304df

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/ui/widget/TwitterButton;

    .line 32
    invoke-static {}, Lbsd;->m()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 33
    const v2, 0x7f0a0559

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 34
    invoke-virtual {v1, v2}, Lcom/twitter/ui/widget/TwitterButton;->setText(Ljava/lang/CharSequence;)V

    .line 40
    :goto_0
    const v2, 0x7f1304e0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/twitter/ui/widget/TwitterButton;

    .line 41
    const v3, 0x7f0a055c

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    .line 42
    invoke-virtual {v2, v3}, Lcom/twitter/ui/widget/TwitterButton;->setText(Ljava/lang/CharSequence;)V

    .line 43
    const v3, 0x7f1304de

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/RecyclerView;

    .line 44
    new-instance v4, Landroid/support/v7/widget/GridLayoutManager;

    const/4 v5, 0x4

    invoke-direct {v4, p0, v5}, Landroid/support/v7/widget/GridLayoutManager;-><init>(Landroid/content/Context;I)V

    .line 45
    invoke-virtual {v3, v4}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 46
    new-instance v4, Lcom/twitter/android/moments/ui/maker/viewdelegate/l;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/twitter/android/moments/ui/maker/viewdelegate/l;-><init>(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;)V

    return-object v4

    .line 36
    :cond_0
    const v2, 0x7f0a055a

    .line 37
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 38
    invoke-virtual {v1, v2}, Lcom/twitter/ui/widget/TwitterButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/l;->a:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public a(Lajt;)V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/l;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p1}, Lajt;->b()Lajs;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 62
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/l;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    return-void
.end method

.method public b(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/l;->c:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    return-void
.end method
