.class Lcom/twitter/android/moments/ui/maker/ar;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/preference/PreferenceActivity;

.field private final b:Lxa;

.field private final c:J


# direct methods
.method constructor <init>(Landroid/preference/PreferenceActivity;Lxa;J)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/ar;->a:Landroid/preference/PreferenceActivity;

    .line 20
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/ar;->b:Lxa;

    .line 21
    iput-wide p3, p0, Lcom/twitter/android/moments/ui/maker/ar;->c:J

    .line 22
    return-void
.end method


# virtual methods
.method a(Lwy;Z)Lcom/twitter/android/moments/ui/maker/az;
    .locals 4

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ar;->a:Landroid/preference/PreferenceActivity;

    iget-wide v2, p0, Lcom/twitter/android/moments/ui/maker/ar;->c:J

    invoke-static {v0, p1, p2, v2, v3}, Lcom/twitter/android/moments/ui/maker/az;->a(Landroid/preference/PreferenceActivity;Lwy;ZJ)Lcom/twitter/android/moments/ui/maker/az;

    move-result-object v0

    return-object v0
.end method

.method public a(Lwy;Lcom/twitter/model/moments/f;)Lcom/twitter/android/moments/ui/maker/bh;
    .locals 4

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ar;->a:Landroid/preference/PreferenceActivity;

    iget-wide v2, p0, Lcom/twitter/android/moments/ui/maker/ar;->c:J

    invoke-static {v0, p1, p2, v2, v3}, Lcom/twitter/android/moments/ui/maker/bh;->a(Landroid/preference/PreferenceActivity;Lwy;Lcom/twitter/model/moments/f;J)Lcom/twitter/android/moments/ui/maker/bh;

    move-result-object v0

    return-object v0
.end method

.method a()Lcom/twitter/android/moments/ui/maker/w;
    .locals 4

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ar;->a:Landroid/preference/PreferenceActivity;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/ar;->b:Lxa;

    iget-wide v2, p0, Lcom/twitter/android/moments/ui/maker/ar;->c:J

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/moments/ui/maker/w;->a(Landroid/preference/PreferenceActivity;Lxa;J)Lcom/twitter/android/moments/ui/maker/w;

    move-result-object v0

    return-object v0
.end method

.method b(Lwy;Z)Lcom/twitter/android/moments/ui/maker/ah;
    .locals 4

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ar;->a:Landroid/preference/PreferenceActivity;

    iget-wide v2, p0, Lcom/twitter/android/moments/ui/maker/ar;->c:J

    invoke-static {v0, p1, p2, v2, v3}, Lcom/twitter/android/moments/ui/maker/ah;->a(Landroid/preference/PreferenceActivity;Lwy;ZJ)Lcom/twitter/android/moments/ui/maker/ah;

    move-result-object v0

    return-object v0
.end method

.method b()Lcom/twitter/android/moments/ui/maker/ba;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ar;->a:Landroid/preference/PreferenceActivity;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/as;->a(Landroid/preference/PreferenceActivity;)Lcom/twitter/android/moments/ui/maker/ba;

    move-result-object v0

    return-object v0
.end method
