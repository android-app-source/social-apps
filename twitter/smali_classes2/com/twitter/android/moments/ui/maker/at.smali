.class public Lcom/twitter/android/moments/ui/maker/at;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/twitter/library/media/manager/TwitterImageRequester$Factory;

.field private final d:Lcom/twitter/android/moments/viewmodels/l;

.field private e:Lcom/twitter/model/moments/viewmodels/MomentPage;

.field private f:Lcom/twitter/android/moments/viewmodels/k;

.field private g:Lcom/twitter/library/media/manager/TwitterImageRequester;

.field private h:Z


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;Lcom/twitter/android/moments/viewmodels/l;Landroid/content/Context;Lcom/twitter/library/media/manager/TwitterImageRequester$Factory;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/at;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;

    .line 55
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/at;->d:Lcom/twitter/android/moments/viewmodels/l;

    .line 56
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/at;->b:Landroid/content/Context;

    .line 57
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/at;->c:Lcom/twitter/library/media/manager/TwitterImageRequester$Factory;

    .line 58
    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;)Lcom/twitter/android/moments/ui/maker/at;
    .locals 3

    .prologue
    .line 47
    new-instance v0, Lcom/twitter/android/moments/ui/maker/at;

    invoke-static {}, Lcom/twitter/android/moments/viewmodels/l;->a()Lcom/twitter/android/moments/viewmodels/l;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/media/manager/TwitterImageRequester$Factory;

    invoke-direct {v2}, Lcom/twitter/library/media/manager/TwitterImageRequester$Factory;-><init>()V

    invoke-direct {v0, p1, v1, p0, v2}, Lcom/twitter/android/moments/ui/maker/at;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;Lcom/twitter/android/moments/viewmodels/l;Landroid/content/Context;Lcom/twitter/library/media/manager/TwitterImageRequester$Factory;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/at;)Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/at;->e:Lcom/twitter/model/moments/viewmodels/MomentPage;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/at;Z)Z
    .locals 0

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/twitter/android/moments/ui/maker/at;->h:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/maker/at;)Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/maker/at;->h:Z

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/maker/at;)Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/at;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/moments/ui/maker/at;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/at;->b:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/viewmodels/MomentPage;)V
    .locals 4

    .prologue
    .line 67
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/at;->e:Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 68
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/at;->d:Lcom/twitter/android/moments/viewmodels/l;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/at;->e:Lcom/twitter/model/moments/viewmodels/MomentPage;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/viewmodels/l;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/android/moments/viewmodels/k;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/at;->f:Lcom/twitter/android/moments/viewmodels/k;

    .line 69
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/at;->f:Lcom/twitter/android/moments/viewmodels/k;

    invoke-interface {v0}, Lcom/twitter/android/moments/viewmodels/k;->c()Lcom/twitter/model/moments/e;

    move-result-object v1

    .line 70
    iget-object v0, v1, Lcom/twitter/model/moments/e;->e:Lcom/twitter/model/moments/d;

    .line 71
    if-eqz v0, :cond_1

    .line 72
    invoke-virtual {v0}, Lcom/twitter/model/moments/d;->a()Landroid/graphics/Rect;

    move-result-object v2

    iget-object v0, v0, Lcom/twitter/model/moments/d;->f:Lcom/twitter/util/math/Size;

    invoke-static {v2, v0}, Lcom/twitter/util/math/c;->a(Landroid/graphics/Rect;Lcom/twitter/util/math/Size;)Lcom/twitter/util/math/c;

    move-result-object v0

    .line 73
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/at;->f:Lcom/twitter/android/moments/viewmodels/k;

    invoke-interface {v2}, Lcom/twitter/android/moments/viewmodels/k;->a()Lcom/twitter/media/request/a$a;

    move-result-object v2

    .line 74
    new-instance v3, Lcom/twitter/android/moments/ui/maker/at$1;

    invoke-direct {v3, p0, v0, v1}, Lcom/twitter/android/moments/ui/maker/at$1;-><init>(Lcom/twitter/android/moments/ui/maker/at;Lcom/twitter/util/math/c;Lcom/twitter/model/moments/e;)V

    invoke-virtual {v2, v3}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/media/request/b$b;)Lcom/twitter/media/request/b$a;

    .line 87
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/at;->g:Lcom/twitter/library/media/manager/TwitterImageRequester;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/at;->g:Lcom/twitter/library/media/manager/TwitterImageRequester;

    invoke-virtual {v0}, Lcom/twitter/library/media/manager/TwitterImageRequester;->c()Z

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/at;->c:Lcom/twitter/library/media/manager/TwitterImageRequester$Factory;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/at;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/TwitterImageRequester$Factory;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/TwitterImageRequester;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/at;->g:Lcom/twitter/library/media/manager/TwitterImageRequester;

    .line 91
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/at;->g:Lcom/twitter/library/media/manager/TwitterImageRequester;

    invoke-virtual {v2}, Lcom/twitter/media/request/a$a;->a()Lcom/twitter/media/request/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/TwitterImageRequester;->a(Lcom/twitter/media/request/a;)Z

    .line 92
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/at;->g:Lcom/twitter/library/media/manager/TwitterImageRequester;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/media/manager/TwitterImageRequester;->a(Z)V

    .line 93
    return-void

    .line 72
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/at;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;->aN_()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/twitter/model/moments/e;
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/at;->e:Lcom/twitter/model/moments/viewmodels/MomentPage;

    if-nez v0, :cond_0

    .line 98
    const/4 v0, 0x0

    .line 108
    :goto_0
    return-object v0

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/at;->f:Lcom/twitter/android/moments/viewmodels/k;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/k;

    .line 101
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/at;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;->b()Lcom/twitter/util/math/c;

    move-result-object v1

    .line 102
    if-nez v1, :cond_1

    .line 103
    new-instance v1, Lcom/twitter/model/moments/e$a;

    invoke-direct {v1}, Lcom/twitter/model/moments/e$a;-><init>()V

    .line 104
    invoke-interface {v0}, Lcom/twitter/android/moments/viewmodels/k;->b()Lcom/twitter/util/math/Size;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/model/moments/e$a;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/model/moments/e$a;

    move-result-object v0

    const/4 v1, 0x1

    .line 105
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/e$a;->a(Z)Lcom/twitter/model/moments/e$a;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Lcom/twitter/model/moments/e$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/e;

    goto :goto_0

    .line 109
    :cond_1
    invoke-interface {v0}, Lcom/twitter/android/moments/viewmodels/k;->b()Lcom/twitter/util/math/Size;

    move-result-object v0

    .line 108
    invoke-static {v1, v0}, Lcom/twitter/model/moments/e$a;->a(Lcom/twitter/util/math/c;Lcom/twitter/util/math/Size;)Lcom/twitter/model/moments/e$a;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Lcom/twitter/model/moments/e$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/e;

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/at;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;->c()V

    .line 115
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/maker/at;->h:Z

    return v0
.end method

.method public e()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/at;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;->d()Lrx/c;

    move-result-object v0

    return-object v0
.end method
