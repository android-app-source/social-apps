.class Lcom/twitter/android/moments/ui/maker/l$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcpv;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/maker/l;->a(Lcim;)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcpv",
        "<",
        "Lada;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcim;

.field final synthetic b:Lcom/twitter/android/moments/ui/maker/l;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/maker/l;Lcim;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/l$3;->b:Lcom/twitter/android/moments/ui/maker/l;

    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/l$3;->a:Lcim;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lada;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 66
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lada;->b()Lcom/twitter/model/moments/viewmodels/g;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 75
    :goto_0
    return v0

    .line 69
    :cond_1
    invoke-virtual {p1}, Lada;->b()Lcom/twitter/model/moments/viewmodels/g;

    move-result-object v0

    .line 70
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/g;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 71
    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/l$3;->a:Lcim;

    invoke-virtual {v3, v0}, Lcim;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 72
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 75
    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 63
    check-cast p1, Lada;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/l$3;->a(Lada;)Z

    move-result v0

    return v0
.end method
