.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/p;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Lcom/twitter/media/ui/image/MediaImageView;

.field private final c:Landroid/widget/EditText;

.field private final d:Landroid/widget/EditText;

.field private final e:Landroid/view/inputmethod/InputMethodManager;

.field private final f:Landroid/app/Activity;

.field private g:Lacx$a;


# direct methods
.method public constructor <init>(Landroid/widget/EditText;Landroid/widget/EditText;Landroid/view/View;Lcom/twitter/media/ui/image/MediaImageView;Landroid/view/inputmethod/InputMethodManager;Landroid/app/Activity;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->a:Landroid/view/View;

    .line 55
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->c:Landroid/widget/EditText;

    .line 56
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->d:Landroid/widget/EditText;

    .line 57
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->b:Lcom/twitter/media/ui/image/MediaImageView;

    .line 58
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->e:Landroid/view/inputmethod/InputMethodManager;

    .line 59
    iput-object p6, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->f:Landroid/app/Activity;

    .line 60
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/p$1;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/p;)V

    invoke-virtual {p7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->d:Landroid/widget/EditText;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/viewdelegate/p$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/p$2;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/p;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 77
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->c()V

    .line 78
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/app/Activity;)Lcom/twitter/android/moments/ui/maker/viewdelegate/p;
    .locals 8

    .prologue
    .line 40
    const v0, 0x7f0401ce

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 41
    const v0, 0x7f13029f

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 42
    const v0, 0x7f1304e3

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 43
    const v0, 0x7f1304e4

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 44
    const v0, 0x7f1304e2

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/twitter/media/ui/image/MediaImageView;

    .line 45
    const-string/jumbo v0, "input_method"

    .line 46
    invoke-virtual {p2, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/inputmethod/InputMethodManager;

    .line 47
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;

    move-object v6, p2

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;-><init>(Landroid/widget/EditText;Landroid/widget/EditText;Landroid/view/View;Lcom/twitter/media/ui/image/MediaImageView;Landroid/view/inputmethod/InputMethodManager;Landroid/app/Activity;Landroid/view/View;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/viewdelegate/p;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->d()V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/maker/viewdelegate/p;)Landroid/view/inputmethod/InputMethodManager;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->e:Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 82
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->c:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setHorizontallyScrolling(Z)V

    .line 83
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->c:Landroid/widget/EditText;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setMaxLines(I)V

    .line 84
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->d:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setHorizontallyScrolling(Z)V

    .line 85
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->d:Landroid/widget/EditText;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setMaxLines(I)V

    .line 86
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 105
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->g:Lacx$a;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->g:Lacx$a;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->d:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lacx$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lacx$a;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->g:Lacx$a;

    .line 102
    return-void
.end method

.method public a(Landroid/content/DialogInterface$OnClickListener;)V
    .locals 3

    .prologue
    .line 121
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->f:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a035e

    .line 122
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->f:Landroid/app/Activity;

    .line 123
    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a035c

    .line 124
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 123
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a03d5

    .line 125
    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 127
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 128
    return-void
.end method

.method public a(Lcom/twitter/media/request/a$a;)V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->b:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, p1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 142
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->d:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->d:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->d:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 113
    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->a:Landroid/view/View;

    return-object v0
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 89
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 90
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->e:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->c:Landroid/widget/EditText;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 91
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->e:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0, v3, v3}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInput(II)V

    .line 92
    return-void
.end method

.method public b(Landroid/content/DialogInterface$OnClickListener;)V
    .locals 3

    .prologue
    .line 131
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->f:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a035e

    .line 132
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->f:Landroid/app/Activity;

    .line 133
    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a035b

    .line 134
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 133
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a03d5

    .line 135
    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 136
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 137
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 138
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->c:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->c:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/p;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 118
    return-void
.end method
