.class public Lcom/twitter/android/moments/ui/maker/navigation/v;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/maker/navigation/ak;


# instance fields
.field private final b:Landroid/animation/ObjectAnimator;

.field private c:Lrx/subjects/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/a",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final d:I

.field private final e:I


# direct methods
.method public constructor <init>(Landroid/view/View;II)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput p2, p0, Lcom/twitter/android/moments/ui/maker/navigation/v;->d:I

    .line 31
    iput p3, p0, Lcom/twitter/android/moments/ui/maker/navigation/v;->e:I

    .line 32
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/maker/navigation/v;->a(Landroid/view/View;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/v;->b:Landroid/animation/ObjectAnimator;

    .line 33
    return-void
.end method

.method private a(Landroid/view/View;)Landroid/animation/ObjectAnimator;
    .locals 4

    .prologue
    .line 38
    sget-object v0, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 39
    new-instance v1, Lcom/twitter/android/moments/ui/maker/navigation/v$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/maker/navigation/v$1;-><init>(Lcom/twitter/android/moments/ui/maker/navigation/v;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 48
    iget v1, p0, Lcom/twitter/android/moments/ui/maker/navigation/v;->e:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 49
    iget v1, p0, Lcom/twitter/android/moments/ui/maker/navigation/v;->d:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 50
    return-object v0

    .line 38
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public static a(Lcom/twitter/android/moments/ui/maker/navigation/ai;)Lcom/twitter/android/moments/ui/maker/navigation/ak;
    .locals 4

    .prologue
    .line 25
    new-instance v0, Lcom/twitter/android/moments/ui/maker/navigation/v;

    invoke-interface {p0}, Lcom/twitter/android/moments/ui/maker/navigation/ai;->a()Landroid/view/View;

    move-result-object v1

    const/16 v2, 0xe1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/moments/ui/maker/navigation/v;-><init>(Landroid/view/View;II)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/navigation/v;)Lrx/subjects/a;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/v;->c:Lrx/subjects/a;

    return-object v0
.end method


# virtual methods
.method public a()Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    invoke-static {}, Lrx/subjects/a;->r()Lrx/subjects/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/v;->c:Lrx/subjects/a;

    .line 57
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/v;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 58
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/v;->c:Lrx/subjects/a;

    invoke-virtual {v0}, Lrx/subjects/a;->b()Lrx/g;

    move-result-object v0

    return-object v0
.end method
