.class Lcom/twitter/android/moments/ui/maker/q$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lakv;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/maker/q;-><init>(Lwy;Lcom/twitter/android/moments/ui/maker/y;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lzw;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lakv",
        "<",
        "Lcom/twitter/util/collection/k",
        "<",
        "Landroid/net/Uri;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/ui/maker/navigation/ah;

.field final synthetic b:Lcom/twitter/android/moments/ui/maker/q;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/maker/q;Lcom/twitter/android/moments/ui/maker/navigation/ah;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/q$1;->b:Lcom/twitter/android/moments/ui/maker/q;

    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/q$1;->a:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILcom/twitter/util/collection/k;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/twitter/util/collection/k",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/twitter/util/collection/k;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/q$1;->b:Lcom/twitter/android/moments/ui/maker/q;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/q;->a(Lcom/twitter/android/moments/ui/maker/q;)Lzw;

    move-result-object v0

    invoke-virtual {v0}, Lzw;->b()V

    .line 55
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/q$1;->b:Lcom/twitter/android/moments/ui/maker/q;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/q;->b(Lcom/twitter/android/moments/ui/maker/q;)Lwy;

    move-result-object v1

    new-instance v2, Lcfj;

    .line 56
    invoke-virtual {p2}, Lcom/twitter/util/collection/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    sget-object v3, Lcom/twitter/media/model/MediaType;->b:Lcom/twitter/media/model/MediaType;

    sget-object v4, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    invoke-direct {v2, v0, v3, v4}, Lcfj;-><init>(Landroid/net/Uri;Lcom/twitter/media/model/MediaType;Lcom/twitter/util/math/Size;)V

    invoke-virtual {v1, v2}, Lwy;->a(Lcfd;)V

    .line 57
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/q$1;->b:Lcom/twitter/android/moments/ui/maker/q;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/q;->b(Lcom/twitter/android/moments/ui/maker/q;)Lwy;

    move-result-object v0

    invoke-virtual {v0}, Lwy;->c()Lrx/g;

    move-result-object v0

    invoke-static {}, Lcqw;->d()Lcqw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    .line 58
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/q$1;->a:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/maker/navigation/ah;->a()V

    .line 60
    :cond_0
    return-void
.end method

.method public bridge synthetic a(ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 50
    check-cast p2, Lcom/twitter/util/collection/k;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/moments/ui/maker/q$1;->a(ILcom/twitter/util/collection/k;)V

    return-void
.end method
