.class Lcom/twitter/android/moments/ui/maker/ba;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/preference/PreferenceActivity;

.field private final b:Lcom/twitter/library/widget/StatusToolBar;

.field private final c:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/preference/PreferenceActivity;Lcom/twitter/library/widget/StatusToolBar;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/ba;->a:Landroid/preference/PreferenceActivity;

    .line 21
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/ba;->b:Lcom/twitter/library/widget/StatusToolBar;

    .line 22
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/ba;->c:Ljava/lang/String;

    .line 23
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/ba;)Landroid/preference/PreferenceActivity;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ba;->a:Landroid/preference/PreferenceActivity;

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ba;->b:Lcom/twitter/library/widget/StatusToolBar;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/ba;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/StatusToolBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 27
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ba;->b:Lcom/twitter/library/widget/StatusToolBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/StatusToolBar;->setDisplayShowHomeAsUpEnabled(Z)V

    .line 28
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ba;->b:Lcom/twitter/library/widget/StatusToolBar;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/ba$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/ba$1;-><init>(Lcom/twitter/android/moments/ui/maker/ba;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/StatusToolBar;->setOnToolBarItemSelectedListener(Lcmr$a;)V

    .line 40
    return-void
.end method
