.class Lcom/twitter/android/moments/ui/maker/at$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/media/request/b$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/maker/at;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/media/request/b$b",
        "<",
        "Lcom/twitter/media/request/ImageResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/util/math/c;

.field final synthetic b:Lcom/twitter/model/moments/e;

.field final synthetic c:Lcom/twitter/android/moments/ui/maker/at;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/maker/at;Lcom/twitter/util/math/c;Lcom/twitter/model/moments/e;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/at$1;->c:Lcom/twitter/android/moments/ui/maker/at;

    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/at$1;->a:Lcom/twitter/util/math/c;

    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/at$1;->b:Lcom/twitter/model/moments/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/media/request/ImageResponse;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 77
    invoke-virtual {p1}, Lcom/twitter/media/request/ImageResponse;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 78
    if-eqz v0, :cond_1

    .line 80
    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/at$1;->c:Lcom/twitter/android/moments/ui/maker/at;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    if-lt v3, v4, :cond_0

    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/at$1;->c:Lcom/twitter/android/moments/ui/maker/at;

    invoke-static {v3}, Lcom/twitter/android/moments/ui/maker/at;->a(Lcom/twitter/android/moments/ui/maker/at;)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/model/moments/viewmodels/MomentPage;->k()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-static {v2, v1}, Lcom/twitter/android/moments/ui/maker/at;->a(Lcom/twitter/android/moments/ui/maker/at;Z)Z

    .line 81
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/at$1;->c:Lcom/twitter/android/moments/ui/maker/at;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/maker/at;->c(Lcom/twitter/android/moments/ui/maker/at;)Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/at$1;->a:Lcom/twitter/util/math/c;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/at$1;->b:Lcom/twitter/model/moments/e;

    iget-boolean v3, v3, Lcom/twitter/model/moments/e;->h:Z

    iget-object v4, p0, Lcom/twitter/android/moments/ui/maker/at$1;->c:Lcom/twitter/android/moments/ui/maker/at;

    invoke-static {v4}, Lcom/twitter/android/moments/ui/maker/at;->b(Lcom/twitter/android/moments/ui/maker/at;)Z

    move-result v4

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ad;->a(Landroid/graphics/Bitmap;Lcom/twitter/util/math/c;ZZ)V

    .line 85
    :goto_0
    return-void

    .line 83
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/at$1;->c:Lcom/twitter/android/moments/ui/maker/at;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/at;->d(Lcom/twitter/android/moments/ui/maker/at;)Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0a04b5

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public bridge synthetic a(Lcom/twitter/media/request/ResourceResponse;)V
    .locals 0

    .prologue
    .line 74
    check-cast p1, Lcom/twitter/media/request/ImageResponse;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/at$1;->a(Lcom/twitter/media/request/ImageResponse;)V

    return-void
.end method
