.class public final Lcom/twitter/android/moments/ui/maker/navigation/aj$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/maker/navigation/aj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S1::",
        "Lcom/twitter/android/moments/ui/maker/navigation/ai;",
        "S2::",
        "Lcom/twitter/android/moments/ui/maker/navigation/ai;",
        "TS::",
        "Lcom/twitter/android/moments/ui/maker/navigation/am;",
        ">",
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/android/moments/ui/maker/navigation/aj",
        "<TS1;TS2;TTS;>;>;"
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/android/moments/ui/maker/navigation/ai;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TS1;"
        }
    .end annotation
.end field

.field private b:Lcom/twitter/android/moments/ui/maker/navigation/ai;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TS2;"
        }
    .end annotation
.end field

.field private c:Lcom/twitter/android/moments/ui/maker/navigation/am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TTS;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/navigation/aj$a;)Lcom/twitter/android/moments/ui/maker/navigation/ai;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/aj$a;->a:Lcom/twitter/android/moments/ui/maker/navigation/ai;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/maker/navigation/aj$a;)Lcom/twitter/android/moments/ui/maker/navigation/ai;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/aj$a;->b:Lcom/twitter/android/moments/ui/maker/navigation/ai;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/maker/navigation/aj$a;)Lcom/twitter/android/moments/ui/maker/navigation/am;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/aj$a;->c:Lcom/twitter/android/moments/ui/maker/navigation/am;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/ui/maker/navigation/ai;)Lcom/twitter/android/moments/ui/maker/navigation/aj$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS1;)",
            "Lcom/twitter/android/moments/ui/maker/navigation/aj$a",
            "<TS1;TS2;TTS;>;"
        }
    .end annotation

    .prologue
    .line 43
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/navigation/aj$a;->a:Lcom/twitter/android/moments/ui/maker/navigation/ai;

    .line 44
    return-object p0
.end method

.method public a(Lcom/twitter/android/moments/ui/maker/navigation/am;)Lcom/twitter/android/moments/ui/maker/navigation/aj$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTS;)",
            "Lcom/twitter/android/moments/ui/maker/navigation/aj$a",
            "<TS1;TS2;TTS;>;"
        }
    .end annotation

    .prologue
    .line 55
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/navigation/aj$a;->c:Lcom/twitter/android/moments/ui/maker/navigation/am;

    .line 56
    return-object p0
.end method

.method public b(Lcom/twitter/android/moments/ui/maker/navigation/ai;)Lcom/twitter/android/moments/ui/maker/navigation/aj$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS2;)",
            "Lcom/twitter/android/moments/ui/maker/navigation/aj$a",
            "<TS1;TS2;TTS;>;"
        }
    .end annotation

    .prologue
    .line 49
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/navigation/aj$a;->b:Lcom/twitter/android/moments/ui/maker/navigation/ai;

    .line 50
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/navigation/aj$a;->d()Lcom/twitter/android/moments/ui/maker/navigation/aj;

    move-result-object v0

    return-object v0
.end method

.method protected d()Lcom/twitter/android/moments/ui/maker/navigation/aj;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/android/moments/ui/maker/navigation/aj",
            "<TS1;TS2;TTS;>;"
        }
    .end annotation

    .prologue
    .line 62
    new-instance v0, Lcom/twitter/android/moments/ui/maker/navigation/aj;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/maker/navigation/aj;-><init>(Lcom/twitter/android/moments/ui/maker/navigation/aj$a;)V

    return-object v0
.end method
