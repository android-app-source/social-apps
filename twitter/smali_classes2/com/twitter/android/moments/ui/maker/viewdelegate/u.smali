.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/u;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/maker/viewdelegate/j;


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/viewdelegate/j;

.field private final b:Lcom/twitter/android/moments/ui/maker/viewdelegate/t;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/j;Lcom/twitter/android/moments/ui/maker/viewdelegate/t;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/u;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/j;

    .line 31
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/u;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/t;

    .line 32
    return-void
.end method

.method public static a(Lcom/twitter/android/moments/ui/maker/viewdelegate/v;Lcom/twitter/android/moments/ui/maker/viewdelegate/t;)Lcom/twitter/android/moments/ui/maker/viewdelegate/u;
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/u;

    .line 24
    invoke-static {p0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/q;->a(Lcom/twitter/android/moments/ui/maker/viewdelegate/z;)Lcom/twitter/android/moments/ui/maker/viewdelegate/q;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/u;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/j;Lcom/twitter/android/moments/ui/maker/viewdelegate/t;)V

    .line 23
    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/u;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/j;

    invoke-interface {v0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/j;->a(I)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public a(ILandroid/graphics/Rect;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/graphics/Rect;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/u;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/j;

    invoke-interface {v0, p1, p2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/j;->a(ILandroid/graphics/Rect;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/u;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/j;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/j;->a()V

    .line 57
    return-void
.end method

.method public b(ILandroid/graphics/Rect;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/graphics/Rect;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/u;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/j;

    invoke-interface {v0, p1, p2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/j;->b(ILandroid/graphics/Rect;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/u;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/t;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/t;->a()V

    .line 61
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/u;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/t;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/t;->b()V

    .line 65
    return-void
.end method
