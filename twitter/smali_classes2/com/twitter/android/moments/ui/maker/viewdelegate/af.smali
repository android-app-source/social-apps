.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/af;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/maker/viewdelegate/z;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/maker/viewdelegate/af$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;

.field private final b:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject",
            "<",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/twitter/android/moments/ui/maker/viewdelegate/y;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->b:Lrx/subjects/PublishSubject;

    .line 43
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;

    .line 44
    return-void
.end method

.method public static a(Landroid/support/v7/widget/RecyclerView;)Lcom/twitter/android/moments/ui/maker/viewdelegate/af;
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    invoke-direct {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/viewdelegate/af;)Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;

    return-object v0
.end method

.method private a()Lrx/functions/a;
    .locals 1

    .prologue
    .line 88
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af$2;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/af$2;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/af;)V

    return-object v0
.end method

.method private a(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)Lrx/functions/a;
    .locals 1

    .prologue
    .line 98
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af$3;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/af$3;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/af;Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/viewdelegate/af;Lcom/twitter/android/moments/ui/maker/viewdelegate/y;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->c(Lcom/twitter/android/moments/ui/maker/viewdelegate/y;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/viewdelegate/af;Ljava/util/List;)Z
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 152
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;->c()I

    move-result v1

    if-lez v1, :cond_3

    .line 153
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Lcom/twitter/util/collection/MutableList;->a(I)Ljava/util/List;

    move-result-object v1

    .line 154
    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 155
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;

    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;->c()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 156
    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;

    .line 157
    invoke-virtual {v3, v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;->b(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;->a(Landroid/view/View;)Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;

    move-result-object v2

    .line 158
    if-eqz v2, :cond_0

    invoke-interface {v2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;->b()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 159
    invoke-interface {v2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 155
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 162
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    .line 164
    :cond_2
    :goto_1
    return v0

    :cond_3
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;->c()I

    move-result v1

    if-lez v1, :cond_2

    const/4 v0, 0x1

    goto :goto_1
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/maker/viewdelegate/af;)Lrx/subjects/PublishSubject;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->b:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method private b(Lcom/twitter/android/moments/ui/maker/viewdelegate/y;)V
    .locals 3

    .prologue
    .line 107
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;->c()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 108
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;

    .line 109
    invoke-virtual {v2, v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;->b(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;->a(Landroid/view/View;)Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;

    move-result-object v1

    .line 110
    if-eqz v1, :cond_0

    .line 111
    invoke-interface {p1, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/y;->a(Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;)V

    .line 107
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 114
    :cond_1
    return-void
.end method

.method private c()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<-",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/y;",
            "+",
            "Lrx/c",
            "<",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 169
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af$4;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/af$4;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/af;)V

    return-object v0
.end method

.method private c(Lcom/twitter/android/moments/ui/maker/viewdelegate/y;)V
    .locals 6

    .prologue
    .line 117
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;

    .line 118
    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;->c()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v3

    .line 119
    const/4 v1, 0x0

    .line 120
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;->c()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;

    .line 122
    invoke-virtual {v4, v2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;->b(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;->a(Landroid/view/View;)Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;

    move-result-object v0

    .line 123
    if-eqz v0, :cond_2

    .line 124
    invoke-virtual {v3, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 125
    invoke-interface {p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/y;->b()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;->b()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 120
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 130
    :cond_0
    invoke-virtual {v3}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;

    .line 131
    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;

    invoke-virtual {v3}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;->d()Landroid/view/View;

    move-result-object v3

    invoke-interface {p1, v0, v1, v3}, Lcom/twitter/android/moments/ui/maker/viewdelegate/y;->a(Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;Landroid/view/View;)V

    goto :goto_2

    .line 133
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/ui/maker/viewdelegate/y;)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/y;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->b()V

    .line 50
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->c:Lcom/twitter/android/moments/ui/maker/viewdelegate/y;

    .line 52
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;->b()Lcom/twitter/android/moments/ui/maker/viewdelegate/ak;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/viewdelegate/af$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/af$1;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/af;Lcom/twitter/android/moments/ui/maker/viewdelegate/y;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ak;->b(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 71
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;->a()Landroid/support/v7/widget/RecyclerView$ItemAnimator;

    move-result-object v0

    .line 72
    invoke-static {p1}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v1

    .line 73
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a()Lrx/functions/a;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/g;->a(Lrx/functions/a;)Lrx/g;

    move-result-object v1

    .line 74
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->c()Lrx/functions/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/g;->b(Lrx/functions/d;)Lrx/c;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->b:Lrx/subjects/PublishSubject;

    .line 75
    invoke-virtual {v1, v2}, Lrx/c;->d(Lrx/c;)Lrx/c;

    move-result-object v1

    .line 76
    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)Lrx/functions/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lrx/c;->a(Lrx/functions/a;)Lrx/c;

    move-result-object v0

    .line 72
    return-object v0
.end method

.method public b(I)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ag;->a(I)Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;

    move-result-object v0

    .line 139
    if-eqz v0, :cond_0

    .line 140
    invoke-interface {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;->a()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/ui/k;->f(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 139
    :goto_0
    return-object v0

    .line 140
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->c:Lcom/twitter/android/moments/ui/maker/viewdelegate/y;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->c:Lcom/twitter/android/moments/ui/maker/viewdelegate/y;

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/af;->b(Lcom/twitter/android/moments/ui/maker/viewdelegate/y;)V

    .line 84
    :cond_0
    return-void
.end method
