.class public Lcom/twitter/android/moments/ui/maker/navigation/f;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/maker/navigation/ak;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/twitter/android/moments/ui/maker/navigation/ak;"
    }
.end annotation


# instance fields
.field private final b:Lcom/twitter/android/moments/ui/maker/navigation/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/moments/ui/maker/navigation/aj",
            "<",
            "Lcom/twitter/android/moments/ui/maker/navigation/ab",
            "<TT1;",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/j;",
            ">;",
            "Lcom/twitter/android/moments/ui/maker/navigation/ab",
            "<TT2;",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/j;",
            ">;",
            "Lcom/twitter/android/moments/ui/maker/navigation/am;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lrx/subjects/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/a",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/navigation/aj;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/ui/maker/navigation/aj",
            "<",
            "Lcom/twitter/android/moments/ui/maker/navigation/ab",
            "<TT1;",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/j;",
            ">;",
            "Lcom/twitter/android/moments/ui/maker/navigation/ab",
            "<TT2;",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/j;",
            ">;",
            "Lcom/twitter/android/moments/ui/maker/navigation/am;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {}, Lrx/subjects/a;->r()Lrx/subjects/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/f;->c:Lrx/subjects/a;

    .line 43
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/navigation/f;->b:Lcom/twitter/android/moments/ui/maker/navigation/aj;

    .line 44
    return-void
.end method

.method private a(Lcom/twitter/android/moments/ui/maker/navigation/ab;Lcom/twitter/android/moments/ui/maker/navigation/ab;)Lcqw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/ui/maker/navigation/ab",
            "<TT1;",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/j;",
            ">;",
            "Lcom/twitter/android/moments/ui/maker/navigation/ab",
            "<TT2;",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/j;",
            ">;)",
            "Lcqw",
            "<",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    new-instance v0, Lcom/twitter/android/moments/ui/maker/navigation/f$1;

    invoke-direct {v0, p0, p2, p1}, Lcom/twitter/android/moments/ui/maker/navigation/f$1;-><init>(Lcom/twitter/android/moments/ui/maker/navigation/f;Lcom/twitter/android/moments/ui/maker/navigation/ab;Lcom/twitter/android/moments/ui/maker/navigation/ab;)V

    return-object v0
.end method

.method private a(I)Lrx/functions/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lrx/functions/b",
            "<-",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    new-instance v0, Lcom/twitter/android/moments/ui/maker/navigation/f$3;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/moments/ui/maker/navigation/f$3;-><init>(Lcom/twitter/android/moments/ui/maker/navigation/f;I)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/navigation/f;I)Lrx/functions/b;
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/maker/navigation/f;->a(I)Lrx/functions/b;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/twitter/android/moments/ui/maker/navigation/ab;)Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/ui/maker/navigation/ab",
            "<TT1;",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/j;",
            ">;)",
            "Lrx/functions/d",
            "<-",
            "Lcom/twitter/android/moments/ui/maker/navigation/ab",
            "<TT2;",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/j;",
            ">;+",
            "Lrx/c",
            "<",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 92
    new-instance v0, Lcom/twitter/android/moments/ui/maker/navigation/f$2;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/moments/ui/maker/navigation/f$2;-><init>(Lcom/twitter/android/moments/ui/maker/navigation/f;Lcom/twitter/android/moments/ui/maker/navigation/ab;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/navigation/f;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/navigation/f;->d()V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/maker/navigation/f;)Lrx/subjects/a;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/f;->c:Lrx/subjects/a;

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/navigation/f;->b()Lcom/twitter/android/moments/ui/maker/viewdelegate/j;

    move-result-object v0

    .line 81
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/navigation/f;->c()Lcom/twitter/android/moments/ui/maker/viewdelegate/j;

    move-result-object v1

    .line 82
    invoke-interface {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/j;->a()V

    .line 83
    if-eqz v1, :cond_0

    .line 84
    invoke-interface {v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/j;->a()V

    .line 86
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lrx/g;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/f;->b:Lcom/twitter/android/moments/ui/maker/navigation/aj;

    iget-object v0, v0, Lcom/twitter/android/moments/ui/maker/navigation/aj;->a:Lcom/twitter/android/moments/ui/maker/navigation/ai;

    .line 50
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/maker/navigation/ab;

    .line 51
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/navigation/f;->b:Lcom/twitter/android/moments/ui/maker/navigation/aj;

    iget-object v1, v1, Lcom/twitter/android/moments/ui/maker/navigation/aj;->b:Lcom/twitter/android/moments/ui/maker/navigation/ai;

    check-cast v1, Lcom/twitter/android/moments/ui/maker/navigation/ab;

    .line 52
    invoke-static {v1}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v2

    .line 53
    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/maker/navigation/f;->a(Lcom/twitter/android/moments/ui/maker/navigation/ab;)Lrx/functions/d;

    move-result-object v3

    invoke-virtual {v2, v3}, Lrx/c;->f(Lrx/functions/d;)Lrx/c;

    move-result-object v2

    .line 54
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/moments/ui/maker/navigation/f;->a(Lcom/twitter/android/moments/ui/maker/navigation/ab;Lcom/twitter/android/moments/ui/maker/navigation/ab;)Lcqw;

    move-result-object v0

    invoke-virtual {v2, v0}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 56
    invoke-interface {v1}, Lcom/twitter/android/moments/ui/maker/navigation/ab;->a()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 57
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/f;->c:Lrx/subjects/a;

    invoke-virtual {v0}, Lrx/subjects/a;->b()Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/twitter/android/moments/ui/maker/viewdelegate/j;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/f;->b:Lcom/twitter/android/moments/ui/maker/navigation/aj;

    iget-object v0, v0, Lcom/twitter/android/moments/ui/maker/navigation/aj;->b:Lcom/twitter/android/moments/ui/maker/navigation/ai;

    check-cast v0, Lcom/twitter/android/moments/ui/maker/navigation/ab;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/maker/navigation/ab;->d()Lcom/twitter/android/moments/ui/maker/navigation/an;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/j;

    return-object v0
.end method

.method public c()Lcom/twitter/android/moments/ui/maker/viewdelegate/j;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/f;->b:Lcom/twitter/android/moments/ui/maker/navigation/aj;

    iget-object v0, v0, Lcom/twitter/android/moments/ui/maker/navigation/aj;->a:Lcom/twitter/android/moments/ui/maker/navigation/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/f;->b:Lcom/twitter/android/moments/ui/maker/navigation/aj;

    iget-object v0, v0, Lcom/twitter/android/moments/ui/maker/navigation/aj;->a:Lcom/twitter/android/moments/ui/maker/navigation/ai;

    check-cast v0, Lcom/twitter/android/moments/ui/maker/navigation/ab;

    .line 136
    invoke-interface {v0}, Lcom/twitter/android/moments/ui/maker/navigation/ab;->d()Lcom/twitter/android/moments/ui/maker/navigation/an;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/j;

    .line 135
    :goto_0
    return-object v0

    .line 136
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
