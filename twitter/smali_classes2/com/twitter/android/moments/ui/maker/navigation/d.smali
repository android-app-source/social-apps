.class public Lcom/twitter/android/moments/ui/maker/navigation/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/object/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/object/f",
        "<",
        "Lcom/twitter/android/moments/ui/maker/navigation/ah;",
        "Lcim;",
        "Lcom/twitter/android/moments/ui/maker/navigation/b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/app/common/base/BaseFragmentActivity;

.field private final b:Lcom/twitter/android/moments/ui/maker/ap$a;

.field private final c:Lwy;

.field private final d:Landroid/view/ViewGroup;

.field private final e:Lcom/twitter/android/moments/ui/maker/a;

.field private final f:Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;

.field private final g:J


# direct methods
.method public constructor <init>(Lcom/twitter/app/common/base/BaseFragmentActivity;Lwy;Lcom/twitter/android/moments/ui/maker/ap$a;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/maker/a;Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;J)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/navigation/d;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    .line 34
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/navigation/d;->c:Lwy;

    .line 35
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/navigation/d;->b:Lcom/twitter/android/moments/ui/maker/ap$a;

    .line 36
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/navigation/d;->d:Landroid/view/ViewGroup;

    .line 37
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/navigation/d;->e:Lcom/twitter/android/moments/ui/maker/a;

    .line 38
    iput-object p6, p0, Lcom/twitter/android/moments/ui/maker/navigation/d;->f:Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;

    .line 39
    iput-wide p7, p0, Lcom/twitter/android/moments/ui/maker/navigation/d;->g:J

    .line 40
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcim;)Lcom/twitter/android/moments/ui/maker/navigation/b;
    .locals 12

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/d;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/navigation/d;->d:Landroid/view/ViewGroup;

    invoke-static {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/maker/viewdelegate/h;

    move-result-object v3

    .line 48
    invoke-virtual {v3}, Lcom/twitter/android/moments/ui/maker/viewdelegate/h;->aN_()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/f;->a(Landroid/view/View;)Lcom/twitter/android/moments/ui/maker/viewdelegate/f;

    move-result-object v4

    .line 49
    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/navigation/d;->a:Lcom/twitter/app/common/base/BaseFragmentActivity;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/maker/navigation/d;->c:Lwy;

    iget-object v6, p0, Lcom/twitter/android/moments/ui/maker/navigation/d;->b:Lcom/twitter/android/moments/ui/maker/ap$a;

    iget-object v8, p0, Lcom/twitter/android/moments/ui/maker/navigation/d;->e:Lcom/twitter/android/moments/ui/maker/a;

    iget-object v9, p0, Lcom/twitter/android/moments/ui/maker/navigation/d;->f:Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;

    iget-wide v10, p0, Lcom/twitter/android/moments/ui/maker/navigation/d;->g:J

    move-object v1, p2

    move-object v7, p1

    invoke-static/range {v1 .. v11}, Lcom/twitter/android/moments/ui/maker/n;->a(Lcim;Lcom/twitter/app/common/base/BaseFragmentActivity;Lcom/twitter/android/moments/ui/maker/viewdelegate/h;Lcom/twitter/android/moments/ui/maker/viewdelegate/f;Lwy;Lcom/twitter/android/moments/ui/maker/ap$a;Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/ui/maker/a;Lcom/twitter/android/moments/ui/maker/MomentSettingsActivity$b;J)Lcom/twitter/android/moments/ui/maker/n;

    move-result-object v0

    .line 53
    invoke-static {v3, v4}, Lcom/twitter/android/moments/ui/maker/viewdelegate/g;->a(Lcom/twitter/android/moments/ui/maker/viewdelegate/h;Lcom/twitter/android/moments/ui/maker/viewdelegate/f;)Lcom/twitter/android/moments/ui/maker/viewdelegate/g;

    move-result-object v1

    .line 55
    new-instance v2, Lcom/twitter/android/moments/ui/maker/navigation/b;

    invoke-direct {v2, v0, v1}, Lcom/twitter/android/moments/ui/maker/navigation/b;-><init>(Lcom/twitter/android/moments/ui/maker/n;Lcom/twitter/android/moments/ui/maker/viewdelegate/g;)V

    return-object v2
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    check-cast p1, Lcom/twitter/android/moments/ui/maker/navigation/ah;

    check-cast p2, Lcim;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/moments/ui/maker/navigation/d;->a(Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcim;)Lcom/twitter/android/moments/ui/maker/navigation/b;

    move-result-object v0

    return-object v0
.end method
