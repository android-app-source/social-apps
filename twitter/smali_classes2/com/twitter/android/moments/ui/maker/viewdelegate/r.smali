.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/r;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/maker/viewdelegate/r$a;,
        Lcom/twitter/android/moments/ui/maker/viewdelegate/r$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/viewdelegate/r$a;

.field private final b:Landroid/support/v7/widget/helper/ItemTouchHelper;


# direct methods
.method public constructor <init>(Lcom/twitter/util/object/d;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/r$a;",
            "Landroid/support/v7/widget/helper/ItemTouchHelper;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/r$a;

    invoke-direct {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/r$a;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/r;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/r$a;

    .line 31
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/r;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/r$a;

    invoke-interface {p1, v0}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/helper/ItemTouchHelper;

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/r;->b:Landroid/support/v7/widget/helper/ItemTouchHelper;

    .line 32
    return-void
.end method

.method public static a()Lcom/twitter/android/moments/ui/maker/viewdelegate/r;
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/r;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/viewdelegate/r$1;

    invoke-direct {v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/r$1;-><init>()V

    invoke-direct {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/r;-><init>(Lcom/twitter/util/object/d;)V

    return-object v0
.end method


# virtual methods
.method public a(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/r;->b:Landroid/support/v7/widget/helper/ItemTouchHelper;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/helper/ItemTouchHelper;->attachToRecyclerView(Landroid/support/v7/widget/RecyclerView;)V

    .line 36
    return-void
.end method

.method public a(Lcom/twitter/android/moments/ui/maker/viewdelegate/r$b;)V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/r;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/r$a;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/r$a;->a(Lcom/twitter/android/moments/ui/maker/viewdelegate/r$b;)V

    .line 40
    return-void
.end method
