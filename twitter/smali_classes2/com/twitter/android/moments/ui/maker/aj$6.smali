.class Lcom/twitter/android/moments/ui/maker/aj$6;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/maker/aj;->b(JLcim;)Lcqw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Lcom/twitter/util/collection/m",
        "<",
        "Lcom/twitter/model/moments/viewmodels/b;",
        "Lcom/twitter/model/moments/b;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Lcim;

.field final synthetic c:Lcom/twitter/android/moments/ui/maker/aj;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/maker/aj;JLcim;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/aj$6;->c:Lcom/twitter/android/moments/ui/maker/aj;

    iput-wide p2, p0, Lcom/twitter/android/moments/ui/maker/aj$6;->a:J

    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/aj$6;->b:Lcim;

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/util/collection/m;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 171
    invoke-virtual {p1}, Lcom/twitter/util/collection/m;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/aj$6;->c:Lcom/twitter/android/moments/ui/maker/aj;

    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj$6;->c:Lcom/twitter/android/moments/ui/maker/aj;

    .line 173
    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/aj;->e(Lcom/twitter/android/moments/ui/maker/aj;)Lcom/twitter/util/object/d;

    move-result-object v2

    invoke-virtual {p1}, Lcom/twitter/util/collection/m;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/b;

    iget-object v0, v0, Lcom/twitter/model/moments/viewmodels/b;->a:Lcom/twitter/model/moments/viewmodels/a;

    invoke-interface {v2, v0}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwy;

    .line 172
    invoke-static {v1, v0}, Lcom/twitter/android/moments/ui/maker/aj;->a(Lcom/twitter/android/moments/ui/maker/aj;Lwy;)Lwy;

    .line 174
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj$6;->c:Lcom/twitter/android/moments/ui/maker/aj;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/aj$6;->c:Lcom/twitter/android/moments/ui/maker/aj;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/maker/aj;->g(Lcom/twitter/android/moments/ui/maker/aj;)Lcom/twitter/android/moments/ui/maker/ak$a;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/moments/ui/maker/aj$6;->a:J

    iget-object v4, p0, Lcom/twitter/android/moments/ui/maker/aj$6;->b:Lcim;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/maker/aj$6;->c:Lcom/twitter/android/moments/ui/maker/aj;

    .line 175
    invoke-static {v5}, Lcom/twitter/android/moments/ui/maker/aj;->f(Lcom/twitter/android/moments/ui/maker/aj;)Lwy;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/twitter/android/moments/ui/maker/ak$a;->a(JLcim;Lwy;)Lcom/twitter/android/moments/ui/maker/ak;

    move-result-object v1

    .line 174
    invoke-static {v0, v1}, Lcom/twitter/android/moments/ui/maker/aj;->a(Lcom/twitter/android/moments/ui/maker/aj;Lcom/twitter/android/moments/ui/maker/ak;)Lcom/twitter/android/moments/ui/maker/ak;

    .line 176
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj$6;->c:Lcom/twitter/android/moments/ui/maker/aj;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/aj;->o()Lanh;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/aj$6;->c:Lcom/twitter/android/moments/ui/maker/aj;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/maker/aj;->h(Lcom/twitter/android/moments/ui/maker/aj;)Lcom/twitter/android/moments/ui/maker/ak;

    move-result-object v1

    invoke-virtual {v0, v1}, Lanh;->a(Ljava/lang/Object;)Lanh;

    .line 177
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj$6;->c:Lcom/twitter/android/moments/ui/maker/aj;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/aj;->i(Lcom/twitter/android/moments/ui/maker/aj;)Lcom/twitter/android/moments/ui/maker/aj$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/aj$6;->c:Lcom/twitter/android/moments/ui/maker/aj;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/maker/aj;->h(Lcom/twitter/android/moments/ui/maker/aj;)Lcom/twitter/android/moments/ui/maker/ak;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/ak;->aN_()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/aj$a;->a(Landroid/view/View;)V

    .line 181
    :goto_0
    return-void

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/aj$6;->c:Lcom/twitter/android/moments/ui/maker/aj;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/aj;->b(Lcom/twitter/android/moments/ui/maker/aj;)Labg;

    move-result-object v0

    const v1, 0x7f0a0588

    invoke-virtual {v0, v1}, Labg;->a(I)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 167
    check-cast p1, Lcom/twitter/util/collection/m;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/maker/aj$6;->a(Lcom/twitter/util/collection/m;)V

    return-void
.end method
