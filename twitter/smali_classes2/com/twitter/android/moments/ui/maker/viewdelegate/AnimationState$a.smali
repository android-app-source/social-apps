.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState",
        "<",
        "Landroid/graphics/Rect;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;

.field private final d:Landroid/graphics/Rect;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$a;->c:Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;

    .line 75
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$a;->d:Landroid/graphics/Rect;

    .line 76
    return-void
.end method

.method public static a(Landroid/graphics/Rect;)Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$a;
    .locals 2

    .prologue
    .line 69
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$a;

    sget-object v1, Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;

    invoke-direct {v0, v1, p0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$a;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;Landroid/graphics/Rect;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$a;->c:Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$AnimationStateType;

    return-object v0
.end method

.method public b()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState$a;->d:Landroid/graphics/Rect;

    return-object v0
.end method
