.class public Lcom/twitter/android/moments/ui/maker/navigation/aj;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/maker/navigation/aj$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S1::",
        "Lcom/twitter/android/moments/ui/maker/navigation/ai;",
        "S2::",
        "Lcom/twitter/android/moments/ui/maker/navigation/ai;",
        "TS::",
        "Lcom/twitter/android/moments/ui/maker/navigation/am;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Lcom/twitter/android/moments/ui/maker/navigation/ai;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TS1;"
        }
    .end annotation
.end field

.field public final b:Lcom/twitter/android/moments/ui/maker/navigation/ai;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TS2;"
        }
    .end annotation
.end field

.field public final c:Lcom/twitter/android/moments/ui/maker/navigation/am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TTS;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/navigation/aj$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/ui/maker/navigation/aj$a",
            "<TS1;TS2;TTS;>;)V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p1}, Lcom/twitter/android/moments/ui/maker/navigation/aj$a;->a(Lcom/twitter/android/moments/ui/maker/navigation/aj$a;)Lcom/twitter/android/moments/ui/maker/navigation/ai;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/aj;->a:Lcom/twitter/android/moments/ui/maker/navigation/ai;

    .line 31
    invoke-static {p1}, Lcom/twitter/android/moments/ui/maker/navigation/aj$a;->b(Lcom/twitter/android/moments/ui/maker/navigation/aj$a;)Lcom/twitter/android/moments/ui/maker/navigation/ai;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/maker/navigation/ai;

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/aj;->b:Lcom/twitter/android/moments/ui/maker/navigation/ai;

    .line 32
    invoke-static {p1}, Lcom/twitter/android/moments/ui/maker/navigation/aj$a;->c(Lcom/twitter/android/moments/ui/maker/navigation/aj$a;)Lcom/twitter/android/moments/ui/maker/navigation/am;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/maker/navigation/am;

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/aj;->c:Lcom/twitter/android/moments/ui/maker/navigation/am;

    .line 33
    return-void
.end method

.method public static a()Lcom/twitter/android/moments/ui/maker/navigation/aj$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S1::",
            "Lcom/twitter/android/moments/ui/maker/navigation/ai;",
            "S2::",
            "Lcom/twitter/android/moments/ui/maker/navigation/ai;",
            "TS::",
            "Lcom/twitter/android/moments/ui/maker/navigation/am;",
            ">()",
            "Lcom/twitter/android/moments/ui/maker/navigation/aj$a",
            "<TS1;TS2;TTS;>;"
        }
    .end annotation

    .prologue
    .line 26
    new-instance v0, Lcom/twitter/android/moments/ui/maker/navigation/aj$a;

    invoke-direct {v0}, Lcom/twitter/android/moments/ui/maker/navigation/aj$a;-><init>()V

    return-object v0
.end method
