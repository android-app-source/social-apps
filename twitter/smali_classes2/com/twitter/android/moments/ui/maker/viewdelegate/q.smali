.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/q;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/maker/viewdelegate/j;


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/viewdelegate/z;

.field private final b:Lcom/twitter/util/object/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/f",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Rect;",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/y;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/util/object/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/f",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Rect;",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/y;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/z;Lcom/twitter/util/object/f;Lcom/twitter/util/object/f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/z;",
            "Lcom/twitter/util/object/f",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Rect;",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/y;",
            ">;",
            "Lcom/twitter/util/object/f",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Rect;",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/y;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/q;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/z;

    .line 45
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/q;->b:Lcom/twitter/util/object/f;

    .line 46
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/q;->c:Lcom/twitter/util/object/f;

    .line 47
    return-void
.end method

.method public static a(Lcom/twitter/android/moments/ui/maker/viewdelegate/z;)Lcom/twitter/android/moments/ui/maker/viewdelegate/q;
    .locals 3

    .prologue
    .line 20
    new-instance v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/q;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/viewdelegate/q$1;

    invoke-direct {v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/q$1;-><init>()V

    new-instance v2, Lcom/twitter/android/moments/ui/maker/viewdelegate/q$2;

    invoke-direct {v2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/q$2;-><init>()V

    invoke-direct {v0, p0, v1, v2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/q;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/z;Lcom/twitter/util/object/f;Lcom/twitter/util/object/f;)V

    return-object v0
.end method


# virtual methods
.method public a(I)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/q;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/z;

    invoke-interface {v0, p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/z;->b(I)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public a(ILandroid/graphics/Rect;)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/graphics/Rect;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/q;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/z;

    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/q;->b:Lcom/twitter/util/object/f;

    .line 58
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2, p2}, Lcom/twitter/util/object/f;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/y;

    .line 57
    invoke-interface {v1, v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/z;->a(Lcom/twitter/android/moments/ui/maker/viewdelegate/y;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/q;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/z;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/z;->b()V

    .line 82
    return-void
.end method

.method public b(ILandroid/graphics/Rect;)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/graphics/Rect;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/q;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/z;

    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/q;->c:Lcom/twitter/util/object/f;

    .line 70
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2, p2}, Lcom/twitter/util/object/f;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/y;

    .line 69
    invoke-interface {v1, v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/z;->a(Lcom/twitter/android/moments/ui/maker/viewdelegate/y;)Lrx/c;

    move-result-object v0

    return-object v0
.end method
