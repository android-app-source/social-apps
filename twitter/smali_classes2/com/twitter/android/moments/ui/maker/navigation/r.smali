.class public Lcom/twitter/android/moments/ui/maker/navigation/r;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/object/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/object/f",
        "<",
        "Lcom/twitter/android/moments/ui/maker/navigation/ah;",
        "Lcom/twitter/model/moments/viewmodels/g;",
        "Lcom/twitter/android/moments/ui/maker/navigation/p;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Landroid/view/ViewGroup;

.field private final c:Lwy;

.field private final d:J


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lwy;J)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/navigation/r;->a:Landroid/app/Activity;

    .line 22
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/navigation/r;->b:Landroid/view/ViewGroup;

    .line 23
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/navigation/r;->c:Lwy;

    .line 24
    iput-wide p4, p0, Lcom/twitter/android/moments/ui/maker/navigation/r;->d:J

    .line 25
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/model/moments/viewmodels/g;)Lcom/twitter/android/moments/ui/maker/navigation/p;
    .locals 6

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/r;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/navigation/r;->b:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/navigation/r;->c:Lwy;

    iget-wide v4, p0, Lcom/twitter/android/moments/ui/maker/navigation/r;->d:J

    move-object v3, p1

    .line 32
    invoke-static/range {v0 .. v5}, Lcom/twitter/android/moments/ui/maker/u;->a(Landroid/app/Activity;Landroid/view/ViewGroup;Lwy;Lcom/twitter/android/moments/ui/maker/navigation/ah;J)Lcom/twitter/android/moments/ui/maker/u;

    move-result-object v0

    .line 33
    new-instance v1, Lcom/twitter/android/moments/ui/maker/navigation/p;

    invoke-direct {v1, v0, p2}, Lcom/twitter/android/moments/ui/maker/navigation/p;-><init>(Lcom/twitter/android/moments/ui/maker/u;Lcom/twitter/model/moments/viewmodels/g;)V

    return-object v1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    check-cast p1, Lcom/twitter/android/moments/ui/maker/navigation/ah;

    check-cast p2, Lcom/twitter/model/moments/viewmodels/g;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/moments/ui/maker/navigation/r;->a(Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/model/moments/viewmodels/g;)Lcom/twitter/android/moments/ui/maker/navigation/p;

    move-result-object v0

    return-object v0
.end method
