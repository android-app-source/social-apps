.class public Lcom/twitter/android/moments/ui/maker/navigation/ad;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/maker/navigation/ah;


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/navigation/ae;

.field private final b:Landroid/app/Activity;

.field private final c:Landroid/view/ViewGroup;

.field private final d:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "Lcom/twitter/android/moments/ui/maker/navigation/ai;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/twitter/android/moments/ui/maker/navigation/al;

.field private final f:Lcom/twitter/android/moments/ui/maker/navigation/ac;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/maker/navigation/ac;Lcom/twitter/android/moments/ui/maker/navigation/ae;Lcom/twitter/android/moments/ui/maker/navigation/al;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/navigation/ad;->b:Landroid/app/Activity;

    .line 31
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/navigation/ad;->c:Landroid/view/ViewGroup;

    .line 32
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/navigation/ad;->f:Lcom/twitter/android/moments/ui/maker/navigation/ac;

    .line 33
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/navigation/ad;->a:Lcom/twitter/android/moments/ui/maker/navigation/ae;

    .line 34
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/ad;->d:Ljava/util/Deque;

    .line 35
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/navigation/ad;->e:Lcom/twitter/android/moments/ui/maker/navigation/al;

    .line 36
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/navigation/ad;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/ad;->c:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/navigation/ad;Z)Z
    .locals 0

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/twitter/android/moments/ui/maker/navigation/ad;->g:Z

    return p1
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 67
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/ad;->g:Z

    if-eqz v0, :cond_0

    .line 92
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/ad;->d:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->size()I

    move-result v0

    if-gt v0, v3, :cond_1

    .line 71
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/ad;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/ad;->d:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/maker/navigation/ai;

    .line 75
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/navigation/ad;->d:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/moments/ui/maker/navigation/ai;

    .line 76
    invoke-interface {v0}, Lcom/twitter/android/moments/ui/maker/navigation/ai;->c()Lcom/twitter/android/moments/ui/maker/navigation/am;

    move-result-object v2

    .line 77
    iput-boolean v3, p0, Lcom/twitter/android/moments/ui/maker/navigation/ad;->g:Z

    .line 78
    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/navigation/ad;->e:Lcom/twitter/android/moments/ui/maker/navigation/al;

    .line 79
    invoke-static {}, Lcom/twitter/android/moments/ui/maker/navigation/aj;->a()Lcom/twitter/android/moments/ui/maker/navigation/aj$a;

    move-result-object v4

    .line 80
    invoke-virtual {v4, v0}, Lcom/twitter/android/moments/ui/maker/navigation/aj$a;->a(Lcom/twitter/android/moments/ui/maker/navigation/ai;)Lcom/twitter/android/moments/ui/maker/navigation/aj$a;

    move-result-object v4

    .line 81
    invoke-virtual {v4, v1}, Lcom/twitter/android/moments/ui/maker/navigation/aj$a;->b(Lcom/twitter/android/moments/ui/maker/navigation/ai;)Lcom/twitter/android/moments/ui/maker/navigation/aj$a;

    move-result-object v1

    .line 82
    invoke-virtual {v1, v2}, Lcom/twitter/android/moments/ui/maker/navigation/aj$a;->a(Lcom/twitter/android/moments/ui/maker/navigation/am;)Lcom/twitter/android/moments/ui/maker/navigation/aj$a;

    move-result-object v1

    .line 83
    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/navigation/aj$a;->d()Lcom/twitter/android/moments/ui/maker/navigation/aj;

    move-result-object v1

    .line 79
    invoke-interface {v3, v1}, Lcom/twitter/android/moments/ui/maker/navigation/al;->a(Lcom/twitter/android/moments/ui/maker/navigation/aj;)Lrx/g;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/moments/ui/maker/navigation/ad$2;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/moments/ui/maker/navigation/ad$2;-><init>(Lcom/twitter/android/moments/ui/maker/navigation/ad;Lcom/twitter/android/moments/ui/maker/navigation/ai;)V

    .line 84
    invoke-virtual {v1, v2}, Lrx/g;->a(Lrx/i;)Lrx/j;

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/moments/ui/maker/navigation/NavigationKey;Lcom/twitter/android/moments/ui/maker/navigation/ag;)V
    .locals 5

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/ad;->g:Z

    if-eqz v0, :cond_0

    .line 63
    :goto_0
    return-void

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/ad;->a:Lcom/twitter/android/moments/ui/maker/navigation/ae;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/navigation/ad;->f:Lcom/twitter/android/moments/ui/maker/navigation/ac;

    invoke-virtual {v0, p0, p1, p2, v1}, Lcom/twitter/android/moments/ui/maker/navigation/ae;->a(Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/android/moments/ui/maker/navigation/NavigationKey;Lcom/twitter/android/moments/ui/maker/navigation/ag;Lcom/twitter/android/moments/ui/maker/navigation/ac;)Lcom/twitter/android/moments/ui/maker/navigation/ai;

    move-result-object v2

    .line 45
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/ad;->d:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/maker/navigation/ai;

    .line 46
    if-eqz v0, :cond_1

    .line 47
    invoke-interface {v0}, Lcom/twitter/android/moments/ui/maker/navigation/ai;->c()Lcom/twitter/android/moments/ui/maker/navigation/am;

    move-result-object v1

    .line 48
    :goto_1
    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/navigation/ad;->c:Landroid/view/ViewGroup;

    invoke-interface {v2}, Lcom/twitter/android/moments/ui/maker/navigation/ai;->a()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 49
    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/navigation/ad;->d:Ljava/util/Deque;

    invoke-interface {v3, v2}, Ljava/util/Deque;->push(Ljava/lang/Object;)V

    .line 50
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/twitter/android/moments/ui/maker/navigation/ad;->g:Z

    .line 51
    iget-object v3, p0, Lcom/twitter/android/moments/ui/maker/navigation/ad;->e:Lcom/twitter/android/moments/ui/maker/navigation/al;

    .line 52
    invoke-static {}, Lcom/twitter/android/moments/ui/maker/navigation/aj;->a()Lcom/twitter/android/moments/ui/maker/navigation/aj$a;

    move-result-object v4

    .line 53
    invoke-virtual {v4, v0}, Lcom/twitter/android/moments/ui/maker/navigation/aj$a;->a(Lcom/twitter/android/moments/ui/maker/navigation/ai;)Lcom/twitter/android/moments/ui/maker/navigation/aj$a;

    move-result-object v0

    .line 54
    invoke-virtual {v0, v2}, Lcom/twitter/android/moments/ui/maker/navigation/aj$a;->b(Lcom/twitter/android/moments/ui/maker/navigation/ai;)Lcom/twitter/android/moments/ui/maker/navigation/aj$a;

    move-result-object v0

    .line 55
    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/navigation/aj$a;->a(Lcom/twitter/android/moments/ui/maker/navigation/am;)Lcom/twitter/android/moments/ui/maker/navigation/aj$a;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/navigation/aj$a;->d()Lcom/twitter/android/moments/ui/maker/navigation/aj;

    move-result-object v0

    .line 52
    invoke-interface {v3, v0}, Lcom/twitter/android/moments/ui/maker/navigation/al;->a(Lcom/twitter/android/moments/ui/maker/navigation/aj;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/navigation/ad$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/navigation/ad$1;-><init>(Lcom/twitter/android/moments/ui/maker/navigation/ad;)V

    .line 57
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    goto :goto_0

    .line 47
    :cond_1
    sget-object v1, Lcom/twitter/android/moments/ui/maker/navigation/am;->b:Lcom/twitter/android/moments/ui/maker/navigation/am;

    goto :goto_1
.end method
