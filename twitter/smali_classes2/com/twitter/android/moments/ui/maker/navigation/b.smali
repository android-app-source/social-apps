.class public Lcom/twitter/android/moments/ui/maker/navigation/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/maker/navigation/ab;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/android/moments/ui/maker/navigation/ab",
        "<",
        "Lcom/twitter/model/moments/r;",
        "Lcom/twitter/android/moments/ui/maker/viewdelegate/g;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/n;

.field private final b:Lcom/twitter/android/moments/ui/maker/viewdelegate/g;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/n;Lcom/twitter/android/moments/ui/maker/viewdelegate/g;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/navigation/b;->a:Lcom/twitter/android/moments/ui/maker/n;

    .line 19
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/navigation/b;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/g;

    .line 20
    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/b;->a:Lcom/twitter/android/moments/ui/maker/n;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/n;->c()V

    .line 26
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/b;->a:Lcom/twitter/android/moments/ui/maker/n;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/n;->aN_()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/model/moments/r;)V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/b;->a:Lcom/twitter/android/moments/ui/maker/n;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/maker/n;->a(Lcom/twitter/model/moments/r;)V

    .line 54
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/b;->a:Lcom/twitter/android/moments/ui/maker/n;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/n;->d()V

    .line 32
    return-void
.end method

.method public c()Lcom/twitter/android/moments/ui/maker/navigation/am;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/twitter/android/moments/ui/maker/navigation/am;->b:Lcom/twitter/android/moments/ui/maker/navigation/am;

    return-object v0
.end method

.method public synthetic d()Lcom/twitter/android/moments/ui/maker/navigation/an;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/navigation/b;->e()Lcom/twitter/android/moments/ui/maker/viewdelegate/g;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/twitter/android/moments/ui/maker/viewdelegate/g;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/b;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/g;

    return-object v0
.end method

.method public f()Lcom/twitter/model/moments/r;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/b;->a:Lcom/twitter/android/moments/ui/maker/n;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/n;->e()Lcom/twitter/model/moments/r;

    move-result-object v0

    return-object v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/b;->a:Lcom/twitter/android/moments/ui/maker/n;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/n;->b()Z

    move-result v0

    return v0
.end method

.method public synthetic h()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/maker/navigation/b;->f()Lcom/twitter/model/moments/r;

    move-result-object v0

    return-object v0
.end method
