.class public Lcom/twitter/android/moments/ui/maker/navigation/y;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/object/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/object/f",
        "<",
        "Lcom/twitter/android/moments/ui/maker/navigation/ah;",
        "Lcom/twitter/model/moments/r;",
        "Lcom/twitter/android/moments/ui/maker/navigation/w;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/twitter/android/moments/ui/maker/ap$a;

.field private final c:Lwy;

.field private final d:Landroid/view/ViewGroup;

.field private final e:J


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lwy;Lcom/twitter/android/moments/ui/maker/ap$a;Landroid/view/ViewGroup;J)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/navigation/y;->a:Landroid/app/Activity;

    .line 29
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/navigation/y;->b:Lcom/twitter/android/moments/ui/maker/ap$a;

    .line 30
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/navigation/y;->c:Lwy;

    .line 31
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/navigation/y;->d:Landroid/view/ViewGroup;

    .line 32
    iput-wide p5, p0, Lcom/twitter/android/moments/ui/maker/navigation/y;->e:J

    .line 33
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/model/moments/r;)Lcom/twitter/android/moments/ui/maker/navigation/w;
    .locals 10

    .prologue
    .line 38
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/navigation/y;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/v;->a(Landroid/content/Context;)Lcom/twitter/android/moments/ui/maker/viewdelegate/v;

    move-result-object v3

    .line 40
    invoke-virtual {v3}, Lcom/twitter/android/moments/ui/maker/viewdelegate/v;->aN_()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/t;->a(Landroid/view/View;)Lcom/twitter/android/moments/ui/maker/viewdelegate/t;

    move-result-object v4

    .line 41
    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/navigation/y;->a:Landroid/app/Activity;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/maker/navigation/y;->c:Lwy;

    iget-object v6, p0, Lcom/twitter/android/moments/ui/maker/navigation/y;->b:Lcom/twitter/android/moments/ui/maker/ap$a;

    iget-wide v8, p0, Lcom/twitter/android/moments/ui/maker/navigation/y;->e:J

    move-object v1, p2

    move-object v7, p1

    invoke-static/range {v1 .. v9}, Lcom/twitter/android/moments/ui/maker/ad;->a(Lcom/twitter/model/moments/r;Landroid/content/Context;Lcom/twitter/android/moments/ui/maker/viewdelegate/v;Lcom/twitter/android/moments/ui/maker/viewdelegate/t;Lwy;Lcom/twitter/android/moments/ui/maker/ap$a;Lcom/twitter/android/moments/ui/maker/navigation/ah;J)Lcom/twitter/android/moments/ui/maker/ad;

    move-result-object v0

    .line 44
    invoke-static {v3, v4}, Lcom/twitter/android/moments/ui/maker/viewdelegate/u;->a(Lcom/twitter/android/moments/ui/maker/viewdelegate/v;Lcom/twitter/android/moments/ui/maker/viewdelegate/t;)Lcom/twitter/android/moments/ui/maker/viewdelegate/u;

    move-result-object v1

    .line 45
    new-instance v2, Lcom/twitter/android/moments/ui/maker/navigation/w;

    invoke-direct {v2, v0, v1}, Lcom/twitter/android/moments/ui/maker/navigation/w;-><init>(Lcom/twitter/android/moments/ui/maker/ad;Lcom/twitter/android/moments/ui/maker/viewdelegate/u;)V

    return-object v2
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    check-cast p1, Lcom/twitter/android/moments/ui/maker/navigation/ah;

    check-cast p2, Lcom/twitter/model/moments/r;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/moments/ui/maker/navigation/y;->a(Lcom/twitter/android/moments/ui/maker/navigation/ah;Lcom/twitter/model/moments/r;)Lcom/twitter/android/moments/ui/maker/navigation/w;

    move-result-object v0

    return-object v0
.end method
