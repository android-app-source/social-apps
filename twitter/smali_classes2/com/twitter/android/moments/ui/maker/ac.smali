.class public Lcom/twitter/android/moments/ui/maker/ac;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/viewdelegate/t;

.field private final b:Lcom/twitter/android/moments/ui/maker/navigation/ah;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/t;Lcom/twitter/android/moments/ui/maker/navigation/ah;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/ac;->b:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    .line 28
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/ac;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/t;

    .line 29
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ac;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/t;

    invoke-virtual {v0, p0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/t;->a(Landroid/view/View$OnClickListener;)V

    .line 30
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/ac;->a()V

    .line 31
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ac;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/t;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/t;->a()V

    .line 40
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ac;->b:Lcom/twitter/android/moments/ui/maker/navigation/ah;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/maker/navigation/ah;->a()V

    .line 36
    return-void
.end method
