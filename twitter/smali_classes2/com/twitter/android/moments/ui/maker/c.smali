.class public Lcom/twitter/android/moments/ui/maker/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/maker/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/maker/c$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/viewdelegate/b;

.field private final b:Lcom/twitter/android/moments/ui/maker/al;

.field private final c:Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;

.field private final d:Lrx/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/g",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Lajt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajt",
            "<",
            "Lcom/twitter/model/moments/viewmodels/g;",
            "Lcom/twitter/android/moments/ui/maker/d;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/content/res/Resources;

.field private final g:Lzt;

.field private final h:Lxm;

.field private final i:J

.field private j:Lrx/j;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/b;Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;Lcom/twitter/android/moments/ui/maker/al;Lrx/g;Lajt;Landroid/content/res/Resources;Lxm;JLzt;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/b;",
            "Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;",
            "Lcom/twitter/android/moments/ui/maker/al;",
            "Lrx/g",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;>;",
            "Lajt",
            "<",
            "Lcom/twitter/model/moments/viewmodels/g;",
            "Lcom/twitter/android/moments/ui/maker/d;",
            ">;",
            "Landroid/content/res/Resources;",
            "Lxm;",
            "J",
            "Lzt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object p6, p0, Lcom/twitter/android/moments/ui/maker/c;->f:Landroid/content/res/Resources;

    .line 93
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/c;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/b;

    .line 94
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/c;->b:Lcom/twitter/android/moments/ui/maker/al;

    .line 95
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/c;->c:Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;

    .line 96
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/c;->d:Lrx/g;

    .line 97
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/c;->e:Lajt;

    .line 98
    iput-object p10, p0, Lcom/twitter/android/moments/ui/maker/c;->g:Lzt;

    .line 99
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/c;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/b;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/c;->e:Lajt;

    invoke-virtual {v1}, Lajt;->b()Lajs;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->a(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 100
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/c;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/b;

    new-instance v1, Lcom/twitter/android/moments/ui/maker/c$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/c$1;-><init>(Lcom/twitter/android/moments/ui/maker/c;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->a(Landroid/view/View$OnClickListener;)V

    .line 106
    iput-object p7, p0, Lcom/twitter/android/moments/ui/maker/c;->h:Lxm;

    .line 107
    iput-wide p8, p0, Lcom/twitter/android/moments/ui/maker/c;->i:J

    .line 108
    return-void
.end method

.method public static a(Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;Landroid/content/Context;Lcom/twitter/android/moments/ui/maker/viewdelegate/b;Lcom/twitter/android/moments/ui/maker/ap$a;Lcom/twitter/android/moments/data/af;Lzt;Lanh;Lrx/g;Lxm;J)Lcom/twitter/android/moments/ui/maker/c;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;",
            "Landroid/content/Context;",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/b;",
            "Lcom/twitter/android/moments/ui/maker/ap$a;",
            "Lcom/twitter/android/moments/data/af",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;",
            "Lzt;",
            "Lanh;",
            "Lrx/g",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;>;",
            "Lxm;",
            "J)",
            "Lcom/twitter/android/moments/ui/maker/c;"
        }
    .end annotation

    .prologue
    .line 67
    new-instance v4, Lcom/twitter/android/moments/ui/maker/c$a;

    .line 68
    invoke-static/range {p1 .. p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    invoke-direct {v4, v5, v0, v1, v2}, Lcom/twitter/android/moments/ui/maker/c$a;-><init>(Landroid/view/LayoutInflater;Lcom/twitter/android/moments/ui/maker/ap$a;Lcom/twitter/android/moments/data/af;Lzt;)V

    .line 71
    invoke-static {}, Lcom/twitter/android/moments/ui/maker/al;->a()Lcom/twitter/android/moments/ui/maker/al;

    move-result-object v7

    .line 72
    new-instance v9, Lajt;

    new-instance v5, Lajw;

    move-object/from16 v0, p6

    invoke-direct {v5, v0, v7, v4}, Lajw;-><init>(Lanh;Lajv;Lajr;)V

    invoke-direct {v9, v5}, Lajt;-><init>(Lajs;)V

    .line 76
    new-instance v4, Lcom/twitter/android/moments/ui/maker/c;

    .line 82
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    move-object/from16 v5, p2

    move-object v6, p0

    move-object/from16 v8, p7

    move-object/from16 v11, p8

    move-wide/from16 v12, p9

    move-object/from16 v14, p5

    invoke-direct/range {v4 .. v14}, Lcom/twitter/android/moments/ui/maker/c;-><init>(Lcom/twitter/android/moments/ui/maker/viewdelegate/b;Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;Lcom/twitter/android/moments/ui/maker/al;Lrx/g;Lajt;Landroid/content/res/Resources;Lxm;JLzt;)V

    .line 76
    return-object v4
.end method

.method private a(Lcom/twitter/library/api/moments/maker/RecommendationType;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 181
    sget-object v0, Lcom/twitter/android/moments/ui/maker/c$4;->a:[I

    invoke-virtual {p1}, Lcom/twitter/library/api/moments/maker/RecommendationType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 192
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/c;->f:Landroid/content/res/Resources;

    const v1, 0x7f0a0502

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 183
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/c;->f:Landroid/content/res/Resources;

    const v1, 0x7f0a0505

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 186
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/c;->f:Landroid/content/res/Resources;

    const v1, 0x7f0a0503

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 189
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/c;->f:Landroid/content/res/Resources;

    const v1, 0x7f0a0504

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 181
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/c;Ljava/util/List;Lcom/twitter/model/moments/viewmodels/a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/moments/ui/maker/c;->a(Ljava/util/List;Lcom/twitter/model/moments/viewmodels/a;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/List;Lcom/twitter/model/moments/viewmodels/a;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;",
            "Lcom/twitter/model/moments/viewmodels/a;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 154
    invoke-virtual {p2}, Lcom/twitter/model/moments/viewmodels/a;->h()Ljava/util/List;

    move-result-object v2

    .line 155
    new-instance v3, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v3, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>(Ljava/util/Collection;)V

    .line 156
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/g;

    .line 157
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/moments/viewmodels/g;

    .line 158
    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/g;->a()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/model/moments/r;->e:Ljava/lang/Long;

    .line 159
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/g;->a()Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/model/moments/viewmodels/MomentPage;->i()Lcom/twitter/model/moments/r;

    move-result-object v6

    iget-object v6, v6, Lcom/twitter/model/moments/r;->e:Ljava/lang/Long;

    .line 158
    invoke-static {v1, v6}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 160
    invoke-interface {v3, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 165
    :cond_2
    return-object v3
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/c;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/maker/c;->a(Ljava/util/List;)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 169
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/c;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/b;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/c;->c:Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;->b()Lcom/twitter/library/api/moments/maker/RecommendationType;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/twitter/android/moments/ui/maker/c;->a(Lcom/twitter/library/api/moments/maker/RecommendationType;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->a(Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/c;->g:Lzt;

    invoke-virtual {v0}, Lzt;->b()V

    .line 177
    :goto_0
    return-void

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/c;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/b;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->c()V

    .line 174
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/c;->b:Lcom/twitter/android/moments/ui/maker/al;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/maker/al;->a(Ljava/util/List;)V

    .line 175
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/c;->e:Lajt;

    invoke-virtual {v0}, Lajt;->a()V

    goto :goto_0
.end method

.method private d()Lrx/functions/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/e",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/moments/viewmodels/g;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 138
    new-instance v0, Lcom/twitter/android/moments/ui/maker/c$3;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/maker/c$3;-><init>(Lcom/twitter/android/moments/ui/maker/c;)V

    return-object v0
.end method


# virtual methods
.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/c;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/b;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->aN_()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 4

    .prologue
    .line 121
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/c;->j:Lrx/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/c;->j:Lrx/j;

    invoke-interface {v0}, Lrx/j;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 133
    :goto_0
    return-void

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/c;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/b;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/viewdelegate/b;->b()V

    .line 125
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/c;->d:Lrx/g;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/c;->h:Lxm;

    iget-wide v2, p0, Lcom/twitter/android/moments/ui/maker/c;->i:J

    .line 126
    invoke-interface {v1, v2, v3}, Lxm;->a(J)Lrx/c;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lrx/c;->d(I)Lrx/c;

    move-result-object v1

    invoke-virtual {v1}, Lrx/c;->b()Lrx/g;

    move-result-object v1

    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/c;->d()Lrx/functions/e;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lrx/g;->a(Lrx/g;Lrx/functions/e;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/maker/c$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/c$2;-><init>(Lcom/twitter/android/moments/ui/maker/c;)V

    .line 127
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/c;->j:Lrx/j;

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/c;->j:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 203
    return-void
.end method
