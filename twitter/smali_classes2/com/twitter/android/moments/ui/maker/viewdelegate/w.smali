.class public Lcom/twitter/android/moments/ui/maker/viewdelegate/w;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/maker/viewdelegate/y;


# static fields
.field private static final a:Landroid/view/animation/Interpolator;


# instance fields
.field private final b:Lcom/twitter/android/moments/ui/maker/viewdelegate/aa;

.field private final c:Lrx/subjects/ReplaySubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/ReplaySubject",
            "<",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/graphics/Rect;

.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    new-instance v0, Lcom/twitter/ui/anim/l;

    const v1, 0x3f0ccccd    # 0.55f

    invoke-direct {v0, v1}, Lcom/twitter/ui/anim/l;-><init>(F)V

    sput-object v0, Lcom/twitter/android/moments/ui/maker/viewdelegate/w;->a:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(ILandroid/graphics/Rect;Lcom/twitter/android/moments/ui/maker/viewdelegate/aa;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {}, Lrx/subjects/ReplaySubject;->r()Lrx/subjects/ReplaySubject;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/w;->c:Lrx/subjects/ReplaySubject;

    .line 42
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/w;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/aa;

    .line 43
    iput p1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/w;->e:I

    .line 44
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/w;->d:Landroid/graphics/Rect;

    .line 45
    return-void
.end method


# virtual methods
.method public a()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/w;->c:Lrx/subjects/ReplaySubject;

    return-object v0
.end method

.method public a(Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 99
    invoke-interface {p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;->a()Landroid/view/View;

    move-result-object v0

    .line 100
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 101
    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 102
    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleX(F)V

    .line 103
    invoke-virtual {v0, v2}, Landroid/view/View;->setScaleY(F)V

    .line 104
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setPivotX(F)V

    .line 105
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setPivotY(F)V

    .line 106
    return-void
.end method

.method public a(Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/16 v4, 0x1f3

    .line 57
    invoke-interface {p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;->a()Landroid/view/View;

    move-result-object v1

    .line 58
    if-eqz p2, :cond_0

    invoke-interface {p2}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;->a()Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    .line 59
    :goto_0
    invoke-interface {p1}, Lcom/twitter/android/moments/ui/maker/viewdelegate/ab;->b()I

    move-result v0

    iget v5, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/w;->e:I

    if-ne v0, v5, :cond_1

    .line 61
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 64
    invoke-static {v3}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    .line 65
    invoke-static {v4, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-long v2, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lrx/c;->b(JLjava/util/concurrent/TimeUnit;)Lrx/c;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;

    .line 66
    invoke-static {v1}, Lcre;->a(Ljava/lang/Object;)Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;->a:Lcom/twitter/android/moments/ui/maker/viewdelegate/AnimationState;

    .line 67
    invoke-virtual {v0, v1}, Lrx/c;->d(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/w;->c:Lrx/subjects/ReplaySubject;

    .line 68
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/d;)Lrx/j;

    .line 95
    :goto_1
    return-void

    :cond_0
    move-object v2, v3

    .line 58
    goto :goto_0

    .line 71
    :cond_1
    invoke-static {v1, p3}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/twitter/util/ui/k;->f(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v3

    .line 73
    invoke-static {v2, p3}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/twitter/util/ui/k;->f(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 74
    iget-object v2, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/w;->d:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    div-float v6, v2, v5

    .line 77
    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v5, v6, v2

    .line 78
    const/4 v2, 0x0

    cmpl-float v2, v6, v2

    if-eqz v2, :cond_2

    .line 79
    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-float v2, v2

    .line 80
    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v7

    iget-object v8, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/w;->d:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->centerX()I

    move-result v8

    sub-int/2addr v7, v8

    int-to-float v7, v7

    div-float/2addr v7, v5

    add-float/2addr v2, v7

    iget v7, v3, Landroid/graphics/Rect;->left:I

    int-to-float v7, v7

    sub-float/2addr v2, v7

    .line 81
    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v7

    int-to-float v7, v7

    .line 82
    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    iget-object v8, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/w;->d:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->centerY()I

    move-result v8

    sub-int/2addr v0, v8

    int-to-float v0, v0

    div-float/2addr v0, v5

    add-float/2addr v0, v7

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    sub-float/2addr v0, v3

    move v3, v0

    move v5, v2

    .line 87
    :goto_2
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/w;->b:Lcom/twitter/android/moments/ui/maker/viewdelegate/aa;

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, v5, v3}, Landroid/graphics/PointF;-><init>(FF)V

    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3, v6, v6}, Landroid/graphics/PointF;-><init>(FF)V

    sget-object v6, Lcom/twitter/android/moments/ui/maker/viewdelegate/w;->a:Landroid/view/animation/Interpolator;

    move v5, v4

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/moments/ui/maker/viewdelegate/aa;->a(Landroid/view/View;Landroid/graphics/PointF;Landroid/graphics/PointF;IILandroid/view/animation/Interpolator;)V

    goto :goto_1

    .line 84
    :cond_2
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v2, v0

    .line 85
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    move v3, v0

    move v5, v2

    goto :goto_2
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    iget v0, p0, Lcom/twitter/android/moments/ui/maker/viewdelegate/w;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
