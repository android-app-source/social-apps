.class public Lcom/twitter/android/moments/ui/maker/ap;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/maker/ap$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/android/moments/ui/maker/j$a;

.field private final c:Lcom/twitter/android/moments/ui/maker/k;

.field private final d:Lrx/f;

.field private final e:Lrx/f;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/moments/ui/maker/k;Lcom/twitter/android/moments/ui/maker/j$a;Lrx/f;Lrx/f;)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/twitter/android/moments/ui/maker/ap;->a:Landroid/content/Context;

    .line 83
    iput-object p3, p0, Lcom/twitter/android/moments/ui/maker/ap;->b:Lcom/twitter/android/moments/ui/maker/j$a;

    .line 84
    iput-object p2, p0, Lcom/twitter/android/moments/ui/maker/ap;->c:Lcom/twitter/android/moments/ui/maker/k;

    .line 85
    iput-object p4, p0, Lcom/twitter/android/moments/ui/maker/ap;->d:Lrx/f;

    .line 86
    iput-object p5, p0, Lcom/twitter/android/moments/ui/maker/ap;->e:Lrx/f;

    .line 87
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/maker/ap;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ap;->a:Landroid/content/Context;

    return-object v0
.end method

.method public static a(Landroid/app/Activity;Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/util/math/Size;Lcom/twitter/util/math/Size;Lcom/twitter/android/moments/ui/maker/bc;Lyf;)Lcom/twitter/android/moments/ui/maker/ap;
    .locals 8

    .prologue
    .line 42
    new-instance v6, Lcom/twitter/android/moments/ui/maker/ap$1;

    invoke-direct {v6, p0}, Lcom/twitter/android/moments/ui/maker/ap$1;-><init>(Landroid/app/Activity;)V

    .line 49
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 50
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 51
    new-instance v2, Lbxo;

    invoke-direct {v2, v1}, Lbxo;-><init>(Landroid/content/res/Resources;)V

    .line 52
    new-instance v3, Lcom/twitter/android/moments/ui/fullscreen/cb;

    new-instance v4, Lcom/twitter/android/card/p;

    invoke-direct {v4}, Lcom/twitter/android/card/p;-><init>()V

    invoke-direct {v3, p0, v4, p5, v2}, Lcom/twitter/android/moments/ui/fullscreen/cb;-><init>(Landroid/app/Activity;Lcom/twitter/android/card/b;Lyf;Lbxo;)V

    .line 54
    new-instance v4, Lcom/twitter/android/moments/ui/fullscreen/as;

    .line 55
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v5

    new-instance v7, Lcom/twitter/android/moments/ui/maker/an;

    invoke-direct {v7}, Lcom/twitter/android/moments/ui/maker/an;-><init>()V

    invoke-direct {v4, v3, v5, v7}, Lcom/twitter/android/moments/ui/fullscreen/as;-><init>(Lcom/twitter/android/moments/ui/fullscreen/cb;Lrx/f;Lcom/twitter/android/moments/ui/maker/an;)V

    .line 57
    invoke-static {v1, v0, p0, v4, v2}, Lcom/twitter/android/moments/ui/maker/bg;->a(Landroid/content/res/Resources;Landroid/view/LayoutInflater;Landroid/app/Activity;Lcom/twitter/android/moments/ui/fullscreen/as;Lbxo;)Lcom/twitter/android/moments/ui/maker/bg;

    move-result-object v4

    .line 59
    new-instance v0, Lcom/twitter/android/moments/ui/maker/ao;

    .line 60
    invoke-static {}, Lcom/twitter/android/moments/ui/maker/bk;->a()Lcom/twitter/android/moments/ui/maker/bk;

    move-result-object v5

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/moments/ui/maker/ao;-><init>(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/util/math/Size;Lcom/twitter/util/math/Size;Lcom/twitter/android/moments/ui/maker/bl;Lcom/twitter/android/moments/ui/maker/bj;Lcom/twitter/util/object/d;)V

    .line 62
    new-instance v2, Lcom/twitter/android/moments/ui/maker/k;

    invoke-direct {v2, p1, v0, p4}, Lcom/twitter/android/moments/ui/maker/k;-><init>(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/android/moments/ui/maker/ao;Lcom/twitter/android/moments/ui/maker/bc;)V

    .line 64
    new-instance v0, Lcom/twitter/android/moments/ui/maker/ap;

    new-instance v3, Lcom/twitter/android/moments/ui/maker/j$a;

    invoke-direct {v3}, Lcom/twitter/android/moments/ui/maker/j$a;-><init>()V

    .line 65
    invoke-static {}, Lcom/twitter/android/moments/ui/maker/am;->a()Lrx/f;

    move-result-object v4

    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/moments/ui/maker/ap;-><init>(Landroid/content/Context;Lcom/twitter/android/moments/ui/maker/k;Lcom/twitter/android/moments/ui/maker/j$a;Lrx/f;Lrx/f;)V

    .line 64
    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/maker/ap;)Lcom/twitter/android/moments/ui/maker/j$a;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ap;->b:Lcom/twitter/android/moments/ui/maker/j$a;

    return-object v0
.end method

.method private c()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<",
            "Landroid/graphics/Bitmap;",
            "Lcom/twitter/android/moments/ui/maker/bb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    new-instance v0, Lcom/twitter/android/moments/ui/maker/ap$2;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/maker/ap$2;-><init>(Lcom/twitter/android/moments/ui/maker/ap;)V

    return-object v0
.end method


# virtual methods
.method public a()Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Lcom/twitter/android/moments/ui/maker/bb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ap;->c:Lcom/twitter/android/moments/ui/maker/k;

    .line 106
    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/k;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcws;->a()Lrx/f;

    move-result-object v0

    .line 107
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/ap;->c:Lcom/twitter/android/moments/ui/maker/k;

    .line 108
    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/maker/k;->b()Lrx/g;

    move-result-object v1

    .line 109
    invoke-virtual {v1, v0}, Lrx/g;->b(Lrx/f;)Lrx/g;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/maker/ap;->e:Lrx/f;

    .line 110
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/f;)Lrx/g;

    move-result-object v0

    .line 113
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/maker/ap;->c()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    .line 107
    return-object v0

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ap;->d:Lrx/f;

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/twitter/android/moments/ui/maker/ap;->c:Lcom/twitter/android/moments/ui/maker/k;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/maker/k;->a()Z

    move-result v0

    return v0
.end method
