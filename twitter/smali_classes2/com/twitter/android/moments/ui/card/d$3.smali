.class Lcom/twitter/android/moments/ui/card/d$3;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/card/d;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/ui/card/d;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/card/d;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/twitter/android/moments/ui/card/d$3;->a:Lcom/twitter/android/moments/ui/card/d;

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 120
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d$3;->a:Lcom/twitter/android/moments/ui/card/d;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/card/d;->a(Lcom/twitter/android/moments/ui/card/d;)Lcom/twitter/android/moments/ui/card/b;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/card/d$3;->a:Lcom/twitter/android/moments/ui/card/d;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/card/d;->c(Lcom/twitter/android/moments/ui/card/d;)Lcom/twitter/android/moments/viewmodels/m;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/moments/ui/card/d$3;->a:Lcom/twitter/android/moments/ui/card/d;

    .line 121
    invoke-static {v2}, Lcom/twitter/android/moments/ui/card/d;->d(Lcom/twitter/android/moments/ui/card/d;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v2

    .line 120
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/moments/ui/card/b;->a(Lcom/twitter/android/moments/viewmodels/m;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 122
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d$3;->a:Lcom/twitter/android/moments/ui/card/d;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/card/d;->e(Lcom/twitter/android/moments/ui/card/d;)Lcom/twitter/model/moments/a;

    move-result-object v0

    if-nez v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d$3;->a:Lcom/twitter/android/moments/ui/card/d;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/card/d;->b(Lcom/twitter/android/moments/ui/card/d;)Lcom/twitter/android/moments/ui/card/g;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/card/d$3;->a:Lcom/twitter/android/moments/ui/card/d;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/card/d;->c(Lcom/twitter/android/moments/ui/card/d;)Lcom/twitter/android/moments/viewmodels/m;

    move-result-object v1

    invoke-interface {v1}, Lcom/twitter/android/moments/viewmodels/m;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/card/g;->a(Ljava/lang/String;)V

    .line 125
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 117
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/card/d$3;->a(Ljava/lang/Boolean;)V

    return-void
.end method
