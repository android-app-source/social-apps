.class public Lcom/twitter/android/moments/ui/card/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/card/f;


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/card/b;

.field private final b:Lcom/twitter/android/moments/ui/card/g;

.field private c:Lrx/subjects/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/b",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/twitter/android/moments/viewmodels/m;

.field private e:Lcom/twitter/model/moments/a;

.field private f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/card/g;Lcom/twitter/android/moments/ui/card/b;)V
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Lrx/subjects/b;->r()Lrx/subjects/b;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/moments/ui/card/d;-><init>(Lcom/twitter/android/moments/ui/card/g;Lcom/twitter/android/moments/ui/card/b;Lrx/subjects/b;)V

    .line 51
    return-void
.end method

.method constructor <init>(Lcom/twitter/android/moments/ui/card/g;Lcom/twitter/android/moments/ui/card/b;Lrx/subjects/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/ui/card/g;",
            "Lcom/twitter/android/moments/ui/card/b;",
            "Lrx/subjects/b",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p2, p0, Lcom/twitter/android/moments/ui/card/d;->a:Lcom/twitter/android/moments/ui/card/b;

    .line 57
    iput-object p1, p0, Lcom/twitter/android/moments/ui/card/d;->b:Lcom/twitter/android/moments/ui/card/g;

    .line 58
    iput-object p3, p0, Lcom/twitter/android/moments/ui/card/d;->c:Lrx/subjects/b;

    .line 59
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/card/d;)Lcom/twitter/android/moments/ui/card/b;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->a:Lcom/twitter/android/moments/ui/card/b;

    return-object v0
.end method

.method public static a(Landroid/app/Activity;Lcom/twitter/library/client/Session;)Lcom/twitter/android/moments/ui/card/d;
    .locals 3

    .prologue
    .line 44
    new-instance v0, Lcom/twitter/android/moments/ui/card/d;

    new-instance v1, Lcom/twitter/android/moments/ui/card/g;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/card/g;-><init>(Landroid/content/Context;)V

    .line 45
    invoke-static {p0, p1}, Lcom/twitter/android/moments/ui/card/b;->a(Landroid/app/Activity;Lcom/twitter/library/client/Session;)Lcom/twitter/android/moments/ui/card/b;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/moments/ui/card/d;-><init>(Lcom/twitter/android/moments/ui/card/g;Lcom/twitter/android/moments/ui/card/b;)V

    .line 44
    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/card/d;)Lcom/twitter/android/moments/ui/card/g;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->b:Lcom/twitter/android/moments/ui/card/g;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/card/d;)Lcom/twitter/android/moments/viewmodels/m;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->d:Lcom/twitter/android/moments/viewmodels/m;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/moments/ui/card/d;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/moments/ui/card/d;)Lcom/twitter/model/moments/a;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->e:Lcom/twitter/model/moments/a;

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->d:Lcom/twitter/android/moments/viewmodels/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->d:Lcom/twitter/android/moments/viewmodels/m;

    invoke-interface {v0}, Lcom/twitter/android/moments/viewmodels/m;->b()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->b:Lcom/twitter/android/moments/ui/card/g;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/card/g;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->a:Lcom/twitter/android/moments/ui/card/b;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/card/b;->a(Landroid/view/View$OnClickListener;)V

    .line 145
    return-void
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/m;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 2

    .prologue
    .line 76
    iput-object p1, p0, Lcom/twitter/android/moments/ui/card/d;->d:Lcom/twitter/android/moments/viewmodels/m;

    .line 77
    iput-object p2, p0, Lcom/twitter/android/moments/ui/card/d;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 78
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->c:Lrx/subjects/b;

    invoke-static {}, Lcre;->f()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/b;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/card/d$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/card/d$1;-><init>(Lcom/twitter/android/moments/ui/card/d;Lcom/twitter/android/moments/viewmodels/m;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 85
    return-void
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 163
    if-nez p1, :cond_0

    .line 164
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->c:Lrx/subjects/b;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/b;->a(Ljava/lang/Object;)V

    .line 177
    :goto_0
    return-void

    .line 167
    :cond_0
    iget v0, p1, Lcom/twitter/model/core/TwitterUser;->U:I

    .line 168
    invoke-static {v0}, Lcom/twitter/model/core/g;->e(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 169
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->c:Lrx/subjects/b;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/b;->a(Ljava/lang/Object;)V

    .line 170
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->b:Lcom/twitter/android/moments/ui/card/g;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/card/g;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 171
    :cond_1
    invoke-static {v0}, Lcom/twitter/model/core/g;->f(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 172
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->c:Lrx/subjects/b;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/b;->a(Ljava/lang/Object;)V

    .line 173
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->b:Lcom/twitter/android/moments/ui/card/g;

    iget-object v1, p1, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/card/g;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 175
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->c:Lrx/subjects/b;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/b;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/moments/a;)V
    .locals 2

    .prologue
    .line 96
    iput-object p1, p0, Lcom/twitter/android/moments/ui/card/d;->e:Lcom/twitter/model/moments/a;

    .line 97
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->e:Lcom/twitter/model/moments/a;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->c:Lrx/subjects/b;

    invoke-static {}, Lcre;->f()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/b;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/card/d$2;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/card/d$2;-><init>(Lcom/twitter/android/moments/ui/card/d;Lcom/twitter/model/moments/a;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 105
    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->d:Lcom/twitter/android/moments/viewmodels/m;

    if-nez v0, :cond_0

    .line 127
    :goto_0
    return-void

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->c:Lrx/subjects/b;

    invoke-static {}, Lcre;->f()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/b;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/card/d$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/card/d$3;-><init>(Lcom/twitter/android/moments/ui/card/d;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->c:Lrx/subjects/b;

    invoke-virtual {v0}, Lrx/subjects/b;->by_()V

    .line 132
    invoke-static {}, Lrx/subjects/b;->r()Lrx/subjects/b;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->c:Lrx/subjects/b;

    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->d:Lcom/twitter/android/moments/viewmodels/m;

    .line 134
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->a:Lcom/twitter/android/moments/ui/card/b;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/card/b;->a()V

    .line 135
    return-void
.end method

.method public d()Lcom/twitter/library/widget/a;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->a:Lcom/twitter/android/moments/ui/card/b;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/card/b;->b()Lcom/twitter/library/widget/a;

    move-result-object v0

    return-object v0
.end method

.method public e()Landroid/view/View;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/d;->b:Lcom/twitter/android/moments/ui/card/g;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/card/g;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
