.class public Lcom/twitter/android/moments/ui/card/i;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/card/f;


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/fullscreen/bb;

.field private final b:Lcom/twitter/android/moments/ui/guide/y;

.field private c:Lcom/twitter/android/moments/viewmodels/m;

.field private d:Lcom/twitter/model/moments/a;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/fullscreen/bb;Lcom/twitter/android/moments/viewmodels/m;Lcom/twitter/android/moments/ui/guide/y;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/twitter/android/moments/ui/card/i;->a:Lcom/twitter/android/moments/ui/fullscreen/bb;

    .line 49
    iput-object p2, p0, Lcom/twitter/android/moments/ui/card/i;->c:Lcom/twitter/android/moments/viewmodels/m;

    .line 50
    iput-object p3, p0, Lcom/twitter/android/moments/ui/card/i;->b:Lcom/twitter/android/moments/ui/guide/y;

    .line 51
    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/twitter/android/moments/viewmodels/m;)Lcom/twitter/android/moments/ui/card/i;
    .locals 4

    .prologue
    .line 36
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 37
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-direct {v1, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 38
    const v2, 0x7f0401e1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 40
    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/f;->b(Landroid/view/View;)Lcom/twitter/android/moments/ui/guide/f;

    move-result-object v1

    .line 41
    invoke-static {v0}, Lcom/twitter/android/moments/ui/fullscreen/bb;->a(Landroid/view/View;)Lcom/twitter/android/moments/ui/fullscreen/bb;

    move-result-object v0

    .line 43
    new-instance v2, Lcom/twitter/android/moments/ui/card/i;

    invoke-direct {v2, v0, p1, v1}, Lcom/twitter/android/moments/ui/card/i;-><init>(Lcom/twitter/android/moments/ui/fullscreen/bb;Lcom/twitter/android/moments/viewmodels/m;Lcom/twitter/android/moments/ui/guide/y;)V

    return-object v2
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/i;->c:Lcom/twitter/android/moments/viewmodels/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/i;->c:Lcom/twitter/android/moments/viewmodels/m;

    invoke-interface {v0}, Lcom/twitter/android/moments/viewmodels/m;->b()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 111
    return-void
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/m;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/i;->c:Lcom/twitter/android/moments/viewmodels/m;

    if-nez v0, :cond_0

    .line 66
    :goto_0
    return-void

    .line 64
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/moments/ui/card/i;->c:Lcom/twitter/android/moments/viewmodels/m;

    .line 65
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/i;->a:Lcom/twitter/android/moments/ui/fullscreen/bb;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/card/i;->c:Lcom/twitter/android/moments/viewmodels/m;

    invoke-interface {v1}, Lcom/twitter/android/moments/viewmodels/m;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/bb;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;)V
    .locals 0

    .prologue
    .line 107
    return-void
.end method

.method public a(Lcom/twitter/model/moments/a;)V
    .locals 1

    .prologue
    .line 99
    if-eqz p1, :cond_0

    .line 100
    iput-object p1, p0, Lcom/twitter/android/moments/ui/card/i;->d:Lcom/twitter/model/moments/a;

    .line 101
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/i;->b:Lcom/twitter/android/moments/ui/guide/y;

    invoke-interface {v0, p1}, Lcom/twitter/android/moments/ui/guide/y;->a(Lcom/twitter/model/moments/a;)V

    .line 103
    :cond_0
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    .line 70
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/i;->d:Lcom/twitter/model/moments/a;

    if-nez v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/i;->b:Lcom/twitter/android/moments/ui/guide/y;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/card/i;->c:Lcom/twitter/android/moments/viewmodels/m;

    invoke-interface {v1}, Lcom/twitter/android/moments/viewmodels/m;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/moments/ui/guide/y;->a(Ljava/lang/String;)V

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/i;->a:Lcom/twitter/android/moments/ui/fullscreen/bb;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/bb;->b()Lcom/twitter/media/ui/image/MediaImageView;

    move-result-object v0

    .line 75
    iget-object v1, p0, Lcom/twitter/android/moments/ui/card/i;->c:Lcom/twitter/android/moments/viewmodels/m;

    .line 76
    invoke-interface {v1}, Lcom/twitter/android/moments/viewmodels/m;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/moments/ui/card/i;->c:Lcom/twitter/android/moments/viewmodels/m;

    invoke-interface {v2}, Lcom/twitter/android/moments/viewmodels/m;->g()Lcom/twitter/model/moments/d;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/moments/ui/card/i;->c:Lcom/twitter/android/moments/viewmodels/m;

    .line 77
    invoke-interface {v3}, Lcom/twitter/android/moments/viewmodels/m;->f()Lcom/twitter/util/math/Size;

    move-result-object v3

    .line 75
    invoke-static {v1, v0, v2, v3}, Lcom/twitter/android/moments/data/r;->a(Ljava/lang/String;Lcom/twitter/media/ui/image/MediaImageView;Lcom/twitter/model/moments/d;Lcom/twitter/util/math/Size;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 78
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/card/i;->c:Lcom/twitter/android/moments/viewmodels/m;

    .line 83
    return-void
.end method

.method public d()Lcom/twitter/library/widget/a;
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lcom/twitter/library/widget/a;->j:Lcom/twitter/library/widget/a;

    return-object v0
.end method

.method public e()Landroid/view/View;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/i;->a:Lcom/twitter/android/moments/ui/fullscreen/bb;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/fullscreen/bb;->aN_()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
