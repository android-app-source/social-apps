.class public Lcom/twitter/android/moments/ui/card/j;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/card/h;


# static fields
.field public static final a:Lcom/twitter/util/object/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/d",
            "<",
            "Landroid/app/Activity;",
            "Lcom/twitter/android/moments/ui/card/j;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/twitter/android/moments/ui/card/k;

.field private final c:Lcom/twitter/android/moments/ui/card/b;

.field private d:Lcom/twitter/android/moments/viewmodels/m;

.field private e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/twitter/android/moments/ui/card/j$1;

    invoke-direct {v0}, Lcom/twitter/android/moments/ui/card/j$1;-><init>()V

    sput-object v0, Lcom/twitter/android/moments/ui/card/j;->a:Lcom/twitter/util/object/d;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/android/moments/ui/card/k;Lcom/twitter/android/moments/ui/card/b;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/twitter/android/moments/ui/card/j;->b:Lcom/twitter/android/moments/ui/card/k;

    .line 44
    iput-object p2, p0, Lcom/twitter/android/moments/ui/card/j;->c:Lcom/twitter/android/moments/ui/card/b;

    .line 45
    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/twitter/library/client/Session;)Lcom/twitter/android/moments/ui/card/j;
    .locals 3

    .prologue
    .line 35
    new-instance v0, Lcom/twitter/android/moments/ui/card/j;

    new-instance v1, Lcom/twitter/android/moments/ui/card/k;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/card/k;-><init>(Landroid/content/Context;)V

    .line 37
    invoke-static {p0, p1}, Lcom/twitter/android/moments/ui/card/b;->b(Landroid/app/Activity;Lcom/twitter/library/client/Session;)Lcom/twitter/android/moments/ui/card/b;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/moments/ui/card/j;-><init>(Lcom/twitter/android/moments/ui/card/k;Lcom/twitter/android/moments/ui/card/b;)V

    .line 35
    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/j;->d:Lcom/twitter/android/moments/viewmodels/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/j;->d:Lcom/twitter/android/moments/viewmodels/m;

    invoke-interface {v0}, Lcom/twitter/android/moments/viewmodels/m;->b()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/j;->b:Lcom/twitter/android/moments/ui/card/k;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/card/k;->a(Landroid/view/View$OnClickListener;)V

    .line 99
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/j;->c:Lcom/twitter/android/moments/ui/card/b;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/card/b;->a(Landroid/view/View$OnClickListener;)V

    .line 100
    return-void
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/m;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/twitter/android/moments/ui/card/j;->d:Lcom/twitter/android/moments/viewmodels/m;

    .line 62
    iput-object p2, p0, Lcom/twitter/android/moments/ui/card/j;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 63
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/j;->c:Lcom/twitter/android/moments/ui/card/b;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/card/j;->d:Lcom/twitter/android/moments/viewmodels/m;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/card/j;->e:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/moments/ui/card/b;->a(Lcom/twitter/android/moments/viewmodels/m;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 73
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/j;->b:Lcom/twitter/android/moments/ui/card/k;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/card/j;->c:Lcom/twitter/android/moments/ui/card/b;

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/card/b;->c()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/card/k;->a(Landroid/view/View;)V

    .line 74
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/j;->c:Lcom/twitter/android/moments/ui/card/b;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/card/b;->a()V

    .line 68
    return-void
.end method

.method public d()Lcom/twitter/library/widget/a;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/j;->c:Lcom/twitter/android/moments/ui/card/b;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/card/b;->b()Lcom/twitter/library/widget/a;

    move-result-object v0

    return-object v0
.end method

.method public e()Landroid/view/View;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/j;->b:Lcom/twitter/android/moments/ui/card/k;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/card/k;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
