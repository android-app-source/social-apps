.class public Lcom/twitter/android/moments/ui/card/b;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/card/c;

.field private final b:Lcom/twitter/android/av/video/e$b;

.field private final c:Landroid/app/Activity;

.field private final d:Lcom/twitter/android/moments/ui/card/l;

.field private e:Lyf;

.field private f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private g:Lcom/twitter/android/moments/viewmodels/m;

.field private h:Lrx/j;

.field private i:Lcom/twitter/android/av/video/e;

.field private j:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/card/c;Lcom/twitter/android/moments/ui/card/l;Lcom/twitter/android/av/video/e$b;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/twitter/android/moments/ui/card/b;->a:Lcom/twitter/android/moments/ui/card/c;

    .line 70
    iput-object p2, p0, Lcom/twitter/android/moments/ui/card/b;->d:Lcom/twitter/android/moments/ui/card/l;

    .line 71
    iput-object p3, p0, Lcom/twitter/android/moments/ui/card/b;->b:Lcom/twitter/android/av/video/e$b;

    .line 72
    iput-object p4, p0, Lcom/twitter/android/moments/ui/card/b;->c:Landroid/app/Activity;

    .line 73
    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/twitter/library/client/Session;)Lcom/twitter/android/moments/ui/card/b;
    .locals 4

    .prologue
    .line 48
    new-instance v0, Lcom/twitter/android/moments/ui/card/l;

    invoke-direct {v0, p1}, Lcom/twitter/android/moments/ui/card/l;-><init>(Lcom/twitter/library/client/Session;)V

    .line 49
    new-instance v1, Lcom/twitter/android/moments/ui/card/b;

    new-instance v2, Lcom/twitter/android/moments/ui/card/c;

    invoke-direct {v2, p0}, Lcom/twitter/android/moments/ui/card/c;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/twitter/android/av/video/e$b;

    invoke-direct {v3}, Lcom/twitter/android/av/video/e$b;-><init>()V

    invoke-direct {v1, v2, v0, v3, p0}, Lcom/twitter/android/moments/ui/card/b;-><init>(Lcom/twitter/android/moments/ui/card/c;Lcom/twitter/android/moments/ui/card/l;Lcom/twitter/android/av/video/e$b;Landroid/app/Activity;)V

    return-object v1
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/card/b;Lcom/twitter/model/core/Tweet;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/card/b;->a(Lcom/twitter/model/core/Tweet;)V

    return-void
.end method

.method private a(Lcom/twitter/model/core/Tweet;)V
    .locals 6

    .prologue
    .line 179
    new-instance v4, Lcom/twitter/library/av/playback/TweetAVDataSource;

    invoke-direct {v4, p1}, Lcom/twitter/library/av/playback/TweetAVDataSource;-><init>(Lcom/twitter/model/core/Tweet;)V

    .line 180
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->i:Lcom/twitter/android/av/video/e;

    if-nez v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->g:Lcom/twitter/android/moments/viewmodels/m;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/m;

    .line 182
    iget-object v1, p0, Lcom/twitter/android/moments/ui/card/b;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 183
    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 184
    iget-object v1, p0, Lcom/twitter/android/moments/ui/card/b;->a:Lcom/twitter/android/moments/ui/card/c;

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/card/c;->b(Lcom/twitter/android/moments/viewmodels/m;)V

    .line 185
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->b:Lcom/twitter/android/av/video/e$b;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/card/b;->c:Landroid/app/Activity;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/card/b;->a:Lcom/twitter/android/moments/ui/card/c;

    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/card/c;->b()Landroid/view/ViewGroup;

    move-result-object v2

    iget-object v5, p0, Lcom/twitter/android/moments/ui/card/b;->j:Landroid/view/View$OnClickListener;

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/av/video/e$b;->a(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/av/playback/AVDataSource;Landroid/view/View$OnClickListener;)Lcom/twitter/android/av/video/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->i:Lcom/twitter/android/av/video/e;

    .line 187
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->i:Lcom/twitter/android/av/video/e;

    sget-object v1, Lbyo;->c:Lbyf;

    sget-object v2, Lcom/twitter/library/av/VideoPlayerView$Mode;->g:Lcom/twitter/library/av/VideoPlayerView$Mode;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/av/video/e;->a(Lbyf;Lcom/twitter/library/av/VideoPlayerView$Mode;)V

    .line 188
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->a:Lcom/twitter/android/moments/ui/card/c;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/card/b;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v1}, Lcom/twitter/android/av/video/e;->l()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/card/c;->a(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V

    .line 189
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->e()V

    .line 191
    :cond_0
    return-void
.end method

.method public static b(Landroid/app/Activity;Lcom/twitter/library/client/Session;)Lcom/twitter/android/moments/ui/card/b;
    .locals 4

    .prologue
    .line 61
    new-instance v0, Lcom/twitter/android/moments/ui/card/l;

    invoke-direct {v0, p1}, Lcom/twitter/android/moments/ui/card/l;-><init>(Lcom/twitter/library/client/Session;)V

    .line 62
    new-instance v1, Lcom/twitter/android/moments/ui/card/b;

    new-instance v2, Lcom/twitter/android/moments/ui/card/c;

    invoke-direct {v2, p0}, Lcom/twitter/android/moments/ui/card/c;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/twitter/android/av/video/e$a;

    invoke-direct {v3}, Lcom/twitter/android/av/video/e$a;-><init>()V

    invoke-direct {v1, v2, v0, v3, p0}, Lcom/twitter/android/moments/ui/card/b;-><init>(Lcom/twitter/android/moments/ui/card/c;Lcom/twitter/android/moments/ui/card/l;Lcom/twitter/android/av/video/e$b;Landroid/app/Activity;)V

    return-object v1
.end method

.method private d()V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->g:Lcom/twitter/android/moments/viewmodels/m;

    if-nez v0, :cond_0

    .line 105
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->g:Lcom/twitter/android/moments/viewmodels/m;

    invoke-interface {v0}, Lcom/twitter/android/moments/viewmodels/m;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->d:Lcom/twitter/android/moments/ui/card/l;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/card/b;->c:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/card/l;->a(Landroid/content/Context;)Lyf;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->e:Lyf;

    .line 101
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/card/b;->e()V

    goto :goto_0

    .line 103
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->a:Lcom/twitter/android/moments/ui/card/c;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/card/b;->g:Lcom/twitter/android/moments/viewmodels/m;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/card/c;->a(Lcom/twitter/android/moments/viewmodels/m;)V

    goto :goto_0
.end method

.method private e()V
    .locals 4

    .prologue
    .line 157
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->h:Lrx/j;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->h:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->e:Lyf;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyf;

    .line 161
    iget-object v1, p0, Lcom/twitter/android/moments/ui/card/b;->g:Lcom/twitter/android/moments/viewmodels/m;

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/moments/viewmodels/m;

    .line 162
    invoke-interface {v1}, Lcom/twitter/android/moments/viewmodels/m;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lyf;->a(J)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/card/b$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/card/b$1;-><init>(Lcom/twitter/android/moments/ui/card/b;)V

    .line 163
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->h:Lrx/j;

    .line 171
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 111
    iput-object v1, p0, Lcom/twitter/android/moments/ui/card/b;->g:Lcom/twitter/android/moments/viewmodels/m;

    .line 112
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->a()V

    .line 114
    iput-object v1, p0, Lcom/twitter/android/moments/ui/card/b;->i:Lcom/twitter/android/av/video/e;

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->h:Lrx/j;

    if-eqz v0, :cond_1

    .line 117
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->h:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 119
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->e:Lyf;

    if-eqz v0, :cond_2

    .line 120
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->e:Lyf;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 121
    iput-object v1, p0, Lcom/twitter/android/moments/ui/card/b;->e:Lyf;

    .line 123
    :cond_2
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 131
    iput-object p1, p0, Lcom/twitter/android/moments/ui/card/b;->j:Landroid/view/View$OnClickListener;

    .line 132
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->a:Lcom/twitter/android/moments/ui/card/c;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/card/c;->a()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/card/b;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    return-void
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/m;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 0

    .prologue
    .line 85
    iput-object p2, p0, Lcom/twitter/android/moments/ui/card/b;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 86
    iput-object p1, p0, Lcom/twitter/android/moments/ui/card/b;->g:Lcom/twitter/android/moments/viewmodels/m;

    .line 87
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/card/b;->d()V

    .line 88
    return-void
.end method

.method public b()Lcom/twitter/library/widget/a;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->i:Lcom/twitter/android/av/video/e;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/library/widget/a;->j:Lcom/twitter/library/widget/a;

    goto :goto_0
.end method

.method public c()Landroid/view/View;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/twitter/android/moments/ui/card/b;->a:Lcom/twitter/android/moments/ui/card/c;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/card/c;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
