.class public Lcom/twitter/android/moments/ui/card/e;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/object/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/object/d",
        "<",
        "Landroid/app/Activity;",
        "Lcom/twitter/android/moments/ui/card/d;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/card/l;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/card/l;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/twitter/android/moments/ui/card/e;->a:Lcom/twitter/android/moments/ui/card/l;

    .line 31
    return-void
.end method

.method public static a(Lcom/twitter/library/client/v;)Lcom/twitter/android/moments/ui/card/e;
    .locals 2

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 18
    new-instance v1, Lcom/twitter/android/moments/ui/card/l;

    invoke-direct {v1, v0}, Lcom/twitter/android/moments/ui/card/l;-><init>(Lcom/twitter/library/client/Session;)V

    .line 19
    new-instance v0, Lcom/twitter/android/moments/ui/card/e;

    invoke-direct {v0, v1}, Lcom/twitter/android/moments/ui/card/e;-><init>(Lcom/twitter/android/moments/ui/card/l;)V

    return-object v0
.end method


# virtual methods
.method public a(Landroid/app/Activity;)Lcom/twitter/android/moments/ui/card/d;
    .locals 4

    .prologue
    .line 36
    new-instance v0, Lcom/twitter/android/moments/ui/card/b;

    new-instance v1, Lcom/twitter/android/moments/ui/card/c;

    invoke-direct {v1, p1}, Lcom/twitter/android/moments/ui/card/c;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/twitter/android/moments/ui/card/e;->a:Lcom/twitter/android/moments/ui/card/l;

    new-instance v3, Lcom/twitter/android/av/video/e$b;

    invoke-direct {v3}, Lcom/twitter/android/av/video/e$b;-><init>()V

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/twitter/android/moments/ui/card/b;-><init>(Lcom/twitter/android/moments/ui/card/c;Lcom/twitter/android/moments/ui/card/l;Lcom/twitter/android/av/video/e$b;Landroid/app/Activity;)V

    .line 39
    new-instance v1, Lcom/twitter/android/moments/ui/card/d;

    new-instance v2, Lcom/twitter/android/moments/ui/card/g;

    invoke-direct {v2, p1}, Lcom/twitter/android/moments/ui/card/g;-><init>(Landroid/content/Context;)V

    invoke-direct {v1, v2, v0}, Lcom/twitter/android/moments/ui/card/d;-><init>(Lcom/twitter/android/moments/ui/card/g;Lcom/twitter/android/moments/ui/card/b;)V

    return-object v1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/card/e;->a(Landroid/app/Activity;)Lcom/twitter/android/moments/ui/card/d;

    move-result-object v0

    return-object v0
.end method
