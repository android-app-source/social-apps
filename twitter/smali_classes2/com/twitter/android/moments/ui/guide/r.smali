.class public Lcom/twitter/android/moments/ui/guide/r;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:F

.field private final b:Z

.field private final c:Z

.field private final d:Z


# direct methods
.method public constructor <init>(FZZZ)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput p1, p0, Lcom/twitter/android/moments/ui/guide/r;->a:F

    .line 18
    iput-boolean p2, p0, Lcom/twitter/android/moments/ui/guide/r;->b:Z

    .line 19
    iput-boolean p3, p0, Lcom/twitter/android/moments/ui/guide/r;->c:Z

    .line 20
    iput-boolean p4, p0, Lcom/twitter/android/moments/ui/guide/r;->d:Z

    .line 21
    return-void
.end method

.method public static a(Landroid/content/res/Resources;)Lcom/twitter/android/moments/ui/guide/r;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 37
    new-instance v0, Lcom/twitter/android/moments/ui/guide/r;

    const v1, 0x7f10000a

    .line 38
    invoke-virtual {p0, v1, v2, v2}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v1

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/twitter/android/moments/ui/guide/r;-><init>(FZZZ)V

    .line 37
    return-object v0
.end method

.method public static b(Landroid/content/res/Resources;)Lcom/twitter/android/moments/ui/guide/r;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 44
    new-instance v0, Lcom/twitter/android/moments/ui/guide/r;

    const v1, 0x7f10000b

    .line 45
    invoke-virtual {p0, v1, v3, v3}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/twitter/android/moments/ui/guide/r;-><init>(FZZZ)V

    .line 44
    return-object v0
.end method


# virtual methods
.method public a()F
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/twitter/android/moments/ui/guide/r;->a:F

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/r;->b:Z

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/r;->c:Z

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/r;->d:Z

    return v0
.end method
