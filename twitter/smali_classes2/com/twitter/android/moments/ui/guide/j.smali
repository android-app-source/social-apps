.class Lcom/twitter/android/moments/ui/guide/j;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/a;

.field private final b:Lcom/twitter/android/moments/data/j;

.field private final c:Lcom/twitter/android/moments/data/j;

.field private final d:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/twitter/android/moments/ui/guide/ae;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lrx/j;

.field private f:Lrx/j;

.field private g:J

.field private h:Z


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/a;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/j;Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/ui/a;",
            "Lcom/twitter/android/moments/data/j;",
            "Lcom/twitter/android/moments/data/j;",
            "Ljava/util/Collection",
            "<",
            "Lcom/twitter/android/moments/ui/guide/ae;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/j;->a:Lcom/twitter/android/moments/ui/a;

    .line 36
    iput-object p2, p0, Lcom/twitter/android/moments/ui/guide/j;->b:Lcom/twitter/android/moments/data/j;

    .line 37
    iput-object p3, p0, Lcom/twitter/android/moments/ui/guide/j;->c:Lcom/twitter/android/moments/data/j;

    .line 38
    iput-object p4, p0, Lcom/twitter/android/moments/ui/guide/j;->d:Ljava/util/Collection;

    .line 39
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/guide/j;)Lcom/twitter/android/moments/data/j;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/j;->b:Lcom/twitter/android/moments/data/j;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/guide/j;J)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/moments/ui/guide/j;->b(J)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/guide/j;)Lcom/twitter/android/moments/ui/a;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/j;->a:Lcom/twitter/android/moments/ui/a;

    return-object v0
.end method

.method private b(J)V
    .locals 5

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/j;->b:Lcom/twitter/android/moments/data/j;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/moments/data/j;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/j;->c:Lcom/twitter/android/moments/data/j;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/moments/data/j;->a(J)Z

    move-result v0

    if-nez v0, :cond_2

    .line 72
    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    .line 73
    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    .line 79
    :goto_0
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/j;->h:Z

    if-eq v1, v0, :cond_3

    .line 80
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/j;->d:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/guide/ae;

    .line 81
    invoke-interface {v0}, Lcom/twitter/android/moments/ui/guide/ae;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 84
    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    goto :goto_1

    .line 75
    :cond_2
    const v1, 0x3f333333    # 0.7f

    .line 76
    const/4 v0, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 89
    :cond_3
    iput-boolean v1, p0, Lcom/twitter/android/moments/ui/guide/j;->h:Z

    .line 90
    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 3

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/twitter/android/moments/ui/guide/j;->g:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_2

    .line 43
    iput-wide p1, p0, Lcom/twitter/android/moments/ui/guide/j;->g:J

    .line 44
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/j;->e:Lrx/j;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/j;->e:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/j;->b:Lcom/twitter/android/moments/data/j;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/data/j;->a(Ljava/lang/Long;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/guide/j$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/twitter/android/moments/ui/guide/j$1;-><init>(Lcom/twitter/android/moments/ui/guide/j;J)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/j;->e:Lrx/j;

    .line 55
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/j;->f:Lrx/j;

    if-eqz v0, :cond_1

    .line 56
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/j;->f:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/j;->c:Lcom/twitter/android/moments/data/j;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/data/j;->a(Ljava/lang/Long;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/guide/j$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/twitter/android/moments/ui/guide/j$2;-><init>(Lcom/twitter/android/moments/ui/guide/j;J)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/j;->f:Lrx/j;

    .line 66
    :cond_2
    return-void
.end method
