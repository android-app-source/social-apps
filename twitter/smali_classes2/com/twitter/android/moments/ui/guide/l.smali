.class public Lcom/twitter/android/moments/ui/guide/l;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/guide/l$a;,
        Lcom/twitter/android/moments/ui/guide/l$b;
    }
.end annotation


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:Landroid/widget/ListView;

.field private final c:Lcom/twitter/android/moments/ui/guide/w;

.field private final d:Lzp;

.field private final e:Lcom/twitter/android/moments/ui/guide/g;

.field private final f:Lcom/twitter/android/moments/ui/guide/p;

.field private final g:Lcom/twitter/android/moments/data/j;

.field private final h:Lcom/twitter/android/moments/data/j;

.field private final i:Lcom/twitter/android/moments/data/w;

.field private final j:Lcom/twitter/android/moments/ui/guide/d;

.field private final k:Lcom/twitter/android/moments/ui/guide/z;

.field private final l:Lacp;

.field private final m:Landroid/content/Context;

.field private final n:Lcom/twitter/android/moments/viewmodels/e;

.field private final o:Lcom/twitter/android/moments/viewmodels/d;

.field private final p:Lcom/twitter/android/moments/data/f;

.field private final q:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final r:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final s:Lcom/twitter/util/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentGuide;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Lcom/twitter/android/moments/data/am;

.field private final u:Laac;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/twitter/android/moments/data/f;Lcom/twitter/android/moments/ui/guide/w;Landroid/widget/ListView;Lzp;Laac;Lcom/twitter/android/moments/ui/guide/g;Lcom/twitter/android/moments/ui/guide/p;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/w;Lcom/twitter/android/moments/data/am;ZLcom/twitter/android/moments/ui/guide/d;Lacp;Lcom/twitter/android/av/k;Lcom/twitter/android/moments/viewmodels/e;Lcom/twitter/android/moments/viewmodels/d;Lcom/twitter/android/moments/ui/guide/l$b;Lack;Lanh;Lcom/twitter/android/moments/ui/guide/r;Lcom/twitter/android/search/SearchSuggestionController;)V
    .locals 22

    .prologue
    .line 107
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v2, Lcom/twitter/android/moments/ui/guide/l$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/twitter/android/moments/ui/guide/l$1;-><init>(Lcom/twitter/android/moments/ui/guide/l;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/moments/ui/guide/l;->s:Lcom/twitter/util/q;

    .line 108
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/guide/l;->m:Landroid/content/Context;

    .line 109
    move-object/from16 v0, p18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/guide/l;->n:Lcom/twitter/android/moments/viewmodels/e;

    .line 110
    move-object/from16 v0, p19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/guide/l;->o:Lcom/twitter/android/moments/viewmodels/d;

    .line 111
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/guide/l;->a:Landroid/view/LayoutInflater;

    .line 112
    move-object/from16 v0, p5

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/guide/l;->b:Landroid/widget/ListView;

    .line 113
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/guide/l;->c:Lcom/twitter/android/moments/ui/guide/w;

    .line 114
    move-object/from16 v0, p6

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/guide/l;->d:Lzp;

    .line 115
    move-object/from16 v0, p8

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/guide/l;->e:Lcom/twitter/android/moments/ui/guide/g;

    .line 116
    move-object/from16 v0, p9

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/guide/l;->f:Lcom/twitter/android/moments/ui/guide/p;

    .line 117
    move-object/from16 v0, p10

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/guide/l;->g:Lcom/twitter/android/moments/data/j;

    .line 118
    move-object/from16 v0, p11

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/guide/l;->h:Lcom/twitter/android/moments/data/j;

    .line 119
    move-object/from16 v0, p12

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/guide/l;->i:Lcom/twitter/android/moments/data/w;

    .line 120
    move-object/from16 v0, p13

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/guide/l;->t:Lcom/twitter/android/moments/data/am;

    .line 121
    move-object/from16 v0, p15

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/guide/l;->j:Lcom/twitter/android/moments/ui/guide/d;

    .line 122
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/guide/l;->p:Lcom/twitter/android/moments/data/f;

    .line 123
    move-object/from16 v0, p7

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/guide/l;->u:Laac;

    .line 124
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/moments/ui/guide/l;->q:Ljava/util/Set;

    .line 125
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/moments/ui/guide/l;->r:Ljava/util/Set;

    .line 127
    move-object/from16 v0, p16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/moments/ui/guide/l;->l:Lacp;

    .line 129
    if-eqz p14, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/guide/l;->b:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v2

    if-nez v2, :cond_0

    .line 130
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/guide/l;->b:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/moments/ui/guide/l;->l:Lacp;

    invoke-virtual {v3}, Lacp;->a()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 133
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/guide/l;->f:Lcom/twitter/android/moments/ui/guide/p;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/twitter/android/moments/ui/guide/p;->b(Z)V

    .line 134
    new-instance v2, Lcom/twitter/android/moments/ui/guide/l$2;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/twitter/android/moments/ui/guide/l$2;-><init>(Lcom/twitter/android/moments/ui/guide/l;)V

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Lcom/twitter/android/moments/ui/guide/w;->a(Lcno$c;)V

    .line 146
    new-instance v2, Lcom/twitter/android/moments/ui/guide/l$3;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/twitter/android/moments/ui/guide/l$3;-><init>(Lcom/twitter/android/moments/ui/guide/l;)V

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Lcom/twitter/android/moments/ui/guide/w;->a(Landroid/view/View$OnClickListener;)V

    .line 154
    new-instance v2, Lcom/twitter/android/moments/ui/guide/z;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/moments/ui/guide/l;->m:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/moments/ui/guide/l;->f:Lcom/twitter/android/moments/ui/guide/p;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/moments/ui/guide/l;->a:Landroid/view/LayoutInflater;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/moments/ui/guide/l;->d:Lzp;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/moments/ui/guide/l;->g:Lcom/twitter/android/moments/data/j;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/android/moments/ui/guide/l;->h:Lcom/twitter/android/moments/data/j;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/moments/ui/guide/l;->i:Lcom/twitter/android/moments/data/w;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/twitter/android/moments/ui/guide/l;->t:Lcom/twitter/android/moments/data/am;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/twitter/android/moments/ui/guide/l;->j:Lcom/twitter/android/moments/ui/guide/d;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/twitter/android/moments/ui/guide/l;->q:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/twitter/android/moments/ui/guide/l;->r:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/guide/l;->e:Lcom/twitter/android/moments/ui/guide/g;

    move-object/from16 v16, v0

    move-object/from16 v7, p7

    move-object/from16 v8, p17

    move-object/from16 v17, p21

    move-object/from16 v18, p22

    move-object/from16 v19, p20

    move-object/from16 v20, p23

    move-object/from16 v21, p24

    invoke-direct/range {v2 .. v21}, Lcom/twitter/android/moments/ui/guide/z;-><init>(Landroid/content/Context;Lcom/twitter/android/moments/ui/guide/p;Landroid/view/LayoutInflater;Lzp;Laac;Lcom/twitter/android/av/k;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/w;Lcom/twitter/android/moments/data/am;Lcom/twitter/android/moments/ui/guide/d;Ljava/util/Set;Ljava/util/Set;Lcom/twitter/android/moments/ui/guide/g;Lack;Lanh;Lcom/twitter/android/moments/ui/guide/l$b;Lcom/twitter/android/moments/ui/guide/r;Lcom/twitter/android/search/SearchSuggestionController;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/moments/ui/guide/l;->k:Lcom/twitter/android/moments/ui/guide/z;

    .line 159
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/guide/l;->c:Lcom/twitter/android/moments/ui/guide/w;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/moments/ui/guide/l;->k:Lcom/twitter/android/moments/ui/guide/z;

    invoke-virtual {v2, v3}, Lcom/twitter/android/moments/ui/guide/w;->a(Lcom/twitter/android/moments/ui/guide/z;)V

    .line 162
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/guide/l;->s:Lcom/twitter/util/q;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/twitter/android/moments/data/f;->a(Lcom/twitter/util/q;)V

    .line 163
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/ui/guide/w;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->c:Lcom/twitter/android/moments/ui/guide/w;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/guide/l;)Lzp;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->d:Lzp;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/data/f;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->p:Lcom/twitter/android/moments/data/f;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/data/j;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->g:Lcom/twitter/android/moments/data/j;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/data/j;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->h:Lcom/twitter/android/moments/data/j;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/viewmodels/e;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->n:Lcom/twitter/android/moments/viewmodels/e;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/viewmodels/d;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->o:Lcom/twitter/android/moments/viewmodels/d;

    return-object v0
.end method

.method private g()V
    .locals 4

    .prologue
    .line 192
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v2

    .line 193
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 194
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 195
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 196
    instance-of v3, v0, Lcom/twitter/android/moments/ui/guide/v;

    if-eqz v3, :cond_0

    .line 197
    check-cast v0, Lcom/twitter/android/moments/ui/guide/v;

    .line 198
    invoke-interface {v0}, Lcom/twitter/android/moments/ui/guide/v;->aW_()V

    .line 193
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 201
    :cond_1
    return-void
.end method

.method static synthetic h(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/ui/guide/z;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->k:Lcom/twitter/android/moments/ui/guide/z;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/ui/guide/p;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->f:Lcom/twitter/android/moments/ui/guide/p;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->e:Lcom/twitter/android/moments/ui/guide/g;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/g;->a()V

    .line 167
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 187
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->b:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/l;->l:Lacp;

    invoke-virtual {v1}, Lacp;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    .line 188
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/l;->p:Lcom/twitter/android/moments/data/f;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/guide/l;->s:Lcom/twitter/util/q;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/moments/data/f;->a(Lcom/twitter/util/q;Z)V

    .line 189
    return-void

    .line 188
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 170
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->f:Lcom/twitter/android/moments/ui/guide/p;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/guide/p;->b(Z)V

    .line 174
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->f:Lcom/twitter/android/moments/ui/guide/p;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/guide/p;->b(Z)V

    .line 178
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->f:Lcom/twitter/android/moments/ui/guide/p;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/p;->a()V

    .line 179
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->d:Lzp;

    invoke-virtual {v0}, Lzp;->a()V

    .line 180
    invoke-static {}, Lwk;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->u:Laac;

    invoke-virtual {v0}, Laac;->a()V

    .line 183
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/guide/l;->g()V

    .line 184
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->p:Lcom/twitter/android/moments/data/f;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/f;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 208
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->c:Lcom/twitter/android/moments/ui/guide/w;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/w;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->c:Lcom/twitter/android/moments/ui/guide/w;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/w;->c()V

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->p:Lcom/twitter/android/moments/data/f;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/f;->c()Z

    move-result v0

    .line 213
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->p:Lcom/twitter/android/moments/data/f;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/f;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->c:Lcom/twitter/android/moments/ui/guide/w;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/w;->f()V

    .line 222
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l;->p:Lcom/twitter/android/moments/data/f;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/f;->d()V

    .line 224
    :cond_0
    return-void
.end method
