.class public Lcom/twitter/android/moments/ui/guide/ah;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/guide/l$b;


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;

.field private final b:Lcom/twitter/android/moments/ui/guide/g;

.field private final c:Lcev;

.field private final d:Laad;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;Lcom/twitter/android/moments/ui/guide/g;Lcev;Laad;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/ah;->a:Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;

    .line 44
    iput-object p2, p0, Lcom/twitter/android/moments/ui/guide/ah;->b:Lcom/twitter/android/moments/ui/guide/g;

    .line 45
    iput-object p3, p0, Lcom/twitter/android/moments/ui/guide/ah;->c:Lcev;

    .line 46
    iput-object p4, p0, Lcom/twitter/android/moments/ui/guide/ah;->d:Laad;

    .line 47
    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/twitter/android/moments/ui/guide/g;Laad;)Lcom/twitter/android/moments/ui/guide/ah;
    .locals 3

    .prologue
    .line 33
    new-instance v1, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;-><init>(Landroid/app/Activity;)V

    .line 34
    new-instance v0, Lcev$a;

    invoke-direct {v0}, Lcev$a;-><init>()V

    invoke-virtual {v0}, Lcev$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcev;

    .line 35
    new-instance v2, Lcom/twitter/android/moments/ui/guide/ah;

    invoke-direct {v2, v1, p1, v0, p2}, Lcom/twitter/android/moments/ui/guide/ah;-><init>(Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;Lcom/twitter/android/moments/ui/guide/g;Lcev;Laad;)V

    return-object v2
.end method

.method private a(Lcom/twitter/model/moments/Moment;)V
    .locals 3

    .prologue
    .line 85
    iget-object v0, p1, Lcom/twitter/model/moments/Moment;->r:Lcom/twitter/model/moments/f;

    .line 86
    if-nez v0, :cond_1

    .line 94
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    iget-object v1, v0, Lcom/twitter/model/moments/f;->c:Lcom/twitter/model/moments/MomentVisibilityMode;

    sget-object v2, Lcom/twitter/model/moments/MomentVisibilityMode;->a:Lcom/twitter/model/moments/MomentVisibilityMode;

    if-ne v1, v2, :cond_2

    .line 90
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ah;->d:Laad;

    invoke-virtual {v0, p1}, Laad;->a(Lcom/twitter/model/moments/Moment;)V

    goto :goto_0

    .line 91
    :cond_2
    iget-object v0, v0, Lcom/twitter/model/moments/f;->c:Lcom/twitter/model/moments/MomentVisibilityMode;

    sget-object v1, Lcom/twitter/model/moments/MomentVisibilityMode;->b:Lcom/twitter/model/moments/MomentVisibilityMode;

    if-ne v0, v1, :cond_0

    .line 92
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ah;->d:Laad;

    invoke-virtual {v0, p1}, Laad;->b(Lcom/twitter/model/moments/Moment;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ah;->d:Laad;

    invoke-virtual {v0}, Laad;->b()V

    .line 66
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ah;->a:Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ah;->c:Lcev;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;->a(Lcev;)Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;->a(Lako;)V

    .line 67
    return-void
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/MomentModule;)V
    .locals 1

    .prologue
    .line 71
    const-string/jumbo v0, "Module pivot is not supported in moment picker."

    invoke-static {v0}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    .line 72
    return-void
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/MomentModule;Landroid/view/View;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ":",
            "Lcom/twitter/media/ui/image/d;",
            ">(",
            "Lcom/twitter/android/moments/viewmodels/MomentModule;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->b()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    .line 53
    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/guide/ah;->a(Lcom/twitter/model/moments/Moment;)V

    .line 54
    iget-object v1, v0, Lcom/twitter/model/moments/Moment;->r:Lcom/twitter/model/moments/f;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/twitter/model/moments/Moment;->r:Lcom/twitter/model/moments/f;

    iget-object v1, v1, Lcom/twitter/model/moments/f;->c:Lcom/twitter/model/moments/MomentVisibilityMode;

    sget-object v2, Lcom/twitter/model/moments/MomentVisibilityMode;->a:Lcom/twitter/model/moments/MomentVisibilityMode;

    if-ne v1, v2, :cond_0

    .line 55
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ah;->b:Lcom/twitter/android/moments/ui/guide/g;

    iget-wide v2, v0, Lcom/twitter/model/moments/Moment;->b:J

    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->e()Lcom/twitter/model/moments/DisplayStyle;

    move-result-object v4

    move-object v5, p1

    move-object v6, p2

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/moments/ui/guide/g;->a(JLcom/twitter/model/moments/DisplayStyle;Lcom/twitter/android/moments/viewmodels/MomentModule;Landroid/view/View;)V

    .line 61
    :goto_0
    return-void

    .line 58
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ah;->a:Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;

    .line 59
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->b()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    iget-wide v2, v0, Lcom/twitter/model/moments/Moment;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    new-instance v0, Lcik;

    invoke-direct {v0}, Lcik;-><init>()V

    .line 58
    invoke-static {v2, v3, v0}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;->a(JLcim;)Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;->a(Lako;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/x;)V
    .locals 1

    .prologue
    .line 76
    const-string/jumbo v0, "Guide pivot is not supported in moment picker."

    invoke-static {v0}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/y;)V
    .locals 1

    .prologue
    .line 81
    const-string/jumbo v0, "Trend item is not supported in moment picker."

    invoke-static {v0}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    .line 82
    return-void
.end method
