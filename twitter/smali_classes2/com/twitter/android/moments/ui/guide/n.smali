.class public Lcom/twitter/android/moments/ui/guide/n;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/guide/l$b;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/twitter/android/moments/ui/guide/g;

.field private final c:Lakr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lakr",
            "<",
            "Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$c;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lzp;

.field private final e:Lcom/twitter/android/client/n;

.field private final f:Lcom/twitter/android/search/c;

.field private final g:Laac;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/android/moments/ui/guide/g;Lakr;Lzp;Laac;Lcom/twitter/android/client/n;Lcom/twitter/android/search/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/twitter/android/moments/ui/guide/g;",
            "Lakr",
            "<",
            "Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$c;",
            ">;",
            "Lzp;",
            "Laac;",
            "Lcom/twitter/android/client/n;",
            "Lcom/twitter/android/search/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/n;->a:Landroid/app/Activity;

    .line 45
    iput-object p2, p0, Lcom/twitter/android/moments/ui/guide/n;->b:Lcom/twitter/android/moments/ui/guide/g;

    .line 46
    iput-object p3, p0, Lcom/twitter/android/moments/ui/guide/n;->c:Lakr;

    .line 47
    iput-object p4, p0, Lcom/twitter/android/moments/ui/guide/n;->d:Lzp;

    .line 48
    iput-object p5, p0, Lcom/twitter/android/moments/ui/guide/n;->g:Laac;

    .line 49
    iput-object p6, p0, Lcom/twitter/android/moments/ui/guide/n;->e:Lcom/twitter/android/client/n;

    .line 50
    iput-object p7, p0, Lcom/twitter/android/moments/ui/guide/n;->f:Lcom/twitter/android/search/c;

    .line 51
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 101
    const-string/jumbo v0, "Create moment is not supported in moment picker."

    invoke-static {v0}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    .line 102
    return-void
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/MomentModule;)V
    .locals 2

    .prologue
    .line 82
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->f()Lceg;

    move-result-object v0

    .line 83
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    iget-object v1, v0, Lceg;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/n;->e:Lcom/twitter/android/client/n;

    iget-object v0, v0, Lceg;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/n;->a(Ljava/lang/String;)V

    .line 86
    return-void
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/MomentModule;Landroid/view/View;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ":",
            "Lcom/twitter/media/ui/image/d;",
            ">(",
            "Lcom/twitter/android/moments/viewmodels/MomentModule;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/n;->b:Lcom/twitter/android/moments/ui/guide/g;

    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->b()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    iget-wide v2, v0, Lcom/twitter/model/moments/Moment;->b:J

    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->e()Lcom/twitter/model/moments/DisplayStyle;

    move-result-object v4

    move-object v5, p1

    move-object v6, p2

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/moments/ui/guide/g;->a(JLcom/twitter/model/moments/DisplayStyle;Lcom/twitter/android/moments/viewmodels/MomentModule;Landroid/view/View;)V

    .line 58
    return-void
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/x;)V
    .locals 5

    .prologue
    .line 62
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/x;->b()Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/x;->e()Ljava/lang/String;

    move-result-object v1

    .line 65
    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 66
    iget-object v2, p0, Lcom/twitter/android/moments/ui/guide/n;->c:Lakr;

    new-instance v3, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$c;

    new-instance v4, Lcom/twitter/model/moments/h;

    invoke-direct {v4, v0, v1}, Lcom/twitter/model/moments/h;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v3, v4}, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$c;-><init>(Lcom/twitter/model/moments/h;)V

    invoke-virtual {v2, v3}, Lakr;->a(Lako;)V

    .line 69
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/n;->d:Lzp;

    invoke-virtual {v1, v0}, Lzp;->b(Ljava/lang/String;)V

    .line 78
    :goto_0
    return-void

    .line 70
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/x;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 71
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/x;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "twitter://trends"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/n;->g:Laac;

    invoke-virtual {v0}, Laac;->c()V

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/n;->e:Lcom/twitter/android/client/n;

    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/x;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/n;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 76
    :cond_2
    const-string/jumbo v0, "Section Pivot does not contain a categoryId"

    invoke-static {v0}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/y;)V
    .locals 3

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/n;->g:Laac;

    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/y;->a()Lcom/twitter/model/moments/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Laac;->b(Lcom/twitter/model/moments/i;)V

    .line 91
    new-instance v0, Lage$a;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/n;->a:Landroid/app/Activity;

    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/y;->e()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lage$a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 92
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/y;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lage$a;->a(Ljava/lang/String;)Lage$a;

    move-result-object v1

    .line 93
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/y;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "promoted_trend_click"

    :goto_0
    invoke-virtual {v1, v0}, Lage$a;->b(Ljava/lang/String;)Lage$a;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    .line 95
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/y;->i()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Lage$a;->a(Ljava/util/ArrayList;)Lage$a;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/n;->f:Lcom/twitter/android/search/c;

    invoke-virtual {v0}, Lage$a;->a()Lage;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/search/c;->a(Lako;)V

    .line 97
    return-void

    .line 93
    :cond_0
    const-string/jumbo v0, "trend_click"

    goto :goto_0
.end method
