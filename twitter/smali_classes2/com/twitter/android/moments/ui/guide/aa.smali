.class public Lcom/twitter/android/moments/ui/guide/aa;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laku;
.implements Lakw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Laku",
        "<",
        "Lcom/twitter/util/collection/k",
        "<",
        "Lcom/twitter/model/moments/Moment;",
        ">;>;",
        "Lakw",
        "<",
        "Lcom/twitter/util/collection/k",
        "<",
        "Lcom/twitter/model/moments/Moment;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Intent;)Lcom/twitter/util/collection/k;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 24
    if-nez p1, :cond_0

    .line 25
    invoke-static {}, Lcom/twitter/util/collection/k;->a()Lcom/twitter/util/collection/k;

    move-result-object v0

    .line 27
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "extra_moment"

    sget-object v1, Lcom/twitter/model/moments/Moment;->a:Lcom/twitter/util/serialization/l;

    .line 29
    invoke-static {v1}, Lcom/twitter/util/collection/d;->c(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    .line 28
    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    .line 27
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/k;

    goto :goto_0
.end method

.method public a(Landroid/content/Intent;Lcom/twitter/util/collection/k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    const-string/jumbo v0, "extra_moment"

    sget-object v1, Lcom/twitter/model/moments/Moment;->a:Lcom/twitter/util/serialization/l;

    .line 35
    invoke-static {v1}, Lcom/twitter/util/collection/d;->c(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    .line 34
    invoke-static {p1, v0, p2, v1}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/content/Intent;

    .line 36
    return-void
.end method

.method public bridge synthetic a(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 16
    check-cast p2, Lcom/twitter/util/collection/k;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/moments/ui/guide/aa;->a(Landroid/content/Intent;Lcom/twitter/util/collection/k;)V

    return-void
.end method

.method public synthetic b(Landroid/content/Intent;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/guide/aa;->a(Landroid/content/Intent;)Lcom/twitter/util/collection/k;

    move-result-object v0

    return-object v0
.end method
