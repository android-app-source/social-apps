.class Lcom/twitter/android/moments/ui/guide/a$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/guide/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/app/Activity;

.field private b:J

.field private c:J


# direct methods
.method constructor <init>(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/a$a;->a:Landroid/app/Activity;

    .line 76
    return-void
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 6

    .prologue
    .line 92
    iget-wide v0, p0, Lcom/twitter/android/moments/ui/guide/a$a;->c:J

    new-instance v2, Lcin;

    iget-wide v4, p0, Lcom/twitter/android/moments/ui/guide/a$a;->b:J

    invoke-direct {v2, v4, v5}, Lcin;-><init>(J)V

    invoke-static {v0, v1, v2}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;->a(JLcim;)Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/a$a;->a:Landroid/app/Activity;

    const-class v2, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity;

    .line 93
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 92
    return-object v0
.end method

.method public a(J)Lcom/twitter/android/moments/ui/guide/a$a;
    .locals 1

    .prologue
    .line 80
    iput-wide p1, p0, Lcom/twitter/android/moments/ui/guide/a$a;->b:J

    .line 81
    return-object p0
.end method

.method public b(J)Lcom/twitter/android/moments/ui/guide/a$a;
    .locals 1

    .prologue
    .line 86
    iput-wide p1, p0, Lcom/twitter/android/moments/ui/guide/a$a;->c:J

    .line 87
    return-object p0
.end method
