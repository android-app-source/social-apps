.class Lcom/twitter/android/moments/ui/guide/i;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lajq;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/guide/i$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lajq",
        "<",
        "Lcom/twitter/android/moments/viewmodels/r;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/guide/i$a;

.field private final b:Lcom/twitter/android/moments/ui/guide/l$b;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/guide/i$a;Lcom/twitter/android/moments/ui/guide/l$b;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/i;->a:Lcom/twitter/android/moments/ui/guide/i$a;

    .line 29
    iput-object p2, p0, Lcom/twitter/android/moments/ui/guide/i;->b:Lcom/twitter/android/moments/ui/guide/l$b;

    .line 30
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/guide/l$b;)Lcom/twitter/android/moments/ui/guide/i;
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcom/twitter/android/moments/ui/guide/i;

    invoke-static {p0, p1}, Lcom/twitter/android/moments/ui/guide/i$a;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/guide/i$a;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/twitter/android/moments/ui/guide/i;-><init>(Lcom/twitter/android/moments/ui/guide/i$a;Lcom/twitter/android/moments/ui/guide/l$b;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/guide/i;)Lcom/twitter/android/moments/ui/guide/l$b;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/i;->b:Lcom/twitter/android/moments/ui/guide/l$b;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/viewmodels/r;)V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/i;->a:Lcom/twitter/android/moments/ui/guide/i$a;

    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/r;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/guide/i$a;->a(Ljava/lang/String;)V

    .line 41
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/r;->b()Lcom/twitter/android/moments/viewmodels/x;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/i;->a:Lcom/twitter/android/moments/ui/guide/i$a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/i$a;->c()V

    .line 43
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/i;->a:Lcom/twitter/android/moments/ui/guide/i$a;

    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/r;->b()Lcom/twitter/android/moments/viewmodels/x;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/moments/viewmodels/x;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/guide/i$a;->b(Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/i;->a:Lcom/twitter/android/moments/ui/guide/i$a;

    new-instance v1, Lcom/twitter/android/moments/ui/guide/i$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/guide/i$1;-><init>(Lcom/twitter/android/moments/ui/guide/i;Lcom/twitter/android/moments/viewmodels/r;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/guide/i$a;->a(Landroid/view/View$OnClickListener;)V

    .line 53
    :goto_0
    return-void

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/i;->a:Lcom/twitter/android/moments/ui/guide/i$a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/i$a;->b()V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 14
    check-cast p1, Lcom/twitter/android/moments/viewmodels/r;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/guide/i;->a(Lcom/twitter/android/moments/viewmodels/r;)V

    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/i;->a:Lcom/twitter/android/moments/ui/guide/i$a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/i$a;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
