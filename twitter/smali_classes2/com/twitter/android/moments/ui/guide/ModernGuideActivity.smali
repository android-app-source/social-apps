.class public Lcom/twitter/android/moments/ui/guide/ModernGuideActivity;
.super Lcom/twitter/android/ListFragmentActivity;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$e;,
        Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$a;,
        Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$c;,
        Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$f;,
        Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$d;,
        Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$b;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/twitter/android/ListFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Intent;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/android/ListFragmentActivity$a;
    .locals 10

    .prologue
    const/4 v6, 0x0

    const-wide/16 v8, 0x0

    .line 58
    new-instance v1, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;

    invoke-direct {v1}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;-><init>()V

    .line 59
    const-string/jumbo v0, "guide_type"

    .line 60
    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 61
    const-string/jumbo v0, "guide_category_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 62
    const-string/jumbo v0, "guide_category_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 63
    const-string/jumbo v0, "twitter_scribe_association"

    .line 64
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 65
    const-string/jumbo v5, "show_category_pills"

    .line 66
    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 67
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 68
    new-instance v0, Lcom/twitter/android/moments/ui/guide/ab$a;

    invoke-direct {v0}, Lcom/twitter/android/moments/ui/guide/ab$a;-><init>()V

    .line 70
    invoke-virtual {v0, v3, v4}, Lcom/twitter/android/moments/ui/guide/ab$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/android/moments/ui/guide/ab$a;

    move-result-object v0

    .line 71
    invoke-virtual {v0, v5}, Lcom/twitter/android/moments/ui/guide/ab$a;->a(Z)Lcom/twitter/android/moments/ui/guide/ab$a;

    move-result-object v0

    .line 72
    invoke-virtual {v0, v2}, Lcom/twitter/android/moments/ui/guide/ab$a;->a(I)Lcom/twitter/android/moments/ui/guide/ab$a;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/ab$a;->a()Lcom/twitter/android/moments/ui/guide/ab;

    move-result-object v0

    .line 68
    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a(Lcom/twitter/app/common/base/b;)V

    .line 99
    :goto_0
    new-instance v0, Lcom/twitter/android/ListFragmentActivity$a;

    invoke-direct {v0, v1}, Lcom/twitter/android/ListFragmentActivity$a;-><init>(Lcom/twitter/app/common/list/TwitterListFragment;)V

    return-object v0

    .line 74
    :cond_0
    const-string/jumbo v3, "add_to_moment_tweet_id"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 75
    const-string/jumbo v3, "add_to_moment_tweet_id"

    .line 76
    invoke-virtual {p1, v3, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 77
    const-string/jumbo v3, "moments_owner_id"

    invoke-virtual {p1, v3, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    .line 78
    new-instance v3, Lcom/twitter/android/moments/ui/guide/ab$a;

    invoke-direct {v3}, Lcom/twitter/android/moments/ui/guide/ab$a;-><init>()V

    .line 80
    invoke-virtual {v3, v6, v7}, Lcom/twitter/android/moments/ui/guide/ab$a;->b(J)Lcom/twitter/android/moments/ui/guide/ab$a;

    move-result-object v3

    .line 81
    invoke-virtual {v3, v4, v5}, Lcom/twitter/android/moments/ui/guide/ab$a;->a(J)Lcom/twitter/android/moments/ui/guide/ab$a;

    move-result-object v3

    .line 82
    invoke-virtual {v3, v0}, Lcom/twitter/android/moments/ui/guide/ab$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/moments/ui/guide/ab$a;

    move-result-object v0

    .line 83
    invoke-virtual {v0, v2}, Lcom/twitter/android/moments/ui/guide/ab$a;->a(I)Lcom/twitter/android/moments/ui/guide/ab$a;

    move-result-object v0

    .line 84
    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/ab$a;->a()Lcom/twitter/android/moments/ui/guide/ab;

    move-result-object v0

    .line 78
    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a(Lcom/twitter/app/common/base/b;)V

    goto :goto_0

    .line 85
    :cond_1
    const-string/jumbo v0, "moments_owner_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 86
    const-string/jumbo v0, "moments_owner_id"

    invoke-virtual {p1, v0, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 87
    new-instance v0, Lcom/twitter/android/moments/ui/guide/ab$a;

    invoke-direct {v0}, Lcom/twitter/android/moments/ui/guide/ab$a;-><init>()V

    .line 89
    invoke-virtual {v0, v4, v5}, Lcom/twitter/android/moments/ui/guide/ab$a;->b(J)Lcom/twitter/android/moments/ui/guide/ab$a;

    move-result-object v0

    .line 90
    invoke-virtual {v0, v2}, Lcom/twitter/android/moments/ui/guide/ab$a;->a(I)Lcom/twitter/android/moments/ui/guide/ab$a;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/ab$a;->a()Lcom/twitter/android/moments/ui/guide/ab;

    move-result-object v0

    .line 87
    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a(Lcom/twitter/app/common/base/b;)V

    goto :goto_0

    .line 93
    :cond_2
    new-instance v0, Lcom/twitter/android/moments/ui/guide/ab$a;

    invoke-direct {v0}, Lcom/twitter/android/moments/ui/guide/ab$a;-><init>()V

    .line 95
    invoke-virtual {v0, v5}, Lcom/twitter/android/moments/ui/guide/ab$a;->a(Z)Lcom/twitter/android/moments/ui/guide/ab$a;

    move-result-object v0

    .line 96
    invoke-virtual {v0, v2}, Lcom/twitter/android/moments/ui/guide/ab$a;->a(I)Lcom/twitter/android/moments/ui/guide/ab$a;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/ab$a;->a()Lcom/twitter/android/moments/ui/guide/ab;

    move-result-object v0

    .line 93
    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a(Lcom/twitter/app/common/base/b;)V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ListFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    .line 47
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(I)V

    .line 49
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 51
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(Z)V

    .line 52
    return-object v0
.end method

.method protected a(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 30
    const-string/jumbo v0, "guide_category_name"

    .line 31
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a0596

    .line 32
    invoke-virtual {p0, v1}, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 30
    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 2

    .prologue
    .line 37
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ListFragmentActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 38
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Lzp;->a(J)V

    .line 39
    return-void
.end method
