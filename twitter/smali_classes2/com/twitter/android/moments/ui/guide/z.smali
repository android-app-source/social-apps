.class Lcom/twitter/android/moments/ui/guide/z;
.super Lcjr;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcjr",
        "<",
        "Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/guide/p;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:Lcom/twitter/android/search/SearchSuggestionController;

.field private final d:Lcom/twitter/android/av/k;

.field private final e:Lzp$c;

.field private final f:Lcom/twitter/android/moments/data/j;

.field private final g:Lcom/twitter/android/moments/data/j;

.field private final h:Lcom/twitter/android/moments/data/w;

.field private final i:Lcom/twitter/android/moments/data/am;

.field private final j:Lcom/twitter/android/moments/ui/guide/d;

.field private final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Lcom/twitter/android/moments/ui/guide/g;

.field private final n:Lzp$b;

.field private final o:Lzp;

.field private final p:Lack;

.field private final q:Lanh;

.field private final r:Lcom/twitter/android/moments/ui/guide/l$b;

.field private final s:Lcom/twitter/android/moments/ui/guide/r;

.field private final t:Laac;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/android/moments/ui/guide/p;Landroid/view/LayoutInflater;Lzp;Laac;Lcom/twitter/android/av/k;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/w;Lcom/twitter/android/moments/data/am;Lcom/twitter/android/moments/ui/guide/d;Ljava/util/Set;Ljava/util/Set;Lcom/twitter/android/moments/ui/guide/g;Lack;Lanh;Lcom/twitter/android/moments/ui/guide/l$b;Lcom/twitter/android/moments/ui/guide/r;Lcom/twitter/android/search/SearchSuggestionController;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/android/moments/ui/guide/p;",
            "Landroid/view/LayoutInflater;",
            "Lzp;",
            "Laac;",
            "Lcom/twitter/android/av/k;",
            "Lcom/twitter/android/moments/data/j;",
            "Lcom/twitter/android/moments/data/j;",
            "Lcom/twitter/android/moments/data/w;",
            "Lcom/twitter/android/moments/data/am;",
            "Lcom/twitter/android/moments/ui/guide/d;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/twitter/android/moments/ui/guide/g;",
            "Lack;",
            "Lanh;",
            "Lcom/twitter/android/moments/ui/guide/l$b;",
            "Lcom/twitter/android/moments/ui/guide/r;",
            "Lcom/twitter/android/search/SearchSuggestionController;",
            ")V"
        }
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcjr;-><init>(Landroid/content/Context;)V

    .line 93
    iput-object p2, p0, Lcom/twitter/android/moments/ui/guide/z;->a:Lcom/twitter/android/moments/ui/guide/p;

    .line 94
    iput-object p3, p0, Lcom/twitter/android/moments/ui/guide/z;->b:Landroid/view/LayoutInflater;

    .line 95
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/z;->c:Lcom/twitter/android/search/SearchSuggestionController;

    .line 96
    invoke-virtual {p4}, Lzp;->c()Lzp$c;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/moments/ui/guide/z;->e:Lzp$c;

    .line 97
    invoke-virtual {p4}, Lzp;->d()Lzp$b;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/moments/ui/guide/z;->n:Lzp$b;

    .line 98
    iput-object p4, p0, Lcom/twitter/android/moments/ui/guide/z;->o:Lzp;

    .line 99
    iput-object p5, p0, Lcom/twitter/android/moments/ui/guide/z;->t:Laac;

    .line 100
    iput-object p6, p0, Lcom/twitter/android/moments/ui/guide/z;->d:Lcom/twitter/android/av/k;

    .line 101
    iput-object p7, p0, Lcom/twitter/android/moments/ui/guide/z;->f:Lcom/twitter/android/moments/data/j;

    .line 102
    iput-object p8, p0, Lcom/twitter/android/moments/ui/guide/z;->g:Lcom/twitter/android/moments/data/j;

    .line 103
    iput-object p9, p0, Lcom/twitter/android/moments/ui/guide/z;->h:Lcom/twitter/android/moments/data/w;

    .line 104
    iput-object p10, p0, Lcom/twitter/android/moments/ui/guide/z;->i:Lcom/twitter/android/moments/data/am;

    .line 105
    iput-object p11, p0, Lcom/twitter/android/moments/ui/guide/z;->j:Lcom/twitter/android/moments/ui/guide/d;

    .line 106
    iput-object p12, p0, Lcom/twitter/android/moments/ui/guide/z;->k:Ljava/util/Set;

    .line 107
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/z;->l:Ljava/util/Set;

    .line 108
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/z;->m:Lcom/twitter/android/moments/ui/guide/g;

    .line 109
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/z;->p:Lack;

    .line 110
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/z;->q:Lanh;

    .line 111
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/z;->r:Lcom/twitter/android/moments/ui/guide/l$b;

    .line 112
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/z;->s:Lcom/twitter/android/moments/ui/guide/r;

    .line 114
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/z;->k()Lcjt;

    move-result-object v1

    invoke-static {}, Lcbi;->f()Lcbi;

    move-result-object v2

    invoke-interface {v1, v2}, Lcjt;->a(Lcbi;)Lcbi;

    .line 115
    return-void
.end method

.method private a(Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 264
    sget-object v0, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;->l:Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;

    if-ne p1, v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/z;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f040204

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 280
    :goto_0
    return-object v0

    .line 267
    :cond_0
    sget-object v0, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;->a:Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;

    if-ne p1, v0, :cond_1

    .line 268
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/z;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f040202

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 269
    :cond_1
    sget-object v0, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;->b:Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;

    if-ne p1, v0, :cond_2

    .line 270
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/z;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f040203

    .line 271
    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 273
    const v1, 0x7f130520

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    .line 274
    iget-object v2, p0, Lcom/twitter/android/moments/ui/guide/z;->p:Lack;

    invoke-interface {v2, v1}, Lack;->b(Landroid/support/v7/widget/RecyclerView;)Lacm;

    move-result-object v1

    .line 276
    invoke-interface {v1}, Lacm;->a()V

    goto :goto_0

    .line 279
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Invalid type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " provided to inflateHeroView"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    .line 280
    new-instance v0, Landroid/widget/Space;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Space;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/guide/z;)Lcom/twitter/android/search/SearchSuggestionController;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/z;->c:Lcom/twitter/android/search/SearchSuggestionController;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/guide/z;)Lcom/twitter/android/moments/ui/guide/l$b;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/z;->r:Lcom/twitter/android/moments/ui/guide/l$b;

    return-object v0
.end method


# virtual methods
.method protected a(Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;)I
    .locals 1

    .prologue
    .line 119
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;->c()Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;->ordinal()I

    move-result v0

    return v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 50
    check-cast p1, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/guide/z;->a(Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;)I

    move-result v0

    return v0
.end method

.method protected a(Landroid/content/Context;Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 23

    .prologue
    .line 131
    const/16 v21, 0x0

    .line 132
    sget-object v2, Lcom/twitter/android/moments/ui/guide/z$4;->a:[I

    invoke-virtual/range {p2 .. p2}, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;->c()Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 259
    :goto_0
    return-object v21

    .line 134
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/guide/z;->b:Landroid/view/LayoutInflater;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/moments/ui/guide/z;->r:Lcom/twitter/android/moments/ui/guide/l$b;

    .line 135
    move-object/from16 v0, p3

    invoke-static {v2, v0, v3}, Lcom/twitter/android/moments/ui/guide/i;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/guide/l$b;)Lcom/twitter/android/moments/ui/guide/i;

    move-result-object v2

    .line 136
    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/guide/i;->aN_()Landroid/view/View;

    move-result-object v21

    .line 137
    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 141
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/guide/z;->b:Landroid/view/LayoutInflater;

    const v3, 0x7f04020d

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v21

    goto :goto_0

    .line 145
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/guide/z;->b:Landroid/view/LayoutInflater;

    const v3, 0x7f0403aa

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v21

    goto :goto_0

    .line 149
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/guide/z;->b:Landroid/view/LayoutInflater;

    const v3, 0x7f040212

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v21

    .line 150
    new-instance v2, Lcom/twitter/android/moments/ui/guide/z$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/twitter/android/moments/ui/guide/z$1;-><init>(Lcom/twitter/android/moments/ui/guide/z;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 160
    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/guide/z;->j()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/moments/ui/guide/z;->b:Landroid/view/LayoutInflater;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/moments/ui/guide/z;->r:Lcom/twitter/android/moments/ui/guide/l$b;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/moments/ui/guide/z;->t:Laac;

    move-object/from16 v0, p3

    invoke-static {v2, v3, v0, v4, v5}, Lcom/twitter/android/moments/ui/guide/o;->a(Landroid/content/res/Resources;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/guide/l$b;Laac;)Lcom/twitter/android/moments/ui/guide/o;

    move-result-object v2

    .line 162
    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/guide/o;->aN_()Landroid/view/View;

    move-result-object v21

    .line 163
    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 167
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/guide/z;->b:Landroid/view/LayoutInflater;

    const v3, 0x7f040206

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v21

    .line 168
    new-instance v2, Lcom/twitter/android/moments/ui/guide/z$2;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/twitter/android/moments/ui/guide/z$2;-><init>(Lcom/twitter/android/moments/ui/guide/z;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 177
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/guide/z;->b:Landroid/view/LayoutInflater;

    .line 178
    move-object/from16 v0, p3

    invoke-static {v2, v0}, Lacq;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lacq;

    move-result-object v11

    .line 179
    invoke-virtual {v11}, Lacq;->aN_()Landroid/view/View;

    move-result-object v21

    .line 180
    new-instance v2, Laae;

    move-object/from16 v0, v21

    invoke-direct {v2, v0}, Laae;-><init>(Landroid/view/View;)V

    .line 181
    new-instance v4, Lcom/twitter/android/moments/data/e;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/moments/ui/guide/z;->h:Lcom/twitter/android/moments/data/w;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/moments/ui/guide/z;->e:Lzp$c;

    invoke-direct {v4, v2, v3, v5}, Lcom/twitter/android/moments/data/e;-><init>(Laae;Lcom/twitter/android/moments/data/w;Lzq;)V

    .line 184
    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f130502

    const v5, 0x7f130503

    .line 183
    move-object/from16 v0, v21

    invoke-static {v2, v0, v3, v5}, Laah;->a(Landroid/content/res/Resources;Landroid/view/View;II)Laaf;

    move-result-object v2

    .line 185
    new-instance v5, Lcom/twitter/android/moments/data/ai;

    new-instance v3, Laag;

    invoke-direct {v3, v2}, Laag;-><init>(Laaf;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/guide/z;->i:Lcom/twitter/android/moments/data/am;

    invoke-direct {v5, v3, v2}, Lcom/twitter/android/moments/data/ai;-><init>(Laag;Lcom/twitter/android/moments/data/am;)V

    .line 187
    new-instance v2, Lcom/twitter/android/moments/data/ac;

    const v3, 0x7f130521

    .line 188
    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/twitter/android/moments/data/ac;-><init>(Landroid/view/View;)V

    .line 189
    new-instance v6, Lcom/twitter/android/moments/data/ab;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/moments/ui/guide/z;->r:Lcom/twitter/android/moments/ui/guide/l$b;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/moments/ui/guide/z;->e:Lzp$c;

    invoke-direct {v6, v2, v3, v7}, Lcom/twitter/android/moments/data/ab;-><init>(Lcom/twitter/android/moments/data/ac;Lcom/twitter/android/moments/ui/guide/l$b;Lzp$a;)V

    .line 193
    invoke-static/range {v21 .. v21}, Lcom/twitter/android/moments/ui/guide/f;->b(Landroid/view/View;)Lcom/twitter/android/moments/ui/guide/f;

    move-result-object v15

    .line 194
    new-instance v2, Lcom/twitter/android/moments/ui/guide/ad;

    .line 195
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/moments/ui/guide/z;->a:Lcom/twitter/android/moments/ui/guide/p;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/moments/ui/guide/z;->f:Lcom/twitter/android/moments/data/j;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/moments/ui/guide/z;->g:Lcom/twitter/android/moments/data/j;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/android/moments/ui/guide/z;->j:Lcom/twitter/android/moments/ui/guide/d;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/twitter/android/moments/ui/guide/z;->e:Lzp$c;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/twitter/android/moments/ui/guide/z;->r:Lcom/twitter/android/moments/ui/guide/l$b;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/twitter/android/moments/ui/guide/z;->k:Ljava/util/Set;

    invoke-direct/range {v2 .. v15}, Lcom/twitter/android/moments/ui/guide/ad;-><init>(Landroid/content/res/Resources;Lcom/twitter/android/moments/data/e;Lcom/twitter/android/moments/data/ai;Lcom/twitter/android/moments/data/ab;Lcom/twitter/android/moments/ui/guide/p;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/ui/guide/d;Lacq;Lzp$c;Lcom/twitter/android/moments/ui/guide/l$b;Ljava/util/Set;Lcom/twitter/android/moments/ui/guide/y;)V

    .line 200
    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 206
    :pswitch_7
    invoke-virtual/range {p2 .. p2}, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;->c()Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v2, v1}, Lcom/twitter/android/moments/ui/guide/z;->a(Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v21

    .line 207
    new-instance v22, Lcom/twitter/android/moments/ui/fullscreen/bd;

    .line 208
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/guide/z;->j()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, Lcom/twitter/library/av/playback/q;->a()Lcom/twitter/library/av/playback/q;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-direct {v0, v2, v3}, Lcom/twitter/android/moments/ui/fullscreen/bd;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/q;)V

    .line 209
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/guide/z;->s:Lcom/twitter/android/moments/ui/guide/r;

    .line 210
    move-object/from16 v0, v21

    invoke-static {v0, v2}, Lcom/twitter/android/moments/ui/guide/q;->a(Landroid/view/View;Lcom/twitter/android/moments/ui/guide/r;)Lcom/twitter/android/moments/ui/guide/q;

    move-result-object v4

    .line 211
    new-instance v2, Lcom/twitter/android/moments/ui/guide/u;

    .line 212
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/guide/z;->j()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/moments/ui/guide/z;->a:Lcom/twitter/android/moments/ui/guide/p;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/moments/ui/guide/z;->b:Landroid/view/LayoutInflater;

    move-object/from16 v7, v21

    check-cast v7, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/moments/ui/guide/z;->m:Lcom/twitter/android/moments/ui/guide/g;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/moments/ui/guide/z;->f:Lcom/twitter/android/moments/data/j;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/android/moments/ui/guide/z;->g:Lcom/twitter/android/moments/data/j;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/moments/ui/guide/z;->h:Lcom/twitter/android/moments/data/w;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/twitter/android/moments/ui/guide/z;->n:Lzp$b;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/twitter/android/moments/ui/guide/z;->i:Lcom/twitter/android/moments/data/am;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/twitter/android/moments/ui/guide/z;->r:Lcom/twitter/android/moments/ui/guide/l$b;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/twitter/android/moments/ui/guide/z;->s:Lcom/twitter/android/moments/ui/guide/r;

    invoke-direct/range {v2 .. v15}, Lcom/twitter/android/moments/ui/guide/u;-><init>(Landroid/content/Context;Lcom/twitter/android/moments/ui/guide/y;Lcom/twitter/android/moments/ui/guide/p;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/guide/g;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/w;Lzp$b;Lcom/twitter/android/moments/data/am;Lcom/twitter/android/moments/ui/guide/l$b;Lcom/twitter/android/moments/ui/guide/r;)V

    .line 217
    new-instance v5, Lcom/twitter/android/moments/ui/guide/ai;

    .line 218
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/guide/z;->j()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/moments/ui/guide/z;->a:Lcom/twitter/android/moments/ui/guide/p;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/moments/ui/guide/z;->b:Landroid/view/LayoutInflater;

    move-object/from16 v10, v21

    check-cast v10, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/twitter/android/moments/ui/guide/z;->m:Lcom/twitter/android/moments/ui/guide/g;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/twitter/android/moments/ui/guide/z;->f:Lcom/twitter/android/moments/data/j;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/twitter/android/moments/ui/guide/z;->g:Lcom/twitter/android/moments/data/j;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/twitter/android/moments/ui/guide/z;->h:Lcom/twitter/android/moments/data/w;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/guide/z;->n:Lzp$b;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/guide/z;->i:Lcom/twitter/android/moments/data/am;

    move-object/from16 v17, v0

    .line 222
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/guide/z;->j()Landroid/content/Context;

    move-result-object v18

    check-cast v18, Lcom/twitter/app/common/util/j;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/guide/z;->r:Lcom/twitter/android/moments/ui/guide/l$b;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/guide/z;->s:Lcom/twitter/android/moments/ui/guide/r;

    move-object/from16 v20, v0

    move-object v7, v4

    move-object/from16 v11, v22

    invoke-direct/range {v5 .. v20}, Lcom/twitter/android/moments/ui/guide/ai;-><init>(Landroid/content/Context;Lcom/twitter/android/moments/ui/guide/y;Lcom/twitter/android/moments/ui/guide/p;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/fullscreen/bd;Lcom/twitter/android/moments/ui/guide/g;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/w;Lzp$b;Lcom/twitter/android/moments/data/am;Lcom/twitter/app/common/util/j;Lcom/twitter/android/moments/ui/guide/l$b;Lcom/twitter/android/moments/ui/guide/r;)V

    .line 224
    new-instance v6, Lcom/twitter/android/moments/ui/guide/s;

    const v3, 0x7f130167

    .line 225
    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/moments/ui/guide/z;->n:Lzp$b;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/twitter/android/moments/ui/guide/z;->d:Lcom/twitter/android/av/k;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/twitter/android/moments/ui/guide/z;->j:Lcom/twitter/android/moments/ui/guide/d;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/twitter/android/moments/ui/guide/z;->r:Lcom/twitter/android/moments/ui/guide/l$b;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/twitter/android/moments/ui/guide/z;->s:Lcom/twitter/android/moments/ui/guide/r;

    move-object/from16 v7, v21

    move-object v9, v5

    move-object v10, v2

    invoke-direct/range {v6 .. v15}, Lcom/twitter/android/moments/ui/guide/s;-><init>(Landroid/view/View;Lcom/twitter/media/ui/image/AspectRatioFrameLayout;Lcom/twitter/android/moments/ui/guide/ai;Lcom/twitter/android/moments/ui/guide/u;Lzp$b;Lcom/twitter/android/av/k;Lcom/twitter/android/moments/ui/guide/d;Lcom/twitter/android/moments/ui/guide/l$b;Lcom/twitter/android/moments/ui/guide/r;)V

    .line 228
    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 229
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/guide/z;->q:Lanh;

    invoke-virtual {v2, v6}, Lanh;->a(Ljava/lang/Object;)Lanh;

    goto/16 :goto_0

    .line 233
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/guide/z;->b:Landroid/view/LayoutInflater;

    const v3, 0x7f0401ff

    const/4 v4, 0x0

    .line 234
    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v21

    .line 235
    const v2, 0x7f130520

    .line 236
    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/RecyclerView;

    .line 237
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/moments/ui/guide/z;->p:Lack;

    invoke-interface {v3, v2}, Lack;->a(Landroid/support/v7/widget/RecyclerView;)Lacm;

    move-result-object v2

    .line 239
    invoke-interface {v2}, Lacm;->a()V

    goto/16 :goto_0

    .line 244
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/guide/z;->p:Lack;

    move-object/from16 v0, p3

    invoke-interface {v2, v0}, Lack;->a(Landroid/view/ViewGroup;)Lacm;

    move-result-object v2

    .line 245
    invoke-interface {v2}, Lacm;->b()Landroid/view/View;

    move-result-object v21

    .line 246
    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 250
    :pswitch_a
    check-cast p2, Lcom/twitter/android/moments/viewmodels/u;

    .line 251
    check-cast p1, Landroid/app/Activity;

    invoke-static/range {p1 .. p2}, Lcom/twitter/android/moments/ui/guide/x;->a(Landroid/app/Activity;Lcom/twitter/android/moments/viewmodels/u;)Lcom/twitter/android/moments/ui/guide/x;

    move-result-object v2

    .line 252
    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/guide/x;->aN_()Landroid/view/View;

    move-result-object v21

    goto/16 :goto_0

    .line 132
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 50
    check-cast p2, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/moments/ui/guide/z;->a(Landroid/content/Context;Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;)V
    .locals 2

    .prologue
    .line 286
    sget-object v0, Lcom/twitter/android/moments/ui/guide/z$4;->a:[I

    invoke-virtual {p3}, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;->c()Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 330
    :goto_0
    :pswitch_0
    return-void

    .line 288
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/guide/i;

    .line 289
    check-cast p3, Lcom/twitter/android/moments/viewmodels/r;

    invoke-virtual {v0, p3}, Lcom/twitter/android/moments/ui/guide/i;->a(Lcom/twitter/android/moments/viewmodels/r;)V

    goto :goto_0

    .line 293
    :pswitch_2
    const v0, 0x7f13052c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    move-object v1, p3

    .line 294
    check-cast v1, Lcom/twitter/android/moments/viewmodels/x;

    invoke-virtual {v1}, Lcom/twitter/android/moments/viewmodels/x;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 295
    new-instance v0, Lcom/twitter/android/moments/ui/guide/z$3;

    invoke-direct {v0, p0, p3}, Lcom/twitter/android/moments/ui/guide/z$3;-><init>(Lcom/twitter/android/moments/ui/guide/z;Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 304
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/guide/ad;

    .line 305
    check-cast p3, Lcom/twitter/android/moments/viewmodels/w;

    iget-object v1, p3, Lcom/twitter/android/moments/viewmodels/w;->b:Lcom/twitter/android/moments/viewmodels/MomentModule;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/guide/ad;->a(Lcom/twitter/android/moments/viewmodels/MomentModule;)V

    .line 306
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/z;->a:Lcom/twitter/android/moments/ui/guide/p;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/ad;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/ui/guide/p;->b(Landroid/view/View;)V

    goto :goto_0

    .line 310
    :pswitch_4
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/guide/o;

    .line 311
    check-cast p3, Lcom/twitter/android/moments/viewmodels/y;

    invoke-virtual {v0, p3}, Lcom/twitter/android/moments/ui/guide/o;->a(Lcom/twitter/android/moments/viewmodels/y;)V

    goto :goto_0

    .line 317
    :pswitch_5
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/guide/s;

    .line 318
    invoke-virtual {v0, p3}, Lcom/twitter/android/moments/ui/guide/s;->a(Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;)V

    goto :goto_0

    .line 322
    :pswitch_6
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacm;

    .line 323
    invoke-interface {v0}, Lacm;->a()V

    goto :goto_0

    .line 286
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method protected bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 50
    check-cast p3, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/moments/ui/guide/z;->a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;)V

    return-void
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 124
    sget v0, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;->a:I

    return v0
.end method
