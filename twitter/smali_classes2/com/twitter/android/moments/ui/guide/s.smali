.class Lcom/twitter/android/moments/ui/guide/s;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lanj;
.implements Lcom/twitter/android/moments/ui/guide/v;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "Lcom/twitter/media/ui/image/d;",
        ">",
        "Ljava/lang/Object;",
        "Lanj;",
        "Lcom/twitter/android/moments/ui/guide/v;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

.field private final c:Lcom/twitter/android/moments/ui/guide/ai;

.field private final d:Lcom/twitter/android/moments/ui/guide/u;

.field private final e:Lzp$b;

.field private final f:Lcom/twitter/android/av/k;

.field private final g:Lcom/twitter/android/moments/ui/guide/d;

.field private final h:Lcom/twitter/android/moments/ui/guide/l$b;

.field private i:Lcom/twitter/android/moments/viewmodels/MomentModule;

.field private j:Lcom/twitter/android/moments/ui/guide/t;


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/twitter/media/ui/image/AspectRatioFrameLayout;Lcom/twitter/android/moments/ui/guide/ai;Lcom/twitter/android/moments/ui/guide/u;Lzp$b;Lcom/twitter/android/av/k;Lcom/twitter/android/moments/ui/guide/d;Lcom/twitter/android/moments/ui/guide/l$b;Lcom/twitter/android/moments/ui/guide/r;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/s;->a:Landroid/view/View;

    .line 42
    iput-object p2, p0, Lcom/twitter/android/moments/ui/guide/s;->b:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    .line 43
    iput-object p5, p0, Lcom/twitter/android/moments/ui/guide/s;->e:Lzp$b;

    .line 44
    iput-object p3, p0, Lcom/twitter/android/moments/ui/guide/s;->c:Lcom/twitter/android/moments/ui/guide/ai;

    .line 45
    iput-object p4, p0, Lcom/twitter/android/moments/ui/guide/s;->d:Lcom/twitter/android/moments/ui/guide/u;

    .line 46
    iput-object p6, p0, Lcom/twitter/android/moments/ui/guide/s;->f:Lcom/twitter/android/av/k;

    .line 47
    iput-object p7, p0, Lcom/twitter/android/moments/ui/guide/s;->g:Lcom/twitter/android/moments/ui/guide/d;

    .line 48
    iput-object p8, p0, Lcom/twitter/android/moments/ui/guide/s;->h:Lcom/twitter/android/moments/ui/guide/l$b;

    .line 49
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s;->b:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    invoke-virtual {p9}, Lcom/twitter/android/moments/ui/guide/r;->a()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;->setAspectRatio(F)V

    .line 50
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/guide/s;)Lcom/twitter/android/moments/viewmodels/MomentModule;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s;->i:Lcom/twitter/android/moments/viewmodels/MomentModule;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/guide/s;)Lzp$b;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s;->e:Lzp$b;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/guide/s;)Lcom/twitter/android/moments/ui/guide/l$b;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s;->h:Lcom/twitter/android/moments/ui/guide/l$b;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/moments/ui/guide/s;)Lcom/twitter/android/moments/ui/guide/t;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s;->j:Lcom/twitter/android/moments/ui/guide/t;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<GI:",
            "Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;",
            ":",
            "Lcom/twitter/android/moments/viewmodels/h;",
            ">(TGI;)V"
        }
    .end annotation

    .prologue
    .line 61
    move-object v0, p1

    check-cast v0, Lcom/twitter/android/moments/viewmodels/h;

    invoke-interface {v0}, Lcom/twitter/android/moments/viewmodels/h;->a()Lcom/twitter/android/moments/viewmodels/MomentModule;

    move-result-object v1

    .line 62
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;->c()Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;

    move-result-object v2

    .line 63
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s;->i:Lcom/twitter/android/moments/viewmodels/MomentModule;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    invoke-virtual {v1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->b()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/moments/ui/guide/s;->i:Lcom/twitter/android/moments/viewmodels/MomentModule;

    invoke-virtual {v3}, Lcom/twitter/android/moments/viewmodels/MomentModule;->b()Lcom/twitter/model/moments/Moment;

    move-result-object v3

    if-eq v0, v3, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s;->j:Lcom/twitter/android/moments/ui/guide/t;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s;->j:Lcom/twitter/android/moments/ui/guide/t;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/guide/t;->a(Lcom/twitter/android/moments/viewmodels/MomentModule;)V

    .line 69
    iput-object v1, p0, Lcom/twitter/android/moments/ui/guide/s;->i:Lcom/twitter/android/moments/viewmodels/MomentModule;

    .line 97
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s;->a:Landroid/view/View;

    new-instance v1, Lcom/twitter/android/moments/ui/guide/s$1;

    invoke-direct {v1, p0, v2}, Lcom/twitter/android/moments/ui/guide/s$1;-><init>(Lcom/twitter/android/moments/ui/guide/s;Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    return-void

    .line 72
    :cond_1
    invoke-virtual {v1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->a()Lcom/twitter/android/moments/viewmodels/MomentModule$Type;

    move-result-object v0

    sget-object v3, Lcom/twitter/android/moments/viewmodels/MomentModule$Type;->a:Lcom/twitter/android/moments/viewmodels/MomentModule$Type;

    if-ne v0, v3, :cond_4

    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s;->f:Lcom/twitter/android/av/k;

    invoke-virtual {v0}, Lcom/twitter/android/av/k;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, v1

    .line 76
    check-cast v0, Lcom/twitter/android/moments/viewmodels/ah;

    .line 77
    iget-object v3, p0, Lcom/twitter/android/moments/ui/guide/s;->j:Lcom/twitter/android/moments/ui/guide/t;

    if-eqz v3, :cond_2

    .line 78
    iget-object v3, p0, Lcom/twitter/android/moments/ui/guide/s;->j:Lcom/twitter/android/moments/ui/guide/t;

    invoke-virtual {v3}, Lcom/twitter/android/moments/ui/guide/t;->d()V

    .line 80
    :cond_2
    iget-object v3, p0, Lcom/twitter/android/moments/ui/guide/s;->c:Lcom/twitter/android/moments/ui/guide/ai;

    invoke-virtual {v3, v0}, Lcom/twitter/android/moments/ui/guide/ai;->a(Lcom/twitter/android/moments/viewmodels/ah;)V

    .line 81
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s;->c:Lcom/twitter/android/moments/ui/guide/ai;

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/s;->j:Lcom/twitter/android/moments/ui/guide/t;

    .line 91
    :cond_3
    :goto_1
    iput-object v1, p0, Lcom/twitter/android/moments/ui/guide/s;->i:Lcom/twitter/android/moments/viewmodels/MomentModule;

    .line 92
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s;->g:Lcom/twitter/android/moments/ui/guide/d;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/guide/d;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s;->e:Lzp$b;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/s;->i:Lcom/twitter/android/moments/viewmodels/MomentModule;

    invoke-virtual {v1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->b()Lcom/twitter/model/moments/Moment;

    move-result-object v1

    iget-wide v4, v1, Lcom/twitter/model/moments/Moment;->b:J

    invoke-virtual {v0, v4, v5}, Lzp$b;->d(J)V

    goto :goto_0

    .line 82
    :cond_4
    invoke-virtual {v1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->a()Lcom/twitter/android/moments/viewmodels/MomentModule$Type;

    move-result-object v0

    sget-object v3, Lcom/twitter/android/moments/viewmodels/MomentModule$Type;->b:Lcom/twitter/android/moments/viewmodels/MomentModule$Type;

    if-eq v0, v3, :cond_5

    invoke-virtual {v1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->a()Lcom/twitter/android/moments/viewmodels/MomentModule$Type;

    move-result-object v0

    sget-object v3, Lcom/twitter/android/moments/viewmodels/MomentModule$Type;->a:Lcom/twitter/android/moments/viewmodels/MomentModule$Type;

    if-ne v0, v3, :cond_3

    .line 85
    :cond_5
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s;->j:Lcom/twitter/android/moments/ui/guide/t;

    if-eqz v0, :cond_6

    .line 86
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s;->j:Lcom/twitter/android/moments/ui/guide/t;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/t;->d()V

    .line 88
    :cond_6
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s;->d:Lcom/twitter/android/moments/ui/guide/u;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/guide/u;->b(Lcom/twitter/android/moments/viewmodels/MomentModule;)V

    .line 89
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s;->d:Lcom/twitter/android/moments/ui/guide/u;

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/s;->j:Lcom/twitter/android/moments/ui/guide/t;

    goto :goto_1
.end method

.method public aW_()V
    .locals 4

    .prologue
    .line 133
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s;->i:Lcom/twitter/android/moments/viewmodels/MomentModule;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s;->e:Lzp$b;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/s;->i:Lcom/twitter/android/moments/viewmodels/MomentModule;

    invoke-virtual {v1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->b()Lcom/twitter/model/moments/Moment;

    move-result-object v1

    iget-wide v2, v1, Lcom/twitter/model/moments/Moment;->b:J

    invoke-virtual {v0, v2, v3}, Lzp$b;->d(J)V

    .line 136
    :cond_0
    return-void
.end method

.method public am_()V
    .locals 0

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/s;->c()V

    .line 141
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s;->c:Lcom/twitter/android/moments/ui/guide/ai;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/ai;->d()V

    .line 128
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s;->d:Lcom/twitter/android/moments/ui/guide/u;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/u;->d()V

    .line 129
    return-void
.end method
