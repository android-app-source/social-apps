.class Lcom/twitter/android/moments/ui/guide/s$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/guide/s;->a(Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;

.field final synthetic b:Lcom/twitter/android/moments/ui/guide/s;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/guide/s;Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/s$1;->b:Lcom/twitter/android/moments/ui/guide/s;

    iput-object p2, p0, Lcom/twitter/android/moments/ui/guide/s$1;->a:Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 101
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s$1;->a:Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;

    sget-object v1, Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;->l:Lcom/twitter/android/moments/viewmodels/MomentGuideListItem$Type;

    if-ne v0, v1, :cond_0

    .line 102
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s$1;->b:Lcom/twitter/android/moments/ui/guide/s;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/s;->b(Lcom/twitter/android/moments/ui/guide/s;)Lzp$b;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/s$1;->b:Lcom/twitter/android/moments/ui/guide/s;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/guide/s;->a(Lcom/twitter/android/moments/ui/guide/s;)Lcom/twitter/android/moments/viewmodels/MomentModule;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->b()Lcom/twitter/model/moments/Moment;

    move-result-object v1

    iget-wide v2, v1, Lcom/twitter/model/moments/Moment;->b:J

    invoke-virtual {v0, v2, v3}, Lzp$b;->e(J)V

    .line 103
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s$1;->b:Lcom/twitter/android/moments/ui/guide/s;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/s;->c(Lcom/twitter/android/moments/ui/guide/s;)Lcom/twitter/android/moments/ui/guide/l$b;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/s$1;->b:Lcom/twitter/android/moments/ui/guide/s;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/guide/s;->a(Lcom/twitter/android/moments/ui/guide/s;)Lcom/twitter/android/moments/viewmodels/MomentModule;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/moments/ui/guide/l$b;->a(Lcom/twitter/android/moments/viewmodels/MomentModule;)V

    .line 109
    :goto_0
    return-void

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s$1;->b:Lcom/twitter/android/moments/ui/guide/s;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/s;->c(Lcom/twitter/android/moments/ui/guide/s;)Lcom/twitter/android/moments/ui/guide/l$b;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s$1;->b:Lcom/twitter/android/moments/ui/guide/s;

    .line 106
    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/s;->a(Lcom/twitter/android/moments/ui/guide/s;)Lcom/twitter/android/moments/viewmodels/MomentModule;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/s$1;->b:Lcom/twitter/android/moments/ui/guide/s;

    .line 107
    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/s;->d(Lcom/twitter/android/moments/ui/guide/s;)Lcom/twitter/android/moments/ui/guide/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/t;->a()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 106
    invoke-interface {v1, v2, v0}, Lcom/twitter/android/moments/ui/guide/l$b;->a(Lcom/twitter/android/moments/viewmodels/MomentModule;Landroid/view/View;)V

    goto :goto_0
.end method
