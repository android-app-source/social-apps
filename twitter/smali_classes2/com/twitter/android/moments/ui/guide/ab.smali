.class public Lcom/twitter/android/moments/ui/guide/ab;
.super Lcom/twitter/app/common/list/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/guide/ab$a;
    }
.end annotation


# direct methods
.method protected constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/twitter/app/common/list/i;-><init>(Landroid/os/Bundle;)V

    .line 29
    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lcom/twitter/android/moments/ui/guide/ab;
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/twitter/android/moments/ui/guide/ab;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/guide/ab;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/android/moments/ui/guide/ab$a;
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/twitter/android/moments/ui/guide/ab$a;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/guide/ab$a;-><init>(Lcom/twitter/android/moments/ui/guide/ab;)V

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ab;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "guide_category_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ab;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "moments_owner_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ab;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "home_view_tag"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ab;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "guide_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ab;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "guide_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ab;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "guide_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic h()Lcom/twitter/app/common/list/i$a;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/ab;->a()Lcom/twitter/android/moments/ui/guide/ab$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lcom/twitter/app/common/base/b$a;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/ab;->a()Lcom/twitter/android/moments/ui/guide/ab$a;

    move-result-object v0

    return-object v0
.end method

.method public j()J
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ab;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "add_to_moment_tweet_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public k()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ab;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "twitter_scribe_association"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    return-object v0
.end method
