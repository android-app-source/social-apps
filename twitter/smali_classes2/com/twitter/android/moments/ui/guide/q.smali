.class public Lcom/twitter/android/moments/ui/guide/q;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/guide/y;


# instance fields
.field private final a:Lacr;

.field private final b:Lcom/twitter/android/moments/ui/guide/r;


# direct methods
.method constructor <init>(Lacr;Lcom/twitter/android/moments/ui/guide/r;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/q;->a:Lacr;

    .line 26
    iput-object p2, p0, Lcom/twitter/android/moments/ui/guide/q;->b:Lcom/twitter/android/moments/ui/guide/r;

    .line 27
    return-void
.end method

.method public static a(Landroid/view/View;Lcom/twitter/android/moments/ui/guide/r;)Lcom/twitter/android/moments/ui/guide/q;
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/twitter/android/moments/ui/guide/q;

    .line 19
    invoke-static {p0}, Lacr;->b(Landroid/view/View;)Lacr;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/twitter/android/moments/ui/guide/q;-><init>(Lacr;Lcom/twitter/android/moments/ui/guide/r;)V

    .line 18
    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/Moment;)V
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/q;->b:Lcom/twitter/android/moments/ui/guide/r;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/r;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/q;->a:Lacr;

    iget-object v1, p1, Lcom/twitter/model/moments/Moment;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lacr;->b(Ljava/lang/String;)V

    .line 36
    :goto_0
    iget-object v0, p1, Lcom/twitter/model/moments/Moment;->o:Lcom/twitter/model/moments/a;

    if-eqz v0, :cond_1

    .line 37
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/q;->a:Lacr;

    iget-object v1, p1, Lcom/twitter/model/moments/Moment;->o:Lcom/twitter/model/moments/a;

    invoke-virtual {v0, v1}, Lacr;->a(Lcom/twitter/model/moments/a;)V

    .line 43
    :goto_1
    return-void

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/q;->a:Lacr;

    invoke-virtual {v0}, Lacr;->f()V

    goto :goto_0

    .line 38
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/q;->b:Lcom/twitter/android/moments/ui/guide/r;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/r;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/q;->a:Lacr;

    iget-object v1, p1, Lcom/twitter/model/moments/Moment;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lacr;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 41
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/q;->a:Lacr;

    invoke-virtual {v0}, Lacr;->c()V

    goto :goto_1
.end method

.method public a(Lcom/twitter/model/moments/a;)V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/q;->a:Lacr;

    invoke-virtual {v0, p1}, Lacr;->a(Lcom/twitter/model/moments/a;)V

    .line 53
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/q;->a:Lacr;

    invoke-virtual {v0, p1}, Lacr;->a(Ljava/lang/String;)V

    .line 48
    return-void
.end method
