.class public Lcom/twitter/android/moments/ui/guide/a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/guide/a$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Lakp;

.field private final c:Lcom/twitter/android/moments/ui/guide/a$a;

.field private final d:Ljava/lang/Long;


# direct methods
.method protected constructor <init>(Landroid/content/res/Resources;Lakp;Lcom/twitter/android/moments/ui/guide/a$a;J)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/a;->a:Landroid/content/res/Resources;

    .line 38
    iput-object p2, p0, Lcom/twitter/android/moments/ui/guide/a;->b:Lakp;

    .line 39
    iput-object p3, p0, Lcom/twitter/android/moments/ui/guide/a;->c:Lcom/twitter/android/moments/ui/guide/a$a;

    .line 40
    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/a;->d:Ljava/lang/Long;

    .line 41
    return-void
.end method

.method public static a(Landroid/app/Activity;J)Lcom/twitter/android/moments/ui/guide/a;
    .locals 7

    .prologue
    .line 31
    new-instance v0, Lcom/twitter/android/moments/ui/guide/a;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-instance v2, Lakp;

    invoke-direct {v2, p0}, Lakp;-><init>(Landroid/app/Activity;)V

    new-instance v3, Lcom/twitter/android/moments/ui/guide/a$a;

    invoke-direct {v3, p0}, Lcom/twitter/android/moments/ui/guide/a$a;-><init>(Landroid/app/Activity;)V

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/moments/ui/guide/a;-><init>(Landroid/content/res/Resources;Lakp;Lcom/twitter/android/moments/ui/guide/a$a;J)V

    return-object v0
.end method

.method private a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/app/common/base/SnackbarData;
    .locals 7

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/a;->c:Lcom/twitter/android/moments/ui/guide/a$a;

    iget-wide v2, p1, Lcom/twitter/model/moments/Moment;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/moments/ui/guide/a$a;->b(J)Lcom/twitter/android/moments/ui/guide/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/a;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/moments/ui/guide/a$a;->a(J)Lcom/twitter/android/moments/ui/guide/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/a$a;->a()Landroid/content/Intent;

    move-result-object v0

    .line 59
    new-instance v1, Lcom/twitter/app/common/base/SnackbarData$a;

    invoke-direct {v1}, Lcom/twitter/app/common/base/SnackbarData$a;-><init>()V

    iget-object v2, p0, Lcom/twitter/android/moments/ui/guide/a;->a:Landroid/content/res/Resources;

    const v3, 0x7f0a0597

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p1, Lcom/twitter/model/moments/Moment;->c:Ljava/lang/String;

    aput-object v6, v4, v5

    .line 60
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/app/common/base/SnackbarData$a;->a(Ljava/lang/String;)Lcom/twitter/app/common/base/SnackbarData$a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/moments/ui/guide/a;->a:Landroid/content/res/Resources;

    const v3, 0x7f0a0335

    .line 61
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/app/common/base/SnackbarData$a;->b(Ljava/lang/String;)Lcom/twitter/app/common/base/SnackbarData$a;

    move-result-object v1

    .line 62
    invoke-virtual {v1, v0}, Lcom/twitter/app/common/base/SnackbarData$a;->a(Landroid/content/Intent;)Lcom/twitter/app/common/base/SnackbarData$a;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lcom/twitter/app/common/base/SnackbarData$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/base/SnackbarData;

    .line 59
    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/util/collection/k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p1}, Lcom/twitter/util/collection/k;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    invoke-virtual {p1}, Lcom/twitter/util/collection/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/Moment;

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/guide/a;->a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/app/common/base/SnackbarData;

    move-result-object v0

    .line 46
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/a;->b:Lakp;

    new-instance v2, Lcom/twitter/android/moments/ui/guide/aa;

    invoke-direct {v2}, Lcom/twitter/android/moments/ui/guide/aa;-><init>()V

    .line 47
    invoke-virtual {v1, v2, p1}, Lakp;->a(Lakw;Ljava/lang/Object;)Lakp;

    move-result-object v1

    new-instance v2, Lcom/twitter/app/common/base/g;

    invoke-direct {v2}, Lcom/twitter/app/common/base/g;-><init>()V

    .line 48
    invoke-virtual {v1, v2, v0}, Lakp;->a(Lakw;Ljava/lang/Object;)Lakp;

    move-result-object v0

    const/4 v1, -0x1

    .line 49
    invoke-virtual {v0, v1}, Lakp;->a(I)V

    .line 53
    :goto_0
    return-void

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/a;->b:Lakp;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lakp;->a(I)V

    goto :goto_0
.end method
