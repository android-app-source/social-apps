.class public Lcom/twitter/android/moments/ui/guide/h$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/guide/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/app/Activity;

.field private final c:Landroid/content/res/Resources;

.field private final d:Lcom/twitter/android/moments/ui/guide/g;

.field private final e:Lxh;

.field private final f:Lacj;

.field private final g:Lcom/twitter/util/object/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/e",
            "<",
            "Lcom/twitter/android/moments/ui/guide/n;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Laad;

.field private final i:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/Activity;Landroid/content/res/Resources;Lcom/twitter/android/moments/ui/guide/g;Lxh;Lacj;Lcom/twitter/util/object/e;JLaad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/app/Activity;",
            "Landroid/content/res/Resources;",
            "Lcom/twitter/android/moments/ui/guide/g;",
            "Lxh;",
            "Lacj;",
            "Lcom/twitter/util/object/e",
            "<",
            "Lcom/twitter/android/moments/ui/guide/n;",
            ">;J",
            "Laad;",
            ")V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/h$a;->a:Landroid/content/Context;

    .line 68
    iput-object p2, p0, Lcom/twitter/android/moments/ui/guide/h$a;->b:Landroid/app/Activity;

    .line 69
    iput-object p3, p0, Lcom/twitter/android/moments/ui/guide/h$a;->c:Landroid/content/res/Resources;

    .line 70
    iput-object p4, p0, Lcom/twitter/android/moments/ui/guide/h$a;->d:Lcom/twitter/android/moments/ui/guide/g;

    .line 71
    iput-object p5, p0, Lcom/twitter/android/moments/ui/guide/h$a;->e:Lxh;

    .line 72
    iput-object p6, p0, Lcom/twitter/android/moments/ui/guide/h$a;->f:Lacj;

    .line 73
    iput-object p7, p0, Lcom/twitter/android/moments/ui/guide/h$a;->g:Lcom/twitter/util/object/e;

    .line 74
    iput-wide p8, p0, Lcom/twitter/android/moments/ui/guide/h$a;->i:J

    .line 75
    iput-object p10, p0, Lcom/twitter/android/moments/ui/guide/h$a;->h:Laad;

    .line 76
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/ui/guide/ab;)Lcom/twitter/android/moments/ui/guide/h;
    .locals 10

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/h$a;->c:Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/r;->a(Landroid/content/res/Resources;)Lcom/twitter/android/moments/ui/guide/r;

    move-result-object v6

    .line 86
    invoke-virtual {p1}, Lcom/twitter/android/moments/ui/guide/ab;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    invoke-virtual {p1}, Lcom/twitter/android/moments/ui/guide/ab;->j()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 88
    invoke-virtual {p1}, Lcom/twitter/android/moments/ui/guide/ab;->j()J

    move-result-wide v2

    .line 89
    invoke-static {}, Lrx/c;->d()Lrx/c;

    move-result-object v9

    .line 90
    new-instance v8, Lcom/twitter/android/moments/viewmodels/c;

    invoke-direct {v8}, Lcom/twitter/android/moments/viewmodels/c;-><init>()V

    .line 91
    new-instance v7, Lcom/twitter/android/moments/viewmodels/ag;

    invoke-direct {v7}, Lcom/twitter/android/moments/viewmodels/ag;-><init>()V

    .line 93
    invoke-virtual {p1}, Lcom/twitter/android/moments/ui/guide/ab;->k()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 94
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/h$a;->b:Landroid/app/Activity;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/h$a;->e:Lxh;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/guide/h$a;->h:Laad;

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/moments/ui/guide/b;->a(Landroid/app/Activity;Lxh;JLcom/twitter/analytics/feature/model/TwitterScribeAssociation;Laad;)Lcom/twitter/android/moments/ui/guide/b;

    move-result-object v0

    move-object v5, v6

    move-object v4, v0

    move-object v3, v7

    move-object v2, v8

    move-object v1, v9

    .line 138
    :goto_0
    new-instance v0, Lcom/twitter/android/moments/ui/guide/h;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/moments/ui/guide/h;-><init>(Lrx/c;Lcom/twitter/android/moments/viewmodels/e;Lcom/twitter/android/moments/viewmodels/d;Lcom/twitter/android/moments/ui/guide/l$b;Lcom/twitter/android/moments/ui/guide/r;)V

    return-object v0

    .line 97
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string/jumbo v1, "Trying to add To moment guide without tweet id"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 99
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/android/moments/ui/guide/ab;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 100
    invoke-static {}, Lrx/c;->d()Lrx/c;

    move-result-object v1

    .line 101
    new-instance v2, Lcom/twitter/android/moments/viewmodels/c;

    invoke-direct {v2}, Lcom/twitter/android/moments/viewmodels/c;-><init>()V

    .line 102
    invoke-virtual {p1}, Lcom/twitter/android/moments/ui/guide/ab;->c()J

    move-result-wide v4

    iget-wide v8, p0, Lcom/twitter/android/moments/ui/guide/h$a;->i:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_3

    .line 103
    new-instance v3, Lcom/twitter/android/moments/viewmodels/ag;

    invoke-direct {v3}, Lcom/twitter/android/moments/viewmodels/ag;-><init>()V

    .line 104
    invoke-static {}, Lbrz;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/h$a;->b:Landroid/app/Activity;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/guide/h$a;->d:Lcom/twitter/android/moments/ui/guide/g;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/guide/h$a;->h:Laad;

    .line 105
    invoke-static {v0, v4, v5}, Lcom/twitter/android/moments/ui/guide/ah;->a(Landroid/app/Activity;Lcom/twitter/android/moments/ui/guide/g;Laad;)Lcom/twitter/android/moments/ui/guide/ah;

    move-result-object v0

    :goto_1
    move-object v5, v6

    move-object v4, v0

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/twitter/android/moments/ui/guide/af;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/guide/h$a;->d:Lcom/twitter/android/moments/ui/guide/g;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/guide/h$a;->h:Laad;

    invoke-direct {v0, v4, v5}, Lcom/twitter/android/moments/ui/guide/af;-><init>(Lcom/twitter/android/moments/ui/guide/g;Laad;)V

    goto :goto_1

    .line 109
    :cond_3
    sget-object v3, Lcom/twitter/android/moments/viewmodels/d;->a:Lcom/twitter/android/moments/viewmodels/d;

    .line 110
    new-instance v0, Lcom/twitter/android/moments/ui/guide/af;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/guide/h$a;->d:Lcom/twitter/android/moments/ui/guide/g;

    iget-object v5, p0, Lcom/twitter/android/moments/ui/guide/h$a;->h:Laad;

    invoke-direct {v0, v4, v5}, Lcom/twitter/android/moments/ui/guide/af;-><init>(Lcom/twitter/android/moments/ui/guide/g;Laad;)V

    move-object v5, v6

    move-object v4, v0

    goto :goto_0

    .line 113
    :cond_4
    invoke-virtual {p1}, Lcom/twitter/android/moments/ui/guide/ab;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 115
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/h$a;->f:Lacj;

    invoke-virtual {v0}, Lacj;->a()Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/guide/h$a$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/guide/h$a$1;-><init>(Lcom/twitter/android/moments/ui/guide/h$a;)V

    .line 116
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v4

    .line 123
    invoke-static {}, Lwk;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/h$a;->c:Landroid/content/res/Resources;

    .line 124
    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/r;->b(Landroid/content/res/Resources;)Lcom/twitter/android/moments/ui/guide/r;

    move-result-object v0

    move-object v1, v0

    .line 126
    :goto_2
    invoke-static {}, Lwk;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Lcom/twitter/android/moments/viewmodels/g;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/guide/h$a;->c:Landroid/content/res/Resources;

    invoke-direct {v0, v2}, Lcom/twitter/android/moments/viewmodels/g;-><init>(Landroid/content/res/Resources;)V

    move-object v2, v0

    .line 129
    :goto_3
    sget-object v3, Lcom/twitter/android/moments/viewmodels/d;->a:Lcom/twitter/android/moments/viewmodels/d;

    .line 130
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/h$a;->g:Lcom/twitter/util/object/e;

    invoke-interface {v0}, Lcom/twitter/util/object/e;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/guide/l$b;

    move-object v5, v1

    move-object v1, v4

    move-object v4, v0

    goto/16 :goto_0

    .line 124
    :cond_5
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/h$a;->c:Landroid/content/res/Resources;

    .line 125
    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/r;->a(Landroid/content/res/Resources;)Lcom/twitter/android/moments/ui/guide/r;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    .line 126
    :cond_6
    new-instance v0, Lcom/twitter/android/moments/viewmodels/f;

    new-instance v2, Lcom/twitter/android/moments/viewmodels/c;

    invoke-direct {v2}, Lcom/twitter/android/moments/viewmodels/c;-><init>()V

    invoke-direct {v0, v2}, Lcom/twitter/android/moments/viewmodels/f;-><init>(Lcom/twitter/android/moments/viewmodels/e;)V

    move-object v2, v0

    goto :goto_3

    .line 133
    :cond_7
    invoke-static {}, Lrx/c;->d()Lrx/c;

    move-result-object v1

    .line 134
    new-instance v2, Lcom/twitter/android/moments/viewmodels/c;

    invoke-direct {v2}, Lcom/twitter/android/moments/viewmodels/c;-><init>()V

    .line 135
    sget-object v3, Lcom/twitter/android/moments/viewmodels/d;->a:Lcom/twitter/android/moments/viewmodels/d;

    .line 136
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/h$a;->g:Lcom/twitter/util/object/e;

    invoke-interface {v0}, Lcom/twitter/util/object/e;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/guide/l$b;

    move-object v5, v6

    move-object v4, v0

    goto/16 :goto_0
.end method
