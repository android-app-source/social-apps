.class Lcom/twitter/android/moments/ui/guide/l$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/guide/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/q",
        "<",
        "Lcom/twitter/android/moments/viewmodels/MomentGuide;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/ui/guide/l;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/guide/l;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/l$1;->a:Lcom/twitter/android/moments/ui/guide/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEvent(Lcom/twitter/android/moments/viewmodels/MomentGuide;)V
    .locals 3

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l$1;->a:Lcom/twitter/android/moments/ui/guide/l;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/l;->a(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/ui/guide/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/w;->g()V

    .line 67
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l$1;->a:Lcom/twitter/android/moments/ui/guide/l;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/l;->b(Lcom/twitter/android/moments/ui/guide/l;)Lzp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lzp;->a(Lcom/twitter/android/moments/viewmodels/MomentGuide;)V

    .line 68
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l$1;->a:Lcom/twitter/android/moments/ui/guide/l;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/l;->c(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/data/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l$1;->a:Lcom/twitter/android/moments/ui/guide/l;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/l;->a(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/ui/guide/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/w;->a()V

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l$1;->a:Lcom/twitter/android/moments/ui/guide/l;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/l;->d(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/data/j;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/android/moments/viewmodels/MomentGuide;->a:Ljava/util/Map;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/data/j;->putAll(Ljava/util/Map;)V

    .line 73
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l$1;->a:Lcom/twitter/android/moments/ui/guide/l;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/l;->e(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/data/j;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/android/moments/viewmodels/MomentGuide;->b:Ljava/util/Map;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/data/j;->putAll(Ljava/util/Map;)V

    .line 75
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l$1;->a:Lcom/twitter/android/moments/ui/guide/l;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/l;->f(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/viewmodels/e;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/twitter/android/moments/viewmodels/e;->a(Lcom/twitter/android/moments/viewmodels/MomentGuide;)Ljava/util/List;

    move-result-object v0

    .line 76
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/l$1;->a:Lcom/twitter/android/moments/ui/guide/l;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/guide/l;->g(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/viewmodels/d;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/twitter/android/moments/viewmodels/d;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 77
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/l$1;->a:Lcom/twitter/android/moments/ui/guide/l;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/guide/l;->h(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/ui/guide/z;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/guide/z;->k()Lcjt;

    move-result-object v1

    new-instance v2, Lcbl;

    invoke-direct {v2, v0}, Lcbl;-><init>(Ljava/lang/Iterable;)V

    invoke-interface {v1, v2}, Lcjt;->a(Lcbi;)Lcbi;

    .line 78
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 79
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l$1;->a:Lcom/twitter/android/moments/ui/guide/l;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/l;->a(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/ui/guide/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/w;->d()V

    goto :goto_0

    .line 80
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l$1;->a:Lcom/twitter/android/moments/ui/guide/l;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/l;->a(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/ui/guide/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/w;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l$1;->a:Lcom/twitter/android/moments/ui/guide/l;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/l;->c(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/data/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/f;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l$1;->a:Lcom/twitter/android/moments/ui/guide/l;

    .line 81
    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/l;->c(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/data/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/f;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/l$1;->a:Lcom/twitter/android/moments/ui/guide/l;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/l;->a(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/ui/guide/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/w;->b()V

    goto :goto_0
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 63
    check-cast p1, Lcom/twitter/android/moments/viewmodels/MomentGuide;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/guide/l$1;->onEvent(Lcom/twitter/android/moments/viewmodels/MomentGuide;)V

    return-void
.end method
