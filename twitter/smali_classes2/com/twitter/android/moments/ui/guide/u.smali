.class Lcom/twitter/android/moments/ui/guide/u;
.super Lcom/twitter/android/moments/ui/guide/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/moments/ui/guide/t",
        "<",
        "Lcom/twitter/android/moments/viewmodels/MomentModule;",
        "Lcom/twitter/media/ui/image/MediaImageView;",
        ">;"
    }
.end annotation


# instance fields
.field private l:Lcom/twitter/media/ui/image/MediaImageView;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/android/moments/ui/guide/y;Lcom/twitter/android/moments/ui/guide/p;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/guide/g;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/w;Lzp$b;Lcom/twitter/android/moments/data/am;Lcom/twitter/android/moments/ui/guide/l$b;Lcom/twitter/android/moments/ui/guide/r;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct/range {p0 .. p13}, Lcom/twitter/android/moments/ui/guide/t;-><init>(Landroid/content/Context;Lcom/twitter/android/moments/ui/guide/y;Lcom/twitter/android/moments/ui/guide/p;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/guide/g;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/w;Lzp$b;Lcom/twitter/android/moments/data/am;Lcom/twitter/android/moments/ui/guide/l$b;Lcom/twitter/android/moments/ui/guide/r;)V

    .line 47
    return-void
.end method


# virtual methods
.method public synthetic a()Landroid/view/View;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/u;->c()Lcom/twitter/media/ui/image/MediaImageView;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/twitter/android/moments/viewmodels/MomentModule;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 59
    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/guide/u;->a(Lcom/twitter/android/moments/viewmodels/MomentModule;)V

    .line 61
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->a()Lcom/twitter/android/moments/viewmodels/MomentModule$Type;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/moments/viewmodels/MomentModule$Type;->a:Lcom/twitter/android/moments/viewmodels/MomentModule$Type;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->a()Lcom/twitter/android/moments/viewmodels/MomentModule$Type;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/moments/viewmodels/MomentModule$Type;->b:Lcom/twitter/android/moments/viewmodels/MomentModule$Type;

    if-eq v0, v1, :cond_0

    .line 101
    :goto_0
    return-void

    .line 70
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->a()Lcom/twitter/android/moments/viewmodels/MomentModule$Type;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/moments/viewmodels/MomentModule$Type;->a:Lcom/twitter/android/moments/viewmodels/MomentModule$Type;

    if-ne v0, v1, :cond_2

    .line 71
    check-cast p1, Lcom/twitter/android/moments/viewmodels/ah;

    .line 72
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/ah;->h()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/av/playback/ab;->m(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/card/property/ImageSpec;

    move-result-object v0

    .line 75
    if-eqz v0, :cond_1

    .line 76
    iget-object v1, v0, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    iget v1, v1, Lcom/twitter/model/card/property/Vector2F;->x:F

    iget-object v3, v0, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    iget v3, v3, Lcom/twitter/model/card/property/Vector2F;->y:F

    invoke-static {v1, v3}, Lcom/twitter/util/math/Size;->a(FF)Lcom/twitter/util/math/Size;

    move-result-object v1

    .line 77
    iget-object v3, p1, Lcom/twitter/android/moments/viewmodels/ah;->b:Lcom/twitter/model/moments/e;

    invoke-static {v3}, Lcom/twitter/model/moments/e;->a(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/d;

    move-result-object v3

    .line 78
    iget-object v0, v0, Lcom/twitter/model/card/property/ImageSpec;->b:Ljava/lang/String;

    :goto_1
    move-object v4, v1

    move-object v5, v3

    move-object v3, v0

    .line 91
    :goto_2
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/u;->c:Landroid/view/ViewGroup;

    const v1, 0x7f13051e

    .line 92
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/FillCropFrameLayout;

    .line 93
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/u;->h:Landroid/view/LayoutInflater;

    const v6, 0x7f04034c

    const/4 v7, 0x0

    .line 94
    invoke-virtual {v1, v6, v0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v1, p0, Lcom/twitter/android/moments/ui/guide/u;->l:Lcom/twitter/media/ui/image/MediaImageView;

    .line 96
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/u;->l:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/FillCropFrameLayout;->addView(Landroid/view/View;)V

    .line 98
    if-nez v5, :cond_3

    :goto_3
    invoke-virtual {v0, v4, v2}, Lcom/twitter/android/moments/ui/FillCropFrameLayout;->a(Lcom/twitter/util/math/Size;Landroid/graphics/Rect;)V

    .line 99
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/u;->l:Lcom/twitter/media/ui/image/MediaImageView;

    sget-object v1, Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;->a:Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setScaleType(Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;)V

    .line 100
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/u;->l:Lcom/twitter/media/ui/image/MediaImageView;

    new-instance v1, Lcom/twitter/media/request/a$a;

    invoke-direct {v1, v3}, Lcom/twitter/media/request/a$a;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    goto :goto_0

    .line 80
    :cond_1
    sget-object v0, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    move-object v1, v0

    move-object v3, v2

    move-object v0, v2

    .line 82
    goto :goto_1

    .line 85
    :cond_2
    check-cast p1, Lcom/twitter/android/moments/viewmodels/i;

    .line 86
    iget-object v1, p1, Lcom/twitter/android/moments/viewmodels/i;->a:Lcom/twitter/util/math/Size;

    .line 87
    iget-object v0, p1, Lcom/twitter/android/moments/viewmodels/i;->c:Lcom/twitter/model/moments/e;

    invoke-static {v0}, Lcom/twitter/model/moments/e;->a(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/d;

    move-result-object v3

    .line 88
    iget-object v0, p1, Lcom/twitter/android/moments/viewmodels/i;->b:Ljava/lang/String;

    move-object v4, v1

    move-object v5, v3

    move-object v3, v0

    goto :goto_2

    .line 98
    :cond_3
    invoke-virtual {v5}, Lcom/twitter/model/moments/d;->a()Landroid/graphics/Rect;

    move-result-object v2

    goto :goto_3
.end method

.method public c()Lcom/twitter/media/ui/image/MediaImageView;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/u;->l:Lcom/twitter/media/ui/image/MediaImageView;

    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0}, Lcom/twitter/android/moments/ui/guide/t;->d()V

    .line 52
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/u;->c:Landroid/view/ViewGroup;

    const v1, 0x7f13051e

    .line 53
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/FillCropFrameLayout;

    .line 54
    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/FillCropFrameLayout;->removeAllViews()V

    .line 55
    return-void
.end method
