.class public Lcom/twitter/android/moments/ui/guide/f;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/guide/y;


# instance fields
.field private final a:Lacr;


# direct methods
.method constructor <init>(Lacr;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/f;->a:Lacr;

    .line 29
    return-void
.end method

.method public static a(Landroid/view/View;)Lcom/twitter/android/moments/ui/guide/f;
    .locals 2

    .prologue
    .line 16
    new-instance v0, Lcom/twitter/android/moments/ui/guide/f;

    .line 17
    invoke-static {p0}, Lacr;->b(Landroid/view/View;)Lacr;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/moments/ui/guide/f;-><init>(Lacr;)V

    .line 16
    return-object v0
.end method

.method public static b(Landroid/view/View;)Lcom/twitter/android/moments/ui/guide/f;
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcom/twitter/android/moments/ui/guide/f;

    .line 23
    invoke-static {p0}, Lacr;->a(Landroid/view/View;)Lacr;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/moments/ui/guide/f;-><init>(Lacr;)V

    .line 22
    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/Moment;)V
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/f;->a:Lacr;

    invoke-virtual {v0}, Lacr;->f()V

    .line 34
    iget-object v0, p1, Lcom/twitter/model/moments/Moment;->o:Lcom/twitter/model/moments/a;

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/f;->a:Lacr;

    iget-object v1, p1, Lcom/twitter/model/moments/Moment;->o:Lcom/twitter/model/moments/a;

    invoke-virtual {v0, v1}, Lacr;->a(Lcom/twitter/model/moments/a;)V

    .line 39
    :goto_0
    return-void

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/f;->a:Lacr;

    iget-object v1, p1, Lcom/twitter/model/moments/Moment;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lacr;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/moments/a;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/f;->a:Lacr;

    invoke-virtual {v0, p1}, Lacr;->a(Lcom/twitter/model/moments/a;)V

    .line 49
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/f;->a:Lacr;

    invoke-virtual {v0, p1}, Lacr;->a(Ljava/lang/String;)V

    .line 44
    return-void
.end method
