.class public Lcom/twitter/android/moments/ui/guide/af;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/guide/l$b;


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/guide/g;

.field private final b:Laad;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/guide/g;Laad;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/af;->a:Lcom/twitter/android/moments/ui/guide/g;

    .line 21
    iput-object p2, p0, Lcom/twitter/android/moments/ui/guide/af;->b:Laad;

    .line 22
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 34
    const-string/jumbo v0, "Create is not supported in moment picker."

    invoke-static {v0}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    .line 35
    return-void
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/MomentModule;)V
    .locals 1

    .prologue
    .line 39
    const-string/jumbo v0, "Module pivot is not supported in moment picker."

    invoke-static {v0}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    .line 40
    return-void
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/MomentModule;Landroid/view/View;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ":",
            "Lcom/twitter/media/ui/image/d;",
            ">(",
            "Lcom/twitter/android/moments/viewmodels/MomentModule;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/af;->b:Laad;

    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->b()Lcom/twitter/model/moments/Moment;

    move-result-object v1

    invoke-virtual {v0, v1}, Laad;->c(Lcom/twitter/model/moments/Moment;)V

    .line 28
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/af;->a:Lcom/twitter/android/moments/ui/guide/g;

    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->b()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    iget-wide v2, v0, Lcom/twitter/model/moments/Moment;->b:J

    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->e()Lcom/twitter/model/moments/DisplayStyle;

    move-result-object v4

    move-object v5, p1

    move-object v6, p2

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/moments/ui/guide/g;->a(JLcom/twitter/model/moments/DisplayStyle;Lcom/twitter/android/moments/viewmodels/MomentModule;Landroid/view/View;)V

    .line 30
    return-void
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/x;)V
    .locals 1

    .prologue
    .line 44
    const-string/jumbo v0, "Guide pivot is not supported in moment picker."

    invoke-static {v0}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    .line 45
    return-void
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/y;)V
    .locals 1

    .prologue
    .line 49
    const-string/jumbo v0, "Trend item is not supported in moment picker."

    invoke-static {v0}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    .line 50
    return-void
.end method
