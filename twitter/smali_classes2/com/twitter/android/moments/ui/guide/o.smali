.class Lcom/twitter/android/moments/ui/guide/o;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lajq;
.implements Lcom/twitter/android/moments/ui/guide/v;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lajq",
        "<",
        "Lcom/twitter/android/moments/viewmodels/y;",
        ">;",
        "Lcom/twitter/android/moments/ui/guide/v;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/trends/b;

.field private final b:Lcom/twitter/android/moments/ui/guide/l$b;

.field private final c:Laac;

.field private d:Lcom/twitter/model/moments/i;


# direct methods
.method constructor <init>(Lcom/twitter/android/trends/b;Lcom/twitter/android/moments/ui/guide/l$b;Laac;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/o;->a:Lcom/twitter/android/trends/b;

    .line 39
    iput-object p2, p0, Lcom/twitter/android/moments/ui/guide/o;->b:Lcom/twitter/android/moments/ui/guide/l$b;

    .line 40
    iput-object p3, p0, Lcom/twitter/android/moments/ui/guide/o;->c:Laac;

    .line 41
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/guide/o;)Lcom/twitter/android/moments/ui/guide/l$b;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/o;->b:Lcom/twitter/android/moments/ui/guide/l$b;

    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/guide/l$b;Laac;)Lcom/twitter/android/moments/ui/guide/o;
    .locals 2

    .prologue
    .line 31
    new-instance v0, Lcom/twitter/android/moments/ui/guide/o;

    invoke-static {p1, p2, p0}, Lcom/twitter/android/trends/b;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/content/res/Resources;)Lcom/twitter/android/trends/b;

    move-result-object v1

    invoke-direct {v0, v1, p3, p4}, Lcom/twitter/android/moments/ui/guide/o;-><init>(Lcom/twitter/android/trends/b;Lcom/twitter/android/moments/ui/guide/l$b;Laac;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/viewmodels/y;)V
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/y;->a()Lcom/twitter/model/moments/i;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/o;->d:Lcom/twitter/model/moments/i;

    .line 52
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/o;->a:Lcom/twitter/android/trends/b;

    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/y;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/trends/a;->a(Lcom/twitter/android/trends/b;Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/o;->a:Lcom/twitter/android/trends/b;

    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/y;->f()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/trends/b;->a(Ljava/util/List;)V

    .line 54
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/y;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/o;->a:Lcom/twitter/android/trends/b;

    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/y;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/trends/b;->c(Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/o;->a:Lcom/twitter/android/trends/b;

    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/y;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/trends/b;->d(Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/o;->a:Lcom/twitter/android/trends/b;

    invoke-virtual {v0}, Lcom/twitter/android/trends/b;->b()V

    .line 63
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/o;->a:Lcom/twitter/android/trends/b;

    new-instance v1, Lcom/twitter/android/moments/ui/guide/o$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/guide/o$1;-><init>(Lcom/twitter/android/moments/ui/guide/o;Lcom/twitter/android/moments/viewmodels/y;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/trends/b;->a(Landroid/view/View$OnClickListener;)V

    .line 70
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/o;->aW_()V

    .line 71
    return-void

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/o;->a:Lcom/twitter/android/trends/b;

    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/y;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/trends/b;->b(Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/o;->a:Lcom/twitter/android/trends/b;

    invoke-virtual {v0}, Lcom/twitter/android/trends/b;->d()V

    .line 61
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/o;->a:Lcom/twitter/android/trends/b;

    invoke-virtual {v0}, Lcom/twitter/android/trends/b;->c()V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lcom/twitter/android/moments/viewmodels/y;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/guide/o;->a(Lcom/twitter/android/moments/viewmodels/y;)V

    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/o;->a:Lcom/twitter/android/trends/b;

    invoke-virtual {v0}, Lcom/twitter/android/trends/b;->aN_()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public aW_()V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/o;->d:Lcom/twitter/model/moments/i;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/o;->c:Laac;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/o;->d:Lcom/twitter/model/moments/i;

    invoke-virtual {v0, v1}, Laac;->a(Lcom/twitter/model/moments/i;)V

    .line 78
    :cond_0
    return-void
.end method
