.class public Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$a;
.super Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/guide/ModernGuideActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public final a:J
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method public constructor <init>(JJLcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 3

    .prologue
    .line 154
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$b;-><init>()V

    .line 155
    iput-wide p3, p0, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$a;->a:J

    .line 156
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "moments_owner_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 157
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "add_to_moment_tweet_id"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 158
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "twitter_scribe_association"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 159
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "guide_type"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 161
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$a;->d(Z)Lcom/twitter/app/common/base/h;

    .line 162
    return-void
.end method
