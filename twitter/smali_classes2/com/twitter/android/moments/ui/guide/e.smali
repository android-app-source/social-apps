.class public Lcom/twitter/android/moments/ui/guide/e;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/guide/m;


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/guide/l;

.field private final b:Lcqt;

.field private c:J


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/guide/l;Lcqt;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/android/moments/ui/guide/e;->c:J

    .line 27
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/e;->a:Lcom/twitter/android/moments/ui/guide/l;

    .line 28
    iput-object p2, p0, Lcom/twitter/android/moments/ui/guide/e;->b:Lcqt;

    .line 29
    return-void
.end method

.method public static a(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/ui/guide/e;
    .locals 2

    .prologue
    .line 22
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->a()Lcqt;

    move-result-object v0

    .line 23
    new-instance v1, Lcom/twitter/android/moments/ui/guide/e;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/moments/ui/guide/e;-><init>(Lcom/twitter/android/moments/ui/guide/l;Lcqt;)V

    return-object v1
.end method

.method private d()J
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/e;->b:Lcqt;

    invoke-interface {v0}, Lcqt;->b()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public a()Z
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/guide/e;->d()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/moments/ui/guide/e;->c:J

    .line 34
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/e;->a:Lcom/twitter/android/moments/ui/guide/l;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/l;->e()Z

    move-result v0

    return v0
.end method

.method public b()Z
    .locals 6

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/twitter/android/moments/ui/guide/e;->c:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/moments/ui/guide/e;->d()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/twitter/android/moments/ui/guide/e;->c:J

    sub-long/2addr v0, v2

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0xa

    .line 40
    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 39
    :goto_0
    return v0

    .line 40
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 44
    return-void
.end method
