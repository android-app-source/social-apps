.class public Lcom/twitter/android/moments/ui/guide/ag;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/guide/m;


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/guide/e;

.field private final b:Lcif;

.field private c:Z

.field private d:Lrx/j;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/guide/e;Lcif;)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/ag;->c:Z

    .line 33
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/ag;->a:Lcom/twitter/android/moments/ui/guide/e;

    .line 35
    iput-object p2, p0, Lcom/twitter/android/moments/ui/guide/ag;->b:Lcif;

    .line 36
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ag;->b:Lcif;

    invoke-virtual {v0}, Lcif;->a()Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/guide/ag$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/guide/ag$1;-><init>(Lcom/twitter/android/moments/ui/guide/ag;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/ag;->d:Lrx/j;

    .line 42
    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/ui/guide/ag;
    .locals 3

    .prologue
    .line 25
    new-instance v0, Lcif;

    new-instance v1, Lcrc;

    .line 26
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    invoke-direct {v1, v2}, Lcrc;-><init>(Landroid/support/v4/content/LocalBroadcastManager;)V

    invoke-direct {v0, v1}, Lcif;-><init>(Lcrc;)V

    .line 27
    invoke-static {p1}, Lcom/twitter/android/moments/ui/guide/e;->a(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/ui/guide/e;

    move-result-object v1

    .line 28
    new-instance v2, Lcom/twitter/android/moments/ui/guide/ag;

    invoke-direct {v2, v1, v0}, Lcom/twitter/android/moments/ui/guide/ag;-><init>(Lcom/twitter/android/moments/ui/guide/e;Lcif;)V

    return-object v2
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/guide/ag;Z)Z
    .locals 0

    .prologue
    .line 15
    iput-boolean p1, p0, Lcom/twitter/android/moments/ui/guide/ag;->c:Z

    return p1
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ag;->a:Lcom/twitter/android/moments/ui/guide/e;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/e;->a()Z

    move-result v0

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/ag;->c:Z

    if-eqz v0, :cond_0

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/ag;->c:Z

    .line 53
    const/4 v0, 0x1

    .line 55
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ag;->a:Lcom/twitter/android/moments/ui/guide/e;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/e;->b()Z

    move-result v0

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ag;->d:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 61
    return-void
.end method
