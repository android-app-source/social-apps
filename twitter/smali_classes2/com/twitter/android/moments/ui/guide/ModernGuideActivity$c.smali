.class public Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$c;
.super Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/guide/ModernGuideActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# instance fields
.field public final a:Lcom/twitter/model/moments/h;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/model/moments/h;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 140
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$b;-><init>()V

    .line 141
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$c;->a:Lcom/twitter/model/moments/h;

    .line 142
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$c;->d:Landroid/content/Intent;

    const-string/jumbo v1, "guide_category_id"

    iget-object v2, p1, Lcom/twitter/model/moments/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "guide_category_name"

    iget-object v2, p1, Lcom/twitter/model/moments/h;->b:Ljava/lang/String;

    .line 143
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "guide_type"

    .line 144
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 146
    invoke-virtual {p0, v3}, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$c;->d(Z)Lcom/twitter/app/common/base/h;

    .line 147
    return-void
.end method
