.class Lcom/twitter/android/moments/ui/guide/w;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/app/common/list/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/app/common/list/l",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;",
            "Lcom/twitter/android/moments/ui/guide/z;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/twitter/app/common/list/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/app/common/list/l",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;",
            "Lcom/twitter/android/moments/ui/guide/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/w;->a:Lcom/twitter/app/common/list/l;

    .line 21
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/w;->a:Lcom/twitter/app/common/list/l;

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->aN_()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0585

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 35
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/w;->a:Lcom/twitter/app/common/list/l;

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/w;->a:Lcom/twitter/app/common/list/l;

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    :cond_0
    return-void
.end method

.method public a(Lcno$c;)V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/w;->a:Lcom/twitter/app/common/list/l;

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/list/l;->a(Lcno$c;)V

    .line 30
    return-void
.end method

.method public a(Lcom/twitter/android/moments/ui/guide/z;)V
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/w;->a:Lcom/twitter/app/common/list/l;

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->m()V

    .line 25
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/w;->a:Lcom/twitter/app/common/list/l;

    invoke-virtual {v0, p1, p1}, Lcom/twitter/app/common/list/l;->a(Lcjr;Landroid/widget/ListAdapter;)V

    .line 26
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/w;->a:Lcom/twitter/app/common/list/l;

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->t()V

    .line 39
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/w;->a:Lcom/twitter/app/common/list/l;

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->s()V

    .line 43
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/w;->a:Lcom/twitter/app/common/list/l;

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->t()V

    .line 47
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/w;->a:Lcom/twitter/app/common/list/l;

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->l()Z

    move-result v0

    return v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/w;->a:Lcom/twitter/app/common/list/l;

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->v()V

    .line 61
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/w;->a:Lcom/twitter/app/common/list/l;

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->w()V

    .line 65
    return-void
.end method
