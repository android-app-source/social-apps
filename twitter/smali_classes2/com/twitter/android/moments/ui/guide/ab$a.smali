.class public Lcom/twitter/android/moments/ui/guide/ab$a;
.super Lcom/twitter/app/common/list/i$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/guide/ab;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/list/i$a",
        "<",
        "Lcom/twitter/android/moments/ui/guide/ab$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/twitter/app/common/list/i$a;-><init>()V

    .line 84
    return-void
.end method

.method public constructor <init>(Lcom/twitter/android/moments/ui/guide/ab;)V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0, p1}, Lcom/twitter/app/common/list/i$a;-><init>(Lcom/twitter/app/common/list/i;)V

    .line 92
    return-void
.end method


# virtual methods
.method public a(I)Lcom/twitter/android/moments/ui/guide/ab$a;
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ab$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "guide_type"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 133
    return-object p0
.end method

.method public a(J)Lcom/twitter/android/moments/ui/guide/ab$a;
    .locals 3

    .prologue
    .line 120
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ab$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "add_to_moment_tweet_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 121
    return-object p0
.end method

.method public a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/moments/ui/guide/ab$a;
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ab$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "twitter_scribe_association"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 139
    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/android/moments/ui/guide/ab$a;
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ab$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "guide_category_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ab$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "guide_category_name"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    return-object p0
.end method

.method public a(Z)Lcom/twitter/android/moments/ui/guide/ab$a;
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ab$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "show_category_pills"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 109
    return-object p0
.end method

.method public a()Lcom/twitter/android/moments/ui/guide/ab;
    .locals 2

    .prologue
    .line 145
    new-instance v0, Lcom/twitter/android/moments/ui/guide/ab;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ab$a;->a:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Lcom/twitter/android/moments/ui/guide/ab;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method

.method public b(J)Lcom/twitter/android/moments/ui/guide/ab$a;
    .locals 3

    .prologue
    .line 126
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ab$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "moments_owner_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 127
    return-object p0
.end method

.method public synthetic b()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/ab$a;->a()Lcom/twitter/android/moments/ui/guide/ab;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/ab$a;->a()Lcom/twitter/android/moments/ui/guide/ab;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(J)Lcom/twitter/app/common/list/i$a;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/moments/ui/guide/ab$a;->b(J)Lcom/twitter/android/moments/ui/guide/ab$a;

    move-result-object v0

    return-object v0
.end method
