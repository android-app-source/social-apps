.class public Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;
.super Lcom/twitter/app/common/list/TwitterListFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/fullscreen/c;
.implements Lcom/twitter/android/moments/ui/guide/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/list/TwitterListFragment",
        "<",
        "Lcom/twitter/android/moments/viewmodels/MomentGuideListItem;",
        "Lcom/twitter/android/moments/ui/guide/z;",
        ">;",
        "Lcom/twitter/android/moments/ui/fullscreen/c;",
        "Lcom/twitter/android/moments/ui/guide/d;"
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/android/av/k;

.field private b:Lcom/twitter/android/moments/data/f;

.field private c:Lacj;

.field private d:Lzp;

.field private e:Laac;

.field private f:Lcom/twitter/android/moments/data/j;

.field private g:Lcom/twitter/android/moments/data/j;

.field private h:Lcom/twitter/android/moments/data/w;

.field private i:Lcom/twitter/android/moments/data/am;

.field private j:Lcom/twitter/android/moments/data/ag;

.field private k:Lcom/twitter/android/moments/ui/guide/l;

.field private l:Lcom/twitter/android/moments/ui/guide/p;

.field private m:Lcom/twitter/android/moments/ui/guide/g;

.field private n:Lcom/twitter/android/moments/ui/guide/w;

.field private o:Lacp;

.field private p:Lcom/twitter/android/moments/ui/guide/m;

.field private q:Lcom/twitter/android/search/c;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Lcom/twitter/ui/view/i;

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/twitter/app/common/list/TwitterListFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;)Lcom/twitter/android/moments/ui/guide/m;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->p:Lcom/twitter/android/moments/ui/guide/m;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;)Lcom/twitter/android/moments/ui/guide/g;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->m:Lcom/twitter/android/moments/ui/guide/g;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;)Lzp;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->d:Lzp;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;)Laac;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->e:Laac;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;)Lcom/twitter/android/search/c;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->q:Lcom/twitter/android/search/c;

    return-object v0
.end method

.method private q()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->i:Lcom/twitter/android/moments/data/am;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 128
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->h:Lcom/twitter/android/moments/data/w;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 129
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->c:Lacj;

    invoke-virtual {v0}, Lacj;->b()V

    .line 130
    return-void
.end method

.method private r()V
    .locals 18

    .prologue
    .line 133
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    .line 134
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a_:J

    invoke-static {v4, v5}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v4

    .line 135
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v13

    .line 136
    new-instance v14, Lbsb;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a_:J

    invoke-direct {v14, v3, v4, v6, v7}, Lbsb;-><init>(Landroid/content/Context;Lcom/twitter/library/provider/t;J)V

    .line 138
    new-instance v15, Lcom/twitter/android/moments/data/i;

    invoke-direct {v15}, Lcom/twitter/android/moments/data/i;-><init>()V

    .line 139
    new-instance v5, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment$1;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v3}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment$1;-><init>(Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;Landroid/app/Activity;)V

    .line 151
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->w:Z

    if-eqz v2, :cond_1

    .line 152
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->y:J

    const-wide/16 v8, 0x0

    cmp-long v2, v6, v8

    if-lez v2, :cond_0

    .line 154
    new-instance v2, Lbdx;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->y:J

    invoke-direct {v2, v6, v7}, Lbdx;-><init>(J)V

    move-object v12, v2

    .line 169
    :goto_0
    new-instance v2, Lcom/twitter/android/moments/data/l;

    .line 170
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v6

    invoke-direct {v2, v3, v6, v13, v14}, Lcom/twitter/android/moments/data/l;-><init>(Landroid/content/Context;Lcom/twitter/library/client/v;Lcom/twitter/library/client/p;Lbsb;)V

    .line 171
    new-instance v6, Lcom/twitter/android/moments/data/w;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a_:J

    new-instance v7, Lcom/twitter/android/moments/data/c;

    invoke-direct {v7, v5}, Lcom/twitter/android/moments/data/c;-><init>(Lcom/twitter/util/object/j;)V

    invoke-direct {v6, v8, v9, v7, v2}, Lcom/twitter/android/moments/data/w;-><init>(JLcom/twitter/android/moments/data/c;Lcom/twitter/android/moments/data/l;)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->h:Lcom/twitter/android/moments/data/w;

    .line 173
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v16

    .line 174
    new-instance v2, Lzp;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a_:J

    invoke-interface {v12}, Lbdn;->c()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v2, v6, v7, v5, v0}, Lzp;-><init>(JLjava/lang/String;Lcom/twitter/android/moments/ui/fullscreen/c;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->d:Lzp;

    .line 175
    new-instance v2, Laac;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-direct {v2, v5, v0, v6}, Laac;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/p;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->e:Laac;

    .line 176
    new-instance v2, Lbds;

    move-object/from16 v0, v16

    invoke-direct {v2, v3, v0}, Lbds;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 177
    invoke-static {v2}, Lcom/twitter/android/moments/data/ag;->a(Lbds;)Lcom/twitter/android/moments/data/ag;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->j:Lcom/twitter/android/moments/data/ag;

    .line 178
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->j:Lcom/twitter/android/moments/data/ag;

    invoke-static {v5, v2, v4}, Lcom/twitter/android/moments/data/an;->a(Lcom/twitter/android/moments/data/ag;Lbds;Lcom/twitter/library/provider/t;)Lcom/twitter/android/moments/data/an;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->i:Lcom/twitter/android/moments/data/am;

    .line 180
    new-instance v2, Lcom/twitter/android/moments/data/j;

    invoke-direct {v2}, Lcom/twitter/android/moments/data/j;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->g:Lcom/twitter/android/moments/data/j;

    .line 181
    new-instance v2, Lcom/twitter/android/moments/data/j;

    invoke-direct {v2}, Lcom/twitter/android/moments/data/j;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->f:Lcom/twitter/android/moments/data/j;

    .line 182
    const-string/jumbo v2, "guide_post_follow_fatigue"

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a_:J

    .line 183
    invoke-static {v3, v2, v4, v5}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v5

    .line 184
    const-string/jumbo v2, "guide_follow_button_fatigue"

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a_:J

    .line 185
    invoke-static {v3, v2, v6, v7}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v6

    .line 186
    const-string/jumbo v2, "guide_thumbnail_fatigue"

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a_:J

    .line 187
    invoke-static {v3, v2, v8, v9}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v7

    .line 189
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->s:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 190
    const v9, 0x7f130043

    .line 194
    :goto_1
    new-instance v2, Lcom/twitter/android/moments/ui/guide/p;

    .line 195
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    .line 196
    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->s()Z

    move-result v8

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->s:Ljava/lang/String;

    invoke-direct/range {v2 .. v10}, Lcom/twitter/android/moments/ui/guide/p;-><init>(Landroid/app/Activity;Landroid/support/v4/app/FragmentManager;Lcom/twitter/android/util/h;Lcom/twitter/android/util/h;Lcom/twitter/android/util/h;ZILjava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->l:Lcom/twitter/android/moments/ui/guide/p;

    .line 197
    new-instance v2, Lcom/twitter/android/moments/ui/guide/g;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->d:Lzp;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->g:Lcom/twitter/android/moments/data/j;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->f:Lcom/twitter/android/moments/data/j;

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/twitter/android/moments/ui/guide/g;-><init>(Landroid/content/Context;Lzp;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/j;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->m:Lcom/twitter/android/moments/ui/guide/g;

    .line 200
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->S:Lcom/twitter/library/client/p;

    invoke-static {v3, v2, v14, v4, v12}, Lcom/twitter/android/moments/data/g;->a(Landroid/content/Context;Lcom/twitter/library/client/v;Lbsb;Lcom/twitter/library/client/p;Lbdn;)Lcom/twitter/android/moments/data/g;

    move-result-object v17

    .line 202
    new-instance v11, Lbsa;

    .line 203
    invoke-virtual/range {v16 .. v16}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v11, v3, v4, v5}, Lbsa;-><init>(Landroid/content/Context;J)V

    .line 204
    new-instance v2, Lcom/twitter/android/moments/data/b;

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v4

    .line 205
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v6

    const-class v5, Lcom/twitter/android/moments/data/b;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a_:J

    .line 206
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 205
    invoke-virtual {v15, v5, v7}, Lcom/twitter/android/moments/data/i;->a(Ljava/lang/Class;Ljava/lang/Object;)I

    move-result v7

    const-wide/16 v9, 0x0

    move-object v5, v13

    move-object v8, v14

    invoke-direct/range {v2 .. v11}, Lcom/twitter/android/moments/data/b;-><init>(Landroid/content/Context;Lcom/twitter/library/client/v;Lcom/twitter/library/client/p;Landroid/support/v4/app/LoaderManager;ILbsb;JLbsa;)V

    .line 207
    new-instance v4, Lacj;

    invoke-direct {v4, v2}, Lacj;-><init>(Lcom/twitter/android/moments/data/b;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->c:Lacj;

    .line 208
    new-instance v2, Lcom/twitter/android/moments/data/f;

    .line 211
    invoke-interface {v12}, Lbdn;->d()I

    move-result v4

    invoke-interface {v12}, Lbdn;->c()Ljava/lang/String;

    move-result-object v5

    .line 210
    invoke-virtual {v11, v4, v5}, Lbsa;->a(ILjava/lang/String;)Lcom/twitter/util/android/d;

    move-result-object v4

    .line 212
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v5

    const-class v6, Lcom/twitter/android/moments/data/f;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a_:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v15, v6, v7}, Lcom/twitter/android/moments/data/i;->a(Ljava/lang/Class;Ljava/lang/Object;)I

    move-result v6

    move-object/from16 v0, v17

    invoke-direct {v2, v0, v4, v5, v6}, Lcom/twitter/android/moments/data/f;-><init>(Lcom/twitter/android/moments/data/g;Landroid/support/v4/content/Loader;Landroid/support/v4/app/LoaderManager;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->b:Lcom/twitter/android/moments/data/f;

    .line 213
    invoke-static {v3}, Lcom/twitter/android/search/c;->a(Landroid/app/Activity;)Lcom/twitter/android/search/c;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->q:Lcom/twitter/android/search/c;

    .line 214
    return-void

    .line 156
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Trying to display a user guide without userId"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 158
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->x:Z

    if-eqz v2, :cond_3

    .line 159
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->k()Lcom/twitter/android/moments/ui/guide/ab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/guide/ab;->j()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v2, v6, v8

    if-lez v2, :cond_2

    .line 161
    new-instance v2, Lbdx;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a_:J

    invoke-direct {v2, v6, v7}, Lbdx;-><init>(J)V

    move-object v12, v2

    goto/16 :goto_0

    .line 163
    :cond_2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Trying to add To moment guide without tweet id"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 167
    :cond_3
    new-instance v2, Lwm;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->r:Ljava/lang/String;

    invoke-static {}, Lwk;->a()Z

    move-result v7

    invoke-direct {v2, v6, v7}, Lwm;-><init>(Ljava/lang/String;Z)V

    move-object v12, v2

    goto/16 :goto_0

    .line 192
    :cond_4
    const/4 v9, 0x0

    goto/16 :goto_1
.end method

.method private s()Z
    .locals 1

    .prologue
    .line 225
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->v:Z

    return v0
.end method

.method private t()V
    .locals 6

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->r()V

    .line 244
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->ah()Lann;

    move-result-object v0

    check-cast v0, Lyt;

    .line 245
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->k()Lcom/twitter/android/moments/ui/guide/ab;

    move-result-object v1

    .line 246
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->u()Lcom/twitter/android/moments/ui/guide/l;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->k:Lcom/twitter/android/moments/ui/guide/l;

    .line 247
    iget-wide v2, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->y:J

    iget-wide v4, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a_:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 248
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->k:Lcom/twitter/android/moments/ui/guide/l;

    invoke-static {v2, v3}, Lcom/twitter/android/moments/ui/guide/ag;->a(Landroid/app/Activity;Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/ui/guide/ag;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->p:Lcom/twitter/android/moments/ui/guide/m;

    .line 249
    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/guide/ab;->j()J

    move-result-wide v2

    .line 250
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 252
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-interface {v0}, Lyt;->d()Lxh;

    move-result-object v5

    .line 253
    invoke-virtual {v1}, Lcom/twitter/android/moments/ui/guide/ab;->k()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 251
    invoke-static {v4, v2, v3, v5, v0}, Lcom/twitter/ui/view/i;->a(Landroid/app/Activity;JLxh;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/ui/view/i;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->t:Lcom/twitter/ui/view/i;

    .line 258
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->t:Lcom/twitter/ui/view/i;

    invoke-virtual {v0}, Lcom/twitter/ui/view/i;->a()V

    .line 262
    :goto_1
    return-void

    .line 255
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->m:Lcom/twitter/android/moments/ui/guide/g;

    invoke-static {v0, v1}, Lcom/twitter/ui/view/i;->a(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/android/moments/ui/guide/g;)Lcom/twitter/ui/view/i;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->t:Lcom/twitter/ui/view/i;

    goto :goto_0

    .line 260
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->k:Lcom/twitter/android/moments/ui/guide/l;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/e;->a(Lcom/twitter/android/moments/ui/guide/l;)Lcom/twitter/android/moments/ui/guide/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->p:Lcom/twitter/android/moments/ui/guide/m;

    goto :goto_1
.end method

.method private u()Lcom/twitter/android/moments/ui/guide/l;
    .locals 27

    .prologue
    .line 266
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    .line 267
    new-instance v13, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$d;

    invoke-direct {v13, v3}, Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$d;-><init>(Landroid/app/Activity;)V

    .line 269
    new-instance v10, Lcom/twitter/android/client/n;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a_:J

    invoke-direct {v10, v3, v4, v5}, Lcom/twitter/android/client/n;-><init>(Landroid/content/Context;J)V

    .line 270
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->ah()Lann;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lyt;

    .line 271
    invoke-static {}, Laad;->a()Laad;

    move-result-object v12

    .line 272
    new-instance v2, Lcom/twitter/android/moments/ui/guide/h$a;

    .line 273
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->m:Lcom/twitter/android/moments/ui/guide/g;

    invoke-interface {v4}, Lyt;->d()Lxh;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->c:Lacj;

    new-instance v9, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment$3;

    move-object/from16 v0, p0

    invoke-direct {v9, v0, v3, v13, v10}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment$3;-><init>(Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;Landroid/app/Activity;Lcom/twitter/android/moments/ui/guide/ModernGuideActivity$d;Lcom/twitter/android/client/n;)V

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a_:J

    move-object v4, v3

    invoke-direct/range {v2 .. v12}, Lcom/twitter/android/moments/ui/guide/h$a;-><init>(Landroid/content/Context;Landroid/app/Activity;Landroid/content/res/Resources;Lcom/twitter/android/moments/ui/guide/g;Lxh;Lacj;Lcom/twitter/util/object/e;JLaad;)V

    .line 283
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->k()Lcom/twitter/android/moments/ui/guide/ab;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/twitter/android/moments/ui/guide/h$a;->a(Lcom/twitter/android/moments/ui/guide/ab;)Lcom/twitter/android/moments/ui/guide/h;

    move-result-object v17

    .line 284
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->d:Lzp;

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/twitter/android/moments/ui/guide/h;->e:Lcom/twitter/android/moments/ui/guide/r;

    invoke-virtual {v2, v4}, Lzp;->a(Lcom/twitter/android/moments/ui/guide/r;)V

    .line 285
    new-instance v23, Lacl;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/twitter/android/moments/ui/guide/h;->a:Lrx/c;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->d:Lzp;

    move-object/from16 v0, v23

    invoke-direct {v0, v2, v4, v13, v5}, Lacl;-><init>(Landroid/view/LayoutInflater;Lrx/c;Lakr;Lzp;)V

    .line 287
    new-instance v2, Lcom/twitter/android/moments/ui/guide/l;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->b:Lcom/twitter/android/moments/data/f;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->n:Lcom/twitter/android/moments/ui/guide/w;

    .line 288
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v7

    iget-object v7, v7, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->d:Lzp;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->e:Laac;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->m:Lcom/twitter/android/moments/ui/guide/g;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->l:Lcom/twitter/android/moments/ui/guide/p;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->g:Lcom/twitter/android/moments/data/j;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->f:Lcom/twitter/android/moments/data/j;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->h:Lcom/twitter/android/moments/data/w;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->i:Lcom/twitter/android/moments/data/am;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->o:Lacp;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a:Lcom/twitter/android/av/k;

    move-object/from16 v19, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/twitter/android/moments/ui/guide/h;->b:Lcom/twitter/android/moments/viewmodels/e;

    move-object/from16 v20, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/twitter/android/moments/ui/guide/h;->c:Lcom/twitter/android/moments/viewmodels/d;

    move-object/from16 v21, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/twitter/android/moments/ui/guide/h;->d:Lcom/twitter/android/moments/ui/guide/l$b;

    move-object/from16 v22, v0

    .line 292
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->ag()Lanh;

    move-result-object v24

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/twitter/android/moments/ui/guide/h;->e:Lcom/twitter/android/moments/ui/guide/r;

    move-object/from16 v25, v0

    .line 294
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->aK()Lcom/twitter/app/common/base/TwitterFragmentActivity;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->P()Lcom/twitter/android/search/SearchSuggestionController;

    move-result-object v26

    move-object/from16 v17, p0

    invoke-direct/range {v2 .. v26}, Lcom/twitter/android/moments/ui/guide/l;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/twitter/android/moments/data/f;Lcom/twitter/android/moments/ui/guide/w;Landroid/widget/ListView;Lzp;Laac;Lcom/twitter/android/moments/ui/guide/g;Lcom/twitter/android/moments/ui/guide/p;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/w;Lcom/twitter/android/moments/data/am;ZLcom/twitter/android/moments/ui/guide/d;Lacp;Lcom/twitter/android/av/k;Lcom/twitter/android/moments/viewmodels/e;Lcom/twitter/android/moments/viewmodels/d;Lcom/twitter/android/moments/ui/guide/l$b;Lack;Lanh;Lcom/twitter/android/moments/ui/guide/r;Lcom/twitter/android/search/SearchSuggestionController;)V

    .line 287
    return-object v2
.end method


# virtual methods
.method public synthetic H()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->k()Lcom/twitter/android/moments/ui/guide/ab;

    move-result-object v0

    return-object v0
.end method

.method protected H_()V
    .locals 2

    .prologue
    .line 323
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->p:Lcom/twitter/android/moments/ui/guide/m;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/guide/m;->a()Z

    .line 324
    invoke-static {}, Lbpu;->a()Lbpu;

    move-result-object v0

    invoke-virtual {v0}, Lbpu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l;->c(Z)Z

    .line 330
    :cond_0
    return-void
.end method

.method public synthetic I()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->k()Lcom/twitter/android/moments/ui/guide/ab;

    move-result-object v0

    return-object v0
.end method

.method protected M_()Z
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->p:Lcom/twitter/android/moments/ui/guide/m;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/guide/m;->a()Z

    move-result v0

    return v0
.end method

.method protected a(Lank;)Lyt;
    .locals 2

    .prologue
    .line 219
    invoke-static {}, Lyr;->a()Lyr$a;

    move-result-object v0

    .line 220
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lyr$a;->a(Lamu;)Lyr$a;

    move-result-object v0

    .line 221
    invoke-virtual {v0}, Lyr$a;->a()Lyt;

    move-result-object v0

    .line 219
    return-object v0
.end method

.method protected a(JJ)V
    .locals 2

    .prologue
    .line 299
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->k:Lcom/twitter/android/moments/ui/guide/l;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/guide/l;->a(Z)V

    .line 300
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->q()V

    .line 301
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->t()V

    .line 302
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->p:Lcom/twitter/android/moments/ui/guide/m;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/guide/m;->a()Z

    .line 303
    return-void
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 4

    .prologue
    .line 373
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 374
    const v0, 0x7f040177

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->c(I)Lcom/twitter/app/common/list/l$d;

    move-result-object v0

    const v1, 0x7f0400e5

    .line 375
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l$d;->f(I)Lcom/twitter/app/common/list/l$d;

    .line 376
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->w:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->x:Z

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 377
    :goto_0
    if-eqz v0, :cond_1

    .line 378
    const v0, 0x7f0a0589

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->a(I)Lcom/twitter/app/common/list/l$d;

    .line 379
    iget-wide v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a_:J

    iget-wide v2, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->y:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    const v0, 0x7f0a057f

    :goto_1
    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->b(I)Lcom/twitter/app/common/list/l$d;

    .line 383
    :cond_1
    return-void

    .line 376
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 379
    :cond_3
    const v0, 0x7f0a058a

    goto :goto_1
.end method

.method protected aP_()V
    .locals 1

    .prologue
    .line 343
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aP_()V

    .line 344
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->k:Lcom/twitter/android/moments/ui/guide/l;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/l;->d()V

    .line 349
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->W:Lcom/twitter/android/av/j;

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->W:Lcom/twitter/android/av/j;

    invoke-virtual {v0}, Lcom/twitter/android/av/j;->a()V

    .line 352
    :cond_0
    return-void
.end method

.method protected aa_()Lcom/twitter/app/common/list/b;
    .locals 1

    .prologue
    .line 308
    new-instance v0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment$4;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment$4;-><init>(Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;)V

    return-object v0
.end method

.method protected synthetic b(Lank;)Lcom/twitter/app/common/list/j;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a(Lank;)Lyt;

    move-result-object v0

    return-object v0
.end method

.method protected b()V
    .locals 1

    .prologue
    .line 334
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->b()V

    .line 335
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->u:Z

    .line 336
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->k:Lcom/twitter/android/moments/ui/guide/l;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/l;->b()V

    .line 337
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->k:Lcom/twitter/android/moments/ui/guide/l;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/l;->a()V

    .line 338
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->j:Lcom/twitter/android/moments/data/ag;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/ag;->b()V

    .line 339
    return-void
.end method

.method protected synthetic c(Lank;)Lcom/twitter/app/common/abs/c;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a(Lank;)Lyt;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic d(Lank;)Lann;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a(Lank;)Lyt;

    move-result-object v0

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 387
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->u:Z

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 393
    const/4 v0, 0x1

    return v0
.end method

.method public k()Lcom/twitter/android/moments/ui/guide/ab;
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/ab;->a(Landroid/os/Bundle;)Lcom/twitter/android/moments/ui/guide/ab;

    move-result-object v0

    return-object v0
.end method

.method protected l()V
    .locals 1

    .prologue
    .line 367
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->l()V

    .line 368
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->k:Lcom/twitter/android/moments/ui/guide/l;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/l;->f()V

    .line 369
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 230
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 231
    new-instance v0, Lcom/twitter/android/moments/ui/guide/w;

    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/moments/ui/guide/w;-><init>(Lcom/twitter/app/common/list/l;)V

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->n:Lcom/twitter/android/moments/ui/guide/w;

    .line 232
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lacp;->a(Landroid/content/Context;)Lacp;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->o:Lacp;

    .line 233
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->t()V

    .line 234
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->n:Lcom/twitter/android/moments/ui/guide/w;

    new-instance v1, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment$2;-><init>(Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/guide/w;->a(Landroid/view/View$OnClickListener;)V

    .line 240
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 99
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 100
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->k()Lcom/twitter/android/moments/ui/guide/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/ab;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->r:Ljava/lang/String;

    .line 101
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->k()Lcom/twitter/android/moments/ui/guide/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/ab;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->v:Z

    .line 102
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->k()Lcom/twitter/android/moments/ui/guide/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/ab;->g()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->x:Z

    .line 103
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->k()Lcom/twitter/android/moments/ui/guide/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/ab;->f()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->w:Z

    .line 104
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->k()Lcom/twitter/android/moments/ui/guide/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/ab;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->y:J

    .line 105
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->k()Lcom/twitter/android/moments/ui/guide/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/ab;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->s:Ljava/lang/String;

    .line 106
    new-instance v0, Lcom/twitter/android/av/k;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->T:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/twitter/android/av/k;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a:Lcom/twitter/android/av/k;

    .line 107
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 117
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->onDestroy()V

    .line 120
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->k:Lcom/twitter/android/moments/ui/guide/l;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/guide/l;->a(Z)V

    .line 121
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->p:Lcom/twitter/android/moments/ui/guide/m;

    invoke-interface {v0}, Lcom/twitter/android/moments/ui/guide/m;->c()V

    .line 122
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->q()V

    .line 123
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->a:Lcom/twitter/android/av/k;

    invoke-virtual {v0}, Lcom/twitter/android/av/k;->b()V

    .line 124
    return-void
.end method

.method protected q_()V
    .locals 1

    .prologue
    .line 356
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->ad()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->e:Laac;

    invoke-virtual {v0}, Laac;->b()V

    .line 359
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->q_()V

    .line 360
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->u:Z

    .line 361
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->k:Lcom/twitter/android/moments/ui/guide/l;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/guide/l;->c()V

    .line 362
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;->j:Lcom/twitter/android/moments/data/ag;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/ag;->a()V

    .line 363
    return-void
.end method
