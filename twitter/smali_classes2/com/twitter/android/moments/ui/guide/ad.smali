.class Lcom/twitter/android/moments/ui/guide/ad;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/guide/ae;
.implements Lcom/twitter/android/moments/ui/guide/v;


# instance fields
.field private final a:Lcom/twitter/android/moments/data/e;

.field private final b:Lcom/twitter/android/moments/ui/guide/p;

.field private final c:Lcom/twitter/android/moments/data/j;

.field private final d:Lcom/twitter/android/moments/data/ab;

.field private final e:Landroid/content/res/Resources;

.field private final f:Lacq;

.field private final g:Lcom/twitter/android/moments/ui/guide/j;

.field private final h:Lcom/twitter/android/moments/ui/guide/d;

.field private final i:Lzp$a;

.field private final j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcom/twitter/android/moments/data/ai;

.field private final l:Lcom/twitter/android/moments/ui/guide/l$b;

.field private final m:Lcom/twitter/android/moments/ui/guide/y;

.field private n:Lcom/twitter/android/moments/viewmodels/MomentModule;


# direct methods
.method constructor <init>(Landroid/content/res/Resources;Lcom/twitter/android/moments/data/e;Lcom/twitter/android/moments/data/ai;Lcom/twitter/android/moments/data/ab;Lcom/twitter/android/moments/ui/guide/p;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/ui/guide/d;Lacq;Lzp$c;Lcom/twitter/android/moments/ui/guide/l$b;Ljava/util/Set;Lcom/twitter/android/moments/ui/guide/y;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Lcom/twitter/android/moments/data/e;",
            "Lcom/twitter/android/moments/data/ai;",
            "Lcom/twitter/android/moments/data/ab;",
            "Lcom/twitter/android/moments/ui/guide/p;",
            "Lcom/twitter/android/moments/data/j;",
            "Lcom/twitter/android/moments/data/j;",
            "Lcom/twitter/android/moments/ui/guide/d;",
            "Lacq;",
            "Lzp$c;",
            "Lcom/twitter/android/moments/ui/guide/l$b;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/twitter/android/moments/ui/guide/y;",
            ")V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/ad;->e:Landroid/content/res/Resources;

    .line 60
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/ad;->f:Lacq;

    .line 61
    iput-object p2, p0, Lcom/twitter/android/moments/ui/guide/ad;->a:Lcom/twitter/android/moments/data/e;

    .line 62
    iput-object p3, p0, Lcom/twitter/android/moments/ui/guide/ad;->k:Lcom/twitter/android/moments/data/ai;

    .line 63
    iput-object p4, p0, Lcom/twitter/android/moments/ui/guide/ad;->d:Lcom/twitter/android/moments/data/ab;

    .line 64
    iput-object p5, p0, Lcom/twitter/android/moments/ui/guide/ad;->b:Lcom/twitter/android/moments/ui/guide/p;

    .line 65
    iput-object p6, p0, Lcom/twitter/android/moments/ui/guide/ad;->c:Lcom/twitter/android/moments/data/j;

    .line 66
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/ad;->i:Lzp$a;

    .line 67
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/ad;->j:Ljava/util/Set;

    .line 68
    iput-object p8, p0, Lcom/twitter/android/moments/ui/guide/ad;->h:Lcom/twitter/android/moments/ui/guide/d;

    .line 69
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ad;->f:Lacq;

    invoke-virtual {v1}, Lacq;->e()Lcom/twitter/android/moments/ui/a;

    move-result-object v1

    .line 70
    new-instance v2, Lcom/twitter/android/moments/ui/guide/j;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/guide/ad;->c:Lcom/twitter/android/moments/data/j;

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/twitter/android/moments/ui/guide/ae;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/twitter/android/moments/ui/guide/ad;->k:Lcom/twitter/android/moments/data/ai;

    aput-object v6, v4, v5

    .line 71
    invoke-static {p0, v4}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v1, v3, p7, v4}, Lcom/twitter/android/moments/ui/guide/j;-><init>(Lcom/twitter/android/moments/ui/a;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/j;Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/twitter/android/moments/ui/guide/ad;->g:Lcom/twitter/android/moments/ui/guide/j;

    .line 72
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/ad;->l:Lcom/twitter/android/moments/ui/guide/l$b;

    .line 73
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/ad;->m:Lcom/twitter/android/moments/ui/guide/y;

    .line 74
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/guide/ad;)Lcom/twitter/android/moments/ui/guide/p;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ad;->b:Lcom/twitter/android/moments/ui/guide/p;

    return-object v0
.end method

.method private a(Lcom/twitter/model/moments/Moment;)V
    .locals 7

    .prologue
    .line 137
    iget-boolean v0, p1, Lcom/twitter/model/moments/Moment;->e:Z

    if-eqz v0, :cond_1

    .line 138
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ad;->f:Lacq;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ad;->e:Landroid/content/res/Resources;

    const v2, 0x7f0a058b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lacq;->b(Ljava/lang/CharSequence;)V

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    iget-object v0, p1, Lcom/twitter/model/moments/Moment;->r:Lcom/twitter/model/moments/f;

    if-eqz v0, :cond_4

    .line 140
    iget-object v0, p1, Lcom/twitter/model/moments/Moment;->r:Lcom/twitter/model/moments/f;

    iget-object v0, v0, Lcom/twitter/model/moments/f;->c:Lcom/twitter/model/moments/MomentVisibilityMode;

    .line 141
    sget-object v1, Lcom/twitter/model/moments/MomentVisibilityMode;->b:Lcom/twitter/model/moments/MomentVisibilityMode;

    if-ne v1, v0, :cond_2

    .line 142
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ad;->f:Lacq;

    invoke-virtual {v0}, Lacq;->h()V

    goto :goto_0

    .line 143
    :cond_2
    sget-object v1, Lcom/twitter/model/moments/MomentVisibilityMode;->a:Lcom/twitter/model/moments/MomentVisibilityMode;

    if-ne v1, v0, :cond_3

    .line 144
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ad;->f:Lacq;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ad;->e:Landroid/content/res/Resources;

    const v2, 0x7f0a0bd5

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/moments/ui/guide/ad;->e:Landroid/content/res/Resources;

    const v6, 0x7f0a0567

    .line 145
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p1, Lcom/twitter/model/moments/Moment;->i:Ljava/lang/String;

    invoke-static {v5}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 144
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lacq;->b(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 146
    :cond_3
    sget-object v1, Lcom/twitter/model/moments/MomentVisibilityMode;->c:Lcom/twitter/model/moments/MomentVisibilityMode;

    if-ne v1, v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ad;->f:Lacq;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ad;->e:Landroid/content/res/Resources;

    const v2, 0x7f0a0570

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lacq;->b(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 150
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ad;->n:Lcom/twitter/android/moments/viewmodels/MomentModule;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ad;->f:Lacq;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ad;->e:Landroid/content/res/Resources;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/guide/ad;->n:Lcom/twitter/android/moments/viewmodels/MomentModule;

    .line 152
    invoke-static {v1, v2}, Lcom/twitter/android/moments/data/x;->a(Landroid/content/res/Resources;Lcom/twitter/android/moments/viewmodels/MomentModule;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lacq;->b(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/guide/ad;)Lacq;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ad;->f:Lacq;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/guide/ad;)Lcom/twitter/android/moments/ui/guide/l$b;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ad;->l:Lcom/twitter/android/moments/ui/guide/l$b;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ad;->f:Lacq;

    invoke-virtual {v0}, Lacq;->d()Lcom/twitter/media/ui/image/MediaImageView;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/MomentModule;)V
    .locals 4

    .prologue
    .line 83
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/ad;->n:Lcom/twitter/android/moments/viewmodels/MomentModule;

    .line 84
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->b()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ad;->f:Lacq;

    invoke-virtual {v1}, Lacq;->d()Lcom/twitter/media/ui/image/MediaImageView;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/twitter/android/moments/data/r;->a(Lcom/twitter/android/moments/viewmodels/MomentModule;Lcom/twitter/media/ui/image/MediaImageView;)V

    .line 86
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ad;->f:Lacq;

    iget-object v2, v0, Lcom/twitter/model/moments/Moment;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lacq;->a(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ad;->m:Lcom/twitter/android/moments/ui/guide/y;

    invoke-interface {v1, v0}, Lcom/twitter/android/moments/ui/guide/y;->a(Lcom/twitter/model/moments/Moment;)V

    .line 88
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ad;->a:Lcom/twitter/android/moments/data/e;

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/data/e;->a(Lcom/twitter/model/moments/Moment;)V

    .line 89
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ad;->a:Lcom/twitter/android/moments/data/e;

    new-instance v2, Lcom/twitter/android/moments/ui/guide/ad$1;

    invoke-direct {v2, p0}, Lcom/twitter/android/moments/ui/guide/ad$1;-><init>(Lcom/twitter/android/moments/ui/guide/ad;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/moments/data/e;->a(Lcom/twitter/android/moments/data/e$a;)V

    .line 96
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ad;->b:Lcom/twitter/android/moments/ui/guide/p;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/guide/ad;->a:Lcom/twitter/android/moments/data/e;

    invoke-virtual {v2}, Lcom/twitter/android/moments/data/e;->b()Lcom/twitter/ui/widget/ToggleTwitterButton;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/moments/ui/guide/p;->a(Landroid/view/View;)V

    .line 98
    invoke-virtual {v0}, Lcom/twitter/model/moments/Moment;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 99
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ad;->f:Lacq;

    invoke-virtual {v1}, Lacq;->b()Lcom/twitter/ui/widget/BadgeView;

    move-result-object v1

    const v2, 0x7f020840

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/moments/ui/d;->a(Lcom/twitter/model/moments/Moment;Lcom/twitter/ui/widget/BadgeView;IZ)V

    .line 101
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ad;->f:Lacq;

    invoke-virtual {v1}, Lacq;->g()V

    .line 108
    :goto_0
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->d()Lcfn;

    move-result-object v1

    .line 109
    if-eqz v1, :cond_2

    .line 110
    iget-object v2, p0, Lcom/twitter/android/moments/ui/guide/ad;->k:Lcom/twitter/android/moments/data/ai;

    invoke-virtual {v2, v1}, Lcom/twitter/android/moments/data/ai;->a(Lcfn;)V

    .line 115
    :goto_1
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ad;->d:Lcom/twitter/android/moments/data/ab;

    invoke-virtual {v1, p1}, Lcom/twitter/android/moments/data/ab;->a(Lcom/twitter/android/moments/viewmodels/MomentModule;)V

    .line 120
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ad;->g:Lcom/twitter/android/moments/ui/guide/j;

    iget-wide v2, v0, Lcom/twitter/model/moments/Moment;->b:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/moments/ui/guide/j;->a(J)V

    .line 122
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ad;->h:Lcom/twitter/android/moments/ui/guide/d;

    invoke-interface {v1}, Lcom/twitter/android/moments/ui/guide/d;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ad;->j:Ljava/util/Set;

    iget-wide v2, v0, Lcom/twitter/model/moments/Moment;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 123
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ad;->i:Lzp$a;

    iget-wide v2, v0, Lcom/twitter/model/moments/Moment;->b:J

    invoke-virtual {v1, v2, v3}, Lzp$a;->d(J)V

    .line 124
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ad;->j:Ljava/util/Set;

    iget-wide v2, v0, Lcom/twitter/model/moments/Moment;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ad;->f:Lacq;

    new-instance v1, Lcom/twitter/android/moments/ui/guide/ad$2;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/guide/ad$2;-><init>(Lcom/twitter/android/moments/ui/guide/ad;Lcom/twitter/android/moments/viewmodels/MomentModule;)V

    invoke-virtual {v0, v1}, Lacq;->a(Landroid/view/View$OnClickListener;)V

    .line 134
    return-void

    .line 103
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ad;->f:Lacq;

    invoke-virtual {v1}, Lacq;->f()V

    .line 104
    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/guide/ad;->a(Lcom/twitter/model/moments/Moment;)V

    goto :goto_0

    .line 112
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ad;->k:Lcom/twitter/android/moments/data/ai;

    invoke-virtual {v1}, Lcom/twitter/android/moments/data/ai;->a()V

    goto :goto_1
.end method

.method public aW_()V
    .locals 4

    .prologue
    .line 159
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ad;->n:Lcom/twitter/android/moments/viewmodels/MomentModule;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ad;->i:Lzp$a;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ad;->n:Lcom/twitter/android/moments/viewmodels/MomentModule;

    invoke-virtual {v1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->b()Lcom/twitter/model/moments/Moment;

    move-result-object v1

    iget-wide v2, v1, Lcom/twitter/model/moments/Moment;->b:J

    invoke-virtual {v0, v2, v3}, Lzp$a;->d(J)V

    .line 161
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ad;->j:Ljava/util/Set;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ad;->n:Lcom/twitter/android/moments/viewmodels/MomentModule;

    invoke-virtual {v1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->b()Lcom/twitter/model/moments/Moment;

    move-result-object v1

    iget-wide v2, v1, Lcom/twitter/model/moments/Moment;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 163
    :cond_0
    return-void
.end method

.method public b()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ad;->f:Lacq;

    invoke-virtual {v0}, Lacq;->c()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
