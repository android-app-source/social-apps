.class public Lcom/twitter/android/moments/ui/guide/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/guide/l$b;


# instance fields
.field private final a:Lxh;

.field private final b:Lcom/twitter/util/object/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/d",
            "<",
            "Ljava/lang/Long;",
            "Lces;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/android/moments/ui/guide/a;

.field private final d:Lcev;

.field private final e:Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;

.field private final f:Laad;

.field private final g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;


# direct methods
.method protected constructor <init>(Lxh;Lcom/twitter/util/object/d;Lcom/twitter/android/moments/ui/guide/a;Lcev;Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;Laad;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lxh;",
            "Lcom/twitter/util/object/d",
            "<",
            "Ljava/lang/Long;",
            "Lces;",
            ">;",
            "Lcom/twitter/android/moments/ui/guide/a;",
            "Lcev;",
            "Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;",
            "Laad;",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            ")V"
        }
    .end annotation

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/b;->a:Lxh;

    .line 70
    iput-object p2, p0, Lcom/twitter/android/moments/ui/guide/b;->b:Lcom/twitter/util/object/d;

    .line 71
    iput-object p3, p0, Lcom/twitter/android/moments/ui/guide/b;->c:Lcom/twitter/android/moments/ui/guide/a;

    .line 72
    iput-object p4, p0, Lcom/twitter/android/moments/ui/guide/b;->d:Lcev;

    .line 73
    iput-object p5, p0, Lcom/twitter/android/moments/ui/guide/b;->e:Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;

    .line 74
    iput-object p6, p0, Lcom/twitter/android/moments/ui/guide/b;->f:Laad;

    .line 75
    iput-object p7, p0, Lcom/twitter/android/moments/ui/guide/b;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 76
    return-void
.end method

.method public static a(Landroid/app/Activity;Lxh;JLcom/twitter/analytics/feature/model/TwitterScribeAssociation;Laad;)Lcom/twitter/android/moments/ui/guide/b;
    .locals 8

    .prologue
    .line 42
    new-instance v2, Lcom/twitter/android/moments/ui/guide/b$1;

    invoke-direct {v2, p2, p3}, Lcom/twitter/android/moments/ui/guide/b$1;-><init>(J)V

    .line 53
    invoke-static {p0, p2, p3}, Lcom/twitter/android/moments/ui/guide/a;->a(Landroid/app/Activity;J)Lcom/twitter/android/moments/ui/guide/a;

    move-result-object v3

    .line 54
    new-instance v0, Lcev$a;

    invoke-direct {v0}, Lcev$a;-><init>()V

    .line 55
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcev$a;->a(Ljava/lang/Long;)Lcev$a;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lcev$a;->q()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcev;

    .line 57
    new-instance v5, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;

    invoke-direct {v5, p0}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;-><init>(Landroid/app/Activity;)V

    .line 59
    new-instance v0, Lcom/twitter/android/moments/ui/guide/b;

    move-object v1, p1

    move-object v6, p5

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/moments/ui/guide/b;-><init>(Lxh;Lcom/twitter/util/object/d;Lcom/twitter/android/moments/ui/guide/a;Lcev;Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;Laad;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    return-object v0
.end method

.method private a(Lcom/twitter/model/moments/Moment;)V
    .locals 6

    .prologue
    .line 108
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/b;->f:Laad;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/b;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/moments/ui/guide/b;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 109
    invoke-virtual {v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/moments/ui/guide/b;->d:Lcev;

    iget-object v3, v3, Lcev;->c:Ljava/lang/Long;

    .line 110
    invoke-static {v3}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object v3, p1

    .line 108
    invoke-virtual/range {v0 .. v5}, Laad;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/moments/Moment;J)V

    .line 111
    return-void
.end method

.method private a(Lcom/twitter/util/collection/k;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 93
    invoke-virtual {p1}, Lcom/twitter/util/collection/k;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/b;->a:Lxh;

    .line 95
    invoke-virtual {p1}, Lcom/twitter/util/collection/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/Moment;

    iget-wide v2, v0, Lcom/twitter/model/moments/Moment;->b:J

    iget-object v4, p0, Lcom/twitter/android/moments/ui/guide/b;->b:Lcom/twitter/util/object/d;

    invoke-virtual {p1}, Lcom/twitter/util/collection/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/Moment;

    iget-wide v6, v0, Lcom/twitter/model/moments/Moment;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v4, v0}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfd;

    invoke-virtual {v1, v2, v3, v0}, Lxh;->a(JLcfd;)Lrx/g;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Lrx/g;->b()Lrx/j;

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/b;->c:Lcom/twitter/android/moments/ui/guide/a;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/guide/a;->a(Lcom/twitter/util/collection/k;)V

    .line 99
    return-void
.end method

.method private b()V
    .locals 6

    .prologue
    .line 102
    iget-object v2, p0, Lcom/twitter/android/moments/ui/guide/b;->f:Laad;

    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/b;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/b;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 103
    invoke-virtual {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/b;->d:Lcev;

    iget-object v1, v1, Lcev;->c:Ljava/lang/Long;

    .line 104
    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 102
    invoke-virtual {v2, v0, v3, v4, v5}, Laad;->a(Ljava/lang/String;Ljava/lang/String;J)V

    .line 105
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/guide/b;->b()V

    .line 88
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/b;->e:Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/b;->d:Lcev;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;->a(Lcev;)Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/maker/MomentMakerMainActivity$b;->a(Lako;)V

    .line 89
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/b;->c:Lcom/twitter/android/moments/ui/guide/a;

    invoke-static {}, Lcom/twitter/util/collection/k;->a()Lcom/twitter/util/collection/k;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/guide/a;->a(Lcom/twitter/util/collection/k;)V

    .line 90
    return-void
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/MomentModule;)V
    .locals 1

    .prologue
    .line 120
    const-string/jumbo v0, "Module pivot is not supported in moment picker."

    invoke-static {v0}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    .line 121
    return-void
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/MomentModule;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ":",
            "Lcom/twitter/media/ui/image/d;",
            ">(",
            "Lcom/twitter/android/moments/viewmodels/MomentModule;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 81
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->b()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/guide/b;->a(Lcom/twitter/model/moments/Moment;)V

    .line 82
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->b()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/k;->a(Ljava/lang/Object;)Lcom/twitter/util/collection/k;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/guide/b;->a(Lcom/twitter/util/collection/k;)V

    .line 83
    return-void
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/x;)V
    .locals 1

    .prologue
    .line 115
    const-string/jumbo v0, "Guide pivot is not supported in moment picker."

    invoke-static {v0}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    .line 116
    return-void
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/y;)V
    .locals 1

    .prologue
    .line 125
    const-string/jumbo v0, "Trend items are not supported in moment picker."

    invoke-static {v0}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    .line 126
    return-void
.end method
