.class Lcom/twitter/android/moments/ui/guide/ai;
.super Lcom/twitter/android/moments/ui/guide/t;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/moments/ui/guide/t",
        "<",
        "Lcom/twitter/android/moments/viewmodels/ah;",
        "Lcom/twitter/android/moments/ui/fullscreen/MomentsVideoPlayerView;",
        ">;",
        "Lcom/twitter/library/widget/a;"
    }
.end annotation


# instance fields
.field private final l:Lcom/twitter/android/moments/ui/fullscreen/bd;

.field private final m:Lcom/twitter/app/common/util/j;

.field private final n:Lcom/twitter/app/common/util/b$a;

.field private o:Lcom/twitter/android/moments/ui/fullscreen/MomentsVideoPlayerView;

.field private p:Lcom/twitter/android/moments/ui/video/a;

.field private final q:F

.field private r:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/android/moments/ui/guide/y;Lcom/twitter/android/moments/ui/guide/p;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/fullscreen/bd;Lcom/twitter/android/moments/ui/guide/g;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/w;Lzp$b;Lcom/twitter/android/moments/data/am;Lcom/twitter/app/common/util/j;Lcom/twitter/android/moments/ui/guide/l$b;Lcom/twitter/android/moments/ui/guide/r;)V
    .locals 15

    .prologue
    .line 74
    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p14

    move-object/from16 v14, p15

    invoke-direct/range {v1 .. v14}, Lcom/twitter/android/moments/ui/guide/t;-><init>(Landroid/content/Context;Lcom/twitter/android/moments/ui/guide/y;Lcom/twitter/android/moments/ui/guide/p;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/guide/g;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/w;Lzp$b;Lcom/twitter/android/moments/data/am;Lcom/twitter/android/moments/ui/guide/l$b;Lcom/twitter/android/moments/ui/guide/r;)V

    .line 41
    new-instance v1, Lcom/twitter/android/moments/ui/guide/ai$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/guide/ai$1;-><init>(Lcom/twitter/android/moments/ui/guide/ai;)V

    iput-object v1, p0, Lcom/twitter/android/moments/ui/guide/ai;->n:Lcom/twitter/app/common/util/b$a;

    .line 78
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/ai;->l:Lcom/twitter/android/moments/ui/fullscreen/bd;

    .line 79
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/ai;->m:Lcom/twitter/app/common/util/j;

    .line 80
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f10000a

    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v1

    iput v1, p0, Lcom/twitter/android/moments/ui/guide/ai;->q:F

    .line 81
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/guide/ai;)Lcom/twitter/android/moments/ui/video/a;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ai;->p:Lcom/twitter/android/moments/ui/video/a;

    return-object v0
.end method


# virtual methods
.method public synthetic a()Landroid/view/View;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/ai;->e()Lcom/twitter/android/moments/ui/fullscreen/MomentsVideoPlayerView;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/ah;)V
    .locals 7

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/guide/ai;->a(Lcom/twitter/android/moments/viewmodels/MomentModule;)V

    .line 86
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ai;->c:Landroid/view/ViewGroup;

    const v1, 0x7f13051e

    .line 87
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/AutoplayableVideoFillCropFrameLayout;

    .line 88
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ai;->l:Lcom/twitter/android/moments/ui/fullscreen/bd;

    .line 89
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/ah;->h()Lcom/twitter/model/core/Tweet;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/moments/ui/fullscreen/bd;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/moments/ui/video/a;

    move-result-object v2

    .line 90
    invoke-virtual {v0, p0}, Lcom/twitter/android/moments/ui/AutoplayableVideoFillCropFrameLayout;->setAutoplayableItem(Lcom/twitter/library/widget/a;)V

    .line 91
    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/video/a;->g()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/AutoplayableVideoFillCropFrameLayout;->setAVPlayerAttachment(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V

    .line 92
    new-instance v1, Lcom/twitter/android/moments/ui/fullscreen/MomentsVideoPlayerView;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/guide/ai;->a:Landroid/content/Context;

    .line 93
    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/video/a;->g()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v4

    new-instance v5, Lcom/twitter/android/moments/ui/fullscreen/bq;

    invoke-direct {v5}, Lcom/twitter/android/moments/ui/fullscreen/bq;-><init>()V

    sget-object v6, Lcom/twitter/library/av/VideoPlayerView$Mode;->e:Lcom/twitter/library/av/VideoPlayerView$Mode;

    invoke-direct {v1, v3, v4, v5, v6}, Lcom/twitter/android/moments/ui/fullscreen/MomentsVideoPlayerView;-><init>(Landroid/content/Context;Lcom/twitter/library/av/playback/AVPlayerAttachment;Lcom/twitter/library/av/ag;Lcom/twitter/library/av/VideoPlayerView$Mode;)V

    iput-object v1, p0, Lcom/twitter/android/moments/ui/guide/ai;->o:Lcom/twitter/android/moments/ui/fullscreen/MomentsVideoPlayerView;

    .line 95
    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/AutoplayableVideoFillCropFrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0207d9

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/AutoplayableVideoFillCropFrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 97
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ai;->o:Lcom/twitter/android/moments/ui/fullscreen/MomentsVideoPlayerView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/AutoplayableVideoFillCropFrameLayout;->addView(Landroid/view/View;)V

    .line 98
    iget-object v1, p1, Lcom/twitter/android/moments/viewmodels/ah;->b:Lcom/twitter/model/moments/e;

    invoke-static {v1}, Lcom/twitter/model/moments/e;->a(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/d;

    move-result-object v3

    .line 99
    iget v1, p0, Lcom/twitter/android/moments/ui/guide/ai;->q:F

    invoke-static {p1, v1}, Lcom/twitter/android/moments/data/r;->a(Lcom/twitter/android/moments/viewmodels/MomentModule;F)Lcom/twitter/util/math/Size;

    move-result-object v4

    .line 100
    if-eqz v4, :cond_0

    .line 101
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/twitter/model/moments/d;->a()Landroid/graphics/Rect;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v4, v1}, Lcom/twitter/android/moments/ui/AutoplayableVideoFillCropFrameLayout;->a(Lcom/twitter/util/math/Size;Landroid/graphics/Rect;)V

    .line 103
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ai;->o:Lcom/twitter/android/moments/ui/fullscreen/MomentsVideoPlayerView;

    new-instance v5, Lcom/twitter/android/moments/ui/guide/ai$2;

    invoke-direct {v5, p0, v4, v0, v3}, Lcom/twitter/android/moments/ui/guide/ai$2;-><init>(Lcom/twitter/android/moments/ui/guide/ai;Lcom/twitter/util/math/Size;Lcom/twitter/android/moments/ui/AutoplayableVideoFillCropFrameLayout;Lcom/twitter/model/moments/d;)V

    invoke-virtual {v1, v5}, Lcom/twitter/android/moments/ui/fullscreen/MomentsVideoPlayerView;->setAVPlayerListener(Lcom/twitter/library/av/k;)V

    .line 116
    iput-object v2, p0, Lcom/twitter/android/moments/ui/guide/ai;->p:Lcom/twitter/android/moments/ui/video/a;

    .line 117
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/ai;->r:Z

    if-eqz v0, :cond_2

    .line 118
    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/video/a;->a()V

    .line 119
    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/video/a;->c()V

    .line 123
    :goto_1
    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/video/a;->f()Lcom/twitter/library/av/playback/AVPlayer;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->d(Z)V

    .line 124
    return-void

    .line 101
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 121
    :cond_2
    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/video/a;->b()V

    goto :goto_1
.end method

.method public at_()V
    .locals 0

    .prologue
    .line 172
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/ai;->j()V

    .line 173
    return-void
.end method

.method public au_()V
    .locals 0

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/guide/ai;->k()V

    .line 178
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x1

    return v0
.end method

.method public d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 134
    invoke-super {p0}, Lcom/twitter/android/moments/ui/guide/t;->d()V

    .line 135
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ai;->p:Lcom/twitter/android/moments/ui/video/a;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ai;->p:Lcom/twitter/android/moments/ui/video/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/video/a;->e()V

    .line 137
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ai;->l:Lcom/twitter/android/moments/ui/fullscreen/bd;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ai;->p:Lcom/twitter/android/moments/ui/video/a;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/fullscreen/bd;->a(Lcom/twitter/android/moments/ui/video/a;)V

    .line 138
    iput-object v2, p0, Lcom/twitter/android/moments/ui/guide/ai;->p:Lcom/twitter/android/moments/ui/video/a;

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ai;->c:Landroid/view/ViewGroup;

    const v1, 0x7f13051e

    .line 142
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/FillCropFrameLayout;

    .line 143
    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/FillCropFrameLayout;->removeAllViews()V

    .line 144
    invoke-virtual {v0, v2}, Lcom/twitter/android/moments/ui/FillCropFrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    iput-object v2, p0, Lcom/twitter/android/moments/ui/guide/ai;->o:Lcom/twitter/android/moments/ui/fullscreen/MomentsVideoPlayerView;

    .line 146
    return-void
.end method

.method public e()Lcom/twitter/android/moments/ui/fullscreen/MomentsVideoPlayerView;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ai;->o:Lcom/twitter/android/moments/ui/fullscreen/MomentsVideoPlayerView;

    return-object v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ai;->p:Lcom/twitter/android/moments/ui/video/a;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ai;->p:Lcom/twitter/android/moments/ui/video/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/video/a;->b()V

    .line 185
    :cond_0
    return-void
.end method

.method public i()Landroid/view/View;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ai;->o:Lcom/twitter/android/moments/ui/fullscreen/MomentsVideoPlayerView;

    return-object v0
.end method

.method public j()V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ai;->m:Lcom/twitter/app/common/util/j;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ai;->n:Lcom/twitter/app/common/util/b$a;

    invoke-interface {v0, v1}, Lcom/twitter/app/common/util/j;->a(Lcom/twitter/app/common/util/b$a;)V

    .line 150
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ai;->p:Lcom/twitter/android/moments/ui/video/a;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ai;->p:Lcom/twitter/android/moments/ui/video/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/video/a;->a()V

    .line 152
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ai;->p:Lcom/twitter/android/moments/ui/video/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/video/a;->d()V

    .line 154
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/ai;->r:Z

    .line 155
    return-void
.end method

.method public k()V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ai;->p:Lcom/twitter/android/moments/ui/video/a;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ai;->p:Lcom/twitter/android/moments/ui/video/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/video/a;->e()V

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/ai;->m:Lcom/twitter/app/common/util/j;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/ai;->n:Lcom/twitter/app/common/util/b$a;

    invoke-interface {v0, v1}, Lcom/twitter/app/common/util/j;->b(Lcom/twitter/app/common/util/b$a;)V

    .line 162
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/ai;->r:Z

    .line 163
    return-void
.end method
