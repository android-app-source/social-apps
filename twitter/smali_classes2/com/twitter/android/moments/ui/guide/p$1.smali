.class Lcom/twitter/android/moments/ui/guide/p$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/guide/p;->a(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/twitter/android/moments/ui/guide/p;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/guide/p;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/p$1;->b:Lcom/twitter/android/moments/ui/guide/p;

    iput-object p2, p0, Lcom/twitter/android/moments/ui/guide/p$1;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 4

    .prologue
    .line 136
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/p$1;->a:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 137
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 138
    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/p$1;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 141
    sub-int v1, p5, p3

    div-int/lit8 v1, v1, 0x2

    .line 142
    iget-object v2, p0, Lcom/twitter/android/moments/ui/guide/p$1;->b:Lcom/twitter/android/moments/ui/guide/p;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/guide/p$1;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, Lcom/twitter/android/moments/ui/guide/p;->a(Landroid/content/Context;[II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/p$1;->b:Lcom/twitter/android/moments/ui/guide/p;

    .line 143
    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/p;->a(Lcom/twitter/android/moments/ui/guide/p;)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/p$1;->b:Lcom/twitter/android/moments/ui/guide/p;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/p$1;->a:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/twitter/android/moments/ui/guide/p;->a(Lcom/twitter/android/moments/ui/guide/p;Landroid/view/View;)Landroid/view/View;

    .line 145
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/p$1;->b:Lcom/twitter/android/moments/ui/guide/p;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/guide/p;->b(Lcom/twitter/android/moments/ui/guide/p;)V

    .line 147
    :cond_0
    return-void
.end method
