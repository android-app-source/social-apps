.class public Lcom/twitter/android/moments/ui/guide/p;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Landroid/support/v4/app/FragmentManager;

.field private final c:Lcom/twitter/android/util/h;

.field private final d:Lcom/twitter/android/util/h;

.field private final e:Lcom/twitter/android/util/h;

.field private final f:I
    .annotation build Landroid/support/annotation/IdRes;
    .end annotation
.end field

.field private final g:Ljava/lang/String;

.field private final h:Z

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field private k:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/support/v4/app/FragmentManager;Lcom/twitter/android/util/h;Lcom/twitter/android/util/h;Lcom/twitter/android/util/h;ZILjava/lang/String;)V
    .locals 1
    .param p7    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/p;->k:Z

    .line 65
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/p;->a:Landroid/app/Activity;

    .line 66
    iput-object p2, p0, Lcom/twitter/android/moments/ui/guide/p;->b:Landroid/support/v4/app/FragmentManager;

    .line 67
    iput-object p3, p0, Lcom/twitter/android/moments/ui/guide/p;->c:Lcom/twitter/android/util/h;

    .line 68
    iput-object p4, p0, Lcom/twitter/android/moments/ui/guide/p;->d:Lcom/twitter/android/util/h;

    .line 69
    iput-object p5, p0, Lcom/twitter/android/moments/ui/guide/p;->e:Lcom/twitter/android/util/h;

    .line 70
    iput-boolean p6, p0, Lcom/twitter/android/moments/ui/guide/p;->h:Z

    .line 71
    iput p7, p0, Lcom/twitter/android/moments/ui/guide/p;->f:I

    .line 72
    iput-object p8, p0, Lcom/twitter/android/moments/ui/guide/p;->g:Ljava/lang/String;

    .line 73
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/guide/p;)Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/p;->i:Landroid/view/View;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/guide/p;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/p;->i:Landroid/view/View;

    return-object p1
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 93
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/p;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/p;->i:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/p;->d:Lcom/twitter/android/util/h;

    invoke-virtual {v0}, Lcom/twitter/android/util/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    iput-boolean v2, p0, Lcom/twitter/android/moments/ui/guide/p;->k:Z

    .line 95
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/p;->i:Landroid/view/View;

    const-string/jumbo v1, "first_follow_button_tag"

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 96
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/p;->a:Landroid/app/Activity;

    const-string/jumbo v1, "first_follow_button_tag"

    invoke-static {v0, v1}, Lcom/twitter/ui/widget/Tooltip;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v1, 0x7f0a0587

    .line 97
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->a(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    .line 98
    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/Tooltip$a;->c(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v1, 0x7f0d0282

    .line 99
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->b(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const/4 v1, 0x1

    .line 100
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->a(Z)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/p;->b:Landroid/support/v4/app/FragmentManager;

    const-string/jumbo v2, "follow_button_tag"

    .line 101
    invoke-virtual {v0, v1, v2}, Lcom/twitter/ui/widget/Tooltip$a;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)Lcom/twitter/ui/widget/Tooltip;

    .line 102
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/p;->d:Lcom/twitter/android/util/h;

    invoke-virtual {v0}, Lcom/twitter/android/util/h;->b()V

    .line 104
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/guide/p;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/guide/p;->b()V

    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 107
    invoke-static {}, Lwk;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/p;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/p;->j:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/p;->e:Lcom/twitter/android/util/h;

    .line 108
    invoke-virtual {v0}, Lcom/twitter/android/util/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    iput-boolean v2, p0, Lcom/twitter/android/moments/ui/guide/p;->k:Z

    .line 110
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/p;->j:Landroid/view/View;

    const-string/jumbo v1, "first_thumbnail_tag"

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 111
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/p;->a:Landroid/app/Activity;

    const-string/jumbo v1, "first_thumbnail_tag"

    invoke-static {v0, v1}, Lcom/twitter/ui/widget/Tooltip;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v1, 0x7f0a0595

    .line 112
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->a(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    .line 113
    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/Tooltip$a;->c(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v1, 0x7f0d0282

    .line 114
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->b(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const/4 v1, 0x1

    .line 115
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->a(Z)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/p;->b:Landroid/support/v4/app/FragmentManager;

    const-string/jumbo v2, "first_thumbnail_tag"

    .line 116
    invoke-virtual {v0, v1, v2}, Lcom/twitter/ui/widget/Tooltip$a;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)Lcom/twitter/ui/widget/Tooltip;

    .line 117
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/p;->e:Lcom/twitter/android/util/h;

    invoke-virtual {v0}, Lcom/twitter/android/util/h;->b()V

    .line 119
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/guide/p;->c()V

    .line 175
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/guide/p;->b()V

    .line 176
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/p;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/p;->i:Landroid/view/View;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 131
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    if-nez v0, :cond_1

    .line 132
    new-instance v0, Lcom/twitter/android/moments/ui/guide/p$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/moments/ui/guide/p$1;-><init>(Lcom/twitter/android/moments/ui/guide/p;Landroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/p;->i:Landroid/view/View;

    .line 151
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/guide/p;->b()V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 76
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/p;->c:Lcom/twitter/android/util/h;

    invoke-virtual {v0}, Lcom/twitter/android/util/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/p;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/p;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/p;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/ui/widget/Tooltip;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    .line 83
    :goto_0
    const v1, 0x7f0a058c

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->a(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    .line 84
    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/Tooltip$a;->c(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    .line 85
    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/Tooltip$a;->a(Z)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/p;->b:Landroid/support/v4/app/FragmentManager;

    const-string/jumbo v2, "post_follow_tag"

    .line 86
    invoke-virtual {v0, v1, v2}, Lcom/twitter/ui/widget/Tooltip$a;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)Lcom/twitter/ui/widget/Tooltip;

    .line 87
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/p;->c:Lcom/twitter/android/util/h;

    invoke-virtual {v0}, Lcom/twitter/android/util/h;->b()V

    .line 88
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/p;->d:Lcom/twitter/android/util/h;

    invoke-virtual {v0}, Lcom/twitter/android/util/h;->b()V

    .line 90
    :cond_0
    return-void

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/p;->a:Landroid/app/Activity;

    iget v1, p0, Lcom/twitter/android/moments/ui/guide/p;->f:I

    invoke-static {v0, v1}, Lcom/twitter/ui/widget/Tooltip;->a(Landroid/content/Context;I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    goto :goto_0
.end method

.method a(Landroid/content/Context;[II)Z
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 158
    invoke-static {p1}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;)Lcom/twitter/util/math/Size;

    move-result-object v1

    .line 159
    aget v2, p2, v0

    if-ltz v2, :cond_0

    aget v2, p2, v0

    add-int/2addr v2, p3

    invoke-virtual {v1}, Lcom/twitter/util/math/Size;->b()I

    move-result v1

    if-gt v2, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/twitter/android/moments/ui/guide/p;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/p;->j:Landroid/view/View;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 164
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/p;->j:Landroid/view/View;

    .line 165
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/guide/p;->c()V

    .line 167
    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 170
    iput-boolean p1, p0, Lcom/twitter/android/moments/ui/guide/p;->k:Z

    .line 171
    return-void
.end method
