.class public Lcom/twitter/android/moments/ui/guide/x;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/card/AutoPlayableViewHost;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/card/AutoPlayableViewHost;Lcom/twitter/library/widget/renderablecontent/d;)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/x;->a:Lcom/twitter/android/moments/ui/card/AutoPlayableViewHost;

    .line 36
    invoke-interface {p2}, Lcom/twitter/library/widget/renderablecontent/d;->bg_()V

    .line 37
    invoke-interface {p2}, Lcom/twitter/library/widget/renderablecontent/d;->c()V

    .line 38
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/x;->a:Lcom/twitter/android/moments/ui/card/AutoPlayableViewHost;

    invoke-interface {p2}, Lcom/twitter/library/widget/renderablecontent/d;->e()Lcom/twitter/library/widget/renderablecontent/c;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/widget/c;->a(Lcom/twitter/library/widget/renderablecontent/c;)Lcom/twitter/library/widget/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/card/AutoPlayableViewHost;->setAutoPlayableItem(Lcom/twitter/library/widget/a;)V

    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/x;->a:Lcom/twitter/android/moments/ui/card/AutoPlayableViewHost;

    invoke-interface {p2}, Lcom/twitter/library/widget/renderablecontent/d;->d()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/card/AutoPlayableViewHost;->addView(Landroid/view/View;)V

    .line 40
    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/twitter/android/moments/viewmodels/u;)Lcom/twitter/android/moments/ui/guide/x;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 25
    new-instance v7, Lcom/twitter/android/moments/ui/card/AutoPlayableViewHost;

    invoke-direct {v7, p0}, Lcom/twitter/android/moments/ui/card/AutoPlayableViewHost;-><init>(Landroid/content/Context;)V

    .line 26
    new-instance v0, Lbxu;

    invoke-direct {v0}, Lbxu;-><init>()V

    .line 28
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/u;->a()Lcej;

    move-result-object v1

    iget-object v2, v1, Lcej;->f:Lcax;

    sget-object v3, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 29
    invoke-static {}, Lcom/twitter/library/card/m;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v4, Lcom/twitter/library/card/m;

    invoke-direct {v4}, Lcom/twitter/library/card/m;-><init>()V

    :goto_0
    new-instance v5, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v5}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    move-object v1, p0

    .line 27
    invoke-virtual/range {v0 .. v6}, Lbxu;->a(Landroid/app/Activity;Lcax;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/library/card/m;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/widget/renderablecontent/d;

    move-result-object v0

    .line 31
    new-instance v1, Lcom/twitter/android/moments/ui/guide/x;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/renderablecontent/d;

    invoke-direct {v1, v7, v0}, Lcom/twitter/android/moments/ui/guide/x;-><init>(Lcom/twitter/android/moments/ui/card/AutoPlayableViewHost;Lcom/twitter/library/widget/renderablecontent/d;)V

    return-object v1

    :cond_0
    move-object v4, v6

    .line 29
    goto :goto_0
.end method


# virtual methods
.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/x;->a:Lcom/twitter/android/moments/ui/card/AutoPlayableViewHost;

    return-object v0
.end method
