.class abstract Lcom/twitter/android/moments/ui/guide/t;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/guide/ae;
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/android/moments/viewmodels/MomentModule;",
        "V:",
        "Landroid/view/View;",
        ":",
        "Lcom/twitter/media/ui/image/d;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/twitter/android/moments/ui/guide/ae;",
        "Lcom/twitter/util/q",
        "<",
        "Lcom/twitter/model/moments/Moment;",
        ">;"
    }
.end annotation


# instance fields
.field protected final a:Landroid/content/Context;

.field protected final b:Lcom/twitter/android/moments/data/e;

.field protected final c:Landroid/view/ViewGroup;

.field protected final c_:Lcom/twitter/android/moments/ui/guide/p;

.field protected final d:Landroid/view/View;

.field protected final e:Landroid/widget/TextView;

.field protected final f:Landroid/widget/TextView;

.field protected final g:Lcom/twitter/ui/widget/BadgeView;

.field protected final h:Landroid/view/LayoutInflater;

.field protected final i:Lcom/twitter/android/moments/ui/guide/g;

.field protected final k:Lcom/twitter/android/moments/data/j;

.field private final l:Lcom/twitter/android/moments/ui/guide/l$b;

.field private final m:Lcom/twitter/android/moments/ui/guide/j;

.field private final n:Lcom/twitter/android/moments/data/ai;

.field private final o:Lcom/twitter/android/moments/ui/guide/y;

.field private final p:Lcom/twitter/android/moments/data/ab;

.field private final q:Lcom/twitter/android/moments/ui/guide/BadgeableRichImageView;

.field private final r:Lcom/twitter/android/moments/ui/guide/r;

.field private s:Lcom/twitter/model/moments/Moment;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/android/moments/ui/guide/y;Lcom/twitter/android/moments/ui/guide/p;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/twitter/android/moments/ui/guide/g;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/w;Lzp$b;Lcom/twitter/android/moments/data/am;Lcom/twitter/android/moments/ui/guide/l$b;Lcom/twitter/android/moments/ui/guide/r;)V
    .locals 8

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/t;->a:Landroid/content/Context;

    .line 72
    iput-object p2, p0, Lcom/twitter/android/moments/ui/guide/t;->o:Lcom/twitter/android/moments/ui/guide/y;

    .line 73
    iput-object p3, p0, Lcom/twitter/android/moments/ui/guide/t;->c_:Lcom/twitter/android/moments/ui/guide/p;

    .line 74
    iput-object p4, p0, Lcom/twitter/android/moments/ui/guide/t;->h:Landroid/view/LayoutInflater;

    .line 75
    iput-object p5, p0, Lcom/twitter/android/moments/ui/guide/t;->c:Landroid/view/ViewGroup;

    .line 76
    iput-object p6, p0, Lcom/twitter/android/moments/ui/guide/t;->i:Lcom/twitter/android/moments/ui/guide/g;

    .line 77
    iput-object p7, p0, Lcom/twitter/android/moments/ui/guide/t;->k:Lcom/twitter/android/moments/data/j;

    .line 78
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->l:Lcom/twitter/android/moments/ui/guide/l$b;

    .line 79
    iget-object v2, p0, Lcom/twitter/android/moments/ui/guide/t;->c:Landroid/view/ViewGroup;

    const v3, 0x7f1304f2

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/twitter/android/moments/ui/guide/t;->e:Landroid/widget/TextView;

    .line 80
    iget-object v2, p0, Lcom/twitter/android/moments/ui/guide/t;->c:Landroid/view/ViewGroup;

    const v3, 0x7f1304f4

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/twitter/android/moments/ui/guide/t;->f:Landroid/widget/TextView;

    .line 81
    iget-object v2, p0, Lcom/twitter/android/moments/ui/guide/t;->c:Landroid/view/ViewGroup;

    const v3, 0x7f130527

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/twitter/ui/widget/BadgeView;

    iput-object v2, p0, Lcom/twitter/android/moments/ui/guide/t;->g:Lcom/twitter/ui/widget/BadgeView;

    .line 82
    iget-object v2, p0, Lcom/twitter/android/moments/ui/guide/t;->c:Landroid/view/ViewGroup;

    const v3, 0x7f130525

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/moments/ui/guide/t;->d:Landroid/view/View;

    .line 83
    iget-object v2, p0, Lcom/twitter/android/moments/ui/guide/t;->c:Landroid/view/ViewGroup;

    const v3, 0x7f130526

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/moments/ui/guide/BadgeableRichImageView;

    iput-object v2, p0, Lcom/twitter/android/moments/ui/guide/t;->q:Lcom/twitter/android/moments/ui/guide/BadgeableRichImageView;

    .line 84
    new-instance v2, Laae;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/guide/t;->c:Landroid/view/ViewGroup;

    invoke-direct {v2, v3}, Laae;-><init>(Landroid/view/View;)V

    .line 85
    new-instance v3, Lcom/twitter/android/moments/data/e;

    move-object/from16 v0, p9

    move-object/from16 v1, p10

    invoke-direct {v3, v2, v0, v1}, Lcom/twitter/android/moments/data/e;-><init>(Laae;Lcom/twitter/android/moments/data/w;Lzq;)V

    iput-object v3, p0, Lcom/twitter/android/moments/ui/guide/t;->b:Lcom/twitter/android/moments/data/e;

    .line 88
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/moments/ui/guide/t;->c:Landroid/view/ViewGroup;

    const v4, 0x7f130502

    const v5, 0x7f130503

    .line 87
    invoke-static {v2, v3, v4, v5}, Laah;->a(Landroid/content/res/Resources;Landroid/view/View;II)Laaf;

    move-result-object v2

    .line 89
    new-instance v3, Lcom/twitter/android/moments/data/ai;

    new-instance v4, Laag;

    invoke-direct {v4, v2}, Laag;-><init>(Laaf;)V

    move-object/from16 v0, p11

    invoke-direct {v3, v4, v0}, Lcom/twitter/android/moments/data/ai;-><init>(Laag;Lcom/twitter/android/moments/data/am;)V

    iput-object v3, p0, Lcom/twitter/android/moments/ui/guide/t;->n:Lcom/twitter/android/moments/data/ai;

    .line 90
    new-instance v2, Lcom/twitter/android/moments/ui/guide/j;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/guide/t;->q:Lcom/twitter/android/moments/ui/guide/BadgeableRichImageView;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/guide/t;->k:Lcom/twitter/android/moments/data/j;

    const/4 v5, 0x1

    new-array v5, v5, [Lcom/twitter/android/moments/ui/guide/ae;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/twitter/android/moments/ui/guide/t;->n:Lcom/twitter/android/moments/data/ai;

    aput-object v7, v5, v6

    .line 91
    invoke-static {p0, v5}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    move-object/from16 v0, p8

    invoke-direct {v2, v3, v4, v0, v5}, Lcom/twitter/android/moments/ui/guide/j;-><init>(Lcom/twitter/android/moments/ui/a;Lcom/twitter/android/moments/data/j;Lcom/twitter/android/moments/data/j;Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/twitter/android/moments/ui/guide/t;->m:Lcom/twitter/android/moments/ui/guide/j;

    .line 92
    new-instance v2, Lcom/twitter/android/moments/data/ac;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/guide/t;->c:Landroid/view/ViewGroup;

    const v4, 0x7f130521

    .line 93
    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/twitter/android/moments/data/ac;-><init>(Landroid/view/View;)V

    .line 94
    new-instance v3, Lcom/twitter/android/moments/data/ab;

    move-object/from16 v0, p12

    move-object/from16 v1, p10

    invoke-direct {v3, v2, v0, v1}, Lcom/twitter/android/moments/data/ab;-><init>(Lcom/twitter/android/moments/data/ac;Lcom/twitter/android/moments/ui/guide/l$b;Lzp$a;)V

    iput-object v3, p0, Lcom/twitter/android/moments/ui/guide/t;->p:Lcom/twitter/android/moments/data/ab;

    .line 96
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->r:Lcom/twitter/android/moments/ui/guide/r;

    .line 97
    iget-object v2, p0, Lcom/twitter/android/moments/ui/guide/t;->r:Lcom/twitter/android/moments/ui/guide/r;

    invoke-virtual {v2}, Lcom/twitter/android/moments/ui/guide/r;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 98
    iget-object v2, p0, Lcom/twitter/android/moments/ui/guide/t;->q:Lcom/twitter/android/moments/ui/guide/BadgeableRichImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/twitter/android/moments/ui/guide/BadgeableRichImageView;->setVisibility(I)V

    .line 102
    :goto_0
    return-void

    .line 100
    :cond_0
    iget-object v2, p0, Lcom/twitter/android/moments/ui/guide/t;->q:Lcom/twitter/android/moments/ui/guide/BadgeableRichImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/twitter/android/moments/ui/guide/BadgeableRichImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Lcfn;)V
    .locals 2

    .prologue
    .line 146
    if-eqz p1, :cond_0

    .line 147
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->d:Landroid/view/View;

    const v1, 0x7f020080

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 148
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->n:Lcom/twitter/android/moments/data/ai;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/data/ai;->a(Lcfn;)V

    .line 153
    :goto_0
    return-void

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->d:Landroid/view/View;

    const v1, 0x7f020081

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 151
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->n:Lcom/twitter/android/moments/data/ai;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/ai;->a()V

    goto :goto_0
.end method

.method private a(Lcom/twitter/model/moments/Moment;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 120
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->e:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/twitter/model/moments/Moment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/t;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/twitter/android/moments/data/x;->a(Landroid/content/res/Resources;Lcom/twitter/model/moments/Moment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->o:Lcom/twitter/android/moments/ui/guide/y;

    invoke-interface {v0, p1}, Lcom/twitter/android/moments/ui/guide/y;->a(Lcom/twitter/model/moments/Moment;)V

    .line 124
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->b:Lcom/twitter/android/moments/data/e;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/data/e;->a(Lcom/twitter/model/moments/Moment;)V

    .line 125
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->b:Lcom/twitter/android/moments/data/e;

    new-instance v1, Lcom/twitter/android/moments/ui/guide/t$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/guide/t$1;-><init>(Lcom/twitter/android/moments/ui/guide/t;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/data/e;->a(Lcom/twitter/android/moments/data/e$a;)V

    .line 133
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->c_:Lcom/twitter/android/moments/ui/guide/p;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/t;->b:Lcom/twitter/android/moments/data/e;

    invoke-virtual {v1}, Lcom/twitter/android/moments/data/e;->b()Lcom/twitter/ui/widget/ToggleTwitterButton;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/guide/p;->a(Landroid/view/View;)V

    .line 135
    invoke-virtual {p1}, Lcom/twitter/model/moments/Moment;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->g:Lcom/twitter/ui/widget/BadgeView;

    const v1, 0x7f020842

    const/4 v2, 0x1

    invoke-static {p1, v0, v1, v2}, Lcom/twitter/android/moments/ui/d;->a(Lcom/twitter/model/moments/Moment;Lcom/twitter/ui/widget/BadgeView;IZ)V

    .line 138
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 143
    :goto_0
    return-void

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->g:Lcom/twitter/ui/widget/BadgeView;

    invoke-virtual {v0, v3}, Lcom/twitter/ui/widget/BadgeView;->setVisibility(I)V

    .line 141
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public abstract a()Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation
.end method

.method public a(Lcom/twitter/android/moments/viewmodels/MomentModule;)V
    .locals 4

    .prologue
    .line 111
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->b()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->s:Lcom/twitter/model/moments/Moment;

    .line 112
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->s:Lcom/twitter/model/moments/Moment;

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/guide/t;->a(Lcom/twitter/model/moments/Moment;)V

    .line 113
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->d()Lcfn;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/guide/t;->a(Lcfn;)V

    .line 114
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->p:Lcom/twitter/android/moments/data/ab;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/data/ab;->a(Lcom/twitter/android/moments/viewmodels/MomentModule;)V

    .line 116
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->m:Lcom/twitter/android/moments/ui/guide/j;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/guide/t;->s:Lcom/twitter/model/moments/Moment;

    iget-wide v2, v1, Lcom/twitter/model/moments/Moment;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/moments/ui/guide/j;->a(J)V

    .line 117
    return-void
.end method

.method public b()Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->e:Landroid/widget/TextView;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/view/View;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/moments/ui/guide/t;->f:Landroid/widget/TextView;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/util/collection/o;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->b:Lcom/twitter/android/moments/data/e;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/e;->a()V

    .line 173
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->n:Lcom/twitter/android/moments/data/ai;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/ai;->a()V

    .line 174
    return-void
.end method

.method public onEvent(Lcom/twitter/model/moments/Moment;)V
    .locals 4

    .prologue
    .line 157
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->s:Lcom/twitter/model/moments/Moment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->s:Lcom/twitter/model/moments/Moment;

    iget-wide v0, v0, Lcom/twitter/model/moments/Moment;->b:J

    iget-wide v2, p1, Lcom/twitter/model/moments/Moment;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 158
    iput-object p1, p0, Lcom/twitter/android/moments/ui/guide/t;->s:Lcom/twitter/model/moments/Moment;

    .line 159
    iget-object v0, p0, Lcom/twitter/android/moments/ui/guide/t;->s:Lcom/twitter/model/moments/Moment;

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/ui/guide/t;->a(Lcom/twitter/model/moments/Moment;)V

    .line 161
    :cond_0
    return-void
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 38
    check-cast p1, Lcom/twitter/model/moments/Moment;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/guide/t;->onEvent(Lcom/twitter/model/moments/Moment;)V

    return-void
.end method
