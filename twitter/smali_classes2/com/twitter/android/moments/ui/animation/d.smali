.class public Lcom/twitter/android/moments/ui/animation/d;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/animation/d$b;,
        Lcom/twitter/android/moments/ui/animation/d$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/twitter/android/moments/ui/animation/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/moments/ui/animation/d;->a:Ljava/lang/String;

    .line 34
    const-class v0, Lcom/twitter/android/moments/ui/animation/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/moments/ui/animation/d;->b:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/animation/d;->c:Landroid/content/Context;

    .line 45
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/animation/d;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/d;->c:Landroid/content/Context;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/twitter/android/moments/ui/animation/d;
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/twitter/android/moments/ui/animation/d;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/animation/d;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 73
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 74
    const-string/jumbo v1, "ton.twimg.com"

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 78
    :goto_0
    return v0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    sget-object v1, Lcom/twitter/android/moments/ui/animation/d;->b:Ljava/lang/String;

    const-string/jumbo v2, "Invalid url"

    invoke-static {v1, v2, v0}, Lcqj;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 78
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .prologue
    .line 90
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 91
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 92
    invoke-static {v0}, Lcqc;->e(Ljava/io/File;)Z

    .line 93
    return-object v0
.end method


# virtual methods
.method a(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Lrx/c;
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/io/File;",
            ")",
            "Lrx/c",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    new-instance v0, Lcom/twitter/android/moments/ui/animation/d$b;

    sget-object v1, Lcom/twitter/android/moments/ui/animation/d;->a:Ljava/lang/String;

    invoke-direct {v0, p1, v1, p3}, Lcom/twitter/android/moments/ui/animation/d$b;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)V

    invoke-virtual {v0, p2}, Lcom/twitter/android/moments/ui/animation/d$b;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/lang/String;Ljava/lang/String;)Lrx/c;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lrx/c",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    invoke-direct {p0, p2, p1}, Lcom/twitter/android/moments/ui/animation/d;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lrx/c",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/ui/animation/d;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {p0, p3, p2}, Lcom/twitter/android/moments/ui/animation/d;->a(Ljava/lang/String;Ljava/lang/String;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/animation/d$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/ui/animation/d$1;-><init>(Lcom/twitter/android/moments/ui/animation/d;Ljava/lang/String;)V

    .line 56
    invoke-virtual {v0, v1}, Lrx/c;->f(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 67
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lrx/c;->d()Lrx/c;

    move-result-object v0

    goto :goto_0
.end method
