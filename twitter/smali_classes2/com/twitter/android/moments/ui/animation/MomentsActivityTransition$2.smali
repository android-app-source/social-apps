.class Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->c()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$2;->a:Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 6

    .prologue
    .line 138
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$2;->a:Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->b(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 139
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 140
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$2;->a:Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->h(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)Lcom/twitter/android/moments/ui/animation/b;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$2;->a:Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->c(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)Landroid/view/ViewGroup;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$2;->a:Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;

    invoke-static {v2}, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->d(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)Landroid/widget/ImageView;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$2;->a:Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;

    invoke-static {v3}, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->e(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)Landroid/view/View;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$2;->a:Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;

    invoke-static {v4}, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->f(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)Lcom/twitter/ui/anim/b$a;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$2;->a:Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;

    .line 144
    invoke-static {v5}, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->g(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)Lcom/twitter/ui/anim/a$a;

    move-result-object v5

    .line 143
    invoke-interface/range {v0 .. v5}, Lcom/twitter/android/moments/ui/animation/b;->a(Landroid/view/View;Landroid/widget/ImageView;Landroid/view/View;Lcom/twitter/ui/anim/b$a;Lcom/twitter/ui/anim/a$a;)V

    .line 147
    const/4 v0, 0x1

    return v0
.end method
