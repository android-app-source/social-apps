.class Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/media/request/b$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->a(Lcom/twitter/ui/anim/a$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/media/request/b$b",
        "<",
        "Lcom/twitter/media/request/ImageResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/model/moments/d;

.field final synthetic b:Lcom/twitter/util/math/Size;

.field final synthetic c:Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;Lcom/twitter/model/moments/d;Lcom/twitter/util/math/Size;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$3;->c:Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;

    iput-object p2, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$3;->a:Lcom/twitter/model/moments/d;

    iput-object p3, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$3;->b:Lcom/twitter/util/math/Size;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/media/request/ImageResponse;)V
    .locals 4

    .prologue
    .line 171
    invoke-virtual {p1}, Lcom/twitter/media/request/ImageResponse;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 172
    invoke-virtual {p1}, Lcom/twitter/media/request/ImageResponse;->a()Lcom/twitter/media/model/MediaFile;

    move-result-object v1

    .line 173
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 174
    iget-object v2, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$3;->a:Lcom/twitter/model/moments/d;

    iget-object v1, v1, Lcom/twitter/media/model/MediaFile;->f:Lcom/twitter/util/math/Size;

    iget-object v3, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$3;->b:Lcom/twitter/util/math/Size;

    invoke-static {v2, v1, v3}, Lcio;->a(Lcom/twitter/model/moments/d;Lcom/twitter/util/math/Size;Lcom/twitter/util/math/Size;)Landroid/graphics/Rect;

    move-result-object v1

    .line 175
    iget-object v2, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$3;->c:Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;

    invoke-static {v2}, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->d(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 176
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$3;->c:Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->d(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$3;->c:Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;

    invoke-static {v2}, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->d(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-static {v2, v1}, Lcio;->a(Landroid/widget/ImageView;Landroid/graphics/Rect;)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$3;->c:Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->c(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$3;->c:Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;

    invoke-static {v1}, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->i(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 179
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$3;->c:Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;

    invoke-static {v0}, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->j(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)V

    .line 180
    return-void
.end method

.method public bridge synthetic a(Lcom/twitter/media/request/ResourceResponse;)V
    .locals 0

    .prologue
    .line 168
    check-cast p1, Lcom/twitter/media/request/ImageResponse;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$3;->a(Lcom/twitter/media/request/ImageResponse;)V

    return-void
.end method
