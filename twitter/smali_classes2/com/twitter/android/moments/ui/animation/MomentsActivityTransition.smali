.class public Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;
.super Lcom/twitter/ui/anim/a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$Type;
    }
.end annotation


# instance fields
.field private final a:Landroid/support/v4/app/FragmentActivity;

.field private final b:Lcom/twitter/android/moments/ui/animation/b;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/twitter/util/math/Size;

.field private final e:Landroid/view/ViewGroup;

.field private final f:Lcom/twitter/ui/anim/b$a;

.field private final g:Landroid/view/ViewGroup;

.field private final h:Landroid/widget/ImageView;

.field private final i:Landroid/view/View;

.field private final j:Lcom/twitter/model/moments/e;

.field private final k:Landroid/view/View;

.field private l:Lcom/twitter/ui/anim/a$a;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/android/moments/ui/animation/b;Lcom/twitter/ui/anim/b$a;Ljava/lang/String;Lcom/twitter/util/math/Size;Lcom/twitter/model/moments/e;Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/twitter/ui/anim/a;-><init>()V

    .line 114
    iput-object p1, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->a:Landroid/support/v4/app/FragmentActivity;

    .line 115
    iput-object p2, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->b:Lcom/twitter/android/moments/ui/animation/b;

    .line 116
    iput-object p7, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->e:Landroid/view/ViewGroup;

    .line 117
    iput-object p4, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->c:Ljava/lang/String;

    .line 118
    iput-object p5, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->d:Lcom/twitter/util/math/Size;

    .line 119
    iput-object p3, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->f:Lcom/twitter/ui/anim/b$a;

    .line 120
    iput-object p6, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->j:Lcom/twitter/model/moments/e;

    .line 121
    iput-object p8, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->k:Landroid/view/View;

    .line 122
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->e:Landroid/view/ViewGroup;

    const v1, 0x7f130515

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->g:Landroid/view/ViewGroup;

    .line 123
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->g:Landroid/view/ViewGroup;

    const v1, 0x7f130550

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 124
    new-instance v1, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$1;-><init>(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->g:Landroid/view/ViewGroup;

    const v1, 0x7f13054e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->h:Landroid/widget/ImageView;

    .line 131
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->g:Landroid/view/ViewGroup;

    const v1, 0x7f13054f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->i:Landroid/view/View;

    .line 132
    return-void
.end method

.method public static a(Landroid/content/Intent;Lcom/twitter/model/moments/Moment;Lcom/twitter/util/math/Size;Ljava/lang/String;Lcom/twitter/model/moments/e;Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$Type;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 100
    const-string/jumbo v0, "moment"

    sget-object v1, Lcom/twitter/model/moments/Moment;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p0, v0, p1, v1}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/content/Intent;

    .line 101
    const-string/jumbo v0, "media_size"

    invoke-virtual {p0, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 102
    const-string/jumbo v0, "media_entity"

    invoke-virtual {p0, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 103
    const-string/jumbo v0, "crop_hint"

    sget-object v1, Lcom/twitter/model/moments/e;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p0, v0, p4, v1}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/content/Intent;

    .line 104
    const-string/jumbo v0, "transition_type"

    invoke-virtual {p5}, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$Type;->ordinal()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 105
    const-string/jumbo v0, "transition_type"

    const-class v1, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$Type;

    invoke-static {v1}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    invoke-static {p0, v0, p5, v1}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/content/Intent;

    .line 106
    return-object p0
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)Landroid/support/v4/app/FragmentActivity;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->a:Landroid/support/v4/app/FragmentActivity;

    return-object v0
.end method

.method public static a(Landroid/support/v4/app/FragmentActivity;Landroid/content/Intent;Landroid/view/ViewGroup;)Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;
    .locals 10

    .prologue
    .line 70
    const-string/jumbo v0, "transition_type"

    const-class v1, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$Type;

    .line 71
    invoke-static {v1}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    .line 70
    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$Type;

    .line 73
    sget-object v1, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$4;->a:[I

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$Type;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 79
    new-instance v2, Lcom/twitter/android/moments/ui/animation/c;

    invoke-direct {v2}, Lcom/twitter/android/moments/ui/animation/c;-><init>()V

    .line 82
    :goto_0
    const-string/jumbo v0, "crop_hint"

    sget-object v1, Lcom/twitter/model/moments/e;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/model/moments/e;

    .line 83
    const-string/jumbo v0, "media_entity"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 84
    const-string/jumbo v0, "media_size"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/twitter/util/math/Size;

    .line 85
    const-string/jumbo v0, "moment"

    sget-object v1, Lcom/twitter/model/moments/Moment;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/Moment;

    .line 86
    const-string/jumbo v1, "tweet"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/core/Tweet;

    .line 88
    invoke-static {p1}, Lcom/twitter/ui/anim/b;->b(Landroid/content/Intent;)Lcom/twitter/ui/anim/b$a;

    move-result-object v3

    .line 90
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    invoke-static {v7}, Labh;->a(Landroid/view/LayoutInflater;)Labh;

    move-result-object v7

    .line 91
    new-instance v8, Labi;

    .line 92
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-direct {v8, v9, v7}, Labi;-><init>(Landroid/content/res/Resources;Labh;)V

    .line 93
    invoke-virtual {v8, v0, v1}, Labi;->a(Lcom/twitter/model/moments/Moment;Lcom/twitter/model/core/Tweet;)V

    .line 94
    new-instance v0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;

    .line 95
    invoke-virtual {v7}, Labh;->i()Landroid/view/View;

    move-result-object v8

    move-object v1, p0

    move-object v7, p2

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/android/moments/ui/animation/b;Lcom/twitter/ui/anim/b$a;Ljava/lang/String;Lcom/twitter/util/math/Size;Lcom/twitter/model/moments/e;Landroid/view/ViewGroup;Landroid/view/View;)V

    .line 94
    return-object v0

    .line 75
    :pswitch_0
    new-instance v2, Lcom/twitter/android/moments/ui/animation/f;

    invoke-direct {v2}, Lcom/twitter/android/moments/ui/animation/f;-><init>()V

    goto :goto_0

    .line 73
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->e:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->g:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$2;-><init>(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 150
    return-void
.end method

.method static synthetic d(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->h:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)Landroid/view/View;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->i:Landroid/view/View;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)Lcom/twitter/ui/anim/b$a;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->f:Lcom/twitter/ui/anim/b$a;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)Lcom/twitter/ui/anim/a$a;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->l:Lcom/twitter/ui/anim/a$a;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)Lcom/twitter/android/moments/ui/animation/b;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->b:Lcom/twitter/android/moments/ui/animation/b;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)Landroid/view/View;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->k:Landroid/view/View;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->c()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->g:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 195
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->e:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 196
    return-void
.end method

.method public a(Lcom/twitter/ui/anim/a$a;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 154
    iput-object p1, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->l:Lcom/twitter/ui/anim/a$a;

    .line 155
    sget-object v1, Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;->a:Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

    .line 156
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0}, Lcom/twitter/util/ui/k;->d(Landroid/content/Context;)Lcom/twitter/util/math/Size;

    move-result-object v2

    .line 157
    invoke-virtual {v2}, Lcom/twitter/util/math/Size;->g()F

    move-result v0

    .line 158
    iget-object v3, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->j:Lcom/twitter/model/moments/e;

    invoke-static {v3, v0}, Lcom/twitter/model/moments/e;->a(Lcom/twitter/model/moments/e;F)Lcom/twitter/model/moments/d;

    move-result-object v3

    .line 159
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->h:Landroid/widget/ImageView;

    sget-object v4, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 162
    new-instance v0, Lcom/twitter/media/request/a$a;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->c:Ljava/lang/String;

    invoke-direct {v0, v4}, Lcom/twitter/media/request/a$a;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->d:Lcom/twitter/util/math/Size;

    .line 163
    invoke-virtual {v0, v4}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    .line 164
    invoke-virtual {v0, v5}, Lcom/twitter/media/request/a$a;->d(Z)Lcom/twitter/media/request/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/a$a;

    const/4 v4, 0x1

    .line 165
    invoke-virtual {v0, v4}, Lcom/twitter/media/request/a$a;->e(Z)Lcom/twitter/media/request/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/a$a;

    iget-object v1, v1, Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;->decoderScaleType:Lcom/twitter/media/decoder/ImageDecoder$ScaleType;

    .line 166
    invoke-virtual {v0, v1}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/media/decoder/ImageDecoder$ScaleType;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    .line 167
    new-instance v1, Lcom/twitter/library/media/manager/TwitterImageRequester;

    iget-object v4, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-direct {v1, v4}, Lcom/twitter/library/media/manager/TwitterImageRequester;-><init>(Landroid/content/Context;)V

    .line 168
    new-instance v4, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$3;

    invoke-direct {v4, p0, v3, v2}, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition$3;-><init>(Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;Lcom/twitter/model/moments/d;Lcom/twitter/util/math/Size;)V

    invoke-virtual {v0, v4}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/media/request/b$b;)Lcom/twitter/media/request/b$a;

    .line 182
    invoke-virtual {v0}, Lcom/twitter/media/request/a$a;->a()Lcom/twitter/media/request/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/media/manager/TwitterImageRequester;->a(Lcom/twitter/media/request/a;)Z

    .line 183
    invoke-virtual {v1, v5}, Lcom/twitter/library/media/manager/TwitterImageRequester;->a(Z)V

    .line 184
    return-void
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/MomentsActivityTransition;->g:Landroid/view/ViewGroup;

    return-object v0
.end method
