.class public Lcom/twitter/android/moments/ui/animation/e;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field static final a:Ljava/util/List;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static b:Lcom/twitter/android/moments/ui/animation/e;


# instance fields
.field private final c:Lcom/twitter/android/moments/ui/animation/a;

.field private final d:Lcom/twitter/android/moments/ui/animation/d;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 34
    const v0, 0x7f0201b7

    .line 35
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v1, 0x37

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const v3, 0x7f0201b8

    .line 36
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const v3, 0x7f0201b9

    .line 37
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const v3, 0x7f0201ba

    .line 38
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const v3, 0x7f0201bb

    .line 39
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const v3, 0x7f0201bc

    .line 40
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const v3, 0x7f0201bd

    .line 41
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const v3, 0x7f0201be

    .line 42
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const v3, 0x7f0201bf

    .line 43
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const v3, 0x7f0201c0

    .line 44
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const v3, 0x7f0201c1

    .line 45
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const v3, 0x7f0201c2

    .line 46
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const v3, 0x7f0201c3

    .line 47
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const v3, 0x7f0201c4

    .line 48
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const v3, 0x7f0201c5

    .line 49
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const v3, 0x7f0201c6

    .line 50
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const v3, 0x7f0201c7

    .line 51
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const v3, 0x7f0201c8

    .line 52
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const v3, 0x7f0201c9

    .line 53
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const v3, 0x7f0201ca

    .line 54
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const v3, 0x7f0201cb

    .line 55
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const v3, 0x7f0201cc

    .line 56
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const v3, 0x7f0201cd

    .line 57
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const v3, 0x7f0201ce

    .line 58
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const v3, 0x7f0201cf

    .line 59
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const v3, 0x7f0201d0

    .line 60
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x19

    const v3, 0x7f0201d1

    .line 61
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    const v3, 0x7f0201d2

    .line 62
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    const v3, 0x7f0201d3

    .line 63
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x1c

    const v3, 0x7f0201d4

    .line 64
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x1d

    const v3, 0x7f0201d5

    .line 65
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x1e

    const v3, 0x7f0201d6

    .line 66
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x1f

    const v3, 0x7f0201d7

    .line 67
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x20

    const v3, 0x7f0201d8

    .line 68
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x21

    const v3, 0x7f0201d9

    .line 69
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x22

    const v3, 0x7f0201da

    .line 70
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x23

    const v3, 0x7f0201db

    .line 71
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x24

    const v3, 0x7f0201dc

    .line 72
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x25

    const v3, 0x7f0201dd

    .line 73
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x26

    const v3, 0x7f0201de

    .line 74
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x27

    const v3, 0x7f0201df

    .line 75
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x28

    const v3, 0x7f0201e0

    .line 76
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x29

    const v3, 0x7f0201e1

    .line 77
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x2a

    const v3, 0x7f0201e2

    .line 78
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x2b

    const v3, 0x7f0201e3

    .line 79
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x2c

    const v3, 0x7f0201e4

    .line 80
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x2d

    const v3, 0x7f0201e5

    .line 81
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x2e

    const v3, 0x7f0201e6

    .line 82
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x2f

    const v3, 0x7f0201e7

    .line 83
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x30

    const v3, 0x7f0201e8

    .line 84
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x31

    const v3, 0x7f0201e9

    .line 85
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x32

    const v3, 0x7f0201ea

    .line 86
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x33

    const v3, 0x7f0201eb

    .line 87
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x34

    const v3, 0x7f0201ec

    .line 88
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x35

    const v3, 0x7f0201ed

    .line 89
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x36

    const v3, 0x7f0201ee

    .line 90
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 34
    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/moments/ui/animation/e;->a:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 114
    const-string/jumbo v0, "http://ton.twimg.com/assets/additional_moments_animations.zip"

    invoke-static {p1, v0}, Lcom/twitter/android/moments/ui/animation/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/android/moments/ui/animation/a;

    move-result-object v0

    invoke-static {p1}, Lcom/twitter/android/moments/ui/animation/d;->a(Landroid/content/Context;)Lcom/twitter/android/moments/ui/animation/d;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/moments/ui/animation/e;-><init>(Lcom/twitter/android/moments/ui/animation/a;Lcom/twitter/android/moments/ui/animation/d;)V

    .line 115
    return-void
.end method

.method constructor <init>(Lcom/twitter/android/moments/ui/animation/a;Lcom/twitter/android/moments/ui/animation/d;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    iput-object p1, p0, Lcom/twitter/android/moments/ui/animation/e;->c:Lcom/twitter/android/moments/ui/animation/a;

    .line 121
    iput-object p2, p0, Lcom/twitter/android/moments/ui/animation/e;->d:Lcom/twitter/android/moments/ui/animation/d;

    .line 122
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/ui/animation/e;)Lcom/twitter/android/moments/ui/animation/a;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/e;->c:Lcom/twitter/android/moments/ui/animation/a;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/twitter/android/moments/ui/animation/e;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/twitter/android/moments/ui/animation/e;->b:Lcom/twitter/android/moments/ui/animation/e;

    if-nez v0, :cond_0

    .line 102
    new-instance v0, Lcom/twitter/android/moments/ui/animation/e;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/ui/animation/e;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/android/moments/ui/animation/e;->b:Lcom/twitter/android/moments/ui/animation/e;

    .line 103
    const-class v0, Lcom/twitter/android/moments/ui/animation/e;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 105
    :cond_0
    sget-object v0, Lcom/twitter/android/moments/ui/animation/e;->b:Lcom/twitter/android/moments/ui/animation/e;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/ui/animation/e;)Lcom/twitter/android/moments/ui/animation/d;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/e;->d:Lcom/twitter/android/moments/ui/animation/d;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 125
    invoke-static {}, Lbsd;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/twitter/android/moments/ui/animation/e;->b()Lrx/c;

    move-result-object v0

    .line 127
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/animation/e$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/animation/e$1;-><init>(Lcom/twitter/android/moments/ui/animation/e;)V

    .line 128
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 135
    :cond_0
    return-void
.end method

.method a(IILbpl;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 179
    if-ne p1, p2, :cond_0

    .line 180
    sget-object v0, Lcom/twitter/android/moments/ui/animation/e;->a:Ljava/util/List;

    invoke-virtual {p3, v0}, Lbpl;->a(Ljava/util/List;)V

    .line 185
    :goto_0
    return-void

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/e;->c:Lcom/twitter/android/moments/ui/animation/a;

    .line 183
    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/animation/a;->b(I)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p3, v0}, Lbpl;->b(Ljava/util/List;)V

    goto :goto_0
.end method

.method public a(Lbpl;)V
    .locals 3

    .prologue
    .line 165
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/e;->c:Lcom/twitter/android/moments/ui/animation/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/animation/a;->c()I

    move-result v0

    .line 166
    if-nez v0, :cond_0

    .line 168
    sget-object v0, Lcom/twitter/android/moments/ui/animation/e;->a:Ljava/util/List;

    invoke-virtual {p1, v0}, Lbpl;->a(Ljava/util/List;)V

    .line 174
    :goto_0
    return-void

    .line 171
    :cond_0
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    .line 172
    invoke-virtual {p0, v1, v0, p1}, Lcom/twitter/android/moments/ui/animation/e;->a(IILbpl;)V

    goto :goto_0
.end method

.method b()Lrx/c;
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 139
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/e;->c:Lcom/twitter/android/moments/ui/animation/a;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/animation/a;->d()Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/ui/animation/e$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/ui/animation/e$2;-><init>(Lcom/twitter/android/moments/ui/animation/e;)V

    .line 140
    invoke-virtual {v0, v1}, Lrx/c;->f(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 139
    return-object v0
.end method
