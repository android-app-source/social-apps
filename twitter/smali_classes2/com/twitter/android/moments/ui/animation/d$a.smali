.class Lcom/twitter/android/moments/ui/animation/d$a;
.super Lcom/twitter/library/service/s;
.source "Twttr"

# interfaces
.implements Lcom/twitter/network/j;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/ui/animation/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/io/File;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0, p1, p2}, Lcom/twitter/library/service/s;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 115
    iput-object p3, p0, Lcom/twitter/android/moments/ui/animation/d$a;->a:Ljava/lang/String;

    .line 116
    iput-object p4, p0, Lcom/twitter/android/moments/ui/animation/d$a;->b:Ljava/io/File;

    .line 117
    return-void
.end method


# virtual methods
.method public a(ILjava/io/InputStream;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    invoke-static {p2}, Lcqc;->b(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 132
    if-eqz v0, :cond_0

    .line 133
    iget-object v1, p0, Lcom/twitter/android/moments/ui/animation/d$a;->b:Ljava/io/File;

    invoke-static {v0, v1}, Lcqc;->a([BLjava/io/File;)Z

    .line 135
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/network/l;)V
    .locals 0

    .prologue
    .line 140
    return-void
.end method

.method protected a_(Lcom/twitter/library/service/u;)V
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/twitter/android/moments/ui/animation/d$a;->p:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/moments/ui/animation/d$a;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/moments/ui/animation/d$a;->a(Landroid/content/Context;Ljava/lang/CharSequence;)Lcom/twitter/library/network/k;

    move-result-object v0

    .line 122
    invoke-virtual {v0, p0}, Lcom/twitter/library/network/k;->a(Lcom/twitter/network/j;)Lcom/twitter/library/network/k;

    move-result-object v0

    const/16 v1, 0x7530

    .line 123
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/k;->a(I)Lcom/twitter/library/network/k;

    move-result-object v0

    .line 124
    invoke-virtual {v0}, Lcom/twitter/library/network/k;->a()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/network/HttpOperation;->c()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    .line 125
    invoke-virtual {p1, v0}, Lcom/twitter/library/service/u;->a(Lcom/twitter/network/HttpOperation;)V

    .line 126
    return-void
.end method
