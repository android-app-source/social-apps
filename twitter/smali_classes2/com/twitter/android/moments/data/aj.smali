.class public Lcom/twitter/android/moments/data/aj;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lauj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lauj",
        "<",
        "Lapb;",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lauj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lauj",
            "<",
            "Lapb;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcbp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcbp",
            "<+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lauj;Lcbp;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lauj",
            "<",
            "Lapb;",
            "Landroid/database/Cursor;",
            ">;",
            "Lcbp",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/twitter/android/moments/data/aj;->a:Lauj;

    .line 24
    iput-object p2, p0, Lcom/twitter/android/moments/data/aj;->b:Lcbp;

    .line 25
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/data/aj;)Lcbp;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/twitter/android/moments/data/aj;->b:Lcbp;

    return-object v0
.end method


# virtual methods
.method public a(Lapb;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lapb;",
            ")",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/moments/data/aj;->a:Lauj;

    invoke-interface {v0, p1}, Lauj;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/data/aj$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/data/aj$1;-><init>(Lcom/twitter/android/moments/data/aj;)V

    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a_(Ljava/lang/Object;)Lrx/c;
    .locals 1

    .prologue
    .line 16
    check-cast p1, Lapb;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/data/aj;->a(Lapb;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/moments/data/aj;->a:Lauj;

    invoke-interface {v0}, Lauj;->close()V

    .line 41
    return-void
.end method
