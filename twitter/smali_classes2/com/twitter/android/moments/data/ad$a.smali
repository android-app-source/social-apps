.class Lcom/twitter/android/moments/data/ad$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/h;


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/data/ad;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/twitter/model/core/Tweet;

.field private final b:Lcom/twitter/library/widget/h;

.field private final c:Lbsb;

.field private final d:Lcqu;

.field private final e:Lrx/f;


# direct methods
.method constructor <init>(Lcom/twitter/library/widget/h;Lcom/twitter/model/core/Tweet;Lbsb;Lcqu;Lrx/f;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/twitter/android/moments/data/ad$a;->b:Lcom/twitter/library/widget/h;

    .line 65
    iput-object p2, p0, Lcom/twitter/android/moments/data/ad$a;->a:Lcom/twitter/model/core/Tweet;

    .line 66
    iput-object p3, p0, Lcom/twitter/android/moments/data/ad$a;->c:Lbsb;

    .line 67
    iput-object p4, p0, Lcom/twitter/android/moments/data/ad$a;->d:Lcqu;

    .line 68
    iput-object p5, p0, Lcom/twitter/android/moments/data/ad$a;->e:Lrx/f;

    .line 69
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/data/ad$a;)Lcom/twitter/library/widget/h;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/android/moments/data/ad$a;->b:Lcom/twitter/library/widget/h;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/data/ad$a;)Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/android/moments/data/ad$a;->a:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/data/ad$a;)Lbsb;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/android/moments/data/ad$a;->c:Lbsb;

    return-object v0
.end method


# virtual methods
.method public a(Z)V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/twitter/android/moments/data/ad$a;->b:Lcom/twitter/library/widget/h;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/twitter/android/moments/data/ad$a;->b:Lcom/twitter/library/widget/h;

    invoke-interface {v0, p1}, Lcom/twitter/library/widget/h;->a(Z)V

    .line 76
    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/moments/data/ad$a;->b:Lcom/twitter/library/widget/h;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/twitter/android/moments/data/ad$a;->b:Lcom/twitter/library/widget/h;

    invoke-interface {v0, p1}, Lcom/twitter/library/widget/h;->b(Z)V

    .line 83
    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/android/moments/data/ad$a;->b:Lcom/twitter/library/widget/h;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/twitter/android/moments/data/ad$a;->b:Lcom/twitter/library/widget/h;

    invoke-interface {v0, p1}, Lcom/twitter/library/widget/h;->c(Z)V

    .line 90
    :cond_0
    return-void
.end method

.method public d(Z)V
    .locals 2

    .prologue
    .line 95
    if-eqz p1, :cond_1

    .line 96
    iget-object v0, p0, Lcom/twitter/android/moments/data/ad$a;->d:Lcqu;

    new-instance v1, Lcom/twitter/android/moments/data/ad$a$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/data/ad$a$2;-><init>(Lcom/twitter/android/moments/data/ad$a;)V

    invoke-virtual {v0, v1}, Lcqu;->a(Ljava/util/concurrent/Callable;)Lrx/a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/data/ad$a;->e:Lrx/f;

    .line 103
    invoke-virtual {v0, v1}, Lrx/a;->a(Lrx/f;)Lrx/a;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/data/ad$a$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/data/ad$a$1;-><init>(Lcom/twitter/android/moments/data/ad$a;)V

    .line 104
    invoke-virtual {v0, v1}, Lrx/a;->b(Lrx/b;)V

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/moments/data/ad$a;->b:Lcom/twitter/library/widget/h;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/twitter/android/moments/data/ad$a;->b:Lcom/twitter/library/widget/h;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/twitter/library/widget/h;->d(Z)V

    goto :goto_0
.end method
