.class public Lcom/twitter/android/moments/data/g;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Lcom/twitter/library/service/b;",
        ":",
        "Lbdr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/client/p;

.field private b:Lcom/twitter/library/service/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/util/object/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/util/collection/k",
            "<",
            "Ljava/lang/String;",
            ">;TR;>;"
        }
    .end annotation
.end field

.field private final d:I


# direct methods
.method public constructor <init>(Lcom/twitter/library/client/p;ILcom/twitter/util/object/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/client/p;",
            "I",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/util/collection/k",
            "<",
            "Ljava/lang/String;",
            ">;TR;>;)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/twitter/android/moments/data/g;->a:Lcom/twitter/library/client/p;

    .line 46
    iput-object p3, p0, Lcom/twitter/android/moments/data/g;->c:Lcom/twitter/util/object/d;

    .line 47
    iput p2, p0, Lcom/twitter/android/moments/data/g;->d:I

    .line 48
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/v;Lbsb;Lcom/twitter/library/client/p;Lbdn;)Lcom/twitter/android/moments/data/g;
    .locals 3

    .prologue
    .line 32
    new-instance v0, Lcom/twitter/android/moments/data/g;

    const/16 v1, 0x224d

    new-instance v2, Lcom/twitter/android/moments/data/g$1;

    invoke-direct {v2, p0, p1, p2, p4}, Lcom/twitter/android/moments/data/g$1;-><init>(Landroid/content/Context;Lcom/twitter/library/client/v;Lbsb;Lbdn;)V

    invoke-direct {v0, p3, v1, v2}, Lcom/twitter/android/moments/data/g;-><init>(Lcom/twitter/library/client/p;ILcom/twitter/util/object/d;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/s;)Z
    .locals 3

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/android/moments/data/g;->c:Lcom/twitter/util/object/d;

    invoke-static {}, Lcom/twitter/util/collection/k;->a()Lcom/twitter/util/collection/k;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/b;

    iput-object v0, p0, Lcom/twitter/android/moments/data/g;->b:Lcom/twitter/library/service/b;

    .line 52
    iget-object v0, p0, Lcom/twitter/android/moments/data/g;->a:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/android/moments/data/g;->b:Lcom/twitter/library/service/b;

    iget v2, p0, Lcom/twitter/android/moments/data/g;->d:I

    invoke-virtual {v0, v1, v2, p1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;ILcom/twitter/library/client/s;)Z

    move-result v0

    return v0
.end method

.method public b(Lcom/twitter/library/client/s;)V
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/android/moments/data/g;->b:Lcom/twitter/library/service/b;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/twitter/android/moments/data/g;->b:Lcom/twitter/library/service/b;

    check-cast v0, Lbdr;

    invoke-interface {v0}, Lbdr;->g()Ljava/lang/String;

    move-result-object v0

    .line 58
    iget-object v1, p0, Lcom/twitter/android/moments/data/g;->c:Lcom/twitter/util/object/d;

    invoke-static {v0}, Lcom/twitter/util/collection/k;->b(Ljava/lang/Object;)Lcom/twitter/util/collection/k;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/b;

    iput-object v0, p0, Lcom/twitter/android/moments/data/g;->b:Lcom/twitter/library/service/b;

    .line 59
    iget-object v0, p0, Lcom/twitter/android/moments/data/g;->a:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/android/moments/data/g;->b:Lcom/twitter/library/service/b;

    iget v2, p0, Lcom/twitter/android/moments/data/g;->d:I

    invoke-virtual {v0, v1, v2, p1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;ILcom/twitter/library/client/s;)Z

    .line 61
    :cond_0
    return-void
.end method
