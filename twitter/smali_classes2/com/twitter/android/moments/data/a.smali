.class public Lcom/twitter/android/moments/data/a;
.super Lcbp;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbp",
        "<",
        "Lcom/twitter/util/collection/k",
        "<",
        "Lcom/twitter/model/moments/viewmodels/b;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/data/t;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lcom/twitter/android/moments/data/t;

    invoke-direct {v0}, Lcom/twitter/android/moments/data/t;-><init>()V

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/data/a;-><init>(Lcom/twitter/android/moments/data/t;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Lcom/twitter/android/moments/data/t;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcbp;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/twitter/android/moments/data/a;->a:Lcom/twitter/android/moments/data/t;

    .line 48
    return-void
.end method

.method private a(Landroid/database/Cursor;Ljava/lang/String;)Lcfn;
    .locals 2

    .prologue
    .line 156
    const-string/jumbo v0, "SPORTS"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    const-string/jumbo v0, "moment_sports_events_value"

    .line 158
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcfn;->a:Lcom/twitter/util/serialization/l;

    .line 157
    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfn;

    .line 161
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/database/Cursor;Lcom/twitter/model/moments/Moment;)Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 52
    const-string/jumbo v0, "moments_pages_capsule_page_data"

    .line 53
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v2, Lceh;->a:Lcom/twitter/util/serialization/l;

    .line 52
    invoke-static {v0, v2}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lceh;

    .line 56
    const-string/jumbo v2, "tweet_flags"

    .line 57
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 56
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 58
    const-string/jumbo v3, "user_friendship"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 62
    if-nez v0, :cond_0

    move-object v0, v1

    .line 88
    :goto_0
    return-object v0

    .line 66
    :cond_0
    iget-object v3, v0, Lceh;->d:Lcom/twitter/model/moments/r;

    .line 67
    iget-object v4, v0, Lceh;->c:Lcom/twitter/model/moments/MomentPageDisplayMode;

    .line 69
    and-int/lit16 v2, v2, 0x100

    if-gtz v2, :cond_1

    .line 70
    invoke-static {v5}, Lcom/twitter/model/core/g;->e(I)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v5}, Lcom/twitter/model/core/g;->f(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 74
    :cond_1
    sget-object v0, Lcom/twitter/model/moments/MomentPageDisplayMode;->a:Lcom/twitter/model/moments/MomentPageDisplayMode;

    if-eq v4, v0, :cond_2

    sget-object v0, Lcom/twitter/model/moments/MomentPageDisplayMode;->c:Lcom/twitter/model/moments/MomentPageDisplayMode;

    if-ne v4, v0, :cond_3

    .line 75
    :cond_2
    new-instance v0, Lcom/twitter/model/moments/viewmodels/j$b;

    invoke-direct {v0}, Lcom/twitter/model/moments/viewmodels/j$b;-><init>()V

    .line 76
    invoke-virtual {v0, p2}, Lcom/twitter/model/moments/viewmodels/j$b;->a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/j$b;

    .line 77
    invoke-virtual {v0, v3}, Lcom/twitter/model/moments/viewmodels/j$b;->a(Lcom/twitter/model/moments/r;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/j$b;

    .line 78
    invoke-virtual {v0, v4}, Lcom/twitter/model/moments/viewmodels/j$b;->a(Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/j$b;

    .line 79
    invoke-static {}, Lbsd;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/viewmodels/j$b;->a(Ljava/lang/String;)Lcom/twitter/model/moments/viewmodels/j$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/j$b;

    .line 80
    invoke-static {}, Lbsd;->j()Lcom/twitter/util/math/Size;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/viewmodels/j$b;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/model/moments/viewmodels/j$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/j$b;

    .line 81
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/j$b;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentPage;

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 83
    goto :goto_0

    .line 86
    :cond_4
    sget-object v1, Lbtd;->a:Lbtd;

    invoke-virtual {v1, p1}, Lbtd;->a(Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet;

    move-result-object v2

    .line 87
    iget-object v5, v0, Lceh;->b:Lceo;

    .line 88
    iget-object v0, p0, Lcom/twitter/android/moments/data/a;->a:Lcom/twitter/android/moments/data/t;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/moments/data/t;->a(Lcom/twitter/model/moments/Moment;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/moments/r;Lcom/twitter/model/moments/MomentPageDisplayMode;Lceo;)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Landroid/database/Cursor;)Lcom/twitter/model/moments/Moment;
    .locals 24

    .prologue
    .line 93
    const-string/jumbo v2, "moments_pages_moment_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 95
    const-string/jumbo v2, "moments_title"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 96
    const-string/jumbo v2, "moments_can_subscribe"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    move v6, v2

    .line 98
    :goto_0
    const-string/jumbo v2, "moments_is_live"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    const/4 v2, 0x1

    move v7, v2

    .line 100
    :goto_1
    const-string/jumbo v2, "moments_subcategory_string"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 102
    const-string/jumbo v2, "moments_subcategory_favicon_url"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 104
    const-string/jumbo v2, "moments_time_string"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 106
    const-string/jumbo v2, "moments_duration_string"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 108
    const-string/jumbo v2, "moments_is_subscribed"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    const/4 v2, 0x1

    move v8, v2

    .line 110
    :goto_2
    const-string/jumbo v2, "moments_description"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 112
    const-string/jumbo v2, "moments_moment_url"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 114
    const-string/jumbo v2, "moments_num_subscribers"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 116
    const-string/jumbo v2, "moments_is_sensitive"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    const/4 v2, 0x1

    move v9, v2

    .line 118
    :goto_3
    const-string/jumbo v2, "moments_author_info"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    sget-object v3, Lcom/twitter/model/moments/a;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v2, v3}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/a;

    .line 120
    const-string/jumbo v3, "moments_promoted_content"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    sget-object v4, Lcgi;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v3, v4}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcgi;

    .line 122
    const-string/jumbo v4, "moments_event_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 123
    const-string/jumbo v5, "moments_event_type"

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 124
    new-instance v10, Lcom/twitter/model/moments/g$a;

    invoke-direct {v10}, Lcom/twitter/model/moments/g$a;-><init>()V

    invoke-virtual {v10, v4}, Lcom/twitter/model/moments/g$a;->a(Ljava/lang/String;)Lcom/twitter/model/moments/g$a;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/twitter/model/moments/g$a;->b(Ljava/lang/String;)Lcom/twitter/model/moments/g$a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/model/moments/g$a;->r()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/model/moments/g;

    .line 125
    const-string/jumbo v5, "moments_curation_metadata"

    .line 126
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    sget-object v10, Lcom/twitter/model/moments/f;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v5, v10}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/twitter/model/moments/f;

    .line 128
    const-string/jumbo v10, "moments_is_liked"

    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v10, v0, :cond_4

    const/4 v10, 0x1

    .line 129
    :goto_4
    const-string/jumbo v21, "moments_total_likes"

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 131
    new-instance v21, Lcom/twitter/model/moments/Moment$a;

    invoke-direct/range {v21 .. v21}, Lcom/twitter/model/moments/Moment$a;-><init>()V

    .line 132
    move-object/from16 v0, v21

    invoke-virtual {v0, v12, v13}, Lcom/twitter/model/moments/Moment$a;->a(J)Lcom/twitter/model/moments/Moment$a;

    move-result-object v12

    .line 133
    invoke-virtual {v12, v11}, Lcom/twitter/model/moments/Moment$a;->a(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v11

    .line 134
    invoke-virtual {v11, v6}, Lcom/twitter/model/moments/Moment$a;->a(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v6

    .line 135
    invoke-virtual {v6, v7}, Lcom/twitter/model/moments/Moment$a;->b(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v6

    .line 136
    invoke-virtual {v6, v9}, Lcom/twitter/model/moments/Moment$a;->c(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v6

    .line 137
    invoke-virtual {v6, v14}, Lcom/twitter/model/moments/Moment$a;->b(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v6

    .line 138
    invoke-virtual {v6, v15}, Lcom/twitter/model/moments/Moment$a;->c(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v6

    .line 139
    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Lcom/twitter/model/moments/Moment$a;->d(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v6

    .line 140
    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Lcom/twitter/model/moments/Moment$a;->e(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v6

    .line 141
    invoke-virtual {v6, v8}, Lcom/twitter/model/moments/Moment$a;->d(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v6

    .line 142
    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Lcom/twitter/model/moments/Moment$a;->f(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v6

    .line 143
    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Lcom/twitter/model/moments/Moment$a;->g(Ljava/lang/String;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v6

    .line 144
    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/twitter/model/moments/Moment$a;->a(I)Lcom/twitter/model/moments/Moment$a;

    move-result-object v6

    .line 145
    invoke-virtual {v6, v2}, Lcom/twitter/model/moments/Moment$a;->a(Lcom/twitter/model/moments/a;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v2

    .line 146
    invoke-virtual {v2, v3}, Lcom/twitter/model/moments/Moment$a;->a(Lcgi;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v2

    .line 147
    invoke-virtual {v2, v4}, Lcom/twitter/model/moments/Moment$a;->a(Lcom/twitter/model/moments/g;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v2

    .line 148
    invoke-virtual {v2, v5}, Lcom/twitter/model/moments/Moment$a;->a(Lcom/twitter/model/moments/f;)Lcom/twitter/model/moments/Moment$a;

    move-result-object v2

    .line 149
    invoke-virtual {v2, v10}, Lcom/twitter/model/moments/Moment$a;->e(Z)Lcom/twitter/model/moments/Moment$a;

    move-result-object v2

    .line 150
    move-wide/from16 v0, v22

    invoke-virtual {v2, v0, v1}, Lcom/twitter/model/moments/Moment$a;->c(J)Lcom/twitter/model/moments/Moment$a;

    move-result-object v2

    .line 151
    invoke-virtual {v2}, Lcom/twitter/model/moments/Moment$a;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/Moment;

    .line 131
    return-object v2

    .line 96
    :cond_0
    const/4 v2, 0x0

    move v6, v2

    goto/16 :goto_0

    .line 98
    :cond_1
    const/4 v2, 0x0

    move v7, v2

    goto/16 :goto_1

    .line 108
    :cond_2
    const/4 v2, 0x0

    move v8, v2

    goto/16 :goto_2

    .line 116
    :cond_3
    const/4 v2, 0x0

    move v9, v2

    goto/16 :goto_3

    .line 128
    :cond_4
    const/4 v10, 0x0

    goto/16 :goto_4
.end method

.method private c(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 213
    const-string/jumbo v1, "moments_pages_meta_type"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Lcom/twitter/util/collection/k;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 167
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v4

    .line 168
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 169
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/data/a;->b(Landroid/database/Cursor;)Lcom/twitter/model/moments/Moment;

    move-result-object v5

    .line 173
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v6

    .line 174
    iget-object v0, v5, Lcom/twitter/model/moments/Moment;->q:Lcom/twitter/model/moments/g;

    if-eqz v0, :cond_5

    .line 175
    iget-object v0, v5, Lcom/twitter/model/moments/Moment;->q:Lcom/twitter/model/moments/g;

    iget-object v0, v0, Lcom/twitter/model/moments/g;->c:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/moments/data/a;->a(Landroid/database/Cursor;Ljava/lang/String;)Lcfn;

    move-result-object v0

    move-object v3, v0

    move-object v2, v1

    move-object v0, v1

    .line 178
    :goto_0
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/data/a;->c(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 179
    invoke-direct {p0, p1, v5}, Lcom/twitter/android/moments/data/a;->a(Landroid/database/Cursor;Lcom/twitter/model/moments/Moment;)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v1

    .line 180
    if-eqz v1, :cond_0

    .line 181
    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->h()Lcom/twitter/model/moments/MomentPageDisplayMode;

    move-result-object v7

    sget-object v8, Lcom/twitter/model/moments/MomentPageDisplayMode;->a:Lcom/twitter/model/moments/MomentPageDisplayMode;

    if-ne v7, v8, :cond_1

    move-object v2, v1

    .line 188
    :goto_1
    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->j()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v7, "moments_pages_last_read_timestamp"

    .line 190
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 189
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 188
    invoke-virtual {v6, v1, v7}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    :cond_0
    move-object v1, v0

    .line 193
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_4

    .line 194
    invoke-virtual {v4}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 195
    new-instance v4, Lcom/twitter/model/moments/viewmodels/a$a;

    invoke-direct {v4}, Lcom/twitter/model/moments/viewmodels/a$a;-><init>()V

    .line 196
    invoke-virtual {v4, v5}, Lcom/twitter/model/moments/viewmodels/a$a;->a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/model/moments/viewmodels/a$a;

    move-result-object v4

    .line 197
    invoke-virtual {v4, v2}, Lcom/twitter/model/moments/viewmodels/a$a;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/model/moments/viewmodels/a$a;

    move-result-object v2

    .line 198
    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/a$a;->a(Ljava/util/List;)Lcom/twitter/model/moments/viewmodels/a$a;

    move-result-object v0

    .line 199
    invoke-virtual {v0, v1}, Lcom/twitter/model/moments/viewmodels/a$a;->b(Lcom/twitter/model/moments/viewmodels/MomentPage;)Lcom/twitter/model/moments/viewmodels/a$a;

    move-result-object v0

    .line 200
    invoke-virtual {v0, v3}, Lcom/twitter/model/moments/viewmodels/a$a;->a(Lcfn;)Lcom/twitter/model/moments/viewmodels/a$a;

    move-result-object v0

    .line 201
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/a$a;->a()Lcom/twitter/model/moments/viewmodels/a;

    move-result-object v1

    .line 202
    new-instance v2, Lcom/twitter/model/moments/viewmodels/b;

    invoke-virtual {v6}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-direct {v2, v1, v0}, Lcom/twitter/model/moments/viewmodels/b;-><init>(Lcom/twitter/model/moments/viewmodels/a;Ljava/util/Map;)V

    invoke-static {v2}, Lcom/twitter/util/collection/k;->a(Ljava/lang/Object;)Lcom/twitter/util/collection/k;

    move-result-object v0

    .line 204
    :goto_2
    return-object v0

    .line 183
    :cond_1
    invoke-virtual {v1}, Lcom/twitter/model/moments/viewmodels/MomentPage;->h()Lcom/twitter/model/moments/MomentPageDisplayMode;

    move-result-object v7

    sget-object v8, Lcom/twitter/model/moments/MomentPageDisplayMode;->c:Lcom/twitter/model/moments/MomentPageDisplayMode;

    if-ne v7, v8, :cond_2

    move-object v0, v1

    .line 184
    goto :goto_1

    .line 186
    :cond_2
    invoke-virtual {v4, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_1

    .line 204
    :cond_3
    invoke-static {}, Lcom/twitter/util/collection/k;->a()Lcom/twitter/util/collection/k;

    move-result-object v0

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_0

    :cond_5
    move-object v3, v1

    move-object v0, v1

    move-object v2, v1

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/data/a;->a(Landroid/database/Cursor;)Lcom/twitter/util/collection/k;

    move-result-object v0

    return-object v0
.end method
