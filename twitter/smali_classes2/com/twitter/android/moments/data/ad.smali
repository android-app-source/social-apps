.class public Lcom/twitter/android/moments/data/ad;
.super Lcom/twitter/android/ck;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/data/ad$a;
    }
.end annotation


# instance fields
.field private final a:Lbsb;

.field private final b:Lcqu;

.field private final i:Lrx/f;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lbsb;Lcqu;Lrx/f;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/ck;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 38
    iput-object p3, p0, Lcom/twitter/android/moments/data/ad;->a:Lbsb;

    .line 39
    iput-object p4, p0, Lcom/twitter/android/moments/data/ad;->b:Lcqu;

    .line 40
    iput-object p5, p0, Lcom/twitter/android/moments/data/ad;->i:Lrx/f;

    .line 41
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;Lcom/twitter/library/widget/h;)V
    .locals 7

    .prologue
    .line 47
    new-instance v0, Lcom/twitter/android/moments/data/ad$a;

    iget-object v3, p0, Lcom/twitter/android/moments/data/ad;->a:Lbsb;

    iget-object v4, p0, Lcom/twitter/android/moments/data/ad;->b:Lcqu;

    iget-object v5, p0, Lcom/twitter/android/moments/data/ad;->i:Lrx/f;

    move-object v1, p5

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/moments/data/ad$a;-><init>(Lcom/twitter/library/widget/h;Lcom/twitter/model/core/Tweet;Lbsb;Lcqu;Lrx/f;)V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, v0

    invoke-super/range {v1 .. v6}, Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;Lcom/twitter/library/widget/h;)V

    .line 50
    return-void
.end method
