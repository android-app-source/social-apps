.class public Lcom/twitter/android/moments/data/al;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lauj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lauj",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Ljava/util/List",
        "<",
        "Lcfn;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lbry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbry",
            "<",
            "Ljava/lang/String;",
            "Lcfn;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lbry;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbry",
            "<",
            "Ljava/lang/String;",
            "Lcfn;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/twitter/android/moments/data/al;->a:Lbry;

    .line 27
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/data/al;)Lbry;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/twitter/android/moments/data/al;->a:Lbry;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/util/List;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lrx/c",
            "<",
            "Ljava/util/List",
            "<",
            "Lcfn;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 37
    invoke-static {p1}, Lrx/c;->a(Ljava/lang/Iterable;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/data/al$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/data/al$2;-><init>(Lcom/twitter/android/moments/data/al;)V

    .line 38
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/data/al$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/data/al$1;-><init>(Lcom/twitter/android/moments/data/al;)V

    .line 44
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 50
    invoke-static {}, Lcre;->d()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lrx/c;->q()Lrx/c;

    move-result-object v0

    .line 52
    invoke-static {}, Lcre;->b()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 37
    return-object v0
.end method

.method public synthetic a_(Ljava/lang/Object;)Lrx/c;
    .locals 1

    .prologue
    .line 22
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/data/al;->a(Ljava/util/List;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    return-void
.end method
