.class public Lcom/twitter/android/moments/data/an;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/data/am;


# instance fields
.field private final a:Lbds;

.field private final b:Lcom/twitter/android/moments/data/al;

.field private final c:Lcom/twitter/android/moments/data/ak;

.field private final d:Lcom/twitter/android/moments/data/ag;

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcfn;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/data/ag;Lbds;Lcom/twitter/android/moments/data/al;Lcom/twitter/android/moments/data/ak;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/moments/data/an;->e:Ljava/util/Map;

    .line 74
    iput-object p1, p0, Lcom/twitter/android/moments/data/an;->d:Lcom/twitter/android/moments/data/ag;

    .line 75
    iput-object p2, p0, Lcom/twitter/android/moments/data/an;->a:Lbds;

    .line 76
    iput-object p3, p0, Lcom/twitter/android/moments/data/an;->b:Lcom/twitter/android/moments/data/al;

    .line 77
    iput-object p4, p0, Lcom/twitter/android/moments/data/an;->c:Lcom/twitter/android/moments/data/ak;

    .line 78
    return-void
.end method

.method public static a(Lcom/twitter/android/moments/data/ag;Lbds;Lcom/twitter/library/provider/t;)Lcom/twitter/android/moments/data/an;
    .locals 5

    .prologue
    .line 47
    invoke-virtual {p2}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v1

    .line 48
    new-instance v2, Lbrx;

    sget-object v3, Lcfn;->a:Lcom/twitter/util/serialization/l;

    const-class v0, Laxx;

    .line 50
    invoke-interface {v1, v0}, Lcom/twitter/database/model/i;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    check-cast v0, Laxx;

    invoke-interface {v0}, Laxx;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    const-class v4, Laxx$c;

    .line 51
    invoke-interface {v1, v4}, Lcom/twitter/database/model/i;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v1

    invoke-direct {v2, v3, v0, v1}, Lbrx;-><init>(Lcom/twitter/util/serialization/l;Lcom/twitter/database/model/l;Lcom/twitter/database/model/m;)V

    .line 52
    new-instance v0, Lcom/twitter/android/moments/data/an;

    new-instance v1, Lcom/twitter/android/moments/data/al;

    invoke-direct {v1, v2}, Lcom/twitter/android/moments/data/al;-><init>(Lbry;)V

    new-instance v3, Lcom/twitter/android/moments/data/ak;

    invoke-direct {v3, v2}, Lcom/twitter/android/moments/data/ak;-><init>(Lbry;)V

    invoke-direct {v0, p0, p1, v1, v3}, Lcom/twitter/android/moments/data/an;-><init>(Lcom/twitter/android/moments/data/ag;Lbds;Lcom/twitter/android/moments/data/al;Lcom/twitter/android/moments/data/ak;)V

    return-object v0
.end method

.method private a()Lrx/functions/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/b",
            "<",
            "Ljava/util/List",
            "<",
            "Lcfn;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 166
    new-instance v0, Lcom/twitter/android/moments/data/an$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/data/an$1;-><init>(Lcom/twitter/android/moments/data/an;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/data/an;)Lrx/functions/b;
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/twitter/android/moments/data/an;->c()Lrx/functions/b;

    move-result-object v0

    return-object v0
.end method

.method private b()Lrx/functions/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/b",
            "<",
            "Ljava/util/List",
            "<",
            "Lcfn;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 181
    new-instance v0, Lcom/twitter/android/moments/data/an$2;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/data/an$2;-><init>(Lcom/twitter/android/moments/data/an;)V

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/data/an;)Lrx/functions/b;
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/twitter/android/moments/data/an;->b()Lrx/functions/b;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/data/an;)Lcom/twitter/android/moments/data/ak;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/moments/data/an;->c:Lcom/twitter/android/moments/data/ak;

    return-object v0
.end method

.method private c()Lrx/functions/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/b",
            "<",
            "Ljava/util/List",
            "<",
            "Lcfn;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 197
    new-instance v0, Lcom/twitter/android/moments/data/an$3;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/data/an$3;-><init>(Lcom/twitter/android/moments/data/an;)V

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/moments/data/an;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/moments/data/an;->e:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcfn;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/twitter/android/moments/data/an;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfn;

    return-object v0
.end method

.method public a(Ljava/util/List;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lrx/c",
            "<",
            "Ljava/util/List",
            "<",
            "Lcfn;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 136
    iget-object v0, p0, Lcom/twitter/android/moments/data/an;->d:Lcom/twitter/android/moments/data/ag;

    .line 137
    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/data/ag;->a(Ljava/util/List;)Lrx/c;

    move-result-object v0

    .line 138
    invoke-direct {p0}, Lcom/twitter/android/moments/data/an;->a()Lrx/functions/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/functions/b;)Lrx/c;

    move-result-object v0

    .line 136
    return-object v0
.end method

.method public b(Ljava/lang/String;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/c",
            "<",
            "Lcfn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    invoke-static {p1}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/moments/data/an;->a(Ljava/util/List;)Lrx/c;

    move-result-object v0

    .line 121
    invoke-static {}, Lcre;->b()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 122
    invoke-static {}, Lcre;->c()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 120
    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 223
    iget-object v0, p0, Lcom/twitter/android/moments/data/an;->d:Lcom/twitter/android/moments/data/ag;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/ag;->close()V

    .line 224
    iget-object v0, p0, Lcom/twitter/android/moments/data/an;->a:Lbds;

    invoke-virtual {v0}, Lbds;->close()V

    .line 225
    iget-object v0, p0, Lcom/twitter/android/moments/data/an;->b:Lcom/twitter/android/moments/data/al;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/al;->close()V

    .line 226
    return-void
.end method
