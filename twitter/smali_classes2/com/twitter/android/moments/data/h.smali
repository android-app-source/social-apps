.class public Lcom/twitter/android/moments/data/h;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lbrd;


# direct methods
.method public constructor <init>(Lbrd;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/twitter/android/moments/data/h;->a:Lbrd;

    .line 23
    return-void
.end method

.method private d(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/livevideo/b;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/android/moments/data/h;->a:Lbrd;

    invoke-virtual {v0, p1}, Lbrd;->a(Lcom/twitter/model/core/Tweet;)Lbrc;

    move-result-object v0

    invoke-virtual {v0}, Lbrc;->p()Lcom/twitter/model/livevideo/b;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/Tweet;)Lbrc;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/android/moments/data/h;->a:Lbrd;

    invoke-virtual {v0, p1}, Lbrd;->a(Lcom/twitter/model/core/Tweet;)Lbrc;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/twitter/model/core/Tweet;)Z
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/data/h;->d(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/livevideo/b;

    move-result-object v0

    .line 32
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/twitter/model/livevideo/b;->i:Lcom/twitter/model/livevideo/a;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/twitter/model/livevideo/b;->i:Lcom/twitter/model/livevideo/a;

    iget-object v0, v0, Lcom/twitter/model/livevideo/a;->d:Lcom/twitter/model/livevideo/BroadcastState;

    sget-object v1, Lcom/twitter/model/livevideo/BroadcastState;->b:Lcom/twitter/model/livevideo/BroadcastState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Lcom/twitter/model/core/Tweet;)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/data/h;->d(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/livevideo/b;

    move-result-object v0

    .line 39
    if-eqz v0, :cond_0

    iget-wide v0, v0, Lcom/twitter/model/livevideo/b;->d:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
