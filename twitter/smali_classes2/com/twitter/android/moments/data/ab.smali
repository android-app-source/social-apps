.class public Lcom/twitter/android/moments/data/ab;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/moments/data/ac;

.field private final b:Lcom/twitter/android/moments/ui/guide/l$b;

.field private final c:Lzp$a;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/data/ac;Lcom/twitter/android/moments/ui/guide/l$b;Lzp$a;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/twitter/android/moments/data/ab;->a:Lcom/twitter/android/moments/data/ac;

    .line 23
    iput-object p2, p0, Lcom/twitter/android/moments/data/ab;->b:Lcom/twitter/android/moments/ui/guide/l$b;

    .line 24
    iput-object p3, p0, Lcom/twitter/android/moments/data/ab;->c:Lzp$a;

    .line 25
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/data/ab;)Lzp$a;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/twitter/android/moments/data/ab;->c:Lzp$a;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/data/ab;)Lcom/twitter/android/moments/ui/guide/l$b;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/twitter/android/moments/data/ab;->b:Lcom/twitter/android/moments/ui/guide/l$b;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/android/moments/viewmodels/MomentModule;)V
    .locals 5

    .prologue
    .line 28
    invoke-virtual {p1}, Lcom/twitter/android/moments/viewmodels/MomentModule;->f()Lceg;

    move-result-object v0

    .line 29
    if-eqz v0, :cond_0

    .line 30
    iget-object v1, p0, Lcom/twitter/android/moments/data/ab;->a:Lcom/twitter/android/moments/data/ac;

    invoke-virtual {v1}, Lcom/twitter/android/moments/data/ac;->a()V

    .line 31
    iget-object v1, v0, Lceg;->d:Lcom/twitter/model/moments/m;

    .line 32
    iget-object v2, p0, Lcom/twitter/android/moments/data/ab;->a:Lcom/twitter/android/moments/data/ac;

    iget-object v3, v1, Lcom/twitter/model/moments/m;->b:Ljava/lang/String;

    iget v4, v1, Lcom/twitter/model/moments/m;->c:I

    iget v1, v1, Lcom/twitter/model/moments/m;->d:I

    invoke-virtual {v2, v3, v4, v1}, Lcom/twitter/android/moments/data/ac;->a(Ljava/lang/String;II)V

    .line 33
    iget-object v1, p0, Lcom/twitter/android/moments/data/ab;->a:Lcom/twitter/android/moments/data/ac;

    iget-object v0, v0, Lceg;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/data/ac;->a(Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/twitter/android/moments/data/ab;->a:Lcom/twitter/android/moments/data/ac;

    new-instance v1, Lcom/twitter/android/moments/data/ab$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/data/ab$1;-><init>(Lcom/twitter/android/moments/data/ab;Lcom/twitter/android/moments/viewmodels/MomentModule;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/data/ac;->a(Landroid/view/View$OnClickListener;)V

    .line 44
    :goto_0
    return-void

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/data/ab;->a:Lcom/twitter/android/moments/data/ac;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/ac;->b()V

    goto :goto_0
.end method
