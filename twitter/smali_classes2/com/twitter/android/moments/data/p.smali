.class public Lcom/twitter/android/moments/data/p;
.super Lcbp;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbp",
        "<",
        "Lcom/twitter/android/moments/viewmodels/MomentModule;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcbp;-><init>()V

    return-void
.end method

.method private static a(Landroid/database/Cursor;Lcom/twitter/model/core/Tweet;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 94
    const-string/jumbo v0, "moments_guide_media_id"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 95
    const-string/jumbo v2, "moments_guide_media_url"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 97
    invoke-static {p1, v0, v1, v2}, Lcom/twitter/android/moments/data/o;->a(Lcom/twitter/model/core/Tweet;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Lcom/twitter/android/moments/viewmodels/MomentModule;
    .locals 11

    .prologue
    .line 32
    invoke-static {p1}, Lbsb;->b(Landroid/database/Cursor;)Lcom/twitter/model/moments/Moment;

    move-result-object v5

    .line 34
    invoke-static {p1}, Lbsb;->a(Landroid/database/Cursor;)Lcfn;

    move-result-object v6

    .line 35
    const-string/jumbo v0, "moments_guide_media_size"

    .line 36
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/util/math/Size;->a:Lcom/twitter/util/serialization/l;

    .line 35
    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/math/Size;

    .line 39
    sget-object v1, Lbtd;->a:Lbtd;

    invoke-virtual {v1, p1}, Lbtd;->a(Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet;

    move-result-object v7

    .line 40
    invoke-static {p1, v7}, Lcom/twitter/android/moments/data/p;->a(Landroid/database/Cursor;Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v8

    .line 41
    const-string/jumbo v1, "moments_guide_crop_data"

    .line 44
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 43
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    sget-object v2, Lcom/twitter/model/moments/e;->a:Lcom/twitter/util/serialization/l;

    .line 42
    invoke-static {v1, v2}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/moments/e;->b:Lcom/twitter/model/moments/e;

    .line 41
    invoke-static {v1, v2}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/moments/e;

    .line 46
    const-string/jumbo v2, "moments_guide_display_type"

    .line 49
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    const-class v3, Lcom/twitter/model/moments/DisplayStyle;

    .line 50
    invoke-static {v3}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v3

    .line 48
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/twitter/model/moments/DisplayStyle;->a:Lcom/twitter/model/moments/DisplayStyle;

    .line 47
    invoke-static {v2, v3}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/DisplayStyle;

    .line 51
    const-string/jumbo v3, "moments_guide_context_string"

    .line 52
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 53
    const-string/jumbo v3, "moments_guide_context_scribe_info"

    .line 55
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    sget-object v4, Lcom/twitter/model/moments/u;->a:Lcom/twitter/util/serialization/l;

    .line 54
    invoke-static {v3, v4}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/model/moments/u;

    .line 57
    const-string/jumbo v4, "moments_guide_cta"

    .line 59
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    sget-object v10, Lceg;->a:Lcom/twitter/util/serialization/l;

    .line 58
    invoke-static {v4, v10}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lceg;

    .line 62
    invoke-static {v7}, Lcom/twitter/library/av/playback/ab;->b(Lcom/twitter/model/core/Tweet;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 63
    new-instance v8, Lcom/twitter/android/moments/viewmodels/ah$a;

    invoke-direct {v8}, Lcom/twitter/android/moments/viewmodels/ah$a;-><init>()V

    .line 64
    invoke-virtual {v8, v5}, Lcom/twitter/android/moments/viewmodels/ah$a;->a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/android/moments/viewmodels/MomentModule$a;

    move-result-object v5

    check-cast v5, Lcom/twitter/android/moments/viewmodels/ah$a;

    .line 65
    invoke-virtual {v5, v6}, Lcom/twitter/android/moments/viewmodels/ah$a;->a(Lcfn;)Lcom/twitter/android/moments/viewmodels/MomentModule$a;

    move-result-object v5

    check-cast v5, Lcom/twitter/android/moments/viewmodels/ah$a;

    .line 66
    invoke-virtual {v5, v0}, Lcom/twitter/android/moments/viewmodels/ah$a;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/android/moments/viewmodels/ah$a;

    move-result-object v0

    .line 67
    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/viewmodels/ah$a;->a(Lcom/twitter/model/moments/e;)Lcom/twitter/android/moments/viewmodels/ah$a;

    move-result-object v0

    .line 68
    invoke-virtual {v0, v2}, Lcom/twitter/android/moments/viewmodels/ah$a;->a(Lcom/twitter/model/moments/DisplayStyle;)Lcom/twitter/android/moments/viewmodels/MomentModule$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/ah$a;

    .line 69
    invoke-virtual {v0, v9}, Lcom/twitter/android/moments/viewmodels/ah$a;->b(Ljava/lang/String;)Lcom/twitter/android/moments/viewmodels/MomentModule$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/ah$a;

    .line 70
    invoke-virtual {v0, v7}, Lcom/twitter/android/moments/viewmodels/ah$a;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/moments/viewmodels/MomentModule$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/ah$a;

    .line 71
    invoke-virtual {v0, v3}, Lcom/twitter/android/moments/viewmodels/ah$a;->a(Lcom/twitter/model/moments/u;)Lcom/twitter/android/moments/viewmodels/MomentModule$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/ah$a;

    .line 72
    invoke-virtual {v0, v4}, Lcom/twitter/android/moments/viewmodels/ah$a;->a(Lceg;)Lcom/twitter/android/moments/viewmodels/MomentModule$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/ah$a;

    .line 73
    invoke-virtual {v0}, Lcom/twitter/android/moments/viewmodels/ah$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/MomentModule;

    .line 75
    :goto_0
    return-object v0

    :cond_0
    new-instance v10, Lcom/twitter/android/moments/viewmodels/i$a;

    invoke-direct {v10}, Lcom/twitter/android/moments/viewmodels/i$a;-><init>()V

    .line 76
    invoke-virtual {v10, v5}, Lcom/twitter/android/moments/viewmodels/i$a;->a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/android/moments/viewmodels/MomentModule$a;

    move-result-object v5

    check-cast v5, Lcom/twitter/android/moments/viewmodels/i$a;

    .line 77
    invoke-virtual {v5, v6}, Lcom/twitter/android/moments/viewmodels/i$a;->a(Lcfn;)Lcom/twitter/android/moments/viewmodels/MomentModule$a;

    move-result-object v5

    check-cast v5, Lcom/twitter/android/moments/viewmodels/i$a;

    .line 78
    invoke-virtual {v5, v2}, Lcom/twitter/android/moments/viewmodels/i$a;->a(Lcom/twitter/model/moments/DisplayStyle;)Lcom/twitter/android/moments/viewmodels/MomentModule$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/moments/viewmodels/i$a;

    .line 79
    invoke-virtual {v2, v8}, Lcom/twitter/android/moments/viewmodels/i$a;->a(Ljava/lang/String;)Lcom/twitter/android/moments/viewmodels/i$a;

    move-result-object v2

    .line 80
    invoke-virtual {v2, v0}, Lcom/twitter/android/moments/viewmodels/i$a;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/android/moments/viewmodels/i$a;

    move-result-object v0

    .line 81
    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/viewmodels/i$a;->a(Lcom/twitter/model/moments/e;)Lcom/twitter/android/moments/viewmodels/i$a;

    move-result-object v0

    .line 82
    invoke-virtual {v0, v9}, Lcom/twitter/android/moments/viewmodels/i$a;->b(Ljava/lang/String;)Lcom/twitter/android/moments/viewmodels/MomentModule$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/i$a;

    .line 83
    invoke-virtual {v0, v7}, Lcom/twitter/android/moments/viewmodels/i$a;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/moments/viewmodels/MomentModule$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/i$a;

    .line 84
    invoke-virtual {v0, v3}, Lcom/twitter/android/moments/viewmodels/i$a;->a(Lcom/twitter/model/moments/u;)Lcom/twitter/android/moments/viewmodels/MomentModule$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/i$a;

    .line 85
    invoke-virtual {v0, v4}, Lcom/twitter/android/moments/viewmodels/i$a;->a(Lceg;)Lcom/twitter/android/moments/viewmodels/MomentModule$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/i$a;

    .line 86
    invoke-virtual {v0}, Lcom/twitter/android/moments/viewmodels/i$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/MomentModule;

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/data/p;->a(Landroid/database/Cursor;)Lcom/twitter/android/moments/viewmodels/MomentModule;

    move-result-object v0

    return-object v0
.end method
