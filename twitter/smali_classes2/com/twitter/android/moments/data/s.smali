.class public Lcom/twitter/android/moments/data/s;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final a:Lcom/twitter/android/moments/data/q;

.field private final b:J


# direct methods
.method public constructor <init>(JLcom/twitter/android/moments/data/q;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-wide p1, p0, Lcom/twitter/android/moments/data/s;->b:J

    .line 24
    iput-object p3, p0, Lcom/twitter/android/moments/data/s;->a:Lcom/twitter/android/moments/data/q;

    .line 25
    return-void
.end method


# virtual methods
.method public a(J)Lrx/c;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/c",
            "<",
            "Lcbi",
            "<",
            "Lcom/twitter/android/moments/viewmodels/MomentModule;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 29
    new-instance v0, Lapb$a;

    invoke-direct {v0}, Lapb$a;-><init>()V

    .line 30
    invoke-static {p1, p2}, Lcom/twitter/library/provider/m$b;->a(J)Landroid/net/Uri;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/moments/data/s;->b:J

    invoke-static {v1, v2, v3}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lapb$a;->a(Landroid/net/Uri;)Lapb$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/provider/m$b;->b:[Ljava/lang/String;

    .line 32
    invoke-virtual {v0, v1}, Lapb$a;->b([Ljava/lang/String;)Lapb$a;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lapb$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapb;

    .line 34
    iget-object v1, p0, Lcom/twitter/android/moments/data/s;->a:Lcom/twitter/android/moments/data/q;

    invoke-virtual {v1, v0}, Lcom/twitter/android/moments/data/q;->a(Lapb;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/data/s;->a:Lcom/twitter/android/moments/data/q;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/q;->close()V

    .line 40
    return-void
.end method
