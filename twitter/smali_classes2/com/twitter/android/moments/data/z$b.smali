.class Lcom/twitter/android/moments/data/z$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lbhe$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/moments/data/z;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private final a:Lbsb;

.field private final b:J


# direct methods
.method constructor <init>(Lbsb;J)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-object p1, p0, Lcom/twitter/android/moments/data/z$b;->a:Lbsb;

    .line 89
    iput-wide p2, p0, Lcom/twitter/android/moments/data/z$b;->b:J

    .line 90
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/ac;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 95
    if-eqz p3, :cond_0

    .line 96
    iget-object v0, p0, Lcom/twitter/android/moments/data/z$b;->a:Lbsb;

    iget-wide v2, p0, Lcom/twitter/android/moments/data/z$b;->b:J

    invoke-virtual {v0, v2, v3, p3}, Lbsb;->a(JLjava/util/Collection;)V

    .line 98
    :cond_0
    return-void
.end method
