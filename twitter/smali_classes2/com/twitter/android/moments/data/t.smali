.class public Lcom/twitter/android/moments/data/t;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Lcom/twitter/model/moments/MomentPageType;)Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 159
    sget-object v0, Lcom/twitter/android/moments/data/t$1;->a:[I

    invoke-virtual {p0}, Lcom/twitter/model/moments/MomentPageType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 177
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 162
    :pswitch_0
    sget-object v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;->a:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    goto :goto_0

    .line 165
    :pswitch_1
    sget-object v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;->c:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    goto :goto_0

    .line 168
    :pswitch_2
    sget-object v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;->b:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    goto :goto_0

    .line 171
    :pswitch_3
    sget-object v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;->d:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    goto :goto_0

    .line 174
    :pswitch_4
    sget-object v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;->e:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    goto :goto_0

    .line 159
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method private static b(Lcom/twitter/model/moments/MomentPageType;)Lcom/twitter/model/moments/MomentPageType;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/twitter/model/moments/MomentPageType;->k:Lcom/twitter/model/moments/MomentPageType;

    if-ne p0, v0, :cond_0

    invoke-static {}, Lbsd;->n()Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    sget-object p0, Lcom/twitter/model/moments/MomentPageType;->a:Lcom/twitter/model/moments/MomentPageType;

    .line 46
    :cond_0
    return-object p0
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/Moment;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/moments/r;Lcom/twitter/model/moments/MomentPageDisplayMode;Lceo;)Lcom/twitter/model/moments/viewmodels/MomentPage;
    .locals 15

    .prologue
    .line 52
    move-object/from16 v0, p5

    iget-object v2, v0, Lceo;->c:Lcom/twitter/model/moments/MomentPageType;

    invoke-static {v2}, Lcom/twitter/android/moments/data/t;->b(Lcom/twitter/model/moments/MomentPageType;)Lcom/twitter/model/moments/MomentPageType;

    move-result-object v6

    .line 53
    move-object/from16 v0, p5

    iget-object v7, v0, Lceo;->e:Lcom/twitter/model/moments/e;

    .line 54
    move-object/from16 v0, p5

    iget-object v8, v0, Lceo;->f:Lcom/twitter/model/moments/w;

    .line 55
    move-object/from16 v0, p5

    iget-object v2, v0, Lceo;->g:Lcem;

    if-eqz v2, :cond_0

    move-object/from16 v0, p5

    iget-object v2, v0, Lceo;->g:Lcem;

    iget-wide v2, v2, Lcem;->b:J

    move-wide v4, v2

    .line 56
    :goto_0
    move-object/from16 v0, p5

    iget-object v9, v0, Lceo;->d:Lcom/twitter/model/moments/n;

    .line 57
    move-object/from16 v0, p5

    iget-object v10, v0, Lceo;->i:Lcom/twitter/model/moments/o;

    .line 58
    move-object/from16 v0, p5

    iget-object v11, v0, Lceo;->j:Lcom/twitter/model/moments/s;

    .line 60
    sget-object v2, Lcom/twitter/model/moments/MomentPageType;->j:Lcom/twitter/model/moments/MomentPageType;

    if-ne v6, v2, :cond_1

    .line 61
    move-object/from16 v0, p5

    iget-object v2, v0, Lceo;->g:Lcem;

    invoke-static {v2}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcem;

    .line 62
    iget-object v3, v2, Lcem;->d:Ljava/lang/String;

    .line 63
    iget-object v4, v2, Lcem;->c:Lcom/twitter/util/math/Size;

    .line 64
    new-instance v2, Lcom/twitter/model/moments/viewmodels/j$b;

    invoke-direct {v2}, Lcom/twitter/model/moments/viewmodels/j$b;-><init>()V

    .line 65
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/j$b;->a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/j$b;

    .line 66
    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/j$b;->a(Lcom/twitter/model/moments/r;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/j$b;

    .line 67
    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/j$b;->a(Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/j$b;

    .line 68
    invoke-virtual {v2, v3}, Lcom/twitter/model/moments/viewmodels/j$b;->a(Ljava/lang/String;)Lcom/twitter/model/moments/viewmodels/j$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/j$b;

    .line 69
    invoke-virtual {v2, v4}, Lcom/twitter/model/moments/viewmodels/j$b;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/model/moments/viewmodels/j$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/j$b;

    .line 70
    invoke-virtual {v2, v7}, Lcom/twitter/model/moments/viewmodels/j$b;->a(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/j$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/j$b;

    .line 71
    invoke-virtual {v2, v9}, Lcom/twitter/model/moments/viewmodels/j$b;->a(Lcom/twitter/model/moments/n;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/j$b;

    .line 72
    invoke-virtual {v2, v10}, Lcom/twitter/model/moments/viewmodels/j$b;->a(Lcom/twitter/model/moments/o;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/j$b;

    .line 73
    invoke-virtual {v2, v11}, Lcom/twitter/model/moments/viewmodels/j$b;->a(Lcom/twitter/model/moments/s;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/j$b;

    .line 74
    invoke-virtual {v2}, Lcom/twitter/model/moments/viewmodels/j$b;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/MomentPage;

    .line 141
    :goto_1
    return-object v2

    .line 55
    :cond_0
    const-wide/16 v2, 0x0

    move-wide v4, v2

    goto :goto_0

    .line 77
    :cond_1
    move-object/from16 v0, p5

    iget-wide v12, v0, Lceo;->k:J

    .line 78
    move-object/from16 v0, p5

    iget-object v14, v0, Lceo;->h:Lcom/twitter/model/moments/l;

    .line 79
    sget-object v2, Lcom/twitter/android/moments/data/t$1;->a:[I

    invoke-virtual {v6}, Lcom/twitter/model/moments/MomentPageType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 141
    new-instance v2, Lcom/twitter/model/moments/viewmodels/q$b;

    invoke-direct {v2}, Lcom/twitter/model/moments/viewmodels/q$b;-><init>()V

    .line 142
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/q$b;->a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/q$b;

    .line 143
    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/q$b;->a(Lcom/twitter/model/moments/r;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/q$b;

    .line 144
    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v12, v13}, Lcom/twitter/model/moments/viewmodels/q$b;->a(Lcom/twitter/model/core/Tweet;J)Lcom/twitter/model/moments/viewmodels/n$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/q$b;

    .line 145
    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/q$b;->a(Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/q$b;

    .line 146
    invoke-virtual {v2, v8}, Lcom/twitter/model/moments/viewmodels/q$b;->a(Lcom/twitter/model/moments/w;)Lcom/twitter/model/moments/viewmodels/q$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/q$b;

    .line 147
    invoke-virtual {v2, v14}, Lcom/twitter/model/moments/viewmodels/q$b;->a(Lcom/twitter/model/moments/l;)Lcom/twitter/model/moments/viewmodels/n$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/q$b;

    .line 148
    invoke-virtual {v2, v9}, Lcom/twitter/model/moments/viewmodels/q$b;->a(Lcom/twitter/model/moments/n;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/q$b;

    .line 149
    invoke-virtual {v2, v10}, Lcom/twitter/model/moments/viewmodels/q$b;->a(Lcom/twitter/model/moments/o;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/q$b;

    .line 150
    invoke-virtual {v2, v11}, Lcom/twitter/model/moments/viewmodels/q$b;->a(Lcom/twitter/model/moments/s;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/q$b;

    .line 151
    invoke-virtual {v2}, Lcom/twitter/model/moments/viewmodels/q$b;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/MomentPage;

    goto :goto_1

    .line 81
    :pswitch_0
    new-instance v2, Lcom/twitter/model/moments/viewmodels/p$a;

    invoke-direct {v2}, Lcom/twitter/model/moments/viewmodels/p$a;-><init>()V

    .line 82
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/p$a;->a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/p$a;

    .line 83
    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/p$a;->a(Lcom/twitter/model/moments/r;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/p$a;

    .line 84
    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v12, v13}, Lcom/twitter/model/moments/viewmodels/p$a;->a(Lcom/twitter/model/core/Tweet;J)Lcom/twitter/model/moments/viewmodels/n$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/p$a;

    .line 85
    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/p$a;->a(Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/p$a;

    .line 86
    invoke-virtual {v2, v7}, Lcom/twitter/model/moments/viewmodels/p$a;->a(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/p$a;

    move-result-object v2

    .line 87
    invoke-virtual {v2, v14}, Lcom/twitter/model/moments/viewmodels/p$a;->a(Lcom/twitter/model/moments/l;)Lcom/twitter/model/moments/viewmodels/n$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/p$a;

    .line 88
    invoke-virtual {v2, v9}, Lcom/twitter/model/moments/viewmodels/p$a;->a(Lcom/twitter/model/moments/n;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/p$a;

    .line 89
    invoke-virtual {v2, v10}, Lcom/twitter/model/moments/viewmodels/p$a;->a(Lcom/twitter/model/moments/o;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/p$a;

    .line 90
    invoke-virtual {v2, v11}, Lcom/twitter/model/moments/viewmodels/p$a;->a(Lcom/twitter/model/moments/s;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/p$a;

    .line 91
    invoke-virtual {v2}, Lcom/twitter/model/moments/viewmodels/p$a;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/MomentPage;

    goto/16 :goto_1

    .line 99
    :pswitch_1
    new-instance v2, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;

    invoke-direct {v2}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;-><init>()V

    .line 100
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;->a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;

    .line 101
    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;->a(Lcom/twitter/model/moments/r;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;

    .line 102
    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v12, v13}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;->a(Lcom/twitter/model/core/Tweet;J)Lcom/twitter/model/moments/viewmodels/n$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;

    .line 103
    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;->a(Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;

    .line 104
    invoke-static {v6}, Lcom/twitter/android/moments/data/t;->a(Lcom/twitter/model/moments/MomentPageType;)Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    invoke-virtual {v2, v3}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;->a(Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;)Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;

    move-result-object v2

    .line 105
    invoke-virtual {v2, v7}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;->b(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;

    move-result-object v2

    .line 106
    invoke-virtual {v2, v14}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;->a(Lcom/twitter/model/moments/l;)Lcom/twitter/model/moments/viewmodels/n$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;

    .line 107
    invoke-virtual {v2, v9}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;->a(Lcom/twitter/model/moments/n;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;

    .line 108
    invoke-virtual {v2, v10}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;->a(Lcom/twitter/model/moments/o;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;

    .line 109
    invoke-virtual {v2, v11}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;->a(Lcom/twitter/model/moments/s;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;

    .line 110
    invoke-virtual {v2}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$a;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/MomentPage;

    goto/16 :goto_1

    .line 113
    :pswitch_2
    new-instance v2, Lcom/twitter/model/moments/viewmodels/o$a;

    invoke-direct {v2}, Lcom/twitter/model/moments/viewmodels/o$a;-><init>()V

    .line 114
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/o$a;->a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/o$a;

    .line 115
    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/o$a;->a(Lcom/twitter/model/moments/r;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/o$a;

    .line 116
    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v12, v13}, Lcom/twitter/model/moments/viewmodels/o$a;->a(Lcom/twitter/model/core/Tweet;J)Lcom/twitter/model/moments/viewmodels/n$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/o$a;

    .line 117
    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/o$a;->a(Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/o$a;

    .line 118
    invoke-virtual {v2, v4, v5}, Lcom/twitter/model/moments/viewmodels/o$a;->b(J)Lcom/twitter/model/moments/viewmodels/o$a;

    move-result-object v2

    .line 119
    invoke-virtual {v2, v7}, Lcom/twitter/model/moments/viewmodels/o$a;->b(Lcom/twitter/model/moments/e;)Lcom/twitter/model/moments/viewmodels/o$a;

    move-result-object v2

    .line 120
    invoke-virtual {v2, v14}, Lcom/twitter/model/moments/viewmodels/o$a;->a(Lcom/twitter/model/moments/l;)Lcom/twitter/model/moments/viewmodels/n$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/o$a;

    .line 121
    invoke-virtual {v2, v9}, Lcom/twitter/model/moments/viewmodels/o$a;->a(Lcom/twitter/model/moments/n;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/o$a;

    .line 122
    invoke-virtual {v2, v10}, Lcom/twitter/model/moments/viewmodels/o$a;->a(Lcom/twitter/model/moments/o;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/o$a;

    .line 123
    invoke-virtual {v2, v11}, Lcom/twitter/model/moments/viewmodels/o$a;->a(Lcom/twitter/model/moments/s;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/o$a;

    .line 124
    invoke-virtual {v2}, Lcom/twitter/model/moments/viewmodels/o$a;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/MomentPage;

    goto/16 :goto_1

    .line 127
    :pswitch_3
    new-instance v2, Lcom/twitter/model/moments/viewmodels/m$a;

    invoke-direct {v2}, Lcom/twitter/model/moments/viewmodels/m$a;-><init>()V

    .line 128
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/m$a;->a(Lcom/twitter/model/moments/Moment;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/m$a;

    .line 129
    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/m$a;->a(Lcom/twitter/model/moments/r;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/m$a;

    .line 130
    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v12, v13}, Lcom/twitter/model/moments/viewmodels/m$a;->a(Lcom/twitter/model/core/Tweet;J)Lcom/twitter/model/moments/viewmodels/n$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/m$a;

    .line 131
    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Lcom/twitter/model/moments/viewmodels/m$a;->a(Lcom/twitter/model/moments/MomentPageDisplayMode;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/m$a;

    .line 132
    invoke-virtual {v2, v14}, Lcom/twitter/model/moments/viewmodels/m$a;->a(Lcom/twitter/model/moments/l;)Lcom/twitter/model/moments/viewmodels/n$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/m$a;

    .line 133
    invoke-virtual {v2, v8}, Lcom/twitter/model/moments/viewmodels/m$a;->a(Lcom/twitter/model/moments/w;)Lcom/twitter/model/moments/viewmodels/q$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/m$a;

    .line 134
    invoke-virtual {v2, v9}, Lcom/twitter/model/moments/viewmodels/m$a;->a(Lcom/twitter/model/moments/n;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/m$a;

    .line 135
    invoke-virtual {v2, v10}, Lcom/twitter/model/moments/viewmodels/m$a;->a(Lcom/twitter/model/moments/o;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/m$a;

    .line 136
    invoke-virtual {v2, v11}, Lcom/twitter/model/moments/viewmodels/m$a;->a(Lcom/twitter/model/moments/s;)Lcom/twitter/model/moments/viewmodels/MomentPage$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/m$a;

    .line 137
    invoke-virtual {v2}, Lcom/twitter/model/moments/viewmodels/m$a;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/viewmodels/MomentPage;

    goto/16 :goto_1

    .line 79
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
