.class public Lcom/twitter/android/moments/data/ag;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lauj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lauj",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Ljava/util/List",
        "<",
        "Lcfn;",
        ">;>;"
    }
.end annotation


# instance fields
.field final a:Lcom/twitter/android/moments/data/ah;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/moments/data/ah",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject",
            "<",
            "Ljava/util/List",
            "<",
            "Lcfn;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Lrx/f;

.field private final d:Lrx/f;

.field private e:Lrx/j;

.field private final f:Lbds;

.field private final g:Lrx/f;

.field private final h:I

.field private i:Z


# direct methods
.method constructor <init>(ILbds;Lrx/f;Lrx/f;Lrx/f;Lrx/subjects/PublishSubject;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lbds;",
            "Lrx/f;",
            "Lrx/f;",
            "Lrx/f;",
            "Lrx/subjects/PublishSubject",
            "<",
            "Ljava/util/List",
            "<",
            "Lcfn;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Lcom/twitter/android/moments/data/ah;

    invoke-direct {v0}, Lcom/twitter/android/moments/data/ah;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/moments/data/ag;->a:Lcom/twitter/android/moments/data/ah;

    .line 88
    iput p1, p0, Lcom/twitter/android/moments/data/ag;->h:I

    .line 89
    iput-object p2, p0, Lcom/twitter/android/moments/data/ag;->f:Lbds;

    .line 90
    iput-object p3, p0, Lcom/twitter/android/moments/data/ag;->g:Lrx/f;

    .line 91
    iput-object p4, p0, Lcom/twitter/android/moments/data/ag;->c:Lrx/f;

    .line 92
    iput-object p5, p0, Lcom/twitter/android/moments/data/ag;->d:Lrx/f;

    .line 93
    iput-object p6, p0, Lcom/twitter/android/moments/data/ag;->b:Lrx/subjects/PublishSubject;

    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/moments/data/ag;->i:Z

    .line 95
    return-void
.end method

.method public static a(Lbds;)Lcom/twitter/android/moments/data/ag;
    .locals 7

    .prologue
    .line 61
    new-instance v0, Lcom/twitter/android/moments/data/ag;

    const/16 v1, 0x7530

    .line 64
    invoke-static {}, Lcws;->c()Lrx/f;

    move-result-object v3

    .line 65
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v4

    .line 66
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v5

    .line 67
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v6

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/moments/data/ag;-><init>(ILbds;Lrx/f;Lrx/f;Lrx/f;Lrx/subjects/PublishSubject;)V

    .line 61
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/data/ag;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/data/ag;->c(Ljava/util/List;)V

    return-void
.end method

.method private a(Lrx/j;)V
    .locals 1

    .prologue
    .line 185
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lrx/j;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 186
    invoke-interface {p1}, Lrx/j;->B_()V

    .line 188
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/data/ag;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/twitter/android/moments/data/ag;->i:Z

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/moments/data/ag;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/twitter/android/moments/data/ag;->c()V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/moments/data/ag;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/data/ag;->b(Ljava/util/List;)V

    return-void
.end method

.method private declared-synchronized b(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 194
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/moments/data/ag;->a:Lcom/twitter/android/moments/data/ah;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/data/ah;->a(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 195
    monitor-exit p0

    return-void

    .line 194
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic c(Lcom/twitter/android/moments/data/ag;)Lbds;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/android/moments/data/ag;->f:Lbds;

    return-object v0
.end method

.method private c()V
    .locals 6

    .prologue
    .line 98
    iget-object v0, p0, Lcom/twitter/android/moments/data/ag;->e:Lrx/j;

    if-nez v0, :cond_0

    .line 103
    const-wide/16 v0, 0x12c

    iget v2, p0, Lcom/twitter/android/moments/data/ag;->h:I

    int-to-long v2, v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v5, p0, Lcom/twitter/android/moments/data/ag;->g:Lrx/f;

    .line 104
    invoke-static/range {v0 .. v5}, Lrx/c;->a(JJLjava/util/concurrent/TimeUnit;Lrx/f;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/data/ag;->c:Lrx/f;

    .line 105
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 106
    invoke-direct {p0}, Lcom/twitter/android/moments/data/ag;->f()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 107
    invoke-static {}, Lcre;->b()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 108
    invoke-direct {p0}, Lcom/twitter/android/moments/data/ag;->e()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->f(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/data/ag;->d:Lrx/f;

    .line 109
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/data/ag;->b:Lrx/subjects/PublishSubject;

    .line 110
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/d;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/data/ag;->e:Lrx/j;

    .line 112
    :cond_0
    return-void
.end method

.method private declared-synchronized c(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 201
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/moments/data/ag;->a:Lcom/twitter/android/moments/data/ah;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/data/ah;->b(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    monitor-exit p0

    return-void

    .line 201
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private d(Ljava/util/List;)Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lrx/functions/d",
            "<",
            "Ljava/util/List",
            "<",
            "Lcfn;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcfn;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 221
    new-instance v0, Lcom/twitter/android/moments/data/ag$3;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/moments/data/ag$3;-><init>(Lcom/twitter/android/moments/data/ag;Ljava/util/List;)V

    return-object v0
.end method

.method private declared-synchronized d()V
    .locals 1

    .prologue
    .line 208
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/moments/data/ag;->a:Lcom/twitter/android/moments/data/ah;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/ah;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 209
    monitor-exit p0

    return-void

    .line 208
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private e()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lrx/c",
            "<",
            "Ljava/util/List",
            "<",
            "Lcfn;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 237
    new-instance v0, Lcom/twitter/android/moments/data/ag$4;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/data/ag$4;-><init>(Lcom/twitter/android/moments/data/ag;)V

    return-object v0
.end method

.method private f()Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/d",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 247
    new-instance v0, Lcom/twitter/android/moments/data/ag$5;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/data/ag$5;-><init>(Lcom/twitter/android/moments/data/ag;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/util/List;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lrx/c",
            "<",
            "Ljava/util/List",
            "<",
            "Lcfn;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lcom/twitter/android/moments/data/ag;->b:Lrx/subjects/PublishSubject;

    .line 156
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/data/ag;->d(Ljava/util/List;)Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 157
    invoke-static {}, Lcre;->b()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/data/ag$2;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/data/ag$2;-><init>(Lcom/twitter/android/moments/data/ag;Ljava/util/List;)V

    .line 158
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/functions/a;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/data/ag$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/data/ag$1;-><init>(Lcom/twitter/android/moments/data/ag;Ljava/util/List;)V

    .line 167
    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/a;)Lrx/c;

    move-result-object v0

    .line 155
    return-object v0
.end method

.method public a()V
    .locals 1
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 123
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 124
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/moments/data/ag;->i:Z

    .line 125
    iget-object v0, p0, Lcom/twitter/android/moments/data/ag;->e:Lrx/j;

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/data/ag;->a(Lrx/j;)V

    .line 126
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/moments/data/ag;->e:Lrx/j;

    .line 127
    return-void
.end method

.method public synthetic a_(Ljava/lang/Object;)Lrx/c;
    .locals 1

    .prologue
    .line 40
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/data/ag;->a(Ljava/util/List;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 138
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 139
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/moments/data/ag;->i:Z

    .line 140
    iget-object v0, p0, Lcom/twitter/android/moments/data/ag;->a:Lcom/twitter/android/moments/data/ah;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/ah;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 141
    invoke-direct {p0}, Lcom/twitter/android/moments/data/ag;->c()V

    .line 143
    :cond_0
    return-void
.end method

.method public close()V
    .locals 1
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 178
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 179
    iget-object v0, p0, Lcom/twitter/android/moments/data/ag;->e:Lrx/j;

    invoke-direct {p0, v0}, Lcom/twitter/android/moments/data/ag;->a(Lrx/j;)V

    .line 180
    iget-object v0, p0, Lcom/twitter/android/moments/data/ag;->b:Lrx/subjects/PublishSubject;

    invoke-virtual {v0}, Lrx/subjects/PublishSubject;->by_()V

    .line 181
    invoke-direct {p0}, Lcom/twitter/android/moments/data/ag;->d()V

    .line 182
    return-void
.end method
