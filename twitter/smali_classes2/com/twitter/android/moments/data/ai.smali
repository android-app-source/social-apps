.class public Lcom/twitter/android/moments/data/ai;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/moments/ui/guide/ae;


# instance fields
.field private final a:Laag;

.field private final b:Lcom/twitter/android/moments/data/am;

.field private final c:Lrx/f;

.field private final d:Lrx/f;

.field private e:Lrx/j;

.field private f:Lcfn;


# direct methods
.method public constructor <init>(Laag;Lcom/twitter/android/moments/data/am;)V
    .locals 2

    .prologue
    .line 37
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v0

    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/twitter/android/moments/data/ai;-><init>(Laag;Lcom/twitter/android/moments/data/am;Lrx/f;Lrx/f;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Laag;Lcom/twitter/android/moments/data/am;Lrx/f;Lrx/f;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/twitter/android/moments/data/ai;->a:Laag;

    .line 44
    iput-object p2, p0, Lcom/twitter/android/moments/data/ai;->b:Lcom/twitter/android/moments/data/am;

    .line 45
    iput-object p3, p0, Lcom/twitter/android/moments/data/ai;->c:Lrx/f;

    .line 46
    iput-object p4, p0, Lcom/twitter/android/moments/data/ai;->d:Lrx/f;

    .line 47
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/data/ai;)Laag;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/twitter/android/moments/data/ai;->a:Laag;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 81
    iget-object v0, p0, Lcom/twitter/android/moments/data/ai;->e:Lrx/j;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/twitter/android/moments/data/ai;->e:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 83
    iput-object v1, p0, Lcom/twitter/android/moments/data/ai;->e:Lrx/j;

    .line 85
    :cond_0
    iput-object v1, p0, Lcom/twitter/android/moments/data/ai;->f:Lcfn;

    .line 86
    iget-object v0, p0, Lcom/twitter/android/moments/data/ai;->a:Laag;

    invoke-virtual {v0}, Laag;->a()V

    .line 87
    return-void
.end method

.method public a(Lcfn;)V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/android/moments/data/ai;->f:Lcfn;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcfn;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/moments/data/ai;->f:Lcfn;

    iget-object v1, v1, Lcfn;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    :goto_0
    return-void

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/data/ai;->b:Lcom/twitter/android/moments/data/am;

    iget-object v1, p1, Lcfn;->b:Ljava/lang/String;

    .line 60
    invoke-interface {v0, v1}, Lcom/twitter/android/moments/data/am;->a(Ljava/lang/String;)Lcfn;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfn;

    .line 62
    invoke-virtual {p0}, Lcom/twitter/android/moments/data/ai;->a()V

    .line 63
    iget-object v1, p0, Lcom/twitter/android/moments/data/ai;->a:Laag;

    invoke-virtual {v1, v0}, Laag;->a(Lcfn;)V

    .line 65
    iget-object v0, p0, Lcom/twitter/android/moments/data/ai;->b:Lcom/twitter/android/moments/data/am;

    iget-object v1, p1, Lcfn;->b:Ljava/lang/String;

    .line 66
    invoke-interface {v0, v1}, Lcom/twitter/android/moments/data/am;->b(Ljava/lang/String;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/data/ai;->c:Lrx/f;

    .line 67
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/moments/data/ai;->d:Lrx/f;

    .line 68
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/data/ai$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/data/ai$1;-><init>(Lcom/twitter/android/moments/data/ai;)V

    .line 69
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/data/ai;->e:Lrx/j;

    goto :goto_0
.end method

.method public b()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/android/moments/data/ai;->a:Laag;

    invoke-virtual {v0}, Laag;->b()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
