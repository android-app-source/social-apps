.class public Lcom/twitter/android/moments/data/n;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/Session;

.field private final c:Lcom/twitter/library/client/p;

.field private final d:Lbsb;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/p;Lbsb;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/twitter/android/moments/data/n;->a:Landroid/content/Context;

    .line 24
    iput-object p2, p0, Lcom/twitter/android/moments/data/n;->b:Lcom/twitter/library/client/Session;

    .line 25
    iput-object p3, p0, Lcom/twitter/android/moments/data/n;->c:Lcom/twitter/library/client/p;

    .line 26
    iput-object p4, p0, Lcom/twitter/android/moments/data/n;->d:Lbsb;

    .line 27
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/moments/Moment;)V
    .locals 8

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/android/moments/data/n;->d:Lbsb;

    iget-wide v1, p1, Lcom/twitter/model/moments/Moment;->b:J

    const/4 v3, 0x1

    iget-wide v4, p1, Lcom/twitter/model/moments/Moment;->u:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    invoke-virtual/range {v0 .. v5}, Lbsb;->a(JZJ)V

    .line 32
    new-instance v0, Lbdo;

    iget-object v1, p0, Lcom/twitter/android/moments/data/n;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/moments/data/n;->d:Lbsb;

    iget-object v3, p0, Lcom/twitter/android/moments/data/n;->b:Lcom/twitter/library/client/Session;

    invoke-direct {v0, v1, v2, v3, p1}, Lbdo;-><init>(Landroid/content/Context;Lbsb;Lcom/twitter/library/client/Session;Lcom/twitter/model/moments/Moment;)V

    .line 33
    iget-object v1, p0, Lcom/twitter/android/moments/data/n;->c:Lcom/twitter/library/client/p;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 34
    return-void
.end method

.method public b(Lcom/twitter/model/moments/Moment;)V
    .locals 10

    .prologue
    .line 37
    iget-object v0, p0, Lcom/twitter/android/moments/data/n;->d:Lbsb;

    iget-wide v1, p1, Lcom/twitter/model/moments/Moment;->b:J

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    iget-wide v6, p1, Lcom/twitter/model/moments/Moment;->u:J

    const-wide/16 v8, 0x1

    sub-long/2addr v6, v8

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lbsb;->a(JZJ)V

    .line 38
    new-instance v0, Lbdv;

    iget-object v1, p0, Lcom/twitter/android/moments/data/n;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/moments/data/n;->d:Lbsb;

    iget-object v3, p0, Lcom/twitter/android/moments/data/n;->b:Lcom/twitter/library/client/Session;

    invoke-direct {v0, v1, v2, v3, p1}, Lbdv;-><init>(Landroid/content/Context;Lbsb;Lcom/twitter/library/client/Session;Lcom/twitter/model/moments/Moment;)V

    .line 39
    iget-object v1, p0, Lcom/twitter/android/moments/data/n;->c:Lcom/twitter/library/client/p;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 40
    return-void
.end method
