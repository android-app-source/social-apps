.class public Lcom/twitter/android/moments/data/f;
.super Lcom/twitter/android/moments/data/d;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/moments/data/d",
        "<",
        "Lcom/twitter/android/moments/viewmodels/MomentGuide;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/moments/data/g;

.field private final b:Lcom/twitter/library/client/s;

.field private final c:Landroid/support/v4/content/Loader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/data/g;Landroid/support/v4/content/Loader;Landroid/support/v4/app/LoaderManager;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/moments/data/g;",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/support/v4/app/LoaderManager;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p3, p4}, Lcom/twitter/android/moments/data/d;-><init>(Landroid/support/v4/app/LoaderManager;I)V

    .line 37
    new-instance v0, Lcom/twitter/android/moments/data/f$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/moments/data/f$1;-><init>(Lcom/twitter/android/moments/data/f;)V

    iput-object v0, p0, Lcom/twitter/android/moments/data/f;->b:Lcom/twitter/library/client/s;

    .line 64
    iput-object p1, p0, Lcom/twitter/android/moments/data/f;->a:Lcom/twitter/android/moments/data/g;

    .line 65
    iput-object p2, p0, Lcom/twitter/android/moments/data/f;->c:Landroid/support/v4/content/Loader;

    .line 66
    return-void
.end method

.method private static a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 190
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v2

    .line 191
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 193
    :cond_0
    const-string/jumbo v0, "moments_guide_moment_id"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 194
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    .line 195
    :goto_0
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    .line 196
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 198
    :cond_1
    invoke-virtual {v2}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0

    .line 194
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/moments/data/f;Z)Z
    .locals 0

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/twitter/android/moments/data/f;->e:Z

    return p1
.end method

.method private a(Ljava/lang/String;Lcom/twitter/model/moments/MomentGuideSectionType;Ljava/lang/String;Lcom/twitter/model/moments/MomentGuideSectionType;)Z
    .locals 1

    .prologue
    .line 185
    invoke-static {p1, p3}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eq p2, p4, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/moments/data/f;Z)Z
    .locals 0

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/twitter/android/moments/data/f;->d:Z

    return p1
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Lcom/twitter/android/moments/viewmodels/MomentGuide;
    .locals 21

    .prologue
    .line 92
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 93
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v15

    .line 94
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v10

    .line 95
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v16

    .line 96
    new-instance v17, Lcom/twitter/android/moments/data/p;

    invoke-direct/range {v17 .. v17}, Lcom/twitter/android/moments/data/p;-><init>()V

    .line 97
    const/4 v9, 0x0

    .line 100
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v18

    .line 101
    const/4 v8, 0x0

    .line 102
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 103
    const/4 v7, 0x0

    .line 104
    const/4 v3, 0x0

    .line 105
    const/4 v6, 0x0

    .line 106
    const/4 v5, 0x0

    .line 107
    const/4 v4, 0x0

    .line 109
    :goto_0
    const-string/jumbo v2, "moments_guide_item_type"

    .line 110
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 111
    if-nez v11, :cond_4

    .line 112
    const-string/jumbo v2, "moments_sections_section_title"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 114
    const-string/jumbo v2, "moments_sections_section_type"

    .line 115
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    const-class v11, Lcom/twitter/model/moments/MomentGuideSectionType;

    .line 116
    invoke-static {v11}, Lcom/twitter/util/serialization/f;->a(Ljava/lang/Class;)Lcom/twitter/util/serialization/l;

    move-result-object v11

    .line 114
    invoke-static {v2, v11}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/MomentGuideSectionType;

    .line 117
    const-string/jumbo v11, "moments_sections_section_category_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 119
    const-string/jumbo v11, "moments_sections_section_footer"

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 121
    const-string/jumbo v11, "moments_sections_section_footer_deeplink"

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 124
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v3, v14, v2}, Lcom/twitter/android/moments/data/f;->a(Ljava/lang/String;Lcom/twitter/model/moments/MomentGuideSectionType;Ljava/lang/String;Lcom/twitter/model/moments/MomentGuideSectionType;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 125
    invoke-virtual {v10}, Lcom/twitter/util/collection/h;->h()Z

    move-result v19

    if-nez v19, :cond_0

    if-eqz v3, :cond_0

    .line 126
    new-instance v19, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;

    invoke-direct/range {v19 .. v19}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;-><init>()V

    .line 127
    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;->a(Ljava/lang/String;)Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;

    move-result-object v7

    .line 128
    invoke-virtual {v7, v3}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;->a(Lcom/twitter/model/moments/MomentGuideSectionType;)Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;

    move-result-object v7

    .line 129
    invoke-virtual {v10}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-virtual {v7, v3}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;->a(Ljava/util/List;)Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;

    move-result-object v3

    .line 130
    invoke-virtual {v3, v6}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;->b(Ljava/lang/String;)Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;

    move-result-object v3

    .line 131
    invoke-virtual {v3, v5}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;->c(Ljava/lang/String;)Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;

    move-result-object v3

    .line 132
    invoke-virtual {v3, v4}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;->d(Ljava/lang/String;)Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;

    move-result-object v3

    .line 133
    invoke-virtual {v3}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;->q()Ljava/lang/Object;

    move-result-object v3

    .line 126
    invoke-virtual {v15, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 140
    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v10

    move-object v3, v12

    move-object v4, v13

    move-object v5, v2

    move-object v6, v14

    move-object v2, v11

    .line 142
    :goto_1
    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/data/p;->a(Landroid/database/Cursor;)Lcom/twitter/android/moments/viewmodels/MomentModule;

    move-result-object v7

    .line 143
    invoke-virtual {v7}, Lcom/twitter/android/moments/viewmodels/MomentModule;->b()Lcom/twitter/model/moments/Moment;

    move-result-object v11

    iget-wide v12, v11, Lcom/twitter/model/moments/Moment;->b:J

    .line 144
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    move-object/from16 v0, v18

    invoke-interface {v0, v11}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 145
    invoke-virtual {v10, v7}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 146
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object/from16 v0, v18

    invoke-interface {v0, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move v7, v8

    :goto_2
    move-object v8, v9

    move-object v9, v10

    move-object/from16 v20, v4

    move-object v4, v3

    move-object v3, v2

    move-object v2, v5

    move-object/from16 v5, v20

    .line 164
    :goto_3
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v10

    if-nez v10, :cond_8

    .line 166
    if-eqz v2, :cond_1

    .line 167
    new-instance v7, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;

    invoke-direct {v7}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;-><init>()V

    .line 168
    invoke-virtual {v7, v6}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;->a(Ljava/lang/String;)Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;

    move-result-object v6

    .line 169
    invoke-virtual {v6, v2}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;->a(Lcom/twitter/model/moments/MomentGuideSectionType;)Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;

    move-result-object v6

    .line 170
    invoke-virtual {v9}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-virtual {v6, v2}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;->a(Ljava/util/List;)Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;

    move-result-object v2

    .line 171
    invoke-virtual {v2, v5}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;->b(Ljava/lang/String;)Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;

    move-result-object v2

    .line 172
    invoke-virtual {v2, v4}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;->c(Ljava/lang/String;)Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;

    move-result-object v2

    .line 173
    invoke-virtual {v2, v3}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;->d(Ljava/lang/String;)Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;

    move-result-object v2

    .line 174
    invoke-virtual {v2}, Lcom/twitter/android/moments/viewmodels/MomentGuideSection$a;->q()Ljava/lang/Object;

    move-result-object v2

    .line 167
    invoke-virtual {v15, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    :cond_1
    move-object v7, v8

    .line 178
    :goto_4
    new-instance v2, Lcom/twitter/android/moments/viewmodels/MomentGuide;

    const-string/jumbo v3, "moments_guide_user_states_is_updated"

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/twitter/android/moments/data/f;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    const-string/jumbo v4, "moments_guide_user_states_is_read"

    .line 179
    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/twitter/android/moments/data/f;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v4

    invoke-virtual {v15}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 180
    invoke-virtual/range {v16 .. v16}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    invoke-direct/range {v2 .. v7}, Lcom/twitter/android/moments/viewmodels/MomentGuide;-><init>(Ljava/util/Map;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Lcej;)V

    .line 178
    return-object v2

    .line 147
    :cond_2
    if-nez v8, :cond_3

    .line 148
    const/4 v8, 0x1

    .line 149
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string/jumbo v11, "Got duplicate moment in moments sectioned guide"

    invoke-direct {v7, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Lcpd;->c(Ljava/lang/Throwable;)V

    :cond_3
    move v7, v8

    goto :goto_2

    .line 151
    :cond_4
    const/4 v2, 0x1

    if-ne v11, v2, :cond_5

    .line 152
    const-string/jumbo v2, "moments_guide_data"

    .line 153
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    sget-object v11, Lcom/twitter/model/moments/i;->a:Lcom/twitter/util/serialization/l;

    .line 152
    invoke-static {v2, v11}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/moments/i;

    .line 155
    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move v7, v8

    move-object v8, v9

    move-object v9, v10

    .line 156
    goto/16 :goto_3

    :cond_5
    const/4 v2, 0x2

    if-ne v11, v2, :cond_6

    .line 157
    const-string/jumbo v2, "moments_guide_data"

    .line 158
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    sget-object v9, Lcej;->a:Lcom/twitter/util/serialization/l;

    .line 157
    invoke-static {v2, v9}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcej;

    move-object v9, v10

    move-object/from16 v20, v5

    move-object v5, v6

    move-object v6, v7

    move v7, v8

    move-object v8, v2

    move-object v2, v3

    move-object v3, v4

    move-object/from16 v4, v20

    goto/16 :goto_3

    .line 161
    :cond_6
    const/4 v2, 0x1

    .line 162
    new-instance v8, Ljava/lang/IllegalStateException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "Guide item type invalid: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v8, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v8}, Lcpd;->c(Ljava/lang/Throwable;)V

    move-object v8, v9

    move-object v9, v10

    move-object/from16 v20, v6

    move-object v6, v7

    move v7, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object/from16 v5, v20

    goto/16 :goto_3

    :cond_7
    move-object v2, v4

    move-object v4, v6

    move-object v6, v7

    move-object/from16 v20, v3

    move-object v3, v5

    move-object/from16 v5, v20

    goto/16 :goto_1

    :cond_8
    move-object v10, v9

    move-object v9, v8

    move v8, v7

    move-object v7, v6

    move-object v6, v5

    move-object v5, v4

    move-object v4, v3

    move-object v3, v2

    goto/16 :goto_0

    :cond_9
    move-object v7, v9

    goto/16 :goto_4
.end method

.method public synthetic c(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/data/f;->a(Landroid/database/Cursor;)Lcom/twitter/android/moments/viewmodels/MomentGuide;

    move-result-object v0

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/moments/data/f;->e:Z

    .line 73
    iget-object v0, p0, Lcom/twitter/android/moments/data/f;->a:Lcom/twitter/android/moments/data/g;

    iget-object v1, p0, Lcom/twitter/android/moments/data/f;->b:Lcom/twitter/library/client/s;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/data/g;->a(Lcom/twitter/library/client/s;)Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/moments/data/f;->e:Z

    .line 81
    iget-object v0, p0, Lcom/twitter/android/moments/data/f;->a:Lcom/twitter/android/moments/data/g;

    iget-object v1, p0, Lcom/twitter/android/moments/data/f;->b:Lcom/twitter/library/client/s;

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/data/g;->b(Lcom/twitter/library/client/s;)V

    .line 82
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/twitter/android/moments/data/f;->d:Z

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 207
    iget-boolean v0, p0, Lcom/twitter/android/moments/data/f;->e:Z

    return v0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 203
    iget-object v0, p0, Lcom/twitter/android/moments/data/f;->c:Landroid/support/v4/content/Loader;

    return-object v0
.end method
