.class public Lcom/twitter/android/moments/data/z;
.super Lcom/twitter/android/moments/data/d;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/client/s;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/data/z$a;,
        Lcom/twitter/android/moments/data/z$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/moments/data/d",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/twitter/model/core/Tweet;",
        ">;>;",
        "Lcom/twitter/library/client/s;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lbsb;

.field private final c:Lcom/twitter/library/client/p;

.field private final d:Lbsa;

.field private e:Lcom/twitter/android/moments/data/z$a;

.field private final f:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbsb;Landroid/support/v4/app/LoaderManager;JILcom/twitter/library/client/p;Lbsa;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p3, p6}, Lcom/twitter/android/moments/data/d;-><init>(Landroid/support/v4/app/LoaderManager;I)V

    .line 43
    iput-object p1, p0, Lcom/twitter/android/moments/data/z;->a:Landroid/content/Context;

    .line 44
    iput-object p2, p0, Lcom/twitter/android/moments/data/z;->b:Lbsb;

    .line 45
    iput-wide p4, p0, Lcom/twitter/android/moments/data/z;->f:J

    .line 46
    iput-object p7, p0, Lcom/twitter/android/moments/data/z;->c:Lcom/twitter/library/client/p;

    .line 47
    iput-object p8, p0, Lcom/twitter/android/moments/data/z;->d:Lbsa;

    .line 48
    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 104
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    sget-object v1, Lbtd;->a:Lbtd;

    invoke-virtual {v1, p1}, Lbtd;->a(Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet;

    move-result-object v1

    .line 106
    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 108
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public a(ILandroid/os/Bundle;Lcom/twitter/library/service/s;)V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public a(ILcom/twitter/library/service/s;)V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method public a(Ljava/util/List;Lcom/twitter/library/client/Session;Lcom/twitter/android/moments/data/z$a;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/twitter/library/client/Session;",
            "Lcom/twitter/android/moments/data/z$a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 56
    iput-object p3, p0, Lcom/twitter/android/moments/data/z;->e:Lcom/twitter/android/moments/data/z$a;

    .line 57
    new-instance v0, Lbhe;

    iget-object v1, p0, Lcom/twitter/android/moments/data/z;->a:Landroid/content/Context;

    new-instance v2, Lcom/twitter/android/moments/data/z$b;

    iget-object v3, p0, Lcom/twitter/android/moments/data/z;->b:Lbsb;

    iget-wide v4, p0, Lcom/twitter/android/moments/data/z;->f:J

    invoke-direct {v2, v3, v4, v5}, Lcom/twitter/android/moments/data/z$b;-><init>(Lbsb;J)V

    invoke-direct {v0, v1, p2, p1, v2}, Lbhe;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/util/List;Lbhe$a;)V

    .line 60
    iget-object v1, p0, Lcom/twitter/android/moments/data/z;->c:Lcom/twitter/library/client/p;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, p0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;ILcom/twitter/library/client/s;)Z

    .line 61
    return-void
.end method

.method public b(ILcom/twitter/library/service/s;)V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/twitter/android/moments/data/z;->e:Lcom/twitter/android/moments/data/z$a;

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/twitter/library/service/s;->T()Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/twitter/android/moments/data/z;->e:Lcom/twitter/android/moments/data/z$a;

    invoke-interface {v0}, Lcom/twitter/android/moments/data/z$a;->a()V

    .line 76
    :cond_0
    return-void
.end method

.method public synthetic c(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/data/z;->a(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lcom/twitter/android/moments/data/z;->d:Lbsa;

    iget-wide v2, p0, Lcom/twitter/android/moments/data/z;->f:J

    invoke-virtual {v0, v2, v3}, Lbsa;->a(J)Lcom/twitter/util/android/d;

    move-result-object v0

    return-object v0
.end method
