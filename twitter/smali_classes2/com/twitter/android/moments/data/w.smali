.class public Lcom/twitter/android/moments/data/w;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lauj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lauj",
        "<",
        "Ljava/lang/Long;",
        "Lcom/twitter/model/moments/Moment;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:Lcom/twitter/android/moments/data/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/moments/data/c",
            "<",
            "Lapb;",
            "Lcbi",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/android/moments/data/l;


# direct methods
.method public constructor <init>(JLcom/twitter/android/moments/data/c;Lcom/twitter/android/moments/data/l;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/twitter/android/moments/data/c",
            "<",
            "Lapb;",
            "Lcbi",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;>;",
            "Lcom/twitter/android/moments/data/l;",
            ")V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-wide p1, p0, Lcom/twitter/android/moments/data/w;->a:J

    .line 30
    iput-object p3, p0, Lcom/twitter/android/moments/data/w;->b:Lcom/twitter/android/moments/data/c;

    .line 31
    iput-object p4, p0, Lcom/twitter/android/moments/data/w;->c:Lcom/twitter/android/moments/data/l;

    .line 32
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Long;)Lrx/c;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    new-instance v0, Lapb$a;

    invoke-direct {v0}, Lapb$a;-><init>()V

    .line 46
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/twitter/library/provider/m$d;->a(J)Landroid/net/Uri;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/moments/data/w;->a:J

    invoke-static {v1, v2, v3}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lapb$a;->a(Landroid/net/Uri;)Lapb$a;

    move-result-object v0

    sget-object v1, Laxy;->a:[Ljava/lang/String;

    .line 48
    invoke-virtual {v0, v1}, Lapb$a;->b([Ljava/lang/String;)Lapb$a;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lapb$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapb;

    .line 50
    iget-object v1, p0, Lcom/twitter/android/moments/data/w;->b:Lcom/twitter/android/moments/data/c;

    invoke-virtual {v1}, Lcom/twitter/android/moments/data/c;->a()Lauj;

    move-result-object v1

    invoke-interface {v1, v0}, Lauj;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/data/w$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/data/w$1;-><init>(Lcom/twitter/android/moments/data/w;)V

    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/moments/data/w;->c:Lcom/twitter/android/moments/data/l;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/moments/data/l;->a(J)V

    .line 36
    return-void
.end method

.method public synthetic a_(Ljava/lang/Object;)Lrx/c;
    .locals 1

    .prologue
    .line 20
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/twitter/android/moments/data/w;->a(Ljava/lang/Long;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/data/w;->c:Lcom/twitter/android/moments/data/l;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/moments/data/l;->b(J)V

    .line 40
    return-void
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/android/moments/data/w;->b:Lcom/twitter/android/moments/data/c;

    invoke-virtual {v0}, Lcom/twitter/android/moments/data/c;->close()V

    .line 62
    return-void
.end method
