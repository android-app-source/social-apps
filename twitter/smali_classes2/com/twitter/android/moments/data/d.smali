.class public abstract Lcom/twitter/android/moments/data/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/util/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/p",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:Landroid/support/v4/app/LoaderManager;

.field private final c:I

.field private d:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private e:Z


# direct methods
.method protected constructor <init>(Landroid/support/v4/app/LoaderManager;I)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Lcom/twitter/util/p;

    invoke-direct {v0}, Lcom/twitter/util/p;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/moments/data/d;->a:Lcom/twitter/util/p;

    .line 28
    iput-object p1, p0, Lcom/twitter/android/moments/data/d;->b:Landroid/support/v4/app/LoaderManager;

    .line 29
    iput p2, p0, Lcom/twitter/android/moments/data/d;->c:I

    .line 30
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/android/moments/data/d;->a:Lcom/twitter/util/p;

    invoke-virtual {v0}, Lcom/twitter/util/p;->i()V

    .line 63
    iget-object v0, p0, Lcom/twitter/android/moments/data/d;->b:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/twitter/android/moments/data/d;->c:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/moments/data/d;->e:Z

    .line 65
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 83
    invoke-static {p2}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/data/d$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/data/d$2;-><init>(Lcom/twitter/android/moments/data/d;)V

    .line 84
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 90
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 91
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/data/d$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/data/d$1;-><init>(Lcom/twitter/android/moments/data/d;)V

    .line 92
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 98
    return-void
.end method

.method public a(Lcom/twitter/util/q;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/q",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/twitter/android/moments/data/d;->a:Lcom/twitter/util/p;

    invoke-virtual {v0, p1}, Lcom/twitter/util/p;->a(Lcom/twitter/util/q;)Z

    .line 38
    iget-boolean v0, p0, Lcom/twitter/android/moments/data/d;->e:Z

    if-nez v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/twitter/android/moments/data/d;->b:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/twitter/android/moments/data/d;->c:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/moments/data/d;->e:Z

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/data/d;->d:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 43
    iget-object v0, p0, Lcom/twitter/android/moments/data/d;->d:Ljava/lang/Object;

    invoke-interface {p1, v0}, Lcom/twitter/util/q;->onEvent(Ljava/lang/Object;)V

    .line 45
    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/util/q;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/q",
            "<TT;>;Z)V"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/android/moments/data/d;->a:Lcom/twitter/util/p;

    invoke-virtual {v0, p1}, Lcom/twitter/util/p;->b(Lcom/twitter/util/q;)Z

    .line 53
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/moments/data/d;->a:Lcom/twitter/util/p;

    invoke-virtual {v0}, Lcom/twitter/util/p;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    invoke-virtual {p0}, Lcom/twitter/android/moments/data/d;->a()V

    .line 56
    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/twitter/android/moments/data/d;->a:Lcom/twitter/util/p;

    invoke-virtual {v0, p1}, Lcom/twitter/util/p;->a(Ljava/lang/Object;)V

    .line 74
    iput-object p1, p0, Lcom/twitter/android/moments/data/d;->d:Ljava/lang/Object;

    .line 75
    return-void
.end method

.method public b()Lcom/twitter/util/p;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/p",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/twitter/android/moments/data/d;->a:Lcom/twitter/util/p;

    return-object v0
.end method

.method public b(Lcom/twitter/util/q;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/q",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 48
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/moments/data/d;->a(Lcom/twitter/util/q;Z)V

    .line 49
    return-void
.end method

.method public abstract c(Landroid/database/Cursor;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")TT;"
        }
    .end annotation
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/moments/data/d;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/moments/data/d;->d:Ljava/lang/Object;

    .line 103
    return-void
.end method
