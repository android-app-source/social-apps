.class public Lcom/twitter/android/moments/data/e;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/moments/data/e$a;
    }
.end annotation


# instance fields
.field private final a:Laae;

.field private final b:Lcom/twitter/android/moments/data/w;

.field private final c:Lzq;

.field private d:Lcom/twitter/android/moments/data/e$a;

.field private e:Lrx/j;


# direct methods
.method public constructor <init>(Laae;Lcom/twitter/android/moments/data/w;Lzq;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/twitter/android/moments/data/e;->a:Laae;

    .line 29
    iput-object p2, p0, Lcom/twitter/android/moments/data/e;->b:Lcom/twitter/android/moments/data/w;

    .line 30
    iput-object p3, p0, Lcom/twitter/android/moments/data/e;->c:Lzq;

    .line 31
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/data/e;)Laae;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/twitter/android/moments/data/e;->a:Laae;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/moments/data/e;Lcom/twitter/model/moments/Moment;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/data/e;->b(Lcom/twitter/model/moments/Moment;)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/moments/data/e;)Lcom/twitter/android/moments/data/w;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/twitter/android/moments/data/e;->b:Lcom/twitter/android/moments/data/w;

    return-object v0
.end method

.method private b(Lcom/twitter/model/moments/Moment;)V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/twitter/android/moments/data/e;->a:Laae;

    iget-boolean v1, p1, Lcom/twitter/model/moments/Moment;->k:Z

    invoke-virtual {v0, v1}, Laae;->a(Z)V

    .line 71
    iget-object v0, p0, Lcom/twitter/android/moments/data/e;->a:Laae;

    new-instance v1, Lcom/twitter/android/moments/data/e$2;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/moments/data/e$2;-><init>(Lcom/twitter/android/moments/data/e;Lcom/twitter/model/moments/Moment;)V

    invoke-virtual {v0, v1}, Laae;->a(Landroid/view/View$OnClickListener;)V

    .line 88
    return-void
.end method

.method static synthetic c(Lcom/twitter/android/moments/data/e;)Lzq;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/twitter/android/moments/data/e;->c:Lzq;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/moments/data/e;)Lcom/twitter/android/moments/data/e$a;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/twitter/android/moments/data/e;->d:Lcom/twitter/android/moments/data/e$a;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 91
    iget-object v0, p0, Lcom/twitter/android/moments/data/e;->e:Lrx/j;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/twitter/android/moments/data/e;->e:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 93
    iput-object v1, p0, Lcom/twitter/android/moments/data/e;->e:Lrx/j;

    .line 95
    :cond_0
    invoke-virtual {p0, v1}, Lcom/twitter/android/moments/data/e;->a(Lcom/twitter/android/moments/data/e$a;)V

    .line 96
    return-void
.end method

.method public a(Lcom/twitter/android/moments/data/e$a;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/twitter/android/moments/data/e;->d:Lcom/twitter/android/moments/data/e$a;

    .line 36
    return-void
.end method

.method public a(Lcom/twitter/model/moments/Moment;)V
    .locals 4

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/twitter/android/moments/data/e;->a()V

    .line 51
    invoke-static {p1}, Lbsd;->a(Lcom/twitter/model/moments/Moment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/twitter/android/moments/data/e;->a:Laae;

    invoke-virtual {v0}, Laae;->b()V

    .line 53
    invoke-direct {p0, p1}, Lcom/twitter/android/moments/data/e;->b(Lcom/twitter/model/moments/Moment;)V

    .line 55
    iget-object v0, p0, Lcom/twitter/android/moments/data/e;->b:Lcom/twitter/android/moments/data/w;

    iget-wide v2, p1, Lcom/twitter/model/moments/Moment;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/data/w;->a(Ljava/lang/Long;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/moments/data/e$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/moments/data/e$1;-><init>(Lcom/twitter/android/moments/data/e;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/moments/data/e;->e:Lrx/j;

    .line 67
    :goto_0
    return-void

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/moments/data/e;->a:Laae;

    invoke-virtual {v0}, Laae;->a()V

    goto :goto_0
.end method

.method public b()Lcom/twitter/ui/widget/ToggleTwitterButton;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/twitter/android/moments/data/e;->a:Laae;

    invoke-virtual {v0}, Laae;->c()Lcom/twitter/ui/widget/ToggleTwitterButton;

    move-result-object v0

    return-object v0
.end method
