.class Lcom/twitter/android/moments/data/e$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/moments/data/e;->b(Lcom/twitter/model/moments/Moment;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/model/moments/Moment;

.field final synthetic b:Lcom/twitter/android/moments/data/e;


# direct methods
.method constructor <init>(Lcom/twitter/android/moments/data/e;Lcom/twitter/model/moments/Moment;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/twitter/android/moments/data/e$2;->b:Lcom/twitter/android/moments/data/e;

    iput-object p2, p0, Lcom/twitter/android/moments/data/e$2;->a:Lcom/twitter/model/moments/Moment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/android/moments/data/e$2;->a:Lcom/twitter/model/moments/Moment;

    iget-boolean v0, v0, Lcom/twitter/model/moments/Moment;->k:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 75
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/moments/data/e$2;->b:Lcom/twitter/android/moments/data/e;

    invoke-static {v1}, Lcom/twitter/android/moments/data/e;->a(Lcom/twitter/android/moments/data/e;)Laae;

    move-result-object v1

    invoke-virtual {v1, v0}, Laae;->a(Z)V

    .line 76
    if-eqz v0, :cond_2

    .line 77
    iget-object v1, p0, Lcom/twitter/android/moments/data/e$2;->b:Lcom/twitter/android/moments/data/e;

    invoke-static {v1}, Lcom/twitter/android/moments/data/e;->b(Lcom/twitter/android/moments/data/e;)Lcom/twitter/android/moments/data/w;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/moments/data/e$2;->a:Lcom/twitter/model/moments/Moment;

    iget-wide v2, v2, Lcom/twitter/model/moments/Moment;->b:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/moments/data/w;->a(J)V

    .line 78
    iget-object v1, p0, Lcom/twitter/android/moments/data/e$2;->b:Lcom/twitter/android/moments/data/e;

    invoke-static {v1}, Lcom/twitter/android/moments/data/e;->c(Lcom/twitter/android/moments/data/e;)Lzq;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/moments/data/e$2;->a:Lcom/twitter/model/moments/Moment;

    iget-wide v2, v2, Lcom/twitter/model/moments/Moment;->b:J

    invoke-interface {v1, v2, v3}, Lzq;->a(J)V

    .line 83
    :goto_1
    iget-object v1, p0, Lcom/twitter/android/moments/data/e$2;->b:Lcom/twitter/android/moments/data/e;

    invoke-static {v1}, Lcom/twitter/android/moments/data/e;->d(Lcom/twitter/android/moments/data/e;)Lcom/twitter/android/moments/data/e$a;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 84
    iget-object v1, p0, Lcom/twitter/android/moments/data/e$2;->b:Lcom/twitter/android/moments/data/e;

    invoke-static {v1}, Lcom/twitter/android/moments/data/e;->d(Lcom/twitter/android/moments/data/e;)Lcom/twitter/android/moments/data/e$a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/moments/data/e$2;->a:Lcom/twitter/model/moments/Moment;

    iget-wide v2, v2, Lcom/twitter/model/moments/Moment;->b:J

    invoke-interface {v1, v2, v3, v0}, Lcom/twitter/android/moments/data/e$a;->a(JZ)V

    .line 86
    :cond_0
    return-void

    .line 74
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 80
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/moments/data/e$2;->b:Lcom/twitter/android/moments/data/e;

    invoke-static {v1}, Lcom/twitter/android/moments/data/e;->b(Lcom/twitter/android/moments/data/e;)Lcom/twitter/android/moments/data/w;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/moments/data/e$2;->a:Lcom/twitter/model/moments/Moment;

    iget-wide v2, v2, Lcom/twitter/model/moments/Moment;->b:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/moments/data/w;->b(J)V

    .line 81
    iget-object v1, p0, Lcom/twitter/android/moments/data/e$2;->b:Lcom/twitter/android/moments/data/e;

    invoke-static {v1}, Lcom/twitter/android/moments/data/e;->c(Lcom/twitter/android/moments/data/e;)Lzq;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/moments/data/e$2;->a:Lcom/twitter/model/moments/Moment;

    iget-wide v2, v2, Lcom/twitter/model/moments/Moment;->b:J

    invoke-interface {v1, v2, v3}, Lzq;->b(J)V

    goto :goto_1
.end method
