.class public Lcom/twitter/android/moments/data/ae;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ARGS:",
        "Ljava/lang/Object;",
        "TYPE:",
        "Ljava/lang/Object;",
        "MUTAB",
        "LE_TYPE::Lcom/twitter/model/moments/q",
        "<TTYPE;>;>",
        "Ljava/lang/Object;",
        "Ljava/io/Closeable;"
    }
.end annotation


# instance fields
.field private final a:Lauj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lauj",
            "<TARGS;TTYPE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/util/object/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/f",
            "<TTYPE;",
            "Lrx/c",
            "<TTYPE;>;TMUTAB",
            "LE_TYPE;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lauj;Lcom/twitter/util/object/f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lauj",
            "<TARGS;TTYPE;>;",
            "Lcom/twitter/util/object/f",
            "<TTYPE;",
            "Lrx/c",
            "<TTYPE;>;TMUTAB",
            "LE_TYPE;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/twitter/android/moments/data/ae;->a:Lauj;

    .line 40
    iput-object p2, p0, Lcom/twitter/android/moments/data/ae;->b:Lcom/twitter/util/object/f;

    .line 41
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/moments/data/ae;)Lcom/twitter/util/object/f;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/android/moments/data/ae;->b:Lcom/twitter/util/object/f;

    return-object v0
.end method

.method private a(Lrx/c;)Lrx/functions/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/c",
            "<TTYPE;>;)",
            "Lrx/functions/d",
            "<-TTYPE;TMUTAB",
            "LE_TYPE;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    new-instance v0, Lcom/twitter/android/moments/data/ae$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/moments/data/ae$1;-><init>(Lcom/twitter/android/moments/data/ae;Lrx/c;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TARGS;)",
            "Lrx/g",
            "<TMUTAB",
            "LE_TYPE;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/twitter/android/moments/data/ae;->a:Lauj;

    .line 51
    invoke-interface {v0, p1}, Lauj;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    .line 55
    invoke-static {}, Llb;->a()Llb;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/c$c;)Lrx/c;

    move-result-object v0

    .line 56
    const/4 v1, 0x1

    .line 57
    invoke-virtual {v0, v1}, Lrx/c;->d(I)Lrx/c;

    move-result-object v1

    .line 58
    invoke-virtual {v1}, Lrx/c;->b()Lrx/g;

    move-result-object v1

    .line 59
    invoke-direct {p0, v0}, Lcom/twitter/android/moments/data/ae;->a(Lrx/c;)Lrx/functions/d;

    move-result-object v0

    invoke-virtual {v1, v0}, Lrx/g;->c(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    .line 56
    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/twitter/android/moments/data/ae;->a:Lauj;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 76
    return-void
.end method
