.class public Lcom/twitter/android/AccountsDialogActivity;
.super Lcom/twitter/android/client/TwitterListActivity;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/AccountsDialogActivity$a;
    }
.end annotation


# instance fields
.field private b:Landroid/net/Uri;

.field private c:Lcom/twitter/android/AccountsDialogActivity$a;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/twitter/android/client/TwitterListActivity;-><init>()V

    return-void
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 163
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/app/main/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "AbsFragmentActivity_account_name"

    .line 164
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "page"

    .line 165
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x4000000

    .line 166
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 167
    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountsDialogActivity;->startActivity(Landroid/content/Intent;)V

    .line 168
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 188
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/LoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "add_account"

    const/4 v2, 0x1

    .line 189
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "authorize_account"

    iget-boolean v2, p0, Lcom/twitter/android/AccountsDialogActivity;->d:Z

    .line 190
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 191
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/AccountsDialogActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 192
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 195
    new-instance v1, Lcom/twitter/android/v$a;

    invoke-direct {v1}, Lcom/twitter/android/v$a;-><init>()V

    .line 196
    invoke-virtual {v1, v0}, Lcom/twitter/android/v$a;->a(Z)Lcom/twitter/android/v$a;

    move-result-object v1

    iget-boolean v2, p0, Lcom/twitter/android/AccountsDialogActivity;->d:Z

    .line 197
    invoke-virtual {v1, v2}, Lcom/twitter/android/v$a;->b(Z)Lcom/twitter/android/v$a;

    move-result-object v1

    iget-boolean v2, p0, Lcom/twitter/android/AccountsDialogActivity;->d:Z

    if-nez v2, :cond_0

    .line 198
    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/android/v$a;->c(Z)Lcom/twitter/android/v$a;

    move-result-object v0

    .line 199
    new-instance v1, Lcom/twitter/android/FlowActivity$a;

    invoke-direct {v1, p0}, Lcom/twitter/android/FlowActivity$a;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0}, Lcom/twitter/android/v$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lako;

    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/FlowActivity$a;->a(Lako;I)V

    .line 200
    return-void

    .line 197
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    .line 122
    packed-switch p1, :pswitch_data_0

    .line 145
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/client/TwitterListActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 149
    :goto_0
    return-void

    .line 125
    :pswitch_0
    const/4 v0, -0x1

    if-ne v0, p2, :cond_2

    if-eqz p3, :cond_2

    .line 126
    const-string/jumbo v0, "AbsFragmentActivity_account_name"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 127
    invoke-static {v0}, Lcom/twitter/library/util/b;->b(Ljava/lang/String;)Lakm;

    move-result-object v1

    .line 128
    if-nez v1, :cond_3

    const/4 v0, 0x0

    .line 129
    :goto_1
    invoke-static {p0}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    iget-wide v2, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 130
    invoke-static {p0, v2, v3}, Lcom/twitter/library/platform/notifications/PushRegistration;->c(Landroid/content/Context;J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 131
    iget-wide v2, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    sget v4, Lcga;->a:I

    const/4 v5, 0x0

    invoke-static {p0, v2, v3, v4, v5}, Lcom/twitter/library/platform/notifications/PushRegistration;->b(Landroid/content/Context;JIZ)V

    .line 133
    :cond_0
    iget-object v2, p0, Lcom/twitter/android/AccountsDialogActivity;->b:Landroid/net/Uri;

    if-eqz v2, :cond_1

    .line 134
    iget-object v2, p0, Lcom/twitter/android/AccountsDialogActivity;->b:Landroid/net/Uri;

    const-string/jumbo v3, "AbsFragmentActivity_account_name"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/twitter/android/AccountsDialogActivity;->a(Landroid/net/Uri;Ljava/lang/String;)V

    .line 136
    :cond_1
    iget-boolean v2, p0, Lcom/twitter/android/AccountsDialogActivity;->d:Z

    if-eqz v2, :cond_2

    if-eqz v0, :cond_2

    .line 137
    new-instance v2, Lcom/twitter/model/account/UserAccount;

    invoke-virtual {v1}, Lakm;->a()Landroid/accounts/Account;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Lcom/twitter/model/account/UserAccount;-><init>(Landroid/accounts/Account;Lcom/twitter/model/core/TwitterUser;)V

    .line 138
    const/4 v0, 0x1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v3, "account"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/AccountsDialogActivity;->setResult(ILandroid/content/Intent;)V

    .line 141
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/AccountsDialogActivity;->finish()V

    goto :goto_0

    .line 128
    :cond_3
    invoke-static {v1}, Lcom/twitter/library/util/b;->c(Lakm;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    goto :goto_1

    .line 122
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 172
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 185
    :goto_0
    return-void

    .line 174
    :sswitch_0
    invoke-direct {p0}, Lcom/twitter/android/AccountsDialogActivity;->e()V

    goto :goto_0

    .line 178
    :sswitch_1
    invoke-direct {p0}, Lcom/twitter/android/AccountsDialogActivity;->d()V

    goto :goto_0

    .line 172
    :sswitch_data_0
    .sparse-switch
        0x7f13000c -> :sswitch_1
        0x7f13005b -> :sswitch_0
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 77
    invoke-super {p0, p1}, Lcom/twitter/android/client/TwitterListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 79
    invoke-virtual {p0}, Lcom/twitter/android/AccountsDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 81
    const-string/jumbo v0, "authorize_account"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/AccountsDialogActivity;->d:Z

    .line 84
    const-string/jumbo v0, "page"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/AccountsDialogActivity;->b:Landroid/net/Uri;

    .line 87
    const-string/jumbo v0, "AccountsDialogActivity_add_account"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    invoke-direct {p0}, Lcom/twitter/android/AccountsDialogActivity;->d()V

    .line 109
    :goto_0
    invoke-static {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b(Landroid/content/Context;)V

    .line 110
    return-void

    .line 89
    :cond_0
    const-string/jumbo v0, "AccountsDialogActivity_new_account"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    invoke-direct {p0}, Lcom/twitter/android/AccountsDialogActivity;->e()V

    goto :goto_0

    .line 92
    :cond_1
    const v0, 0x7f04001d

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountsDialogActivity;->setContentView(I)V

    .line 94
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v0

    invoke-virtual {v0}, Lakn;->c()Ljava/util/List;

    move-result-object v0

    .line 95
    invoke-static {v0}, Lcom/twitter/library/util/b;->a(Ljava/util/List;)[Lcom/twitter/model/account/UserAccount;

    move-result-object v0

    .line 96
    const-string/jumbo v2, "AccountsDialogActivity_account_name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 97
    new-instance v3, Lcom/twitter/android/AccountsDialogActivity$a;

    invoke-direct {v3, p0, v0, v2}, Lcom/twitter/android/AccountsDialogActivity$a;-><init>(Landroid/content/Context;[Lcom/twitter/model/account/UserAccount;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/twitter/android/AccountsDialogActivity;->c:Lcom/twitter/android/AccountsDialogActivity$a;

    .line 98
    invoke-virtual {p0}, Lcom/twitter/android/AccountsDialogActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/AccountsDialogActivity;->c:Lcom/twitter/android/AccountsDialogActivity$a;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 100
    const v0, 0x7f130144

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountsDialogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 101
    const-string/jumbo v2, "AccountsDialogActivity_switch_only"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 102
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 104
    :cond_2
    const v1, 0x7f13005b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    const v1, 0x7f13000c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 4

    .prologue
    .line 153
    iget-object v0, p0, Lcom/twitter/android/AccountsDialogActivity;->c:Lcom/twitter/android/AccountsDialogActivity$a;

    invoke-virtual {v0, p3}, Lcom/twitter/android/AccountsDialogActivity$a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/account/UserAccount;

    .line 156
    invoke-virtual {p0}, Lcom/twitter/android/AccountsDialogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/util/v;->a(Landroid/content/Context;)Lcom/twitter/library/util/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/util/v;->a()V

    .line 158
    const/4 v1, 0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v3, "account"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/AccountsDialogActivity;->setResult(ILandroid/content/Intent;)V

    .line 159
    invoke-virtual {p0}, Lcom/twitter/android/AccountsDialogActivity;->finish()V

    .line 160
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 114
    invoke-super {p0}, Lcom/twitter/android/client/TwitterListActivity;->onResume()V

    .line 115
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    invoke-static {p0}, Lcom/twitter/android/DispatchActivity;->a(Landroid/app/Activity;)V

    .line 118
    :cond_0
    return-void
.end method
