.class public Lcom/twitter/android/bt$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/bt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public final a:Lcom/twitter/android/cu;

.field public final b:Lcom/twitter/android/db;

.field public final c:Lcom/twitter/android/bt$b;

.field public final d:Landroid/support/v4/view/ViewPager;

.field public final e:Lcom/twitter/android/widget/PipView;

.field public final f:Landroid/widget/TextView;

.field public final g:Landroid/widget/TextView;

.field public final h:Lcom/twitter/internal/android/widget/HorizontalListView;

.field public final i:Landroid/view/View;

.field public final j:Lcom/twitter/android/widget/TopicView;

.field public final k:Lcom/twitter/android/widget/TextSwitcherView;

.field public final l:Landroid/widget/ImageButton;

.field public m:Lcom/twitter/android/bu;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1002
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1003
    iput-object v0, p0, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    .line 1004
    iput-object v0, p0, Lcom/twitter/android/bt$a;->b:Lcom/twitter/android/db;

    .line 1005
    iput-object v0, p0, Lcom/twitter/android/bt$a;->c:Lcom/twitter/android/bt$b;

    .line 1006
    iput-object v0, p0, Lcom/twitter/android/bt$a;->d:Landroid/support/v4/view/ViewPager;

    .line 1007
    iput-object v0, p0, Lcom/twitter/android/bt$a;->e:Lcom/twitter/android/widget/PipView;

    .line 1008
    iput-object v0, p0, Lcom/twitter/android/bt$a;->f:Landroid/widget/TextView;

    .line 1009
    iput-object v0, p0, Lcom/twitter/android/bt$a;->g:Landroid/widget/TextView;

    .line 1010
    iput-object v0, p0, Lcom/twitter/android/bt$a;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    .line 1011
    iput-object v0, p0, Lcom/twitter/android/bt$a;->i:Landroid/view/View;

    .line 1012
    iput-object v0, p0, Lcom/twitter/android/bt$a;->j:Lcom/twitter/android/widget/TopicView;

    .line 1013
    iput-object v0, p0, Lcom/twitter/android/bt$a;->k:Lcom/twitter/android/widget/TextSwitcherView;

    .line 1014
    iput-object v0, p0, Lcom/twitter/android/bt$a;->l:Landroid/widget/ImageButton;

    .line 1015
    return-void
.end method

.method constructor <init>(Landroid/support/v4/view/ViewPager;Lcom/twitter/android/widget/PipView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1047
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1048
    iput-object v0, p0, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    .line 1049
    iput-object v0, p0, Lcom/twitter/android/bt$a;->b:Lcom/twitter/android/db;

    .line 1050
    iput-object v0, p0, Lcom/twitter/android/bt$a;->c:Lcom/twitter/android/bt$b;

    .line 1051
    iput-object p1, p0, Lcom/twitter/android/bt$a;->d:Landroid/support/v4/view/ViewPager;

    .line 1052
    iput-object p2, p0, Lcom/twitter/android/bt$a;->e:Lcom/twitter/android/widget/PipView;

    .line 1053
    iput-object v0, p0, Lcom/twitter/android/bt$a;->f:Landroid/widget/TextView;

    .line 1054
    iput-object v0, p0, Lcom/twitter/android/bt$a;->g:Landroid/widget/TextView;

    .line 1055
    iput-object v0, p0, Lcom/twitter/android/bt$a;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    .line 1056
    iput-object v0, p0, Lcom/twitter/android/bt$a;->i:Landroid/view/View;

    .line 1057
    iput-object v0, p0, Lcom/twitter/android/bt$a;->j:Lcom/twitter/android/widget/TopicView;

    .line 1058
    iput-object v0, p0, Lcom/twitter/android/bt$a;->k:Lcom/twitter/android/widget/TextSwitcherView;

    .line 1059
    iput-object v0, p0, Lcom/twitter/android/bt$a;->l:Landroid/widget/ImageButton;

    .line 1060
    return-void
.end method

.method constructor <init>(Landroid/widget/TextView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1062
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1063
    iput-object v0, p0, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    .line 1064
    iput-object v0, p0, Lcom/twitter/android/bt$a;->b:Lcom/twitter/android/db;

    .line 1065
    iput-object v0, p0, Lcom/twitter/android/bt$a;->c:Lcom/twitter/android/bt$b;

    .line 1066
    iput-object v0, p0, Lcom/twitter/android/bt$a;->d:Landroid/support/v4/view/ViewPager;

    .line 1067
    iput-object v0, p0, Lcom/twitter/android/bt$a;->e:Lcom/twitter/android/widget/PipView;

    .line 1068
    iput-object p1, p0, Lcom/twitter/android/bt$a;->f:Landroid/widget/TextView;

    .line 1069
    iput-object v0, p0, Lcom/twitter/android/bt$a;->g:Landroid/widget/TextView;

    .line 1070
    iput-object v0, p0, Lcom/twitter/android/bt$a;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    .line 1071
    iput-object v0, p0, Lcom/twitter/android/bt$a;->i:Landroid/view/View;

    .line 1072
    iput-object v0, p0, Lcom/twitter/android/bt$a;->j:Lcom/twitter/android/widget/TopicView;

    .line 1073
    iput-object v0, p0, Lcom/twitter/android/bt$a;->k:Lcom/twitter/android/widget/TextSwitcherView;

    .line 1074
    iput-object v0, p0, Lcom/twitter/android/bt$a;->l:Landroid/widget/ImageButton;

    .line 1075
    return-void
.end method

.method constructor <init>(Landroid/widget/TextView;Landroid/view/View;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1108
    iput-object v0, p0, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    .line 1109
    iput-object v0, p0, Lcom/twitter/android/bt$a;->b:Lcom/twitter/android/db;

    .line 1110
    iput-object v0, p0, Lcom/twitter/android/bt$a;->c:Lcom/twitter/android/bt$b;

    .line 1111
    iput-object v0, p0, Lcom/twitter/android/bt$a;->d:Landroid/support/v4/view/ViewPager;

    .line 1112
    iput-object v0, p0, Lcom/twitter/android/bt$a;->e:Lcom/twitter/android/widget/PipView;

    .line 1113
    iput-object p1, p0, Lcom/twitter/android/bt$a;->f:Landroid/widget/TextView;

    .line 1114
    iput-object v0, p0, Lcom/twitter/android/bt$a;->g:Landroid/widget/TextView;

    .line 1115
    iput-object v0, p0, Lcom/twitter/android/bt$a;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    .line 1116
    iput-object p2, p0, Lcom/twitter/android/bt$a;->i:Landroid/view/View;

    .line 1117
    iput-object v0, p0, Lcom/twitter/android/bt$a;->j:Lcom/twitter/android/widget/TopicView;

    .line 1118
    iput-object v0, p0, Lcom/twitter/android/bt$a;->k:Lcom/twitter/android/widget/TextSwitcherView;

    .line 1119
    iput-object v0, p0, Lcom/twitter/android/bt$a;->l:Landroid/widget/ImageButton;

    .line 1120
    return-void
.end method

.method constructor <init>(Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1077
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1078
    iput-object v0, p0, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    .line 1079
    iput-object v0, p0, Lcom/twitter/android/bt$a;->b:Lcom/twitter/android/db;

    .line 1080
    iput-object v0, p0, Lcom/twitter/android/bt$a;->c:Lcom/twitter/android/bt$b;

    .line 1081
    iput-object v0, p0, Lcom/twitter/android/bt$a;->d:Landroid/support/v4/view/ViewPager;

    .line 1082
    iput-object v0, p0, Lcom/twitter/android/bt$a;->e:Lcom/twitter/android/widget/PipView;

    .line 1083
    iput-object p1, p0, Lcom/twitter/android/bt$a;->f:Landroid/widget/TextView;

    .line 1084
    iput-object p2, p0, Lcom/twitter/android/bt$a;->g:Landroid/widget/TextView;

    .line 1085
    iput-object v0, p0, Lcom/twitter/android/bt$a;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    .line 1086
    iput-object v0, p0, Lcom/twitter/android/bt$a;->i:Landroid/view/View;

    .line 1087
    iput-object v0, p0, Lcom/twitter/android/bt$a;->j:Lcom/twitter/android/widget/TopicView;

    .line 1088
    iput-object v0, p0, Lcom/twitter/android/bt$a;->k:Lcom/twitter/android/widget/TextSwitcherView;

    .line 1089
    iput-object v0, p0, Lcom/twitter/android/bt$a;->l:Landroid/widget/ImageButton;

    .line 1090
    return-void
.end method

.method constructor <init>(Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/ImageButton;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1093
    iput-object v0, p0, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    .line 1094
    iput-object v0, p0, Lcom/twitter/android/bt$a;->b:Lcom/twitter/android/db;

    .line 1095
    iput-object v0, p0, Lcom/twitter/android/bt$a;->c:Lcom/twitter/android/bt$b;

    .line 1096
    iput-object v0, p0, Lcom/twitter/android/bt$a;->d:Landroid/support/v4/view/ViewPager;

    .line 1097
    iput-object v0, p0, Lcom/twitter/android/bt$a;->e:Lcom/twitter/android/widget/PipView;

    .line 1098
    iput-object p1, p0, Lcom/twitter/android/bt$a;->f:Landroid/widget/TextView;

    .line 1099
    iput-object p2, p0, Lcom/twitter/android/bt$a;->g:Landroid/widget/TextView;

    .line 1100
    iput-object v0, p0, Lcom/twitter/android/bt$a;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    .line 1101
    iput-object v0, p0, Lcom/twitter/android/bt$a;->i:Landroid/view/View;

    .line 1102
    iput-object v0, p0, Lcom/twitter/android/bt$a;->j:Lcom/twitter/android/widget/TopicView;

    .line 1103
    iput-object v0, p0, Lcom/twitter/android/bt$a;->k:Lcom/twitter/android/widget/TextSwitcherView;

    .line 1104
    iput-object p3, p0, Lcom/twitter/android/bt$a;->l:Landroid/widget/ImageButton;

    .line 1105
    return-void
.end method

.method constructor <init>(Lcom/twitter/android/cu;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1017
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1018
    iput-object p1, p0, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    .line 1019
    iput-object v0, p0, Lcom/twitter/android/bt$a;->b:Lcom/twitter/android/db;

    .line 1020
    iput-object v0, p0, Lcom/twitter/android/bt$a;->c:Lcom/twitter/android/bt$b;

    .line 1021
    iput-object v0, p0, Lcom/twitter/android/bt$a;->d:Landroid/support/v4/view/ViewPager;

    .line 1022
    iput-object v0, p0, Lcom/twitter/android/bt$a;->e:Lcom/twitter/android/widget/PipView;

    .line 1023
    iput-object v0, p0, Lcom/twitter/android/bt$a;->f:Landroid/widget/TextView;

    .line 1024
    iput-object v0, p0, Lcom/twitter/android/bt$a;->g:Landroid/widget/TextView;

    .line 1025
    iput-object v0, p0, Lcom/twitter/android/bt$a;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    .line 1026
    iput-object v0, p0, Lcom/twitter/android/bt$a;->i:Landroid/view/View;

    .line 1027
    iput-object v0, p0, Lcom/twitter/android/bt$a;->j:Lcom/twitter/android/widget/TopicView;

    .line 1028
    iput-object v0, p0, Lcom/twitter/android/bt$a;->k:Lcom/twitter/android/widget/TextSwitcherView;

    .line 1029
    iput-object v0, p0, Lcom/twitter/android/bt$a;->l:Landroid/widget/ImageButton;

    .line 1030
    return-void
.end method

.method constructor <init>(Lcom/twitter/android/db;Lcom/twitter/android/bt$b;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1032
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1033
    iput-object v0, p0, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    .line 1034
    iput-object p1, p0, Lcom/twitter/android/bt$a;->b:Lcom/twitter/android/db;

    .line 1035
    iput-object p2, p0, Lcom/twitter/android/bt$a;->c:Lcom/twitter/android/bt$b;

    .line 1036
    iput-object v0, p0, Lcom/twitter/android/bt$a;->d:Landroid/support/v4/view/ViewPager;

    .line 1037
    iput-object v0, p0, Lcom/twitter/android/bt$a;->e:Lcom/twitter/android/widget/PipView;

    .line 1038
    iput-object v0, p0, Lcom/twitter/android/bt$a;->f:Landroid/widget/TextView;

    .line 1039
    iput-object v0, p0, Lcom/twitter/android/bt$a;->g:Landroid/widget/TextView;

    .line 1040
    iput-object v0, p0, Lcom/twitter/android/bt$a;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    .line 1041
    iput-object v0, p0, Lcom/twitter/android/bt$a;->i:Landroid/view/View;

    .line 1042
    iput-object v0, p0, Lcom/twitter/android/bt$a;->j:Lcom/twitter/android/widget/TopicView;

    .line 1043
    iput-object v0, p0, Lcom/twitter/android/bt$a;->k:Lcom/twitter/android/widget/TextSwitcherView;

    .line 1044
    iput-object v0, p0, Lcom/twitter/android/bt$a;->l:Landroid/widget/ImageButton;

    .line 1045
    return-void
.end method

.method constructor <init>(Lcom/twitter/android/widget/TopicView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1138
    iput-object v0, p0, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    .line 1139
    iput-object v0, p0, Lcom/twitter/android/bt$a;->b:Lcom/twitter/android/db;

    .line 1140
    iput-object v0, p0, Lcom/twitter/android/bt$a;->c:Lcom/twitter/android/bt$b;

    .line 1141
    iput-object v0, p0, Lcom/twitter/android/bt$a;->d:Landroid/support/v4/view/ViewPager;

    .line 1142
    iput-object v0, p0, Lcom/twitter/android/bt$a;->e:Lcom/twitter/android/widget/PipView;

    .line 1143
    iput-object v0, p0, Lcom/twitter/android/bt$a;->f:Landroid/widget/TextView;

    .line 1144
    iput-object v0, p0, Lcom/twitter/android/bt$a;->g:Landroid/widget/TextView;

    .line 1145
    iput-object v0, p0, Lcom/twitter/android/bt$a;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    .line 1146
    iput-object v0, p0, Lcom/twitter/android/bt$a;->i:Landroid/view/View;

    .line 1147
    iput-object p1, p0, Lcom/twitter/android/bt$a;->j:Lcom/twitter/android/widget/TopicView;

    .line 1148
    iput-object v0, p0, Lcom/twitter/android/bt$a;->k:Lcom/twitter/android/widget/TextSwitcherView;

    .line 1149
    iput-object v0, p0, Lcom/twitter/android/bt$a;->l:Landroid/widget/ImageButton;

    .line 1150
    return-void
.end method

.method constructor <init>(Lcom/twitter/internal/android/widget/HorizontalListView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1123
    iput-object v0, p0, Lcom/twitter/android/bt$a;->a:Lcom/twitter/android/cu;

    .line 1124
    iput-object v0, p0, Lcom/twitter/android/bt$a;->b:Lcom/twitter/android/db;

    .line 1125
    iput-object v0, p0, Lcom/twitter/android/bt$a;->c:Lcom/twitter/android/bt$b;

    .line 1126
    iput-object v0, p0, Lcom/twitter/android/bt$a;->d:Landroid/support/v4/view/ViewPager;

    .line 1127
    iput-object v0, p0, Lcom/twitter/android/bt$a;->e:Lcom/twitter/android/widget/PipView;

    .line 1128
    iput-object v0, p0, Lcom/twitter/android/bt$a;->f:Landroid/widget/TextView;

    .line 1129
    iput-object v0, p0, Lcom/twitter/android/bt$a;->g:Landroid/widget/TextView;

    .line 1130
    iput-object p1, p0, Lcom/twitter/android/bt$a;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    .line 1131
    iput-object v0, p0, Lcom/twitter/android/bt$a;->i:Landroid/view/View;

    .line 1132
    iput-object v0, p0, Lcom/twitter/android/bt$a;->j:Lcom/twitter/android/widget/TopicView;

    .line 1133
    iput-object v0, p0, Lcom/twitter/android/bt$a;->k:Lcom/twitter/android/widget/TextSwitcherView;

    .line 1134
    iput-object v0, p0, Lcom/twitter/android/bt$a;->l:Landroid/widget/ImageButton;

    .line 1135
    return-void
.end method
