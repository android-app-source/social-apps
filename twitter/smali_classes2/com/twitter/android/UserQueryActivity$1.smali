.class Lcom/twitter/android/UserQueryActivity$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/cz$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/UserQueryActivity;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/UserQueryActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/UserQueryActivity;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/twitter/android/UserQueryActivity$1;->a:Lcom/twitter/android/UserQueryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/TwitterUser;)V
    .locals 4

    .prologue
    .line 73
    iget-object v0, p0, Lcom/twitter/android/UserQueryActivity$1;->a:Lcom/twitter/android/UserQueryActivity;

    iget-boolean v0, v0, Lcom/twitter/android/UserQueryActivity;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/UserQueryActivity$1;->a:Lcom/twitter/android/UserQueryActivity;

    invoke-virtual {v0}, Lcom/twitter/android/UserQueryActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/twitter/model/core/TwitterUser;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 78
    iget-object v0, p0, Lcom/twitter/android/UserQueryActivity$1;->a:Lcom/twitter/android/UserQueryActivity;

    invoke-virtual {v0, p1}, Lcom/twitter/android/UserQueryActivity;->a(Lcom/twitter/model/core/TwitterUser;)V

    goto :goto_0

    .line 79
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/UserQueryActivity$1;->a:Lcom/twitter/android/UserQueryActivity;

    iget-wide v0, v0, Lcom/twitter/android/UserQueryActivity;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/UserQueryActivity$1;->a:Lcom/twitter/android/UserQueryActivity;

    iget-object v0, v0, Lcom/twitter/android/UserQueryActivity;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/UserQueryActivity$1;->a:Lcom/twitter/android/UserQueryActivity;

    invoke-virtual {v0}, Lcom/twitter/android/UserQueryActivity;->o_()V

    goto :goto_0
.end method
