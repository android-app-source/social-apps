.class public Lcom/twitter/android/ConnectContactsUploadHelperActivity;
.super Lcom/twitter/android/a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/twitter/android/a;-><init>()V

    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 51
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/app/users/AddressbookContactsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 52
    const-string/jumbo v1, "scribe_page_term"

    iget-object v2, p0, Lcom/twitter/android/ConnectContactsUploadHelperActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/app/common/base/h;->a(Landroid/content/Intent;Z)Landroid/content/Intent;

    .line 54
    invoke-virtual {p0, v0}, Lcom/twitter/android/ConnectContactsUploadHelperActivity;->startActivity(Landroid/content/Intent;)V

    .line 55
    invoke-virtual {p0}, Lcom/twitter/android/ConnectContactsUploadHelperActivity;->finish()V

    .line 56
    return-void
.end method


# virtual methods
.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 21
    invoke-super {p0, p1, p2}, Lcom/twitter/android/a;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 23
    invoke-virtual {p0}, Lcom/twitter/android/ConnectContactsUploadHelperActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;->a(Landroid/content/Intent;)Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ConnectContactsUploadHelperActivity;->a:Ljava/lang/String;

    .line 25
    if-nez p1, :cond_0

    .line 26
    invoke-static {}, Lcom/twitter/android/ContactsUploadService;->c()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/twitter/android/ContactsUploadService;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 27
    invoke-static {p0, v1}, Lbmp;->a(Landroid/content/Context;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 28
    iget-object v0, p0, Lcom/twitter/android/ConnectContactsUploadHelperActivity;->a:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/twitter/android/ContactsUploadService;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 29
    invoke-direct {p0}, Lcom/twitter/android/ConnectContactsUploadHelperActivity;->i()V

    .line 39
    :cond_0
    :goto_0
    return-void

    .line 31
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ConnectContactsUploadHelperActivity;->a:Ljava/lang/String;

    invoke-virtual {p0, v1, v0, v1}, Lcom/twitter/android/ConnectContactsUploadHelperActivity;->a(ILjava/lang/String;Z)V

    goto :goto_0

    .line 33
    :cond_2
    invoke-static {p0, v1}, Lbmp;->a(Landroid/content/Context;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 34
    iget-object v0, p0, Lcom/twitter/android/ConnectContactsUploadHelperActivity;->a:Ljava/lang/String;

    invoke-virtual {p0, v1, v0, v1}, Lcom/twitter/android/ConnectContactsUploadHelperActivity;->a(ILjava/lang/String;Z)V

    goto :goto_0

    .line 36
    :cond_3
    invoke-direct {p0}, Lcom/twitter/android/ConnectContactsUploadHelperActivity;->i()V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 43
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/a;->onActivityResult(IILandroid/content/Intent;)V

    .line 44
    if-ne p1, v1, :cond_0

    .line 45
    iget-object v0, p0, Lcom/twitter/android/ConnectContactsUploadHelperActivity;->a:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/twitter/android/ContactsUploadService;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    .line 47
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/ConnectContactsUploadHelperActivity;->i()V

    .line 48
    return-void
.end method
