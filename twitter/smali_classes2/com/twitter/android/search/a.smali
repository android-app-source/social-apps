.class public Lcom/twitter/android/search/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/client/s;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lagg;

.field private final c:Laht;


# direct methods
.method constructor <init>(Lcom/twitter/app/common/base/BaseFragmentActivity;Lagg;Laht;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/twitter/android/search/a;->a:Landroid/app/Activity;

    .line 49
    iput-object p2, p0, Lcom/twitter/android/search/a;->b:Lagg;

    .line 50
    iput-object p3, p0, Lcom/twitter/android/search/a;->c:Laht;

    .line 51
    return-void
.end method

.method public static a(Lcom/twitter/app/common/base/BaseFragmentActivity;)Lcom/twitter/android/client/s;
    .locals 3

    .prologue
    .line 39
    invoke-static {p0}, Lagg;->a(Landroid/app/Activity;)Lagg;

    move-result-object v0

    .line 40
    new-instance v1, Laht;

    .line 41
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v1, v2}, Laht;-><init>(Lcom/twitter/library/client/Session;)V

    .line 42
    new-instance v2, Lcom/twitter/android/search/a;

    invoke-direct {v2, p0, v0, v1}, Lcom/twitter/android/search/a;-><init>(Lcom/twitter/app/common/base/BaseFragmentActivity;Lagg;Laht;)V

    return-object v2
.end method

.method private static a(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;)Z
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->c()Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->g:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    if-eq v0, v1, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->c()Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->h:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 87
    :goto_0
    return v0

    .line 88
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;IILjava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lage;
    .locals 6

    .prologue
    .line 95
    new-instance v0, Lage$a;

    iget-object v1, p0, Lcom/twitter/android/search/a;->a:Landroid/app/Activity;

    invoke-virtual {p1}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->e()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lage$a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 96
    invoke-virtual {p1}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lage$a;->d(Ljava/lang/String;)Lage$a;

    move-result-object v0

    .line 97
    invoke-virtual {p1}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lage$a;->a(Ljava/lang/String;)Lage$a;

    move-result-object v0

    .line 98
    invoke-virtual {v0, p4}, Lage$a;->c(Ljava/lang/String;)Lage$a;

    move-result-object v0

    .line 99
    invoke-virtual {v0, p2}, Lage$a;->b(I)Lage$a;

    move-result-object v0

    .line 100
    invoke-virtual {v0, p5}, Lage$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lage$a;

    move-result-object v0

    .line 101
    invoke-virtual {p1}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->h()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 102
    invoke-virtual {p1}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->h()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lage$a;->a(J)Lage$a;

    .line 104
    :cond_0
    sget-object v1, Lcom/twitter/android/search/a$1;->a:[I

    invoke-virtual {p1}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->c()Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 125
    :goto_0
    const/4 v1, -0x1

    if-eq p3, v1, :cond_1

    .line 126
    invoke-virtual {v0, p3}, Lage$a;->a(I)Lage$a;

    .line 128
    :cond_1
    invoke-virtual {v0}, Lage$a;->a()Lage;

    move-result-object v0

    return-object v0

    .line 106
    :pswitch_0
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lage$a;->a(I)Lage$a;

    goto :goto_0

    .line 110
    :pswitch_1
    check-cast p1, Lcom/twitter/model/search/viewmodel/c;

    .line 111
    invoke-virtual {p1}, Lcom/twitter/model/search/viewmodel/c;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lage$a;->a(I)Lage$a;

    goto :goto_0

    .line 118
    :pswitch_2
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lage$a;->a(Ljava/lang/String;)Lage$a;

    goto :goto_0

    .line 104
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;IILjava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 5

    .prologue
    .line 61
    .line 62
    invoke-direct/range {p0 .. p5}, Lcom/twitter/android/search/a;->b(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;IILjava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lage;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lage;->i()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 77
    invoke-static {p1}, Lcom/twitter/android/search/a;->a(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 79
    iget-object v1, p0, Lcom/twitter/android/search/a;->b:Lagg;

    invoke-virtual {v1, v0}, Lagg;->a(Lage;)V

    .line 84
    :cond_0
    :goto_0
    return-void

    .line 70
    :pswitch_0
    iget-object v1, p0, Lcom/twitter/android/search/a;->c:Laht;

    invoke-virtual {v1, v0}, Laht;->a(Lage;)V

    .line 71
    iget-object v1, p0, Lcom/twitter/android/search/a;->a:Landroid/app/Activity;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/twitter/android/search/a;->a:Landroid/app/Activity;

    const-class v4, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "screen_name"

    .line 72
    invoke-virtual {v0}, Lage;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "association"

    .line 73
    invoke-virtual {v0}, Lage;->e()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 71
    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 65
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ljava/lang/String;ILcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 2

    .prologue
    .line 137
    new-instance v0, Lage$a;

    iget-object v1, p0, Lcom/twitter/android/search/a;->a:Landroid/app/Activity;

    invoke-direct {v0, v1, p1}, Lage$a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const-string/jumbo v1, "com.twitter.android.action.SEARCH"

    .line 138
    invoke-virtual {v0, v1}, Lage$a;->d(Ljava/lang/String;)Lage$a;

    move-result-object v0

    .line 139
    invoke-virtual {v0, p1}, Lage$a;->c(Ljava/lang/String;)Lage$a;

    move-result-object v0

    .line 140
    invoke-virtual {v0, p1}, Lage$a;->a(Ljava/lang/String;)Lage$a;

    move-result-object v0

    const-string/jumbo v1, "typed_query"

    .line 141
    invoke-virtual {v0, v1}, Lage$a;->b(Ljava/lang/String;)Lage$a;

    move-result-object v0

    .line 142
    invoke-virtual {v0, p3}, Lage$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lage$a;

    move-result-object v0

    .line 143
    const/4 v1, -0x1

    if-eq p2, v1, :cond_0

    .line 144
    invoke-virtual {v0, p2}, Lage$a;->a(I)Lage$a;

    .line 146
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/search/a;->b:Lagg;

    invoke-virtual {v0}, Lage$a;->a()Lage;

    move-result-object v0

    invoke-virtual {v1, v0}, Lagg;->a(Lage;)V

    .line 147
    return-void
.end method
