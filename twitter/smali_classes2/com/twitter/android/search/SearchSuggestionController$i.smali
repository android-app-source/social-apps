.class public Lcom/twitter/android/search/SearchSuggestionController$i;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/av;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/search/SearchSuggestionController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "i"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/android/av",
        "<",
        "Landroid/view/View;",
        "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/search/SearchSuggestionController;


# direct methods
.method public constructor <init>(Lcom/twitter/android/search/SearchSuggestionController;)V
    .locals 0

    .prologue
    .line 607
    iput-object p1, p0, Lcom/twitter/android/search/SearchSuggestionController$i;->a:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 611
    if-eqz p2, :cond_0

    .line 612
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController$i;->a:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-static {v0}, Lcom/twitter/android/search/SearchSuggestionController;->e(Lcom/twitter/android/search/SearchSuggestionController;)Ljava/lang/String;

    move-result-object v1

    .line 613
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController$i;->a:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-static {v0}, Lcom/twitter/android/search/SearchSuggestionController;->f(Lcom/twitter/android/search/SearchSuggestionController;)Lcom/twitter/android/search/SearchSuggestionController$c;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/search/SearchSuggestionController$c;

    iget-object v2, p2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Lcom/twitter/android/search/SearchSuggestionController$c;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614
    iput-object v1, p2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->w:Ljava/lang/String;

    .line 615
    const-string/jumbo v0, "position"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    .line 616
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController$i;->a:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-static {v0}, Lcom/twitter/android/search/SearchSuggestionController;->g(Lcom/twitter/android/search/SearchSuggestionController;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 619
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 607
    check-cast p2, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/search/SearchSuggestionController$i;->a(Landroid/view/View;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Landroid/os/Bundle;)V

    return-void
.end method
