.class public Lcom/twitter/android/search/i;
.super Lcom/twitter/android/moments/data/d;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/moments/data/d",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/util/object/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/d",
            "<",
            "Ljava/lang/String;",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Landroid/support/v4/app/LoaderManager;

.field private final c:Lcom/twitter/util/object/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/d",
            "<",
            "Ljava/lang/String;",
            "Lagx;",
            ">;"
        }
    .end annotation
.end field

.field private final d:I

.field private final e:Landroid/content/res/Resources;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/twitter/util/object/d;Lcom/twitter/util/object/d;Landroid/support/v4/app/LoaderManager;ILandroid/content/res/Resources;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/object/d",
            "<",
            "Ljava/lang/String;",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;>;",
            "Lcom/twitter/util/object/d",
            "<",
            "Ljava/lang/String;",
            "Lagx;",
            ">;",
            "Landroid/support/v4/app/LoaderManager;",
            "I",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p3, p4}, Lcom/twitter/android/moments/data/d;-><init>(Landroid/support/v4/app/LoaderManager;I)V

    .line 39
    iput-object p1, p0, Lcom/twitter/android/search/i;->a:Lcom/twitter/util/object/d;

    .line 40
    iput-object p2, p0, Lcom/twitter/android/search/i;->c:Lcom/twitter/util/object/d;

    .line 41
    iput-object p3, p0, Lcom/twitter/android/search/i;->b:Landroid/support/v4/app/LoaderManager;

    .line 42
    iput p4, p0, Lcom/twitter/android/search/i;->d:I

    .line 43
    iput-object p5, p0, Lcom/twitter/android/search/i;->e:Landroid/content/res/Resources;

    .line 44
    return-void
.end method

.method private a(I)Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;
    .locals 1

    .prologue
    .line 74
    packed-switch p1, :pswitch_data_0

    .line 80
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 77
    :pswitch_0
    new-instance v0, Lcom/twitter/model/search/viewmodel/b;

    invoke-direct {v0}, Lcom/twitter/model/search/viewmodel/b;-><init>()V

    goto :goto_0

    .line 74
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private b(I)Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;
    .locals 3

    .prologue
    .line 87
    packed-switch p1, :pswitch_data_0

    .line 97
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 89
    :pswitch_0
    new-instance v0, Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem;

    iget-object v1, p0, Lcom/twitter/android/search/i;->e:Landroid/content/res/Resources;

    const v2, 0x7f0a07bc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem$HeaderType;->b:Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem$HeaderType;

    invoke-direct {v0, v1, v2}, Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem;-><init>(Ljava/lang/String;Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem$HeaderType;)V

    goto :goto_0

    .line 93
    :pswitch_1
    new-instance v0, Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem;

    iget-object v1, p0, Lcom/twitter/android/search/i;->e:Landroid/content/res/Resources;

    const v2, 0x7f0a0749

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem$HeaderType;->a:Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem$HeaderType;

    invoke-direct {v0, v1, v2}, Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem;-><init>(Ljava/lang/String;Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem$HeaderType;)V

    goto :goto_0

    .line 87
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lcom/twitter/android/search/i;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    iget-object v0, p0, Lcom/twitter/android/search/i;->c:Lcom/twitter/util/object/d;

    iget-object v1, p0, Lcom/twitter/android/search/i;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lagx;

    .line 56
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v3

    .line 57
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 58
    const/4 v1, -0x1

    .line 60
    :goto_0
    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 61
    if-eq v2, v1, :cond_0

    .line 62
    invoke-direct {p0, v1}, Lcom/twitter/android/search/i;->a(I)Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 63
    invoke-direct {p0, v2}, Lcom/twitter/android/search/i;->b(I)Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 65
    :cond_0
    invoke-virtual {v0, p1}, Lagx;->a(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 67
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_2

    .line 69
    :cond_1
    invoke-virtual {v3}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 103
    iput-object p1, p0, Lcom/twitter/android/search/i;->f:Ljava/lang/String;

    .line 104
    iget-object v0, p0, Lcom/twitter/android/search/i;->b:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/twitter/android/search/i;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 105
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/twitter/util/q;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/twitter/util/q",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 47
    iput-object p1, p0, Lcom/twitter/android/search/i;->f:Ljava/lang/String;

    .line 48
    invoke-virtual {p0, p2}, Lcom/twitter/android/search/i;->a(Lcom/twitter/util/q;)V

    .line 49
    return-void
.end method

.method public synthetic c(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/twitter/android/search/i;->a(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lcom/twitter/android/search/i;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/twitter/android/search/i;->a:Lcom/twitter/util/object/d;

    iget-object v1, p0, Lcom/twitter/android/search/i;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/content/Loader;

    return-object v0

    .line 112
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Query must be set before reading from Search Suggestions"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
