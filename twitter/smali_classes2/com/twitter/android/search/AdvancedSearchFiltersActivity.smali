.class public Lcom/twitter/android/search/AdvancedSearchFiltersActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/search/AdvancedSearchFiltersActivity$a;,
        Lcom/twitter/android/search/AdvancedSearchFiltersActivity$b;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method

.method private i()Laid;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/twitter/android/search/AdvancedSearchFiltersActivity;->Y()Lans;

    move-result-object v0

    check-cast v0, Lagj;

    .line 61
    invoke-interface {v0}, Lagj;->c()Laid;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->b(Z)V

    .line 37
    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 38
    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(I)V

    .line 39
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic a(Lank;)Lcom/twitter/app/common/base/j;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/twitter/android/search/AdvancedSearchFiltersActivity;->d(Lank;)Lagj;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic b(Lank;)Lcom/twitter/app/common/abs/b;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/twitter/android/search/AdvancedSearchFiltersActivity;->d(Lank;)Lagj;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic c(Lank;)Lans;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/twitter/android/search/AdvancedSearchFiltersActivity;->d(Lank;)Lagj;

    move-result-object v0

    return-object v0
.end method

.method protected d(Lank;)Lagj;
    .locals 2

    .prologue
    .line 45
    invoke-static {}, Lagm;->d()Lagm$a;

    move-result-object v0

    new-instance v1, Lant;

    invoke-direct {v1, p0, p1}, Lant;-><init>(Landroid/app/Activity;Lank;)V

    .line 46
    invoke-virtual {v0, v1}, Lagm$a;->a(Lant;)Lagm$a;

    move-result-object v0

    .line 47
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lagm$a;->a(Lamu;)Lagm$a;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lagm$a;->a()Lagj;

    move-result-object v0

    .line 45
    return-object v0
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 66
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->finish()V

    .line 67
    const v0, 0x7f05003c

    const v1, 0x7f05003d

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/search/AdvancedSearchFiltersActivity;->overridePendingTransition(II)V

    .line 68
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 1

    .prologue
    .line 54
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    .line 55
    invoke-direct {p0}, Lcom/twitter/android/search/AdvancedSearchFiltersActivity;->i()Laid;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Laid;->a(I[Ljava/lang/String;[I)V

    .line 56
    return-void
.end method
