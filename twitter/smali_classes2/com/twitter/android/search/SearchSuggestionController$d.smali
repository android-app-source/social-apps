.class Lcom/twitter/android/search/SearchSuggestionController$d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/dialog/b$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/search/SearchSuggestionController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field final synthetic b:Lcom/twitter/android/search/SearchSuggestionController;


# direct methods
.method constructor <init>(Lcom/twitter/android/search/SearchSuggestionController;)V
    .locals 0

    .prologue
    .line 721
    iput-object p1, p0, Lcom/twitter/android/search/SearchSuggestionController$d;->b:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 722
    return-void
.end method

.method constructor <init>(Lcom/twitter/android/search/SearchSuggestionController;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 724
    iput-object p1, p0, Lcom/twitter/android/search/SearchSuggestionController$d;->b:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 725
    iput-object p2, p0, Lcom/twitter/android/search/SearchSuggestionController$d;->a:Ljava/lang/String;

    .line 726
    return-void
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;II)V
    .locals 3

    .prologue
    .line 730
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    .line 732
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController$d;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 733
    new-instance v0, Lcom/twitter/library/api/search/e;

    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController$d;->b:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-static {v1}, Lcom/twitter/android/search/SearchSuggestionController;->b(Lcom/twitter/android/search/SearchSuggestionController;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/search/SearchSuggestionController$d;->b:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-static {v2}, Lcom/twitter/android/search/SearchSuggestionController;->j(Lcom/twitter/android/search/SearchSuggestionController;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/api/search/e;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v1, 0x5

    .line 734
    invoke-virtual {v0, v1}, Lcom/twitter/library/api/search/e;->d(I)Lcom/twitter/library/service/j;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/search/e;

    .line 740
    :goto_0
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 741
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController$d;->b:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-virtual {v0}, Lcom/twitter/android/search/SearchSuggestionController;->c()V

    .line 743
    :cond_0
    return-void

    .line 736
    :cond_1
    new-instance v0, Lcom/twitter/library/api/search/e;

    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController$d;->b:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-static {v1}, Lcom/twitter/android/search/SearchSuggestionController;->b(Lcom/twitter/android/search/SearchSuggestionController;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/search/SearchSuggestionController$d;->b:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-static {v2}, Lcom/twitter/android/search/SearchSuggestionController;->j(Lcom/twitter/android/search/SearchSuggestionController;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/api/search/e;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v1, 0x2

    .line 737
    invoke-virtual {v0, v1}, Lcom/twitter/library/api/search/e;->d(I)Lcom/twitter/library/service/j;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/search/e;

    .line 738
    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController$d;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/library/api/search/e;->c:Ljava/lang/String;

    goto :goto_0
.end method
