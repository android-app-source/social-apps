.class Lcom/twitter/android/search/SearchSuggestionController$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/search/SearchSuggestionController;-><init>(Landroid/support/v4/app/FragmentActivity;Landroid/content/res/Resources;Lcom/twitter/library/client/Session;ILcom/twitter/android/search/i;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/q",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/search/SearchSuggestionController;


# direct methods
.method constructor <init>(Lcom/twitter/android/search/SearchSuggestionController;)V
    .locals 0

    .prologue
    .line 195
    iput-object p1, p0, Lcom/twitter/android/search/SearchSuggestionController$3;->a:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 195
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/twitter/android/search/SearchSuggestionController$3;->onEvent(Ljava/util/List;)V

    return-void
.end method

.method public onEvent(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 198
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController$3;->a:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-static {v0}, Lcom/twitter/android/search/SearchSuggestionController;->c(Lcom/twitter/android/search/SearchSuggestionController;)Lcom/twitter/android/search/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/search/h;->k()Lcjt;

    move-result-object v0

    new-instance v1, Lcbl;

    invoke-direct {v1, p1}, Lcbl;-><init>(Ljava/lang/Iterable;)V

    invoke-interface {v0, v1}, Lcjt;->a(Lcbi;)Lcbi;

    .line 200
    return-void
.end method
