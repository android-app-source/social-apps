.class public Lcom/twitter/android/search/h;
.super Lcjr;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcjr",
        "<",
        "Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Landroid/util/SparseIntArray;


# instance fields
.field private final b:Landroid/view/View$OnClickListener;

.field private final c:Landroid/view/View$OnClickListener;

.field private final d:Lcom/twitter/android/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/av",
            "<",
            "Landroid/view/View;",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/view/LayoutInflater;

.field private final f:Z

.field private final g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 60
    new-instance v0, Landroid/util/SparseIntArray;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    sput-object v0, Lcom/twitter/android/search/h;->a:Landroid/util/SparseIntArray;

    .line 61
    sget-object v0, Lcom/twitter/android/search/h;->a:Landroid/util/SparseIntArray;

    const/4 v1, 0x6

    const v2, 0x7f0a01fc

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 62
    sget-object v0, Lcom/twitter/android/search/h;->a:Landroid/util/SparseIntArray;

    const/4 v1, 0x3

    const v2, 0x7f0a01fe

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 63
    sget-object v0, Lcom/twitter/android/search/h;->a:Landroid/util/SparseIntArray;

    const/4 v1, 0x5

    const v2, 0x7f0a01ff

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 64
    sget-object v0, Lcom/twitter/android/search/h;->a:Landroid/util/SparseIntArray;

    const/4 v1, 0x2

    const v2, 0x7f0a01fd

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View$OnClickListener;Lcom/twitter/android/av;Landroid/view/View$OnClickListener;Landroid/view/LayoutInflater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/View$OnClickListener;",
            "Lcom/twitter/android/av",
            "<",
            "Landroid/view/View;",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            "Landroid/view/LayoutInflater;",
            ")V"
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcjr;-><init>(Landroid/content/Context;)V

    .line 71
    iput-object p2, p0, Lcom/twitter/android/search/h;->b:Landroid/view/View$OnClickListener;

    .line 72
    iput-object p3, p0, Lcom/twitter/android/search/h;->d:Lcom/twitter/android/av;

    .line 73
    iput-object p4, p0, Lcom/twitter/android/search/h;->c:Landroid/view/View$OnClickListener;

    .line 74
    iput-object p5, p0, Lcom/twitter/android/search/h;->e:Landroid/view/LayoutInflater;

    .line 75
    const-string/jumbo v0, "search_features_reverse_bolding_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/search/h;->g:Z

    .line 76
    const-string/jumbo v0, "search_features_tap_ahead_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/search/h;->f:Z

    .line 77
    return-void
.end method

.method private static a(I)I
    .locals 1

    .prologue
    .line 358
    packed-switch p0, :pswitch_data_0

    .line 376
    :pswitch_0
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 360
    :pswitch_1
    const v0, 0x7f0205da

    goto :goto_0

    .line 363
    :pswitch_2
    const v0, 0x7f0205dd

    goto :goto_0

    .line 366
    :pswitch_3
    const v0, 0x7f0205e3

    goto :goto_0

    .line 369
    :pswitch_4
    const v0, 0x7f0205e6

    goto :goto_0

    .line 373
    :pswitch_5
    const v0, 0x7f0205e0

    goto :goto_0

    .line 358
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method private a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/model/search/viewmodel/c;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 221
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/search/g$a;

    .line 225
    sget-object v1, Lcom/twitter/android/search/h;->a:Landroid/util/SparseIntArray;

    invoke-virtual {p3}, Lcom/twitter/model/search/viewmodel/c;->b()I

    move-result v2

    invoke-virtual {v1, v2, v6}, Landroid/util/SparseIntArray;->get(II)I

    move-result v1

    .line 226
    if-lez v1, :cond_1

    .line 227
    new-array v2, v5, [Ljava/lang/Object;

    .line 228
    invoke-virtual {p3}, Lcom/twitter/model/search/viewmodel/c;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {p2, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 229
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 231
    invoke-virtual {p3}, Lcom/twitter/model/search/viewmodel/c;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 232
    if-lez v3, :cond_0

    .line 233
    new-instance v4, Landroid/text/style/StyleSpan;

    invoke-direct {v4, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 234
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v5, 0x21

    .line 233
    invoke-virtual {v2, v4, v3, v1, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 237
    :cond_0
    iget-object v1, v0, Lcom/twitter/android/search/g$a;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 241
    :goto_0
    invoke-virtual {p3}, Lcom/twitter/model/search/viewmodel/c;->b()I

    move-result v1

    invoke-static {v1}, Lcom/twitter/android/search/h;->a(I)I

    move-result v1

    .line 242
    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 243
    iget-object v1, v0, Lcom/twitter/android/search/g$a;->b:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 249
    :goto_1
    iget-object v0, v0, Lcom/twitter/android/search/g$a;->c:Landroid/view/View;

    invoke-virtual {p3}, Lcom/twitter/model/search/viewmodel/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Lcom/twitter/model/search/viewmodel/c;->b()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/search/h;->a(Landroid/view/View;Ljava/lang/String;I)V

    .line 251
    invoke-virtual {p3}, Lcom/twitter/model/search/viewmodel/c;->a()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xc

    .line 252
    invoke-direct {p0, p3}, Lcom/twitter/android/search/h;->b(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;)I

    move-result v2

    .line 251
    invoke-static {v0, v1, v2}, Lcom/twitter/library/scribe/b;->a(Ljava/lang/String;II)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 253
    return-object v0

    .line 239
    :cond_1
    iget-object v1, v0, Lcom/twitter/android/search/g$a;->a:Landroid/widget/TextView;

    invoke-virtual {p3}, Lcom/twitter/model/search/viewmodel/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3}, Lcom/twitter/model/search/viewmodel/c;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/twitter/android/search/h;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 245
    :cond_2
    iget-object v2, v0, Lcom/twitter/android/search/g$a;->b:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 246
    iget-object v2, v0, Lcom/twitter/android/search/g$a;->b:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method private a(Landroid/view/View;Lcom/twitter/model/search/viewmodel/a;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 4

    .prologue
    .line 185
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/search/g$a;

    .line 187
    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/a;->b()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 188
    iget-object v1, v0, Lcom/twitter/android/search/g$a;->a:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/a;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/twitter/android/search/h;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    iget-boolean v1, p0, Lcom/twitter/android/search/h;->f:Z

    if-eqz v1, :cond_0

    .line 190
    iget-object v0, v0, Lcom/twitter/android/search/g$a;->c:Landroid/view/View;

    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/search/h;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 197
    :cond_0
    :goto_0
    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/a;->a()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xc

    .line 198
    invoke-direct {p0, p2}, Lcom/twitter/android/search/h;->b(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;)I

    move-result v2

    .line 197
    invoke-static {v0, v1, v2}, Lcom/twitter/library/scribe/b;->a(Ljava/lang/String;II)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 199
    return-object v0

    .line 193
    :cond_1
    iget-object v1, v0, Lcom/twitter/android/search/g$a;->a:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 194
    iget-object v0, v0, Lcom/twitter/android/search/g$a;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;Lcom/twitter/model/search/viewmodel/d;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 4

    .prologue
    .line 259
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/search/g$b;

    .line 261
    iget-object v1, v0, Lcom/twitter/android/search/g$b;->a:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/d;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/d;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/twitter/android/search/h;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    iget-object v1, v0, Lcom/twitter/android/search/g$b;->a:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/d;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 263
    iget-object v0, v0, Lcom/twitter/android/search/g$b;->b:Landroid/view/View;

    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/search/h;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 265
    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/d;->a()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xc

    .line 266
    invoke-direct {p0, p2}, Lcom/twitter/android/search/h;->b(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;)I

    move-result v2

    .line 265
    invoke-static {v0, v1, v2}, Lcom/twitter/library/scribe/b;->a(Ljava/lang/String;II)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 267
    return-object v0
.end method

.method private a(Landroid/view/View;Lcom/twitter/model/search/viewmodel/e;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 4

    .prologue
    .line 273
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/search/g$a;

    .line 274
    iget-object v1, v0, Lcom/twitter/android/search/g$a;->a:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/e;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/e;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/twitter/android/search/h;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    iget-object v1, v0, Lcom/twitter/android/search/g$a;->b:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 276
    iget-object v0, v0, Lcom/twitter/android/search/g$a;->c:Landroid/view/View;

    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/e;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/search/h;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 278
    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/e;->a()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xd

    .line 279
    invoke-direct {p0, p2}, Lcom/twitter/android/search/h;->b(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;)I

    move-result v2

    .line 278
    invoke-static {v0, v1, v2}, Lcom/twitter/library/scribe/b;->a(Ljava/lang/String;II)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 280
    return-object v0
.end method

.method private a(Landroid/view/View;Lcom/twitter/model/search/viewmodel/f;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 3

    .prologue
    .line 285
    .line 286
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/search/g$c;

    .line 287
    iget-object v1, v0, Lcom/twitter/android/search/g$c;->a:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 288
    iget-object v0, v0, Lcom/twitter/android/search/g$c;->b:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 289
    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/f;->a()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x8

    .line 290
    invoke-direct {p0, p2}, Lcom/twitter/android/search/h;->b(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;)I

    move-result v2

    .line 289
    invoke-static {v0, v1, v2}, Lcom/twitter/library/scribe/b;->a(Ljava/lang/String;II)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 291
    return-object v0
.end method

.method private a(Lcom/twitter/ui/user/UserSocialView;Lcom/twitter/model/search/viewmodel/h;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 8

    .prologue
    const v2, 0x7f020835

    const/4 v4, 0x0

    .line 297
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserSocialView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/db;

    .line 298
    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/h;->h()J

    move-result-wide v6

    invoke-virtual {p1, v6, v7}, Lcom/twitter/ui/user/UserSocialView;->setUserId(J)V

    .line 299
    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/h;->h()J

    move-result-wide v6

    iput-wide v6, v0, Lcom/twitter/android/db;->e:J

    .line 300
    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/h;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/twitter/ui/user/UserSocialView;->setUserImageUrl(Ljava/lang/String;)V

    .line 302
    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/h;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/h;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v1, v3}, Lcom/twitter/ui/user/UserSocialView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/h;->j()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/twitter/ui/user/UserSocialView;->setVerified(Z)V

    .line 304
    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/h;->k()I

    move-result v1

    iput v1, v0, Lcom/twitter/android/db;->f:I

    .line 305
    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/h;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbld;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306
    const/16 v1, 0x2d

    const v2, 0x7f0205a2

    .line 307
    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/h;->l()Ljava/lang/String;

    move-result-object v3

    .line 308
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v5

    move-object v0, p1

    .line 306
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/ui/user/UserSocialView;->a(IILjava/lang/String;IZ)V

    .line 318
    :goto_0
    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/h;->g()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    .line 319
    invoke-direct {p0, p2}, Lcom/twitter/android/search/h;->b(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;)I

    move-result v2

    .line 318
    invoke-static {v0, v1, v2}, Lcom/twitter/library/scribe/b;->a(Ljava/lang/String;II)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 320
    return-object v0

    .line 309
    :cond_0
    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/h;->k()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/model/core/g;->c(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 310
    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/h;->k()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 312
    :cond_1
    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/h;->k()I

    move-result v0

    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v1

    .line 311
    invoke-virtual {p1, v2, v0, v1}, Lcom/twitter/ui/user/UserSocialView;->a(IIZ)V

    goto :goto_0

    .line 314
    :cond_2
    const/16 v1, 0x28

    .line 315
    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/h;->m()Ljava/lang/String;

    move-result-object v3

    .line 316
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v5

    move-object v0, p1

    .line 314
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/ui/user/UserSocialView;->a(IILjava/lang/String;IZ)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem;)V
    .locals 3

    .prologue
    .line 171
    const v0, 0x7f1307e5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 172
    const v1, 0x7f1307e6

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 173
    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem;->b()Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem$HeaderType;

    move-result-object v0

    sget-object v2, Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem$HeaderType;->a:Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem$HeaderType;

    if-ne v0, v2, :cond_0

    .line 175
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 176
    iget-object v0, p0, Lcom/twitter/android/search/h;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 180
    :goto_0
    return-void

    .line 178
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 343
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/search/h;->a(Landroid/view/View;Ljava/lang/String;I)V

    .line 344
    return-void
.end method

.method private a(Landroid/view/View;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 348
    iget-boolean v0, p0, Lcom/twitter/android/search/h;->f:Z

    if-eqz v0, :cond_0

    if-nez p3, :cond_0

    .line 349
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 350
    iget-object v0, p0, Lcom/twitter/android/search/h;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 351
    invoke-virtual {p1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 355
    :goto_0
    return-void

    .line 353
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 324
    iget-boolean v0, p0, Lcom/twitter/android/search/h;->g:Z

    if-eqz v0, :cond_1

    invoke-static {p3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 325
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 326
    invoke-static {p2}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 327
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v2

    .line 328
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 329
    const/4 v4, -0x1

    if-le v3, v4, :cond_0

    add-int v4, v3, v2

    if-ge v4, v0, :cond_0

    .line 331
    new-instance v4, Landroid/text/style/StyleSpan;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int/2addr v2, v3

    const/16 v3, 0x21

    invoke-virtual {v1, v4, v2, v0, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 335
    :cond_0
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 339
    :goto_0
    return-void

    .line 337
    :cond_1
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private b(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;)I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 203
    invoke-virtual {p0}, Lcom/twitter/android/search/h;->g()Lcbi;

    move-result-object v0

    .line 204
    if-eqz v0, :cond_1

    .line 205
    invoke-virtual {v0}, Lcbi;->i()Ljava/util/ListIterator;

    move-result-object v3

    move v1, v2

    .line 207
    :goto_0
    invoke-interface {v3}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 208
    invoke-interface {v3}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    :goto_1
    return v1

    .line 211
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v1, v2

    .line 214
    goto :goto_1
.end method


# virtual methods
.method protected a(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;)I
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p1}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->c()Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->ordinal()I

    move-result v0

    return v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 48
    check-cast p1, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;

    invoke-virtual {p0, p1}, Lcom/twitter/android/search/h;->a(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;)I

    move-result v0

    return v0
.end method

.method protected a(Landroid/content/Context;Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 93
    sget-object v0, Lcom/twitter/android/search/h$1;->a:[I

    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->c()Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 122
    iget-object v0, p0, Lcom/twitter/android/search/h;->e:Landroid/view/LayoutInflater;

    const v1, 0x7f0403dd

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 123
    invoke-static {v0}, Lcom/twitter/android/search/g;->c(Landroid/view/View;)Lcom/twitter/android/search/g$a;

    move-result-object v1

    .line 124
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 125
    :goto_0
    return-object v0

    .line 95
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/search/h;->e:Landroid/view/LayoutInflater;

    const v1, 0x7f0403a2

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 98
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/search/h;->e:Landroid/view/LayoutInflater;

    const v1, 0x7f0403aa

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 101
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/search/h;->e:Landroid/view/LayoutInflater;

    const v1, 0x7f04041b

    .line 102
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/user/UserSocialView;

    .line 103
    new-instance v1, Lcom/twitter/android/db;

    invoke-direct {v1, v0}, Lcom/twitter/android/db;-><init>(Lcom/twitter/ui/user/BaseUserView;)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 107
    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/search/h;->e:Landroid/view/LayoutInflater;

    const v1, 0x7f0403a1

    .line 108
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 109
    invoke-static {v0}, Lcom/twitter/android/search/g;->b(Landroid/view/View;)Lcom/twitter/android/search/g$c;

    move-result-object v1

    .line 110
    const v2, 0x7f130068

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 111
    iget-object v2, v1, Lcom/twitter/android/search/g$c;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 112
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 116
    :pswitch_4
    iget-object v0, p0, Lcom/twitter/android/search/h;->e:Landroid/view/LayoutInflater;

    const v1, 0x7f04033d

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 117
    invoke-static {v0}, Lcom/twitter/android/search/g;->a(Landroid/view/View;)Lcom/twitter/android/search/g$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 93
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 48
    check-cast p2, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/search/h;->a(Landroid/content/Context;Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;)V
    .locals 3

    .prologue
    .line 132
    const/4 v0, 0x0

    .line 133
    sget-object v1, Lcom/twitter/android/search/h$1;->a:[I

    invoke-virtual {p3}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->c()Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 162
    check-cast p3, Lcom/twitter/model/search/viewmodel/a;

    invoke-direct {p0, p1, p3}, Lcom/twitter/android/search/h;->a(Landroid/view/View;Lcom/twitter/model/search/viewmodel/a;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 165
    :goto_0
    :pswitch_0
    iget-object v1, p0, Lcom/twitter/android/search/h;->d:Lcom/twitter/android/av;

    if-eqz v1, :cond_0

    .line 166
    iget-object v1, p0, Lcom/twitter/android/search/h;->d:Lcom/twitter/android/av;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-interface {v1, p1, v0, v2}, Lcom/twitter/android/av;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 168
    :cond_0
    return-void

    .line 135
    :pswitch_1
    check-cast p3, Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem;

    invoke-direct {p0, p1, p3}, Lcom/twitter/android/search/h;->a(Landroid/view/View;Lcom/twitter/model/search/viewmodel/HeaderSearchSuggestionListItem;)V

    goto :goto_0

    :pswitch_2
    move-object v0, p1

    .line 142
    check-cast v0, Lcom/twitter/ui/user/UserSocialView;

    check-cast p3, Lcom/twitter/model/search/viewmodel/h;

    invoke-direct {p0, v0, p3}, Lcom/twitter/android/search/h;->a(Lcom/twitter/ui/user/UserSocialView;Lcom/twitter/model/search/viewmodel/h;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    goto :goto_0

    .line 146
    :pswitch_3
    check-cast p3, Lcom/twitter/model/search/viewmodel/f;

    invoke-direct {p0, p1, p3}, Lcom/twitter/android/search/h;->a(Landroid/view/View;Lcom/twitter/model/search/viewmodel/f;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    goto :goto_0

    .line 150
    :pswitch_4
    check-cast p3, Lcom/twitter/model/search/viewmodel/e;

    invoke-direct {p0, p1, p3}, Lcom/twitter/android/search/h;->a(Landroid/view/View;Lcom/twitter/model/search/viewmodel/e;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    goto :goto_0

    .line 154
    :pswitch_5
    check-cast p3, Lcom/twitter/model/search/viewmodel/d;

    invoke-direct {p0, p1, p3}, Lcom/twitter/android/search/h;->a(Landroid/view/View;Lcom/twitter/model/search/viewmodel/d;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    goto :goto_0

    .line 158
    :pswitch_6
    check-cast p3, Lcom/twitter/model/search/viewmodel/c;

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/search/h;->a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/model/search/viewmodel/c;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    goto :goto_0

    .line 133
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_4
    .end packed-switch
.end method

.method protected bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 48
    check-cast p3, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/search/h;->a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;)V

    return-void
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 86
    invoke-static {}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->values()[Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
