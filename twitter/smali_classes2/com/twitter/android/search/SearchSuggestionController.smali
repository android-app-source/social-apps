.class public Lcom/twitter/android/search/SearchSuggestionController;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/twitter/internal/android/widget/PopupEditText$c;
.implements Lcom/twitter/library/widget/SearchQueryView$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/search/SearchSuggestionController$QueryTextUpdateReceiver;,
        Lcom/twitter/android/search/SearchSuggestionController$d;,
        Lcom/twitter/android/search/SearchSuggestionController$g;,
        Lcom/twitter/android/search/SearchSuggestionController$c;,
        Lcom/twitter/android/search/SearchSuggestionController$a;,
        Lcom/twitter/android/search/SearchSuggestionController$b;,
        Lcom/twitter/android/search/SearchSuggestionController$h;,
        Lcom/twitter/android/search/SearchSuggestionController$i;,
        Lcom/twitter/android/search/SearchSuggestionController$e;,
        Lcom/twitter/android/search/SearchSuggestionController$f;
    }
.end annotation


# instance fields
.field private A:Z

.field private B:Z

.field private C:I

.field private D:Z

.field private E:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field private F:Z

.field private final a:I

.field private final b:Landroid/support/v4/app/LoaderManager;

.field private final c:Landroid/support/v4/app/FragmentManager;

.field private final d:Lcom/twitter/android/search/SearchSuggestionController$f;

.field private final e:Lcom/twitter/android/search/SearchSuggestionController$e;

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/twitter/android/search/SearchSuggestionController$QueryTextUpdateReceiver;

.field private final h:Landroid/support/v4/app/FragmentActivity;

.field private final i:Lcom/twitter/library/client/Session;

.field private final j:Lcom/twitter/android/search/i;

.field private final k:Lcom/twitter/android/search/h;

.field private final l:Ljava/lang/Runnable;

.field private final m:Lcom/twitter/util/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/q",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private final n:Lcom/twitter/android/search/SearchSuggestionController$h;

.field private final o:Lcom/twitter/android/search/SearchSuggestionController$b;

.field private final p:Landroid/content/res/Resources;

.field private q:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private r:Lcom/twitter/android/client/t;

.field private s:Lcom/twitter/internal/android/widget/PopupEditText;

.field private t:Ljava/lang/String;

.field private u:Lcom/twitter/android/search/SearchSuggestionController$c;

.field private v:Lazv;

.field private w:Lcom/twitter/android/search/SearchSuggestionController$g;

.field private x:Lcom/twitter/android/client/s;

.field private y:Ljava/lang/String;

.field private z:[Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Landroid/content/res/Resources;Lcom/twitter/library/client/Session;ILcom/twitter/android/search/i;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->f:Ljava/util/List;

    .line 104
    new-instance v0, Lcom/twitter/android/search/SearchSuggestionController$QueryTextUpdateReceiver;

    new-instance v1, Landroid/os/Handler;

    .line 105
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, v1}, Lcom/twitter/android/search/SearchSuggestionController$QueryTextUpdateReceiver;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->g:Lcom/twitter/android/search/SearchSuggestionController$QueryTextUpdateReceiver;

    .line 110
    new-instance v0, Lcom/twitter/android/search/SearchSuggestionController$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/search/SearchSuggestionController$1;-><init>(Lcom/twitter/android/search/SearchSuggestionController;)V

    iput-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->l:Ljava/lang/Runnable;

    .line 137
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->C:I

    .line 178
    iput-object p1, p0, Lcom/twitter/android/search/SearchSuggestionController;->h:Landroid/support/v4/app/FragmentActivity;

    .line 179
    iput-object p2, p0, Lcom/twitter/android/search/SearchSuggestionController;->p:Landroid/content/res/Resources;

    .line 180
    iput-object p3, p0, Lcom/twitter/android/search/SearchSuggestionController;->i:Lcom/twitter/library/client/Session;

    .line 181
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->b:Landroid/support/v4/app/LoaderManager;

    .line 182
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->c:Landroid/support/v4/app/FragmentManager;

    .line 183
    new-instance v0, Lcom/twitter/android/search/SearchSuggestionController$h;

    invoke-direct {v0, p0}, Lcom/twitter/android/search/SearchSuggestionController$h;-><init>(Lcom/twitter/android/search/SearchSuggestionController;)V

    iput-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->n:Lcom/twitter/android/search/SearchSuggestionController$h;

    .line 184
    new-instance v0, Lcom/twitter/android/search/SearchSuggestionController$b;

    invoke-direct {v0, p0}, Lcom/twitter/android/search/SearchSuggestionController$b;-><init>(Lcom/twitter/android/search/SearchSuggestionController;)V

    iput-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->o:Lcom/twitter/android/search/SearchSuggestionController$b;

    .line 185
    iput p4, p0, Lcom/twitter/android/search/SearchSuggestionController;->a:I

    .line 186
    new-instance v0, Lcom/twitter/android/search/h;

    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->h:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/twitter/android/search/SearchSuggestionController;->n:Lcom/twitter/android/search/SearchSuggestionController$h;

    new-instance v3, Lcom/twitter/android/search/SearchSuggestionController$i;

    invoke-direct {v3, p0}, Lcom/twitter/android/search/SearchSuggestionController$i;-><init>(Lcom/twitter/android/search/SearchSuggestionController;)V

    new-instance v4, Lcom/twitter/android/search/SearchSuggestionController$a;

    invoke-direct {v4, p0}, Lcom/twitter/android/search/SearchSuggestionController$a;-><init>(Lcom/twitter/android/search/SearchSuggestionController;)V

    iget-object v5, p0, Lcom/twitter/android/search/SearchSuggestionController;->h:Landroid/support/v4/app/FragmentActivity;

    .line 188
    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/search/h;-><init>(Landroid/content/Context;Landroid/view/View$OnClickListener;Lcom/twitter/android/av;Landroid/view/View$OnClickListener;Landroid/view/LayoutInflater;)V

    iput-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->k:Lcom/twitter/android/search/h;

    .line 190
    iput-object p6, p0, Lcom/twitter/android/search/SearchSuggestionController;->q:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 191
    new-instance v0, Lcom/twitter/android/search/SearchSuggestionController$e;

    invoke-direct {v0, p0}, Lcom/twitter/android/search/SearchSuggestionController$e;-><init>(Lcom/twitter/android/search/SearchSuggestionController;)V

    iput-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->e:Lcom/twitter/android/search/SearchSuggestionController$e;

    .line 192
    new-instance v0, Lcom/twitter/android/search/SearchSuggestionController$f;

    invoke-direct {v0, p0}, Lcom/twitter/android/search/SearchSuggestionController$f;-><init>(Lcom/twitter/android/search/SearchSuggestionController;)V

    iput-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->d:Lcom/twitter/android/search/SearchSuggestionController$f;

    .line 193
    const v0, 0x7f0a07d7

    iput v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->E:I

    .line 194
    iput-object p5, p0, Lcom/twitter/android/search/SearchSuggestionController;->j:Lcom/twitter/android/search/i;

    .line 195
    new-instance v0, Lcom/twitter/android/search/SearchSuggestionController$3;

    invoke-direct {v0, p0}, Lcom/twitter/android/search/SearchSuggestionController$3;-><init>(Lcom/twitter/android/search/SearchSuggestionController;)V

    iput-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->m:Lcom/twitter/util/q;

    .line 202
    invoke-direct {p0, p7}, Lcom/twitter/android/search/SearchSuggestionController;->b(Landroid/os/Bundle;)V

    .line 203
    return-void
.end method

.method public static a(Lcom/twitter/app/common/base/BaseFragmentActivity;Landroid/os/Bundle;)Lcom/twitter/android/search/SearchSuggestionController;
    .locals 13

    .prologue
    const/high16 v4, -0x80000000

    .line 148
    .line 149
    invoke-static {p0}, Lcom/twitter/android/search/f;->a(Landroid/app/Activity;)Lcom/twitter/util/object/d;

    move-result-object v1

    .line 150
    new-instance v0, Lcom/twitter/android/search/i;

    new-instance v2, Lcom/twitter/android/search/SearchSuggestionController$2;

    invoke-direct {v2}, Lcom/twitter/android/search/SearchSuggestionController$2;-><init>()V

    .line 158
    invoke-virtual {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    .line 160
    invoke-virtual {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/search/i;-><init>(Lcom/twitter/util/object/d;Lcom/twitter/util/object/d;Landroid/support/v4/app/LoaderManager;ILandroid/content/res/Resources;)V

    .line 161
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v8

    .line 162
    new-instance v5, Lcom/twitter/android/search/SearchSuggestionController;

    .line 163
    invoke-virtual {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    new-instance v1, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const-string/jumbo v2, "app"

    .line 167
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v11

    check-cast v11, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-object v6, p0

    move v9, v4

    move-object v10, v0

    move-object v12, p1

    invoke-direct/range {v5 .. v12}, Lcom/twitter/android/search/SearchSuggestionController;-><init>(Landroid/support/v4/app/FragmentActivity;Landroid/content/res/Resources;Lcom/twitter/library/client/Session;ILcom/twitter/android/search/i;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Landroid/os/Bundle;)V

    .line 169
    invoke-static {p0}, Lcom/twitter/android/search/a;->a(Lcom/twitter/app/common/base/BaseFragmentActivity;)Lcom/twitter/android/client/s;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/twitter/android/search/SearchSuggestionController;->a(Lcom/twitter/android/client/s;)V

    .line 170
    return-object v5
.end method

.method static synthetic a(Lcom/twitter/android/search/SearchSuggestionController;)Lcom/twitter/internal/android/widget/PopupEditText;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->s:Lcom/twitter/internal/android/widget/PopupEditText;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/search/SearchSuggestionController;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/twitter/android/search/SearchSuggestionController;->y:Ljava/lang/String;

    return-object p1
.end method

.method private a(ILcom/twitter/model/search/viewmodel/SearchSuggestionListItem;)V
    .locals 6

    .prologue
    .line 368
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->x:Lcom/twitter/android/client/s;

    if-eqz v0, :cond_0

    .line 369
    iget-boolean v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->D:Z

    if-eqz v0, :cond_1

    .line 370
    invoke-direct {p0}, Lcom/twitter/android/search/SearchSuggestionController;->g()I

    move-result v3

    .line 371
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->x:Lcom/twitter/android/client/s;

    iget-object v4, p0, Lcom/twitter/android/search/SearchSuggestionController;->t:Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/android/search/SearchSuggestionController;->q:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-object v1, p2

    move v2, p1

    .line 372
    invoke-interface/range {v0 .. v5}, Lcom/twitter/android/client/s;->a(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;IILjava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 376
    :cond_0
    invoke-virtual {p2}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->t:Ljava/lang/String;

    .line 377
    return-void

    .line 370
    :cond_1
    const/4 v3, -0x1

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/search/SearchSuggestionController;)Landroid/support/v4/app/FragmentActivity;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->h:Landroid/support/v4/app/FragmentActivity;

    return-object v0
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 223
    if-eqz p1, :cond_0

    .line 224
    const-string/jumbo v0, "search_topic"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->y:Ljava/lang/String;

    .line 225
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->h:Landroid/support/v4/app/FragmentActivity;

    .line 226
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v1, "TAG_CLEAR_RECENT_SEARCH_DIALOG"

    .line 227
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PromptDialogFragment;

    .line 228
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->y:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 229
    new-instance v1, Lcom/twitter/android/search/SearchSuggestionController$d;

    iget-object v2, p0, Lcom/twitter/android/search/SearchSuggestionController;->y:Ljava/lang/String;

    invoke-direct {v1, p0, v2}, Lcom/twitter/android/search/SearchSuggestionController$d;-><init>(Lcom/twitter/android/search/SearchSuggestionController;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    .line 232
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/twitter/android/search/SearchSuggestionController;)Lcom/twitter/android/search/h;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->k:Lcom/twitter/android/search/h;

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 444
    invoke-static {p1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445
    invoke-static {p1}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Ljava/lang/String;)Lcom/twitter/library/api/search/TwitterTypeAheadGroup;

    move-result-object v0

    if-nez v0, :cond_0

    .line 446
    new-instance v0, Lcom/twitter/library/api/search/m;

    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->h:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/twitter/android/search/SearchSuggestionController;->i:Lcom/twitter/library/client/Session;

    iget v4, p0, Lcom/twitter/android/search/SearchSuggestionController;->C:I

    const/4 v5, 0x0

    const-string/jumbo v6, "search_box"

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/api/search/m;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;IILjava/lang/String;)V

    .line 449
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/search/SearchSuggestionController;->e:Lcom/twitter/android/search/SearchSuggestionController$e;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 452
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/twitter/android/search/SearchSuggestionController;)Lcom/twitter/android/search/SearchSuggestionController$b;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->o:Lcom/twitter/android/search/SearchSuggestionController$b;

    return-object v0
.end method

.method private d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 455
    invoke-static {p1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "#"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 456
    const/4 v0, 0x1

    .line 458
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lcom/twitter/android/search/SearchSuggestionController;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->t:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/search/SearchSuggestionController;)Lcom/twitter/android/search/SearchSuggestionController$c;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->u:Lcom/twitter/android/search/SearchSuggestionController$c;

    return-object v0
.end method

.method private g()I
    .locals 1

    .prologue
    .line 381
    iget v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->C:I

    packed-switch v0, :pswitch_data_0

    .line 386
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 383
    :pswitch_0
    const/4 v0, 0x2

    goto :goto_0

    .line 381
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic g(Lcom/twitter/android/search/SearchSuggestionController;)Ljava/util/List;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->f:Ljava/util/List;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/search/SearchSuggestionController;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->y:Ljava/lang/String;

    return-object v0
.end method

.method private h()V
    .locals 3

    .prologue
    .line 487
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->v:Lazv;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazv;

    invoke-virtual {v0}, Lazv;->e()Landroid/view/View;

    move-result-object v0

    .line 488
    instance-of v1, v0, Lcom/twitter/library/widget/SearchQueryView;

    if-eqz v1, :cond_0

    .line 489
    check-cast v0, Lcom/twitter/library/widget/SearchQueryView;

    .line 490
    invoke-virtual {v0, p0}, Lcom/twitter/library/widget/SearchQueryView;->setOnClearClickListener(Lcom/twitter/library/widget/SearchQueryView$a;)V

    .line 491
    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->h:Landroid/support/v4/app/FragmentActivity;

    const v2, 0x7f0a00dd

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SearchQueryView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 492
    iget v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->E:I

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SearchQueryView;->setHint(I)V

    .line 493
    invoke-virtual {v0}, Lcom/twitter/library/widget/SearchQueryView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->z:[Landroid/graphics/drawable/Drawable;

    .line 495
    :cond_0
    return-void
.end method

.method static synthetic i(Lcom/twitter/android/search/SearchSuggestionController;)Landroid/support/v4/app/FragmentManager;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->c:Landroid/support/v4/app/FragmentManager;

    return-object v0
.end method

.method private i()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 543
    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->s:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 544
    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->z:[Landroid/graphics/drawable/Drawable;

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 545
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 546
    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->s:Lcom/twitter/internal/android/widget/PopupEditText;

    iget-boolean v2, p0, Lcom/twitter/android/search/SearchSuggestionController;->B:Z

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->z:[Landroid/graphics/drawable/Drawable;

    aget-object v0, v0, v3

    :cond_0
    iget-object v2, p0, Lcom/twitter/android/search/SearchSuggestionController;->z:[Landroid/graphics/drawable/Drawable;

    aget-object v2, v2, v4

    iget-object v3, p0, Lcom/twitter/android/search/SearchSuggestionController;->z:[Landroid/graphics/drawable/Drawable;

    aget-object v3, v3, v5

    iget-object v4, p0, Lcom/twitter/android/search/SearchSuggestionController;->z:[Landroid/graphics/drawable/Drawable;

    aget-object v4, v4, v6

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/twitter/internal/android/widget/PopupEditText;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 552
    :goto_0
    return-void

    .line 549
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->s:Lcom/twitter/internal/android/widget/PopupEditText;

    iget-object v2, p0, Lcom/twitter/android/search/SearchSuggestionController;->z:[Landroid/graphics/drawable/Drawable;

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/twitter/android/search/SearchSuggestionController;->z:[Landroid/graphics/drawable/Drawable;

    aget-object v3, v3, v4

    iget-boolean v4, p0, Lcom/twitter/android/search/SearchSuggestionController;->B:Z

    if-eqz v4, :cond_2

    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->z:[Landroid/graphics/drawable/Drawable;

    aget-object v0, v0, v5

    :cond_2
    iget-object v4, p0, Lcom/twitter/android/search/SearchSuggestionController;->z:[Landroid/graphics/drawable/Drawable;

    aget-object v4, v4, v6

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/twitter/internal/android/widget/PopupEditText;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method static synthetic j(Lcom/twitter/android/search/SearchSuggestionController;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->i:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method private j()V
    .locals 7

    .prologue
    .line 695
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 696
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->i:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/search/SearchSuggestionController;->q:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v4, "search_box"

    const-string/jumbo v5, "typeahead"

    const-string/jumbo v6, "results"

    .line 697
    invoke-static {v3, v4, v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->f:Ljava/util/List;

    .line 699
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(Ljava/util/List;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 696
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 700
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 702
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/search/SearchSuggestionController;
    .locals 0

    .prologue
    .line 240
    if-eqz p1, :cond_0

    .line 241
    iput-object p1, p0, Lcom/twitter/android/search/SearchSuggestionController;->q:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 243
    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/android/search/SearchSuggestionController;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->q:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    .line 249
    return-object p0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 555
    iput p1, p0, Lcom/twitter/android/search/SearchSuggestionController;->C:I

    .line 556
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->D:Z

    .line 557
    return-void
.end method

.method public a(II)V
    .locals 0

    .prologue
    .line 424
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 235
    const-string/jumbo v0, "search_topic"

    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->y:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    return-void
.end method

.method public a(Lcom/twitter/android/client/s;)V
    .locals 0

    .prologue
    .line 709
    iput-object p1, p0, Lcom/twitter/android/search/SearchSuggestionController;->x:Lcom/twitter/android/client/s;

    .line 710
    return-void
.end method

.method public a(Lcom/twitter/android/client/t;)V
    .locals 0

    .prologue
    .line 212
    iput-object p1, p0, Lcom/twitter/android/search/SearchSuggestionController;->r:Lcom/twitter/android/client/t;

    .line 213
    return-void
.end method

.method public a(Lcom/twitter/android/search/SearchSuggestionController$g;)V
    .locals 0

    .prologue
    .line 705
    iput-object p1, p0, Lcom/twitter/android/search/SearchSuggestionController;->w:Lcom/twitter/android/search/SearchSuggestionController$g;

    .line 706
    return-void
.end method

.method a(Lcom/twitter/internal/android/widget/PopupEditText;)V
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->r:Lcom/twitter/android/client/t;

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->r:Lcom/twitter/android/client/t;

    invoke-virtual {v0}, Lcom/twitter/android/client/t;->a()V

    .line 354
    :goto_0
    return-void

    .line 352
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/internal/android/widget/PopupEditText;->a()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 2

    .prologue
    .line 470
    const v0, 0x7f13088d

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    .line 471
    new-instance v1, Lcom/twitter/android/search/SearchSuggestionController$6;

    invoke-direct {v1, p0}, Lcom/twitter/android/search/SearchSuggestionController$6;-><init>(Lcom/twitter/android/search/SearchSuggestionController;)V

    invoke-virtual {v0, v1}, Lazv;->a(Lazv$a;)Lazv;

    .line 482
    iput-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->v:Lazv;

    .line 483
    invoke-direct {p0}, Lcom/twitter/android/search/SearchSuggestionController;->h()V

    .line 484
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 418
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/search/SearchSuggestionController;->c(Ljava/lang/String;)V

    .line 419
    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->j:Lcom/twitter/android/search/i;

    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->s:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/search/i;->a(Ljava/lang/String;)V

    .line 420
    return-void
.end method

.method public a(Ljava/lang/CharSequence;Z)V
    .locals 2

    .prologue
    .line 434
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->s:Lcom/twitter/internal/android/widget/PopupEditText;

    if-eqz v0, :cond_0

    .line 435
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->s:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/PopupEditText;->setText(Ljava/lang/CharSequence;)V

    .line 436
    invoke-virtual {p0, p1}, Lcom/twitter/android/search/SearchSuggestionController;->a(Ljava/lang/CharSequence;)V

    .line 437
    if-eqz p2, :cond_0

    .line 438
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->s:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setSelection(I)V

    .line 441
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 219
    iput-boolean p1, p0, Lcom/twitter/android/search/SearchSuggestionController;->F:Z

    .line 220
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->r:Lcom/twitter/android/client/t;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->r:Lcom/twitter/android/client/t;

    invoke-virtual {v0}, Lcom/twitter/android/client/t;->c()Z

    move-result v0

    .line 262
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->s:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->d()Z

    move-result v0

    goto :goto_0
.end method

.method a(Lazv;)Z
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongViewCast"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 269
    iget-boolean v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->A:Z

    if-eqz v0, :cond_0

    .line 322
    :goto_0
    return v3

    .line 272
    :cond_0
    invoke-virtual {p1}, Lazv;->e()Landroid/view/View;

    move-result-object v0

    .line 273
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v1, 0x7f13065d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/PopupEditText;

    .line 274
    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/PopupEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 275
    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/PopupEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 276
    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/PopupEditText;->setPopupEditTextListener(Lcom/twitter/internal/android/widget/PopupEditText$c;)V

    .line 278
    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->r:Lcom/twitter/android/client/t;

    if-eqz v1, :cond_2

    .line 280
    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->r:Lcom/twitter/android/client/t;

    iget-object v4, p0, Lcom/twitter/android/search/SearchSuggestionController;->k:Lcom/twitter/android/search/h;

    invoke-virtual {v1, v4}, Lcom/twitter/android/client/t;->a(Landroid/widget/ListAdapter;)V

    .line 281
    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->r:Lcom/twitter/android/client/t;

    new-instance v4, Lcom/twitter/android/search/SearchSuggestionController$4;

    invoke-direct {v4, p0}, Lcom/twitter/android/search/SearchSuggestionController$4;-><init>(Lcom/twitter/android/search/SearchSuggestionController;)V

    invoke-virtual {v1, v4}, Lcom/twitter/android/client/t;->a(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 288
    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->r:Lcom/twitter/android/client/t;

    new-instance v4, Lcom/twitter/android/search/SearchSuggestionController$5;

    invoke-direct {v4, p0}, Lcom/twitter/android/search/SearchSuggestionController$5;-><init>(Lcom/twitter/android/search/SearchSuggestionController;)V

    invoke-virtual {v1, v4}, Lcom/twitter/android/client/t;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 301
    :goto_1
    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->n:Lcom/twitter/android/search/SearchSuggestionController$h;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 303
    iput-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->s:Lcom/twitter/internal/android/widget/PopupEditText;

    .line 304
    iget-object v4, p0, Lcom/twitter/android/search/SearchSuggestionController;->j:Lcom/twitter/android/search/i;

    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->s:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v1}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/twitter/android/search/SearchSuggestionController;->m:Lcom/twitter/util/q;

    invoke-virtual {v4, v1, v5}, Lcom/twitter/android/search/i;->a(Ljava/lang/String;Lcom/twitter/util/q;)V

    .line 306
    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->s:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v1}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/twitter/android/search/SearchSuggestionController;->c(Ljava/lang/String;)V

    .line 307
    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->s:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v1}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-lez v1, :cond_3

    move v1, v2

    :goto_2
    iput-boolean v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->B:Z

    .line 308
    invoke-direct {p0}, Lcom/twitter/android/search/SearchSuggestionController;->i()V

    .line 310
    new-instance v1, Lcom/twitter/android/search/SearchSuggestionController$c;

    invoke-direct {v1}, Lcom/twitter/android/search/SearchSuggestionController$c;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->u:Lcom/twitter/android/search/SearchSuggestionController$c;

    .line 311
    const-string/jumbo v1, "saved_searches_ttl_hours"

    invoke-static {v1, v2}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v1

    int-to-long v4, v1

    const-wide/32 v6, 0x36ee80

    mul-long/2addr v4, v6

    .line 313
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    new-instance v6, Lcom/twitter/library/api/search/c;

    iget-object v7, p0, Lcom/twitter/android/search/SearchSuggestionController;->h:Landroid/support/v4/app/FragmentActivity;

    iget-object v8, p0, Lcom/twitter/android/search/SearchSuggestionController;->i:Lcom/twitter/library/client/Session;

    invoke-direct {v6, v7, v8, v4, v5}, Lcom/twitter/library/api/search/c;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    iget-object v4, p0, Lcom/twitter/android/search/SearchSuggestionController;->d:Lcom/twitter/android/search/SearchSuggestionController$f;

    invoke-virtual {v1, v6, v4}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 315
    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->l:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->post(Ljava/lang/Runnable;)Z

    .line 316
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->i:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v2, [Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/search/SearchSuggestionController;->q:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v5, "search_box"

    const-string/jumbo v6, ""

    const-string/jumbo v7, "focus_field"

    .line 317
    invoke-static {v4, v5, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 316
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 318
    iput-boolean v2, p0, Lcom/twitter/android/search/SearchSuggestionController;->A:Z

    .line 319
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->w:Lcom/twitter/android/search/SearchSuggestionController$g;

    if-eqz v0, :cond_1

    .line 320
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->w:Lcom/twitter/android/search/SearchSuggestionController$g;

    invoke-interface {v0}, Lcom/twitter/android/search/SearchSuggestionController$g;->e_()V

    :cond_1
    move v3, v2

    .line 322
    goto/16 :goto_0

    .line 296
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->k:Lcom/twitter/android/search/h;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 297
    sget-object v1, Lcom/twitter/internal/android/widget/PopupEditText;->a:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    sget-object v4, Lcom/twitter/internal/android/widget/PopupEditText;->b:Landroid/widget/Filterable;

    .line 298
    invoke-static {}, Lcom/twitter/android/client/w;->h()I

    move-result v5

    int-to-long v6, v5

    .line 297
    invoke-virtual {v0, v1, v4, v6, v7}, Lcom/twitter/internal/android/widget/PopupEditText;->a(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;Landroid/widget/Filterable;J)V

    goto/16 :goto_1

    :cond_3
    move v1, v3

    .line 307
    goto :goto_2
.end method

.method public a(Lcom/twitter/library/widget/SearchQueryView;)Z
    .locals 1

    .prologue
    .line 511
    invoke-virtual {p1}, Lcom/twitter/library/widget/SearchQueryView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 512
    invoke-virtual {p0}, Lcom/twitter/android/search/SearchSuggestionController;->e()Z

    .line 516
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 514
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/SearchQueryView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 529
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    .line 530
    :goto_0
    iget-boolean v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->B:Z

    if-eq v0, v1, :cond_0

    .line 531
    iput-boolean v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->B:Z

    .line 532
    invoke-direct {p0}, Lcom/twitter/android/search/SearchSuggestionController;->i()V

    .line 534
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->r:Lcom/twitter/android/client/t;

    if-eqz v0, :cond_1

    .line 538
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/search/SearchSuggestionController;->a(Ljava/lang/CharSequence;)V

    .line 540
    :cond_1
    return-void

    .line 529
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/android/search/SearchSuggestionController;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->q:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    .line 255
    return-object p0
.end method

.method b()V
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->r:Lcom/twitter/android/client/t;

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->r:Lcom/twitter/android/client/t;

    invoke-virtual {v0}, Lcom/twitter/android/client/t;->b()V

    .line 360
    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->k:Lcom/twitter/android/search/h;

    invoke-virtual {v0, p1}, Lcom/twitter/android/search/h;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/search/SearchSuggestionController;->a(ILcom/twitter/model/search/viewmodel/SearchSuggestionListItem;)V

    .line 365
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 430
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/search/SearchSuggestionController;->a(Ljava/lang/CharSequence;Z)V

    .line 431
    return-void
.end method

.method b(Lazv;)Z
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 327
    iget-boolean v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->A:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->F:Z

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 345
    :goto_0
    return v0

    .line 330
    :cond_1
    invoke-static {}, Lcom/twitter/android/provider/SuggestionsProvider;->a()V

    .line 331
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->j:Lcom/twitter/android/search/i;

    iget-object v2, p0, Lcom/twitter/android/search/SearchSuggestionController;->m:Lcom/twitter/util/q;

    invoke-virtual {v0, v2}, Lcom/twitter/android/search/i;->b(Lcom/twitter/util/q;)V

    .line 333
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->s:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/PopupEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 334
    invoke-virtual {p1}, Lazv;->e()Landroid/view/View;

    move-result-object v0

    .line 335
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 336
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->g:Lcom/twitter/android/search/SearchSuggestionController$QueryTextUpdateReceiver;

    iget-object v2, p0, Lcom/twitter/android/search/SearchSuggestionController;->t:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/twitter/android/search/SearchSuggestionController$QueryTextUpdateReceiver;->a(Ljava/lang/String;)Lcom/twitter/android/search/SearchSuggestionController$QueryTextUpdateReceiver;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/search/SearchSuggestionController;->s:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0, v2}, Lcom/twitter/android/search/SearchSuggestionController$QueryTextUpdateReceiver;->a(Landroid/widget/TextView;)Lcom/twitter/android/search/SearchSuggestionController$QueryTextUpdateReceiver;

    .line 337
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->h:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/twitter/android/search/SearchSuggestionController;->s:Lcom/twitter/internal/android/widget/PopupEditText;

    iget-object v3, p0, Lcom/twitter/android/search/SearchSuggestionController;->g:Lcom/twitter/android/search/SearchSuggestionController$QueryTextUpdateReceiver;

    invoke-static {v0, v2, v1, v3}, Lcom/twitter/util/ui/k;->a(Landroid/content/Context;Landroid/view/View;ZLandroid/os/ResultReceiver;)V

    .line 339
    invoke-direct {p0}, Lcom/twitter/android/search/SearchSuggestionController;->j()V

    .line 340
    iput-boolean v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->A:Z

    .line 341
    invoke-virtual {p0}, Lcom/twitter/android/search/SearchSuggestionController;->b()V

    .line 342
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->w:Lcom/twitter/android/search/SearchSuggestionController$g;

    if-eqz v0, :cond_2

    .line 343
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->w:Lcom/twitter/android/search/SearchSuggestionController$g;

    invoke-interface {v0}, Lcom/twitter/android/search/SearchSuggestionController$g;->k_()V

    .line 345
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 521
    return-void
.end method

.method c()V
    .locals 2

    .prologue
    .line 462
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->b:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->a:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v0

    .line 463
    if-eqz v0, :cond_0

    .line 464
    invoke-virtual {v0}, Landroid/support/v4/content/Loader;->onContentChanged()V

    .line 466
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController;->j:Lcom/twitter/android/search/i;

    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->s:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/search/i;->a(Ljava/lang/String;)V

    .line 467
    return-void
.end method

.method public c(I)V
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 560
    iput p1, p0, Lcom/twitter/android/search/SearchSuggestionController;->E:I

    .line 561
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 498
    iget-boolean v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->A:Z

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 502
    iget-boolean v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->F:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->A:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->v:Lazv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->v:Lazv;

    invoke-virtual {v0}, Lazv;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 506
    iget-boolean v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->A:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->v:Lazv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->v:Lazv;

    invoke-virtual {v0}, Lazv;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    .line 393
    invoke-virtual {p1}, Landroid/widget/TextView;->getId()I

    move-result v0

    .line 394
    const v1, 0x7f13065d

    if-ne v0, v1, :cond_3

    .line 395
    const/4 v0, 0x3

    if-eq p2, v0, :cond_0

    if-eqz p3, :cond_3

    const/16 v0, 0x42

    .line 396
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 397
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->s:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 398
    invoke-direct {p0, v2}, Lcom/twitter/android/search/SearchSuggestionController;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 399
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->x:Lcom/twitter/android/client/s;

    if-eqz v0, :cond_1

    .line 400
    iget-boolean v0, p0, Lcom/twitter/android/search/SearchSuggestionController;->D:Z

    if-eqz v0, :cond_2

    .line 401
    invoke-direct {p0}, Lcom/twitter/android/search/SearchSuggestionController;->g()I

    move-result v0

    move v1, v0

    .line 402
    :goto_0
    iget-object v3, p0, Lcom/twitter/android/search/SearchSuggestionController;->x:Lcom/twitter/android/client/s;

    .line 403
    invoke-static {v2}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/search/SearchSuggestionController;->q:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 402
    invoke-interface {v3, v0, v1, v2}, Lcom/twitter/android/client/s;->a(Ljava/lang/String;ILcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 405
    :cond_1
    const/4 v0, 0x1

    .line 409
    :goto_1
    return v0

    .line 401
    :cond_2
    const/4 v0, -0x1

    move v1, v0

    goto :goto_0

    .line 409
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 525
    return-void
.end method

.method public x()V
    .locals 0

    .prologue
    .line 414
    return-void
.end method
