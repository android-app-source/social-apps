.class public Lcom/twitter/android/search/SearchSuggestionController$h;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/search/SearchSuggestionController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "h"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/search/SearchSuggestionController;


# direct methods
.method public constructor <init>(Lcom/twitter/android/search/SearchSuggestionController;)V
    .locals 0

    .prologue
    .line 622
    iput-object p1, p0, Lcom/twitter/android/search/SearchSuggestionController$h;->a:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 625
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 626
    const v1, 0x7f13065d

    if-ne v0, v1, :cond_1

    .line 627
    check-cast p1, Lcom/twitter/internal/android/widget/PopupEditText;

    .line 628
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController$h;->a:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-static {v0}, Lcom/twitter/android/search/SearchSuggestionController;->b(Lcom/twitter/android/search/SearchSuggestionController;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p1, v2}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    .line 629
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController$h;->a:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-virtual {v0, p1}, Lcom/twitter/android/search/SearchSuggestionController;->a(Lcom/twitter/internal/android/widget/PopupEditText;)V

    .line 634
    :cond_0
    :goto_0
    return-void

    .line 630
    :cond_1
    const v1, 0x7f1307b1

    if-ne v0, v1, :cond_0

    .line 631
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 632
    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController$h;->a:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/search/SearchSuggestionController;->a(Ljava/lang/CharSequence;Z)V

    goto :goto_0
.end method
