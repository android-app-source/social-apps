.class public Lcom/twitter/android/search/SearchActivity2;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Lagd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/base/TwitterFragmentActivity;",
        "Lagd",
        "<",
        "Lage;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method

.method private i()Laif;
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/twitter/android/search/SearchActivity2;->Y()Lans;

    move-result-object v0

    check-cast v0, Lago;

    .line 78
    invoke-interface {v0}, Lago;->d()Laif;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->b(Z)V

    .line 28
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 29
    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(I)V

    .line 30
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic a(Lank;)Lcom/twitter/app/common/base/j;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/twitter/android/search/SearchActivity2;->d(Lank;)Lago;

    move-result-object v0

    return-object v0
.end method

.method public a(Lage;)Z
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/twitter/android/search/SearchActivity2;->i()Laif;

    move-result-object v0

    invoke-virtual {v0, p1}, Laif;->a(Lage;)Z

    move-result v0

    .line 59
    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/twitter/android/search/SearchActivity2;->M()Z

    .line 63
    :cond_0
    return v0
.end method

.method public bridge synthetic a(Lako;)Z
    .locals 1

    .prologue
    .line 21
    check-cast p1, Lage;

    invoke-virtual {p0, p1}, Lcom/twitter/android/search/SearchActivity2;->a(Lage;)Z

    move-result v0

    return v0
.end method

.method public a(Lcmm;)Z
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/twitter/android/search/SearchActivity2;->i()Laif;

    move-result-object v0

    invoke-virtual {v0, p1}, Laif;->a(Lcmm;)Z

    move-result v0

    .line 49
    if-eqz v0, :cond_0

    .line 50
    const/4 v0, 0x1

    .line 52
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmm;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lcmr;)Z
    .locals 3

    .prologue
    .line 35
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmr;)Z

    .line 36
    invoke-direct {p0}, Lcom/twitter/android/search/SearchActivity2;->i()Laif;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/search/SearchActivity2;->F()Lcmt;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/search/SearchActivity2;->P()Lcom/twitter/android/search/SearchSuggestionController;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Laif;->a(Lcmt;Lcom/twitter/android/search/SearchSuggestionController;)V

    .line 37
    const/4 v0, 0x1

    return v0
.end method

.method public b(Lcmr;)I
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/twitter/android/search/SearchActivity2;->i()Laif;

    move-result-object v0

    invoke-virtual {v0, p1}, Laif;->a(Lcmr;)V

    .line 43
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b(Lcmr;)I

    move-result v0

    return v0
.end method

.method protected synthetic b(Lank;)Lcom/twitter/app/common/abs/b;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/twitter/android/search/SearchActivity2;->d(Lank;)Lago;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic c(Lank;)Lans;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/twitter/android/search/SearchActivity2;->d(Lank;)Lago;

    move-result-object v0

    return-object v0
.end method

.method protected d(Lank;)Lago;
    .locals 2

    .prologue
    .line 69
    invoke-static {}, Lagn;->c()Lagn$a;

    move-result-object v0

    .line 70
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lagn$a;->a(Lamu;)Lagn$a;

    move-result-object v0

    new-instance v1, Lant;

    invoke-direct {v1, p0, p1}, Lant;-><init>(Landroid/app/Activity;Lank;)V

    .line 71
    invoke-virtual {v0, v1}, Lagn$a;->a(Lant;)Lagn$a;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lagn$a;->a()Lago;

    move-result-object v0

    .line 69
    return-object v0
.end method

.method protected k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/twitter/android/search/SearchActivity2;->i()Laif;

    move-result-object v0

    invoke-virtual {v0}, Laif;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
