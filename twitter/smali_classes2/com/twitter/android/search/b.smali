.class public Lcom/twitter/android/search/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/client/s;


# instance fields
.field private final a:Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$a;


# direct methods
.method public constructor <init>(Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$a;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/twitter/android/search/b;->a:Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$a;

    .line 20
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;IILjava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 3

    .prologue
    .line 26
    .line 27
    invoke-static {p1}, Lcom/twitter/android/client/r;->a(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;)Lcom/twitter/android/client/r;

    move-result-object v0

    .line 28
    iget-object v1, p0, Lcom/twitter/android/search/b;->a:Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$a;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$a;->a(ILjava/lang/Object;)V

    .line 29
    return-void
.end method

.method public a(Ljava/lang/String;ILcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 3

    .prologue
    .line 34
    invoke-static {p1}, Lcom/twitter/android/client/r;->a(Ljava/lang/String;)Lcom/twitter/android/client/r;

    move-result-object v0

    .line 35
    iget-object v1, p0, Lcom/twitter/android/search/b;->a:Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$a;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/moments/ui/maker/MomentMakerSearchActivity$a;->a(ILjava/lang/Object;)V

    .line 36
    return-void
.end method
