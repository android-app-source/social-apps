.class public Lcom/twitter/android/search/SearchSuggestionController$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/search/SearchSuggestionController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/search/SearchSuggestionController;


# direct methods
.method public constructor <init>(Lcom/twitter/android/search/SearchSuggestionController;)V
    .locals 0

    .prologue
    .line 663
    iput-object p1, p0, Lcom/twitter/android/search/SearchSuggestionController$a;->a:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 666
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController$a;->a:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-static {v0}, Lcom/twitter/android/search/SearchSuggestionController;->b(Lcom/twitter/android/search/SearchSuggestionController;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0a0748

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 667
    new-instance v1, Lcom/twitter/android/widget/aj$b;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    .line 668
    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/aj$b;->a(Ljava/lang/CharSequence;)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0105

    .line 669
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a00f6

    .line 670
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 671
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/search/SearchSuggestionController$d;

    iget-object v2, p0, Lcom/twitter/android/search/SearchSuggestionController$a;->a:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-direct {v1, v2}, Lcom/twitter/android/search/SearchSuggestionController$d;-><init>(Lcom/twitter/android/search/SearchSuggestionController;)V

    .line 672
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController$a;->a:Lcom/twitter/android/search/SearchSuggestionController;

    .line 673
    invoke-static {v1}, Lcom/twitter/android/search/SearchSuggestionController;->i(Lcom/twitter/android/search/SearchSuggestionController;)Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "TAG_CLEAR_RECENT_SEARCH_DIALOG"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 674
    return-void
.end method
