.class public Lcom/twitter/android/search/SearchSuggestionController$b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/search/SearchSuggestionController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/search/SearchSuggestionController;


# direct methods
.method public constructor <init>(Lcom/twitter/android/search/SearchSuggestionController;)V
    .locals 0

    .prologue
    .line 637
    iput-object p1, p0, Lcom/twitter/android/search/SearchSuggestionController$b;->a:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;)Z
    .locals 2

    .prologue
    .line 640
    invoke-virtual {p1}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->c()Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;->e:Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 644
    invoke-direct {p0, p1}, Lcom/twitter/android/search/SearchSuggestionController$b;->b(Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 645
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController$b;->a:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-virtual {p1}, Lcom/twitter/model/search/viewmodel/SearchSuggestionListItem;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/search/SearchSuggestionController;->a(Lcom/twitter/android/search/SearchSuggestionController;Ljava/lang/String;)Ljava/lang/String;

    .line 647
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController$b;->a:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-static {v0}, Lcom/twitter/android/search/SearchSuggestionController;->h(Lcom/twitter/android/search/SearchSuggestionController;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 648
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController$b;->a:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-static {v0}, Lcom/twitter/android/search/SearchSuggestionController;->b(Lcom/twitter/android/search/SearchSuggestionController;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0a0746

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/search/SearchSuggestionController$b;->a:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-static {v4}, Lcom/twitter/android/search/SearchSuggestionController;->h(Lcom/twitter/android/search/SearchSuggestionController;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 652
    :goto_0
    new-instance v1, Lcom/twitter/android/widget/aj$b;

    invoke-direct {v1, v5}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    .line 653
    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/aj$b;->a(Ljava/lang/CharSequence;)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0105

    .line 654
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a00f6

    .line 655
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 656
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/search/SearchSuggestionController$d;

    iget-object v2, p0, Lcom/twitter/android/search/SearchSuggestionController$b;->a:Lcom/twitter/android/search/SearchSuggestionController;

    iget-object v3, p0, Lcom/twitter/android/search/SearchSuggestionController$b;->a:Lcom/twitter/android/search/SearchSuggestionController;

    .line 657
    invoke-static {v3}, Lcom/twitter/android/search/SearchSuggestionController;->h(Lcom/twitter/android/search/SearchSuggestionController;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/twitter/android/search/SearchSuggestionController$d;-><init>(Lcom/twitter/android/search/SearchSuggestionController;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/search/SearchSuggestionController$b;->a:Lcom/twitter/android/search/SearchSuggestionController;

    .line 658
    invoke-static {v1}, Lcom/twitter/android/search/SearchSuggestionController;->i(Lcom/twitter/android/search/SearchSuggestionController;)Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "TAG_CLEAR_RECENT_SEARCH_DIALOG"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 660
    :cond_0
    return-void

    .line 650
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController$b;->a:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-static {v0}, Lcom/twitter/android/search/SearchSuggestionController;->b(Lcom/twitter/android/search/SearchSuggestionController;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0a0747

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
