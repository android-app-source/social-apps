.class Lcom/twitter/android/search/SearchSuggestionController$e;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/search/SearchSuggestionController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "e"
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/android/search/SearchSuggestionController;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/twitter/android/search/SearchSuggestionController;)V
    .locals 1

    .prologue
    .line 585
    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    .line 586
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController$e;->a:Ljava/lang/ref/WeakReference;

    .line 587
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 582
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/android/search/SearchSuggestionController$e;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 2

    .prologue
    .line 591
    move-object v0, p1

    check-cast v0, Lcom/twitter/library/api/search/m;

    .line 592
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/service/u;

    invoke-virtual {v1}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 593
    invoke-virtual {v0}, Lcom/twitter/library/api/search/m;->e()I

    move-result v1

    if-nez v1, :cond_0

    .line 594
    invoke-virtual {v0}, Lcom/twitter/library/api/search/m;->h()Lcom/twitter/library/api/search/TwitterTypeAheadGroup;

    move-result-object v1

    .line 595
    invoke-virtual {v0}, Lcom/twitter/library/api/search/m;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Ljava/lang/String;Lcom/twitter/library/api/search/TwitterTypeAheadGroup;)V

    .line 597
    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/search/TwitterTypeAheadGroup;

    invoke-virtual {v0}, Lcom/twitter/library/api/search/TwitterTypeAheadGroup;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 598
    iget-object v0, p0, Lcom/twitter/android/search/SearchSuggestionController$e;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/search/SearchSuggestionController;

    .line 599
    if-eqz v0, :cond_0

    .line 600
    invoke-virtual {v0}, Lcom/twitter/android/search/SearchSuggestionController;->c()V

    .line 604
    :cond_0
    return-void
.end method
