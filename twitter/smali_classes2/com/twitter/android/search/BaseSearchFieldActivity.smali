.class public abstract Lcom/twitter/android/search/BaseSearchFieldActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected synthetic a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Laog;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/search/BaseSearchFieldActivity;->b(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Laih;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->b(Z)V

    .line 30
    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 31
    const/16 v0, 0x104

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(I)V

    .line 32
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcmr;)I
    .locals 3

    .prologue
    .line 44
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b(Lcmr;)I

    move-result v1

    .line 45
    invoke-virtual {p0}, Lcom/twitter/android/search/BaseSearchFieldActivity;->Z()Laog;

    move-result-object v0

    check-cast v0, Laih;

    .line 46
    invoke-virtual {p0}, Lcom/twitter/android/search/BaseSearchFieldActivity;->P()Lcom/twitter/android/search/SearchSuggestionController;

    move-result-object v2

    invoke-virtual {v0, v2}, Laih;->a(Lcom/twitter/android/search/SearchSuggestionController;)V

    .line 47
    return v1
.end method

.method protected b(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Laih;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/twitter/android/search/BaseSearchFieldActivity;->b()Lcom/twitter/android/client/s;

    move-result-object v0

    invoke-static {p0, v0}, Laih;->a(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/android/client/s;)Laih;

    move-result-object v0

    return-object v0
.end method

.method protected abstract b()Lcom/twitter/android/client/s;
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/twitter/android/search/BaseSearchFieldActivity;->finish()V

    .line 55
    return-void
.end method
