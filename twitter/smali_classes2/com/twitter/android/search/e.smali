.class public Lcom/twitter/android/search/e;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/search/e$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:Z

.field private final e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;IZZZ)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/twitter/android/search/e;->a:Landroid/content/Context;

    .line 44
    iput p3, p0, Lcom/twitter/android/search/e;->c:I

    .line 45
    iput-object p2, p0, Lcom/twitter/android/search/e;->b:Ljava/lang/String;

    .line 46
    iput-boolean p4, p0, Lcom/twitter/android/search/e;->f:Z

    .line 47
    iput-boolean p5, p0, Lcom/twitter/android/search/e;->e:Z

    .line 48
    iput-boolean p6, p0, Lcom/twitter/android/search/e;->d:Z

    .line 49
    return-void
.end method

.method private static a(I)I
    .locals 0

    .prologue
    .line 114
    packed-switch p0, :pswitch_data_0

    .line 150
    :goto_0
    :pswitch_0
    return p0

    .line 117
    :pswitch_1
    const/4 p0, 0x0

    .line 118
    goto :goto_0

    .line 122
    :pswitch_2
    const/4 p0, 0x6

    .line 123
    goto :goto_0

    .line 127
    :pswitch_3
    const/4 p0, 0x3

    .line 128
    goto :goto_0

    .line 132
    :pswitch_4
    const/16 p0, 0xb

    .line 133
    goto :goto_0

    .line 137
    :pswitch_5
    const/4 p0, 0x7

    .line 138
    goto :goto_0

    .line 142
    :pswitch_6
    const/16 p0, 0x9

    .line 143
    goto :goto_0

    .line 114
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_6
        :pswitch_6
    .end packed-switch
.end method

.method private a(Ljava/util/List;Landroid/database/Cursor;Ljava/util/List;Lcom/twitter/android/bu;IZIII)Lcom/twitter/android/bu;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/bu;",
            ">;",
            "Landroid/database/Cursor;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/search/e$a;",
            ">;",
            "Lcom/twitter/android/bu;",
            "IZIII)",
            "Lcom/twitter/android/bu;"
        }
    .end annotation

    .prologue
    .line 209
    move/from16 v0, p8

    move/from16 v1, p7

    if-lt v0, v1, :cond_0

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    move-object/from16 v5, p4

    .line 391
    :cond_1
    :goto_0
    return-object v5

    .line 213
    :cond_2
    packed-switch p9, :pswitch_data_0

    .line 369
    :pswitch_0
    iget v2, p0, Lcom/twitter/android/search/e;->c:I

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/twitter/android/search/e;->b:Ljava/lang/String;

    invoke-static {v2}, Lcom/twitter/android/events/a;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_12

    .line 370
    :cond_3
    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/search/e$a;

    iget-wide v6, v2, Lcom/twitter/android/search/e$a;->b:J

    move-object v3, p0

    move-object v4, p1

    move-object/from16 v5, p4

    move/from16 v8, p9

    move/from16 v9, p5

    move/from16 v10, p7

    move/from16 v11, p8

    invoke-direct/range {v3 .. v11}, Lcom/twitter/android/search/e;->a(Ljava/util/List;Lcom/twitter/android/bu;JIIII)Lcom/twitter/android/bu;

    move-result-object v5

    .line 376
    :cond_4
    :goto_1
    if-eqz p6, :cond_1

    .line 377
    packed-switch p9, :pswitch_data_1

    goto :goto_0

    .line 379
    :pswitch_1
    const-wide/16 v6, -0x1

    const/16 v8, 0x13

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v3, p0

    move-object v4, p1

    move/from16 v9, p5

    invoke-direct/range {v3 .. v11}, Lcom/twitter/android/search/e;->a(Ljava/util/List;Lcom/twitter/android/bu;JIIII)Lcom/twitter/android/bu;

    move-result-object v5

    .line 382
    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/search/e$a;

    iget-object v2, v2, Lcom/twitter/android/search/e$a;->a:[B

    sget-object v3, Lcom/twitter/model/search/a;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v2, v3}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/search/a;

    iput-object v2, v5, Lcom/twitter/android/bu;->i:Lcom/twitter/model/search/a;

    goto :goto_0

    .line 215
    :pswitch_2
    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/search/e$a;

    iget-wide v6, v2, Lcom/twitter/android/search/e$a;->b:J

    const/4 v8, 0x6

    move-object v3, p0

    move-object v4, p1

    move-object/from16 v5, p4

    move/from16 v9, p5

    move/from16 v10, p7

    move/from16 v11, p8

    invoke-direct/range {v3 .. v11}, Lcom/twitter/android/search/e;->a(Ljava/util/List;Lcom/twitter/android/bu;JIIII)Lcom/twitter/android/bu;

    move-result-object v5

    .line 218
    const/4 v2, 0x0

    .line 219
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v3, v2

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/search/e$a;

    .line 220
    iget-object v2, v2, Lcom/twitter/android/search/e$a;->a:[B

    sget-object v6, Lcom/twitter/util/serialization/f;->b:Lcom/twitter/util/serialization/l;

    invoke-static {v2, v6}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    .line 221
    if-eqz v2, :cond_13

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 222
    add-int/lit8 v3, v3, 0x1

    move v2, v3

    :goto_3
    move v3, v2

    .line 224
    goto :goto_2

    .line 226
    :cond_5
    if-eqz v3, :cond_4

    sub-int v2, p8, p7

    add-int/lit8 v2, v2, 0x1

    if-ne v3, v2, :cond_4

    .line 227
    const-wide/16 v6, -0x1

    const/16 v8, 0xd

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v3, p0

    move-object v4, p1

    move/from16 v9, p5

    invoke-direct/range {v3 .. v11}, Lcom/twitter/android/search/e;->a(Ljava/util/List;Lcom/twitter/android/bu;JIIII)Lcom/twitter/android/bu;

    move-result-object v5

    goto/16 :goto_1

    .line 233
    :pswitch_3
    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/search/e$a;

    iget-wide v6, v2, Lcom/twitter/android/search/e$a;->b:J

    const/16 v8, 0x8

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v3, p0

    move-object v4, p1

    move-object/from16 v5, p4

    move/from16 v9, p5

    invoke-direct/range {v3 .. v11}, Lcom/twitter/android/search/e;->a(Ljava/util/List;Lcom/twitter/android/bu;JIIII)Lcom/twitter/android/bu;

    move-result-object v5

    goto/16 :goto_1

    .line 239
    :pswitch_4
    const-wide/16 v6, -0x1

    const/16 v8, 0xe

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v3, p0

    move-object v4, p1

    move-object/from16 v5, p4

    move/from16 v9, p5

    invoke-direct/range {v3 .. v11}, Lcom/twitter/android/search/e;->a(Ljava/util/List;Lcom/twitter/android/bu;JIIII)Lcom/twitter/android/bu;

    move-result-object v5

    .line 241
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_6
    :goto_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/search/e$a;

    .line 242
    iget-object v3, v2, Lcom/twitter/android/search/e$a;->a:[B

    if-eqz v3, :cond_6

    .line 243
    iget-object v3, v2, Lcom/twitter/android/search/e$a;->a:[B

    sget-object v4, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    invoke-static {v3, v4}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v3

    move-object v12, v3

    check-cast v12, Ljava/lang/String;

    .line 244
    if-eqz v12, :cond_6

    .line 245
    iget-wide v6, v2, Lcom/twitter/android/search/e$a;->b:J

    const/4 v8, 0x3

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v3, p0

    move-object v4, p1

    move/from16 v9, p5

    invoke-direct/range {v3 .. v11}, Lcom/twitter/android/search/e;->a(Ljava/util/List;Lcom/twitter/android/bu;JIIII)Lcom/twitter/android/bu;

    move-result-object v5

    .line 247
    iput-object v12, v5, Lcom/twitter/android/bu;->h:Ljava/lang/String;

    goto :goto_4

    .line 254
    :pswitch_5
    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/search/e$a;

    .line 255
    iget-object v3, v2, Lcom/twitter/android/search/e$a;->a:[B

    if-eqz v3, :cond_12

    .line 256
    iget-object v3, v2, Lcom/twitter/android/search/e$a;->a:[B

    sget-object v4, Lcom/twitter/model/search/c;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v3, v4}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v3

    move-object v12, v3

    check-cast v12, Lcom/twitter/model/search/c;

    .line 258
    if-eqz v12, :cond_7

    .line 259
    iget-wide v6, v2, Lcom/twitter/android/search/e$a;->b:J

    move-object v3, p0

    move-object v4, p1

    move-object/from16 v5, p4

    move/from16 v8, p9

    move/from16 v9, p5

    move/from16 v10, p7

    move/from16 v11, p8

    invoke-direct/range {v3 .. v11}, Lcom/twitter/android/search/e;->a(Ljava/util/List;Lcom/twitter/android/bu;JIIII)Lcom/twitter/android/bu;

    move-result-object p4

    .line 261
    move-object/from16 v0, p4

    iput-object v12, v0, Lcom/twitter/android/bu;->g:Lcom/twitter/model/search/c;

    :cond_7
    move-object/from16 v5, p4

    .line 263
    goto/16 :goto_1

    .line 268
    :pswitch_6
    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/search/e$a;

    .line 269
    iget-object v3, v2, Lcom/twitter/android/search/e$a;->a:[B

    if-eqz v3, :cond_12

    .line 270
    iget-object v3, v2, Lcom/twitter/android/search/e$a;->a:[B

    sget-object v4, Lcom/twitter/model/search/d;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v3, v4}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v3

    move-object v12, v3

    check-cast v12, Lcom/twitter/model/search/d;

    .line 272
    if-eqz v12, :cond_12

    .line 274
    invoke-virtual {v12}, Lcom/twitter/model/search/d;->b()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 275
    const-wide/16 v6, -0x1

    const/16 v8, 0x11

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v3, p0

    move-object v4, p1

    move-object/from16 v5, p4

    move/from16 v9, p5

    invoke-direct/range {v3 .. v11}, Lcom/twitter/android/search/e;->a(Ljava/util/List;Lcom/twitter/android/bu;JIIII)Lcom/twitter/android/bu;

    move-result-object v5

    .line 278
    iput-object v12, v5, Lcom/twitter/android/bu;->j:Lcom/twitter/model/search/d;

    move/from16 v13, p7

    .line 279
    :goto_5
    move/from16 v0, p8

    if-gt v13, v0, :cond_4

    .line 280
    sub-int v2, v13, p7

    .line 281
    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/search/e$a;

    iget-wide v6, v2, Lcom/twitter/android/search/e$a;->b:J

    const/16 v8, 0x12

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v3, p0

    move-object v4, p1

    move/from16 v9, p5

    .line 280
    invoke-direct/range {v3 .. v11}, Lcom/twitter/android/search/e;->a(Ljava/util/List;Lcom/twitter/android/bu;JIIII)Lcom/twitter/android/bu;

    move-result-object v5

    .line 283
    iput-object v12, v5, Lcom/twitter/android/bu;->j:Lcom/twitter/model/search/d;

    .line 279
    add-int/lit8 v2, v13, 0x1

    move v13, v2

    goto :goto_5

    .line 287
    :cond_8
    iget-wide v6, v2, Lcom/twitter/android/search/e$a;->b:J

    move-object v3, p0

    move-object v4, p1

    move-object/from16 v5, p4

    move/from16 v8, p9

    move/from16 v9, p5

    move/from16 v10, p7

    move/from16 v11, p8

    invoke-direct/range {v3 .. v11}, Lcom/twitter/android/search/e;->a(Ljava/util/List;Lcom/twitter/android/bu;JIIII)Lcom/twitter/android/bu;

    move-result-object v5

    .line 289
    iput-object v12, v5, Lcom/twitter/android/bu;->j:Lcom/twitter/model/search/d;

    goto/16 :goto_1

    .line 296
    :pswitch_7
    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/search/e$a;

    iget-wide v6, v2, Lcom/twitter/android/search/e$a;->b:J

    const/16 v8, 0x14

    move-object v3, p0

    move-object v4, p1

    move-object/from16 v5, p4

    move/from16 v9, p5

    move/from16 v10, p7

    move/from16 v11, p7

    invoke-direct/range {v3 .. v11}, Lcom/twitter/android/search/e;->a(Ljava/util/List;Lcom/twitter/android/bu;JIIII)Lcom/twitter/android/bu;

    move-result-object v5

    .line 299
    add-int/lit8 v10, p7, 0x1

    :goto_6
    move/from16 v0, p8

    if-gt v10, v0, :cond_4

    .line 300
    sub-int v2, v10, p7

    .line 301
    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/search/e$a;

    iget-wide v6, v2, Lcom/twitter/android/search/e$a;->b:J

    const/16 v8, 0x14

    move-object v3, p0

    move-object v4, p1

    move/from16 v9, p5

    move v11, v10

    .line 300
    invoke-direct/range {v3 .. v11}, Lcom/twitter/android/search/e;->a(Ljava/util/List;Lcom/twitter/android/bu;JIIII)Lcom/twitter/android/bu;

    move-result-object v5

    .line 299
    add-int/lit8 v10, v10, 0x1

    goto :goto_6

    .line 308
    :pswitch_8
    iget-object v2, p0, Lcom/twitter/android/search/e;->a:Landroid/content/Context;

    invoke-static {v2}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v2

    invoke-virtual {v2}, Lbaa;->b()Z

    move-result v2

    if-eqz v2, :cond_9

    const-string/jumbo v2, "twitter_access_android_media_forward_enabled"

    .line 309
    invoke-static {v2}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 311
    :cond_9
    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/search/e$a;

    iget-wide v6, v2, Lcom/twitter/android/search/e$a;->b:J

    move-object v3, p0

    move-object v4, p1

    move-object/from16 v5, p4

    move/from16 v8, p9

    move/from16 v9, p5

    move/from16 v10, p7

    move/from16 v11, p8

    invoke-direct/range {v3 .. v11}, Lcom/twitter/android/search/e;->a(Ljava/util/List;Lcom/twitter/android/bu;JIIII)Lcom/twitter/android/bu;

    move-result-object v5

    .line 313
    const-wide/16 v6, -0x1

    const/16 v8, 0xf

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v3, p0

    move-object v4, p1

    move/from16 v9, p5

    invoke-direct/range {v3 .. v11}, Lcom/twitter/android/search/e;->a(Ljava/util/List;Lcom/twitter/android/bu;JIIII)Lcom/twitter/android/bu;

    move-result-object v5

    goto/16 :goto_1

    .line 320
    :pswitch_9
    iget-boolean v2, p0, Lcom/twitter/android/search/e;->f:Z

    if-nez v2, :cond_a

    iget-boolean v2, p0, Lcom/twitter/android/search/e;->e:Z

    if-eqz v2, :cond_d

    .line 321
    :cond_a
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    .line 322
    move-object/from16 v0, p2

    move/from16 v1, p7

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 323
    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/twitter/android/search/e;->b(Landroid/database/Cursor;)Lcom/twitter/android/widget/TopicView$TopicData;

    move-result-object v3

    .line 324
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 326
    iget-boolean v2, p0, Lcom/twitter/android/search/e;->f:Z

    if-nez v2, :cond_b

    iget-object v2, p0, Lcom/twitter/android/search/e;->b:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/twitter/android/search/e;->b:Ljava/lang/String;

    iget-object v4, v3, Lcom/twitter/android/widget/TopicView$TopicData;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 327
    :cond_b
    iget-object v2, p0, Lcom/twitter/android/search/e;->a:Landroid/content/Context;

    instance-of v2, v2, Lcom/twitter/android/u;

    if-eqz v2, :cond_c

    .line 328
    iget-object v2, p0, Lcom/twitter/android/search/e;->a:Landroid/content/Context;

    check-cast v2, Lcom/twitter/android/u;

    invoke-interface {v2, v3}, Lcom/twitter/android/u;->a(Lcom/twitter/android/widget/TopicView$TopicData;)V

    .line 329
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/twitter/android/search/e;->f:Z

    .line 331
    :cond_c
    iget-boolean v2, p0, Lcom/twitter/android/search/e;->d:Z

    if-eqz v2, :cond_d

    move-object/from16 v5, p4

    .line 332
    goto/16 :goto_1

    :cond_d
    move/from16 v10, p7

    move-object/from16 v5, p4

    .line 337
    :goto_7
    move/from16 v0, p8

    if-gt v10, v0, :cond_11

    .line 338
    move-object/from16 v0, p2

    invoke-interface {v0, v10}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 339
    sget v2, Lbtv;->r:I

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 344
    iget v3, p0, Lcom/twitter/android/search/e;->c:I

    const/16 v4, 0x8

    if-eq v3, v4, :cond_e

    .line 345
    invoke-static {v2}, Lcom/twitter/android/events/a;->f(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_10

    .line 346
    :cond_e
    sub-int v2, v10, p7

    .line 347
    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/search/e$a;

    iget-wide v6, v2, Lcom/twitter/android/search/e$a;->b:J

    sget v2, Lbtv;->s:I

    .line 348
    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Lcom/twitter/android/bu;->a(I)I

    move-result v8

    move-object v3, p0

    move-object v4, p1

    move/from16 v9, p5

    move v11, v10

    .line 346
    invoke-direct/range {v3 .. v11}, Lcom/twitter/android/search/e;->a(Ljava/util/List;Lcom/twitter/android/bu;JIIII)Lcom/twitter/android/bu;

    move-result-object v5

    .line 351
    sget v2, Lbtv;->y:I

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 353
    const-string/jumbo v3, "IN_PROGRESS"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    const-string/jumbo v3, "SCHEDULED"

    .line 354
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 355
    :cond_f
    const/4 v2, 0x1

    iput-boolean v2, v5, Lcom/twitter/android/bu;->k:Z

    .line 337
    :cond_10
    add-int/lit8 v10, v10, 0x1

    goto :goto_7

    .line 360
    :cond_11
    add-int/lit8 v2, p8, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto/16 :goto_1

    :pswitch_a
    move-object/from16 v5, p4

    .line 364
    goto/16 :goto_1

    :cond_12
    move-object/from16 v5, p4

    goto/16 :goto_1

    :cond_13
    move v2, v3

    goto/16 :goto_3

    .line 213
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_8
        :pswitch_3
        :pswitch_7
        :pswitch_9
        :pswitch_6
        :pswitch_a
    .end packed-switch

    .line 377
    :pswitch_data_1
    .packed-switch 0x9
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ljava/util/List;Lcom/twitter/android/bu;JIIII)Lcom/twitter/android/bu;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/bu;",
            ">;",
            "Lcom/twitter/android/bu;",
            "JIIII)",
            "Lcom/twitter/android/bu;"
        }
    .end annotation

    .prologue
    .line 188
    new-instance v1, Lcom/twitter/android/bu;

    move-wide v2, p3

    move v4, p5

    move v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    invoke-direct/range {v1 .. v7}, Lcom/twitter/android/bu;-><init>(JIIII)V

    .line 189
    iget v0, p0, Lcom/twitter/android/search/e;->c:I

    invoke-static {v1, p2, v0}, Lcom/twitter/android/search/e;->a(Lcom/twitter/android/bu;Lcom/twitter/android/bu;I)V

    .line 190
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    return-object v1
.end method

.method private static a(Lcom/twitter/android/bu;)V
    .locals 2

    .prologue
    .line 103
    if-eqz p0, :cond_0

    .line 104
    iget v0, p0, Lcom/twitter/android/bu;->f:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 105
    const/4 v0, 0x4

    iput v0, p0, Lcom/twitter/android/bu;->f:I

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    const/4 v0, 0x3

    iput v0, p0, Lcom/twitter/android/bu;->f:I

    goto :goto_0
.end method

.method private static a(Lcom/twitter/android/bu;Lcom/twitter/android/bu;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, -0x1

    .line 156
    iget v2, p0, Lcom/twitter/android/bu;->b:I

    .line 158
    if-eqz p1, :cond_0

    .line 159
    iget v0, p1, Lcom/twitter/android/bu;->b:I

    .line 165
    :goto_0
    const/4 v3, 0x7

    if-ne v2, v3, :cond_1

    .line 166
    const/4 v0, 0x5

    iput v0, p0, Lcom/twitter/android/bu;->f:I

    .line 167
    invoke-static {p1}, Lcom/twitter/android/search/e;->a(Lcom/twitter/android/bu;)V

    .line 179
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 161
    goto :goto_0

    .line 168
    :cond_1
    const/16 v3, 0x11

    if-ne v2, v3, :cond_2

    .line 169
    const/4 v0, 0x3

    iput v0, p0, Lcom/twitter/android/bu;->f:I

    goto :goto_1

    .line 170
    :cond_2
    if-eq v0, v1, :cond_3

    .line 171
    invoke-static {v2, v0}, Lcom/twitter/android/search/e;->a(II)Z

    move-result v0

    if-nez v0, :cond_3

    .line 172
    iput v4, p0, Lcom/twitter/android/bu;->f:I

    .line 173
    invoke-static {p1}, Lcom/twitter/android/search/e;->a(Lcom/twitter/android/bu;)V

    goto :goto_1

    .line 174
    :cond_3
    const/16 v0, 0x8

    if-ne p2, v0, :cond_4

    if-eqz p1, :cond_4

    .line 175
    iput v4, p0, Lcom/twitter/android/bu;->f:I

    goto :goto_1

    .line 177
    :cond_4
    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/android/bu;->f:I

    goto :goto_1
.end method

.method private static a(II)Z
    .locals 2

    .prologue
    .line 182
    invoke-static {p1}, Lcom/twitter/android/search/e;->a(I)I

    move-result v0

    invoke-static {p0}, Lcom/twitter/android/search/e;->a(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/database/Cursor;)Lcom/twitter/android/widget/TopicView$TopicData;
    .locals 18

    .prologue
    .line 396
    sget v2, Lbtv;->r:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 397
    sget v2, Lbtv;->s:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 398
    sget v2, Lbtv;->u:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 399
    sget v2, Lbtv;->v:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 400
    sget v2, Lbtv;->z:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 401
    sget v2, Lbtv;->w:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 402
    sget v2, Lbtv;->A:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 403
    sget v2, Lbtv;->t:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 404
    sget v2, Lbtv;->x:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 405
    sget v2, Lbtv;->y:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 406
    sget v2, Lbtv;->D:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v16

    .line 407
    sget v2, Lbtv;->B:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 408
    sget v2, Lbtv;->C:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 409
    new-instance v3, Lcom/twitter/android/widget/TopicView$TopicData;

    invoke-direct/range {v3 .. v17}, Lcom/twitter/android/widget/TopicView$TopicData;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJ[BLjava/lang/String;)V

    return-object v3
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/bu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    const/4 v6, 0x0

    .line 62
    if-eqz p1, :cond_1

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 63
    new-instance v15, Ljava/util/ArrayList;

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v15, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 64
    const/4 v12, 0x0

    .line 65
    const/4 v9, 0x0

    .line 67
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 68
    const/4 v11, -0x1

    .line 69
    const/4 v7, -0x1

    .line 71
    :goto_0
    sget v2, Lbtv;->c:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 72
    sget v2, Lbtv;->d:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 73
    if-eq v7, v13, :cond_2

    .line 74
    invoke-static {v14, v11}, Lcom/twitter/android/search/e;->a(II)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v8, 0x1

    .line 75
    :goto_1
    add-int/lit8 v10, v12, -0x1

    move-object/from16 v2, p0

    move-object v3, v15

    move-object/from16 v4, p1

    move-object/from16 v5, v16

    .line 76
    invoke-direct/range {v2 .. v11}, Lcom/twitter/android/search/e;->a(Ljava/util/List;Landroid/database/Cursor;Ljava/util/List;Lcom/twitter/android/bu;IZIII)Lcom/twitter/android/bu;

    move-result-object v6

    .line 80
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->clear()V

    move v9, v12

    move-object v11, v6

    .line 82
    :goto_2
    new-instance v2, Lcom/twitter/android/search/e$a;

    sget v3, Lbtv;->e:I

    .line 83
    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    const/4 v4, 0x0

    .line 84
    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v6, 0x1

    .line 85
    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-direct/range {v2 .. v7}, Lcom/twitter/android/search/e$a;-><init>([BJJ)V

    .line 82
    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    add-int/lit8 v12, v12, 0x1

    .line 89
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 92
    add-int/lit8 v10, v12, -0x1

    .line 93
    const/4 v8, 0x0

    move-object/from16 v2, p0

    move-object v3, v15

    move-object/from16 v4, p1

    move-object/from16 v5, v16

    move-object v6, v11

    move v7, v13

    move v11, v14

    invoke-direct/range {v2 .. v11}, Lcom/twitter/android/search/e;->a(Ljava/util/List;Landroid/database/Cursor;Ljava/util/List;Lcom/twitter/android/bu;IZIII)Lcom/twitter/android/bu;

    move-result-object v2

    .line 95
    invoke-static {v2}, Lcom/twitter/android/search/e;->a(Lcom/twitter/android/bu;)V

    move-object v2, v15

    .line 99
    :goto_3
    return-object v2

    .line 74
    :cond_0
    const/4 v8, 0x0

    goto :goto_1

    .line 97
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    goto :goto_3

    :cond_2
    move-object v11, v6

    goto :goto_2

    :cond_3
    move v7, v13

    move-object v6, v11

    move v11, v14

    goto :goto_0
.end method
