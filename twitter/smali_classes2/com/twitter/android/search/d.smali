.class public Lcom/twitter/android/search/d;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Lcoi$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-string/jumbo v0, "android_search_filter_bar_5089"

    .line 17
    invoke-static {v0}, Lcoi;->f(Ljava/lang/String;)Lcoi$a;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/search/d;->a:Lcoi$a;

    .line 16
    return-void
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 24
    invoke-static {}, Lwk;->a()Z

    move-result v0

    return v0
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/twitter/android/search/d;->a:Lcoi$a;

    invoke-virtual {v0}, Lcoi$a;->c()Z

    move-result v0

    return v0
.end method

.method public static c()Z
    .locals 1

    .prologue
    .line 36
    const-string/jumbo v0, "urt_search_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static d()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/app/common/base/BaseFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    invoke-static {}, Lcom/twitter/android/search/d;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    const-class v0, Lcom/twitter/android/search/UrtSearchResultsFragment;

    .line 44
    :goto_0
    return-object v0

    :cond_0
    const-class v0, Lcom/twitter/android/SearchResultsFragment;

    goto :goto_0
.end method

.method public static e()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 52
    invoke-static {}, Lcom/twitter/android/search/d;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/twitter/android/search/SearchActivity2;

    :goto_0
    return-object v0

    :cond_0
    const-class v0, Lcom/twitter/android/SearchActivity;

    goto :goto_0
.end method

.method public static f()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 57
    invoke-static {}, Lcom/twitter/android/search/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/twitter/android/search/SearchFieldActivity;

    :goto_0
    return-object v0

    :cond_0
    const-class v0, Lcom/twitter/android/trends/TrendsPlusActivity;

    goto :goto_0
.end method
