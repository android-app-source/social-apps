.class public Lcom/twitter/android/settings/TweetSettingsActivity;
.super Lcom/twitter/android/client/TwitterPreferenceActivity;
.source "Twttr"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/settings/TweetSettingsActivity$a;
    }
.end annotation


# instance fields
.field a:Landroid/preference/PreferenceCategory;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/twitter/library/client/Session;

.field private e:Landroid/preference/Preference;

.field private f:Landroid/preference/Preference;

.field private g:Landroid/content/Intent;

.field private h:I

.field private i:I

.field private j:Z

.field private k:Z

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/twitter/android/client/TwitterPreferenceActivity;-><init>()V

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->b:Ljava/util/Map;

    .line 56
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->h:I

    return-void
.end method

.method static a(Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;)Landroid/preference/Preference;
    .locals 1

    .prologue
    .line 315
    new-instance v0, Lcom/twitter/android/widget/UserPreference;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/UserPreference;-><init>(Landroid/content/Context;)V

    .line 316
    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/UserPreference;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 317
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/settings/TweetSettingsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->c:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/settings/TweetSettingsActivity;Z)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/twitter/android/settings/TweetSettingsActivity;->b(Z)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/settings/TweetSettingsActivity;Z)V
    .locals 0

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/twitter/android/settings/TweetSettingsActivity;->d(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 1

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->j:Z

    iput-boolean p1, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->k:Z

    .line 111
    iget-boolean v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->j:Z

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/TweetSettingsActivity;->c(Z)V

    .line 112
    return-void
.end method

.method private c()Landroid/content/Intent;
    .locals 6

    .prologue
    .line 199
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->g:Landroid/content/Intent;

    if-nez v0, :cond_0

    .line 200
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->g:Landroid/content/Intent;

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->g:Landroid/content/Intent;

    const-string/jumbo v1, "TweetSettingsActivity_enabled"

    iget-boolean v2, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->j:Z

    .line 204
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "TweetSettingsActivity_count"

    iget v2, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->i:I

    .line 205
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 214
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 215
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->l:Ljava/util/List;

    .line 229
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->g:Landroid/content/Intent;

    const-string/jumbo v2, "NotificationSettingsActivity_tweet_follow_users"

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 231
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->g:Landroid/content/Intent;

    return-object v0

    .line 219
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->l:Ljava/util/List;

    .line 220
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 221
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 222
    iget-object v3, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->b:Ljava/util/Map;

    iget-wide v4, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 225
    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_1

    .line 227
    :cond_3
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    goto :goto_0
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 321
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->e:Landroid/preference/Preference;

    if-nez v0, :cond_0

    .line 322
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 323
    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOrder(I)V

    .line 324
    const v1, 0x7f0a0862

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    .line 325
    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setShouldDisableView(Z)V

    .line 326
    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setSelectable(Z)V

    .line 327
    iput-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->e:Landroid/preference/Preference;

    .line 329
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->a:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->e:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 330
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/service/s;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 334
    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/TwitterPreferenceActivity;->a(Lcom/twitter/library/service/s;I)V

    .line 335
    if-ne p2, v2, :cond_0

    .line 336
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 337
    check-cast p1, Lbhn;

    invoke-virtual {p1}, Lbhn;->a()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->a:Landroid/preference/PreferenceCategory;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/settings/TweetSettingsActivity;->a(Ljava/util/List;Landroid/preference/PreferenceCategory;)V

    .line 344
    :cond_0
    :goto_0
    return-void

    .line 340
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->a:Landroid/preference/PreferenceCategory;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 341
    const v0, 0x7f0a09ec

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method a(Lcom/twitter/model/core/TwitterUser;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 240
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->a:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v3

    .line 241
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 242
    :goto_0
    if-ge v1, v3, :cond_2

    .line 243
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->a:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/android/widget/UserPreference;

    if-nez v0, :cond_1

    .line 242
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 247
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->a:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/UserPreference;

    .line 248
    invoke-virtual {v0}, Lcom/twitter/android/widget/UserPreference;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 249
    iget-wide v6, p1, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-wide v8, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 250
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 256
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->a:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    .line 257
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->f:Landroid/preference/Preference;

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOrder(I)V

    .line 258
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->a:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->f:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 259
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->a:Landroid/preference/PreferenceCategory;

    invoke-virtual {p0, v4, v0}, Lcom/twitter/android/settings/TweetSettingsActivity;->a(Ljava/util/List;Landroid/preference/PreferenceCategory;)V

    .line 260
    return-void
.end method

.method a(Ljava/util/List;Landroid/preference/PreferenceCategory;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;",
            "Landroid/preference/PreferenceCategory;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 270
    invoke-virtual {p0}, Lcom/twitter/android/settings/TweetSettingsActivity;->m()Z

    move-result v0

    .line 273
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 274
    if-eqz v0, :cond_3

    .line 275
    invoke-direct {p0}, Lcom/twitter/android/settings/TweetSettingsActivity;->d()V

    move v1, v2

    .line 290
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/settings/TweetSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0c0026

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    .line 291
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    .line 290
    invoke-virtual {v0, v3, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 292
    iput-object p2, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->a:Landroid/preference/PreferenceCategory;

    .line 293
    iput v1, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->i:I

    .line 295
    iput-object p1, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->l:Ljava/util/List;

    .line 298
    iget v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->h:I

    if-gez v0, :cond_0

    .line 299
    iput v1, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->h:I

    .line 302
    :cond_0
    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/twitter/android/settings/TweetSettingsActivity;->c()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/settings/TweetSettingsActivity;->setResult(ILandroid/content/Intent;)V

    .line 303
    return-void

    .line 279
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 281
    invoke-static {p0, v0}, Lcom/twitter/android/settings/TweetSettingsActivity;->a(Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;)Landroid/preference/Preference;

    move-result-object v0

    .line 282
    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 283
    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOrder(I)V

    .line 284
    invoke-virtual {p2, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 285
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 286
    goto :goto_1

    .line 287
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->f:Landroid/preference/Preference;

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setOrder(I)V

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method protected a(Z)Z
    .locals 2

    .prologue
    .line 145
    iget v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->h:I

    if-nez v0, :cond_0

    .line 146
    if-eqz p1, :cond_1

    .line 147
    invoke-direct {p0}, Lcom/twitter/android/settings/TweetSettingsActivity;->d()V

    .line 152
    :cond_0
    :goto_0
    iput-boolean p1, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->j:Z

    .line 153
    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/twitter/android/settings/TweetSettingsActivity;->c()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/settings/TweetSettingsActivity;->setResult(ILandroid/content/Intent;)V

    .line 154
    const/4 v0, 0x1

    return v0

    .line 148
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->e:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->a:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->e:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method protected h()Z
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x1

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 170
    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 171
    if-nez p3, :cond_1

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    const-string/jumbo v0, "AccountNotificationActivity_notifications_enabled"

    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 178
    if-nez v0, :cond_0

    .line 183
    const-string/jumbo v0, "AccountNotificationActivity_profile_account_user"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 185
    if-eqz v0, :cond_0

    .line 193
    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/TweetSettingsActivity;->a(Lcom/twitter/model/core/TwitterUser;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 64
    invoke-super {p0, p1}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 66
    const v0, 0x7f080023

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/TweetSettingsActivity;->addPreferencesFromResource(I)V

    .line 67
    const v0, 0x7f0a0860

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/TweetSettingsActivity;->setTitle(I)V

    .line 70
    invoke-virtual {p0, v4}, Lcom/twitter/android/settings/TweetSettingsActivity;->d(Z)V

    .line 72
    invoke-virtual {p0}, Lcom/twitter/android/settings/TweetSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 74
    new-instance v1, Landroid/preference/PreferenceCategory;

    invoke-direct {v1, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 75
    const v2, 0x7f0a04b6

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    .line 76
    invoke-virtual {p0}, Lcom/twitter/android/settings/TweetSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 77
    iput-object v1, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->a:Landroid/preference/PreferenceCategory;

    .line 79
    new-instance v2, Lcom/twitter/android/widget/MessagePreference;

    invoke-direct {v2, p0}, Lcom/twitter/android/widget/MessagePreference;-><init>(Landroid/content/Context;)V

    .line 80
    const-string/jumbo v3, "msg"

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/MessagePreference;->setKey(Ljava/lang/String;)V

    .line 81
    const v3, 0x7f0a0861

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/MessagePreference;->setSummary(I)V

    .line 82
    invoke-virtual {v2, v4}, Lcom/twitter/android/widget/MessagePreference;->setShouldDisableView(Z)V

    .line 83
    invoke-virtual {v2, v4}, Lcom/twitter/android/widget/MessagePreference;->setSelectable(Z)V

    .line 84
    invoke-virtual {v2, v4}, Lcom/twitter/android/widget/MessagePreference;->setPersistent(Z)V

    .line 85
    iput-object v2, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->f:Landroid/preference/Preference;

    .line 86
    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 88
    const-string/jumbo v1, "TweetSettingsActivity_account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 89
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/twitter/library/client/v;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->c:Lcom/twitter/library/client/Session;

    .line 91
    const-string/jumbo v1, "NotificationSettingsActivity_tweet_follow_users"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->l:Ljava/util/List;

    .line 93
    iget-object v1, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->l:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 94
    iget-object v1, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->l:Ljava/util/List;

    iget-object v2, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->a:Landroid/preference/PreferenceCategory;

    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/settings/TweetSettingsActivity;->a(Ljava/util/List;Landroid/preference/PreferenceCategory;)V

    .line 100
    :goto_0
    const-string/jumbo v1, "TweetSettingsActivity_enabled"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 101
    const-string/jumbo v1, "TweetSettingsActivity_enabled"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/twitter/android/settings/TweetSettingsActivity;->b(Z)V

    .line 102
    invoke-virtual {p0, v5}, Lcom/twitter/android/settings/TweetSettingsActivity;->d(Z)V

    .line 107
    :goto_1
    return-void

    .line 96
    :cond_0
    new-instance v1, Lbhn;

    iget-object v2, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->c:Lcom/twitter/library/client/Session;

    const/16 v3, 0x2b

    invoke-direct {v1, p0, v2, v3}, Lbhn;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;I)V

    const/16 v2, 0x190

    .line 97
    invoke-virtual {v1, v2}, Lbhn;->a(I)Lbhn;

    move-result-object v1

    .line 96
    invoke-virtual {p0, v1, v5}, Lcom/twitter/android/settings/TweetSettingsActivity;->b(Lcom/twitter/library/service/s;I)Z

    goto :goto_0

    .line 104
    :cond_1
    new-instance v0, Lcom/twitter/android/settings/TweetSettingsActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/settings/TweetSettingsActivity$a;-><init>(Lcom/twitter/android/settings/TweetSettingsActivity;Lcom/twitter/android/settings/TweetSettingsActivity$1;)V

    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/settings/TweetSettingsActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 121
    invoke-super {p0}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onPause()V

    .line 122
    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/twitter/android/settings/TweetSettingsActivity;->c()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/settings/TweetSettingsActivity;->setResult(ILandroid/content/Intent;)V

    .line 123
    return-void
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7

    .prologue
    .line 159
    check-cast p1, Lcom/twitter/android/widget/UserPreference;

    .line 160
    invoke-virtual {p1}, Lcom/twitter/android/widget/UserPreference;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v2

    .line 162
    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/settings/TweetSettingsActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    const/16 v6, 0x64

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->a(Landroid/app/Activity;Lcom/twitter/model/core/TwitterUser;Lcgi;JI)V

    .line 165
    const/4 v0, 0x1

    return v0
.end method

.method protected onStop()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 127
    invoke-super {p0}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onStop()V

    .line 130
    iget-boolean v1, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->k:Z

    iget-boolean v2, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->j:Z

    if-eq v1, v2, :cond_1

    .line 131
    invoke-virtual {p0}, Lcom/twitter/android/settings/TweetSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "TweetSettingsActivity_from_notification_landing"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 132
    iget-object v1, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->c:Lcom/twitter/library/client/Session;

    .line 133
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lcom/twitter/library/platform/notifications/PushRegistration;->b(Landroid/content/Context;J)Z

    move-result v1

    .line 135
    invoke-static {p0}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;)Lcom/twitter/android/client/l;

    move-result-object v2

    .line 136
    iget-object v3, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->c:Lcom/twitter/library/client/Session;

    .line 137
    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iget-boolean v1, p0, Lcom/twitter/android/settings/TweetSettingsActivity;->j:Z

    invoke-virtual {v2, v4, v5, v0, v1}, Lcom/twitter/android/client/l;->a(JZZ)V

    .line 140
    :cond_1
    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/twitter/android/settings/TweetSettingsActivity;->c()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/settings/TweetSettingsActivity;->setResult(ILandroid/content/Intent;)V

    .line 141
    return-void
.end method
