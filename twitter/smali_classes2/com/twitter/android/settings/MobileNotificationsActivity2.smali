.class public Lcom/twitter/android/settings/MobileNotificationsActivity2;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"


# instance fields
.field private final a:Laiv;

.field private b:Landroid/content/Context;

.field private c:Lcom/twitter/library/client/Session;

.field private d:Laiy;

.field private e:Laiw;

.field private f:Lbed;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    .line 35
    new-instance v0, Laiv;

    invoke-direct {v0}, Laiv;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->a:Laiv;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/settings/MobileNotificationsActivity2;)Laiv;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->a:Laiv;

    return-object v0
.end method

.method private a(Lcfq;)V
    .locals 4

    .prologue
    .line 166
    iget-object v0, p1, Lcfq;->f:Lcgb;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->a:Laiv;

    invoke-virtual {v0, p1}, Laiv;->a(Lcfq;)V

    .line 191
    :goto_0
    return-void

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->a:Laiv;

    .line 179
    invoke-virtual {v0}, Laiv;->a()Lcfq;

    move-result-object v0

    invoke-virtual {v0}, Lcfq;->a()Lcfq$a;

    move-result-object v0

    .line 181
    iget-object v1, p1, Lcfq;->e:Ljava/util/Map;

    if-eqz v1, :cond_1

    .line 182
    iget-object v1, p1, Lcfq;->e:Ljava/util/Map;

    invoke-virtual {v0, v1}, Lcfq$a;->b(Ljava/util/Map;)Lcfq$a;

    .line 187
    :cond_1
    iget-wide v2, p1, Lcfq;->i:D

    .line 188
    invoke-virtual {v0, v2, v3}, Lcfq$a;->a(D)Lcfq$a;

    .line 190
    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->a:Laiv;

    invoke-virtual {v0}, Lcfq$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfq;

    invoke-virtual {v1, v0}, Laiv;->a(Lcfq;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/settings/MobileNotificationsActivity2;)Laiy;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->d:Laiy;

    return-object v0
.end method

.method private b(Lcfq;)V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p1, Lcfq;->f:Lcgb;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->e:Laiw;

    invoke-interface {v0, p1}, Laiw;->a(Lcfq;)V

    .line 202
    :cond_0
    return-void
.end method

.method private i()V
    .locals 1

    .prologue
    .line 157
    new-instance v0, Lcom/twitter/android/settings/MobileNotificationsActivity2$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/settings/MobileNotificationsActivity2$1;-><init>(Lcom/twitter/android/settings/MobileNotificationsActivity2;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity2;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 163
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53
    const v0, 0x7f0401bb

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 54
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(Z)V

    .line 55
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 56
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(Z)V

    .line 57
    const/16 v0, 0x4c

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(I)V

    .line 59
    return-object p2
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 8

    .prologue
    .line 65
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V

    .line 66
    invoke-virtual {p0}, Lcom/twitter/android/settings/MobileNotificationsActivity2;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v5

    .line 67
    const v0, 0x7f130391

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity2;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 68
    const v0, 0x7f1304bd

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity2;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 69
    invoke-virtual {p0}, Lcom/twitter/android/settings/MobileNotificationsActivity2;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->c:Lcom/twitter/library/client/Session;

    .line 70
    invoke-virtual {p0}, Lcom/twitter/android/settings/MobileNotificationsActivity2;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->b:Landroid/content/Context;

    .line 71
    new-instance v0, Laix;

    iget-object v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->b:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->c:Lcom/twitter/library/client/Session;

    .line 72
    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-direct {v0, v2, v6, v7}, Laix;-><init>(Landroid/content/Context;J)V

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->e:Laiw;

    .line 73
    new-instance v0, Laiy;

    iget-object v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->c:Lcom/twitter/library/client/Session;

    .line 75
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v2

    iget-boolean v3, v2, Lcom/twitter/model/core/TwitterUser;->m:Z

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Laiy;-><init>(Landroid/widget/ListView;Landroid/content/Context;ZLandroid/widget/TextView;Lcom/twitter/internal/android/widget/ToolBar;)V

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->d:Laiy;

    .line 76
    new-instance v0, Lbed;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lbed;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->f:Lbed;

    .line 78
    const-string/jumbo v0, "Mobile notifications"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity2;->setTitle(Ljava/lang/CharSequence;)V

    .line 79
    return-void
.end method

.method public a(Lcom/twitter/library/service/s;I)V
    .locals 4

    .prologue
    .line 112
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcom/twitter/library/service/s;I)V

    .line 114
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/settings/MobileNotificationsActivity2;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/library/service/v;->c:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 154
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 119
    :cond_1
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 124
    :pswitch_1
    check-cast p1, Lbee;

    .line 126
    iget-object v0, p1, Lbee;->a:Lcfq;

    .line 129
    if-eqz v0, :cond_2

    .line 130
    invoke-direct {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity2;->a(Lcfq;)V

    .line 132
    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/settings/MobileNotificationsActivity2;->i()V

    goto :goto_0

    .line 136
    :pswitch_2
    check-cast p1, Lbeh;

    .line 138
    iget-object v0, p1, Lbeh;->a:Lcfq;

    .line 140
    if-eqz v0, :cond_0

    .line 144
    invoke-direct {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity2;->b(Lcfq;)V

    goto :goto_0

    .line 119
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public onStart()V
    .locals 5

    .prologue
    .line 83
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onStart()V

    .line 86
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->a:Laiv;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->e:Laiw;

    invoke-interface {v1}, Laiw;->a()Lcfq;

    move-result-object v1

    invoke-virtual {v0, v1}, Laiv;->a(Lcfq;)V

    .line 91
    new-instance v0, Lbee;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->c:Lcom/twitter/library/client/Session;

    iget-object v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->f:Lbed;

    iget-object v4, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->a:Laiv;

    .line 92
    invoke-virtual {v4}, Laiv;->c()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lbee;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lbed;Ljava/lang/String;)V

    const/4 v1, 0x1

    .line 91
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity2;->b(Lcom/twitter/library/service/s;I)Z

    .line 94
    return-void
.end method

.method public onStop()V
    .locals 6

    .prologue
    .line 98
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onStop()V

    .line 100
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->a:Laiv;

    invoke-virtual {v0}, Laiv;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    new-instance v0, Lbeh;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->c:Lcom/twitter/library/client/Session;

    iget-object v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->f:Lbed;

    iget-object v4, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->a:Laiv;

    .line 104
    invoke-virtual {v4}, Laiv;->c()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/settings/MobileNotificationsActivity2;->a:Laiv;

    .line 105
    invoke-virtual {v5}, Laiv;->d()Ljava/util/Map;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lbeh;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lbed;Ljava/lang/String;Ljava/util/Map;)V

    const/4 v1, 0x3

    .line 101
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity2;->b(Lcom/twitter/library/service/s;I)Z

    .line 108
    :cond_0
    return-void
.end method
