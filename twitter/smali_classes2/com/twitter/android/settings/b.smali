.class public Lcom/twitter/android/settings/b;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/library/api/t;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    .line 45
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string/jumbo v1, "moments/home_suggested_moments_injection_with_video_debug.json"

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 44
    invoke-static {v0}, Lcom/twitter/library/api/z;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    .line 46
    invoke-static {v0, p1}, Lcom/twitter/library/api/z;->b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/library/api/t;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 24
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v1

    .line 27
    :try_start_0
    invoke-static {p0, v0}, Lcom/twitter/android/settings/b;->a(Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/library/api/t;

    move-result-object v2

    .line 28
    invoke-static {v2}, Lcom/twitter/library/provider/s$a;->a(Lcom/twitter/library/api/t;)Lcom/twitter/library/provider/s$a;

    move-result-object v2

    .line 29
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/twitter/library/provider/s$a;->a(J)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    const/4 v2, 0x1

    .line 30
    invoke-virtual {v0, v2}, Lcom/twitter/library/provider/s$a;->e(Z)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    const/4 v2, 0x0

    .line 31
    invoke-virtual {v0, v2}, Lcom/twitter/library/provider/s$a;->a(I)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    const/4 v2, 0x1

    .line 32
    invoke-virtual {v0, v2}, Lcom/twitter/library/provider/s$a;->d(Z)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lcom/twitter/library/provider/s$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/s;

    .line 28
    invoke-virtual {v1, v0}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/library/provider/s;)I

    .line 34
    const-string/jumbo v0, "Injected carousel. It should appear at the top of the timeline"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 39
    :goto_0
    return-void

    .line 36
    :catch_0
    move-exception v0

    .line 37
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
