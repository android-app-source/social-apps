.class public Lcom/twitter/android/settings/DisplayAndSoundActivity;
.super Lcom/twitter/android/client/TwitterPreferenceActivity;
.source "Twttr"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/twitter/android/client/TwitterPreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 20
    invoke-super {p0, p1}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 21
    const v0, 0x7f08000c

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/DisplayAndSoundActivity;->addPreferencesFromResource(I)V

    .line 24
    invoke-static {}, Lcom/twitter/android/client/OpenUriHelper;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/twitter/library/client/k;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 25
    const-string/jumbo v0, "in_app_browser"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/DisplayAndSoundActivity;->b(Ljava/lang/String;)Z

    .line 28
    :cond_0
    const-string/jumbo v0, "pref_accessibility"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/DisplayAndSoundActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 29
    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 30
    return-void
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    .line 35
    if-nez v2, :cond_0

    .line 45
    :goto_0
    return v0

    .line 39
    :cond_0
    const/4 v1, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_1
    :goto_1
    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 41
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/settings/AccessibilityActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/DisplayAndSoundActivity;->startActivity(Landroid/content/Intent;)V

    .line 42
    const/4 v0, 0x1

    goto :goto_0

    .line 39
    :pswitch_1
    const-string/jumbo v3, "pref_accessibility"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v1, v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x7c0a3c32
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
