.class public Lcom/twitter/android/settings/PrivacyAndContentActivity;
.super Lcom/twitter/android/settings/BaseAccountSettingsActivity;
.source "Twttr"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private b:Landroid/preference/CheckBoxPreference;

.field private c:Landroid/preference/Preference;

.field private e:Landroid/preference/CheckBoxPreference;

.field private f:Landroid/preference/CheckBoxPreference;

.field private g:Landroid/preference/ListPreference;

.field private h:Landroid/preference/CheckBoxPreference;

.field private i:Landroid/preference/CheckBoxPreference;

.field private j:Landroid/preference/CheckBoxPreference;

.field private k:Landroid/preference/Preference;

.field private l:Lcom/twitter/android/widget/UrlLinkableCheckboxPreference;

.field private m:Z

.field private n:Lcom/twitter/android/periscope/q;

.field private o:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/twitter/android/settings/BaseAccountSettingsActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/settings/PrivacyAndContentActivity;)Landroid/preference/CheckBoxPreference;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->h:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/settings/PrivacyAndContentActivity;Landroid/preference/CheckBoxPreference;)Landroid/preference/CheckBoxPreference;
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->i:Landroid/preference/CheckBoxPreference;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/settings/PrivacyAndContentActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->d(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;)V
    .locals 3

    .prologue
    .line 453
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->H:Lcom/twitter/library/client/p;

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 454
    invoke-static {p0, p1, p2, v1, v2}, Lbbg;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;ZLjava/lang/String;)Lbbg;

    move-result-object v1

    .line 453
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 455
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/settings/PrivacyAndContentActivity;)Landroid/preference/CheckBoxPreference;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->i:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method private b(Z)V
    .locals 4

    .prologue
    .line 479
    if-eqz p1, :cond_0

    .line 480
    const-string/jumbo v0, "privacy_settings:muted_automated:toggle::opt_in"

    .line 485
    :goto_0
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->G:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 486
    return-void

    .line 482
    :cond_0
    const-string/jumbo v0, "privacy_settings:muted_automated:toggle::opt_out"

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/settings/PrivacyAndContentActivity;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0, p1}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 246
    invoke-virtual {p0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    .line 247
    iget-object v2, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->h:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    iget-boolean v0, v0, Lcom/twitter/model/account/UserSettings;->i:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 248
    iget-boolean v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->m:Z

    if-eqz v0, :cond_0

    .line 249
    invoke-virtual {p0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->j()Lcom/twitter/library/client/v;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/twitter/library/client/v;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 250
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v2

    .line 251
    iget-object v3, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->h:Landroid/preference/CheckBoxPreference;

    const v4, 0x7f0a082d

    invoke-virtual {p0, v4}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 252
    new-instance v3, Lbht;

    invoke-direct {v3, p0, v0, v1, v1}, Lbht;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;ZZ)V

    new-instance v0, Lcom/twitter/android/settings/PrivacyAndContentActivity$2;

    invoke-direct {v0, p0}, Lcom/twitter/android/settings/PrivacyAndContentActivity$2;-><init>(Lcom/twitter/android/settings/PrivacyAndContentActivity;)V

    invoke-virtual {v2, v3, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 264
    :cond_0
    return-void

    .line 247
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 462
    const v0, 0x7f0a0bd2

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 463
    const-string/jumbo v0, "privacy_settings:who_can_tag_me::from_anyone:select"

    .line 472
    :goto_0
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->G:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 473
    :cond_0
    return-void

    .line 464
    :cond_1
    const v0, 0x7f0a0bd3

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 465
    const-string/jumbo v0, "privacy_settings:who_can_tag_me::from_people_you_follow:select"

    goto :goto_0

    .line 466
    :cond_2
    const v0, 0x7f0a0bd4

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 467
    const-string/jumbo v0, "privacy_settings:who_can_tag_me:::deselect"

    goto :goto_0
.end method

.method private d()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 267
    iget-object v2, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->i:Landroid/preference/CheckBoxPreference;

    if-nez v2, :cond_0

    .line 321
    :goto_0
    return-void

    .line 271
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v2

    .line 272
    invoke-virtual {p0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 273
    invoke-static {v3}, Lcom/twitter/library/util/v;->a(Landroid/content/Context;)Lcom/twitter/library/util/v;

    move-result-object v4

    .line 274
    iget-boolean v5, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->m:Z

    if-eqz v5, :cond_3

    .line 275
    iget-object v5, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->i:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 276
    iget-object v5, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->i:Landroid/preference/CheckBoxPreference;

    if-eqz v2, :cond_1

    iget-boolean v2, v2, Lcom/twitter/model/account/UserSettings;->l:Z

    if-eqz v2, :cond_1

    :goto_1
    invoke-virtual {v5, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 277
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->i:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f0a0878

    invoke-virtual {p0, v1}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 278
    invoke-virtual {v4}, Lcom/twitter/library/util/v;->c()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v4}, Lcom/twitter/library/util/v;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 279
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->i:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f0a0826

    invoke-virtual {p0, v1}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 276
    goto :goto_1

    .line 281
    :cond_2
    invoke-static {v3}, Lcom/twitter/android/util/t;->a(Landroid/content/Context;)Lcom/twitter/android/util/s;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/settings/PrivacyAndContentActivity$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/settings/PrivacyAndContentActivity$3;-><init>(Lcom/twitter/android/settings/PrivacyAndContentActivity;)V

    invoke-interface {v0, v1}, Lcom/twitter/android/util/s;->a(Lcom/twitter/android/util/s$a;)V

    goto :goto_0

    .line 292
    :cond_3
    invoke-virtual {v4}, Lcom/twitter/library/util/v;->c()Z

    move-result v5

    if-nez v5, :cond_6

    .line 293
    invoke-virtual {v4}, Lcom/twitter/library/util/v;->b()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 294
    iget-object v3, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->i:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 295
    iget-object v3, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->i:Landroid/preference/CheckBoxPreference;

    if-eqz v2, :cond_4

    iget-boolean v2, v2, Lcom/twitter/model/account/UserSettings;->l:Z

    if-eqz v2, :cond_4

    :goto_2
    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2

    .line 297
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->i:Landroid/preference/CheckBoxPreference;

    .line 298
    const-string/jumbo v0, "discoverable_by_mobile_phone"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->b(Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 301
    :cond_6
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->i:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 302
    invoke-static {v3}, Lcom/twitter/android/util/t;->a(Landroid/content/Context;)Lcom/twitter/android/util/s;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/settings/PrivacyAndContentActivity$4;

    invoke-direct {v1, p0, v2}, Lcom/twitter/android/settings/PrivacyAndContentActivity$4;-><init>(Lcom/twitter/android/settings/PrivacyAndContentActivity;Lcom/twitter/model/account/UserSettings;)V

    invoke-interface {v0, v1}, Lcom/twitter/android/util/s;->a(Lcom/twitter/android/util/s$a;)V

    goto/16 :goto_0
.end method

.method private d(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 500
    invoke-virtual {p0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->getListView()Landroid/widget/ListView;

    move-result-object v2

    .line 501
    if-nez v2, :cond_1

    .line 514
    :cond_0
    :goto_0
    return-void

    .line 504
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 505
    invoke-virtual {v2}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 506
    instance-of v3, v0, Landroid/preference/Preference;

    if-eqz v3, :cond_2

    .line 507
    check-cast v0, Landroid/preference/Preference;

    invoke-virtual {v0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 508
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 509
    invoke-virtual {v2, v1}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    .line 504
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method private e()V
    .locals 2

    .prologue
    .line 324
    iget-boolean v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->j:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->j:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 326
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->j:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v1

    iget-boolean v1, v1, Lcom/twitter/model/account/UserSettings;->B:Z

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 330
    :goto_0
    return-void

    .line 328
    :cond_0
    const-string/jumbo v0, "periscope_auth"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->b(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method private e(Z)V
    .locals 5

    .prologue
    .line 491
    if-eqz p1, :cond_0

    const-string/jumbo v0, "enable"

    .line 492
    :goto_0
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->G:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "settings:privacy::read_receipts_setting"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    .line 493
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 492
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 494
    return-void

    .line 491
    :cond_0
    const-string/jumbo v0, "disable"

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 81
    invoke-super {p0, p1}, Lcom/twitter/android/settings/BaseAccountSettingsActivity;->onCreate(Landroid/os/Bundle;)V

    .line 82
    const v0, 0x7f080019

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->addPreferencesFromResource(I)V

    .line 84
    const-string/jumbo v0, "people_discoverability_settings_update_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->m:Z

    .line 87
    invoke-virtual {p0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/periscope/auth/PeriscopeAuthenticator;->b(Lcom/twitter/library/client/Session;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->o:Z

    .line 88
    new-instance v0, Lcom/twitter/android/periscope/q;

    invoke-virtual {p0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const/4 v1, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/twitter/android/periscope/q;-><init>(JLjava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->n:Lcom/twitter/android/periscope/q;

    .line 91
    const-string/jumbo v0, "allow_dms_from"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->b:Landroid/preference/CheckBoxPreference;

    .line 92
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->b:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 95
    invoke-static {}, Lcom/twitter/library/dm/d;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 96
    const-string/jumbo v0, "dm_read_receipts"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 97
    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 98
    new-instance v1, Lcom/twitter/util/a;

    iget-wide v2, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->G:J

    invoke-direct {v1, p0, v2, v3}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    const-string/jumbo v2, "dm_read_receipts"

    const/4 v3, 0x1

    .line 99
    invoke-virtual {v1, v2, v3}, Lcom/twitter/util/a;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 98
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 105
    :goto_0
    const-string/jumbo v0, "display_sensitive_media"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->e:Landroid/preference/CheckBoxPreference;

    .line 106
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->e:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 108
    const-string/jumbo v0, "contacts_sync"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 110
    const-string/jumbo v0, "app_graph_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 111
    const-string/jumbo v0, "app_graph"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->c:Landroid/preference/Preference;

    .line 112
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->c:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 118
    :goto_1
    const-string/jumbo v0, "mute_list_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 119
    const-string/jumbo v0, "mute_list"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 125
    :goto_2
    const-string/jumbo v0, "block_list_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 126
    const-string/jumbo v0, "block_list"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 132
    :goto_3
    const-string/jumbo v0, "protected"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->f:Landroid/preference/CheckBoxPreference;

    .line 133
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->f:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 136
    const-string/jumbo v0, "allow_media_tagging"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->g:Landroid/preference/ListPreference;

    .line 137
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->g:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 140
    const-string/jumbo v0, "discoverable_by_email"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->h:Landroid/preference/CheckBoxPreference;

    .line 141
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->h:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 144
    const-string/jumbo v0, "discoverable_by_mobile_phone"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->i:Landroid/preference/CheckBoxPreference;

    .line 147
    const-string/jumbo v0, "periscope_auth"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->j:Landroid/preference/CheckBoxPreference;

    .line 150
    const-string/jumbo v0, "personalized_ads"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->k:Landroid/preference/Preference;

    .line 151
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->k:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 154
    const-string/jumbo v0, "automated_mute_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 155
    const-string/jumbo v0, "smart_mute"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/UrlLinkableCheckboxPreference;

    iput-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->l:Lcom/twitter/android/widget/UrlLinkableCheckboxPreference;

    .line 156
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->l:Lcom/twitter/android/widget/UrlLinkableCheckboxPreference;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/UrlLinkableCheckboxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 161
    :goto_4
    if-nez p1, :cond_0

    .line 162
    invoke-virtual {p0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "scroll_to_row"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 163
    if-eqz v0, :cond_0

    .line 164
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    .line 165
    new-instance v2, Lcom/twitter/android/settings/PrivacyAndContentActivity$1;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity$1;-><init>(Lcom/twitter/android/settings/PrivacyAndContentActivity;Ljava/lang/String;)V

    .line 171
    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 174
    :cond_0
    return-void

    .line 101
    :cond_1
    const-string/jumbo v0, "dm_read_receipts"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->b(Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 114
    :cond_2
    const-string/jumbo v0, "app_graph"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->b(Ljava/lang/String;)Z

    goto/16 :goto_1

    .line 121
    :cond_3
    const-string/jumbo v0, "mute_list"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->b(Ljava/lang/String;)Z

    goto/16 :goto_2

    .line 128
    :cond_4
    const-string/jumbo v0, "block_list"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->b(Ljava/lang/String;)Z

    goto/16 :goto_3

    .line 158
    :cond_5
    const-string/jumbo v0, "smart_mute"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->b(Ljava/lang/String;)Z

    goto :goto_4
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 374
    invoke-virtual {p0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 375
    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v4

    .line 376
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    .line 377
    if-eqz v5, :cond_0

    if-nez v4, :cond_1

    .line 447
    :cond_0
    :goto_0
    return v0

    .line 381
    :cond_1
    const/4 v2, -0x1

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_2
    :goto_1
    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 383
    :pswitch_0
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, p2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, v4, Lcom/twitter/model/account/UserSettings;->k:Z

    .line 384
    invoke-direct {p0, v3, v4}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->a(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;)V

    move v0, v1

    .line 385
    goto :goto_0

    .line 381
    :sswitch_0
    const-string/jumbo v6, "display_sensitive_media"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v2, v0

    goto :goto_1

    :sswitch_1
    const-string/jumbo v6, "allow_dms_from"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v2, v1

    goto :goto_1

    :sswitch_2
    const-string/jumbo v6, "dm_read_receipts"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v2, 0x2

    goto :goto_1

    :sswitch_3
    const-string/jumbo v6, "protected"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v2, 0x3

    goto :goto_1

    :sswitch_4
    const-string/jumbo v6, "smart_mute"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v2, 0x4

    goto :goto_1

    :sswitch_5
    const-string/jumbo v6, "allow_media_tagging"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v2, 0x5

    goto :goto_1

    :sswitch_6
    const-string/jumbo v6, "discoverable_by_email"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v2, 0x6

    goto :goto_1

    :sswitch_7
    const-string/jumbo v6, "discoverable_by_mobile_phone"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v2, 0x7

    goto :goto_1

    :sswitch_8
    const-string/jumbo v6, "periscope_auth"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/16 v2, 0x8

    goto :goto_1

    .line 388
    :pswitch_1
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, p2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 389
    if-eqz v0, :cond_3

    .line 390
    const-string/jumbo v0, "all"

    iput-object v0, v4, Lcom/twitter/model/account/UserSettings;->s:Ljava/lang/String;

    .line 394
    :goto_2
    invoke-direct {p0, v3, v4}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->a(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;)V

    move v0, v1

    .line 395
    goto/16 :goto_0

    .line 392
    :cond_3
    const-string/jumbo v0, "following"

    iput-object v0, v4, Lcom/twitter/model/account/UserSettings;->s:Ljava/lang/String;

    goto :goto_2

    .line 398
    :pswitch_2
    invoke-static {}, Lcom/twitter/library/dm/d;->h()Z

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 399
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, p2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 400
    new-instance v0, Lcom/twitter/util/a;

    iget-wide v6, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->G:J

    invoke-direct {v0, p0, v6, v7}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 401
    invoke-virtual {v0}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v5, "dm_read_receipts"

    .line 402
    invoke-virtual {v0, v5, v2}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Z)Lcom/twitter/util/a$a;

    move-result-object v0

    .line 403
    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 404
    if-eqz v2, :cond_4

    const-string/jumbo v0, "all_enabled"

    :goto_3
    iput-object v0, v4, Lcom/twitter/model/account/UserSettings;->y:Ljava/lang/String;

    .line 406
    invoke-direct {p0, v3, v4}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->a(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;)V

    .line 407
    invoke-direct {p0, v2}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->e(Z)V

    move v0, v1

    .line 408
    goto/16 :goto_0

    .line 404
    :cond_4
    const-string/jumbo v0, "all_disabled"

    goto :goto_3

    .line 411
    :pswitch_3
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, p2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, v4, Lcom/twitter/model/account/UserSettings;->j:Z

    .line 412
    invoke-direct {p0, v3, v4}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->a(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;)V

    move v0, v1

    .line 413
    goto/16 :goto_0

    .line 416
    :pswitch_4
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, p2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, v4, Lcom/twitter/model/account/UserSettings;->t:Z

    .line 417
    invoke-direct {p0, v3, v4}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->a(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;)V

    .line 418
    iget-boolean v0, v4, Lcom/twitter/model/account/UserSettings;->t:Z

    invoke-direct {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->b(Z)V

    move v0, v1

    .line 419
    goto/16 :goto_0

    .line 422
    :pswitch_5
    check-cast p2, Ljava/lang/String;

    .line 423
    iput-object p2, v4, Lcom/twitter/model/account/UserSettings;->n:Ljava/lang/String;

    .line 424
    invoke-direct {p0, v3, v4}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->a(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;)V

    .line 425
    check-cast p1, Landroid/preference/ListPreference;

    invoke-static {p1, p2}, Lcom/twitter/library/util/af;->a(Landroid/preference/ListPreference;Ljava/lang/String;)V

    .line 426
    invoke-direct {p0, p2}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->c(Ljava/lang/String;)V

    move v0, v1

    .line 427
    goto/16 :goto_0

    .line 430
    :pswitch_6
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, p2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, v4, Lcom/twitter/model/account/UserSettings;->i:Z

    .line 431
    invoke-direct {p0, v3, v4}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->a(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;)V

    move v0, v1

    .line 432
    goto/16 :goto_0

    .line 435
    :pswitch_7
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, p2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, v4, Lcom/twitter/model/account/UserSettings;->l:Z

    .line 436
    invoke-direct {p0, v3, v4}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->a(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;)V

    move v0, v1

    .line 437
    goto/16 :goto_0

    .line 440
    :pswitch_8
    iget-boolean v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->o:Z

    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 441
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, p2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, v4, Lcom/twitter/model/account/UserSettings;->B:Z

    .line 442
    invoke-direct {p0, v3, v4}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->a(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;)V

    .line 443
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->n:Lcom/twitter/android/periscope/q;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2, p2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/periscope/q;->b(Z)V

    move v0, v1

    .line 444
    goto/16 :goto_0

    .line 381
    :sswitch_data_0
    .sparse-switch
        -0x6ba65531 -> :sswitch_4
        -0x6828bf1c -> :sswitch_5
        -0x5c71fc82 -> :sswitch_0
        -0x2e6a8d03 -> :sswitch_7
        -0x24459452 -> :sswitch_3
        0x1cacd08e -> :sswitch_2
        0x45b249b0 -> :sswitch_6
        0x5e35e89f -> :sswitch_8
        0x799911f5 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 335
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    .line 336
    if-nez v3, :cond_0

    .line 367
    :goto_0
    return v0

    .line 340
    :cond_0
    const/4 v2, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 342
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/AppGraphSettingsActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "AppGraphSettingsActivity_account_id"

    iget-wide v4, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->G:J

    .line 343
    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    .line 342
    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->startActivity(Landroid/content/Intent;)V

    move v0, v1

    .line 344
    goto :goto_0

    .line 340
    :sswitch_0
    const-string/jumbo v4, "app_graph"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v2, v0

    goto :goto_1

    :sswitch_1
    const-string/jumbo v4, "mute_list"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v2, v1

    goto :goto_1

    :sswitch_2
    const-string/jumbo v4, "block_list"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x2

    goto :goto_1

    :sswitch_3
    const-string/jumbo v4, "personalized_ads"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x3

    goto :goto_1

    :sswitch_4
    const-string/jumbo v4, "contacts_sync"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x4

    goto :goto_1

    .line 347
    :pswitch_1
    iget-wide v2, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->G:J

    invoke-static {p0, v2, v3}, Lcom/twitter/android/util/y;->b(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->startActivity(Landroid/content/Intent;)V

    .line 348
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->G:J

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v4, "settings:mute_list:::click"

    aput-object v4, v3, v0

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    move v0, v1

    .line 349
    goto :goto_0

    .line 352
    :pswitch_2
    iget-wide v2, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->G:J

    invoke-static {p0, v2, v3}, Lcom/twitter/android/util/y;->a(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->startActivity(Landroid/content/Intent;)V

    .line 353
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->G:J

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v4, "settings:block_list:::click"

    aput-object v4, v3, v0

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    move v0, v1

    .line 354
    goto/16 :goto_0

    .line 357
    :pswitch_3
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/PersonalizedAdsSettingsActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->startActivity(Landroid/content/Intent;)V

    move v0, v1

    .line 358
    goto/16 :goto_0

    .line 361
    :pswitch_4
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/settings/ContactsSyncSettingsActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "ContactsSyncSettingsActivity_account_name"

    iget-object v3, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->a:Ljava/lang/String;

    .line 362
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "ContactsSyncSettingsActivity_account_id"

    iget-wide v4, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->G:J

    .line 363
    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    .line 361
    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->startActivity(Landroid/content/Intent;)V

    move v0, v1

    .line 364
    goto/16 :goto_0

    .line 340
    :sswitch_data_0
    .sparse-switch
        -0x32e5283f -> :sswitch_3
        0x45267f04 -> :sswitch_1
        0x4cab7510 -> :sswitch_2
        0x5a41af07 -> :sswitch_4
        0x6d28c1b0 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected onResume()V
    .locals 8

    .prologue
    const v1, 0x7f0a037e

    const v2, 0x7f0a0277

    const/4 v0, 0x0

    .line 178
    invoke-super {p0}, Lcom/twitter/android/settings/BaseAccountSettingsActivity;->onResume()V

    .line 179
    invoke-virtual {p0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v4

    .line 180
    if-nez v4, :cond_1

    .line 181
    invoke-static {p0}, Lcom/twitter/android/DispatchActivity;->a(Landroid/app/Activity;)V

    .line 243
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    iget-object v3, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->b:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Lcom/twitter/model/account/UserSettings;->f()Z

    move-result v5

    invoke-virtual {v3, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 189
    iget-object v3, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->e:Landroid/preference/CheckBoxPreference;

    iget-boolean v5, v4, Lcom/twitter/model/account/UserSettings;->k:Z

    invoke-virtual {v3, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 192
    iget-object v3, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->c:Landroid/preference/Preference;

    if-eqz v3, :cond_3

    .line 193
    new-instance v3, Lcom/twitter/util/a;

    invoke-virtual {p0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iget-wide v6, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->G:J

    invoke-direct {v3, v5, v6, v7}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 195
    const-string/jumbo v5, "app_graph_status"

    const-string/jumbo v6, "undetermined"

    invoke-virtual {v3, v5, v6}, Lcom/twitter/util/a;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v3, -0x1

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_2
    :goto_1
    packed-switch v3, :pswitch_data_0

    .line 210
    :goto_2
    if-lez v0, :cond_4

    .line 211
    iget-object v3, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->c:Landroid/preference/Preference;

    invoke-virtual {v3, v0}, Landroid/preference/Preference;->setSummary(I)V

    .line 218
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->f:Landroid/preference/CheckBoxPreference;

    iget-boolean v3, v4, Lcom/twitter/model/account/UserSettings;->j:Z

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 221
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->g:Landroid/preference/ListPreference;

    iget-object v3, v4, Lcom/twitter/model/account/UserSettings;->n:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 224
    invoke-direct {p0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->c()V

    .line 227
    invoke-direct {p0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->d()V

    .line 230
    invoke-direct {p0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->e()V

    .line 233
    iget-boolean v0, v4, Lcom/twitter/model/account/UserSettings;->q:Z

    if-eqz v0, :cond_5

    .line 234
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->k:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    .line 240
    :goto_4
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->l:Lcom/twitter/android/widget/UrlLinkableCheckboxPreference;

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->l:Lcom/twitter/android/widget/UrlLinkableCheckboxPreference;

    iget-boolean v1, v4, Lcom/twitter/model/account/UserSettings;->t:Z

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/UrlLinkableCheckboxPreference;->setChecked(Z)V

    goto :goto_0

    .line 195
    :sswitch_0
    const-string/jumbo v6, "optin"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v3, v0

    goto :goto_1

    :sswitch_1
    const-string/jumbo v6, "optout"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v3, 0x1

    goto :goto_1

    :pswitch_0
    move v0, v1

    .line 199
    goto :goto_2

    :pswitch_1
    move v0, v2

    .line 203
    goto :goto_2

    .line 213
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->c:Landroid/preference/Preference;

    const-string/jumbo v3, ""

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 236
    :cond_5
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity;->k:Landroid/preference/Preference;

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_4

    .line 195
    :sswitch_data_0
    .sparse-switch
        -0x3c356045 -> :sswitch_1
        0x650db18 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
