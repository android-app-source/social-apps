.class public abstract Lcom/twitter/android/settings/developer/b;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Landroid/preference/PreferenceActivity;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    :goto_0
    return v1

    .line 29
    :cond_0
    const v0, 0x7f08000b

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->addPreferencesFromResource(I)V

    .line 31
    const-string/jumbo v0, "category_developer"

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 32
    if-eqz v0, :cond_1

    .line 33
    :goto_1
    invoke-virtual {v0}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 34
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    .line 35
    new-instance v3, Lcom/twitter/android/settings/developer/b$1;

    invoke-direct {v3, v2}, Lcom/twitter/android/settings/developer/b$1;-><init>(Landroid/preference/Preference;)V

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 33
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 45
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method
