.class final Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$b$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/c$a",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 485
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486
    iput-object p1, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$b$a;->a:Landroid/widget/EditText;

    .line 487
    return-void
.end method


# virtual methods
.method public a(Lrx/i;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/i",
            "<-",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 491
    invoke-virtual {p1}, Lrx/i;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 516
    :goto_0
    return-void

    .line 494
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$b$a;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/i;->a(Ljava/lang/Object;)V

    .line 495
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$b$a;->a:Landroid/widget/EditText;

    new-instance v1, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$b$a$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$b$a$1;-><init>(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$b$a;Lrx/i;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 510
    new-instance v0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$b$a$2;

    invoke-direct {v0, p0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$b$a$2;-><init>(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$b$a;)V

    invoke-virtual {p1, v0}, Lrx/i;->a(Lrx/j;)V

    goto :goto_0
.end method

.method public synthetic call(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 481
    check-cast p1, Lrx/i;

    invoke-virtual {p0, p1}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$b$a;->a(Lrx/i;)V

    return-void
.end method
