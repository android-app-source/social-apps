.class Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$2;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Ljava/util/List",
        "<",
        "Lccv;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$2;->a:Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 143
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$2;->a(Ljava/util/List;)V

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lccv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 146
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$2;->a:Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->b(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;)Landroid/preference/PreferenceCategory;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    .line 147
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccv;

    .line 148
    iget-object v2, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$2;->a:Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;

    iget-object v0, v0, Lccv;->b:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->a(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;Ljava/lang/String;)Landroid/preference/Preference;

    move-result-object v0

    .line 149
    if-eqz v0, :cond_0

    .line 150
    iget-object v2, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$2;->a:Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;

    invoke-static {v2}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->b(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;)Landroid/preference/PreferenceCategory;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 153
    :cond_1
    return-void
.end method
