.class Lcom/twitter/android/settings/developer/DebugSettingsActivity$4;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/settings/developer/DebugSettingsActivity;->onPreferenceClick(Landroid/preference/Preference;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/settings/developer/DebugSettingsActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/settings/developer/DebugSettingsActivity;)V
    .locals 0

    .prologue
    .line 384
    iput-object p1, p0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$4;->a:Lcom/twitter/android/settings/developer/DebugSettingsActivity;

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/File;)V
    .locals 2

    .prologue
    .line 387
    iget-object v1, p0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$4;->a:Lcom/twitter/android/settings/developer/DebugSettingsActivity;

    .line 388
    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 387
    invoke-static {v1, v0}, Lcoq;->a(Landroid/app/Activity;Ljava/io/File;)V

    .line 389
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 384
    check-cast p1, Ljava/io/File;

    invoke-virtual {p0, p1}, Lcom/twitter/android/settings/developer/DebugSettingsActivity$4;->a(Ljava/io/File;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 393
    iget-object v0, p0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$4;->a:Lcom/twitter/android/settings/developer/DebugSettingsActivity;

    const-string/jumbo v1, "Couldn\'t generate thread dump"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 394
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 395
    return-void
.end method
