.class Lcom/twitter/android/settings/developer/DebugSettingsActivity$9;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/settings/developer/DebugSettingsActivity;->onPreferenceClick(Landroid/preference/Preference;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/widget/EditText;

.field final synthetic b:Lcom/twitter/android/settings/developer/DebugSettingsActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/settings/developer/DebugSettingsActivity;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 519
    iput-object p1, p0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$9;->b:Lcom/twitter/android/settings/developer/DebugSettingsActivity;

    iput-object p2, p0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$9;->a:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 522
    iget-object v0, p0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$9;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 524
    new-instance v1, Lcom/twitter/android/v$a;

    invoke-direct {v1}, Lcom/twitter/android/v$a;-><init>()V

    .line 525
    invoke-virtual {v1, v2}, Lcom/twitter/android/v$a;->c(Z)Lcom/twitter/android/v$a;

    move-result-object v1

    .line 526
    invoke-virtual {v1, v0}, Lcom/twitter/android/v$a;->a(Ljava/lang/String;)Lcom/twitter/android/v$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$9;->b:Lcom/twitter/android/settings/developer/DebugSettingsActivity;

    .line 527
    invoke-virtual {v0, v1}, Lcom/twitter/android/v$a;->a(Landroid/content/Context;)Lcom/twitter/android/v$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$9;->b:Lcom/twitter/android/settings/developer/DebugSettingsActivity;

    .line 528
    invoke-virtual {v0, v1}, Lcom/twitter/android/v$a;->b(Landroid/content/Context;)Lcom/twitter/android/v$a;

    move-result-object v0

    .line 529
    invoke-virtual {v0, v2}, Lcom/twitter/android/v$a;->e(Z)Lcom/twitter/android/v$a;

    move-result-object v0

    .line 530
    new-instance v1, Lcom/twitter/android/FlowActivity$a;

    iget-object v2, p0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$9;->b:Lcom/twitter/android/settings/developer/DebugSettingsActivity;

    invoke-direct {v1, v2}, Lcom/twitter/android/FlowActivity$a;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0}, Lcom/twitter/android/v$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lako;

    invoke-virtual {v1, v0}, Lcom/twitter/android/FlowActivity$a;->a(Lako;)V

    .line 531
    return-void
.end method
