.class public final Lcom/twitter/android/settings/developer/DebugSettingsActivity;
.super Lcom/twitter/android/client/TwitterPreferenceActivity;
.source "Twttr"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Lcom/twitter/android/util/s$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/settings/developer/DebugSettingsActivity$CrashlyticsTestException;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/twitter/android/client/TwitterPreferenceActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 608
    const-string/jumbo v0, "debug"

    invoke-static {p0, v0}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 609
    return-void
.end method

.method private a(Landroid/preference/Preference;)V
    .locals 6

    .prologue
    .line 697
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 698
    new-instance v1, Lcom/twitter/util/a;

    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 699
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v1, v2, v4, v5}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 700
    new-instance v0, Ljava/util/Date;

    const-string/jumbo v2, "auto_clean"

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v2, v4, v5}, Lcom/twitter/util/a;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 701
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyy-MM-dd h:m:sa"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 702
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Last cleanup: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 703
    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 612
    new-instance v0, Lcom/twitter/android/smartfollow/d;

    invoke-direct {v0}, Lcom/twitter/android/smartfollow/d;-><init>()V

    const-string/jumbo v1, "debug"

    .line 613
    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/d;->a(Ljava/lang/String;)Lcom/twitter/android/smartfollow/d;

    move-result-object v0

    .line 614
    invoke-virtual {v0, v2}, Lcom/twitter/android/smartfollow/d;->a(I)Lcom/twitter/android/smartfollow/d;

    move-result-object v0

    .line 615
    invoke-virtual {v0, v2}, Lcom/twitter/android/smartfollow/d;->a(Z)Lcom/twitter/android/smartfollow/d;

    move-result-object v0

    const-string/jumbo v1, "resurrection"

    .line 616
    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/d;->b(Ljava/lang/String;)Lcom/twitter/android/smartfollow/d;

    move-result-object v0

    .line 617
    invoke-virtual {v0, p0}, Lcom/twitter/android/smartfollow/d;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 612
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 618
    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 826
    const-string/jumbo v0, "Crash Test exception"

    .line 827
    new-instance v0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$CrashlyticsTestException;

    const-string/jumbo v1, "Crash Test exception"

    invoke-direct {v0, v1}, Lcom/twitter/android/settings/developer/DebugSettingsActivity$CrashlyticsTestException;-><init>(Ljava/lang/String;)V

    .line 828
    if-eqz p1, :cond_0

    .line 829
    throw v0

    .line 831
    :cond_0
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 832
    const-string/jumbo v0, "Exception logged"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 833
    return-void
.end method

.method public static c(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 621
    new-instance v0, Lcom/twitter/android/smartfollow/d;

    invoke-direct {v0}, Lcom/twitter/android/smartfollow/d;-><init>()V

    const-string/jumbo v1, "debug"

    .line 622
    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/d;->a(Ljava/lang/String;)Lcom/twitter/android/smartfollow/d;

    move-result-object v0

    .line 623
    invoke-virtual {v0, v2}, Lcom/twitter/android/smartfollow/d;->a(I)Lcom/twitter/android/smartfollow/d;

    move-result-object v0

    .line 624
    invoke-virtual {v0, v2}, Lcom/twitter/android/smartfollow/d;->a(Z)Lcom/twitter/android/smartfollow/d;

    move-result-object v0

    .line 625
    invoke-virtual {v0, p0}, Lcom/twitter/android/smartfollow/d;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 621
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 626
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 347
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 349
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Are you sure you want to set concon bundle with ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string/jumbo v1, "Yes"

    new-instance v2, Lcom/twitter/android/settings/developer/DebugSettingsActivity$1;

    invoke-direct {v2, p0, p1}, Lcom/twitter/android/settings/developer/DebugSettingsActivity$1;-><init>(Lcom/twitter/android/settings/developer/DebugSettingsActivity;Ljava/lang/String;)V

    .line 350
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string/jumbo v1, "No"

    const/4 v2, 0x0

    .line 356
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 357
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 358
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 204
    const v0, 0x7f08000a

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->addPreferencesFromResource(I)V

    .line 207
    const-string/jumbo v0, "check_phone"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 209
    const-string/jumbo v0, "logcat_viewer"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 212
    const-string/jumbo v0, "thread_dump"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 215
    const-string/jumbo v0, "feature_switches_export"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 218
    const-string/jumbo v0, "logged_in_mt"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 221
    const-string/jumbo v0, "phone_ownership"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 224
    const-string/jumbo v0, "bouncer"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 227
    const-string/jumbo v0, "start_edit_profile_nux"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 230
    const-string/jumbo v0, "start_smart_follow_nux"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 233
    const-string/jumbo v0, "start_smart_rux"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 236
    const-string/jumbo v0, "start_ocf_component"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 239
    const-string/jumbo v0, "start_highlights"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 242
    const-string/jumbo v0, "start_highlights_nux"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 245
    const-string/jumbo v0, "start_highlights_empty"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 248
    const-string/jumbo v0, "start_highlights_sample"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 251
    const-string/jumbo v0, "pref_signup_phone100"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 254
    const-string/jumbo v0, "start_live_video_landing_screen"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 256
    const-string/jumbo v0, "pref_debug_push"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 257
    const-string/jumbo v1, "simulate_logged_in_push"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 260
    invoke-static {p0}, Lcom/twitter/library/platform/b;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    const-string/jumbo v0, "geo_debug"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 266
    :goto_0
    const-string/jumbo v0, "launch_moments"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 267
    const-string/jumbo v0, "inject_suggested_moments_carousel"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 268
    const-string/jumbo v0, "reset_moments_tutorial"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 270
    const-string/jumbo v0, "reset_sticker_tooltips"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 272
    const-string/jumbo v0, "debug_delete_cached_tweets"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 273
    const-string/jumbo v0, "debug_start_session_tracking"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 274
    const-string/jumbo v0, "inject_live_header"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 276
    const-string/jumbo v0, "extra_dtab"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 277
    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 279
    const-string/jumbo v0, "concon_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 280
    const-string/jumbo v0, "extra_concon_id"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 285
    :goto_1
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 287
    const-string/jumbo v1, "debug_card_commerce_host_v2"

    invoke-virtual {p0, v1}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 288
    const-string/jumbo v2, "debug_card_commerce_host_v2"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 289
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 291
    const-string/jumbo v1, "debug_card_tpay_host_v2"

    invoke-virtual {p0, v1}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 292
    const-string/jumbo v2, "debug_card_tpay_host_v2"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 293
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 295
    const-string/jumbo v0, "debug_show_hashflags"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 297
    const-string/jumbo v0, "debug_crash_fatal"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 298
    const-string/jumbo v0, "debug_crash_nonfatal"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 299
    const-string/jumbo v0, "data_usage_meter"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 301
    const-string/jumbo v0, "debug_cleanup"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 302
    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 303
    invoke-direct {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->a(Landroid/preference/Preference;)V

    .line 305
    const-string/jumbo v0, "force_data_sync"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 306
    const-string/jumbo v0, "show_dock"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 308
    const-string/jumbo v0, "set_test_data_to_latest_news_ids"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 310
    const-string/jumbo v0, "pref_signup_phone100_password"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 311
    const-string/jumbo v0, "pref_signup_phone100_SSPC"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 312
    const-string/jumbo v0, "pref_signup_phone100_add_phone"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 314
    const-string/jumbo v0, "pref_dev_key_hash"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 316
    const-string/jumbo v0, "ptr_debug"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 318
    const-string/jumbo v0, "add_email"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 319
    const-string/jumbo v0, "update_email"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 321
    const-string/jumbo v0, "debug_muted_keywords_edu"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 322
    const-string/jumbo v0, "debug_mute_conversation_edu"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 323
    const-string/jumbo v0, "debug_muted_keywords_edu_tooltip"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 324
    const-string/jumbo v0, "debug_muted_keywords_unmute_prompt"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 325
    return-void

    .line 264
    :cond_0
    const-string/jumbo v0, "pref_debug_main"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    const-string/jumbo v1, "geo_debug"

    invoke-virtual {p0, v1}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    .line 282
    :cond_1
    const-string/jumbo v0, "extra_concon_id"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->b(Ljava/lang/String;)Z

    goto/16 :goto_1
.end method

.method private e()V
    .locals 3

    .prologue
    .line 591
    const/4 v0, 0x0

    .line 592
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_0

    .line 594
    invoke-static {p0}, Landroid/provider/Settings;->canDrawOverlays(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 595
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.settings.action.MANAGE_OVERLAY_PERMISSION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 596
    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 597
    const/4 v0, 0x1

    .line 600
    :cond_0
    if-nez v0, :cond_1

    .line 601
    invoke-static {}, Lalg;->ag()Lalg;

    move-result-object v0

    .line 602
    invoke-virtual {v0}, Lalg;->V()Lckq;

    move-result-object v0

    new-instance v1, Lckz;

    const-string/jumbo v2, "TEST"

    invoke-direct {v1, p0, v2}, Lckz;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 603
    invoke-interface {v0, v1}, Lckq;->a(Lcki;)Lcki;

    .line 605
    :cond_1
    return-void
.end method

.method private f()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 631
    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string/jumbo v1, "com.twitter.android"

    const/16 v2, 0x40

    .line 632
    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 633
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v1, v1

    if-nez v1, :cond_0

    .line 634
    const-string/jumbo v0, "No package info for com.twitter.android found."

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 665
    :goto_0
    return-void

    .line 637
    :cond_0
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 638
    const-string/jumbo v1, "SHA"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 639
    invoke-virtual {v0}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/security/MessageDigest;->update([B)V

    .line 640
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 641
    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 642
    const-string/jumbo v0, "Could not generate KeyHash."

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 645
    :catch_0
    move-exception v0

    .line 646
    :goto_1
    const-string/jumbo v0, "KeyHash generation caused exception."

    invoke-static {p0, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 650
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 651
    const-string/jumbo v2, "android.intent.action.SEND"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 652
    const-string/jumbo v2, "message/rfc822"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 653
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Android Developer Hash: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 654
    invoke-static {}, Ljava/text/DateFormat;->getInstance()Ljava/text/DateFormat;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 655
    const-string/jumbo v3, "android.intent.extra.EMAIL"

    new-array v4, v7, [Ljava/lang/String;

    const-string/jumbo v5, ""

    aput-object v5, v4, v6

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 656
    const-string/jumbo v3, "android.intent.extra.SUBJECT"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 657
    const-string/jumbo v2, "android.intent.extra.TEXT"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Key Hash = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 658
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 660
    :try_start_1
    invoke-virtual {p0, v1}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 661
    :catch_1
    move-exception v1

    .line 662
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No email client found, please install one. KeyHash = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 663
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 645
    :catch_2
    move-exception v0

    goto/16 :goto_1
.end method

.method private g()V
    .locals 1

    .prologue
    .line 706
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/client/l;->a(Landroid/content/Context;)Lcom/twitter/library/client/l;

    move-result-object v0

    .line 707
    invoke-virtual {v0}, Lcom/twitter/library/client/l;->a()V

    .line 708
    return-void
.end method

.method private i()V
    .locals 4

    .prologue
    .line 711
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->j()Lcom/twitter/library/client/v;

    move-result-object v0

    .line 712
    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 714
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 715
    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v2

    .line 716
    invoke-virtual {v2}, Lcom/twitter/library/provider/t;->e()V

    .line 717
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    .line 718
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->d(Ljava/lang/String;)V

    .line 719
    const-string/jumbo v0, "Cached tweets deleted"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 720
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 721
    return-void
.end method

.method private p()V
    .locals 12

    .prologue
    const-wide v10, 0x7fffffffffffffffL

    const/4 v7, 0x0

    .line 725
    invoke-static {}, Lbpw;->a()Lbpw;

    move-result-object v0

    iget v3, v0, Lbpw;->b:I

    .line 726
    if-eqz v3, :cond_0

    .line 727
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Live Header debug injection not supported for timeline type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 728
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 793
    :goto_0
    return-void

    .line 732
    :cond_0
    const-string/jumbo v0, "timeline_live_banner_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 733
    const-string/jumbo v0, "Please enable timeline_live_banner_enabled in Feature Switches"

    invoke-static {p0, v0, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 735
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 739
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->j()Lcom/twitter/library/client/v;

    move-result-object v0

    .line 740
    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 742
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 743
    invoke-static {v4, v5}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v2

    .line 745
    const-string/jumbo v0, "SeeFewer"

    const-string/jumbo v1, "See less often"

    const/4 v6, 0x0

    .line 746
    invoke-static {v0, v1, v6, v7}, Lcom/twitter/model/timeline/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/twitter/model/timeline/g;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 745
    invoke-static {v0}, Lcom/twitter/model/timeline/h;->a(Ljava/util/List;)Lcom/twitter/model/timeline/h;

    move-result-object v6

    .line 749
    const-string/jumbo v0, "Watch the NFL in HD"

    const-string/jumbo v1, "49ers vs. Cardinals is live now"

    const-string/jumbo v7, "http://twitter.com/i/live/770331194252636160"

    invoke-static {v0, v1, v7}, Lcom/twitter/model/timeline/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/model/timeline/b;

    move-result-object v7

    .line 755
    new-instance v0, Lcom/twitter/model/timeline/r$a;

    invoke-direct {v0}, Lcom/twitter/model/timeline/r$a;-><init>()V

    const-string/jumbo v1, "LiveEvent"

    .line 756
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/r$a;->a(Ljava/lang/String;)Lcom/twitter/model/timeline/r$a;

    move-result-object v0

    const-string/jumbo v1, "751361910818299904"

    .line 757
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/r$a;->e(Ljava/lang/String;)Lcom/twitter/model/timeline/r$a;

    move-result-object v0

    const-string/jumbo v1, "suggest_live_event"

    .line 758
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/r$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/r$a;

    move-result-object v0

    .line 759
    invoke-virtual {v0}, Lcom/twitter/model/timeline/r$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/r;

    .line 761
    new-instance v1, Lcom/twitter/model/timeline/v$a;

    invoke-direct {v1}, Lcom/twitter/model/timeline/v$a;-><init>()V

    const-string/jumbo v8, "fake-live-header-entity-group-id"

    .line 762
    invoke-virtual {v1, v8}, Lcom/twitter/model/timeline/v$a;->d(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/timeline/v$a;

    const-string/jumbo v8, "fake-live-header-entity-id"

    .line 763
    invoke-virtual {v1, v8}, Lcom/twitter/model/timeline/v$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/timeline/v$a;

    .line 764
    invoke-virtual {v1, v10, v11}, Lcom/twitter/model/timeline/v$a;->a(J)Lcom/twitter/model/timeline/y$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/timeline/v$a;

    .line 765
    invoke-virtual {v1, v0}, Lcom/twitter/model/timeline/v$a;->a(Lcom/twitter/model/timeline/r;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/v$a;

    .line 766
    invoke-virtual {v0, v10, v11}, Lcom/twitter/model/timeline/v$a;->b(J)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/v$a;

    .line 767
    invoke-virtual {v0, v6}, Lcom/twitter/model/timeline/v$a;->a(Lcom/twitter/model/timeline/h;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/v$a;

    .line 768
    invoke-virtual {v0, v7}, Lcom/twitter/model/timeline/v$a;->a(Lcom/twitter/model/timeline/b;)Lcom/twitter/model/timeline/v$a;

    move-result-object v0

    .line 769
    invoke-virtual {v0}, Lcom/twitter/model/timeline/v$a;->q()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/model/timeline/v;

    .line 771
    new-instance v0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$3;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/settings/developer/DebugSettingsActivity$3;-><init>(Lcom/twitter/android/settings/developer/DebugSettingsActivity;Lcom/twitter/library/provider/t;IJLcom/twitter/model/timeline/v;)V

    invoke-static {v0}, Lcre;->a(Lrx/functions/a;)Lrx/a;

    move-result-object v0

    .line 786
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/a;->a(Lrx/f;)Lrx/a;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/settings/developer/DebugSettingsActivity$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity$2;-><init>(Lcom/twitter/android/settings/developer/DebugSettingsActivity;)V

    .line 787
    invoke-virtual {v0, v1}, Lrx/a;->b(Lrx/functions/a;)Lrx/j;

    goto/16 :goto_0
.end method

.method private q()V
    .locals 6

    .prologue
    .line 796
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->j()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 797
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 798
    const-string/jumbo v2, "guide_post_follow_fatigue"

    invoke-static {p0, v2, v0, v1}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v2

    .line 800
    const-string/jumbo v3, "guide_follow_button_fatigue"

    invoke-static {p0, v3, v0, v1}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v3

    .line 802
    const-string/jumbo v4, "guide_thumbnail_fatigue"

    invoke-static {p0, v4, v0, v1}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v4

    .line 804
    const-string/jumbo v5, "tap_to_fit_tutorial_fatigue"

    invoke-static {p0, v5, v0, v1}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v0

    .line 807
    invoke-virtual {v2}, Lcom/twitter/android/util/h;->c()V

    .line 808
    invoke-virtual {v3}, Lcom/twitter/android/util/h;->c()V

    .line 809
    invoke-virtual {v4}, Lcom/twitter/android/util/h;->c()V

    .line 810
    invoke-virtual {v0}, Lcom/twitter/android/util/h;->c()V

    .line 811
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0be3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 812
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 813
    return-void
.end method

.method private r()V
    .locals 3

    .prologue
    .line 836
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->j()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 837
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 838
    const-string/jumbo v2, "stickers_composer_tooltip"

    invoke-static {p0, v2, v0, v1}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v2

    .line 840
    invoke-virtual {v2}, Lcom/twitter/android/util/h;->c()V

    .line 841
    const-string/jumbo v2, "sticker_selector_tooltip"

    invoke-static {p0, v2, v0, v1}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v2

    .line 843
    invoke-virtual {v2}, Lcom/twitter/android/util/h;->c()V

    .line 844
    const-string/jumbo v2, "sticker_edit_tooltip"

    invoke-static {p0, v2, v0, v1}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v0

    .line 846
    invoke-virtual {v0}, Lcom/twitter/android/util/h;->c()V

    .line 847
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 848
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "sticker_media_viewer_tooltip"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 849
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0c34

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 850
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 851
    return-void
.end method

.method private s()V
    .locals 4

    .prologue
    .line 854
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 855
    const-string/jumbo v1, "mute_conversation_prompt"

    .line 856
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 855
    invoke-static {p0, v1, v2, v3}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v0

    .line 857
    invoke-virtual {v0}, Lcom/twitter/android/util/h;->c()V

    .line 858
    return-void
.end method

.method private t()V
    .locals 4

    .prologue
    .line 861
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 862
    const-string/jumbo v1, "muted_keywords_education_tooltip_fatigue"

    .line 864
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 863
    invoke-static {p0, v1, v2, v3}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v0

    .line 865
    invoke-virtual {v0}, Lcom/twitter/android/util/h;->c()V

    .line 866
    return-void
.end method

.method private u()V
    .locals 4

    .prologue
    .line 869
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 870
    const-string/jumbo v1, "muted_keywords_prompt"

    .line 871
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 870
    invoke-static {p0, v1, v2, v3}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v0

    .line 872
    invoke-virtual {v0}, Lcom/twitter/android/util/h;->c()V

    .line 873
    return-void
.end method

.method private v()V
    .locals 4

    .prologue
    .line 876
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 877
    const-string/jumbo v1, "muted_keyword_delete_confirmation"

    .line 878
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 877
    invoke-static {p0, v1, v2, v3}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v0

    .line 879
    invoke-virtual {v0}, Lcom/twitter/android/util/h;->c()V

    .line 880
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 361
    new-instance v0, Lcom/twitter/android/settings/developer/a;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/settings/developer/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 362
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/settings/developer/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 363
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 339
    if-eqz p2, :cond_0

    .line 340
    invoke-direct {p0, p1}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->c(Ljava/lang/String;)V

    .line 344
    :goto_0
    return-void

    .line 342
    :cond_0
    invoke-virtual {p0, p1}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 668
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/dialog/PhoneVerificationDialogFragmentActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 670
    const-string/jumbo v1, "extra_is_blocking"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 671
    const-string/jumbo v1, "extra_forward_result"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 673
    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 674
    return-void
.end method

.method public c_(Z)V
    .locals 2

    .prologue
    .line 679
    if-eqz p1, :cond_0

    const-string/jumbo v0, "current user has a verified phone"

    .line 681
    :goto_0
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 682
    return-void

    .line 679
    :cond_0
    const-string/jumbo v0, "current user does not have a phone"

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 686
    const/16 v0, 0x1388

    if-ne p1, v0, :cond_0

    .line 687
    new-instance v0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$11;

    invoke-direct {v0, p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity$11;-><init>(Lcom/twitter/android/settings/developer/DebugSettingsActivity;)V

    invoke-static {v0}, Lcre;->a(Lrx/functions/a;)Lrx/a;

    .line 694
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 189
    invoke-super {p0, p1}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 192
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 193
    invoke-direct {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->d()V

    .line 194
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "is_from_debug_deeplink"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "is_from_debug_deeplink"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "concon_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->a(Ljava/lang/String;Z)V

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 199
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->finish()V

    goto :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 329
    instance-of v0, p1, Landroid/preference/ListPreference;

    if-eqz v0, :cond_1

    .line 330
    check-cast p1, Landroid/preference/ListPreference;

    check-cast p2, Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/twitter/library/util/af;->a(Landroid/preference/ListPreference;Ljava/lang/String;)V

    .line 334
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 331
    :cond_1
    const-string/jumbo v0, "extra_concon_id"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    check-cast p2, Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->a(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7

    .prologue
    const v6, 0x7f0a0616

    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 367
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    .line 368
    const-string/jumbo v3, "check_phone"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 369
    invoke-static {p0}, Lcom/twitter/android/util/t;->a(Landroid/content/Context;)Lcom/twitter/android/util/s;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/twitter/android/util/s;->a(Lcom/twitter/android/util/s$a;)V

    :cond_0
    :goto_0
    move v0, v1

    .line 587
    :cond_1
    :goto_1
    return v0

    .line 370
    :cond_2
    const-string/jumbo v3, "logcat_viewer"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 371
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/LogViewerActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 372
    :cond_3
    const-string/jumbo v3, "thread_dump"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 373
    new-instance v0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$5;

    invoke-direct {v0, p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity$5;-><init>(Lcom/twitter/android/settings/developer/DebugSettingsActivity;)V

    invoke-static {v0}, Lcre;->a(Ljava/util/concurrent/Callable;)Lrx/g;

    move-result-object v0

    .line 383
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v2

    invoke-virtual {v0, v2}, Lrx/g;->a(Lrx/f;)Lrx/g;

    move-result-object v0

    new-instance v2, Lcom/twitter/android/settings/developer/DebugSettingsActivity$4;

    invoke-direct {v2, p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity$4;-><init>(Lcom/twitter/android/settings/developer/DebugSettingsActivity;)V

    .line 384
    invoke-virtual {v0, v2}, Lrx/g;->a(Lrx/i;)Lrx/j;

    goto :goto_0

    .line 397
    :cond_4
    const-string/jumbo v3, "feature_switches_export"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 398
    new-instance v0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$7;

    invoke-direct {v0, p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity$7;-><init>(Lcom/twitter/android/settings/developer/DebugSettingsActivity;)V

    invoke-static {v0}, Lcre;->a(Lrx/functions/a;)Lrx/a;

    move-result-object v0

    .line 405
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v2

    invoke-virtual {v0, v2}, Lrx/a;->a(Lrx/f;)Lrx/a;

    move-result-object v0

    new-instance v2, Lcom/twitter/android/settings/developer/DebugSettingsActivity$6;

    invoke-direct {v2, p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity$6;-><init>(Lcom/twitter/android/settings/developer/DebugSettingsActivity;)V

    .line 406
    invoke-virtual {v0, v2}, Lrx/a;->b(Lrx/b;)V

    goto :goto_0

    .line 412
    :cond_5
    const-string/jumbo v3, "logged_in_mt"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 413
    invoke-static {p0, v1}, Lcom/twitter/android/client/z;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 414
    :cond_6
    const-string/jumbo v3, "phone_ownership"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 415
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/PhoneOwnershipActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 416
    :cond_7
    const-string/jumbo v3, "bouncer"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 417
    invoke-static {p0, v5, v0}, Lcom/twitter/android/BouncerWebViewActivity;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 418
    :cond_8
    const-string/jumbo v3, "start_edit_profile_nux"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 419
    invoke-static {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->a(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 421
    :cond_9
    const-string/jumbo v3, "start_smart_follow_nux"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 422
    invoke-static {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->c(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 424
    :cond_a
    const-string/jumbo v3, "start_smart_rux"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 425
    invoke-static {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->b(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 426
    :cond_b
    const-string/jumbo v3, "start_ocf_component"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 427
    invoke-static {p0}, Laqz;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 428
    :cond_c
    const-string/jumbo v3, "start_highlights"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 429
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/highlights/HighlightsStoriesActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 431
    :cond_d
    const-string/jumbo v3, "start_highlights_nux"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 432
    const/4 v1, 0x2

    invoke-static {p0, v1}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a(Landroid/app/Activity;I)V

    goto/16 :goto_1

    .line 434
    :cond_e
    const-string/jumbo v3, "start_highlights_empty"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 435
    const/4 v1, 0x3

    invoke-static {p0, v1}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a(Landroid/app/Activity;I)V

    goto/16 :goto_1

    .line 437
    :cond_f
    const-string/jumbo v3, "start_highlights_sample"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 438
    invoke-static {p0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity;->a(Landroid/app/Activity;)V

    goto/16 :goto_1

    .line 440
    :cond_10
    const-string/jumbo v3, "start_live_video_landing_screen"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 441
    new-instance v1, Lcom/twitter/android/livevideo/landing/b;

    const-wide/16 v2, 0x1

    invoke-direct {v1, v2, v3}, Lcom/twitter/android/livevideo/landing/b;-><init>(J)V

    .line 442
    invoke-static {p0, v1}, Lcom/twitter/android/livevideo/landing/LiveVideoLandingActivity;->a(Landroid/content/Context;Lcom/twitter/android/livevideo/landing/b;)Landroid/content/Intent;

    move-result-object v1

    .line 443
    invoke-virtual {p0, v1}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 445
    :cond_11
    const-string/jumbo v3, "geo_debug"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 446
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/GeoDebugActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 448
    :cond_12
    const-string/jumbo v3, "launch_moments"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 449
    invoke-static {p0}, Lcom/twitter/android/moments/ui/guide/ac;->a(Landroid/app/Activity;)V

    goto/16 :goto_1

    .line 451
    :cond_13
    const-string/jumbo v3, "inject_suggested_moments_carousel"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 452
    invoke-static {p0}, Lcom/twitter/android/settings/b;->a(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 453
    :cond_14
    const-string/jumbo v3, "reset_moments_tutorial"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 454
    invoke-direct {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->q()V

    goto/16 :goto_1

    .line 456
    :cond_15
    const-string/jumbo v3, "debug_delete_cached_tweets"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_16

    .line 457
    invoke-direct {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->i()V

    goto/16 :goto_1

    .line 459
    :cond_16
    const-string/jumbo v3, "debug_start_session_tracking"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 460
    invoke-direct {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->g()V

    goto/16 :goto_1

    .line 462
    :cond_17
    const-string/jumbo v3, "inject_live_header"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 463
    invoke-direct {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->p()V

    goto/16 :goto_0

    .line 464
    :cond_18
    const-string/jumbo v3, "debug_show_hashflags"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_19

    .line 465
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/HashflagsViewerActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 466
    :cond_19
    const-string/jumbo v3, "debug_crash_fatal"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 467
    invoke-direct {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->b(Z)V

    goto/16 :goto_1

    .line 469
    :cond_1a
    const-string/jumbo v3, "debug_crash_nonfatal"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 470
    invoke-direct {p0, v1}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->b(Z)V

    goto/16 :goto_1

    .line 472
    :cond_1b
    const-string/jumbo v3, "simulate_logged_in_push"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1c

    .line 473
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/twitter/android/PushDebugSettingsActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 476
    :cond_1c
    const-string/jumbo v3, "data_usage_meter"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 477
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_1

    invoke-static {p0}, Landroid/provider/Settings;->canDrawOverlays(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 478
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string/jumbo v2, "Need Additional Permission"

    .line 479
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string/jumbo v2, "Please grant the \'Draw over other apps\' permission to the app so that the Data Usage Meter can persist"

    .line 480
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 482
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a00f6

    .line 483
    invoke-virtual {v1, v2, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/settings/developer/DebugSettingsActivity$8;

    invoke-direct {v2, p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity$8;-><init>(Lcom/twitter/android/settings/developer/DebugSettingsActivity;)V

    .line 484
    invoke-virtual {v1, v6, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 492
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_1

    .line 496
    :cond_1d
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 497
    const-string/jumbo v4, "set_test_data_to_latest_news_ids"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1f

    .line 498
    new-instance v4, Lcom/twitter/util/a;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const-string/jumbo v0, "news"

    invoke-direct {v4, p0, v2, v3, v0}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    .line 500
    const-string/jumbo v0, "latestTopNewsIds"

    sget-object v2, Lcom/twitter/util/serialization/f;->j:Lcom/twitter/util/serialization/l;

    .line 501
    invoke-static {v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 500
    invoke-virtual {v4, v0, v2}, Lcom/twitter/util/a;->a(Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    move v2, v1

    .line 503
    :goto_2
    const/4 v3, 0x5

    if-ge v2, v3, :cond_1e

    .line 504
    const-string/jumbo v3, "Old-News-XXX"

    invoke-interface {v0, v2, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 503
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 506
    :cond_1e
    invoke-virtual {v4}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v2

    const-string/jumbo v3, "latestTopNewsIds"

    sget-object v4, Lcom/twitter/util/serialization/f;->j:Lcom/twitter/util/serialization/l;

    .line 507
    invoke-static {v4}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v4

    .line 506
    invoke-virtual {v2, v3, v0, v4}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/a$a;

    move-result-object v0

    .line 507
    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    goto/16 :goto_0

    .line 508
    :cond_1f
    const-string/jumbo v4, "pref_signup_phone100"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_20

    .line 509
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/FlowActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 510
    :cond_20
    const-string/jumbo v4, "pref_signup_phone100_password"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_21

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->d()Z

    move-result v4

    if-eqz v4, :cond_21

    .line 511
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 512
    const-string/jumbo v2, "Enter your password"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 514
    new-instance v2, Landroid/widget/EditText;

    invoke-direct {v2, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 515
    const/16 v3, 0x81

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 517
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 519
    new-instance v3, Lcom/twitter/android/settings/developer/DebugSettingsActivity$9;

    invoke-direct {v3, p0, v2}, Lcom/twitter/android/settings/developer/DebugSettingsActivity$9;-><init>(Lcom/twitter/android/settings/developer/DebugSettingsActivity;Landroid/widget/EditText;)V

    invoke-virtual {v1, v6, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 533
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_1

    .line 536
    :cond_21
    const-string/jumbo v4, "pref_signup_phone100_SSPC"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_22

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->d()Z

    move-result v4

    if-eqz v4, :cond_22

    .line 537
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->c()V

    goto/16 :goto_0

    .line 538
    :cond_22
    const-string/jumbo v4, "pref_signup_phone100_add_phone"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_23

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->d()Z

    move-result v3

    if-eqz v3, :cond_23

    .line 539
    new-instance v2, Lcom/twitter/android/v$a;

    invoke-direct {v2}, Lcom/twitter/android/v$a;-><init>()V

    .line 540
    invoke-virtual {v2, v0}, Lcom/twitter/android/v$a;->f(Z)Lcom/twitter/android/v$a;

    move-result-object v0

    .line 541
    invoke-virtual {v0}, Lcom/twitter/android/v$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/v;

    .line 542
    new-instance v2, Lcom/twitter/android/FlowActivity$a;

    invoke-direct {v2, p0}, Lcom/twitter/android/FlowActivity$a;-><init>(Landroid/app/Activity;)V

    .line 543
    invoke-virtual {v2, v0}, Lcom/twitter/android/FlowActivity$a;->a(Lako;)V

    goto/16 :goto_0

    .line 544
    :cond_23
    const-string/jumbo v3, "pref_dev_key_hash"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_24

    .line 545
    invoke-direct {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->f()V

    goto/16 :goto_0

    .line 546
    :cond_24
    const-string/jumbo v3, "ptr_debug"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_25

    .line 547
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/twitter/android/PtrDebugSettingsActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 549
    :cond_25
    const-string/jumbo v3, "debug_cleanup"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_26

    .line 552
    new-instance v0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$10;

    invoke-direct {v0, p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity$10;-><init>(Lcom/twitter/android/settings/developer/DebugSettingsActivity;)V

    invoke-static {v0}, Lcom/twitter/util/f;->a(Lcom/twitter/util/concurrent/i;)Ljava/lang/Object;

    .line 560
    invoke-direct {p0, p1}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->a(Landroid/preference/Preference;)V

    goto/16 :goto_0

    .line 561
    :cond_26
    const-string/jumbo v3, "force_data_sync"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_27

    .line 562
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 563
    const-string/jumbo v3, "expedited"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 564
    const-string/jumbo v3, "force"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 565
    const-string/jumbo v3, "show_notif"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 566
    sget-object v0, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    invoke-static {v5, v0, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 567
    :cond_27
    const-string/jumbo v3, "add_email"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_28

    .line 568
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/AddUpdateEmailActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "umf_update_email"

    .line 569
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 568
    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 570
    :cond_28
    const-string/jumbo v3, "update_email"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_29

    .line 571
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/AddUpdateEmailActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "umf_update_email"

    .line 572
    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 571
    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 573
    :cond_29
    const-string/jumbo v0, "reset_sticker_tooltips"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 574
    invoke-direct {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->r()V

    goto/16 :goto_0

    .line 575
    :cond_2a
    const-string/jumbo v0, "debug_mute_conversation_edu"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 576
    invoke-direct {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->s()V

    goto/16 :goto_0

    .line 577
    :cond_2b
    const-string/jumbo v0, "debug_muted_keywords_edu"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 578
    invoke-direct {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->u()V

    goto/16 :goto_0

    .line 579
    :cond_2c
    const-string/jumbo v0, "debug_muted_keywords_edu_tooltip"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 580
    invoke-direct {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->t()V

    goto/16 :goto_0

    .line 581
    :cond_2d
    const-string/jumbo v0, "debug_muted_keywords_unmute_prompt"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 582
    invoke-direct {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->v()V

    goto/16 :goto_0

    .line 583
    :cond_2e
    const-string/jumbo v0, "show_dock"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 584
    invoke-direct {p0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->e()V

    goto/16 :goto_0
.end method
