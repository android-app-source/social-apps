.class Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$5;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->onPreferenceClick(Landroid/preference/Preference;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;)V
    .locals 0

    .prologue
    .line 317
    iput-object p1, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$5;->a:Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 320
    const/4 v0, -0x1

    if-ne v0, p2, :cond_0

    .line 322
    invoke-static {}, Lbqa;->c()Laua;

    move-result-object v0

    invoke-virtual {v0}, Laua;->a()V

    .line 325
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$5;->a:Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->c(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 326
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$5;->a:Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;

    new-instance v1, Lcom/twitter/library/api/h;

    iget-object v2, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$5;->a:Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;

    iget-object v3, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$5;->a:Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;

    invoke-static {v3}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->d(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;)Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 327
    invoke-static {}, Lbqa;->d()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/twitter/library/api/h;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    const-string/jumbo v2, "FS fetches are always polling."

    .line 328
    invoke-virtual {v1, v2}, Lcom/twitter/library/api/h;->l(Ljava/lang/String;)Lcom/twitter/library/service/s;

    move-result-object v1

    const/4 v2, 0x0

    .line 326
    invoke-static {v0, v1, v2}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->a(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;Lcom/twitter/library/service/s;I)Z

    .line 330
    :cond_0
    return-void
.end method
