.class Lcom/twitter/android/settings/developer/e$a;
.super Landroid/widget/ArrayAdapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/settings/developer/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/twitter/analytics/model/ScribeLog;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/model/ScribeLog;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/text/SimpleDateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 190
    const/16 v0, 0x12c

    invoke-static {v0}, Lcom/twitter/util/collection/MutableList;->a(I)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/settings/developer/e$a;->a:Ljava/util/List;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 196
    const/4 v0, 0x0

    sget-object v1, Lcom/twitter/android/settings/developer/e$a;->a:Ljava/util/List;

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 197
    iput-object p1, p0, Lcom/twitter/android/settings/developer/e$a;->b:Landroid/content/Context;

    .line 198
    const-string/jumbo v0, "HH:mm:ss.SSS"

    .line 199
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "HH:mm:ss.SSS"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/twitter/android/settings/developer/e$a;->c:Ljava/text/SimpleDateFormat;

    .line 200
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/settings/developer/e$a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e$a;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a()Ljava/util/List;
    .locals 1

    .prologue
    .line 188
    sget-object v0, Lcom/twitter/android/settings/developer/e$a;->a:Ljava/util/List;

    return-object v0
.end method

.method private a(ILandroid/view/View;)V
    .locals 6

    .prologue
    .line 223
    const v0, 0x7f1307d9

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 224
    const v1, 0x7f1307d8

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 225
    const v2, 0x7f1307da

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 226
    sget-object v3, Lcom/twitter/android/settings/developer/e$a;->a:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/analytics/model/ScribeLog;

    .line 228
    invoke-virtual {v3}, Lcom/twitter/analytics/model/ScribeLog;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 229
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e$a;->c:Ljava/text/SimpleDateFormat;

    invoke-virtual {v3}, Lcom/twitter/analytics/model/ScribeLog;->g()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 230
    invoke-virtual {v3}, Lcom/twitter/analytics/model/ScribeLog;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 232
    new-instance v0, Lcom/twitter/android/settings/developer/e$a$1;

    invoke-direct {v0, p0, v2}, Lcom/twitter/android/settings/developer/e$a$1;-><init>(Lcom/twitter/android/settings/developer/e$a;Landroid/widget/TextView;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 240
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/view/View;->setLongClickable(Z)V

    .line 241
    new-instance v0, Lcom/twitter/android/settings/developer/e$a$2;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/settings/developer/e$a$2;-><init>(Lcom/twitter/android/settings/developer/e$a;I)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 252
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/settings/developer/e$a;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 188
    invoke-direct {p0, p1}, Lcom/twitter/android/settings/developer/e$a;->a(Ljava/util/List;)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/model/ScribeLog;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 204
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 205
    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 206
    sget-object v1, Lcom/twitter/android/settings/developer/e$a;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 207
    sget-object v1, Lcom/twitter/android/settings/developer/e$a;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 208
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/e$a;->notifyDataSetChanged()V

    .line 209
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 214
    .line 215
    if-nez p2, :cond_0

    .line 216
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/e$a;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040395

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 218
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/settings/developer/e$a;->a(ILandroid/view/View;)V

    .line 219
    return-object p2
.end method
