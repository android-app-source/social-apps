.class Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/object/j;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/object/j",
        "<",
        "Landroid/preference/Preference;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lcom/twitter/android/settings/developer/c$a;

.field final b:I

.field final synthetic c:Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;

.field private d:Landroid/preference/Preference;

.field private e:Z


# direct methods
.method constructor <init>(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;Lcom/twitter/android/settings/developer/c$a;I)V
    .locals 1

    .prologue
    .line 428
    iput-object p1, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;->c:Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 426
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;->e:Z

    .line 429
    iput-object p2, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;->a:Lcom/twitter/android/settings/developer/c$a;

    .line 430
    iput p3, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;->b:I

    .line 431
    return-void
.end method

.method private a(Lcom/twitter/android/settings/developer/c$a;I)Landroid/preference/Preference;
    .locals 5

    .prologue
    .line 448
    iget-object v1, p1, Lcom/twitter/android/settings/developer/c$a;->a:Ljava/lang/String;

    .line 449
    iget-object v0, p1, Lcom/twitter/android/settings/developer/c$a;->c:Ljava/util/List;

    .line 450
    iget-object v2, p1, Lcom/twitter/android/settings/developer/c$a;->b:Ljava/lang/String;

    .line 454
    if-eqz v0, :cond_0

    .line 455
    iget-object v3, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;->c:Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;

    .line 456
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/CharSequence;

    invoke-interface {v0, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    .line 455
    invoke-static {v3, v1, v0, v2}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->a(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;Ljava/lang/String;[Ljava/lang/CharSequence;Ljava/lang/String;)Landroid/preference/ListPreference;

    move-result-object v0

    .line 461
    :goto_0
    invoke-virtual {v0, p2}, Landroid/preference/Preference;->setOrder(I)V

    .line 462
    invoke-direct {p0, v0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;->a(Landroid/preference/Preference;)V

    .line 463
    return-object v0

    .line 459
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;->c:Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;

    invoke-static {v0, v1, v2}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->a(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;Ljava/lang/String;Ljava/lang/String;)Landroid/preference/EditTextPreference;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/preference/Preference;)V
    .locals 2

    .prologue
    .line 467
    iget-boolean v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;->e:Z

    if-eqz v0, :cond_0

    .line 468
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "[DUPLICATE FEATURESWITCH - PLEASE CHECK CONFIG] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 469
    invoke-virtual {p1}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 470
    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 472
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 434
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;->e:Z

    .line 435
    return-void
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 421
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;->c()Landroid/preference/Preference;

    move-result-object v0

    return-object v0
.end method

.method public c()Landroid/preference/Preference;
    .locals 2

    .prologue
    .line 440
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;->d:Landroid/preference/Preference;

    if-nez v0, :cond_0

    .line 441
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;->a:Lcom/twitter/android/settings/developer/c$a;

    iget v1, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;->b:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;->a(Lcom/twitter/android/settings/developer/c$a;I)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;->d:Landroid/preference/Preference;

    .line 443
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;->d:Landroid/preference/Preference;

    return-object v0
.end method
