.class Lcom/twitter/android/settings/developer/DebugSettingsActivity$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/settings/developer/DebugSettingsActivity;->p()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/provider/t;

.field final synthetic b:I

.field final synthetic c:J

.field final synthetic d:Lcom/twitter/model/timeline/v;

.field final synthetic e:Lcom/twitter/android/settings/developer/DebugSettingsActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/settings/developer/DebugSettingsActivity;Lcom/twitter/library/provider/t;IJLcom/twitter/model/timeline/v;)V
    .locals 0

    .prologue
    .line 771
    iput-object p1, p0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$3;->e:Lcom/twitter/android/settings/developer/DebugSettingsActivity;

    iput-object p2, p0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$3;->a:Lcom/twitter/library/provider/t;

    iput p3, p0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$3;->b:I

    iput-wide p4, p0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$3;->c:J

    iput-object p6, p0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$3;->d:Lcom/twitter/model/timeline/v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 774
    iget-object v0, p0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$3;->a:Lcom/twitter/library/provider/t;

    iget v1, p0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$3;->b:I

    iget-wide v2, p0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$3;->c:J

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/library/provider/t;->a(IJLjava/lang/String;)I

    move-result v1

    .line 775
    iget-object v2, p0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$3;->a:Lcom/twitter/library/provider/t;

    iget-object v0, p0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$3;->d:Lcom/twitter/model/timeline/v;

    .line 776
    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/provider/s$a;->a(Ljava/util/List;)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    iget v3, p0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$3;->b:I

    .line 777
    invoke-virtual {v0, v3}, Lcom/twitter/library/provider/s$a;->a(I)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    iget-wide v4, p0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$3;->c:J

    .line 778
    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/provider/s$a;->a(J)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    const/4 v3, 0x1

    .line 779
    invoke-virtual {v0, v3}, Lcom/twitter/library/provider/s$a;->d(Z)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 780
    invoke-virtual {v0}, Lcom/twitter/library/provider/s$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/s;

    .line 775
    invoke-virtual {v2, v0}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/library/provider/s;)I

    move-result v0

    .line 782
    if-gtz v1, :cond_0

    if-lez v0, :cond_1

    .line 783
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/settings/developer/DebugSettingsActivity$3;->e:Lcom/twitter/android/settings/developer/DebugSettingsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/settings/developer/DebugSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lawc;->a(Landroid/content/Context;)V

    .line 785
    :cond_1
    return-void
.end method
