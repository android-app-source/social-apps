.class Lcom/twitter/android/settings/developer/e$c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/settings/developer/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation


# instance fields
.field final a:Landroid/view/ViewGroup;

.field final b:Landroid/widget/EditText;

.field final c:Landroid/widget/Button;

.field final d:Landroid/widget/Button;

.field final e:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040394

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/settings/developer/e$c;->a:Landroid/view/ViewGroup;

    .line 156
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e$c;->a:Landroid/view/ViewGroup;

    const v1, 0x7f1307d5

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/settings/developer/e$c;->b:Landroid/widget/EditText;

    .line 157
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e$c;->a:Landroid/view/ViewGroup;

    const v1, 0x7f130508

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/settings/developer/e$c;->c:Landroid/widget/Button;

    .line 158
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e$c;->a:Landroid/view/ViewGroup;

    const v1, 0x7f1307d6

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/settings/developer/e$c;->d:Landroid/widget/Button;

    .line 159
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e$c;->a:Landroid/view/ViewGroup;

    const v1, 0x7f1307d7

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/twitter/android/settings/developer/e$c;->e:Landroid/widget/ListView;

    .line 160
    return-void
.end method

.method private a(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e$c;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 174
    return-void
.end method

.method private a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e$c;->c:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    return-void
.end method

.method private a(Landroid/widget/ArrayAdapter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/twitter/analytics/model/ScribeLog;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 169
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e$c;->e:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 170
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/settings/developer/e$c;Landroid/text/TextWatcher;)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lcom/twitter/android/settings/developer/e$c;->a(Landroid/text/TextWatcher;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/settings/developer/e$c;Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lcom/twitter/android/settings/developer/e$c;->b(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/settings/developer/e$c;Landroid/widget/ArrayAdapter;)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lcom/twitter/android/settings/developer/e$c;->a(Landroid/widget/ArrayAdapter;)V

    return-void
.end method

.method private b(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e$c;->d:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/settings/developer/e$c;Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lcom/twitter/android/settings/developer/e$c;->a(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e$c;->a:Landroid/view/ViewGroup;

    return-object v0
.end method
