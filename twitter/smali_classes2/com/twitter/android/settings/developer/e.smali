.class Lcom/twitter/android/settings/developer/e;
.super Laog;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/settings/developer/e$b;,
        Lcom/twitter/android/settings/developer/e$a;,
        Lcom/twitter/android/settings/developer/e$c;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/android/settings/developer/e$a;

.field private final c:Lcom/twitter/android/settings/developer/e$c;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/model/ScribeLog;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/content/SharedPreferences;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/twitter/android/settings/developer/e$a;Lcom/twitter/android/settings/developer/e$c;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Laog;-><init>()V

    .line 42
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/developer/e;->d:Ljava/util/List;

    .line 55
    iput-object p1, p0, Lcom/twitter/android/settings/developer/e;->a:Landroid/content/Context;

    .line 56
    iput-object p3, p0, Lcom/twitter/android/settings/developer/e;->c:Lcom/twitter/android/settings/developer/e$c;

    .line 57
    iput-object p2, p0, Lcom/twitter/android/settings/developer/e;->b:Lcom/twitter/android/settings/developer/e$a;

    .line 58
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e;->b:Lcom/twitter/android/settings/developer/e$a;

    invoke-static {}, Lcom/twitter/android/settings/developer/e;->j()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/settings/developer/e$a;->a(Lcom/twitter/android/settings/developer/e$a;Ljava/util/List;)V

    .line 59
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e;->c:Lcom/twitter/android/settings/developer/e$c;

    iget-object v1, p0, Lcom/twitter/android/settings/developer/e;->b:Lcom/twitter/android/settings/developer/e$a;

    invoke-static {v0, v1}, Lcom/twitter/android/settings/developer/e$c;->a(Lcom/twitter/android/settings/developer/e$c;Landroid/widget/ArrayAdapter;)V

    .line 60
    invoke-static {}, Lcom/twitter/android/settings/developer/e;->j()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/developer/e;->d:Ljava/util/List;

    .line 61
    invoke-direct {p0}, Lcom/twitter/android/settings/developer/e;->h()V

    .line 62
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e;->c:Lcom/twitter/android/settings/developer/e$c;

    invoke-virtual {v0}, Lcom/twitter/android/settings/developer/e$c;->aN_()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/e;->a(Landroid/view/View;)V

    .line 63
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/developer/e;->e:Landroid/content/SharedPreferences;

    .line 64
    invoke-direct {p0}, Lcom/twitter/android/settings/developer/e;->f()V

    .line 65
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/settings/developer/e;)Lcom/twitter/android/settings/developer/e$a;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e;->b:Lcom/twitter/android/settings/developer/e$a;

    return-object v0
.end method

.method static a(Landroid/content/Context;)Lcom/twitter/android/settings/developer/e;
    .locals 3

    .prologue
    .line 48
    new-instance v0, Lcom/twitter/android/settings/developer/e$a;

    invoke-direct {v0, p0}, Lcom/twitter/android/settings/developer/e$a;-><init>(Landroid/content/Context;)V

    .line 49
    new-instance v1, Lcom/twitter/android/settings/developer/e$c;

    invoke-direct {v1, p0}, Lcom/twitter/android/settings/developer/e$c;-><init>(Landroid/content/Context;)V

    .line 50
    new-instance v2, Lcom/twitter/android/settings/developer/e;

    invoke-direct {v2, p0, v0, v1}, Lcom/twitter/android/settings/developer/e;-><init>(Landroid/content/Context;Lcom/twitter/android/settings/developer/e$a;Lcom/twitter/android/settings/developer/e$c;)V

    return-object v2
.end method

.method static synthetic a(Lcom/twitter/android/settings/developer/e;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/twitter/android/settings/developer/e;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private a(Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 123
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 125
    invoke-static {}, Lcom/twitter/android/settings/developer/e;->j()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/developer/e;->d:Ljava/util/List;

    .line 126
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e;->b:Lcom/twitter/android/settings/developer/e$a;

    invoke-static {}, Lcom/twitter/android/settings/developer/e;->j()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/settings/developer/e$a;->a(Lcom/twitter/android/settings/developer/e$a;Ljava/util/List;)V

    .line 136
    :goto_0
    return-void

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 129
    invoke-static {}, Lcom/twitter/android/settings/developer/e;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    .line 130
    invoke-virtual {v0}, Lcom/twitter/analytics/model/ScribeLog;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 131
    iget-object v2, p0, Lcom/twitter/android/settings/developer/e;->d:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 134
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e;->b:Lcom/twitter/android/settings/developer/e$a;

    iget-object v1, p0, Lcom/twitter/android/settings/developer/e;->d:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/twitter/android/settings/developer/e$a;->a(Lcom/twitter/android/settings/developer/e$a;Ljava/util/List;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/settings/developer/e;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/twitter/android/settings/developer/e;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/settings/developer/e;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e;->a:Landroid/content/Context;

    return-object v0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e;->e:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "key_last_used_filter"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lcom/twitter/android/settings/developer/e;->c:Lcom/twitter/android/settings/developer/e$c;

    iget-object v1, v1, Lcom/twitter/android/settings/developer/e$c;->b:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 70
    invoke-direct {p0, v0}, Lcom/twitter/android/settings/developer/e;->a(Ljava/lang/CharSequence;)V

    .line 71
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e;->c:Lcom/twitter/android/settings/developer/e$c;

    iget-object v0, v0, Lcom/twitter/android/settings/developer/e$c;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 81
    iget-object v1, p0, Lcom/twitter/android/settings/developer/e;->e:Landroid/content/SharedPreferences;

    .line 82
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "key_last_used_filter"

    .line 83
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 84
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 85
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e;->c:Lcom/twitter/android/settings/developer/e$c;

    new-instance v1, Lcom/twitter/android/settings/developer/e$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/settings/developer/e$1;-><init>(Lcom/twitter/android/settings/developer/e;)V

    invoke-static {v0, v1}, Lcom/twitter/android/settings/developer/e$c;->a(Lcom/twitter/android/settings/developer/e$c;Landroid/view/View$OnClickListener;)V

    .line 95
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e;->c:Lcom/twitter/android/settings/developer/e$c;

    new-instance v1, Lcom/twitter/android/settings/developer/e$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/settings/developer/e$2;-><init>(Lcom/twitter/android/settings/developer/e;)V

    invoke-static {v0, v1}, Lcom/twitter/android/settings/developer/e$c;->b(Lcom/twitter/android/settings/developer/e$c;Landroid/view/View$OnClickListener;)V

    .line 105
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e;->c:Lcom/twitter/android/settings/developer/e$c;

    new-instance v1, Lcom/twitter/android/settings/developer/e$3;

    invoke-direct {v1, p0}, Lcom/twitter/android/settings/developer/e$3;-><init>(Lcom/twitter/android/settings/developer/e;)V

    invoke-static {v0, v1}, Lcom/twitter/android/settings/developer/e$c;->a(Lcom/twitter/android/settings/developer/e$c;Landroid/text/TextWatcher;)V

    .line 111
    return-void
.end method

.method private i()Ljava/lang/String;
    .locals 4

    .prologue
    .line 115
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 116
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    .line 117
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, "\n\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 119
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static j()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/model/ScribeLog;",
            ">;"
        }
    .end annotation

    .prologue
    .line 140
    invoke-static {}, Lcom/twitter/library/scribe/h;->a()Lcom/twitter/util/collection/b;

    move-result-object v0

    .line 141
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v1

    .line 142
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 143
    return-object v1
.end method


# virtual methods
.method public am_()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/twitter/android/settings/developer/e;->g()V

    .line 76
    invoke-super {p0}, Laog;->am_()V

    .line 77
    return-void
.end method
