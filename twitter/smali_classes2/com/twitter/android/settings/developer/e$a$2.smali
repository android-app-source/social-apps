.class Lcom/twitter/android/settings/developer/e$a$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/settings/developer/e$a;->a(ILandroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/twitter/android/settings/developer/e$a;


# direct methods
.method constructor <init>(Lcom/twitter/android/settings/developer/e$a;I)V
    .locals 0

    .prologue
    .line 241
    iput-object p1, p0, Lcom/twitter/android/settings/developer/e$a$2;->b:Lcom/twitter/android/settings/developer/e$a;

    iput p2, p0, Lcom/twitter/android/settings/developer/e$a$2;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 4

    .prologue
    .line 245
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v0, "android.intent.action.SEND"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 246
    const-string/jumbo v2, "android.intent.extra.TEXT"

    invoke-static {}, Lcom/twitter/android/settings/developer/e$a;->a()Ljava/util/List;

    move-result-object v0

    iget v3, p0, Lcom/twitter/android/settings/developer/e$a$2;->a:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/model/ScribeLog;

    invoke-virtual {v0}, Lcom/twitter/analytics/model/ScribeLog;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 247
    const-string/jumbo v0, "text/html"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 248
    iget-object v0, p0, Lcom/twitter/android/settings/developer/e$a$2;->b:Lcom/twitter/android/settings/developer/e$a;

    invoke-static {v0}, Lcom/twitter/android/settings/developer/e$a;->a(Lcom/twitter/android/settings/developer/e$a;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 249
    const/4 v0, 0x1

    return v0
.end method
