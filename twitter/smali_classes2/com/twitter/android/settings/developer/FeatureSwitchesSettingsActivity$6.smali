.class Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$6;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->e()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/e",
        "<",
        "Lccv;",
        "Lccv;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;)V
    .locals 0

    .prologue
    .line 383
    iput-object p1, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$6;->a:Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lccv;Lccv;)Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 388
    invoke-static {}, Lbqa;->c()Laua;

    move-result-object v0

    .line 389
    iget-object v1, p2, Lccv;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Laua;->a(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-object v2, p1, Lccv;->b:Ljava/lang/String;

    .line 390
    invoke-virtual {v0, v2}, Laua;->a(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    .line 391
    if-eqz v0, :cond_0

    .line 392
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 394
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Lccv;->b:Ljava/lang/String;

    iget-object v1, p2, Lccv;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 383
    check-cast p1, Lccv;

    check-cast p2, Lccv;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$6;->a(Lccv;Lccv;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
