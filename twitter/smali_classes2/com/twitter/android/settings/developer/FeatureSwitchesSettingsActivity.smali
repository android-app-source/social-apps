.class public Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;
.super Lcom/twitter/android/client/TwitterPreferenceActivity;
.source "Twttr"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$b;,
        Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/twitter/ui/widget/TwitterEditText;

.field private c:Landroid/preference/PreferenceCategory;

.field private e:Landroid/content/SharedPreferences;

.field private f:Landroid/content/SharedPreferences;

.field private g:Landroid/os/Handler;

.field private h:Lrx/j;

.field private final i:Lrx/subjects/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/d",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/twitter/android/client/TwitterPreferenceActivity;-><init>()V

    .line 73
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->a:Ljava/util/Map;

    .line 81
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->i:Lrx/subjects/d;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->e:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;Ljava/lang/String;Ljava/lang/String;)Landroid/preference/EditTextPreference;
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/preference/EditTextPreference;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Landroid/preference/EditTextPreference;
    .locals 2

    .prologue
    .line 269
    invoke-static {}, Lbqa;->c()Laua;

    move-result-object v0

    invoke-virtual {v0, p1}, Laua;->a(Ljava/lang/String;)Z

    move-result v0

    .line 270
    if-eqz v0, :cond_0

    const-string/jumbo v0, "[Overridden] "

    .line 271
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "Current: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 273
    new-instance v1, Landroid/preference/EditTextPreference;

    invoke-direct {v1, p0}, Landroid/preference/EditTextPreference;-><init>(Landroid/content/Context;)V

    .line 275
    invoke-virtual {v1, p1}, Landroid/preference/EditTextPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 276
    invoke-virtual {v1, p1}, Landroid/preference/EditTextPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 277
    invoke-virtual {v1, p1}, Landroid/preference/EditTextPreference;->setKey(Ljava/lang/String;)V

    .line 278
    invoke-virtual {v1, p2}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 279
    invoke-virtual {v1, v0}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 280
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/preference/EditTextPreference;->setPersistent(Z)V

    .line 281
    invoke-virtual {v1, p0}, Landroid/preference/EditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 282
    return-object v1

    .line 270
    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;Ljava/lang/String;[Ljava/lang/CharSequence;Ljava/lang/String;)Landroid/preference/ListPreference;
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->a(Ljava/lang/String;[Ljava/lang/CharSequence;Ljava/lang/String;)Landroid/preference/ListPreference;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;[Ljava/lang/CharSequence;Ljava/lang/String;)Landroid/preference/ListPreference;
    .locals 2

    .prologue
    .line 242
    invoke-static {}, Lbqa;->c()Laua;

    move-result-object v0

    invoke-virtual {v0, p1}, Laua;->a(Ljava/lang/String;)Z

    move-result v0

    .line 243
    if-eqz v0, :cond_0

    const-string/jumbo v0, "[Overridden] "

    .line 244
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "Current: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 245
    new-instance v1, Landroid/preference/ListPreference;

    invoke-direct {v1, p0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    .line 247
    invoke-virtual {v1, p1}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 248
    invoke-virtual {v1, p1}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 249
    invoke-virtual {v1, p1}, Landroid/preference/ListPreference;->setKey(Ljava/lang/String;)V

    .line 250
    invoke-virtual {v1, p2}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 251
    invoke-virtual {v1, p2}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 252
    invoke-virtual {v1, p3}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 254
    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 255
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setPersistent(Z)V

    .line 256
    invoke-virtual {v1, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 257
    return-object v1

    .line 243
    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;Ljava/lang/String;)Landroid/preference/Preference;
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->c(Ljava/lang/String;)Landroid/preference/Preference;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/widget/EditText;)Lrx/c;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/EditText;",
            ")",
            "Lrx/c",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 403
    invoke-static {p0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$b;->a(Landroid/widget/EditText;)Lrx/c;

    move-result-object v0

    const-wide/16 v2, 0xfa

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 404
    invoke-virtual {v0, v2, v3, v1}, Lrx/c;->e(JLjava/util/concurrent/TimeUnit;)Lrx/c;

    move-result-object v0

    .line 403
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->a(Ljava/util/List;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;)V
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;

    .line 224
    if-eqz v0, :cond_0

    .line 225
    invoke-virtual {v0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;->a()V

    .line 229
    :goto_0
    return-void

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lccv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 205
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 206
    const/4 v0, 0x0

    .line 208
    invoke-static {p1}, Lcom/twitter/android/settings/developer/c;->a(Ljava/util/List;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/settings/developer/c$a;

    .line 209
    iget-object v4, v0, Lcom/twitter/android/settings/developer/c$a;->a:Ljava/lang/String;

    new-instance v5, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;

    add-int/lit8 v2, v1, 0x1

    invoke-direct {v5, p0, v0, v1}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;-><init>(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;Lcom/twitter/android/settings/developer/c$a;I)V

    invoke-direct {p0, v4, v5}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->a(Ljava/lang/String;Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;)V

    move v1, v2

    .line 210
    goto :goto_0

    .line 211
    :cond_0
    return-void
.end method

.method static synthetic a(Lccv;)Z
    .locals 1

    .prologue
    .line 59
    invoke-static {p0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->b(Lccv;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lccv;[Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 59
    invoke-static {p0, p1}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->b(Lccv;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;Lcom/twitter/library/service/s;I)Z
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->b(Lcom/twitter/library/service/s;I)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;)Landroid/preference/PreferenceCategory;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->c:Landroid/preference/PreferenceCategory;

    return-object v0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 347
    invoke-direct {p0, p1}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->c(Ljava/lang/String;)Landroid/preference/Preference;

    move-result-object v0

    .line 348
    if-eqz v0, :cond_0

    .line 349
    invoke-virtual {p0, v0, p2}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z

    .line 350
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->finish()V

    .line 354
    :goto_0
    return-void

    .line 352
    :cond_0
    const-string/jumbo v0, "FeatureSwitches"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No feature switch found with key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static b(Lccv;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 358
    iget-object v1, p0, Lccv;->b:Ljava/lang/String;

    const-string/jumbo v2, "hashflags_settings"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 367
    :cond_0
    :goto_0
    return v0

    .line 364
    :cond_1
    iget-object v1, p0, Lccv;->c:Ljava/lang/Object;

    if-nez v1, :cond_2

    iget-object v1, p0, Lccv;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 367
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(Lccv;[Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 167
    array-length v2, p1

    if-nez v2, :cond_1

    .line 177
    :cond_0
    :goto_0
    return v0

    .line 171
    :cond_1
    iget-object v2, p0, Lccv;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "[^A-Za-z0-9 ]"

    const-string/jumbo v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 172
    array-length v4, p1

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v5, p1, v2

    .line 173
    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    move v0, v1

    .line 174
    goto :goto_0

    .line 172
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method static synthetic c(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->f:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private c(Ljava/lang/String;)Landroid/preference/Preference;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;

    .line 216
    if-eqz v0, :cond_0

    .line 217
    invoke-virtual {v0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$a;->c()Landroid/preference/Preference;

    move-result-object v0

    .line 219
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 194
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->e:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "feature_switch_search"

    const-string/jumbo v2, ""

    .line 195
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 196
    iget-object v1, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->b:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TwitterEditText;->setText(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v1, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->b:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TwitterEditText;->setSelection(I)V

    .line 198
    return-void
.end method

.method static synthetic d(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->i:Lrx/subjects/d;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/d;->a(Ljava/lang/Object;)V

    .line 202
    return-void
.end method

.method private e()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lccv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 372
    invoke-static {p0}, Lcom/twitter/android/settings/developer/c;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lrx/c;->a(Ljava/lang/Iterable;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$7;

    invoke-direct {v1, p0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$7;-><init>(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;)V

    .line 373
    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$6;

    invoke-direct {v1, p0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$6;-><init>(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;)V

    .line 383
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/functions/e;)Lrx/c;

    move-result-object v0

    .line 397
    invoke-virtual {v0}, Lrx/c;->p()Lcwa;

    move-result-object v0

    .line 398
    invoke-virtual {v0}, Lcwa;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 372
    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;)Ljava/util/List;
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->e()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private f()Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Ljava/util/List",
            "<",
            "Lccv;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 409
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->i:Lrx/subjects/d;

    .line 410
    invoke-static {}, Lcws;->c()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/d;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$8;

    invoke-direct {v1, p0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$8;-><init>(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;)V

    .line 411
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 409
    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/library/service/s;I)V
    .locals 0

    .prologue
    .line 182
    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/TwitterPreferenceActivity;->a(Lcom/twitter/library/service/s;I)V

    .line 183
    if-nez p2, :cond_0

    .line 184
    invoke-direct {p0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->d()V

    .line 186
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 85
    invoke-super {p0, p1}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 86
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->g:Landroid/os/Handler;

    .line 88
    const v0, 0x7f0a0c30

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->setTitle(I)V

    .line 89
    const v0, 0x7f08000e

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->addPreferencesFromResource(I)V

    .line 91
    const v0, 0x7f1304bc

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterEditText;

    iput-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->b:Lcom/twitter/ui/widget/TwitterEditText;

    .line 92
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->b:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, v3}, Lcom/twitter/ui/widget/TwitterEditText;->setVisibility(I)V

    .line 93
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->b:Lcom/twitter/ui/widget/TwitterEditText;

    const v1, 0x7f0a0c2e

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setLabelText(I)V

    .line 95
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 96
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->e:Landroid/content/SharedPreferences;

    .line 98
    const-string/jumbo v0, "fs_values"

    .line 99
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->c:Landroid/preference/PreferenceCategory;

    .line 100
    const-string/jumbo v0, "fs_override"

    invoke-virtual {p0, v0, v3}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->f:Landroid/content/SharedPreferences;

    .line 102
    const-string/jumbo v0, "reset"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 103
    invoke-direct {p0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->c()V

    .line 105
    new-instance v0, Lcom/twitter/library/api/h;

    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 106
    invoke-static {}, Lbqa;->d()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/twitter/library/api/h;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    const-string/jumbo v1, "FS fetches are always polling."

    .line 107
    invoke-virtual {v0, v1}, Lcom/twitter/library/api/h;->l(Ljava/lang/String;)Lcom/twitter/library/service/s;

    move-result-object v0

    .line 105
    invoke-virtual {p0, v0, v3}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 109
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "override_key"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 110
    invoke-virtual {p0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "override_value"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 112
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v2

    invoke-virtual {v2}, Lcof;->p()Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 113
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->b:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-static {v0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->a(Landroid/widget/EditText;)Lrx/c;

    move-result-object v0

    .line 117
    invoke-static {}, Lcws;->c()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$1;-><init>(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;)V

    .line 118
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 126
    invoke-direct {p0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->f()Lrx/c;

    move-result-object v1

    .line 128
    new-instance v2, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$3;

    invoke-direct {v2, p0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$3;-><init>(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;)V

    invoke-static {v0, v1, v2}, Lrx/c;->a(Lrx/c;Lrx/c;Lrx/functions/e;)Lrx/c;

    move-result-object v0

    .line 142
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$2;-><init>(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;)V

    .line 143
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->h:Lrx/j;

    .line 155
    invoke-direct {p0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->d()V

    .line 156
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 160
    invoke-super {p0}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onDestroy()V

    .line 161
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->h:Lrx/j;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->h:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 164
    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 290
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 292
    :goto_0
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    .line 293
    invoke-static {p0, v1, v0}, Lcom/twitter/android/settings/developer/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 295
    if-nez v1, :cond_1

    .line 296
    iget-object v1, p0, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;->g:Landroid/os/Handler;

    new-instance v2, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$4;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$4;-><init>(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 303
    const/4 v0, 0x0

    .line 306
    :goto_1
    return v0

    .line 290
    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0

    .line 305
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "[Overridden] Current: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 306
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 315
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    .line 316
    const-string/jumbo v2, "reset"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 317
    new-instance v1, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$5;

    invoke-direct {v1, p0}, Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity$5;-><init>(Lcom/twitter/android/settings/developer/FeatureSwitchesSettingsActivity;)V

    .line 333
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0a0c2a

    .line 334
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a0c2b

    .line 335
    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a00f6

    const/4 v3, 0x0

    .line 336
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 337
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 338
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 339
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 343
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 190
    const/4 v0, 0x0

    return v0
.end method
