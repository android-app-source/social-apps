.class public Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;
.super Lcom/twitter/android/settings/BaseAccountSettingsActivity;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;,
        Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$a;
    }
.end annotation


# instance fields
.field private b:Z

.field private c:Z

.field private e:I

.field private f:Landroid/preference/CheckBoxPreference;

.field private g:Landroid/preference/ListPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/twitter/android/settings/BaseAccountSettingsActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;I)I
    .locals 0

    .prologue
    .line 28
    iput p1, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->e:I

    return p1
.end method

.method static synthetic a(Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;)Landroid/preference/CheckBoxPreference;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->f:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;Z)Z
    .locals 0

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->c:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;)Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->b:Z

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;)Landroid/preference/ListPreference;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->g:Landroid/preference/ListPreference;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;)Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->c:Z

    return v0
.end method

.method static synthetic e(Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;)I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->e:I

    return v0
.end method

.method static synthetic f(Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;)J
    .locals 2

    .prologue
    .line 28
    iget-wide v0, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->G:J

    return-wide v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/twitter/android/settings/BaseAccountSettingsActivity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    const v0, 0x7f080015

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->addPreferencesFromResource(I)V

    .line 44
    invoke-static {p0}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->b:Z

    .line 46
    const-string/jumbo v0, "sync_data"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->f:Landroid/preference/CheckBoxPreference;

    .line 47
    iget-object v0, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->f:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f0a0891

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummaryOn(I)V

    .line 48
    iget-object v0, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->f:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f0a0890

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummaryOff(I)V

    .line 50
    const-string/jumbo v0, "polling_interval"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->g:Landroid/preference/ListPreference;

    .line 51
    return-void
.end method

.method public onStart()V
    .locals 4

    .prologue
    .line 55
    invoke-super {p0}, Lcom/twitter/android/settings/BaseAccountSettingsActivity;->onStart()V

    .line 56
    new-instance v0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$a;

    iget-wide v2, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->G:J

    iget-object v1, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->a:Ljava/lang/String;

    invoke-direct {v0, p0, v2, v3, v1}, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$a;-><init>(Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;JLjava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 57
    return-void
.end method

.method public onStop()V
    .locals 4

    .prologue
    .line 61
    invoke-super {p0}, Lcom/twitter/android/settings/BaseAccountSettingsActivity;->onStop()V

    .line 62
    new-instance v0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;

    iget-wide v2, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->G:J

    iget-object v1, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->a:Ljava/lang/String;

    invoke-direct {v0, p0, v2, v3, v1}, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;-><init>(Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;JLjava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 63
    return-void
.end method
