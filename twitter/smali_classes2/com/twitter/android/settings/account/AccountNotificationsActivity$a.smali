.class Lcom/twitter/android/settings/account/AccountNotificationsActivity$a;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/settings/account/AccountNotificationsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/settings/account/AccountNotificationsActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/settings/account/AccountNotificationsActivity;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity$a;->a:Lcom/twitter/android/settings/account/AccountNotificationsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/settings/account/AccountNotificationsActivity;Lcom/twitter/android/settings/account/AccountNotificationsActivity$1;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/twitter/android/settings/account/AccountNotificationsActivity$a;-><init>(Lcom/twitter/android/settings/account/AccountNotificationsActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 4

    .prologue
    .line 63
    invoke-static {}, Lcom/twitter/library/provider/j;->c()Lcom/twitter/library/provider/j;

    move-result-object v0

    .line 64
    iget-object v1, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity$a;->a:Lcom/twitter/android/settings/account/AccountNotificationsActivity;

    invoke-static {v1}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->a(Lcom/twitter/android/settings/account/AccountNotificationsActivity;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/provider/j;->a(J)I

    move-result v0

    .line 65
    const/16 v1, 0x200

    invoke-static {v0, v1}, Lcga;->b(II)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 70
    iget-object v0, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity$a;->a:Lcom/twitter/android/settings/account/AccountNotificationsActivity;

    iget-object v1, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity$a;->a:Lcom/twitter/android/settings/account/AccountNotificationsActivity;

    .line 71
    invoke-static {v1}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->b(Lcom/twitter/android/settings/account/AccountNotificationsActivity;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/twitter/library/platform/notifications/PushRegistration;->b(Landroid/content/Context;J)Z

    move-result v0

    .line 73
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    iget-object v0, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity$a;->a:Lcom/twitter/android/settings/account/AccountNotificationsActivity;

    invoke-static {v0}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->c(Lcom/twitter/android/settings/account/AccountNotificationsActivity;)V

    .line 92
    :goto_0
    return-void

    .line 78
    :cond_0
    if-nez v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity$a;->a:Lcom/twitter/android/settings/account/AccountNotificationsActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity$a;->a:Lcom/twitter/android/settings/account/AccountNotificationsActivity;

    const-class v3, Lcom/twitter/android/settings/MobileNotificationsActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "NotificationSettingsActivity_account_name"

    iget-object v3, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity$a;->a:Lcom/twitter/android/settings/account/AccountNotificationsActivity;

    .line 81
    invoke-static {v3}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->d(Lcom/twitter/android/settings/account/AccountNotificationsActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 79
    invoke-static {v0, v1, v4}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->a(Lcom/twitter/android/settings/account/AccountNotificationsActivity;Landroid/content/Intent;I)V

    goto :goto_0

    .line 86
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity$a;->a:Lcom/twitter/android/settings/account/AccountNotificationsActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity$a;->a:Lcom/twitter/android/settings/account/AccountNotificationsActivity;

    const-class v3, Lcom/twitter/android/settings/TweetSettingsActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "TweetSettingsActivity_account_name"

    iget-object v3, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity$a;->a:Lcom/twitter/android/settings/account/AccountNotificationsActivity;

    .line 88
    invoke-static {v3}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->e(Lcom/twitter/android/settings/account/AccountNotificationsActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "TweetSettingsActivity_enabled"

    const/4 v3, 0x0

    .line 89
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "TweetSettingsActivity_from_notification_landing"

    .line 90
    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x2

    .line 86
    invoke-static {v0, v1, v2}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->a(Lcom/twitter/android/settings/account/AccountNotificationsActivity;Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 60
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/settings/account/AccountNotificationsActivity$a;->a([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 60
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/twitter/android/settings/account/AccountNotificationsActivity$a;->a(Ljava/lang/Boolean;)V

    return-void
.end method
