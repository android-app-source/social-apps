.class public Lcom/twitter/android/settings/account/AccountNotificationsActivity;
.super Lcom/twitter/android/settings/BaseAccountSettingsActivity;
.source "Twttr"

# interfaces
.implements Laiu;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/settings/account/AccountNotificationsActivity$a;
    }
.end annotation


# instance fields
.field private b:Lcom/twitter/android/settings/ListPreference;

.field private c:Laip;

.field private e:Lcom/twitter/model/core/TwitterUser;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/twitter/android/settings/BaseAccountSettingsActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/settings/account/AccountNotificationsActivity;)J
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->G:J

    return-wide v0
.end method

.method private static a(Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;Lcgi;J)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 137
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/settings/account/AccountNotificationsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 138
    const-string/jumbo v1, "extra_account_id"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 139
    const-string/jumbo v1, "AccountNotificationActivity_profile_account_user"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 140
    const-string/jumbo v1, "AccountNotificationActivity_profile_username"

    invoke-virtual {p1}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    const-string/jumbo v1, "pc"

    invoke-static {p2}, Lcgi;->a(Lcgi;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 142
    return-object v0
.end method

.method private a(ZI)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 236
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 237
    const-string/jumbo v1, "AccountNotificationActivity_notifications_enabled"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 238
    const-string/jumbo v1, "AccountNotificationActivity_profile_account_user"

    iget-object v2, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->e:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 239
    const-string/jumbo v1, "AccountNotificationActivity_friendship"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 240
    return-object v0
.end method

.method public static a(Landroid/app/Activity;Lcom/twitter/model/core/TwitterUser;Lcgi;JI)V
    .locals 1

    .prologue
    .line 57
    invoke-static {p0, p1, p2, p3, p4}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->a(Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;Lcgi;J)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, p5}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 58
    return-void
.end method

.method private a(Landroid/content/Intent;I)V
    .locals 5

    .prologue
    .line 96
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a097d

    .line 97
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 98
    invoke-virtual {p0}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a097c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->e:Lcom/twitter/model/core/TwitterUser;

    .line 99
    invoke-virtual {v4}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 98
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00f6

    const/4 v2, 0x0

    .line 100
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0800

    new-instance v2, Lcom/twitter/android/settings/account/AccountNotificationsActivity$1;

    invoke-direct {v2, p0, p1, p2}, Lcom/twitter/android/settings/account/AccountNotificationsActivity$1;-><init>(Lcom/twitter/android/settings/account/AccountNotificationsActivity;Landroid/content/Intent;I)V

    .line 101
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 107
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/settings/account/AccountNotificationsActivity;Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->a(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/settings/account/AccountNotificationsActivity;)J
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->G:J

    return-wide v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->b:Lcom/twitter/android/settings/ListPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/settings/ListPreference;->showDialog(Landroid/os/Bundle;)V

    .line 132
    return-void
.end method

.method static synthetic c(Lcom/twitter/android/settings/account/AccountNotificationsActivity;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->c()V

    return-void
.end method

.method static synthetic d(Lcom/twitter/android/settings/account/AccountNotificationsActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/settings/account/AccountNotificationsActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->b:Lcom/twitter/android/settings/ListPreference;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/settings/ListPreference;->setValue(Ljava/lang/String;)V

    .line 211
    return-void
.end method

.method public a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->b:Lcom/twitter/android/settings/ListPreference;

    invoke-virtual {v0, p1}, Lcom/twitter/android/settings/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 216
    iget-object v0, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->b:Lcom/twitter/android/settings/ListPreference;

    invoke-virtual {v0, p2}, Lcom/twitter/android/settings/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 217
    return-void
.end method

.method protected ad_()V
    .locals 2

    .prologue
    .line 190
    invoke-super {p0}, Lcom/twitter/android/settings/BaseAccountSettingsActivity;->ad_()V

    .line 191
    const-string/jumbo v0, "account_notif_presenter"

    iget-object v1, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->c:Laip;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 111
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-nez p3, :cond_1

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    const-string/jumbo v0, "NotificationSettingsActivity_enabled"

    invoke-virtual {p3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 116
    const-string/jumbo v1, "TweetSettingsActivity_enabled"

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 118
    const/4 v2, 0x1

    if-ne p1, v2, :cond_2

    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 120
    invoke-direct {p0}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->c()V

    goto :goto_0

    .line 124
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    if-eqz v1, :cond_0

    .line 125
    invoke-direct {p0}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->c()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 147
    invoke-super {p0, p1}, Lcom/twitter/android/settings/BaseAccountSettingsActivity;->onCreate(Landroid/os/Bundle;)V

    .line 149
    invoke-virtual {p0}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 150
    const-string/jumbo v0, "AccountNotificationActivity_profile_account_user"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iput-object v0, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->e:Lcom/twitter/model/core/TwitterUser;

    .line 151
    const-string/jumbo v0, "AccountNotificationActivity_profile_username"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 152
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 153
    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 156
    :cond_0
    const v0, 0x7f080003

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->addPreferencesFromResource(I)V

    .line 158
    const-string/jumbo v0, "account_notif"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/settings/ListPreference;

    iput-object v0, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->b:Lcom/twitter/android/settings/ListPreference;

    .line 159
    iget-object v0, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->b:Lcom/twitter/android/settings/ListPreference;

    invoke-virtual {v0, p0}, Lcom/twitter/android/settings/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 160
    iget-object v0, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->b:Lcom/twitter/android/settings/ListPreference;

    new-instance v2, Lcom/twitter/android/settings/account/AccountNotificationsActivity$2;

    invoke-direct {v2, p0}, Lcom/twitter/android/settings/account/AccountNotificationsActivity$2;-><init>(Lcom/twitter/android/settings/account/AccountNotificationsActivity;)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/settings/ListPreference;->a(Lcom/twitter/android/settings/ListPreference$a;)V

    .line 168
    const-string/jumbo v0, "account_notif_presenter"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->b_(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laip;

    .line 169
    if-eqz v0, :cond_1

    .line 170
    iput-object v0, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->c:Laip;

    .line 184
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->c:Laip;

    invoke-interface {v0, p0}, Laip;->a(Laiu;)V

    .line 185
    iget-object v0, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->c:Laip;

    invoke-interface {v0, p0}, Laip;->a(Landroid/content/Context;)V

    .line 186
    return-void

    .line 172
    :cond_1
    const-string/jumbo v0, "pc"

    .line 173
    invoke-virtual {v1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 172
    invoke-static {v0}, Lcgi;->a([B)Lcgi;

    move-result-object v0

    .line 174
    iget-wide v2, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->G:J

    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v1

    .line 175
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v2

    .line 176
    new-instance v3, Lais;

    iget-object v4, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->e:Lcom/twitter/model/core/TwitterUser;

    invoke-direct {v3, v1, v2, v4, v0}, Lais;-><init>(Lcom/twitter/library/provider/t;Lcom/twitter/library/client/p;Lcom/twitter/model/core/TwitterUser;Lcgi;)V

    .line 179
    new-instance v0, Laio;

    invoke-direct {v0, v3}, Laio;-><init>(Lair;)V

    .line 181
    new-instance v1, Laiq;

    invoke-direct {v1, v0}, Laiq;-><init>(Lain;)V

    iput-object v1, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->c:Laip;

    goto :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 221
    const-string/jumbo v1, "account_notif"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 222
    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 223
    iget-object v1, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->c:Laip;

    invoke-interface {v1, v2}, Laip;->a(I)Z

    move-result v1

    .line 225
    if-eqz v2, :cond_0

    const/4 v0, 0x1

    .line 226
    :cond_0
    iget-object v2, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->c:Laip;

    iget-object v3, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->e:Lcom/twitter/model/core/TwitterUser;

    invoke-interface {v2, v3}, Laip;->a(Lcom/twitter/model/core/TwitterUser;)I

    move-result v2

    .line 227
    const/4 v3, -0x1

    invoke-direct {p0, v0, v2}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->a(ZI)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v3, v0}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->setResult(ILandroid/content/Intent;)V

    move v0, v1

    .line 231
    :cond_1
    return v0
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 196
    invoke-super {p0}, Lcom/twitter/android/settings/BaseAccountSettingsActivity;->onStart()V

    .line 197
    iget-object v0, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->c:Laip;

    invoke-interface {v0, p0}, Laip;->a(Laiu;)V

    .line 198
    iget-object v0, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->c:Laip;

    invoke-virtual {p0}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-interface {v0, p0, v1}, Laip;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 199
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 203
    invoke-super {p0}, Lcom/twitter/android/settings/BaseAccountSettingsActivity;->onStop()V

    .line 204
    iget-object v0, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->c:Laip;

    invoke-virtual {p0}, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-interface {v0, p0, v1}, Laip;->b(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 205
    iget-object v0, p0, Lcom/twitter/android/settings/account/AccountNotificationsActivity;->c:Laip;

    invoke-interface {v0}, Laip;->a()V

    .line 206
    return-void
.end method
