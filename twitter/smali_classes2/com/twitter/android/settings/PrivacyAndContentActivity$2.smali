.class Lcom/twitter/android/settings/PrivacyAndContentActivity$2;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/settings/PrivacyAndContentActivity;->c()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/settings/PrivacyAndContentActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/settings/PrivacyAndContentActivity;)V
    .locals 0

    .prologue
    .line 252
    iput-object p1, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity$2;->a:Lcom/twitter/android/settings/PrivacyAndContentActivity;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 252
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/android/settings/PrivacyAndContentActivity$2;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 3

    .prologue
    .line 255
    move-object v0, p1

    check-cast v0, Lbht;

    invoke-virtual {v0}, Lbht;->e()Lbil;

    move-result-object v0

    .line 256
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 257
    invoke-virtual {v0}, Lbil;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity$2;->a:Lcom/twitter/android/settings/PrivacyAndContentActivity;

    invoke-static {v0}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->a(Lcom/twitter/android/settings/PrivacyAndContentActivity;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/settings/PrivacyAndContentActivity$2;->a:Lcom/twitter/android/settings/PrivacyAndContentActivity;

    const v2, 0x7f0a0823

    invoke-virtual {v1, v2}, Lcom/twitter/android/settings/PrivacyAndContentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 261
    :cond_0
    return-void
.end method
