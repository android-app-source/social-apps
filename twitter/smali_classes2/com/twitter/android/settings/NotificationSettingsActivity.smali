.class public Lcom/twitter/android/settings/NotificationSettingsActivity;
.super Lcom/twitter/android/settings/BaseAccountSettingsActivity;
.source "Twttr"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field private c:Landroid/preference/CheckBoxPreference;

.field private e:Landroid/preference/CheckBoxPreference;

.field private f:Landroid/preference/CheckBoxPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "muted_keywords"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "privacy_and_safety"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "mobile_notifications"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "advanced"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/settings/NotificationSettingsActivity;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/twitter/android/settings/BaseAccountSettingsActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;JZ)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 53
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/settings/NotificationSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "extra_account_id"

    .line 54
    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "sync_settings"

    .line 55
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 53
    return-object v0
.end method

.method private a(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 222
    invoke-direct {p0, p2}, Lcom/twitter/android/settings/NotificationSettingsActivity;->a(Lcom/twitter/model/account/UserSettings;)V

    .line 223
    const/4 v0, 0x0

    .line 224
    invoke-static {p0, p1, p2, v2, v0}, Lbbg;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;ZLjava/lang/String;)Lbbg;

    move-result-object v0

    .line 225
    new-instance v1, Lcom/twitter/library/service/o;

    invoke-direct {v1}, Lcom/twitter/library/service/o;-><init>()V

    invoke-virtual {v0, v1}, Lbbg;->a(Lcom/twitter/async/service/k;)Lcom/twitter/async/service/AsyncOperation;

    .line 226
    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/settings/NotificationSettingsActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 227
    return-void
.end method

.method private a(Lcom/twitter/model/account/UserSettings;)V
    .locals 3

    .prologue
    .line 230
    iget-object v0, p0, Lcom/twitter/android/settings/NotificationSettingsActivity;->e:Landroid/preference/CheckBoxPreference;

    iget-object v1, p1, Lcom/twitter/model/account/UserSettings;->A:Ljava/lang/String;

    const-string/jumbo v2, "following"

    .line 231
    invoke-static {v1, v2}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 230
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 232
    iget-object v0, p0, Lcom/twitter/android/settings/NotificationSettingsActivity;->e:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 234
    iget-object v0, p0, Lcom/twitter/android/settings/NotificationSettingsActivity;->f:Landroid/preference/CheckBoxPreference;

    iget-object v1, p1, Lcom/twitter/model/account/UserSettings;->z:Ljava/lang/String;

    const-string/jumbo v2, "enabled"

    invoke-static {v1, v2}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 236
    iget-object v0, p0, Lcom/twitter/android/settings/NotificationSettingsActivity;->f:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 237
    iget-object v0, p0, Lcom/twitter/android/settings/NotificationSettingsActivity;->f:Landroid/preference/CheckBoxPreference;

    iget-object v1, p1, Lcom/twitter/model/account/UserSettings;->A:Ljava/lang/String;

    const-string/jumbo v2, "unfiltered"

    invoke-static {v1, v2}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 239
    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 5

    .prologue
    .line 242
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/settings/NotificationSettingsActivity;->G:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz p2, :cond_0

    const-string/jumbo v0, "on"

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 243
    return-void

    .line 242
    :cond_0
    const-string/jumbo v0, "off"

    goto :goto_0
.end method

.method private b(Z)V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 189
    if-eqz p1, :cond_0

    .line 190
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/settings/NotificationSettingsActivity;->G:J

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v4, "settings"

    aput-object v4, v1, v5

    const-string/jumbo v4, "notifications_timeline"

    aput-object v4, v1, v6

    const-string/jumbo v4, "badge"

    aput-object v4, v1, v7

    const-string/jumbo v4, "all"

    aput-object v4, v1, v8

    const-string/jumbo v4, "select"

    aput-object v4, v1, v9

    invoke-direct {v0, v2, v3, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 196
    :goto_0
    return-void

    .line 193
    :cond_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/settings/NotificationSettingsActivity;->G:J

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v4, "settings"

    aput-object v4, v1, v5

    const-string/jumbo v4, "notifications_timeline"

    aput-object v4, v1, v6

    const-string/jumbo v4, "badge"

    aput-object v4, v1, v7

    const/4 v4, 0x0

    aput-object v4, v1, v8

    const-string/jumbo v4, "deselect"

    aput-object v4, v1, v9

    invoke-direct {v0, v2, v3, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 102
    iget-object v0, p0, Lcom/twitter/android/settings/NotificationSettingsActivity;->c:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    .line 103
    new-instance v1, Lcom/twitter/util/a;

    iget-wide v2, p0, Lcom/twitter/android/settings/NotificationSettingsActivity;->G:J

    invoke-direct {v1, p0, v2, v3}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    invoke-virtual {v1}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v1

    const-string/jumbo v2, "launcher_icon_badge_enabled"

    .line 104
    invoke-virtual {v1, v2, v0}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Z)Lcom/twitter/util/a$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/util/a$a;->apply()V

    .line 105
    if-eqz v0, :cond_0

    .line 106
    invoke-static {p0}, Lcom/twitter/badge/LauncherIconBadgeUpdaterService;->a(Landroid/content/Context;)V

    .line 110
    :goto_0
    return-void

    .line 108
    :cond_0
    invoke-static {p0}, Lcom/twitter/badge/LauncherIconBadgeUpdaterService;->b(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/service/s;I)V
    .locals 2

    .prologue
    .line 200
    invoke-super {p0, p1, p2}, Lcom/twitter/android/settings/BaseAccountSettingsActivity;->a(Lcom/twitter/library/service/s;I)V

    .line 201
    invoke-virtual {p0}, Lcom/twitter/android/settings/NotificationSettingsActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 219
    :cond_0
    :goto_0
    return-void

    .line 204
    :cond_1
    if-eqz p2, :cond_2

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 205
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/settings/NotificationSettingsActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    .line 206
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 207
    if-nez v0, :cond_3

    .line 209
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Unexpected null userSettings, they should have been written as part of UserSettingsAPIRequest!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 212
    :cond_3
    invoke-direct {p0, v0}, Lcom/twitter/android/settings/NotificationSettingsActivity;->a(Lcom/twitter/model/account/UserSettings;)V

    goto :goto_0

    .line 216
    :cond_4
    const v0, 0x7f0a03ce

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/NotificationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 60
    invoke-super {p0, p1}, Lcom/twitter/android/settings/BaseAccountSettingsActivity;->onCreate(Landroid/os/Bundle;)V

    .line 62
    const v0, 0x7f0a086e

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/NotificationSettingsActivity;->setTitle(I)V

    .line 64
    const v0, 0x7f080016

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/NotificationSettingsActivity;->addPreferencesFromResource(I)V

    .line 66
    sget-object v2, Lcom/twitter/android/settings/NotificationSettingsActivity;->b:[Ljava/lang/String;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 67
    invoke-virtual {p0, v4}, Lcom/twitter/android/settings/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 66
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 70
    :cond_0
    const-string/jumbo v0, "notifications_follow_only"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/twitter/android/settings/NotificationSettingsActivity;->e:Landroid/preference/CheckBoxPreference;

    .line 71
    const-string/jumbo v0, "quality_filter"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/twitter/android/settings/NotificationSettingsActivity;->f:Landroid/preference/CheckBoxPreference;

    .line 73
    invoke-static {}, Lbph;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 74
    const-string/jumbo v0, "muted_keywords"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/NotificationSettingsActivity;->b(Ljava/lang/String;)Z

    .line 78
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/settings/NotificationSettingsActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    .line 79
    if-eqz v0, :cond_2

    .line 80
    invoke-direct {p0, v0}, Lcom/twitter/android/settings/NotificationSettingsActivity;->a(Lcom/twitter/model/account/UserSettings;)V

    .line 83
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/settings/NotificationSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "sync_settings"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 85
    if-eqz v0, :cond_3

    if-nez p1, :cond_3

    .line 86
    invoke-virtual {p0}, Lcom/twitter/android/settings/NotificationSettingsActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-static {p0, v0}, Lbbg;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lbbg;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Lcom/twitter/android/settings/NotificationSettingsActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 89
    :cond_3
    const-string/jumbo v0, "launcher_icon_badge_enabled"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/twitter/android/settings/NotificationSettingsActivity;->c:Landroid/preference/CheckBoxPreference;

    .line 90
    iget-object v0, p0, Lcom/twitter/android/settings/NotificationSettingsActivity;->c:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/twitter/util/a;

    iget-wide v2, p0, Lcom/twitter/android/settings/NotificationSettingsActivity;->G:J

    invoke-direct {v1, p0, v2, v3}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    const-string/jumbo v2, "launcher_icon_badge_enabled"

    .line 91
    invoke-virtual {v1, v2, v5}, Lcom/twitter/util/a;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 90
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 92
    iget-object v0, p0, Lcom/twitter/android/settings/NotificationSettingsActivity;->c:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 93
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 149
    invoke-virtual {p0}, Lcom/twitter/android/settings/NotificationSettingsActivity;->k()Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 150
    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v4

    .line 151
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    .line 152
    if-eqz v5, :cond_0

    if-nez v4, :cond_1

    .line 183
    :cond_0
    :goto_0
    return v0

    .line 155
    :cond_1
    const/4 v2, -0x1

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_2
    move v0, v2

    :goto_1
    packed-switch v0, :pswitch_data_0

    :goto_2
    move v0, v1

    .line 183
    goto :goto_0

    .line 155
    :sswitch_0
    const-string/jumbo v6, "notifications_follow_only"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto :goto_1

    :sswitch_1
    const-string/jumbo v0, "quality_filter"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_1

    :sswitch_2
    const-string/jumbo v0, "launcher_icon_badge_enabled"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    goto :goto_1

    .line 157
    :pswitch_0
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 158
    if-eqz v2, :cond_3

    const-string/jumbo v0, "following"

    :goto_3
    iput-object v0, v4, Lcom/twitter/model/account/UserSettings;->A:Ljava/lang/String;

    .line 160
    invoke-direct {p0, v3, v4}, Lcom/twitter/android/settings/NotificationSettingsActivity;->a(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;)V

    .line 161
    const-string/jumbo v0, "settings:notifications_timeline:notifications_timeline_settings:following_filter_enabled:"

    invoke-direct {p0, v0, v2}, Lcom/twitter/android/settings/NotificationSettingsActivity;->a(Ljava/lang/String;Z)V

    goto :goto_2

    .line 158
    :cond_3
    const-string/jumbo v0, "unfiltered"

    goto :goto_3

    .line 166
    :pswitch_1
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 167
    if-eqz v2, :cond_4

    const-string/jumbo v0, "enabled"

    :goto_4
    iput-object v0, v4, Lcom/twitter/model/account/UserSettings;->z:Ljava/lang/String;

    .line 170
    invoke-direct {p0, v3, v4}, Lcom/twitter/android/settings/NotificationSettingsActivity;->a(Lcom/twitter/library/client/Session;Lcom/twitter/model/account/UserSettings;)V

    .line 171
    const-string/jumbo v0, "settings:notifications_timeline:notifications_timeline_settings:quality_filter_enabled:"

    invoke-direct {p0, v0, v2}, Lcom/twitter/android/settings/NotificationSettingsActivity;->a(Ljava/lang/String;Z)V

    goto :goto_2

    .line 167
    :cond_4
    const-string/jumbo v0, "disabled"

    goto :goto_4

    .line 176
    :pswitch_2
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/twitter/android/settings/NotificationSettingsActivity;->b(Z)V

    goto :goto_2

    .line 155
    nop

    :sswitch_data_0
    .sparse-switch
        -0x3f3a6ba2 -> :sswitch_2
        0x3f9bbab8 -> :sswitch_1
        0x5b6b1aa3 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 114
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    .line 115
    if-nez v3, :cond_0

    .line 138
    :goto_0
    return v0

    .line 119
    :cond_0
    const/4 v2, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 121
    :pswitch_0
    invoke-static {p0}, Lcom/twitter/app/safety/mutedkeywords/list/MutedKeywordsListActivity$b;->a(Landroid/app/Activity;)Lcom/twitter/app/safety/mutedkeywords/list/MutedKeywordsListActivity$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/safety/mutedkeywords/list/MutedKeywordsListActivity$b;->a()V

    move v0, v1

    .line 122
    goto :goto_0

    .line 119
    :sswitch_0
    const-string/jumbo v4, "muted_keywords"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v2, v0

    goto :goto_1

    :sswitch_1
    const-string/jumbo v4, "privacy_and_safety"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v2, v1

    goto :goto_1

    :sswitch_2
    const-string/jumbo v4, "mobile_notifications"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x2

    goto :goto_1

    :sswitch_3
    const-string/jumbo v4, "advanced"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x3

    goto :goto_1

    .line 125
    :pswitch_1
    const-class v0, Lcom/twitter/android/settings/PrivacyAndContentActivity;

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/NotificationSettingsActivity;->a(Ljava/lang/Class;)V

    move v0, v1

    .line 126
    goto :goto_0

    .line 129
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/settings/MobileNotificationsActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "NotificationSettingsActivity_account_name"

    iget-object v3, p0, Lcom/twitter/android/settings/NotificationSettingsActivity;->a:Ljava/lang/String;

    .line 130
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 129
    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/NotificationSettingsActivity;->startActivity(Landroid/content/Intent;)V

    move v0, v1

    .line 131
    goto :goto_0

    .line 134
    :pswitch_3
    const-class v0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/NotificationSettingsActivity;->a(Ljava/lang/Class;)V

    move v0, v1

    .line 135
    goto :goto_0

    .line 119
    nop

    :sswitch_data_0
    .sparse-switch
        -0x2ad897de -> :sswitch_3
        -0x2aa1a0af -> :sswitch_1
        -0x2373cbf5 -> :sswitch_2
        -0x6b1b802 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 97
    invoke-super {p0}, Lcom/twitter/android/settings/BaseAccountSettingsActivity;->onStop()V

    .line 98
    invoke-virtual {p0}, Lcom/twitter/android/settings/NotificationSettingsActivity;->a()V

    .line 99
    return-void
.end method
