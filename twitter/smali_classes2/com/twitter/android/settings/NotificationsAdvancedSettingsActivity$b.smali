.class Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;

.field private final b:J

.field private final c:Ljava/lang/String;

.field private d:Z

.field private e:Z


# direct methods
.method constructor <init>(Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;->a:Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 149
    iput-wide p2, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;->b:J

    .line 150
    iput-object p4, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;->c:Ljava/lang/String;

    .line 151
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 163
    iget-object v0, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;->a:Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    .line 164
    iget-object v0, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/util/b;->b(Ljava/lang/String;)Lakm;

    move-result-object v3

    .line 165
    if-nez v3, :cond_0

    .line 204
    :goto_0
    return-object v12

    .line 169
    :cond_0
    iget-boolean v9, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;->d:Z

    .line 170
    iget-boolean v10, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;->e:Z

    .line 172
    iget-object v0, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;->a:Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;

    const-string/jumbo v4, "polling_interval"

    .line 173
    invoke-virtual {v0, v4}, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 172
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    .line 174
    iget-object v0, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;->a:Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->e(Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;)I

    move-result v0

    if-eq v0, v11, :cond_4

    move v7, v1

    .line 176
    :goto_1
    if-eqz v10, :cond_1

    .line 177
    sget-object v0, Lcom/twitter/database/schema/a;->c:Ljava/lang/String;

    invoke-static {v3, v0, v9}, Lcom/twitter/library/util/b;->a(Lakm;Ljava/lang/String;Z)V

    .line 179
    if-eqz v9, :cond_5

    .line 180
    const-string/jumbo v0, "settings::::enable_sync"

    .line 184
    :goto_2
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v4, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;->a:Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;

    invoke-static {v4}, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->f(Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;)J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v1, [Ljava/lang/String;

    aput-object v0, v1, v2

    invoke-virtual {v3, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 188
    :cond_1
    new-instance v4, Landroid/content/ContentValues;

    const/4 v0, 0x4

    invoke-direct {v4, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 189
    const-string/jumbo v0, "interval"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 192
    new-instance v6, Laut;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {v6, v0}, Laut;-><init>(Landroid/content/ContentResolver;)V

    .line 193
    invoke-static {}, Lcom/twitter/library/provider/j;->c()Lcom/twitter/library/provider/j;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;->b:J

    iget-object v0, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;->a:Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->b(Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;)Z

    move-result v5

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/j;->a(JLandroid/content/ContentValues;ZLaut;)I

    .line 194
    invoke-virtual {v6}, Laut;->a()V

    .line 197
    if-nez v7, :cond_2

    if-eqz v10, :cond_3

    :cond_2
    if-eqz v9, :cond_3

    .line 198
    invoke-static {v8}, Lcom/twitter/library/platform/TwitterDataSyncService;->a(Landroid/content/Context;)V

    .line 201
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;->a:Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;

    invoke-static {v0, v9}, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->a(Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;Z)Z

    .line 202
    iget-object v0, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;->a:Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;

    invoke-static {v0, v11}, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->a(Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;I)I

    goto/16 :goto_0

    :cond_4
    move v7, v2

    .line 174
    goto :goto_1

    .line 182
    :cond_5
    const-string/jumbo v0, "settings::::disable_sync"

    goto :goto_2
.end method

.method protected a(Ljava/lang/Void;)V
    .locals 0

    .prologue
    .line 209
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 142
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 142
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;->a(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 155
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 156
    iget-object v0, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;->a:Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->a(Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;->d:Z

    .line 157
    iget-object v0, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;->a:Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;->d(Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity;)Z

    move-result v0

    iget-boolean v1, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;->d:Z

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/settings/NotificationsAdvancedSettingsActivity$b;->e:Z

    .line 158
    return-void

    .line 157
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
