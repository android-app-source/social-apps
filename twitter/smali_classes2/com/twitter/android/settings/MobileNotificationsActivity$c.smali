.class Lcom/twitter/android/settings/MobileNotificationsActivity$c;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/settings/MobileNotificationsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/settings/MobileNotificationsActivity;

.field private final b:J

.field private c:Z

.field private d:Z

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:I

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method constructor <init>(Lcom/twitter/android/settings/MobileNotificationsActivity;J)V
    .locals 0

    .prologue
    .line 738
    iput-object p1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 739
    iput-wide p2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->b:J

    .line 740
    return-void
.end method

.method private a(Landroid/preference/Preference;)I
    .locals 1

    .prologue
    .line 846
    if-eqz p1, :cond_0

    check-cast p1, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(ILjava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1041
    packed-switch p1, :pswitch_data_0

    .line 1056
    :goto_0
    return-void

    .line 1043
    :pswitch_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v1, v1, Lcom/twitter/android/settings/MobileNotificationsActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "settings"

    aput-object v2, v1, v4

    const-string/jumbo v2, "notifications"

    aput-object v2, v1, v5

    aput-object p2, v1, v6

    const/4 v2, 0x0

    aput-object v2, v1, v7

    const-string/jumbo v2, "deselect"

    aput-object v2, v1, v8

    .line 1044
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1043
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 1048
    :pswitch_1
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v1, v1, Lcom/twitter/android/settings/MobileNotificationsActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "settings"

    aput-object v2, v1, v4

    const-string/jumbo v2, "notifications"

    aput-object v2, v1, v5

    aput-object p2, v1, v6

    const/4 v2, 0x0

    aput-object v2, v1, v7

    const-string/jumbo v2, "select"

    aput-object v2, v1, v8

    .line 1049
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1048
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 1041
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b(Landroid/preference/Preference;)I
    .locals 1

    .prologue
    .line 851
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->e:Z

    if-eqz v0, :cond_0

    .line 852
    check-cast p1, Landroid/preference/ListPreference;

    invoke-virtual {p1}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 854
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(Landroid/preference/Preference;)I

    move-result v0

    goto :goto_0
.end method

.method private b(ILjava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1071
    packed-switch p1, :pswitch_data_0

    .line 1091
    :goto_0
    return-void

    .line 1073
    :pswitch_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v1, v1, Lcom/twitter/android/settings/MobileNotificationsActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "settings"

    aput-object v2, v1, v4

    const-string/jumbo v2, "notifications"

    aput-object v2, v1, v5

    aput-object p2, v1, v6

    const/4 v2, 0x0

    aput-object v2, v1, v7

    const-string/jumbo v2, "deselect"

    aput-object v2, v1, v8

    .line 1074
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1073
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 1078
    :pswitch_1
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v1, v1, Lcom/twitter/android/settings/MobileNotificationsActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "settings"

    aput-object v2, v1, v4

    const-string/jumbo v2, "notifications"

    aput-object v2, v1, v5

    aput-object p2, v1, v6

    const-string/jumbo v2, "from_people_you_follow"

    aput-object v2, v1, v7

    const-string/jumbo v2, "select"

    aput-object v2, v1, v8

    .line 1079
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1078
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 1083
    :pswitch_2
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v1, v1, Lcom/twitter/android/settings/MobileNotificationsActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "settings"

    aput-object v2, v1, v4

    const-string/jumbo v2, "notifications"

    aput-object v2, v1, v5

    aput-object p2, v1, v6

    const-string/jumbo v2, "from_anyone"

    aput-object v2, v1, v7

    const-string/jumbo v2, "select"

    aput-object v2, v1, v8

    .line 1084
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1083
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 1071
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 32

    .prologue
    .line 864
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->c:Z

    if-nez v4, :cond_0

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->d:Z

    if-nez v4, :cond_0

    .line 865
    const/4 v4, 0x0

    .line 1017
    :goto_0
    return-object v4

    .line 867
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    move-object/from16 v28, v0

    .line 870
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-boolean v0, v4, Lcom/twitter/android/settings/MobileNotificationsActivity;->e:Z

    move/from16 v25, v0

    .line 871
    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->e:I

    .line 872
    move-object/from16 v0, p0

    iget v9, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->j:I

    .line 873
    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->f:I

    .line 874
    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->g:I

    .line 875
    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->h:I

    .line 876
    move-object/from16 v0, p0

    iget v8, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->i:I

    .line 877
    move-object/from16 v0, p0

    iget v10, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->k:I

    .line 878
    move-object/from16 v0, p0

    iget v11, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->l:I

    .line 879
    move-object/from16 v0, p0

    iget v12, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->m:I

    .line 880
    move-object/from16 v0, p0

    iget v13, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->n:I

    .line 881
    move-object/from16 v0, p0

    iget v14, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->o:I

    .line 882
    move-object/from16 v0, p0

    iget v15, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->p:I

    .line 883
    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->r:I

    move/from16 v16, v0

    .line 884
    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->s:I

    move/from16 v17, v0

    .line 885
    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->q:I

    move/from16 v18, v0

    .line 886
    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->t:I

    move/from16 v19, v0

    .line 887
    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->u:I

    move/from16 v20, v0

    .line 888
    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->v:I

    move/from16 v21, v0

    .line 891
    const/16 v27, 0x0

    .line 892
    const/16 v24, 0x0

    .line 893
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->b:Lcom/twitter/library/client/Session;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v22

    move-object/from16 v0, v28

    move-wide/from16 v1, v22

    invoke-static {v0, v1, v2}, Lbaw;->a(Landroid/content/Context;J)Z

    move-result v22

    .line 894
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->c:Z

    move/from16 v23, v0

    if-eqz v23, :cond_18

    .line 897
    if-nez v25, :cond_15

    .line 898
    const/16 v23, 0x1

    .line 915
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->d:Z

    move/from16 v24, v0

    if-nez v24, :cond_1

    if-eqz v23, :cond_2

    .line 916
    :cond_1
    new-instance v23, Lcom/twitter/library/provider/n$a;

    invoke-direct/range {v23 .. v23}, Lcom/twitter/library/provider/n$a;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->f:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 917
    invoke-virtual/range {v23 .. v24}, Lcom/twitter/library/provider/n$a;->a(Ljava/lang/String;)Lcom/twitter/library/provider/n$a;

    move-result-object v23

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->y:Z

    move/from16 v24, v0

    .line 918
    invoke-virtual/range {v23 .. v24}, Lcom/twitter/library/provider/n$a;->a(Z)Lcom/twitter/library/provider/n$a;

    move-result-object v23

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->z:Z

    move/from16 v24, v0

    .line 919
    invoke-virtual/range {v23 .. v24}, Lcom/twitter/library/provider/n$a;->b(Z)Lcom/twitter/library/provider/n$a;

    move-result-object v23

    .line 920
    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Lcom/twitter/library/provider/n$a;->k(I)Lcom/twitter/library/provider/n$a;

    move-result-object v23

    .line 921
    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Lcom/twitter/library/provider/n$a;->l(I)Lcom/twitter/library/provider/n$a;

    move-result-object v23

    .line 922
    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Lcom/twitter/library/provider/n$a;->c(I)Lcom/twitter/library/provider/n$a;

    move-result-object v23

    .line 923
    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Lcom/twitter/library/provider/n$a;->d(I)Lcom/twitter/library/provider/n$a;

    move-result-object v23

    .line 924
    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Lcom/twitter/library/provider/n$a;->e(I)Lcom/twitter/library/provider/n$a;

    move-result-object v23

    .line 925
    move-object/from16 v0, v23

    invoke-virtual {v0, v8}, Lcom/twitter/library/provider/n$a;->f(I)Lcom/twitter/library/provider/n$a;

    move-result-object v23

    .line 926
    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lcom/twitter/library/provider/n$a;->g(I)Lcom/twitter/library/provider/n$a;

    move-result-object v23

    .line 927
    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Lcom/twitter/library/provider/n$a;->m(I)Lcom/twitter/library/provider/n$a;

    move-result-object v23

    .line 928
    move-object/from16 v0, v23

    invoke-virtual {v0, v12}, Lcom/twitter/library/provider/n$a;->n(I)Lcom/twitter/library/provider/n$a;

    move-result-object v23

    .line 929
    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Lcom/twitter/library/provider/n$a;->o(I)Lcom/twitter/library/provider/n$a;

    move-result-object v23

    .line 930
    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Lcom/twitter/library/provider/n$a;->p(I)Lcom/twitter/library/provider/n$a;

    move-result-object v23

    .line 931
    move-object/from16 v0, v23

    invoke-virtual {v0, v15}, Lcom/twitter/library/provider/n$a;->q(I)Lcom/twitter/library/provider/n$a;

    move-result-object v23

    .line 932
    move-object/from16 v0, v23

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/n$a;->h(I)Lcom/twitter/library/provider/n$a;

    move-result-object v16

    .line 933
    invoke-virtual/range {v16 .. v17}, Lcom/twitter/library/provider/n$a;->i(I)Lcom/twitter/library/provider/n$a;

    move-result-object v16

    .line 934
    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/n$a;->j(I)Lcom/twitter/library/provider/n$a;

    move-result-object v16

    .line 935
    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/n$a;->r(I)Lcom/twitter/library/provider/n$a;

    move-result-object v16

    .line 936
    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/n$a;->s(I)Lcom/twitter/library/provider/n$a;

    move-result-object v16

    .line 937
    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/n$a;->t(I)Lcom/twitter/library/provider/n$a;

    move-result-object v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->w:I

    move/from16 v17, v0

    .line 938
    invoke-virtual/range {v16 .. v17}, Lcom/twitter/library/provider/n$a;->u(I)Lcom/twitter/library/provider/n$a;

    move-result-object v16

    .line 939
    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/n$a;->c(Z)Lcom/twitter/library/provider/n$a;

    move-result-object v16

    .line 940
    invoke-virtual/range {v16 .. v16}, Lcom/twitter/library/provider/n$a;->a()Lcom/twitter/library/provider/n;

    move-result-object v24

    .line 941
    new-instance v26, Laut;

    invoke-virtual/range {v28 .. v28}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v16

    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Laut;-><init>(Landroid/content/ContentResolver;)V

    .line 942
    invoke-static {}, Lcom/twitter/library/provider/j;->c()Lcom/twitter/library/provider/j;

    move-result-object v21

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->b:J

    move-wide/from16 v22, v0

    invoke-virtual/range {v21 .. v26}, Lcom/twitter/library/provider/j;->a(JLcom/twitter/library/provider/n;ZLaut;)I

    .line 943
    invoke-virtual/range {v26 .. v26}, Laut;->a()V

    .line 946
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/twitter/library/provider/n;->e:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-eq v0, v5, :cond_3

    .line 947
    const-string/jumbo v16, "mention"

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v5, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->b(ILjava/lang/String;)V

    .line 949
    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v5, v5, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v5, v5, Lcom/twitter/library/provider/n;->n:I

    if-eq v5, v9, :cond_4

    .line 950
    const-string/jumbo v5, "message"

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v5}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(ILjava/lang/String;)V

    .line 952
    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v5, v5, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v5, v5, Lcom/twitter/library/provider/n;->m:I

    if-eq v5, v4, :cond_5

    .line 953
    const-string/jumbo v5, "tweet"

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(ILjava/lang/String;)V

    .line 955
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v4, v4, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v4, v4, Lcom/twitter/library/provider/n;->f:I

    if-eq v4, v6, :cond_6

    .line 956
    const-string/jumbo v4, "retweet"

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->b(ILjava/lang/String;)V

    .line 958
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v4, v4, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v4, v4, Lcom/twitter/library/provider/n;->g:I

    if-eq v4, v7, :cond_7

    .line 959
    const-string/jumbo v4, "favorite"

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v4}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->b(ILjava/lang/String;)V

    .line 961
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v4, v4, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v4, v4, Lcom/twitter/library/provider/n;->h:I

    if-eq v4, v8, :cond_8

    .line 962
    const-string/jumbo v4, "follow"

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v4}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(ILjava/lang/String;)V

    .line 964
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v4, v4, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v4, v4, Lcom/twitter/library/provider/n;->i:I

    if-eq v4, v10, :cond_9

    .line 965
    const-string/jumbo v4, "address_book"

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v4}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(ILjava/lang/String;)V

    .line 967
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v4, v4, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v4, v4, Lcom/twitter/library/provider/n;->o:I

    if-eq v4, v11, :cond_a

    .line 968
    const-string/jumbo v4, "experimental"

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v4}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(ILjava/lang/String;)V

    .line 970
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v4, v4, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v4, v4, Lcom/twitter/library/provider/n;->p:I

    if-eq v4, v12, :cond_b

    .line 971
    const-string/jumbo v4, "lifeline_alert"

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v4}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(ILjava/lang/String;)V

    .line 973
    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v4, v4, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v4, v4, Lcom/twitter/library/provider/n;->q:I

    if-eq v4, v13, :cond_c

    .line 974
    const-string/jumbo v4, "recommendation"

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v4}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->b(ILjava/lang/String;)V

    .line 976
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v4, v4, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v4, v4, Lcom/twitter/library/provider/n;->r:I

    if-eq v4, v14, :cond_d

    .line 977
    const-string/jumbo v4, "news"

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v4}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(ILjava/lang/String;)V

    .line 979
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v4, v4, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v4, v4, Lcom/twitter/library/provider/n;->s:I

    if-eq v4, v15, :cond_e

    .line 980
    const-string/jumbo v4, "vit_notable_event"

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v4}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(ILjava/lang/String;)V

    .line 982
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v4, v4, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v4, v4, Lcom/twitter/library/provider/n;->l:I

    move/from16 v0, v18

    if-eq v4, v0, :cond_f

    .line 983
    const-string/jumbo v4, "followed_verified"

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1, v4}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(ILjava/lang/String;)V

    .line 985
    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v4, v4, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v4, v4, Lcom/twitter/library/provider/n;->t:I

    move/from16 v0, v19

    if-eq v4, v0, :cond_10

    .line 986
    const-string/jumbo v4, "offer_redemption"

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1, v4}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(ILjava/lang/String;)V

    .line 988
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v4, v4, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v4, v4, Lcom/twitter/library/provider/n;->u:I

    move/from16 v0, v20

    if-eq v4, v0, :cond_1c

    .line 989
    const-string/jumbo v4, "highlights"

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1, v4}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(ILjava/lang/String;)V

    .line 990
    const/4 v4, 0x1

    .line 992
    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v5, v5, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v5, v5, Lcom/twitter/library/provider/n;->w:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->w:I

    if-eq v5, v6, :cond_11

    .line 993
    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->w:I

    const-string/jumbo v6, "photo_tags"

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(ILjava/lang/String;)V

    .line 995
    :cond_11
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-boolean v5, v5, Lcom/twitter/android/settings/MobileNotificationsActivity;->n:Z

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->x:Z

    if-eq v5, v6, :cond_13

    .line 996
    const/4 v5, 0x1

    move/from16 v0, v20

    if-ne v0, v5, :cond_12

    .line 997
    const/4 v4, 0x1

    .line 999
    :cond_12
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->x:Z

    if-eqz v5, :cond_19

    .line 1000
    new-instance v5, Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v6, v6, Lcom/twitter/android/settings/MobileNotificationsActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string/jumbo v8, "settings"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string/jumbo v8, "notifications"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const/4 v8, 0x0

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const/4 v8, 0x0

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const-string/jumbo v8, "enable_notifications"

    aput-object v8, v6, v7

    .line 1001
    invoke-virtual {v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v5

    .line 1000
    invoke-static {v5}, Lcpm;->a(Lcpk;)V

    .line 1007
    :cond_13
    :goto_3
    if-eqz v4, :cond_14

    .line 1008
    const/4 v4, 0x1

    move/from16 v0, v20

    if-ne v0, v4, :cond_1a

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->x:Z

    if-eqz v4, :cond_1a

    const/4 v4, 0x1

    .line 1013
    :goto_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "settings:notifications:highlights::"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz v4, :cond_1b

    const-string/jumbo v4, "opt_in"

    :goto_5
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1015
    new-instance v5, Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v6, v6, Lcom/twitter/android/settings/MobileNotificationsActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    invoke-virtual {v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v4

    invoke-static {v4}, Lcpm;->a(Lcpk;)V

    .line 1017
    :cond_14
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 899
    :cond_15
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->x:Z

    move/from16 v23, v0

    if-nez v23, :cond_16

    if-nez v22, :cond_16

    .line 901
    const/16 v23, 0x1

    .line 902
    invoke-static {}, Lbuu;->a()Lbuu;

    move-result-object v24

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->b:J

    move-wide/from16 v30, v0

    move-object/from16 v0, v24

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Lbuu;->b(J)V

    goto/16 :goto_1

    .line 903
    :cond_16
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->x:Z

    move/from16 v23, v0

    if-nez v23, :cond_17

    .line 905
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->b:J

    move-wide/from16 v30, v0

    const/16 v23, 0x400

    move-object/from16 v0, v28

    move-wide/from16 v1, v30

    move/from16 v3, v23

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/platform/notifications/PushRegistration;->a(Landroid/content/Context;JI)V

    move/from16 v23, v24

    goto/16 :goto_1

    .line 908
    :cond_17
    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->w:I

    move/from16 v23, v0

    invoke-static/range {v4 .. v23}, Lcom/twitter/library/provider/NotificationSetting;->a(IIIIIIIIIIIIIIIIIIZI)I

    move-result v23

    .line 912
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->b:J

    move-wide/from16 v30, v0

    move-object/from16 v0, v28

    move-wide/from16 v1, v30

    move/from16 v3, v23

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/platform/notifications/PushRegistration;->a(Landroid/content/Context;JI)V

    :cond_18
    move/from16 v23, v24

    goto/16 :goto_1

    .line 1003
    :cond_19
    new-instance v5, Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v6, v6, Lcom/twitter/android/settings/MobileNotificationsActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string/jumbo v8, "settings"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string/jumbo v8, "notifications"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const/4 v8, 0x0

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const/4 v8, 0x0

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const-string/jumbo v8, "disable_notifications"

    aput-object v8, v6, v7

    .line 1004
    invoke-virtual {v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v5

    .line 1003
    invoke-static {v5}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_3

    .line 1008
    :cond_1a
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 1013
    :cond_1b
    const-string/jumbo v4, "opt_out"

    goto/16 :goto_5

    :cond_1c
    move/from16 v4, v27

    goto/16 :goto_2
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 711
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected onPreExecute()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 744
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 745
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->e:Z

    if-eqz v0, :cond_5

    .line 746
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    invoke-static {v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->d(Lcom/twitter/android/settings/MobileNotificationsActivity;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->x:Z

    .line 747
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->g:Z

    if-eqz v0, :cond_2

    .line 748
    iput v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->e:I

    .line 753
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->v:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->l:I

    .line 754
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->u:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->k:I

    .line 755
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->y:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->n:I

    .line 756
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->z:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->o:I

    .line 757
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->i:Z

    if-eqz v0, :cond_3

    .line 758
    iput v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->r:I

    .line 762
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->k:Z

    if-eqz v0, :cond_4

    .line 763
    iput v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->s:I

    .line 770
    :goto_2
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->l:Z

    if-eqz v0, :cond_8

    .line 771
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->F:Landroid/preference/ListPreference;

    invoke-direct {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->b(Landroid/preference/Preference;)I

    move-result v0

    .line 772
    if-ne v0, v2, :cond_6

    .line 773
    iput v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->q:I

    .line 774
    iput v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->i:I

    .line 785
    :goto_3
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->w:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->j:I

    .line 786
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->q:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->b(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->f:I

    .line 787
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->x:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->m:I

    .line 788
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->y:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->n:I

    .line 789
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->z:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->o:I

    .line 790
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->A:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->p:I

    .line 791
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->B:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->t:I

    .line 792
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->C:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->u:I

    .line 793
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->D:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->v:I

    .line 794
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->r:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->b(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->g:I

    .line 795
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->s:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->b(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->h:I

    .line 796
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->E:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->w:I

    .line 799
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->e:Z

    if-eqz v0, :cond_0

    .line 800
    iget v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->g:I

    if-eqz v0, :cond_9

    .line 801
    iget v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->l:I

    iput v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->r:I

    .line 805
    :goto_4
    iget v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->h:I

    if-eqz v0, :cond_a

    .line 806
    iget v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->l:I

    iput v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->s:I

    .line 811
    :cond_0
    :goto_5
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->n:Z

    iget-boolean v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->x:Z

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v0, v0, Lcom/twitter/library/provider/n;->e:I

    iget v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->f:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v0, v0, Lcom/twitter/library/provider/n;->n:I

    iget v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->j:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v0, v0, Lcom/twitter/library/provider/n;->f:I

    iget v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->g:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v0, v0, Lcom/twitter/library/provider/n;->g:I

    iget v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->h:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v0, v0, Lcom/twitter/library/provider/n;->h:I

    iget v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->i:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v0, v0, Lcom/twitter/library/provider/n;->m:I

    iget v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->e:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v0, v0, Lcom/twitter/library/provider/n;->i:I

    iget v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->k:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v0, v0, Lcom/twitter/library/provider/n;->o:I

    iget v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->l:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v0, v0, Lcom/twitter/library/provider/n;->p:I

    iget v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->m:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v0, v0, Lcom/twitter/library/provider/n;->q:I

    iget v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->n:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v0, v0, Lcom/twitter/library/provider/n;->r:I

    iget v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->o:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v0, v0, Lcom/twitter/library/provider/n;->s:I

    iget v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->p:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v0, v0, Lcom/twitter/library/provider/n;->l:I

    iget v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->q:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v0, v0, Lcom/twitter/library/provider/n;->j:I

    iget v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->r:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v0, v0, Lcom/twitter/library/provider/n;->k:I

    iget v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->s:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v0, v0, Lcom/twitter/library/provider/n;->t:I

    iget v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->t:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v0, v0, Lcom/twitter/library/provider/n;->u:I

    iget v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->u:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v0, v0, Lcom/twitter/library/provider/n;->v:I

    iget v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->v:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v0, v0, Lcom/twitter/library/provider/n;->w:I

    iget v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->w:I

    if-eq v0, v3, :cond_b

    :cond_1
    move v0, v2

    :goto_6
    iput-boolean v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->c:Z

    .line 831
    invoke-static {}, Lcom/twitter/android/settings/MobileNotificationsActivity;->i()[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_7
    if-ge v0, v4, :cond_d

    aget-object v5, v3, v0

    .line 832
    iget-object v6, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    invoke-virtual {v6, v5}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    if-nez v6, :cond_c

    .line 833
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "NotificationSettingsActivity preference (key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") was unexpectedly null in WriteAccountUserTask.onPreExecute()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 843
    :goto_8
    return-void

    .line 750
    :cond_2
    iput v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->e:I

    goto/16 :goto_0

    .line 760
    :cond_3
    iput v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->r:I

    goto/16 :goto_1

    .line 765
    :cond_4
    iput v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->s:I

    goto/16 :goto_2

    .line 768
    :cond_5
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->p:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->e:I

    goto/16 :goto_2

    .line 775
    :cond_6
    const/4 v3, 0x2

    if-ne v0, v3, :cond_7

    .line 776
    iput v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->q:I

    .line 777
    iput v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->i:I

    goto/16 :goto_3

    .line 779
    :cond_7
    iput v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->q:I

    .line 780
    iput v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->i:I

    goto/16 :goto_3

    .line 783
    :cond_8
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->t:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->i:I

    goto/16 :goto_3

    .line 803
    :cond_9
    iput v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->r:I

    goto/16 :goto_4

    .line 808
    :cond_a
    iput v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->s:I

    goto/16 :goto_5

    :cond_b
    move v0, v1

    .line 811
    goto :goto_6

    .line 831
    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 838
    :cond_d
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    const-string/jumbo v3, "use_led"

    invoke-virtual {v0, v3}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->z:Z

    .line 839
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    const-string/jumbo v3, "vibrate"

    invoke-virtual {v0, v3}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->y:Z

    .line 840
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget-boolean v0, v0, Lcom/twitter/library/provider/n;->d:Z

    iget-boolean v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->z:Z

    if-ne v0, v3, :cond_e

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget-boolean v0, v0, Lcom/twitter/library/provider/n;->b:Z

    iget-boolean v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->y:Z

    if-ne v0, v3, :cond_e

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget-object v0, v0, Lcom/twitter/library/provider/n;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v3, v3, Lcom/twitter/android/settings/MobileNotificationsActivity;->f:Ljava/lang/String;

    .line 842
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    :cond_e
    move v1, v2

    :cond_f
    iput-boolean v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->d:Z

    goto/16 :goto_8
.end method
