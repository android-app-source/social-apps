.class Lcom/twitter/android/settings/TweetSettingsActivity$a;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/settings/TweetSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/settings/TweetSettingsActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/settings/TweetSettingsActivity;)V
    .locals 0

    .prologue
    .line 346
    iput-object p1, p0, Lcom/twitter/android/settings/TweetSettingsActivity$a;->a:Lcom/twitter/android/settings/TweetSettingsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/settings/TweetSettingsActivity;Lcom/twitter/android/settings/TweetSettingsActivity$1;)V
    .locals 0

    .prologue
    .line 346
    invoke-direct {p0, p1}, Lcom/twitter/android/settings/TweetSettingsActivity$a;-><init>(Lcom/twitter/android/settings/TweetSettingsActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 349
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity$a;->a:Lcom/twitter/android/settings/TweetSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/settings/TweetSettingsActivity;->a(Lcom/twitter/android/settings/TweetSettingsActivity;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/twitter/library/platform/notifications/PushRegistration;->a(J)I

    move-result v0

    const/16 v1, 0x200

    invoke-static {v0, v1}, Lcga;->b(II)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 354
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity$a;->a:Lcom/twitter/android/settings/TweetSettingsActivity;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/android/settings/TweetSettingsActivity;->a(Lcom/twitter/android/settings/TweetSettingsActivity;Z)V

    .line 355
    iget-object v0, p0, Lcom/twitter/android/settings/TweetSettingsActivity$a;->a:Lcom/twitter/android/settings/TweetSettingsActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/android/settings/TweetSettingsActivity;->b(Lcom/twitter/android/settings/TweetSettingsActivity;Z)V

    .line 356
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 346
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/settings/TweetSettingsActivity$a;->a([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 346
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/twitter/android/settings/TweetSettingsActivity$a;->a(Ljava/lang/Boolean;)V

    return-void
.end method
