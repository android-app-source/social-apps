.class Lcom/twitter/android/settings/MobileNotificationsActivity$a;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/settings/MobileNotificationsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/settings/MobileNotificationsActivity;

.field private final b:J

.field private c:Z

.field private d:Lcom/twitter/library/provider/n;


# direct methods
.method constructor <init>(Lcom/twitter/android/settings/MobileNotificationsActivity;J)V
    .locals 0

    .prologue
    .line 586
    iput-object p1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 587
    iput-wide p2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->b:J

    .line 588
    return-void
.end method

.method private a(Landroid/preference/Preference;I)V
    .locals 1

    .prologue
    .line 692
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->e:Z

    if-eqz v0, :cond_0

    .line 693
    check-cast p1, Landroid/preference/ListPreference;

    .line 694
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 695
    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 696
    invoke-static {p1, v0}, Lcom/twitter/library/util/af;->a(Landroid/preference/ListPreference;Ljava/lang/String;)V

    .line 697
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 701
    :goto_0
    return-void

    .line 699
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->b(Landroid/preference/Preference;I)V

    goto :goto_0
.end method

.method private b(Landroid/preference/Preference;I)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 704
    check-cast p1, Landroid/preference/CheckBoxPreference;

    if-ne p2, v0, :cond_0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 705
    return-void

    .line 704
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 599
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->e:Z

    if-eqz v0, :cond_0

    .line 600
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-wide v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->b:J

    invoke-static {v0, v2, v3}, Lcom/twitter/library/platform/notifications/PushRegistration;->b(Landroid/content/Context;J)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->c:Z

    .line 603
    :cond_0
    invoke-static {}, Lcom/twitter/library/provider/j;->c()Lcom/twitter/library/provider/j;

    move-result-object v0

    .line 604
    iget-wide v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->b:J

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    invoke-static {v1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->a(Lcom/twitter/android/settings/MobileNotificationsActivity;)Z

    move-result v1

    invoke-virtual {v0, v2, v3, v1}, Lcom/twitter/library/provider/j;->a(JZ)Lcom/twitter/library/provider/n;

    move-result-object v1

    .line 606
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->b:J

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x5

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v4, "settings"

    aput-object v4, v3, v0

    const/4 v0, 0x1

    const-string/jumbo v4, "notifications"

    aput-object v4, v3, v0

    const/4 v0, 0x2

    const-string/jumbo v4, "read_settings"

    aput-object v4, v3, v0

    const/4 v0, 0x3

    aput-object v6, v3, v0

    const/4 v4, 0x4

    if-eqz v1, :cond_1

    const-string/jumbo v0, "success"

    :goto_0
    aput-object v0, v3, v4

    .line 607
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 606
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 609
    if-nez v1, :cond_2

    .line 613
    sget-object v0, Lcom/twitter/library/provider/n;->a:Lcom/twitter/library/provider/n;

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    .line 614
    const-string/jumbo v0, "Failed to find notification settings"

    invoke-static {v0}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    .line 618
    :goto_1
    return-object v6

    .line 606
    :cond_1
    const-string/jumbo v0, "failure"

    goto :goto_0

    .line 616
    :cond_2
    iput-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    goto :goto_1
.end method

.method protected a(Ljava/lang/Void;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 623
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 689
    :goto_0
    return-void

    .line 627
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 629
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    const-string/jumbo v4, "vibrate"

    invoke-virtual {v0, v4}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 630
    iget-object v4, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    invoke-static {v4}, Lcom/twitter/android/settings/MobileNotificationsActivity;->a(Lcom/twitter/android/settings/MobileNotificationsActivity;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 631
    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setSelectable(Z)V

    .line 632
    const v4, 0x7f0a089a

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setSummary(I)V

    .line 634
    :cond_1
    iget-object v4, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v5, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iput-object v5, v4, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    .line 635
    check-cast v0, Landroid/preference/CheckBoxPreference;

    iget-object v4, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget-boolean v4, v4, Lcom/twitter/library/provider/n;->b:Z

    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 638
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    const-string/jumbo v4, "ringtone"

    invoke-virtual {v0, v4}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 639
    iget-object v4, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget-object v4, v4, Lcom/twitter/library/provider/n;->c:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setDefaultValue(Ljava/lang/Object;)V

    .line 642
    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 643
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v3, "ringtone"

    iget-object v4, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget-object v4, v4, Lcom/twitter/library/provider/n;->c:Ljava/lang/String;

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 644
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v3, v3, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget-object v3, v3, Lcom/twitter/library/provider/n;->c:Ljava/lang/String;

    iput-object v3, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->f:Ljava/lang/String;

    .line 646
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    const-string/jumbo v3, "use_led"

    invoke-virtual {v0, v3}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 647
    check-cast v0, Landroid/preference/CheckBoxPreference;

    iget-object v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget-boolean v3, v3, Lcom/twitter/library/provider/n;->d:Z

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 648
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->q:Landroid/preference/Preference;

    iget-object v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget v3, v3, Lcom/twitter/library/provider/n;->e:I

    invoke-direct {p0, v0, v3}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a(Landroid/preference/Preference;I)V

    .line 649
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->l:Z

    if-eqz v0, :cond_5

    .line 650
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget v0, v0, Lcom/twitter/library/provider/n;->l:I

    if-lez v0, :cond_3

    .line 651
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->F:Landroid/preference/ListPreference;

    const/4 v3, 0x2

    invoke-direct {p0, v0, v3}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a(Landroid/preference/Preference;I)V

    .line 660
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->w:Landroid/preference/Preference;

    iget-object v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget v3, v3, Lcom/twitter/library/provider/n;->n:I

    invoke-direct {p0, v0, v3}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->b(Landroid/preference/Preference;I)V

    .line 661
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->e:Z

    if-eqz v0, :cond_7

    .line 662
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-boolean v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->c:Z

    iput-boolean v3, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->n:Z

    .line 664
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    invoke-static {v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->b(Lcom/twitter/android/settings/MobileNotificationsActivity;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 665
    iget-object v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget v0, v0, Lcom/twitter/library/provider/n;->m:I

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_2
    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    .line 666
    invoke-static {v1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->c(Lcom/twitter/android/settings/MobileNotificationsActivity;)I

    move-result v1

    .line 665
    invoke-virtual {v3, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->a(ZI)V

    .line 668
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->x:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget v1, v1, Lcom/twitter/library/provider/n;->p:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->b(Landroid/preference/Preference;I)V

    .line 669
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->u:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget v1, v1, Lcom/twitter/library/provider/n;->i:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->b(Landroid/preference/Preference;I)V

    .line 670
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->v:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget v1, v1, Lcom/twitter/library/provider/n;->o:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->b(Landroid/preference/Preference;I)V

    .line 671
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->y:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget v1, v1, Lcom/twitter/library/provider/n;->q:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->b(Landroid/preference/Preference;I)V

    .line 672
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->z:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget v1, v1, Lcom/twitter/library/provider/n;->r:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->b(Landroid/preference/Preference;I)V

    .line 673
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->A:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget v1, v1, Lcom/twitter/library/provider/n;->s:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->b(Landroid/preference/Preference;I)V

    .line 674
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->B:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget v1, v1, Lcom/twitter/library/provider/n;->t:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->b(Landroid/preference/Preference;I)V

    .line 675
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->C:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget v1, v1, Lcom/twitter/library/provider/n;->u:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->b(Landroid/preference/Preference;I)V

    .line 676
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->D:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget v1, v1, Lcom/twitter/library/provider/n;->v:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->b(Landroid/preference/Preference;I)V

    .line 677
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->r:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget v1, v1, Lcom/twitter/library/provider/n;->f:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a(Landroid/preference/Preference;I)V

    .line 678
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->s:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget v1, v1, Lcom/twitter/library/provider/n;->g:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a(Landroid/preference/Preference;I)V

    .line 680
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget v1, v1, Lcom/twitter/library/provider/n;->f:I

    iput v1, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->h:I

    .line 681
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget v1, v1, Lcom/twitter/library/provider/n;->g:I

    iput v1, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->j:I

    .line 687
    :goto_3
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->E:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget v1, v1, Lcom/twitter/library/provider/n;->w:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->b(Landroid/preference/Preference;I)V

    .line 688
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-boolean v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->c:Z

    invoke-static {v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->c(Lcom/twitter/android/settings/MobileNotificationsActivity;Z)V

    goto/16 :goto_0

    .line 652
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget v0, v0, Lcom/twitter/library/provider/n;->h:I

    if-lez v0, :cond_4

    .line 653
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->F:Landroid/preference/ListPreference;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a(Landroid/preference/Preference;I)V

    goto/16 :goto_1

    .line 655
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->F:Landroid/preference/ListPreference;

    invoke-direct {p0, v0, v2}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a(Landroid/preference/Preference;I)V

    goto/16 :goto_1

    .line 658
    :cond_5
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->t:Landroid/preference/Preference;

    iget-object v3, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget v3, v3, Lcom/twitter/library/provider/n;->h:I

    invoke-direct {p0, v0, v3}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->b(Landroid/preference/Preference;I)V

    goto/16 :goto_1

    :cond_6
    move v0, v2

    .line 665
    goto/16 :goto_2

    .line 683
    :cond_7
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->p:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget v1, v1, Lcom/twitter/library/provider/n;->m:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->b(Landroid/preference/Preference;I)V

    .line 684
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->r:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget v1, v1, Lcom/twitter/library/provider/n;->f:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a(Landroid/preference/Preference;I)V

    .line 685
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    iget-object v0, v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->s:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->d:Lcom/twitter/library/provider/n;

    iget v1, v1, Lcom/twitter/library/provider/n;->g:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a(Landroid/preference/Preference;I)V

    goto :goto_3
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 581
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 581
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 592
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 593
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->a:Lcom/twitter/android/settings/MobileNotificationsActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->b(Lcom/twitter/android/settings/MobileNotificationsActivity;Z)V

    .line 594
    return-void
.end method
