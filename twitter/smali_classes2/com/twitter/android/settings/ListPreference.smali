.class public Lcom/twitter/android/settings/ListPreference;
.super Landroid/preference/ListPreference;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/settings/ListPreference$a;
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/android/settings/ListPreference$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 63
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/settings/ListPreference$a;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/twitter/android/settings/ListPreference;->a:Lcom/twitter/android/settings/ListPreference$a;

    .line 88
    return-void
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 82
    invoke-super {p0, p1}, Landroid/preference/ListPreference;->onBindView(Landroid/view/View;)V

    .line 83
    invoke-static {p1}, Lcom/twitter/android/settings/MultilineTitlePreference;->a(Landroid/view/View;)V

    .line 84
    return-void
.end method

.method protected onClick()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/android/settings/ListPreference;->a:Lcom/twitter/android/settings/ListPreference$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/settings/ListPreference;->a:Lcom/twitter/android/settings/ListPreference$a;

    invoke-interface {v0}, Lcom/twitter/android/settings/ListPreference$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    :goto_0
    return-void

    .line 95
    :cond_0
    invoke-super {p0}, Landroid/preference/ListPreference;->onClick()V

    goto :goto_0
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 70
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_1

    .line 71
    invoke-virtual {p0}, Lcom/twitter/android/settings/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    invoke-super {p0, p1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 73
    invoke-virtual {p0}, Lcom/twitter/android/settings/ListPreference;->notifyChanged()V

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    invoke-super {p0, p1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showDialog(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/preference/ListPreference;->showDialog(Landroid/os/Bundle;)V

    .line 105
    return-void
.end method
