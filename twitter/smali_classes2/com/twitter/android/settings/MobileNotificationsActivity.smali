.class public Lcom/twitter/android/settings/MobileNotificationsActivity;
.super Lcom/twitter/android/client/TwitterPreferenceActivity;
.source "Twttr"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/settings/MobileNotificationsActivity$b;,
        Lcom/twitter/android/settings/MobileNotificationsActivity$c;,
        Lcom/twitter/android/settings/MobileNotificationsActivity$a;
    }
.end annotation


# static fields
.field private static final J:[Ljava/lang/String;


# instance fields
.field A:Landroid/preference/Preference;

.field B:Landroid/preference/Preference;

.field C:Landroid/preference/Preference;

.field D:Landroid/preference/Preference;

.field E:Landroid/preference/Preference;

.field F:Landroid/preference/ListPreference;

.field private final K:Lcoj$a;

.field private L:Z

.field private M:Lcom/twitter/android/settings/MobileNotificationsActivity$b;

.field private N:Z

.field private O:I

.field private P:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field

.field a:Ljava/lang/String;

.field b:Lcom/twitter/library/client/Session;

.field c:Lcom/twitter/model/core/TwitterUser;

.field e:Z

.field f:Ljava/lang/String;

.field g:Z

.field h:I

.field i:Z

.field j:I

.field k:Z

.field l:Z

.field m:Lcom/twitter/library/provider/n;

.field n:Z

.field o:Landroid/preference/PreferenceCategory;

.field p:Landroid/preference/Preference;

.field q:Landroid/preference/Preference;

.field r:Landroid/preference/Preference;

.field s:Landroid/preference/Preference;

.field t:Landroid/preference/Preference;

.field u:Landroid/preference/Preference;

.field v:Landroid/preference/Preference;

.field w:Landroid/preference/Preference;

.field x:Landroid/preference/Preference;

.field y:Landroid/preference/Preference;

.field z:Landroid/preference/Preference;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 117
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "use_led"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "vibrate"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->J:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/twitter/android/client/TwitterPreferenceActivity;-><init>()V

    .line 153
    new-instance v0, Lcom/twitter/android/settings/MobileNotificationsActivity$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/settings/MobileNotificationsActivity$1;-><init>(Lcom/twitter/android/settings/MobileNotificationsActivity;)V

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->K:Lcoj$a;

    return-void
.end method

.method static synthetic a(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 66
    invoke-static {p0, p1, p2, p3, p4}, Lcom/twitter/android/settings/MobileNotificationsActivity;->b(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Intent;Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 1113
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->a(Landroid/content/Intent;Landroid/view/ViewGroup;Landroid/content/Context;)V

    .line 1114
    return-void
.end method

.method public static a(Landroid/content/Intent;Landroid/view/ViewGroup;Landroid/content/Context;)V
    .locals 13

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 1132
    const-string/jumbo v0, "NotificationSettingsActivity_text"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1133
    const-string/jumbo v0, "NotificationSettingsActivity_notif_random_id"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1134
    const-string/jumbo v0, "NotificationSettingsActivity_notif_type"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1135
    const-string/jumbo v0, "NotificationSettingsActivity_scribe_component"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "notif_settings_link_num_times_shown_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1137
    const v0, 0x7f13081b

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 1138
    const v0, 0x7f1305e0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1139
    invoke-static {v3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    .line 1140
    if-eqz v1, :cond_0

    .line 1141
    invoke-static {p2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 1142
    invoke-static {v7, v5}, Lcom/twitter/android/settings/MobileNotificationsActivity;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)Z

    move-result v1

    .line 1143
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "_"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 1144
    invoke-interface {v7, v11, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-lez v2, :cond_1

    move v2, v9

    .line 1145
    :goto_0
    if-eqz v1, :cond_2

    .line 1146
    if-nez v2, :cond_0

    .line 1147
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v7, v6, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v12

    add-int/lit8 v12, v12, 0x1

    invoke-interface {v2, v6, v12}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1148
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2, v11, v9}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1156
    :cond_0
    :goto_1
    if-nez v1, :cond_4

    .line 1157
    const/16 v0, 0x8

    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1180
    :goto_2
    return-void

    :cond_1
    move v2, v10

    .line 1144
    goto :goto_0

    .line 1150
    :cond_2
    if-eqz v2, :cond_0

    .line 1152
    invoke-interface {v7, v6, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-lez v1, :cond_3

    move v1, v9

    goto :goto_1

    :cond_3
    move v1, v10

    goto :goto_1

    .line 1160
    :cond_4
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1161
    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    .line 1163
    const-string/jumbo v1, "NotificationSettingsActivity_username"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1164
    const-string/jumbo v1, "NotificationSettingsActivity_user_id"

    const-wide/16 v2, -0x1

    invoke-virtual {p0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1165
    const-string/jumbo v1, "follow"

    invoke-virtual {v5, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_5

    const-string/jumbo v7, "profile"

    .line 1166
    :goto_3
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v11, 0x5

    new-array v11, v11, [Ljava/lang/String;

    aput-object v7, v11, v10

    const-string/jumbo v10, "notification_landing"

    aput-object v10, v11, v9

    const/4 v9, 0x2

    aput-object v8, v11, v9

    const/4 v9, 0x3

    const-string/jumbo v10, "header"

    aput-object v10, v11, v9

    const/4 v9, 0x4

    const-string/jumbo v10, "show"

    aput-object v10, v11, v9

    .line 1167
    invoke-virtual {v1, v11}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 1169
    new-instance v1, Lcom/twitter/android/settings/MobileNotificationsActivity$3;

    invoke-direct/range {v1 .. v8}, Lcom/twitter/android/settings/MobileNotificationsActivity$3;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 1165
    :cond_5
    const-string/jumbo v7, "tweet"

    goto :goto_3
.end method

.method private static a(Landroid/preference/Preference;I)V
    .locals 1

    .prologue
    .line 546
    if-nez p1, :cond_1

    .line 547
    const v0, 0x7f0a0863

    invoke-virtual {p0, v0}, Landroid/preference/Preference;->setSummary(I)V

    .line 553
    :cond_0
    :goto_0
    return-void

    .line 548
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 549
    const v0, 0x7f0a085f

    invoke-virtual {p0, v0}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_0

    .line 550
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 551
    const v0, 0x7f0a085e

    invoke-virtual {p0, v0}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_0
.end method

.method private static a(Landroid/preference/PreferenceGroup;Landroid/preference/Preference;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1218
    if-eqz p1, :cond_0

    .line 1219
    invoke-virtual {p0, p1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 1224
    :goto_0
    return-void

    .line 1221
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "NotificationSettingsActivity preference (key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") was unexpectedly null when trying to remove it."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/settings/MobileNotificationsActivity;Z)V
    .locals 0

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->c(Z)V

    return-void
.end method

.method public static a(Landroid/content/SharedPreferences;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1208
    const-string/jumbo v1, "notif_settings_link_on_push_landing_pages_enabled"

    invoke-static {v1}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1213
    :cond_0
    :goto_0
    return v0

    .line 1211
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "notif_settings_link_num_times_shown_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v1, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 1213
    if-ltz v1, :cond_0

    const/4 v2, 0x3

    if-ge v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/settings/MobileNotificationsActivity;)Z
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->N:Z

    return v0
.end method

.method private static b(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 1193
    const-string/jumbo v0, "tweet_"

    invoke-virtual {p4, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    move v1, v2

    .line 1194
    :goto_0
    if-eqz v1, :cond_1

    const-class v0, Lcom/twitter/android/settings/TweetSettingsActivity;

    .line 1196
    :goto_1
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1197
    if-eqz v1, :cond_2

    .line 1198
    const-string/jumbo v0, "TweetSettingsActivity_account_name"

    .line 1199
    invoke-virtual {v3, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "TweetSettingsActivity_from_notification_landing"

    .line 1200
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1204
    :goto_2
    return-object v3

    .line 1193
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 1194
    :cond_1
    const-class v0, Lcom/twitter/android/settings/MobileNotificationsActivity;

    goto :goto_1

    .line 1202
    :cond_2
    const-string/jumbo v0, "NotificationSettingsActivity_account_name"

    invoke-virtual {v3, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2
.end method

.method private b(ZZ)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 463
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "NotificationSettingsActivity_enabled"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "TweetSettingsActivity_enabled"

    .line 464
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 463
    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/settings/MobileNotificationsActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->P:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/settings/MobileNotificationsActivity;Z)V
    .locals 0

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->c(Z)V

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/settings/MobileNotificationsActivity;)I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->O:I

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/settings/MobileNotificationsActivity;Z)V
    .locals 0

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->c(Z)V

    return-void
.end method

.method static synthetic d(Lcom/twitter/android/settings/MobileNotificationsActivity;Z)V
    .locals 0

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->c(Z)V

    return-void
.end method

.method static synthetic d(Lcom/twitter/android/settings/MobileNotificationsActivity;)Z
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->m()Z

    move-result v0

    return v0
.end method

.method static synthetic i()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/twitter/android/settings/MobileNotificationsActivity;->J:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected a()V
    .locals 4

    .prologue
    .line 278
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 279
    iget-object v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->b:Lcom/twitter/library/client/Session;

    invoke-static {v0}, Lbqa;->a(Lcom/twitter/library/client/Session;)V

    .line 282
    :cond_0
    return-void
.end method

.method a(IZ)V
    .locals 1

    .prologue
    .line 556
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->r:Landroid/preference/Preference;

    invoke-static {v0, p1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->a(Landroid/preference/Preference;I)V

    .line 557
    iput p1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->h:I

    .line 558
    iput-boolean p2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->i:Z

    .line 559
    return-void
.end method

.method a(Landroid/preference/Preference;)V
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->o:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 345
    return-void
.end method

.method public a(Lcom/twitter/library/service/s;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 261
    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/TwitterPreferenceActivity;->a(Lcom/twitter/library/service/s;I)V

    .line 262
    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    .line 263
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    check-cast p1, Lbhn;

    invoke-virtual {p1}, Lbhn;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->P:Ljava/util/ArrayList;

    .line 265
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->P:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->O:I

    .line 267
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    iget v0, v0, Lcom/twitter/library/provider/n;->m:I

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_0
    iget v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->O:I

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->a(ZI)V

    .line 273
    :cond_0
    return-void

    .line 268
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/model/core/TwitterUser;)V
    .locals 2

    .prologue
    .line 285
    invoke-static {p1}, Lcom/twitter/android/util/aj;->a(Lcom/twitter/model/core/TwitterUser;)Z

    move-result v0

    .line 286
    invoke-virtual {p0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->e()Landroid/preference/Preference;

    move-result-object v1

    .line 287
    if-eqz v0, :cond_0

    .line 288
    invoke-virtual {p0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->a(Landroid/preference/Preference;)V

    .line 289
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 294
    :goto_0
    return-void

    .line 291
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 292
    invoke-virtual {p0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->b(Landroid/preference/Preference;)V

    goto :goto_0
.end method

.method a(ZI)V
    .locals 6

    .prologue
    .line 533
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->p:Landroid/preference/Preference;

    .line 534
    if-eqz p1, :cond_0

    .line 535
    invoke-virtual {p0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0026

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 536
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, p2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 535
    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 540
    :goto_0
    iput-boolean p1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->g:Z

    .line 541
    iput p2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->O:I

    .line 542
    const/4 v0, -0x1

    invoke-virtual {p0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->m()Z

    move-result v1

    invoke-direct {p0, v1, p1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->b(ZZ)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->setResult(ILandroid/content/Intent;)V

    .line 543
    return-void

    .line 538
    :cond_0
    const v1, 0x7f0a0863

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_0
.end method

.method protected a(Z)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 440
    if-eqz p1, :cond_1

    .line 441
    invoke-static {p0}, Lcom/google/android/gcm/b;->g(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 442
    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->showDialog(I)V

    .line 443
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 444
    sget-object v2, Lcom/twitter/library/platform/notifications/PushRegistration;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 445
    sget-object v2, Lcom/twitter/library/platform/notifications/PushRegistration;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 446
    new-instance v2, Lcom/twitter/android/settings/MobileNotificationsActivity$b;

    invoke-direct {v2, p0, v4}, Lcom/twitter/android/settings/MobileNotificationsActivity$b;-><init>(Lcom/twitter/android/settings/MobileNotificationsActivity;Lcom/twitter/android/settings/MobileNotificationsActivity$1;)V

    iput-object v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->M:Lcom/twitter/android/settings/MobileNotificationsActivity$b;

    .line 447
    iget-object v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->M:Lcom/twitter/android/settings/MobileNotificationsActivity$b;

    sget-object v3, Lcom/twitter/database/schema/a;->a:Ljava/lang/String;

    invoke-virtual {p0, v2, v1, v3, v4}, Lcom/twitter/android/settings/MobileNotificationsActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 448
    invoke-static {p0}, Lcom/twitter/library/platform/notifications/PushRegistration;->a(Landroid/content/Context;)V

    .line 458
    :cond_0
    const/4 v1, -0x1

    iget-boolean v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->g:Z

    invoke-direct {p0, p1, v2}, Lcom/twitter/android/settings/MobileNotificationsActivity;->b(ZZ)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/settings/MobileNotificationsActivity;->setResult(ILandroid/content/Intent;)V

    .line 459
    :goto_0
    return v0

    .line 452
    :cond_1
    invoke-static {p0}, Lcom/google/android/gcm/b;->g(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->b:Lcom/twitter/library/client/Session;

    .line 453
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 452
    invoke-static {p0, v2, v3}, Lbaw;->a(Landroid/content/Context;J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 454
    const v1, 0x7f0a00fb

    invoke-virtual {p0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 455
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b(IZ)V
    .locals 1

    .prologue
    .line 562
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->s:Landroid/preference/Preference;

    invoke-static {v0, p1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->a(Landroid/preference/Preference;I)V

    .line 563
    iput p1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->j:I

    .line 564
    iput-boolean p2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->k:Z

    .line 565
    return-void
.end method

.method b(Landroid/preference/Preference;)V
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->o:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 349
    return-void
.end method

.method protected b(Lcom/twitter/model/core/TwitterUser;)V
    .locals 2

    .prologue
    .line 297
    invoke-static {p1}, Lcom/twitter/android/util/aj;->b(Lcom/twitter/model/core/TwitterUser;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->l:Z

    .line 298
    invoke-virtual {p0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->f()Landroid/preference/Preference;

    move-result-object v0

    .line 299
    iget-boolean v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->l:Z

    if-eqz v1, :cond_0

    .line 300
    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->a(Landroid/preference/Preference;)V

    .line 301
    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 302
    invoke-virtual {p0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->g()Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->b(Landroid/preference/Preference;)V

    .line 306
    :goto_0
    return-void

    .line 304
    :cond_0
    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->b(Landroid/preference/Preference;)V

    goto :goto_0
.end method

.method b(Z)V
    .locals 1

    .prologue
    .line 568
    if-nez p1, :cond_0

    .line 576
    :goto_0
    return-void

    .line 572
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->c:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 573
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->c:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->b(Lcom/twitter/model/core/TwitterUser;)V

    .line 574
    invoke-virtual {p0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->c()V

    .line 575
    invoke-virtual {p0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->d()V

    goto :goto_0
.end method

.method protected c()V
    .locals 2

    .prologue
    .line 309
    invoke-static {}, Lcom/twitter/android/commerce/util/b;->d()Z

    move-result v0

    .line 310
    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->o:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->B:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 312
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->B:Landroid/preference/Preference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 317
    :goto_0
    return-void

    .line 314
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->B:Landroid/preference/Preference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 315
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->o:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->B:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method protected d()V
    .locals 2

    .prologue
    .line 322
    invoke-static {}, Lbsd;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->o:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->D:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 324
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->D:Landroid/preference/Preference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 329
    :goto_0
    return-void

    .line 326
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->D:Landroid/preference/Preference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 327
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->o:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->D:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method e()Landroid/preference/Preference;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->A:Landroid/preference/Preference;

    return-object v0
.end method

.method f()Landroid/preference/Preference;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->F:Landroid/preference/ListPreference;

    return-object v0
.end method

.method g()Landroid/preference/Preference;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->t:Landroid/preference/Preference;

    return-object v0
.end method

.method protected h()Z
    .locals 1

    .prologue
    .line 353
    iget-boolean v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->e:Z

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 402
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 403
    if-ne p2, v1, :cond_1

    if-eqz p3, :cond_1

    .line 404
    const-string/jumbo v0, "TweetSettingsActivity_enabled"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 405
    const-string/jumbo v0, "TweetSettingsActivity_enabled"

    invoke-virtual {p3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const-string/jumbo v1, "TweetSettingsActivity_count"

    .line 406
    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 405
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->a(ZI)V

    .line 408
    :cond_0
    const-string/jumbo v0, "NotificationSettingsActivity_tweet_follow_users"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 409
    const-string/jumbo v0, "NotificationSettingsActivity_tweet_follow_users"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->P:Ljava/util/ArrayList;

    .line 431
    :cond_1
    :goto_0
    return-void

    .line 412
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_4

    .line 413
    if-ne p2, v1, :cond_1

    if-eqz p3, :cond_1

    .line 414
    const-string/jumbo v0, "pref_mention"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string/jumbo v0, "pref_choice"

    .line 415
    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 416
    :cond_3
    const-string/jumbo v0, "pref_choice"

    invoke-virtual {p3, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string/jumbo v1, "pref_mention"

    .line 417
    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 416
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->a(IZ)V

    goto :goto_0

    .line 420
    :cond_4
    const/4 v0, 0x3

    if-ne p1, v0, :cond_6

    .line 421
    if-ne p2, v1, :cond_1

    if-eqz p3, :cond_1

    .line 422
    const-string/jumbo v0, "pref_mention"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string/jumbo v0, "pref_choice"

    .line 423
    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 424
    :cond_5
    const-string/jumbo v0, "pref_choice"

    invoke-virtual {p3, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string/jumbo v1, "pref_mention"

    .line 425
    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 424
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->b(IZ)V

    goto :goto_0

    .line 429
    :cond_6
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 178
    invoke-static {p0}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->e:Z

    .line 179
    invoke-super {p0, p1}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 180
    const v0, 0x7f0a0848

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->setTitle(I)V

    .line 181
    invoke-virtual {p0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "NotificationSettingsActivity_account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->a:Ljava/lang/String;

    .line 182
    invoke-virtual {p0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->j()Lcom/twitter/library/client/v;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->b:Lcom/twitter/library/client/Session;

    .line 183
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->c:Lcom/twitter/model/core/TwitterUser;

    .line 184
    invoke-static {p0}, Lcom/twitter/util/d;->e(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->N:Z

    .line 185
    iput-boolean v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->L:Z

    .line 187
    iget-boolean v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->e:Z

    if-eqz v0, :cond_4

    .line 188
    const v0, 0x7f080014

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->addPreferencesFromResource(I)V

    .line 193
    :goto_0
    const-string/jumbo v0, "legacy_deciders_lifeline_alerts_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    move v1, v2

    .line 195
    :goto_1
    const-string/jumbo v0, "push_notifications_separate_photo_tags"

    .line 196
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v4

    .line 197
    const-string/jumbo v0, "notif_lifeline_alerts"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->x:Landroid/preference/Preference;

    .line 198
    const-string/jumbo v0, "notif_photo_tags"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->E:Landroid/preference/Preference;

    .line 199
    iget-boolean v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->e:Z

    if-eqz v0, :cond_6

    .line 200
    const-string/jumbo v0, "notif_tweets"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->p:Landroid/preference/Preference;

    .line 201
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->p:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 202
    const-string/jumbo v0, "notif_mentions_choice"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->q:Landroid/preference/Preference;

    .line 203
    const-string/jumbo v0, "notif_address_book"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->u:Landroid/preference/Preference;

    .line 204
    const-string/jumbo v0, "notif_experimental"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->v:Landroid/preference/Preference;

    .line 205
    const-string/jumbo v0, "notif_recommendations"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->y:Landroid/preference/Preference;

    .line 206
    const-string/jumbo v0, "notif_news"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->z:Landroid/preference/Preference;

    .line 207
    const-string/jumbo v0, "notif_vit_notable_event"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->A:Landroid/preference/Preference;

    .line 208
    const-string/jumbo v0, "notif_vit_follows"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->F:Landroid/preference/ListPreference;

    .line 209
    const-string/jumbo v0, "notif_offer_redemption"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->B:Landroid/preference/Preference;

    .line 210
    const-string/jumbo v0, "notif_highlights"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->C:Landroid/preference/Preference;

    .line 211
    const-string/jumbo v0, "notif_moments"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->D:Landroid/preference/Preference;

    .line 212
    invoke-virtual {p0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v5, "notif_pref_category"

    invoke-virtual {v0, v5}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->o:Landroid/preference/PreferenceCategory;

    .line 213
    const-string/jumbo v0, "notif_retweets_choice"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->r:Landroid/preference/Preference;

    .line 214
    const-string/jumbo v0, "notif_favorites_choice"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->s:Landroid/preference/Preference;

    .line 215
    if-eqz v1, :cond_0

    .line 216
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->o:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->x:Landroid/preference/Preference;

    const-string/jumbo v5, "notif_lifeline_alerts"

    invoke-static {v0, v1, v5}, Lcom/twitter/android/settings/MobileNotificationsActivity;->a(Landroid/preference/PreferenceGroup;Landroid/preference/Preference;Ljava/lang/String;)V

    .line 218
    :cond_0
    if-nez v4, :cond_1

    .line 219
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->o:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->E:Landroid/preference/Preference;

    const-string/jumbo v5, "notif_photo_tags"

    invoke-static {v0, v1, v5}, Lcom/twitter/android/settings/MobileNotificationsActivity;->a(Landroid/preference/PreferenceGroup;Landroid/preference/Preference;Ljava/lang/String;)V

    .line 233
    :cond_1
    :goto_2
    const-string/jumbo v0, "notif_follows"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->t:Landroid/preference/Preference;

    .line 234
    const-string/jumbo v0, "notif_direct_messages"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->w:Landroid/preference/Preference;

    .line 237
    if-eqz v4, :cond_8

    .line 238
    const v0, 0x7f0a0854

    move v1, v0

    .line 242
    :goto_3
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->q:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    .line 244
    iget-boolean v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->e:Z

    if-eqz v0, :cond_2

    .line 245
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->q:Landroid/preference/Preference;

    check-cast v0, Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setDialogTitle(I)V

    .line 248
    :cond_2
    const-string/jumbo v0, "ringtone"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 250
    if-nez p1, :cond_3

    .line 251
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v2, [Ljava/lang/String;

    const-string/jumbo v2, "settings:notifications:::impression"

    aput-object v2, v1, v3

    .line 252
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 251
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 255
    :cond_3
    new-instance v0, Lbhn;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->b:Lcom/twitter/library/client/Session;

    const/16 v2, 0x2b

    invoke-direct {v0, p0, v1, v2}, Lbhn;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;I)V

    const/16 v1, 0x190

    .line 256
    invoke-virtual {v0, v1}, Lbhn;->a(I)Lbhn;

    move-result-object v0

    const/4 v1, 0x4

    .line 255
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 257
    return-void

    .line 190
    :cond_4
    const v0, 0x7f080013

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->addPreferencesFromResource(I)V

    goto/16 :goto_0

    :cond_5
    move v1, v3

    .line 193
    goto/16 :goto_1

    .line 222
    :cond_6
    const-string/jumbo v0, "notif_timeline"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->p:Landroid/preference/Preference;

    .line 223
    const-string/jumbo v0, "notif_mentions"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->q:Landroid/preference/Preference;

    .line 224
    const-string/jumbo v0, "notif_retweets"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->r:Landroid/preference/Preference;

    .line 225
    const-string/jumbo v0, "notif_favorites"

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->s:Landroid/preference/Preference;

    .line 226
    if-eqz v1, :cond_7

    .line 227
    invoke-virtual {p0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->x:Landroid/preference/Preference;

    const-string/jumbo v5, "notif_lifeline_alerts"

    invoke-static {v0, v1, v5}, Lcom/twitter/android/settings/MobileNotificationsActivity;->a(Landroid/preference/PreferenceGroup;Landroid/preference/Preference;Ljava/lang/String;)V

    .line 229
    :cond_7
    if-nez v4, :cond_1

    .line 230
    invoke-virtual {p0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->E:Landroid/preference/Preference;

    const-string/jumbo v5, "notif_photo_tags"

    invoke-static {v0, v1, v5}, Lcom/twitter/android/settings/MobileNotificationsActivity;->a(Landroid/preference/PreferenceGroup;Landroid/preference/Preference;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 240
    :cond_8
    const v0, 0x7f0a0853

    move v1, v0

    goto/16 :goto_3
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 469
    if-ne p1, v2, :cond_0

    .line 470
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 471
    const v1, 0x7f0a0832

    invoke-virtual {p0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 472
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 473
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 474
    new-instance v1, Lcom/twitter/android/settings/MobileNotificationsActivity$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/settings/MobileNotificationsActivity$2;-><init>(Lcom/twitter/android/settings/MobileNotificationsActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 482
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 394
    invoke-super {p0}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onDestroy()V

    .line 395
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->M:Lcom/twitter/android/settings/MobileNotificationsActivity$b;

    if-eqz v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->M:Lcom/twitter/android/settings/MobileNotificationsActivity$b;

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 398
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 378
    invoke-super {p0}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onPause()V

    .line 379
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->K:Lcoj$a;

    invoke-static {v0}, Lcoj;->b(Lcoj$a;)V

    .line 380
    const/4 v0, -0x1

    invoke-virtual {p0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->m()Z

    move-result v1

    iget-boolean v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->g:Z

    invoke-direct {p0, v1, v2}, Lcom/twitter/android/settings/MobileNotificationsActivity;->b(ZZ)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->setResult(ILandroid/content/Intent;)V

    .line 381
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 487
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 488
    const-string/jumbo v1, "ringtone"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 489
    check-cast p2, Ljava/lang/String;

    iput-object p2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->f:Ljava/lang/String;

    .line 493
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 490
    :cond_1
    instance-of v0, p1, Landroid/preference/ListPreference;

    if-eqz v0, :cond_0

    .line 491
    check-cast p1, Landroid/preference/ListPreference;

    check-cast p2, Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/twitter/library/util/af;->a(Landroid/preference/ListPreference;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 498
    const-string/jumbo v0, "notif_tweets"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 501
    iput-boolean v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->L:Z

    .line 502
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/settings/TweetSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "TweetSettingsActivity_account_name"

    iget-object v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->a:Ljava/lang/String;

    .line 503
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "TweetSettingsActivity_enabled"

    iget-boolean v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->g:Z

    .line 504
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "NotificationSettingsActivity_tweet_follow_users"

    iget-object v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->P:Ljava/util/ArrayList;

    .line 505
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    .line 502
    invoke-virtual {p0, v0, v3}, Lcom/twitter/android/settings/MobileNotificationsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 529
    :cond_0
    :goto_0
    return v3

    .line 507
    :cond_1
    const-string/jumbo v0, "notif_retweets"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 508
    iput-boolean v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->L:Z

    .line 509
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/settings/RtFavSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "pref_choice"

    iget v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->h:I

    .line 510
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "pref_choice_key"

    const-string/jumbo v2, "notif_retweets_choice"

    .line 511
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "pref_mention"

    iget-boolean v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->i:Z

    .line 512
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "pref_mention_key"

    const-string/jumbo v2, "notif_retweeted_mention"

    .line 513
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "pref_title"

    const v2, 0x7f0a085d

    .line 514
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "pref_xml"

    const v2, 0x7f08001c

    .line 515
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x2

    .line 509
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 518
    :cond_2
    const-string/jumbo v0, "notif_favorites"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 519
    iput-boolean v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->L:Z

    .line 520
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/settings/RtFavSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "pref_choice"

    iget v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->j:I

    .line 521
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "pref_choice_key"

    const-string/jumbo v2, "notif_favorites_choice"

    .line 522
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "pref_mention"

    iget-boolean v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->k:Z

    .line 523
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "pref_mention_key"

    const-string/jumbo v2, "notif_favorited_mention"

    .line 524
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "pref_title"

    const v2, 0x7f0a0852

    .line 525
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "pref_xml"

    const v2, 0x7f08000d

    .line 526
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x3

    .line 520
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 370
    invoke-super {p0}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onResume()V

    .line 371
    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->K:Lcoj$a;

    invoke-static {v0}, Lcoj;->a(Lcoj$a;)V

    .line 372
    invoke-virtual {p0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->a()V

    .line 373
    iget-boolean v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->e:Z

    invoke-virtual {p0, v0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->b(Z)V

    .line 374
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 435
    const/4 v0, 0x0

    return v0
.end method

.method protected onStart()V
    .locals 4

    .prologue
    .line 358
    invoke-super {p0}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onStart()V

    .line 359
    iget-boolean v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->L:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 360
    new-instance v0, Lcom/twitter/android/settings/MobileNotificationsActivity$a;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, p0, v2, v3}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;-><init>(Lcom/twitter/android/settings/MobileNotificationsActivity;J)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 366
    :cond_0
    :goto_0
    return-void

    .line 361
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->L:Z

    if-nez v0, :cond_0

    .line 364
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->L:Z

    goto :goto_0
.end method

.method public onStop()V
    .locals 4

    .prologue
    .line 385
    invoke-super {p0}, Lcom/twitter/android/client/TwitterPreferenceActivity;->onStop()V

    .line 386
    iget-boolean v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->L:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->m:Lcom/twitter/library/provider/n;

    if-eqz v0, :cond_0

    .line 387
    new-instance v0, Lcom/twitter/android/settings/MobileNotificationsActivity$c;

    iget-object v1, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, p0, v2, v3}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;-><init>(Lcom/twitter/android/settings/MobileNotificationsActivity;J)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity$c;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 389
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0}, Lcom/twitter/android/settings/MobileNotificationsActivity;->m()Z

    move-result v1

    iget-boolean v2, p0, Lcom/twitter/android/settings/MobileNotificationsActivity;->g:Z

    invoke-direct {p0, v1, v2}, Lcom/twitter/android/settings/MobileNotificationsActivity;->b(ZZ)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/settings/MobileNotificationsActivity;->setResult(ILandroid/content/Intent;)V

    .line 390
    return-void
.end method
