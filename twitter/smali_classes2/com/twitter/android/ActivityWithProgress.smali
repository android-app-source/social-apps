.class public abstract Lcom/twitter/android/ActivityWithProgress;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/base/TwitterFragmentActivity;",
        "Lcom/twitter/util/q",
        "<",
        "Lcom/twitter/library/api/progress/ProgressUpdatedEvent;",
        ">;"
    }
.end annotation


# instance fields
.field protected a:Landroid/content/SharedPreferences;

.field private b:Lcom/twitter/library/api/progress/b;

.field private c:Lcom/twitter/media/ui/AnimatingProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcmr;)Z
    .locals 2

    .prologue
    .line 33
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmr;)Z

    move-result v1

    .line 34
    const v0, 0x7f130450

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityWithProgress;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/AnimatingProgressBar;

    iput-object v0, p0, Lcom/twitter/android/ActivityWithProgress;->c:Lcom/twitter/media/ui/AnimatingProgressBar;

    .line 35
    return v1
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 1

    .prologue
    .line 54
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 56
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ActivityWithProgress;->a:Landroid/content/SharedPreferences;

    .line 57
    return-void
.end method

.method protected d()V
    .locals 3

    .prologue
    .line 40
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d()V

    .line 42
    iget-object v0, p0, Lcom/twitter/android/ActivityWithProgress;->b:Lcom/twitter/library/api/progress/b;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/twitter/android/ActivityWithProgress;->b:Lcom/twitter/library/api/progress/b;

    invoke-virtual {v0}, Lcom/twitter/library/api/progress/b;->c()V

    .line 44
    invoke-static {}, Lcom/twitter/library/api/progress/c;->a()Lcom/twitter/library/api/progress/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ActivityWithProgress;->b:Lcom/twitter/library/api/progress/b;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/api/progress/c;->b(Lcom/twitter/util/q;I)V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/ActivityWithProgress;->b:Lcom/twitter/library/api/progress/b;

    .line 47
    invoke-static {}, Lcom/twitter/library/api/progress/c;->a()Lcom/twitter/library/api/progress/c;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, p0, v1}, Lcom/twitter/library/api/progress/c;->b(Lcom/twitter/util/q;I)V

    .line 50
    :cond_0
    return-void
.end method

.method protected m_()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x1

    .line 77
    iget-object v0, p0, Lcom/twitter/android/ActivityWithProgress;->c:Lcom/twitter/media/ui/AnimatingProgressBar;

    if-nez v0, :cond_0

    .line 88
    :goto_0
    return-void

    .line 81
    :cond_0
    new-instance v0, Lcom/twitter/library/api/progress/b;

    invoke-direct {v0, v3}, Lcom/twitter/library/api/progress/b;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/ActivityWithProgress;->b:Lcom/twitter/library/api/progress/b;

    .line 83
    iget-object v0, p0, Lcom/twitter/android/ActivityWithProgress;->b:Lcom/twitter/library/api/progress/b;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/progress/b;->b(I)V

    .line 84
    invoke-static {}, Lcom/twitter/library/api/progress/c;->a()Lcom/twitter/library/api/progress/c;

    move-result-object v0

    invoke-virtual {v0, p0, v3}, Lcom/twitter/library/api/progress/c;->a(Lcom/twitter/util/q;I)V

    .line 85
    iget-object v0, p0, Lcom/twitter/android/ActivityWithProgress;->c:Lcom/twitter/media/ui/AnimatingProgressBar;

    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/AnimatingProgressBar;->setHideOnComplete(Z)V

    .line 86
    iget-object v0, p0, Lcom/twitter/android/ActivityWithProgress;->c:Lcom/twitter/media/ui/AnimatingProgressBar;

    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/AnimatingProgressBar;->setResetPrimaryOnComplete(Z)V

    .line 87
    iget-object v0, p0, Lcom/twitter/android/ActivityWithProgress;->c:Lcom/twitter/media/ui/AnimatingProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/AnimatingProgressBar;->setResetSecondaryOnComplete(Z)V

    goto :goto_0
.end method

.method public onEvent(Lcom/twitter/library/api/progress/ProgressUpdatedEvent;)V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/android/ActivityWithProgress;->c:Lcom/twitter/media/ui/AnimatingProgressBar;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 72
    iget-object v0, p0, Lcom/twitter/android/ActivityWithProgress;->c:Lcom/twitter/media/ui/AnimatingProgressBar;

    iget v1, p1, Lcom/twitter/library/api/progress/ProgressUpdatedEvent;->c:I

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/AnimatingProgressBar;->a(I)V

    .line 74
    :cond_0
    return-void
.end method

.method public bridge synthetic onEvent(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lcom/twitter/library/api/progress/ProgressUpdatedEvent;

    invoke-virtual {p0, p1}, Lcom/twitter/android/ActivityWithProgress;->onEvent(Lcom/twitter/library/api/progress/ProgressUpdatedEvent;)V

    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 61
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onResume()V

    .line 62
    invoke-virtual {p0}, Lcom/twitter/android/ActivityWithProgress;->m_()V

    .line 63
    return-void
.end method
