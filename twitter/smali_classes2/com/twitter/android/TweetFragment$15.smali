.class Lcom/twitter/android/TweetFragment$15;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/PageableListView$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/TweetFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/widget/PageableListView;

.field final synthetic b:Lcno;

.field final synthetic c:Lcom/twitter/android/TweetFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/TweetFragment;Lcom/twitter/library/widget/PageableListView;Lcno;)V
    .locals 0

    .prologue
    .line 554
    iput-object p1, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    iput-object p2, p0, Lcom/twitter/android/TweetFragment$15;->a:Lcom/twitter/library/widget/PageableListView;

    iput-object p3, p0, Lcom/twitter/android/TweetFragment$15;->b:Lcno;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/widget/AbsListView;)V
    .locals 4

    .prologue
    const/16 v3, 0x9

    .line 557
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->f(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/TweetFragment$a;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/TweetFragment$a;->a(Lcom/twitter/android/TweetFragment$a;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 564
    :cond_0
    :goto_0
    return-void

    .line 560
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0, v3}, Lcom/twitter/android/TweetFragment;->c_(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 561
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$15;->a:Lcom/twitter/library/widget/PageableListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/PageableListView;->a(Z)V

    .line 562
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;)Lcom/twitter/model/core/Tweet;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    invoke-static {v2}, Lcom/twitter/android/TweetFragment;->d(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/client/Session;I)V

    goto :goto_0
.end method

.method public b(Landroid/widget/AbsListView;)V
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 568
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->f(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/TweetFragment$a;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/TweetFragment$a;->a(Lcom/twitter/android/TweetFragment$a;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 577
    :cond_0
    :goto_0
    return-void

    .line 572
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0, v3}, Lcom/twitter/android/TweetFragment;->c_(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    .line 573
    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->f(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/TweetFragment$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment$a;->getCount()I

    move-result v0

    const/16 v1, 0x190

    if-ge v0, v1, :cond_0

    .line 574
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$15;->a:Lcom/twitter/library/widget/PageableListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/PageableListView;->b(Z)V

    .line 575
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;)Lcom/twitter/model/core/Tweet;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    invoke-static {v2}, Lcom/twitter/android/TweetFragment;->d(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/client/Session;I)V

    goto :goto_0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 6

    .prologue
    .line 589
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->i(Lcom/twitter/android/TweetFragment;)Lbpl;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    .line 590
    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->i(Lcom/twitter/android/TweetFragment;)Lbpl;

    move-result-object v0

    invoke-virtual {v0}, Lbpl;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 591
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->i(Lcom/twitter/android/TweetFragment;)Lbpl;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetFragment;->j(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/widget/ToggleImageButton;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbpl;->a(Landroid/view/View;)V

    .line 594
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->k(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/av/j;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 595
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->l(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/av/j;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment$15;->b:Lcno;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/j;->a(Lcno;)V

    .line 598
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->m(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/revenue/o;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 599
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->n(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/revenue/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment$15;->b:Lcno;

    const/4 v5, 0x0

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/revenue/o;->a(Lcno;IIIZ)V

    .line 602
    :cond_2
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    .line 581
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->g(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/av/j;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 582
    iget-object v0, p0, Lcom/twitter/android/TweetFragment$15;->c:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->h(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/av/j;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment$15;->b:Lcno;

    invoke-virtual {v0, v1, p2}, Lcom/twitter/android/av/j;->a(Lcno;I)V

    .line 584
    :cond_0
    return-void
.end method
