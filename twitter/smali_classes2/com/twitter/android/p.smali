.class public Lcom/twitter/android/p;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method private static a(Landroid/content/Context;)Lcom/twitter/model/timeline/k;
    .locals 4

    .prologue
    .line 66
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 68
    const-string/jumbo v1, "SeeFewer"

    const/4 v2, 0x0

    const v3, 0x7f0a054f

    .line 70
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 69
    invoke-static {v1, v2, v0}, Lcom/twitter/model/timeline/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/model/timeline/g;

    move-result-object v0

    .line 71
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v1

    .line 68
    invoke-static {v0, v1}, Lcom/twitter/model/timeline/k;->a(Lcom/twitter/model/timeline/g;Ljava/util/List;)Lcom/twitter/model/timeline/k;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/android/timeline/bk;)Lcom/twitter/model/timeline/k;
    .locals 2

    .prologue
    .line 42
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 43
    invoke-virtual {p1}, Lcom/twitter/android/timeline/bk;->e()Lcom/twitter/android/timeline/bg;

    move-result-object v1

    iget v1, v1, Lcom/twitter/android/timeline/bg;->r:I

    packed-switch v1, :pswitch_data_0

    .line 62
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 45
    :pswitch_1
    invoke-static {v0, p1}, Lcom/twitter/android/p;->a(Landroid/content/res/Resources;Lcom/twitter/android/timeline/bk;)Lcom/twitter/model/timeline/k;

    move-result-object v0

    goto :goto_0

    .line 48
    :pswitch_2
    invoke-static {p0}, Lcom/twitter/android/p;->b(Landroid/content/Context;)Lcom/twitter/model/timeline/k;

    move-result-object v0

    goto :goto_0

    .line 51
    :pswitch_3
    invoke-static {p0}, Lcom/twitter/android/p;->c(Landroid/content/Context;)Lcom/twitter/model/timeline/k;

    move-result-object v0

    goto :goto_0

    .line 54
    :pswitch_4
    invoke-static {p0}, Lcom/twitter/android/p;->a(Landroid/content/Context;)Lcom/twitter/model/timeline/k;

    move-result-object v0

    goto :goto_0

    .line 43
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static a(Landroid/content/res/Resources;Lcom/twitter/android/timeline/bk;)Lcom/twitter/model/timeline/k;
    .locals 4

    .prologue
    .line 79
    instance-of v0, p1, Lcom/twitter/android/timeline/k;

    if-eqz v0, :cond_0

    .line 80
    const v0, 0x7f0a09ad

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    check-cast p1, Lcom/twitter/android/timeline/k;

    .line 81
    invoke-interface {p1}, Lcom/twitter/android/timeline/k;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 80
    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 87
    :goto_0
    const-string/jumbo v1, "unfollow"

    const/4 v2, 0x0

    .line 88
    invoke-static {v1, v2, v0}, Lcom/twitter/model/timeline/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/model/timeline/g;

    move-result-object v0

    .line 89
    invoke-static {}, Lcom/twitter/util/collection/ImmutableList;->c()Lcom/twitter/util/collection/ImmutableList;

    move-result-object v1

    const v2, 0x7f0a09af

    .line 90
    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 87
    invoke-static {v0, v1, v2}, Lcom/twitter/model/timeline/k;->a(Lcom/twitter/model/timeline/g;Ljava/util/List;Ljava/lang/String;)Lcom/twitter/model/timeline/k;

    move-result-object v0

    return-object v0

    .line 83
    :cond_0
    const v0, 0x7f0a09ae

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 84
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Attempting to get Unfollow Dismiss context for unfollowable TimelineItem!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;)Lcom/twitter/model/timeline/k;
    .locals 4

    .prologue
    .line 95
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 97
    const-string/jumbo v1, "dontlike"

    const/4 v2, 0x0

    const v3, 0x7f0a0983

    .line 99
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 98
    invoke-static {v1, v2, v0}, Lcom/twitter/model/timeline/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/model/timeline/g;

    move-result-object v0

    .line 100
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v1

    .line 97
    invoke-static {v0, v1}, Lcom/twitter/model/timeline/k;->a(Lcom/twitter/model/timeline/g;Ljava/util/List;)Lcom/twitter/model/timeline/k;

    move-result-object v0

    return-object v0
.end method

.method private static c(Landroid/content/Context;)Lcom/twitter/model/timeline/k;
    .locals 4

    .prologue
    .line 106
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 108
    const-string/jumbo v1, "dontlike"

    const/4 v2, 0x0

    const v3, 0x7f0a0983

    .line 110
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 109
    invoke-static {v1, v2, v0}, Lcom/twitter/model/timeline/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/model/timeline/g;

    move-result-object v0

    .line 111
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v1

    .line 108
    invoke-static {v0, v1}, Lcom/twitter/model/timeline/k;->a(Lcom/twitter/model/timeline/g;Ljava/util/List;)Lcom/twitter/model/timeline/k;

    move-result-object v0

    return-object v0
.end method
