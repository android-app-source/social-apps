.class public Lcom/twitter/android/GalleryGridActivity$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/selection/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/GalleryGridActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/GalleryGridActivity;


# direct methods
.method protected constructor <init>(Lcom/twitter/android/GalleryGridActivity;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/twitter/android/GalleryGridActivity$a;->a:Lcom/twitter/android/GalleryGridActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/twitter/android/GalleryGridActivity$a;->a:Lcom/twitter/android/GalleryGridActivity;

    invoke-virtual {v0}, Lcom/twitter/android/GalleryGridActivity;->finish()V

    .line 160
    return-void
.end method

.method public a(Lcom/twitter/model/media/EditableMedia;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 143
    invoke-virtual {p1}, Lcom/twitter/model/media/EditableMedia;->f()Lcom/twitter/media/model/MediaType;

    move-result-object v0

    sget-object v2, Lcom/twitter/media/model/MediaType;->d:Lcom/twitter/media/model/MediaType;

    if-ne v0, v2, :cond_0

    move v0, v1

    .line 144
    :goto_0
    if-eqz v0, :cond_1

    .line 145
    iget-object v0, p0, Lcom/twitter/android/GalleryGridActivity$a;->a:Lcom/twitter/android/GalleryGridActivity;

    invoke-static {v0}, Lcom/twitter/android/GalleryGridActivity;->a(Lcom/twitter/android/GalleryGridActivity;)Lcom/twitter/android/media/selection/c;

    move-result-object v0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/GalleryGridActivity$a;->a:Lcom/twitter/android/GalleryGridActivity;

    invoke-virtual {v0, p1, v2, v3, v1}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;Lcom/twitter/android/media/selection/a;Z)V

    .line 150
    :goto_1
    return-void

    .line 143
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 148
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/GalleryGridActivity$a;->a:Lcom/twitter/android/GalleryGridActivity;

    invoke-static {v0}, Lcom/twitter/android/GalleryGridActivity;->a(Lcom/twitter/android/GalleryGridActivity;)Lcom/twitter/android/media/selection/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/GalleryGridActivity$a;->a:Lcom/twitter/android/GalleryGridActivity;

    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/selection/a;)V

    goto :goto_1
.end method

.method public b()V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/twitter/android/GalleryGridActivity$a;->a:Lcom/twitter/android/GalleryGridActivity;

    invoke-virtual {v0}, Lcom/twitter/android/GalleryGridActivity;->finish()V

    .line 165
    return-void
.end method

.method public b(Lcom/twitter/model/media/EditableMedia;)V
    .locals 0

    .prologue
    .line 155
    return-void
.end method
