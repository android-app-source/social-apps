.class Lcom/twitter/android/TweetActivity$b;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/TweetActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/TweetActivity;)V
    .locals 0

    .prologue
    .line 1568
    iput-object p1, p0, Lcom/twitter/android/TweetActivity$b;->a:Lcom/twitter/android/TweetActivity;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/TweetActivity;Lcom/twitter/android/TweetActivity$1;)V
    .locals 0

    .prologue
    .line 1568
    invoke-direct {p0, p1}, Lcom/twitter/android/TweetActivity$b;-><init>(Lcom/twitter/android/TweetActivity;)V

    return-void
.end method

.method private a(Lcom/twitter/library/service/s;Lcom/twitter/async/service/j;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/service/s;",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1594
    .line 1595
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->L()I

    move-result v1

    .line 1596
    packed-switch v1, :pswitch_data_0

    .line 1610
    :goto_0
    :pswitch_0
    if-eqz v0, :cond_0

    .line 1611
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$b;->a:Lcom/twitter/android/TweetActivity;

    iget-object v0, v0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    .line 1612
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$b;->a:Lcom/twitter/android/TweetActivity;

    iget-object v1, v0, Lcom/twitter/android/TweetActivity;->d:Lata;

    invoke-virtual {p2}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    invoke-interface {v1, v0}, Lata;->c(Z)V

    .line 1615
    :cond_0
    return-void

    .line 1598
    :pswitch_1
    invoke-virtual {p2}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    goto :goto_0

    .line 1596
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 1568
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetActivity$b;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 1571
    instance-of v0, p1, Lcom/twitter/library/api/upload/w;

    if-eqz v0, :cond_1

    .line 1572
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/TweetActivity$b;->a(Lcom/twitter/library/service/s;Lcom/twitter/async/service/j;)V

    .line 1591
    :cond_0
    :goto_0
    return-void

    .line 1573
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$b;->a:Lcom/twitter/android/TweetActivity;

    iget-object v0, v0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1574
    instance-of v0, p1, Lbhd;

    if-eqz v0, :cond_2

    .line 1575
    check-cast p1, Lbhd;

    .line 1576
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$b;->a:Lcom/twitter/android/TweetActivity;

    iget-object v0, v0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v0, Lcom/twitter/model/core/Tweet;->t:J

    invoke-virtual {p1}, Lbhd;->h()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 1577
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$b;->a:Lcom/twitter/android/TweetActivity;

    invoke-virtual {p1}, Lbhd;->s()J

    move-result-wide v2

    iget-object v4, p0, Lcom/twitter/android/TweetActivity$b;->a:Lcom/twitter/android/TweetActivity;

    iget-object v4, v4, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    .line 1578
    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 1577
    invoke-static {v2, v3, v4, v5}, Lcom/twitter/database/schema/a;->a(JJ)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/twitter/android/TweetActivity;->a(Lcom/twitter/android/TweetActivity;Landroid/net/Uri;)Landroid/net/Uri;

    .line 1579
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$b;->a:Lcom/twitter/android/TweetActivity;

    invoke-virtual {v0}, Lcom/twitter/android/TweetActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/TweetActivity$b;->a:Lcom/twitter/android/TweetActivity;

    invoke-virtual {v0, v1, v6, v2}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0

    .line 1581
    :cond_2
    instance-of v0, p1, Lbhg;

    if-eqz v0, :cond_0

    .line 1582
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$b;->a:Lcom/twitter/android/TweetActivity;

    iget-object v0, v0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v0, Lcom/twitter/model/core/Tweet;->t:J

    check-cast p1, Lbhg;

    .line 1583
    invoke-virtual {p1}, Lbhg;->g()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_3

    const/4 v0, 0x1

    .line 1584
    :goto_1
    if-eqz v0, :cond_0

    .line 1585
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$b;->a:Lcom/twitter/android/TweetActivity;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity$b;->a:Lcom/twitter/android/TweetActivity;

    iget-object v2, v2, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v2, Lcom/twitter/model/core/Tweet;->t:J

    iget-object v4, p0, Lcom/twitter/android/TweetActivity$b;->a:Lcom/twitter/android/TweetActivity;

    iget-object v4, v4, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    .line 1586
    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 1585
    invoke-static {v2, v3, v4, v5}, Lcom/twitter/database/schema/a;->b(JJ)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/twitter/android/TweetActivity;->a(Lcom/twitter/android/TweetActivity;Landroid/net/Uri;)Landroid/net/Uri;

    .line 1587
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$b;->a:Lcom/twitter/android/TweetActivity;

    invoke-virtual {v0}, Lcom/twitter/android/TweetActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/TweetActivity$b;->a:Lcom/twitter/android/TweetActivity;

    invoke-virtual {v0, v1, v6, v2}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto/16 :goto_0

    :cond_3
    move v0, v1

    .line 1583
    goto :goto_1
.end method
