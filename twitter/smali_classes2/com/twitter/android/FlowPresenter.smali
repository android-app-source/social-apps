.class public interface abstract Lcom/twitter/android/FlowPresenter;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/FlowPresenter$a;,
        Lcom/twitter/android/FlowPresenter$Direction;
    }
.end annotation


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(IZ)V
.end method

.method public abstract a(Landroid/os/Bundle;)V
.end method

.method public abstract a(Lcom/twitter/android/Flow$Step;Lcom/twitter/android/w;)V
.end method

.method public abstract a(Lcom/twitter/android/FlowPresenter$Direction;)V
.end method

.method public abstract a(Lcom/twitter/android/ValidationState;)V
.end method

.method public abstract a(Lcom/twitter/android/w;)V
.end method

.method public abstract a(Ljava/util/List;Lcom/twitter/android/FlowData;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/Flow$Step;",
            ">;",
            "Lcom/twitter/android/FlowData;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Z)V
.end method

.method public abstract b()Lcom/twitter/android/FlowData;
.end method

.method public abstract b(Landroid/os/Bundle;)V
.end method

.method public abstract b(Lcom/twitter/android/FlowPresenter$Direction;)V
.end method

.method public abstract c()Ljava/lang/String;
.end method

.method public abstract d()V
.end method

.method public abstract e()V
.end method

.method public abstract f()Lcom/twitter/android/ValidationState;
.end method
