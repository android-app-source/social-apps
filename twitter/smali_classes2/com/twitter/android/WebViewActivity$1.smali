.class Lcom/twitter/android/WebViewActivity$1;
.super Landroid/webkit/WebViewClient;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/WebViewActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/res/Resources;

.field final synthetic b:Landroid/net/Uri;

.field final synthetic c:Lcom/twitter/model/account/OAuthToken;

.field final synthetic d:Ljava/util/HashMap;

.field final synthetic e:Lcom/twitter/android/WebViewActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/WebViewActivity;Landroid/content/res/Resources;Landroid/net/Uri;Lcom/twitter/model/account/OAuthToken;Ljava/util/HashMap;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/twitter/android/WebViewActivity$1;->e:Lcom/twitter/android/WebViewActivity;

    iput-object p2, p0, Lcom/twitter/android/WebViewActivity$1;->a:Landroid/content/res/Resources;

    iput-object p3, p0, Lcom/twitter/android/WebViewActivity$1;->b:Landroid/net/Uri;

    iput-object p4, p0, Lcom/twitter/android/WebViewActivity$1;->c:Lcom/twitter/model/account/OAuthToken;

    iput-object p5, p0, Lcom/twitter/android/WebViewActivity$1;->d:Ljava/util/HashMap;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method private a(Landroid/net/Uri;Z)V
    .locals 5

    .prologue
    .line 169
    iget-object v0, p0, Lcom/twitter/android/WebViewActivity$1;->e:Lcom/twitter/android/WebViewActivity;

    iget-object v0, v0, Lcom/twitter/android/WebViewActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/WebViewActivity$1;->e:Lcom/twitter/android/WebViewActivity;

    iget-object v3, p0, Lcom/twitter/android/WebViewActivity$1;->c:Lcom/twitter/model/account/OAuthToken;

    iget-object v4, p0, Lcom/twitter/android/WebViewActivity$1;->d:Ljava/util/HashMap;

    invoke-virtual {v2, v3, p1, p2, v4}, Lcom/twitter/android/WebViewActivity;->a(Lcom/twitter/model/account/OAuthToken;Landroid/net/Uri;ZLjava/util/Map;)Ljava/util/Map;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/twitter/library/util/af;->a(Landroid/webkit/WebView;Ljava/lang/String;Ljava/util/Map;)V

    .line 170
    return-void
.end method

.method private a(Landroid/net/Uri;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 162
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    .line 163
    iget-object v2, p0, Lcom/twitter/android/WebViewActivity$1;->a:Landroid/content/res/Resources;

    const v3, 0x7f0a0c43

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 164
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    const-string/jumbo v2, "articles"

    .line 165
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 163
    :cond_0
    return v0
.end method

.method private b(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 179
    iget-object v0, p0, Lcom/twitter/android/WebViewActivity$1;->e:Lcom/twitter/android/WebViewActivity;

    invoke-virtual {v0}, Lcom/twitter/android/WebViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 180
    invoke-static {v0}, Lcom/twitter/util/b;->b(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 181
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 182
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 183
    const-string/jumbo v2, "lang"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 185
    :cond_0
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 121
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v0

    invoke-virtual {v0}, Lcrr;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "<h2>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/WebViewActivity$1;->e:Lcom/twitter/android/WebViewActivity;

    const v2, 0x7f0a0741

    invoke-virtual {v1, v2}, Lcom/twitter/android/WebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "</h2><p>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/WebViewActivity$1;->e:Lcom/twitter/android/WebViewActivity;

    const v2, 0x7f0a0742

    .line 123
    invoke-virtual {v1, v2}, Lcom/twitter/android/WebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "</p><ul><li>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/WebViewActivity$1;->e:Lcom/twitter/android/WebViewActivity;

    const v2, 0x7f0a0740

    .line 124
    invoke-virtual {v1, v2}, Lcom/twitter/android/WebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "</li></ul>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "text/html"

    const-string/jumbo v2, "UTF-8"

    .line 122
    invoke-virtual {p1, v0, v1, v2}, Landroid/webkit/WebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :goto_0
    return-void

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/WebViewActivity$1;->e:Lcom/twitter/android/WebViewActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/WebViewActivity;->openBrowser(Landroid/view/View;)V

    .line 128
    iget-object v0, p0, Lcom/twitter/android/WebViewActivity$1;->e:Lcom/twitter/android/WebViewActivity;

    invoke-virtual {v0}, Lcom/twitter/android/WebViewActivity;->finish()V

    goto :goto_0
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 138
    invoke-static {p2}, Lcom/twitter/android/util/al;->a(Ljava/lang/String;)Z

    move-result v1

    .line 139
    iget-object v0, p0, Lcom/twitter/android/WebViewActivity$1;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a0c3e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/WebViewActivity$1;->b:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 140
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 141
    invoke-direct {p0, v0}, Lcom/twitter/android/WebViewActivity$1;->a(Landroid/net/Uri;)Z

    move-result v3

    .line 143
    if-nez v1, :cond_0

    if-nez v2, :cond_0

    if-eqz v3, :cond_2

    .line 144
    :cond_0
    if-eqz v3, :cond_1

    invoke-direct {p0, v0}, Lcom/twitter/android/WebViewActivity$1;->b(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    :cond_1
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/WebViewActivity$1;->a(Landroid/net/Uri;Z)V

    .line 145
    const/4 v0, 0x0

    .line 157
    :goto_0
    return v0

    .line 147
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/WebViewActivity$1;->e:Lcom/twitter/android/WebViewActivity;

    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/WebViewActivity;->startActivity(Landroid/content/Intent;)V

    .line 148
    if-eqz p2, :cond_3

    .line 151
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->toURI()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->getHost()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/WebViewActivity$1;->e:Lcom/twitter/android/WebViewActivity;

    invoke-virtual {v0}, Lcom/twitter/android/WebViewActivity;->finish()V

    .line 157
    const/4 v0, 0x1

    goto :goto_0

    .line 153
    :catch_0
    move-exception v0

    goto :goto_1

    .line 152
    :catch_1
    move-exception v0

    goto :goto_1
.end method
