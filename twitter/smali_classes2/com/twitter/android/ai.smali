.class public Lcom/twitter/android/ai;
.super Lcom/twitter/app/common/timeline/c;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/ai$a;
    }
.end annotation


# direct methods
.method protected constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/twitter/app/common/timeline/c;-><init>(Landroid/os/Bundle;)V

    .line 21
    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lcom/twitter/android/ai;
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/twitter/android/ai;

    invoke-direct {v0, p0}, Lcom/twitter/android/ai;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    return v0
.end method

.method public b()Lbgw;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lbgw;->a:Lbgw;

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 42
    const/16 v0, 0xd

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x3

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/android/ai;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "timeline_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/android/ai;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "hashtag"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g()Lcom/twitter/android/ai$a;
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lcom/twitter/android/ai$a;

    invoke-direct {v0, p0}, Lcom/twitter/android/ai$a;-><init>(Lcom/twitter/android/ai;)V

    return-object v0
.end method

.method public synthetic h()Lcom/twitter/app/common/list/i$a;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/twitter/android/ai;->g()Lcom/twitter/android/ai$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lcom/twitter/app/common/base/b$a;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/twitter/android/ai;->g()Lcom/twitter/android/ai$a;

    move-result-object v0

    return-object v0
.end method
