.class public Lcom/twitter/android/TweetActivity$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/TweetActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Z

.field private c:J

.field private d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private e:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

.field private f:Lcom/twitter/model/core/Tweet;

.field private g:Lcom/twitter/android/timeline/bk;

.field private h:Z

.field private i:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 1633
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1624
    iput-wide v0, p0, Lcom/twitter/android/TweetActivity$a;->c:J

    .line 1631
    iput-wide v0, p0, Lcom/twitter/android/TweetActivity$a;->i:J

    .line 1634
    iput-object p1, p0, Lcom/twitter/android/TweetActivity$a;->a:Landroid/content/Context;

    .line 1635
    return-void
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 6

    .prologue
    .line 1685
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity$a;->a:Landroid/content/Context;

    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity$a;->b:Z

    if-eqz v0, :cond_3

    const-class v0, Lcom/twitter/android/RootTweetActivity;

    :goto_0
    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v0, "association"

    iget-object v2, p0, Lcom/twitter/android/TweetActivity$a;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 1687
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "tag"

    iget-wide v2, p0, Lcom/twitter/android/TweetActivity$a;->c:J

    .line 1688
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "focus_compose"

    iget-boolean v2, p0, Lcom/twitter/android/TweetActivity$a;->h:Z

    .line 1689
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    .line 1690
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$a;->f:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_4

    .line 1691
    const-string/jumbo v0, "tw"

    iget-object v2, p0, Lcom/twitter/android/TweetActivity$a;->f:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "type"

    iget-object v3, p0, Lcom/twitter/android/TweetActivity$a;->f:Lcom/twitter/model/core/Tweet;

    iget v3, v3, Lcom/twitter/model/core/Tweet;->P:I

    .line 1692
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1694
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$a;->f:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->H()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1695
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$a;->f:Lcom/twitter/model/core/Tweet;

    iget-object v0, v0, Lcom/twitter/model/core/Tweet;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity$a;->f:Lcom/twitter/model/core/Tweet;

    iget v2, v2, Lcom/twitter/model/core/Tweet;->h:I

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/twitter/android/TweetActivity$a;->f:Lcom/twitter/model/core/Tweet;

    iget-object v4, v4, Lcom/twitter/model/core/Tweet;->j:Ljava/util/List;

    invoke-static {v0, v2, v3, v4}, Laji;->a(Ljava/lang/String;IZLjava/util/List;)Laji;

    move-result-object v0

    .line 1699
    const-string/jumbo v2, "social_proof_override"

    sget-object v3, Laji;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v1, v2, v0, v3}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/content/Intent;

    .line 1702
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$a;->f:Lcom/twitter/model/core/Tweet;

    iget-object v0, v0, Lcom/twitter/model/core/Tweet;->ad:Lcom/twitter/model/timeline/r;

    if-eqz v0, :cond_1

    .line 1703
    const-string/jumbo v0, "tw_scribe_content"

    iget-object v2, p0, Lcom/twitter/android/TweetActivity$a;->f:Lcom/twitter/model/core/Tweet;

    iget-object v2, v2, Lcom/twitter/model/core/Tweet;->ad:Lcom/twitter/model/timeline/r;

    sget-object v3, Lcom/twitter/model/timeline/r;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v1, v0, v2, v3}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/content/Intent;

    .line 1718
    :cond_1
    :goto_1
    const-string/jumbo v0, "scribe_item"

    iget-object v2, p0, Lcom/twitter/android/TweetActivity$a;->e:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1720
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$a;->g:Lcom/twitter/android/timeline/bk;

    instance-of v0, v0, Lcom/twitter/android/timeline/ae;

    if-eqz v0, :cond_2

    .line 1721
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$a;->g:Lcom/twitter/android/timeline/bk;

    check-cast v0, Lcom/twitter/android/timeline/ae;

    iget-object v0, v0, Lcom/twitter/android/timeline/ae;->a:Lcom/twitter/model/moments/x;

    .line 1723
    const-string/jumbo v2, "timeline_moment"

    iget-object v0, v0, Lcom/twitter/model/moments/x;->c:Lcom/twitter/model/moments/Moment;

    sget-object v3, Lcom/twitter/model/moments/Moment;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v1, v2, v0, v3}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/content/Intent;

    .line 1726
    :cond_2
    return-object v1

    .line 1685
    :cond_3
    const-class v0, Lcom/twitter/android/TweetActivity;

    goto/16 :goto_0

    .line 1706
    :cond_4
    iget-wide v2, p0, Lcom/twitter/android/TweetActivity$a;->i:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_5

    .line 1707
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string/jumbo v2, "twitter"

    .line 1708
    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v2, "tweet"

    .line 1709
    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v2, "status_id"

    iget-wide v4, p0, Lcom/twitter/android/TweetActivity$a;->i:J

    .line 1710
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 1711
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 1712
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_1

    .line 1714
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Tried to create TweetActivity intent with no tweet specified"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public a(J)Lcom/twitter/android/TweetActivity$a;
    .locals 1

    .prologue
    .line 1680
    iput-wide p1, p0, Lcom/twitter/android/TweetActivity$a;->i:J

    .line 1681
    return-object p0
.end method

.method public a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/TweetActivity$a;
    .locals 0

    .prologue
    .line 1653
    iput-object p1, p0, Lcom/twitter/android/TweetActivity$a;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 1654
    return-object p0
.end method

.method public a(Lcom/twitter/analytics/feature/model/TwitterScribeItem;)Lcom/twitter/android/TweetActivity$a;
    .locals 0

    .prologue
    .line 1659
    iput-object p1, p0, Lcom/twitter/android/TweetActivity$a;->e:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 1660
    return-object p0
.end method

.method public a(Lcom/twitter/android/timeline/bk;)Lcom/twitter/android/TweetActivity$a;
    .locals 0

    .prologue
    .line 1669
    iput-object p1, p0, Lcom/twitter/android/TweetActivity$a;->g:Lcom/twitter/android/timeline/bk;

    .line 1670
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/TweetActivity$a;
    .locals 0

    .prologue
    .line 1664
    iput-object p1, p0, Lcom/twitter/android/TweetActivity$a;->f:Lcom/twitter/model/core/Tweet;

    .line 1665
    return-object p0
.end method

.method public a(Z)Lcom/twitter/android/TweetActivity$a;
    .locals 0

    .prologue
    .line 1638
    iput-boolean p1, p0, Lcom/twitter/android/TweetActivity$a;->b:Z

    .line 1639
    return-object p0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 1730
    iget-object v0, p0, Lcom/twitter/android/TweetActivity$a;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity$a;->a()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1731
    return-void
.end method
