.class Lcom/twitter/android/BaseEditProfileActivity$a;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/BaseEditProfileActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/twitter/media/model/MediaFile;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/android/BaseEditProfileActivity;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/model/media/EditableImage;


# direct methods
.method constructor <init>(Lcom/twitter/android/BaseEditProfileActivity;Lcom/twitter/model/media/EditableImage;)V
    .locals 1

    .prologue
    .line 778
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 779
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity$a;->a:Ljava/lang/ref/WeakReference;

    .line 780
    iput-object p2, p0, Lcom/twitter/android/BaseEditProfileActivity$a;->b:Lcom/twitter/model/media/EditableImage;

    .line 781
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Lcom/twitter/media/model/MediaFile;
    .locals 2

    .prologue
    .line 785
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/BaseEditProfileActivity;

    .line 786
    if-eqz v0, :cond_0

    .line 787
    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity$a;->b:Lcom/twitter/model/media/EditableImage;

    invoke-static {v0, v1}, Lbrt;->a(Landroid/content/Context;Lcom/twitter/model/media/EditableMedia;)Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    .line 790
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/media/model/MediaFile;)V
    .locals 2

    .prologue
    .line 795
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/BaseEditProfileActivity;

    .line 796
    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 797
    invoke-virtual {v0, p1}, Lcom/twitter/android/BaseEditProfileActivity;->b(Lcom/twitter/media/model/MediaFile;)V

    .line 798
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/BaseEditProfileActivity;->a(Lcom/twitter/android/BaseEditProfileActivity;Lcom/twitter/android/BaseEditProfileActivity$a;)Lcom/twitter/android/BaseEditProfileActivity$a;

    .line 799
    invoke-static {v0, p1}, Lcom/twitter/android/BaseEditProfileActivity;->a(Lcom/twitter/android/BaseEditProfileActivity;Lcom/twitter/media/model/MediaFile;)Lcom/twitter/media/model/MediaFile;

    .line 803
    :cond_0
    :goto_0
    return-void

    .line 800
    :cond_1
    if-eqz p1, :cond_0

    .line 801
    invoke-virtual {p1}, Lcom/twitter/media/model/MediaFile;->c()Lrx/g;

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 770
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/BaseEditProfileActivity$a;->a([Ljava/lang/Void;)Lcom/twitter/media/model/MediaFile;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 770
    check-cast p1, Lcom/twitter/media/model/MediaFile;

    invoke-virtual {p0, p1}, Lcom/twitter/android/BaseEditProfileActivity$a;->a(Lcom/twitter/media/model/MediaFile;)V

    return-void
.end method
