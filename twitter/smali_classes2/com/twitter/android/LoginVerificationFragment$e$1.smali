.class Lcom/twitter/android/LoginVerificationFragment$e$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/LoginVerificationFragment$e;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/model/account/LoginVerificationRequest;

.field final synthetic b:Lcom/twitter/android/LoginVerificationFragment$e;


# direct methods
.method constructor <init>(Lcom/twitter/android/LoginVerificationFragment$e;Lcom/twitter/model/account/LoginVerificationRequest;)V
    .locals 0

    .prologue
    .line 500
    iput-object p1, p0, Lcom/twitter/android/LoginVerificationFragment$e$1;->b:Lcom/twitter/android/LoginVerificationFragment$e;

    iput-object p2, p0, Lcom/twitter/android/LoginVerificationFragment$e$1;->a:Lcom/twitter/model/account/LoginVerificationRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 503
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/LoginVerificationFragment$e$1;->b:Lcom/twitter/android/LoginVerificationFragment$e;

    iget-object v1, v1, Lcom/twitter/android/LoginVerificationFragment$e;->a:Lcom/twitter/android/LoginVerificationFragment;

    .line 504
    invoke-static {v1}, Lcom/twitter/android/LoginVerificationFragment;->c(Lcom/twitter/android/LoginVerificationFragment;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "login_verification::request:accept:click"

    aput-object v2, v1, v6

    .line 505
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 506
    iget-object v1, p0, Lcom/twitter/android/LoginVerificationFragment$e$1;->a:Lcom/twitter/model/account/LoginVerificationRequest;

    iget-object v1, v1, Lcom/twitter/model/account/LoginVerificationRequest;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/library/scribe/b;->b(Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 508
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 509
    new-instance v0, Lcom/twitter/android/LoginVerificationFragment$a;

    iget-object v1, p0, Lcom/twitter/android/LoginVerificationFragment$e$1;->b:Lcom/twitter/android/LoginVerificationFragment$e;

    iget-object v1, v1, Lcom/twitter/android/LoginVerificationFragment$e;->a:Lcom/twitter/android/LoginVerificationFragment;

    iget-object v2, p0, Lcom/twitter/android/LoginVerificationFragment$e$1;->b:Lcom/twitter/android/LoginVerificationFragment$e;

    iget-object v2, v2, Lcom/twitter/android/LoginVerificationFragment$e;->a:Lcom/twitter/android/LoginVerificationFragment;

    .line 510
    invoke-virtual {v2}, Lcom/twitter/android/LoginVerificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/LoginVerificationFragment$e$1;->a:Lcom/twitter/model/account/LoginVerificationRequest;

    iget-object v3, v3, Lcom/twitter/model/account/LoginVerificationRequest;->g:Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/LoginVerificationFragment$e$1;->a:Lcom/twitter/model/account/LoginVerificationRequest;

    iget-object v4, v4, Lcom/twitter/model/account/LoginVerificationRequest;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/android/LoginVerificationFragment$e$1;->a:Lcom/twitter/model/account/LoginVerificationRequest;

    iget-object v5, v5, Lcom/twitter/model/account/LoginVerificationRequest;->c:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/LoginVerificationFragment$a;-><init>(Lcom/twitter/android/LoginVerificationFragment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    new-array v1, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/LoginVerificationFragment$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 513
    return-void
.end method
