.class Lcom/twitter/android/revenue/p$a;
.super Landroid/support/v4/view/ViewPager$SimpleOnPageChangeListener;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/revenue/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field private a:I

.field private final b:Lcom/twitter/model/core/Tweet;

.field private final c:Lbsp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsp",
            "<",
            "Lbsq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/twitter/model/core/Tweet;Lbsp;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/Tweet;",
            "Lbsp",
            "<",
            "Lbsq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 174
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager$SimpleOnPageChangeListener;-><init>()V

    .line 175
    iput-object p1, p0, Lcom/twitter/android/revenue/p$a;->b:Lcom/twitter/model/core/Tweet;

    .line 176
    iput-object p2, p0, Lcom/twitter/android/revenue/p$a;->c:Lbsp;

    .line 177
    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    .line 194
    iget-object v0, p0, Lcom/twitter/android/revenue/p$a;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 195
    if-eqz p1, :cond_1

    const/4 v0, 0x2

    .line 197
    :goto_0
    invoke-static {v0}, Lbsn;->a(I)Lbsn;

    move-result-object v0

    invoke-static {v0}, Lbsr;->a(Lbsr$a;)Ljava/lang/String;

    move-result-object v0

    .line 198
    iget-object v1, p0, Lcom/twitter/android/revenue/p$a;->c:Lbsp;

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->aC:Lcom/twitter/library/api/PromotedEvent;

    iget-object v3, p0, Lcom/twitter/android/revenue/p$a;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v3}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v3

    invoke-static {v2, v3}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v2

    .line 199
    invoke-virtual {v2, v0}, Lbsq$a;->d(Ljava/lang/String;)Lbsq$a;

    move-result-object v0

    .line 200
    invoke-virtual {v0}, Lbsq$a;->a()Lbsq;

    move-result-object v0

    .line 198
    invoke-interface {v1, v0}, Lbsp;->a(Ljava/lang/Object;)V

    .line 202
    :cond_0
    return-void

    .line 195
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method a(I)V
    .locals 0

    .prologue
    .line 190
    iput p1, p0, Lcom/twitter/android/revenue/p$a;->a:I

    .line 191
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/twitter/android/revenue/p$a;->a:I

    if-eq v0, p1, :cond_0

    .line 182
    iget v0, p0, Lcom/twitter/android/revenue/p$a;->a:I

    if-ge v0, p1, :cond_1

    const/4 v0, 0x1

    .line 183
    :goto_0
    invoke-direct {p0, v0}, Lcom/twitter/android/revenue/p$a;->a(Z)V

    .line 186
    :cond_0
    iput p1, p0, Lcom/twitter/android/revenue/p$a;->a:I

    .line 187
    return-void

    .line 182
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
