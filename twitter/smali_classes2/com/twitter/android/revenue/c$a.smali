.class Lcom/twitter/android/revenue/c$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcpv;


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/revenue/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcpv",
        "<",
        "Lcom/twitter/android/revenue/a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/android/revenue/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:F


# direct methods
.method constructor <init>(Ljava/lang/Iterable;F)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/android/revenue/a;",
            ">;F)V"
        }
    .end annotation

    .prologue
    .line 386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387
    iput-object p1, p0, Lcom/twitter/android/revenue/c$a;->a:Ljava/lang/Iterable;

    .line 388
    iput p2, p0, Lcom/twitter/android/revenue/c$a;->b:F

    .line 389
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/revenue/a;)Z
    .locals 3

    .prologue
    .line 393
    new-instance v0, Lcom/twitter/android/revenue/c$d;

    iget v1, p0, Lcom/twitter/android/revenue/c$a;->b:F

    invoke-direct {v0, p1, v1}, Lcom/twitter/android/revenue/c$d;-><init>(Lcom/twitter/android/revenue/a;F)V

    .line 394
    new-instance v1, Lcom/twitter/android/revenue/c$c;

    invoke-direct {v1, p1}, Lcom/twitter/android/revenue/c$c;-><init>(Lcom/twitter/android/revenue/a;)V

    .line 395
    iget-object v2, p0, Lcom/twitter/android/revenue/c$a;->a:Ljava/lang/Iterable;

    invoke-static {v2, v0}, Lcpt;->a(Ljava/lang/Iterable;Lcpv;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0, v1}, Lcpt;->b(Ljava/lang/Iterable;Lcpv;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 379
    check-cast p1, Lcom/twitter/android/revenue/a;

    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/c$a;->a(Lcom/twitter/android/revenue/a;)Z

    move-result v0

    return v0
.end method
