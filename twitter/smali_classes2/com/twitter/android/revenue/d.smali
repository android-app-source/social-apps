.class public Lcom/twitter/android/revenue/d;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field private final b:Lcom/twitter/library/provider/t;


# direct methods
.method constructor <init>(Lcom/twitter/library/provider/t;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/d;->a:Ljava/util/concurrent/ExecutorService;

    .line 42
    iput-object p1, p0, Lcom/twitter/android/revenue/d;->b:Lcom/twitter/library/provider/t;

    .line 43
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/revenue/d;)Lcom/twitter/library/provider/t;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/android/revenue/d;->b:Lcom/twitter/library/provider/t;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/revenue/d;Ljava/lang/String;)Lcpb;
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/twitter/android/revenue/d;->a(Ljava/lang/String;)Lcpb;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Exception;Ljava/lang/String;)Lcpb;
    .locals 3

    .prologue
    .line 164
    new-instance v0, Lcpb;

    invoke-direct {v0, p1}, Lcpb;-><init>(Ljava/lang/Throwable;)V

    const-string/jumbo v1, "class"

    .line 165
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    const-string/jumbo v1, "method"

    .line 166
    invoke-virtual {v0, v1, p2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    .line 164
    return-object v0
.end method

.method private a(Ljava/lang/String;)Lcpb;
    .locals 2

    .prologue
    .line 160
    new-instance v0, Ljava/lang/Exception;

    const-string/jumbo v1, "db_operation_failed"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/revenue/d;->a(Ljava/lang/Exception;Ljava/lang/String;)Lcpb;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/library/provider/t;Lapb;)Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 132
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v1, Lcom/twitter/android/revenue/d$5;

    invoke-direct {v1, p0, p1, p2}, Lcom/twitter/android/revenue/d$5;-><init>(Lcom/twitter/android/revenue/d;Lcom/twitter/library/provider/t;Lapb;)V

    invoke-direct {v0, v1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 148
    iget-object v1, p0, Lcom/twitter/android/revenue/d;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 150
    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 156
    :goto_0
    return-object v0

    .line 151
    :catch_0
    move-exception v0

    .line 152
    const-string/jumbo v1, "queryAdsView"

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/revenue/d;->a(Ljava/lang/Exception;Ljava/lang/String;)Lcpb;

    move-result-object v0

    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    .line 156
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 153
    :catch_1
    move-exception v0

    .line 154
    const-string/jumbo v1, "queryAdsView"

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/revenue/d;->a(Ljava/lang/Exception;Ljava/lang/String;)Lcpb;

    move-result-object v0

    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    goto :goto_1
.end method

.method public a(J)Ljava/util/concurrent/Future;
    .locals 3

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/android/revenue/d;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/twitter/android/revenue/d$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/twitter/android/revenue/d$2;-><init>(Lcom/twitter/android/revenue/d;J)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/android/revenue/a;)Ljava/util/concurrent/Future;
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/android/revenue/d;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/twitter/android/revenue/d$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/revenue/d$1;-><init>(Lcom/twitter/android/revenue/d;Lcom/twitter/android/revenue/a;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/List;J)Ljava/util/concurrent/Future;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/revenue/a;",
            ">;J)",
            "Ljava/util/concurrent/Future;"
        }
    .end annotation

    .prologue
    .line 101
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    const/4 v0, 0x0

    .line 108
    :goto_0
    return-object v0

    .line 104
    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v1

    .line 105
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/revenue/a;

    .line 106
    invoke-virtual {v0}, Lcom/twitter/android/revenue/a;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 108
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/revenue/d;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/twitter/android/revenue/d$4;

    invoke-direct {v2, p0, p2, p3, v1}, Lcom/twitter/android/revenue/d$4;-><init>(Lcom/twitter/android/revenue/d;JLjava/util/List;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    goto :goto_0
.end method

.method public b(J)Ljava/util/concurrent/Future;
    .locals 3

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/android/revenue/d;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/twitter/android/revenue/d$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/twitter/android/revenue/d$3;-><init>(Lcom/twitter/android/revenue/d;J)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method
