.class Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$a$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcpp;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$a;->a([Ljava/lang/String;)Ljava/lang/Boolean;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcpp",
        "<",
        "Lcom/twitter/model/timeline/y;",
        "Lcom/twitter/model/timeline/y;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$a;


# direct methods
.method constructor <init>(Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$a;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$a$1;->a:Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/timeline/y;)Lcom/twitter/model/timeline/y;
    .locals 3

    .prologue
    .line 214
    instance-of v0, p1, Lcom/twitter/model/timeline/t;

    if-eqz v0, :cond_0

    .line 215
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/t;

    .line 216
    new-instance v1, Lcom/twitter/model/timeline/t$a;

    invoke-direct {v1}, Lcom/twitter/model/timeline/t$a;-><init>()V

    iget-object v0, v0, Lcom/twitter/model/timeline/t;->a:Lcom/twitter/model/revenue/c;

    .line 217
    invoke-virtual {v1, v0}, Lcom/twitter/model/timeline/t$a;->a(Lcom/twitter/model/revenue/c;)Lcom/twitter/model/timeline/t$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/timeline/y;->c:Ljava/lang/String;

    .line 218
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/t$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/t$a;

    .line 219
    invoke-virtual {v0}, Lcom/twitter/model/timeline/t$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y;

    .line 228
    :goto_0
    return-object v0

    .line 220
    :cond_0
    instance-of v0, p1, Lcom/twitter/model/timeline/al;

    if-eqz v0, :cond_1

    .line 221
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/al;

    .line 222
    new-instance v1, Lcom/twitter/model/timeline/al$a;

    invoke-direct {v1}, Lcom/twitter/model/timeline/al$a;-><init>()V

    iget-object v0, v0, Lcom/twitter/model/timeline/al;->a:Lcom/twitter/model/timeline/e;

    .line 223
    invoke-virtual {v1, v0}, Lcom/twitter/model/timeline/al$a;->a(Lcom/twitter/model/timeline/e;)Lcom/twitter/model/timeline/al$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/timeline/y;->c:Ljava/lang/String;

    .line 224
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/al$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/al$a;

    .line 225
    invoke-virtual {v0}, Lcom/twitter/model/timeline/al$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y;

    goto :goto_0

    .line 226
    :cond_1
    instance-of v0, p1, Lcom/twitter/model/timeline/ah;

    if-eqz v0, :cond_2

    .line 227
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ah;

    .line 228
    new-instance v1, Lcom/twitter/model/timeline/ah$a;

    invoke-direct {v1}, Lcom/twitter/model/timeline/ah$a;-><init>()V

    iget-object v0, v0, Lcom/twitter/model/timeline/ah;->a:Lcom/twitter/model/core/ac;

    .line 229
    invoke-virtual {v1, v0}, Lcom/twitter/model/timeline/ah$a;->a(Lcom/twitter/model/core/ac;)Lcom/twitter/model/timeline/ah$a;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/timeline/y;->c:Ljava/lang/String;

    .line 230
    invoke-virtual {v0, v1}, Lcom/twitter/model/timeline/ah$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ah$a;

    .line 231
    invoke-virtual {v0}, Lcom/twitter/model/timeline/ah$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/y;

    goto :goto_0

    .line 233
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Encountered an unexpected TimelineEntity of type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/twitter/model/timeline/y;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 208
    check-cast p1, Lcom/twitter/model/timeline/y;

    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$a$1;->a(Lcom/twitter/model/timeline/y;)Lcom/twitter/model/timeline/y;

    move-result-object v0

    return-object v0
.end method
