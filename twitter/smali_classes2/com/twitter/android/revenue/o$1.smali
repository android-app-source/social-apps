.class Lcom/twitter/android/revenue/o$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/revenue/o;->a(Lcno;IIIZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/model/core/Tweet;

.field final synthetic b:Lcom/twitter/android/revenue/o;


# direct methods
.method constructor <init>(Lcom/twitter/android/revenue/o;Lcom/twitter/model/core/Tweet;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/twitter/android/revenue/o$1;->b:Lcom/twitter/android/revenue/o;

    iput-object p2, p0, Lcom/twitter/android/revenue/o$1;->a:Lcom/twitter/model/core/Tweet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 111
    iget-object v0, p0, Lcom/twitter/android/revenue/o$1;->b:Lcom/twitter/android/revenue/o;

    invoke-static {v0}, Lcom/twitter/android/revenue/o;->a(Lcom/twitter/android/revenue/o;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    invoke-static {}, Lcom/twitter/library/revenue/QualifiedDwellTracker;->a()Lcom/twitter/library/revenue/QualifiedDwellTracker;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/revenue/o$1;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/revenue/QualifiedDwellTracker;->a(Lcgi;)V

    .line 118
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/revenue/o$1;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    iget-object v0, v0, Lcgi;->c:Ljava/lang/String;

    .line 119
    invoke-static {}, Lcom/twitter/android/revenue/o;->b()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    iget-object v0, p0, Lcom/twitter/android/revenue/o$1;->b:Lcom/twitter/android/revenue/o;

    iget-object v1, p0, Lcom/twitter/android/revenue/o$1;->a:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v1, Lcom/twitter/model/core/Tweet;->G:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/revenue/o;->a(J)V

    .line 121
    return-void

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/o$1;->b:Lcom/twitter/android/revenue/o;

    invoke-static {v0}, Lcom/twitter/android/revenue/o;->b(Lcom/twitter/android/revenue/o;)Lbsp;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->at:Lcom/twitter/library/api/PromotedEvent;

    iget-object v2, p0, Lcom/twitter/android/revenue/o$1;->a:Lcom/twitter/model/core/Tweet;

    .line 115
    invoke-virtual {v2}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v2

    .line 114
    invoke-static {v1, v2}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v1

    .line 115
    invoke-virtual {v1}, Lbsq$a;->a()Lbsq;

    move-result-object v1

    .line 114
    invoke-interface {v0, v1}, Lbsp;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method
