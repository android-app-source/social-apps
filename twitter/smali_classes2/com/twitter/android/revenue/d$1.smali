.class Lcom/twitter/android/revenue/d$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/revenue/d;->a(Lcom/twitter/android/revenue/a;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/revenue/a;

.field final synthetic b:Lcom/twitter/android/revenue/d;


# direct methods
.method constructor <init>(Lcom/twitter/android/revenue/d;Lcom/twitter/android/revenue/a;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/twitter/android/revenue/d$1;->b:Lcom/twitter/android/revenue/d;

    iput-object p2, p0, Lcom/twitter/android/revenue/d$1;->a:Lcom/twitter/android/revenue/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/android/revenue/d$1;->b:Lcom/twitter/android/revenue/d;

    invoke-static {v0}, Lcom/twitter/android/revenue/d;->a(Lcom/twitter/android/revenue/d;)Lcom/twitter/library/provider/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v0

    const-class v1, Layq;

    invoke-interface {v0, v1}, Lcom/twitter/database/schema/TwitterSchema;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v0

    .line 51
    invoke-interface {v0}, Lcom/twitter/database/model/m;->b()Lcom/twitter/database/model/h;

    move-result-object v1

    .line 52
    iget-object v0, v1, Lcom/twitter/database/model/h;->d:Ljava/lang/Object;

    check-cast v0, Layq$a;

    iget-object v2, p0, Lcom/twitter/android/revenue/d$1;->a:Lcom/twitter/android/revenue/a;

    invoke-virtual {v2}, Lcom/twitter/android/revenue/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Layq$a;->c(Ljava/lang/String;)Layq$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/revenue/d$1;->a:Lcom/twitter/android/revenue/a;

    .line 53
    invoke-virtual {v2}, Lcom/twitter/android/revenue/a;->c()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Layq$a;->f(J)Layq$a;

    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "ad_id="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/revenue/d$1;->a:Lcom/twitter/android/revenue/a;

    invoke-virtual {v2}, Lcom/twitter/android/revenue/a;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/database/model/h;->a(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 56
    if-gez v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/twitter/android/revenue/d$1;->b:Lcom/twitter/android/revenue/d;

    const-string/jumbo v1, "startUpdateAd"

    invoke-static {v0, v1}, Lcom/twitter/android/revenue/d;->a(Lcom/twitter/android/revenue/d;Ljava/lang/String;)Lcpb;

    move-result-object v0

    const-string/jumbo v1, "message"

    const-string/jumbo v2, "Failed to update ad"

    .line 58
    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    const-string/jumbo v1, "ad_id"

    iget-object v2, p0, Lcom/twitter/android/revenue/d$1;->a:Lcom/twitter/android/revenue/a;

    .line 59
    invoke-virtual {v2}, Lcom/twitter/android/revenue/a;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    .line 57
    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    .line 61
    :cond_0
    return-void
.end method
