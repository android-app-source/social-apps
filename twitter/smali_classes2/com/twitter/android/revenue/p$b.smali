.class Lcom/twitter/android/revenue/p$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/revenue/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/android/widget/e",
        "<",
        "Lafu;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/ui/view/h;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/twitter/model/core/Tweet;

.field private final d:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

.field private final e:Lcom/twitter/library/view/d;

.field private final f:Lbsp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsp",
            "<",
            "Lbsq;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Z

.field private final h:I

.field private final i:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final j:Lbxu;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/library/view/d;Lbsp;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/model/core/Tweet;",
            "Lcom/twitter/library/widget/renderablecontent/DisplayMode;",
            "Lcom/twitter/library/view/d;",
            "Lbsp",
            "<",
            "Lbsq;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207
    new-instance v0, Lcom/twitter/ui/view/h$a;

    invoke-direct {v0}, Lcom/twitter/ui/view/h$a;-><init>()V

    .line 210
    invoke-virtual {v0, v1}, Lcom/twitter/ui/view/h$a;->c(Z)Lcom/twitter/ui/view/h$a;

    move-result-object v0

    .line 211
    invoke-virtual {v0, v1}, Lcom/twitter/ui/view/h$a;->g(Z)Lcom/twitter/ui/view/h$a;

    move-result-object v0

    .line 212
    invoke-virtual {v0, v1}, Lcom/twitter/ui/view/h$a;->e(Z)Lcom/twitter/ui/view/h$a;

    move-result-object v0

    .line 213
    invoke-virtual {v0, v1}, Lcom/twitter/ui/view/h$a;->i(Z)Lcom/twitter/ui/view/h$a;

    move-result-object v0

    .line 214
    invoke-virtual {v0, v1}, Lcom/twitter/ui/view/h$a;->k(Z)Lcom/twitter/ui/view/h$a;

    move-result-object v0

    .line 215
    invoke-virtual {v0}, Lcom/twitter/ui/view/h$a;->a()Lcom/twitter/ui/view/h;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/p$b;->a:Lcom/twitter/ui/view/h;

    .line 228
    invoke-static {}, Lcom/twitter/android/revenue/g;->a()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/revenue/p$b;->h:I

    .line 230
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/revenue/p$b;->i:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 232
    new-instance v0, Lbxu;

    invoke-direct {v0}, Lbxu;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/revenue/p$b;->j:Lbxu;

    .line 237
    iput-object p1, p0, Lcom/twitter/android/revenue/p$b;->b:Landroid/content/Context;

    .line 238
    iput-object p2, p0, Lcom/twitter/android/revenue/p$b;->c:Lcom/twitter/model/core/Tweet;

    .line 239
    iput-object p3, p0, Lcom/twitter/android/revenue/p$b;->d:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 240
    iput-object p4, p0, Lcom/twitter/android/revenue/p$b;->e:Lcom/twitter/library/view/d;

    .line 241
    iput-object p5, p0, Lcom/twitter/android/revenue/p$b;->f:Lbsp;

    .line 242
    iput-boolean p6, p0, Lcom/twitter/android/revenue/p$b;->g:Z

    .line 243
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/revenue/p$b;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/twitter/android/revenue/p$b;->b:Landroid/content/Context;

    return-object v0
.end method

.method private a(Landroid/content/Context;Lcax;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 262
    sget-object v3, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->g:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 263
    iget-object v0, p0, Lcom/twitter/android/revenue/p$b;->j:Lbxu;

    move-object v1, p1

    check-cast v1, Landroid/app/Activity;

    .line 264
    invoke-direct {p0, p2}, Lcom/twitter/android/revenue/p$b;->a(Lcax;)Lcom/twitter/library/card/CardContext;

    move-result-object v2

    iget-object v5, p0, Lcom/twitter/android/revenue/p$b;->i:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-object v6, v4

    .line 263
    invoke-virtual/range {v0 .. v6}, Lbxu;->a(Landroid/app/Activity;Lcom/twitter/library/card/CardContext;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/library/card/m;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/library/widget/renderablecontent/d;

    move-result-object v0

    .line 266
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 267
    if-nez v0, :cond_0

    .line 268
    const-string/jumbo v0, "Error when instantiate a card host."

    invoke-static {v0}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    .line 279
    :goto_0
    return-object v1

    .line 270
    :cond_0
    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->bg_()V

    .line 271
    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->c()V

    .line 272
    invoke-interface {v0}, Lcom/twitter/library/widget/renderablecontent/d;->d()Landroid/view/View;

    move-result-object v0

    .line 273
    if-nez v0, :cond_1

    .line 274
    const-string/jumbo v0, "Error when instantiate a card view."

    invoke-static {v0}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 276
    :cond_1
    invoke-static {}, Lcom/twitter/android/revenue/j;->a()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 284
    .line 285
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040410

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/TweetView;

    .line 286
    iget-object v1, p0, Lcom/twitter/android/revenue/p$b;->e:Lcom/twitter/library/view/d;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setOnTweetViewClickListener(Lcom/twitter/library/view/d;)V

    .line 287
    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/TweetView;->setHideInlineActions(Z)V

    .line 288
    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/TweetView;->setShowSocialBadge(Z)V

    .line 289
    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/TweetView;->setHideProfileImage(Z)V

    .line 290
    iget-boolean v1, p0, Lcom/twitter/android/revenue/p$b;->g:Z

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setAlwaysExpandMedia(Z)V

    .line 292
    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/p$b;->a(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 293
    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setTruncateText(Ljava/lang/CharSequence;)V

    .line 295
    iget-boolean v1, p0, Lcom/twitter/android/revenue/p$b;->g:Z

    if-eqz v1, :cond_2

    invoke-virtual {p2}, Lcom/twitter/model/core/Tweet;->i()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p2}, Lcom/twitter/library/av/playback/ab;->b(Lcom/twitter/model/core/Tweet;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 296
    :cond_0
    const v1, 0x7f130086

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 297
    if-eqz v1, :cond_1

    .line 298
    iget v2, p0, Lcom/twitter/android/revenue/p$b;->h:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setMinimumHeight(I)V

    .line 300
    :cond_1
    invoke-virtual {v0, v5}, Lcom/twitter/library/widget/TweetView;->setMaxLines(I)V

    .line 301
    invoke-virtual {v0, v5}, Lcom/twitter/library/widget/TweetView;->setMinLines(I)V

    move-object v1, v0

    .line 319
    :goto_0
    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/revenue/p$b;->a(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;)Lbxy;

    move-result-object v2

    .line 320
    iget-object v3, p0, Lcom/twitter/android/revenue/p$b;->a:Lcom/twitter/ui/view/h;

    invoke-virtual {v0, p2, v3, v4, v2}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/ui/view/h;ZLbxy;)V

    .line 322
    new-instance v0, Lcom/twitter/android/revenue/p$b$1;

    invoke-direct {v0, p0, p2}, Lcom/twitter/android/revenue/p$b$1;-><init>(Lcom/twitter/android/revenue/p$b;Lcom/twitter/model/core/Tweet;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 330
    const v0, 0x7f0207fd

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 331
    return-object v1

    .line 305
    :cond_2
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 308
    const/16 v2, 0x11

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 309
    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->getPaddingLeft()I

    move-result v2

    .line 310
    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->getPaddingRight()I

    move-result v3

    .line 309
    invoke-virtual {v0, v2, v4, v3, v4}, Lcom/twitter/library/widget/TweetView;->setPadding(IIII)V

    .line 311
    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 312
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 313
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 315
    const/4 v2, 0x6

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/TweetView;->setMaxLines(I)V

    goto :goto_0
.end method

.method private a(Lcax;)Lcom/twitter/library/card/CardContext;
    .locals 2

    .prologue
    .line 370
    invoke-static {p1}, Lcom/twitter/library/card/CardContextFactory;->a(Lcax;)Lcom/twitter/library/card/CardContext$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/revenue/p$b;->c:Lcom/twitter/model/core/Tweet;

    .line 371
    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->Z()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/CardContext$a;->a(Z)Lcom/twitter/library/card/CardContext$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/revenue/p$b;->c:Lcom/twitter/model/core/Tweet;

    .line 372
    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/CardContext$a;->a(Lcgi;)Lcom/twitter/library/card/CardContext$a;

    move-result-object v0

    .line 373
    invoke-virtual {v0}, Lcom/twitter/library/card/CardContext$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/CardContext;

    .line 370
    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 363
    iget-object v0, p0, Lcom/twitter/android/revenue/p$b;->c:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/twitter/android/revenue/p$b;->f:Lbsp;

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->g:Lcom/twitter/library/api/PromotedEvent;

    iget-object v2, p0, Lcom/twitter/android/revenue/p$b;->c:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v2}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v2

    invoke-static {v1, v2}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v1

    invoke-virtual {v1}, Lbsq$a;->a()Lbsq;

    move-result-object v1

    invoke-interface {v0, v1}, Lbsp;->a(Ljava/lang/Object;)V

    .line 366
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/revenue/p$b;)Lcom/twitter/library/widget/renderablecontent/DisplayMode;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/twitter/android/revenue/p$b;->d:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/revenue/p$b;)V
    .locals 0

    .prologue
    .line 205
    invoke-direct {p0}, Lcom/twitter/android/revenue/p$b;->a()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lafu;I)Landroid/view/View;
    .locals 2

    .prologue
    .line 248
    iget-object v0, p2, Lafu;->b:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_1

    .line 249
    iget-object v0, p2, Lafu;->b:Lcom/twitter/model/core/Tweet;

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/revenue/p$b;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;)Landroid/view/View;

    move-result-object v0

    .line 253
    :goto_0
    if-eqz v0, :cond_0

    .line 255
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 258
    :cond_0
    return-object v0

    .line 251
    :cond_1
    iget-object v0, p2, Lafu;->c:Lcax;

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/revenue/p$b;->a(Landroid/content/Context;Lcax;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 205
    check-cast p2, Lafu;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/revenue/p$b;->a(Landroid/content/Context;Lafu;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method a(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;)Lbxy;
    .locals 7

    .prologue
    .line 358
    invoke-static {p2}, Lcom/twitter/library/av/playback/ab;->b(Lcom/twitter/model/core/Tweet;)Z

    move-result v1

    .line 359
    new-instance v0, Lbxy;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->g:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    iget-object v5, p0, Lcom/twitter/android/revenue/p$b;->i:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const/4 v6, 0x0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lbxy;-><init>(ZLandroid/app/Activity;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    return-object v0
.end method

.method a(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 344
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 345
    const v1, 0x7f0a0362

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 346
    const v2, 0x7f0a024f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 347
    const v3, 0x7f1100c8

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 348
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 349
    invoke-virtual {v3, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 350
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v4, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v0, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    const/16 v2, 0x21

    invoke-virtual {v3, v4, v0, v1, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 352
    return-object v3
.end method

.method public a(Landroid/view/View;Lafu;I)V
    .locals 0

    .prologue
    .line 336
    return-void
.end method

.method public synthetic a(Landroid/view/View;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 205
    check-cast p2, Lafu;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/revenue/p$b;->b(Landroid/view/View;Lafu;I)V

    return-void
.end method

.method public b(Landroid/view/View;Lafu;I)V
    .locals 0

    .prologue
    .line 340
    return-void
.end method

.method public synthetic b(Landroid/view/View;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 205
    check-cast p2, Lafu;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/revenue/p$b;->a(Landroid/view/View;Lafu;I)V

    return-void
.end method
