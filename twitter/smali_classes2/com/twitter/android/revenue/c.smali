.class public Lcom/twitter/android/revenue/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/revenue/c$c;,
        Lcom/twitter/android/revenue/c$d;,
        Lcom/twitter/android/revenue/c$a;,
        Lcom/twitter/android/revenue/c$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Laow",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field static final a:Ljava/lang/String;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field


# instance fields
.field private volatile b:Z

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/android/revenue/a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/PriorityQueue",
            "<",
            "Lcom/twitter/android/revenue/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/android/revenue/a;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/twitter/android/revenue/b;

.field private final g:Landroid/os/Handler;

.field private final h:Lcom/twitter/android/revenue/d;

.field private final i:F

.field private j:J

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    sput-object v0, Lcom/twitter/android/revenue/c;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(J)V
    .locals 3

    .prologue
    .line 96
    new-instance v0, Lcom/twitter/android/revenue/d;

    invoke-static {p1, p2}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/revenue/d;-><init>(Lcom/twitter/library/provider/t;)V

    invoke-direct {p0, v0}, Lcom/twitter/android/revenue/c;-><init>(Lcom/twitter/android/revenue/d;)V

    .line 97
    return-void
.end method

.method constructor <init>(Lcom/twitter/android/revenue/d;)V
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/revenue/c;->b:Z

    .line 101
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/android/revenue/c;->g:Landroid/os/Handler;

    .line 102
    new-instance v0, Lcom/twitter/android/revenue/b;

    invoke-direct {v0}, Lcom/twitter/android/revenue/b;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/revenue/c;->f:Lcom/twitter/android/revenue/b;

    .line 103
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/c;->c:Ljava/util/Map;

    .line 104
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/c;->d:Ljava/util/Map;

    .line 105
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/c;->e:Ljava/util/Set;

    .line 106
    iput-object p1, p0, Lcom/twitter/android/revenue/c;->h:Lcom/twitter/android/revenue/d;

    .line 107
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-string/jumbo v1, "ad_formats_stale_ads_deduping_timeout"

    const-wide/16 v2, 0x78

    .line 108
    invoke-static {v1, v2, v3}, Lcoj;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 107
    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-float v0, v0

    iput v0, p0, Lcom/twitter/android/revenue/c;->i:F

    .line 110
    return-void
.end method

.method private a(Ljava/lang/String;Z)Lcom/twitter/android/revenue/a;
    .locals 6

    .prologue
    .line 160
    iget-object v0, p0, Lcom/twitter/android/revenue/c;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/revenue/a;

    .line 161
    iget-object v1, p0, Lcom/twitter/android/revenue/c;->e:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 198
    :goto_0
    return-object v0

    .line 165
    :cond_0
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/twitter/android/revenue/a;->d()Z

    move-result v1

    move v3, v1

    .line 168
    :goto_1
    iget-object v1, p0, Lcom/twitter/android/revenue/c;->d:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/PriorityQueue;

    .line 169
    iget-object v2, p0, Lcom/twitter/android/revenue/c;->d:Ljava/util/Map;

    sget-object v4, Lcom/twitter/android/revenue/c;->a:Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/PriorityQueue;

    .line 171
    if-eqz v0, :cond_4

    if-nez v3, :cond_4

    move-object v1, v0

    .line 183
    :goto_2
    if-nez p2, :cond_2

    .line 185
    if-eqz v3, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 186
    iget-object v2, p0, Lcom/twitter/android/revenue/c;->h:Lcom/twitter/android/revenue/d;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/a;->a()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/twitter/android/revenue/d;->a(J)Ljava/util/concurrent/Future;

    .line 190
    :cond_1
    invoke-virtual {v1, p1}, Lcom/twitter/android/revenue/a;->a(Ljava/lang/String;)V

    .line 191
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/revenue/a;->a(J)V

    .line 192
    iget-object v0, p0, Lcom/twitter/android/revenue/c;->e:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 193
    iget-object v0, p0, Lcom/twitter/android/revenue/c;->c:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    iget-object v0, p0, Lcom/twitter/android/revenue/c;->h:Lcom/twitter/android/revenue/d;

    invoke-virtual {v0, v1}, Lcom/twitter/android/revenue/d;->a(Lcom/twitter/android/revenue/a;)Ljava/util/concurrent/Future;

    :cond_2
    move-object v0, v1

    .line 198
    goto :goto_0

    .line 165
    :cond_3
    const/4 v1, 0x0

    move v3, v1

    goto :goto_1

    .line 173
    :cond_4
    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 174
    if-eqz p2, :cond_5

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/revenue/a;

    goto :goto_2

    :cond_5
    invoke-virtual {v1}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/revenue/a;

    goto :goto_2

    .line 175
    :cond_6
    invoke-static {v2}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 176
    if-eqz p2, :cond_7

    invoke-virtual {v2}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/revenue/a;

    goto :goto_2

    :cond_7
    invoke-virtual {v2}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/revenue/a;

    goto :goto_2

    .line 177
    :cond_8
    if-eqz v0, :cond_9

    move-object v1, v0

    .line 178
    goto :goto_2

    .line 180
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private b(Ljava/util/List;J)Ljava/lang/Iterable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/revenue/a;",
            ">;J)",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/android/revenue/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 255
    new-instance v0, Lcom/twitter/android/revenue/c$b;

    invoke-direct {v0, p2, p3}, Lcom/twitter/android/revenue/c$b;-><init>(J)V

    .line 256
    invoke-static {v0}, Lcpq;->a(Lcpv;)Lcpv;

    move-result-object v1

    .line 257
    invoke-static {p1, v0}, Lcpt;->a(Ljava/lang/Iterable;Lcpv;)Ljava/lang/Iterable;

    move-result-object v0

    .line 258
    new-instance v2, Lcom/twitter/android/revenue/c$a;

    iget v3, p0, Lcom/twitter/android/revenue/c;->i:F

    invoke-direct {v2, v0, v3}, Lcom/twitter/android/revenue/c$a;-><init>(Ljava/lang/Iterable;F)V

    .line 260
    invoke-static {p1, v1}, Lcpt;->a(Ljava/lang/Iterable;Lcpv;)Ljava/lang/Iterable;

    move-result-object v0

    .line 261
    invoke-static {v0, v2}, Lcpt;->a(Ljava/lang/Iterable;Lcpv;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcpt;->c(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 263
    iget-object v1, p0, Lcom/twitter/android/revenue/c;->h:Lcom/twitter/android/revenue/d;

    invoke-virtual {v1, v0, p2, p3}, Lcom/twitter/android/revenue/d;->a(Ljava/util/List;J)Ljava/util/concurrent/Future;

    .line 265
    invoke-static {p1}, Lcom/twitter/util/collection/MutableSet;->a(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v1

    .line 266
    invoke-interface {v1, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 267
    invoke-interface {v1, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 268
    return-object v1
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 126
    iget v0, p0, Lcom/twitter/android/revenue/c;->k:I

    iget-object v1, p0, Lcom/twitter/android/revenue/c;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/android/revenue/a;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/twitter/android/revenue/c;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/revenue/a;

    return-object v0
.end method

.method a(Ljava/util/List;J)Ljava/util/List;
    .locals 6
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/revenue/a;",
            ">;J)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/revenue/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 231
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/collection/MutableList;->a(I)Ljava/util/List;

    move-result-object v1

    .line 232
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/revenue/a;

    .line 237
    iget-wide v4, v0, Lcom/twitter/android/revenue/a;->c:J

    cmp-long v3, v4, p2

    if-eqz v3, :cond_1

    .line 238
    invoke-virtual {v0}, Lcom/twitter/android/revenue/a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, v0, Lcom/twitter/android/revenue/a;->d:Ljava/lang/String;

    .line 239
    invoke-static {v3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 240
    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 244
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/revenue/c;->h:Lcom/twitter/android/revenue/d;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/android/revenue/d;->b(J)Ljava/util/concurrent/Future;

    .line 245
    return-object v1
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 122
    iput p1, p0, Lcom/twitter/android/revenue/c;->k:I

    .line 123
    return-void
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 204
    if-eqz p1, :cond_0

    .line 205
    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/c;->b(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/revenue/c;->a(Ljava/util/List;)V

    .line 207
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/provider/t;Lapb;)V
    .locals 4

    .prologue
    .line 325
    iget-boolean v0, p0, Lcom/twitter/android/revenue/c;->b:Z

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/twitter/android/revenue/c;->h:Lcom/twitter/android/revenue/d;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/revenue/d;->a(Lcom/twitter/library/provider/t;Lapb;)Landroid/database/Cursor;

    move-result-object v1

    .line 327
    if-eqz v1, :cond_0

    .line 329
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/twitter/android/revenue/c;->b(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v0

    .line 330
    iget-object v2, p0, Lcom/twitter/android/revenue/c;->g:Landroid/os/Handler;

    new-instance v3, Lcom/twitter/android/revenue/c$1;

    invoke-direct {v3, p0, v0}, Lcom/twitter/android/revenue/c$1;-><init>(Lcom/twitter/android/revenue/c;Ljava/util/List;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 337
    invoke-static {v1}, Lcqc;->a(Landroid/database/Cursor;)V

    .line 341
    :cond_0
    return-void

    .line 337
    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcqc;->a(Landroid/database/Cursor;)V

    throw v0
.end method

.method a(Ljava/lang/Iterable;J)V
    .locals 6
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/android/revenue/a;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 275
    iget-object v0, p0, Lcom/twitter/android/revenue/c;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 276
    iget-object v0, p0, Lcom/twitter/android/revenue/c;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 277
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/revenue/a;

    .line 278
    invoke-virtual {v0}, Lcom/twitter/android/revenue/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 279
    iget-object v1, p0, Lcom/twitter/android/revenue/c;->c:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 280
    :cond_1
    iget-wide v4, v0, Lcom/twitter/android/revenue/a;->c:J

    cmp-long v1, v4, p2

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/twitter/android/revenue/a;->d:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 281
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/revenue/c;->d:Ljava/util/Map;

    iget-object v3, v0, Lcom/twitter/android/revenue/a;->d:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 282
    iget-object v1, p0, Lcom/twitter/android/revenue/c;->d:Ljava/util/Map;

    iget-object v3, v0, Lcom/twitter/android/revenue/a;->d:Ljava/lang/String;

    new-instance v4, Ljava/util/PriorityQueue;

    invoke-direct {v4}, Ljava/util/PriorityQueue;-><init>()V

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    :cond_3
    iget-object v1, p0, Lcom/twitter/android/revenue/c;->d:Ljava/util/Map;

    iget-object v3, v0, Lcom/twitter/android/revenue/a;->d:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 287
    :cond_4
    return-void
.end method

.method a(Ljava/util/List;)V
    .locals 5
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/revenue/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 212
    iget-boolean v0, p0, Lcom/twitter/android/revenue/c;->b:Z

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 213
    const-wide/16 v0, -0x1

    .line 214
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/revenue/a;

    .line 215
    iget-wide v0, v0, Lcom/twitter/android/revenue/a;->c:J

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    move-wide v2, v0

    .line 216
    goto :goto_0

    .line 218
    :cond_0
    invoke-virtual {p0, p1, v2, v3}, Lcom/twitter/android/revenue/c;->a(Ljava/util/List;J)Ljava/util/List;

    move-result-object v0

    .line 220
    invoke-direct {p0, v0, v2, v3}, Lcom/twitter/android/revenue/c;->b(Ljava/util/List;J)Ljava/lang/Iterable;

    move-result-object v0

    .line 222
    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/android/revenue/c;->a(Ljava/lang/Iterable;J)V

    .line 224
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/revenue/c;->b:Z

    .line 226
    :cond_1
    return-void
.end method

.method public b()J
    .locals 2

    .prologue
    .line 133
    iget-wide v0, p0, Lcom/twitter/android/revenue/c;->j:J

    return-wide v0
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/revenue/c;->a(Ljava/lang/String;Z)Lcom/twitter/android/revenue/a;

    move-result-object v0

    .line 148
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/revenue/a;->f()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b(Landroid/database/Cursor;)Ljava/util/List;
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/revenue/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 295
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/collection/MutableList;->a(I)Ljava/util/List;

    move-result-object v0

    .line 296
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 298
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/revenue/c;->f:Lcom/twitter/android/revenue/b;

    invoke-virtual {v1, p1}, Lcom/twitter/android/revenue/b;->a(Landroid/database/Cursor;)Lcom/twitter/android/revenue/a;

    move-result-object v1

    .line 299
    if-eqz v1, :cond_1

    .line 300
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 302
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 304
    :cond_2
    return-object v0
.end method

.method public synthetic b_(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 43
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/c;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/android/revenue/a;
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/revenue/c;->a(Ljava/lang/String;Z)Lcom/twitter/android/revenue/a;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 312
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/revenue/c;->b:Z

    .line 313
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/revenue/c;->j:J

    .line 314
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 356
    iget-object v0, p0, Lcom/twitter/android/revenue/c;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/revenue/a;

    .line 357
    if-eqz v0, :cond_0

    .line 358
    iget-object v1, p0, Lcom/twitter/android/revenue/c;->e:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 359
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/revenue/a;->a(J)V

    .line 360
    iget-object v1, p0, Lcom/twitter/android/revenue/c;->h:Lcom/twitter/android/revenue/d;

    invoke-virtual {v1, v0}, Lcom/twitter/android/revenue/d;->a(Lcom/twitter/android/revenue/a;)Ljava/util/concurrent/Future;

    .line 362
    :cond_0
    return-void
.end method
