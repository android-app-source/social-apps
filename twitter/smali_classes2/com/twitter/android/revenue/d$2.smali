.class Lcom/twitter/android/revenue/d$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/revenue/d;->a(J)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Lcom/twitter/android/revenue/d;


# direct methods
.method constructor <init>(Lcom/twitter/android/revenue/d;J)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/twitter/android/revenue/d$2;->b:Lcom/twitter/android/revenue/d;

    iput-wide p2, p0, Lcom/twitter/android/revenue/d$2;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 70
    iget-object v0, p0, Lcom/twitter/android/revenue/d$2;->b:Lcom/twitter/android/revenue/d;

    invoke-static {v0}, Lcom/twitter/android/revenue/d;->a(Lcom/twitter/android/revenue/d;)Lcom/twitter/library/provider/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v0

    const-class v1, Layq;

    invoke-interface {v0, v1}, Lcom/twitter/database/schema/TwitterSchema;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v0

    .line 71
    const-string/jumbo v1, "ad_id"

    invoke-static {v1}, Laux;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/twitter/android/revenue/d$2;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/twitter/database/model/m;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    move-result v0

    .line 72
    if-gez v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/twitter/android/revenue/d$2;->b:Lcom/twitter/android/revenue/d;

    const-string/jumbo v1, "startDeleteAd"

    invoke-static {v0, v1}, Lcom/twitter/android/revenue/d;->a(Lcom/twitter/android/revenue/d;Ljava/lang/String;)Lcpb;

    move-result-object v0

    const-string/jumbo v1, "message"

    const-string/jumbo v2, "Failed to delete ad"

    .line 74
    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    const-string/jumbo v1, "ad_id"

    iget-wide v2, p0, Lcom/twitter/android/revenue/d$2;->a:J

    .line 75
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    .line 73
    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    .line 77
    :cond_0
    return-void
.end method
