.class public Lcom/twitter/android/revenue/p;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/revenue/h;
.implements Lcom/twitter/library/widget/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/revenue/p$b;,
        Lcom/twitter/android/revenue/p$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/android/revenue/h",
        "<",
        "Lafu;",
        ">;",
        "Lcom/twitter/library/widget/b;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field private final b:Landroid/support/v4/view/ViewPager;

.field private final c:Lcom/twitter/android/widget/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/widget/d",
            "<",
            "Lafu;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/twitter/android/revenue/p$a;

.field private e:I

.field private f:Lcom/twitter/library/widget/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/library/view/d;Lbsp;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/model/core/Tweet;",
            "Lcom/twitter/library/widget/renderablecontent/DisplayMode;",
            "Lcom/twitter/library/view/d;",
            "Lbsp",
            "<",
            "Lbsq;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/revenue/p;->e:I

    .line 65
    sget-object v0, Lcom/twitter/library/widget/a;->j:Lcom/twitter/library/widget/a;

    iput-object v0, p0, Lcom/twitter/android/revenue/p;->f:Lcom/twitter/library/widget/a;

    .line 71
    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/p;->a(Landroid/content/Context;)Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/p;->a:Landroid/view/ViewGroup;

    .line 73
    iget-object v0, p0, Lcom/twitter/android/revenue/p;->a:Landroid/view/ViewGroup;

    const/high16 v1, 0x60000

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    .line 76
    sget-object v0, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    invoke-virtual {p3, v0}, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    invoke-static {p1}, Lcom/twitter/android/client/k;->a(Landroid/content/Context;)Lcom/twitter/android/client/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/k;->a()Z

    move-result v6

    .line 83
    :goto_0
    new-instance v0, Lcom/twitter/android/revenue/p$b;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/revenue/p$b;-><init>(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/library/view/d;Lbsp;Z)V

    .line 85
    new-instance v1, Lcom/twitter/android/widget/d;

    invoke-direct {v1, p1, v0}, Lcom/twitter/android/widget/d;-><init>(Landroid/content/Context;Lcom/twitter/android/widget/e;)V

    iput-object v1, p0, Lcom/twitter/android/revenue/p;->c:Lcom/twitter/android/widget/d;

    .line 87
    iget-object v0, p0, Lcom/twitter/android/revenue/p;->a:Landroid/view/ViewGroup;

    const v1, 0x7f130378

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    .line 88
    iget-object v1, p0, Lcom/twitter/android/revenue/p;->c:Lcom/twitter/android/widget/d;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 89
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 90
    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 92
    new-instance v1, Lcom/twitter/android/revenue/p$a;

    invoke-direct {v1, p2, p5}, Lcom/twitter/android/revenue/p$a;-><init>(Lcom/twitter/model/core/Tweet;Lbsp;)V

    iput-object v1, p0, Lcom/twitter/android/revenue/p;->d:Lcom/twitter/android/revenue/p$a;

    .line 93
    iget-object v1, p0, Lcom/twitter/android/revenue/p;->d:Lcom/twitter/android/revenue/p$a;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 95
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 96
    const v2, 0x7f0e0050

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 97
    invoke-static {p3, v1}, Lcom/twitter/android/revenue/g;->a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;Landroid/content/res/Resources;)I

    move-result v3

    .line 98
    invoke-virtual {v0, v3, v7, v2, v7}, Landroid/support/v4/view/ViewPager;->setPadding(IIII)V

    .line 101
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v4, v4

    .line 102
    int-to-float v3, v3

    sub-float v3, v4, v3

    int-to-float v2, v2

    sub-float v2, v3, v2

    iget-object v3, p0, Lcom/twitter/android/revenue/p;->c:Lcom/twitter/android/widget/d;

    .line 103
    invoke-virtual {v3, v7}, Lcom/twitter/android/widget/d;->getPageWidth(I)F

    move-result v3

    mul-float/2addr v2, v3

    const/high16 v3, 0x41a00000    # 20.0f

    sub-float/2addr v2, v3

    .line 104
    iget v3, p2, Lcom/twitter/model/core/Tweet;->N:I

    invoke-static {v3}, Lcom/twitter/model/util/e;->a(I)Z

    move-result v3

    .line 105
    invoke-static {}, Lcom/twitter/android/revenue/k;->h()Lcom/twitter/library/av/model/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/av/model/b;->a()F

    move-result v4

    .line 106
    if-eqz v3, :cond_0

    .line 108
    :cond_0
    if-eqz v3, :cond_2

    .line 109
    invoke-static {v1, v2, v4, v8}, Lcom/twitter/android/revenue/g;->a(Landroid/content/res/Resources;FFI)F

    move-result v1

    .line 112
    :goto_1
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setMinimumHeight(I)V

    .line 113
    iput-object v0, p0, Lcom/twitter/android/revenue/p;->b:Landroid/support/v4/view/ViewPager;

    .line 114
    return-void

    .line 80
    :cond_1
    const/4 v6, 0x1

    goto/16 :goto_0

    .line 111
    :cond_2
    invoke-static {p1}, Lcom/twitter/util/d;->f(Landroid/content/Context;)Z

    move-result v3

    .line 110
    invoke-static {v1, v2, v4, v8, v3}, Lcom/twitter/android/revenue/g;->a(Landroid/content/res/Resources;FFIZ)F

    move-result v1

    goto :goto_1
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/twitter/android/revenue/p;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    return v0
.end method

.method a(Landroid/content/Context;)Landroid/view/ViewGroup;
    .locals 4

    .prologue
    .line 118
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 119
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04040f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/twitter/android/revenue/p;->d:Lcom/twitter/android/revenue/p$a;

    invoke-virtual {v0, p1}, Lcom/twitter/android/revenue/p$a;->a(I)V

    .line 132
    iget-object v0, p0, Lcom/twitter/android/revenue/p;->b:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 133
    return-void
.end method

.method public a(Lcbi;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lafu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 124
    iget-object v0, p0, Lcom/twitter/android/revenue/p;->c:Lcom/twitter/android/widget/d;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/d;->a(Lcbi;)V

    .line 126
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/revenue/p;->e:I

    .line 127
    return-void
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/twitter/android/revenue/p;->a:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/twitter/android/revenue/p;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->clearOnPageChangeListeners()V

    .line 149
    return-void
.end method

.method public getAutoPlayableItem()Lcom/twitter/library/widget/a;
    .locals 2

    .prologue
    .line 154
    iget v0, p0, Lcom/twitter/android/revenue/p;->e:I

    iget-object v1, p0, Lcom/twitter/android/revenue/p;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 155
    iget-object v0, p0, Lcom/twitter/android/revenue/p;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/revenue/p;->e:I

    .line 156
    iget-object v0, p0, Lcom/twitter/android/revenue/p;->b:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/twitter/android/revenue/p;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 157
    instance-of v1, v0, Lcom/twitter/library/widget/TweetView;

    if-eqz v1, :cond_1

    .line 158
    check-cast v0, Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->getAutoPlayableItem()Lcom/twitter/library/widget/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/p;->f:Lcom/twitter/library/widget/a;

    .line 163
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/revenue/p;->f:Lcom/twitter/library/widget/a;

    return-object v0

    .line 160
    :cond_1
    sget-object v0, Lcom/twitter/library/widget/a;->j:Lcom/twitter/library/widget/a;

    iput-object v0, p0, Lcom/twitter/android/revenue/p;->f:Lcom/twitter/library/widget/a;

    goto :goto_0
.end method
