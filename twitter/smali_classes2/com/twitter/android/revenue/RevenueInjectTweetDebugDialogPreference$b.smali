.class final Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$b;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/Runnable;


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 132
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 133
    iput-object p1, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$b;->a:Landroid/content/Context;

    .line 134
    iput-object p2, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$b;->b:Ljava/lang/Runnable;

    .line 135
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Ljava/lang/Runnable;Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$1;)V
    .locals 0

    .prologue
    .line 126
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$b;-><init>(Landroid/content/Context;Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 8

    .prologue
    .line 139
    invoke-static {p1}, Lcom/twitter/util/collection/CollectionUtils;->a([Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 170
    :goto_0
    return-object v0

    .line 144
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$b;->a:Landroid/content/Context;

    .line 145
    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    const-class v1, Lcom/twitter/model/core/ac;

    invoke-static {v0, v1}, Lcom/twitter/model/json/common/e;->a(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac;

    .line 146
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 147
    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v4

    .line 150
    const-string/jumbo v1, "timeline"

    const-string/jumbo v5, "data_id"

    invoke-virtual {v0}, Lcom/twitter/model/core/ac;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v1, v5, v6}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)I

    .line 152
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v5

    .line 153
    new-instance v1, Lcom/twitter/model/timeline/ah$a;

    invoke-direct {v1}, Lcom/twitter/model/timeline/ah$a;-><init>()V

    .line 154
    invoke-virtual {v1, v0}, Lcom/twitter/model/timeline/ah$a;->a(Lcom/twitter/model/core/ac;)Lcom/twitter/model/timeline/ah$a;

    move-result-object v1

    const/4 v6, 0x0

    .line 155
    invoke-virtual {v1, v6}, Lcom/twitter/model/timeline/ah$a;->a(I)Lcom/twitter/model/timeline/y$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/timeline/ah$a;

    iget-wide v6, v0, Lcom/twitter/model/core/ac;->M:J

    .line 156
    invoke-virtual {v1, v6, v7}, Lcom/twitter/model/timeline/ah$a;->b(J)Lcom/twitter/model/timeline/y$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/timeline/ah$a;

    .line 157
    invoke-virtual {v0}, Lcom/twitter/model/core/ac;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/model/timeline/ah$a;->c(Ljava/lang/String;)Lcom/twitter/model/timeline/y$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/ah$a;

    .line 158
    invoke-virtual {v0}, Lcom/twitter/model/timeline/ah$a;->q()Ljava/lang/Object;

    move-result-object v0

    .line 153
    invoke-virtual {v5, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 161
    invoke-virtual {v5}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/library/provider/s$a;->a(Ljava/util/List;)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 162
    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/provider/s$a;->a(J)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 163
    invoke-static {}, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/s$a;->a(I)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 164
    invoke-virtual {v0}, Lcom/twitter/library/provider/s$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/s;

    .line 160
    invoke-virtual {v4, v0}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/library/provider/s;)I

    .line 166
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 167
    :catch_0
    move-exception v0

    .line 170
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto/16 :goto_0
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 175
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v0, p1, :cond_0

    .line 176
    iget-object v0, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$b;->a:Landroid/content/Context;

    const-string/jumbo v1, "Tweet Injected!"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$b;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 179
    iget-object v0, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$b;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 181
    :cond_1
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 126
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$b;->a([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 126
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$b;->a(Ljava/lang/Boolean;)V

    return-void
.end method
