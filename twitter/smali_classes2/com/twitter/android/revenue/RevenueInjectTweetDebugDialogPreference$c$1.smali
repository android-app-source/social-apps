.class Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$c$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lbwa;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$c;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbwa",
        "<",
        "Lcom/mopub/nativeads/NativeAd;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lbwb;

.field final synthetic b:Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$c;


# direct methods
.method constructor <init>(Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$c;Lbwb;)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$c$1;->b:Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$c;

    iput-object p2, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$c$1;->a:Lbwb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/mopub/nativeads/NativeAd;)V
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$c$1;->a:Lbwb;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 290
    return-void
.end method

.method public a(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 294
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Failed to load rtb ad: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 295
    iget-object v1, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$c$1;->b:Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$c;

    invoke-static {v1}, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$c;->a(Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$c;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 296
    iget-object v0, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$c$1;->a:Lbwb;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 297
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 286
    check-cast p1, Lcom/mopub/nativeads/NativeAd;

    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$c$1;->a(Lcom/mopub/nativeads/NativeAd;)V

    return-void
.end method
