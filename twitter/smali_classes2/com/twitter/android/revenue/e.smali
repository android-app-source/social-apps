.class public Lcom/twitter/android/revenue/e;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field final a:Lcpp;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcpp",
            "<",
            "Lcom/twitter/library/widget/TweetView;",
            "Lcom/twitter/android/timeline/i;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/library/widget/TweetView;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/android/revenue/c;

.field private final d:Lcpv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcpv",
            "<",
            "Lcom/twitter/library/widget/TweetView;",
            ">;"
        }
    .end annotation
.end field

.field private e:J


# direct methods
.method public constructor <init>(Lcom/twitter/android/revenue/c;)V
    .locals 2

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Lcom/twitter/android/revenue/e$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/revenue/e$1;-><init>(Lcom/twitter/android/revenue/e;)V

    iput-object v0, p0, Lcom/twitter/android/revenue/e;->a:Lcpp;

    .line 48
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/e;->b:Ljava/util/Set;

    .line 53
    new-instance v0, Lcom/twitter/android/revenue/e$2;

    invoke-direct {v0, p0}, Lcom/twitter/android/revenue/e$2;-><init>(Lcom/twitter/android/revenue/e;)V

    iput-object v0, p0, Lcom/twitter/android/revenue/e;->d:Lcpv;

    .line 61
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/twitter/android/revenue/e;->e:J

    .line 64
    iput-object p1, p0, Lcom/twitter/android/revenue/e;->c:Lcom/twitter/android/revenue/c;

    .line 65
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/revenue/e;)Lcom/twitter/android/revenue/c;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/twitter/android/revenue/e;->c:Lcom/twitter/android/revenue/c;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/library/widget/TweetView;)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/android/revenue/e;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 69
    return-void
.end method

.method public a()Z
    .locals 4

    .prologue
    .line 75
    iget-object v0, p0, Lcom/twitter/android/revenue/e;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/twitter/android/revenue/e;->e:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 79
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/twitter/android/revenue/e;->e:J

    .line 80
    return-void
.end method

.method public c()V
    .locals 6

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/android/revenue/e;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 89
    new-instance v0, Lcom/twitter/android/revenue/e$3;

    invoke-direct {v0, p0}, Lcom/twitter/android/revenue/e$3;-><init>(Lcom/twitter/android/revenue/e;)V

    .line 99
    iget-object v1, p0, Lcom/twitter/android/revenue/e;->b:Ljava/util/Set;

    .line 100
    invoke-static {v1, v0}, Lcpt;->b(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v0

    .line 101
    invoke-static {}, Lcom/twitter/util/object/ObjectUtils;->b()Ljava/util/Comparator;

    move-result-object v1

    .line 99
    invoke-static {v0, v1}, Lcpt;->a(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 104
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/android/revenue/e;->e:J

    .line 106
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/twitter/android/revenue/e;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/twitter/android/revenue/e;->b:Ljava/util/Set;

    iget-object v1, p0, Lcom/twitter/android/revenue/e;->d:Lcpv;

    .line 114
    invoke-static {v0, v1}, Lcpt;->a(Ljava/lang/Iterable;Lcpv;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcpt;->c(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 116
    iget-object v1, p0, Lcom/twitter/android/revenue/e;->a:Lcpp;

    invoke-static {v0, v1}, Lcpt;->a(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-static {v1}, Lcpt;->c(Ljava/lang/Iterable;)Ljava/util/List;

    .line 119
    iget-object v1, p0, Lcom/twitter/android/revenue/e;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 121
    :cond_0
    return-void
.end method

.method public e()Lcno$a;
    .locals 1

    .prologue
    .line 125
    new-instance v0, Lcom/twitter/android/revenue/e$4;

    invoke-direct {v0, p0}, Lcom/twitter/android/revenue/e$4;-><init>(Lcom/twitter/android/revenue/e;)V

    return-object v0
.end method
