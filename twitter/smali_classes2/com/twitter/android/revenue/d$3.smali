.class Lcom/twitter/android/revenue/d$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/revenue/d;->b(J)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Lcom/twitter/android/revenue/d;


# direct methods
.method constructor <init>(Lcom/twitter/android/revenue/d;J)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/twitter/android/revenue/d$3;->b:Lcom/twitter/android/revenue/d;

    iput-wide p2, p0, Lcom/twitter/android/revenue/d$3;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 86
    iget-object v0, p0, Lcom/twitter/android/revenue/d$3;->b:Lcom/twitter/android/revenue/d;

    invoke-static {v0}, Lcom/twitter/android/revenue/d;->a(Lcom/twitter/android/revenue/d;)Lcom/twitter/library/provider/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v0

    const-class v1, Layq;

    invoke-interface {v0, v1}, Lcom/twitter/database/schema/TwitterSchema;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v0

    .line 87
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "assigned_slot_id IS NULL AND slot_id IS NULL AND created_at<"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/revenue/d$3;->a:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/twitter/database/model/m;->a(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 91
    if-gez v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/twitter/android/revenue/d$3;->b:Lcom/twitter/android/revenue/d;

    const-string/jumbo v1, "startDeleteExpiredAndUnusedAds"

    invoke-static {v0, v1}, Lcom/twitter/android/revenue/d;->a(Lcom/twitter/android/revenue/d;Ljava/lang/String;)Lcpb;

    move-result-object v0

    const-string/jumbo v1, "message"

    const-string/jumbo v2, "Failed to delete ads with old timestamp"

    .line 93
    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    .line 92
    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    .line 95
    :cond_0
    return-void
.end method
