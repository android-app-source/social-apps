.class public Lcom/twitter/android/revenue/k;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Lcom/twitter/ui/view/h;)Lcom/twitter/ui/view/h;
    .locals 2

    .prologue
    .line 74
    new-instance v0, Lcom/twitter/ui/view/h$a;

    invoke-direct {v0}, Lcom/twitter/ui/view/h$a;-><init>()V

    iget-boolean v1, p0, Lcom/twitter/ui/view/h;->b:Z

    .line 75
    invoke-virtual {v0, v1}, Lcom/twitter/ui/view/h$a;->b(Z)Lcom/twitter/ui/view/h$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/ui/view/h;->c:Z

    .line 76
    invoke-virtual {v0, v1}, Lcom/twitter/ui/view/h$a;->c(Z)Lcom/twitter/ui/view/h$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/ui/view/h;->g:Z

    .line 77
    invoke-virtual {v0, v1}, Lcom/twitter/ui/view/h$a;->g(Z)Lcom/twitter/ui/view/h$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/ui/view/h;->i:Z

    .line 78
    invoke-virtual {v0, v1}, Lcom/twitter/ui/view/h$a;->i(Z)Lcom/twitter/ui/view/h$a;

    move-result-object v0

    const/4 v1, 0x1

    .line 79
    invoke-virtual {v0, v1}, Lcom/twitter/ui/view/h$a;->j(Z)Lcom/twitter/ui/view/h$a;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lcom/twitter/ui/view/h$a;->a()Lcom/twitter/ui/view/h;

    move-result-object v0

    .line 74
    return-object v0
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 34
    const-string/jumbo v0, "ad_formats_tweet_view_dwell_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 25
    invoke-static {p2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    invoke-static {p0, p1}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 25
    :goto_0
    return v0

    .line 26
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcar;Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcar;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 30
    invoke-static {p1, p0}, Lcas;->a(Ljava/util/List;Lcar;)Lcas;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()F
    .locals 2

    .prologue
    .line 38
    const-string/jumbo v0, "ad_formats_tweet_view_visibility_threshold"

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-static {v0, v1}, Lcoj;->a(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public static c()D
    .locals 4

    .prologue
    .line 43
    const-string/jumbo v0, "ad_formats_tweet_view_dwell_threshold"

    const-wide v2, 0x3f847ae147ae147bL    # 0.01

    invoke-static {v0, v2, v3}, Lcoj;->a(Ljava/lang/String;D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static d()Z
    .locals 1

    .prologue
    .line 48
    const-string/jumbo v0, "ad_formats_media_tweet_dwell_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static e()Z
    .locals 1

    .prologue
    .line 52
    const-string/jumbo v0, "ad_formats_qualified_tweet_dwell_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static f()I
    .locals 1

    .prologue
    .line 56
    const v0, 0x7f04040e

    return v0
.end method

.method public static g()I
    .locals 1

    .prologue
    .line 64
    const v0, 0x7f0a06fa

    return v0
.end method

.method public static h()Lcom/twitter/library/av/model/b;
    .locals 1

    .prologue
    .line 69
    const v0, 0x3ff47ae1    # 1.91f

    invoke-static {v0}, Lcom/twitter/library/av/model/b;->a(F)Lcom/twitter/library/av/model/b;

    move-result-object v0

    return-object v0
.end method

.method public static i()Z
    .locals 2

    .prologue
    .line 84
    const-string/jumbo v0, "ad_formats_promoted_account_redesign_android_4569"

    const-string/jumbo v1, "wtf_view_large_button"

    invoke-static {v0, v1}, Lcoi;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static j()Z
    .locals 1

    .prologue
    .line 89
    const-string/jumbo v0, "ad_formats_profile_wtf_redesign_android_4637"

    invoke-static {v0}, Lcoi;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static k()I
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x2

    return v0
.end method
