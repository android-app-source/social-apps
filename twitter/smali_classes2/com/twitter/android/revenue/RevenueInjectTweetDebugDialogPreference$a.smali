.class final Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$a;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcpv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcpv",
            "<",
            "Lcom/twitter/model/timeline/y;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcpv;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcpv",
            "<",
            "Lcom/twitter/model/timeline/y;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 190
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 191
    iput-object p1, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$a;->a:Landroid/content/Context;

    .line 192
    iput-object p2, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$a;->b:Lcpv;

    .line 193
    iput-object p3, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$a;->c:Ljava/lang/String;

    .line 194
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 4

    .prologue
    .line 198
    invoke-static {p1}, Lcom/twitter/util/collection/CollectionUtils;->a([Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 249
    :goto_0
    return-object v0

    .line 203
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$a;->a:Landroid/content/Context;

    .line 204
    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference;->a(Ljava/io/InputStream;)Lcom/twitter/library/api/t;

    move-result-object v0

    .line 206
    iget-object v0, v0, Lcom/twitter/library/api/t;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$a;->b:Lcpv;

    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;Lcpv;)Ljava/util/List;

    move-result-object v0

    .line 207
    new-instance v1, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$a$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$a$1;-><init>(Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$a;)V

    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/List;Lcpp;)Ljava/util/List;

    move-result-object v0

    .line 238
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 239
    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v1

    .line 240
    invoke-static {v0}, Lcom/twitter/library/provider/s$a;->a(Ljava/util/List;)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 241
    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/provider/s$a;->a(J)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 242
    invoke-static {}, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/provider/s$a;->a(I)Lcom/twitter/library/provider/s$a;

    move-result-object v0

    .line 243
    invoke-virtual {v0}, Lcom/twitter/library/provider/s$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/s;

    .line 240
    invoke-virtual {v1, v0}, Lcom/twitter/library/provider/t;->a(Lcom/twitter/library/provider/s;)I

    .line 245
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 246
    :catch_0
    move-exception v0

    .line 249
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 254
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v0, p1, :cond_0

    .line 255
    iget-object v0, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$a;->c:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 257
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 184
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$a;->a([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 184
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$a;->a(Ljava/lang/Boolean;)V

    return-void
.end method
