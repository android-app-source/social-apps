.class public Lcom/twitter/android/revenue/card/r;
.super Lcom/twitter/android/card/n;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/card/ak$a;
.implements Lcom/twitter/library/card/q$a;


# instance fields
.field private A:Lcas;

.field private C:Z

.field private final D:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

.field protected a:Landroid/view/ViewGroup;

.field protected b:Lcom/twitter/media/ui/image/MediaImageView;

.field protected c:Lcom/twitter/media/ui/image/MediaImageView;

.field protected d:Landroid/widget/TextView;

.field protected e:Ljava/lang/Long;

.field protected f:Landroid/view/ViewGroup;

.field protected g:Lcom/twitter/library/util/t;

.field protected h:Lcom/twitter/library/util/s;

.field protected i:Lcom/twitter/library/util/s;

.field private final j:Landroid/widget/TextView;

.field private final k:Landroid/widget/TextView;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Lcom/twitter/ui/widget/ProgressLayout;

.field private o:Landroid/widget/TextView;

.field private p:Lcom/twitter/ui/widget/TwitterButton;

.field private z:Z


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V
    .locals 2

    .prologue
    .line 80
    new-instance v0, Lcom/twitter/android/card/f;

    invoke-direct {v0, p1}, Lcom/twitter/android/card/f;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/twitter/android/card/c;

    invoke-direct {v1, p1}, Lcom/twitter/android/card/c;-><init>(Landroid/app/Activity;)V

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/twitter/android/revenue/card/r;-><init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V

    .line 81
    return-void
.end method

.method constructor <init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V
    .locals 3

    .prologue
    .line 85
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/card/n;-><init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/revenue/card/r;->C:Z

    .line 87
    iput-object p2, p0, Lcom/twitter/android/revenue/card/r;->D:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 88
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/r;->e()Landroid/view/View;

    move-result-object v1

    .line 89
    const v0, 0x7f13057a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/r;->c:Lcom/twitter/media/ui/image/MediaImageView;

    .line 90
    const v0, 0x7f13057b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/r;->d:Landroid/widget/TextView;

    .line 91
    const v0, 0x7f13057d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/r;->b:Lcom/twitter/media/ui/image/MediaImageView;

    .line 92
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->b:Lcom/twitter/media/ui/image/MediaImageView;

    const/high16 v2, 0x40200000    # 2.5f

    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 94
    const v0, 0x7f13057e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/r;->j:Landroid/widget/TextView;

    .line 95
    const v0, 0x7f13057f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/r;->k:Landroid/widget/TextView;

    .line 97
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/r;->f()V

    .line 98
    return-void
.end method

.method static a(Ljava/lang/String;Lcar;)I
    .locals 2

    .prologue
    .line 273
    const/16 v0, 0x25

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 274
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 275
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 276
    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/revenue/card/r;)Lcom/twitter/library/card/CardContext;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->w:Lcom/twitter/library/card/CardContext;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/revenue/card/r;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/twitter/android/revenue/card/r;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 345
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/r;->l()Landroid/app/Activity;

    move-result-object v0

    .line 346
    if-nez v0, :cond_0

    .line 356
    :goto_0
    return-void

    .line 350
    :cond_0
    invoke-static {}, Lcom/twitter/android/card/h;->b()Lcom/twitter/android/card/h;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/revenue/card/r;->y:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/card/h;->a(J)V

    .line 351
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 352
    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v2

    const/4 v3, 0x0

    .line 353
    invoke-virtual {v2, p1, v3}, Lcom/twitter/android/composer/a;->a(Ljava/lang/String;[I)Lcom/twitter/android/composer/a;

    move-result-object v2

    .line 354
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/twitter/android/composer/a;->b(Ljava/lang/String;)Lcom/twitter/android/composer/a;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/revenue/card/r;->y:J

    .line 355
    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/composer/a;->c(J)Lcom/twitter/android/composer/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    const/16 v2, 0x2711

    .line 352
    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/revenue/card/r;)Lcom/twitter/android/card/CardActionHelper;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->v:Lcom/twitter/android/card/CardActionHelper;

    return-object v0
.end method

.method private b(Lcar;)Z
    .locals 2

    .prologue
    .line 204
    const-string/jumbo v0, "unlocked"

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/twitter/library/card/g;->a(Ljava/lang/String;Lcar;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/revenue/card/r;)Lcom/twitter/library/card/CardContext;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->w:Lcom/twitter/library/card/CardContext;

    return-object v0
.end method

.method private c(Lcar;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 209
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->D:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/r;->C:Z

    if-nez v0, :cond_1

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 211
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 218
    :goto_0
    return-void

    .line 213
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 214
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 215
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->j:Landroid/widget/TextView;

    const-string/jumbo v1, "share_unlocked_cta_line_1"

    invoke-static {v1, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->k:Landroid/widget/TextView;

    const-string/jumbo v1, "share_unlocked_cta_line_2"

    invoke-static {v1, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/twitter/android/revenue/card/r;)Lcom/twitter/android/card/CardActionHelper;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->v:Lcom/twitter/android/card/CardActionHelper;

    return-object v0
.end method

.method private d(Lcar;)V
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->p:Lcom/twitter/ui/widget/TwitterButton;

    const-string/jumbo v1, "forward_cta"

    invoke-static {v1, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setText(Ljava/lang/CharSequence;)V

    .line 222
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->o:Landroid/widget/TextView;

    const-string/jumbo v1, "forward_title"

    invoke-static {v1, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->g:Lcom/twitter/library/util/t;

    if-nez v0, :cond_0

    .line 225
    new-instance v0, Lcom/twitter/android/revenue/card/r$1;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/r;->p:Lcom/twitter/ui/widget/TwitterButton;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/revenue/card/r$1;-><init>(Lcom/twitter/android/revenue/card/r;Lcom/twitter/ui/widget/TwitterButton;)V

    iput-object v0, p0, Lcom/twitter/android/revenue/card/r;->g:Lcom/twitter/library/util/t;

    .line 233
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->p:Lcom/twitter/ui/widget/TwitterButton;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/r;->g:Lcom/twitter/library/util/t;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 235
    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/twitter/android/revenue/card/r;)Lcom/twitter/android/card/CardActionHelper;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->v:Lcom/twitter/android/card/CardActionHelper;

    return-object v0
.end method

.method private e(Lcar;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 238
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->o:Landroid/widget/TextView;

    const-string/jumbo v1, "footer_title"

    invoke-static {v1, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 240
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/r;->C:Z

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->l:Landroid/widget/TextView;

    const-string/jumbo v1, "unlocked_description"

    invoke-static {v1, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 242
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->p:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, v3}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 243
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->n:Lcom/twitter/ui/widget/ProgressLayout;

    invoke-virtual {v0, v3}, Lcom/twitter/ui/widget/ProgressLayout;->setVisibility(I)V

    .line 266
    :goto_0
    return-void

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->l:Landroid/widget/TextView;

    const-string/jumbo v1, "description"

    invoke-static {v1, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->n:Lcom/twitter/ui/widget/ProgressLayout;

    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/ProgressLayout;->setVisibility(I)V

    .line 248
    const-string/jumbo v0, "percentage"

    invoke-static {v0, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    .line 249
    iget-object v1, p0, Lcom/twitter/android/revenue/card/r;->m:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    iget-object v1, p0, Lcom/twitter/android/revenue/card/r;->n:Lcom/twitter/ui/widget/ProgressLayout;

    invoke-static {v0, p1}, Lcom/twitter/android/revenue/card/r;->a(Ljava/lang/String;Lcar;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/ProgressLayout;->setProgress(I)V

    .line 252
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->p:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 253
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->p:Lcom/twitter/ui/widget/TwitterButton;

    const-string/jumbo v1, "cta"

    invoke-static {v1, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setText(Ljava/lang/CharSequence;)V

    .line 255
    const-string/jumbo v0, "compose"

    invoke-static {v0, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    .line 256
    iget-object v1, p0, Lcom/twitter/android/revenue/card/r;->g:Lcom/twitter/library/util/t;

    if-nez v1, :cond_1

    .line 257
    new-instance v1, Lcom/twitter/android/revenue/card/r$2;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/r;->p:Lcom/twitter/ui/widget/TwitterButton;

    invoke-direct {v1, p0, v2, v0}, Lcom/twitter/android/revenue/card/r$2;-><init>(Lcom/twitter/android/revenue/card/r;Lcom/twitter/ui/widget/TwitterButton;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/twitter/android/revenue/card/r;->g:Lcom/twitter/library/util/t;

    .line 264
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->p:Lcom/twitter/ui/widget/TwitterButton;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/r;->g:Lcom/twitter/library/util/t;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/r;->e()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f130580

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 102
    iget-object v1, p0, Lcom/twitter/android/revenue/card/r;->D:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v2, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne v1, v2, :cond_1

    const v1, 0x7f04023c

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 104
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/r;->f:Landroid/view/ViewGroup;

    .line 105
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->f:Landroid/view/ViewGroup;

    const v1, 0x7f130217

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/r;->p:Lcom/twitter/ui/widget/TwitterButton;

    .line 106
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->f:Landroid/view/ViewGroup;

    const v1, 0x7f130218

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/r;->o:Landroid/widget/TextView;

    .line 108
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->D:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne v0, v1, :cond_0

    .line 109
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->f:Landroid/view/ViewGroup;

    const v1, 0x7f130219

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/r;->l:Landroid/widget/TextView;

    .line 110
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->f:Landroid/view/ViewGroup;

    const v1, 0x7f130583

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/r;->m:Landroid/widget/TextView;

    .line 111
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->f:Landroid/view/ViewGroup;

    const v1, 0x7f130582

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/ProgressLayout;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/r;->n:Lcom/twitter/ui/widget/ProgressLayout;

    .line 113
    :cond_0
    return-void

    .line 102
    :cond_1
    const v1, 0x7f04023d

    goto :goto_0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/r;->z:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->A:Lcas;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->D:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-eq v0, v1, :cond_1

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->b:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/r;->A:Lcas;

    iget-object v1, v1, Lcas;->a:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 179
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->b:Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setFromMemoryOnly(Z)V

    .line 180
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->b:Lcom/twitter/media/ui/image/MediaImageView;

    const-string/jumbo v1, "participated_image"

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setTag(Ljava/lang/Object;)V

    .line 181
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->b:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/r;->A:Lcas;

    const/high16 v2, 0x40200000    # 2.5f

    invoke-virtual {v1, v2}, Lcas;->a(F)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 183
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->p:Lcom/twitter/ui/widget/TwitterButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 137
    invoke-super {p0}, Lcom/twitter/android/card/n;->a()V

    .line 138
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->e:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 139
    invoke-static {}, Lcom/twitter/library/card/ak;->a()Lcom/twitter/library/card/ak;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/revenue/card/r;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/ak;->b(JLjava/lang/Object;)V

    .line 141
    :cond_0
    invoke-static {}, Lcom/twitter/library/card/q;->a()Lcom/twitter/library/card/q;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/revenue/card/r;->y:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/q;->b(JLjava/lang/Object;)V

    .line 143
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->b:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->j()Z

    .line 144
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->c:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->j()Z

    .line 145
    return-void
.end method

.method public a(JLcaq;)V
    .locals 2

    .prologue
    .line 169
    const-string/jumbo v0, "participated"

    const/4 v1, 0x0

    invoke-static {v0, p3, v1}, Lcom/twitter/library/card/g;->a(Ljava/lang/String;Lcar;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/revenue/card/r;->z:Z

    .line 170
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/r;->g()V

    .line 171
    return-void
.end method

.method public a(JLcar;)V
    .locals 2

    .prologue
    .line 188
    invoke-direct {p0, p3}, Lcom/twitter/android/revenue/card/r;->b(Lcar;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/revenue/card/r;->C:Z

    .line 190
    invoke-virtual {p0, p3}, Lcom/twitter/android/revenue/card/r;->a(Lcar;)V

    .line 191
    invoke-direct {p0, p3}, Lcom/twitter/android/revenue/card/r;->c(Lcar;)V

    .line 193
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->D:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne v0, v1, :cond_1

    .line 194
    invoke-direct {p0, p3}, Lcom/twitter/android/revenue/card/r;->d(Lcar;)V

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 195
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->D:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne v0, v1, :cond_0

    .line 196
    invoke-direct {p0, p3}, Lcom/twitter/android/revenue/card/r;->e(Lcar;)V

    .line 197
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/r;->z:Z

    if-eqz v0, :cond_0

    .line 198
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/r;->g()V

    goto :goto_0
.end method

.method public a(JLcom/twitter/model/core/TwitterUser;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 323
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->h:Lcom/twitter/library/util/s;

    if-nez v0, :cond_0

    .line 324
    new-instance v0, Lcom/twitter/android/revenue/card/r$5;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/android/revenue/card/r$5;-><init>(Lcom/twitter/android/revenue/card/r;J)V

    iput-object v0, p0, Lcom/twitter/android/revenue/card/r;->h:Lcom/twitter/library/util/s;

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 333
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->d:Landroid/widget/TextView;

    iget-object v1, p3, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 334
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->d:Landroid/widget/TextView;

    const-string/jumbo v1, "author_name"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 335
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/r;->h:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 337
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->c:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setVisibility(I)V

    .line 338
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->c:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v1, p3, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 339
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->c:Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setFromMemoryOnly(Z)V

    .line 340
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->c:Lcom/twitter/media/ui/image/MediaImageView;

    const-string/jumbo v1, "author_image"

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setTag(Ljava/lang/Object;)V

    .line 341
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->c:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/r;->h:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 342
    return-void
.end method

.method a(Lcar;)V
    .locals 5

    .prologue
    .line 280
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->D:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne v0, v1, :cond_2

    const-string/jumbo v0, "forward_image"

    .line 282
    :goto_0
    invoke-static {v0, p1}, Lcas;->a(Ljava/lang/String;Lcar;)Lcas;

    move-result-object v1

    .line 283
    const-string/jumbo v2, "player_url"

    invoke-static {v2, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v2

    .line 284
    const-string/jumbo v3, "participated_image"

    invoke-static {v3, p1}, Lcas;->a(Ljava/lang/String;Lcar;)Lcas;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/revenue/card/r;->A:Lcas;

    .line 285
    if-eqz v1, :cond_1

    .line 286
    iget-object v3, p0, Lcom/twitter/android/revenue/card/r;->b:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v4, v1, Lcas;->a:Ljava/lang/String;

    invoke-static {v4}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 287
    iget-object v3, p0, Lcom/twitter/android/revenue/card/r;->b:Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/twitter/media/ui/image/MediaImageView;->setFromMemoryOnly(Z)V

    .line 288
    iget-object v3, p0, Lcom/twitter/android/revenue/card/r;->b:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v3, v0}, Lcom/twitter/media/ui/image/MediaImageView;->setTag(Ljava/lang/Object;)V

    .line 289
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->b:Lcom/twitter/media/ui/image/MediaImageView;

    const/high16 v3, 0x40200000    # 2.5f

    invoke-virtual {v1, v3}, Lcas;->a(F)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 291
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->i:Lcom/twitter/library/util/s;

    if-nez v0, :cond_1

    .line 292
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->D:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne v0, v1, :cond_4

    .line 293
    new-instance v0, Lcom/twitter/android/revenue/card/r$3;

    invoke-direct {v0, p0}, Lcom/twitter/android/revenue/card/r$3;-><init>(Lcom/twitter/android/revenue/card/r;)V

    iput-object v0, p0, Lcom/twitter/android/revenue/card/r;->i:Lcom/twitter/library/util/s;

    .line 316
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->b:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/r;->i:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 319
    :cond_1
    return-void

    .line 280
    :cond_2
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/r;->C:Z

    if-eqz v0, :cond_3

    const-string/jumbo v0, "player_image"

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "photo_image"

    goto :goto_0

    .line 302
    :cond_4
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/r;->C:Z

    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    .line 303
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->b:Lcom/twitter/media/ui/image/MediaImageView;

    const v1, 0x7f130044

    .line 304
    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/RichImageView;

    .line 305
    const v1, 0x7f020714

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/RichImageView;->setOverlayDrawable(I)V

    .line 306
    new-instance v0, Lcom/twitter/android/revenue/card/r$4;

    invoke-direct {v0, p0, v2}, Lcom/twitter/android/revenue/card/r$4;-><init>(Lcom/twitter/android/revenue/card/r;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/revenue/card/r;->i:Lcom/twitter/library/util/s;

    goto :goto_1
.end method

.method public a(Lcom/twitter/library/card/z$a;)V
    .locals 4

    .prologue
    .line 125
    invoke-super {p0, p1}, Lcom/twitter/android/card/n;->a(Lcom/twitter/library/card/z$a;)V

    .line 126
    iget-wide v0, p1, Lcom/twitter/library/card/z$a;->b:J

    iput-wide v0, p0, Lcom/twitter/android/revenue/card/r;->y:J

    .line 127
    invoke-static {}, Lcom/twitter/library/card/q;->a()Lcom/twitter/library/card/q;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/revenue/card/r;->y:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/q;->a(JLjava/lang/Object;)V

    .line 129
    const-string/jumbo v0, "site"

    iget-object v1, p1, Lcom/twitter/library/card/z$a;->c:Lcar;

    invoke-static {v0, v1}, Lcom/twitter/library/card/y;->a(Ljava/lang/String;Lcar;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/r;->e:Ljava/lang/Long;

    .line 130
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->e:Ljava/lang/Long;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->D:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne v0, v1, :cond_0

    .line 131
    invoke-static {}, Lcom/twitter/library/card/ak;->a()Lcom/twitter/library/card/ak;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/revenue/card/r;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/ak;->a(JLjava/lang/Object;)V

    .line 133
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 44
    check-cast p1, Lcom/twitter/library/card/z$a;

    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/card/r;->a(Lcom/twitter/library/card/z$a;)V

    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 149
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->b:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setFromMemoryOnly(Z)V

    .line 150
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->c:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setFromMemoryOnly(Z)V

    .line 151
    return-void
.end method

.method public d()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 155
    invoke-super {p0}, Lcom/twitter/android/card/n;->d()V

    .line 157
    invoke-static {}, Lcom/twitter/android/card/h;->b()Lcom/twitter/android/card/h;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/revenue/card/r;->y:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/card/h;->b(J)Ljava/lang/Integer;

    move-result-object v0

    .line 158
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 159
    new-instance v0, Lcaq;

    invoke-direct {v0}, Lcaq;-><init>()V

    .line 160
    const-string/jumbo v1, "participated"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcaq;->a(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 161
    iget-wide v2, p0, Lcom/twitter/android/revenue/card/r;->y:J

    invoke-virtual {p0, v2, v3, v0}, Lcom/twitter/android/revenue/card/r;->b(JLcaq;)V

    .line 162
    iput-boolean v4, p0, Lcom/twitter/android/revenue/card/r;->z:Z

    .line 163
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/r;->g()V

    .line 165
    :cond_0
    return-void
.end method

.method public e()Landroid/view/View;
    .locals 3

    .prologue
    .line 117
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->a:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->q:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04023b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/r;->a:Landroid/view/ViewGroup;

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/r;->a:Landroid/view/ViewGroup;

    return-object v0
.end method
