.class public Lcom/twitter/android/revenue/card/l;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/revenue/card/al;


# instance fields
.field private a:Lcom/twitter/android/revenue/card/CardMediaView;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcar;F)V
    .locals 3

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Lcom/twitter/android/revenue/card/CardMediaView;

    invoke-direct {v0, p1}, Lcom/twitter/android/revenue/card/CardMediaView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/revenue/card/l;->a:Lcom/twitter/android/revenue/card/CardMediaView;

    .line 23
    invoke-direct {p0, p2}, Lcom/twitter/android/revenue/card/l;->a(Lcar;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcas;->a(Ljava/lang/String;Lcar;)Lcas;

    move-result-object v0

    .line 24
    iget-object v1, p0, Lcom/twitter/android/revenue/card/l;->a:Lcom/twitter/android/revenue/card/CardMediaView;

    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2, p3}, Lcom/twitter/android/revenue/card/l;->a(Lcom/twitter/android/revenue/card/CardMediaView;Lcas;ZF)V

    .line 25
    return-void
.end method

.method private a(Lcar;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    const-string/jumbo v0, "cover_player_image"

    invoke-virtual {p1, v0}, Lcar;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    const-string/jumbo v0, "cover_player_image"

    .line 31
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "cover_promo_image"

    goto :goto_0
.end method

.method private a(Lcom/twitter/android/revenue/card/CardMediaView;Lcas;ZF)V
    .locals 0

    .prologue
    .line 36
    if-eqz p2, :cond_0

    .line 37
    invoke-static {p1, p2, p3, p4}, Lcom/twitter/android/revenue/j;->a(Lcom/twitter/android/revenue/card/CardMediaView;Lcas;ZF)V

    .line 39
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/twitter/android/revenue/card/l;->a:Lcom/twitter/android/revenue/card/CardMediaView;

    return-object v0
.end method

.method public a(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;JLcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 0

    .prologue
    .line 76
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 66
    return-void
.end method

.method public at_()V
    .locals 0

    .prologue
    .line 48
    return-void
.end method

.method public au_()V
    .locals 0

    .prologue
    .line 52
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 80
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 84
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/twitter/android/revenue/card/l;->a:Lcom/twitter/android/revenue/card/CardMediaView;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/CardMediaView;->getMediaImageView()Lcom/twitter/media/ui/image/MediaImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setFromMemoryOnly(Z)V

    .line 89
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/twitter/android/revenue/card/l;->a:Lcom/twitter/android/revenue/card/CardMediaView;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/CardMediaView;->getMediaImageView()Lcom/twitter/media/ui/image/MediaImageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->j()Z

    .line 94
    return-void
.end method

.method public h()V
    .locals 0

    .prologue
    .line 56
    return-void
.end method

.method public i()Landroid/view/View;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/twitter/library/widget/a;->j:Lcom/twitter/library/widget/a;

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->i()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
