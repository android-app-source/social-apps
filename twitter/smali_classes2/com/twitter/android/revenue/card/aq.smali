.class public Lcom/twitter/android/revenue/card/aq;
.super Lcom/twitter/android/revenue/card/n;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/av/card/f$a;
.implements Lcom/twitter/library/widget/a;


# instance fields
.field protected d:Lcom/twitter/android/av/card/f;

.field private e:Lcom/twitter/media/ui/image/MediaImageView;

.field private f:Lcom/twitter/android/revenue/card/CardMediaView;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;Lcar;)V
    .locals 7

    .prologue
    .line 41
    invoke-direct/range {p0 .. p5}, Lcom/twitter/android/revenue/card/n;-><init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;Lcar;)V

    .line 42
    new-instance v0, Lcom/twitter/android/av/card/f;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/aq;->e:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v3, p0, Lcom/twitter/android/revenue/card/aq;->b:Landroid/view/ViewGroup;

    const v4, 0x7f13051e

    sget-object v6, Lbyo;->c:Lbyf;

    move-object v1, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/av/card/f;-><init>(Landroid/content/Context;Lcom/twitter/media/ui/image/MediaImageView;Landroid/view/ViewGroup;ILcom/twitter/android/av/card/f$a;Lbyf;)V

    iput-object v0, p0, Lcom/twitter/android/revenue/card/aq;->d:Lcom/twitter/android/av/card/f;

    .line 44
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->q:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/revenue/card/aq;->q:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/aq;->x:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 45
    invoke-static {v1, v2}, Lcom/twitter/android/revenue/j;->a(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Z

    move-result v1

    .line 44
    invoke-static {v0, v1}, Lcom/twitter/android/revenue/j;->a(Landroid/content/res/Resources;Z)[F

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/revenue/card/aq;->a([F)V

    .line 46
    return-void
.end method

.method private s()V
    .locals 8

    .prologue
    .line 88
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->w:Lcom/twitter/library/card/CardContext;

    invoke-static {v0}, Lcom/twitter/library/card/CardContext;->a(Lcom/twitter/library/card/CardContext;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    .line 89
    iget-object v2, p0, Lcom/twitter/android/revenue/card/aq;->a:Lcar;

    iget-wide v4, p0, Lcom/twitter/android/revenue/card/aq;->y:J

    iget-boolean v1, p0, Lcom/twitter/android/revenue/card/aq;->c:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    .line 90
    :goto_0
    invoke-static {v2, v4, v5, v0, v1}, Lcom/twitter/android/av/revenue/VideoConversationCardData;->a(Lcar;JLcom/twitter/model/core/Tweet;Z)Lcom/twitter/android/av/revenue/VideoConversationCardData;

    move-result-object v7

    .line 91
    const-string/jumbo v0, "player_stream_url"

    iget-object v1, p0, Lcom/twitter/android/revenue/card/aq;->a:Lcar;

    invoke-static {v0, v1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    .line 92
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->q:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/aq;->b:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/twitter/android/revenue/card/aq;->x:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 93
    invoke-static {v3}, Lcom/twitter/android/card/g;->a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/revenue/card/aq;->r:Lcom/twitter/android/card/d;

    iget-object v5, p0, Lcom/twitter/android/revenue/card/aq;->f:Lcom/twitter/android/revenue/card/CardMediaView;

    iget-object v6, p0, Lcom/twitter/android/revenue/card/aq;->w:Lcom/twitter/library/card/CardContext;

    .line 92
    invoke-static/range {v0 .. v7}, Lcom/twitter/android/revenue/card/ap;->a(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;Ljava/lang/String;Lcom/twitter/android/card/d;Landroid/view/View;Lcom/twitter/library/card/CardContext;Lcom/twitter/android/av/revenue/VideoConversationCardData;)V

    .line 95
    return-void

    .line 89
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 73
    invoke-super {p0}, Lcom/twitter/android/revenue/card/n;->a()V

    .line 74
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->e:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->e:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->j()Z

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->x:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->d:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-eq v0, v1, :cond_1

    .line 78
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->d:Lcom/twitter/android/av/card/f;

    invoke-virtual {v0}, Lcom/twitter/android/av/card/f;->d()V

    .line 80
    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/library/card/z$a;)V
    .locals 3

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/twitter/android/revenue/card/n;->a(Lcom/twitter/library/card/z$a;)V

    .line 65
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->x:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->d:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-eq v0, v1, :cond_0

    .line 66
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->d:Lcom/twitter/android/av/card/f;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/aq;->w:Lcom/twitter/library/card/CardContext;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/aq;->t:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/av/card/f;->a(Lcom/twitter/library/card/CardContext;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 67
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/aq;->s()V

    .line 69
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 31
    check-cast p1, Lcom/twitter/library/card/z$a;

    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/card/aq;->a(Lcom/twitter/library/card/z$a;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 105
    invoke-super {p0, p1}, Lcom/twitter/android/revenue/card/n;->a(Z)V

    .line 106
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->d:Lcom/twitter/android/av/card/f;

    invoke-virtual {v0}, Lcom/twitter/android/av/card/f;->k()V

    .line 107
    return-void
.end method

.method public aY_()V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/aq;->s()V

    .line 120
    return-void
.end method

.method public at_()V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->d:Lcom/twitter/android/av/card/f;

    invoke-virtual {v0}, Lcom/twitter/android/av/card/f;->at_()V

    .line 130
    return-void
.end method

.method public au_()V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->d:Lcom/twitter/android/av/card/f;

    invoke-virtual {v0}, Lcom/twitter/android/av/card/f;->au_()V

    .line 135
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->e:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->e:Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setFromMemoryOnly(Z)V

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->d:Lcom/twitter/android/av/card/f;

    invoke-virtual {v0}, Lcom/twitter/android/av/card/f;->l()V

    .line 115
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->d:Lcom/twitter/android/av/card/f;

    invoke-virtual {v0}, Lcom/twitter/android/av/card/f;->c()Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 99
    invoke-super {p0}, Lcom/twitter/android/revenue/card/n;->d()V

    .line 100
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->d:Lcom/twitter/android/av/card/f;

    invoke-virtual {v0}, Lcom/twitter/android/av/card/f;->j()V

    .line 101
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->d:Lcom/twitter/android/av/card/f;

    invoke-virtual {v0}, Lcom/twitter/android/av/card/f;->h()V

    .line 140
    return-void
.end method

.method public i()Landroid/view/View;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->d:Lcom/twitter/android/av/card/f;

    invoke-virtual {v0}, Lcom/twitter/android/av/card/f;->i()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public p()V
    .locals 3

    .prologue
    .line 50
    const-string/jumbo v0, "player_image"

    iget-object v1, p0, Lcom/twitter/android/revenue/card/aq;->a:Lcar;

    invoke-static {v0, v1}, Lcas;->a(Ljava/lang/String;Lcar;)Lcas;

    move-result-object v1

    .line 51
    if-eqz v1, :cond_0

    .line 52
    new-instance v0, Lcom/twitter/android/revenue/card/CardMediaView;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/aq;->q:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/twitter/android/revenue/card/CardMediaView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/revenue/card/aq;->f:Lcom/twitter/android/revenue/card/CardMediaView;

    .line 53
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->f:Lcom/twitter/android/revenue/card/CardMediaView;

    const v2, 0x7f13051e

    invoke-virtual {v0, v2}, Lcom/twitter/android/revenue/card/CardMediaView;->setId(I)V

    .line 54
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->f:Lcom/twitter/android/revenue/card/CardMediaView;

    const v2, 0x7f130214

    invoke-virtual {v0, v2}, Lcom/twitter/android/revenue/card/CardMediaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/aq;->e:Lcom/twitter/media/ui/image/MediaImageView;

    .line 55
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->e:Lcom/twitter/media/ui/image/MediaImageView;

    const-string/jumbo v2, "card"

    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setImageType(Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->e:Lcom/twitter/media/ui/image/MediaImageView;

    const/high16 v2, 0x40200000    # 2.5f

    invoke-virtual {v1, v2}, Lcas;->a(F)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 57
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->e:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v1, v1, Lcas;->a:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 58
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->e:Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setFromMemoryOnly(Z)V

    .line 60
    :cond_0
    return-void
.end method

.method public q()Landroid/view/View;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aq;->f:Lcom/twitter/android/revenue/card/CardMediaView;

    return-object v0
.end method
