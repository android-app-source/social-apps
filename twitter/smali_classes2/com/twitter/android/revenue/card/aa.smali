.class public Lcom/twitter/android/revenue/card/aa;
.super Lcom/twitter/android/revenue/card/a;
.source "Twttr"


# instance fields
.field a:Lcom/twitter/media/ui/image/MediaImageView;

.field b:Landroid/widget/TextView;

.field c:Landroid/widget/TextView;

.field d:Landroid/widget/TextView;

.field e:Landroid/widget/RatingBar;

.field f:Lcom/twitter/ui/widget/TwitterButton;

.field g:Landroid/view/View;

.field private final h:Lcom/twitter/library/util/s;

.field private r:Lcom/twitter/library/card/e;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/revenue/card/a;-><init>(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V

    .line 58
    new-instance v0, Lcom/twitter/android/revenue/card/aa$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/revenue/card/aa$1;-><init>(Lcom/twitter/android/revenue/card/aa;)V

    iput-object v0, p0, Lcom/twitter/android/revenue/card/aa;->h:Lcom/twitter/library/util/s;

    .line 64
    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/card/aa;->a(Landroid/content/Context;)V

    .line 65
    return-void
.end method


# virtual methods
.method a(Lcom/twitter/android/card/CardActionHelper$AppStatus;)I
    .locals 2

    .prologue
    .line 215
    sget-object v0, Lcom/twitter/android/revenue/card/aa$4;->a:[I

    invoke-virtual {p1}, Lcom/twitter/android/card/CardActionHelper$AppStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 223
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/aa;->j()I

    move-result v0

    :goto_0
    return v0

    .line 217
    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/aa;->h()I

    move-result v0

    goto :goto_0

    .line 220
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/aa;->i()I

    move-result v0

    goto :goto_0

    .line 215
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 247
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    :goto_0
    return-object p1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toTitleCase(C)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 69
    invoke-super {p0}, Lcom/twitter/android/revenue/card/a;->a()V

    .line 71
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->a:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->a:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->j()Z

    .line 74
    :cond_0
    return-void
.end method

.method public a(JLcar;)V
    .locals 3

    .prologue
    .line 120
    const-string/jumbo v0, "app_url"

    const-string/jumbo v1, "app_url_resolved"

    invoke-static {v0, v1, p3}, Lcom/twitter/library/card/e;->a(Ljava/lang/String;Ljava/lang/String;Lcar;)Lcom/twitter/library/card/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/aa;->r:Lcom/twitter/library/card/e;

    .line 121
    const-string/jumbo v0, "app_id"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/aa;->s:Ljava/lang/String;

    .line 122
    const-string/jumbo v0, "card_url"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/aa;->t:Ljava/lang/String;

    .line 124
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->d_:Lcom/twitter/android/card/d;

    const-string/jumbo v1, "_card_data"

    invoke-static {v1, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/card/d;->a(Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->d_:Lcom/twitter/android/card/d;

    invoke-interface {v0, p1, p2}, Lcom/twitter/android/card/d;->a(J)V

    .line 126
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->d_:Lcom/twitter/android/card/d;

    invoke-interface {v0, p3}, Lcom/twitter/android/card/d;->a(Lcar;)V

    .line 128
    invoke-virtual {p0, p3}, Lcom/twitter/android/revenue/card/aa;->a(Lcar;)V

    .line 130
    invoke-virtual {p0, p3}, Lcom/twitter/android/revenue/card/aa;->b(Lcar;)V

    .line 132
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/aa;->g()V

    .line 133
    return-void
.end method

.method protected a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/aa;->f()I

    move-result v0

    .line 85
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/aa;->q:Landroid/view/View;

    .line 87
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->q:Landroid/view/View;

    const v1, 0x7f130214

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/aa;->a:Lcom/twitter/media/ui/image/MediaImageView;

    .line 88
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->a:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->a:Lcom/twitter/media/ui/image/MediaImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 90
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->a:Lcom/twitter/media/ui/image/MediaImageView;

    const-string/jumbo v1, "card"

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setImageType(Ljava/lang/String;)V

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->q:Landroid/view/View;

    const v1, 0x7f130218

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/aa;->b:Landroid/widget/TextView;

    .line 94
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->q:Landroid/view/View;

    const v1, 0x7f13058a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/aa;->c:Landroid/widget/TextView;

    .line 95
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->q:Landroid/view/View;

    const v1, 0x7f1305c5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/aa;->g:Landroid/view/View;

    .line 96
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->q:Landroid/view/View;

    const v1, 0x7f1305c7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/aa;->d:Landroid/widget/TextView;

    .line 97
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->q:Landroid/view/View;

    const v1, 0x7f1305c6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/aa;->e:Landroid/widget/RatingBar;

    .line 98
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->q:Landroid/view/View;

    const v1, 0x7f130217

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/aa;->f:Lcom/twitter/ui/widget/TwitterButton;

    .line 100
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->q:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/revenue/card/aa$2;

    invoke-direct {v1, p0}, Lcom/twitter/android/revenue/card/aa$2;-><init>(Lcom/twitter/android/revenue/card/aa;)V

    .line 101
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 112
    return-void
.end method

.method a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 5

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/aa;->e()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, p2, v1}, Lcom/twitter/library/scribe/b;->a(Landroid/view/View;Landroid/view/View;Landroid/view/MotionEvent;I)Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    move-result-object v0

    .line 243
    iget-object v1, p0, Lcom/twitter/android/revenue/card/aa;->n:Lcom/twitter/android/card/CardActionHelper;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/aa;->r:Lcom/twitter/library/card/e;

    iget-object v3, p0, Lcom/twitter/android/revenue/card/aa;->s:Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/revenue/card/aa;->t:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/twitter/android/card/CardActionHelper;->a(Lcom/twitter/library/card/e;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 244
    return-void
.end method

.method a(Lcar;)V
    .locals 3

    .prologue
    .line 136
    const-string/jumbo v0, "thumbnail"

    invoke-static {v0, p1}, Lcas;->a(Ljava/lang/String;Lcar;)Lcas;

    move-result-object v0

    .line 137
    if-eqz v0, :cond_0

    .line 138
    iget-object v1, p0, Lcom/twitter/android/revenue/card/aa;->a:Lcom/twitter/media/ui/image/MediaImageView;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Lcas;->a(F)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 139
    iget-object v1, p0, Lcom/twitter/android/revenue/card/aa;->a:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v0, v0, Lcas;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 140
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->a:Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setFromMemoryOnly(Z)V

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->a:Lcom/twitter/media/ui/image/MediaImageView;

    const-string/jumbo v1, "thumbnail"

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setTag(Ljava/lang/Object;)V

    .line 143
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->a:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/aa;->h:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 144
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->a:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->a:Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setFromMemoryOnly(Z)V

    .line 81
    :cond_0
    return-void
.end method

.method b(Lcar;)V
    .locals 0

    .prologue
    .line 147
    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/card/aa;->c(Lcar;)V

    .line 148
    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/card/aa;->g(Lcar;)V

    .line 149
    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/card/aa;->d(Lcar;)V

    .line 150
    return-void
.end method

.method c(Lcar;)V
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->b:Landroid/widget/TextView;

    sget-object v1, Lcom/twitter/android/revenue/card/h;->c:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 154
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->b:Landroid/widget/TextView;

    const-string/jumbo v1, "title"

    invoke-static {v1, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    return-void
.end method

.method d(Lcar;)V
    .locals 4

    .prologue
    .line 158
    const-string/jumbo v0, "app_star_rating"

    invoke-static {v0, p1}, Lcom/twitter/library/card/u;->a(Ljava/lang/String;Lcar;)Ljava/lang/Double;

    move-result-object v0

    .line 159
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    .line 160
    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/card/aa;->e(Lcar;)V

    .line 161
    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/card/aa;->f(Lcar;)V

    .line 165
    :goto_0
    return-void

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->g:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method e(Lcar;)V
    .locals 5

    .prologue
    .line 168
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 169
    const-string/jumbo v1, "app_num_ratings"

    invoke-static {v1, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    .line 170
    iget-object v2, p0, Lcom/twitter/android/revenue/card/aa;->d:Landroid/widget/TextView;

    sget-object v3, Lcom/twitter/android/revenue/card/h;->b:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 172
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 173
    const v2, 0x7f0a0100

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    .line 174
    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 175
    iget-object v1, p0, Lcom/twitter/android/revenue/card/aa;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    :cond_0
    return-void
.end method

.method f()I
    .locals 1

    .prologue
    .line 115
    const v0, 0x7f040252

    return v0
.end method

.method f(Lcar;)V
    .locals 2

    .prologue
    .line 180
    const-string/jumbo v0, "app_star_rating"

    invoke-static {v0, p1}, Lcom/twitter/library/card/u;->a(Ljava/lang/String;Lcar;)Ljava/lang/Double;

    move-result-object v0

    .line 181
    if-eqz v0, :cond_0

    .line 182
    iget-object v1, p0, Lcom/twitter/android/revenue/card/aa;->e:Landroid/widget/RatingBar;

    invoke-virtual {v0}, Ljava/lang/Double;->floatValue()F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/RatingBar;->setRating(F)V

    .line 184
    :cond_0
    return-void
.end method

.method g()V
    .locals 3

    .prologue
    .line 200
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->n:Lcom/twitter/android/card/CardActionHelper;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/aa;->s:Ljava/lang/String;

    .line 201
    invoke-virtual {v0, v1}, Lcom/twitter/android/card/CardActionHelper;->a(Ljava/lang/String;)Lcom/twitter/android/card/CardActionHelper$AppStatus;

    move-result-object v0

    .line 203
    invoke-virtual {p0, v0}, Lcom/twitter/android/revenue/card/aa;->a(Lcom/twitter/android/card/CardActionHelper$AppStatus;)I

    move-result v0

    .line 204
    iget-object v1, p0, Lcom/twitter/android/revenue/card/aa;->f:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TwitterButton;->setText(I)V

    .line 205
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->f:Lcom/twitter/ui/widget/TwitterButton;

    const-string/jumbo v1, "button"

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setTag(Ljava/lang/Object;)V

    .line 206
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->f:Lcom/twitter/ui/widget/TwitterButton;

    new-instance v1, Lcom/twitter/android/revenue/card/aa$3;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/aa;->f:Lcom/twitter/ui/widget/TwitterButton;

    invoke-direct {v1, p0, v2}, Lcom/twitter/android/revenue/card/aa$3;-><init>(Lcom/twitter/android/revenue/card/aa;Lcom/twitter/ui/widget/TwitterButton;)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 212
    return-void
.end method

.method g(Lcar;)V
    .locals 3

    .prologue
    .line 187
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aa;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 188
    const-string/jumbo v0, "app_price"

    invoke-static {v0, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    .line 189
    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const v0, 0x7f0a005c

    .line 190
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/revenue/card/aa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 191
    :cond_0
    const v2, 0x7f0a06a2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 193
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " \u2022 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 195
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/revenue/card/aa;->c:Landroid/widget/TextView;

    sget-object v2, Lcom/twitter/android/revenue/card/h;->b:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 196
    iget-object v1, p0, Lcom/twitter/android/revenue/card/aa;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    return-void

    .line 193
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " \u2022 "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method h()I
    .locals 1

    .prologue
    .line 229
    const v0, 0x7f0a0062

    return v0
.end method

.method i()I
    .locals 1

    .prologue
    .line 233
    const v0, 0x7f0a0060

    return v0
.end method

.method j()I
    .locals 1

    .prologue
    .line 237
    const v0, 0x7f0a00fe

    return v0
.end method
