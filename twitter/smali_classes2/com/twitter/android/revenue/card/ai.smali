.class public Lcom/twitter/android/revenue/card/ai;
.super Lcom/twitter/android/revenue/card/ar;
.source "Twttr"


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 19
    const-string/jumbo v0, "thumbnail_image"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "summary_photo_image"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "player_image"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "promo_image"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/revenue/card/ai;->a:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V
    .locals 6

    .prologue
    .line 24
    const v5, 0x7f040257

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/revenue/card/ai;-><init>(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;I)V

    .line 25
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;I)V
    .locals 0

    .prologue
    .line 29
    invoke-direct/range {p0 .. p5}, Lcom/twitter/android/revenue/card/ar;-><init>(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;I)V

    .line 30
    return-void
.end method


# virtual methods
.method protected a(Lcas;)F
    .locals 1

    .prologue
    .line 45
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method protected f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    sget-object v0, Lcom/twitter/android/revenue/card/ai;->a:Ljava/util/List;

    return-object v0
.end method

.method protected g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    const-string/jumbo v0, "card_url"

    return-object v0
.end method

.method protected h()Z
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    return v0
.end method
