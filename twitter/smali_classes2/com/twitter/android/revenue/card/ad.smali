.class public Lcom/twitter/android/revenue/card/ad;
.super Lcom/twitter/library/card/aa;
.source "Twttr"


# static fields
.field private static final a:Lcom/twitter/library/card/n;

.field private static final b:Lcom/twitter/library/card/n;

.field private static final c:Lcom/twitter/library/card/n;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 29
    new-instance v0, Lcom/twitter/library/card/n;

    const-class v1, Lcom/twitter/android/revenue/card/ac;

    sget-object v2, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/card/n;-><init>(Ljava/lang/Class;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    sput-object v0, Lcom/twitter/android/revenue/card/ad;->a:Lcom/twitter/library/card/n;

    .line 30
    new-instance v0, Lcom/twitter/library/card/n;

    const-class v1, Lcom/twitter/android/revenue/card/ac;

    sget-object v2, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->g:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/card/n;-><init>(Ljava/lang/Class;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    sput-object v0, Lcom/twitter/android/revenue/card/ad;->b:Lcom/twitter/library/card/n;

    .line 31
    new-instance v0, Lcom/twitter/library/card/n;

    const-class v1, Lcom/twitter/android/revenue/card/ac;

    sget-object v2, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->h:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/card/n;-><init>(Ljava/lang/Class;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    sput-object v0, Lcom/twitter/android/revenue/card/ad;->c:Lcom/twitter/library/card/n;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/twitter/library/card/aa;-><init>()V

    return-void
.end method

.method private static a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;Z)I
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-eq v0, p0, :cond_0

    sget-object v0, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->g:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne v0, p0, :cond_1

    :cond_0
    if-eqz p1, :cond_1

    .line 94
    const v0, 0x7f040254

    .line 97
    :goto_0
    return v0

    :cond_1
    const v0, 0x7f040253

    goto :goto_0
.end method

.method private static a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;F)[F
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->g:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne v0, p0, :cond_0

    .line 102
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/twitter/android/revenue/j;->a(F)[F

    move-result-object v0

    .line 105
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/twitter/android/revenue/j;->a(F)[F

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;ZF)[F
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x4

    const/4 v2, 0x0

    .line 109
    sget-object v0, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->g:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne v0, p0, :cond_0

    .line 110
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    .line 117
    :goto_0
    return-object v0

    .line 113
    :cond_0
    sget-object v0, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne v0, p0, :cond_1

    if-eqz p1, :cond_1

    .line 114
    new-array v0, v1, [F

    aput p2, v0, v3

    aput v2, v0, v4

    aput v2, v0, v5

    const/4 v1, 0x3

    aput p2, v0, v1

    goto :goto_0

    .line 117
    :cond_1
    new-array v0, v1, [F

    aput p2, v0, v3

    aput p2, v0, v4

    aput v2, v0, v5

    const/4 v1, 0x3

    aput v2, v0, v1

    goto :goto_0

    .line 110
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public a(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcar;)Lcom/twitter/library/card/z;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 61
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 62
    const v1, 0x7f0e0116

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 63
    const v2, 0x7f0e0119

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    .line 64
    invoke-static {p1, p2}, Lcom/twitter/android/revenue/j;->a(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Z

    move-result v7

    .line 66
    const-string/jumbo v0, "realtime_ad_impression_id"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    .line 67
    if-eqz v0, :cond_0

    invoke-static {}, Lbwd;->c()Lbwd;

    move-result-object v2

    invoke-virtual {v2, v0}, Lbwd;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mopub/nativeads/NativeAd;

    move-object v2, v0

    .line 69
    :goto_0
    if-eqz v2, :cond_2

    .line 70
    invoke-virtual {v2}, Lcom/mopub/nativeads/NativeAd;->getBaseNativeAd()Lcom/mopub/nativeads/BaseNativeAd;

    move-result-object v3

    .line 71
    instance-of v0, v3, Lcom/mopub/nativeads/GooglePlayServicesNative$GooglePlayServicesNativeAd;

    if-eqz v0, :cond_1

    .line 72
    new-instance v0, Lcom/twitter/android/revenue/card/t;

    check-cast v3, Lcom/mopub/nativeads/GooglePlayServicesNative$GooglePlayServicesNativeAd;

    new-instance v5, Lcom/twitter/android/card/f;

    invoke-direct {v5, p1}, Lcom/twitter/android/card/f;-><init>(Landroid/content/Context;)V

    new-instance v6, Lcom/twitter/android/card/c;

    invoke-direct {v6, p1}, Lcom/twitter/android/card/c;-><init>(Landroid/app/Activity;)V

    .line 75
    invoke-static {v1}, Lcom/twitter/android/revenue/j;->a(F)[F

    move-result-object v7

    .line 76
    invoke-static {p2, v9, v8}, Lcom/twitter/android/revenue/card/ad;->a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;ZF)[F

    move-result-object v8

    move-object v1, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/revenue/card/t;-><init>(Landroid/content/Context;Lcom/mopub/nativeads/NativeAd;Lcom/mopub/nativeads/GooglePlayServicesNative$GooglePlayServicesNativeAd;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;[F[F)V

    .line 84
    :goto_1
    return-object v0

    .line 67
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 78
    :cond_1
    new-instance v0, Lcom/twitter/android/revenue/card/z;

    new-instance v4, Lcom/twitter/android/card/f;

    invoke-direct {v4, p1}, Lcom/twitter/android/card/f;-><init>(Landroid/content/Context;)V

    new-instance v5, Lcom/twitter/android/card/c;

    invoke-direct {v5, p1}, Lcom/twitter/android/card/c;-><init>(Landroid/app/Activity;)V

    .line 80
    invoke-static {v1}, Lcom/twitter/android/revenue/j;->a(F)[F

    move-result-object v6

    .line 81
    invoke-static {p2, v9, v8}, Lcom/twitter/android/revenue/card/ad;->a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;ZF)[F

    move-result-object v7

    move-object v1, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/revenue/card/z;-><init>(Landroid/content/Context;Lcom/mopub/nativeads/NativeAd;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;[F[F)V

    goto :goto_1

    .line 84
    :cond_2
    new-instance v0, Lcom/twitter/android/revenue/card/ac;

    new-instance v3, Lcom/twitter/android/card/f;

    invoke-direct {v3, p1}, Lcom/twitter/android/card/f;-><init>(Landroid/content/Context;)V

    new-instance v4, Lcom/twitter/android/card/c;

    invoke-direct {v4, p1}, Lcom/twitter/android/card/c;-><init>(Landroid/app/Activity;)V

    .line 86
    invoke-static {p2, v7}, Lcom/twitter/android/revenue/card/ad;->a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;Z)I

    move-result v5

    .line 87
    invoke-static {p2, v1}, Lcom/twitter/android/revenue/card/ad;->a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;F)[F

    move-result-object v6

    .line 88
    invoke-static {p2, v7, v8}, Lcom/twitter/android/revenue/card/ad;->a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;ZF)[F

    move-result-object v7

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/revenue/card/ac;-><init>(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;I[F[F)V

    goto :goto_1
.end method

.method public a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcar;)Z
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    return v0
.end method

.method public b(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcar;)Lcom/twitter/library/card/n;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 37
    sget-object v1, Lcom/twitter/android/revenue/card/ad$1;->a:[I

    invoke-virtual {p2}, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 48
    :cond_0
    :goto_0
    return-object v0

    .line 39
    :pswitch_0
    invoke-static {p3}, Lbwi;->a(Lcar;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v0, Lcom/twitter/android/revenue/card/ad;->a:Lcom/twitter/library/card/n;

    goto :goto_0

    .line 42
    :pswitch_1
    sget-object v0, Lcom/twitter/android/revenue/card/ad;->b:Lcom/twitter/library/card/n;

    goto :goto_0

    .line 45
    :pswitch_2
    sget-object v0, Lcom/twitter/android/revenue/card/ad;->c:Lcom/twitter/library/card/n;

    goto :goto_0

    .line 37
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
