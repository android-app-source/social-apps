.class public abstract Lcom/twitter/android/revenue/card/a;
.super Lcom/twitter/library/card/z;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/card/p$a;
.implements Lcom/twitter/library/card/q$a;


# instance fields
.field private a:J

.field private b:J

.field protected final d_:Lcom/twitter/android/card/d;

.field protected final i:Landroid/content/Context;

.field protected final k:Lcom/twitter/android/card/b;

.field protected l:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field protected m:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field protected n:Lcom/twitter/android/card/CardActionHelper;

.field protected o:Lcom/twitter/library/card/CardContext;

.field protected p:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

.field protected q:Landroid/view/View;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V
    .locals 4

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/twitter/library/card/z;-><init>()V

    .line 36
    iput-object p2, p0, Lcom/twitter/android/revenue/card/a;->p:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/a;->i:Landroid/content/Context;

    .line 38
    iput-object p4, p0, Lcom/twitter/android/revenue/card/a;->k:Lcom/twitter/android/card/b;

    .line 39
    iput-object p3, p0, Lcom/twitter/android/revenue/card/a;->d_:Lcom/twitter/android/card/d;

    .line 40
    iget-object v0, p0, Lcom/twitter/android/revenue/card/a;->d_:Lcom/twitter/android/card/d;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/a;->l:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-interface {v0, v1}, Lcom/twitter/android/card/d;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 41
    new-instance v0, Lcom/twitter/android/card/CardActionHelper;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/a;->k:Lcom/twitter/android/card/b;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/a;->d_:Lcom/twitter/android/card/d;

    iget-object v3, p0, Lcom/twitter/android/revenue/card/a;->p:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 42
    invoke-static {v3}, Lcom/twitter/android/card/g;->a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/card/CardActionHelper;-><init>(Lcom/twitter/android/card/b;Lcom/twitter/android/card/d;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/revenue/card/a;->n:Lcom/twitter/android/card/CardActionHelper;

    .line 43
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 64
    invoke-static {}, Lcom/twitter/library/card/p;->a()Lcom/twitter/library/card/p;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/revenue/card/a;->a:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/p;->b(JLjava/lang/Object;)V

    .line 65
    invoke-static {}, Lcom/twitter/library/card/q;->a()Lcom/twitter/library/card/q;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/revenue/card/a;->b:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/q;->b(JLjava/lang/Object;)V

    .line 66
    return-void
.end method

.method public a(JLcom/twitter/library/card/CardContext;)V
    .locals 1

    .prologue
    .line 70
    iput-object p3, p0, Lcom/twitter/android/revenue/card/a;->o:Lcom/twitter/library/card/CardContext;

    .line 71
    iget-object v0, p0, Lcom/twitter/android/revenue/card/a;->d_:Lcom/twitter/android/card/d;

    invoke-interface {v0, p3}, Lcom/twitter/android/card/d;->a(Lcom/twitter/library/card/CardContext;)V

    .line 72
    return-void
.end method

.method public a(Lcom/twitter/library/card/z$a;)V
    .locals 4

    .prologue
    .line 51
    iget-wide v0, p1, Lcom/twitter/library/card/z$a;->a:J

    iput-wide v0, p0, Lcom/twitter/android/revenue/card/a;->a:J

    .line 52
    invoke-static {}, Lcom/twitter/library/card/p;->a()Lcom/twitter/library/card/p;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/revenue/card/a;->a:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/p;->a(JLjava/lang/Object;)V

    .line 53
    iget-wide v0, p1, Lcom/twitter/library/card/z$a;->b:J

    iput-wide v0, p0, Lcom/twitter/android/revenue/card/a;->b:J

    .line 54
    invoke-static {}, Lcom/twitter/library/card/q;->a()Lcom/twitter/library/card/q;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/revenue/card/a;->b:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/q;->a(JLjava/lang/Object;)V

    .line 55
    const-string/jumbo v0, "params_extra_scribe_association"

    const-class v1, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/card/z$a;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/a;->l:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 57
    iget-object v0, p0, Lcom/twitter/android/revenue/card/a;->d_:Lcom/twitter/android/card/d;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/a;->l:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-interface {v0, v1}, Lcom/twitter/android/card/d;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 58
    const-string/jumbo v0, "params_extra_source_scribe_association"

    const-class v1, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/card/z$a;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/a;->m:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 60
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lcom/twitter/library/card/z$a;

    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/card/a;->a(Lcom/twitter/library/card/z$a;)V

    return-void
.end method

.method public e()Landroid/view/View;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/twitter/android/revenue/card/a;->q:Landroid/view/View;

    return-object v0
.end method
