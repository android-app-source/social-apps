.class public Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/revenue/card/al;
.implements Lcom/twitter/android/revenue/card/k$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/revenue/card/UnlockableVideoToVideo$LockedAVDataSource;
    }
.end annotation


# instance fields
.field private a:Landroid/view/ViewGroup;

.field private b:Lcom/twitter/android/revenue/card/k;

.field private c:Lcom/twitter/android/revenue/card/k;

.field private final d:Landroid/content/Context;

.field private final e:Lcar;

.field private f:Lcom/twitter/model/core/Tweet;

.field private g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private h:J

.field private i:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Lcar;Lcom/twitter/android/av/video/e$b;)V
    .locals 3

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->i:Z

    .line 61
    iput-object p1, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->d:Landroid/content/Context;

    .line 62
    iput-object p2, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->e:Lcar;

    .line 64
    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->a(Landroid/content/Context;)Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->a:Landroid/view/ViewGroup;

    .line 66
    new-instance v0, Lcom/twitter/android/revenue/card/k;

    invoke-direct {v0, p1, p3}, Lcom/twitter/android/revenue/card/k;-><init>(Landroid/content/Context;Lcom/twitter/android/av/video/e$b;)V

    iput-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->b:Lcom/twitter/android/revenue/card/k;

    .line 67
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->b:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0, p0}, Lcom/twitter/android/revenue/card/k;->a(Lcom/twitter/android/revenue/card/k$a;)V

    .line 69
    new-instance v0, Lcom/twitter/android/revenue/card/k;

    invoke-direct {v0, p1, p3}, Lcom/twitter/android/revenue/card/k;-><init>(Landroid/content/Context;Lcom/twitter/android/av/video/e$b;)V

    iput-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->c:Lcom/twitter/android/revenue/card/k;

    .line 70
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->c:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0, p0}, Lcom/twitter/android/revenue/card/k;->a(Lcom/twitter/android/revenue/card/k$a;)V

    .line 72
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->b:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v1}, Lcom/twitter/android/revenue/card/k;->e()Landroid/view/View;

    move-result-object v1

    invoke-static {}, Lcom/twitter/android/revenue/j;->a()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 73
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->c:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v1}, Lcom/twitter/android/revenue/card/k;->e()Landroid/view/View;

    move-result-object v1

    invoke-static {}, Lcom/twitter/android/revenue/j;->a()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 74
    return-void
.end method

.method private a(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "locked-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p1, Lcom/twitter/model/core/Tweet;->t:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->a:Landroid/view/ViewGroup;

    return-object v0
.end method

.method a(Landroid/content/Context;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 77
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method a(Lcom/twitter/model/core/Tweet;Lcar;Z)Lcom/twitter/library/av/playback/AVDataSource;
    .locals 4

    .prologue
    .line 109
    if-eqz p3, :cond_0

    new-instance v0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo$LockedAVDataSource;

    const-string/jumbo v1, "cover_player_stream_url"

    .line 111
    invoke-static {v1, p2}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "cover_player_image"

    .line 113
    invoke-direct {p0, p1}, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->a(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo$LockedAVDataSource;-><init>(Lcom/twitter/model/core/Tweet;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    :goto_0
    return-object v0

    .line 113
    :cond_0
    new-instance v0, Lcom/twitter/library/av/playback/TweetAVDataSource;

    invoke-direct {v0, p1}, Lcom/twitter/library/av/playback/TweetAVDataSource;-><init>(Lcom/twitter/model/core/Tweet;)V

    goto :goto_0
.end method

.method public a(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;JLcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 3

    .prologue
    .line 95
    iput-object p2, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->f:Lcom/twitter/model/core/Tweet;

    .line 96
    iput-wide p3, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->h:J

    .line 97
    iput-object p5, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 99
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->i:Z

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->b:Lcom/twitter/android/revenue/card/k;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->e:Lcar;

    const/4 v2, 0x1

    invoke-virtual {p0, p2, v1, v2}, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->a(Lcom/twitter/model/core/Tweet;Lcar;Z)Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v1

    invoke-virtual {v0, p1, v1, p5}, Lcom/twitter/android/revenue/card/k;->a(Landroid/app/Activity;Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 101
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->c:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/k;->b()V

    .line 106
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->c:Lcom/twitter/android/revenue/card/k;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->e:Lcar;

    const/4 v2, 0x0

    invoke-virtual {p0, p2, v1, v2}, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->a(Lcom/twitter/model/core/Tweet;Lcar;Z)Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v1

    invoke-virtual {v0, p1, v1, p5}, Lcom/twitter/android/revenue/card/k;->a(Landroid/app/Activity;Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 104
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->b:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/k;->b()V

    goto :goto_0
.end method

.method public a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 5

    .prologue
    .line 189
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->e:Lcar;

    iget-wide v2, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->h:J

    iget-object v1, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->f:Lcom/twitter/model/core/Tweet;

    iget-boolean v4, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->i:Z

    .line 190
    invoke-static {v0, v2, v3, v1, v4}, Lcom/twitter/android/av/revenue/VideoConversationCardData;->a(Lcar;JLcom/twitter/model/core/Tweet;Z)Lcom/twitter/android/av/revenue/VideoConversationCardData;

    move-result-object v1

    .line 191
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->i:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "cover_player_stream_url"

    .line 193
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->e:Lcar;

    invoke-static {v0, v2}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    .line 195
    iget-object v2, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->f:Lcom/twitter/model/core/Tweet;

    iget-object v3, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->e:Lcar;

    iget-boolean v4, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->i:Z

    invoke-virtual {p0, v2, v3, v4}, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->a(Lcom/twitter/model/core/Tweet;Lcar;Z)Lcom/twitter/library/av/playback/AVDataSource;

    move-result-object v2

    .line 196
    new-instance v3, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity$a;

    invoke-direct {v3, v1, v2}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity$a;-><init>(Lcom/twitter/android/av/revenue/VideoConversationCardData;Lcom/twitter/library/av/playback/AVDataSource;)V

    iget-object v1, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->f:Lcom/twitter/model/core/Tweet;

    .line 197
    invoke-virtual {v3, v1}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity$a;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/av/AVCardCanvasActivity$a;

    move-result-object v1

    .line 198
    invoke-virtual {v1, v0}, Lcom/twitter/android/av/AVCardCanvasActivity$a;->a(Ljava/lang/String;)Lcom/twitter/android/av/AVCardCanvasActivity$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 199
    invoke-virtual {v0, v1}, Lcom/twitter/android/av/AVCardCanvasActivity$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/av/AVCardCanvasActivity$a;

    move-result-object v0

    .line 200
    invoke-virtual {v0, p1}, Lcom/twitter/android/av/AVCardCanvasActivity$a;->a(Landroid/view/View;)Lcom/twitter/android/av/AVCardCanvasActivity$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->d:Landroid/content/Context;

    .line 202
    invoke-static {v1}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v1

    invoke-virtual {v1}, Lbaa;->k()Z

    move-result v1

    .line 201
    invoke-virtual {v0, v1}, Lcom/twitter/android/av/AVCardCanvasActivity$a;->a(Z)Lcom/twitter/android/av/AVCardCanvasActivity$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->d:Landroid/content/Context;

    .line 203
    invoke-virtual {v0, v1}, Lcom/twitter/android/av/AVCardCanvasActivity$a;->b(Landroid/content/Context;)V

    .line 204
    return-void

    .line 191
    :cond_0
    const-string/jumbo v0, "player_stream_url"

    goto :goto_0
.end method

.method public a(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 82
    iput-boolean p1, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->i:Z

    .line 83
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->b:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/k;->e()Landroid/view/View;

    move-result-object v3

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 84
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->c:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/k;->e()Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 85
    return-void

    :cond_0
    move v0, v2

    .line 83
    goto :goto_0

    :cond_1
    move v2, v1

    .line 84
    goto :goto_1
.end method

.method public at_()V
    .locals 1

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->j()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->at_()V

    .line 165
    return-void
.end method

.method public au_()V
    .locals 1

    .prologue
    .line 169
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->j()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->au_()V

    .line 170
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->i:Z

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->b:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/k;->a()V

    .line 130
    :goto_0
    return-void

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->c:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/k;->a()V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->i:Z

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->b:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0, p1}, Lcom/twitter/android/revenue/card/k;->a(Z)V

    .line 139
    :goto_0
    return-void

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->c:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0, p1}, Lcom/twitter/android/revenue/card/k;->a(Z)V

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->j()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->c()Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->i:Z

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->b:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/k;->d()V

    .line 148
    :goto_0
    return-void

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->c:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/k;->d()V

    goto :goto_0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->b:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/k;->b()V

    .line 154
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->c:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/k;->b()V

    .line 155
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->j()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->h()V

    .line 175
    return-void
.end method

.method public i()Landroid/view/View;
    .locals 1

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->j()Lcom/twitter/library/widget/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->i()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method j()Lcom/twitter/library/widget/a;
    .locals 1

    .prologue
    .line 184
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->b:Lcom/twitter/android/revenue/card/k;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;->c:Lcom/twitter/android/revenue/card/k;

    goto :goto_0
.end method
