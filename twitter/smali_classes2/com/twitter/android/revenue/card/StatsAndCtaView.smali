.class public Lcom/twitter/android/revenue/card/StatsAndCtaView;
.super Landroid/widget/RelativeLayout;
.source "Twttr"


# static fields
.field private static final a:Ljava/lang/Double;


# instance fields
.field private b:Lcom/twitter/library/util/s;

.field private c:Landroid/widget/TextView;

.field private d:Lcom/twitter/ui/widget/TwitterButton;

.field private e:Landroid/view/ViewGroup;

.field private f:Landroid/widget/LinearLayout;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    const-wide/high16 v0, 0x400c000000000000L    # 3.5

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->a:Ljava/lang/Double;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/revenue/card/StatsAndCtaView;)Lcom/twitter/library/util/s;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->b:Lcom/twitter/library/util/s;

    return-object v0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 77
    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x10100f2

    aput v1, v0, v2

    .line 78
    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 79
    const v1, 0x7f0403cf

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 81
    invoke-static {p1, v1, p0}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 83
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 84
    return-void
.end method

.method private d(Lcar;)V
    .locals 4

    .prologue
    .line 121
    const-string/jumbo v0, "cta_key"

    invoke-static {v0, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/revenue/i;->a(Ljava/lang/String;)I

    move-result v0

    .line 122
    iget-object v1, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->d:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TwitterButton;->setText(I)V

    .line 123
    iget-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->d:Lcom/twitter/ui/widget/TwitterButton;

    const-string/jumbo v1, "button"

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setTag(Ljava/lang/Object;)V

    .line 124
    iget-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->b:Lcom/twitter/library/util/s;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->d:Lcom/twitter/ui/widget/TwitterButton;

    new-instance v1, Lcom/twitter/android/revenue/card/StatsAndCtaView$1;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->d:Lcom/twitter/ui/widget/TwitterButton;

    iget-object v3, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->b:Lcom/twitter/library/util/s;

    .line 126
    invoke-virtual {v3}, Lcom/twitter/library/util/s;->a()Z

    move-result v3

    invoke-direct {v1, p0, v2, v3}, Lcom/twitter/android/revenue/card/StatsAndCtaView$1;-><init>(Lcom/twitter/android/revenue/card/StatsAndCtaView;Lcom/twitter/ui/widget/TwitterButton;Z)V

    .line 125
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 133
    :cond_0
    return-void
.end method

.method public static getAppCategoryViewId()I
    .locals 1

    .prologue
    .line 73
    const v0, 0x7f1307fe

    return v0
.end method

.method public static getCtaViewId()I
    .locals 1

    .prologue
    .line 61
    const v0, 0x7f130217

    return v0
.end method

.method public static getRatingsViewId()I
    .locals 1

    .prologue
    .line 69
    const v0, 0x7f1305c7

    return v0
.end method

.method public static getTitleViewId()I
    .locals 1

    .prologue
    .line 65
    const v0, 0x7f130218

    return v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 174
    iget-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 175
    iget-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 176
    return-void
.end method

.method public a(Lcar;)V
    .locals 2

    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->d(Lcar;)V

    .line 108
    iget-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->c:Landroid/widget/TextView;

    const-string/jumbo v1, "title"

    invoke-static {v1, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->b(Lcar;)V

    .line 112
    iget-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->e:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->b:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 113
    iget-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->e:Landroid/view/ViewGroup;

    const-string/jumbo v1, "stats_container"

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    .line 116
    const-string/jumbo v0, "container"

    invoke-virtual {p0, v0}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->setTag(Ljava/lang/Object;)V

    .line 117
    iget-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->b:Lcom/twitter/library/util/s;

    invoke-virtual {p0, v0}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 118
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 191
    iget-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->d:Lcom/twitter/ui/widget/TwitterButton;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->d:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 193
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 194
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 196
    :cond_0
    return-void
.end method

.method b(Lcar;)V
    .locals 9

    .prologue
    const/16 v0, 0x8

    const/4 v8, 0x0

    .line 136
    const-string/jumbo v1, "app_star_rating"

    invoke-static {v1, p1}, Lcom/twitter/library/card/u;->a(Ljava/lang/String;Lcar;)Ljava/lang/Double;

    move-result-object v6

    .line 137
    if-eqz v6, :cond_2

    sget-object v1, Lcom/twitter/android/revenue/card/StatsAndCtaView;->a:Ljava/lang/Double;

    invoke-virtual {v1, v6}, Ljava/lang/Double;->compareTo(Ljava/lang/Double;)I

    move-result v1

    if-gez v1, :cond_2

    .line 138
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e04fe

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    .line 139
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->f:Landroid/widget/LinearLayout;

    const v2, 0x7f020655

    const v3, 0x7f020659

    const v4, 0x7f020657

    .line 141
    invoke-virtual {v6}, Ljava/lang/Double;->floatValue()F

    move-result v6

    const/high16 v7, 0x40a00000    # 5.0f

    .line 139
    invoke-static/range {v0 .. v7}, Lcom/twitter/android/revenue/j;->a(Landroid/content/Context;Landroid/widget/LinearLayout;IIIIFF)V

    .line 143
    const-string/jumbo v0, "app_num_ratings"

    invoke-static {v0, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    .line 144
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 145
    iget-object v1, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->h:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00ff

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v8

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    :cond_0
    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->c(Lcar;)V

    .line 162
    :cond_1
    :goto_0
    return-void

    .line 149
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 150
    iget-object v1, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 152
    const-string/jumbo v1, "app_category"

    invoke-static {v1, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    .line 153
    if-eqz v1, :cond_3

    .line 154
    iget-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 156
    :cond_3
    iget-object v1, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->h:Landroid/widget/TextView;

    const v2, 0x7f0a03d4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 157
    iget-object v1, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->g:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 158
    iget-object v1, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->g:Landroid/widget/TextView;

    iget-boolean v2, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->i:Z

    if-eqz v2, :cond_4

    const/4 v0, 0x4

    :cond_4
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method c(Lcar;)V
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 166
    const-string/jumbo v0, "app_category"

    invoke-static {v0, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    .line 167
    if-eqz v0, :cond_0

    .line 168
    iget-object v1, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    :cond_0
    return-void
.end method

.method public getStarsContainerView()Landroid/view/View;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->f:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getStatsContainerView()Landroid/view/View;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->e:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getStoreView()Landroid/view/View;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->g:Landroid/widget/TextView;

    return-object v0
.end method

.method public getTitleView()Landroid/view/View;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->c:Landroid/widget/TextView;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 88
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 89
    invoke-static {}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->getTitleViewId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->c:Landroid/widget/TextView;

    .line 90
    invoke-static {}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->getCtaViewId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->d:Lcom/twitter/ui/widget/TwitterButton;

    .line 91
    const v0, 0x7f1307fc

    invoke-virtual {p0, v0}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->e:Landroid/view/ViewGroup;

    .line 92
    const v0, 0x7f1307fd

    invoke-virtual {p0, v0}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->f:Landroid/widget/LinearLayout;

    .line 93
    invoke-static {}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->getRatingsViewId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->h:Landroid/widget/TextView;

    .line 94
    invoke-static {}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->getAppCategoryViewId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->g:Landroid/widget/TextView;

    .line 95
    return-void
.end method

.method public setCtaVisibility(I)V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->d:Lcom/twitter/ui/widget/TwitterButton;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->d:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 188
    :cond_0
    return-void
.end method

.method public setOnClickTouchListener(Lcom/twitter/library/util/s;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->b:Lcom/twitter/library/util/s;

    .line 99
    return-void
.end method

.method public setRatingContainerTextVisibility(I)V
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->h:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 182
    :cond_0
    return-void
.end method

.method public setShowBlankLine(Z)V
    .locals 0

    .prologue
    .line 102
    iput-boolean p1, p0, Lcom/twitter/android/revenue/card/StatsAndCtaView;->i:Z

    .line 103
    return-void
.end method
