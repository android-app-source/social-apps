.class public Lcom/twitter/android/revenue/card/c;
.super Lcom/twitter/android/revenue/card/a;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/library/util/s;

.field private final b:Lcom/twitter/media/ui/image/MediaImageView;

.field private final c:Lcom/twitter/android/revenue/card/StatsAndCtaView;

.field private d:Lcom/twitter/library/card/e;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 41
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/revenue/card/a;-><init>(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V

    .line 42
    new-instance v0, Lcom/twitter/android/revenue/card/c$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/revenue/card/c$1;-><init>(Lcom/twitter/android/revenue/card/c;)V

    iput-object v0, p0, Lcom/twitter/android/revenue/card/c;->a:Lcom/twitter/library/util/s;

    .line 52
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040238

    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/c;->q:Landroid/view/View;

    .line 54
    iget-object v0, p0, Lcom/twitter/android/revenue/card/c;->q:Landroid/view/View;

    const v1, 0x7f130569

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 55
    const v1, 0x7f04022a

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 56
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 58
    iget-object v0, p0, Lcom/twitter/android/revenue/card/c;->q:Landroid/view/View;

    const v1, 0x7f130572

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 59
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 60
    const v2, 0x7f0e0116

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 61
    new-array v3, v6, [F

    aput v2, v3, v5

    const/4 v4, 0x1

    aput v2, v3, v4

    const/4 v4, 0x2

    aput v2, v3, v4

    const/4 v4, 0x3

    aput v2, v3, v4

    const/4 v4, 0x4

    aput v2, v3, v4

    const/4 v4, 0x5

    aput v2, v3, v4

    const/4 v4, 0x6

    aput v2, v3, v4

    const/4 v4, 0x7

    aput v2, v3, v4

    .line 62
    invoke-static {v0, v1, v3}, Lcom/twitter/android/revenue/j;->a(Landroid/view/View;Landroid/content/res/Resources;[F)V

    .line 63
    iget-object v1, p0, Lcom/twitter/android/revenue/card/c;->a:Lcom/twitter/library/util/s;

    invoke-virtual {v1, v0}, Lcom/twitter/library/util/s;->a(Landroid/view/View;)V

    .line 65
    iget-object v0, p0, Lcom/twitter/android/revenue/card/c;->q:Landroid/view/View;

    const v1, 0x7f130214

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/c;->b:Lcom/twitter/media/ui/image/MediaImageView;

    .line 66
    iget-object v0, p0, Lcom/twitter/android/revenue/card/c;->b:Lcom/twitter/media/ui/image/MediaImageView;

    const/high16 v1, 0x40200000    # 2.5f

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 67
    iget-object v0, p0, Lcom/twitter/android/revenue/card/c;->b:Lcom/twitter/media/ui/image/MediaImageView;

    const-string/jumbo v1, "card"

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setImageType(Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/twitter/android/revenue/card/c;->q:Landroid/view/View;

    const v1, 0x7f130565

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/revenue/card/StatsAndCtaView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/c;->c:Lcom/twitter/android/revenue/card/StatsAndCtaView;

    .line 70
    iget-object v0, p0, Lcom/twitter/android/revenue/card/c;->c:Lcom/twitter/android/revenue/card/StatsAndCtaView;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/c;->a:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->setOnClickTouchListener(Lcom/twitter/library/util/s;)V

    .line 72
    iget-object v0, p0, Lcom/twitter/android/revenue/card/c;->o:Lcom/twitter/library/card/CardContext;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/revenue/card/c;->o:Lcom/twitter/library/card/CardContext;

    invoke-virtual {v0}, Lcom/twitter/library/card/CardContext;->a()Lcax;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "promo_app"

    iget-object v1, p0, Lcom/twitter/android/revenue/card/c;->o:Lcom/twitter/library/card/CardContext;

    .line 73
    invoke-virtual {v1}, Lcom/twitter/library/card/CardContext;->a()Lcax;

    move-result-object v1

    invoke-virtual {v1}, Lcax;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    invoke-static {p1, p2}, Lcom/twitter/android/revenue/j;->a(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/twitter/android/revenue/card/c;->c:Lcom/twitter/android/revenue/card/StatsAndCtaView;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->b()V

    .line 86
    :goto_0
    return-void

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/c;->c:Lcom/twitter/android/revenue/card/StatsAndCtaView;

    invoke-virtual {v0, v6}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->setRatingContainerTextVisibility(I)V

    goto :goto_0

    .line 83
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/revenue/card/c;->c:Lcom/twitter/android/revenue/card/StatsAndCtaView;

    invoke-virtual {v0, v5}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->setRatingContainerTextVisibility(I)V

    .line 84
    iget-object v0, p0, Lcom/twitter/android/revenue/card/c;->c:Lcom/twitter/android/revenue/card/StatsAndCtaView;

    invoke-virtual {v0, v6}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->setCtaVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/revenue/card/c;)Lcom/twitter/library/card/e;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/revenue/card/c;->d:Lcom/twitter/library/card/e;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/revenue/card/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/revenue/card/c;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/revenue/card/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/android/revenue/card/c;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 97
    invoke-super {p0}, Lcom/twitter/android/revenue/card/a;->a()V

    .line 98
    iget-object v0, p0, Lcom/twitter/android/revenue/card/c;->b:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/twitter/android/revenue/card/c;->b:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->j()Z

    .line 101
    :cond_0
    return-void
.end method

.method public a(JLcar;)V
    .locals 3

    .prologue
    .line 105
    const-string/jumbo v0, "app_url"

    const-string/jumbo v1, "app_url_resolved"

    invoke-static {v0, v1, p3}, Lcom/twitter/library/card/e;->a(Ljava/lang/String;Ljava/lang/String;Lcar;)Lcom/twitter/library/card/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/c;->d:Lcom/twitter/library/card/e;

    .line 106
    const-string/jumbo v0, "app_id"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/c;->e:Ljava/lang/String;

    .line 107
    const-string/jumbo v0, "card_url"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/c;->f:Ljava/lang/String;

    .line 108
    iget-object v0, p0, Lcom/twitter/android/revenue/card/c;->d_:Lcom/twitter/android/card/d;

    const-string/jumbo v1, "_card_data"

    invoke-static {v1, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/card/d;->a(Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/twitter/android/revenue/card/c;->c:Lcom/twitter/android/revenue/card/StatsAndCtaView;

    invoke-virtual {v0, p3}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->a(Lcar;)V

    .line 112
    const-string/jumbo v0, "thumbnail"

    invoke-static {v0, p3}, Lcas;->a(Ljava/lang/String;Lcar;)Lcas;

    move-result-object v0

    .line 113
    if-eqz v0, :cond_0

    .line 114
    iget-object v1, p0, Lcom/twitter/android/revenue/card/c;->b:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v2, v0, Lcas;->a:Ljava/lang/String;

    invoke-static {v2}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 115
    iget-object v1, p0, Lcom/twitter/android/revenue/card/c;->b:Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setFromMemoryOnly(Z)V

    .line 116
    iget-object v1, p0, Lcom/twitter/android/revenue/card/c;->b:Lcom/twitter/media/ui/image/MediaImageView;

    const-string/jumbo v2, "thumbnail"

    invoke-virtual {v1, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setTag(Ljava/lang/Object;)V

    .line 117
    iget-object v1, p0, Lcom/twitter/android/revenue/card/c;->b:Lcom/twitter/media/ui/image/MediaImageView;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Lcas;->a(F)F

    move-result v0

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 118
    iget-object v0, p0, Lcom/twitter/android/revenue/card/c;->b:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/c;->a:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 120
    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/android/revenue/card/c;->b:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/twitter/android/revenue/card/c;->b:Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setFromMemoryOnly(Z)V

    .line 93
    :cond_0
    return-void
.end method
