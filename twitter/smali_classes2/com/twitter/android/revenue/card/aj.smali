.class public Lcom/twitter/android/revenue/card/aj;
.super Lcom/twitter/android/revenue/card/n;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/revenue/card/aj$a;
    }
.end annotation


# instance fields
.field private d:Lcom/twitter/android/revenue/card/am;

.field private e:Lcom/twitter/android/revenue/card/al;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;Lcar;)V
    .locals 4

    .prologue
    .line 37
    invoke-direct/range {p0 .. p5}, Lcom/twitter/android/revenue/card/n;-><init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;Lcar;)V

    .line 39
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj;->q:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/revenue/card/aj;->a:Lcar;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/aj;->q:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/revenue/card/aj;->x:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 40
    invoke-static {v2, v3}, Lcom/twitter/android/revenue/j;->a(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Z

    move-result v2

    const/4 v3, 0x1

    .line 39
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/twitter/android/revenue/card/aj;->a(Landroid/content/res/Resources;Lcar;ZZ)[F

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/revenue/card/aj;->a([F)V

    .line 41
    return-void
.end method

.method private a(Landroid/content/res/Resources;Lcar;ZZ)[F
    .locals 1

    .prologue
    .line 140
    const-string/jumbo v0, "cover_player_stream_url"

    invoke-virtual {p2, v0}, Lcar;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p4, :cond_1

    const-string/jumbo v0, "player_stream_url"

    .line 141
    invoke-virtual {p2, v0}, Lcar;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 142
    :cond_0
    invoke-static {p1, p3}, Lcom/twitter/android/revenue/j;->a(Landroid/content/res/Resources;Z)[F

    move-result-object v0

    .line 144
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p1}, Lcom/twitter/android/revenue/j;->b(Landroid/content/res/Resources;)[F

    move-result-object v0

    goto :goto_0
.end method

.method private s()Lcom/twitter/android/revenue/card/am;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj;->d:Lcom/twitter/android/revenue/card/am;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lcom/twitter/android/revenue/card/am;

    invoke-direct {v0}, Lcom/twitter/android/revenue/card/am;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/revenue/card/aj;->d:Lcom/twitter/android/revenue/card/am;

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj;->d:Lcom/twitter/android/revenue/card/am;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 109
    invoke-super {p0}, Lcom/twitter/android/revenue/card/n;->a()V

    .line 110
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj;->e:Lcom/twitter/android/revenue/card/al;

    invoke-interface {v0}, Lcom/twitter/android/revenue/card/al;->e()V

    .line 111
    return-void
.end method

.method public a(Lcom/twitter/library/card/z$a;)V
    .locals 7

    .prologue
    .line 79
    invoke-super {p0, p1}, Lcom/twitter/android/revenue/card/n;->a(Lcom/twitter/library/card/z$a;)V

    .line 80
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj;->x:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    invoke-static {v0}, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj;->w:Lcom/twitter/library/card/CardContext;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    const-string/jumbo v1, "CardContext should not be null in onAttach!"

    invoke-static {v0, v1}, Lcom/twitter/util/f;->a(ZLjava/lang/String;)Z

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj;->w:Lcom/twitter/library/card/CardContext;

    invoke-static {v0}, Lcom/twitter/library/card/CardContext;->a(Lcom/twitter/library/card/CardContext;)Lcom/twitter/model/core/Tweet;

    move-result-object v3

    .line 85
    if-eqz v3, :cond_1

    .line 86
    iget-object v1, p0, Lcom/twitter/android/revenue/card/aj;->e:Lcom/twitter/android/revenue/card/al;

    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/aj;->l()Landroid/app/Activity;

    move-result-object v2

    iget-wide v4, p0, Lcom/twitter/android/revenue/card/aj;->y:J

    iget-object v6, p0, Lcom/twitter/android/revenue/card/aj;->t:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-interface/range {v1 .. v6}, Lcom/twitter/android/revenue/card/al;->a(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;JLcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 88
    :cond_1
    return-void

    .line 81
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lcom/twitter/library/card/z$a;

    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/card/aj;->a(Lcom/twitter/library/card/z$a;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 103
    invoke-super {p0, p1}, Lcom/twitter/android/revenue/card/n;->a(Z)V

    .line 104
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj;->e:Lcom/twitter/android/revenue/card/al;

    invoke-interface {v0, p1}, Lcom/twitter/android/revenue/card/al;->b(Z)V

    .line 105
    return-void
.end method

.method public at_()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj;->e:Lcom/twitter/android/revenue/card/al;

    invoke-interface {v0}, Lcom/twitter/android/revenue/card/al;->at_()V

    .line 121
    return-void
.end method

.method public au_()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj;->e:Lcom/twitter/android/revenue/card/al;

    invoke-interface {v0}, Lcom/twitter/android/revenue/card/al;->au_()V

    .line 126
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj;->e:Lcom/twitter/android/revenue/card/al;

    invoke-interface {v0}, Lcom/twitter/android/revenue/card/al;->d()V

    .line 99
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj;->e:Lcom/twitter/android/revenue/card/al;

    invoke-interface {v0}, Lcom/twitter/android/revenue/card/al;->c()Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 92
    invoke-super {p0}, Lcom/twitter/android/revenue/card/n;->d()V

    .line 93
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj;->e:Lcom/twitter/android/revenue/card/al;

    invoke-interface {v0}, Lcom/twitter/android/revenue/card/al;->b()V

    .line 94
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj;->e:Lcom/twitter/android/revenue/card/al;

    invoke-interface {v0}, Lcom/twitter/android/revenue/card/al;->h()V

    .line 131
    return-void
.end method

.method public i()Landroid/view/View;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj;->e:Lcom/twitter/android/revenue/card/al;

    invoke-interface {v0}, Lcom/twitter/android/revenue/card/al;->i()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected k()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 59
    invoke-super {p0}, Lcom/twitter/android/revenue/card/n;->k()V

    .line 61
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj;->e:Lcom/twitter/android/revenue/card/al;

    invoke-interface {v0, v7}, Lcom/twitter/android/revenue/card/al;->a(Z)V

    .line 62
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj;->w:Lcom/twitter/library/card/CardContext;

    invoke-static {v0}, Lcom/twitter/library/card/CardContext;->a(Lcom/twitter/library/card/CardContext;)Lcom/twitter/model/core/Tweet;

    move-result-object v3

    .line 63
    if-eqz v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string/jumbo v1, "Tweet should not be null in onTweetSent!"

    invoke-static {v0, v1}, Lcom/twitter/util/f;->a(ZLjava/lang/String;)Z

    .line 64
    if-eqz v3, :cond_0

    .line 65
    iget-object v1, p0, Lcom/twitter/android/revenue/card/aj;->e:Lcom/twitter/android/revenue/card/al;

    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/aj;->l()Landroid/app/Activity;

    move-result-object v2

    iget-wide v4, p0, Lcom/twitter/android/revenue/card/aj;->y:J

    iget-object v6, p0, Lcom/twitter/android/revenue/card/aj;->t:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-interface/range {v1 .. v6}, Lcom/twitter/android/revenue/card/al;->a(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;JLcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj;->q:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/revenue/card/aj;->a:Lcar;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/aj;->q:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/revenue/card/aj;->x:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 69
    invoke-static {v2, v3}, Lcom/twitter/android/revenue/j;->a(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Z

    move-result v2

    .line 68
    invoke-direct {p0, v0, v1, v2, v7}, Lcom/twitter/android/revenue/card/aj;->a(Landroid/content/res/Resources;Lcar;ZZ)[F

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/revenue/card/aj;->a([F)V

    .line 70
    return-void

    :cond_1
    move v0, v7

    .line 63
    goto :goto_0
.end method

.method public p()V
    .locals 4

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/aj;->s()Lcom/twitter/android/revenue/card/am;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/revenue/card/aj;->q:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/aj;->a:Lcar;

    iget-object v3, p0, Lcom/twitter/android/revenue/card/aj;->x:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/revenue/card/am;->a(Landroid/content/Context;Lcar;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Lcom/twitter/android/revenue/card/al;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/aj;->e:Lcom/twitter/android/revenue/card/al;

    .line 54
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj;->e:Lcom/twitter/android/revenue/card/al;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/twitter/android/revenue/card/al;->a(Z)V

    .line 55
    return-void
.end method

.method public q()Landroid/view/View;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj;->e:Lcom/twitter/android/revenue/card/al;

    invoke-interface {v0}, Lcom/twitter/android/revenue/card/al;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
