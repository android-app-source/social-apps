.class public Lcom/twitter/android/revenue/card/an;
.super Lcom/twitter/android/revenue/card/ac;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/av/card/f$a;
.implements Lcom/twitter/library/widget/a;


# instance fields
.field protected r:Lcom/twitter/android/av/card/f;

.field private s:Lcar;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;I[F[F)V
    .locals 0

    .prologue
    .line 33
    invoke-direct/range {p0 .. p7}, Lcom/twitter/android/revenue/card/ac;-><init>(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;I[F[F)V

    .line 34
    return-void
.end method

.method private l()V
    .locals 8

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/an;->j()Landroid/view/View;

    move-result-object v5

    .line 85
    if-eqz v5, :cond_0

    .line 86
    iget-object v0, p0, Lcom/twitter/android/revenue/card/an;->i:Landroid/content/Context;

    const-string/jumbo v1, "player_stream_url"

    iget-object v2, p0, Lcom/twitter/android/revenue/card/an;->s:Lcar;

    .line 87
    invoke-static {v1, v2}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/revenue/card/an;->c:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/twitter/android/revenue/card/an;->p:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 88
    invoke-static {v3}, Lcom/twitter/android/card/g;->a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/revenue/card/an;->d_:Lcom/twitter/android/card/d;

    iget-object v6, p0, Lcom/twitter/android/revenue/card/an;->o:Lcom/twitter/library/card/CardContext;

    iget-object v7, p0, Lcom/twitter/android/revenue/card/an;->s:Lcar;

    .line 89
    invoke-static {v7}, Lcom/twitter/android/av/revenue/VideoAppCardData;->a(Lcar;)Lcom/twitter/android/av/revenue/VideoAppCardData;

    move-result-object v7

    .line 86
    invoke-static/range {v0 .. v7}, Lcom/twitter/android/revenue/card/ap;->a(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;Ljava/lang/String;Lcom/twitter/android/card/d;Landroid/view/View;Lcom/twitter/library/card/CardContext;Lcom/twitter/android/av/revenue/VideoAppCardData;)V

    .line 91
    :cond_0
    return-void
.end method


# virtual methods
.method a(Landroid/content/Context;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/twitter/android/revenue/card/ac;->a(Landroid/content/Context;)Landroid/view/ViewGroup;

    .line 39
    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/card/an;->c(Landroid/content/Context;)Lcom/twitter/android/av/card/f;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/an;->r:Lcom/twitter/android/av/card/f;

    .line 41
    iget-object v0, p0, Lcom/twitter/android/revenue/card/an;->c:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 66
    invoke-super {p0}, Lcom/twitter/android/revenue/card/ac;->a()V

    .line 67
    iget-object v0, p0, Lcom/twitter/android/revenue/card/an;->r:Lcom/twitter/android/av/card/f;

    invoke-virtual {v0}, Lcom/twitter/android/av/card/f;->d()V

    .line 68
    return-void
.end method

.method public a(JLcar;)V
    .locals 1

    .prologue
    .line 78
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/revenue/card/ac;->a(JLcar;)V

    .line 79
    iput-object p3, p0, Lcom/twitter/android/revenue/card/an;->s:Lcar;

    .line 80
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/an;->l()V

    .line 81
    return-void
.end method

.method public a(Lcom/twitter/library/card/z$a;)V
    .locals 3

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/twitter/android/revenue/card/ac;->a(Lcom/twitter/library/card/z$a;)V

    .line 52
    iget-object v0, p1, Lcom/twitter/library/card/z$a;->c:Lcar;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/an;->s:Lcar;

    .line 54
    iget-object v0, p0, Lcom/twitter/android/revenue/card/an;->r:Lcom/twitter/android/av/card/f;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/an;->o:Lcom/twitter/library/card/CardContext;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/an;->l:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/av/card/f;->a(Lcom/twitter/library/card/CardContext;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 55
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/an;->l()V

    .line 56
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lcom/twitter/library/card/z$a;

    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/card/an;->a(Lcom/twitter/library/card/z$a;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 111
    invoke-super {p0, p1}, Lcom/twitter/android/revenue/card/ac;->a(Z)V

    .line 112
    iget-object v0, p0, Lcom/twitter/android/revenue/card/an;->r:Lcom/twitter/android/av/card/f;

    invoke-virtual {v0}, Lcom/twitter/android/av/card/f;->k()V

    .line 113
    return-void
.end method

.method public aY_()V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/an;->l()V

    .line 101
    return-void
.end method

.method public at_()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/twitter/android/revenue/card/an;->r:Lcom/twitter/android/av/card/f;

    invoke-virtual {v0}, Lcom/twitter/android/av/card/f;->at_()V

    .line 123
    return-void
.end method

.method public au_()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/twitter/android/revenue/card/an;->r:Lcom/twitter/android/av/card/f;

    invoke-virtual {v0}, Lcom/twitter/android/av/card/f;->au_()V

    .line 128
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 72
    invoke-super {p0}, Lcom/twitter/android/revenue/card/ac;->b()V

    .line 73
    iget-object v0, p0, Lcom/twitter/android/revenue/card/an;->r:Lcom/twitter/android/av/card/f;

    invoke-virtual {v0}, Lcom/twitter/android/av/card/f;->l()V

    .line 74
    return-void
.end method

.method c(Landroid/content/Context;)Lcom/twitter/android/av/card/f;
    .locals 7

    .prologue
    .line 45
    new-instance v0, Lcom/twitter/android/av/card/f;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/an;->d:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v3, p0, Lcom/twitter/android/revenue/card/an;->c:Landroid/view/ViewGroup;

    const v4, 0x7f13051e

    sget-object v6, Lbyo;->c:Lbyf;

    move-object v1, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/av/card/f;-><init>(Landroid/content/Context;Lcom/twitter/media/ui/image/MediaImageView;Landroid/view/ViewGroup;ILcom/twitter/android/av/card/f$a;Lbyf;)V

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/twitter/android/revenue/card/an;->r:Lcom/twitter/android/av/card/f;

    invoke-virtual {v0}, Lcom/twitter/android/av/card/f;->c()Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 105
    invoke-super {p0}, Lcom/twitter/android/revenue/card/ac;->d()V

    .line 106
    iget-object v0, p0, Lcom/twitter/android/revenue/card/an;->r:Lcom/twitter/android/av/card/f;

    invoke-virtual {v0}, Lcom/twitter/android/av/card/f;->j()V

    .line 107
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/twitter/android/revenue/card/an;->r:Lcom/twitter/android/av/card/f;

    invoke-virtual {v0}, Lcom/twitter/android/av/card/f;->h()V

    .line 133
    return-void
.end method

.method public i()Landroid/view/View;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/twitter/android/revenue/card/an;->r:Lcom/twitter/android/av/card/f;

    invoke-virtual {v0}, Lcom/twitter/android/av/card/f;->i()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method j()Landroid/view/View;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/android/revenue/card/an;->r:Lcom/twitter/android/av/card/f;

    invoke-virtual {v0}, Lcom/twitter/android/av/card/f;->b()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    const-string/jumbo v0, "player_image"

    return-object v0
.end method
