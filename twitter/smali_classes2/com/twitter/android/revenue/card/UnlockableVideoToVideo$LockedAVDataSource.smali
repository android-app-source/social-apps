.class Lcom/twitter/android/revenue/card/UnlockableVideoToVideo$LockedAVDataSource;
.super Lcom/twitter/library/av/playback/TweetAVDataSource;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/revenue/card/UnlockableVideoToVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LockedAVDataSource"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/android/revenue/card/UnlockableVideoToVideo$LockedAVDataSource;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 223
    new-instance v0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo$LockedAVDataSource$1;

    invoke-direct {v0}, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo$LockedAVDataSource$1;-><init>()V

    sput-object v0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo$LockedAVDataSource;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 251
    invoke-direct {p0, p1}, Lcom/twitter/library/av/playback/TweetAVDataSource;-><init>(Landroid/os/Parcel;)V

    .line 252
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo$LockedAVDataSource;->d:Ljava/lang/String;

    .line 253
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo$LockedAVDataSource;->c:Ljava/lang/String;

    .line 254
    return-void
.end method

.method constructor <init>(Lcom/twitter/model/core/Tweet;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 245
    invoke-direct {p0, p1, p2}, Lcom/twitter/library/av/playback/TweetAVDataSource;-><init>(Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 246
    iput-object p3, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo$LockedAVDataSource;->d:Ljava/lang/String;

    .line 247
    iput-object p4, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo$LockedAVDataSource;->c:Ljava/lang/String;

    .line 248
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo$LockedAVDataSource;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/twitter/library/av/playback/TweetAVDataSource;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo$LockedAVDataSource;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public b()Lcom/twitter/model/card/property/ImageSpec;
    .locals 3

    .prologue
    .line 264
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo$LockedAVDataSource;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo$LockedAVDataSource;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ad()Lcax;

    move-result-object v0

    .line 266
    if-eqz v0, :cond_0

    .line 267
    iget-object v1, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo$LockedAVDataSource;->d:Ljava/lang/String;

    const-class v2, Lcom/twitter/model/card/property/ImageSpec;

    invoke-virtual {v0, v1, v2}, Lcax;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/card/property/ImageSpec;

    .line 270
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/twitter/library/av/playback/TweetAVDataSource;->b()Lcom/twitter/model/card/property/ImageSpec;

    move-result-object v0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 275
    invoke-super {p0, p1, p2}, Lcom/twitter/library/av/playback/TweetAVDataSource;->writeToParcel(Landroid/os/Parcel;I)V

    .line 276
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo$LockedAVDataSource;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 277
    iget-object v0, p0, Lcom/twitter/android/revenue/card/UnlockableVideoToVideo$LockedAVDataSource;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 278
    return-void
.end method
