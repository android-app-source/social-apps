.class public abstract Lcom/twitter/android/revenue/card/n;
.super Lcom/twitter/android/card/n;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/revenue/card/n$a;
    }
.end annotation


# instance fields
.field private A:Landroid/view/View;

.field private C:Lcom/twitter/model/core/Tweet;

.field private D:Lcom/twitter/android/revenue/card/o;

.field protected final a:Lcar;

.field protected b:Landroid/view/ViewGroup;

.field protected c:Z

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/ui/widget/TwitterButton;",
            "Lcom/twitter/android/revenue/card/n$a;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private final l:Lcom/twitter/library/util/s;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/TextView;

.field private o:Landroid/widget/TextView;

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/android/revenue/card/n$a;",
            ">;"
        }
    .end annotation
.end field

.field private z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/ui/widget/TwitterButton;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;Lcar;)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 84
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/card/n;-><init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V

    .line 86
    new-instance v0, Lcom/twitter/android/revenue/card/o;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 87
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/revenue/card/o;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    iput-object v0, p0, Lcom/twitter/android/revenue/card/n;->D:Lcom/twitter/android/revenue/card/o;

    .line 89
    iput-object p5, p0, Lcom/twitter/android/revenue/card/n;->a:Lcar;

    .line 91
    const-string/jumbo v0, "thank_you_text"

    invoke-static {v0, p5}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/n;->f:Ljava/lang/String;

    .line 92
    const-string/jumbo v0, "thank_you_url"

    invoke-static {v0, p5}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/n;->g:Ljava/lang/String;

    .line 93
    const-string/jumbo v0, "thank_you_vanity_url"

    invoke-static {v0, p5}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/n;->h:Ljava/lang/String;

    .line 94
    const-string/jumbo v0, "title"

    invoke-static {v0, p5}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/n;->i:Ljava/lang/String;

    .line 95
    const-string/jumbo v0, "card_url"

    invoke-static {v0, p5}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/n;->j:Ljava/lang/String;

    .line 96
    const-string/jumbo v0, "card_id"

    invoke-static {v0, p5}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/n;->k:Ljava/lang/String;

    .line 97
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->k:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string/jumbo v1, "Card id should be specified."

    invoke-static {v0, v1}, Lcom/twitter/util/f;->a(ZLjava/lang/String;)Z

    .line 98
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->k:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    new-instance v0, Lcpb;

    new-instance v1, Ljava/lang/Exception;

    const-string/jumbo v2, "card_id_not_present"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcpb;-><init>(Ljava/lang/Throwable;)V

    const-string/jumbo v1, "card_id_not_present_card_url"

    iget-object v2, p0, Lcom/twitter/android/revenue/card/n;->j:Ljava/lang/String;

    .line 100
    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    .line 99
    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->r:Lcom/twitter/android/card/d;

    const-string/jumbo v1, "_card_data"

    invoke-static {v1, p5}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/card/d;->a(Ljava/lang/String;)V

    .line 105
    new-instance v0, Lcom/twitter/android/revenue/card/n$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/revenue/card/n$1;-><init>(Lcom/twitter/android/revenue/card/n;)V

    iput-object v0, p0, Lcom/twitter/android/revenue/card/n;->l:Lcom/twitter/library/util/s;

    .line 112
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/n;->p()V

    .line 113
    invoke-direct {p0, p2}, Lcom/twitter/android/revenue/card/n;->a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V

    .line 115
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/revenue/card/n;->e:Ljava/util/Map;

    .line 116
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v7

    .line 117
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v8

    .line 118
    sget-object v9, Lcom/twitter/android/revenue/card/q;->d:Ljava/util/List;

    move v4, v6

    .line 119
    :goto_0
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_2

    .line 120
    invoke-interface {v9, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/twitter/util/collection/Pair;

    .line 121
    new-instance v0, Lcom/twitter/android/revenue/card/n$a;

    .line 122
    invoke-virtual {v3}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1, p5}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    .line 124
    invoke-virtual {v3}, Lcom/twitter/util/collection/Pair;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2, p5}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/revenue/card/n$a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/twitter/android/revenue/card/n$1;)V

    .line 126
    invoke-static {v0}, Lcom/twitter/android/revenue/card/n$a;->a(Lcom/twitter/android/revenue/card/n$a;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 127
    invoke-virtual {v8, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 128
    iget-object v0, v0, Lcom/twitter/android/revenue/card/n$a;->a:Ljava/lang/String;

    .line 129
    iget-object v1, p0, Lcom/twitter/android/revenue/card/n;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0212

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 130
    iget-object v2, p0, Lcom/twitter/android/revenue/card/n;->q:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/twitter/android/revenue/i;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/text/Spannable;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 119
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 133
    :cond_2
    invoke-virtual {v8}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/n;->p:Ljava/util/List;

    .line 134
    invoke-virtual {v7}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/n;->d:Ljava/util/List;

    .line 136
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/n;->i()V

    .line 137
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/n;->h()V

    .line 138
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/n;->f()V

    .line 139
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/revenue/card/n;)Lcom/twitter/android/card/d;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->r:Lcom/twitter/android/card/d;

    return-object v0
.end method

.method private a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 317
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->b:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    .line 318
    invoke-static {v0, p1, p2, v1}, Lcom/twitter/library/scribe/b;->a(Landroid/view/View;Landroid/view/View;Landroid/view/MotionEvent;I)Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    move-result-object v0

    .line 320
    iget-object v1, p0, Lcom/twitter/android/revenue/card/n;->r:Lcom/twitter/android/card/d;

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->o:Lcom/twitter/library/api/PromotedEvent;

    invoke-interface {v1, v2, v0}, Lcom/twitter/android/card/d;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 321
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/revenue/card/n$a;

    .line 323
    iget-object v1, v0, Lcom/twitter/android/revenue/card/n$a;->b:Ljava/lang/String;

    iget v0, v0, Lcom/twitter/android/revenue/card/n$a;->d:I

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/revenue/card/n;->a(Ljava/lang/String;I)V

    .line 331
    :goto_0
    return-void

    .line 325
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/n;->c:Z

    if-eqz v0, :cond_1

    .line 326
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->v:Lcom/twitter/android/card/CardActionHelper;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/n;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/card/CardActionHelper;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 328
    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/n;->s()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/revenue/card/n;Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/revenue/card/n;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    return-void
.end method

.method private a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V
    .locals 4

    .prologue
    .line 213
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->q:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/twitter/android/revenue/card/n;->a(Landroid/content/Context;)Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/n;->b:Landroid/view/ViewGroup;

    .line 214
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->b:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/n;->l:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 216
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->b:Landroid/view/ViewGroup;

    const v1, 0x7f130569

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 217
    const v1, 0x7f04023a

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 218
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 222
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->q:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/twitter/android/revenue/j;->a(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    const/4 v1, 0x0

    .line 224
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0e0062

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    move v2, v1

    move v1, v0

    .line 229
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->b:Landroid/view/ViewGroup;

    const v3, 0x7f130213

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 230
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 231
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->b:Landroid/view/ViewGroup;

    const v2, 0x7f130573

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 232
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x2

    invoke-direct {v2, v1, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 234
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/n;->q()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 236
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->b:Landroid/view/ViewGroup;

    const v1, 0x7f130572

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/n;->A:Landroid/view/View;

    .line 237
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->l:Lcom/twitter/library/util/s;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/n;->A:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/twitter/library/util/s;->a(Landroid/view/View;)V

    .line 238
    return-void

    .line 226
    :cond_0
    const/4 v1, 0x1

    .line 227
    const/4 v0, -0x1

    move v2, v1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/revenue/card/n;)Ljava/util/List;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->p:Ljava/util/List;

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->b:Landroid/view/ViewGroup;

    const v1, 0x7f130574

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/n;->n:Landroid/widget/TextView;

    .line 148
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->n:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/n;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->g:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->b:Landroid/view/ViewGroup;

    const v1, 0x7f130575

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/n;->o:Landroid/widget/TextView;

    .line 151
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->h:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 152
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->o:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/n;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->o:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/n;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private g()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 200
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->m:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    .line 204
    invoke-virtual {v0, v3}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    goto :goto_0

    .line 206
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 207
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->o:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 208
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 210
    :cond_2
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 253
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->b:Landroid/view/ViewGroup;

    const v1, 0x7f13011b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/n;->m:Landroid/widget/TextView;

    .line 254
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->m:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 262
    :goto_0
    return-void

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->i:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 258
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->m:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/n;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 260
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->m:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private i()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 265
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 267
    const/4 v0, 0x4

    new-array v2, v0, [Lcom/twitter/ui/widget/TwitterButton;

    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->b:Landroid/view/ViewGroup;

    const v4, 0x7f130576

    .line 268
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    aput-object v0, v2, v3

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->b:Landroid/view/ViewGroup;

    const v5, 0x7f130577

    .line 269
    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    aput-object v0, v2, v4

    const/4 v4, 0x2

    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->b:Landroid/view/ViewGroup;

    const v5, 0x7f130578

    .line 270
    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    aput-object v0, v2, v4

    const/4 v4, 0x3

    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->b:Landroid/view/ViewGroup;

    const v5, 0x7f130579

    .line 271
    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    aput-object v0, v2, v4

    .line 268
    invoke-virtual {v1, v2}, Lcom/twitter/util/collection/h;->b([Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 272
    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/n;->z:Ljava/util/List;

    move v2, v3

    .line 273
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->z:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    .line 275
    iget-object v1, p0, Lcom/twitter/android/revenue/card/n;->d:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    sget-object v4, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v1, v4}, Lcom/twitter/ui/widget/TwitterButton;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 276
    iget-object v1, p0, Lcom/twitter/android/revenue/card/n;->p:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/revenue/card/n$a;

    iget-object v1, v1, Lcom/twitter/android/revenue/card/n$a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setTag(Ljava/lang/Object;)V

    .line 277
    new-instance v1, Lcom/twitter/android/revenue/card/n$2;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/revenue/card/n$2;-><init>(Lcom/twitter/android/revenue/card/n;Lcom/twitter/ui/widget/TwitterButton;)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 283
    iget-object v1, p0, Lcom/twitter/android/revenue/card/n;->e:Ljava/util/Map;

    iget-object v4, p0, Lcom/twitter/android/revenue/card/n;->p:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    invoke-virtual {v0, v3}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 273
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 286
    :cond_0
    return-void
.end method

.method private s()V
    .locals 2

    .prologue
    .line 289
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 290
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->p:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/revenue/card/n$a;

    .line 291
    iget-object v1, v0, Lcom/twitter/android/revenue/card/n$a;->b:Ljava/lang/String;

    iget v0, v0, Lcom/twitter/android/revenue/card/n$a;->d:I

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/revenue/card/n;->a(Ljava/lang/String;I)V

    .line 295
    :goto_0
    return-void

    .line 293
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/n;->t()V

    goto :goto_0
.end method

.method private t()V
    .locals 3

    .prologue
    .line 298
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/n;->l()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a0213

    .line 299
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->d:Ljava/util/List;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/n;->d:Ljava/util/List;

    .line 300
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/CharSequence;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    new-instance v2, Lcom/twitter/android/revenue/card/n$3;

    invoke-direct {v2, p0}, Lcom/twitter/android/revenue/card/n$3;-><init>(Lcom/twitter/android/revenue/card/n;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 308
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 309
    return-void
.end method


# virtual methods
.method a(Landroid/content/Context;)Landroid/view/ViewGroup;
    .locals 4

    .prologue
    .line 241
    .line 242
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040238

    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x0

    .line 243
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 242
    return-object v0
.end method

.method public a(JLcaq;)V
    .locals 3

    .prologue
    .line 184
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/card/n;->a(JLcaq;)V

    .line 185
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/n;->j()Ljava/lang/String;

    move-result-object v0

    .line 187
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string/jumbo v2, "tweet_send"

    invoke-virtual {p3, v2}, Lcaq;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 188
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/revenue/card/n;->D:Lcom/twitter/android/revenue/card/o;

    invoke-virtual {v1, v0}, Lcom/twitter/android/revenue/card/o;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->D:Lcom/twitter/android/revenue/card/o;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/n;->k:Ljava/lang/String;

    .line 189
    invoke-virtual {v0, v1}, Lcom/twitter/android/revenue/card/o;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 190
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/n;->k()V

    .line 192
    :cond_2
    return-void
.end method

.method public a(JLcom/twitter/library/card/CardContext;)V
    .locals 3

    .prologue
    .line 173
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/card/n;->a(JLcom/twitter/library/card/CardContext;)V

    .line 174
    invoke-static {p3}, Lcom/twitter/library/card/CardContext;->a(Lcom/twitter/library/card/CardContext;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/n;->C:Lcom/twitter/model/core/Tweet;

    .line 175
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/n;->j()Ljava/lang/String;

    move-result-object v0

    .line 176
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/revenue/card/n;->D:Lcom/twitter/android/revenue/card/o;

    invoke-virtual {v1, v0}, Lcom/twitter/android/revenue/card/o;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->D:Lcom/twitter/android/revenue/card/o;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/n;->k:Ljava/lang/String;

    .line 177
    invoke-virtual {v0, v1}, Lcom/twitter/android/revenue/card/o;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 178
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/n;->k()V

    .line 180
    :cond_2
    return-void
.end method

.method protected a(Ljava/lang/String;I)V
    .locals 7

    .prologue
    .line 334
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->w:Lcom/twitter/library/card/CardContext;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->w:Lcom/twitter/library/card/CardContext;

    invoke-virtual {v0}, Lcom/twitter/library/card/CardContext;->a()Lcax;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 335
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->s:Lcom/twitter/android/card/b;

    iget-wide v2, p0, Lcom/twitter/android/revenue/card/n;->y:J

    iget-object v1, p0, Lcom/twitter/android/revenue/card/n;->w:Lcom/twitter/library/card/CardContext;

    .line 336
    invoke-virtual {v1}, Lcom/twitter/library/card/CardContext;->a()Lcax;

    move-result-object v4

    iget-object v1, p0, Lcom/twitter/android/revenue/card/n;->w:Lcom/twitter/library/card/CardContext;

    invoke-virtual {v1}, Lcom/twitter/library/card/CardContext;->i()Lcgi;

    move-result-object v5

    move-object v1, p1

    move v6, p2

    .line 335
    invoke-interface/range {v0 .. v6}, Lcom/twitter/android/card/b;->a(Ljava/lang/String;JLcax;Lcgi;I)V

    .line 338
    :cond_0
    return-void
.end method

.method a([F)V
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->A:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->A:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/n;->q:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/twitter/android/revenue/j;->a(Landroid/view/View;Landroid/content/res/Resources;[F)V

    .line 250
    :cond_0
    return-void
.end method

.method public d()V
    .locals 4

    .prologue
    .line 161
    invoke-super {p0}, Lcom/twitter/android/card/n;->d()V

    .line 162
    invoke-static {}, Lcom/twitter/android/card/h;->b()Lcom/twitter/android/card/h;

    move-result-object v0

    .line 163
    iget-wide v2, p0, Lcom/twitter/android/revenue/card/n;->y:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/card/h;->b(J)Ljava/lang/Integer;

    move-result-object v0

    .line 165
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/n;->k()V

    .line 167
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->D:Lcom/twitter/android/revenue/card/o;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/n;->k:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/revenue/card/o;->b(Ljava/lang/String;)V

    .line 169
    :cond_0
    return-void
.end method

.method public e()Landroid/view/View;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->b:Landroid/view/ViewGroup;

    return-object v0
.end method

.method j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->C:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->C:Lcom/twitter/model/core/Tweet;

    invoke-static {v0}, Lcom/twitter/android/revenue/card/j;->a(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/n;->j:Ljava/lang/String;

    goto :goto_0
.end method

.method k()V
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/revenue/card/n;->c:Z

    .line 196
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/n;->g()V

    .line 197
    return-void
.end method

.method public abstract p()V
.end method

.method public abstract q()Landroid/view/View;
.end method
