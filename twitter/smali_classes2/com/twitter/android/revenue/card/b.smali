.class public abstract Lcom/twitter/android/revenue/card/b;
.super Lcom/twitter/android/revenue/card/a;
.source "Twttr"


# instance fields
.field protected final a:Landroid/view/ViewGroup;

.field protected final b:Landroid/view/View;

.field protected final c:Lcom/twitter/library/util/s;

.field protected final d:Lcom/twitter/android/revenue/card/StatsAndCtaView;

.field protected final e:Lcom/twitter/media/ui/image/MediaImageView;

.field protected final f:Landroid/widget/ImageView;

.field private final g:Lcom/mopub/nativeads/NativeAd;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/mopub/nativeads/NativeAd;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;[F[F)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 52
    invoke-direct {p0, p1, p3, p4, p5}, Lcom/twitter/android/revenue/card/a;-><init>(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V

    .line 53
    iget-object v0, p0, Lcom/twitter/android/revenue/card/b;->i:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/revenue/card/b;->a(Landroid/content/Context;)Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/b;->a:Landroid/view/ViewGroup;

    .line 54
    iput-object p2, p0, Lcom/twitter/android/revenue/card/b;->g:Lcom/mopub/nativeads/NativeAd;

    .line 56
    iget-object v0, p0, Lcom/twitter/android/revenue/card/b;->i:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Lcom/mopub/nativeads/NativeAd;->createAdView(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/b;->b:Landroid/view/View;

    .line 57
    iget-object v0, p0, Lcom/twitter/android/revenue/card/b;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/b;->b:Landroid/view/View;

    invoke-virtual {v0, v1, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 59
    new-instance v0, Lcom/twitter/android/revenue/card/b$1;

    invoke-direct {v0, p0, v5}, Lcom/twitter/android/revenue/card/b$1;-><init>(Lcom/twitter/android/revenue/card/b;Z)V

    iput-object v0, p0, Lcom/twitter/android/revenue/card/b;->c:Lcom/twitter/library/util/s;

    .line 67
    iget-object v0, p0, Lcom/twitter/android/revenue/card/b;->a:Landroid/view/ViewGroup;

    invoke-static {}, Lcom/twitter/android/revenue/card/b;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/revenue/card/StatsAndCtaView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/b;->d:Lcom/twitter/android/revenue/card/StatsAndCtaView;

    .line 68
    iget-object v0, p0, Lcom/twitter/android/revenue/card/b;->d:Lcom/twitter/android/revenue/card/StatsAndCtaView;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/b;->c:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->setOnClickTouchListener(Lcom/twitter/library/util/s;)V

    .line 70
    iget-object v0, p0, Lcom/twitter/android/revenue/card/b;->a:Landroid/view/ViewGroup;

    const v1, 0x7f130214

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/b;->e:Lcom/twitter/media/ui/image/MediaImageView;

    .line 71
    iget-object v0, p0, Lcom/twitter/android/revenue/card/b;->e:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/b;->c:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 72
    iget-object v0, p0, Lcom/twitter/android/revenue/card/b;->e:Lcom/twitter/media/ui/image/MediaImageView;

    aget v1, p7, v5

    const/4 v2, 0x1

    aget v2, p7, v2

    const/4 v3, 0x2

    aget v3, p7, v3

    const/4 v4, 0x3

    aget v4, p7, v4

    invoke-static {v1, v2, v3, v4}, Lcom/twitter/media/ui/image/config/d;->a(FFFF)Lcom/twitter/media/ui/image/config/g;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setRoundingStrategy(Lcom/twitter/media/ui/image/config/g;)V

    .line 74
    iget-object v0, p0, Lcom/twitter/android/revenue/card/b;->e:Lcom/twitter/media/ui/image/MediaImageView;

    const v1, 0x3ff47ae1    # 1.91f

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 75
    iget-object v0, p0, Lcom/twitter/android/revenue/card/b;->e:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-static {}, Lcom/twitter/android/revenue/card/b;->h()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/b;->f:Landroid/widget/ImageView;

    .line 76
    iget-object v0, p0, Lcom/twitter/android/revenue/card/b;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 77
    return-void
.end method

.method private static a(Landroid/content/Context;)Landroid/view/ViewGroup;
    .locals 4

    .prologue
    .line 86
    .line 87
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040256

    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x0

    .line 88
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 87
    return-object v0
.end method

.method public static f()Lcom/mopub/nativeads/ViewBinder;
    .locals 3

    .prologue
    .line 120
    new-instance v0, Lcom/mopub/nativeads/ViewBinder$Builder;

    const v1, 0x7f040255

    invoke-direct {v0, v1}, Lcom/mopub/nativeads/ViewBinder$Builder;-><init>(I)V

    .line 122
    invoke-static {}, Lcom/twitter/android/revenue/card/b;->h()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mopub/nativeads/ViewBinder$Builder;->mainImageId(I)Lcom/mopub/nativeads/ViewBinder$Builder;

    move-result-object v0

    .line 123
    invoke-static {}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->getCtaViewId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mopub/nativeads/ViewBinder$Builder;->callToActionId(I)Lcom/mopub/nativeads/ViewBinder$Builder;

    move-result-object v0

    .line 124
    invoke-static {}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->getTitleViewId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mopub/nativeads/ViewBinder$Builder;->titleId(I)Lcom/mopub/nativeads/ViewBinder$Builder;

    move-result-object v0

    const-string/jumbo v1, "key_store"

    .line 126
    invoke-static {}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->getAppCategoryViewId()I

    move-result v2

    .line 125
    invoke-virtual {v0, v1, v2}, Lcom/mopub/nativeads/ViewBinder$Builder;->addExtra(Ljava/lang/String;I)Lcom/mopub/nativeads/ViewBinder$Builder;

    move-result-object v0

    .line 127
    invoke-virtual {v0}, Lcom/mopub/nativeads/ViewBinder$Builder;->build()Lcom/mopub/nativeads/ViewBinder;

    move-result-object v0

    return-object v0
.end method

.method private static g()I
    .locals 1

    .prologue
    .line 112
    const v0, 0x7f130565

    return v0
.end method

.method private static h()I
    .locals 1

    .prologue
    .line 116
    const v0, 0x7f130044

    return v0
.end method


# virtual methods
.method public a(JLcar;)V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/twitter/android/revenue/card/b;->d:Lcom/twitter/android/revenue/card/StatsAndCtaView;

    invoke-virtual {v0, p3}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->a(Lcar;)V

    .line 99
    const-string/jumbo v0, "app_category"

    invoke-virtual {p3, v0}, Lcar;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/twitter/android/revenue/card/b;->d:Lcom/twitter/android/revenue/card/StatsAndCtaView;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->a()V

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/b;->g:Lcom/mopub/nativeads/NativeAd;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/b;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/mopub/nativeads/NativeAd;->prepare(Landroid/view/View;)V

    .line 104
    iget-object v0, p0, Lcom/twitter/android/revenue/card/b;->g:Lcom/mopub/nativeads/NativeAd;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/b;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/mopub/nativeads/NativeAd;->renderAdView(Landroid/view/View;)V

    .line 105
    return-void
.end method

.method protected a(Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V
    .locals 3

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/revenue/card/b;->d_:Lcom/twitter/android/card/d;

    const-string/jumbo v1, "open_link"

    iget-object v2, p0, Lcom/twitter/android/revenue/card/b;->p:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 81
    invoke-static {v2}, Lcom/twitter/android/card/g;->a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Ljava/lang/String;

    move-result-object v2

    .line 80
    invoke-interface {v0, v1, v2, p1}, Lcom/twitter/android/card/d;->c(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 82
    iget-object v0, p0, Lcom/twitter/android/revenue/card/b;->d_:Lcom/twitter/android/card/d;

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->n:Lcom/twitter/library/api/PromotedEvent;

    invoke-interface {v0, v1, p1}, Lcom/twitter/android/card/d;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 83
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 109
    return-void
.end method

.method public e()Landroid/view/View;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/twitter/android/revenue/card/b;->a:Landroid/view/ViewGroup;

    return-object v0
.end method
