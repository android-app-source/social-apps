.class final Lcom/twitter/android/revenue/card/ap$1;
.super Lcom/twitter/library/util/s;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/revenue/card/ap;->a(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;Ljava/lang/String;Lcom/twitter/android/card/d;Landroid/view/View;Lcom/twitter/library/card/CardContext;Lcom/twitter/android/av/AVCardCanvasActivity$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/twitter/android/card/d;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Landroid/content/Context;

.field final synthetic f:Lcom/twitter/library/card/CardContext;

.field final synthetic g:Lcom/twitter/android/av/AVCardCanvasActivity$a;

.field final synthetic h:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/twitter/android/card/d;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Lcom/twitter/library/card/CardContext;Lcom/twitter/android/av/AVCardCanvasActivity$a;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/twitter/android/revenue/card/ap$1;->a:Landroid/view/View;

    iput-object p2, p0, Lcom/twitter/android/revenue/card/ap$1;->b:Lcom/twitter/android/card/d;

    iput-object p3, p0, Lcom/twitter/android/revenue/card/ap$1;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/android/revenue/card/ap$1;->d:Ljava/lang/String;

    iput-object p5, p0, Lcom/twitter/android/revenue/card/ap$1;->e:Landroid/content/Context;

    iput-object p6, p0, Lcom/twitter/android/revenue/card/ap$1;->f:Lcom/twitter/library/card/CardContext;

    iput-object p7, p0, Lcom/twitter/android/revenue/card/ap$1;->g:Lcom/twitter/android/av/AVCardCanvasActivity$a;

    iput-object p8, p0, Lcom/twitter/android/revenue/card/ap$1;->h:Landroid/view/View;

    invoke-direct {p0}, Lcom/twitter/library/util/s;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 86
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ap$1;->a:Landroid/view/View;

    .line 87
    invoke-static {v0, p1, p2, v4}, Lcom/twitter/library/scribe/b;->a(Landroid/view/View;Landroid/view/View;Landroid/view/MotionEvent;I)Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/twitter/android/revenue/card/ap$1;->b:Lcom/twitter/android/card/d;

    const-string/jumbo v2, "card_click"

    iget-object v3, p0, Lcom/twitter/android/revenue/card/ap$1;->c:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, Lcom/twitter/android/card/d;->c(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 91
    iget-object v1, p0, Lcom/twitter/android/revenue/card/ap$1;->b:Lcom/twitter/android/card/d;

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->p:Lcom/twitter/library/api/PromotedEvent;

    invoke-interface {v1, v2, v0}, Lcom/twitter/android/card/d;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 93
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ap$1;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ap$1;->e:Landroid/content/Context;

    const v1, 0x7f0a009b

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 106
    :goto_0
    return-void

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ap$1;->f:Lcom/twitter/library/card/CardContext;

    invoke-static {v0}, Lcom/twitter/library/card/CardContext;->a(Lcom/twitter/library/card/CardContext;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    .line 99
    iget-object v1, p0, Lcom/twitter/android/revenue/card/ap$1;->g:Lcom/twitter/android/av/AVCardCanvasActivity$a;

    invoke-virtual {v1, v0}, Lcom/twitter/android/av/AVCardCanvasActivity$a;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/av/AVCardCanvasActivity$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/revenue/card/ap$1;->d:Ljava/lang/String;

    .line 100
    invoke-virtual {v0, v1}, Lcom/twitter/android/av/AVCardCanvasActivity$a;->a(Ljava/lang/String;)Lcom/twitter/android/av/AVCardCanvasActivity$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/revenue/card/ap$1;->b:Lcom/twitter/android/card/d;

    .line 101
    invoke-interface {v1}, Lcom/twitter/android/card/d;->a()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/AVCardCanvasActivity$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/av/AVCardCanvasActivity$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/revenue/card/ap$1;->h:Landroid/view/View;

    .line 102
    invoke-virtual {v0, v1}, Lcom/twitter/android/av/AVCardCanvasActivity$a;->a(Landroid/view/View;)Lcom/twitter/android/av/AVCardCanvasActivity$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/revenue/card/ap$1;->e:Landroid/content/Context;

    .line 104
    invoke-static {v1}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v1

    invoke-virtual {v1}, Lbaa;->k()Z

    move-result v1

    .line 103
    invoke-virtual {v0, v1}, Lcom/twitter/android/av/AVCardCanvasActivity$a;->a(Z)Lcom/twitter/android/av/AVCardCanvasActivity$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/revenue/card/ap$1;->e:Landroid/content/Context;

    .line 105
    invoke-virtual {v0, v1}, Lcom/twitter/android/av/AVCardCanvasActivity$a;->b(Landroid/content/Context;)V

    goto :goto_0
.end method
