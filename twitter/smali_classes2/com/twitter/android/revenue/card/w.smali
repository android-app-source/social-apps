.class public abstract Lcom/twitter/android/revenue/card/w;
.super Lcom/twitter/android/card/n;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/revenue/card/v$a;
.implements Lcom/twitter/library/card/ak$a;
.implements Lcom/twitter/library/card/q$a;


# instance fields
.field private A:Landroid/view/View;

.field private C:Landroid/view/View;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field private F:Z

.field private G:Ljava/lang/String;

.field private H:Z

.field private I:Ljava/lang/String;

.field private J:Ljava/lang/Long;

.field protected final a:Landroid/view/View;

.field protected final b:Lcom/twitter/ui/widget/TwitterButton;

.field protected c:Ljava/lang/String;

.field protected d:Lcom/twitter/library/util/s;

.field protected e:Landroid/widget/TextView;

.field protected final f:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

.field private final g:Lcaq;

.field private h:Lcom/twitter/android/revenue/card/v;

.field private final i:Lcom/twitter/media/ui/image/MediaImageView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/TextView;

.field private m:Lcom/twitter/media/ui/image/UserImageView;

.field private n:Landroid/widget/TextView;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/widget/TextView;

.field private z:Landroid/widget/TextView;


# direct methods
.method protected constructor <init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V
    .locals 3

    .prologue
    .line 81
    new-instance v0, Lcom/twitter/android/card/f;

    invoke-direct {v0, p1}, Lcom/twitter/android/card/f;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/twitter/android/card/c;

    invoke-direct {v1, p1}, Lcom/twitter/android/card/c;-><init>(Landroid/app/Activity;)V

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/twitter/android/card/n;-><init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V

    .line 55
    new-instance v0, Lcaq;

    invoke-direct {v0}, Lcaq;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/revenue/card/w;->g:Lcaq;

    .line 83
    iput-object p2, p0, Lcom/twitter/android/revenue/card/w;->f:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 84
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/w;->f()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/w;->a:Landroid/view/View;

    .line 86
    sget-object v0, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne v0, p2, :cond_0

    .line 87
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->a:Landroid/view/View;

    const v1, 0x7f130585

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 88
    if-eqz v0, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/w;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 90
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->a:Landroid/view/View;

    const v1, 0x7f130214

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/w;->i:Lcom/twitter/media/ui/image/MediaImageView;

    .line 95
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->i:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->i:Lcom/twitter/media/ui/image/MediaImageView;

    const/high16 v1, 0x40800000    # 4.0f

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 97
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->i:Lcom/twitter/media/ui/image/MediaImageView;

    const-string/jumbo v1, "card"

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setImageType(Ljava/lang/String;)V

    .line 99
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->a:Landroid/view/View;

    sget-object v0, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/w;->f:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne v0, v2, :cond_5

    const v0, 0x7f130588

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/w;->b:Lcom/twitter/ui/widget/TwitterButton;

    .line 101
    sget-object v0, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne p2, v0, :cond_3

    .line 102
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->a:Landroid/view/View;

    const v1, 0x7f130218

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/w;->e:Landroid/widget/TextView;

    .line 103
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->a:Landroid/view/View;

    const v1, 0x7f13058a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/w;->j:Landroid/widget/TextView;

    .line 104
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->a:Landroid/view/View;

    const v1, 0x7f13058b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/w;->m:Lcom/twitter/media/ui/image/UserImageView;

    .line 105
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->m:Lcom/twitter/media/ui/image/UserImageView;

    if-eqz v0, :cond_2

    .line 106
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->m:Lcom/twitter/media/ui/image/UserImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setAspectRatio(F)V

    .line 108
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->a:Landroid/view/View;

    const v1, 0x7f130589

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/w;->k:Landroid/widget/TextView;

    .line 109
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->a:Landroid/view/View;

    const v1, 0x7f130591

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/w;->l:Landroid/widget/TextView;

    .line 110
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->a:Landroid/view/View;

    const v1, 0x7f13058c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/w;->n:Landroid/widget/TextView;

    .line 111
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->a:Landroid/view/View;

    const v1, 0x7f13058d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/w;->o:Landroid/widget/TextView;

    .line 112
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->a:Landroid/view/View;

    const v1, 0x7f13058e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/w;->p:Landroid/widget/TextView;

    .line 113
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->a:Landroid/view/View;

    const v1, 0x7f130592

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/w;->z:Landroid/widget/TextView;

    .line 114
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->a:Landroid/view/View;

    const v1, 0x7f130586

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/w;->A:Landroid/view/View;

    .line 115
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->a:Landroid/view/View;

    const v1, 0x7f13058f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/w;->C:Landroid/view/View;

    .line 118
    :cond_3
    new-instance v0, Lcom/twitter/android/revenue/card/w$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/revenue/card/w$1;-><init>(Lcom/twitter/android/revenue/card/w;)V

    iput-object v0, p0, Lcom/twitter/android/revenue/card/w;->d:Lcom/twitter/library/util/s;

    .line 124
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->f:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne v0, v1, :cond_4

    .line 125
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->d:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 127
    :cond_4
    return-void

    .line 99
    :cond_5
    const v0, 0x7f130217

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/revenue/card/w;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->D:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/revenue/card/w;Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/revenue/card/w;->b(Landroid/view/View;Landroid/view/MotionEvent;)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/revenue/card/w;)Lcom/twitter/android/card/CardActionHelper;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->v:Lcom/twitter/android/card/CardActionHelper;

    return-object v0
.end method

.method private b(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 4

    .prologue
    .line 291
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->w:Lcom/twitter/library/card/CardContext;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->v:Lcom/twitter/android/card/CardActionHelper;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->w:Lcom/twitter/library/card/CardContext;

    .line 294
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/w;->e()Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    .line 293
    invoke-static {v2, p1, p2, v3}, Lcom/twitter/library/scribe/b;->a(Landroid/view/View;Landroid/view/View;Landroid/view/MotionEvent;I)Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    move-result-object v2

    .line 292
    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/card/CardActionHelper;->a(Lcom/twitter/library/card/CardContext;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 296
    :cond_0
    return-void
.end method

.method private b(Lcar;)V
    .locals 3

    .prologue
    .line 257
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->i:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_1

    .line 258
    const-string/jumbo v0, "promo_image"

    invoke-static {v0, p1}, Lcas;->a(Ljava/lang/String;Lcar;)Lcas;

    move-result-object v0

    .line 259
    if-eqz v0, :cond_0

    .line 260
    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->i:Lcom/twitter/media/ui/image/MediaImageView;

    const/high16 v2, 0x40800000    # 4.0f

    invoke-virtual {v0, v2}, Lcas;->a(F)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 261
    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->i:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v0, v0, Lcas;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 262
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->i:Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setFromMemoryOnly(Z)V

    .line 264
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->i:Lcom/twitter/media/ui/image/MediaImageView;

    const-string/jumbo v1, "promo_image"

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setTag(Ljava/lang/Object;)V

    .line 266
    :cond_1
    return-void
.end method

.method static synthetic c(Lcom/twitter/android/revenue/card/w;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->E:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/revenue/card/w;)Lcom/twitter/android/card/CardActionHelper;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->v:Lcom/twitter/android/card/CardActionHelper;

    return-object v0
.end method

.method private k()V
    .locals 1

    .prologue
    .line 249
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/w;->H:Z

    if-eqz v0, :cond_0

    .line 250
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/w;->q()V

    .line 254
    :goto_0
    return-void

    .line 252
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/w;->p()V

    goto :goto_0
.end method

.method private p()V
    .locals 4

    .prologue
    .line 314
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 315
    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->q:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 316
    iget-object v2, p0, Lcom/twitter/android/revenue/card/w;->p:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 317
    iget-object v2, p0, Lcom/twitter/android/revenue/card/w;->p:Landroid/widget/TextView;

    const v3, 0x7f0a0a25

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 318
    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->p:Landroid/widget/TextView;

    new-instance v2, Lcom/twitter/android/revenue/card/w$3;

    invoke-direct {v2, p0}, Lcom/twitter/android/revenue/card/w$3;-><init>(Lcom/twitter/android/revenue/card/w;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 326
    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->m:Lcom/twitter/media/ui/image/UserImageView;

    if-eqz v1, :cond_1

    .line 327
    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->m:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/model/core/TwitterUser;)Z

    .line 328
    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->m:Lcom/twitter/media/ui/image/UserImageView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/twitter/media/ui/image/UserImageView;->setFromMemoryOnly(Z)V

    .line 330
    :cond_1
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->n:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 331
    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->n:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 333
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->o:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 334
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->o:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->G:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 336
    :cond_3
    return-void
.end method

.method private q()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 339
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->A:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->A:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 342
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->C:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 343
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->C:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 345
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->q:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 346
    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->j:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 347
    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->j:Landroid/widget/TextView;

    const v2, 0x7f0a043e

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/android/revenue/card/w;->I:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 348
    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->j:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 350
    :cond_2
    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->z:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    .line 351
    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->z:Landroid/widget/TextView;

    const v2, 0x7f0a0443

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 352
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->z:Landroid/widget/TextView;

    const-string/jumbo v1, "title"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 353
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->z:Landroid/widget/TextView;

    new-instance v1, Lcom/twitter/android/revenue/card/w$4;

    invoke-direct {v1, p0}, Lcom/twitter/android/revenue/card/w$4;-><init>(Lcom/twitter/android/revenue/card/w;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 361
    :cond_3
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 155
    invoke-super {p0}, Lcom/twitter/android/card/n;->a()V

    .line 156
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->J:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 157
    invoke-static {}, Lcom/twitter/library/card/ak;->a()Lcom/twitter/library/card/ak;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->J:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/ak;->b(JLjava/lang/Object;)V

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->i:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->i:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->j()Z

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->m:Lcom/twitter/media/ui/image/UserImageView;

    if-eqz v0, :cond_2

    .line 164
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->m:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/UserImageView;->j()Z

    .line 167
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->h:Lcom/twitter/android/revenue/card/v;

    if-eqz v0, :cond_3

    .line 168
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->h:Lcom/twitter/android/revenue/card/v;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/v;->g()V

    .line 171
    :cond_3
    invoke-static {}, Lcom/twitter/library/card/q;->a()Lcom/twitter/library/card/q;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/revenue/card/w;->y:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/q;->b(JLjava/lang/Object;)V

    .line 172
    return-void
.end method

.method public a(JLcaq;)V
    .locals 2

    .prologue
    .line 244
    const-string/jumbo v0, "promotion_lead_submitted"

    const/4 v1, 0x0

    invoke-static {v0, p3, v1}, Lcom/twitter/library/card/g;->a(Ljava/lang/String;Lcar;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/revenue/card/w;->H:Z

    .line 245
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/w;->k()V

    .line 246
    return-void
.end method

.method public a(JLcar;)V
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->r:Lcom/twitter/android/card/d;

    const-string/jumbo v1, "_card_data"

    invoke-static {v1, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/card/d;->a(Ljava/lang/String;)V

    .line 208
    const-string/jumbo v0, "promotion_api_url"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/w;->c:Ljava/lang/String;

    .line 209
    const-string/jumbo v0, "promotion_privacy_url"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/w;->D:Ljava/lang/String;

    .line 210
    const-string/jumbo v0, "promotion_learn_more_url"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/w;->E:Ljava/lang/String;

    .line 211
    const-string/jumbo v0, "promotion_has_destination_url"

    const/4 v1, 0x0

    invoke-static {v0, p3, v1}, Lcom/twitter/library/card/g;->a(Ljava/lang/String;Lcar;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/revenue/card/w;->F:Z

    .line 212
    const-string/jumbo v0, "viewing_user_obfuscated_email_address"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/w;->G:Ljava/lang/String;

    .line 214
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->e:Landroid/widget/TextView;

    const-string/jumbo v1, "title"

    invoke-static {v1, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->e:Landroid/widget/TextView;

    const-string/jumbo v1, "title"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 219
    :cond_0
    invoke-direct {p0, p3}, Lcom/twitter/android/revenue/card/w;->b(Lcar;)V

    .line 221
    invoke-virtual {p0, p3}, Lcom/twitter/android/revenue/card/w;->a(Lcar;)V

    .line 223
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/w;->k()V

    .line 224
    return-void
.end method

.method public a(JLcom/twitter/model/core/TwitterUser;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 192
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->f:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->b:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->j:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p3, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/w;->I:Ljava/lang/String;

    .line 194
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->q:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 195
    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->j:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 196
    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/w;->i()I

    move-result v2

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p3, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->l:Landroid/widget/TextView;

    const v2, 0x7f0a043e

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p3, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    :cond_0
    return-void
.end method

.method protected a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 299
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->b:Lcom/twitter/ui/widget/TwitterButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 300
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 302
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/w;->j()Lcom/twitter/android/revenue/card/v;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/w;->h:Lcom/twitter/android/revenue/card/v;

    .line 303
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->h:Lcom/twitter/android/revenue/card/v;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/v;->e()V

    .line 304
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->r:Lcom/twitter/android/card/d;

    const-string/jumbo v1, "submit"

    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/w;->m()Ljava/lang/String;

    move-result-object v2

    .line 306
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/w;->e()Landroid/view/View;

    move-result-object v3

    .line 305
    invoke-static {v3, p1, p2, v4}, Lcom/twitter/library/scribe/b;->a(Landroid/view/View;Landroid/view/View;Landroid/view/MotionEvent;I)Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    move-result-object v3

    .line 304
    invoke-interface {v0, v1, v2, v3}, Lcom/twitter/android/card/d;->c(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 307
    return-void
.end method

.method protected a(Lcaq;)V
    .locals 2

    .prologue
    .line 239
    const-string/jumbo v0, "promotion_lead_submitted"

    iget-boolean v1, p0, Lcom/twitter/android/revenue/card/w;->H:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcaq;->a(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 240
    return-void
.end method

.method protected a(Lcar;)V
    .locals 3

    .prologue
    .line 269
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->b:Lcom/twitter/ui/widget/TwitterButton;

    if-eqz v0, :cond_0

    .line 270
    const-string/jumbo v0, "promotion_cta"

    invoke-static {v0, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    .line 271
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 272
    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->b:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TwitterButton;->setText(Ljava/lang/CharSequence;)V

    .line 276
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->b:Lcom/twitter/ui/widget/TwitterButton;

    const-string/jumbo v1, "button"

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setTag(Ljava/lang/Object;)V

    .line 277
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->b:Lcom/twitter/ui/widget/TwitterButton;

    new-instance v1, Lcom/twitter/android/revenue/card/w$2;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/w;->b:Lcom/twitter/ui/widget/TwitterButton;

    invoke-direct {v1, p0, v2}, Lcom/twitter/android/revenue/card/w$2;-><init>(Lcom/twitter/android/revenue/card/w;Lcom/twitter/ui/widget/TwitterButton;)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 288
    :cond_0
    return-void

    .line 274
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->b:Lcom/twitter/ui/widget/TwitterButton;

    const v1, 0x7f0a0439

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setText(I)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/card/z$a;)V
    .locals 4

    .prologue
    .line 144
    invoke-super {p0, p1}, Lcom/twitter/android/card/n;->a(Lcom/twitter/library/card/z$a;)V

    .line 145
    invoke-static {}, Lcom/twitter/library/card/q;->a()Lcom/twitter/library/card/q;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/revenue/card/w;->y:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/q;->a(JLjava/lang/Object;)V

    .line 147
    const-string/jumbo v0, "site"

    iget-object v1, p1, Lcom/twitter/library/card/z$a;->c:Lcar;

    invoke-static {v0, v1}, Lcom/twitter/library/card/y;->a(Ljava/lang/String;Lcar;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/w;->J:Ljava/lang/Long;

    .line 148
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->J:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 149
    invoke-static {}, Lcom/twitter/library/card/ak;->a()Lcom/twitter/library/card/ak;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->J:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/ak;->a(JLjava/lang/Object;)V

    .line 151
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 42
    check-cast p1, Lcom/twitter/library/card/z$a;

    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/card/w;->a(Lcom/twitter/library/card/z$a;)V

    return-void
.end method

.method public a(ZLjava/lang/String;)V
    .locals 3

    .prologue
    .line 365
    iput-boolean p1, p0, Lcom/twitter/android/revenue/card/w;->H:Z

    .line 366
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/w;->k()V

    .line 367
    if-eqz p1, :cond_1

    .line 368
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->g:Lcaq;

    const-string/jumbo v1, "promotion_lead_submitted"

    iget-boolean v2, p0, Lcom/twitter/android/revenue/card/w;->H:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcaq;->a(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 369
    iget-wide v0, p0, Lcom/twitter/android/revenue/card/w;->y:J

    iget-object v2, p0, Lcom/twitter/android/revenue/card/w;->g:Lcaq;

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/revenue/card/w;->b(JLcaq;)V

    .line 370
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/w;->F:Z

    if-eqz v0, :cond_0

    invoke-static {p2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 371
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->v:Lcom/twitter/android/card/CardActionHelper;

    invoke-virtual {v0, p2}, Lcom/twitter/android/card/CardActionHelper;->b(Ljava/lang/String;)V

    .line 378
    :cond_0
    :goto_0
    return-void

    .line 374
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->q:Landroid/content/Context;

    const v1, 0x7f0a043c

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 375
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->b:Lcom/twitter/ui/widget/TwitterButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 376
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->k:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public aj_()V
    .locals 3

    .prologue
    .line 228
    invoke-super {p0}, Lcom/twitter/android/card/n;->aj_()V

    .line 229
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->g:Lcaq;

    invoke-virtual {p0, v0}, Lcom/twitter/android/revenue/card/w;->a(Lcaq;)V

    .line 230
    iget-wide v0, p0, Lcom/twitter/android/revenue/card/w;->y:J

    iget-object v2, p0, Lcom/twitter/android/revenue/card/w;->g:Lcaq;

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/revenue/card/w;->b(JLcaq;)V

    .line 231
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 176
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->i:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->i:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setFromMemoryOnly(Z)V

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->m:Lcom/twitter/media/ui/image/UserImageView;

    if-eqz v0, :cond_1

    .line 181
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->m:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setFromMemoryOnly(Z)V

    .line 183
    :cond_1
    return-void
.end method

.method public e()Landroid/view/View;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->a:Landroid/view/View;

    return-object v0
.end method

.method protected f()Landroid/view/View;
    .locals 3

    .prologue
    .line 130
    iget-object v0, p0, Lcom/twitter/android/revenue/card/w;->q:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/w;->h()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected g()I
    .locals 1

    .prologue
    .line 139
    const v0, 0x7f040172

    return v0
.end method

.method protected h()I
    .locals 2

    .prologue
    .line 134
    sget-object v0, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->f:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne v0, v1, :cond_0

    const v0, 0x7f04023f

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f040240

    goto :goto_0
.end method

.method protected i()I
    .locals 1

    .prologue
    .line 202
    const v0, 0x7f0a08a8

    return v0
.end method

.method protected j()Lcom/twitter/android/revenue/card/v;
    .locals 4

    .prologue
    .line 310
    new-instance v0, Lcom/twitter/android/revenue/card/v;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/w;->q:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/w;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/revenue/card/w;->w:Lcom/twitter/library/card/CardContext;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/twitter/android/revenue/card/v;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/card/CardContext;Lcom/twitter/android/revenue/card/v$a;)V

    return-object v0
.end method
