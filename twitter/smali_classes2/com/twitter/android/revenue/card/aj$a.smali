.class Lcom/twitter/android/revenue/card/aj$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/revenue/card/al;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/revenue/card/aj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field private final b:Lcom/twitter/android/revenue/card/CardMediaView;

.field private final c:Lcom/twitter/android/revenue/card/CardMediaView;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcar;Lcom/twitter/library/widget/renderablecontent/DisplayMode;F)V
    .locals 4

    .prologue
    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 161
    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-direct {v1, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/twitter/android/revenue/card/aj$a;->a:Landroid/view/ViewGroup;

    .line 162
    iget-object v1, p0, Lcom/twitter/android/revenue/card/aj$a;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 164
    invoke-static {p1, p3}, Lcom/twitter/android/revenue/j;->a(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Z

    move-result v1

    .line 166
    new-instance v2, Lcom/twitter/android/revenue/card/CardMediaView;

    invoke-direct {v2, p1}, Lcom/twitter/android/revenue/card/CardMediaView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/twitter/android/revenue/card/aj$a;->b:Lcom/twitter/android/revenue/card/CardMediaView;

    .line 167
    const-string/jumbo v2, "cover_promo_image"

    invoke-static {v2, p2}, Lcas;->a(Ljava/lang/String;Lcar;)Lcas;

    move-result-object v2

    .line 168
    iget-object v3, p0, Lcom/twitter/android/revenue/card/aj$a;->b:Lcom/twitter/android/revenue/card/CardMediaView;

    invoke-direct {p0, v3, v2, v1, p4}, Lcom/twitter/android/revenue/card/aj$a;->a(Lcom/twitter/android/revenue/card/CardMediaView;Lcas;ZF)V

    .line 170
    new-instance v2, Lcom/twitter/android/revenue/card/CardMediaView;

    invoke-direct {v2, p1}, Lcom/twitter/android/revenue/card/CardMediaView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/twitter/android/revenue/card/aj$a;->c:Lcom/twitter/android/revenue/card/CardMediaView;

    .line 171
    const-string/jumbo v2, "promo_image"

    invoke-static {v2, p2}, Lcas;->a(Ljava/lang/String;Lcar;)Lcas;

    move-result-object v2

    .line 172
    iget-object v3, p0, Lcom/twitter/android/revenue/card/aj$a;->c:Lcom/twitter/android/revenue/card/CardMediaView;

    invoke-direct {p0, v3, v2, v1, p4}, Lcom/twitter/android/revenue/card/aj$a;->a(Lcom/twitter/android/revenue/card/CardMediaView;Lcas;ZF)V

    .line 174
    iget-object v1, p0, Lcom/twitter/android/revenue/card/aj$a;->a:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/aj$a;->c:Lcom/twitter/android/revenue/card/CardMediaView;

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 175
    iget-object v1, p0, Lcom/twitter/android/revenue/card/aj$a;->a:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/aj$a;->b:Lcom/twitter/android/revenue/card/CardMediaView;

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 176
    return-void
.end method

.method private a(Lcom/twitter/android/revenue/card/CardMediaView;Lcas;ZF)V
    .locals 0

    .prologue
    .line 203
    if-eqz p2, :cond_0

    .line 204
    invoke-static {p1, p2, p3, p4}, Lcom/twitter/android/revenue/j;->a(Lcom/twitter/android/revenue/card/CardMediaView;Lcas;ZF)V

    .line 206
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj$a;->a:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public a(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;JLcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 0

    .prologue
    .line 222
    return-void
.end method

.method public a(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 210
    iget-object v3, p0, Lcom/twitter/android/revenue/card/aj$a;->b:Lcom/twitter/android/revenue/card/CardMediaView;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/twitter/android/revenue/card/CardMediaView;->setVisibility(I)V

    .line 211
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj$a;->c:Lcom/twitter/android/revenue/card/CardMediaView;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Lcom/twitter/android/revenue/card/CardMediaView;->setVisibility(I)V

    .line 212
    return-void

    :cond_0
    move v0, v2

    .line 210
    goto :goto_0

    :cond_1
    move v2, v1

    .line 211
    goto :goto_1
.end method

.method public at_()V
    .locals 0

    .prologue
    .line 185
    return-void
.end method

.method public au_()V
    .locals 0

    .prologue
    .line 189
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 226
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 230
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x0

    return v0
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 234
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj$a;->b:Lcom/twitter/android/revenue/card/CardMediaView;

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj$a;->b:Lcom/twitter/android/revenue/card/CardMediaView;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/CardMediaView;->getMediaImageView()Lcom/twitter/media/ui/image/MediaImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setFromMemoryOnly(Z)V

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj$a;->c:Lcom/twitter/android/revenue/card/CardMediaView;

    if-eqz v0, :cond_1

    .line 239
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj$a;->c:Lcom/twitter/android/revenue/card/CardMediaView;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/CardMediaView;->getMediaImageView()Lcom/twitter/media/ui/image/MediaImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setFromMemoryOnly(Z)V

    .line 241
    :cond_1
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj$a;->b:Lcom/twitter/android/revenue/card/CardMediaView;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj$a;->b:Lcom/twitter/android/revenue/card/CardMediaView;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/CardMediaView;->getMediaImageView()Lcom/twitter/media/ui/image/MediaImageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->j()Z

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj$a;->c:Lcom/twitter/android/revenue/card/CardMediaView;

    if-eqz v0, :cond_1

    .line 250
    iget-object v0, p0, Lcom/twitter/android/revenue/card/aj$a;->c:Lcom/twitter/android/revenue/card/CardMediaView;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/CardMediaView;->getMediaImageView()Lcom/twitter/media/ui/image/MediaImageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->j()Z

    .line 252
    :cond_1
    return-void
.end method

.method public h()V
    .locals 0

    .prologue
    .line 193
    return-void
.end method

.method public i()Landroid/view/View;
    .locals 1

    .prologue
    .line 198
    sget-object v0, Lcom/twitter/library/widget/a;->j:Lcom/twitter/library/widget/a;

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->i()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
