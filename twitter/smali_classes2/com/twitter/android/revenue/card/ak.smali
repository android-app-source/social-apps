.class public Lcom/twitter/android/revenue/card/ak;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/revenue/card/al;
.implements Lcom/twitter/android/revenue/card/k$a;


# instance fields
.field private a:Landroid/view/ViewGroup;

.field private b:Lcom/twitter/android/revenue/card/CardMediaView;

.field private c:Lcom/twitter/android/revenue/card/k;

.field private d:Landroid/content/Context;

.field private e:Lcar;

.field private f:Lcom/twitter/model/core/Tweet;

.field private g:J

.field private h:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private i:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Lcar;Lcom/twitter/android/av/video/e$b;ZF)V
    .locals 3

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/revenue/card/ak;->i:Z

    .line 56
    iput-object p1, p0, Lcom/twitter/android/revenue/card/ak;->d:Landroid/content/Context;

    .line 57
    iput-object p2, p0, Lcom/twitter/android/revenue/card/ak;->e:Lcar;

    .line 58
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/revenue/card/ak;->a:Landroid/view/ViewGroup;

    .line 60
    new-instance v0, Lcom/twitter/android/revenue/card/CardMediaView;

    invoke-direct {v0, p1}, Lcom/twitter/android/revenue/card/CardMediaView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/revenue/card/ak;->b:Lcom/twitter/android/revenue/card/CardMediaView;

    .line 61
    const-string/jumbo v0, "cover_promo_image"

    invoke-static {v0, p2}, Lcas;->a(Ljava/lang/String;Lcar;)Lcas;

    move-result-object v0

    .line 62
    iget-object v1, p0, Lcom/twitter/android/revenue/card/ak;->b:Lcom/twitter/android/revenue/card/CardMediaView;

    invoke-direct {p0, v1, v0, p4, p5}, Lcom/twitter/android/revenue/card/ak;->a(Lcom/twitter/android/revenue/card/CardMediaView;Lcas;ZF)V

    .line 64
    new-instance v0, Lcom/twitter/android/revenue/card/k;

    invoke-direct {v0, p1, p3}, Lcom/twitter/android/revenue/card/k;-><init>(Landroid/content/Context;Lcom/twitter/android/av/video/e$b;)V

    iput-object v0, p0, Lcom/twitter/android/revenue/card/ak;->c:Lcom/twitter/android/revenue/card/k;

    .line 65
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ak;->c:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0, p0}, Lcom/twitter/android/revenue/card/k;->a(Lcom/twitter/android/revenue/card/k$a;)V

    .line 67
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ak;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/ak;->b:Lcom/twitter/android/revenue/card/CardMediaView;

    invoke-static {}, Lcom/twitter/android/revenue/j;->a()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 68
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ak;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/ak;->c:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v1}, Lcom/twitter/android/revenue/card/k;->e()Landroid/view/View;

    move-result-object v1

    invoke-static {}, Lcom/twitter/android/revenue/j;->a()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 69
    return-void
.end method

.method private a(Lcom/twitter/android/revenue/card/CardMediaView;Lcas;ZF)V
    .locals 0

    .prologue
    .line 73
    if-eqz p2, :cond_0

    .line 74
    invoke-static {p1, p2, p3, p4}, Lcom/twitter/android/revenue/j;->a(Lcom/twitter/android/revenue/card/CardMediaView;Lcas;ZF)V

    .line 76
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ak;->a:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public a(Landroid/app/Activity;Lcom/twitter/model/core/Tweet;JLcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 3

    .prologue
    .line 92
    iput-object p2, p0, Lcom/twitter/android/revenue/card/ak;->f:Lcom/twitter/model/core/Tweet;

    .line 93
    iput-wide p3, p0, Lcom/twitter/android/revenue/card/ak;->g:J

    .line 94
    iput-object p5, p0, Lcom/twitter/android/revenue/card/ak;->h:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 95
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/ak;->i:Z

    if-nez v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ak;->c:Lcom/twitter/android/revenue/card/k;

    new-instance v1, Lcom/twitter/library/av/playback/TweetAVDataSource;

    invoke-direct {v1, p2}, Lcom/twitter/library/av/playback/TweetAVDataSource;-><init>(Lcom/twitter/model/core/Tweet;)V

    invoke-virtual {v0, p1, v1, p5}, Lcom/twitter/android/revenue/card/k;->a(Landroid/app/Activity;Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 97
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ak;->c:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0, p0}, Lcom/twitter/android/revenue/card/k;->a(Lcom/twitter/android/revenue/card/k$a;)V

    .line 99
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 5

    .prologue
    .line 167
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ak;->e:Lcar;

    iget-wide v2, p0, Lcom/twitter/android/revenue/card/ak;->g:J

    iget-object v1, p0, Lcom/twitter/android/revenue/card/ak;->f:Lcom/twitter/model/core/Tweet;

    iget-boolean v4, p0, Lcom/twitter/android/revenue/card/ak;->i:Z

    .line 168
    invoke-static {v0, v2, v3, v1, v4}, Lcom/twitter/android/av/revenue/VideoConversationCardData;->a(Lcar;JLcom/twitter/model/core/Tweet;Z)Lcom/twitter/android/av/revenue/VideoConversationCardData;

    move-result-object v0

    .line 169
    const-string/jumbo v1, "player_stream_url"

    iget-object v2, p0, Lcom/twitter/android/revenue/card/ak;->e:Lcar;

    invoke-static {v1, v2}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    .line 171
    new-instance v2, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity$a;

    invoke-direct {v2, v0}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity$a;-><init>(Lcom/twitter/android/av/revenue/VideoConversationCardData;)V

    iget-object v0, p0, Lcom/twitter/android/revenue/card/ak;->f:Lcom/twitter/model/core/Tweet;

    .line 172
    invoke-virtual {v2, v0}, Lcom/twitter/android/av/revenue/VideoConversationCardCanvasActivity$a;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/av/AVCardCanvasActivity$a;

    move-result-object v0

    .line 173
    invoke-virtual {v0, v1}, Lcom/twitter/android/av/AVCardCanvasActivity$a;->a(Ljava/lang/String;)Lcom/twitter/android/av/AVCardCanvasActivity$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/revenue/card/ak;->h:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 174
    invoke-virtual {v0, v1}, Lcom/twitter/android/av/AVCardCanvasActivity$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/android/av/AVCardCanvasActivity$a;

    move-result-object v0

    .line 175
    invoke-virtual {v0, p1}, Lcom/twitter/android/av/AVCardCanvasActivity$a;->a(Landroid/view/View;)Lcom/twitter/android/av/AVCardCanvasActivity$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/revenue/card/ak;->d:Landroid/content/Context;

    .line 177
    invoke-static {v1}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v1

    invoke-virtual {v1}, Lbaa;->k()Z

    move-result v1

    .line 176
    invoke-virtual {v0, v1}, Lcom/twitter/android/av/AVCardCanvasActivity$a;->a(Z)Lcom/twitter/android/av/AVCardCanvasActivity$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/revenue/card/ak;->d:Landroid/content/Context;

    .line 178
    invoke-virtual {v0, v1}, Lcom/twitter/android/av/AVCardCanvasActivity$a;->b(Landroid/content/Context;)V

    .line 179
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 80
    iput-boolean p1, p0, Lcom/twitter/android/revenue/card/ak;->i:Z

    .line 81
    iget-object v1, p0, Lcom/twitter/android/revenue/card/ak;->b:Lcom/twitter/android/revenue/card/CardMediaView;

    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/ak;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/android/revenue/card/CardMediaView;->setVisibility(I)V

    .line 82
    return-void

    .line 81
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public at_()V
    .locals 1

    .prologue
    .line 140
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/ak;->i:Z

    if-nez v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ak;->c:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/k;->at_()V

    .line 143
    :cond_0
    return-void
.end method

.method public au_()V
    .locals 1

    .prologue
    .line 147
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/ak;->i:Z

    if-nez v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ak;->c:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/k;->au_()V

    .line 150
    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/ak;->i:Z

    if-nez v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ak;->c:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/k;->a()V

    .line 115
    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/ak;->i:Z

    if-nez v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ak;->c:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0, p1}, Lcom/twitter/android/revenue/card/k;->a(Z)V

    .line 122
    :cond_0
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/ak;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ak;->c:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/k;->c()Z

    move-result v0

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 126
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/ak;->i:Z

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ak;->b:Lcom/twitter/android/revenue/card/CardMediaView;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/CardMediaView;->getMediaImageView()Lcom/twitter/media/ui/image/MediaImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setFromMemoryOnly(Z)V

    .line 131
    :goto_0
    return-void

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ak;->c:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/k;->d()V

    goto :goto_0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/ak;->i:Z

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ak;->b:Lcom/twitter/android/revenue/card/CardMediaView;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/CardMediaView;->getMediaImageView()Lcom/twitter/media/ui/image/MediaImageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->j()Z

    .line 108
    :goto_0
    return-void

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ak;->c:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/k;->b()V

    goto :goto_0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/ak;->i:Z

    if-nez v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ak;->c:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/k;->h()V

    .line 157
    :cond_0
    return-void
.end method

.method public i()Landroid/view/View;
    .locals 1

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/ak;->i:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/library/widget/a;->j:Lcom/twitter/library/widget/a;

    invoke-interface {v0}, Lcom/twitter/library/widget/a;->i()Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ak;->c:Lcom/twitter/android/revenue/card/k;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/k;->i()Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method
