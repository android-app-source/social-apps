.class public Lcom/twitter/android/revenue/card/t;
.super Lcom/twitter/android/revenue/card/b;
.source "Twttr"


# instance fields
.field private g:Z

.field private final h:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/mopub/nativeads/NativeAd;Lcom/mopub/nativeads/GooglePlayServicesNative$GooglePlayServicesNativeAd;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;[F[F)V
    .locals 8

    .prologue
    .line 36
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    move-object/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/revenue/card/b;-><init>(Landroid/content/Context;Lcom/mopub/nativeads/NativeAd;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;[F[F)V

    .line 37
    iget-object v0, p0, Lcom/twitter/android/revenue/card/t;->a:Landroid/view/ViewGroup;

    const v1, 0x7f130572

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/t;->h:Landroid/view/View;

    .line 38
    iget-object v0, p0, Lcom/twitter/android/revenue/card/t;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/twitter/android/revenue/card/t;->h:Landroid/view/View;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1, p7}, Lcom/twitter/android/revenue/j;->a(Landroid/view/View;Landroid/content/res/Resources;[F)V

    .line 41
    :cond_0
    new-instance v0, Lcom/twitter/android/revenue/card/t$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/revenue/card/t$1;-><init>(Lcom/twitter/android/revenue/card/t;)V

    .line 47
    iget-object v1, p0, Lcom/twitter/android/revenue/card/t;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/twitter/library/util/s;->a(Landroid/view/View;)V

    .line 48
    iget-object v1, p0, Lcom/twitter/android/revenue/card/t;->e:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/MediaImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 50
    new-instance v0, Lcom/twitter/android/revenue/card/t$2;

    invoke-direct {v0, p0}, Lcom/twitter/android/revenue/card/t$2;-><init>(Lcom/twitter/android/revenue/card/t;)V

    invoke-virtual {p3, v0}, Lcom/mopub/nativeads/GooglePlayServicesNative$GooglePlayServicesNativeAd;->setOnAdOpenListener(Lcom/mopub/nativeads/GooglePlayServicesNative$GooglePlayServicesNativeAd$OnAdOpenListener;)V

    .line 57
    return-void
.end method

.method private g()V
    .locals 4

    .prologue
    .line 110
    iget-object v0, p0, Lcom/twitter/android/revenue/card/t;->b:Landroid/view/View;

    const/16 v1, 0x3ea

    .line 111
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 110
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 112
    if-eqz v0, :cond_0

    .line 113
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 114
    if-lez v1, :cond_0

    .line 115
    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_0
    if-ltz v2, :cond_0

    .line 116
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    const v3, 0x7f130213

    if-eq v1, v3, :cond_1

    .line 117
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 118
    if-eqz v1, :cond_1

    .line 119
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 120
    if-eqz v1, :cond_1

    .line 121
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 122
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/revenue/card/t;->g:Z

    .line 130
    :cond_0
    return-void

    .line 115
    :cond_1
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_0
.end method


# virtual methods
.method public a(JLcar;)V
    .locals 5

    .prologue
    .line 61
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/revenue/card/b;->a(JLcar;)V

    .line 63
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/t;->g:Z

    if-nez v0, :cond_0

    .line 64
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/t;->g()V

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/t;->d:Lcom/twitter/android/revenue/card/StatsAndCtaView;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->getStarsContainerView()Landroid/view/View;

    move-result-object v0

    .line 68
    iget-object v1, p0, Lcom/twitter/android/revenue/card/t;->d:Lcom/twitter/android/revenue/card/StatsAndCtaView;

    invoke-virtual {v1}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->getStoreView()Landroid/view/View;

    move-result-object v1

    .line 69
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 70
    new-instance v2, Lcom/twitter/android/revenue/card/t$3;

    invoke-direct {v2, p0, v1}, Lcom/twitter/android/revenue/card/t$3;-><init>(Lcom/twitter/android/revenue/card/t;Landroid/view/View;)V

    .line 76
    iget-object v1, p0, Lcom/twitter/android/revenue/card/t;->h:Landroid/view/View;

    invoke-virtual {v2, v1}, Lcom/twitter/library/util/s;->a(Landroid/view/View;)V

    .line 77
    invoke-virtual {v0, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/revenue/card/t;->d:Lcom/twitter/android/revenue/card/StatsAndCtaView;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->getTitleView()Landroid/view/View;

    move-result-object v0

    .line 81
    iget-object v1, p0, Lcom/twitter/android/revenue/card/t;->d:Lcom/twitter/android/revenue/card/StatsAndCtaView;

    invoke-virtual {v1}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->getStatsContainerView()Landroid/view/View;

    move-result-object v1

    .line 82
    if-eqz v0, :cond_2

    .line 83
    new-instance v2, Lcom/twitter/android/revenue/card/t$4;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/revenue/card/t$4;-><init>(Lcom/twitter/android/revenue/card/t;Landroid/view/View;)V

    .line 89
    iget-object v3, p0, Lcom/twitter/android/revenue/card/t;->h:Landroid/view/View;

    invoke-virtual {v2, v3}, Lcom/twitter/library/util/s;->a(Landroid/view/View;)V

    .line 90
    iget-object v3, p0, Lcom/twitter/android/revenue/card/t;->d:Lcom/twitter/android/revenue/card/StatsAndCtaView;

    invoke-virtual {v3, v2}, Lcom/twitter/android/revenue/card/StatsAndCtaView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 91
    if-eqz v1, :cond_2

    .line 92
    new-instance v3, Lcom/twitter/android/revenue/card/t$5;

    invoke-direct {v3, p0, v0}, Lcom/twitter/android/revenue/card/t$5;-><init>(Lcom/twitter/android/revenue/card/t;Landroid/view/View;)V

    .line 98
    iget-object v0, p0, Lcom/twitter/android/revenue/card/t;->h:Landroid/view/View;

    invoke-virtual {v3, v0}, Lcom/twitter/library/util/s;->a(Landroid/view/View;)V

    .line 99
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 102
    :cond_2
    return-void
.end method
