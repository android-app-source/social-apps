.class public Lcom/twitter/android/revenue/card/ar;
.super Lcom/twitter/android/revenue/card/a;
.source "Twttr"


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected b:I

.field protected c:Ljava/lang/String;

.field protected d:Ljava/lang/String;

.field protected e:Lcom/twitter/media/ui/image/MediaImageView;

.field protected f:Landroid/graphics/drawable/Drawable;

.field protected g:Landroid/widget/TextView;

.field protected h:Lcom/twitter/ui/widget/TwitterButton;

.field protected r:Landroid/view/View;

.field protected final s:Lcom/twitter/library/util/s;

.field protected t:Z

.field protected u:Landroid/widget/TextView;

.field protected v:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-string/jumbo v0, "promo_image"

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/revenue/card/ar;->a:Ljava/util/List;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V
    .locals 6

    .prologue
    .line 72
    const v5, 0x7f04022c

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/revenue/card/ar;-><init>(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;I)V

    .line 74
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;I)V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/revenue/card/a;-><init>(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;Lcom/twitter/android/card/b;)V

    .line 79
    new-instance v0, Lcom/twitter/android/revenue/card/ar$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/revenue/card/ar$1;-><init>(Lcom/twitter/android/revenue/card/ar;)V

    iput-object v0, p0, Lcom/twitter/android/revenue/card/ar;->s:Lcom/twitter/library/util/s;

    .line 85
    iput p5, p0, Lcom/twitter/android/revenue/card/ar;->b:I

    .line 86
    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/card/ar;->a(Landroid/content/Context;)V

    .line 87
    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 261
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 262
    iget-object v1, p0, Lcom/twitter/android/revenue/card/ar;->d:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/android/livevideo/c;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 263
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->d:Ljava/lang/String;

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/revenue/card/ar;->a(Landroid/view/View;Landroid/view/MotionEvent;Ljava/lang/String;)V

    .line 270
    :goto_0
    return-void

    .line 264
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/revenue/card/ar;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/ar;->d:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/twitter/android/revenue/k;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->d:Ljava/lang/String;

    .line 265
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/twitter/android/revenue/card/ar;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 266
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->d:Ljava/lang/String;

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/revenue/card/ar;->a(Landroid/view/View;Landroid/view/MotionEvent;Ljava/lang/String;)V

    goto :goto_0

    .line 268
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->c:Ljava/lang/String;

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/revenue/card/ar;->a(Landroid/view/View;Landroid/view/MotionEvent;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/revenue/card/ar;Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/revenue/card/ar;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    return-void
.end method

.method private static k()Z
    .locals 1

    .prologue
    .line 303
    invoke-static {}, Lcom/twitter/library/client/k;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "cct_unwrap_card_url"

    .line 304
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 303
    :goto_0
    return v0

    .line 304
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(Lcas;)F
    .locals 3

    .prologue
    const/high16 v1, 0x40000000    # 2.0f

    const/high16 v0, 0x40200000    # 2.5f

    .line 148
    iget-boolean v2, p0, Lcom/twitter/android/revenue/card/ar;->t:Z

    if-eqz v2, :cond_1

    .line 149
    invoke-virtual {p1, v1}, Lcas;->a(F)F

    move-result v0

    .line 151
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1, v0}, Lcas;->a(F)F

    move-result v2

    cmpl-float v2, v2, v0

    if-gez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 119
    invoke-super {p0}, Lcom/twitter/android/revenue/card/a;->a()V

    .line 121
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->e:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->e:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->j()Z

    .line 123
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->e:Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 125
    :cond_0
    return-void
.end method

.method public a(JLcar;)V
    .locals 3

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/ar;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/ar;->c:Ljava/lang/String;

    .line 158
    const-string/jumbo v0, "website_dest_url"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/ar;->d:Ljava/lang/String;

    .line 161
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->o:Lcom/twitter/library/card/CardContext;

    invoke-static {v0}, Lcom/twitter/android/revenue/card/ag;->a(Lcom/twitter/library/card/CardContext;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/ar;->d:Ljava/lang/String;

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 166
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->o:Lcom/twitter/library/card/CardContext;

    invoke-static {v0}, Lcom/twitter/android/livevideo/c;->a(Lcom/twitter/library/card/CardContext;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/ar;->d:Ljava/lang/String;

    .line 172
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/twitter/android/revenue/card/ar;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 173
    const-string/jumbo v0, "app_url_resolved"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    .line 174
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/revenue/card/ar;->i:Landroid/content/Context;

    .line 175
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/twitter/android/client/OpenUriHelper;->b(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 176
    iput-object v0, p0, Lcom/twitter/android/revenue/card/ar;->d:Ljava/lang/String;

    .line 180
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->d_:Lcom/twitter/android/card/d;

    const-string/jumbo v1, "_card_data"

    invoke-static {v1, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/card/d;->a(Ljava/lang/String;)V

    .line 182
    invoke-virtual {p0, p3}, Lcom/twitter/android/revenue/card/ar;->b(Lcar;)V

    .line 184
    invoke-virtual {p0, p3}, Lcom/twitter/android/revenue/card/ar;->c(Lcar;)V

    .line 186
    invoke-virtual {p0, p3}, Lcom/twitter/android/revenue/card/ar;->d(Lcar;)V

    .line 188
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/ar;->i()V

    .line 190
    invoke-virtual {p0, p3}, Lcom/twitter/android/revenue/card/ar;->a(Lcar;)V

    .line 191
    return-void
.end method

.method protected a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->p:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    invoke-static {p1, v0}, Lcom/twitter/android/revenue/j;->a(Landroid/content/Context;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/revenue/card/ar;->t:Z

    .line 91
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/ar;->t:Z

    if-eqz v0, :cond_0

    .line 92
    const v0, 0x7f040257

    iput v0, p0, Lcom/twitter/android/revenue/card/ar;->b:I

    .line 95
    :cond_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040239

    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/ar;->q:Landroid/view/View;

    .line 97
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->q:Landroid/view/View;

    const v1, 0x7f130569

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 98
    iget v1, p0, Lcom/twitter/android/revenue/card/ar;->b:I

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 99
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 101
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->q:Landroid/view/View;

    const v1, 0x7f13056a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/ar;->v:Landroid/view/View;

    .line 102
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->s:Lcom/twitter/library/util/s;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/ar;->v:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/twitter/library/util/s;->a(Landroid/view/View;)V

    .line 104
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->q:Landroid/view/View;

    const v1, 0x7f130214

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/ar;->e:Lcom/twitter/media/ui/image/MediaImageView;

    .line 105
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->e:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->e:Lcom/twitter/media/ui/image/MediaImageView;

    const/high16 v1, 0x40200000    # 2.5f

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 107
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->e:Lcom/twitter/media/ui/image/MediaImageView;

    const-string/jumbo v1, "card"

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setImageType(Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->e:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->getDefaultDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/ar;->f:Landroid/graphics/drawable/Drawable;

    .line 111
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->q:Landroid/view/View;

    const v1, 0x7f130560

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/ar;->u:Landroid/widget/TextView;

    .line 112
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->q:Landroid/view/View;

    const v1, 0x7f130217

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/ar;->h:Lcom/twitter/ui/widget/TwitterButton;

    .line 113
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->q:Landroid/view/View;

    const v1, 0x7f130567

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/revenue/card/ar;->g:Landroid/widget/TextView;

    .line 114
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->q:Landroid/view/View;

    const v1, 0x7f130216

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/ar;->r:Landroid/view/View;

    .line 115
    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 279
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->p:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    invoke-static {v0}, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 281
    const v1, 0x7f0e0115

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v1, v0

    .line 282
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 283
    instance-of v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    .line 285
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 286
    invoke-virtual {v0, v2, v2, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 287
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 288
    invoke-virtual {p1}, Landroid/view/View;->requestLayout()V

    .line 291
    :cond_0
    return-void
.end method

.method protected a(Landroid/view/View;Landroid/view/MotionEvent;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 294
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/ar;->e()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, p2, v1}, Lcom/twitter/library/scribe/b;->a(Landroid/view/View;Landroid/view/View;Landroid/view/MotionEvent;I)Lcom/twitter/analytics/feature/model/NativeCardUserAction;

    move-result-object v0

    .line 296
    iget-object v1, p0, Lcom/twitter/android/revenue/card/ar;->n:Lcom/twitter/android/card/CardActionHelper;

    invoke-virtual {v1, p3, v0}, Lcom/twitter/android/card/CardActionHelper;->a(Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 297
    return-void
.end method

.method a(Lcar;)V
    .locals 4

    .prologue
    .line 194
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->u:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 195
    const-string/jumbo v0, "vanity_url"

    invoke-static {v0, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    .line 196
    iget-object v1, p0, Lcom/twitter/android/revenue/card/ar;->u:Landroid/widget/TextView;

    const/4 v2, 0x0

    sget v3, Lcni;->a:F

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 197
    if-nez v0, :cond_1

    .line 198
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->u:Landroid/widget/TextView;

    const v1, 0x7f0a0a37

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 202
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->u:Landroid/widget/TextView;

    const-string/jumbo v1, "vanity_url"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 203
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->u:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/ar;->s:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 204
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->u:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/twitter/android/revenue/card/ar;->a(Landroid/view/View;)V

    .line 206
    :cond_0
    return-void

    .line 200
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/revenue/card/ar;->u:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->e:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->e:Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setFromMemoryOnly(Z)V

    .line 132
    :cond_0
    return-void
.end method

.method b(Lcar;)V
    .locals 3

    .prologue
    .line 209
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->e:Lcom/twitter/media/ui/image/MediaImageView;

    if-eqz v0, :cond_0

    .line 210
    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/ar;->f()Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p1}, Lcas;->a(Ljava/util/List;Lcar;)Lcas;

    move-result-object v0

    .line 211
    iget-object v1, p0, Lcom/twitter/android/revenue/card/ar;->e:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v1}, Lcom/twitter/media/ui/image/MediaImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 213
    if-eqz v0, :cond_1

    .line 214
    iget-object v1, p0, Lcom/twitter/android/revenue/card/ar;->e:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {p0, v0}, Lcom/twitter/android/revenue/card/ar;->a(Lcas;)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 215
    iget-object v1, p0, Lcom/twitter/android/revenue/card/ar;->e:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v0, v0, Lcas;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 216
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->e:Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setFromMemoryOnly(Z)V

    .line 217
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->e:Lcom/twitter/media/ui/image/MediaImageView;

    const-string/jumbo v1, "promo_image"

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setTag(Ljava/lang/Object;)V

    .line 218
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->e:Lcom/twitter/media/ui/image/MediaImageView;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/ar;->s:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 219
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->f:Landroid/graphics/drawable/Drawable;

    .line 226
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/revenue/card/ar;->e:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/MediaImageView;->setDefaultDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 228
    :cond_0
    return-void

    .line 221
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->e:Lcom/twitter/media/ui/image/MediaImageView;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/image/MediaImageView;->setAspectRatio(F)V

    .line 222
    const v0, 0x7f02082f

    invoke-static {v1, v0}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 223
    const v2, 0x7f110003

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_0
.end method

.method c(Lcar;)V
    .locals 4

    .prologue
    .line 231
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 232
    const-string/jumbo v0, "title"

    invoke-static {v0, p1}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v1

    .line 233
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 234
    iget-object v2, p0, Lcom/twitter/android/revenue/card/ar;->g:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->p:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v3, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->g:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne v0, v3, :cond_1

    const/4 v0, 0x2

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setMinLines(I)V

    .line 235
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 236
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->g:Landroid/widget/TextView;

    const-string/jumbo v1, "title"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 237
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/ar;->s:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 238
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    sget v2, Lcni;->a:F

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 239
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->g:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/twitter/android/revenue/card/ar;->a(Landroid/view/View;)V

    .line 244
    :cond_0
    :goto_1
    return-void

    .line 234
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    .line 241
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->g:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method d(Lcar;)V
    .locals 3

    .prologue
    .line 247
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->h:Lcom/twitter/ui/widget/TwitterButton;

    if-eqz v0, :cond_0

    .line 248
    iget-object v1, p0, Lcom/twitter/android/revenue/card/ar;->h:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {p0}, Lcom/twitter/android/revenue/card/ar;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 249
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->h:Lcom/twitter/ui/widget/TwitterButton;

    invoke-static {p1}, Lcom/twitter/android/revenue/i;->a(Lcar;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setText(I)V

    .line 250
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->h:Lcom/twitter/ui/widget/TwitterButton;

    const-string/jumbo v1, "button"

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setTag(Ljava/lang/Object;)V

    .line 251
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->h:Lcom/twitter/ui/widget/TwitterButton;

    new-instance v1, Lcom/twitter/android/revenue/card/ar$2;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/ar;->h:Lcom/twitter/ui/widget/TwitterButton;

    invoke-direct {v1, p0, v2}, Lcom/twitter/android/revenue/card/ar$2;-><init>(Lcom/twitter/android/revenue/card/ar;Lcom/twitter/ui/widget/TwitterButton;)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 258
    :cond_0
    return-void

    .line 248
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 140
    sget-object v0, Lcom/twitter/android/revenue/card/ar;->a:Ljava/util/List;

    return-object v0
.end method

.method protected g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    const-string/jumbo v0, "website_url"

    return-object v0
.end method

.method protected h()Z
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    return v0
.end method

.method i()V
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->r:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/twitter/android/revenue/card/ar;->r:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/revenue/card/ar;->s:Lcom/twitter/library/util/s;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 276
    :cond_0
    return-void
.end method
