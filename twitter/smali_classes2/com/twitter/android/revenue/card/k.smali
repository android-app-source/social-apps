.class public Lcom/twitter/android/revenue/card/k;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/revenue/card/k$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

.field private final b:Lcom/twitter/android/av/video/e$b;

.field private c:Lcom/twitter/android/av/video/e;

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Lcom/twitter/android/revenue/card/k$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/av/video/e$b;)V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/revenue/card/k;->e:Z

    .line 49
    iput-object p2, p0, Lcom/twitter/android/revenue/card/k;->b:Lcom/twitter/android/av/video/e$b;

    .line 51
    invoke-virtual {p0, p1}, Lcom/twitter/android/revenue/card/k;->a(Landroid/content/Context;)Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/k;->a:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    .line 53
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->a:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    new-instance v1, Lcom/twitter/android/revenue/card/k$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/revenue/card/k$1;-><init>(Lcom/twitter/android/revenue/card/k;)V

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 61
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/revenue/card/k;)Lcom/twitter/android/revenue/card/k$a;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->g:Lcom/twitter/android/revenue/card/k$a;

    return-object v0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->c:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->c:Lcom/twitter/android/av/video/e;

    sget-object v1, Lbyo;->c:Lbyf;

    sget-object v2, Lcom/twitter/library/av/VideoPlayerView$Mode;->a:Lcom/twitter/library/av/VideoPlayerView$Mode;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/av/video/e;->a(Lbyf;Lcom/twitter/library/av/VideoPlayerView$Mode;)V

    .line 97
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/revenue/card/k;->f:Z

    .line 98
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/k;->d:Z

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->c:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->e()V

    .line 100
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/revenue/card/k;->d:Z

    .line 103
    :cond_0
    return-void
.end method


# virtual methods
.method a(Landroid/content/Context;)Lcom/twitter/media/ui/image/AspectRatioFrameLayout;
    .locals 1

    .prologue
    .line 187
    new-instance v0, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    invoke-direct {v0, p1}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->c:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->c:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->b()V

    .line 109
    :cond_0
    return-void
.end method

.method public a(Landroid/app/Activity;Lcom/twitter/library/av/playback/AVDataSource;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 6

    .prologue
    .line 69
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->c:Lcom/twitter/android/av/video/e;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 70
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->b:Lcom/twitter/android/av/video/e$b;

    iget-object v2, p0, Lcom/twitter/android/revenue/card/k;->a:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    const/4 v5, 0x0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/av/video/e$b;->a(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/av/playback/AVDataSource;Landroid/view/View$OnClickListener;)Lcom/twitter/android/av/video/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/k;->c:Lcom/twitter/android/av/video/e;

    .line 71
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->a:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    invoke-interface {p2}, Lcom/twitter/library/av/playback/AVDataSource;->o()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;->setAspectRatio(F)V

    .line 73
    invoke-virtual {p0, p2}, Lcom/twitter/android/revenue/card/k;->a(Lcom/twitter/library/av/playback/AVDataSource;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/revenue/card/k;->f:Z

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/k;->j()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/revenue/card/k$a;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/twitter/android/revenue/card/k;->g:Lcom/twitter/android/revenue/card/k$a;

    .line 65
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->c:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->c:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->d()V

    .line 115
    :cond_0
    return-void
.end method

.method a(Lcom/twitter/library/av/playback/AVDataSource;)Z
    .locals 2

    .prologue
    .line 191
    invoke-static {}, Lcom/twitter/library/av/playback/q;->a()Lcom/twitter/library/av/playback/q;

    move-result-object v0

    invoke-interface {p1}, Lcom/twitter/library/av/playback/AVDataSource;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/q;->d(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public at_()V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->c:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->c:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->at_()V

    .line 149
    :cond_0
    return-void
.end method

.method public au_()V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->c:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->c:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->au_()V

    .line 156
    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->c:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->c:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->a()V

    .line 120
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/revenue/card/k;->c:Lcom/twitter/android/av/video/e;

    .line 122
    :cond_0
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/k;->e:Z

    return v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->c:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->c:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->e()V

    .line 133
    :goto_0
    return-void

    .line 131
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/revenue/card/k;->d:Z

    goto :goto_0
.end method

.method public e()Landroid/view/View;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->a:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    return-object v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->c:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->c:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->h()V

    .line 163
    :cond_0
    return-void
.end method

.method public i()Landroid/view/View;
    .locals 1

    .prologue
    .line 168
    iget-boolean v0, p0, Lcom/twitter/android/revenue/card/k;->f:Z

    if-nez v0, :cond_1

    .line 169
    invoke-direct {p0}, Lcom/twitter/android/revenue/card/k;->j()V

    .line 170
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->c:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->l()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v0

    .line 171
    if-eqz v0, :cond_0

    .line 172
    invoke-virtual {v0}, Lcom/twitter/library/av/playback/AVPlayerAttachment;->i()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->a:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/AspectRatioFrameLayout;->requestLayout()V

    .line 176
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/revenue/card/k;->a:Lcom/twitter/media/ui/image/AspectRatioFrameLayout;

    return-object v0
.end method
