.class Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/app/AlertDialog$Builder;

.field final synthetic b:Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference;


# direct methods
.method constructor <init>(Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference;Landroid/app/AlertDialog$Builder;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$1;->b:Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference;

    iput-object p2, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$1;->a:Landroid/app/AlertDialog$Builder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 95
    if-ne p2, v6, :cond_0

    .line 97
    new-instance v0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$1$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$1$1;-><init>(Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$1;)V

    .line 106
    new-instance v1, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$a;

    iget-object v2, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$1;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "MAP Carousel Injected!"

    invoke-direct {v1, v2, v0, v3}, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$a;-><init>(Landroid/content/Context;Lcpv;Ljava/lang/String;)V

    new-array v2, v5, [Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$1;->b:Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference;

    .line 108
    invoke-static {v0}, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference;->a(Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v4

    invoke-virtual {v1, v2}, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 115
    :goto_0
    return-void

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$1;->a:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 111
    const/16 v0, 0xa

    if-ne p2, v0, :cond_1

    new-instance v0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$c;

    invoke-direct {v0, v2, v1}, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$c;-><init>(Landroid/content/Context;Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$1;)V

    .line 113
    :goto_1
    new-instance v3, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$b;

    invoke-direct {v3, v2, v0, v1}, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$b;-><init>(Landroid/content/Context;Ljava/lang/Runnable;Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$1;)V

    new-array v1, v5, [Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$1;->b:Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference;

    invoke-static {v0}, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference;->a(Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v1, v4

    invoke-virtual {v3, v1}, Lcom/twitter/android/revenue/RevenueInjectTweetDebugDialogPreference$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 111
    goto :goto_1
.end method
