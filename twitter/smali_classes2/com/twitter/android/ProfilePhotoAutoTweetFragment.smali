.class public Lcom/twitter/android/ProfilePhotoAutoTweetFragment;
.super Lcom/twitter/app/common/abs/AbsFragment;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/ProfilePhotoAutoTweetFragment$a;
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/android/ProfilePhotoAutoTweetFragment$a;

.field private b:Lcom/twitter/model/media/EditableMedia;

.field private c:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/twitter/app/common/abs/AbsFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/twitter/media/model/MediaFile;)Lcom/twitter/android/ProfilePhotoAutoTweetFragment;
    .locals 3

    .prologue
    .line 40
    new-instance v0, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;

    invoke-direct {v0}, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;-><init>()V

    .line 42
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 43
    const-string/jumbo v2, "profile_photo"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 44
    invoke-virtual {v0, v1}, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->setArguments(Landroid/os/Bundle;)V

    .line 46
    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 52
    const v0, 0x7f0402c3

    invoke-virtual {p1, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 54
    const v0, 0x7f13035b

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    const v0, 0x7f130681

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    const v0, 0x7f1302a3

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    const v0, 0x7f130680

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    invoke-virtual {p0}, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->c:J

    .line 61
    if-nez p2, :cond_1

    .line 62
    invoke-virtual {p0}, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->I()Lcom/twitter/app/common/base/b;

    move-result-object v0

    const-string/jumbo v1, "profile_photo"

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/b;->h(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/model/MediaFile;

    .line 63
    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {v0}, Lcom/twitter/media/model/MediaFile;->a()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/twitter/model/media/MediaSource;->b:Lcom/twitter/model/media/MediaSource;

    .line 64
    invoke-static {v0, v1, v2}, Lcom/twitter/model/media/EditableMedia;->a(Lcom/twitter/media/model/MediaFile;Landroid/net/Uri;Lcom/twitter/model/media/MediaSource;)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->b:Lcom/twitter/model/media/EditableMedia;

    .line 67
    :cond_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, p0, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->c:J

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v4, "profile_tweet_preview"

    aput-object v4, v1, v2

    aput-object v3, v1, v7

    const/4 v2, 0x2

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v4, "impression"

    aput-object v4, v1, v2

    .line 68
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 67
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 73
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v5

    .line 74
    const v0, 0x7f13031a

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    .line 75
    invoke-virtual {v0, v5}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/model/core/TwitterUser;)Z

    .line 77
    const v0, 0x7f130041

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TweetHeaderView;

    .line 78
    sget v1, Lcni;->a:F

    .line 79
    invoke-static {v1}, Lcni;->a(F)F

    move-result v2

    .line 80
    invoke-virtual {v0, v1, v2, v2}, Lcom/twitter/ui/widget/TweetHeaderView;->a(FFF)V

    .line 81
    invoke-virtual {v0, v7}, Lcom/twitter/ui/widget/TweetHeaderView;->b(Z)V

    .line 82
    iget-object v1, v5, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    iget-object v2, v5, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    iget-boolean v4, v5, Lcom/twitter/model/core/TwitterUser;->m:Z

    iget-boolean v5, v5, Lcom/twitter/model/core/TwitterUser;->l:Z

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/ui/widget/TweetHeaderView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 84
    iget-object v0, p0, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->b:Lcom/twitter/model/media/EditableMedia;

    if-eqz v0, :cond_2

    .line 85
    const v0, 0x7f13067f

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 86
    iget-object v1, p0, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->b:Lcom/twitter/model/media/EditableMedia;

    invoke-virtual {v1}, Lcom/twitter/model/media/EditableMedia;->d()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 91
    :goto_1
    return-object v6

    .line 70
    :cond_1
    const-string/jumbo v0, "profile_photo"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableMedia;

    iput-object v0, p0, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->b:Lcom/twitter/model/media/EditableMedia;

    goto :goto_0

    .line 88
    :cond_2
    const v0, 0x7f13067e

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public a(Lcom/twitter/android/ProfilePhotoAutoTweetFragment$a;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->a:Lcom/twitter/android/ProfilePhotoAutoTweetFragment$a;

    .line 96
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 103
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 106
    :sswitch_0
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->c:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "profile_tweet_preview"

    aput-object v3, v2, v4

    aput-object v0, v2, v5

    aput-object v0, v2, v6

    aput-object v0, v2, v7

    const/4 v0, 0x4

    const-string/jumbo v3, "cancel"

    aput-object v3, v2, v0

    .line 107
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 106
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 108
    iget-object v0, p0, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->a:Lcom/twitter/android/ProfilePhotoAutoTweetFragment$a;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->a:Lcom/twitter/android/ProfilePhotoAutoTweetFragment$a;

    invoke-interface {v0}, Lcom/twitter/android/ProfilePhotoAutoTweetFragment$a;->a()V

    goto :goto_0

    .line 114
    :sswitch_1
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->c:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "profile_tweet_preview"

    aput-object v3, v2, v4

    aput-object v0, v2, v5

    aput-object v0, v2, v6

    aput-object v0, v2, v7

    const/4 v3, 0x4

    const-string/jumbo v4, "send_tweet"

    aput-object v4, v2, v3

    .line 115
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    .line 114
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 116
    invoke-virtual {p0}, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0a05c7

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 117
    iget-object v2, p0, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->b:Lcom/twitter/model/media/EditableMedia;

    if-eqz v2, :cond_1

    new-instance v0, Lcom/twitter/model/drafts/DraftAttachment;

    iget-object v2, p0, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->b:Lcom/twitter/model/media/EditableMedia;

    invoke-direct {v0, v2}, Lcom/twitter/model/drafts/DraftAttachment;-><init>(Lcom/twitter/model/media/EditableMedia;)V

    .line 118
    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 120
    :cond_1
    new-instance v2, Lcom/twitter/model/drafts/a$a;

    invoke-direct {v2}, Lcom/twitter/model/drafts/a$a;-><init>()V

    .line 121
    invoke-virtual {v2, v1}, Lcom/twitter/model/drafts/a$a;->a(Ljava/lang/String;)Lcom/twitter/model/drafts/a$a;

    move-result-object v1

    .line 122
    invoke-virtual {v1, v0}, Lcom/twitter/model/drafts/a$a;->a(Ljava/util/List;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 123
    invoke-virtual {v0}, Lcom/twitter/model/drafts/a$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/a;

    .line 124
    invoke-virtual {p0}, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/twitter/android/client/x;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/drafts/a;)Ljava/lang/String;

    .line 125
    iget-object v0, p0, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->a:Lcom/twitter/android/ProfilePhotoAutoTweetFragment$a;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->a:Lcom/twitter/android/ProfilePhotoAutoTweetFragment$a;

    invoke-interface {v0}, Lcom/twitter/android/ProfilePhotoAutoTweetFragment$a;->a()V

    goto/16 :goto_0

    .line 131
    :sswitch_2
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->c:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "profile_tweet_preview"

    aput-object v3, v2, v4

    aput-object v0, v2, v5

    aput-object v0, v2, v6

    const-string/jumbo v3, "edit_button"

    aput-object v3, v2, v7

    const/4 v3, 0x4

    const-string/jumbo v4, "click"

    aput-object v4, v2, v3

    .line 132
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    .line 131
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 133
    invoke-virtual {p0}, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 134
    const v2, 0x7f0a05c7

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 135
    iget-object v3, p0, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->b:Lcom/twitter/model/media/EditableMedia;

    if-eqz v3, :cond_2

    new-instance v0, Lcom/twitter/model/drafts/DraftAttachment;

    iget-object v3, p0, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->b:Lcom/twitter/model/media/EditableMedia;

    invoke-direct {v0, v3}, Lcom/twitter/model/drafts/DraftAttachment;-><init>(Lcom/twitter/model/media/EditableMedia;)V

    .line 136
    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 138
    :cond_2
    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v3

    .line 139
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v2, v4}, Lcom/twitter/android/composer/a;->a(Ljava/lang/String;I)Lcom/twitter/android/composer/a;

    move-result-object v2

    .line 140
    invoke-virtual {v2, v0}, Lcom/twitter/android/composer/a;->a(Ljava/util/List;)Lcom/twitter/android/composer/a;

    move-result-object v0

    const-string/jumbo v2, "profile_tweet_preview"

    .line 141
    invoke-virtual {v0, v2}, Lcom/twitter/android/composer/a;->a(Ljava/lang/String;)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 142
    invoke-virtual {p0}, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/composer/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 138
    invoke-virtual {v1, v0, v5}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 103
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f1302a3 -> :sswitch_1
        0x7f13035b -> :sswitch_0
        0x7f130680 -> :sswitch_2
        0x7f130681 -> :sswitch_0
    .end sparse-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 153
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 154
    const-string/jumbo v0, "profile_photo"

    iget-object v1, p0, Lcom/twitter/android/ProfilePhotoAutoTweetFragment;->b:Lcom/twitter/model/media/EditableMedia;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 155
    return-void
.end method
