.class public Lcom/twitter/android/EmailEntryFragment;
.super Lcom/twitter/app/common/abs/AbsFragment;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/twitter/android/s$a;
.implements Lcom/twitter/android/util/SpannableTextUtil$a;
.implements Lcom/twitter/app/common/dialog/b$d;
.implements Lcom/twitter/app/onboarding/flowstep/common/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/EmailEntryFragment$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/abs/AbsFragment;",
        "Landroid/text/TextWatcher;",
        "Landroid/widget/TextView$OnEditorActionListener;",
        "Lcom/twitter/android/s$a;",
        "Lcom/twitter/android/util/SpannableTextUtil$a;",
        "Lcom/twitter/app/common/dialog/b$d;",
        "Lcom/twitter/app/onboarding/flowstep/common/h",
        "<",
        "Lcom/twitter/model/onboarding/a;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:[I

.field private static final d:[I


# instance fields
.field a:Lcom/twitter/android/EmailEntryFragment$a;

.field b:Lcom/twitter/ui/widget/TwitterEditText;

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Lcom/twitter/ui/widget/TypefacesTextView;

.field private i:Lcom/twitter/ui/widget/TypefacesTextView;

.field private j:Landroid/view/View;

.field private k:Landroid/view/View;

.field private l:Lcom/twitter/android/au;

.field private m:Lcom/twitter/android/ValidationState$a;

.field private n:Lcom/twitter/android/by;

.field private o:Lcom/twitter/android/s;

.field private p:Lcom/twitter/android/w;

.field private q:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

.field private r:Lcom/twitter/android/c;

.field private s:J

.field private t:Lcom/twitter/library/client/Session;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 73
    const/4 v0, 0x0

    sput-object v0, Lcom/twitter/android/EmailEntryFragment;->c:[I

    .line 74
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f010471

    aput v2, v0, v1

    sput-object v0, Lcom/twitter/android/EmailEntryFragment;->d:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/twitter/app/common/abs/AbsFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/EmailEntryFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->T:Landroid/content/Context;

    return-object v0
.end method

.method private a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 385
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    check-cast v0, Lcom/twitter/internal/android/widget/PopupEditText;

    .line 387
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/twitter/android/EmailEntryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f0403ef

    invoke-direct {v1, v2, v3, p1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 390
    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 392
    new-instance v1, Lcom/twitter/android/util/a;

    invoke-direct {v1, v0}, Lcom/twitter/android/util/a;-><init>(Lcom/twitter/internal/android/widget/PopupEditText;)V

    .line 394
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/EmailEntryFragment;Lcom/twitter/library/service/s;II)Z
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/EmailEntryFragment;->c(Lcom/twitter/library/service/s;II)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/EmailEntryFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->t:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method private c(I)Lcom/twitter/android/ValidationState$State;
    .locals 2

    .prologue
    .line 296
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/EmailEntryFragment;->a(Z)V

    .line 297
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->e()V

    .line 298
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->length()I

    move-result v0

    .line 299
    if-lt v0, p1, :cond_0

    .line 300
    sget-object v0, Lcom/twitter/android/ValidationState$State;->b:Lcom/twitter/android/ValidationState$State;

    .line 304
    :goto_0
    return-object v0

    .line 301
    :cond_0
    const/4 v1, 0x1

    if-le p1, v1, :cond_1

    if-ge v0, p1, :cond_1

    .line 302
    sget-object v0, Lcom/twitter/android/ValidationState$State;->a:Lcom/twitter/android/ValidationState$State;

    goto :goto_0

    .line 304
    :cond_1
    sget-object v0, Lcom/twitter/android/ValidationState$State;->d:Lcom/twitter/android/ValidationState$State;

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/EmailEntryFragment;)Lcom/twitter/android/ValidationState$a;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->m:Lcom/twitter/android/ValidationState$a;

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 380
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p1}, Lcom/twitter/ui/widget/TwitterEditText;->setText(Ljava/lang/CharSequence;)V

    .line 382
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;II)V
    .locals 5

    .prologue
    .line 448
    if-nez p2, :cond_0

    .line 450
    const/4 v0, -0x1

    if-ne p3, v0, :cond_1

    .line 451
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->l:Lcom/twitter/android/au;

    invoke-interface {v0}, Lcom/twitter/android/au;->bl_()V

    .line 452
    const-string/jumbo v0, "ok"

    .line 456
    :goto_0
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/EmailEntryFragment;->s:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "phone100_email_optional"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "skip_confirm_dialog"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-boolean v4, p0, Lcom/twitter/android/EmailEntryFragment;->f:Z

    .line 458
    invoke-static {v4}, Lcom/twitter/android/bw;->a(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const/4 v4, 0x0

    aput-object v4, v2, v3

    const/4 v3, 0x4

    aput-object v0, v2, v3

    .line 457
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 456
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 460
    :cond_0
    return-void

    .line 454
    :cond_1
    const-string/jumbo v0, "cancel"

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/w;)V
    .locals 2

    .prologue
    .line 463
    iget-object v0, p1, Lcom/twitter/android/w;->b:Ljava/lang/String;

    .line 464
    invoke-virtual {p0}, Lcom/twitter/android/EmailEntryFragment;->Y()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 465
    iget-object v1, p0, Lcom/twitter/android/EmailEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TwitterEditText;->setError(Ljava/lang/CharSequence;)V

    .line 466
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->p:Lcom/twitter/android/w;

    .line 470
    :goto_0
    return-void

    .line 468
    :cond_0
    iput-object p1, p0, Lcom/twitter/android/EmailEntryFragment;->p:Lcom/twitter/android/w;

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 9
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 194
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/abs/AbsFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 195
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 197
    packed-switch p2, :pswitch_data_0

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 199
    :pswitch_0
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 200
    invoke-virtual {p0, v7}, Lcom/twitter/android/EmailEntryFragment;->a(Z)V

    .line 201
    sget-object v0, Lcom/twitter/android/ValidationState$State;->c:Lcom/twitter/android/ValidationState$State;

    .line 202
    iget-object v1, p0, Lcom/twitter/android/EmailEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/TwitterEditText;->e()V

    .line 214
    :goto_1
    iget-object v1, p0, Lcom/twitter/android/EmailEntryFragment;->m:Lcom/twitter/android/ValidationState$a;

    if-eqz v1, :cond_3

    .line 215
    iget-object v1, p0, Lcom/twitter/android/EmailEntryFragment;->m:Lcom/twitter/android/ValidationState$a;

    new-instance v2, Lcom/twitter/android/ValidationState;

    sget-object v3, Lcom/twitter/android/ValidationState$Level;->b:Lcom/twitter/android/ValidationState$Level;

    invoke-direct {v2, v0, v3}, Lcom/twitter/android/ValidationState;-><init>(Lcom/twitter/android/ValidationState$State;Lcom/twitter/android/ValidationState$Level;)V

    invoke-interface {v1, v2}, Lcom/twitter/android/ValidationState$a;->a(Lcom/twitter/android/ValidationState;)V

    goto :goto_0

    .line 204
    :cond_1
    invoke-virtual {p0, v6}, Lcom/twitter/android/EmailEntryFragment;->a(Z)V

    .line 205
    sget-object v2, Lcom/twitter/android/ValidationState$State;->d:Lcom/twitter/android/ValidationState$State;

    .line 207
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 208
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->e()Ljava/lang/String;

    move-result-object v0

    .line 212
    :goto_2
    iget-object v1, p0, Lcom/twitter/android/EmailEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TwitterEditText;->setError(Ljava/lang/CharSequence;)V

    move-object v0, v2

    goto :goto_1

    .line 209
    :cond_2
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v0

    invoke-virtual {v0}, Lcrr;->g()Z

    move-result v0

    if-nez v0, :cond_7

    .line 210
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a08c0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 218
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->l:Lcom/twitter/android/au;

    invoke-virtual {p0}, Lcom/twitter/android/EmailEntryFragment;->d()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/twitter/android/au;->a(Z)V

    goto :goto_0

    .line 223
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/android/EmailEntryFragment;->g()V

    .line 224
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    .line 225
    if-eqz v0, :cond_0

    .line 226
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    iget-object v0, v0, Lcom/twitter/library/service/v;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/twitter/library/client/v;->c(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 227
    if-eqz v2, :cond_0

    .line 228
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    .line 229
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 232
    if-eqz v0, :cond_5

    .line 233
    const-string/jumbo v0, "success"

    .line 234
    iget-object v2, p0, Lcom/twitter/android/EmailEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v2}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 235
    iget-boolean v4, p0, Lcom/twitter/android/EmailEntryFragment;->g:Z

    if-eqz v4, :cond_4

    .line 236
    invoke-static {v2, p0}, Lcom/twitter/android/widget/PendingEmailOverlayPrompt;->a(Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 246
    :goto_3
    iget-boolean v2, p0, Lcom/twitter/android/EmailEntryFragment;->g:Z

    if-eqz v2, :cond_6

    .line 247
    new-array v1, v8, [Ljava/lang/String;

    const-string/jumbo v2, "add_update_email:::email_attach"

    aput-object v2, v1, v6

    aput-object v0, v1, v7

    invoke-virtual {v3, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 253
    :goto_4
    invoke-static {v3}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 238
    :cond_4
    iget-object v2, p0, Lcom/twitter/android/EmailEntryFragment;->l:Lcom/twitter/android/au;

    invoke-interface {v2}, Lcom/twitter/android/au;->bl_()V

    goto :goto_3

    .line 241
    :cond_5
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->T:Landroid/content/Context;

    const v2, 0x7f0a0336

    invoke-static {v0, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 242
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 243
    const-string/jumbo v0, "failure"

    goto :goto_3

    .line 249
    :cond_6
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v4, "phone100_email_optional"

    aput-object v4, v2, v6

    const-string/jumbo v4, "add_email"

    aput-object v4, v2, v7

    iget-boolean v4, p0, Lcom/twitter/android/EmailEntryFragment;->f:Z

    .line 250
    invoke-static {v4}, Lcom/twitter/android/bw;->a(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v8

    const/4 v4, 0x3

    aput-object v1, v2, v4

    const/4 v1, 0x4

    aput-object v0, v2, v1

    .line 249
    invoke-virtual {v3, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    goto :goto_4

    :cond_7
    move-object v0, v1

    goto/16 :goto_2

    .line 197
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 317
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 318
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 319
    const v1, 0x7f0a08ce

    invoke-virtual {p0, v1}, Lcom/twitter/android/EmailEntryFragment;->b_(I)V

    .line 320
    invoke-virtual {p0, v0, p1}, Lcom/twitter/android/EmailEntryFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 372
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->T:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/EmailEntryFragment;->t:Lcom/twitter/library/client/Session;

    move-object v3, p2

    move-object v4, v2

    move-object v5, p1

    .line 373
    invoke-static/range {v0 .. v6}, Lbbg;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lbbg;

    move-result-object v0

    .line 375
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1, v6}, Lcom/twitter/android/EmailEntryFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 376
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 415
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 416
    invoke-direct {p0, p1}, Lcom/twitter/android/EmailEntryFragment;->c(Ljava/lang/String;)V

    .line 417
    invoke-direct {p0, p2}, Lcom/twitter/android/EmailEntryFragment;->a(Ljava/util/List;)V

    .line 418
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->requestFocus()Z

    .line 419
    return-void
.end method

.method public a(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 433
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 434
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->i:Lcom/twitter/ui/widget/TypefacesTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TypefacesTextView;->setVisibility(I)V

    .line 440
    :goto_0
    return-void

    .line 436
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->i:Lcom/twitter/ui/widget/TypefacesTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TypefacesTextView;->setVisibility(I)V

    .line 437
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->T:Landroid/content/Context;

    invoke-static {p1}, Lcom/twitter/util/collection/CollectionUtils;->d(Ljava/util/Collection;)[I

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/EmailEntryFragment;->i:Lcom/twitter/ui/widget/TypefacesTextView;

    invoke-static {v0, v1, v2, p0}, Lcom/twitter/android/util/SpannableTextUtil;->a(Landroid/content/Context;[ILandroid/widget/TextView;Lcom/twitter/android/util/SpannableTextUtil$a;)V

    goto :goto_0
.end method

.method a(Z)V
    .locals 2

    .prologue
    .line 309
    iget-object v1, p0, Lcom/twitter/android/EmailEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    if-eqz p1, :cond_0

    sget-object v0, Lcom/twitter/android/EmailEntryFragment;->d:[I

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TwitterEditText;->setExtraState([I)V

    .line 310
    return-void

    .line 309
    :cond_0
    sget-object v0, Lcom/twitter/android/EmailEntryFragment;->c:[I

    goto :goto_0
.end method

.method public a(ZII)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 398
    if-eqz p1, :cond_2

    .line 399
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->j:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 400
    if-eqz p2, :cond_0

    .line 401
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->j:Landroid/view/View;

    const v1, 0x7f1303a0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    .line 403
    :cond_0
    if-eqz p3, :cond_1

    .line 404
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->j:Landroid/view/View;

    const v1, 0x7f1307f4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 405
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(I)V

    .line 406
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 411
    :cond_1
    :goto_0
    return-void

    .line 409
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->j:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 284
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 285
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/twitter/android/EmailEntryFragment;->c(I)Lcom/twitter/android/ValidationState$State;

    move-result-object v0

    .line 286
    sget-object v1, Lcom/twitter/android/ValidationState$State;->b:Lcom/twitter/android/ValidationState$State;

    if-ne v0, v1, :cond_1

    .line 287
    iget-object v1, p0, Lcom/twitter/android/EmailEntryFragment;->a:Lcom/twitter/android/EmailEntryFragment$a;

    invoke-virtual {v1, v2}, Lcom/twitter/android/EmailEntryFragment$a;->a(I)V

    .line 291
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/EmailEntryFragment;->m:Lcom/twitter/android/ValidationState$a;

    new-instance v2, Lcom/twitter/android/ValidationState;

    sget-object v3, Lcom/twitter/android/ValidationState$Level;->a:Lcom/twitter/android/ValidationState$Level;

    invoke-direct {v2, v0, v3}, Lcom/twitter/android/ValidationState;-><init>(Lcom/twitter/android/ValidationState$State;Lcom/twitter/android/ValidationState$Level;)V

    invoke-interface {v1, v2}, Lcom/twitter/android/ValidationState$a;->a(Lcom/twitter/android/ValidationState;)V

    .line 293
    :cond_0
    return-void

    .line 289
    :cond_1
    iget-object v1, p0, Lcom/twitter/android/EmailEntryFragment;->a:Lcom/twitter/android/EmailEntryFragment$a;

    invoke-virtual {v1, v2}, Lcom/twitter/android/EmailEntryFragment$a;->removeMessages(I)V

    goto :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 155
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->b()V

    .line 156
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 157
    iget-boolean v0, p0, Lcom/twitter/android/EmailEntryFragment;->e:Z

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->T:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/EmailEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    iget-object v2, p0, Lcom/twitter/android/EmailEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v2}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/twitter/util/ui/k;->a(Landroid/content/Context;Landroid/view/View;Z)V

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->m:Lcom/twitter/android/ValidationState$a;

    if-eqz v0, :cond_3

    .line 162
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->m:Lcom/twitter/android/ValidationState$a;

    invoke-interface {v0}, Lcom/twitter/android/ValidationState$a;->e()Lcom/twitter/android/ValidationState;

    move-result-object v0

    .line 166
    :goto_0
    if-eqz v0, :cond_1

    .line 167
    invoke-virtual {v0}, Lcom/twitter/android/ValidationState;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 168
    invoke-virtual {p0, v3}, Lcom/twitter/android/EmailEntryFragment;->a(Z)V

    .line 175
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->p:Lcom/twitter/android/w;

    if-eqz v0, :cond_2

    .line 176
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->p:Lcom/twitter/android/w;

    invoke-virtual {p0, v0}, Lcom/twitter/android/EmailEntryFragment;->a(Lcom/twitter/android/w;)V

    .line 178
    :cond_2
    return-void

    .line 164
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 169
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->m:Lcom/twitter/android/ValidationState$a;

    invoke-interface {v0}, Lcom/twitter/android/ValidationState$a;->e()Lcom/twitter/android/ValidationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/ValidationState;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    .line 170
    invoke-direct {p0, v0}, Lcom/twitter/android/EmailEntryFragment;->c(I)Lcom/twitter/android/ValidationState$State;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/ValidationState$State;->b:Lcom/twitter/android/ValidationState$State;

    if-ne v0, v1, :cond_1

    .line 172
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->a:Lcom/twitter/android/EmailEntryFragment$a;

    invoke-virtual {v0, v3}, Lcom/twitter/android/EmailEntryFragment$a;->a(I)V

    goto :goto_1
.end method

.method public b(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 474
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->h:Lcom/twitter/ui/widget/TypefacesTextView;

    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/TypefacesTextView;->setVisibility(I)V

    .line 475
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->T:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/EmailEntryFragment;->h:Lcom/twitter/ui/widget/TypefacesTextView;

    invoke-static {v0, v1, p1, v2}, Lcom/twitter/android/util/SpannableTextUtil;->a(Landroid/content/Context;Landroid/widget/TextView;IZ)V

    .line 476
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->h:Lcom/twitter/ui/widget/TypefacesTextView;

    iget-object v1, p0, Lcom/twitter/android/EmailEntryFragment;->T:Landroid/content/Context;

    const v2, 0x7f0d02d2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/ui/widget/TypefacesTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 477
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 493
    const v0, 0x7f0a0884

    invoke-virtual {p0, v0}, Lcom/twitter/android/EmailEntryFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 494
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->r:Lcom/twitter/android/c;

    iget-object v1, p0, Lcom/twitter/android/EmailEntryFragment;->o:Lcom/twitter/android/s;

    invoke-virtual {v1}, Lcom/twitter/android/s;->a()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/twitter/android/c;->b(Z)V

    .line 498
    :cond_0
    :goto_0
    return-void

    .line 495
    :cond_1
    const v0, 0x7f0a09cc

    invoke-virtual {p0, v0}, Lcom/twitter/android/EmailEntryFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->n:Lcom/twitter/android/by;

    invoke-interface {v0}, Lcom/twitter/android/by;->s()V

    goto :goto_0
.end method

.method public b_(I)V
    .locals 3

    .prologue
    .line 338
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->q:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    if-nez v0, :cond_0

    .line 339
    invoke-static {p1}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->a(I)Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->q:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    .line 340
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->q:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->setRetainInstance(Z)V

    .line 341
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->q:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    invoke-virtual {p0}, Lcom/twitter/android/EmailEntryFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 343
    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 276
    return-void
.end method

.method d()Z
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->m:Lcom/twitter/android/ValidationState$a;

    invoke-interface {v0}, Lcom/twitter/android/ValidationState$a;->e()Lcom/twitter/android/ValidationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/ValidationState;->a()Z

    move-result v0

    return v0
.end method

.method public e()Lcom/twitter/model/onboarding/a;
    .locals 2

    .prologue
    .line 327
    new-instance v0, Lcom/twitter/model/onboarding/g;

    iget-object v1, p0, Lcom/twitter/android/EmailEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/model/onboarding/g;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method protected f()V
    .locals 2

    .prologue
    .line 331
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->n:Lcom/twitter/android/by;

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->n:Lcom/twitter/android/by;

    iget-object v1, p0, Lcom/twitter/android/EmailEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v1}, Lcom/twitter/ui/widget/TwitterEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/by;->e(Ljava/lang/String;)V

    .line 333
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->n:Lcom/twitter/android/by;

    invoke-interface {v0}, Lcom/twitter/android/by;->t()V

    .line 335
    :cond_0
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->q:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->q:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->e()V

    .line 348
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->q:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    .line 350
    :cond_0
    return-void
.end method

.method protected h()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 355
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    invoke-direct {v0, v4}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a036a

    .line 356
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0369

    .line 357
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a026a

    .line 358
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0268

    .line 359
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 360
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 361
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 362
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 363
    invoke-virtual {p0}, Lcom/twitter/android/EmailEntryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 365
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/EmailEntryFragment;->s:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "phone100_email_optional"

    aput-object v2, v1, v4

    const/4 v2, 0x1

    const-string/jumbo v3, "skip_confirm_dialog"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-boolean v3, p0, Lcom/twitter/android/EmailEntryFragment;->f:Z

    .line 367
    invoke-static {v3}, Lcom/twitter/android/bw;->a(Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const/4 v3, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "impression"

    aput-object v3, v1, v2

    .line 366
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 365
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 368
    return-void
.end method

.method public i()V
    .locals 3

    .prologue
    .line 423
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    iget-object v1, p0, Lcom/twitter/android/EmailEntryFragment;->T:Landroid/content/Context;

    const v2, 0x7f0d0456

    invoke-virtual {v0, v1, v2}, Lcom/twitter/ui/widget/TwitterEditText;->setTextAppearance(Landroid/content/Context;I)V

    .line 424
    return-void
.end method

.method public j()V
    .locals 3

    .prologue
    .line 428
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->T:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/EmailEntryFragment;->k:Landroid/view/View;

    const v2, 0x7f0e0063

    invoke-static {v0, v1, v2}, Lcom/twitter/android/k;->a(Landroid/content/Context;Landroid/view/View;I)V

    .line 429
    return-void
.end method

.method public k()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 481
    invoke-virtual {p0}, Lcom/twitter/android/EmailEntryFragment;->I()Lcom/twitter/app/common/base/b;

    move-result-object v0

    .line 482
    const-string/jumbo v1, "current_email"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 483
    const-string/jumbo v2, "umf_update_email_flow"

    invoke-virtual {v0, v2, v3}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 484
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 485
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->k:Landroid/view/View;

    const v2, 0x7f130364

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterEditText;

    .line 486
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterEditText;->setText(Ljava/lang/CharSequence;)V

    .line 487
    invoke-virtual {v0, v3}, Lcom/twitter/ui/widget/TwitterEditText;->setVisibility(I)V

    .line 489
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 146
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onAttach(Landroid/app/Activity;)V

    .line 147
    const-class v0, Lcom/twitter/android/au;

    invoke-virtual {p0, v0}, Lcom/twitter/android/EmailEntryFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/au;

    iput-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->l:Lcom/twitter/android/au;

    .line 148
    const-class v0, Lcom/twitter/android/ValidationState$a;

    invoke-virtual {p0, v0}, Lcom/twitter/android/EmailEntryFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ValidationState$a;

    iput-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->m:Lcom/twitter/android/ValidationState$a;

    .line 149
    const-class v0, Lcom/twitter/android/by;

    invoke-virtual {p0, v0}, Lcom/twitter/android/EmailEntryFragment;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/by;

    iput-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->n:Lcom/twitter/android/by;

    .line 150
    const-class v0, Lcom/twitter/android/c;

    invoke-virtual {p0, v0}, Lcom/twitter/android/EmailEntryFragment;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/c;

    iput-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->r:Lcom/twitter/android/c;

    .line 151
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 104
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 106
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->T:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/network/f;->a(Landroid/content/Context;)Lcom/twitter/library/network/e;

    move-result-object v0

    .line 107
    const-string/jumbo v1, "people_discoverability_settings_update_enabled"

    invoke-static {v1}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v1

    .line 109
    new-instance v2, Lcom/twitter/android/s;

    invoke-direct {v2, p0, v0, v1}, Lcom/twitter/android/s;-><init>(Lcom/twitter/android/s$a;Lcom/twitter/library/network/e;Z)V

    iput-object v2, p0, Lcom/twitter/android/EmailEntryFragment;->o:Lcom/twitter/android/s;

    .line 110
    new-instance v0, Lcom/twitter/android/EmailEntryFragment$a;

    invoke-direct {v0, p0}, Lcom/twitter/android/EmailEntryFragment$a;-><init>(Lcom/twitter/android/EmailEntryFragment;)V

    iput-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->a:Lcom/twitter/android/EmailEntryFragment$a;

    .line 111
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    .line 116
    const v0, 0x7f0400e0

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/EmailEntryFragment;->k:Landroid/view/View;

    .line 117
    invoke-virtual {p0}, Lcom/twitter/android/EmailEntryFragment;->I()Lcom/twitter/app/common/base/b;

    move-result-object v2

    .line 119
    const v0, 0x7f130365

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterEditText;

    iput-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    .line 120
    const v0, 0x7f13016a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    iput-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->h:Lcom/twitter/ui/widget/TypefacesTextView;

    .line 121
    const v0, 0x7f130366

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    iput-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->i:Lcom/twitter/ui/widget/TypefacesTextView;

    .line 122
    const v0, 0x7f1307f3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->j:Landroid/view/View;

    .line 124
    const-string/jumbo v0, "user_id"

    invoke-virtual {v2, v0, v8, v9}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/android/EmailEntryFragment;->s:J

    .line 125
    iget-wide v4, p0, Lcom/twitter/android/EmailEntryFragment;->s:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/twitter/android/EmailEntryFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/android/EmailEntryFragment;->s:J

    .line 127
    invoke-virtual {p0}, Lcom/twitter/android/EmailEntryFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->t:Lcom/twitter/library/client/Session;

    .line 132
    :goto_0
    const-string/jumbo v0, "add_email"

    const-string/jumbo v3, "phone_100_step"

    .line 133
    invoke-virtual {v2, v3}, Lcom/twitter/app/common/base/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 132
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/EmailEntryFragment;->e:Z

    .line 134
    const-string/jumbo v0, "umf_add_update_email_flow"

    invoke-virtual {v2, v0, v6}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/EmailEntryFragment;->g:Z

    .line 136
    const-string/jumbo v0, "suggested_email"

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 138
    invoke-static {v2}, Lcom/twitter/android/bw;->a(Lcom/twitter/app/common/base/b;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/android/EmailEntryFragment;->f:Z

    .line 140
    iget-object v3, p0, Lcom/twitter/android/EmailEntryFragment;->o:Lcom/twitter/android/s;

    const-string/jumbo v4, "phone_100_step"

    const-string/jumbo v5, ""

    invoke-virtual {v2, v4, v5}, Lcom/twitter/app/common/base/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2, v0}, Lcom/twitter/android/s;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    return-object v1

    .line 129
    :cond_0
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    iget-wide v4, p0, Lcom/twitter/android/EmailEntryFragment;->s:J

    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->t:Lcom/twitter/library/client/Session;

    goto :goto_0
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 266
    invoke-virtual {p1}, Landroid/widget/TextView;->getId()I

    move-result v0

    .line 267
    const v1, 0x7f130365

    if-ne v0, v1, :cond_0

    const/4 v0, 0x5

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->l:Lcom/twitter/android/au;

    invoke-interface {v0}, Lcom/twitter/android/au;->n_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->l:Lcom/twitter/android/au;

    invoke-interface {v0}, Lcom/twitter/android/au;->b()V

    .line 269
    const/4 v0, 0x1

    .line 271
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 280
    return-void
.end method

.method public q_()V
    .locals 3

    .prologue
    .line 182
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->T:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/EmailEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    .line 183
    iget-object v0, p0, Lcom/twitter/android/EmailEntryFragment;->b:Lcom/twitter/ui/widget/TwitterEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 184
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->q_()V

    .line 185
    return-void
.end method
