.class public Lcom/twitter/android/ay;
.super Lcom/twitter/internal/android/widget/DockLayout$c;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/android/ax;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/twitter/android/ax;Lcom/twitter/internal/android/widget/ToolBar;I)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/DockLayout$c;-><init>()V

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/ay;->c:Z

    .line 25
    iput p3, p0, Lcom/twitter/android/ay;->b:I

    .line 26
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/ay;->a:Ljava/lang/ref/WeakReference;

    .line 28
    if-eqz p2, :cond_0

    instance-of v0, p2, Lcom/twitter/library/widget/StatusToolBar;

    if-eqz v0, :cond_0

    .line 29
    check-cast p2, Lcom/twitter/library/widget/StatusToolBar;

    .line 30
    new-instance v0, Lcom/twitter/android/ay$1;

    invoke-direct {v0, p0, p3, p2}, Lcom/twitter/android/ay$1;-><init>(Lcom/twitter/android/ay;ILcom/twitter/library/widget/StatusToolBar;)V

    invoke-virtual {p2, v0}, Lcom/twitter/library/widget/StatusToolBar;->setStatusToolBarListener(Lcom/twitter/library/widget/StatusToolBar$b;)V

    .line 42
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/ay;I)I
    .locals 0

    .prologue
    .line 16
    iput p1, p0, Lcom/twitter/android/ay;->b:I

    return p1
.end method


# virtual methods
.method public a(IIII)V
    .locals 5

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/android/ay;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ax;

    .line 47
    if-nez v0, :cond_1

    .line 67
    :cond_0
    return-void

    .line 51
    :cond_1
    invoke-interface {v0}, Lcom/twitter/android/ax;->j_()Lcom/twitter/android/AbsPagesAdapter;

    move-result-object v1

    .line 52
    if-eqz v1, :cond_0

    .line 56
    invoke-virtual {v1}, Lcom/twitter/android/AbsPagesAdapter;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/client/m;

    .line 57
    invoke-interface {v0, v1}, Lcom/twitter/android/ax;->a(Lcom/twitter/library/client/m;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 59
    instance-of v2, v1, Lcom/twitter/app/common/list/TwitterListFragment;

    if-eqz v2, :cond_2

    .line 60
    iget-boolean v2, p0, Lcom/twitter/android/ay;->c:Z

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/twitter/android/ay;->b:I

    :goto_1
    add-int/2addr v2, p2

    .line 61
    check-cast v1, Lcom/twitter/app/common/list/TwitterListFragment;

    .line 62
    invoke-virtual {v1}, Lcom/twitter/app/common/list/TwitterListFragment;->aj()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v1}, Lcom/twitter/app/common/list/TwitterListFragment;->am()I

    move-result v4

    if-eq v2, v4, :cond_2

    .line 63
    invoke-virtual {v1, v2}, Lcom/twitter/app/common/list/TwitterListFragment;->g(I)V

    goto :goto_0

    .line 60
    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/twitter/android/ay;->c:Z

    .line 72
    return-void
.end method
