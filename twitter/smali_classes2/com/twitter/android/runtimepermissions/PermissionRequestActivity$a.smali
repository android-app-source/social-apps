.class public Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:[Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:I
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation
.end field

.field private i:I
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation
.end field

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Z


# direct methods
.method public varargs constructor <init>(Ljava/lang/String;Landroid/content/Context;[Ljava/lang/String;)V
    .locals 3

    .prologue
    const v2, 0x7f0d0196

    .line 350
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 351
    iput-object p2, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->a:Landroid/content/Context;

    .line 352
    iput-object p3, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->b:[Ljava/lang/String;

    .line 353
    iput-object p1, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->c:Ljava/lang/String;

    .line 355
    iget-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 356
    const v1, 0x7f0a064d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->c(Ljava/lang/String;)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    .line 357
    const v1, 0x7f0a0209

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->a(Ljava/lang/String;)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    .line 358
    const v1, 0x7f0a05eb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->b(Ljava/lang/String;)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    .line 359
    const v1, 0x7f0a064c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->d(Ljava/lang/String;)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    .line 360
    invoke-virtual {p0, v2}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->a(I)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    .line 361
    invoke-virtual {p0, v2}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->b(I)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    .line 362
    return-void
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 366
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->a:Landroid/content/Context;

    const-class v2, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "extra_permissions"

    iget-object v2, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->b:[Ljava/lang/String;

    .line 367
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_prelim_title"

    iget-object v2, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->c:Ljava/lang/String;

    .line 368
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 369
    const-string/jumbo v1, "extra_prelim_pos_text"

    iget-object v2, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 370
    const-string/jumbo v1, "extra_prelim_neg_text"

    iget-object v2, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 371
    const-string/jumbo v1, "extra_always_prelim"

    iget-boolean v2, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->g:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 372
    const-string/jumbo v1, "extra_prelim_dialog_theme"

    iget v2, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->h:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 373
    const-string/jumbo v1, "extra_retarget_dialog_theme"

    iget v2, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->i:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 374
    const-string/jumbo v1, "extra_retarget_title"

    iget-object v2, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 375
    const-string/jumbo v1, "extra_prelim_msg"

    iget-object v2, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 376
    const-string/jumbo v1, "extra_retarget_msg_fmt"

    iget-object v2, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 377
    const-string/jumbo v1, "extra_event_prefix"

    iget-object v2, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 378
    const-string/jumbo v1, "extra_use_snackbar"

    iget-boolean v2, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->m:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 379
    return-object v0
.end method

.method public a(I)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param

    .prologue
    .line 398
    iput p1, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->h:I

    .line 399
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;
    .locals 0

    .prologue
    .line 383
    iput-object p1, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->e:Ljava/lang/String;

    .line 384
    return-object p0
.end method

.method public a(Z)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;
    .locals 0

    .prologue
    .line 393
    iput-boolean p1, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->g:Z

    .line 394
    return-object p0
.end method

.method public b(I)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param

    .prologue
    .line 403
    iput p1, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->i:I

    .line 404
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;
    .locals 0

    .prologue
    .line 388
    iput-object p1, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->f:Ljava/lang/String;

    .line 389
    return-object p0
.end method

.method public b(Z)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;
    .locals 0

    .prologue
    .line 428
    iput-boolean p1, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->m:Z

    .line 429
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;
    .locals 0

    .prologue
    .line 408
    iput-object p1, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->d:Ljava/lang/String;

    .line 409
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;
    .locals 0

    .prologue
    .line 413
    iput-object p1, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->k:Ljava/lang/String;

    .line 414
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;
    .locals 0

    .prologue
    .line 418
    iput-object p1, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->j:Ljava/lang/String;

    .line 419
    return-object p0
.end method

.method public f(Ljava/lang/String;)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;
    .locals 0

    .prologue
    .line 423
    iput-object p1, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->l:Ljava/lang/String;

    .line 424
    return-object p0
.end method
