.class public Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;
.super Lcom/twitter/app/common/base/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/dialog/b$c;
.implements Lcom/twitter/app/common/dialog/b$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;,
        Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;
    }
.end annotation


# instance fields
.field protected a:Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field protected b:Ljava/lang/String;

.field c:Lcom/twitter/util/android/PermissionResult;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field d:I
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation

    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field e:I
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation

    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private f:[Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 297
    const-string/jumbo v1, "%s:%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 298
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    new-array v3, v3, [Ljava/lang/String;

    aput-object v1, v3, v0

    invoke-direct {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>([Ljava/lang/String;)V

    .line 299
    array-length v1, p2

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v3, p2, v0

    .line 300
    new-instance v4, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v4}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 301
    iput-object v3, v4, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 302
    invoke-virtual {v2, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 299
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 304
    :cond_0
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 305
    return-void
.end method

.method public static a(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 327
    if-eqz p0, :cond_0

    const-string/jumbo v0, "extra_perm_result"

    .line 328
    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/android/PermissionResult;

    .line 329
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/util/android/PermissionResult;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 328
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 329
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private i()V
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->g:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 278
    iget-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->c:Lcom/twitter/util/android/PermissionResult;

    iget-object v0, v0, Lcom/twitter/util/android/PermissionResult;->a:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 279
    invoke-virtual {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->e()V

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->c:Lcom/twitter/util/android/PermissionResult;

    iget-object v0, v0, Lcom/twitter/util/android/PermissionResult;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 282
    invoke-virtual {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->f()V

    .line 285
    :cond_1
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 3

    .prologue
    .line 161
    sget-object v0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;->b:Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;

    iput-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a:Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;

    .line 162
    invoke-virtual {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 163
    new-instance v0, Lcom/twitter/android/dialog/g$b;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Lcom/twitter/android/dialog/g$b;-><init>(I)V

    const-string/jumbo v2, "extra_prelim_title"

    .line 165
    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/dialog/g$b;->a(Ljava/lang/CharSequence;)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const-string/jumbo v2, "extra_prelim_pos_text"

    .line 166
    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/dialog/g$b;->c(Ljava/lang/CharSequence;)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const-string/jumbo v2, "extra_prelim_neg_text"

    .line 167
    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/dialog/g$b;->d(Ljava/lang/CharSequence;)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    .line 169
    const-string/jumbo v2, "extra_prelim_msg"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 170
    if-eqz v1, :cond_0

    .line 171
    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/g$b;->b(Ljava/lang/CharSequence;)Lcom/twitter/android/dialog/f$a;

    .line 173
    :cond_0
    iget v1, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->d:I

    .line 174
    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/g$b;->i(I)Lcom/twitter/app/common/dialog/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    .line 175
    invoke-virtual {v0}, Lcom/twitter/android/dialog/g$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 176
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$c;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 177
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 178
    invoke-virtual {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 179
    return-void
.end method

.method public a(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 233
    invoke-virtual {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->isChangingConfigurations()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 253
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 239
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a:Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;

    sget-object v1, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;->b:Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;

    if-ne v0, v1, :cond_0

    .line 240
    iget-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->f:[Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/twitter/util/android/PermissionResult;->a(Landroid/content/Context;[Ljava/lang/String;)Lcom/twitter/util/android/PermissionResult;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->c:Lcom/twitter/util/android/PermissionResult;

    .line 241
    invoke-virtual {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->h()V

    goto :goto_0

    .line 246
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->h()V

    goto :goto_0

    .line 237
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 211
    packed-switch p2, :pswitch_data_0

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 213
    :pswitch_0
    if-ne p3, v0, :cond_0

    .line 214
    invoke-virtual {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->c()V

    goto :goto_0

    .line 219
    :pswitch_1
    if-ne p3, v0, :cond_0

    .line 220
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/util/android/f;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 221
    invoke-virtual {p0, v0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 211
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected b()Z
    .locals 2

    .prologue
    .line 127
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->f:[Ljava/lang/String;

    invoke-virtual {v0, p0, v1}, Lcom/twitter/util/android/f;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected c()V
    .locals 3

    .prologue
    .line 182
    sget-object v0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;->d:Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;

    iput-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a:Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;

    .line 183
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->f:[Ljava/lang/String;

    invoke-virtual {v0, v1, p0, v2}, Lcom/twitter/util/android/f;->a(ILandroid/app/Activity;[Ljava/lang/String;)V

    .line 184
    return-void
.end method

.method protected d()V
    .locals 6

    .prologue
    .line 187
    sget-object v0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;->f:Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;

    iput-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a:Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;

    .line 188
    invoke-virtual {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 190
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->c:Lcom/twitter/util/android/PermissionResult;

    iget-object v2, v2, Lcom/twitter/util/android/PermissionResult;->b:[Ljava/lang/String;

    invoke-virtual {v0, p0, v2}, Lcom/twitter/util/android/f;->b(Landroid/content/Context;[Ljava/lang/String;)Ljava/util/Set;

    move-result-object v2

    .line 193
    new-instance v0, Lcom/twitter/android/dialog/g$b;

    const/4 v3, 0x2

    invoke-direct {v0, v3}, Lcom/twitter/android/dialog/g$b;-><init>(I)V

    const-string/jumbo v3, "extra_retarget_title"

    .line 195
    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/android/dialog/g$b;->a(Ljava/lang/CharSequence;)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const v3, 0x7f0a03d2

    .line 196
    invoke-virtual {v0, v3}, Lcom/twitter/android/dialog/g$b;->d(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    const v3, 0x7f0a05eb

    .line 197
    invoke-virtual {v0, v3}, Lcom/twitter/android/dialog/g$b;->e(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    .line 199
    const-string/jumbo v3, "extra_retarget_msg_fmt"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 200
    if-eqz v1, :cond_0

    .line 201
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v5, ", "

    invoke-static {v5, v2}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/g$b;->b(Ljava/lang/CharSequence;)Lcom/twitter/android/dialog/f$a;

    .line 203
    :cond_0
    iget v1, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->e:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/dialog/g$b;->i(I)Lcom/twitter/app/common/dialog/a$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/dialog/g$b;

    .line 204
    invoke-virtual {v0}, Lcom/twitter/android/dialog/g$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 205
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$c;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 206
    invoke-virtual {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 207
    return-void
.end method

.method protected e()V
    .locals 3

    .prologue
    .line 288
    iget-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->g:Ljava/lang/String;

    const-string/jumbo v1, "permissions_granted"

    iget-object v2, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->c:Lcom/twitter/util/android/PermissionResult;

    iget-object v2, v2, Lcom/twitter/util/android/PermissionResult;->a:[Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 289
    return-void
.end method

.method protected f()V
    .locals 3

    .prologue
    .line 292
    iget-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->g:Ljava/lang/String;

    const-string/jumbo v1, "permissions_denied"

    iget-object v2, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->c:Lcom/twitter/util/android/PermissionResult;

    iget-object v2, v2, Lcom/twitter/util/android/PermissionResult;->b:[Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 293
    return-void
.end method

.method protected h()V
    .locals 4

    .prologue
    .line 308
    const/4 v0, -0x1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v2, "extra_perm_result"

    iget-object v3, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->c:Lcom/twitter/util/android/PermissionResult;

    .line 309
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "extra_permissions"

    iget-object v3, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->f:[Ljava/lang/String;

    .line 310
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 308
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->setResult(ILandroid/content/Intent;)V

    .line 311
    invoke-virtual {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->finish()V

    .line 312
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 84
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 86
    invoke-virtual {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 87
    const-string/jumbo v0, "extra_permissions"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->f:[Ljava/lang/String;

    .line 88
    const-string/jumbo v0, "extra_prelim_dialog_theme"

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->d:I

    .line 89
    const-string/jumbo v0, "extra_retarget_dialog_theme"

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->e:I

    .line 91
    const-string/jumbo v0, "extra_event_prefix"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->g:Ljava/lang/String;

    .line 92
    iget-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->g:Ljava/lang/String;

    const-string/jumbo v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->b:Ljava/lang/String;

    .line 94
    const-string/jumbo v0, ":"

    iget-object v2, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->g:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/twitter/util/y;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_0

    .line 95
    new-instance v0, Lcpb;

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Event prefix excluding action \"%s\" should have format: <page>:<section>:<component>:<element>"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->g:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v2}, Lcpb;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    .line 102
    :cond_0
    if-eqz p1, :cond_1

    .line 103
    const-string/jumbo v0, "key_perm_result"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/android/PermissionResult;

    iput-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->c:Lcom/twitter/util/android/PermissionResult;

    .line 104
    const-string/jumbo v0, "key_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;

    iput-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a:Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;

    .line 115
    :goto_0
    return-void

    .line 106
    :cond_1
    const-string/jumbo v0, "extra_always_prelim"

    .line 107
    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 108
    if-nez v0, :cond_2

    .line 109
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->f:[Ljava/lang/String;

    invoke-virtual {v0, p0, v1}, Lcom/twitter/util/android/f;->a(Landroid/app/Activity;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 110
    :cond_2
    sget-object v0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;->a:Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;

    iput-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a:Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;

    goto :goto_0

    .line 112
    :cond_3
    sget-object v0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;->c:Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;

    iput-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a:Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;

    goto :goto_0
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
            to = 0xffffL
        .end annotation
    .end param

    .prologue
    .line 260
    const/4 v0, 0x1

    if-ne v0, p1, :cond_1

    .line 261
    iget-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->f:[Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/twitter/util/android/PermissionResult;->a(Landroid/content/Context;[Ljava/lang/String;)Lcom/twitter/util/android/PermissionResult;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->c:Lcom/twitter/util/android/PermissionResult;

    .line 263
    invoke-direct {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->i()V

    .line 266
    invoke-virtual {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_use_snackbar"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->c:Lcom/twitter/util/android/PermissionResult;

    invoke-virtual {v0}, Lcom/twitter/util/android/PermissionResult;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 267
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->h()V

    .line 274
    :cond_1
    :goto_0
    return-void

    .line 271
    :cond_2
    sget-object v0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;->e:Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;

    iput-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a:Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 119
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->onResume()V

    .line 120
    invoke-virtual {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->f:[Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/android/PermissionResult;->a([Ljava/lang/String;)Lcom/twitter/util/android/PermissionResult;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->c:Lcom/twitter/util/android/PermissionResult;

    .line 122
    invoke-virtual {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->h()V

    .line 124
    :cond_0
    return-void
.end method

.method protected onResumeFragments()V
    .locals 2

    .prologue
    .line 132
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->onResumeFragments()V

    .line 134
    sget-object v0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$1;->a:[I

    iget-object v1, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a:Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;

    invoke-virtual {v1}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 151
    :goto_0
    return-void

    .line 136
    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a()V

    goto :goto_0

    .line 140
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->c()V

    goto :goto_0

    .line 144
    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->d()V

    goto :goto_0

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 155
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 156
    const-string/jumbo v0, "key_perm_result"

    iget-object v1, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->c:Lcom/twitter/util/android/PermissionResult;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 157
    const-string/jumbo v0, "key_state"

    iget-object v1, p0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a:Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$State;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 158
    return-void
.end method
