.class public Lcom/twitter/android/SearchPhotosFragment;
.super Lcom/twitter/android/SearchFragment;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/SearchPhotosFragment$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/SearchFragment",
        "<",
        "Lcom/twitter/android/be$b;",
        "Lcom/twitter/android/be;",
        ">;"
    }
.end annotation


# instance fields
.field private aa:Ljava/lang/String;

.field private ab:Z

.field private final ac:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation
.end field

.field private final ad:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/twitter/android/SearchFragment;-><init>()V

    .line 51
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->ac:Ljava/util/List;

    .line 52
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->ad:Ljava/util/Set;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/SearchPhotosFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/content/Context;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lcom/twitter/android/SearchPhotosFragment$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/SearchPhotosFragment$1;-><init>(Lcom/twitter/android/SearchPhotosFragment;Landroid/content/Context;)V

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/SearchPhotosFragment;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->ad:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/SearchPhotosFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->ac:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method protected E()Ljava/lang/String;
    .locals 1

    .prologue
    .line 300
    const-string/jumbo v0, "photo_grid"

    return-object v0
.end method

.method protected F()V
    .locals 2

    .prologue
    .line 274
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/SearchPhotosFragment;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":photo_grid:::impression"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchPhotosFragment;->a(Ljava/lang/String;)V

    .line 275
    return-void
.end method

.method protected G()V
    .locals 5

    .prologue
    .line 280
    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->ac:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 282
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/SearchPhotosFragment;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":photo_grid:stream::results"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 283
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 284
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    .line 285
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/SearchPhotosFragment;->ac:Ljava/util/List;

    .line 286
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(Ljava/util/List;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/SearchPhotosFragment;->t:Ljava/lang/String;

    const/4 v2, 0x3

    .line 287
    invoke-static {v2}, Lcom/twitter/android/SearchPhotosFragment;->c(I)Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lcom/twitter/android/SearchPhotosFragment;->c:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchPhotosFragment;->b:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 283
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 288
    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->ac:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 290
    :cond_0
    return-void
.end method

.method protected J()Lcom/twitter/android/be;
    .locals 8

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 76
    new-instance v0, Lcom/twitter/android/be;

    .line 77
    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    sget v3, Lbtv;->d:I

    const v4, 0x7fffffff

    .line 79
    invoke-direct {p0, v1}, Lcom/twitter/android/SearchPhotosFragment;->b(Landroid/content/Context;)Landroid/view/View$OnClickListener;

    move-result-object v5

    new-instance v6, Lcom/twitter/android/SearchPhotosFragment$a;

    const/4 v7, 0x0

    invoke-direct {v6, p0, v7}, Lcom/twitter/android/SearchPhotosFragment$a;-><init>(Lcom/twitter/android/SearchPhotosFragment;Lcom/twitter/android/SearchPhotosFragment$1;)V

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/be;-><init>(Landroid/content/Context;FIILandroid/view/View$OnClickListener;Lcom/twitter/android/av;Z)V

    .line 76
    return-object v0
.end method

.method protected a(Landroid/content/Context;)V
    .locals 13

    .prologue
    const/4 v10, 0x1

    .line 141
    new-instance v1, Lcom/twitter/library/api/search/d;

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/android/SearchPhotosFragment;->r:J

    iget-object v6, p0, Lcom/twitter/android/SearchPhotosFragment;->t:Ljava/lang/String;

    .line 142
    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->B()I

    move-result v7

    iget-object v8, p0, Lcom/twitter/android/SearchPhotosFragment;->u:Ljava/lang/String;

    iget-object v9, p0, Lcom/twitter/android/SearchPhotosFragment;->s:Ljava/lang/String;

    iget-object v11, p0, Lcom/twitter/android/SearchPhotosFragment;->z:Ljava/lang/String;

    const/4 v12, 0x0

    move-object v2, p1

    invoke-direct/range {v1 .. v12}, Lcom/twitter/library/api/search/d;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V

    .line 144
    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->e()I

    move-result v0

    iget-boolean v2, p0, Lcom/twitter/android/SearchPhotosFragment;->c:Z

    iget-boolean v3, p0, Lcom/twitter/android/SearchPhotosFragment;->d:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchPhotosFragment;->e:Z

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/twitter/library/api/search/d;->a(IZZZ)Lcom/twitter/library/api/search/d;

    move-result-object v0

    .line 145
    iget-object v1, p0, Lcom/twitter/android/SearchPhotosFragment;->C:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 146
    iget-object v1, p0, Lcom/twitter/android/SearchPhotosFragment;->C:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/api/search/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/api/search/d;

    .line 148
    :cond_0
    const-string/jumbo v1, "Not triggered by a user action."

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/search/d;->l(Ljava/lang/String;)Lcom/twitter/library/service/s;

    .line 149
    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchPhotosFragment;->a(Lcom/twitter/library/api/search/d;)V

    .line 150
    iget-boolean v1, p0, Lcom/twitter/android/SearchPhotosFragment;->b:Z

    if-eqz v1, :cond_1

    .line 151
    iget-object v1, p0, Lcom/twitter/android/SearchPhotosFragment;->F:Lbqn;

    invoke-virtual {v1}, Lbqn;->a()Landroid/location/Location;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/search/d;->a(Landroid/location/Location;)Lcom/twitter/library/api/search/d;

    .line 153
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v10, v1}, Lcom/twitter/android/SearchPhotosFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 154
    return-void
.end method

.method protected a(Lcbi;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/android/be$b;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x3

    .line 158
    iget v1, p0, Lcom/twitter/android/SearchPhotosFragment;->k:I

    .line 159
    iget-boolean v0, p0, Lcom/twitter/android/SearchPhotosFragment;->ab:Z

    if-eqz v0, :cond_1

    .line 160
    if-ne v1, v4, :cond_0

    .line 161
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const/4 v2, 0x6

    .line 162
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-wide v2, p0, Lcom/twitter/android/SearchPhotosFragment;->a_:J

    .line 163
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v2, p0, Lcom/twitter/android/SearchPhotosFragment;->A:Ljava/lang/String;

    .line 164
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v2, "photo_grid"

    .line 165
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 161
    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchPhotosFragment;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 167
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->I_()V

    .line 170
    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/android/SearchFragment;->a(Lcbi;)V

    .line 172
    iget-boolean v0, p0, Lcom/twitter/android/SearchPhotosFragment;->ab:Z

    if-eqz v0, :cond_3

    .line 173
    const/4 v0, 0x2

    if-ne v1, v0, :cond_2

    .line 174
    iput v4, p0, Lcom/twitter/android/SearchPhotosFragment;->k:I

    .line 182
    :cond_2
    :goto_0
    return-void

    .line 177
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/be;

    invoke-virtual {v0}, Lcom/twitter/android/be;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 178
    invoke-virtual {p0, v4}, Lcom/twitter/android/SearchPhotosFragment;->a(I)Z

    .line 180
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/SearchPhotosFragment;->ab:Z

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x1

    .line 233
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/SearchFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 234
    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    .line 235
    check-cast p1, Lcom/twitter/library/api/search/d;

    .line 236
    invoke-virtual {p1}, Lcom/twitter/library/api/search/d;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 237
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a07e9

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 252
    :cond_1
    :goto_0
    return-void

    .line 239
    :cond_2
    invoke-virtual {p1}, Lcom/twitter/library/api/search/d;->h()I

    move-result v0

    if-nez v0, :cond_4

    .line 240
    if-ne p3, v5, :cond_3

    .line 241
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    new-array v2, v4, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/twitter/android/SearchPhotosFragment;->A:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ":photo_grid:stream::no_results"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    .line 242
    invoke-static {v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/SearchPhotosFragment;->t:Ljava/lang/String;

    .line 243
    invoke-static {v5}, Lcom/twitter/android/SearchPhotosFragment;->c(I)Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lcom/twitter/android/SearchPhotosFragment;->c:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchPhotosFragment;->b:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 241
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 245
    :cond_3
    if-ne p3, v4, :cond_1

    .line 246
    iput-boolean v4, p0, Lcom/twitter/android/SearchPhotosFragment;->g:Z

    goto :goto_0

    .line 248
    :cond_4
    iget-boolean v0, p0, Lcom/twitter/android/SearchPhotosFragment;->i:Z

    if-eqz v0, :cond_1

    if-ne p3, v5, :cond_1

    .line 249
    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->J_()V

    goto :goto_0
.end method

.method protected a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 264
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/android/SearchPhotosFragment;->aa:Ljava/lang/String;

    .line 265
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->k(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 266
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/SearchPhotosFragment;->t:Ljava/lang/String;

    const/4 v2, 0x3

    .line 267
    invoke-static {v2}, Lcom/twitter/android/SearchPhotosFragment;->c(I)Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lcom/twitter/android/SearchPhotosFragment;->c:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchPhotosFragment;->b:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 268
    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 264
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 269
    return-void
.end method

.method protected a(I)Z
    .locals 13

    .prologue
    .line 188
    invoke-virtual {p0, p1}, Lcom/twitter/android/SearchPhotosFragment;->c_(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 189
    const/4 v0, 0x0

    .line 228
    :goto_0
    return v0

    .line 191
    :cond_0
    iput p1, p0, Lcom/twitter/android/SearchPhotosFragment;->k:I

    .line 194
    packed-switch p1, :pswitch_data_0

    .line 208
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fetch type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 196
    :pswitch_0
    const/4 v10, 0x0

    .line 212
    :goto_1
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/SearchPhotosFragment;->a_:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/SearchPhotosFragment;->A:Ljava/lang/String;

    const-string/jumbo v4, "photo_grid"

    .line 213
    invoke-static {v3, v4, p1}, Lcom/twitter/android/SearchPhotosFragment;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/SearchPhotosFragment;->t:Ljava/lang/String;

    const/4 v2, 0x3

    .line 214
    invoke-static {v2}, Lcom/twitter/android/SearchPhotosFragment;->c(I)Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lcom/twitter/android/SearchPhotosFragment;->c:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchPhotosFragment;->b:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 216
    new-instance v1, Lcom/twitter/library/api/search/d;

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/android/SearchPhotosFragment;->r:J

    iget-object v6, p0, Lcom/twitter/android/SearchPhotosFragment;->t:Ljava/lang/String;

    .line 217
    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->B()I

    move-result v7

    iget-object v8, p0, Lcom/twitter/android/SearchPhotosFragment;->u:Ljava/lang/String;

    iget-object v9, p0, Lcom/twitter/android/SearchPhotosFragment;->s:Ljava/lang/String;

    iget-object v11, p0, Lcom/twitter/android/SearchPhotosFragment;->z:Ljava/lang/String;

    const/4 v12, 0x0

    invoke-direct/range {v1 .. v12}, Lcom/twitter/library/api/search/d;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V

    .line 218
    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->e()I

    move-result v2

    iget-boolean v3, p0, Lcom/twitter/android/SearchPhotosFragment;->c:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchPhotosFragment;->d:Z

    iget-boolean v5, p0, Lcom/twitter/android/SearchPhotosFragment;->e:Z

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/twitter/library/api/search/d;->a(IZZZ)Lcom/twitter/library/api/search/d;

    move-result-object v1

    .line 219
    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchPhotosFragment;->a(Lcom/twitter/library/api/search/d;)V

    .line 220
    iget-object v2, p0, Lcom/twitter/android/SearchPhotosFragment;->C:Ljava/lang/String;

    invoke-static {v2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 221
    iget-object v2, p0, Lcom/twitter/android/SearchPhotosFragment;->C:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/api/search/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/api/search/d;

    .line 223
    :cond_1
    const-string/jumbo v2, "scribe_log"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/api/search/d;->a(Ljava/lang/String;Landroid/os/Parcelable;)Lcom/twitter/library/service/s;

    .line 224
    iget-boolean v0, p0, Lcom/twitter/android/SearchPhotosFragment;->b:Z

    if-eqz v0, :cond_2

    .line 225
    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->F:Lbqn;

    invoke-virtual {v0}, Lbqn;->a()Landroid/location/Location;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/api/search/d;->a(Landroid/location/Location;)Lcom/twitter/library/api/search/d;

    .line 227
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, v1, v0, p1}, Lcom/twitter/android/SearchPhotosFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 228
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 200
    :pswitch_1
    const/4 v10, 0x1

    .line 201
    goto/16 :goto_1

    .line 204
    :pswitch_2
    const/4 v10, 0x2

    .line 205
    goto/16 :goto_1

    .line 194
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected ar_()V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0}, Lcom/twitter/android/SearchFragment;->ar_()V

    .line 57
    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->ak()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/be;

    invoke-virtual {v0}, Lcom/twitter/android/be;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58
    :cond_0
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchPhotosFragment;->a(I)Z

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/SearchPhotosFragment;->ab:Z

    .line 61
    :cond_1
    return-void
.end method

.method protected b(J)I
    .locals 1

    .prologue
    .line 256
    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/be;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/be;->a(J)I

    move-result v0

    .line 259
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 295
    const/4 v0, 0x3

    return v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    const-string/jumbo v0, "statuses_flags&1537 !=0 AND search_id=?"

    return-object v0
.end method

.method protected j()Landroid/support/v4/content/Loader;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    new-instance v0, Lcom/twitter/util/android/d;

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/database/schema/a$t;->a:Landroid/net/Uri;

    .line 120
    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 119
    invoke-static {v2, v4, v5}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lbtv;->a:[Ljava/lang/String;

    .line 122
    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->f()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-wide v8, p0, Lcom/twitter/android/SearchPhotosFragment;->r:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const-string/jumbo v6, "type_id ASC, _id ASC"

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 65
    invoke-super {p0, p1}, Lcom/twitter/android/SearchFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 67
    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->k()Lcom/twitter/android/bs;

    move-result-object v0

    const-string/jumbo v1, "scribe_context"

    invoke-virtual {v0, v1}, Lcom/twitter/android/bs;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->aa:Ljava/lang/String;

    .line 69
    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->J()Lcom/twitter/android/be;

    move-result-object v0

    .line 70
    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchPhotosFragment;->a(Lcom/twitter/android/client/j;)Lcom/twitter/app/common/list/TwitterListFragment;

    .line 71
    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/app/common/list/l;->a(Lcjr;)V

    .line 72
    return-void
.end method

.method protected r()Z
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x1

    return v0
.end method
