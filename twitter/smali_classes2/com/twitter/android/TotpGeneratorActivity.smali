.class public Lcom/twitter/android/TotpGeneratorActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"


# instance fields
.field private a:Ljava/lang/String;

.field private b:J

.field private c:Lcom/twitter/ui/widget/TypefacesTextView;

.field private d:Landroid/widget/ProgressBar;

.field private e:Lcom/twitter/internal/android/widget/ToolBar;

.field private f:Lcom/twitter/ui/widget/TypefacesTextView;

.field private g:Ljava/lang/Runnable;

.field private h:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    .line 34
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/android/TotpGeneratorActivity;->a:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TotpGeneratorActivity;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/twitter/android/TotpGeneratorActivity;->d:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private a(I)V
    .locals 4

    .prologue
    .line 140
    invoke-static {}, Lcom/twitter/util/i;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    rem-long/2addr v0, v2

    .line 142
    const/4 v2, 0x5

    if-gt p1, v2, :cond_1

    .line 143
    const-wide/16 v2, 0x1f4

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 144
    invoke-virtual {p0}, Lcom/twitter/android/TotpGeneratorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f1100e4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v1, v0

    .line 154
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/TotpGeneratorActivity;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v1, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 155
    iget-object v1, p0, Lcom/twitter/android/TotpGeneratorActivity;->c:Lcom/twitter/ui/widget/TypefacesTextView;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TypefacesTextView;->setTextColor(I)V

    .line 156
    return-void

    .line 147
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/TotpGeneratorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110070

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v1, v0

    .line 148
    goto :goto_0

    .line 151
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/TotpGeneratorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110190

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 152
    invoke-virtual {p0}, Lcom/twitter/android/TotpGeneratorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f11011b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/TotpGeneratorActivity;I)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/twitter/android/TotpGeneratorActivity;->a(I)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/TotpGeneratorActivity;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/twitter/android/TotpGeneratorActivity;->i()V

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/TotpGeneratorActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/twitter/android/TotpGeneratorActivity;->h:Landroid/os/Handler;

    return-object v0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/twitter/android/TotpGeneratorActivity;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TotpGeneratorActivity;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    .line 103
    iget-object v0, p0, Lcom/twitter/android/TotpGeneratorActivity;->c:Lcom/twitter/ui/widget/TypefacesTextView;

    iget-object v1, p0, Lcom/twitter/android/TotpGeneratorActivity;->a:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    :goto_0
    return-void

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TotpGeneratorActivity;->c:Lcom/twitter/ui/widget/TypefacesTextView;

    const v1, 0x7f0a0969

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    .line 48
    const v0, 0x7f0403f9

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 49
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 50
    return-object p2
.end method

.method protected a(Lcom/twitter/library/service/s;I)V
    .locals 2

    .prologue
    .line 82
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcom/twitter/library/service/s;I)V

    .line 83
    packed-switch p2, :pswitch_data_0

    .line 99
    :cond_0
    :goto_0
    return-void

    :pswitch_0
    move-object v0, p1

    .line 85
    check-cast v0, Lbbd;

    invoke-virtual {v0}, Lbbd;->e()Lcom/twitter/model/account/d;

    move-result-object v0

    .line 86
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87
    invoke-virtual {v0}, Lcom/twitter/model/account/d;->a()Ljava/lang/String;

    move-result-object v0

    .line 88
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89
    iput-object v0, p0, Lcom/twitter/android/TotpGeneratorActivity;->a:Ljava/lang/String;

    .line 90
    invoke-direct {p0}, Lcom/twitter/android/TotpGeneratorActivity;->i()V

    goto :goto_0

    .line 83
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 55
    const v0, 0x7f0a04f6

    invoke-virtual {p0, v0}, Lcom/twitter/android/TotpGeneratorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TotpGeneratorActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 57
    const v0, 0x7f130813

    invoke-virtual {p0, v0}, Lcom/twitter/android/TotpGeneratorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    iput-object v0, p0, Lcom/twitter/android/TotpGeneratorActivity;->c:Lcom/twitter/ui/widget/TypefacesTextView;

    .line 58
    const v0, 0x7f130814

    invoke-virtual {p0, v0}, Lcom/twitter/android/TotpGeneratorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/twitter/android/TotpGeneratorActivity;->d:Landroid/widget/ProgressBar;

    .line 59
    const v0, 0x7f13007f

    invoke-virtual {p0, v0}, Lcom/twitter/android/TotpGeneratorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    iput-object v0, p0, Lcom/twitter/android/TotpGeneratorActivity;->e:Lcom/twitter/internal/android/widget/ToolBar;

    .line 60
    const v0, 0x7f130558

    invoke-virtual {p0, v0}, Lcom/twitter/android/TotpGeneratorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    iput-object v0, p0, Lcom/twitter/android/TotpGeneratorActivity;->f:Lcom/twitter/ui/widget/TypefacesTextView;

    .line 62
    iget-object v0, p0, Lcom/twitter/android/TotpGeneratorActivity;->d:Landroid/widget/ProgressBar;

    const/16 v1, 0x1e

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 63
    iget-object v0, p0, Lcom/twitter/android/TotpGeneratorActivity;->e:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {p0}, Lcom/twitter/android/TotpGeneratorActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 65
    invoke-virtual {p0}, Lcom/twitter/android/TotpGeneratorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "TotpGeneratorActivity_account_id"

    invoke-virtual {p0}, Lcom/twitter/android/TotpGeneratorActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/TotpGeneratorActivity;->b:J

    .line 67
    iget-wide v0, p0, Lcom/twitter/android/TotpGeneratorActivity;->b:J

    invoke-static {p0, v0, v1}, Lbaw;->i(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TotpGeneratorActivity;->a:Ljava/lang/String;

    .line 68
    new-instance v0, Lbbd;

    invoke-virtual {p0}, Lcom/twitter/android/TotpGeneratorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/TotpGeneratorActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v2

    iget-wide v4, p0, Lcom/twitter/android/TotpGeneratorActivity;->b:J

    invoke-virtual {v2, v4, v5}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lbbd;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {p0, v0, v6}, Lcom/twitter/android/TotpGeneratorActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 71
    new-array v0, v6, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/TotpGeneratorActivity;->f:Lcom/twitter/ui/widget/TypefacesTextView;

    .line 72
    invoke-virtual {v2}, Lcom/twitter/ui/widget/TypefacesTextView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a0bce

    const v4, 0x7f1100c9

    const v5, 0x7f1100c8

    const-class v6, Lcom/twitter/android/WebViewActivity;

    invoke-static {v2, v3, v4, v5, v6}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;IIILjava/lang/Class;)Lcom/twitter/ui/view/a;

    move-result-object v2

    aput-object v2, v0, v1

    .line 77
    iget-object v1, p0, Lcom/twitter/android/TotpGeneratorActivity;->f:Lcom/twitter/ui/widget/TypefacesTextView;

    invoke-static {v1}, Lcom/twitter/ui/view/g;->a(Landroid/widget/TextView;)V

    .line 78
    iget-object v1, p0, Lcom/twitter/android/TotpGeneratorActivity;->f:Lcom/twitter/ui/widget/TypefacesTextView;

    iget-object v2, p0, Lcom/twitter/android/TotpGeneratorActivity;->f:Lcom/twitter/ui/widget/TypefacesTextView;

    invoke-virtual {v2}, Lcom/twitter/ui/widget/TypefacesTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "{{}}"

    invoke-static {v0, v2, v3}, Lcom/twitter/library/util/af;->a([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 111
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onPause()V

    .line 112
    iget-object v0, p0, Lcom/twitter/android/TotpGeneratorActivity;->h:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/TotpGeneratorActivity;->g:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 113
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 117
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onResume()V

    .line 118
    iget-object v0, p0, Lcom/twitter/android/TotpGeneratorActivity;->d:Landroid/widget/ProgressBar;

    invoke-static {}, Lcom/twitter/util/ab;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 119
    invoke-direct {p0}, Lcom/twitter/android/TotpGeneratorActivity;->i()V

    .line 121
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/TotpGeneratorActivity;->h:Landroid/os/Handler;

    .line 123
    new-instance v0, Lcom/twitter/android/TotpGeneratorActivity$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/TotpGeneratorActivity$1;-><init>(Lcom/twitter/android/TotpGeneratorActivity;)V

    iput-object v0, p0, Lcom/twitter/android/TotpGeneratorActivity;->g:Ljava/lang/Runnable;

    .line 136
    iget-object v0, p0, Lcom/twitter/android/TotpGeneratorActivity;->h:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/TotpGeneratorActivity;->g:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 137
    return-void
.end method
