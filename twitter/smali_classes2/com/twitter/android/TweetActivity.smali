.class public Lcom/twitter/android/TweetActivity;
.super Lcom/twitter/android/ActivityWithProgress;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lati;
.implements Lbrp;
.implements Lcom/twitter/app/common/dialog/b$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/TweetActivity$a;,
        Lcom/twitter/android/TweetActivity$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/ActivityWithProgress;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lati;",
        "Lbrp;",
        "Lcom/twitter/app/common/dialog/b$d;"
    }
.end annotation


# instance fields
.field private A:Z

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:Lcom/twitter/android/ar;

.field b:Lcom/twitter/library/client/Session;

.field c:Lcom/twitter/model/core/Tweet;

.field d:Lata;

.field e:Lcom/twitter/library/service/t;

.field private f:J

.field private g:Landroid/net/Uri;

.field private h:Ljava/lang/String;

.field private i:[Ljava/lang/String;

.field private j:Z

.field private k:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private l:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

.field private m:Laji;

.field private n:Lcom/twitter/android/media/selection/c;

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:J

.field private s:Landroid/net/Uri;

.field private t:Lcom/twitter/model/core/Tweet;

.field private u:Landroid/widget/RelativeLayout;

.field private v:Z

.field private w:Z

.field private x:Lcom/twitter/model/timeline/r;

.field private y:Latd;

.field private z:Latj;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 150
    invoke-direct {p0}, Lcom/twitter/android/ActivityWithProgress;-><init>()V

    .line 248
    iput-boolean v0, p0, Lcom/twitter/android/TweetActivity;->p:Z

    .line 249
    iput-boolean v0, p0, Lcom/twitter/android/TweetActivity;->q:Z

    .line 250
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/android/TweetActivity;->r:J

    .line 256
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/TweetActivity;->w:Z

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TweetActivity;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lcom/twitter/android/TweetActivity;->g:Landroid/net/Uri;

    return-object p1
.end method

.method private a(JLjava/util/List;Ljava/util/Set;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 918
    new-instance v1, Lbfn;

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    move-object v2, p0

    move-wide v4, p1

    move-object v8, p3

    move-object v9, p4

    invoke-direct/range {v1 .. v9}, Lbfn;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJLjava/util/List;Ljava/util/Set;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/TweetActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 920
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TweetActivity;Lcom/twitter/model/core/Tweet;)V
    .locals 0

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lcom/twitter/android/TweetActivity;->b(Lcom/twitter/model/core/Tweet;)V

    return-void
.end method

.method private a(Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 6

    .prologue
    .line 1177
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 1178
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1179
    const v1, 0x7f1308c8

    invoke-virtual {p1, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v1

    .line 1180
    if-eqz v1, :cond_0

    .line 1182
    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v2, :cond_1

    .line 1183
    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    sget-object v3, Lcom/twitter/util/math/Size;->b:Lcom/twitter/util/math/Size;

    .line 1184
    invoke-static {v2, v3}, Lcom/twitter/model/util/c;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/util/math/Size;)Ljava/util/List;

    move-result-object v2

    .line 1185
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/twitter/model/util/d;->a(Ljava/util/List;J)Z

    move-result v0

    .line 1189
    :goto_0
    invoke-virtual {v1, v0}, Lazv;->b(Z)Lazv;

    .line 1192
    :cond_0
    return-void

    .line 1187
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;III)V
    .locals 2

    .prologue
    .line 543
    invoke-virtual {p0, p2}, Lcom/twitter/android/TweetActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, Lcom/twitter/android/TweetActivity;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 545
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->u:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/twitter/android/TweetActivity$4;

    invoke-direct {v1, p0, p1, p4}, Lcom/twitter/android/TweetActivity$4;-><init>(Lcom/twitter/android/TweetActivity;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 576
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 606
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "tweet:notification_landing"

    aput-object v2, v1, v4

    aput-object p1, v1, v5

    const/4 v2, 0x2

    const-string/jumbo v3, ":impression"

    aput-object v3, v1, v2

    .line 607
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 606
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 609
    iput-boolean v5, p0, Lcom/twitter/android/TweetActivity;->p:Z

    .line 610
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->u:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 612
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->u:Landroid/widget/RelativeLayout;

    const v1, 0x7f13086b

    .line 613
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    .line 614
    invoke-virtual {v0, p2}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    .line 616
    const v0, 0x7f13086a

    .line 617
    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 618
    invoke-virtual {v0, p3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 619
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TweetActivity;)Z
    .locals 1

    .prologue
    .line 150
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->t()Z

    move-result v0

    return v0
.end method

.method private b(Lcom/twitter/internal/android/widget/ToolBar;)Lazv;
    .locals 2

    .prologue
    .line 1510
    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->w:Z

    if-eqz v0, :cond_1

    .line 1511
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1512
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->D:Lcom/twitter/android/ar;

    invoke-virtual {v1, p1, v0}, Lcom/twitter/android/ar;->a(Lcom/twitter/internal/android/widget/ToolBar;Z)Lazv;

    move-result-object v0

    .line 1514
    :goto_1
    return-object v0

    .line 1511
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1514
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->D:Lcom/twitter/android/ar;

    invoke-virtual {v0, p1}, Lcom/twitter/android/ar;->a(Lcom/twitter/internal/android/widget/ToolBar;)Lazv;

    move-result-object v0

    goto :goto_1
.end method

.method static synthetic b(Lcom/twitter/android/TweetActivity;)V
    .locals 0

    .prologue
    .line 150
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->s()V

    return-void
.end method

.method private b(Lcom/twitter/model/core/Tweet;)V
    .locals 2

    .prologue
    .line 639
    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 640
    invoke-direct {p0, p1}, Lcom/twitter/android/TweetActivity;->c(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/drafts/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->a(Lcom/twitter/model/drafts/a;)Lcom/twitter/android/composer/a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->y:Latd;

    .line 641
    invoke-interface {v1}, Latd;->aK_()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->a(Z)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 642
    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/a;->a(Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/composer/a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    .line 643
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->b(Ljava/lang/String;)Lcom/twitter/android/composer/a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->y:Latd;

    .line 644
    invoke-interface {v1}, Latd;->l()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->a([I)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 645
    invoke-virtual {v0, p0}, Lcom/twitter/android/composer/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 646
    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->y:Latd;

    invoke-interface {v1}, Latd;->j()V

    .line 647
    const/16 v1, 0x66

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/TweetActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 648
    return-void
.end method

.method private static b(Landroid/net/Uri;)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 1240
    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    .line 1243
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1244
    const-string/jumbo v1, "status_groups_retweets_view"

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1245
    sget-object v0, Lbuj;->b:[Ljava/lang/String;

    .line 1251
    :goto_0
    return-object v0

    .line 1248
    :cond_0
    new-instance v0, Lcpb;

    invoke-direct {v0}, Lcpb;-><init>()V

    const-string/jumbo v1, "activity.uri"

    .line 1249
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Partial content requested"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 1250
    invoke-virtual {v0, v1}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    move-result-object v0

    .line 1248
    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    .line 1251
    sget-object v0, Lbuj;->a:[Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/TweetActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private c(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/drafts/a;
    .locals 4

    .prologue
    .line 1480
    new-instance v0, Lcom/twitter/model/drafts/a$a;

    invoke-direct {v0}, Lcom/twitter/model/drafts/a$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->y:Latd;

    .line 1481
    invoke-interface {v1}, Latd;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->a(Ljava/lang/String;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->y:Latd;

    .line 1482
    invoke-interface {v1}, Latd;->h()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->a(Ljava/util/List;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 1483
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->a(Lcgi;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->y:Latd;

    .line 1484
    invoke-interface {v1}, Latd;->f()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->c(Ljava/util/List;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 1485
    invoke-static {p1}, Lbpj;->a(Lcom/twitter/model/core/Tweet;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/drafts/a$a;->b(J)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->y:Latd;

    .line 1486
    invoke-interface {v1}, Latd;->m()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->a(Z)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 1487
    invoke-virtual {v0}, Lcom/twitter/model/drafts/a$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/a;

    .line 1480
    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/TweetActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private d(Lcom/twitter/model/core/Tweet;)V
    .locals 4

    .prologue
    .line 1525
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lata;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->m:Laji;

    iget-boolean v3, p0, Lcom/twitter/android/TweetActivity;->p:Z

    invoke-interface {v0, p1, v1, v2, v3}, Lata;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/library/client/Session;Laji;Z)V

    .line 1526
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/TweetActivity;->v:Z

    .line 1527
    iput-object p1, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    .line 1528
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->h()V

    .line 1529
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->y:Latd;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-interface {v0, v1}, Latd;->a(Lcom/twitter/model/core/Tweet;)V

    .line 1530
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->l()V

    .line 1531
    return-void
.end method

.method static synthetic e(Lcom/twitter/android/TweetActivity;)Lcom/twitter/model/core/Tweet;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->t:Lcom/twitter/model/core/Tweet;

    return-object v0
.end method

.method private i()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 497
    const-string/jumbo v0, "vit_show_push_notif_context_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 498
    iput-boolean v1, p0, Lcom/twitter/android/TweetActivity;->p:Z

    .line 539
    :goto_0
    return-void

    .line 501
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 502
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 504
    invoke-static {v3}, Lcom/twitter/android/util/ai;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 505
    invoke-static {v4}, Lcom/twitter/android/util/ai;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 506
    iput-boolean v1, p0, Lcom/twitter/android/TweetActivity;->p:Z

    goto :goto_0

    .line 509
    :cond_1
    const v0, 0x7f130817

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->u:Landroid/widget/RelativeLayout;

    .line 511
    const/4 v0, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_2
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 534
    iput-boolean v1, p0, Lcom/twitter/android/TweetActivity;->p:Z

    .line 535
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->u:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 511
    :sswitch_0
    const-string/jumbo v5, "r"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v0, v1

    goto :goto_1

    :sswitch_1
    const-string/jumbo v5, "f"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v0, v2

    goto :goto_1

    :sswitch_2
    const-string/jumbo v5, "e"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v0, 0x2

    goto :goto_1

    .line 513
    :pswitch_0
    const-string/jumbo v0, "vit_retweet_spike"

    const v1, 0x7f0a0a33

    const v2, 0x7f020267

    const/16 v3, 0xc

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/twitter/android/TweetActivity;->a(Ljava/lang/String;III)V

    goto :goto_0

    .line 520
    :pswitch_1
    const-string/jumbo v0, "vit_favorite_spike"

    const v1, 0x7f0a0a2a

    const v2, 0x7f020266

    const/16 v3, 0xb

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/twitter/android/TweetActivity;->a(Ljava/lang/String;III)V

    goto :goto_0

    .line 526
    :pswitch_2
    invoke-static {v3}, Lcom/twitter/android/util/ai;->a(Landroid/net/Uri;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/TweetActivity;->r:J

    .line 527
    iget-wide v0, p0, Lcom/twitter/android/TweetActivity;->r:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 528
    iput-boolean v2, p0, Lcom/twitter/android/TweetActivity;->p:Z

    .line 530
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->u:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 511
    nop

    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_2
        0x66 -> :sswitch_1
        0x72 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private j()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 579
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->t:Lcom/twitter/model/core/Tweet;

    if-nez v0, :cond_0

    .line 580
    iput-boolean v4, p0, Lcom/twitter/android/TweetActivity;->p:Z

    .line 599
    :goto_0
    return-void

    .line 584
    :cond_0
    const-string/jumbo v0, "vit_tweet_embedded"

    const v1, 0x7f0a0a34

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->t:Lcom/twitter/model/core/Tweet;

    iget-object v3, v3, Lcom/twitter/model/core/Tweet;->z:Ljava/lang/String;

    aput-object v3, v2, v4

    .line 585
    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/TweetActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f020852

    .line 584
    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/TweetActivity;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 589
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->u:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/twitter/android/TweetActivity$5;

    invoke-direct {v1, p0}, Lcom/twitter/android/TweetActivity$5;-><init>(Lcom/twitter/android/TweetActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 651
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 659
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 660
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->y:Latd;

    invoke-interface {v0}, Latd;->k()V

    .line 662
    :cond_0
    return-void
.end method

.method private n()V
    .locals 4

    .prologue
    .line 1111
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "tweet"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    .line 1112
    invoke-static {v3}, Lcom/twitter/model/core/Tweet;->b(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "block_dialog"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "block"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1111
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1113
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->o()V

    .line 1114
    return-void
.end method

.method private o()V
    .locals 8

    .prologue
    .line 1117
    new-instance v1, Lbes;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v0, Lcom/twitter/model/core/Tweet;->s:J

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    .line 1118
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v6

    const/4 v7, 0x1

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lbes;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;I)V

    const/4 v0, 0x4

    .line 1117
    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/TweetActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 1120
    return-void
.end method

.method private q()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1124
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_1

    .line 1127
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->u:J

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/database/schema/a;->a(JJ)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->g:Landroid/net/Uri;

    .line 1128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/TweetActivity;->v:Z

    .line 1129
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v5, v4, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 1169
    :cond_0
    :goto_0
    return-void

    .line 1131
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 1133
    if-eqz v1, :cond_6

    .line 1134
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1135
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 1136
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    .line 1137
    const-string/jumbo v3, "twitter"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1139
    :try_start_0
    const-string/jumbo v0, "status_id"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1140
    if-nez v0, :cond_2

    .line 1141
    const-string/jumbo v0, "id"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1144
    :cond_2
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 1145
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_3

    .line 1146
    new-instance v0, Ljava/lang/NumberFormatException;

    invoke-direct {v0}, Ljava/lang/NumberFormatException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1156
    :catch_0
    move-exception v0

    .line 1157
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->r()V

    goto :goto_0

    .line 1148
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/database/schema/a;->a(JJ)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->g:Landroid/net/Uri;

    .line 1149
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 1150
    iget-wide v0, p0, Lcom/twitter/android/TweetActivity;->r:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1151
    iget-wide v0, p0, Lcom/twitter/android/TweetActivity;->r:J

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    .line 1152
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 1151
    invoke-static {v0, v1, v2, v3}, Lcom/twitter/database/schema/a;->a(JJ)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->s:Landroid/net/Uri;

    .line 1153
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1159
    :cond_4
    const-string/jumbo v2, "vnd.android.cursor.item/vnd.twitter.android.statuses"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1160
    iput-object v1, p0, Lcom/twitter/android/TweetActivity;->g:Landroid/net/Uri;

    .line 1161
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v5, v4, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto/16 :goto_0

    .line 1163
    :cond_5
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->r()V

    goto/16 :goto_0

    .line 1166
    :cond_6
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->r()V

    goto/16 :goto_0
.end method

.method private r()V
    .locals 2

    .prologue
    .line 1172
    const v0, 0x7f0a0999

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1173
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->finish()V

    .line 1174
    return-void
.end method

.method private s()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1453
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-nez v0, :cond_0

    .line 1456
    const v0, 0x7f0a0999

    invoke-static {p0, v0, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1477
    :goto_0
    return-void

    .line 1460
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->m_()V

    .line 1461
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetActivity;->c(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/drafts/a;

    move-result-object v1

    .line 1462
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-static {v0, v2, v1}, Lcom/twitter/android/client/x;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/drafts/a;)Ljava/lang/String;

    .line 1463
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lata;

    invoke-interface {v0}, Lata;->n()V

    .line 1465
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 1466
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 1467
    invoke-static {v0}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    .line 1468
    new-array v4, v9, [Ljava/lang/String;

    const-string/jumbo v5, "tweet:composition:::send_reply"

    aput-object v5, v4, v8

    invoke-virtual {v0, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1469
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    iget-object v4, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget-object v5, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    .line 1470
    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    const/4 v5, 0x0

    invoke-static {v4, v6, v7, v5}, Lbxd;->a(Lcom/twitter/model/core/Tweet;JLjava/util/Collection;)Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    new-array v4, v9, [Ljava/lang/String;

    const-string/jumbo v5, "tweet:composition:::num_recipients"

    aput-object v5, v4, v8

    .line 1471
    invoke-virtual {v0, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1469
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1472
    sget-object v0, Lcom/twitter/android/composer/ComposerType;->b:Lcom/twitter/android/composer/ComposerType;

    iget-object v4, v1, Lcom/twitter/model/drafts/a;->d:Ljava/util/List;

    invoke-static {v2, v3, v0, v4}, Lcom/twitter/android/composer/p;->a(JLcom/twitter/android/composer/ComposerType;Ljava/util/List;)V

    .line 1473
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-static {v1, v0, v2, v3, v8}, Lcom/twitter/android/composer/p;->a(Lcom/twitter/model/drafts/a;Lcom/twitter/model/core/Tweet;JZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1474
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->l:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    const-string/jumbo v1, "tweet"

    invoke-static {v0, v2, v3, v1}, Lcom/twitter/android/composer/p;->a(Lcom/twitter/analytics/model/ScribeItem;JLjava/lang/String;)V

    .line 1476
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->y:Latd;

    invoke-interface {v0}, Latd;->j()V

    goto :goto_0
.end method

.method private t()Z
    .locals 1

    .prologue
    .line 1491
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ao()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private u()V
    .locals 4

    .prologue
    .line 1495
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    .line 1496
    new-instance v0, Lbfa;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-direct {v0, p0, v1}, Lbfa;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v1, Lcom/twitter/model/core/Tweet;->s:J

    invoke-virtual {v0, v2, v3}, Lbfa;->a(J)Lbeq;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/TweetActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 1499
    :cond_0
    return-void
.end method

.method private v()V
    .locals 4

    .prologue
    .line 1502
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    .line 1503
    new-instance v0, Lbff;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-direct {v0, p0, v1}, Lbff;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v1, Lcom/twitter/model/core/Tweet;->s:J

    invoke-virtual {v0, v2, v3}, Lbff;->a(J)Lbeq;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/TweetActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 1506
    :cond_0
    return-void
.end method

.method private w()V
    .locals 5

    .prologue
    .line 1534
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 1535
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->x()Lcom/twitter/app/common/base/BaseFragment;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 1536
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/TweetActivity;->w:Z

    .line 1537
    const-string/jumbo v0, "tweet_blocked_by_author_fragment"

    .line 1538
    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/TweetBlockedByAuthorFragment;

    .line 1539
    if-nez v0, :cond_0

    .line 1540
    new-instance v0, Lcom/twitter/android/TweetBlockedByAuthorFragment;

    invoke-direct {v0}, Lcom/twitter/android/TweetBlockedByAuthorFragment;-><init>()V

    .line 1541
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    .line 1542
    const v3, 0x7f1302e4

    const-string/jumbo v4, "tweet_blocked_by_author_fragment"

    invoke-virtual {v2, v3, v0, v4}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 1543
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 1545
    :cond_0
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->x()Lcom/twitter/app/common/base/BaseFragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 1546
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->h()V

    .line 1547
    return-void
.end method

.method private x()Lcom/twitter/app/common/base/BaseFragment;
    .locals 1

    .prologue
    .line 1551
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lata;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/base/BaseFragment;

    return-object v0
.end method

.method private static y()Lcom/twitter/app/common/list/TwitterListFragment;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<F:",
            "Lcom/twitter/app/common/list/TwitterListFragment;",
            ":",
            "Lata;",
            ">()TF;"
        }
    .end annotation

    .prologue
    .line 1555
    new-instance v0, Lcom/twitter/android/TweetFragment;

    invoke-direct {v0}, Lcom/twitter/android/TweetFragment;-><init>()V

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/TwitterListFragment;

    return-object v0
.end method

.method private z()V
    .locals 3

    .prologue
    .line 1559
    const-string/jumbo v0, "urt_conv:focal:complete"

    .line 1560
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->O()Lcom/twitter/metrics/j;

    move-result-object v1

    sget-object v2, Lcom/twitter/metrics/g;->n:Lcom/twitter/metrics/g$b;

    .line 1559
    invoke-static {v0, v1, v2}, Lcom/twitter/metrics/e;->a(Ljava/lang/String;Lcom/twitter/metrics/j;Lcom/twitter/metrics/g$b;)Lcom/twitter/metrics/e;

    move-result-object v0

    .line 1560
    invoke-virtual {v0}, Lcom/twitter/metrics/e;->i()V

    .line 1561
    const-string/jumbo v0, "urt_conv:complete"

    .line 1562
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->O()Lcom/twitter/metrics/j;

    move-result-object v1

    sget-object v2, Lcom/twitter/metrics/g;->n:Lcom/twitter/metrics/g$b;

    .line 1561
    invoke-static {v0, v1, v2}, Lcom/twitter/metrics/e;->a(Ljava/lang/String;Lcom/twitter/metrics/j;Lcom/twitter/metrics/g$b;)Lcom/twitter/metrics/e;

    move-result-object v0

    .line 1562
    invoke-virtual {v0}, Lcom/twitter/metrics/e;->i()V

    .line 1563
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Laog;
    .locals 9

    .prologue
    .line 419
    new-instance v0, Lcom/twitter/android/media/selection/c;

    const-string/jumbo v3, "reply_composition"

    sget-object v4, Lcom/twitter/media/model/MediaType;->h:Ljava/util/EnumSet;

    const/4 v5, 0x1

    sget-object v6, Lcom/twitter/android/composer/ComposerType;->b:Lcom/twitter/android/composer/ComposerType;

    .line 426
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v7

    move-object v1, p0

    move-object v2, p0

    move-object v8, p0

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/media/selection/c;-><init>(Landroid/content/Context;Lbrp;Ljava/lang/String;Ljava/util/EnumSet;ILcom/twitter/android/composer/ComposerType;Lcom/twitter/library/client/Session;Lcom/twitter/app/common/util/j;)V

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->n:Lcom/twitter/android/media/selection/c;

    .line 429
    new-instance v0, Lato;

    .line 430
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->V()Lcom/twitter/app/common/base/d;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/TweetActivity;->n:Lcom/twitter/android/media/selection/c;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lato;-><init>(Landroid/support/v4/app/FragmentActivity;Landroid/view/LayoutInflater;Landroid/os/Bundle;Lcom/twitter/app/common/base/d;Lcom/twitter/android/media/selection/c;)V

    .line 431
    invoke-virtual {v0}, Lato;->f()Latd;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/TweetActivity;->y:Latd;

    .line 432
    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->y:Latd;

    new-instance v2, Lcom/twitter/android/TweetActivity$3;

    invoke-direct {v2, p0}, Lcom/twitter/android/TweetActivity$3;-><init>(Lcom/twitter/android/TweetActivity;)V

    invoke-interface {v1, v2}, Latd;->a(Latg$a;)V

    .line 493
    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 272
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ActivityWithProgress;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    .line 273
    invoke-virtual {v0, v2}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 274
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 275
    invoke-virtual {v0, v2}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(Z)V

    .line 276
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(I)V

    .line 279
    :cond_0
    const/16 v1, 0x4c

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(I)V

    .line 281
    return-object v0
.end method

.method protected synthetic a(Lank;)Lcom/twitter/app/common/base/j;
    .locals 1

    .prologue
    .line 150
    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetActivity;->d(Lank;)Latp;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 10

    .prologue
    const/4 v4, 0x2

    const/4 v9, 0x5

    const/4 v7, 0x3

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 1036
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-static {v0}, Lcom/twitter/model/core/Tweet;->b(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v0

    .line 1037
    packed-switch p2, :pswitch_data_0

    .line 1107
    :cond_0
    :goto_0
    return-void

    .line 1039
    :pswitch_0
    const/4 v0, -0x1

    if-ne p3, v0, :cond_1

    .line 1040
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetActivity;->c(Lcom/twitter/model/core/Tweet;)Lcom/twitter/model/drafts/a;

    move-result-object v0

    .line 1042
    new-instance v1, Lcom/twitter/android/composer/y;

    .line 1043
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-direct {v1, v2, v3, v0, v6}, Lcom/twitter/android/composer/y;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/drafts/a;Z)V

    .line 1044
    sget-object v0, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;->g:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/y;->a(Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)V

    .line 1045
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->G:Lcom/twitter/library/client/p;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 1046
    invoke-virtual {p0, v6}, Lcom/twitter/android/TweetActivity;->setResult(I)V

    .line 1047
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v9, [Ljava/lang/String;

    const-string/jumbo v2, "tweet"

    aput-object v2, v1, v6

    const-string/jumbo v2, "composition"

    aput-object v2, v1, v8

    const-string/jumbo v2, "cancel_reply_sheet"

    aput-object v2, v1, v4

    const-string/jumbo v2, "save_draft"

    aput-object v2, v1, v7

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    .line 1048
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1047
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1049
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->finish()V

    goto :goto_0

    .line 1050
    :cond_1
    const/4 v0, -0x2

    if-ne p3, v0, :cond_0

    .line 1051
    invoke-virtual {p0, v6}, Lcom/twitter/android/TweetActivity;->setResult(I)V

    .line 1052
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v9, [Ljava/lang/String;

    const-string/jumbo v2, "tweet"

    aput-object v2, v1, v6

    const-string/jumbo v2, "composition"

    aput-object v2, v1, v8

    const-string/jumbo v2, "cancel_reply_sheet"

    aput-object v2, v1, v4

    const-string/jumbo v2, "dont_save"

    aput-object v2, v1, v7

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    .line 1053
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1052
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1054
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->finish()V

    goto/16 :goto_0

    .line 1059
    :pswitch_1
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    .line 1060
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    const v1, 0x7f1308c8

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    invoke-virtual {v0, v6}, Lazv;->b(Z)Lazv;

    .line 1062
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->G:J

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    .line 1063
    invoke-virtual {v2}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/model/core/v;->d:Lcom/twitter/model/core/k;

    invoke-static {v2}, Lcom/twitter/model/util/c;->k(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    .line 1064
    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v3

    .line 1062
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/twitter/android/TweetActivity;->a(JLjava/util/List;Ljava/util/Set;)V

    .line 1065
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v8, [Ljava/lang/String;

    const-string/jumbo v2, "tweet::tweet:remove_my_media_tag:click"

    aput-object v2, v1, v6

    .line 1066
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1065
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 1071
    :pswitch_2
    const/4 v1, -0x1

    if-ne p3, v1, :cond_2

    .line 1072
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v9, [Ljava/lang/String;

    const-string/jumbo v3, "tweet"

    aput-object v3, v2, v6

    const/4 v3, 0x0

    aput-object v3, v2, v8

    aput-object v0, v2, v4

    const-string/jumbo v0, "mute_dialog"

    aput-object v0, v2, v7

    const/4 v0, 0x4

    const-string/jumbo v3, "mute_user"

    aput-object v3, v2, v0

    .line 1073
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1072
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1074
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->u()V

    goto/16 :goto_0

    .line 1076
    :cond_2
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v9, [Ljava/lang/String;

    const-string/jumbo v3, "tweet"

    aput-object v3, v2, v6

    const/4 v3, 0x0

    aput-object v3, v2, v8

    aput-object v0, v2, v4

    const-string/jumbo v0, "mute_dialog"

    aput-object v0, v2, v7

    const/4 v0, 0x4

    const-string/jumbo v3, "cancel"

    aput-object v3, v2, v0

    .line 1077
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1076
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 1082
    :pswitch_3
    const/4 v1, -0x1

    if-ne p3, v1, :cond_3

    .line 1083
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->n()V

    goto/16 :goto_0

    .line 1085
    :cond_3
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v9, [Ljava/lang/String;

    const-string/jumbo v3, "tweet"

    aput-object v3, v2, v6

    const/4 v3, 0x0

    aput-object v3, v2, v8

    aput-object v0, v2, v4

    const-string/jumbo v0, "block_dialog"

    aput-object v0, v2, v7

    const/4 v0, 0x4

    const-string/jumbo v3, "cancel"

    aput-object v3, v2, v0

    .line 1086
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1085
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 1091
    :pswitch_4
    const/4 v1, -0x1

    if-ne p3, v1, :cond_4

    .line 1092
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v9, [Ljava/lang/String;

    const-string/jumbo v3, "tweet"

    aput-object v3, v2, v6

    const/4 v3, 0x0

    aput-object v3, v2, v8

    aput-object v0, v2, v4

    const-string/jumbo v0, "unblock_dialog"

    aput-object v0, v2, v7

    const/4 v0, 0x4

    const-string/jumbo v3, "unblock"

    aput-object v3, v2, v0

    .line 1093
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1092
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1094
    new-instance v1, Lbes;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget-wide v4, v0, Lcom/twitter/model/core/Tweet;->s:J

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    .line 1095
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v6

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lbes;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;I)V

    .line 1094
    invoke-virtual {p0, v1, v9}, Lcom/twitter/android/TweetActivity;->b(Lcom/twitter/library/service/s;I)Z

    goto/16 :goto_0

    .line 1098
    :cond_4
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v9, [Ljava/lang/String;

    const-string/jumbo v3, "tweet"

    aput-object v3, v2, v6

    const/4 v3, 0x0

    aput-object v3, v2, v8

    aput-object v0, v2, v4

    const-string/jumbo v0, "unblock_dialog"

    aput-object v0, v2, v7

    const/4 v0, 0x4

    const-string/jumbo v3, "cancel"

    aput-object v3, v2, v0

    .line 1099
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1098
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 1037
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public a(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1521
    invoke-static {p0, p1, p2, p3}, Landroid/support/v4/app/ActivityCompat;->startActivityForResult(Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 1522
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const v8, 0x7f0a0999

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1258
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v1

    if-nez v1, :cond_8

    .line 1259
    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/twitter/android/TweetActivity;->v:Z

    if-eqz v1, :cond_6

    .line 1261
    :cond_0
    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1262
    sget-object v1, Lbtc;->a:Lbtc;

    invoke-virtual {v1, p2}, Lbtc;->a(Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v1

    .line 1263
    iget-wide v2, v1, Lcom/twitter/model/core/Tweet$a;->n:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_b

    iget-object v2, v1, Lcom/twitter/model/core/Tweet$a;->p:Ljava/lang/String;

    invoke-static {v2}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1264
    new-instance v2, Lcpb;

    invoke-direct {v2}, Lcpb;-><init>()V

    const-string/jumbo v3, "tweet.statusId"

    iget-wide v4, v1, Lcom/twitter/model/core/Tweet$a;->o:J

    .line 1265
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v2

    const-string/jumbo v3, "tweet.groupType"

    iget v4, v1, Lcom/twitter/model/core/Tweet$a;->I:I

    .line 1266
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v2

    const-string/jumbo v3, "tweet.content"

    iget-object v1, v1, Lcom/twitter/model/core/Tweet$a;->a:Ljava/lang/String;

    .line 1267
    invoke-virtual {v2, v3, v1}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v1

    const-string/jumbo v2, "activity.uri"

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->g:Landroid/net/Uri;

    .line 1268
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v1

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "UserId of tweet is 0 and UserName is empty"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 1269
    invoke-virtual {v1, v2}, Lcpb;->a(Ljava/lang/Throwable;)Lcpb;

    move-result-object v1

    .line 1264
    invoke-static {v1}, Lcpd;->c(Lcpb;)V

    .line 1274
    :cond_1
    :goto_0
    if-eqz v0, :cond_3

    .line 1275
    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->x:Lcom/twitter/model/timeline/r;

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/Tweet$a;->a(Lcom/twitter/model/timeline/r;)Lcom/twitter/model/core/Tweet$a;

    .line 1276
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet$a;->a()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    .line 1277
    invoke-direct {p0, v0}, Lcom/twitter/android/TweetActivity;->d(Lcom/twitter/model/core/Tweet;)V

    .line 1328
    :cond_2
    :goto_1
    return-void

    .line 1279
    :cond_3
    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->j:Z

    if-nez v0, :cond_4

    .line 1280
    new-instance v0, Lbga;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->g:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    invoke-direct {v0, p0, v1, v2, v3}, Lbga;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    invoke-virtual {p0, v0, v7}, Lcom/twitter/android/TweetActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 1281
    iput-boolean v6, p0, Lcom/twitter/android/TweetActivity;->j:Z

    goto :goto_1

    .line 1283
    :cond_4
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-nez v0, :cond_5

    .line 1285
    invoke-static {p0, v8, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1286
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->finish()V

    goto :goto_1

    .line 1288
    :cond_5
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetActivity;->d(Lcom/twitter/model/core/Tweet;)V

    goto :goto_1

    .line 1295
    :cond_6
    if-eqz p2, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1296
    sget-object v0, Lbtc;->a:Lbtc;

    invoke-virtual {v0, p2}, Lbtc;->a(Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->x:Lcom/twitter/model/timeline/r;

    .line 1297
    invoke-virtual {v0, v1}, Lcom/twitter/model/core/Tweet$a;->a(Lcom/twitter/model/timeline/r;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    .line 1298
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet$a;->a()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    .line 1299
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lata;

    if-eqz v0, :cond_7

    .line 1300
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lata;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-interface {v0, v1}, Lata;->a(Lcom/twitter/model/core/Tweet;)V

    .line 1302
    :cond_7
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    .line 1303
    if-eqz v0, :cond_2

    .line 1304
    invoke-direct {p0, v0}, Lcom/twitter/android/TweetActivity;->b(Lcom/twitter/internal/android/widget/ToolBar;)Lazv;

    goto :goto_1

    .line 1308
    :cond_8
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 1309
    if-eqz p2, :cond_9

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1310
    sget-object v0, Lbtd;->a:Lbtd;

    invoke-virtual {v0, p2}, Lbtd;->a(Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->t:Lcom/twitter/model/core/Tweet;

    .line 1311
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->j()V

    goto :goto_1

    .line 1313
    :cond_9
    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->q:Z

    if-nez v0, :cond_a

    .line 1314
    iput-boolean v6, p0, Lcom/twitter/android/TweetActivity;->q:Z

    .line 1315
    new-instance v0, Lbga;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->s:Landroid/net/Uri;

    .line 1316
    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    invoke-direct {v0, p0, v1, v2, v3}, Lbga;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    const/4 v1, 0x6

    .line 1315
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/TweetActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 1317
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "tweet:notification_landing:vit_tweet_embedded::loading"

    aput-object v2, v1, v7

    .line 1318
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1317
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_1

    .line 1320
    :cond_a
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "tweet:notification_landing:vit_tweet_embedded::error"

    aput-object v2, v1, v7

    .line 1321
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1320
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1322
    invoke-static {p0, v8, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 1323
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1324
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->finish()V

    goto/16 :goto_1

    :cond_b
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public a(Lcom/twitter/library/service/s;I)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const v4, 0x7f0a0999

    const/4 v2, 0x4

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1337
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ActivityWithProgress;->a(Lcom/twitter/library/service/s;I)V

    .line 1338
    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->o:Z

    if-nez v0, :cond_1

    .line 1426
    :cond_0
    :goto_0
    return-void

    .line 1341
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 1342
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 1344
    :pswitch_0
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1345
    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->w:Z

    if-nez v0, :cond_3

    .line 1346
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 1348
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->x()Lcom/twitter/app/common/base/BaseFragment;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 1352
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v2, "tweet_blocked_by_author_fragment"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/TweetBlockedByAuthorFragment;

    .line 1353
    if-eqz v0, :cond_2

    .line 1354
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 1356
    :cond_2
    iput-boolean v6, p0, Lcom/twitter/android/TweetActivity;->w:Z

    .line 1358
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v7, v3, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0

    .line 1359
    :cond_4
    const/16 v1, 0x88

    invoke-static {v0, v1}, Lcom/twitter/library/network/ab;->a(Lcom/twitter/library/service/u;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1360
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->w()V

    goto :goto_0

    .line 1363
    :cond_5
    invoke-static {p0, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 1364
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1365
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->finish()V

    goto :goto_0

    .line 1370
    :pswitch_1
    check-cast p1, Lbfn;

    .line 1371
    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget-wide v2, v1, Lcom/twitter/model/core/Tweet;->G:J

    invoke-virtual {p1}, Lbfn;->b()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 1373
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1374
    const v0, 0x7f0a052a

    .line 1379
    :goto_1
    invoke-static {p0, v0, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1376
    :cond_6
    const v0, 0x7f0a0529

    .line 1377
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v1

    const v2, 0x7f1308c8

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v1

    invoke-virtual {v1, v6}, Lazv;->b(Z)Lazv;

    goto :goto_1

    .line 1384
    :pswitch_2
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1385
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->finish()V

    goto/16 :goto_0

    .line 1390
    :pswitch_3
    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1391
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget v1, v1, Lcom/twitter/model/core/Tweet;->l:I

    const/16 v2, 0x2000

    invoke-static {v1, v2}, Lcom/twitter/model/core/g;->b(II)I

    move-result v1

    iput v1, v0, Lcom/twitter/model/core/Tweet;->l:I

    .line 1392
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->h()V

    goto/16 :goto_0

    .line 1397
    :pswitch_4
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1398
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->finish()V

    goto/16 :goto_0

    .line 1403
    :pswitch_5
    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1404
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget v1, v1, Lcom/twitter/model/core/Tweet;->l:I

    invoke-static {v1, v2}, Lcom/twitter/model/core/g;->b(II)I

    move-result v1

    iput v1, v0, Lcom/twitter/model/core/Tweet;->l:I

    .line 1406
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->h()V

    goto/16 :goto_0

    .line 1411
    :pswitch_6
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1412
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v2, v3, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto/16 :goto_0

    .line 1414
    :cond_7
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "tweet:notification_landing:vit_tweet_embedded::error"

    aput-object v2, v1, v7

    .line 1415
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1414
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1416
    invoke-static {p0, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 1417
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1418
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->finish()V

    goto/16 :goto_0

    .line 1342
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public a(Lcom/twitter/model/core/Tweet;)V
    .locals 1

    .prologue
    .line 623
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 624
    invoke-direct {p0, p1}, Lcom/twitter/android/TweetActivity;->b(Lcom/twitter/model/core/Tweet;)V

    .line 626
    :cond_0
    return-void
.end method

.method public a(Lcmm;)Z
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v3, 0x3

    const/4 v10, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 809
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-nez v0, :cond_1

    .line 810
    invoke-super {p0, p1}, Lcom/twitter/android/ActivityWithProgress;->a(Lcmm;)Z

    move-result v6

    .line 914
    :cond_0
    :goto_0
    return v6

    .line 813
    :cond_1
    invoke-interface {p1}, Lcmm;->a()I

    move-result v0

    .line 814
    const v1, 0x7f130043

    if-ne v0, v1, :cond_2

    .line 816
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->y:Latd;

    invoke-interface {v0}, Latd;->aK_()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 817
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->b()V

    goto :goto_0

    .line 820
    :cond_2
    const v1, 0x7f1308c8

    if-ne v0, v1, :cond_4

    .line 821
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    invoke-direct {v0, v10}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a075b

    .line 822
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0616

    .line 823
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a00f6

    .line 824
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 825
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 826
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 827
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 914
    :cond_3
    :goto_1
    invoke-super {p0, p1}, Lcom/twitter/android/ActivityWithProgress;->a(Lcmm;)Z

    move-result v6

    goto :goto_0

    .line 828
    :cond_4
    const v1, 0x7f13088e

    if-ne v0, v1, :cond_5

    .line 829
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lata;

    invoke-interface {v0}, Lata;->t()V

    goto :goto_1

    .line 830
    :cond_5
    const v1, 0x7f1308b3

    if-ne v0, v1, :cond_7

    .line 831
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget v0, v0, Lcom/twitter/model/core/Tweet;->l:I

    invoke-static {v0}, Lcom/twitter/model/core/g;->d(I)Z

    move-result v0

    .line 833
    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->V()Ljava/lang/String;

    move-result-object v8

    .line 834
    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget-object v1, v1, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    .line 835
    if-eqz v0, :cond_6

    .line 836
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "tweet"

    aput-object v2, v1, v7

    const/4 v2, 0x0

    aput-object v2, v1, v6

    aput-object v8, v1, v10

    const-string/jumbo v2, "unmute_dialog"

    aput-object v2, v1, v3

    const-string/jumbo v2, "open"

    aput-object v2, v1, v11

    .line 837
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 836
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 838
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "tweet"

    aput-object v2, v1, v7

    const/4 v2, 0x0

    aput-object v2, v1, v6

    aput-object v8, v1, v10

    const-string/jumbo v2, "unmute_dialog"

    aput-object v2, v1, v3

    const-string/jumbo v2, "unmute_user"

    aput-object v2, v1, v11

    .line 839
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 838
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 840
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->v()V

    goto/16 :goto_0

    .line 842
    :cond_6
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v4, "tweet::tweet:mute_dialog:open"

    aput-object v4, v2, v7

    .line 843
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 842
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 844
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget v2, v0, Lcom/twitter/model/core/Tweet;->l:I

    .line 845
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, p0

    .line 844
    invoke-static/range {v0 .. v5}, Lcom/twitter/android/util/y;->a(Landroid/content/Context;Ljava/lang/String;IILandroid/support/v4/app/FragmentManager;Landroid/support/v4/app/Fragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 846
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "tweet"

    aput-object v2, v1, v7

    const/4 v2, 0x0

    aput-object v2, v1, v6

    aput-object v8, v1, v10

    const-string/jumbo v2, "mute_dialog"

    aput-object v2, v1, v3

    const-string/jumbo v2, "mute_user"

    aput-object v2, v1, v11

    .line 847
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 846
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 848
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->u()V

    goto/16 :goto_0

    .line 852
    :cond_7
    const v1, 0x7f1308b4

    if-ne v0, v1, :cond_9

    .line 853
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->A()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 854
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->D:Lcom/twitter/android/ar;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, p0, v1}, Lcom/twitter/android/ar;->b(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;)V

    goto/16 :goto_0

    .line 856
    :cond_8
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->D:Lcom/twitter/android/ar;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, p0, v1}, Lcom/twitter/android/ar;->a(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;)V

    goto/16 :goto_0

    .line 859
    :cond_9
    const v1, 0x7f130883

    if-ne v0, v1, :cond_b

    .line 860
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget v0, v0, Lcom/twitter/model/core/Tweet;->l:I

    invoke-static {v0}, Lcom/twitter/model/core/g;->e(I)Z

    move-result v0

    .line 861
    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v1}, Lcom/twitter/model/core/Tweet;->V()Ljava/lang/String;

    move-result-object v1

    .line 862
    if-eqz v0, :cond_a

    .line 863
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v4, "tweet"

    aput-object v4, v2, v7

    const/4 v4, 0x0

    aput-object v4, v2, v6

    aput-object v1, v2, v10

    const-string/jumbo v1, "unblock_dialog"

    aput-object v1, v2, v3

    const-string/jumbo v1, "impression"

    aput-object v1, v2, v11

    .line 864
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 863
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 865
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget-object v0, v0, Lcom/twitter/model/core/Tweet;->v:Ljava/lang/String;

    const/4 v1, 0x5

    .line 866
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 865
    invoke-static {p0, v0, v1, v2}, Lcom/twitter/android/util/y;->b(Landroid/content/Context;Ljava/lang/String;ILandroid/support/v4/app/FragmentManager;)V

    goto/16 :goto_1

    .line 868
    :cond_a
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v4, "tweet"

    aput-object v4, v2, v7

    const/4 v4, 0x0

    aput-object v4, v2, v6

    aput-object v1, v2, v10

    const-string/jumbo v1, "block_dialog"

    aput-object v1, v2, v3

    const-string/jumbo v1, "impression"

    aput-object v1, v2, v11

    .line 869
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 868
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 870
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-static {v0}, Lcom/twitter/library/view/e;->b(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v0

    .line 871
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 870
    invoke-static {p0, v0, v11, v1}, Lcom/twitter/android/util/y;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/support/v4/app/FragmentManager;)V

    goto/16 :goto_1

    .line 873
    :cond_b
    const v1, 0x7f1308c5

    if-ne v0, v1, :cond_c

    .line 874
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->z:Latj;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->k:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0, v1, v2}, Latj;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    goto/16 :goto_0

    .line 876
    :cond_c
    const v1, 0x7f13088a

    if-ne v0, v1, :cond_d

    .line 877
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "tweet"

    aput-object v2, v1, v7

    const/4 v2, 0x0

    aput-object v2, v1, v6

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    .line 878
    invoke-virtual {v2}, Lcom/twitter/model/core/Tweet;->V()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v10

    const-string/jumbo v2, "link"

    aput-object v2, v1, v3

    const-string/jumbo v2, "copy"

    aput-object v2, v1, v11

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 877
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 879
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->W()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 880
    const v0, 0x7f0a021b

    invoke-static {p0, v0, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 882
    :cond_d
    const v1, 0x7f1308ba

    if-ne v0, v1, :cond_10

    .line 883
    new-instance v2, Lcom/twitter/android/bf;

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->x()Lcom/twitter/app/common/base/BaseFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->G:Lcom/twitter/library/client/p;

    new-instance v4, Lcom/twitter/android/TweetActivity$6;

    invoke-direct {v4, p0}, Lcom/twitter/android/TweetActivity$6;-><init>(Lcom/twitter/android/TweetActivity;)V

    invoke-direct {v2, v0, p0, v1, v4}, Lcom/twitter/android/bf;-><init>(Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/p;Lcom/twitter/android/bf$a;)V

    .line 890
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/core/Tweet;->a(Lcom/twitter/model/core/TwitterUser;)Z

    move-result v0

    if-nez v0, :cond_e

    move v1, v6

    .line 891
    :goto_2
    new-instance v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    invoke-direct {v4, v8, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x5

    new-array v5, v0, [Ljava/lang/String;

    const-string/jumbo v0, "tweet"

    aput-object v0, v5, v7

    const/4 v0, 0x0

    aput-object v0, v5, v6

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    .line 892
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->V()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v10

    const-string/jumbo v0, ""

    aput-object v0, v5, v3

    if-eqz v1, :cond_f

    const-string/jumbo v0, "pin"

    :goto_3
    aput-object v0, v5, v11

    invoke-virtual {v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 894
    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget-object v4, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v2, v3, v1, v4, v0}, Lcom/twitter/android/bf;->a(Lcom/twitter/model/core/Tweet;ZLcom/twitter/library/client/Session;Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    goto/16 :goto_1

    :cond_e
    move v1, v7

    .line 890
    goto :goto_2

    .line 892
    :cond_f
    const-string/jumbo v0, "unpin"

    goto :goto_3

    .line 895
    :cond_10
    const v1, 0x7f130881

    if-ne v0, v1, :cond_11

    .line 896
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const-string/jumbo v1, "tweet"

    .line 897
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 898
    invoke-static {p0}, Laje$a;->a(Landroid/support/v4/app/FragmentActivity;)Laje$a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    .line 899
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Laje$a;->a(J)Laje$a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    .line 900
    invoke-virtual {v1, v2}, Laje$a;->a(Lcom/twitter/model/core/Tweet;)Laje$a;

    move-result-object v1

    .line 901
    invoke-virtual {v1, v0}, Laje$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Laje$a;

    move-result-object v0

    .line 902
    invoke-virtual {v0}, Laje$a;->a()Lajj;

    move-result-object v0

    .line 903
    invoke-interface {v0}, Lajj;->a()V

    goto/16 :goto_1

    .line 904
    :cond_11
    const v1, 0x7f130880

    if-ne v0, v1, :cond_12

    .line 905
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    .line 906
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/settings/MobileNotificationsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "NotificationSettingsActivity_account_name"

    .line 907
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 906
    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 908
    :cond_12
    const v1, 0x7f13087f

    if-ne v0, v1, :cond_3

    .line 909
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "tweet"

    aput-object v2, v1, v7

    const/4 v2, 0x0

    aput-object v2, v1, v6

    const-string/jumbo v2, "dont_like_recommendation"

    aput-object v2, v1, v10

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->C:Ljava/lang/String;

    aput-object v2, v1, v3

    const-string/jumbo v2, "click"

    aput-object v2, v1, v11

    .line 910
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->B:Ljava/lang/String;

    .line 911
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->g(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 912
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_1
.end method

.method public a(Lcmr;)Z
    .locals 2

    .prologue
    .line 675
    invoke-super {p0, p1}, Lcom/twitter/android/ActivityWithProgress;->a(Lcmr;)Z

    .line 677
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget v0, v0, Lcom/twitter/model/core/Tweet;->Q:I

    .line 678
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->d()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 679
    const v1, 0x7f140025

    invoke-interface {p1, v1}, Lcmr;->a(I)V

    .line 680
    const v1, 0x7f14000b

    invoke-interface {p1, v1}, Lcmr;->a(I)V

    .line 681
    const v1, 0x7f140022

    invoke-interface {p1, v1}, Lcmr;->a(I)V

    .line 682
    invoke-static {v0}, Lcom/twitter/model/core/ae$a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 683
    const v1, 0x7f14001d

    invoke-interface {p1, v1}, Lcmr;->a(I)V

    .line 685
    :cond_0
    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->D:Lcom/twitter/android/ar;

    invoke-virtual {v1, p1}, Lcom/twitter/android/ar;->a(Lcmr;)V

    .line 686
    invoke-static {v0}, Lcom/twitter/model/core/ae$a;->b(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 687
    const v1, 0x7f140003

    invoke-interface {p1, v1}, Lcmr;->a(I)V

    .line 689
    :cond_1
    invoke-static {v0}, Lcom/twitter/model/core/ae$a;->c(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 690
    const v0, 0x7f140026

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 692
    :cond_2
    invoke-static {}, Lbrz;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget-boolean v0, v0, Lcom/twitter/model/core/Tweet;->F:Z

    if-nez v0, :cond_3

    .line 693
    const v0, 0x7f140001

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 695
    :cond_3
    const-string/jumbo v0, "magic_rec_negative_signal_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 696
    const/high16 v0, 0x7f140000

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 699
    :cond_4
    const v0, 0x7f140008

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 701
    const/4 v0, 0x1

    return v0

    .line 677
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcmr;)I
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 707
    invoke-interface {p1}, Lcmr;->k()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 708
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 709
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v5

    .line 710
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    .line 711
    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget-wide v8, v1, Lcom/twitter/model/core/Tweet;->s:J

    cmp-long v1, v8, v6

    if-eqz v1, :cond_a

    move v1, v2

    .line 712
    :goto_0
    iget-object v4, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget v4, v4, Lcom/twitter/model/core/Tweet;->l:I

    invoke-static {v4}, Lcom/twitter/model/core/g;->e(I)Z

    move-result v4

    if-eqz v4, :cond_b

    move v4, v2

    .line 714
    :goto_1
    const v8, 0x7f13088e

    invoke-virtual {v0, v8}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v8

    .line 715
    if-eqz v8, :cond_0

    .line 716
    iget-boolean v9, p0, Lcom/twitter/android/TweetActivity;->w:Z

    if-eqz v9, :cond_c

    iget-boolean v9, p0, Lcom/twitter/android/TweetActivity;->v:Z

    if-nez v9, :cond_c

    iget-object v9, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-static {v9, v6, v7}, Lbxd;->a(Lcom/twitter/model/core/Tweet;J)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 718
    invoke-virtual {v8, v2}, Lazv;->b(Z)Lazv;

    .line 724
    :cond_0
    :goto_2
    const v6, 0x7f1308ba

    invoke-virtual {v0, v6}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v6

    .line 725
    if-eqz v6, :cond_1

    .line 726
    if-eqz v5, :cond_d

    iget-object v7, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v7, :cond_d

    iget-object v7, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v7, v5}, Lcom/twitter/model/core/Tweet;->a(Lcom/twitter/model/core/TwitterUser;)Z

    move-result v5

    if-eqz v5, :cond_d

    move v5, v2

    .line 727
    :goto_3
    invoke-virtual {v6, v2}, Lazv;->f(I)Lazv;

    .line 728
    if-eqz v5, :cond_e

    const v5, 0x7f0a09c0

    :goto_4
    invoke-virtual {v6, v5}, Lazv;->d(I)Lazv;

    .line 729
    iget-boolean v5, p0, Lcom/twitter/android/TweetActivity;->w:Z

    if-eqz v5, :cond_f

    if-nez v1, :cond_f

    move v5, v2

    :goto_5
    invoke-virtual {v6, v5}, Lazv;->b(Z)Lazv;

    .line 732
    :cond_1
    const v5, 0x7f1308b3

    invoke-virtual {v0, v5}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v6

    .line 733
    if-eqz v6, :cond_2

    .line 734
    const/16 v5, 0xa

    invoke-virtual {v6, v5}, Lazv;->f(I)Lazv;

    .line 736
    iget-object v5, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-static {v5}, Lcom/twitter/android/util/y;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v5

    .line 737
    iget-boolean v7, p0, Lcom/twitter/android/TweetActivity;->w:Z

    if-eqz v7, :cond_12

    if-eqz v1, :cond_12

    if-nez v5, :cond_12

    if-nez v4, :cond_12

    .line 738
    iget-object v5, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v5, :cond_10

    iget-object v5, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget v5, v5, Lcom/twitter/model/core/Tweet;->l:I

    invoke-static {v5}, Lcom/twitter/model/core/g;->d(I)Z

    move-result v5

    if-eqz v5, :cond_10

    move v5, v2

    .line 739
    :goto_6
    if-eqz v5, :cond_11

    .line 740
    const v5, 0x7f0a09b0

    invoke-virtual {p0, v5}, Lcom/twitter/android/TweetActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Lazv;->a(Ljava/lang/CharSequence;)Lazv;

    .line 744
    :goto_7
    invoke-virtual {v6, v2}, Lazv;->b(Z)Lazv;

    .line 750
    :cond_2
    :goto_8
    invoke-direct {p0, v0}, Lcom/twitter/android/TweetActivity;->b(Lcom/twitter/internal/android/widget/ToolBar;)Lazv;

    move-result-object v5

    .line 751
    if-eqz v5, :cond_3

    .line 752
    const/16 v6, 0xb

    invoke-virtual {v5, v6}, Lazv;->f(I)Lazv;

    .line 755
    :cond_3
    const v5, 0x7f130883

    invoke-virtual {v0, v5}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v5

    .line 756
    if-eqz v5, :cond_4

    .line 757
    const/16 v6, 0xc

    invoke-virtual {v5, v6}, Lazv;->f(I)Lazv;

    .line 758
    if-eqz v4, :cond_13

    const v4, 0x7f0a0a01

    :goto_9
    invoke-virtual {v5, v4}, Lazv;->d(I)Lazv;

    .line 759
    if-eqz v1, :cond_14

    iget-boolean v4, p0, Lcom/twitter/android/TweetActivity;->w:Z

    if-eqz v4, :cond_14

    move v4, v2

    :goto_a
    invoke-virtual {v5, v4}, Lazv;->b(Z)Lazv;

    .line 762
    :cond_4
    const v4, 0x7f1308c5

    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v5

    .line 763
    if-eqz v5, :cond_5

    .line 764
    const/16 v4, 0xd

    invoke-virtual {v5, v4}, Lazv;->f(I)Lazv;

    .line 765
    iget-object v4, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v4, :cond_15

    iget-object v4, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v4}, Lcom/twitter/model/core/Tweet;->Z()Z

    move-result v4

    if-eqz v4, :cond_15

    move v4, v2

    .line 766
    :goto_b
    if-eqz v4, :cond_16

    const v4, 0x7f0a0767

    :goto_c
    invoke-virtual {v5, v4}, Lazv;->d(I)Lazv;

    .line 767
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v4

    if-nez v4, :cond_17

    if-eqz v1, :cond_17

    iget-boolean v1, p0, Lcom/twitter/android/TweetActivity;->w:Z

    if-eqz v1, :cond_17

    :goto_d
    invoke-virtual {v5, v2}, Lazv;->b(Z)Lazv;

    .line 770
    :cond_5
    invoke-direct {p0, v0}, Lcom/twitter/android/TweetActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)V

    .line 772
    const v1, 0x7f1302d3

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v1

    .line 773
    if-eqz v1, :cond_6

    .line 774
    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Lazv;->f(I)Lazv;

    .line 777
    :cond_6
    const v1, 0x7f13088a

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v1

    .line 778
    if-eqz v1, :cond_7

    .line 779
    iget-boolean v2, p0, Lcom/twitter/android/TweetActivity;->w:Z

    invoke-virtual {v1, v2}, Lazv;->b(Z)Lazv;

    .line 782
    :cond_7
    const v1, 0x7f130880

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v1

    .line 783
    if-eqz v1, :cond_8

    .line 784
    iget-boolean v2, p0, Lcom/twitter/android/TweetActivity;->A:Z

    invoke-virtual {v1, v2}, Lazv;->b(Z)Lazv;

    .line 787
    :cond_8
    const v1, 0x7f13087f

    .line 788
    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    .line 789
    if-eqz v0, :cond_9

    .line 790
    iget-boolean v1, p0, Lcom/twitter/android/TweetActivity;->A:Z

    invoke-virtual {v0, v1}, Lazv;->b(Z)Lazv;

    .line 793
    :cond_9
    invoke-super {p0, p1}, Lcom/twitter/android/ActivityWithProgress;->b(Lcmr;)I

    move-result v0

    return v0

    :cond_a
    move v1, v3

    .line 711
    goto/16 :goto_0

    :cond_b
    move v4, v3

    .line 712
    goto/16 :goto_1

    .line 720
    :cond_c
    invoke-virtual {v8, v3}, Lazv;->b(Z)Lazv;

    goto/16 :goto_2

    :cond_d
    move v5, v3

    .line 726
    goto/16 :goto_3

    .line 728
    :cond_e
    const v5, 0x7f0a069b

    goto/16 :goto_4

    :cond_f
    move v5, v3

    .line 729
    goto/16 :goto_5

    :cond_10
    move v5, v3

    .line 738
    goto/16 :goto_6

    .line 742
    :cond_11
    const v5, 0x7f0a059f

    invoke-virtual {p0, v5}, Lcom/twitter/android/TweetActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Lazv;->a(Ljava/lang/CharSequence;)Lazv;

    goto/16 :goto_7

    .line 746
    :cond_12
    invoke-virtual {v6, v3}, Lazv;->b(Z)Lazv;

    goto/16 :goto_8

    .line 758
    :cond_13
    const v4, 0x7f0a00a6

    goto/16 :goto_9

    :cond_14
    move v4, v3

    .line 759
    goto/16 :goto_a

    :cond_15
    move v4, v3

    .line 765
    goto/16 :goto_b

    .line 766
    :cond_16
    const v4, 0x7f0a0766

    goto/16 :goto_c

    :cond_17
    move v2, v3

    .line 767
    goto :goto_d
.end method

.method protected synthetic b(Lank;)Lcom/twitter/app/common/abs/b;
    .locals 1

    .prologue
    .line 150
    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetActivity;->d(Lank;)Latp;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 1016
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a06bd

    .line 1017
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a07b3

    .line 1018
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0262

    .line 1019
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 1020
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 1021
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 1022
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 1023
    return-void
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x2

    const/4 v3, -0x1

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 287
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ActivityWithProgress;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 289
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 290
    iput-boolean v9, p0, Lcom/twitter/android/TweetActivity;->o:Z

    .line 291
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->z()V

    .line 292
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->P()Lcom/twitter/android/search/SearchSuggestionController;

    move-result-object v0

    const-string/jumbo v1, "tweet"

    invoke-virtual {v0, v1}, Lcom/twitter/android/search/SearchSuggestionController;->a(Ljava/lang/String;)Lcom/twitter/android/search/SearchSuggestionController;

    .line 294
    const v0, 0x7f13043b

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 295
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/TweetActivity$1;

    invoke-direct {v1, p0}, Lcom/twitter/android/TweetActivity$1;-><init>(Lcom/twitter/android/TweetActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 307
    const-string/jumbo v0, "social_proof_override"

    sget-object v1, Laji;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v2, v0, v1}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laji;

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->m:Laji;

    .line 309
    const-string/jumbo v0, "tw_scribe_content"

    sget-object v1, Lcom/twitter/model/timeline/r;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v2, v0, v1}, Lcom/twitter/util/v;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/timeline/r;

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->x:Lcom/twitter/model/timeline/r;

    .line 311
    const-string/jumbo v0, "association"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->k:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 312
    const-string/jumbo v0, "scribe_item"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->l:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 314
    const-string/jumbo v0, "type"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 315
    const-string/jumbo v1, "tag"

    const-wide/16 v4, -0x1

    invoke-virtual {v2, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 316
    if-eq v0, v3, :cond_0

    .line 317
    const-wide/16 v6, -0x1

    cmp-long v1, v4, v6

    if-eqz v1, :cond_a

    .line 318
    const-string/jumbo v1, "status_groups_type=? AND status_groups_tag=?"

    iput-object v1, p0, Lcom/twitter/android/TweetActivity;->h:Ljava/lang/String;

    .line 319
    new-array v1, v10, [Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v8

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v9

    iput-object v1, p0, Lcom/twitter/android/TweetActivity;->i:[Ljava/lang/String;

    .line 325
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    .line 326
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    .line 327
    const-string/jumbo v0, "tweet_fragment"

    invoke-virtual {v3, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/TweetListFragment;

    .line 328
    if-nez v0, :cond_3

    .line 329
    invoke-static {}, Lcom/twitter/android/TweetActivity;->y()Lcom/twitter/app/common/list/TwitterListFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/TweetListFragment;

    .line 330
    const v1, 0x7f1302e4

    const-string/jumbo v5, "tweet_fragment"

    invoke-virtual {v4, v1, v0, v5}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 332
    new-instance v1, Lcom/twitter/app/common/list/i$b;

    .line 333
    invoke-virtual {v0}, Lcom/twitter/android/TweetListFragment;->H()Lcom/twitter/app/common/list/i;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/twitter/app/common/list/i$b;-><init>(Lcom/twitter/app/common/list/i;)V

    .line 334
    const-string/jumbo v5, "source_association"

    iget-object v6, p0, Lcom/twitter/android/TweetActivity;->k:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v1, v5, v6}, Lcom/twitter/app/common/list/i$b;->a(Ljava/lang/String;Landroid/os/Parcelable;)Lcom/twitter/app/common/base/b$a;

    .line 335
    const-string/jumbo v5, "timeline_moment"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v5

    .line 336
    if-eqz v5, :cond_1

    .line 337
    const-string/jumbo v6, "timeline_moment"

    invoke-virtual {v1, v6, v5}, Lcom/twitter/app/common/list/i$b;->a(Ljava/lang/String;Ljava/io/Serializable;)Lcom/twitter/app/common/base/b$a;

    .line 339
    :cond_1
    const-string/jumbo v5, "subbranch"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v5

    .line 340
    if-eqz v5, :cond_2

    .line 341
    const-string/jumbo v6, "subbranch"

    invoke-virtual {v1, v6, v5}, Lcom/twitter/app/common/list/i$b;->a(Ljava/lang/String;Ljava/io/Serializable;)Lcom/twitter/app/common/base/b$a;

    .line 343
    :cond_2
    iget-object v5, p0, Lcom/twitter/android/TweetActivity;->l:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-virtual {v1, v5}, Lcom/twitter/app/common/list/i$b;->a(Lcom/twitter/analytics/feature/model/TwitterScribeItem;)Lcom/twitter/app/common/list/i$a;

    .line 344
    invoke-virtual {v1}, Lcom/twitter/app/common/list/i$b;->b()Lcom/twitter/app/common/list/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetListFragment;->a(Lcom/twitter/app/common/base/b;)V

    :cond_3
    move-object v1, v0

    .line 346
    invoke-virtual {v4}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 347
    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lata;

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lata;

    .line 348
    if-eqz p1, :cond_4

    .line 349
    const-string/jumbo v0, "tw_is_available"

    invoke-virtual {p1, v0, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/TweetActivity;->w:Z

    .line 351
    :cond_4
    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->w:Z

    if-nez v0, :cond_5

    .line 353
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->x()Lcom/twitter/app/common/base/BaseFragment;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 356
    :cond_5
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->i()V

    .line 358
    const v0, 0x7f0a098b

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->setTitle(I)V

    .line 360
    new-instance v0, Lcom/twitter/android/TweetActivity$b;

    invoke-direct {v0, p0, v11}, Lcom/twitter/android/TweetActivity$b;-><init>(Lcom/twitter/android/TweetActivity;Lcom/twitter/android/TweetActivity$1;)V

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->e:Lcom/twitter/library/service/t;

    .line 361
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->G:Lcom/twitter/library/client/p;

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->e:Lcom/twitter/library/service/t;

    invoke-virtual {v0, v3}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/t;)V

    .line 363
    if-eqz p1, :cond_b

    .line 364
    const-string/jumbo v0, "t"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    .line 365
    const-string/jumbo v0, "c"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->t:Lcom/twitter/model/core/Tweet;

    .line 366
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->t:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_6

    .line 367
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->j()V

    .line 382
    :cond_6
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lata;

    invoke-interface {v0, p0}, Lata;->a(Lati;)V

    .line 384
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_7

    invoke-static {}, Lcom/twitter/library/client/k;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    invoke-virtual {v0}, Lcom/twitter/model/core/f;->c()Z

    move-result v0

    if-nez v0, :cond_7

    .line 385
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/model/core/v;->c:Lcom/twitter/model/core/f;

    invoke-virtual {v0, v8}, Lcom/twitter/model/core/f;->a(I)Lcom/twitter/model/core/d;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ad;

    iget-object v0, v0, Lcom/twitter/model/core/ad;->E:Ljava/lang/String;

    .line 386
    invoke-static {p0}, Lcom/twitter/library/client/k;->a(Landroid/content/Context;)Lcom/twitter/library/client/k;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lcom/twitter/library/client/k;->a(Ljava/lang/String;Landroid/content/Context;)Z

    .line 389
    :cond_7
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v0

    if-nez v0, :cond_8

    .line 390
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->l()V

    .line 392
    :cond_8
    const-string/jumbo v0, "focus_compose"

    invoke-virtual {v2, v0, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 393
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->y:Latd;

    invoke-interface {v0}, Latd;->i()V

    .line 395
    :cond_9
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->Y()Lans;

    move-result-object v0

    check-cast v0, Latp;

    .line 396
    invoke-interface {v0}, Latp;->d()Latj;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->z:Latj;

    .line 397
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->z:Latj;

    new-instance v3, Lcom/twitter/android/TweetActivity$2;

    invoke-direct {v3, p0}, Lcom/twitter/android/TweetActivity$2;-><init>(Lcom/twitter/android/TweetActivity;)V

    invoke-virtual {v0, v3}, Latj;->a(Lasy;)V

    .line 405
    const-string/jumbo v0, "tw_is_negative_feedback"

    invoke-virtual {v2, v0, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/TweetActivity;->A:Z

    .line 406
    const-string/jumbo v0, "tw_negative_feedback_type"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->C:Ljava/lang/String;

    .line 407
    const-string/jumbo v0, "tw_negative_feedback_impression_id"

    .line 408
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 407
    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->B:Ljava/lang/String;

    .line 409
    new-instance v0, Lwc;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->C:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->B:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/twitter/android/TweetActivity;->A:Z

    invoke-direct {v0, v2, v3, v4}, Lwc;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v1, v0}, Lcom/twitter/android/TweetListFragment;->a(Lwc;)V

    .line 412
    new-instance v0, Lcom/twitter/android/ar;

    new-instance v1, Lcom/twitter/android/cr;

    invoke-direct {v1}, Lcom/twitter/android/cr;-><init>()V

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->k:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0, p0, v1, v2}, Lcom/twitter/android/ar;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/android/as;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->D:Lcom/twitter/android/ar;

    .line 414
    return-void

    .line 321
    :cond_a
    const-string/jumbo v1, "status_groups_type=?"

    iput-object v1, p0, Lcom/twitter/android/TweetActivity;->h:Ljava/lang/String;

    .line 322
    new-array v1, v9, [Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v8

    iput-object v1, p0, Lcom/twitter/android/TweetActivity;->i:[Ljava/lang/String;

    goto/16 :goto_0

    .line 370
    :cond_b
    const-string/jumbo v0, "tweet"

    invoke-static {p0, v0}, Lcom/twitter/android/al;->b(Landroid/app/Activity;Ljava/lang/String;)V

    .line 371
    const-string/jumbo v0, "tw"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    .line 373
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v3, "tweet:"

    aput-object v3, v0, v8

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-static {v3}, Lcom/twitter/model/core/Tweet;->b(Lcom/twitter/model/core/Tweet;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v9

    const-string/jumbo v3, ":impression"

    aput-object v3, v0, v10

    invoke-static {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 374
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 375
    new-instance v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-direct {v4, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 376
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v5, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-static {v4, v3, v5, v11}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)V

    .line 377
    new-array v3, v9, [Ljava/lang/String;

    aput-object v0, v3, v8

    invoke-virtual {v4, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const-string/jumbo v3, "ref_event"

    .line 378
    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->k:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 379
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 377
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_1
.end method

.method b_(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1228
    if-nez p1, :cond_0

    .line 1235
    :goto_0
    return v0

    .line 1232
    :cond_0
    :try_start_0
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1233
    const/4 v0, 0x1

    goto :goto_0

    .line 1234
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method protected synthetic c(Lank;)Lans;
    .locals 1

    .prologue
    .line 150
    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetActivity;->d(Lank;)Latp;

    move-result-object v0

    return-object v0
.end method

.method protected d(Lank;)Latp;
    .locals 2

    .prologue
    .line 631
    invoke-static {}, Latc;->c()Latc$a;

    move-result-object v0

    .line 632
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Latc$a;->a(Lamu;)Latc$a;

    move-result-object v0

    new-instance v1, Lant;

    invoke-direct {v1, p0, p1}, Lant;-><init>(Landroid/app/Activity;Lank;)V

    .line 633
    invoke-virtual {v0, v1}, Latc$a;->a(Lant;)Latc$a;

    move-result-object v0

    new-instance v1, Latv;

    invoke-direct {v1}, Latv;-><init>()V

    .line 634
    invoke-virtual {v0, v1}, Latc$a;->a(Latv;)Latc$a;

    move-result-object v0

    .line 635
    invoke-virtual {v0}, Latc$a;->a()Latp;

    move-result-object v0

    .line 631
    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 1446
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/TweetActivity;->o:Z

    .line 1447
    invoke-super {p0}, Lcom/twitter/android/ActivityWithProgress;->d()V

    .line 1448
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->G:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->e:Lcom/twitter/library/service/t;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->b(Lcom/twitter/library/service/t;)V

    .line 1449
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    const/16 v2, 0xa

    const/4 v1, -0x1

    const/4 v6, 0x1

    .line 925
    const v0, 0xffff

    and-int/2addr v0, p1

    .line 926
    sparse-switch v0, :sswitch_data_0

    .line 996
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/ActivityWithProgress;->onActivityResult(IILandroid/content/Intent;)V

    .line 997
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->n:Lcom/twitter/android/media/selection/c;

    if-eqz v0, :cond_1

    .line 998
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->n:Lcom/twitter/android/media/selection/c;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->y:Latd;

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/twitter/android/media/selection/c;->a(IILandroid/content/Intent;Lcom/twitter/android/media/selection/a;)V

    .line 1000
    :cond_1
    return-void

    .line 928
    :sswitch_0
    if-ne v2, p2, :cond_2

    .line 929
    invoke-virtual {p0, v2}, Lcom/twitter/android/TweetActivity;->setResult(I)V

    .line 930
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->finish()V

    goto :goto_0

    .line 931
    :cond_2
    if-ne v6, p2, :cond_0

    .line 932
    const-string/jumbo v0, "status_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 933
    const-string/jumbo v0, "status_id"

    .line 934
    invoke-virtual {p3, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 935
    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->d:Lata;

    invoke-interface {v2, v0, v1}, Lata;->f(J)V

    goto :goto_0

    .line 943
    :sswitch_1
    if-eqz p3, :cond_0

    const-string/jumbo v0, "deleted"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 944
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->G:J

    const-string/jumbo v2, "deleted"

    invoke-virtual {p3, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 946
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->finish()V

    goto :goto_0

    .line 952
    :sswitch_2
    if-ne p2, v6, :cond_0

    .line 953
    const-string/jumbo v0, "account"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/account/UserAccount;

    .line 954
    iget-object v2, v0, Lcom/twitter/model/account/UserAccount;->a:Landroid/accounts/Account;

    .line 955
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    .line 956
    if-eqz v0, :cond_0

    iget-object v1, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 957
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string/jumbo v1, "twitter"

    .line 958
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "tweet"

    .line 959
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string/jumbo v4, "status_id"

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->G:J

    .line 960
    :goto_1
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 963
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 964
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/TweetActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 966
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v6, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "tweet::switch_account::success"

    aput-object v5, v3, v4

    .line 967
    invoke-virtual {v1, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    .line 966
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 968
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v1

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1, v0, p0, v2}, Lcom/twitter/app/main/MainActivity;->a(Lcom/twitter/library/client/v;Landroid/content/Intent;Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 959
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->g:Landroid/net/Uri;

    .line 961
    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    goto :goto_1

    .line 974
    :sswitch_3
    if-ne p2, v1, :cond_4

    if-eqz p3, :cond_4

    .line 976
    invoke-static {p3}, Lcom/twitter/android/util/j;->a(Landroid/content/Intent;)Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    .line 977
    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->n:Lcom/twitter/android/media/selection/c;

    new-instance v2, Lcom/twitter/android/media/selection/MediaAttachment;

    invoke-direct {v2, v0}, Lcom/twitter/android/media/selection/MediaAttachment;-><init>(Lcom/twitter/model/drafts/DraftAttachment;)V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->y:Latd;

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/media/selection/a;)V

    .line 979
    :cond_4
    sget-object v0, Lcom/twitter/android/composer/ComposerType;->b:Lcom/twitter/android/composer/ComposerType;

    invoke-static {v0, p3}, Lcom/twitter/android/util/j;->a(Lcom/twitter/android/composer/ComposerType;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 983
    :sswitch_4
    if-ne p2, v1, :cond_0

    if-eqz p3, :cond_0

    .line 985
    invoke-static {p3}, Lcom/twitter/app/users/f;->a(Landroid/content/Intent;)Lcom/twitter/app/users/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/users/f;->e()Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    move-result-object v0

    .line 986
    if-eqz v0, :cond_0

    .line 987
    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->y:Latd;

    iget-object v0, v0, Lcom/twitter/android/UsersAdapter$CheckboxConfig;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Latd;->a(Ljava/util/List;)V

    goto/16 :goto_0

    .line 926
    nop

    :sswitch_data_0
    .sparse-switch
        0x67 -> :sswitch_2
        0x68 -> :sswitch_3
        0x6b -> :sswitch_4
        0x23bf -> :sswitch_1
        0x23c1 -> :sswitch_0
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 1004
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->y:Latd;

    invoke-interface {v0}, Latd;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1013
    :goto_0
    return-void

    .line 1008
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->y:Latd;

    invoke-interface {v0}, Latd;->aK_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1009
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->b()V

    goto :goto_0

    .line 1011
    :cond_1
    invoke-super {p0}, Lcom/twitter/android/ActivityWithProgress;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1206
    if-nez p1, :cond_1

    .line 1207
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->g:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->b_(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1209
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->r()V

    .line 1223
    :goto_0
    return-object v6

    .line 1212
    :cond_0
    new-instance v0, Lcom/twitter/util/android/d;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->g:Landroid/net/Uri;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->g:Landroid/net/Uri;

    invoke-static {v1}, Lcom/twitter/android/TweetActivity;->b(Landroid/net/Uri;)[Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/TweetActivity;->h:Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/android/TweetActivity;->i:[Ljava/lang/String;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v6, v0

    goto :goto_0

    .line 1213
    :cond_1
    const/4 v0, 0x4

    if-ne p1, v0, :cond_3

    .line 1214
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->s:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->b_(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1216
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->r()V

    goto :goto_0

    .line 1219
    :cond_2
    new-instance v2, Lcom/twitter/util/android/d;

    iget-object v4, p0, Lcom/twitter/android/TweetActivity;->s:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->s:Landroid/net/Uri;

    invoke-static {v0}, Lcom/twitter/android/TweetActivity;->b(Landroid/net/Uri;)[Ljava/lang/String;

    move-result-object v5

    move-object v3, p0

    move-object v7, v6

    move-object v8, v6

    invoke-direct/range {v2 .. v8}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v6, v2

    goto :goto_0

    .line 1222
    :cond_3
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->r()V

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 150
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/TweetActivity;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1332
    return-void
.end method

.method protected onPause()V
    .locals 6

    .prologue
    .line 1437
    invoke-super {p0}, Lcom/twitter/android/ActivityWithProgress;->onPause()V

    .line 1438
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "tweet::::navigate"

    aput-object v3, v1, v2

    .line 1439
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 1440
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/twitter/android/TweetActivity;->f:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 1441
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1442
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 1430
    invoke-super {p0}, Lcom/twitter/android/ActivityWithProgress;->onResume()V

    .line 1431
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/TweetActivity;->f:J

    .line 1432
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1196
    invoke-super {p0, p1}, Lcom/twitter/android/ActivityWithProgress;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1197
    const-string/jumbo v0, "t"

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/model/core/Tweet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1198
    const-string/jumbo v0, "tw_is_available"

    iget-boolean v1, p0, Lcom/twitter/android/TweetActivity;->w:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1199
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->t:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_0

    .line 1200
    const-string/jumbo v0, "c"

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->t:Lcom/twitter/model/core/Tweet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1202
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 666
    invoke-super {p0}, Lcom/twitter/android/ActivityWithProgress;->onStart()V

    .line 667
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    if-nez v0, :cond_0

    .line 668
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/client/Session;

    .line 669
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->q()V

    .line 671
    :cond_0
    return-void
.end method
