.class public Lcom/twitter/android/notificationtimeline/ActivityFragment;
.super Lcom/twitter/app/common/list/TwitterListFragment;
.source "Twttr"


# annotations
.annotation build Lcom/twitter/app/AutoSaveState;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/notificationtimeline/ActivityFragment$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/list/TwitterListFragment",
        "<",
        "Lbzz;",
        "Lcom/twitter/android/notificationtimeline/f;",
        ">;"
    }
.end annotation


# instance fields
.field a:Z
    .annotation build Lcom/twitter/app/SaveState;
    .end annotation
.end field

.field b:Z
    .annotation build Lcom/twitter/app/SaveState;
    .end annotation
.end field

.field c:Z
    .annotation build Lcom/twitter/app/SaveState;
    .end annotation
.end field

.field private final d:Lcom/twitter/library/client/u;

.field private final e:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private f:I

.field private g:J

.field private h:J

.field private i:Landroid/view/View;

.field private j:Lcom/twitter/android/ct;

.field private k:Z

.field private final l:Lcom/twitter/model/util/FriendshipCache;

.field private m:Landroid/content/SharedPreferences;

.field private n:Lcom/twitter/android/notificationtimeline/a;

.field private o:Z

.field private p:Lcom/twitter/android/notificationtimeline/h;

.field private q:Lcom/twitter/library/widget/TweetView;

.field private r:Z

.field private s:Lcom/twitter/android/platform/DeviceStorageLowReceiver;

.field private t:Z

.field private u:Lcom/twitter/android/notificationtimeline/c;

.field private v:Z

.field private w:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/twitter/app/common/list/TwitterListFragment;-><init>()V

    .line 142
    new-instance v0, Lcom/twitter/android/notificationtimeline/ActivityFragment$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/notificationtimeline/ActivityFragment$a;-><init>(Lcom/twitter/android/notificationtimeline/ActivityFragment;Lcom/twitter/android/notificationtimeline/ActivityFragment$1;)V

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->d:Lcom/twitter/library/client/u;

    .line 143
    new-instance v0, Lcom/twitter/android/notificationtimeline/ActivityFragment$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment$1;-><init>(Lcom/twitter/android/notificationtimeline/ActivityFragment;)V

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->e:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 155
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->f:I

    .line 162
    new-instance v0, Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/model/util/FriendshipCache;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->l:Lcom/twitter/model/util/FriendshipCache;

    return-void
.end method

.method private A()Z
    .locals 2

    .prologue
    .line 998
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 999
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    .line 1000
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/model/account/UserSettings;->A:Ljava/lang/String;

    const-string/jumbo v1, "following"

    invoke-static {v0, v1}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private B()Z
    .locals 1

    .prologue
    .line 1004
    iget-boolean v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->a:Z

    return v0
.end method

.method private C()Z
    .locals 2

    .prologue
    .line 1008
    iget-boolean v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->a:Z

    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->B()Z

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private D()Z
    .locals 2

    .prologue
    .line 1012
    iget-boolean v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->w:Z

    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->A()Z

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private E()Z
    .locals 2

    .prologue
    .line 1016
    iget-boolean v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->v:Z

    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->z()Z

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private F()Z
    .locals 1

    .prologue
    .line 1020
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->C()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->D()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->E()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private G()V
    .locals 1

    .prologue
    .line 1029
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->B()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->a:Z

    .line 1030
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->A()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->w:Z

    .line 1031
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->z()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->v:Z

    .line 1032
    return-void
.end method

.method private M()Z
    .locals 1

    .prologue
    .line 1035
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 1036
    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/twitter/model/core/TwitterUser;->m:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lael;)Lcom/twitter/android/notificationtimeline/f;
    .locals 9

    .prologue
    .line 289
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->aK()Lcom/twitter/app/common/base/TwitterFragmentActivity;

    move-result-object v1

    .line 290
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v4

    .line 291
    instance-of v0, v1, Lcom/twitter/app/main/MainActivity;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lcom/twitter/app/main/MainActivity;

    .line 292
    invoke-virtual {v0}, Lcom/twitter/app/main/MainActivity;->q()Lcom/twitter/app/main/b;

    move-result-object v8

    .line 293
    :goto_0
    new-instance v0, Lcom/twitter/android/notificationtimeline/f;

    iget-object v2, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->l:Lcom/twitter/model/util/FriendshipCache;

    iget-object v3, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->p:Lcom/twitter/android/notificationtimeline/h;

    iget-object v5, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->n:Lcom/twitter/android/notificationtimeline/a;

    .line 294
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->r()Landroid/view/View$OnClickListener;

    move-result-object v6

    move-object v7, p1

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/notificationtimeline/f;-><init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/android/notificationtimeline/h;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/notificationtimeline/a;Landroid/view/View$OnClickListener;Lael;Lcom/twitter/app/main/b;)V

    .line 293
    return-object v0

    .line 292
    :cond_0
    const/4 v8, 0x0

    goto :goto_0
.end method

.method private a(JI)V
    .locals 5

    .prologue
    const v3, 0x7f0a003a

    .line 676
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 677
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v0, "type"

    iget v2, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->f:I

    .line 678
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "event_type"

    .line 679
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "user_tag"

    .line 680
    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "status_tag"

    .line 681
    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    .line 683
    packed-switch p3, :pswitch_data_0

    .line 720
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Tried to start ActivityDetailActivity for unsupported type: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v0, Lbzx$a;->a:Ljava/util/Map;

    .line 721
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 720
    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 728
    :goto_0
    return-void

    .line 686
    :pswitch_1
    const-string/jumbo v0, "title_res_id"

    const v2, 0x7f0a03ae

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 725
    :goto_1
    sget-object v0, Lbzx$a;->a:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->b(Ljava/lang/String;)V

    .line 727
    invoke-virtual {p0, v1}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 693
    :pswitch_2
    const-string/jumbo v0, "title_res_id"

    const v2, 0x7f0a0039

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 697
    :pswitch_3
    const-string/jumbo v0, "title_res_id"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 701
    :pswitch_4
    const-string/jumbo v0, "title_res_id"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 705
    :pswitch_5
    const-string/jumbo v0, "title_res_id"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 709
    :pswitch_6
    const-string/jumbo v0, "title_res_id"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 683
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_5
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_6
    .end packed-switch
.end method

.method private a(JLjava/lang/String;)V
    .locals 7

    .prologue
    .line 649
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "user_id"

    .line 650
    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "screen_name"

    .line 651
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 652
    const-string/jumbo v2, "association"

    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 653
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    const/4 v3, 0x5

    .line 654
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-wide v4, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->a_:J

    .line 655
    invoke-virtual {v0, v4, v5}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    .line 652
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 657
    invoke-virtual {p0, v1}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->startActivity(Landroid/content/Intent;)V

    .line 658
    return-void
.end method

.method private a(JLjava/lang/String;Ljava/lang/String;J)V
    .locals 3

    .prologue
    .line 662
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/app/lists/ListTabActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "list_id"

    .line 663
    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "list_name"

    .line 664
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "list_fullname"

    .line 665
    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "creator_id"

    .line 666
    invoke-virtual {v0, v1, p5, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    .line 667
    invoke-virtual {p0, v0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->startActivity(Landroid/content/Intent;)V

    .line 668
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/notificationtimeline/ActivityFragment;Z)V
    .locals 0

    .prologue
    .line 112
    invoke-virtual {p0, p1}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->e(Z)V

    return-void
.end method

.method private a(IJ)Z
    .locals 8

    .prologue
    const/16 v0, 0x14

    .line 754
    invoke-virtual {p0, p1}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->c_(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 755
    const/4 v0, 0x0

    .line 800
    :goto_0
    return v0

    .line 759
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 775
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 776
    invoke-static {}, Lcom/twitter/library/api/activity/g;->a()Lcom/twitter/library/api/activity/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/api/activity/g;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->f:I

    .line 777
    invoke-static {v1}, Lcab$a;->b(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 778
    new-instance v1, Lcom/twitter/library/api/activity/f;

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 779
    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    iget v6, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->f:I

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/api/activity/f;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JI)V

    .line 780
    invoke-direct {p0, p1}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->c(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/api/activity/f;->c(J)Lbge;

    move-result-object v1

    .line 781
    invoke-direct {p0, p1}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->e(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lbge;->b(J)Lbge;

    move-result-object v1

    .line 782
    invoke-virtual {v1, v0}, Lbge;->c(I)Lbge;

    move-result-object v0

    .line 783
    iget v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->f:I

    invoke-virtual {p0, v0, v1, p1}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 800
    :cond_1
    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    .line 766
    :pswitch_1
    const/4 v0, -0x1

    .line 767
    goto :goto_1

    .line 785
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    .line 786
    invoke-virtual {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, p1}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 787
    new-instance v2, Lcom/twitter/library/api/activity/FetchActivityTimeline;

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    new-instance v5, Lcom/twitter/library/service/v;

    invoke-direct {v5, v3}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    iget v3, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->f:I

    .line 788
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->M()Z

    move-result v6

    invoke-direct {v2, v4, v5, v3, v6}, Lcom/twitter/library/api/activity/FetchActivityTimeline;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;IZ)V

    .line 789
    invoke-virtual {v2, v1}, Lcom/twitter/library/api/activity/FetchActivityTimeline;->a(Ljava/lang/String;)Lcom/twitter/library/api/activity/FetchActivityTimeline;

    move-result-object v1

    .line 790
    invoke-direct {p0, p1}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->c(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/api/activity/FetchActivityTimeline;->c(J)Lbge;

    move-result-object v1

    .line 791
    invoke-direct {p0, p1}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->e(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lbge;->b(J)Lbge;

    move-result-object v1

    .line 792
    invoke-virtual {v1, v0}, Lbge;->c(I)Lbge;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->f:I

    .line 787
    invoke-virtual {p0, v0, v1, p1}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 794
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x6

    if-ne p1, v0, :cond_1

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_1

    .line 795
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/notificationtimeline/f;

    .line 796
    invoke-virtual {v0, p2, p3}, Lcom/twitter/android/notificationtimeline/f;->a(J)V

    .line 797
    invoke-virtual {v0}, Lcom/twitter/android/notificationtimeline/f;->notifyDataSetChanged()V

    goto :goto_2

    .line 759
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic a(Lcom/twitter/android/notificationtimeline/ActivityFragment;)Z
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->ay()Z

    move-result v0

    return v0
.end method

.method private a(Lcom/twitter/library/service/s;)Z
    .locals 4

    .prologue
    .line 900
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    .line 901
    if-eqz v0, :cond_0

    iget-wide v0, v0, Lcom/twitter/library/service/v;->c:J

    iget-wide v2, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->a_:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/notificationtimeline/ActivityFragment;)Lcjr;
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->az()Lcjr;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 643
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 644
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v3

    const-string/jumbo v4, ""

    const-string/jumbo v5, "click"

    invoke-static {v3, p1, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 645
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 643
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 646
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/notificationtimeline/ActivityFragment;Z)Z
    .locals 0

    .prologue
    .line 112
    iput-boolean p1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->o:Z

    return p1
.end method

.method private c(I)J
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 810
    packed-switch p1, :pswitch_data_0

    .line 832
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fetch type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 813
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 814
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/notificationtimeline/f;

    invoke-virtual {v0}, Lcom/twitter/android/notificationtimeline/f;->g()Lcbi;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzz;

    .line 815
    if-eqz v0, :cond_0

    .line 816
    iget-object v0, v0, Lbzz;->a:Lbzy;

    iget-wide v0, v0, Lbzy;->b:J

    .line 828
    :goto_0
    return-wide v0

    :cond_0
    move-wide v0, v2

    .line 819
    goto :goto_0

    .line 823
    :pswitch_2
    iget-wide v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->h:J

    goto :goto_0

    :pswitch_3
    move-wide v0, v2

    .line 828
    goto :goto_0

    .line 810
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic c(Lcom/twitter/android/notificationtimeline/ActivityFragment;)Lcom/twitter/model/util/FriendshipCache;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->l:Lcom/twitter/model/util/FriendshipCache;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/notificationtimeline/ActivityFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private e(I)J
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 844
    packed-switch p1, :pswitch_data_0

    .line 869
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fetch type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    move-wide v0, v2

    .line 865
    :goto_0
    return-wide v0

    .line 852
    :pswitch_2
    iget-wide v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->g:J

    goto :goto_0

    .line 856
    :pswitch_3
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 857
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/notificationtimeline/f;

    invoke-virtual {v0}, Lcom/twitter/android/notificationtimeline/f;->g()Lcbi;

    move-result-object v0

    .line 858
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 859
    invoke-virtual {v0}, Lcbi;->be_()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcbi;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzz;

    .line 860
    if-eqz v0, :cond_0

    .line 861
    iget-object v0, v0, Lbzz;->a:Lbzy;

    iget-wide v0, v0, Lbzy;->c:J

    goto :goto_0

    :cond_0
    move-wide v0, v2

    .line 865
    goto :goto_0

    .line 844
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic e(Lcom/twitter/android/notificationtimeline/ActivityFragment;)Lcom/twitter/library/client/p;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->S:Lcom/twitter/library/client/p;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/notificationtimeline/ActivityFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/notificationtimeline/ActivityFragment;)Lcom/twitter/library/client/p;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->S:Lcom/twitter/library/client/p;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/notificationtimeline/ActivityFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/notificationtimeline/ActivityFragment;)I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->f:I

    return v0
.end method

.method static synthetic j(Lcom/twitter/android/notificationtimeline/ActivityFragment;)Lcom/twitter/android/notificationtimeline/h;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->p:Lcom/twitter/android/notificationtimeline/h;

    return-object v0
.end method

.method private r()Landroid/view/View$OnClickListener;
    .locals 2

    .prologue
    .line 299
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    .line 300
    new-instance v1, Lcom/twitter/android/notificationtimeline/ActivityFragment$3;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/notificationtimeline/ActivityFragment$3;-><init>(Lcom/twitter/android/notificationtimeline/ActivityFragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    return-object v1
.end method

.method private s()V
    .locals 4

    .prologue
    .line 341
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    .line 342
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":stream::results"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 343
    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->n:Lcom/twitter/android/notificationtimeline/a;

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, v0}, Lcom/twitter/android/notificationtimeline/a;->a(JLjava/lang/String;)V

    .line 344
    return-void
.end method

.method private t()Z
    .locals 1

    .prologue
    .line 745
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->s:Lcom/twitter/android/platform/DeviceStorageLowReceiver;

    invoke-virtual {v0}, Lcom/twitter/android/platform/DeviceStorageLowReceiver;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->t:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private u()V
    .locals 4

    .prologue
    .line 886
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->i:Landroid/view/View;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->aj()Z

    move-result v0

    if-nez v0, :cond_1

    .line 897
    :cond_0
    :goto_0
    return-void

    .line 890
    :cond_1
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 891
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040026

    const/4 v3, 0x0

    .line 892
    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->i:Landroid/view/View;

    .line 893
    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->i:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 894
    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 895
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private v()V
    .locals 3

    .prologue
    .line 905
    iget-boolean v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->o:Z

    if-nez v0, :cond_0

    .line 906
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->o:Z

    .line 907
    new-instance v0, Lcom/twitter/library/api/activity/e;

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/api/activity/e;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x1e62

    const/16 v2, 0x8

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 910
    :cond_0
    return-void
.end method

.method private w()V
    .locals 5

    .prologue
    .line 944
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 945
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->G()V

    .line 946
    iget-boolean v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->c:Z

    iget-boolean v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->w:Z

    iget-boolean v2, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->v:Z

    iget-boolean v3, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->a:Z

    .line 948
    invoke-static {}, Lcom/twitter/library/api/activity/g;->a()Lcom/twitter/library/api/activity/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/api/activity/g;->b()Z

    move-result v4

    .line 946
    invoke-static {v0, v1, v2, v3, v4}, Lcab$a;->a(ZZZZZ)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->f:I

    .line 949
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->a(Z)V

    .line 951
    :cond_0
    return-void
.end method

.method private x()Landroid/view/View;
    .locals 2

    .prologue
    .line 968
    new-instance v0, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;-><init>(Landroid/content/Context;)V

    .line 969
    new-instance v1, Lcom/twitter/android/notificationtimeline/c;

    invoke-direct {v1, v0, p0}, Lcom/twitter/android/notificationtimeline/c;-><init>(Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;Lcom/twitter/android/notificationtimeline/ActivityFragment;)V

    iput-object v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->u:Lcom/twitter/android/notificationtimeline/c;

    .line 970
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->y()V

    .line 971
    invoke-virtual {v0}, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private y()V
    .locals 2

    .prologue
    .line 975
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->u:Lcom/twitter/android/notificationtimeline/c;

    if-eqz v0, :cond_0

    .line 976
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->u:Lcom/twitter/android/notificationtimeline/c;

    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->M()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/notificationtimeline/c;->a(Z)V

    .line 978
    :cond_0
    return-void
.end method

.method private z()Z
    .locals 2

    .prologue
    .line 986
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 987
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    .line 988
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/model/account/UserSettings;->z:Ljava/lang/String;

    const-string/jumbo v1, "enabled"

    invoke-static {v0, v1}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected H_()V
    .locals 2

    .prologue
    .line 587
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->p:Lcom/twitter/android/notificationtimeline/h;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/notificationtimeline/h;->a(Z)V

    .line 588
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->b(I)Z

    .line 589
    return-void
.end method

.method protected K()Laoy;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laoy",
            "<",
            "Lcbi",
            "<",
            "Lbzz;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 454
    new-instance v0, Lcom/twitter/android/notificationtimeline/ActivityFragment$4;

    invoke-direct {v0, p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment$4;-><init>(Lcom/twitter/android/notificationtimeline/ActivityFragment;)V

    .line 465
    new-instance v1, Ladr;

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v0}, Ladr;-><init>(Landroid/support/v4/app/LoaderManager;ILcom/twitter/util/object/j;)V

    return-object v1
.end method

.method protected M_()Z
    .locals 1

    .prologue
    .line 882
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->b(I)Z

    move-result v0

    return v0
.end method

.method protected a(JJ)V
    .locals 7

    .prologue
    .line 545
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->o:Z

    .line 546
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 547
    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->p:Lcom/twitter/android/notificationtimeline/h;

    iget v2, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->f:I

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lcom/twitter/android/notificationtimeline/h;->a(IJ)V

    .line 548
    return-void
.end method

.method protected a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 8

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x1

    .line 595
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 596
    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v2

    .line 597
    instance-of v0, v2, Lcah;

    if-eqz v0, :cond_5

    .line 598
    invoke-static {v2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcah;

    .line 599
    instance-of v2, v1, Laei$c;

    if-eqz v2, :cond_2

    move-object v0, v1

    .line 600
    check-cast v0, Laei$c;

    .line 602
    iget-object v1, v0, Laei$c;->e:Lcom/twitter/model/core/TwitterUser;

    if-eqz v1, :cond_0

    .line 603
    iget-object v1, v0, Laei$c;->e:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v1, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-object v0, v0, Laei$c;->e:Lcom/twitter/model/core/TwitterUser;

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    invoke-direct {p0, v2, v3, v0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->a(JLjava/lang/String;)V

    .line 605
    :cond_0
    const-string/jumbo v0, "joined_twitter"

    invoke-direct {p0, v0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->b(Ljava/lang/String;)V

    .line 640
    :cond_1
    :goto_0
    return-void

    .line 606
    :cond_2
    instance-of v1, v1, Laee$a;

    if-eqz v1, :cond_1

    .line 607
    iget-object v1, v0, Lcah;->b:Lcac;

    check-cast v1, Lcap;

    .line 608
    iget v2, v1, Lcap;->d:I

    if-ne v2, v5, :cond_3

    .line 609
    invoke-virtual {v0}, Lcah;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzv;

    .line 610
    if-eqz v0, :cond_1

    .line 611
    iget-wide v2, v0, Lbzv;->b:J

    iget-object v4, v0, Lbzv;->c:Ljava/lang/String;

    iget-object v5, v0, Lbzv;->d:Ljava/lang/String;

    iget-wide v6, v0, Lbzv;->e:J

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->a(JLjava/lang/String;Ljava/lang/String;J)V

    .line 612
    const-string/jumbo v0, "list_member_added"

    invoke-direct {p0, v0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 614
    :cond_3
    iget v2, v1, Lcap;->d:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_4

    invoke-virtual {v1}, Lcap;->f()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v4, :cond_4

    .line 615
    invoke-virtual {v1}, Lcap;->f()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 616
    if-eqz v0, :cond_1

    .line 617
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v2

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    invoke-direct {p0, v2, v3, v0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->a(JLjava/lang/String;)V

    .line 618
    const-string/jumbo v0, "follow"

    invoke-direct {p0, v0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 621
    :cond_4
    iget-object v1, v0, Lcah;->b:Lcac;

    check-cast v1, Lcap;

    invoke-virtual {v1}, Lcap;->a()J

    move-result-wide v2

    iget-object v0, v0, Lcah;->b:Lcac;

    check-cast v0, Lcap;

    iget v0, v0, Lcap;->d:I

    invoke-direct {p0, v2, v3, v0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->a(JI)V

    goto :goto_0

    .line 624
    :cond_5
    instance-of v0, v2, Lcai;

    if-eqz v0, :cond_6

    .line 625
    invoke-static {v2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcai;

    iget-object v0, v0, Lcai;->b:Lcac;

    .line 626
    invoke-virtual {v0}, Lcac;->d()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->g:J

    .line 627
    invoke-virtual {v0}, Lcac;->e()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->h:J

    .line 628
    invoke-virtual {v0}, Lcac;->e()J

    move-result-wide v0

    invoke-direct {p0, v5, v0, v1}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->a(IJ)Z

    .line 629
    const-string/jumbo v0, "gap"

    invoke-direct {p0, v0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 632
    :cond_6
    if-eqz v2, :cond_7

    .line 633
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Handled click event for invalid activity item: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 634
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 638
    :goto_1
    new-instance v1, Lcpb;

    invoke-direct {v1, v0}, Lcpb;-><init>(Ljava/lang/Throwable;)V

    const-string/jumbo v0, "viewHierarchy"

    invoke-static {p2, v4}, Lcom/twitter/util/ui/k;->a(Landroid/view/View;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    goto/16 :goto_0

    .line 636
    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Handled click event for null activity item"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected a(Laog;)V
    .locals 1

    .prologue
    .line 552
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Laog;)V

    .line 553
    check-cast p1, Lcom/twitter/app/common/list/l;

    .line 555
    new-instance v0, Lcom/twitter/android/notificationtimeline/ActivityFragment$5;

    invoke-direct {v0, p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment$5;-><init>(Lcom/twitter/android/notificationtimeline/ActivityFragment;)V

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l;->a(Lcno$c;)V

    .line 582
    return-void
.end method

.method protected a(Lcbi;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lbzz;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 470
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcbi;)V

    .line 472
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/notificationtimeline/f;

    .line 473
    invoke-virtual {v0}, Lcom/twitter/android/notificationtimeline/f;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 474
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->b(I)Z

    .line 476
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->k:Z

    if-eqz v0, :cond_1

    .line 477
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->k:Z

    .line 480
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->i:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 481
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->i:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 482
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->i:Landroid/view/View;

    .line 484
    :cond_2
    return-void
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 6

    .prologue
    .line 488
    invoke-direct {p0, p1}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->a(Lcom/twitter/library/service/s;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 534
    :cond_0
    :goto_0
    return-void

    .line 492
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 493
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 494
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v1

    .line 498
    const/16 v2, 0x1e62

    if-ne p2, v2, :cond_2

    .line 499
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->o:Z

    .line 500
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 501
    iget-object v2, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->p:Lcom/twitter/android/notificationtimeline/h;

    iget-wide v4, v1, Lcom/twitter/library/service/v;->c:J

    iget-object v0, v0, Lcom/twitter/library/service/u;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "act_read_pos"

    .line 502
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 501
    invoke-virtual {v2, v4, v5, v0, v1}, Lcom/twitter/android/notificationtimeline/h;->a(JJ)V

    goto :goto_0

    .line 508
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/service/v;->a(Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 513
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 518
    iget v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->f:I

    if-ne p2, v0, :cond_3

    .line 521
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v0

    if-nez v0, :cond_3

    .line 524
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a0038

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 525
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 529
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    if-ne p3, v0, :cond_0

    .line 530
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/notificationtimeline/f;

    .line 531
    invoke-virtual {v0}, Lcom/twitter/android/notificationtimeline/f;->b()V

    .line 532
    invoke-virtual {v0}, Lcom/twitter/android/notificationtimeline/f;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method protected a(Z)V
    .locals 4

    .prologue
    .line 538
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->p:Lcom/twitter/android/notificationtimeline/h;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/notificationtimeline/h;->a(Z)V

    .line 539
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->p:Lcom/twitter/android/notificationtimeline/h;

    iget v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->f:I

    iget-wide v2, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->a_:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/notificationtimeline/h;->a(IJ)V

    .line 540
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Z)V

    .line 541
    return-void
.end method

.method public a(ZZ)V
    .locals 3

    .prologue
    .line 926
    iget-boolean v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->w:Z

    iget-boolean v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->v:Z

    .line 928
    invoke-static {}, Lcom/twitter/library/api/activity/g;->a()Lcom/twitter/library/api/activity/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/api/activity/g;->b()Z

    move-result v2

    .line 927
    invoke-static {p2, v0, v1, p1, v2}, Lcab$a;->a(ZZZZZ)I

    move-result v0

    .line 930
    invoke-static {v0}, Lcab$a;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 931
    invoke-static {}, Lcom/twitter/library/api/activity/g;->a()Lcom/twitter/library/api/activity/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/api/activity/g;->c()V

    .line 934
    :cond_0
    iget v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->f:I

    if-eq v0, v1, :cond_1

    .line 935
    iput v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->f:I

    .line 936
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->a(Z)V

    .line 939
    :cond_1
    iput-boolean p1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->a:Z

    .line 940
    iput-boolean p2, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->c:Z

    .line 941
    return-void
.end method

.method protected aP_()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    .line 364
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aP_()V

    .line 366
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->aq()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 367
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;)Lcom/twitter/android/client/l;

    move-result-object v0

    .line 368
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 367
    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/client/l;->c(J)V

    .line 372
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->H()Lcom/twitter/app/common/list/i;

    move-result-object v0

    .line 374
    const-string/jumbo v1, "ref_event"

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->b:Z

    if-nez v1, :cond_2

    .line 375
    const-string/jumbo v1, "ref_event"

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/i;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 376
    iput-boolean v4, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->b:Z

    move-object v1, v0

    .line 380
    :goto_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    new-array v2, v4, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 381
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v4

    const-string/jumbo v5, ""

    const-string/jumbo v6, ""

    const-string/jumbo v7, "impression"

    invoke-static {v4, v5, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 382
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 380
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 384
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/vit/VitNotificationsEducationOverlay;->a(Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 385
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/vit/VitNotificationsEducationOverlay;->b(Landroid/support/v4/app/FragmentActivity;)V

    .line 387
    :cond_1
    return-void

    .line 378
    :cond_2
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0
.end method

.method protected b()V
    .locals 2

    .prologue
    .line 348
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->b()V

    .line 349
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->d:Lcom/twitter/library/client/u;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/u;)V

    .line 351
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->v()V

    .line 354
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/app/main/MainActivity;

    if-eqz v0, :cond_0

    .line 355
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/main/MainActivity;

    invoke-virtual {v0}, Lcom/twitter/app/main/MainActivity;->r()Lcom/twitter/app/main/a;

    move-result-object v0

    .line 356
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->x()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/main/a;->a(Landroid/view/View;)V

    .line 358
    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->w()V

    .line 359
    return-void
.end method

.method protected b(I)Z
    .locals 2

    .prologue
    .line 749
    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->a(IJ)Z

    move-result v0

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 954
    iget-boolean v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->c:Z

    return v0
.end method

.method protected i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 877
    const-string/jumbo v0, "connect"

    return-object v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 958
    iget-boolean v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->a:Z

    return v0
.end method

.method protected l()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 732
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/notificationtimeline/f;

    invoke-virtual {v0}, Lcom/twitter/android/notificationtimeline/f;->g()Lcbi;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladu;

    .line 733
    if-eqz v0, :cond_1

    .line 734
    iget-boolean v3, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->r:Z

    if-eqz v3, :cond_2

    .line 735
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->t()Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 736
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->an()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Ladu;->aX_()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz v1, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->k:Z

    if-nez v0, :cond_1

    .line 737
    invoke-virtual {p0, v2}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->b(I)Z

    .line 738
    iput-boolean v2, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->k:Z

    .line 741
    :cond_1
    return-void

    .line 735
    :cond_2
    invoke-virtual {v0}, Ladu;->be_()I

    move-result v3

    const/16 v4, 0x320

    if-ge v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 222
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 223
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v2

    .line 224
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->aK()Lcom/twitter/app/common/base/TwitterFragmentActivity;

    move-result-object v1

    .line 225
    instance-of v0, v1, Lcom/twitter/app/main/MainActivity;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lcom/twitter/app/main/MainActivity;

    .line 226
    invoke-virtual {v0}, Lcom/twitter/app/main/MainActivity;->q()Lcom/twitter/app/main/b;

    move-result-object v0

    .line 227
    :goto_0
    new-instance v5, Lcom/twitter/android/ck;

    new-instance v3, Lcom/twitter/android/ar;

    new-instance v4, Lcom/twitter/android/notificationtimeline/b;

    invoke-direct {v4, v0}, Lcom/twitter/android/notificationtimeline/b;-><init>(Lcom/twitter/app/main/b;)V

    .line 229
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    invoke-direct {v3, v1, v4, v0}, Lcom/twitter/android/ar;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/android/as;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    invoke-direct {v5, p0, v2, v3}, Lcom/twitter/android/ck;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/ar;)V

    .line 231
    new-instance v0, Lbxc$a;

    invoke-direct {v0}, Lbxc$a;-><init>()V

    const-string/jumbo v1, "tweet:::platform_photo_card:click"

    .line 232
    invoke-virtual {v0, v1}, Lbxc$a;->c(Ljava/lang/String;)Lbxc$a;

    move-result-object v0

    const-string/jumbo v1, "tweet:::platform_player_card:click"

    .line 233
    invoke-virtual {v0, v1}, Lbxc$a;->d(Ljava/lang/String;)Lbxc$a;

    move-result-object v0

    const-string/jumbo v1, "tweet"

    const-string/jumbo v3, "avatar"

    const-string/jumbo v4, "profile_click"

    .line 234
    invoke-static {v2, v1, v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbxc$a;->a(Ljava/lang/String;)Lbxc$a;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 236
    invoke-virtual {v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "::tweet:link:open_link"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbxc$a;->b(Ljava/lang/String;)Lbxc$a;

    move-result-object v0

    .line 237
    invoke-virtual {v0}, Lbxc$a;->a()Lbxc;

    move-result-object v4

    .line 238
    new-instance v0, Lcom/twitter/android/ct;

    const/4 v3, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/ct;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Lbxc;Lcom/twitter/android/ck;)V

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->j:Lcom/twitter/android/ct;

    .line 240
    invoke-static {}, Lbph;->a()Z

    move-result v9

    .line 241
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->j:Lcom/twitter/android/ct;

    invoke-virtual {v0, v9}, Lcom/twitter/android/ct;->b(Z)V

    .line 242
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    .line 243
    iget-object v6, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    .line 245
    new-instance v3, Lcom/twitter/android/cl;

    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->T:Landroid/content/Context;

    .line 247
    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v7

    const/4 v8, 0x0

    move-object v4, p0

    invoke-direct/range {v3 .. v9}, Lcom/twitter/android/cl;-><init>(Lcom/twitter/app/common/list/TwitterListFragment;Lbxb;Landroid/widget/ListView;IZZ)V

    .line 250
    invoke-virtual {v0, v3}, Lcom/twitter/app/common/list/l;->a(Landroid/view/View$OnTouchListener;)V

    .line 252
    new-instance v5, Lcom/twitter/android/notificationtimeline/ActivityFragment$2;

    invoke-direct {v5, p0, v3}, Lcom/twitter/android/notificationtimeline/ActivityFragment$2;-><init>(Lcom/twitter/android/notificationtimeline/ActivityFragment;Lcom/twitter/android/cl;)V

    .line 258
    new-instance v3, Lael;

    iget-object v4, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->j:Lcom/twitter/android/ct;

    iget-object v6, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->l:Lcom/twitter/model/util/FriendshipCache;

    .line 259
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->T:Landroid/content/Context;

    .line 260
    invoke-static {v0}, Lcom/twitter/android/client/k;->a(Landroid/content/Context;)Lcom/twitter/android/client/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/k;->a()Z

    move-result v9

    .line 261
    invoke-static {}, Lcom/twitter/util/collection/ReferenceList;->a()Lcom/twitter/util/collection/ReferenceList;

    move-result-object v10

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->aw()Z

    move-result v11

    move-object v8, v2

    invoke-direct/range {v3 .. v11}, Lael;-><init>(Lcom/twitter/android/ct;Landroid/view/View$OnLongClickListener;Lcom/twitter/model/util/FriendshipCache;Landroid/app/Activity;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ZLcom/twitter/util/collection/ReferenceList;Z)V

    .line 262
    invoke-direct {p0, v3}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->a(Lael;)Lcom/twitter/android/notificationtimeline/f;

    move-result-object v1

    .line 264
    invoke-virtual {p0, v1}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->a(Lcom/twitter/android/client/j;)Lcom/twitter/app/common/list/TwitterListFragment;

    .line 266
    if-eqz p1, :cond_3

    .line 268
    const-string/jumbo v0, "spinning_gap_ids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v2

    .line 269
    if-eqz v2, :cond_2

    .line 270
    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_1

    aget-wide v4, v2, v0

    .line 271
    invoke-virtual {v1, v4, v5}, Lcom/twitter/android/notificationtimeline/f;->a(J)V

    .line 270
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 226
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 273
    :cond_1
    invoke-virtual {v1}, Lcom/twitter/android/notificationtimeline/f;->notifyDataSetChanged()V

    .line 275
    :cond_2
    const-string/jumbo v0, "state_show_stork"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 276
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->u()V

    .line 280
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->m:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "show_stork_text"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_4

    .line 281
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->u()V

    .line 282
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->m:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "show_stork_text"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 285
    :cond_4
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l;->a(Lcjr;)V

    .line 286
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 187
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 189
    invoke-static {p0, p1}, Lcom/twitter/android/notificationtimeline/ActivityFragmentSavedState;->a(Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 190
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->G()V

    .line 192
    iget-boolean v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->c:Z

    iget-boolean v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->w:Z

    iget-boolean v2, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->v:Z

    iget-boolean v3, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->a:Z

    .line 193
    invoke-static {}, Lcom/twitter/library/api/activity/g;->a()Lcom/twitter/library/api/activity/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/api/activity/g;->b()Z

    move-result v4

    .line 192
    invoke-static {v0, v1, v2, v3, v4}, Lcab$a;->a(ZZZZZ)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->f:I

    .line 195
    iget v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->f:I

    invoke-static {v0}, Lcom/twitter/library/api/activity/c;->a(I)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 197
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 198
    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->m:Landroid/content/SharedPreferences;

    .line 199
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->m:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->e:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 201
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 202
    new-instance v1, Lcom/twitter/android/notificationtimeline/h;

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->S:Lcom/twitter/library/client/p;

    iget v5, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->f:I

    .line 203
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lcom/twitter/android/notificationtimeline/h;-><init>(Landroid/content/Context;Lcom/twitter/library/client/v;Lcom/twitter/library/client/p;IJ)V

    iput-object v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->p:Lcom/twitter/android/notificationtimeline/h;

    .line 206
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/util/ak;->a(Lcom/twitter/model/core/TwitterUser;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->r:Z

    .line 207
    iget-boolean v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->r:Z

    if-eqz v0, :cond_1

    .line 208
    if-eqz p1, :cond_0

    .line 210
    const-string/jumbo v0, "is_device_storage_low"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->t:Z

    .line 212
    :cond_0
    new-instance v0, Lcom/twitter/android/platform/DeviceStorageLowReceiver;

    invoke-direct {v0}, Lcom/twitter/android/platform/DeviceStorageLowReceiver;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->s:Lcom/twitter/android/platform/DeviceStorageLowReceiver;

    .line 213
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->s:Lcom/twitter/android/platform/DeviceStorageLowReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string/jumbo v3, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-direct {v1, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Landroid/support/v4/app/FragmentActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 217
    :cond_1
    new-instance v0, Lcom/twitter/android/notificationtimeline/a;

    invoke-direct {v0}, Lcom/twitter/android/notificationtimeline/a;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->n:Lcom/twitter/android/notificationtimeline/a;

    .line 218
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 422
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->onDestroy()V

    .line 423
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->m:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->e:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 425
    iget-boolean v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->r:Z

    if-eqz v0, :cond_0

    .line 426
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 427
    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->s:Lcom/twitter/android/platform/DeviceStorageLowReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 428
    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->S:Lcom/twitter/library/client/p;

    .line 429
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 428
    invoke-static {v0, v2}, Lcom/twitter/android/util/ak;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lcom/twitter/android/util/ak$a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 431
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 435
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 436
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 437
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/notificationtimeline/f;

    invoke-virtual {v0}, Lcom/twitter/android/notificationtimeline/f;->a()Ljava/util/List;

    move-result-object v0

    .line 438
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 439
    const-string/jumbo v1, "spinning_gap_ids"

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->e(Ljava/util/Collection;)[J

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 442
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->i:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 443
    const-string/jumbo v0, "state_show_stork"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 445
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->s:Lcom/twitter/android/platform/DeviceStorageLowReceiver;

    if-eqz v0, :cond_2

    .line 446
    const-string/jumbo v0, "is_device_storage_low"

    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->t()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 448
    :cond_2
    new-instance v0, Lcom/twitter/android/notificationtimeline/ActivityFragmentSavedState;

    invoke-direct {v0, p0}, Lcom/twitter/android/notificationtimeline/ActivityFragmentSavedState;-><init>(Lcom/twitter/android/notificationtimeline/ActivityFragment;)V

    invoke-virtual {v0, p1}, Lcom/twitter/android/notificationtimeline/ActivityFragmentSavedState;->a(Landroid/os/Bundle;)V

    .line 449
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 413
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->onStop()V

    .line 414
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->q:Lcom/twitter/library/widget/TweetView;

    if-eqz v0, :cond_0

    .line 415
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->q:Lcom/twitter/library/widget/TweetView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setHighlighted(Z)V

    .line 416
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->q:Lcom/twitter/library/widget/TweetView;

    .line 418
    :cond_0
    return-void
.end method

.method public q()V
    .locals 4

    .prologue
    .line 962
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 963
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v0, v1, v3}, Lcom/twitter/android/settings/NotificationSettingsActivity;->a(Landroid/content/Context;JZ)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->startActivity(Landroid/content/Intent;)V

    .line 964
    return-void
.end method

.method protected q_()V
    .locals 2

    .prologue
    .line 391
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->ad()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 392
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->s()V

    .line 394
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->p:Lcom/twitter/android/notificationtimeline/h;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/notificationtimeline/h;->a(Z)V

    .line 396
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->i:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 397
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->i:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 400
    :cond_1
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment;->d:Lcom/twitter/library/client/u;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/u;)V

    .line 403
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/app/main/MainActivity;

    if-eqz v0, :cond_2

    .line 404
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/main/MainActivity;

    invoke-virtual {v0}, Lcom/twitter/app/main/MainActivity;->r()Lcom/twitter/app/main/a;

    move-result-object v0

    .line 405
    invoke-virtual {v0}, Lcom/twitter/app/main/a;->a()V

    .line 408
    :cond_2
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->q_()V

    .line 409
    return-void
.end method
