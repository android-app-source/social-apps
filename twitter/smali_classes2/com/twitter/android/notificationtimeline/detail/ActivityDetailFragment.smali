.class public Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;
.super Lcom/twitter/app/common/list/TwitterListFragment;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/list/TwitterListFragment",
        "<",
        "Lads;",
        "Lcjr",
        "<",
        "Lads;",
        ">;>;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:J

.field private d:Lcom/twitter/model/util/FriendshipCache;

.field private e:I

.field private f:I

.field private g:[I

.field private h:Z

.field private i:Lcom/twitter/android/widget/ap;

.field private j:Lcom/twitter/android/UsersAdapter;

.field private k:Lcom/twitter/android/cv;

.field private l:Lcom/twitter/model/core/Tweet;

.field private m:Z

.field private n:Lcom/twitter/android/cp;

.field private o:Lcom/twitter/android/ar;

.field private final p:Lcom/twitter/android/av;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/av",
            "<",
            "Landroid/view/View;",
            "Lcom/twitter/model/core/Tweet;",
            ">;"
        }
    .end annotation
.end field

.field private final q:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final r:Lcom/twitter/ui/user/BaseUserView$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 94
    invoke-direct {p0}, Lcom/twitter/app/common/list/TwitterListFragment;-><init>()V

    .line 120
    iput v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->e:I

    .line 121
    iput v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->f:I

    .line 145
    new-instance v0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment$1;-><init>(Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;)V

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->p:Lcom/twitter/android/av;

    .line 154
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->q:Ljava/util/Set;

    .line 156
    new-instance v0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment$2;

    invoke-direct {v0, p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment$2;-><init>(Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;)V

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->r:Lcom/twitter/ui/user/BaseUserView$a;

    return-void
.end method

.method private a(Lcom/twitter/model/core/Tweet;Lcom/twitter/internal/android/widget/ToolBar;)Lazv;
    .locals 2

    .prologue
    .line 803
    if-eqz p1, :cond_0

    .line 804
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->o:Lcom/twitter/android/ar;

    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->A()Z

    move-result v1

    invoke-virtual {v0, p2, v1}, Lcom/twitter/android/ar;->a(Lcom/twitter/internal/android/widget/ToolBar;Z)Lazv;

    move-result-object v0

    .line 806
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->o:Lcom/twitter/android/ar;

    invoke-virtual {v0, p2}, Lcom/twitter/android/ar;->a(Lcom/twitter/internal/android/widget/ToolBar;)Lazv;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;)Lcom/twitter/android/cp;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->n:Lcom/twitter/android/cp;

    return-object v0
.end method

.method private static a(Lcom/twitter/android/UsersAdapter;)Lcom/twitter/util/collection/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/UsersAdapter;",
            ")",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x3

    .line 340
    invoke-virtual {p0}, Lcom/twitter/android/UsersAdapter;->i()Landroid/database/Cursor;

    move-result-object v0

    .line 341
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_1

    .line 342
    :cond_0
    const/4 v0, 0x0

    .line 347
    :goto_0
    return-object v0

    .line 345
    :cond_1
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 346
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 347
    :goto_1
    invoke-static {v1, v0}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    goto :goto_0

    .line 346
    :cond_2
    const-string/jumbo v0, ""

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 690
    iget v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a:I

    packed-switch v0, :pswitch_data_0

    .line 730
    :cond_0
    :goto_0
    :pswitch_0
    return-object p1

    .line 692
    :pswitch_1
    const-string/jumbo p1, "favorited_you"

    goto :goto_0

    .line 695
    :pswitch_2
    const-string/jumbo p1, "favorited_retweet"

    goto :goto_0

    .line 698
    :pswitch_3
    const-string/jumbo p1, "favorited_mention"

    goto :goto_0

    .line 701
    :pswitch_4
    const-string/jumbo p1, "retweeted_you"

    goto :goto_0

    .line 704
    :pswitch_5
    const-string/jumbo p1, "retweeted_retweet"

    goto :goto_0

    .line 707
    :pswitch_6
    const-string/jumbo p1, "retweeted_mention"

    goto :goto_0

    .line 710
    :pswitch_7
    if-eqz p2, :cond_0

    const-string/jumbo p1, "followed_you"

    goto :goto_0

    .line 713
    :pswitch_8
    if-eqz p2, :cond_0

    const-string/jumbo p1, "joined_twitter"

    goto :goto_0

    .line 716
    :pswitch_9
    const-string/jumbo p1, "media_tagged_you"

    goto :goto_0

    .line 719
    :pswitch_a
    const-string/jumbo p1, "favorited_media_tag"

    goto :goto_0

    .line 722
    :pswitch_b
    const-string/jumbo p1, "retweeted_media_tag"

    goto :goto_0

    .line 690
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_2
        :pswitch_6
        :pswitch_3
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method private a(Landroid/view/View;J)V
    .locals 6

    .prologue
    .line 563
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/db;

    iget-object v0, v0, Lcom/twitter/android/db;->c:Lcom/twitter/ui/user/BaseUserView;

    .line 564
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "user_id"

    .line 565
    invoke-virtual {v1, v2, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "screen_name"

    .line 566
    invoke-virtual {v0}, Lcom/twitter/ui/user/BaseUserView;->getUserName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 567
    const-string/jumbo v2, "association"

    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 568
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    const/4 v3, 0x5

    .line 569
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-wide v4, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a_:J

    .line 570
    invoke-virtual {v0, v4, v5}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    .line 567
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 572
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->d:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/model/util/FriendshipCache;->j(J)Ljava/lang/Integer;

    move-result-object v0

    .line 573
    if-eqz v0, :cond_0

    .line 574
    const-string/jumbo v2, "friendship"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 576
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 577
    return-void
.end method

.method private a(Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 669
    const/4 v0, 0x0

    invoke-direct {p0, v0, v4}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 670
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v4, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, ":::unfollow"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    .line 671
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 672
    invoke-virtual {v0, p1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 670
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 673
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;Lcom/twitter/ui/user/UserView;J)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a(Lcom/twitter/ui/user/UserView;J)V

    return-void
.end method

.method private a(Lcom/twitter/ui/user/UserView;J)V
    .locals 8

    .prologue
    .line 612
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getScribeItem()Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v7

    .line 613
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getPromotedContent()Lcgi;

    move-result-object v6

    .line 614
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 615
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->q:Ljava/util/Set;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 616
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->S:Lcom/twitter/library/client/p;

    new-instance v1, Lbhs;

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Lbhs;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 619
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->d:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/model/util/FriendshipCache;->c(J)V

    .line 620
    invoke-direct {p0, v7}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a(Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 631
    :goto_0
    return-void

    .line 622
    :cond_1
    if-eqz v6, :cond_2

    .line 623
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->S:Lcom/twitter/library/client/p;

    new-instance v1, Lbhq;

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Lbhq;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 628
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->d:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/model/util/FriendshipCache;->b(J)V

    .line 629
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/db;

    iget v0, v0, Lcom/twitter/android/db;->f:I

    invoke-static {v0}, Lcom/twitter/model/core/g;->c(I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v0, v7}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a(Ljava/lang/Boolean;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    goto :goto_0

    .line 626
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->q:Ljava/util/Set;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private a(Ljava/lang/Boolean;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 651
    const/4 v0, 0x0

    invoke-direct {p0, v0, v5}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 652
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 653
    invoke-virtual {v0, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    new-array v2, v5, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ":::follow"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    .line 654
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 656
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 657
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 658
    new-array v2, v5, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, ":::follow_back"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v6

    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 659
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 661
    :cond_0
    return-void
.end method

.method private b(J)V
    .locals 3

    .prologue
    .line 681
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 682
    if-eqz v0, :cond_0

    .line 683
    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->n:Lcom/twitter/android/cp;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "::stream::results"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, p2, v0}, Lcom/twitter/android/cp;->a(JLjava/lang/String;)V

    .line 685
    :cond_0
    return-void
.end method

.method private f()Lcom/twitter/android/UsersAdapter;
    .locals 7

    .prologue
    .line 302
    new-instance v0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment$a;

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0200b0

    iget-object v3, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->r:Lcom/twitter/ui/user/BaseUserView$a;

    iget-object v4, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->d:Lcom/twitter/model/util/FriendshipCache;

    const/4 v5, 0x0

    iget-boolean v6, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->h:Z

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment$a;-><init>(Landroid/content/Context;ILcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/android/UsersAdapter$CheckboxConfig;Z)V

    return-object v0
.end method

.method private k()V
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 307
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->aK()Lcom/twitter/app/common/base/TwitterFragmentActivity;

    move-result-object v0

    .line 308
    if-eqz v0, :cond_0

    .line 309
    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->j:Lcom/twitter/android/UsersAdapter;

    invoke-virtual {v1}, Lcom/twitter/android/UsersAdapter;->getCount()I

    move-result v1

    .line 311
    iget-object v2, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->j:Lcom/twitter/android/UsersAdapter;

    invoke-static {v2}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a(Lcom/twitter/android/UsersAdapter;)Lcom/twitter/util/collection/Pair;

    move-result-object v2

    .line 312
    if-nez v2, :cond_1

    .line 336
    :cond_0
    :goto_0
    return-void

    .line 316
    :cond_1
    packed-switch v1, :pswitch_data_0

    .line 331
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0036

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v5, v6

    add-int/lit8 v1, v1, -0x1

    .line 332
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v7

    .line 331
    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 332
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v2

    .line 330
    invoke-static {v1, v2}, Lcom/twitter/util/b;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 319
    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0a0035

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v4, v6

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 320
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v2

    .line 318
    invoke-static {v1, v2}, Lcom/twitter/util/b;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 325
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0a0037

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v4, v6

    .line 326
    invoke-virtual {v2}, Lcom/twitter/util/collection/Pair;->b()Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v4, v7

    .line 325
    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 326
    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v2

    .line 324
    invoke-static {v1, v2}, Lcom/twitter/util/b;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 316
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private q()Lcom/twitter/internal/android/widget/ToolBar;
    .locals 1

    .prologue
    .line 559
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->aH()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->d()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 639
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0, v1, v2}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string/jumbo v2, ":tweet:link:open_link"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a(JJ)V
    .locals 1

    .prologue
    .line 373
    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/app/common/list/TwitterListFragment;->a(JJ)V

    .line 374
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->b(J)V

    .line 375
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->n:Lcom/twitter/android/cp;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/cp;->a(J)V

    .line 376
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 501
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 524
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->j:Lcom/twitter/android/UsersAdapter;

    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->k()Lcjt;

    move-result-object v0

    invoke-interface {v0}, Lcjt;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->k:Lcom/twitter/android/cv;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->k:Lcom/twitter/android/cv;

    .line 525
    invoke-virtual {v0}, Lcom/twitter/android/cv;->k()Lcjt;

    move-result-object v0

    invoke-interface {v0}, Lcjt;->b()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    .line 526
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    .line 528
    if-eqz v0, :cond_4

    .line 529
    invoke-virtual {v1}, Lcom/twitter/app/common/list/l;->s()V

    .line 535
    :goto_2
    iget-boolean v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->m:Z

    if-eqz v0, :cond_1

    .line 536
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->k()V

    .line 538
    :cond_1
    return-void

    .line 503
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->j:Lcom/twitter/android/UsersAdapter;

    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->k()Lcjt;

    move-result-object v0

    new-instance v1, Lcbe;

    invoke-direct {v1, p2}, Lcbe;-><init>(Landroid/database/Cursor;)V

    invoke-interface {v0, v1}, Lcjt;->a(Lcbi;)Lcbi;

    goto :goto_0

    .line 507
    :pswitch_2
    if-eqz p2, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 508
    sget-object v0, Lbtc;->a:Lbtc;

    invoke-virtual {v0, p2}, Lbtc;->a(Landroid/database/Cursor;)Lcom/twitter/model/core/Tweet$a;

    move-result-object v0

    .line 509
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet$a;->a()Lcom/twitter/model/core/Tweet;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->l:Lcom/twitter/model/core/Tweet;

    .line 511
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->q()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    .line 512
    if-eqz v0, :cond_2

    .line 513
    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->l:Lcom/twitter/model/core/Tweet;

    invoke-direct {p0, v1, v0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/internal/android/widget/ToolBar;)Lazv;

    .line 516
    :cond_2
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->k:Lcom/twitter/android/cv;

    invoke-virtual {v0, p2}, Lcom/twitter/android/cv;->c(Landroid/database/Cursor;)V

    goto :goto_0

    .line 525
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 531
    :cond_4
    invoke-virtual {v1}, Lcom/twitter/app/common/list/l;->t()V

    goto :goto_2

    .line 501
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 4

    .prologue
    .line 582
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->i:Lcom/twitter/android/widget/ap;

    if-nez v0, :cond_1

    .line 603
    :cond_0
    :goto_0
    return-void

    .line 585
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->i:Lcom/twitter/android/widget/ap;

    invoke-virtual {v0, p3}, Lcom/twitter/android/widget/ap;->b(I)I

    move-result v0

    .line 586
    iget v1, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->e:I

    if-ne v0, v1, :cond_2

    .line 587
    invoke-direct {p0, p2, p4, p5}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a(Landroid/view/View;J)V

    goto :goto_0

    .line 588
    :cond_2
    iget v1, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->f:I

    if-ne v0, v1, :cond_0

    .line 589
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->k:Lcom/twitter/android/cv;

    invoke-virtual {v0, p3}, Lcom/twitter/android/cv;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cd;

    .line 590
    if-eqz v0, :cond_0

    .line 591
    iget v1, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 592
    iget-object v1, v0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    const/16 v2, 0x10

    iput v2, v1, Lcom/twitter/model/core/Tweet;->f:I

    .line 594
    :cond_3
    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->j:Lcom/twitter/android/UsersAdapter;

    invoke-static {v1}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a(Lcom/twitter/android/UsersAdapter;)Lcom/twitter/util/collection/Pair;

    move-result-object v1

    .line 595
    if-eqz v1, :cond_4

    .line 596
    iget-object v2, v0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v1}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v2, Lcom/twitter/model/core/Tweet;->g:Ljava/lang/String;

    .line 598
    :cond_4
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/TweetActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "tw"

    iget-object v0, v0, Lcom/twitter/android/timeline/cd;->b:Lcom/twitter/model/core/Tweet;

    .line 599
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    .line 600
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 598
    invoke-virtual {p0, v0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 1

    .prologue
    .line 218
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 219
    const v0, 0x7f04001f

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    .line 220
    return-void
.end method

.method protected a(Z)V
    .locals 6

    .prologue
    .line 413
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Z)V

    .line 414
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    .line 415
    iget-object v2, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->g:[I

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    .line 416
    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 415
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 418
    :cond_0
    return-void
.end method

.method public a(Lcmm;)Z
    .locals 3

    .prologue
    .line 786
    invoke-interface {p1}, Lcmm;->a()I

    move-result v0

    .line 788
    const v1, 0x7f1308b4

    if-ne v0, v1, :cond_1

    .line 789
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->l:Lcom/twitter/model/core/Tweet;

    if-eqz v0, :cond_1

    .line 790
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->l:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 791
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->o:Lcom/twitter/android/ar;

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->l:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/ar;->b(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;)V

    .line 795
    :goto_0
    const/4 v0, 0x1

    .line 798
    :goto_1
    return v0

    .line 793
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->o:Lcom/twitter/android/ar;

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->l:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/ar;->a(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;)V

    goto :goto_0

    .line 798
    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcmm;)Z

    move-result v0

    goto :goto_1
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 777
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcmr;)Z

    .line 779
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->o:Lcom/twitter/android/ar;

    invoke-virtual {v0, p1}, Lcom/twitter/android/ar;->a(Lcmr;)V

    .line 781
    const/4 v0, 0x1

    return v0
.end method

.method protected ar_()V
    .locals 6

    .prologue
    .line 422
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->ar_()V

    .line 423
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    .line 424
    iget-object v2, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->g:[I

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    .line 425
    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 424
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 427
    :cond_0
    return-void
.end method

.method protected b()V
    .locals 0

    .prologue
    .line 352
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->b()V

    .line 353
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->ar_()V

    .line 354
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 225
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 227
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->i:Lcom/twitter/android/widget/ap;

    if-nez v0, :cond_0

    .line 228
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->aK()Lcom/twitter/app/common/base/TwitterFragmentActivity;

    move-result-object v1

    .line 237
    iget v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a:I

    packed-switch v0, :pswitch_data_0

    .line 287
    :pswitch_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 240
    :pswitch_1
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->f()Lcom/twitter/android/UsersAdapter;

    move-result-object v3

    .line 241
    new-instance v1, Lcom/twitter/android/widget/ap;

    new-array v0, v7, [Landroid/widget/BaseAdapter;

    aput-object v3, v0, v6

    invoke-direct {v1, v0}, Lcom/twitter/android/widget/ap;-><init>([Landroid/widget/BaseAdapter;)V

    .line 243
    new-array v0, v7, [I

    aput v6, v0, v6

    .line 246
    const/4 v2, -0x1

    .line 290
    :goto_0
    iput-object v3, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->j:Lcom/twitter/android/UsersAdapter;

    .line 291
    iput v6, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->e:I

    .line 292
    iput-object v4, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->k:Lcom/twitter/android/cv;

    .line 293
    iput v2, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->f:I

    .line 294
    iput-object v1, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->i:Lcom/twitter/android/widget/ap;

    .line 295
    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->g:[I

    .line 297
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->i:Lcom/twitter/android/widget/ap;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 298
    return-void

    .line 258
    :pswitch_2
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->f()Lcom/twitter/android/UsersAdapter;

    move-result-object v8

    .line 259
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->T:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/client/k;->a(Landroid/content/Context;)Lcom/twitter/android/client/k;

    move-result-object v0

    .line 260
    invoke-virtual {v0}, Lcom/twitter/android/client/k;->a()Z

    move-result v2

    .line 261
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v5

    .line 262
    new-instance v0, Lbxc$a;

    invoke-direct {v0}, Lbxc$a;-><init>()V

    const-string/jumbo v3, "tweet:::platform_photo_card:click"

    .line 263
    invoke-virtual {v0, v3}, Lbxc$a;->c(Ljava/lang/String;)Lbxc$a;

    move-result-object v0

    const-string/jumbo v3, "tweet:::platform_player_card:click"

    .line 264
    invoke-virtual {v0, v3}, Lbxc$a;->d(Ljava/lang/String;)Lbxc$a;

    move-result-object v0

    const-string/jumbo v3, "tweet"

    const-string/jumbo v9, "avatar"

    const-string/jumbo v10, "profile_click"

    .line 265
    invoke-static {v5, v3, v9, v10}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lbxc$a;->a(Ljava/lang/String;)Lbxc$a;

    move-result-object v0

    .line 267
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->r()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lbxc$a;->b(Ljava/lang/String;)Lbxc$a;

    move-result-object v0

    .line 268
    invoke-virtual {v0}, Lbxc$a;->a()Lbxc;

    move-result-object v0

    .line 269
    new-instance v3, Lcom/twitter/android/ct;

    invoke-direct {v3, p0, v5, v4, v0}, Lcom/twitter/android/ct;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Lbxc;)V

    .line 271
    new-instance v0, Lcom/twitter/android/cv;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/cv;-><init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;ZLcom/twitter/library/view/d;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 273
    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->p:Lcom/twitter/android/av;

    invoke-virtual {v0, v1}, Lcom/twitter/android/cv;->b(Lcom/twitter/android/av;)V

    .line 274
    new-instance v2, Lcom/twitter/android/widget/ap;

    new-array v1, v11, [Landroid/widget/BaseAdapter;

    aput-object v0, v1, v6

    aput-object v8, v1, v7

    invoke-direct {v2, v1}, Lcom/twitter/android/widget/ap;-><init>([Landroid/widget/BaseAdapter;)V

    .line 277
    new-array v1, v11, [I

    fill-array-data v1, :array_0

    move-object v4, v0

    move-object v3, v8

    move-object v0, v1

    move-object v1, v2

    move v2, v6

    move v6, v7

    .line 278
    goto :goto_0

    .line 237
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 277
    :array_0
    .array-data 4
        0x2
        0x0
    .end array-data
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 389
    packed-switch p1, :pswitch_data_0

    .line 409
    :cond_0
    :goto_0
    return-void

    .line 391
    :pswitch_0
    const/4 v0, -0x1

    if-ne v0, p2, :cond_0

    if-eqz p3, :cond_0

    .line 392
    const-string/jumbo v0, "user_id"

    invoke-virtual {p3, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 393
    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    const-string/jumbo v2, "friendship"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 394
    const-string/jumbo v2, "friendship"

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 396
    iget-object v3, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->d:Lcom/twitter/model/util/FriendshipCache;

    .line 397
    invoke-virtual {v3, v0, v1, v2}, Lcom/twitter/model/util/FriendshipCache;->a(JI)Z

    move-result v4

    if-nez v4, :cond_0

    .line 398
    invoke-virtual {v3, v0, v1, v2}, Lcom/twitter/model/util/FriendshipCache;->b(JI)V

    .line 399
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->i:Lcom/twitter/android/widget/ap;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ap;->notifyDataSetChanged()V

    goto :goto_0

    .line 389
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x5

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 172
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 174
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->H()Lcom/twitter/app/common/list/i;

    move-result-object v0

    .line 175
    const-string/jumbo v1, "event_type"

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/i;->b(Ljava/lang/String;)I

    move-result v1

    .line 176
    iput v1, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a:I

    .line 177
    const-string/jumbo v1, "user_tag"

    invoke-virtual {v0, v1, v8, v9}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->b:J

    .line 178
    const-string/jumbo v1, "status_tag"

    invoke-virtual {v0, v1, v8, v9}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->c:J

    .line 179
    iput-boolean v4, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->m:Z

    .line 181
    if-eqz p1, :cond_2

    .line 182
    const-string/jumbo v0, "friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 183
    const-string/jumbo v0, "friendship_cache"

    .line 184
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 183
    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/util/FriendshipCache;

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->d:Lcom/twitter/model/util/FriendshipCache;

    .line 188
    :goto_0
    const-string/jumbo v0, "hide_action_button"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->h:Z

    .line 202
    :cond_0
    :goto_1
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    .line 203
    invoke-virtual {v0, v6}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v1, "connect"

    .line 204
    invoke-direct {p0, v1, v4}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 202
    invoke-virtual {p0, v0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 205
    new-instance v0, Lcom/twitter/android/ar;

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/cr;

    invoke-direct {v2}, Lcom/twitter/android/cr;-><init>()V

    .line 206
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/ar;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/android/as;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->o:Lcom/twitter/android/ar;

    .line 207
    return-void

    .line 186
    :cond_1
    new-instance v0, Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/model/util/FriendshipCache;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->d:Lcom/twitter/model/util/FriendshipCache;

    goto :goto_0

    .line 190
    :cond_2
    const-string/jumbo v1, "hide_action_button"

    invoke-virtual {v0, v1, v4}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->h:Z

    .line 191
    new-instance v0, Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/model/util/FriendshipCache;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->d:Lcom/twitter/model/util/FriendshipCache;

    .line 192
    iget v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a:I

    if-eq v0, v6, :cond_3

    iget v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a:I

    const/16 v1, 0xd

    if-ne v0, v1, :cond_4

    .line 193
    :cond_3
    iput-boolean v5, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->m:Z

    .line 195
    :cond_4
    const/4 v0, 0x0

    invoke-direct {p0, v0, v4}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 196
    if-eqz v0, :cond_0

    .line 197
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 198
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    aput-object v0, v2, v5

    const-string/jumbo v0, ":::impression"

    aput-object v0, v2, v4

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_1
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    const/4 v8, 0x0

    .line 436
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 437
    packed-switch p1, :pswitch_data_0

    .line 496
    :cond_0
    :goto_0
    :pswitch_0
    return-object v0

    .line 439
    :pswitch_1
    new-instance v0, Lcom/twitter/util/android/d;

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v4, Lcom/twitter/database/schema/a$y;->p:Landroid/net/Uri;

    .line 440
    invoke-static {v4, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4, v2, v3}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lbun;->a:[Ljava/lang/String;

    const-string/jumbo v4, "user_groups_tag=?"

    new-array v5, v5, [Ljava/lang/String;

    iget-wide v6, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->b:J

    .line 445
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    const-string/jumbo v6, "_id ASC"

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    invoke-virtual {v0, v8}, Lcom/twitter/util/android/d;->a(Z)Lcom/twitter/util/android/d;

    move-result-object v0

    goto :goto_0

    .line 451
    :pswitch_2
    iget v1, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a:I

    packed-switch v1, :pswitch_data_1

    :pswitch_3
    move-object v2, v0

    .line 483
    :goto_1
    if-eqz v2, :cond_0

    .line 484
    new-instance v0, Lcom/twitter/util/android/d;

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lbuj;->a:[Ljava/lang/String;

    const-string/jumbo v4, "status_groups_tag=?"

    new-array v5, v5, [Ljava/lang/String;

    iget-wide v6, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->c:J

    .line 487
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    const-string/jumbo v6, "_id ASC"

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 459
    :pswitch_4
    sget-object v1, Lcom/twitter/database/schema/a$v;->j:Landroid/net/Uri;

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1, v2, v3}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    goto :goto_1

    .line 465
    :pswitch_5
    sget-object v1, Lcom/twitter/database/schema/a$v;->k:Landroid/net/Uri;

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1, v2, v3}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    goto :goto_1

    .line 437
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 451
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 94
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 542
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 555
    :goto_0
    :pswitch_0
    return-void

    .line 544
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->j:Lcom/twitter/android/UsersAdapter;

    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->k()Lcjt;

    move-result-object v0

    invoke-interface {v0, v1}, Lcjt;->a(Lcbi;)Lcbi;

    goto :goto_0

    .line 548
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->k:Lcom/twitter/android/cv;

    invoke-virtual {v0}, Lcom/twitter/android/cv;->k()Lcjt;

    move-result-object v0

    invoke-interface {v0, v1}, Lcjt;->a(Lcbi;)Lcbi;

    goto :goto_0

    .line 542
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 380
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 381
    const-string/jumbo v0, "hide_action_button"

    iget-boolean v1, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->h:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 382
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->d:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0}, Lcom/twitter/model/util/FriendshipCache;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 383
    const-string/jumbo v0, "friendship_cache"

    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->d:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 385
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 358
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->q:Ljava/util/Set;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->e(Ljava/util/Collection;)[J

    move-result-object v0

    .line 359
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 360
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 361
    if-eqz v0, :cond_0

    .line 362
    new-instance v4, Lbhr;

    iget-object v5, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->T:Landroid/content/Context;

    const/4 v6, 0x1

    invoke-direct {v4, v5, v1, v0, v6}, Lbhr;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;[JZ)V

    invoke-virtual {p0, v4, v7, v7}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 364
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->q:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 366
    :cond_0
    invoke-direct {p0, v2, v3}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->b(J)V

    .line 367
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->n:Lcom/twitter/android/cp;

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/cp;->a(J)V

    .line 368
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->onStop()V

    .line 369
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 211
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/list/TwitterListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 212
    new-instance v0, Lcom/twitter/android/cq;

    invoke-direct {v0}, Lcom/twitter/android/cq;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v2

    const/4 v3, -0x1

    .line 213
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v4

    .line 212
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/cq;->a(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ILcom/twitter/library/client/v;)Lcom/twitter/android/cp;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->n:Lcom/twitter/android/cp;

    .line 214
    return-void
.end method

.method protected p()V
    .locals 0

    .prologue
    .line 432
    return-void
.end method
