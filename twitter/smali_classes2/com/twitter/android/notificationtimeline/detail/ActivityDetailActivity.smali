.class public Lcom/twitter/android/notificationtimeline/detail/ActivityDetailActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 3

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 22
    const-string/jumbo v1, "title_res_id"

    const v2, 0x7f0a0034

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 24
    if-nez p1, :cond_0

    .line 25
    new-instance v1, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;

    invoke-direct {v1}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;-><init>()V

    .line 27
    invoke-static {v0}, Lcom/twitter/app/common/list/i$b;->a(Landroid/content/Intent;)Lcom/twitter/app/common/list/i$b;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/twitter/app/common/list/i$b;->e(Z)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/i$b;

    invoke-virtual {v0}, Lcom/twitter/app/common/list/i$b;->b()Lcom/twitter/app/common/list/i;

    move-result-object v0

    .line 26
    invoke-virtual {v1, v0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;->a(Lcom/twitter/app/common/base/b;)V

    .line 29
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v2, 0x7f1302e4

    .line 31
    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 32
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 34
    :cond_0
    return-void
.end method
