.class final Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment$a;
.super Lcom/twitter/android/UsersAdapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Z


# direct methods
.method constructor <init>(Landroid/content/Context;ILcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/android/UsersAdapter$CheckboxConfig;Z)V
    .locals 0
    .param p2    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserView;",
            ">;",
            "Lcom/twitter/model/util/FriendshipCache;",
            "Lcom/twitter/android/UsersAdapter$CheckboxConfig;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 742
    invoke-direct/range {p0 .. p5}, Lcom/twitter/android/UsersAdapter;-><init>(Landroid/content/Context;ILcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/android/UsersAdapter$CheckboxConfig;)V

    .line 743
    iput-boolean p6, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment$a;->a:Z

    .line 744
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 750
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 751
    const v1, 0x7f040029

    const/4 v2, 0x0

    .line 752
    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/ActivityUserView;

    .line 753
    invoke-virtual {p0, v0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment$a;->a(Lcom/twitter/ui/user/UserView;)Lcom/twitter/ui/user/UserView;

    .line 754
    return-object v0
.end method

.method public bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 735
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment$a;->a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/ui/user/UserView;)Lcom/twitter/ui/user/UserView;
    .locals 2

    .prologue
    .line 760
    const v0, 0x7f0200b0

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment$a;->b()Lcom/twitter/ui/user/BaseUserView$a;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/twitter/ui/user/UserView;->a(ILcom/twitter/ui/user/BaseUserView$a;)V

    .line 761
    const v0, 0x7f0200b1

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserView;->setFollowBackgroundResource(I)V

    .line 762
    new-instance v0, Lcom/twitter/android/db;

    invoke-direct {v0, p1}, Lcom/twitter/android/db;-><init>(Lcom/twitter/ui/user/BaseUserView;)V

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserView;->setTag(Ljava/lang/Object;)V

    .line 763
    return-object p1
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 6

    .prologue
    .line 768
    invoke-static {p3}, Lads;->a(Landroid/database/Cursor;)Lcom/twitter/model/core/TwitterUser;

    move-result-object v2

    move-object v0, p1

    .line 769
    check-cast v0, Lcom/twitter/android/widget/ActivityUserView;

    .line 770
    iget-wide v4, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v0, v4, v5}, Lcom/twitter/android/widget/ActivityUserView;->setUserId(J)V

    .line 771
    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment$a;->c()Lcom/twitter/model/util/FriendshipCache;

    move-result-object v1

    iget-boolean v3, p0, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment$a;->a:Z

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment$a;->d()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/widget/ActivityUserView;->a(Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/model/core/TwitterUser;ZJ)V

    .line 772
    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 735
    check-cast p3, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/notificationtimeline/detail/ActivityDetailFragment$a;->a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    return-void
.end method
