.class Lcom/twitter/android/notificationtimeline/ActivityFragment$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/notificationtimeline/ActivityFragment;->r()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field final synthetic b:Lcom/twitter/android/notificationtimeline/ActivityFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/notificationtimeline/ActivityFragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 0

    .prologue
    .line 300
    iput-object p1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment$3;->b:Lcom/twitter/android/notificationtimeline/ActivityFragment;

    iput-object p2, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment$3;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v7, 0x0

    .line 303
    .line 304
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laei$b;

    .line 305
    check-cast p1, Lcom/twitter/ui/widget/ActionButton;

    .line 306
    invoke-virtual {p1}, Lcom/twitter/ui/widget/ActionButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 307
    invoke-virtual {p1, v7}, Lcom/twitter/ui/widget/ActionButton;->setChecked(Z)V

    .line 308
    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment$3;->b:Lcom/twitter/android/notificationtimeline/ActivityFragment;

    invoke-static {v1}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->c(Lcom/twitter/android/notificationtimeline/ActivityFragment;)Lcom/twitter/model/util/FriendshipCache;

    move-result-object v1

    iget-object v2, v0, Laei$b;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->c(J)V

    .line 309
    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment$3;->b:Lcom/twitter/android/notificationtimeline/ActivityFragment;

    invoke-static {v1}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->e(Lcom/twitter/android/notificationtimeline/ActivityFragment;)Lcom/twitter/library/client/p;

    move-result-object v8

    new-instance v1, Lbhs;

    iget-object v2, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment$3;->b:Lcom/twitter/android/notificationtimeline/ActivityFragment;

    invoke-virtual {v2}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment$3;->b:Lcom/twitter/android/notificationtimeline/ActivityFragment;

    invoke-static {v3}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->d(Lcom/twitter/android/notificationtimeline/ActivityFragment;)Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-object v4, v0, Laei$b;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v4, v4, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-object v6, v0, Laei$b;->a:Lcom/twitter/model/core/TwitterUser;

    iget-object v6, v6, Lcom/twitter/model/core/TwitterUser;->A:Lcgi;

    invoke-direct/range {v1 .. v6}, Lbhs;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    invoke-virtual {v8, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 311
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    .line 312
    iget-object v2, v0, Laei$b;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-object v4, v0, Laei$b;->a:Lcom/twitter/model/core/TwitterUser;

    iget-object v4, v4, Lcom/twitter/model/core/TwitterUser;->A:Lcgi;

    iget-object v0, v0, Laei$b;->a:Lcom/twitter/model/core/TwitterUser;

    .line 313
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->h()Ljava/lang/String;

    move-result-object v0

    .line 312
    invoke-static {v1, v2, v3, v4, v0}, Lcom/twitter/library/scribe/c;->b(Lcom/twitter/analytics/feature/model/ClientEventLog;JLcgi;Ljava/lang/String;)V

    .line 314
    new-array v0, v9, [Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment$3;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v3, ""

    const-string/jumbo v4, "user"

    const-string/jumbo v5, "unfollow"

    .line 315
    invoke-static {v2, v3, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment$3;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 316
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 314
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 335
    :goto_0
    return-void

    .line 318
    :cond_0
    invoke-virtual {p1, v9}, Lcom/twitter/ui/widget/ActionButton;->setChecked(Z)V

    .line 319
    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment$3;->b:Lcom/twitter/android/notificationtimeline/ActivityFragment;

    .line 320
    invoke-static {v1}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->c(Lcom/twitter/android/notificationtimeline/ActivityFragment;)Lcom/twitter/model/util/FriendshipCache;

    move-result-object v1

    iget-object v2, v0, Laei$b;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->a(J)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment$3;->b:Lcom/twitter/android/notificationtimeline/ActivityFragment;

    .line 321
    invoke-static {v1}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->c(Lcom/twitter/android/notificationtimeline/ActivityFragment;)Lcom/twitter/model/util/FriendshipCache;

    move-result-object v1

    iget-object v2, v0, Laei$b;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->j(J)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 322
    :goto_1
    or-int/lit8 v1, v1, 0x1

    or-int/lit8 v1, v1, 0x40

    .line 323
    iget-object v2, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment$3;->b:Lcom/twitter/android/notificationtimeline/ActivityFragment;

    invoke-static {v2}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->c(Lcom/twitter/android/notificationtimeline/ActivityFragment;)Lcom/twitter/model/util/FriendshipCache;

    move-result-object v2

    iget-object v3, v0, Laei$b;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v4, v3, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v2, v4, v5, v1}, Lcom/twitter/model/util/FriendshipCache;->b(JI)V

    .line 324
    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment$3;->b:Lcom/twitter/android/notificationtimeline/ActivityFragment;

    invoke-static {v1}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->f(Lcom/twitter/android/notificationtimeline/ActivityFragment;)Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 325
    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment$3;->b:Lcom/twitter/android/notificationtimeline/ActivityFragment;

    invoke-static {v1}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->g(Lcom/twitter/android/notificationtimeline/ActivityFragment;)Lcom/twitter/library/client/p;

    move-result-object v8

    new-instance v1, Lbhq;

    iget-object v2, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment$3;->b:Lcom/twitter/android/notificationtimeline/ActivityFragment;

    invoke-virtual {v2}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v4, v0, Laei$b;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v4, v4, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-object v6, v0, Laei$b;->a:Lcom/twitter/model/core/TwitterUser;

    iget-object v6, v6, Lcom/twitter/model/core/TwitterUser;->A:Lcgi;

    invoke-direct/range {v1 .. v6}, Lbhq;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    invoke-virtual {v8, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 327
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    .line 328
    iget-object v2, v0, Laei$b;->a:Lcom/twitter/model/core/TwitterUser;

    iget-wide v2, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-object v4, v0, Laei$b;->a:Lcom/twitter/model/core/TwitterUser;

    iget-object v4, v4, Lcom/twitter/model/core/TwitterUser;->A:Lcgi;

    iget-object v0, v0, Laei$b;->a:Lcom/twitter/model/core/TwitterUser;

    .line 329
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->h()Ljava/lang/String;

    move-result-object v0

    .line 328
    invoke-static {v1, v2, v3, v4, v0}, Lcom/twitter/library/scribe/c;->b(Lcom/twitter/analytics/feature/model/ClientEventLog;JLcgi;Ljava/lang/String;)V

    .line 330
    new-array v0, v9, [Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment$3;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v3, ""

    const-string/jumbo v4, "user"

    const-string/jumbo v5, "follow"

    .line 331
    invoke-static {v2, v3, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    new-array v1, v9, [Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment$3;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v3, ""

    const-string/jumbo v4, "user"

    const-string/jumbo v5, "follow"

    .line 332
    invoke-static {v2, v3, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/ActivityFragment$3;->a:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 333
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 330
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    :cond_1
    move v1, v7

    .line 321
    goto/16 :goto_1
.end method
