.class public Lcom/twitter/android/notificationtimeline/e;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:J

.field private final b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;


# direct methods
.method public constructor <init>(JLcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-wide p1, p0, Lcom/twitter/android/notificationtimeline/e;->a:J

    .line 22
    iput-object p3, p0, Lcom/twitter/android/notificationtimeline/e;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 23
    return-void
.end method

.method private a(Lcaj;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 7

    .prologue
    .line 58
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/notificationtimeline/e;->a:J

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/notificationtimeline/e;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v6, p1, Lcaj;->o:Ljava/lang/String;

    .line 59
    invoke-static {v5, v6, p2, p3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    invoke-direct {v0, v2, v3, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    .line 60
    invoke-static {p1}, Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;->a(Lcaj;)Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 58
    return-object v0
.end method


# virtual methods
.method public a(Lcaj;)V
    .locals 2

    .prologue
    .line 27
    const-string/jumbo v0, ""

    const-string/jumbo v1, "click"

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/notificationtimeline/e;->a(Lcaj;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 28
    return-void
.end method

.method public a(Lcaj;Lcaf;)V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p2, Lcaf;->e:Lcad;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcaf;->e:Lcad;

    iget-object v0, v0, Lcad;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p2, Lcaf;->e:Lcad;

    iget-object v0, v0, Lcad;->a:Ljava/lang/String;

    const-string/jumbo v1, "click"

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/notificationtimeline/e;->a(Lcaj;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 43
    :cond_0
    return-void
.end method

.method public b(Lcaj;)V
    .locals 2

    .prologue
    .line 32
    iget-object v0, p1, Lcaj;->n:Lcae;

    .line 33
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcae;->c:Lcad;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcae;->c:Lcad;

    iget-object v1, v1, Lcad;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 34
    iget-object v0, v0, Lcae;->c:Lcad;

    iget-object v0, v0, Lcad;->a:Ljava/lang/String;

    const-string/jumbo v1, "click"

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/notificationtimeline/e;->a(Lcaj;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 36
    :cond_0
    return-void
.end method

.method public c(Lcaj;)V
    .locals 2

    .prologue
    .line 47
    const-string/jumbo v0, ""

    const-string/jumbo v1, "results"

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/notificationtimeline/e;->a(Lcaj;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 48
    return-void
.end method

.method public d(Lcaj;)V
    .locals 2

    .prologue
    .line 52
    const-string/jumbo v0, "unsupported_url"

    const-string/jumbo v1, "click"

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/notificationtimeline/e;->a(Lcaj;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 53
    return-void
.end method
