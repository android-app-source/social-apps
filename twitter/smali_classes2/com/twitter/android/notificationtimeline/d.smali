.class public Lcom/twitter/android/notificationtimeline/d;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/notificationtimeline/e;

.field private final b:J

.field private final c:Ljava/lang/String;

.field private final d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final e:Lcom/twitter/app/main/b;


# direct methods
.method public constructor <init>(Lcom/twitter/android/notificationtimeline/e;JLjava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/app/main/b;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/twitter/android/notificationtimeline/d;->a:Lcom/twitter/android/notificationtimeline/e;

    .line 42
    iput-wide p2, p0, Lcom/twitter/android/notificationtimeline/d;->b:J

    .line 43
    iput-object p4, p0, Lcom/twitter/android/notificationtimeline/d;->c:Ljava/lang/String;

    .line 44
    iput-object p5, p0, Lcom/twitter/android/notificationtimeline/d;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 45
    iput-object p6, p0, Lcom/twitter/android/notificationtimeline/d;->e:Lcom/twitter/app/main/b;

    .line 46
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/notificationtimeline/d;)Lcom/twitter/app/main/b;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/d;->e:Lcom/twitter/app/main/b;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/notificationtimeline/d;)Lcom/twitter/android/notificationtimeline/e;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/d;->a:Lcom/twitter/android/notificationtimeline/e;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;Lcaj;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 49
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/d;->a:Lcom/twitter/android/notificationtimeline/e;

    invoke-virtual {v0, p2}, Lcom/twitter/android/notificationtimeline/e;->a(Lcaj;)V

    .line 50
    iget-object v3, p2, Lcaj;->g:Ljava/lang/String;

    .line 51
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/UrlInterpreterActivity;->c_(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    iget-wide v4, p0, Lcom/twitter/android/notificationtimeline/d;->b:J

    iget-object v8, p0, Lcom/twitter/android/notificationtimeline/d;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/twitter/android/notificationtimeline/d;->c:Ljava/lang/String;

    move-object v1, p1

    move-object v6, v2

    move-object v7, v2

    invoke-static/range {v1 .. v10}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Lcom/twitter/library/client/BrowserDataSource;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ZLjava/lang/String;)V

    .line 58
    :goto_0
    return-void

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/d;->a:Lcom/twitter/android/notificationtimeline/e;

    invoke-virtual {v0, p2}, Lcom/twitter/android/notificationtimeline/e;->d(Lcaj;)V

    .line 56
    invoke-static {p1, v3}, Lcom/twitter/android/notificationtimeline/GenericActivityWebViewActivity;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Landroid/content/Context;Lcaj;)V
    .locals 4

    .prologue
    .line 62
    iget-object v1, p2, Lcaj;->n:Lcae;

    .line 63
    if-nez v1, :cond_0

    .line 89
    :goto_0
    return-void

    .line 67
    :cond_0
    iget-object v0, v1, Lcae;->b:Ljava/util/List;

    new-instance v2, Lcom/twitter/android/notificationtimeline/d$1;

    invoke-direct {v2, p0}, Lcom/twitter/android/notificationtimeline/d$1;-><init>(Lcom/twitter/android/notificationtimeline/d;)V

    invoke-static {v0, v2}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/List;Lcpp;)Ljava/util/List;

    move-result-object v0

    .line 75
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 76
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/CharSequence;

    invoke-interface {v0, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    new-instance v3, Lcom/twitter/android/notificationtimeline/d$2;

    invoke-direct {v3, p0, v1, p2}, Lcom/twitter/android/notificationtimeline/d$2;-><init>(Lcom/twitter/android/notificationtimeline/d;Lcae;Lcaj;)V

    .line 75
    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00f6

    const/4 v2, 0x0

    .line 86
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 88
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/d;->a:Lcom/twitter/android/notificationtimeline/e;

    invoke-virtual {v0, p2}, Lcom/twitter/android/notificationtimeline/e;->b(Lcaj;)V

    goto :goto_0
.end method
