.class Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$a;
.super Lcjr;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcjr",
        "<",
        "Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$b;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcjr;-><init>(Landroid/content/Context;)V

    .line 150
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$b;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 156
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040281

    const/4 v2, 0x0

    .line 157
    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 156
    return-object v0
.end method

.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 146
    check-cast p2, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$b;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$a;->a(Landroid/content/Context;Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$b;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$b;)V
    .locals 2

    .prologue
    .line 163
    const v0, 0x7f13061b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 164
    iget-object v1, p3, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    return-void
.end method

.method protected bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 146
    check-cast p3, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$b;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$a;->a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$b;)V

    return-void
.end method
