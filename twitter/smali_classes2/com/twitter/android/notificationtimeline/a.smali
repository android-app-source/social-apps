.class public Lcom/twitter/android/notificationtimeline/a;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/model/ScribeItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/a;->a:Ljava/util/Set;

    .line 31
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/a;->b:Ljava/util/List;

    .line 30
    return-void
.end method

.method public static a(I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 4

    .prologue
    .line 86
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 87
    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 88
    add-int/lit8 v1, p0, 0x1

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    .line 89
    return-object v0
.end method

.method public static a(II)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 3

    .prologue
    .line 79
    invoke-static {p0}, Lcom/twitter/android/notificationtimeline/a;->a(I)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    .line 80
    sget-object v0, Lbzx$a;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->x:Ljava/lang/String;

    .line 81
    return-object v1
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;II)Lcom/twitter/analytics/model/ScribeItem;
    .locals 4

    .prologue
    .line 95
    if-eqz p1, :cond_2

    .line 97
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    const-string/jumbo v0, "focal"

    .line 104
    :goto_0
    invoke-static {p0, p1, v0}, Lcom/twitter/library/scribe/b;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    .line 106
    iget-wide v2, p1, Lcom/twitter/model/core/Tweet;->u:J

    iput-wide v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 107
    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->z:Ljava/lang/String;

    iput-object v0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 108
    sget-object v0, Lbzx$a;->a:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->x:Ljava/lang/String;

    .line 109
    add-int/lit8 v0, p2, 0x1

    iput v0, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    move-object v0, v1

    .line 112
    :goto_1
    return-object v0

    .line 99
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/model/core/Tweet;->B()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    const-string/jumbo v0, "ancestor"

    goto :goto_0

    .line 102
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 112
    :cond_2
    invoke-static {p2, p3}, Lcom/twitter/android/notificationtimeline/a;->a(II)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Lbzv;II)Lcom/twitter/analytics/model/ScribeItem;
    .locals 4

    .prologue
    .line 60
    invoke-static {p1, p2}, Lcom/twitter/android/notificationtimeline/a;->a(II)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 61
    if-eqz p0, :cond_0

    .line 62
    iget-wide v2, p0, Lbzv;->b:J

    iput-wide v2, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 64
    :cond_0
    return-object v0
.end method

.method public static a(Lcom/twitter/model/core/TwitterUser;II)Lcom/twitter/analytics/model/ScribeItem;
    .locals 4

    .prologue
    .line 70
    invoke-static {p1, p2}, Lcom/twitter/android/notificationtimeline/a;->a(II)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 71
    if-eqz p0, :cond_0

    .line 72
    invoke-virtual {p0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 74
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a(JLcom/twitter/analytics/model/ScribeItem;ZLcom/twitter/model/core/Tweet;)V
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/a;->a:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 36
    if-eqz p5, :cond_0

    if-nez p4, :cond_0

    .line 37
    invoke-virtual {p5}, Lcom/twitter/model/core/Tweet;->ac()Lcgi;

    move-result-object v0

    .line 38
    if-eqz v0, :cond_0

    .line 41
    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->a:Lcom/twitter/library/api/PromotedEvent;

    invoke-static {v1, v0}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v0

    invoke-virtual {v0}, Lbsq$a;->a()Lbsq;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/a;->b:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    :cond_1
    return-void
.end method

.method public a(JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p1, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/a;->b:Ljava/util/List;

    .line 52
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(Ljava/util/List;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 51
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 53
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 55
    :cond_0
    return-void
.end method
