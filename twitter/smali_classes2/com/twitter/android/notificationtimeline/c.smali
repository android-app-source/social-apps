.class public Lcom/twitter/android/notificationtimeline/c;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;

.field private final b:Lcom/twitter/android/notificationtimeline/ActivityFragment;


# direct methods
.method public constructor <init>(Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;Lcom/twitter/android/notificationtimeline/ActivityFragment;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/twitter/android/notificationtimeline/c;->a:Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;

    .line 25
    iput-object p2, p0, Lcom/twitter/android/notificationtimeline/c;->b:Lcom/twitter/android/notificationtimeline/ActivityFragment;

    .line 26
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/notificationtimeline/c;)Lcom/twitter/android/notificationtimeline/ActivityFragment;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/c;->b:Lcom/twitter/android/notificationtimeline/ActivityFragment;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/c;->b:Lcom/twitter/android/notificationtimeline/ActivityFragment;

    invoke-virtual {v0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/c;->a:Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;

    invoke-virtual {v1}, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;->b()I

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/ui/widget/Tooltip;->a(Landroid/content/Context;I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v1, 0x7f0a05c0

    .line 77
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->a(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const/4 v1, 0x3

    .line 78
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->c(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    const v1, 0x7f130478

    .line 79
    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/Tooltip$a;->d(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/c;->b:Lcom/twitter/android/notificationtimeline/ActivityFragment;

    .line 80
    invoke-virtual {v1}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "muted_keywords_education_tooltip_tag"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/ui/widget/Tooltip$a;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)Lcom/twitter/ui/widget/Tooltip;

    .line 81
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/c;->b:Lcom/twitter/android/notificationtimeline/ActivityFragment;

    invoke-virtual {v0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/c;->a:Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;

    sget-object v1, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$Filter;->c:Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$Filter;

    invoke-virtual {v0, v1}, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;->a(Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$Filter;)V

    .line 91
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/c;->b:Lcom/twitter/android/notificationtimeline/ActivityFragment;

    invoke-virtual {v0}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/c;->a:Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;

    sget-object v1, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$Filter;->b:Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$Filter;

    invoke-virtual {v0, v1}, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;->a(Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$Filter;)V

    goto :goto_0

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/c;->a:Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;

    sget-object v1, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$Filter;->a:Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$Filter;

    invoke-virtual {v0, v1}, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;->a(Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$Filter;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Z)V
    .locals 6

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/c;->b()V

    .line 31
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/c;->a:Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;

    sget-object v1, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$Filter;->a:Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$Filter;

    new-instance v2, Lcom/twitter/android/notificationtimeline/c$1;

    invoke-direct {v2, p0}, Lcom/twitter/android/notificationtimeline/c$1;-><init>(Lcom/twitter/android/notificationtimeline/c;)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;->a(Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$Filter;Landroid/view/View$OnClickListener;)V

    .line 38
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/c;->a:Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;

    sget-object v1, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$Filter;->b:Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$Filter;

    new-instance v2, Lcom/twitter/android/notificationtimeline/c$2;

    invoke-direct {v2, p0}, Lcom/twitter/android/notificationtimeline/c$2;-><init>(Lcom/twitter/android/notificationtimeline/c;)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;->a(Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$Filter;Landroid/view/View$OnClickListener;)V

    .line 45
    sget-object v0, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$Filter;->c:Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$Filter;

    .line 46
    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/c;->a:Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;

    new-instance v2, Lcom/twitter/android/notificationtimeline/c$3;

    invoke-direct {v2, p0}, Lcom/twitter/android/notificationtimeline/c$3;-><init>(Lcom/twitter/android/notificationtimeline/c;)V

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;->a(Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$Filter;Landroid/view/View$OnClickListener;)V

    .line 52
    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/c;->a:Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;

    invoke-virtual {v1, v0, p1}, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;->a(Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$Filter;Z)V

    .line 54
    new-instance v0, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$b;

    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/c;->b:Lcom/twitter/android/notificationtimeline/ActivityFragment;

    const v2, 0x7f0a060d

    .line 55
    invoke-virtual {v1, v2}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/notificationtimeline/c$4;

    invoke-direct {v2, p0}, Lcom/twitter/android/notificationtimeline/c$4;-><init>(Lcom/twitter/android/notificationtimeline/c;)V

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate$b;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 61
    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/c;->a:Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/notificationtimeline/FilterBarViewDelegate;->a(Ljava/util/List;)V

    .line 63
    invoke-static {}, Lbph;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/c;->b:Lcom/twitter/android/notificationtimeline/ActivityFragment;

    .line 66
    invoke-virtual {v1}, Lcom/twitter/android/notificationtimeline/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string/jumbo v2, "muted_keywords_education_tooltip_fatigue"

    .line 67
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 66
    invoke-static {v1, v2, v4, v5}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lcom/twitter/android/util/h;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    invoke-direct {p0}, Lcom/twitter/android/notificationtimeline/c;->a()V

    .line 70
    invoke-virtual {v0}, Lcom/twitter/android/util/h;->b()V

    .line 73
    :cond_0
    return-void
.end method
