.class public Lcom/twitter/android/notificationtimeline/k;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:[I


# instance fields
.field private final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/twitter/android/notificationtimeline/j;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/twitter/android/notificationtimeline/j;",
            ">;"
        }
    .end annotation
.end field

.field private final d:[Lcom/twitter/internal/android/widget/TypefacesSpan;

.field private final e:[Lcom/twitter/internal/android/widget/TypefacesSpan;

.field private final f:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    new-array v0, v0, [I

    sput-object v0, Lcom/twitter/android/notificationtimeline/k;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x3

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Landroid/util/SparseArray;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/k;->b:Landroid/util/SparseArray;

    .line 28
    new-instance v0, Landroid/util/SparseArray;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/k;->c:Landroid/util/SparseArray;

    .line 38
    iput-object p2, p0, Lcom/twitter/android/notificationtimeline/k;->f:Landroid/content/res/Resources;

    .line 40
    new-array v0, v4, [Lcom/twitter/internal/android/widget/TypefacesSpan;

    new-instance v1, Lcom/twitter/internal/android/widget/TypefacesSpan;

    invoke-direct {v1, p1, v4}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/k;->d:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    .line 41
    new-array v0, v5, [Lcom/twitter/internal/android/widget/TypefacesSpan;

    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/k;->d:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    aget-object v1, v1, v2

    aput-object v1, v0, v2

    new-instance v1, Lcom/twitter/internal/android/widget/TypefacesSpan;

    invoke-direct {v1, p1, v4}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    aput-object v1, v0, v4

    const/4 v1, 0x2

    new-instance v2, Lcom/twitter/internal/android/widget/TypefacesSpan;

    invoke-direct {v2, p1, v4}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    aput-object v2, v0, v1

    new-instance v1, Lcom/twitter/internal/android/widget/TypefacesSpan;

    invoke-direct {v1, p1, v4}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/k;->e:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    .line 48
    new-array v0, v6, [I

    fill-array-data v0, :array_0

    sget-object v1, Lcom/twitter/android/notificationtimeline/k;->a:[I

    invoke-direct {p0, v6, v0, v1}, Lcom/twitter/android/notificationtimeline/k;->a(I[I[I)V

    .line 56
    new-array v0, v3, [I

    fill-array-data v0, :array_1

    new-array v1, v3, [I

    fill-array-data v1, :array_2

    invoke-direct {p0, v5, v0, v1}, Lcom/twitter/android/notificationtimeline/k;->a(I[I[I)V

    .line 66
    const/16 v0, 0x9

    new-array v1, v3, [I

    fill-array-data v1, :array_3

    new-array v2, v3, [I

    fill-array-data v2, :array_4

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/notificationtimeline/k;->a(I[I[I)V

    .line 76
    const/16 v0, 0xb

    new-array v1, v3, [I

    fill-array-data v1, :array_5

    new-array v2, v3, [I

    fill-array-data v2, :array_6

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/notificationtimeline/k;->a(I[I[I)V

    .line 86
    const/16 v0, 0x11

    new-array v1, v3, [I

    fill-array-data v1, :array_7

    new-array v2, v3, [I

    fill-array-data v2, :array_8

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/notificationtimeline/k;->a(I[I[I)V

    .line 96
    new-array v0, v3, [I

    fill-array-data v0, :array_9

    new-array v1, v3, [I

    fill-array-data v1, :array_a

    invoke-direct {p0, v4, v0, v1}, Lcom/twitter/android/notificationtimeline/k;->a(I[I[I)V

    .line 106
    const/16 v0, 0xa

    new-array v1, v3, [I

    fill-array-data v1, :array_b

    new-array v2, v3, [I

    fill-array-data v2, :array_c

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/notificationtimeline/k;->a(I[I[I)V

    .line 116
    const/16 v0, 0xc

    new-array v1, v3, [I

    fill-array-data v1, :array_d

    new-array v2, v3, [I

    fill-array-data v2, :array_e

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/notificationtimeline/k;->a(I[I[I)V

    .line 126
    const/16 v0, 0x10

    new-array v1, v3, [I

    fill-array-data v1, :array_f

    new-array v2, v3, [I

    fill-array-data v2, :array_10

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/notificationtimeline/k;->a(I[I[I)V

    .line 136
    new-array v0, v3, [I

    fill-array-data v0, :array_11

    new-array v1, v3, [I

    fill-array-data v1, :array_12

    invoke-direct {p0, v4, v0, v1}, Lcom/twitter/android/notificationtimeline/k;->b(I[I[I)V

    .line 146
    const/4 v0, 0x2

    new-array v1, v3, [I

    fill-array-data v1, :array_13

    new-array v2, v3, [I

    fill-array-data v2, :array_14

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/notificationtimeline/k;->b(I[I[I)V

    .line 156
    const/4 v0, 0x7

    new-array v1, v3, [I

    fill-array-data v1, :array_15

    new-array v2, v3, [I

    fill-array-data v2, :array_16

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/notificationtimeline/k;->b(I[I[I)V

    .line 166
    const/16 v0, 0x8

    new-array v1, v3, [I

    fill-array-data v1, :array_17

    new-array v2, v3, [I

    fill-array-data v2, :array_18

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/notificationtimeline/k;->b(I[I[I)V

    .line 176
    new-array v0, v3, [I

    fill-array-data v0, :array_19

    new-array v1, v3, [I

    fill-array-data v1, :array_1a

    invoke-direct {p0, v6, v0, v1}, Lcom/twitter/android/notificationtimeline/k;->b(I[I[I)V

    .line 186
    const/4 v0, 0x6

    new-array v1, v3, [I

    fill-array-data v1, :array_1b

    new-array v2, v3, [I

    fill-array-data v2, :array_1c

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/notificationtimeline/k;->b(I[I[I)V

    .line 196
    new-array v0, v3, [I

    fill-array-data v0, :array_1d

    new-array v1, v3, [I

    fill-array-data v1, :array_1e

    invoke-direct {p0, v3, v0, v1}, Lcom/twitter/android/notificationtimeline/k;->b(I[I[I)V

    .line 206
    new-array v0, v3, [I

    fill-array-data v0, :array_1f

    new-array v1, v3, [I

    fill-array-data v1, :array_20

    invoke-direct {p0, v5, v0, v1}, Lcom/twitter/android/notificationtimeline/k;->b(I[I[I)V

    .line 216
    const/16 v0, 0x9

    new-array v1, v3, [I

    fill-array-data v1, :array_21

    new-array v2, v3, [I

    fill-array-data v2, :array_22

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/notificationtimeline/k;->b(I[I[I)V

    .line 226
    const/16 v0, 0xa

    new-array v1, v3, [I

    fill-array-data v1, :array_23

    new-array v2, v3, [I

    fill-array-data v2, :array_24

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/notificationtimeline/k;->b(I[I[I)V

    .line 235
    return-void

    .line 48
    nop

    :array_0
    .array-data 4
        0x7f0a03ab
        0x7f0a03af
        0x7f0a03ac
        0x7f0a03aa
        0x7f0a03ad
    .end array-data

    .line 56
    :array_1
    .array-data 4
        0x7f0a079b
        0x7f0a07a3
        0x7f0a079f
    .end array-data

    :array_2
    .array-data 4
        0x7f0a079d
        0x7f0a07a5
        0x7f0a07a1
    .end array-data

    .line 66
    :array_3
    .array-data 4
        0x7f0a07aa
        0x7f0a07ae
        0x7f0a07ac
    .end array-data

    :array_4
    .array-data 4
        0x7f0a07ab
        0x7f0a07af
        0x7f0a07ad
    .end array-data

    .line 76
    :array_5
    .array-data 4
        0x7f0a0786
        0x7f0a078c
        0x7f0a0789
    .end array-data

    :array_6
    .array-data 4
        0x7f0a0788
        0x7f0a078e
        0x7f0a078b
    .end array-data

    .line 86
    :array_7
    .array-data 4
        0x7f0a0780
        0x7f0a0784
        0x7f0a0782
    .end array-data

    :array_8
    .array-data 4
        0x7f0a0781
        0x7f0a0785
        0x7f0a0783
    .end array-data

    .line 96
    :array_9
    .array-data 4
        0x7f0a046e
        0x7f0a0476
        0x7f0a0472
    .end array-data

    :array_a
    .array-data 4
        0x7f0a0470
        0x7f0a0478
        0x7f0a0474
    .end array-data

    .line 106
    :array_b
    .array-data 4
        0x7f0a047d
        0x7f0a0481
        0x7f0a047f
    .end array-data

    :array_c
    .array-data 4
        0x7f0a047e
        0x7f0a0482
        0x7f0a0480
    .end array-data

    .line 116
    :array_d
    .array-data 4
        0x7f0a0459
        0x7f0a045f
        0x7f0a045c
    .end array-data

    :array_e
    .array-data 4
        0x7f0a045b
        0x7f0a0461
        0x7f0a045e
    .end array-data

    .line 126
    :array_f
    .array-data 4
        0x7f0a0453
        0x7f0a0457
        0x7f0a0455
    .end array-data

    :array_10
    .array-data 4
        0x7f0a0454
        0x7f0a0458
        0x7f0a0456
    .end array-data

    .line 136
    :array_11
    .array-data 4
        0x7f0a047a
        0x7f0a047c
        0x7f0a047b
    .end array-data

    :array_12
    .array-data 4
        0x7f0a0471
        0x7f0a0479
        0x7f0a0475
    .end array-data

    .line 146
    :array_13
    .array-data 4
        0x7f0a07a7
        0x7f0a07a9
        0x7f0a07a8
    .end array-data

    :array_14
    .array-data 4
        0x7f0a079e
        0x7f0a07a6
        0x7f0a07a2
    .end array-data

    .line 156
    :array_15
    .array-data 4
        0x7f0a0468
        0x7f0a046a
        0x7f0a0469
    .end array-data

    :array_16
    .array-data 4
        0x7f0a046f
        0x7f0a0477
        0x7f0a0473
    .end array-data

    .line 166
    :array_17
    .array-data 4
        0x7f0a0795
        0x7f0a0797
        0x7f0a0796
    .end array-data

    :array_18
    .array-data 4
        0x7f0a079c
        0x7f0a07a4
        0x7f0a07a0
    .end array-data

    .line 176
    :array_19
    .array-data 4
        0x7f0a0465
        0x7f0a0467
        0x7f0a0466
    .end array-data

    :array_1a
    .array-data 4
        0x7f0a046b
        0x7f0a046d
        0x7f0a046c
    .end array-data

    .line 186
    :array_1b
    .array-data 4
        0x7f0a0792
        0x7f0a0794
        0x7f0a0793
    .end array-data

    :array_1c
    .array-data 4
        0x7f0a0798
        0x7f0a079a
        0x7f0a0799
    .end array-data

    .line 196
    :array_1d
    .array-data 4
        0x7f0a0462
        0x7f0a0464
        0x7f0a0463
    .end array-data

    :array_1e
    .array-data 4
        0x7f0a045a
        0x7f0a0460
        0x7f0a045d
    .end array-data

    .line 206
    :array_1f
    .array-data 4
        0x7f0a078f
        0x7f0a0791
        0x7f0a0790
    .end array-data

    :array_20
    .array-data 4
        0x7f0a0787
        0x7f0a078d
        0x7f0a078a
    .end array-data

    .line 216
    :array_21
    .array-data 4
        0x7f0a044d
        0x7f0a0451
        0x7f0a044f
    .end array-data

    :array_22
    .array-data 4
        0x7f0a044e
        0x7f0a0452
        0x7f0a0450
    .end array-data

    .line 226
    :array_23
    .array-data 4
        0x7f0a077a
        0x7f0a077e
        0x7f0a077c
    .end array-data

    :array_24
    .array-data 4
        0x7f0a077b
        0x7f0a077f
        0x7f0a077d
    .end array-data
.end method

.method private a(I[I[I)V
    .locals 7

    .prologue
    .line 268
    iget-object v6, p0, Lcom/twitter/android/notificationtimeline/k;->b:Landroid/util/SparseArray;

    new-instance v0, Lcom/twitter/android/notificationtimeline/j;

    iget-object v4, p0, Lcom/twitter/android/notificationtimeline/k;->d:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    iget-object v5, p0, Lcom/twitter/android/notificationtimeline/k;->e:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/notificationtimeline/j;-><init>(I[I[I[Lcom/twitter/internal/android/widget/TypefacesSpan;[Lcom/twitter/internal/android/widget/TypefacesSpan;)V

    invoke-virtual {v6, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 270
    return-void
.end method

.method private b(I[I[I)V
    .locals 7

    .prologue
    .line 274
    iget-object v6, p0, Lcom/twitter/android/notificationtimeline/k;->c:Landroid/util/SparseArray;

    new-instance v0, Lcom/twitter/android/notificationtimeline/j;

    iget-object v4, p0, Lcom/twitter/android/notificationtimeline/k;->d:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    iget-object v5, p0, Lcom/twitter/android/notificationtimeline/k;->e:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/notificationtimeline/j;-><init>(I[I[I[Lcom/twitter/internal/android/widget/TypefacesSpan;[Lcom/twitter/internal/android/widget/TypefacesSpan;)V

    invoke-virtual {v6, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 276
    return-void
.end method


# virtual methods
.method public a(ILjava/util/List;ILjava/util/List;ILjava/lang/String;J)Ljava/lang/CharSequence;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;I",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;I",
            "Ljava/lang/String;",
            "J)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 242
    invoke-static {p1, p7, p8, p4, p5}, Lcom/twitter/android/notificationtimeline/i;->a(IJLjava/util/List;I)I

    move-result v1

    .line 246
    if-nez v1, :cond_0

    .line 247
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/k;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/notificationtimeline/j;

    .line 252
    :goto_0
    if-nez v0, :cond_1

    .line 253
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Type not supported: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", Subtype: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    move-object v0, v6

    .line 261
    :goto_1
    return-object v0

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/k;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/notificationtimeline/j;

    goto :goto_0

    .line 258
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/k;->f:Landroid/content/res/Resources;

    move-object v2, p2

    move v3, p3

    move v4, p5

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/notificationtimeline/j;->a(Landroid/content/res/Resources;Ljava/util/List;IILjava/lang/String;)Ljava/lang/CharSequence;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    .line 259
    :catch_0
    move-exception v0

    .line 260
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    move-object v0, v6

    .line 261
    goto :goto_1
.end method
