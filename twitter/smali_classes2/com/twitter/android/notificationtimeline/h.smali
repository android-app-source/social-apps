.class public Lcom/twitter/android/notificationtimeline/h;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:J

.field private b:J

.field private c:J

.field private d:I

.field private e:J

.field private f:Z

.field private final g:Landroid/content/Context;

.field private final h:Lcom/twitter/library/client/v;

.field private final i:Lcom/twitter/library/client/p;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/v;Lcom/twitter/library/client/p;IJ)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/h;->g:Landroid/content/Context;

    .line 35
    iput-object p2, p0, Lcom/twitter/android/notificationtimeline/h;->h:Lcom/twitter/library/client/v;

    .line 36
    iput-object p3, p0, Lcom/twitter/android/notificationtimeline/h;->i:Lcom/twitter/library/client/p;

    .line 37
    invoke-virtual {p0, p4, p5, p6}, Lcom/twitter/android/notificationtimeline/h;->a(IJ)V

    .line 38
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/notificationtimeline/h;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/h;->g:Landroid/content/Context;

    return-object v0
.end method

.method private a(JZ)V
    .locals 9

    .prologue
    .line 103
    iput-wide p1, p0, Lcom/twitter/android/notificationtimeline/h;->a:J

    .line 104
    iget-wide v0, p0, Lcom/twitter/android/notificationtimeline/h;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/h;->g:Landroid/content/Context;

    iget-wide v2, p0, Lcom/twitter/android/notificationtimeline/h;->e:J

    invoke-static {v0, v2, v3, p1, p2}, Lcom/twitter/library/client/a;->b(Landroid/content/Context;JJ)Z

    .line 107
    :cond_0
    new-instance v1, Lcom/twitter/library/api/activity/b;

    iget-object v2, p0, Lcom/twitter/android/notificationtimeline/h;->g:Landroid/content/Context;

    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/h;->h:Lcom/twitter/library/client/v;

    iget-wide v4, p0, Lcom/twitter/android/notificationtimeline/h;->e:J

    .line 108
    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-boolean v6, p0, Lcom/twitter/android/notificationtimeline/h;->f:Z

    iget v7, p0, Lcom/twitter/android/notificationtimeline/h;->d:I

    move-wide v4, p1

    move v8, p3

    invoke-direct/range {v1 .. v8}, Lcom/twitter/library/api/activity/b;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JZIZ)V

    new-instance v0, Lcom/twitter/android/notificationtimeline/h$1;

    invoke-direct {v0, p0}, Lcom/twitter/android/notificationtimeline/h$1;-><init>(Lcom/twitter/android/notificationtimeline/h;)V

    .line 109
    invoke-virtual {v1, v0}, Lcom/twitter/library/api/activity/b;->a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/activity/b;

    .line 116
    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/h;->i:Lcom/twitter/library/client/p;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 117
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/twitter/android/notificationtimeline/h;->a:J

    return-wide v0
.end method

.method public a(IJ)V
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 41
    iput p1, p0, Lcom/twitter/android/notificationtimeline/h;->d:I

    .line 42
    iput-wide p2, p0, Lcom/twitter/android/notificationtimeline/h;->e:J

    .line 43
    iput-wide v2, p0, Lcom/twitter/android/notificationtimeline/h;->c:J

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/notificationtimeline/h;->f:Z

    .line 45
    cmp-long v0, p2, v2

    if-lez v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/h;->g:Landroid/content/Context;

    iget-wide v2, p0, Lcom/twitter/android/notificationtimeline/h;->e:J

    invoke-static {v0, v2, v3}, Lcom/twitter/library/client/a;->a(Landroid/content/Context;J)J

    move-result-wide v2

    .line 49
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    .line 50
    cmp-long v4, v2, v0

    if-lez v4, :cond_1

    .line 52
    iget-object v2, p0, Lcom/twitter/android/notificationtimeline/h;->g:Landroid/content/Context;

    iget-wide v4, p0, Lcom/twitter/android/notificationtimeline/h;->e:J

    invoke-static {v2, v4, v5, v0, v1}, Lcom/twitter/library/client/a;->a(Landroid/content/Context;JJ)V

    .line 54
    :goto_0
    iput-wide v0, p0, Lcom/twitter/android/notificationtimeline/h;->a:J

    iput-wide v0, p0, Lcom/twitter/android/notificationtimeline/h;->b:J

    .line 56
    :cond_0
    return-void

    :cond_1
    move-wide v0, v2

    goto :goto_0
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/twitter/android/notificationtimeline/h;->b:J

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/notificationtimeline/h;->b:J

    .line 67
    return-void
.end method

.method public a(JJ)V
    .locals 3

    .prologue
    .line 80
    iget-wide v0, p0, Lcom/twitter/android/notificationtimeline/h;->e:J

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 81
    iput-wide p3, p0, Lcom/twitter/android/notificationtimeline/h;->c:J

    .line 82
    iget-wide v0, p0, Lcom/twitter/android/notificationtimeline/h;->a:J

    cmp-long v0, p3, v0

    if-lez v0, :cond_0

    .line 83
    invoke-virtual {p0, p3, p4}, Lcom/twitter/android/notificationtimeline/h;->a(J)V

    .line 84
    const/4 v0, 0x1

    invoke-direct {p0, p3, p4, v0}, Lcom/twitter/android/notificationtimeline/h;->a(JZ)V

    .line 87
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 8

    .prologue
    .line 91
    iget-wide v0, p0, Lcom/twitter/android/notificationtimeline/h;->b:J

    invoke-direct {p0, v0, v1, p1}, Lcom/twitter/android/notificationtimeline/h;->a(JZ)V

    .line 93
    iget-wide v0, p0, Lcom/twitter/android/notificationtimeline/h;->b:J

    iget-wide v2, p0, Lcom/twitter/android/notificationtimeline/h;->c:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 94
    iget-wide v0, p0, Lcom/twitter/android/notificationtimeline/h;->b:J

    iput-wide v0, p0, Lcom/twitter/android/notificationtimeline/h;->c:J

    .line 95
    iget-wide v0, p0, Lcom/twitter/android/notificationtimeline/h;->c:J

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 96
    new-instance v2, Lcom/twitter/library/api/activity/h;

    iget-object v3, p0, Lcom/twitter/android/notificationtimeline/h;->g:Landroid/content/Context;

    iget-object v4, p0, Lcom/twitter/android/notificationtimeline/h;->h:Lcom/twitter/library/client/v;

    iget-wide v6, p0, Lcom/twitter/android/notificationtimeline/h;->e:J

    .line 97
    invoke-virtual {v4, v6, v7}, Lcom/twitter/library/client/v;->b(J)Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/twitter/library/api/activity/h;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    .line 98
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/h;->i:Lcom/twitter/library/client/p;

    invoke-virtual {v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 100
    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/notificationtimeline/h;->f:Z

    .line 74
    return-void
.end method
