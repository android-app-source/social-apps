.class public Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;
.super Lcom/twitter/analytics/model/ScribeItem;
.source "Twttr"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:J

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem$1;

    invoke-direct {v0}, Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem$1;-><init>()V

    sput-object v0, Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 7

    .prologue
    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;-><init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/twitter/analytics/model/ScribeItem;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;->a:Ljava/lang/String;

    .line 47
    iput-wide p2, p0, Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;->b:J

    .line 48
    iput-object p4, p0, Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;->c:Ljava/lang/String;

    .line 49
    iput-object p5, p0, Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;->d:Ljava/lang/String;

    .line 50
    iput-object p6, p0, Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;->e:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public static a(Lcaj;)Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;
    .locals 7

    .prologue
    .line 55
    new-instance v0, Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;

    iget-object v1, p0, Lcaj;->h:Ljava/lang/String;

    iget-wide v2, p0, Lcaj;->a:J

    iget-object v4, p0, Lcaj;->p:Ljava/lang/String;

    iget-object v5, p0, Lcaj;->q:Ljava/lang/String;

    iget-object v6, p0, Lcaj;->g:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;-><init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected a(Lcom/fasterxml/jackson/core/JsonGenerator;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    const-string/jumbo v0, "hash_key"

    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string/jumbo v0, "timestamp"

    iget-wide v2, p0, Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    .line 72
    const-string/jumbo v0, "sender_id"

    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string/jumbo v0, "impression_id"

    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string/jumbo v0, "entity_type"

    const-string/jumbo v1, "generic"

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string/jumbo v0, "tap_through_action"

    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 62
    iget-wide v0, p0, Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 63
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/scribe/GenericActivityScribeItem;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 66
    return-void
.end method
