.class public Lcom/twitter/android/notificationtimeline/f;
.super Lcjr;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/client/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcjr",
        "<",
        "Lbzz;",
        ">;",
        "Lcom/twitter/android/client/j;"
    }
.end annotation


# instance fields
.field private final a:Lael;

.field private final b:Landroid/view/View$OnClickListener;

.field private final c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final d:Lcom/twitter/library/client/v;

.field private final e:Lcom/twitter/android/notificationtimeline/a;

.field private final f:Laei;

.field private final g:Laee;

.field private final h:Laeg;

.field private final i:Laeh;

.field private final j:Laek;

.field private final k:Laej;


# direct methods
.method public constructor <init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/android/notificationtimeline/h;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/notificationtimeline/a;Landroid/view/View$OnClickListener;Lael;Lcom/twitter/app/main/b;)V
    .locals 8

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcjr;-><init>(Landroid/content/Context;)V

    .line 80
    iput-object p4, p0, Lcom/twitter/android/notificationtimeline/f;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 81
    iput-object p5, p0, Lcom/twitter/android/notificationtimeline/f;->e:Lcom/twitter/android/notificationtimeline/a;

    .line 82
    iput-object p6, p0, Lcom/twitter/android/notificationtimeline/f;->b:Landroid/view/View$OnClickListener;

    .line 83
    iput-object p7, p0, Lcom/twitter/android/notificationtimeline/f;->a:Lael;

    .line 84
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->d:Lcom/twitter/library/client/v;

    .line 86
    new-instance v0, Laei;

    iget-object v2, p0, Lcom/twitter/android/notificationtimeline/f;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v3, p0, Lcom/twitter/android/notificationtimeline/f;->b:Landroid/view/View$OnClickListener;

    move-object v1, p2

    move-object v4, p5

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Laei;-><init>(Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Landroid/view/View$OnClickListener;Lcom/twitter/android/notificationtimeline/a;Lcom/twitter/android/notificationtimeline/h;)V

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->f:Laei;

    .line 88
    new-instance v0, Laee;

    invoke-virtual {p0}, Lcom/twitter/android/notificationtimeline/f;->j()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/notificationtimeline/k;

    .line 89
    invoke-virtual {p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Lcom/twitter/android/notificationtimeline/k;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    iget-object v3, p0, Lcom/twitter/android/notificationtimeline/f;->d:Lcom/twitter/library/client/v;

    .line 90
    invoke-virtual {v3}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    move-object v3, p4

    move-object v4, p5

    move-object v5, p3

    invoke-direct/range {v0 .. v7}, Laee;-><init>(Landroid/content/res/Resources;Lcom/twitter/android/notificationtimeline/k;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/notificationtimeline/a;Lcom/twitter/android/notificationtimeline/h;J)V

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->g:Laee;

    .line 91
    new-instance v0, Laeg;

    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/f;->e:Lcom/twitter/android/notificationtimeline/a;

    invoke-direct {v0, v1, p3}, Laeg;-><init>(Lcom/twitter/android/notificationtimeline/a;Lcom/twitter/android/notificationtimeline/h;)V

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->h:Laeg;

    .line 92
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->d:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 93
    new-instance v1, Lcom/twitter/android/notificationtimeline/e;

    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v1, v2, v3, v0}, Lcom/twitter/android/notificationtimeline/e;-><init>(JLcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 94
    new-instance v7, Laeh;

    new-instance v0, Lcom/twitter/android/notificationtimeline/d;

    .line 95
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/notificationtimeline/f;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-object/from16 v6, p8

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/notificationtimeline/d;-><init>(Lcom/twitter/android/notificationtimeline/e;JLjava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/app/main/b;)V

    invoke-direct {v7, p5, p3, v0, v1}, Laeh;-><init>(Lcom/twitter/android/notificationtimeline/a;Lcom/twitter/android/notificationtimeline/h;Lcom/twitter/android/notificationtimeline/d;Lcom/twitter/android/notificationtimeline/e;)V

    iput-object v7, p0, Lcom/twitter/android/notificationtimeline/f;->i:Laeh;

    .line 97
    new-instance v0, Laek;

    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/f;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0, p7, p5, p3, v1}, Laek;-><init>(Lael;Lcom/twitter/android/notificationtimeline/a;Lcom/twitter/android/notificationtimeline/h;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->j:Laek;

    .line 99
    new-instance v0, Laej;

    iget-object v1, p0, Lcom/twitter/android/notificationtimeline/f;->c:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0, p7, p5, p3, v1}, Laej;-><init>(Lael;Lcom/twitter/android/notificationtimeline/a;Lcom/twitter/android/notificationtimeline/h;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    iput-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->k:Laej;

    .line 101
    return-void
.end method

.method private static a(Lcah;)I
    .locals 5

    .prologue
    .line 244
    iget-object v0, p0, Lcah;->b:Lcac;

    check-cast v0, Lcap;

    iget v1, v0, Lcap;->d:I

    .line 245
    packed-switch v1, :pswitch_data_0

    .line 269
    :pswitch_0
    sget-object v0, Lbzx$a;->a:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 270
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Item view type requested for unsupported event type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v0, :cond_0

    .line 271
    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 256
    :pswitch_1
    const/4 v0, 0x0

    .line 265
    :goto_1
    return v0

    .line 259
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_1

    .line 265
    :pswitch_3
    const/4 v0, 0x1

    goto :goto_1

    .line 271
    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 245
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private a(Lcah;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 137
    invoke-static {p1}, Lcom/twitter/android/notificationtimeline/f;->a(Lcah;)I

    move-result v0

    .line 138
    packed-switch v0, :pswitch_data_0

    .line 152
    :pswitch_0
    iget-object v0, p1, Lcah;->b:Lcac;

    check-cast v0, Lcap;

    iget v1, v0, Lcap;->d:I

    .line 153
    sget-object v0, Lbzx$a;->a:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 154
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "New view requested unsupported view type for event type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v0, :cond_0

    .line 155
    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 140
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->f:Laei;

    invoke-static {p2, v0}, Lckc;->a(Landroid/view/ViewGroup;Lckb;)Landroid/view/View;

    move-result-object v0

    .line 158
    :goto_1
    return-object v0

    .line 144
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->g:Laee;

    invoke-static {p2, v0}, Lckc;->a(Landroid/view/ViewGroup;Lckb;)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 148
    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->j:Laek;

    invoke-static {p2, v0}, Lckc;->a(Landroid/view/ViewGroup;Lckb;)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 155
    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 138
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/view/View;Lcah;I)V
    .locals 1

    .prologue
    .line 196
    invoke-static {p2}, Lcom/twitter/android/notificationtimeline/f;->a(Lcah;)I

    move-result v0

    .line 197
    packed-switch v0, :pswitch_data_0

    .line 214
    :goto_0
    :pswitch_0
    return-void

    .line 199
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->f:Laei;

    invoke-static {p1, v0, p2, p3}, Lckc;->a(Landroid/view/View;Lckb;Ljava/lang/Object;I)V

    goto :goto_0

    .line 203
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->g:Laee;

    invoke-static {p1, v0, p2, p3}, Lckc;->a(Landroid/view/View;Lckb;Ljava/lang/Object;I)V

    goto :goto_0

    .line 207
    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->j:Laek;

    invoke-static {p1, v0, p2, p3}, Lckc;->a(Landroid/view/View;Lckb;Ljava/lang/Object;I)V

    goto :goto_0

    .line 197
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static b(Lbzz;)V
    .locals 3

    .prologue
    .line 130
    new-instance v0, Lcpb;

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Unsupported ActivityItem"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcpb;-><init>(Ljava/lang/Throwable;)V

    const-string/jumbo v1, "item"

    .line 131
    invoke-virtual {v0, v1, p0}, Lcpb;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    move-result-object v0

    .line 130
    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    .line 132
    return-void
.end method


# virtual methods
.method public a(Lbzz;)I
    .locals 1

    .prologue
    .line 223
    iget-object v0, p1, Lbzz;->a:Lbzy;

    iget v0, v0, Lbzy;->a:I

    packed-switch v0, :pswitch_data_0

    .line 237
    invoke-static {p1}, Lcom/twitter/android/notificationtimeline/f;->b(Lbzz;)V

    .line 240
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 225
    :pswitch_0
    const/4 v0, 0x4

    goto :goto_0

    .line 228
    :pswitch_1
    check-cast p1, Lcah;

    invoke-static {p1}, Lcom/twitter/android/notificationtimeline/f;->a(Lcah;)I

    move-result v0

    goto :goto_0

    .line 231
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 234
    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    .line 223
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 39
    check-cast p1, Lbzz;

    invoke-virtual {p0, p1}, Lcom/twitter/android/notificationtimeline/f;->a(Lbzz;)I

    move-result v0

    return v0
.end method

.method public a(Landroid/content/Context;Lbzz;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p2, Lbzz;->a:Lbzy;

    iget v0, v0, Lbzy;->a:I

    packed-switch v0, :pswitch_data_0

    .line 123
    invoke-static {p2}, Lcom/twitter/android/notificationtimeline/f;->b(Lbzz;)V

    .line 126
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 111
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->i:Laeh;

    invoke-static {p3, v0}, Lckc;->a(Landroid/view/ViewGroup;Lckb;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 114
    :pswitch_1
    check-cast p2, Lcah;

    invoke-direct {p0, p2, p3}, Lcom/twitter/android/notificationtimeline/f;->a(Lcah;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 117
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->k:Laej;

    invoke-static {p3, v0}, Lckc;->a(Landroid/view/ViewGroup;Lckb;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 120
    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->h:Laeg;

    invoke-static {p3, v0}, Lckc;->a(Landroid/view/ViewGroup;Lckb;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 109
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 39
    check-cast p2, Lbzz;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/notificationtimeline/f;->a(Landroid/content/Context;Lbzz;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 293
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->h:Laeg;

    invoke-virtual {v0}, Laeg;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->h:Laeg;

    invoke-virtual {v0, p1, p2}, Laeg;->a(J)V

    .line 289
    return-void
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Lbzz;)V
    .locals 2

    .prologue
    .line 166
    const/4 v0, 0x0

    const-string/jumbo v1, "Unimplemented bindView() override called"

    invoke-static {v0, v1}, Lcom/twitter/util/g;->a(ZLjava/lang/String;)Z

    .line 167
    return-void
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Lbzz;I)V
    .locals 2

    .prologue
    .line 172
    iget-object v0, p3, Lbzz;->a:Lbzy;

    iget v0, v0, Lbzy;->a:I

    packed-switch v0, :pswitch_data_0

    .line 190
    invoke-static {p3}, Lcom/twitter/android/notificationtimeline/f;->b(Lbzz;)V

    .line 193
    :goto_0
    return-void

    .line 174
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->i:Laeh;

    check-cast p3, Lcak;

    invoke-static {p1, v0, p3, p4}, Lckc;->a(Landroid/view/View;Lckb;Ljava/lang/Object;I)V

    goto :goto_0

    .line 178
    :pswitch_1
    check-cast p3, Lcah;

    invoke-direct {p0, p1, p3, p4}, Lcom/twitter/android/notificationtimeline/f;->a(Landroid/view/View;Lcah;I)V

    goto :goto_0

    .line 182
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->k:Laej;

    check-cast p3, Lcam;

    invoke-static {p1, v0, p3, p4}, Lckc;->a(Landroid/view/View;Lckb;Ljava/lang/Object;I)V

    goto :goto_0

    .line 186
    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->h:Laeg;

    invoke-static {p3}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {p1, v0, v1, p4}, Lckc;->a(Landroid/view/View;Lckb;Ljava/lang/Object;I)V

    goto :goto_0

    .line 172
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 39
    check-cast p3, Lbzz;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/notificationtimeline/f;->a(Landroid/view/View;Landroid/content/Context;Lbzz;)V

    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 39
    check-cast p3, Lbzz;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/android/notificationtimeline/f;->a(Landroid/view/View;Landroid/content/Context;Lbzz;I)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->a:Lael;

    invoke-virtual {v0, p1}, Lael;->b(Z)V

    .line 219
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->h:Laeg;

    invoke-virtual {v0}, Laeg;->b()V

    .line 298
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/twitter/android/notificationtimeline/f;->a:Lael;

    invoke-virtual {v0, p1}, Lael;->a(Z)V

    .line 105
    return-void
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 278
    invoke-virtual {p0, p1}, Lcom/twitter/android/notificationtimeline/f;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzz;

    .line 279
    if-eqz v0, :cond_0

    iget-object v0, v0, Lbzz;->b:Lcac;

    invoke-virtual {v0}, Lcac;->a()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    invoke-super {p0, p1}, Lcjr;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 284
    const/4 v0, 0x5

    return v0
.end method
