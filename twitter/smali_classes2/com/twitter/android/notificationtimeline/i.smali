.class public Lcom/twitter/android/notificationtimeline/i;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method private static a(I)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 67
    sparse-switch p0, :sswitch_data_0

    .line 79
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Conflict resolution type unknown for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 80
    :goto_0
    :sswitch_0
    return v0

    .line 73
    :sswitch_1
    const/16 v0, 0x9

    goto :goto_0

    .line 76
    :sswitch_2
    const/16 v0, 0xa

    goto :goto_0

    .line 67
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x4 -> :sswitch_0
        0xb -> :sswitch_2
        0xc -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(IJLjava/util/List;I)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;I)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 38
    invoke-static {}, Lbpj;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz p4, :cond_0

    .line 39
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 60
    :cond_1
    :goto_0
    return v0

    .line 43
    :cond_2
    sparse-switch p0, :sswitch_data_0

    move v0, v1

    .line 60
    goto :goto_0

    .line 46
    :sswitch_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    if-gt p4, v1, :cond_3

    invoke-static {p3}, Lcom/twitter/android/notificationtimeline/i;->a(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 47
    :cond_3
    invoke-static {p0}, Lcom/twitter/android/notificationtimeline/i;->a(I)I

    move-result v0

    goto :goto_0

    .line 49
    :cond_4
    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    .line 54
    :sswitch_1
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-le p4, v0, :cond_5

    .line 55
    invoke-static {p0}, Lcom/twitter/android/notificationtimeline/i;->a(I)I

    move-result v0

    goto :goto_0

    .line 57
    :cond_5
    invoke-static {p3, p1, p2, p0}, Lcom/twitter/android/notificationtimeline/i;->a(Ljava/util/List;JI)I

    move-result v0

    goto :goto_0

    .line 43
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x4 -> :sswitch_0
        0xb -> :sswitch_1
        0xc -> :sswitch_1
    .end sparse-switch
.end method

.method private static a(Lcom/twitter/model/core/Tweet;JI)I
    .locals 5

    .prologue
    const/16 v4, 0xc

    .line 118
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->ab()Lcom/twitter/model/core/v;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/model/core/v;->b(J)Lcom/twitter/model/core/q;

    move-result-object v0

    .line 119
    invoke-virtual {p0}, Lcom/twitter/model/core/Tweet;->p()Z

    move-result v1

    if-nez v1, :cond_0

    .line 120
    const/4 v0, 0x0

    .line 132
    :goto_0
    return v0

    .line 121
    :cond_0
    iget-wide v2, p0, Lcom/twitter/model/core/Tweet;->E:J

    cmp-long v1, p1, v2

    if-nez v1, :cond_2

    .line 122
    if-ne p3, v4, :cond_1

    const/4 v0, 0x5

    goto :goto_0

    :cond_1
    const/4 v0, 0x6

    goto :goto_0

    .line 124
    :cond_2
    if-nez v0, :cond_4

    .line 125
    if-ne p3, v4, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x2

    goto :goto_0

    .line 127
    :cond_4
    invoke-virtual {v0}, Lcom/twitter/model/core/q;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 128
    if-ne p3, v4, :cond_5

    const/4 v0, 0x7

    goto :goto_0

    :cond_5
    const/16 v0, 0x8

    goto :goto_0

    .line 132
    :cond_6
    if-ne p3, v4, :cond_7

    const/4 v0, 0x3

    goto :goto_0

    :cond_7
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private static a(Ljava/util/List;JI)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;JI)I"
        }
    .end annotation

    .prologue
    .line 100
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    invoke-static {v0, p1, p2, p3}, Lcom/twitter/android/notificationtimeline/i;->a(Lcom/twitter/model/core/Tweet;JI)I

    move-result v2

    .line 101
    const/4 v0, 0x1

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 103
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    invoke-static {v0, p1, p2, p3}, Lcom/twitter/android/notificationtimeline/i;->a(Lcom/twitter/model/core/Tweet;JI)I

    move-result v0

    .line 104
    if-eq v0, v2, :cond_0

    .line 105
    invoke-static {p3}, Lcom/twitter/android/notificationtimeline/i;->a(I)I

    move-result v0

    .line 108
    :goto_1
    return v0

    .line 101
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 108
    goto :goto_1
.end method

.method private static a(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 86
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/Tweet;

    .line 87
    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    const/4 v0, 0x1

    .line 91
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
