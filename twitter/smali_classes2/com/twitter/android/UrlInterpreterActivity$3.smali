.class Lcom/twitter/android/UrlInterpreterActivity$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/util/concurrent/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/UrlInterpreterActivity;->b(Landroid/net/Uri;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/util/concurrent/d",
        "<",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/UrlInterpreterActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/UrlInterpreterActivity;)V
    .locals 0

    .prologue
    .line 595
    iput-object p1, p0, Lcom/twitter/android/UrlInterpreterActivity$3;->a:Lcom/twitter/android/UrlInterpreterActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 598
    iget-object v0, p0, Lcom/twitter/android/UrlInterpreterActivity$3;->a:Lcom/twitter/android/UrlInterpreterActivity;

    invoke-virtual {v0}, Lcom/twitter/android/UrlInterpreterActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 599
    if-eqz p1, :cond_0

    .line 600
    iget-object v0, p0, Lcom/twitter/android/UrlInterpreterActivity$3;->a:Lcom/twitter/android/UrlInterpreterActivity;

    invoke-virtual {v0, p1}, Lcom/twitter/android/UrlInterpreterActivity;->startActivity(Landroid/content/Intent;)V

    .line 602
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/UrlInterpreterActivity$3;->a:Lcom/twitter/android/UrlInterpreterActivity;

    invoke-static {v0}, Lcom/twitter/android/UrlInterpreterActivity;->a(Lcom/twitter/android/UrlInterpreterActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 603
    iget-object v0, p0, Lcom/twitter/android/UrlInterpreterActivity$3;->a:Lcom/twitter/android/UrlInterpreterActivity;

    invoke-virtual {v0}, Lcom/twitter/android/UrlInterpreterActivity;->finish()V

    .line 606
    :cond_1
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 595
    check-cast p1, Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/twitter/android/UrlInterpreterActivity$3;->a(Landroid/content/Intent;)V

    return-void
.end method
