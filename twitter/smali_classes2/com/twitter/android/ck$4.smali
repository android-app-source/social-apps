.class Lcom/twitter/android/ck$4;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/bm$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/android/ck;->a(Lcom/twitter/model/core/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/library/widget/h;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/ref/WeakReference;

.field final synthetic b:Landroid/support/v4/app/FragmentActivity;

.field final synthetic c:Lcom/twitter/model/core/Tweet;

.field final synthetic d:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

.field final synthetic e:J

.field final synthetic f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field final synthetic g:Lcom/twitter/android/ck;


# direct methods
.method constructor <init>(Lcom/twitter/android/ck;Ljava/lang/ref/WeakReference;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;JLcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lcom/twitter/android/ck$4;->g:Lcom/twitter/android/ck;

    iput-object p2, p0, Lcom/twitter/android/ck$4;->a:Ljava/lang/ref/WeakReference;

    iput-object p3, p0, Lcom/twitter/android/ck$4;->b:Landroid/support/v4/app/FragmentActivity;

    iput-object p4, p0, Lcom/twitter/android/ck$4;->c:Lcom/twitter/model/core/Tweet;

    iput-object p5, p0, Lcom/twitter/android/ck$4;->d:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    iput-wide p6, p0, Lcom/twitter/android/ck$4;->e:J

    iput-object p8, p0, Lcom/twitter/android/ck$4;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Z
    .locals 4

    .prologue
    .line 302
    iget-object v0, p0, Lcom/twitter/android/ck$4;->c:Lcom/twitter/model/core/Tweet;

    iget-wide v0, v0, Lcom/twitter/model/core/Tweet;->s:J

    iget-wide v2, p0, Lcom/twitter/android/ck$4;->e:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(JLcom/twitter/model/core/Tweet;Z)V
    .locals 5

    .prologue
    .line 251
    iget-object v0, p0, Lcom/twitter/android/ck$4;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/h;

    .line 252
    if-eqz v0, :cond_0

    .line 253
    invoke-interface {v0, p4}, Lcom/twitter/library/widget/h;->b(Z)V

    .line 255
    :cond_0
    if-eqz p4, :cond_2

    .line 256
    invoke-direct {p0}, Lcom/twitter/android/ck$4;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 257
    iget-object v0, p0, Lcom/twitter/android/ck$4;->g:Lcom/twitter/android/ck;

    iget-object v1, p0, Lcom/twitter/android/ck$4;->g:Lcom/twitter/android/ck;

    iget-object v2, p0, Lcom/twitter/android/ck$4;->b:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1, v2}, Lcom/twitter/android/ck;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "self_unretweet"

    iget-object v3, p0, Lcom/twitter/android/ck$4;->c:Lcom/twitter/model/core/Tweet;

    iget-object v4, p0, Lcom/twitter/android/ck$4;->d:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/ck;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 259
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ck$4;->g:Lcom/twitter/android/ck;

    iget-object v1, p0, Lcom/twitter/android/ck$4;->g:Lcom/twitter/android/ck;

    iget-object v2, p0, Lcom/twitter/android/ck$4;->b:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1, v2}, Lcom/twitter/android/ck;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "unretweet"

    iget-object v3, p0, Lcom/twitter/android/ck$4;->c:Lcom/twitter/model/core/Tweet;

    iget-object v4, p0, Lcom/twitter/android/ck$4;->d:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/ck;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 266
    :goto_0
    return-void

    .line 261
    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/ck$4;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 262
    iget-object v0, p0, Lcom/twitter/android/ck$4;->g:Lcom/twitter/android/ck;

    iget-object v1, p0, Lcom/twitter/android/ck$4;->g:Lcom/twitter/android/ck;

    iget-object v2, p0, Lcom/twitter/android/ck$4;->b:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1, v2}, Lcom/twitter/android/ck;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "self_retweet"

    iget-object v3, p0, Lcom/twitter/android/ck$4;->c:Lcom/twitter/model/core/Tweet;

    iget-object v4, p0, Lcom/twitter/android/ck$4;->d:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/ck;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 264
    :cond_3
    iget-object v0, p0, Lcom/twitter/android/ck$4;->g:Lcom/twitter/android/ck;

    iget-object v1, p0, Lcom/twitter/android/ck$4;->g:Lcom/twitter/android/ck;

    iget-object v2, p0, Lcom/twitter/android/ck$4;->b:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1, v2}, Lcom/twitter/android/ck;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "retweet"

    iget-object v3, p0, Lcom/twitter/android/ck$4;->c:Lcom/twitter/model/core/Tweet;

    iget-object v4, p0, Lcom/twitter/android/ck$4;->d:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/ck;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    goto :goto_0
.end method

.method public a(JZZZ)V
    .locals 2

    .prologue
    .line 272
    iget-object v0, p0, Lcom/twitter/android/ck$4;->g:Lcom/twitter/android/ck;

    iget-object v1, p0, Lcom/twitter/android/ck$4;->g:Lcom/twitter/android/ck;

    iget-object v1, v1, Lcom/twitter/android/ck;->f:Landroid/content/Context;

    invoke-virtual {v0, p3, p4, v1, p5}, Lcom/twitter/android/ck;->a(ZZLandroid/content/Context;Z)V

    .line 273
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Z)V
    .locals 7

    .prologue
    .line 288
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/ck$4;->e:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/ck$4;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v4, "retweet_dialog"

    const-string/jumbo v5, ""

    const-string/jumbo v6, "dismiss"

    .line 289
    invoke-static {v3, v4, v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 288
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 291
    return-void
.end method

.method public b(JLcom/twitter/model/core/Tweet;Z)V
    .locals 5

    .prologue
    .line 279
    invoke-direct {p0}, Lcom/twitter/android/ck$4;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/twitter/android/ck$4;->g:Lcom/twitter/android/ck;

    iget-object v1, p0, Lcom/twitter/android/ck$4;->g:Lcom/twitter/android/ck;

    iget-object v2, p0, Lcom/twitter/android/ck$4;->b:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1, v2}, Lcom/twitter/android/ck;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "self_quote"

    iget-object v3, p0, Lcom/twitter/android/ck$4;->c:Lcom/twitter/model/core/Tweet;

    iget-object v4, p0, Lcom/twitter/android/ck$4;->d:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/ck;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 282
    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ck$4;->g:Lcom/twitter/android/ck;

    iget-object v1, p0, Lcom/twitter/android/ck$4;->g:Lcom/twitter/android/ck;

    iget-object v2, p0, Lcom/twitter/android/ck$4;->b:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1, v2}, Lcom/twitter/android/ck;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "quote"

    iget-object v3, p0, Lcom/twitter/android/ck$4;->c:Lcom/twitter/model/core/Tweet;

    iget-object v4, p0, Lcom/twitter/android/ck$4;->d:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/ck;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V

    .line 283
    return-void
.end method

.method public b(Lcom/twitter/model/core/Tweet;Z)V
    .locals 7

    .prologue
    .line 296
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/android/ck$4;->e:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/ck$4;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v4, "retweet_dialog"

    const-string/jumbo v5, ""

    const-string/jumbo v6, "impression"

    .line 297
    invoke-static {v3, v4, v5, v6}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 296
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 299
    return-void
.end method
