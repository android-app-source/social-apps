.class public Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;
.super Lcom/twitter/app/common/base/h;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/ConnectContactsUploadHelperActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/base/h",
        "<",
        "Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/twitter/app/common/base/h;-><init>()V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/twitter/app/common/base/h;-><init>(Landroid/content/Intent;)V

    .line 66
    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;
    .locals 1

    .prologue
    .line 70
    new-instance v0, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;

    invoke-direct {v0, p0}, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;-><init>(Landroid/content/Intent;)V

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "scribe_page"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    const-string/jumbo v0, "people"

    invoke-virtual {p0, v0}, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;->a(Ljava/lang/String;)Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;

    .line 89
    :cond_0
    const-class v0, Lcom/twitter/android/ConnectContactsUploadHelperActivity;

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10000

    .line 90
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 89
    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "scribe_page"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 76
    return-object p0
.end method

.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "scribe_page"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
