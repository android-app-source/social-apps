.class Lcom/twitter/android/LoginActivity$b;
.super Lcom/twitter/android/util/a;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/android/LoginActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/LoginActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/LoginActivity;Lcom/twitter/internal/android/widget/PopupEditText;)V
    .locals 0

    .prologue
    .line 760
    iput-object p1, p0, Lcom/twitter/android/LoginActivity$b;->a:Lcom/twitter/android/LoginActivity;

    .line 761
    invoke-direct {p0, p2}, Lcom/twitter/android/util/a;-><init>(Lcom/twitter/internal/android/widget/PopupEditText;)V

    .line 762
    invoke-virtual {p2, p0}, Lcom/twitter/internal/android/widget/PopupEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 763
    return-void
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 768
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$b;->b:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 769
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/LoginActivity$b;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v1}, Lcom/twitter/android/LoginActivity;->d(Lcom/twitter/android/LoginActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "login:identifier:%s:typeahead:impression"

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/android/LoginActivity$b;->a:Lcom/twitter/android/LoginActivity;

    .line 771
    invoke-static {v4}, Lcom/twitter/android/LoginActivity;->c(Lcom/twitter/android/LoginActivity;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    .line 770
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 769
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 773
    :cond_0
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 783
    invoke-super {p0, p1}, Lcom/twitter/android/util/a;->afterTextChanged(Landroid/text/Editable;)V

    .line 784
    invoke-direct {p0}, Lcom/twitter/android/LoginActivity$b;->b()V

    .line 785
    return-void
.end method

.method public b(I)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 791
    invoke-super {p0, p1}, Lcom/twitter/android/util/a;->b(I)V

    .line 792
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/android/LoginActivity$b;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v1}, Lcom/twitter/android/LoginActivity;->e(Lcom/twitter/android/LoginActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "login:identifier:%s:typeahead:select"

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/android/LoginActivity$b;->a:Lcom/twitter/android/LoginActivity;

    .line 794
    invoke-static {v4}, Lcom/twitter/android/LoginActivity;->c(Lcom/twitter/android/LoginActivity;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    .line 793
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 792
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 795
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 777
    invoke-super {p0, p1}, Lcom/twitter/android/util/a;->onClick(Landroid/view/View;)V

    .line 778
    invoke-direct {p0}, Lcom/twitter/android/LoginActivity$b;->b()V

    .line 779
    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 799
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$b;->b:Lcom/twitter/internal/android/widget/PopupEditText;

    if-ne p1, v0, :cond_0

    .line 800
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity$b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 801
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$b;->b:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->a()V

    .line 802
    invoke-direct {p0}, Lcom/twitter/android/LoginActivity$b;->b()V

    .line 807
    :cond_0
    :goto_0
    return-void

    .line 804
    :cond_1
    iget-object v0, p0, Lcom/twitter/android/LoginActivity$b;->b:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->b()V

    goto :goto_0
.end method
