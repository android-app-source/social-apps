.class public Lcom/twitter/app/collections/CollectionPermalinkFragment;
.super Lcom/twitter/app/common/timeline/TimelineFragment;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/timeline/TimelineFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private a:Landroid/widget/FrameLayout;

.field private b:Lcom/twitter/android/widget/g;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic H()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/twitter/app/collections/CollectionPermalinkFragment;->s()Lcom/twitter/app/collections/a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic I()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/twitter/app/collections/CollectionPermalinkFragment;->s()Lcom/twitter/app/collections/a;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 82
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 84
    :pswitch_0
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    invoke-virtual {p0}, Lcom/twitter/app/collections/CollectionPermalinkFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 86
    iget-object v1, p0, Lcom/twitter/app/collections/CollectionPermalinkFragment;->b:Lcom/twitter/android/widget/g;

    .line 87
    invoke-virtual {v1, v0, p2}, Lcom/twitter/android/widget/g;->a(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 88
    iget-object v0, p0, Lcom/twitter/app/collections/CollectionPermalinkFragment;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/g;->a(Landroid/view/ViewGroup;)V

    .line 89
    invoke-virtual {p0}, Lcom/twitter/app/collections/CollectionPermalinkFragment;->aH()Lcmt;

    move-result-object v0

    iget-object v1, v1, Lcom/twitter/android/widget/g;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcmt;->a(Ljava/lang/CharSequence;)Z

    goto :goto_0

    .line 82
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 2

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 47
    const v0, 0x7f0a0108

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->a(I)Lcom/twitter/app/common/list/l$d;

    move-result-object v0

    const v1, 0x7f0a0109

    .line 48
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l$d;->b(I)Lcom/twitter/app/common/list/l$d;

    .line 49
    return-void
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 3

    .prologue
    .line 124
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 125
    invoke-virtual {p0}, Lcom/twitter/app/collections/CollectionPermalinkFragment;->Y()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    if-ne p3, v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/twitter/app/collections/CollectionPermalinkFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 128
    :cond_0
    return-void
.end method

.method public a(Lcmm;)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 149
    iget-object v5, p0, Lcom/twitter/app/collections/CollectionPermalinkFragment;->b:Lcom/twitter/android/widget/g;

    .line 150
    invoke-interface {p1}, Lcmm;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 159
    invoke-super {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Lcmm;)Z

    move-result v0

    :goto_0
    return v0

    .line 152
    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/app/collections/CollectionPermalinkFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, v5, Lcom/twitter/android/widget/g;->f:Ljava/lang/String;

    iget-object v2, v5, Lcom/twitter/android/widget/g;->g:Ljava/lang/String;

    iget-object v3, v5, Lcom/twitter/android/widget/g;->c:Ljava/lang/String;

    iget-object v4, v5, Lcom/twitter/android/widget/g;->d:Ljava/lang/String;

    iget-object v5, v5, Lcom/twitter/android/widget/g;->h:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/util/af;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/collections/CollectionPermalinkFragment;->a_:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 155
    invoke-virtual {p0}, Lcom/twitter/app/collections/CollectionPermalinkFragment;->i()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    aput-object v7, v1, v6

    const/4 v2, 0x2

    aput-object v7, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "custom_timeline"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "share"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 154
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    move v0, v6

    .line 156
    goto :goto_0

    .line 150
    :pswitch_data_0
    .packed-switch 0x7f1308bc
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 132
    invoke-super {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Lcmr;)Z

    .line 133
    const v0, 0x7f14002f

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 134
    const/4 v0, 0x1

    return v0
.end method

.method public b(Lcmr;)I
    .locals 2

    .prologue
    .line 140
    invoke-super {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->b(Lcmr;)I

    .line 141
    invoke-interface {p1}, Lcmr;->k()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 142
    const v1, 0x7f1308bc

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/app/collections/CollectionPermalinkFragment;->b:Lcom/twitter/android/widget/g;

    iget-object v0, v0, Lcom/twitter/android/widget/g;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lazv;->b(Z)Lazv;

    .line 143
    const/4 v0, 0x2

    return v0

    .line 142
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic k()Lcom/twitter/app/common/timeline/c;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/twitter/app/collections/CollectionPermalinkFragment;->s()Lcom/twitter/app/collections/a;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 166
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 177
    :goto_0
    return-void

    .line 168
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/app/collections/CollectionPermalinkFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "user_id"

    iget-object v2, p0, Lcom/twitter/app/collections/CollectionPermalinkFragment;->b:Lcom/twitter/android/widget/g;

    iget-wide v2, v2, Lcom/twitter/android/widget/g;->e:J

    .line 169
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    .line 168
    invoke-virtual {p0, v0}, Lcom/twitter/app/collections/CollectionPermalinkFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 166
    :pswitch_data_0
    .packed-switch 0x7f1301e8
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 67
    packed-switch p1, :pswitch_data_0

    move-object v0, v6

    .line 75
    :goto_0
    return-object v0

    .line 69
    :pswitch_0
    new-instance v0, Lcom/twitter/util/android/d;

    invoke-virtual {p0}, Lcom/twitter/app/collections/CollectionPermalinkFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/database/schema/a$n;->a:Landroid/net/Uri;

    iget-wide v4, p0, Lcom/twitter/app/collections/CollectionPermalinkFragment;->a_:J

    .line 70
    invoke-static {v2, v4, v5}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lbti;->a:[Ljava/lang/String;

    const-string/jumbo v4, "topics_ev_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/twitter/app/collections/CollectionPermalinkFragment;->g:Ljava/lang/String;

    aput-object v8, v5, v7

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 67
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 36
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/collections/CollectionPermalinkFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 101
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 53
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/timeline/TimelineFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 54
    invoke-virtual {p0}, Lcom/twitter/app/collections/CollectionPermalinkFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 55
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-direct {v1, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/twitter/app/collections/CollectionPermalinkFragment;->a:Landroid/widget/FrameLayout;

    .line 56
    invoke-virtual {p0}, Lcom/twitter/app/collections/CollectionPermalinkFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/twitter/app/collections/CollectionPermalinkFragment;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 58
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040065

    const/4 v2, 0x0

    .line 59
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 60
    new-instance v1, Lcom/twitter/android/widget/g;

    invoke-direct {v1, v0}, Lcom/twitter/android/widget/g;-><init>(Landroid/view/View;)V

    .line 61
    iget-object v0, v1, Lcom/twitter/android/widget/g;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    iput-object v1, p0, Lcom/twitter/app/collections/CollectionPermalinkFragment;->b:Lcom/twitter/android/widget/g;

    .line 63
    return-void
.end method

.method protected p()V
    .locals 3

    .prologue
    .line 105
    invoke-super {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->p()V

    .line 106
    invoke-virtual {p0}, Lcom/twitter/app/collections/CollectionPermalinkFragment;->ak()Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    invoke-virtual {p0}, Lcom/twitter/app/collections/CollectionPermalinkFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 109
    :cond_0
    return-void
.end method

.method protected q()I
    .locals 1

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/twitter/app/collections/CollectionPermalinkFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/collections/CollectionPermalinkFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cv;

    invoke-virtual {v0}, Lcom/twitter/android/cv;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x3

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method protected r()Z
    .locals 1

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/twitter/app/collections/CollectionPermalinkFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/collections/CollectionPermalinkFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cv;

    invoke-virtual {v0}, Lcom/twitter/android/cv;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/app/collections/CollectionPermalinkFragment;->h:Z

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()Lcom/twitter/app/collections/a;
    .locals 1

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/twitter/app/collections/CollectionPermalinkFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/collections/a;->a(Landroid/os/Bundle;)Lcom/twitter/app/collections/a;

    move-result-object v0

    return-object v0
.end method
