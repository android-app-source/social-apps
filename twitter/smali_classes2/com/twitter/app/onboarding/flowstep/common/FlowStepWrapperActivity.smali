.class public Lcom/twitter/app/onboarding/flowstep/common/FlowStepWrapperActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Laog;
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/twitter/app/onboarding/flowstep/common/FlowStepWrapperActivity;->Y()Lans;

    move-result-object v0

    invoke-interface {v0}, Lans;->a()Laog;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laog;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 27
    return-object p2
.end method

.method protected a(Lank;)Lcom/twitter/app/common/base/j;
    .locals 3

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/twitter/app/onboarding/flowstep/common/FlowStepWrapperActivity;->X()Lann;

    move-result-object v0

    check-cast v0, Laqy;

    .line 43
    new-instance v1, Lcom/twitter/app/onboarding/flowstep/common/g;

    invoke-virtual {p0}, Lcom/twitter/app/onboarding/flowstep/common/FlowStepWrapperActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/app/onboarding/flowstep/common/g;-><init>(Landroid/content/Intent;)V

    .line 44
    invoke-virtual {v1}, Lcom/twitter/app/onboarding/flowstep/common/g;->a()I

    move-result v1

    invoke-static {p0, p1, v1, v0}, Larf;->a(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lank;ILaqy;)Lcom/twitter/app/common/base/j;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic b(Lank;)Lcom/twitter/app/common/abs/b;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/twitter/app/onboarding/flowstep/common/FlowStepWrapperActivity;->a(Lank;)Lcom/twitter/app/common/base/j;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic c(Lank;)Lans;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/twitter/app/onboarding/flowstep/common/FlowStepWrapperActivity;->a(Lank;)Lcom/twitter/app/common/base/j;

    move-result-object v0

    return-object v0
.end method

.method protected e(Lank;)Lcom/twitter/app/common/base/i;
    .locals 2

    .prologue
    .line 33
    invoke-static {}, Laqx;->a()Laqx$b;

    move-result-object v0

    .line 34
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Laqx$b;->a(Lamu;)Laqx$b;

    move-result-object v0

    new-instance v1, Lano;

    invoke-direct {v1, p1}, Lano;-><init>(Lank;)V

    .line 35
    invoke-virtual {v0, v1}, Laqx$b;->a(Lano;)Laqx$b;

    move-result-object v0

    .line 36
    invoke-virtual {v0}, Laqx$b;->a()Laqy;

    move-result-object v0

    .line 33
    return-object v0
.end method

.method protected synthetic f(Lank;)Lcom/twitter/app/common/abs/a;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/twitter/app/onboarding/flowstep/common/FlowStepWrapperActivity;->e(Lank;)Lcom/twitter/app/common/base/i;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic g(Lank;)Lann;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/twitter/app/onboarding/flowstep/common/FlowStepWrapperActivity;->e(Lank;)Lcom/twitter/app/common/base/i;

    move-result-object v0

    return-object v0
.end method
