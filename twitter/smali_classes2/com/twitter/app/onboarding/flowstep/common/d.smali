.class public Lcom/twitter/app/onboarding/flowstep/common/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Landroid/support/v4/app/FragmentManager;

.field private final c:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

.field private final d:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/support/v4/app/FragmentManager;)V
    .locals 2

    .prologue
    .line 35
    const v0, 0x7f0400f9

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, Lcom/twitter/app/onboarding/flowstep/common/d;-><init>(Landroid/app/Activity;Landroid/view/View;Landroid/support/v4/app/FragmentManager;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Landroid/support/v4/app/FragmentManager;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/twitter/app/onboarding/flowstep/common/d;->a:Landroid/app/Activity;

    .line 41
    iput-object p2, p0, Lcom/twitter/app/onboarding/flowstep/common/d;->d:Landroid/view/View;

    .line 42
    iput-object p3, p0, Lcom/twitter/app/onboarding/flowstep/common/d;->b:Landroid/support/v4/app/FragmentManager;

    .line 43
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/d;->b:Landroid/support/v4/app/FragmentManager;

    const-string/jumbo v1, "tag_progress_dialog"

    .line 44
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    .line 45
    if-eqz v0, :cond_0

    .line 46
    iput-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/d;->c:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    .line 50
    :goto_0
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/d;->c:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->setRetainInstance(Z)V

    .line 51
    return-void

    .line 48
    :cond_0
    const v0, 0x7f0a04b6

    invoke-static {v0}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->a(I)Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/d;->c:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/d;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 74
    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/d;->d:Landroid/view/View;

    return-object v0
.end method

.method public b()Landroid/view/View;
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/d;->d:Landroid/view/View;

    const v1, 0x7f13038c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 65
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/d;->c:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    iget-object v1, p0, Lcom/twitter/app/onboarding/flowstep/common/d;->b:Landroid/support/v4/app/FragmentManager;

    const-string/jumbo v2, "tag_progress_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 66
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/d;->c:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->dismissAllowingStateLoss()V

    .line 70
    return-void
.end method
