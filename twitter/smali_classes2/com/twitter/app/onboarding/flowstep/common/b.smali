.class public Lcom/twitter/app/onboarding/flowstep/common/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lanj;
.implements Laoe;


# instance fields
.field private final a:Lcom/twitter/app/onboarding/flowstep/common/d;

.field private final b:Laqw;

.field private final c:Lcjd;

.field private final d:Lrx/j;

.field private final e:Lrx/j;

.field private f:Lrx/j;


# direct methods
.method public constructor <init>(Lcom/twitter/app/onboarding/flowstep/common/d;Laqw;Lcjd;Lcom/twitter/app/onboarding/flowstep/common/i;)V
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/twitter/app/onboarding/flowstep/common/b;->a:Lcom/twitter/app/onboarding/flowstep/common/d;

    .line 48
    iput-object p2, p0, Lcom/twitter/app/onboarding/flowstep/common/b;->b:Laqw;

    .line 49
    iput-object p3, p0, Lcom/twitter/app/onboarding/flowstep/common/b;->c:Lcjd;

    .line 50
    invoke-virtual {p4}, Lcom/twitter/app/onboarding/flowstep/common/i;->c()Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/onboarding/flowstep/common/b$1;

    invoke-direct {v1, p0}, Lcom/twitter/app/onboarding/flowstep/common/b$1;-><init>(Lcom/twitter/app/onboarding/flowstep/common/b;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/b;->d:Lrx/j;

    .line 56
    invoke-virtual {p4}, Lcom/twitter/app/onboarding/flowstep/common/i;->d()Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/onboarding/flowstep/common/b$2;

    invoke-direct {v1, p0}, Lcom/twitter/app/onboarding/flowstep/common/b$2;-><init>(Lcom/twitter/app/onboarding/flowstep/common/b;)V

    .line 57
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/b;->e:Lrx/j;

    .line 64
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/onboarding/flowstep/common/b;)Lcom/twitter/app/onboarding/flowstep/common/d;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/b;->a:Lcom/twitter/app/onboarding/flowstep/common/d;

    return-object v0
.end method

.method private a(Lcgg;)V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/b;->a:Lcom/twitter/app/onboarding/flowstep/common/d;

    invoke-virtual {v0}, Lcom/twitter/app/onboarding/flowstep/common/d;->c()V

    .line 81
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/b;->c:Lcjd;

    invoke-interface {v0, p1}, Lcjd;->a(Lcgg;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/onboarding/flowstep/common/b$3;

    invoke-direct {v1, p0}, Lcom/twitter/app/onboarding/flowstep/common/b$3;-><init>(Lcom/twitter/app/onboarding/flowstep/common/b;)V

    .line 82
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/b;->f:Lrx/j;

    .line 92
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/onboarding/flowstep/common/b;Lcgg;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/twitter/app/onboarding/flowstep/common/b;->a(Lcgg;)V

    return-void
.end method


# virtual methods
.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/b;->a:Lcom/twitter/app/onboarding/flowstep/common/d;

    invoke-virtual {v0}, Lcom/twitter/app/onboarding/flowstep/common/d;->aN_()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public am_()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/b;->d:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 97
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/b;->e:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 98
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/b;->f:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 99
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/b;->f:Lrx/j;

    .line 100
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/b;->b:Laqw;

    invoke-virtual {v0}, Laqw;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    new-instance v0, Lcge;

    iget-object v1, p0, Lcom/twitter/app/onboarding/flowstep/common/b;->b:Laqw;

    invoke-virtual {v1}, Laqw;->e()Lcom/twitter/model/onboarding/a;

    move-result-object v1

    invoke-direct {v0, v1}, Lcge;-><init>(Lcom/twitter/model/onboarding/a;)V

    invoke-direct {p0, v0}, Lcom/twitter/app/onboarding/flowstep/common/b;->a(Lcgg;)V

    .line 76
    :cond_0
    return-void
.end method
