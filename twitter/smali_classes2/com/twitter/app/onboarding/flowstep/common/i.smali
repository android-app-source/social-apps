.class public Lcom/twitter/app/onboarding/flowstep/common/i;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lanj;


# instance fields
.field private final a:Lcom/twitter/app/onboarding/flowstep/common/k;

.field private final b:Lrx/j;


# direct methods
.method public constructor <init>(Lcom/twitter/app/onboarding/flowstep/common/k;Lcom/twitter/app/onboarding/flowstep/common/l;)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/twitter/app/onboarding/flowstep/common/i;->a:Lcom/twitter/app/onboarding/flowstep/common/k;

    .line 34
    invoke-virtual {p2}, Lcom/twitter/app/onboarding/flowstep/common/l;->b()Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/onboarding/flowstep/common/i$1;

    invoke-direct {v1, p0}, Lcom/twitter/app/onboarding/flowstep/common/i$1;-><init>(Lcom/twitter/app/onboarding/flowstep/common/i;)V

    .line 35
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/i;->b:Lrx/j;

    .line 41
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/onboarding/flowstep/common/i;)Lcom/twitter/app/onboarding/flowstep/common/k;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/i;->a:Lcom/twitter/app/onboarding/flowstep/common/k;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/i;->a:Lcom/twitter/app/onboarding/flowstep/common/k;

    invoke-virtual {v0, p1}, Lcom/twitter/app/onboarding/flowstep/common/k;->a(I)V

    .line 49
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/i;->a:Lcom/twitter/app/onboarding/flowstep/common/k;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/app/onboarding/flowstep/common/k;->b(Z)V

    .line 50
    return-void
.end method

.method public am_()V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/i;->b:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 65
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/i;->a:Lcom/twitter/app/onboarding/flowstep/common/k;

    invoke-virtual {v0}, Lcom/twitter/app/onboarding/flowstep/common/k;->a()Z

    move-result v0

    return v0
.end method

.method public c()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcqx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/i;->a:Lcom/twitter/app/onboarding/flowstep/common/k;

    invoke-virtual {v0}, Lcom/twitter/app/onboarding/flowstep/common/k;->b()Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public d()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcqx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/i;->a:Lcom/twitter/app/onboarding/flowstep/common/k;

    invoke-virtual {v0}, Lcom/twitter/app/onboarding/flowstep/common/k;->c()Lrx/c;

    move-result-object v0

    return-object v0
.end method
