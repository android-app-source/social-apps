.class public Lcom/twitter/app/onboarding/flowstep/common/l;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lang;


# annotations
.annotation build Lcom/twitter/app/AutoSaveState;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lang",
        "<",
        "Lcom/twitter/app/common/util/StateSaver",
        "<",
        "Lcom/twitter/app/onboarding/flowstep/common/l;",
        ">;>;"
    }
.end annotation


# instance fields
.field a:Lcom/twitter/android/ValidationState;
    .annotation build Lcom/twitter/app/SaveState;
    .end annotation
.end field

.field private final b:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject",
            "<",
            "Lcom/twitter/android/ValidationState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/app/common/util/StateSaver;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/l;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Lcom/twitter/android/ValidationState;

    invoke-direct {v0}, Lcom/twitter/android/ValidationState;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/l;->a:Lcom/twitter/android/ValidationState;

    .line 34
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/l;->b:Lrx/subjects/PublishSubject;

    .line 35
    invoke-virtual {p1, p0}, Lcom/twitter/app/common/util/StateSaver;->a(Ljava/lang/Object;)V

    .line 37
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/ValidationState;)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/l;->a:Lcom/twitter/android/ValidationState;

    if-eq v0, p1, :cond_0

    .line 41
    iput-object p1, p0, Lcom/twitter/app/onboarding/flowstep/common/l;->a:Lcom/twitter/android/ValidationState;

    .line 42
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/l;->b:Lrx/subjects/PublishSubject;

    invoke-virtual {v0, p1}, Lrx/subjects/PublishSubject;->a(Ljava/lang/Object;)V

    .line 44
    :cond_0
    return-void
.end method

.method public al_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    const-string/jumbo v0, "SaveStateId_ValidationStateManager"

    return-object v0
.end method

.method public b()Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcom/twitter/android/ValidationState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/l;->b:Lrx/subjects/PublishSubject;

    iget-object v1, p0, Lcom/twitter/app/onboarding/flowstep/common/l;->a:Lcom/twitter/android/ValidationState;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject;->d(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/twitter/app/onboarding/flowstep/common/l;->e()Lcom/twitter/app/common/util/StateSaver;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/twitter/android/ValidationState;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/l;->a:Lcom/twitter/android/ValidationState;

    return-object v0
.end method

.method public e()Lcom/twitter/app/common/util/StateSaver;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/app/common/util/StateSaver",
            "<",
            "Lcom/twitter/app/onboarding/flowstep/common/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    new-instance v0, Lcom/twitter/app/onboarding/flowstep/common/ValidationStateManagerSavedState;

    invoke-direct {v0, p0}, Lcom/twitter/app/onboarding/flowstep/common/ValidationStateManagerSavedState;-><init>(Lcom/twitter/app/onboarding/flowstep/common/l;)V

    return-object v0
.end method
