.class public Lcom/twitter/app/onboarding/flowstep/common/ValidationStateManagerSavedState;
.super Lcom/twitter/app/common/util/BaseStateSaver;
.source "Twttr"


# annotations
.annotation build Lcod;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<OBJ:",
        "Lcom/twitter/app/onboarding/flowstep/common/l;",
        ">",
        "Lcom/twitter/app/common/util/BaseStateSaver",
        "<TOBJ;>;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/twitter/app/onboarding/flowstep/common/ValidationStateManagerSavedState$1;

    invoke-direct {v0}, Lcom/twitter/app/onboarding/flowstep/common/ValidationStateManagerSavedState$1;-><init>()V

    sput-object v0, Lcom/twitter/app/onboarding/flowstep/common/ValidationStateManagerSavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/twitter/app/common/util/BaseStateSaver;-><init>(Landroid/os/Parcel;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Lcom/twitter/app/onboarding/flowstep/common/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TOBJ;)V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/twitter/app/common/util/BaseStateSaver;-><init>(Ljava/lang/Object;)V

    .line 33
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;Lcom/twitter/app/onboarding/flowstep/common/l;)Lcom/twitter/app/onboarding/flowstep/common/l;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/n;",
            "TOBJ;)TOBJ;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 48
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/util/BaseStateSaver;->a(Lcom/twitter/util/serialization/n;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/onboarding/flowstep/common/l;

    .line 49
    sget-object v1, Lcom/twitter/android/ValidationState;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v1}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/ValidationState;

    iput-object v1, v0, Lcom/twitter/app/onboarding/flowstep/common/l;->a:Lcom/twitter/android/ValidationState;

    .line 50
    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/n;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 15
    check-cast p2, Lcom/twitter/app/onboarding/flowstep/common/l;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/onboarding/flowstep/common/ValidationStateManagerSavedState;->a(Lcom/twitter/util/serialization/n;Lcom/twitter/app/onboarding/flowstep/common/l;)Lcom/twitter/app/onboarding/flowstep/common/l;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/app/onboarding/flowstep/common/l;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/o;",
            "TOBJ;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/util/BaseStateSaver;->a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V

    .line 42
    iget-object v0, p2, Lcom/twitter/app/onboarding/flowstep/common/l;->a:Lcom/twitter/android/ValidationState;

    sget-object v1, Lcom/twitter/android/ValidationState;->a:Lcom/twitter/util/serialization/l;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 43
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15
    check-cast p2, Lcom/twitter/app/onboarding/flowstep/common/l;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/onboarding/flowstep/common/ValidationStateManagerSavedState;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/app/onboarding/flowstep/common/l;)V

    return-void
.end method
