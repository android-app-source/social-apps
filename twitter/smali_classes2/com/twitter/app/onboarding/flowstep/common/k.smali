.class public Lcom/twitter/app/onboarding/flowstep/common/k;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/widget/Button;

.field private final b:Lrx/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/c",
            "<",
            "Lcqx;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/widget/Button;

.field private final d:Lrx/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/c",
            "<",
            "Lcqx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const v0, 0x7f1301a7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/k;->a:Landroid/widget/Button;

    .line 25
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/k;->a:Landroid/widget/Button;

    invoke-static {v0}, Lcrf;->d(Landroid/view/View;)Lrx/c;

    move-result-object v0

    invoke-static {}, Lcre;->g()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/k;->b:Lrx/c;

    .line 26
    const v0, 0x7f1301ad

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/k;->c:Landroid/widget/Button;

    .line 27
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/k;->c:Landroid/widget/Button;

    invoke-static {v0}, Lcrf;->d(Landroid/view/View;)Lrx/c;

    move-result-object v0

    invoke-static {}, Lcre;->g()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/k;->d:Lrx/c;

    .line 28
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/k;->a:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 54
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/k;->c:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 36
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/k;->c:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public b()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcqx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/k;->d:Lrx/c;

    return-object v0
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 49
    iget-object v1, p0, Lcom/twitter/app/onboarding/flowstep/common/k;->a:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 50
    return-void

    .line 49
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public c()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcqx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/k;->b:Lrx/c;

    return-object v0
.end method
