.class public Lcom/twitter/app/onboarding/flowstep/common/f;
.super Laog;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/ValidationState$a;
.implements Lcom/twitter/android/au;


# instance fields
.field private final a:Lcom/twitter/app/onboarding/flowstep/common/b;

.field private final b:Lcom/twitter/app/onboarding/flowstep/common/l;

.field private final c:Lcom/twitter/app/onboarding/flowstep/common/i;


# direct methods
.method public constructor <init>(Lcom/twitter/app/onboarding/flowstep/common/b;Lcom/twitter/app/onboarding/flowstep/common/l;Lcom/twitter/app/onboarding/flowstep/common/i;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Laog;-><init>()V

    .line 31
    invoke-virtual {p1}, Lcom/twitter/app/onboarding/flowstep/common/b;->aN_()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/onboarding/flowstep/common/f;->a(Landroid/view/View;)V

    .line 32
    iput-object p1, p0, Lcom/twitter/app/onboarding/flowstep/common/f;->a:Lcom/twitter/app/onboarding/flowstep/common/b;

    .line 33
    iput-object p2, p0, Lcom/twitter/app/onboarding/flowstep/common/f;->b:Lcom/twitter/app/onboarding/flowstep/common/l;

    .line 34
    iput-object p3, p0, Lcom/twitter/app/onboarding/flowstep/common/f;->c:Lcom/twitter/app/onboarding/flowstep/common/i;

    .line 35
    invoke-virtual {p0}, Lcom/twitter/app/onboarding/flowstep/common/f;->o()Lanh;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/onboarding/flowstep/common/f;->a:Lcom/twitter/app/onboarding/flowstep/common/b;

    invoke-virtual {v0, v1}, Lanh;->a(Ljava/lang/Object;)Lanh;

    .line 36
    invoke-virtual {p0}, Lcom/twitter/app/onboarding/flowstep/common/f;->o()Lanh;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/onboarding/flowstep/common/f;->b:Lcom/twitter/app/onboarding/flowstep/common/l;

    invoke-virtual {v0, v1}, Lanh;->a(Ljava/lang/Object;)Lanh;

    .line 37
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/f;->c:Lcom/twitter/app/onboarding/flowstep/common/i;

    invoke-virtual {v0, p1}, Lcom/twitter/app/onboarding/flowstep/common/i;->a(I)V

    .line 47
    return-void
.end method

.method public a(Lcom/twitter/android/ValidationState;)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/f;->b:Lcom/twitter/app/onboarding/flowstep/common/l;

    invoke-virtual {v0, p1}, Lcom/twitter/app/onboarding/flowstep/common/l;->a(Lcom/twitter/android/ValidationState;)V

    .line 67
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method public am_()V
    .locals 2

    .prologue
    .line 77
    invoke-super {p0}, Laog;->am_()V

    .line 79
    invoke-virtual {p0}, Lcom/twitter/app/onboarding/flowstep/common/f;->o()Lanh;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/onboarding/flowstep/common/f;->b:Lcom/twitter/app/onboarding/flowstep/common/l;

    invoke-virtual {v0, v1}, Lanh;->b(Ljava/lang/Object;)Lanh;

    .line 80
    invoke-virtual {p0}, Lcom/twitter/app/onboarding/flowstep/common/f;->o()Lanh;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/onboarding/flowstep/common/f;->a:Lcom/twitter/app/onboarding/flowstep/common/b;

    invoke-virtual {v0, v1}, Lanh;->b(Ljava/lang/Object;)Lanh;

    .line 81
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/f;->a:Lcom/twitter/app/onboarding/flowstep/common/b;

    invoke-virtual {v0}, Lcom/twitter/app/onboarding/flowstep/common/b;->b()V

    .line 57
    return-void
.end method

.method public bl_()V
    .locals 0

    .prologue
    .line 62
    return-void
.end method

.method public e()Lcom/twitter/android/ValidationState;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/f;->b:Lcom/twitter/app/onboarding/flowstep/common/l;

    invoke-virtual {v0}, Lcom/twitter/app/onboarding/flowstep/common/l;->d()Lcom/twitter/android/ValidationState;

    move-result-object v0

    return-object v0
.end method

.method public n_()Z
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/app/onboarding/flowstep/common/f;->c:Lcom/twitter/app/onboarding/flowstep/common/i;

    invoke-virtual {v0}, Lcom/twitter/app/onboarding/flowstep/common/i;->b()Z

    move-result v0

    return v0
.end method
