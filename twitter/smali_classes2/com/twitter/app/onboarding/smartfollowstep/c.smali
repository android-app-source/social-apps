.class public Lcom/twitter/app/onboarding/smartfollowstep/c;
.super Laog;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/smartfollow/a$a;


# instance fields
.field private final a:Lcom/twitter/app/onboarding/smartfollowstep/b;

.field private final b:Lcjd;

.field private c:Lcom/twitter/android/smartfollow/a;

.field private d:Lrx/j;


# direct methods
.method public constructor <init>(Lcom/twitter/app/onboarding/smartfollowstep/b;Lcjd;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Laog;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/twitter/app/onboarding/smartfollowstep/c;->a:Lcom/twitter/app/onboarding/smartfollowstep/b;

    .line 43
    iput-object p2, p0, Lcom/twitter/app/onboarding/smartfollowstep/c;->b:Lcjd;

    .line 44
    invoke-virtual {p1}, Lcom/twitter/app/onboarding/smartfollowstep/b;->aN_()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/onboarding/smartfollowstep/c;->a(Landroid/view/View;)V

    .line 45
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/onboarding/smartfollowstep/c;)Lcom/twitter/app/onboarding/smartfollowstep/b;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/c;->a:Lcom/twitter/app/onboarding/smartfollowstep/b;

    return-object v0
.end method

.method private a(Lcgg;)V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/c;->a:Lcom/twitter/app/onboarding/smartfollowstep/b;

    invoke-virtual {v0}, Lcom/twitter/app/onboarding/smartfollowstep/b;->b()V

    .line 73
    iget-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/c;->b:Lcjd;

    invoke-interface {v0, p1}, Lcjd;->a(Lcgg;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/onboarding/smartfollowstep/c$1;

    invoke-direct {v1, p0}, Lcom/twitter/app/onboarding/smartfollowstep/c$1;-><init>(Lcom/twitter/app/onboarding/smartfollowstep/c;)V

    .line 74
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/c;->d:Lrx/j;

    .line 84
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Lcom/twitter/android/smartfollow/SmartFollowFlowData;)V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lcgh;

    invoke-direct {v0}, Lcgh;-><init>()V

    invoke-direct {p0, v0}, Lcom/twitter/app/onboarding/smartfollowstep/c;->a(Lcgg;)V

    .line 58
    return-void
.end method

.method public a(Lcom/twitter/android/twitterflows/d;Lcom/twitter/android/smartfollow/a;)V
    .locals 2

    .prologue
    .line 49
    iput-object p2, p0, Lcom/twitter/app/onboarding/smartfollowstep/c;->c:Lcom/twitter/android/smartfollow/a;

    .line 50
    iget-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/c;->a:Lcom/twitter/app/onboarding/smartfollowstep/b;

    .line 51
    invoke-virtual {p1}, Lcom/twitter/android/twitterflows/d;->a()I

    move-result v1

    .line 50
    invoke-virtual {v0, v1, p2}, Lcom/twitter/app/onboarding/smartfollowstep/b;->a(ILcom/twitter/android/smartfollow/a;)Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    .line 52
    invoke-virtual {p2, p0}, Lcom/twitter/android/smartfollow/a;->a(Lcom/twitter/android/smartfollow/a$a;)V

    .line 53
    return-void
.end method

.method public am_()V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/c;->d:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/c;->d:Lrx/j;

    .line 90
    invoke-super {p0}, Laog;->am_()V

    .line 91
    return-void
.end method

.method public b(Landroid/view/View;Lcom/twitter/android/smartfollow/SmartFollowFlowData;)V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/c;->c:Lcom/twitter/android/smartfollow/a;

    instance-of v0, v0, Lcom/twitter/app/onboarding/flowstep/common/h;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/c;->c:Lcom/twitter/android/smartfollow/a;

    check-cast v0, Lcom/twitter/app/onboarding/flowstep/common/h;

    invoke-interface {v0}, Lcom/twitter/app/onboarding/flowstep/common/h;->e()Lcom/twitter/model/onboarding/a;

    move-result-object v0

    .line 68
    :goto_0
    new-instance v1, Lcge;

    invoke-direct {v1, v0}, Lcge;-><init>(Lcom/twitter/model/onboarding/a;)V

    invoke-direct {p0, v1}, Lcom/twitter/app/onboarding/smartfollowstep/c;->a(Lcgg;)V

    .line 69
    return-void

    .line 66
    :cond_0
    sget-object v0, Lcom/twitter/model/onboarding/a;->a:Lcom/twitter/model/onboarding/a;

    goto :goto_0
.end method
