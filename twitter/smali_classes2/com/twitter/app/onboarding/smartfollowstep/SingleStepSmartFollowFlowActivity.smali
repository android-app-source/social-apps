.class public Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/smartfollow/f;

.field private final b:Lcom/twitter/android/twitterflows/b;

.field private c:Lcom/twitter/android/smartfollow/SmartFollowFlowData;

.field private d:Lcom/twitter/android/twitterflows/h;

.field private e:Lcom/twitter/android/smartfollow/a;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    .line 45
    new-instance v0, Lcom/twitter/android/smartfollow/g;

    invoke-direct {v0, p0}, Lcom/twitter/android/smartfollow/g;-><init>(Lcom/twitter/app/common/base/BaseFragmentActivity;)V

    iput-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->a:Lcom/twitter/android/smartfollow/f;

    .line 46
    new-instance v0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity$a;-><init>(Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity$1;)V

    iput-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->b:Lcom/twitter/android/twitterflows/b;

    return-void
.end method

.method private a(Lcom/twitter/android/smartfollow/w;I)V
    .locals 3

    .prologue
    .line 99
    packed-switch p2, :pswitch_data_0

    .line 116
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Got unsupported step type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :pswitch_0
    new-instance v0, Lcom/twitter/android/smartfollow/e$b;

    invoke-direct {v0}, Lcom/twitter/android/smartfollow/e$b;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->d:Lcom/twitter/android/twitterflows/h;

    .line 102
    invoke-interface {p1}, Lcom/twitter/android/smartfollow/w;->e()Lcom/twitter/android/smartfollow/followpeople/h;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->e:Lcom/twitter/android/smartfollow/a;

    .line 119
    :goto_0
    return-void

    .line 106
    :pswitch_1
    new-instance v0, Lcom/twitter/android/smartfollow/y$b;

    invoke-direct {v0}, Lcom/twitter/android/smartfollow/y$b;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->d:Lcom/twitter/android/twitterflows/h;

    .line 107
    invoke-interface {p1}, Lcom/twitter/android/smartfollow/w;->g()Lcom/twitter/android/smartfollow/sharelocation/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->e:Lcom/twitter/android/smartfollow/a;

    goto :goto_0

    .line 111
    :pswitch_2
    new-instance v0, Lcom/twitter/android/smartfollow/y$a;

    invoke-direct {v0}, Lcom/twitter/android/smartfollow/y$a;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->d:Lcom/twitter/android/twitterflows/h;

    .line 112
    invoke-interface {p1}, Lcom/twitter/android/smartfollow/w;->f()Lcom/twitter/android/smartfollow/interestpicker/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->e:Lcom/twitter/android/smartfollow/a;

    goto :goto_0

    .line 99
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    .line 55
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    .line 56
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 57
    return-object v0
.end method

.method protected a(Lank;)Lcom/twitter/app/common/base/j;
    .locals 2

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->X()Lann;

    move-result-object v0

    check-cast v0, Larz;

    .line 125
    new-instance v1, Lant;

    invoke-direct {v1, p0, p1}, Lant;-><init>(Landroid/app/Activity;Lank;)V

    invoke-interface {v0, v1}, Larz;->a(Lant;)Lasa;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 5

    .prologue
    .line 63
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V

    .line 64
    if-eqz p1, :cond_0

    .line 65
    const-string/jumbo v0, "flow_data"

    .line 66
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 65
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    iput-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->c:Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    .line 71
    :goto_0
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 74
    new-instance v1, Lcom/twitter/app/onboarding/smartfollowstep/a;

    invoke-virtual {p0}, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/twitter/app/onboarding/smartfollowstep/a;-><init>(Landroid/content/Intent;)V

    .line 75
    invoke-virtual {p0}, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->X()Lann;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/w;

    .line 76
    invoke-virtual {v1}, Lcom/twitter/app/onboarding/smartfollowstep/a;->a()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->a(Lcom/twitter/android/smartfollow/w;I)V

    .line 77
    iget-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->e:Lcom/twitter/android/smartfollow/a;

    iget-object v1, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->d:Lcom/twitter/android/twitterflows/h;

    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/a;->a(Lcom/twitter/android/twitterflows/h;)V

    .line 79
    invoke-virtual {p0}, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->W()Lanh;

    move-result-object v0

    .line 80
    iget-object v1, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->e:Lcom/twitter/android/smartfollow/a;

    iget-object v4, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->a:Lcom/twitter/android/smartfollow/f;

    invoke-virtual {v1, v4}, Lcom/twitter/android/smartfollow/a;->a(Lcom/twitter/android/smartfollow/f;)V

    .line 81
    iget-object v1, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->e:Lcom/twitter/android/smartfollow/a;

    iget-object v4, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->c:Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    invoke-virtual {v1, v4}, Lcom/twitter/android/smartfollow/a;->a(Lcom/twitter/android/smartfollow/SmartFollowFlowData;)V

    .line 82
    iget-object v1, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->e:Lcom/twitter/android/smartfollow/a;

    const-string/jumbo v4, ""

    invoke-virtual {v1, v4}, Lcom/twitter/android/smartfollow/a;->c_(Ljava/lang/String;)V

    .line 83
    iget-object v1, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->e:Lcom/twitter/android/smartfollow/a;

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/smartfollow/a;->a(J)V

    .line 84
    iget-object v1, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->e:Lcom/twitter/android/smartfollow/a;

    iget-object v2, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->b:Lcom/twitter/android/twitterflows/b;

    invoke-virtual {v1, v2}, Lcom/twitter/android/smartfollow/a;->a(Lcom/twitter/android/twitterflows/b;)V

    .line 85
    iget-object v1, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->e:Lcom/twitter/android/smartfollow/a;

    invoke-virtual {p0}, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/smartfollow/a;->a(Landroid/content/Context;)V

    .line 86
    iget-object v1, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->e:Lcom/twitter/android/smartfollow/a;

    invoke-virtual {v0, v1}, Lanh;->a(Ljava/lang/Object;)Lanh;

    .line 89
    iget-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->c:Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    sget-object v1, Lcom/twitter/model/stratostore/SourceLocation;->a:Lcom/twitter/model/stratostore/SourceLocation;

    invoke-virtual {v0, v1}, Lcom/twitter/android/smartfollow/SmartFollowFlowData;->a(Lcom/twitter/model/stratostore/SourceLocation;)V

    .line 92
    invoke-virtual {p0}, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->Z()Laog;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/onboarding/smartfollowstep/c;

    .line 93
    iget-object v1, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->d:Lcom/twitter/android/twitterflows/h;

    iget-object v2, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->e:Lcom/twitter/android/smartfollow/a;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/onboarding/smartfollowstep/c;->a(Lcom/twitter/android/twitterflows/d;Lcom/twitter/android/smartfollow/a;)V

    .line 94
    return-void

    .line 68
    :cond_0
    new-instance v0, Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    invoke-direct {v0}, Lcom/twitter/android/smartfollow/SmartFollowFlowData;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->c:Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    goto :goto_0
.end method

.method protected synthetic b(Lank;)Lcom/twitter/app/common/abs/b;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->a(Lank;)Lcom/twitter/app/common/base/j;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic c(Lank;)Lans;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->a(Lank;)Lcom/twitter/app/common/base/j;

    move-result-object v0

    return-object v0
.end method

.method protected d(Lank;)Lcom/twitter/android/smartfollow/w;
    .locals 5

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/smartfollow/d;->a(Landroid/content/Intent;)Lcom/twitter/android/smartfollow/d;

    move-result-object v0

    .line 132
    new-instance v1, Lcom/twitter/analytics/model/c;

    invoke-virtual {v0}, Lcom/twitter/android/smartfollow/d;->a()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "smart_follow_flow"

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/model/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-static {}, Lary;->a()Lary$a;

    move-result-object v2

    .line 136
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v3

    invoke-virtual {v2, v3}, Lary$a;->a(Lamu;)Lary$a;

    move-result-object v2

    new-instance v3, Lano;

    new-instance v4, Lcom/twitter/android/smartfollow/h;

    .line 138
    invoke-virtual {v0}, Lcom/twitter/android/smartfollow/d;->e()I

    move-result v0

    invoke-direct {v4, v0, v1}, Lcom/twitter/android/smartfollow/h;-><init>(ILcom/twitter/analytics/model/c;)V

    invoke-direct {v3, p1, v4}, Lano;-><init>(Lank;Lanr;)V

    .line 137
    invoke-virtual {v2, v3}, Lary$a;->a(Lano;)Lary$a;

    move-result-object v0

    .line 139
    invoke-virtual {v0}, Lary$a;->a()Larz;

    move-result-object v0

    .line 134
    return-object v0
.end method

.method protected synthetic e(Lank;)Lcom/twitter/app/common/base/i;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->d(Lank;)Lcom/twitter/android/smartfollow/w;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f(Lank;)Lcom/twitter/app/common/abs/a;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->d(Lank;)Lcom/twitter/android/smartfollow/w;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic g(Lank;)Lann;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->d(Lank;)Lcom/twitter/android/smartfollow/w;

    move-result-object v0

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 162
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 163
    iget-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->e:Lcom/twitter/android/smartfollow/a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/android/smartfollow/a;->a(IILandroid/content/Intent;)V

    .line 164
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->e:Lcom/twitter/android/smartfollow/a;

    invoke-virtual {v0}, Lcom/twitter/android/smartfollow/a;->q()V

    .line 157
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onBackPressed()V

    .line 158
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->e:Lcom/twitter/android/smartfollow/a;

    invoke-virtual {v0}, Lcom/twitter/android/smartfollow/a;->n()V

    .line 151
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onPause()V

    .line 152
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->e:Lcom/twitter/android/smartfollow/a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/android/smartfollow/a;->a(I[Ljava/lang/String;[I)V

    .line 170
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 144
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 145
    const-string/jumbo v0, "flow_data"

    iget-object v1, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->c:Lcom/twitter/android/smartfollow/SmartFollowFlowData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 146
    return-void
.end method

.method protected p_()V
    .locals 3

    .prologue
    .line 174
    iget-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/SingleStepSmartFollowFlowActivity;->e:Lcom/twitter/android/smartfollow/a;

    const/4 v1, 0x0

    const-string/jumbo v2, "logout"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/smartfollow/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->p_()V

    .line 176
    return-void
.end method
