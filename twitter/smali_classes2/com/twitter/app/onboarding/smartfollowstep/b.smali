.class public Lcom/twitter/app/onboarding/smartfollowstep/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laoe;


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:Landroid/content/Context;

.field private final c:Landroid/view/ViewGroup;

.field private final d:Landroid/support/v4/app/FragmentManager;

.field private final e:Lcom/twitter/app/common/dialog/ProgressDialogFragment;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/support/v4/app/FragmentManager;)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/twitter/app/onboarding/smartfollowstep/b;->b:Landroid/content/Context;

    .line 40
    iput-object p2, p0, Lcom/twitter/app/onboarding/smartfollowstep/b;->a:Landroid/view/LayoutInflater;

    .line 41
    iput-object p3, p0, Lcom/twitter/app/onboarding/smartfollowstep/b;->c:Landroid/view/ViewGroup;

    .line 42
    iput-object p4, p0, Lcom/twitter/app/onboarding/smartfollowstep/b;->d:Landroid/support/v4/app/FragmentManager;

    .line 43
    iget-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/b;->d:Landroid/support/v4/app/FragmentManager;

    const-string/jumbo v1, "tag_progress_dialog"

    .line 44
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    .line 45
    if-eqz v0, :cond_0

    .line 46
    iput-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/b;->e:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    .line 50
    :goto_0
    iget-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/b;->e:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->setRetainInstance(Z)V

    .line 51
    return-void

    .line 48
    :cond_0
    const v0, 0x7f0a04b6

    invoke-static {v0}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->a(I)Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/b;->e:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    goto :goto_0
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/support/v4/app/FragmentManager;)Lcom/twitter/app/onboarding/smartfollowstep/b;
    .locals 3

    .prologue
    .line 32
    const v0, 0x7f0403c0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 33
    new-instance v1, Lcom/twitter/app/onboarding/smartfollowstep/b;

    invoke-virtual {p0}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p0, v0, p1}, Lcom/twitter/app/onboarding/smartfollowstep/b;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/support/v4/app/FragmentManager;)V

    return-object v1
.end method


# virtual methods
.method public a(ILcom/twitter/android/smartfollow/a;)Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/b;->a:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/twitter/app/onboarding/smartfollowstep/b;->c:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    .line 63
    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;

    .line 65
    invoke-virtual {v0, p2}, Lcom/twitter/android/smartfollow/BaseSmartFollowScreen;->setPresenter(Lcom/twitter/android/smartfollow/a;)V

    .line 66
    iget-object v1, p0, Lcom/twitter/app/onboarding/smartfollowstep/b;->c:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 67
    return-object v0
.end method

.method public a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/b;->b:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 80
    return-void
.end method

.method public aN_()Landroid/view/View;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/b;->c:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/b;->e:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    iget-object v1, p0, Lcom/twitter/app/onboarding/smartfollowstep/b;->d:Landroid/support/v4/app/FragmentManager;

    const-string/jumbo v2, "tag_progress_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 72
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/twitter/app/onboarding/smartfollowstep/b;->e:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->dismissAllowingStateLoss()V

    .line 76
    return-void
.end method
