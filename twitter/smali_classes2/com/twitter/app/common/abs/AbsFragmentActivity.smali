.class public abstract Lcom/twitter/app/common/abs/AbsFragmentActivity;
.super Lcom/twitter/app/common/inject/InjectedFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcmr$a;
.implements Lcms;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/common/abs/AbsFragmentActivity$b;,
        Lcom/twitter/app/common/abs/AbsFragmentActivity$a;
    }
.end annotation


# instance fields
.field protected F:J

.field protected G:Lcom/twitter/library/client/p;

.field private final a:Lcom/twitter/library/client/u;

.field private b:Lcom/twitter/app/common/abs/j;

.field private c:Lcmt;

.field private d:Landroid/content/Intent;

.field private e:Lcom/twitter/app/common/abs/AbsFragmentActivity$a;

.field private f:Z

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;-><init>()V

    .line 93
    new-instance v0, Lcom/twitter/app/common/abs/AbsFragmentActivity$b;

    invoke-direct {v0, p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity$b;-><init>(Lcom/twitter/app/common/abs/AbsFragmentActivity;)V

    iput-object v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->a:Lcom/twitter/library/client/u;

    .line 96
    sget-object v0, Lcmt;->a:Lcmt;

    iput-object v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->c:Lcmt;

    return-void
.end method

.method private a(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 478
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    .line 479
    const-string/jumbo v0, "intent.extra.ANCESTOR"

    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->A_()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 480
    return-object p1
.end method

.method static synthetic a(Lcom/twitter/app/common/abs/AbsFragmentActivity;)Lcom/twitter/app/common/abs/AbsFragmentActivity$a;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->e:Lcom/twitter/app/common/abs/AbsFragmentActivity$a;

    return-object v0
.end method

.method private a(Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 4

    .prologue
    .line 210
    const v0, 0x7f13002a

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/navigation/ModernDrawerView;

    .line 211
    const v1, 0x7f13007f

    invoke-virtual {p0, v1}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/internal/android/widget/ToolBar;

    .line 212
    const/4 v2, 0x0

    .line 213
    if-eqz v1, :cond_1

    .line 214
    if-eqz v0, :cond_0

    .line 215
    new-instance v2, Lcom/twitter/library/client/navigation/g;

    iget v3, p1, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->d:I

    invoke-direct {v2, v0, v1, v3, p0}, Lcom/twitter/library/client/navigation/g;-><init>(Lcom/twitter/library/client/navigation/ModernDrawerView;Lcom/twitter/internal/android/widget/ToolBar;ILandroid/app/Activity;)V

    move-object v0, v2

    .line 221
    :goto_0
    invoke-static {v0}, Lcmt;->a(Lcmr;)Lcmt;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->c:Lcmt;

    .line 222
    return-void

    .line 218
    :cond_0
    new-instance v0, Lcom/twitter/library/client/navigation/h;

    iget v2, p1, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->d:I

    invoke-direct {v0, v1, v2, p0}, Lcom/twitter/library/client/navigation/h;-><init>(Lcom/twitter/internal/android/widget/ToolBar;ILandroid/app/Activity;)V

    goto :goto_0

    :cond_1
    move-object v0, v2

    goto :goto_0
.end method

.method private b(Landroid/content/Intent;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 537
    invoke-static {p1}, Lcom/twitter/util/v;->a(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 538
    const/4 v0, 0x0

    check-cast v0, Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->replaceExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move v0, v1

    .line 559
    :goto_0
    return v0

    .line 541
    :cond_0
    const-string/jumbo v0, "intent.extra.ANCESTOR"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 542
    const-string/jumbo v0, "intent.extra.ANCESTOR"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->d:Landroid/content/Intent;

    .line 543
    const-string/jumbo v0, "intent.extra.ANCESTOR"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 545
    :cond_1
    iput-boolean v1, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->f:Z

    .line 546
    const-string/jumbo v0, "AbsFragmentActivity_account_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 547
    if-eqz v0, :cond_3

    .line 549
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v1

    .line 550
    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 551
    invoke-virtual {v1, v0}, Lcom/twitter/library/client/v;->d(Ljava/lang/String;)V

    .line 552
    iput-boolean v2, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->f:Z

    .line 557
    :cond_2
    const-string/jumbo v0, "AbsFragmentActivity_account_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    :cond_3
    move v0, v2

    .line 559
    goto :goto_0
.end method

.method private b(Lcmm;)Z
    .locals 1

    .prologue
    .line 257
    invoke-interface {p1}, Lcmm;->n()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 258
    invoke-interface {p1}, Lcmm;->n()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 259
    const/4 v0, 0x1

    .line 261
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected A_()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->e:Lcom/twitter/app/common/abs/AbsFragmentActivity$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->e:Lcom/twitter/app/common/abs/AbsFragmentActivity$a;

    iget-boolean v0, v0, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->e:Z

    if-eqz v0, :cond_0

    .line 467
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 469
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->d:Landroid/content/Intent;

    goto :goto_0
.end method

.method public E()Lcom/twitter/internal/android/widget/ToolBar;
    .locals 1

    .prologue
    .line 248
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->d()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    return-object v0
.end method

.method public final F()Lcmt;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->c:Lcmt;

    return-object v0
.end method

.method protected G()V
    .locals 1

    .prologue
    .line 330
    invoke-static {p0}, Landroid/support/v4/app/NavUtils;->getParentActivityIntent(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, p0, v0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 331
    return-void
.end method

.method H()V
    .locals 1

    .prologue
    .line 395
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->g_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 396
    invoke-super {p0}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->onBackPressed()V

    .line 398
    :cond_0
    return-void
.end method

.method protected final I()Lcom/twitter/library/client/v;
    .locals 1

    .prologue
    .line 401
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    return-object v0
.end method

.method protected final J()Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 406
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method public final K()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->d:Landroid/content/Intent;

    return-object v0
.end method

.method protected a(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 321
    const/high16 v0, 0x4000000

    invoke-virtual {p2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 322
    invoke-virtual {p0, p2}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->e(Landroid/content/Intent;)V

    .line 323
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 324
    return-void
.end method

.method public abstract a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
.end method

.method protected a(Lcom/twitter/library/service/s;I)V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 440
    return-void
.end method

.method public a(Lcmm;)Z
    .locals 2

    .prologue
    .line 305
    invoke-interface {p1}, Lcmm;->a()I

    move-result v0

    .line 306
    const v1, 0x7f130043

    if-ne v0, v1, :cond_0

    .line 307
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->p()V

    .line 308
    const/4 v0, 0x1

    .line 310
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->b(Lcmm;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 277
    const/4 v0, 0x0

    return v0
.end method

.method public a_(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 301
    return-void
.end method

.method public b(Lcmr;)I
    .locals 1

    .prologue
    .line 289
    const/4 v0, 0x2

    return v0
.end method

.method protected b(Lank;)Lcom/twitter/app/common/abs/b;
    .locals 1

    .prologue
    .line 206
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final b(Lcom/twitter/library/service/s;I)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 416
    iget-object v1, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->b:Lcom/twitter/app/common/abs/j;

    iget-wide v2, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->F:J

    move-object v4, p1

    move v5, p2

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/app/common/abs/j;->a(JLcom/twitter/library/service/s;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 417
    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->c(Lcom/twitter/library/service/s;I)V

    .line 418
    const/4 v6, 0x1

    .line 420
    :cond_0
    return v6
.end method

.method protected synthetic c(Lank;)Lans;
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->b(Lank;)Lcom/twitter/app/common/abs/b;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcmr;)V
    .locals 0

    .prologue
    .line 297
    return-void
.end method

.method protected c(Lcom/twitter/library/service/s;I)V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 431
    return-void
.end method

.method protected d()V
    .locals 0

    .prologue
    .line 229
    return-void
.end method

.method public final d(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 462
    iput-object p1, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->d:Landroid/content/Intent;

    .line 463
    return-void
.end method

.method public e(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 533
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-super {p0, p1, v0, v1}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 534
    return-void
.end method

.method protected f(Lank;)Lcom/twitter/app/common/abs/a;
    .locals 2

    .prologue
    .line 198
    invoke-static {}, Lcom/twitter/app/common/abs/g;->a()Lcom/twitter/app/common/abs/g$a;

    move-result-object v0

    .line 199
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/abs/g$a;->a(Lamu;)Lcom/twitter/app/common/abs/g$a;

    move-result-object v0

    .line 200
    invoke-virtual {v0}, Lcom/twitter/app/common/abs/g$a;->a()Lcom/twitter/app/common/abs/a;

    move-result-object v0

    .line 198
    return-object v0
.end method

.method protected synthetic g(Lank;)Lann;
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->f(Lank;)Lcom/twitter/app/common/abs/a;

    move-result-object v0

    return-object v0
.end method

.method public g(Landroid/os/Bundle;)Lcom/twitter/app/common/abs/AbsFragmentActivity$a;
    .locals 1

    .prologue
    .line 506
    const/4 v0, 0x0

    return-object v0
.end method

.method public onAttachedToWindow()V
    .locals 2

    .prologue
    .line 241
    invoke-super {p0}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->onAttachedToWindow()V

    .line 242
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 243
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setFormat(I)V

    .line 244
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 374
    iget-boolean v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->f:Z

    if-eqz v0, :cond_0

    .line 375
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->f:Z

    .line 377
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->H()V

    .line 378
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 117
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->F:J

    .line 118
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->G:Lcom/twitter/library/client/p;

    .line 119
    invoke-super {p0, p1}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 121
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->X()Lann;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/a;

    .line 122
    invoke-interface {v0}, Lcom/twitter/app/common/abs/a;->c()Lcom/twitter/app/common/abs/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->b:Lcom/twitter/app/common/abs/j;

    .line 123
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->b:Lcom/twitter/app/common/abs/j;

    new-instance v1, Lcom/twitter/app/common/abs/AbsFragmentActivity$1;

    invoke-direct {v1, p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity$1;-><init>(Lcom/twitter/app/common/abs/AbsFragmentActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/abs/j;->a(Lcom/twitter/app/common/abs/j$b;)V

    .line 132
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->W()Lanh;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->b:Lcom/twitter/app/common/abs/j;

    invoke-virtual {v0, v1}, Lanh;->a(Ljava/lang/Object;)Lanh;

    .line 134
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->g(Landroid/os/Bundle;)Lcom/twitter/app/common/abs/AbsFragmentActivity$a;

    move-result-object v0

    .line 136
    if-nez v0, :cond_1

    .line 137
    new-instance v0, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;

    invoke-direct {v0}, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;-><init>()V

    move-object v1, v0

    .line 141
    :goto_0
    iput-boolean v2, v1, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->a:Z

    .line 142
    iput-object v1, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->e:Lcom/twitter/app/common/abs/AbsFragmentActivity$a;

    .line 145
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 146
    invoke-direct {p0, v0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->b(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 149
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->p()V

    .line 153
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->finish()V

    .line 193
    :cond_0
    :goto_1
    return-void

    :cond_1
    move-object v1, v0

    .line 139
    goto :goto_0

    .line 158
    :cond_2
    iget v0, v1, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->b:I

    if-eqz v0, :cond_3

    .line 160
    new-instance v0, Lcom/twitter/app/common/abs/AbsFragmentActivity$2;

    invoke-direct {v0, p0, v1}, Lcom/twitter/app/common/abs/AbsFragmentActivity$2;-><init>(Lcom/twitter/app/common/abs/AbsFragmentActivity;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V

    invoke-static {v0}, Lcom/twitter/util/x;->a(Lcom/twitter/util/concurrent/i;)Ljava/lang/Object;

    .line 170
    :cond_3
    iget-boolean v0, v1, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->c:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-nez v0, :cond_4

    .line 171
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->p_()V

    .line 172
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->finish()V

    goto :goto_1

    .line 176
    :cond_4
    if-eqz p1, :cond_5

    .line 177
    const-string/jumbo v0, "ancestor_root_intent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->d:Landroid/content/Intent;

    .line 180
    :cond_5
    invoke-direct {p0, v1}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->a(Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V

    .line 182
    invoke-virtual {p0, p1, v1}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V

    .line 183
    iput-boolean v2, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->g:Z

    .line 185
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->c:Lcmt;

    invoke-virtual {v0}, Lcmt;->a()Z

    move-result v0

    if-nez v0, :cond_6

    .line 187
    invoke-direct {p0, v1}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->a(Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V

    .line 190
    :cond_6
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->c:Lcmt;

    invoke-virtual {v0, p0}, Lcmt;->a(Lcms;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->c:Lcmt;

    invoke-virtual {v0, p0}, Lcmt;->a(Lcmr$a;)V

    goto :goto_1
.end method

.method protected final onDestroy()V
    .locals 1

    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->g:Z

    if-eqz v0, :cond_0

    .line 234
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->d()V

    .line 236
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->onDestroy()V

    .line 237
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 359
    invoke-super {p0, p1}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 360
    invoke-direct {p0, p1}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->b(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->setIntent(Landroid/content/Intent;)V

    .line 363
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 347
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->a:Lcom/twitter/library/client/u;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/u;)V

    .line 348
    invoke-super {p0}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->onPause()V

    .line 349
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 335
    invoke-super {p0}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->onResume()V

    .line 336
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->e:Lcom/twitter/app/common/abs/AbsFragmentActivity$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->e:Lcom/twitter/app/common/abs/AbsFragmentActivity$a;

    iget-boolean v0, v0, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->c:Z

    if-eqz v0, :cond_0

    .line 337
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 338
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->p_()V

    .line 343
    :goto_0
    return-void

    .line 342
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->a:Lcom/twitter/library/client/u;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/u;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 353
    invoke-super {p0, p1}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 354
    const-string/jumbo v0, "ancestor_root_intent"

    iget-object v1, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity;->d:Landroid/content/Intent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 355
    return-void
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 367
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcmt;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 368
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->onTitleChanged(Ljava/lang/CharSequence;I)V

    .line 370
    :cond_0
    return-void
.end method

.method protected abstract p()V
.end method

.method protected abstract p_()V
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 485
    invoke-direct {p0, p1}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-super {p0, v0, p2}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 486
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 495
    invoke-direct {p0, p1}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-super {p0, v0, p2, p3}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 496
    return-void
.end method

.method public startActivityFromFragment(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 490
    invoke-direct {p0, p2}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-super {p0, p1, v0, p3}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->startActivityFromFragment(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    .line 491
    return-void
.end method
