.class public Lcom/twitter/app/common/abs/AbsFragmentActivity$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/common/abs/AbsFragmentActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field a:Z

.field b:I

.field c:Z

.field d:I

.field e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 570
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 565
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->c:Z

    .line 566
    const/16 v0, 0xe

    iput v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->d:I

    .line 571
    return-void
.end method

.method protected constructor <init>(Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 1

    .prologue
    .line 573
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 565
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->c:Z

    .line 566
    const/16 v0, 0xe

    iput v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->d:I

    .line 574
    iget-boolean v0, p1, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->a:Z

    iput-boolean v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->a:Z

    .line 575
    iget v0, p1, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->b:I

    iput v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->b:I

    .line 576
    iget-boolean v0, p1, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->c:Z

    iput-boolean v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->c:Z

    .line 577
    iget v0, p1, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->d:I

    iput v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->d:I

    .line 578
    iget-boolean v0, p1, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->e:Z

    iput-boolean v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->e:Z

    .line 579
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 620
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->c()V

    .line 621
    iput p1, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->d:I

    .line 622
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 596
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->c()V

    .line 597
    iput-boolean p1, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->c:Z

    .line 598
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 586
    iget-boolean v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->c:Z

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 606
    iget v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->d:I

    return v0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 643
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->b()I

    move-result v0

    or-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->a(I)V

    .line 644
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 681
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->c()V

    .line 682
    iput-boolean p1, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->e:Z

    .line 683
    return-void
.end method

.method protected final c()V
    .locals 2

    .prologue
    .line 686
    iget-boolean v0, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->a:Z

    if-eqz v0, :cond_0

    .line 687
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Options are already configured!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 689
    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 0

    .prologue
    .line 661
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->c()V

    .line 662
    iput p1, p0, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;->b:I

    .line 663
    return-void
.end method
