.class public final Lcom/twitter/app/common/abs/i;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/abs/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/common/abs/i$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/abs/j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/twitter/app/common/abs/i;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/app/common/abs/i;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/twitter/app/common/abs/i$a;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    sget-boolean v0, Lcom/twitter/app/common/abs/i;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 27
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/app/common/abs/i;->a(Lcom/twitter/app/common/abs/i$a;)V

    .line 28
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/app/common/abs/i$a;Lcom/twitter/app/common/abs/i$1;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/twitter/app/common/abs/i;-><init>(Lcom/twitter/app/common/abs/i$a;)V

    return-void
.end method

.method private a(Lcom/twitter/app/common/abs/i$a;)V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/twitter/app/common/abs/i$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/app/common/abs/i$1;-><init>(Lcom/twitter/app/common/abs/i;Lcom/twitter/app/common/abs/i$a;)V

    iput-object v0, p0, Lcom/twitter/app/common/abs/i;->b:Lcta;

    .line 50
    iget-object v0, p0, Lcom/twitter/app/common/abs/i;->b:Lcta;

    .line 52
    invoke-static {v0}, Lcom/twitter/app/common/abs/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 51
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/abs/i;->c:Lcta;

    .line 53
    return-void
.end method

.method public static c()Lcom/twitter/app/common/abs/i$a;
    .locals 2

    .prologue
    .line 31
    new-instance v0, Lcom/twitter/app/common/abs/i$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/app/common/abs/i$a;-><init>(Lcom/twitter/app/common/abs/i$1;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/app/common/abs/j;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/app/common/abs/i;->c:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/j;

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    invoke-static {}, Ldagger/internal/f;->a()Ldagger/internal/c;

    move-result-object v0

    invoke-interface {v0}, Ldagger/internal/c;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method
