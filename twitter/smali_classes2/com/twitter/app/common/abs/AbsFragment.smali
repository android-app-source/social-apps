.class public abstract Lcom/twitter/app/common/abs/AbsFragment;
.super Lcom/twitter/app/common/inject/InjectedFragment;
.source "Twttr"


# instance fields
.field protected S:Lcom/twitter/library/client/p;

.field private a:Lcom/twitter/app/common/abs/j;

.field protected a_:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/twitter/app/common/inject/InjectedFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected final X()Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 124
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 120
    return-void
.end method

.method protected b(Lcom/twitter/library/service/s;II)V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 111
    return-void
.end method

.method protected c(Lank;)Lcom/twitter/app/common/abs/c;
    .locals 2

    .prologue
    .line 61
    invoke-static {}, Lcom/twitter/app/common/abs/h;->a()Lcom/twitter/app/common/abs/h$a;

    move-result-object v0

    .line 62
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/abs/h$a;->a(Lamu;)Lcom/twitter/app/common/abs/h$a;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lcom/twitter/app/common/abs/h$a;->a()Lcom/twitter/app/common/abs/c;

    move-result-object v0

    .line 61
    return-object v0
.end method

.method protected final c(Lcom/twitter/library/service/s;II)Z
    .locals 7

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragment;->Y()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p3}, Lcom/twitter/app/common/abs/AbsFragment;->c_(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/app/common/abs/AbsFragment;->a:Lcom/twitter/app/common/abs/j;

    iget-wide v2, p0, Lcom/twitter/app/common/abs/AbsFragment;->a_:J

    move-object v4, p1

    move v5, p2

    move v6, p3

    .line 97
    invoke-virtual/range {v1 .. v6}, Lcom/twitter/app/common/abs/j;->a(JLcom/twitter/library/service/s;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/app/common/abs/AbsFragment;->b(Lcom/twitter/library/service/s;II)V

    .line 99
    const/4 v0, 0x1

    .line 101
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected c_(I)Z
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x1

    return v0
.end method

.method protected synthetic d(Lank;)Lann;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->c(Lank;)Lcom/twitter/app/common/abs/c;

    move-result-object v0

    return-object v0
.end method

.method protected d(I)Z
    .locals 4

    .prologue
    .line 73
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsFragment;->a:Lcom/twitter/app/common/abs/j;

    iget-wide v2, p0, Lcom/twitter/app/common/abs/AbsFragment;->a_:J

    invoke-virtual {v0, v2, v3, p1}, Lcom/twitter/app/common/abs/j;->a(JI)Z

    move-result v0

    return v0
.end method

.method protected g(Lank;)Lcom/twitter/app/common/abs/d;
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    return-object v0
.end method

.method protected synthetic h(Lank;)Lans;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->g(Lank;)Lcom/twitter/app/common/abs/d;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/app/common/abs/AbsFragment;->a_:J

    .line 41
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/abs/AbsFragment;->S:Lcom/twitter/library/client/p;

    .line 42
    invoke-super {p0, p1}, Lcom/twitter/app/common/inject/InjectedFragment;->onCreate(Landroid/os/Bundle;)V

    .line 44
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragment;->ah()Lann;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/c;

    .line 45
    invoke-interface {v0}, Lcom/twitter/app/common/abs/c;->c()Lcom/twitter/app/common/abs/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/abs/AbsFragment;->a:Lcom/twitter/app/common/abs/j;

    .line 46
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsFragment;->a:Lcom/twitter/app/common/abs/j;

    new-instance v1, Lcom/twitter/app/common/abs/AbsFragment$1;

    invoke-direct {v1, p0}, Lcom/twitter/app/common/abs/AbsFragment$1;-><init>(Lcom/twitter/app/common/abs/AbsFragment;)V

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/abs/j;->a(Lcom/twitter/app/common/abs/j$b;)V

    .line 55
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsFragment;->ag()Lanh;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/common/abs/AbsFragment;->a:Lcom/twitter/app/common/abs/j;

    invoke-virtual {v0, v1}, Lanh;->a(Ljava/lang/Object;)Lanh;

    .line 56
    return-void
.end method

.method protected v_()Z
    .locals 4

    .prologue
    .line 77
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsFragment;->a:Lcom/twitter/app/common/abs/j;

    iget-wide v2, p0, Lcom/twitter/app/common/abs/AbsFragment;->a_:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/app/common/abs/j;->a(J)Z

    move-result v0

    return v0
.end method

.method protected w_()Ljava/lang/Iterable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/app/common/abs/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsFragment;->a:Lcom/twitter/app/common/abs/j;

    iget-wide v2, p0, Lcom/twitter/app/common/abs/AbsFragment;->a_:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/app/common/abs/j;->b(J)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method
