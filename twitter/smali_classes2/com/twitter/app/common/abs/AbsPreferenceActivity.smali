.class public abstract Lcom/twitter/app/common/abs/AbsPreferenceActivity;
.super Lcom/twitter/app/common/inject/InjectedPreferenceActivity;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/common/abs/AbsPreferenceActivity$c;,
        Lcom/twitter/app/common/abs/AbsPreferenceActivity$b;,
        Lcom/twitter/app/common/abs/AbsPreferenceActivity$a;
    }
.end annotation


# instance fields
.field protected G:J

.field protected H:Lcom/twitter/library/client/p;

.field private a:Lcom/twitter/library/client/v;

.field private b:Lcom/twitter/app/common/abs/j;

.field private c:Lcom/twitter/internal/android/widget/ToolBar;

.field private d:Lcom/twitter/app/common/abs/AbsPreferenceActivity$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;-><init>()V

    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 168
    const v0, 0x7f13007f

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 169
    if-eqz v0, :cond_3

    .line 170
    new-instance v1, Lazu;

    invoke-direct {v1, p0}, Lazu;-><init>(Landroid/content/Context;)V

    .line 171
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 172
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_4

    .line 173
    new-instance v2, Lcom/twitter/app/common/abs/AbsPreferenceActivity$c;

    invoke-direct {v2, p0, v1, v0}, Lcom/twitter/app/common/abs/AbsPreferenceActivity$c;-><init>(Lcom/twitter/app/common/abs/AbsPreferenceActivity;Lazu;Lcom/twitter/internal/android/widget/ToolBar;)V

    iput-object v2, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->d:Lcom/twitter/app/common/abs/AbsPreferenceActivity$a;

    .line 178
    :cond_0
    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->a(Lazu;Lcom/twitter/internal/android/widget/ToolBar;)Z

    move-result v1

    .line 179
    iget-object v2, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->d:Lcom/twitter/app/common/abs/AbsPreferenceActivity$a;

    if-nez v2, :cond_1

    if-eqz v1, :cond_6

    .line 180
    :cond_1
    iput-object v0, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->c:Lcom/twitter/internal/android/widget/ToolBar;

    .line 181
    new-instance v1, Lcom/twitter/app/common/abs/AbsPreferenceActivity$2;

    invoke-direct {v1, p0}, Lcom/twitter/app/common/abs/AbsPreferenceActivity$2;-><init>(Lcom/twitter/app/common/abs/AbsPreferenceActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setOnToolBarItemSelectedListener(Lcmr$a;)V

    .line 192
    invoke-virtual {p0, v0}, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)Z

    move-result v1

    .line 193
    iget-object v2, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->d:Lcom/twitter/app/common/abs/AbsPreferenceActivity$a;

    if-nez v2, :cond_2

    if-eqz v1, :cond_5

    .line 194
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setVisibility(I)V

    .line 202
    :cond_3
    :goto_1
    return-void

    .line 175
    :cond_4
    new-instance v2, Lcom/twitter/app/common/abs/AbsPreferenceActivity$b;

    invoke-direct {v2, p0, v1, v0}, Lcom/twitter/app/common/abs/AbsPreferenceActivity$b;-><init>(Lcom/twitter/app/common/abs/AbsPreferenceActivity;Lazu;Lcom/twitter/internal/android/widget/ToolBar;)V

    iput-object v2, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->d:Lcom/twitter/app/common/abs/AbsPreferenceActivity$a;

    goto :goto_0

    .line 196
    :cond_5
    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ToolBar;->setVisibility(I)V

    goto :goto_1

    .line 199
    :cond_6
    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ToolBar;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method protected a(Lank;)Lcom/twitter/app/common/abs/e;
    .locals 2

    .prologue
    .line 96
    invoke-static {}, Lcom/twitter/app/common/abs/i;->c()Lcom/twitter/app/common/abs/i$a;

    move-result-object v0

    .line 97
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/abs/i$a;->a(Lamu;)Lcom/twitter/app/common/abs/i$a;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Lcom/twitter/app/common/abs/i$a;->a()Lcom/twitter/app/common/abs/e;

    move-result-object v0

    .line 96
    return-object v0
.end method

.method protected a(Lcom/twitter/library/service/s;I)V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 156
    return-void
.end method

.method protected a(Lazu;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x1

    return v0
.end method

.method protected a(Lazv;)Z
    .locals 2

    .prologue
    .line 223
    invoke-virtual {p1}, Lazv;->a()I

    move-result v0

    const v1, 0x7f130043

    if-ne v0, v1, :cond_0

    .line 224
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->finish()V

    .line 226
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x1

    return v0
.end method

.method protected a(Z)Z
    .locals 1

    .prologue
    .line 281
    const/4 v0, 0x1

    return v0
.end method

.method a(ZZ)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 266
    iget-object v1, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->d:Lcom/twitter/app/common/abs/AbsPreferenceActivity$a;

    if-nez v1, :cond_0

    .line 273
    :goto_0
    return v0

    .line 268
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p0, p1}, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->a(Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 269
    :cond_1
    iget-object v1, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->d:Lcom/twitter/app/common/abs/AbsPreferenceActivity$a;

    invoke-interface {v1, p1}, Lcom/twitter/app/common/abs/AbsPreferenceActivity$a;->a(Z)V

    goto :goto_0

    .line 273
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic b(Lank;)Lann;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->a(Lank;)Lcom/twitter/app/common/abs/e;

    move-result-object v0

    return-object v0
.end method

.method protected final b(Lcom/twitter/library/service/s;I)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 132
    iget-object v1, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->b:Lcom/twitter/app/common/abs/j;

    iget-wide v2, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->G:J

    move-object v4, p1

    move v5, p2

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/app/common/abs/j;->a(JLcom/twitter/library/service/s;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->c(Lcom/twitter/library/service/s;I)V

    .line 134
    const/4 v6, 0x1

    .line 136
    :cond_0
    return v6
.end method

.method protected c(Lank;)Lcom/twitter/app/common/abs/f;
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x0

    return-object v0
.end method

.method protected c(Lcom/twitter/library/service/s;I)V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 147
    return-void
.end method

.method protected c(Z)V
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->a(ZZ)Z

    .line 263
    return-void
.end method

.method protected synthetic d(Lank;)Lans;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->c(Lank;)Lcom/twitter/app/common/abs/f;

    move-result-object v0

    return-object v0
.end method

.method protected d(Z)V
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->d:Lcom/twitter/app/common/abs/AbsPreferenceActivity$a;

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->d:Lcom/twitter/app/common/abs/AbsPreferenceActivity$a;

    invoke-interface {v0, p1}, Lcom/twitter/app/common/abs/AbsPreferenceActivity$a;->b(Z)V

    .line 288
    :cond_0
    return-void
.end method

.method protected h()Z
    .locals 1

    .prologue
    .line 245
    const/4 v0, 0x0

    return v0
.end method

.method public j()Lcom/twitter/library/client/v;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->a:Lcom/twitter/library/client/v;

    return-object v0
.end method

.method protected final k()Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->a:Lcom/twitter/library/client/v;

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method protected l()Z
    .locals 1

    .prologue
    .line 251
    const/4 v0, 0x0

    return v0
.end method

.method protected m()Z
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->d:Lcom/twitter/app/common/abs/AbsPreferenceActivity$a;

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->d:Lcom/twitter/app/common/abs/AbsPreferenceActivity$a;

    invoke-interface {v0}, Lcom/twitter/app/common/abs/AbsPreferenceActivity$a;->a()Z

    move-result v0

    .line 258
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->isEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 75
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->a:Lcom/twitter/library/client/v;

    .line 76
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->H:Lcom/twitter/library/client/p;

    .line 77
    invoke-super {p0, p1}, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 79
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->o()Lann;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/e;

    .line 80
    invoke-interface {v0}, Lcom/twitter/app/common/abs/e;->a()Lcom/twitter/app/common/abs/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->b:Lcom/twitter/app/common/abs/j;

    .line 81
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->b:Lcom/twitter/app/common/abs/j;

    new-instance v1, Lcom/twitter/app/common/abs/AbsPreferenceActivity$1;

    invoke-direct {v1, p0}, Lcom/twitter/app/common/abs/AbsPreferenceActivity$1;-><init>(Lcom/twitter/app/common/abs/AbsPreferenceActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/abs/j;->a(Lcom/twitter/app/common/abs/j$b;)V

    .line 90
    invoke-virtual {p0}, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->n()Lanh;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->b:Lcom/twitter/app/common/abs/j;

    invoke-virtual {v0, v1}, Lanh;->a(Ljava/lang/Object;)Lanh;

    .line 91
    return-void
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->c:Lcom/twitter/internal/android/widget/ToolBar;

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->c:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 165
    :goto_0
    return-void

    .line 163
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;->onTitleChanged(Ljava/lang/CharSequence;I)V

    goto :goto_0
.end method

.method public final setContentView(I)V
    .locals 0

    .prologue
    .line 109
    invoke-super {p0, p1}, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;->setContentView(I)V

    .line 110
    invoke-direct {p0}, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->c()V

    .line 111
    return-void
.end method

.method public final setContentView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 115
    invoke-super {p0, p1}, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;->setContentView(Landroid/view/View;)V

    .line 116
    invoke-direct {p0}, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->c()V

    .line 117
    return-void
.end method

.method public final setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 121
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 122
    invoke-direct {p0}, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->c()V

    .line 123
    return-void
.end method
