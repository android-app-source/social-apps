.class public final Lcom/twitter/app/common/abs/h$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/common/abs/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private a:Lamu;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/app/common/abs/h$1;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/twitter/app/common/abs/h$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/common/abs/h$a;)Lamu;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/app/common/abs/h$a;->a:Lamu;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/app/common/abs/c;
    .locals 3

    .prologue
    .line 70
    iget-object v0, p0, Lcom/twitter/app/common/abs/h$a;->a:Lamu;

    if-nez v0, :cond_0

    .line 71
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lamu;

    .line 72
    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_0
    new-instance v0, Lcom/twitter/app/common/abs/h;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/app/common/abs/h;-><init>(Lcom/twitter/app/common/abs/h$a;Lcom/twitter/app/common/abs/h$1;)V

    return-object v0
.end method

.method public a(Lamu;)Lcom/twitter/app/common/abs/h$a;
    .locals 1

    .prologue
    .line 88
    invoke-static {p1}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamu;

    iput-object v0, p0, Lcom/twitter/app/common/abs/h$a;->a:Lamu;

    .line 89
    return-object p0
.end method
