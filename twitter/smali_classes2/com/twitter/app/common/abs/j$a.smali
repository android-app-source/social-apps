.class Lcom/twitter/app/common/abs/j$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/client/s;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/common/abs/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/library/client/s;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/app/common/abs/j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/twitter/library/client/s;Lcom/twitter/app/common/abs/j;)V
    .locals 1

    .prologue
    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 194
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/app/common/abs/j$a;->a:Ljava/lang/ref/WeakReference;

    .line 195
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/app/common/abs/j$a;->b:Ljava/lang/ref/WeakReference;

    .line 196
    return-void
.end method

.method private a()Lcom/twitter/library/client/s;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/twitter/app/common/abs/j$a;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/j;

    .line 225
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/app/common/abs/j;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/common/abs/j$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/s;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;Lcom/twitter/library/service/s;)V
    .locals 1

    .prologue
    .line 208
    invoke-direct {p0}, Lcom/twitter/app/common/abs/j$a;->a()Lcom/twitter/library/client/s;

    move-result-object v0

    .line 209
    if-eqz v0, :cond_0

    .line 210
    invoke-interface {v0, p1, p2, p3}, Lcom/twitter/library/client/s;->a(ILandroid/os/Bundle;Lcom/twitter/library/service/s;)V

    .line 212
    :cond_0
    return-void
.end method

.method public a(ILcom/twitter/library/service/s;)V
    .locals 1

    .prologue
    .line 200
    invoke-direct {p0}, Lcom/twitter/app/common/abs/j$a;->a()Lcom/twitter/library/client/s;

    move-result-object v0

    .line 201
    if-eqz v0, :cond_0

    .line 202
    invoke-interface {v0, p1, p2}, Lcom/twitter/library/client/s;->a(ILcom/twitter/library/service/s;)V

    .line 204
    :cond_0
    return-void
.end method

.method public b(ILcom/twitter/library/service/s;)V
    .locals 1

    .prologue
    .line 216
    invoke-direct {p0}, Lcom/twitter/app/common/abs/j$a;->a()Lcom/twitter/library/client/s;

    move-result-object v0

    .line 217
    if-eqz v0, :cond_0

    .line 218
    invoke-interface {v0, p1, p2}, Lcom/twitter/library/client/s;->b(ILcom/twitter/library/service/s;)V

    .line 220
    :cond_0
    return-void
.end method
