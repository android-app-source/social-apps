.class public Lcom/twitter/app/common/abs/j;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lanj;
.implements Lanl;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/common/abs/j$a;,
        Lcom/twitter/app/common/abs/j$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/client/p;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/app/common/abs/l;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/app/common/abs/j$a;

.field private final d:Lcom/twitter/library/client/s;

.field private e:Z

.field private f:Lcom/twitter/app/common/abs/j$b;


# direct methods
.method public constructor <init>(Lcom/twitter/library/client/p;)V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x5

    invoke-static {v0}, Lcom/twitter/util/collection/MutableList;->a(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/abs/j;->b:Ljava/util/List;

    .line 39
    new-instance v0, Lcom/twitter/app/common/abs/j$1;

    invoke-direct {v0, p0}, Lcom/twitter/app/common/abs/j$1;-><init>(Lcom/twitter/app/common/abs/j;)V

    iput-object v0, p0, Lcom/twitter/app/common/abs/j;->d:Lcom/twitter/library/client/s;

    .line 63
    iput-object p1, p0, Lcom/twitter/app/common/abs/j;->a:Lcom/twitter/library/client/p;

    .line 64
    new-instance v0, Lcom/twitter/app/common/abs/j$a;

    iget-object v1, p0, Lcom/twitter/app/common/abs/j;->d:Lcom/twitter/library/client/s;

    invoke-direct {v0, v1, p0}, Lcom/twitter/app/common/abs/j$a;-><init>(Lcom/twitter/library/client/s;Lcom/twitter/app/common/abs/j;)V

    iput-object v0, p0, Lcom/twitter/app/common/abs/j;->c:Lcom/twitter/app/common/abs/j$a;

    .line 65
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/common/abs/j;)Lcom/twitter/app/common/abs/j$b;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/app/common/abs/j;->f:Lcom/twitter/app/common/abs/j$b;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/app/common/abs/j;Lcom/twitter/library/service/s;)Lcom/twitter/app/common/abs/l;
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/twitter/app/common/abs/j;->a(Lcom/twitter/library/service/s;)Lcom/twitter/app/common/abs/l;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/twitter/library/service/s;)Lcom/twitter/app/common/abs/l;
    .locals 4

    .prologue
    .line 167
    iget-object v2, p0, Lcom/twitter/app/common/abs/j;->b:Ljava/util/List;

    .line 168
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 169
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 170
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/l;

    .line 171
    iget-object v0, v0, Lcom/twitter/app/common/abs/l;->e:Lcom/twitter/library/service/s;

    if-ne v0, p1, :cond_0

    .line 172
    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/l;

    .line 175
    :goto_1
    return-object v0

    .line 169
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 175
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private c()V
    .locals 5

    .prologue
    .line 150
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v1

    .line 151
    iget-object v0, p0, Lcom/twitter/app/common/abs/j;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/l;

    .line 152
    iget-object v3, p0, Lcom/twitter/app/common/abs/j;->a:Lcom/twitter/library/client/p;

    iget-object v4, v0, Lcom/twitter/app/common/abs/l;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/twitter/library/client/p;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 153
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 156
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/l;

    .line 157
    iget-object v2, p0, Lcom/twitter/app/common/abs/j;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 159
    iget-object v2, v0, Lcom/twitter/app/common/abs/l;->e:Lcom/twitter/library/service/s;

    invoke-virtual {v2}, Lcom/twitter/library/service/s;->isDone()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/app/common/abs/j;->f:Lcom/twitter/app/common/abs/j$b;

    if-eqz v2, :cond_2

    .line 160
    iget-object v2, p0, Lcom/twitter/app/common/abs/j;->f:Lcom/twitter/app/common/abs/j$b;

    invoke-interface {v2, v0}, Lcom/twitter/app/common/abs/j$b;->a(Lcom/twitter/app/common/abs/l;)V

    goto :goto_1

    .line 163
    :cond_3
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/app/common/abs/j$b;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/twitter/app/common/abs/j;->f:Lcom/twitter/app/common/abs/j$b;

    .line 72
    return-void
.end method

.method public a(J)Z
    .locals 5

    .prologue
    .line 109
    iget-object v0, p0, Lcom/twitter/app/common/abs/j;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/l;

    .line 110
    iget-wide v2, v0, Lcom/twitter/app/common/abs/l;->d:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    iget v0, v0, Lcom/twitter/app/common/abs/l;->c:I

    if-eqz v0, :cond_0

    .line 111
    const/4 v0, 0x1

    .line 114
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(JI)Z
    .locals 5

    .prologue
    .line 96
    iget-object v0, p0, Lcom/twitter/app/common/abs/j;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/l;

    .line 97
    iget-wide v2, v0, Lcom/twitter/app/common/abs/l;->d:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    iget v0, v0, Lcom/twitter/app/common/abs/l;->c:I

    if-ne v0, p3, :cond_0

    .line 98
    const/4 v0, 0x1

    .line 101
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(JLcom/twitter/library/service/s;II)Z
    .locals 9

    .prologue
    .line 134
    iget-object v0, p0, Lcom/twitter/app/common/abs/j;->a:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/app/common/abs/j;->c:Lcom/twitter/app/common/abs/j$a;

    invoke-virtual {v0, p3, p4, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;ILcom/twitter/library/client/s;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/twitter/app/common/abs/j;->b:Ljava/util/List;

    new-instance v1, Lcom/twitter/app/common/abs/l;

    iget-object v2, p3, Lcom/twitter/library/service/s;->d:Ljava/lang/String;

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-wide v6, p1

    invoke-direct/range {v1 .. v7}, Lcom/twitter/app/common/abs/l;-><init>(Ljava/lang/String;Lcom/twitter/library/service/s;IIJ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    const/4 v0, 0x1

    .line 138
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aB_()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/common/abs/j;->e:Z

    .line 78
    invoke-direct {p0}, Lcom/twitter/app/common/abs/j;->c()V

    .line 79
    return-void
.end method

.method public am_()V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/app/common/abs/j;->f:Lcom/twitter/app/common/abs/j$b;

    .line 89
    return-void
.end method

.method public b(J)Ljava/lang/Iterable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/app/common/abs/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lcom/twitter/app/common/abs/j;->b:Ljava/util/List;

    new-instance v1, Lcom/twitter/app/common/abs/j$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/twitter/app/common/abs/j$2;-><init>(Lcom/twitter/app/common/abs/j;J)V

    invoke-static {v0, v1}, Lcpt;->a(Ljava/lang/Iterable;Lcpv;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method b()Z
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/twitter/app/common/abs/j;->e:Z

    return v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/app/common/abs/j;->e:Z

    .line 84
    return-void
.end method
