.class Lcom/twitter/app/common/abs/AbsPreferenceActivity$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/abs/AbsPreferenceActivity$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/common/abs/AbsPreferenceActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field protected final a:Landroid/widget/Switch;

.field final synthetic b:Lcom/twitter/app/common/abs/AbsPreferenceActivity;

.field private final c:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/twitter/app/common/abs/AbsPreferenceActivity;Lazu;Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 2

    .prologue
    .line 315
    iput-object p1, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity$b;->b:Lcom/twitter/app/common/abs/AbsPreferenceActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 313
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity$b;->c:Landroid/os/Handler;

    .line 316
    const v0, 0x7f140023

    invoke-virtual {p2, v0, p3}, Lazu;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    .line 317
    const v0, 0x7f1308bb

    invoke-virtual {p3, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    .line 318
    invoke-virtual {v0}, Lazv;->e()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity$b;->a:Landroid/widget/Switch;

    .line 319
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity$b;->a:Landroid/widget/Switch;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity$b;->a:Landroid/widget/Switch;

    invoke-virtual {p1}, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->l()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 321
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity$b;->a:Landroid/widget/Switch;

    new-instance v1, Lcom/twitter/app/common/abs/AbsPreferenceActivity$b$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/app/common/abs/AbsPreferenceActivity$b$1;-><init>(Lcom/twitter/app/common/abs/AbsPreferenceActivity$b;Lcom/twitter/app/common/abs/AbsPreferenceActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 333
    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity$b;->b:Lcom/twitter/app/common/abs/AbsPreferenceActivity;

    invoke-virtual {v0}, Lcom/twitter/app/common/abs/AbsPreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    .line 338
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity$b;->a:Landroid/widget/Switch;

    invoke-virtual {v0, p1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 339
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity$b;->a:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    return v0
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity$b;->a:Landroid/widget/Switch;

    invoke-virtual {v0, p1}, Landroid/widget/Switch;->setEnabled(Z)V

    .line 361
    return-void
.end method

.method protected c(Z)V
    .locals 2

    .prologue
    .line 345
    iget-object v0, p0, Lcom/twitter/app/common/abs/AbsPreferenceActivity$b;->c:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/app/common/abs/AbsPreferenceActivity$b$2;

    invoke-direct {v1, p0, p1}, Lcom/twitter/app/common/abs/AbsPreferenceActivity$b$2;-><init>(Lcom/twitter/app/common/abs/AbsPreferenceActivity$b;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 351
    return-void
.end method
