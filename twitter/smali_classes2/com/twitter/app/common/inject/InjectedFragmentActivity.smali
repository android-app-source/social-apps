.class public Lcom/twitter/app/common/inject/InjectedFragmentActivity;
.super Lcom/twitter/app/common/base/BaseFragmentActivity;
.source "Twttr"


# instance fields
.field private a:Lani;

.field private b:Lann;

.field private c:Lans;

.field private d:Laog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public W()Lanh;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->a:Lani;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lanh;

    return-object v0
.end method

.method protected X()Lann;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<RC::",
            "Lann;",
            ">()TRC;"
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->b:Lann;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lann;

    return-object v0
.end method

.method public Y()Lans;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<AC::",
            "Lans;",
            ">()TAC;"
        }
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->c:Lans;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lans;

    return-object v0
.end method

.method public final Z()Laog;
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->d:Laog;

    if-nez v0, :cond_0

    .line 133
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "The view host is not available."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->d:Laog;

    return-object v0
.end method

.method protected a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Laog;
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a(Laog;)V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 123
    return-void
.end method

.method public final aa()Z
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->d:Laog;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Laog;)V
    .locals 1

    .prologue
    .line 126
    iput-object p1, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->d:Laog;

    .line 127
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Laog;->aN_()Landroid/view/View;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->setContentView(Landroid/view/View;)V

    .line 128
    return-void

    .line 127
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected c(Lank;)Lans;
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return-object v0
.end method

.method protected g(Lank;)Lann;
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 44
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    invoke-static {p1}, Lank;->a(Landroid/os/Bundle;)Lank;

    move-result-object v1

    .line 46
    const-string/jumbo v0, "retained_object_graph"

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->b_(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lann;

    iput-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->b:Lann;

    .line 47
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->b:Lann;

    if-nez v0, :cond_0

    .line 49
    invoke-virtual {p0, v1}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->g(Lank;)Lann;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->b:Lann;

    .line 50
    const-string/jumbo v0, "retained_object_graph"

    iget-object v2, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->b:Lann;

    invoke-virtual {p0, v0, v2}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    :cond_0
    invoke-virtual {p0, v1}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->c(Lank;)Lans;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->c:Lans;

    .line 55
    new-instance v0, Lani;

    invoke-direct {v0, p0}, Lani;-><init>(Lcom/twitter/app/common/util/j;)V

    iput-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->a:Lani;

    .line 56
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->c:Lans;

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->c:Lans;

    invoke-interface {v0}, Lans;->a()Laog;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->d:Laog;

    .line 60
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->d:Laog;

    if-nez v0, :cond_2

    .line 61
    invoke-virtual {p0}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const-string/jumbo v0, "ViewHost"

    invoke-virtual {v1, v0}, Lank;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    invoke-virtual {p0, v2, v0}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Laog;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->d:Laog;

    .line 64
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->d:Laog;

    if-eqz v0, :cond_3

    .line 65
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->d:Laog;

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->a(Laog;)V

    .line 66
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->d:Laog;

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->b(Laog;)V

    .line 67
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->a:Lani;

    iget-object v1, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->d:Laog;

    invoke-virtual {v0, v1}, Lani;->c(Ljava/lang/Object;)Lani;

    .line 69
    :cond_3
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->c:Lans;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->c:Lans;

    invoke-static {v0}, Lalb$a;->a(Lalb;)V

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->b:Lann;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_1

    .line 87
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->b:Lann;

    invoke-static {v0}, Lakz$a;->a(Lakz;)V

    .line 89
    :cond_1
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->onDestroy()V

    .line 90
    return-void
.end method
