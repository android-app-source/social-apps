.class public Lcom/twitter/app/common/inject/InjectedPreferenceActivity;
.super Lcom/twitter/app/common/base/BasePreferenceActivity;
.source "Twttr"


# instance fields
.field private a:Lani;

.field private b:Lann;

.field private c:Lans;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/twitter/app/common/base/BasePreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected b(Lank;)Lann;
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    return-object v0
.end method

.method protected d(Lank;)Lans;
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return-object v0
.end method

.method public n()Lanh;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;->a:Lani;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lanh;

    return-object v0
.end method

.method public o()Lann;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<RC::",
            "Lann;",
            ">()TRC;"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;->b:Lann;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lann;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    invoke-static {p1}, Lank;->a(Landroid/os/Bundle;)Lank;

    move-result-object v1

    .line 42
    const-string/jumbo v0, "retained_object_graph"

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;->b_(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lann;

    iput-object v0, p0, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;->b:Lann;

    .line 43
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;->b:Lann;

    if-nez v0, :cond_0

    .line 45
    invoke-virtual {p0, v1}, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;->b(Lank;)Lann;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;->b:Lann;

    .line 46
    const-string/jumbo v0, "retained_object_graph"

    iget-object v2, p0, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;->b:Lann;

    invoke-virtual {p0, v0, v2}, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    :cond_0
    invoke-virtual {p0, v1}, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;->d(Lank;)Lans;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;->c:Lans;

    .line 51
    new-instance v0, Lani;

    invoke-direct {v0, p0}, Lani;-><init>(Lcom/twitter/app/common/util/j;)V

    iput-object v0, p0, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;->a:Lani;

    .line 52
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;->c:Lans;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;->c:Lans;

    invoke-static {v0}, Lalb$a;->a(Lalb;)V

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;->b:Lann;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_1

    .line 70
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedPreferenceActivity;->b:Lann;

    invoke-static {v0}, Lakz$a;->a(Lakz;)V

    .line 72
    :cond_1
    invoke-super {p0}, Lcom/twitter/app/common/base/BasePreferenceActivity;->onDestroy()V

    .line 73
    return-void
.end method
