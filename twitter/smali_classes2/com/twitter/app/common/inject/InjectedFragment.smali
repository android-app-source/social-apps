.class public Lcom/twitter/app/common/inject/InjectedFragment;
.super Lcom/twitter/app/common/base/BaseFragment;
.source "Twttr"


# instance fields
.field private a:Lani;

.field private b:Lann;

.field private c:Lanm;

.field private d:Lans;

.field private e:Laog;

.field private f:Lank;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/twitter/app/common/base/BaseFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 165
    if-eqz p2, :cond_1

    .line 166
    invoke-static {p2}, Lank;->a(Landroid/os/Bundle;)Lank;

    move-result-object v0

    .line 171
    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/app/common/inject/InjectedFragment;->h(Lank;)Lans;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->d:Lans;

    .line 172
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->d:Lans;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->d:Lans;

    invoke-interface {v0}, Lans;->a()Laog;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->e:Laog;

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->e:Laog;

    if-eqz v0, :cond_2

    .line 176
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->e:Laog;

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/inject/InjectedFragment;->a(Laog;)V

    .line 177
    invoke-virtual {p0}, Lcom/twitter/app/common/inject/InjectedFragment;->ag()Lanh;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/common/inject/InjectedFragment;->e:Laog;

    invoke-virtual {v0, v1}, Lanh;->a(Ljava/lang/Object;)Lanh;

    .line 178
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->e:Laog;

    invoke-virtual {v0}, Laog;->aN_()Landroid/view/View;

    move-result-object v0

    .line 180
    :goto_1
    return-object v0

    .line 168
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->f:Lank;

    sget-object v1, Lank;->a:Lank;

    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lank;

    goto :goto_0

    .line 180
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 104
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/inject/InjectedFragment;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected a(Laog;)V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 155
    return-void
.end method

.method public final ae()Z
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->e:Laog;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final af()Laog;
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->e:Laog;

    if-nez v0, :cond_0

    .line 94
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "The view host is not available."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->e:Laog;

    return-object v0
.end method

.method public ag()Lanh;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->a:Lani;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lanh;

    return-object v0
.end method

.method public ah()Lann;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<RC::",
            "Lann;",
            ">()TRC;"
        }
    .end annotation

    .prologue
    .line 233
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->b:Lann;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lann;

    return-object v0
.end method

.method public ai()Lanm;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<FC::",
            "Lanm;",
            ">()TFC;"
        }
    .end annotation

    .prologue
    .line 242
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->c:Lanm;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lanm;

    return-object v0
.end method

.method public b(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/twitter/app/common/inject/InjectedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 119
    if-nez v1, :cond_0

    .line 120
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "The Fragment is not attached"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/common/inject/InjectedFragment;->ae()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/app/common/inject/InjectedFragment;->af()Laog;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 123
    invoke-virtual {p0}, Lcom/twitter/app/common/inject/InjectedFragment;->af()Laog;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 131
    :goto_0
    return-object v0

    .line 125
    :cond_1
    instance-of v0, v1, Lcom/twitter/app/common/inject/InjectedFragmentActivity;

    if-eqz v0, :cond_2

    move-object v0, v1

    check-cast v0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;

    invoke-virtual {v0}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->aa()Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 126
    check-cast v0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;

    invoke-virtual {v0}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->Z()Laog;

    move-result-object v0

    .line 127
    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 128
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 131
    :cond_2
    const/4 v0, 0x0

    invoke-static {v1, p1, v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method protected d(Lank;)Lann;
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x0

    return-object v0
.end method

.method protected h(Lank;)Lans;
    .locals 1

    .prologue
    .line 218
    const/4 v0, 0x0

    return-object v0
.end method

.method protected i(Lank;)Lanm;
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 53
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 54
    invoke-static {p1}, Lank;->a(Landroid/os/Bundle;)Lank;

    move-result-object v1

    .line 55
    const-string/jumbo v0, "retained_object_graph"

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/inject/InjectedFragment;->b_(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lann;

    iput-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->b:Lann;

    .line 56
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->b:Lann;

    if-nez v0, :cond_0

    .line 58
    invoke-virtual {p0, v1}, Lcom/twitter/app/common/inject/InjectedFragment;->d(Lank;)Lann;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->b:Lann;

    .line 59
    const-string/jumbo v0, "retained_object_graph"

    iget-object v2, p0, Lcom/twitter/app/common/inject/InjectedFragment;->b:Lann;

    invoke-virtual {p0, v0, v2}, Lcom/twitter/app/common/inject/InjectedFragment;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    :cond_0
    invoke-virtual {p0, v1}, Lcom/twitter/app/common/inject/InjectedFragment;->i(Lank;)Lanm;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->c:Lanm;

    .line 64
    new-instance v0, Lani;

    invoke-direct {v0, p0}, Lani;-><init>(Lcom/twitter/app/common/base/BaseFragment;)V

    iput-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->a:Lani;

    .line 65
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 69
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->c:Lanm;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->c:Lanm;

    invoke-static {v0}, Lakx$a;->a(Lakx;)V

    .line 71
    iput-object v2, p0, Lcom/twitter/app/common/inject/InjectedFragment;->c:Lanm;

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->b:Lann;

    if-eqz v0, :cond_2

    .line 74
    invoke-virtual {p0}, Lcom/twitter/app/common/inject/InjectedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 75
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_1

    .line 76
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->b:Lann;

    invoke-static {v0}, Lakz$a;->a(Lakz;)V

    .line 78
    :cond_1
    iput-object v2, p0, Lcom/twitter/app/common/inject/InjectedFragment;->b:Lann;

    .line 80
    :cond_2
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseFragment;->onDestroy()V

    .line 81
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 186
    invoke-super {p0}, Lcom/twitter/app/common/base/BaseFragment;->onDestroyView()V

    .line 187
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->e:Laog;

    if-eqz v0, :cond_0

    .line 188
    invoke-virtual {p0}, Lcom/twitter/app/common/inject/InjectedFragment;->ag()Lanh;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/common/inject/InjectedFragment;->e:Laog;

    invoke-virtual {v0, v1}, Lanh;->b(Ljava/lang/Object;)Lanh;

    .line 189
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->e:Laog;

    invoke-virtual {v0}, Laog;->am_()V

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->d:Lans;

    if-eqz v0, :cond_1

    .line 192
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->d:Lans;

    invoke-static {v0}, Lalb$a;->a(Lalb;)V

    .line 194
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 198
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 199
    iget-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->e:Laog;

    if-eqz v0, :cond_0

    .line 202
    invoke-static {p1}, Lank;->a(Landroid/os/Bundle;)Lank;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/inject/InjectedFragment;->f:Lank;

    .line 204
    :cond_0
    return-void
.end method
