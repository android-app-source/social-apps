.class Lcom/twitter/app/common/util/b$b;
.super Lcom/twitter/app/common/util/b$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/common/util/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private final a:Lcom/twitter/app/common/util/a;

.field private b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/twitter/app/common/util/b$a;-><init>()V

    .line 93
    new-instance v0, Lcom/twitter/app/common/util/a;

    invoke-direct {v0}, Lcom/twitter/app/common/util/a;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/common/util/b$b;->a:Lcom/twitter/app/common/util/a;

    .line 94
    return-void
.end method


# virtual methods
.method public declared-synchronized a()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 145
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/app/common/util/b$b;->b:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/common/util/b$b;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/twitter/app/common/util/b$b;->a:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/app/common/util/a;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 104
    return-void
.end method

.method public a(Landroid/app/Activity;Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/twitter/app/common/util/b$b;->a:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/app/common/util/a;->a(Landroid/app/Activity;Landroid/content/res/Configuration;)V

    .line 109
    return-void
.end method

.method public a(Lcom/twitter/app/common/util/b$a;)V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/twitter/app/common/util/b$b;->a:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/util/a;->a(Lcom/twitter/app/common/util/b$a;)V

    .line 150
    return-void
.end method

.method public b(Lcom/twitter/app/common/util/b$a;)V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/twitter/app/common/util/b$b;->a:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/util/a;->b(Lcom/twitter/app/common/util/b$a;)Z

    .line 154
    return-void
.end method

.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/twitter/app/common/util/b$b;->a:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/app/common/util/a;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 99
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/twitter/app/common/util/b$b;->a:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/util/a;->e(Landroid/app/Activity;)V

    .line 140
    check-cast p1, Lcom/twitter/app/common/util/j;

    invoke-interface {p1, p0}, Lcom/twitter/app/common/util/j;->b(Lcom/twitter/app/common/util/b$a;)V

    .line 141
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/twitter/app/common/util/b$b;->a:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/util/a;->c(Landroid/app/Activity;)V

    .line 125
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 118
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/app/common/util/b$b;->b:Ljava/lang/ref/WeakReference;

    .line 119
    iget-object v0, p0, Lcom/twitter/app/common/util/b$b;->a:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/util/a;->b(Landroid/app/Activity;)V

    .line 120
    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/twitter/app/common/util/b$b;->a:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/app/common/util/a;->b(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 135
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/twitter/app/common/util/b$b;->a:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/util/a;->a(Landroid/app/Activity;)V

    .line 114
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/twitter/app/common/util/b$b;->a:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/util/a;->d(Landroid/app/Activity;)V

    .line 130
    return-void
.end method
