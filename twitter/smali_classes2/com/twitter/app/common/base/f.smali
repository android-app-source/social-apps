.class public Lcom/twitter/app/common/base/f;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/util/o;


# static fields
.field public static final a:Lcom/twitter/app/common/base/f;


# instance fields
.field private final b:Lcom/twitter/app/common/base/g;

.field private final c:Lcom/twitter/util/object/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/g",
            "<",
            "Landroid/app/Activity;",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            "Landroid/support/design/widget/Snackbar;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcom/twitter/app/common/base/f;

    sget-object v1, Lcom/twitter/app/common/base/g;->b:Lcom/twitter/app/common/base/g;

    invoke-direct {v0, v1}, Lcom/twitter/app/common/base/f;-><init>(Lcom/twitter/app/common/base/g;)V

    sput-object v0, Lcom/twitter/app/common/base/f;->a:Lcom/twitter/app/common/base/f;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/app/common/base/g;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/twitter/app/common/base/f;->b:Lcom/twitter/app/common/base/g;

    .line 30
    new-instance v0, Lcom/twitter/app/common/base/f$1;

    invoke-direct {v0, p0}, Lcom/twitter/app/common/base/f$1;-><init>(Lcom/twitter/app/common/base/f;)V

    iput-object v0, p0, Lcom/twitter/app/common/base/f;->c:Lcom/twitter/util/object/g;

    .line 37
    return-void
.end method

.method private static a(Landroid/app/Activity;)Landroid/view/View;
    .locals 2

    .prologue
    .line 81
    const v0, 0x7f1302e3

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 82
    if-nez v0, :cond_0

    .line 83
    const v0, 0x7f1302e4

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 85
    :cond_0
    if-nez v0, :cond_1

    instance-of v1, p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;

    if-eqz v1, :cond_1

    move-object v0, p0

    .line 86
    check-cast v0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;

    invoke-virtual {v0}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->aa()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 87
    check-cast p0, Lcom/twitter/app/common/inject/InjectedFragmentActivity;

    invoke-virtual {p0}, Lcom/twitter/app/common/inject/InjectedFragmentActivity;->Z()Laog;

    move-result-object v0

    invoke-virtual {v0}, Laog;->aN_()Landroid/view/View;

    move-result-object v0

    .line 91
    :cond_1
    :goto_0
    return-object v0

    .line 89
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/app/Activity;Lcom/twitter/app/common/base/SnackbarData;)V
    .locals 3

    .prologue
    .line 56
    invoke-static {p1}, Lcom/twitter/app/common/base/f;->a(Landroid/app/Activity;)Landroid/view/View;

    move-result-object v0

    .line 57
    if-nez v0, :cond_0

    .line 58
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Cannot find the anchor view to show Snackbar."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 72
    :goto_0
    return-void

    .line 62
    :cond_0
    iget-object v1, p0, Lcom/twitter/app/common/base/f;->c:Lcom/twitter/util/object/g;

    iget-object v2, p2, Lcom/twitter/app/common/base/SnackbarData;->a:Ljava/lang/String;

    invoke-interface {v1, p1, v0, v2}, Lcom/twitter/util/object/g;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/Snackbar;

    .line 63
    iget-object v1, p2, Lcom/twitter/app/common/base/SnackbarData;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p2, Lcom/twitter/app/common/base/SnackbarData;->c:Landroid/content/Intent;

    if-eqz v1, :cond_1

    .line 64
    iget-object v1, p2, Lcom/twitter/app/common/base/SnackbarData;->b:Ljava/lang/String;

    new-instance v2, Lcom/twitter/app/common/base/f$2;

    invoke-direct {v2, p0, p1, p2}, Lcom/twitter/app/common/base/f$2;-><init>(Lcom/twitter/app/common/base/f;Landroid/app/Activity;Lcom/twitter/app/common/base/SnackbarData;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/design/widget/Snackbar;->setAction(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    .line 71
    :cond_1
    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->show()V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/app/Activity;ILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/app/common/base/f;->b:Lcom/twitter/app/common/base/g;

    invoke-virtual {v0, p3}, Lcom/twitter/app/common/base/g;->a(Landroid/content/Intent;)Lcom/twitter/app/common/base/SnackbarData;

    move-result-object v0

    .line 49
    if-eqz v0, :cond_0

    .line 51
    invoke-direct {p0, p1, v0}, Lcom/twitter/app/common/base/f;->a(Landroid/app/Activity;Lcom/twitter/app/common/base/SnackbarData;)V

    .line 53
    :cond_0
    return-void
.end method
