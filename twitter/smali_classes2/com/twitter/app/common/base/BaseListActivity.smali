.class public Lcom/twitter/app/common/base/BaseListActivity;
.super Landroid/app/ListActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/base/e;
.implements Lcom/twitter/app/common/util/j;
.implements Lcom/twitter/util/android/e;


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcwv;

.field private final c:Lcom/twitter/app/common/util/a;

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/twitter/app/common/base/BaseListActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/app/common/base/BaseListActivity;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 31
    new-instance v0, Lcwv;

    invoke-direct {v0}, Lcwv;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->b:Lcwv;

    .line 32
    new-instance v0, Lcom/twitter/app/common/util/a;

    invoke-direct {v0}, Lcom/twitter/app/common/util/a;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->c:Lcom/twitter/app/common/util/a;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 197
    sget-boolean v0, Lcom/twitter/app/common/base/BaseListActivity;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->g:Ljava/util/Map;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 198
    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->g:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public a(ILcom/twitter/app/common/util/o;)V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->c:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/app/common/util/a;->a(ILcom/twitter/app/common/util/o;)V

    .line 159
    return-void
.end method

.method public a(Lcom/twitter/app/common/util/b$a;)V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->c:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/util/a;->a(Lcom/twitter/app/common/util/b$a;)V

    .line 149
    return-void
.end method

.method public b(Lcom/twitter/app/common/util/b$a;)V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->c:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/util/a;->b(Lcom/twitter/app/common/util/b$a;)Z

    .line 154
    return-void
.end method

.method public b_(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 208
    sget-boolean v0, Lcom/twitter/app/common/base/BaseListActivity;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->g:Ljava/util/Map;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected c()V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 218
    return-void
.end method

.method public f_()Z
    .locals 1

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->d:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/common/base/BaseListActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g_()Z
    .locals 1

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->e:Z

    return v0
.end method

.method public isDestroyed()Z
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->f:Z

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->c:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/twitter/app/common/util/a;->a(Landroid/app/Activity;IILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    invoke-super {p0, p1, p2, p3}, Landroid/app/ListActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 129
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 111
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 112
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->c:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p0, p1}, Lcom/twitter/app/common/util/a;->a(Landroid/app/Activity;Landroid/content/res/Configuration;)V

    .line 113
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/twitter/app/common/base/BaseListActivity;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->g:Ljava/util/Map;

    .line 44
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->g:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->g:Ljava/util/Map;

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->c:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p0, p1}, Lcom/twitter/app/common/util/a;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 48
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 49
    return-void
.end method

.method protected onDestroy()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->b:Lcwv;

    invoke-virtual {v0}, Lcwv;->B_()V

    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->f:Z

    .line 120
    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    .line 121
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->c:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p0}, Lcom/twitter/app/common/util/a;->e(Landroid/app/Activity;)V

    .line 122
    return-void
.end method

.method public onMultiWindowModeChanged(Z)V
    .locals 1

    .prologue
    .line 96
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onMultiWindowModeChanged(Z)V

    .line 97
    invoke-static {}, Lalc;->d()Lalc;

    move-result-object v0

    invoke-virtual {v0}, Lalc;->n()Lcom/twitter/app/common/util/m;

    move-result-object v0

    .line 98
    invoke-virtual {v0, p1}, Lcom/twitter/app/common/util/m;->a(Z)V

    .line 99
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->c:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p0, p1}, Lcom/twitter/app/common/util/a;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 55
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 56
    return-void
.end method

.method protected onPause()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 81
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->e:Z

    .line 82
    invoke-super {p0}, Landroid/app/ListActivity;->onPause()V

    .line 83
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->c:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p0}, Lcom/twitter/app/common/util/a;->c(Landroid/app/Activity;)V

    .line 84
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 224
    return-void
.end method

.method protected onResume()V
    .locals 2
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 69
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    .line 70
    invoke-static {}, Lalc;->d()Lalc;

    move-result-object v0

    invoke-virtual {v0}, Lalc;->n()Lcom/twitter/app/common/util/m;

    move-result-object v0

    .line 71
    invoke-virtual {p0}, Lcom/twitter/app/common/base/BaseListActivity;->isInMultiWindowMode()Z

    move-result v1

    .line 70
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/util/m;->a(Z)V

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->c:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p0}, Lcom/twitter/app/common/util/a;->b(Landroid/app/Activity;)V

    .line 74
    invoke-super {p0}, Landroid/app/ListActivity;->onResume()V

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->e:Z

    .line 76
    return-void
.end method

.method public final onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/twitter/app/common/base/BaseListActivity;->c()V

    .line 186
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->g:Ljava/util/Map;

    return-object v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 105
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->c:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p0, p1}, Lcom/twitter/app/common/util/a;->b(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 106
    return-void
.end method

.method protected onStart()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->c:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p0}, Lcom/twitter/app/common/util/a;->a(Landroid/app/Activity;)V

    .line 62
    invoke-super {p0}, Landroid/app/ListActivity;->onStart()V

    .line 63
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->d:Z

    .line 64
    return-void
.end method

.method protected onStop()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->d:Z

    .line 90
    invoke-super {p0}, Landroid/app/ListActivity;->onStop()V

    .line 91
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseListActivity;->c:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p0}, Lcom/twitter/app/common/util/a;->d(Landroid/app/Activity;)V

    .line 92
    return-void
.end method
