.class public Lcom/twitter/app/common/base/TwitterFragmentActivity;
.super Lcom/twitter/app/common/abs/AbsFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/ae$a;
.implements Lcom/twitter/android/search/SearchSuggestionController$g;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    }
.end annotation


# instance fields
.field protected I:Lcom/twitter/android/composer/ComposerDockLayout;

.field protected J:Z

.field private a:Lcom/twitter/app/common/base/d;

.field private b:Lbqn;

.field private c:Lcom/twitter/android/search/SearchSuggestionController;

.field private d:Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

.field private e:Ljava/lang/CharSequence;

.field private f:Lcom/twitter/android/av/audio/b;

.field private g:Z

.field private h:Lcom/twitter/ui/view/e;

.field private i:Lcom/twitter/android/an;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;-><init>()V

    .line 175
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->e:Ljava/lang/CharSequence;

    return-void
.end method

.method private a(Landroid/content/Intent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 532
    invoke-static {p1}, Lcom/twitter/util/v;->a(Landroid/content/Intent;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 533
    const/4 v0, 0x0

    check-cast v0, Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->replaceExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move v0, v1

    .line 547
    :cond_0
    :goto_0
    return v0

    .line 536
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 537
    if-eqz v2, :cond_0

    const-string/jumbo v3, "sb_notification_handled"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 538
    invoke-static {v2}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/os/Bundle;)Lcom/twitter/android/client/notifications/StatusBarNotif;

    move-result-object v1

    .line 539
    if-eqz v1, :cond_2

    .line 540
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/twitter/android/client/NotificationService;->a(Landroid/content/Context;Landroid/os/Bundle;)V

    .line 542
    :cond_2
    invoke-static {p1}, Lcom/twitter/app/common/base/h;->b(Landroid/content/Intent;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->g:Z

    .line 545
    const-string/jumbo v1, "sb_notification_handled"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 739
    const-string/jumbo v0, "search"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 740
    new-instance v1, Lcom/twitter/app/common/base/TwitterFragmentActivity$3;

    invoke-direct {v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$3;-><init>()V

    invoke-virtual {v0, v1}, Landroid/app/SearchManager;->setOnCancelListener(Landroid/app/SearchManager$OnCancelListener;)V

    .line 747
    new-instance v1, Lcom/twitter/app/common/base/TwitterFragmentActivity$4;

    invoke-direct {v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$4;-><init>()V

    invoke-virtual {v0, v1}, Landroid/app/SearchManager;->setOnDismissListener(Landroid/app/SearchManager$OnDismissListener;)V

    .line 753
    return-void
.end method

.method private i()V
    .locals 5

    .prologue
    .line 650
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v1

    .line 651
    if-eqz v1, :cond_1

    .line 652
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    .line 654
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->e:Ljava/lang/CharSequence;

    invoke-static {v2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 655
    const v2, 0x7f0a00e6

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    iget-object v4, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->e:Ljava/lang/CharSequence;

    aput-object v4, v3, v0

    invoke-virtual {p0, v2, v3}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 665
    :cond_0
    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setTitleDescription(Ljava/lang/CharSequence;)V

    .line 667
    :cond_1
    return-void

    .line 657
    :cond_2
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 659
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->e:Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 660
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->e:Ljava/lang/CharSequence;

    goto :goto_0

    .line 662
    :cond_3
    const-string/jumbo v0, ""

    goto :goto_0
.end method


# virtual methods
.method public A()Z
    .locals 1

    .prologue
    .line 354
    const/4 v0, 0x1

    return v0
.end method

.method protected L()Lcom/twitter/android/composer/n;
    .locals 1

    .prologue
    .line 374
    sget-object v0, Lcom/twitter/android/composer/n;->a:Lcom/twitter/android/composer/n;

    return-object v0
.end method

.method protected final M()Z
    .locals 1

    .prologue
    .line 616
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->c:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-virtual {v0}, Lcom/twitter/android/search/SearchSuggestionController;->e()Z

    move-result v0

    return v0
.end method

.method protected final N()Z
    .locals 1

    .prologue
    .line 620
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->c:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-virtual {v0}, Lcom/twitter/android/search/SearchSuggestionController;->f()Z

    move-result v0

    return v0
.end method

.method protected O()Lcom/twitter/metrics/j;
    .locals 1

    .prologue
    .line 670
    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v0

    return-object v0
.end method

.method public P()Lcom/twitter/android/search/SearchSuggestionController;
    .locals 1

    .prologue
    .line 675
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->c:Lcom/twitter/android/search/SearchSuggestionController;

    return-object v0
.end method

.method protected Q()Lcom/twitter/library/media/manager/g;
    .locals 1

    .prologue
    .line 679
    invoke-static {p0}, Lcom/twitter/library/media/manager/g;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/g;

    move-result-object v0

    return-object v0
.end method

.method protected R()Lbqn;
    .locals 1

    .prologue
    .line 688
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b:Lbqn;

    return-object v0
.end method

.method public S()Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    .line 701
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d:Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    if-nez v0, :cond_0

    .line 702
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "ToolBarOptions have not been configured"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 704
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d:Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    return-object v0
.end method

.method public T()Lcom/twitter/android/av/audio/b;
    .locals 1

    .prologue
    .line 715
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->f:Lcom/twitter/android/av/audio/b;

    return-object v0
.end method

.method protected U()Z
    .locals 1

    .prologue
    .line 719
    iget-boolean v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->g:Z

    return v0
.end method

.method protected final V()Lcom/twitter/app/common/base/d;
    .locals 1

    .prologue
    .line 724
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a:Lcom/twitter/app/common/base/d;

    return-object v0
.end method

.method protected X_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 382
    const v0, 0x7f0a01f1

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 0

    .prologue
    .line 395
    return-object p2
.end method

.method protected a(Lank;)Lcom/twitter/app/common/base/j;
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 585
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->isTaskRoot()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->g:Z

    if-eqz v0, :cond_1

    .line 586
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onBackPressed()V

    .line 599
    :goto_0
    return-void

    .line 588
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->K()Landroid/content/Intent;

    move-result-object v0

    .line 589
    if-eqz v0, :cond_2

    .line 590
    invoke-virtual {p0, p0, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_0

    .line 592
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getParent()Landroid/app/Activity;

    move-result-object v0

    .line 593
    if-nez v0, :cond_3

    .line 596
    :goto_1
    invoke-static {p0, p1}, Lcom/twitter/app/main/MainActivity;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    goto :goto_0

    :cond_3
    move-object p0, v0

    goto :goto_1
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 213
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 214
    invoke-direct {p0, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 215
    invoke-static {p0}, Lcom/twitter/android/DispatchActivity;->a(Landroid/app/Activity;)V

    .line 299
    :goto_0
    return-void

    .line 219
    :cond_0
    new-instance v0, Lcom/twitter/android/av/audio/b;

    invoke-direct {v0, p0}, Lcom/twitter/android/av/audio/b;-><init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;)V

    iput-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->f:Lcom/twitter/android/av/audio/b;

    .line 221
    const v0, 0x7f1302e3

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 222
    instance-of v1, v0, Lcom/twitter/android/composer/ComposerDockLayout;

    if-eqz v1, :cond_5

    .line 223
    new-instance v4, Lcom/twitter/app/common/base/TwitterFragmentActivity$1;

    invoke-direct {v4, p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$1;-><init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;)V

    .line 259
    check-cast v0, Lcom/twitter/android/composer/ComposerDockLayout;

    .line 260
    const v1, 0x7f13037d

    invoke-virtual {p0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/ui/widget/DefaultFab;

    .line 261
    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/ComposerDockLayout;->setFab(Lcom/twitter/ui/widget/DefaultFab;)V

    .line 262
    if-eqz v1, :cond_2

    .line 263
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x16

    if-lt v5, v6, :cond_1

    .line 264
    const v5, 0x7f13037a

    invoke-virtual {v1, v5}, Lcom/twitter/ui/widget/DefaultFab;->setAccessibilityTraversalBefore(I)V

    .line 267
    :cond_1
    invoke-static {v1}, Lcom/twitter/ui/view/e;->a(Lcom/twitter/ui/widget/DefaultFab;)Lcom/twitter/ui/view/e;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->h:Lcom/twitter/ui/view/e;

    .line 268
    iget-object v1, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->h:Lcom/twitter/ui/view/e;

    new-instance v5, Lcom/twitter/ui/view/b;

    invoke-direct {v5, p0, v4}, Lcom/twitter/ui/view/b;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/android/composer/ComposerDockLayout$a;)V

    .line 269
    invoke-virtual {v1, v5}, Lcom/twitter/ui/view/e;->a(Lcom/twitter/ui/view/d;)V

    .line 271
    :cond_2
    iget-object v1, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d:Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    iget v1, v1, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->h:I

    const/4 v5, 0x2

    if-eq v1, v5, :cond_3

    iget-object v1, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d:Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    iget v1, v1, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->h:I

    if-eq v1, v7, :cond_3

    iget-object v1, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d:Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    iget v1, v1, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->h:I

    if-ne v1, v8, :cond_7

    :cond_3
    move v1, v3

    :goto_1
    iput-boolean v1, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->J:Z

    .line 274
    iget-boolean v1, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->J:Z

    if-eqz v1, :cond_9

    .line 275
    iget-object v1, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d:Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    iget v1, v1, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->h:I

    if-ne v1, v7, :cond_4

    .line 276
    invoke-virtual {v0, v3}, Lcom/twitter/android/composer/ComposerDockLayout;->setBottomLocked(Z)V

    .line 278
    :cond_4
    iget-object v1, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d:Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    iget v1, v1, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->h:I

    if-ne v1, v8, :cond_8

    .line 279
    invoke-virtual {v0, v3}, Lcom/twitter/android/composer/ComposerDockLayout;->setLoggedOutMode(Z)V

    .line 280
    invoke-virtual {v0, p0}, Lcom/twitter/android/composer/ComposerDockLayout;->a(Landroid/app/Activity;)V

    .line 287
    :goto_2
    iput-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->I:Lcom/twitter/android/composer/ComposerDockLayout;

    .line 290
    :cond_5
    invoke-static {p0}, Lbaa;->a(Landroid/content/Context;)Lbaa;

    move-result-object v0

    invoke-virtual {v0}, Lbaa;->a()V

    .line 292
    const v0, 0x7f13014f

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 293
    if-eqz v0, :cond_6

    .line 294
    iget-object v1, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->c:Lcom/twitter/android/search/SearchSuggestionController;

    .line 295
    invoke-static {v0}, Lcom/twitter/android/client/t;->a(Landroid/view/View;)Lcom/twitter/android/client/t;

    move-result-object v0

    .line 294
    invoke-virtual {v1, v0}, Lcom/twitter/android/search/SearchSuggestionController;->a(Lcom/twitter/android/client/t;)V

    .line 298
    :cond_6
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d:Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    invoke-virtual {p0, p1, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    goto/16 :goto_0

    :cond_7
    move v1, v2

    .line 271
    goto :goto_1

    .line 282
    :cond_8
    invoke-virtual {v0, v4}, Lcom/twitter/android/composer/ComposerDockLayout;->setComposerDockListener(Lcom/twitter/android/composer/ComposerDockLayout$a;)V

    goto :goto_2

    .line 285
    :cond_9
    invoke-virtual {v0, v2}, Lcom/twitter/android/composer/ComposerDockLayout;->setBottomVisible(Z)V

    goto :goto_2
.end method

.method public a(Ljava/lang/CharSequence;Z)V
    .locals 1

    .prologue
    .line 644
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcmt;->b(Ljava/lang/CharSequence;Z)Z

    .line 645
    iput-object p1, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->e:Ljava/lang/CharSequence;

    .line 646
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->i()V

    .line 647
    return-void
.end method

.method public a(Lcmm;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 440
    invoke-interface {p1}, Lcmm;->a()I

    move-result v1

    .line 441
    sparse-switch v1, :sswitch_data_0

    .line 493
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->a(Lcmm;)Z

    move-result v0

    :goto_0
    return v0

    .line 443
    :sswitch_0
    invoke-static {p0}, Lcom/twitter/android/settings/SettingsActivity;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 447
    :sswitch_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/WebViewActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v2, 0x7f0a0b8f

    .line 448
    invoke-virtual {p0, v2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 447
    invoke-virtual {p0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 452
    :sswitch_2
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 455
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->finish()V

    goto :goto_0

    .line 458
    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->a(Lcmm;)Z

    move-result v0

    goto :goto_0

    .line 462
    :sswitch_3
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onSearchRequested()Z

    move-result v0

    goto :goto_0

    .line 465
    :sswitch_4
    invoke-static {p0}, Lcom/twitter/android/dogfood/a;->a(Landroid/content/Context;)Lcom/twitter/android/dogfood/a;

    move-result-object v1

    .line 466
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0b93

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 467
    invoke-virtual {v1}, Lcom/twitter/android/dogfood/a;->b()Ljava/lang/String;

    move-result-object v3

    .line 468
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Thanks for filing a bug!\n\nSummary: \n\nSteps to reproduce: \n\nExpected results: \n\nActual results: \n\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 473
    invoke-virtual {v1}, Lcom/twitter/android/dogfood/a;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 475
    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/twitter/android/dogfood/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lrx/g;

    move-result-object v1

    new-instance v2, Lcom/twitter/app/common/base/TwitterFragmentActivity$2;

    invoke-direct {v2, p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$2;-><init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;)V

    .line 476
    invoke-virtual {v1, v2}, Lrx/g;->a(Lrx/i;)Lrx/j;

    goto :goto_0

    .line 485
    :sswitch_5
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/twitter/library/network/narc/i;->a(Landroid/content/Context;Z)Ljava/lang/String;

    goto :goto_0

    .line 489
    :sswitch_6
    invoke-virtual {p0, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->j(I)V

    goto :goto_0

    .line 441
    :sswitch_data_0
    .sparse-switch
        0x7f130043 -> :sswitch_2
        0x7f1302d3 -> :sswitch_0
        0x7f1302d5 -> :sswitch_4
        0x7f13088c -> :sswitch_6
        0x7f13088d -> :sswitch_3
        0x7f1308b8 -> :sswitch_5
        0x7f1308cf -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Lcmr;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 404
    invoke-interface {p1}, Lcmr;->k()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 405
    iget-object v1, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d:Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    iget-boolean v1, v1, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->f:Z

    if-eqz v1, :cond_1

    .line 406
    const v1, 0x7f14000a

    invoke-interface {p1, v1}, Lcmr;->a(I)V

    .line 407
    iget-object v1, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->c:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-virtual {v1, v0}, Lcom/twitter/android/search/SearchSuggestionController;->a(Lcom/twitter/internal/android/widget/ToolBar;)V

    .line 408
    const v1, 0x7f13088c

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v1

    .line 409
    if-eqz v1, :cond_1

    .line 410
    invoke-virtual {v1}, Lazv;->d()Landroid/view/View;

    move-result-object v2

    .line 411
    if-eqz v2, :cond_0

    .line 412
    const v3, 0x7f0a00d8

    invoke-virtual {p0, v3}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 414
    :cond_0
    iget-object v2, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d:Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    iget v2, v2, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->h:I

    if-eq v2, v5, :cond_1

    .line 415
    invoke-virtual {v1, v4}, Lazv;->b(Z)Lazv;

    .line 419
    :cond_1
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v1

    .line 420
    invoke-virtual {v1}, Lcof;->d()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 421
    const v1, 0x7f140004

    invoke-interface {p1, v1}, Lcmr;->a(I)V

    .line 431
    :cond_2
    :goto_0
    invoke-static {p0}, Lcom/twitter/android/al;->a(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 432
    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayShowHomeAsUpEnabled(Z)V

    .line 435
    :cond_3
    return v5

    .line 422
    :cond_4
    invoke-virtual {v1}, Lcof;->p()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 423
    const v1, 0x7f140020

    invoke-interface {p1, v1}, Lcmr;->a(I)V

    goto :goto_0
.end method

.method protected synthetic b(Lank;)Lcom/twitter/app/common/abs/b;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lank;)Lcom/twitter/app/common/base/j;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 0

    .prologue
    .line 400
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 638
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcmt;->b(Ljava/lang/CharSequence;)Z

    .line 639
    iput-object p1, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->e:Ljava/lang/CharSequence;

    .line 640
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->i()V

    .line 641
    return-void
.end method

.method protected synthetic c(Lank;)Lans;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lank;)Lcom/twitter/app/common/base/j;

    move-result-object v0

    return-object v0
.end method

.method protected e(Lank;)Lcom/twitter/app/common/base/i;
    .locals 2

    .prologue
    .line 199
    invoke-static {}, Lcom/twitter/app/common/base/c;->a()Lcom/twitter/app/common/base/c$a;

    move-result-object v0

    .line 200
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/c$a;->a(Lamu;)Lcom/twitter/app/common/base/c$a;

    move-result-object v0

    .line 201
    invoke-virtual {v0}, Lcom/twitter/app/common/base/c$a;->a()Lcom/twitter/app/common/base/i;

    move-result-object v0

    .line 199
    return-object v0
.end method

.method public e(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 305
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->h:Lcom/twitter/ui/view/e;

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->h:Lcom/twitter/ui/view/e;

    iget-object v1, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->h:Lcom/twitter/ui/view/e;

    invoke-virtual {v1, p1}, Lcom/twitter/ui/view/e;->a(Landroid/net/Uri;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/view/e;->a(I)V

    .line 308
    :cond_0
    return-void
.end method

.method public e_()V
    .locals 1

    .prologue
    .line 329
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->h(Z)V

    .line 330
    return-void
.end method

.method protected synthetic f(Lank;)Lcom/twitter/app/common/abs/a;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->e(Lank;)Lcom/twitter/app/common/base/i;

    move-result-object v0

    return-object v0
.end method

.method protected f()[I
    .locals 1

    .prologue
    .line 386
    const/4 v0, 0x0

    return-object v0
.end method

.method protected synthetic g(Lank;)Lann;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->e(Lank;)Lcom/twitter/app/common/base/i;

    move-result-object v0

    return-object v0
.end method

.method public final g(Landroid/os/Bundle;)Lcom/twitter/app/common/abs/AbsFragmentActivity$a;
    .locals 2

    .prologue
    .line 334
    invoke-static {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b(Landroid/content/Context;)V

    .line 335
    invoke-static {p0}, Lbqn;->a(Landroid/content/Context;)Lbqn;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b:Lbqn;

    .line 336
    new-instance v0, Lcom/twitter/android/an;

    invoke-direct {v0, p0}, Lcom/twitter/android/an;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->i:Lcom/twitter/android/an;

    .line 337
    invoke-static {p0, p1}, Lcom/twitter/android/search/SearchSuggestionController;->a(Lcom/twitter/app/common/base/BaseFragmentActivity;Landroid/os/Bundle;)Lcom/twitter/android/search/SearchSuggestionController;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->c:Lcom/twitter/android/search/SearchSuggestionController;

    .line 339
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->c:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-virtual {v0, p0}, Lcom/twitter/android/search/SearchSuggestionController;->a(Lcom/twitter/android/search/SearchSuggestionController$g;)V

    .line 340
    new-instance v0, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    invoke-direct {v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;-><init>()V

    .line 341
    invoke-static {p0}, Lcom/twitter/util/ui/a;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 342
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(I)V

    .line 347
    :goto_0
    const v1, 0x7f040100

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 348
    invoke-virtual {p0, p1, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d:Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    .line 349
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d:Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    return-object v0

    .line 344
    :cond_0
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(I)V

    goto :goto_0
.end method

.method public h(Z)V
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->I:Lcom/twitter/android/composer/ComposerDockLayout;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->J:Z

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->I:Lcom/twitter/android/composer/ComposerDockLayout;

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/ComposerDockLayout;->setBottomVisible(Z)V

    .line 314
    :cond_0
    return-void
.end method

.method j(I)V
    .locals 3

    .prologue
    .line 358
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 359
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/twitter/app/drafts/DraftsActivity;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 367
    :goto_0
    return-void

    .line 361
    :cond_0
    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 362
    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/a;->a(I)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 363
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->f()[I

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/composer/a;->a(Ljava/lang/String;[I)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 364
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->L()Lcom/twitter/android/composer/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->a(Lcom/twitter/android/composer/n;)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 365
    invoke-virtual {v0, p0}, Lcom/twitter/android/composer/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 361
    invoke-virtual {p0, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 378
    const/4 v0, 0x0

    return-object v0
.end method

.method public k(I)V
    .locals 1

    .prologue
    .line 634
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b(Ljava/lang/CharSequence;)V

    .line 635
    return-void
.end method

.method public k_()V
    .locals 1

    .prologue
    .line 321
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->h(Z)V

    .line 322
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 729
    invoke-static {p1}, Lcom/twitter/app/common/base/d;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 730
    invoke-static {p3}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    const-string/jumbo v1, "extra_perm_result"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 731
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a:Lcom/twitter/app/common/base/d;

    invoke-virtual {v0, p1, p3}, Lcom/twitter/app/common/base/d;->b(ILandroid/content/Intent;)V

    .line 733
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 734
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 513
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->c:Lcom/twitter/android/search/SearchSuggestionController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->c:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-virtual {v0}, Lcom/twitter/android/search/SearchSuggestionController;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 518
    :goto_0
    return-void

    .line 516
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 190
    new-instance v0, Lcom/twitter/app/common/base/d;

    invoke-direct {v0, p0}, Lcom/twitter/app/common/base/d;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a:Lcom/twitter/app/common/base/d;

    .line 191
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 193
    sget-object v0, Lcom/twitter/app/common/base/f;->a:Lcom/twitter/app/common/base/f;

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcom/twitter/app/common/util/o;)V

    .line 194
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 500
    .line 501
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->F()Lcmt;

    move-result-object v1

    invoke-virtual {v1}, Lcmt;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v1, 0x52

    if-ne p1, v1, :cond_2

    iget-object v1, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->c:Lcom/twitter/android/search/SearchSuggestionController;

    .line 502
    invoke-virtual {v1}, Lcom/twitter/android/search/SearchSuggestionController;->d()Z

    move-result v1

    if-nez v1, :cond_2

    .line 503
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->F()Lcmt;

    move-result-object v1

    invoke-virtual {v1}, Lcmt;->f()Z

    move-result v1

    .line 505
    :goto_0
    if-nez v1, :cond_0

    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 522
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 523
    invoke-direct {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/content/Intent;)Z

    .line 524
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 559
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->M()Z

    .line 560
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->i:Lcom/twitter/android/an;

    invoke-static {v0}, Lcom/twitter/android/client/AppBroadcastReceiver;->b(Lcom/twitter/library/client/e;)V

    .line 561
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b:Lbqn;

    invoke-virtual {v0}, Lbqn;->d()V

    .line 563
    invoke-static {}, Lcom/twitter/library/metrics/a;->a()Lcom/twitter/library/metrics/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/metrics/a;->b()V

    .line 564
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->onPause()V

    .line 565
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 552
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->onResume()V

    .line 553
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->i:Lcom/twitter/android/an;

    invoke-static {v0}, Lcom/twitter/android/client/AppBroadcastReceiver;->a(Lcom/twitter/library/client/e;)V

    .line 554
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b:Lbqn;

    invoke-virtual {v0}, Lbqn;->e()V

    .line 555
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 569
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 570
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;->c:Lcom/twitter/android/search/SearchSuggestionController;

    invoke-virtual {v0, p1}, Lcom/twitter/android/search/SearchSuggestionController;->a(Landroid/os/Bundle;)V

    .line 571
    return-void
.end method

.method public onSearchRequested()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 603
    new-instance v0, Lcom/twitter/android/timeline/cb$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/timeline/cb$a;-><init>(Landroid/os/Bundle;)V

    const-string/jumbo v1, "TRENDSPLUS"

    .line 604
    invoke-virtual {v0, v1}, Lcom/twitter/android/timeline/cb$a;->c(Ljava/lang/String;)Lcom/twitter/app/common/timeline/c$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cb$a;

    const-string/jumbo v1, "search"

    .line 605
    invoke-virtual {v0, v1}, Lcom/twitter/android/timeline/cb$a;->d(Ljava/lang/String;)Lcom/twitter/app/common/timeline/c$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cb$a;

    const-string/jumbo v1, "trendsplus"

    .line 606
    invoke-virtual {v0, v1}, Lcom/twitter/android/timeline/cb$a;->e(Ljava/lang/String;)Lcom/twitter/app/common/timeline/c$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/cb$a;

    .line 607
    invoke-virtual {v0}, Lcom/twitter/android/timeline/cb$a;->a()Lcom/twitter/android/timeline/cb;

    move-result-object v0

    .line 608
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/twitter/android/search/d;->f()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 609
    invoke-virtual {v0}, Lcom/twitter/android/timeline/cb;->n()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 608
    invoke-virtual {p0, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 610
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "trendsplus::::search_icon_launch"

    aput-object v3, v1, v2

    .line 611
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 610
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 612
    return v4
.end method

.method protected p()V
    .locals 1

    .prologue
    .line 575
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/net/Uri;)V

    .line 576
    return-void
.end method

.method protected p_()V
    .locals 1

    .prologue
    .line 693
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/android/DispatchActivity;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 694
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 625
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragmentActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 626
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->i()V

    .line 627
    return-void
.end method
