.class Lcom/twitter/app/common/base/TwitterFragmentActivity$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/composer/ComposerDockLayout$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/common/base/TwitterFragmentActivity;


# direct methods
.method constructor <init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;)V
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity$1;->a:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 5

    .prologue
    .line 232
    packed-switch p1, :pswitch_data_0

    .line 250
    const-string/jumbo v0, ""

    .line 254
    :goto_0
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, ""

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, ""

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "compose_bar"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object v0, v2, v3

    const/4 v0, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v2, v0

    .line 255
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 254
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 256
    iget-object v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity$1;->a:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->j(I)V

    .line 257
    return-void

    .line 234
    :pswitch_0
    const-string/jumbo v0, "composebox"

    goto :goto_0

    .line 238
    :pswitch_1
    const-string/jumbo v0, "camera"

    goto :goto_0

    .line 242
    :pswitch_2
    const-string/jumbo v0, "photo_picker"

    goto :goto_0

    .line 246
    :pswitch_3
    const-string/jumbo v0, "drafts"

    goto :goto_0

    .line 232
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
