.class public Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
.super Lcom/twitter/app/common/abs/AbsFragmentActivity$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/common/base/TwitterFragmentActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field f:Z

.field g:Z

.field h:I

.field i:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 769
    invoke-direct {p0}, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;-><init>()V

    .line 764
    iput-boolean v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->f:Z

    .line 765
    iput-boolean v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->g:Z

    .line 766
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->h:I

    .line 767
    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->i:I

    .line 770
    return-void
.end method

.method protected constructor <init>(Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 773
    invoke-direct {p0, p1}, Lcom/twitter/app/common/abs/AbsFragmentActivity$a;-><init>(Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V

    .line 764
    iput-boolean v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->f:Z

    .line 765
    iput-boolean v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->g:Z

    .line 766
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->h:I

    .line 767
    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->i:I

    .line 774
    iget-boolean v0, p1, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->f:Z

    iput-boolean v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->f:Z

    .line 775
    iget-boolean v0, p1, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->g:Z

    iput-boolean v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->g:Z

    .line 776
    iget v0, p1, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->i:I

    iput v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->i:I

    .line 777
    iget v0, p1, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->h:I

    iput v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->h:I

    .line 778
    return-void
.end method


# virtual methods
.method public final c(Z)V
    .locals 0

    .prologue
    .line 820
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c()V

    .line 821
    iput-boolean p1, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->f:Z

    .line 822
    return-void
.end method

.method public final d(I)V
    .locals 0

    .prologue
    .line 791
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c()V

    .line 792
    iput p1, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->h:I

    .line 793
    return-void
.end method

.method public final d(Z)V
    .locals 0

    .prologue
    .line 840
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c()V

    .line 841
    iput-boolean p1, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->g:Z

    .line 842
    return-void
.end method
