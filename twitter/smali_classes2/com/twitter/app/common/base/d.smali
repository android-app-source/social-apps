.class public Lcom/twitter/app/common/base/d;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/common/base/d$a;
    }
.end annotation


# static fields
.field private static b:I


# instance fields
.field final a:Landroid/app/Activity;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/twitter/app/common/base/d$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const/16 v0, 0xf00

    sput v0, Lcom/twitter/app/common/base/d;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/base/d;->c:Ljava/util/Map;

    .line 50
    iput-object p1, p0, Lcom/twitter/app/common/base/d;->a:Landroid/app/Activity;

    .line 51
    return-void
.end method

.method public static a()I
    .locals 2
    .annotation build Landroid/support/annotation/IntRange;
        from = 0x0L
        to = 0xfffL
    .end annotation

    .prologue
    .line 55
    sget v0, Lcom/twitter/app/common/base/d;->b:I

    const/16 v1, 0xfff

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string/jumbo v1, "Too many request codes have been allocated"

    invoke-static {v0, v1}, Lcom/twitter/util/g;->a(ZLjava/lang/String;)Z

    .line 57
    const-class v0, Lcom/twitter/app/common/base/d;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 58
    sget v0, Lcom/twitter/app/common/base/d;->b:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/twitter/app/common/base/d;->b:I

    return v0

    .line 55
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(I)Z
    .locals 2

    .prologue
    const v1, 0xa000

    .line 106
    const v0, 0xffff

    if-eq p0, v0, :cond_0

    and-int v0, p0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(ILandroid/content/Intent;)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
            to = 0xfffL
        .end annotation
    .end param

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/app/common/base/d;->c:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const-string/jumbo v1, "No receiver for permission request found"

    invoke-static {v0, v1}, Lcom/twitter/util/f;->a(ZLjava/lang/String;)Z

    .line 69
    iget-object v0, p0, Lcom/twitter/app/common/base/d;->a:Landroid/app/Activity;

    const v1, 0xa000

    or-int/2addr v1, p1

    invoke-virtual {v0, p2, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 70
    return-void
.end method

.method public a(Ljava/lang/Iterable;Lcom/twitter/app/common/base/d$a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/twitter/app/common/base/d$a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 79
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 80
    iget-object v2, p0, Lcom/twitter/app/common/base/d;->c:Ljava/util/Map;

    invoke-interface {v2, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 82
    :cond_0
    return-void
.end method

.method b(ILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 95
    and-int/lit16 v3, p1, 0xfff

    .line 96
    const-string/jumbo v0, "extra_perm_result"

    .line 97
    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/android/PermissionResult;

    .line 98
    iget-object v1, p0, Lcom/twitter/app/common/base/d;->c:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/app/common/base/d$a;

    .line 99
    if-eqz v1, :cond_1

    const/4 v2, 0x1

    :goto_0
    const-string/jumbo v4, "No receiver found to dispatch permission request result"

    invoke-static {v2, v4}, Lcom/twitter/util/g;->a(ZLjava/lang/String;)Z

    .line 100
    if-eqz v1, :cond_0

    .line 101
    invoke-interface {v1, v3, v0}, Lcom/twitter/app/common/base/d$a;->a(ILcom/twitter/util/android/PermissionResult;)V

    .line 103
    :cond_0
    return-void

    .line 99
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
