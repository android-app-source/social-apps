.class public abstract Lcom/twitter/app/common/base/AppCompatPreferenceActivity;
.super Landroid/preference/PreferenceActivity;
.source "Twttr"


# instance fields
.field private a:Landroid/support/v7/app/AppCompatDelegate;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->b:I

    return-void
.end method

.method private c()Landroid/support/v7/app/AppCompatDelegate;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->a:Landroid/support/v7/app/AppCompatDelegate;

    if-nez v0, :cond_0

    .line 152
    const/4 v0, 0x0

    invoke-static {p0, v0}, Landroid/support/v7/app/AppCompatDelegate;->create(Landroid/app/Activity;Landroid/support/v7/app/AppCompatCallback;)Landroid/support/v7/app/AppCompatDelegate;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->a:Landroid/support/v7/app/AppCompatDelegate;

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->a:Landroid/support/v7/app/AppCompatDelegate;

    return-object v0
.end method


# virtual methods
.method public addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->c()Landroid/support/v7/app/AppCompatDelegate;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/app/AppCompatDelegate;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 113
    return-void
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->c()Landroid/support/v7/app/AppCompatDelegate;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDelegate;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method public invalidateOptionsMenu()V
    .locals 1

    .prologue
    .line 146
    invoke-direct {p0}, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->c()Landroid/support/v7/app/AppCompatDelegate;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDelegate;->invalidateOptionsMenu()V

    .line 147
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 129
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 130
    invoke-direct {p0}, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->c()Landroid/support/v7/app/AppCompatDelegate;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/AppCompatDelegate;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 131
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->c()Landroid/support/v7/app/AppCompatDelegate;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDelegate;->installViewFactory()V

    .line 52
    invoke-virtual {v0, p1}, Landroid/support/v7/app/AppCompatDelegate;->onCreate(Landroid/os/Bundle;)V

    .line 53
    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDelegate;->applyDayNight()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->b:I

    if-eqz v0, :cond_0

    .line 58
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1

    .line 59
    invoke-virtual {p0}, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    iget v1, p0, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->b:I

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->onApplyThemeResource(Landroid/content/res/Resources$Theme;IZ)V

    .line 64
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 65
    return-void

    .line 61
    :cond_1
    iget v0, p0, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->b:I

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->setTheme(I)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 141
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 142
    invoke-direct {p0}, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->c()Landroid/support/v7/app/AppCompatDelegate;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDelegate;->onDestroy()V

    .line 143
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 76
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 77
    invoke-direct {p0}, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->c()Landroid/support/v7/app/AppCompatDelegate;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/AppCompatDelegate;->onPostCreate(Landroid/os/Bundle;)V

    .line 78
    return-void
.end method

.method protected onPostResume()V
    .locals 1

    .prologue
    .line 117
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPostResume()V

    .line 118
    invoke-direct {p0}, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->c()Landroid/support/v7/app/AppCompatDelegate;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDelegate;->onPostResume()V

    .line 119
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 135
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStop()V

    .line 136
    invoke-direct {p0}, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->c()Landroid/support/v7/app/AppCompatDelegate;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDelegate;->onStop()V

    .line 137
    return-void
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 123
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onTitleChanged(Ljava/lang/CharSequence;I)V

    .line 124
    invoke-direct {p0}, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->c()Landroid/support/v7/app/AppCompatDelegate;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/AppCompatDelegate;->setTitle(Ljava/lang/CharSequence;)V

    .line 125
    return-void
.end method

.method public setContentView(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->c()Landroid/support/v7/app/AppCompatDelegate;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/AppCompatDelegate;->setContentView(I)V

    .line 98
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->c()Landroid/support/v7/app/AppCompatDelegate;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/AppCompatDelegate;->setContentView(Landroid/view/View;)V

    .line 103
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->c()Landroid/support/v7/app/AppCompatDelegate;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/app/AppCompatDelegate;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 108
    return-void
.end method

.method public setTheme(I)V
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param

    .prologue
    .line 69
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->setTheme(I)V

    .line 71
    iput p1, p0, Lcom/twitter/app/common/base/AppCompatPreferenceActivity;->b:I

    .line 72
    return-void
.end method
