.class public Lcom/twitter/app/common/base/BaseFragmentActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/base/e;
.implements Lcom/twitter/app/common/util/j;


# static fields
.field static final synthetic H:Z


# instance fields
.field private final a:Lcwv;

.field private final b:Lcom/twitter/app/common/util/a;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private h:Laoo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/twitter/app/common/base/BaseFragmentActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/app/common/base/BaseFragmentActivity;->H:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    .line 34
    new-instance v0, Lcwv;

    invoke-direct {v0}, Lcwv;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->a:Lcwv;

    .line 35
    new-instance v0, Lcom/twitter/app/common/util/a;

    invoke-direct {v0}, Lcom/twitter/app/common/util/a;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->b:Lcom/twitter/app/common/util/a;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 226
    sget-boolean v0, Lcom/twitter/app/common/base/BaseFragmentActivity;->H:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->g:Ljava/util/Map;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 227
    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->g:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public a(ILcom/twitter/app/common/util/o;)V
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->b:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/app/common/util/a;->a(ILcom/twitter/app/common/util/o;)V

    .line 181
    return-void
.end method

.method public a(Lcom/twitter/app/common/util/b$a;)V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->b:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/util/a;->a(Lcom/twitter/app/common/util/b$a;)V

    .line 171
    return-void
.end method

.method public a(Lcom/twitter/app/common/util/o;)V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->b:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/util/a;->a(Lcom/twitter/app/common/util/o;)V

    .line 191
    return-void
.end method

.method public final a(Lcom/twitter/util/concurrent/g;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/concurrent/g",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 260
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->a:Lcwv;

    invoke-static {v0, p1}, Lcqz;->a(Lcwv;Lcom/twitter/util/concurrent/g;)V

    .line 261
    return-void
.end method

.method public final a(Lrx/j;)V
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->a:Lcwv;

    invoke-virtual {v0, p1}, Lcwv;->a(Lrx/j;)V

    .line 254
    return-void
.end method

.method public b(Lcom/twitter/app/common/util/b$a;)V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->b:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/util/a;->b(Lcom/twitter/app/common/util/b$a;)Z

    .line 176
    return-void
.end method

.method public b_(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 237
    sget-boolean v0, Lcom/twitter/app/common/base/BaseFragmentActivity;->H:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->g:Ljava/util/Map;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public f_()Z
    .locals 1

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->c:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g_()Z
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->d:Z

    return v0
.end method

.method public i(I)V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->b:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/util/a;->a(I)Z

    .line 186
    return-void
.end method

.method public isChangingConfigurations()Z
    .locals 1

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->f:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->isChangingConfigurations()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDestroyed()Z
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->e:Z

    return v0
.end method

.method protected l_()V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 247
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->b:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/twitter/app/common/util/a;->a(Landroid/app/Activity;IILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/AppCompatActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 133
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 112
    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0, p1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    .line 113
    iget-object v1, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->h:Laoo;

    invoke-virtual {v1}, Laoo;->a()Z

    move-result v1

    invoke-static {v0, v1}, Laoo;->a(Landroid/content/res/Configuration;Z)V

    .line 114
    invoke-virtual {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 115
    invoke-super {p0, v0}, Landroid/support/v7/app/AppCompatActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 116
    iget-object v1, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->b:Lcom/twitter/app/common/util/a;

    invoke-virtual {v1, p0, v0}, Lcom/twitter/app/common/util/a;->a(Landroid/app/Activity;Landroid/content/res/Configuration;)V

    .line 117
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 48
    new-instance v0, Laoo;

    invoke-direct {v0, p0}, Laoo;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->h:Laoo;

    .line 49
    invoke-virtual {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->getLastCustomNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->g:Ljava/util/Map;

    .line 50
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->g:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->g:Ljava/util/Map;

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->b:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p0, p1}, Lcom/twitter/app/common/util/a;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 54
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    return-void
.end method

.method protected onDestroy()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->a:Lcwv;

    invoke-virtual {v0}, Lcwv;->B_()V

    .line 123
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->e:Z

    .line 124
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onDestroy()V

    .line 125
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->b:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p0}, Lcom/twitter/app/common/util/a;->e(Landroid/app/Activity;)V

    .line 126
    return-void
.end method

.method public onMultiWindowModeChanged(Z)V
    .locals 1

    .prologue
    .line 137
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onMultiWindowModeChanged(Z)V

    .line 138
    invoke-static {}, Lalc;->d()Lalc;

    move-result-object v0

    invoke-virtual {v0}, Lalc;->n()Lcom/twitter/app/common/util/m;

    move-result-object v0

    .line 139
    invoke-virtual {v0, p1}, Lcom/twitter/app/common/util/m;->a(Z)V

    .line 140
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->b:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p0, p1}, Lcom/twitter/app/common/util/a;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 61
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 62
    return-void
.end method

.method protected onPause()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->d:Z

    .line 88
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onPause()V

    .line 89
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->b:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p0}, Lcom/twitter/app/common/util/a;->c(Landroid/app/Activity;)V

    .line 90
    return-void
.end method

.method protected onResume()V
    .locals 2
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 75
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    .line 76
    invoke-static {}, Lalc;->d()Lalc;

    move-result-object v0

    invoke-virtual {v0}, Lalc;->n()Lcom/twitter/app/common/util/m;

    move-result-object v0

    .line 77
    invoke-virtual {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->isInMultiWindowMode()Z

    move-result v1

    .line 76
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/util/m;->a(Z)V

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->b:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p0}, Lcom/twitter/app/common/util/a;->b(Landroid/app/Activity;)V

    .line 80
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onResume()V

    .line 81
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->d:Z

    .line 82
    return-void
.end method

.method public final onRetainCustomNonConfigurationInstance()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/twitter/app/common/base/BaseFragmentActivity;->l_()V

    .line 215
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->g:Ljava/util/Map;

    return-object v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 103
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 104
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->b:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p0, p1}, Lcom/twitter/app/common/util/a;->b(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 105
    return-void
.end method

.method protected onStart()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->b:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p0}, Lcom/twitter/app/common/util/a;->a(Landroid/app/Activity;)V

    .line 68
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onStart()V

    .line 69
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->c:Z

    .line 70
    return-void
.end method

.method protected onStop()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->c:Z

    .line 96
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onStop()V

    .line 97
    iget-object v0, p0, Lcom/twitter/app/common/base/BaseFragmentActivity;->b:Lcom/twitter/app/common/util/a;

    invoke-virtual {v0, p0}, Lcom/twitter/app/common/util/a;->d(Landroid/app/Activity;)V

    .line 98
    return-void
.end method
