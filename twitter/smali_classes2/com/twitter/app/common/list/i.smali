.class public Lcom/twitter/app/common/list/i;
.super Lcom/twitter/app/common/base/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/common/list/i$b;,
        Lcom/twitter/app/common/list/i$a;
    }
.end annotation


# static fields
.field public static final d:Lcom/twitter/app/common/list/i;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/twitter/app/common/list/i;

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Lcom/twitter/app/common/list/i;-><init>(Landroid/os/Bundle;)V

    sput-object v0, Lcom/twitter/app/common/list/i;->d:Lcom/twitter/app/common/list/i;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/twitter/app/common/base/b;-><init>(Landroid/os/Bundle;)V

    .line 33
    return-void
.end method

.method public static c(Landroid/os/Bundle;)Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/twitter/app/common/list/i;

    invoke-direct {v0, p0}, Lcom/twitter/app/common/list/i;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public a(J)J
    .locals 3

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/app/common/list/i;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "owner_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public h()Lcom/twitter/app/common/list/i$a;
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lcom/twitter/app/common/list/i$b;

    invoke-direct {v0, p0}, Lcom/twitter/app/common/list/i$b;-><init>(Lcom/twitter/app/common/list/i;)V

    return-object v0
.end method

.method public synthetic i()Lcom/twitter/app/common/base/b$a;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/twitter/app/common/list/i;->h()Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    return-object v0
.end method

.method public o()Z
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/app/common/list/i;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "is_refreshable"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public p()I
    .locals 3

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/app/common/list/i;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "list_choice_mode"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public q()I
    .locals 3
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/app/common/list/i;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "empty_title_res_id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public r()I
    .locals 3
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/twitter/app/common/list/i;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "empty_description_res_id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public s()I
    .locals 3
    .annotation build Landroid/support/annotation/IdRes;
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/twitter/app/common/list/i;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "list_inflated_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public t()I
    .locals 3

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/app/common/list/i;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "translation_y"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public u()I
    .locals 3

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/app/common/list/i;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "list_bottom_extra_padding"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public v()Z
    .locals 3

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/app/common/list/i;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "is_horizontal_padding_enabled"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public w()Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/app/common/list/i;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "scribe_item"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    return-object v0
.end method
