.class public Lcom/twitter/app/common/list/TwitterListFragment;
.super Lcom/twitter/app/common/abs/AbsFragment;
.source "Twttr"

# interfaces
.implements Lcmr$a;
.implements Lcms;
.implements Lcom/twitter/app/common/list/c;
.implements Lcom/twitter/app/common/list/d$a;
.implements Lcom/twitter/app/common/list/l$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/common/list/TwitterListFragment$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "A:",
        "Lcjr",
        "<TT;>;>",
        "Lcom/twitter/app/common/abs/AbsFragment;",
        "Lcmr$a;",
        "Lcms;",
        "Lcom/twitter/app/common/list/c;",
        "Lcom/twitter/app/common/list/d$a",
        "<TT;>;",
        "Lcom/twitter/app/common/list/l$b;"
    }
.end annotation


# instance fields
.field protected U:Z

.field protected V:Z

.field protected W:Lcom/twitter/android/av/j;

.field protected X:Lcom/twitter/android/revenue/o;

.field protected Y:Lcom/twitter/app/common/list/a;

.field protected Z:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

.field private a:Lcom/twitter/app/common/list/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/app/common/list/d",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcno$c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/android/client/j;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Landroid/os/Handler;

.field private final e:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcnl;

.field private g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private h:Lcmt;

.field private i:Z

.field private j:Z

.field private k:Lcom/twitter/app/common/base/TwitterFragmentActivity;

.field private l:Lcom/twitter/refresh/widget/RefreshableListView$e;

.field private m:Lcom/twitter/library/av/b;

.field private n:Laoy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoy",
            "<",
            "Lcbi",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private final o:Laow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laow",
            "<",
            "Lcbi",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private p:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/twitter/app/common/abs/AbsFragment;-><init>()V

    .line 95
    sget-object v0, Lcom/twitter/app/common/list/a;->a:Lcom/twitter/app/common/list/a;

    iput-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->Y:Lcom/twitter/app/common/list/a;

    .line 102
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->b:Ljava/util/Set;

    .line 103
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->c:Ljava/util/Set;

    .line 104
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->d:Landroid/os/Handler;

    .line 106
    new-instance v0, Lcom/twitter/app/common/list/TwitterListFragment$1;

    invoke-direct {v0, p0}, Lcom/twitter/app/common/list/TwitterListFragment$1;-><init>(Lcom/twitter/app/common/list/TwitterListFragment;)V

    iput-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->e:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 144
    new-instance v0, Lcnj;

    invoke-direct {v0}, Lcnj;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->f:Lcnl;

    .line 148
    sget-object v0, Lcmt;->a:Lcmt;

    iput-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->h:Lcmt;

    .line 151
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->j:Z

    .line 159
    new-instance v0, Lcom/twitter/app/common/list/TwitterListFragment$2;

    invoke-direct {v0, p0}, Lcom/twitter/app/common/list/TwitterListFragment$2;-><init>(Lcom/twitter/app/common/list/TwitterListFragment;)V

    iput-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->o:Laow;

    .line 180
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->p:Ljava/lang/Boolean;

    return-void
.end method

.method private a(Landroid/database/Cursor;)Lcbi;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Lcbi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1228
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->az()Lcjr;

    move-result-object v0

    instance-of v0, v0, Lcbq;

    if-eqz v0, :cond_0

    .line 1229
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->az()Lcjr;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbq;

    invoke-interface {v0, p1}, Lcbq;->a(Landroid/database/Cursor;)Lcbi;

    move-result-object v0

    .line 1233
    :goto_0
    return-object v0

    .line 1231
    :cond_0
    new-instance v0, Lcbe;

    invoke-direct {v0, p1}, Lcbe;-><init>(Landroid/database/Cursor;)V

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbi;

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/app/common/list/TwitterListFragment;Landroid/database/Cursor;)Lcbi;
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Landroid/database/Cursor;)Lcbi;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/app/common/list/TwitterListFragment;)Lcom/twitter/app/common/list/d;
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->f()Lcom/twitter/app/common/list/d;

    move-result-object v0

    return-object v0
.end method

.method protected static a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1106
    packed-switch p2, :pswitch_data_0

    :pswitch_0
    move-object v0, v1

    .line 1129
    :goto_0
    return-object v0

    .line 1108
    :pswitch_1
    const-string/jumbo v0, "get_newer"

    .line 1129
    :goto_1
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    aput-object v1, v2, v3

    const/4 v3, 0x3

    aput-object v1, v2, v3

    const/4 v1, 0x4

    aput-object v0, v2, v1

    invoke-static {v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1112
    :pswitch_2
    const-string/jumbo v0, "get_older"

    goto :goto_1

    .line 1117
    :pswitch_3
    const-string/jumbo v0, "get_initial"

    goto :goto_1

    .line 1121
    :pswitch_4
    const-string/jumbo v0, "get_middle"

    goto :goto_1

    .line 1106
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method static synthetic a(Lcom/twitter/app/common/list/TwitterListFragment;I)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->c(I)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/app/common/list/TwitterListFragment;)Z
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->v_()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/app/common/list/TwitterListFragment;I)Z
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->d(I)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/twitter/app/common/list/TwitterListFragment;)Lcbi;
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->q()Lcbi;

    move-result-object v0

    return-object v0
.end method

.method private c(I)V
    .locals 4

    .prologue
    .line 581
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->ak()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->m:Lcom/twitter/library/av/b;

    if-nez v0, :cond_1

    .line 593
    :cond_0
    :goto_0
    return-void

    .line 586
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aC()Landroid/database/Cursor;

    move-result-object v0

    .line 587
    if-eqz v0, :cond_0

    .line 588
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    invoke-static {v2, p1}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 590
    iget-object v2, p0, Lcom/twitter/app/common/list/TwitterListFragment;->m:Lcom/twitter/library/av/b;

    new-instance v3, Lcom/twitter/library/av/q;

    invoke-direct {v3, v0}, Lcom/twitter/library/av/q;-><init>(Landroid/database/Cursor;)V

    invoke-virtual {v2, v3, v1}, Lcom/twitter/library/av/b;->a(Lcom/twitter/library/av/p;I)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/twitter/app/common/list/TwitterListFragment;)Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->j:Z

    return v0
.end method

.method static synthetic e(Lcom/twitter/app/common/list/TwitterListFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private e(I)V
    .locals 2

    .prologue
    .line 1083
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1084
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    .line 1085
    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 1086
    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->v()V

    .line 1091
    :cond_0
    :goto_0
    return-void

    .line 1087
    :cond_1
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 1088
    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->u()V

    goto :goto_0
.end method

.method private f()Lcom/twitter/app/common/list/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/app/common/list/d",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 478
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->a:Lcom/twitter/app/common/list/d;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/d;

    return-object v0
.end method

.method private k()V
    .locals 2

    .prologue
    .line 1191
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1192
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->d:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/app/common/list/TwitterListFragment$7;

    invoke-direct {v1, p0}, Lcom/twitter/app/common/list/TwitterListFragment$7;-><init>(Lcom/twitter/app/common/list/TwitterListFragment;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1201
    :cond_0
    return-void
.end method

.method private q()Lcbi;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcbi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1238
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Landroid/database/Cursor;)Lcbi;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public H()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 436
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/common/list/i;->c(Landroid/os/Bundle;)Lcom/twitter/app/common/list/i;

    move-result-object v0

    return-object v0
.end method

.method protected H_()V
    .locals 0

    .prologue
    .line 819
    return-void
.end method

.method public synthetic I()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->H()Lcom/twitter/app/common/list/i;

    move-result-object v0

    return-object v0
.end method

.method protected K()Laoy;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laoy",
            "<",
            "Lcbi",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 1209
    const/4 v0, 0x0

    return-object v0
.end method

.method protected K_()Z
    .locals 1

    .prologue
    .line 679
    const/4 v0, 0x0

    return v0
.end method

.method public L()V
    .locals 2

    .prologue
    .line 425
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 426
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l;->a(Lcbi;)V

    .line 428
    :cond_0
    return-void
.end method

.method protected M_()Z
    .locals 1

    .prologue
    .line 305
    const/4 v0, 0x0

    return v0
.end method

.method protected N_()Z
    .locals 1

    .prologue
    .line 689
    const/4 v0, 0x0

    return v0
.end method

.method public V()V
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->M_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->ao()V

    .line 219
    invoke-direct {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->f()Lcom/twitter/app/common/list/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/d;->c()V

    .line 221
    :cond_0
    return-void
.end method

.method public V_()V
    .locals 0

    .prologue
    .line 850
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->H_()V

    .line 851
    return-void
.end method

.method public W()V
    .locals 0

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->ar()Z

    .line 226
    return-void
.end method

.method protected a(J)I
    .locals 7

    .prologue
    const/4 v1, -0x1

    .line 854
    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v0, p1, v2

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aj()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v1

    .line 866
    :cond_1
    :goto_0
    return v0

    .line 858
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    .line 859
    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v3

    .line 861
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_3

    .line 862
    invoke-interface {v2, v0}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    cmp-long v4, v4, p1

    if-eqz v4, :cond_1

    .line 861
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 866
    goto :goto_0
.end method

.method public final a(Lcno$c;)Lcom/twitter/app/common/list/TwitterListFragment;
    .locals 1

    .prologue
    .line 896
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 897
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/list/l;->a(Lcno$c;)V

    .line 901
    :goto_0
    return-object p0

    .line 899
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected a(Lcom/twitter/android/client/j;)Lcom/twitter/app/common/list/TwitterListFragment;
    .locals 2

    .prologue
    .line 1027
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->c:Ljava/util/Set;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1028
    return-object p0
.end method

.method public a(Lcom/twitter/refresh/widget/RefreshableListView$e;)Lcom/twitter/app/common/list/TwitterListFragment;
    .locals 1

    .prologue
    .line 840
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 841
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/list/l;->a(Lcom/twitter/refresh/widget/RefreshableListView$e;)V

    .line 845
    :goto_0
    return-object p0

    .line 843
    :cond_0
    iput-object p1, p0, Lcom/twitter/app/common/list/TwitterListFragment;->l:Lcom/twitter/refresh/widget/RefreshableListView$e;

    goto :goto_0
.end method

.method protected a(JJ)V
    .locals 0

    .prologue
    .line 928
    return-void
.end method

.method protected a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 0

    .prologue
    .line 431
    return-void
.end method

.method protected a(Laog;)V
    .locals 6

    .prologue
    .line 712
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->a(Laog;)V

    .line 713
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/l;

    .line 715
    iget-object v1, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getOnItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    if-nez v1, :cond_0

    .line 716
    iget-object v1, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    new-instance v2, Lcom/twitter/app/common/list/TwitterListFragment$3;

    invoke-direct {v2, p0}, Lcom/twitter/app/common/list/TwitterListFragment$3;-><init>(Lcom/twitter/app/common/list/TwitterListFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 723
    :cond_0
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/list/l;->a(Lcom/twitter/app/common/list/l$b;)V

    .line 724
    iget-object v1, p0, Lcom/twitter/app/common/list/TwitterListFragment;->l:Lcom/twitter/refresh/widget/RefreshableListView$e;

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l;->a(Lcom/twitter/refresh/widget/RefreshableListView$e;)V

    .line 727
    new-instance v1, Lcom/twitter/android/av/j;

    iget-object v2, p0, Lcom/twitter/app/common/list/TwitterListFragment;->T:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/twitter/android/av/j;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/twitter/app/common/list/TwitterListFragment;->W:Lcom/twitter/android/av/j;

    .line 728
    iget-object v1, p0, Lcom/twitter/app/common/list/TwitterListFragment;->W:Lcom/twitter/android/av/j;

    invoke-virtual {v1, v0}, Lcom/twitter/android/av/j;->a(Lcom/twitter/app/common/list/l;)V

    .line 730
    iget-object v1, p0, Lcom/twitter/app/common/list/TwitterListFragment;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcno$c;

    .line 731
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l;->a(Lcno$c;)V

    goto :goto_0

    .line 733
    :cond_1
    iget-object v1, p0, Lcom/twitter/app/common/list/TwitterListFragment;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 734
    new-instance v1, Lcom/twitter/app/common/list/TwitterListFragment$4;

    invoke-direct {v1, p0}, Lcom/twitter/app/common/list/TwitterListFragment$4;-><init>(Lcom/twitter/app/common/list/TwitterListFragment;)V

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l;->a(Lcno$c;)V

    .line 743
    new-instance v1, Lcom/twitter/app/common/list/TwitterListFragment$5;

    invoke-direct {v1, p0, v0}, Lcom/twitter/app/common/list/TwitterListFragment$5;-><init>(Lcom/twitter/app/common/list/TwitterListFragment;Lcom/twitter/app/common/list/l;)V

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l;->a(Lcom/twitter/app/common/list/l$c;)V

    .line 751
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->N_()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 754
    invoke-static {}, Lcom/twitter/android/revenue/k;->b()F

    move-result v1

    .line 755
    invoke-static {}, Lcom/twitter/android/revenue/k;->c()D

    move-result-wide v2

    .line 756
    invoke-static {}, Lcom/twitter/android/revenue/k;->d()Z

    move-result v4

    .line 757
    invoke-static {}, Lcom/twitter/android/revenue/k;->e()Z

    move-result v5

    .line 753
    invoke-static {v1, v2, v3, v4, v5}, Lcom/twitter/android/revenue/o;->a(FDZZ)Lcom/twitter/android/revenue/o;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/common/list/TwitterListFragment;->X:Lcom/twitter/android/revenue/o;

    .line 758
    iget-object v1, p0, Lcom/twitter/app/common/list/TwitterListFragment;->X:Lcom/twitter/android/revenue/o;

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l;->a(Lcno$c;)V

    .line 760
    :cond_2
    return-void
.end method

.method protected a(Lcbi;)V
    .locals 6
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->ay()Z

    move-result v0

    if-nez v0, :cond_0

    .line 356
    :goto_0
    return-void

    .line 339
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    .line 340
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/app/common/list/l;->z()Lcnk;

    move-result-object v1

    .line 341
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->b(Lcbi;)V

    .line 342
    invoke-virtual {p0, v1}, Lcom/twitter/app/common/list/TwitterListFragment;->b(Lcnk;)V

    .line 344
    iget-wide v2, v1, Lcnk;->c:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    .line 346
    iget-wide v2, v1, Lcnk;->c:J

    invoke-virtual {p0, v2, v3}, Lcom/twitter/app/common/list/TwitterListFragment;->a(J)I

    move-result v2

    .line 347
    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 348
    invoke-virtual {p0, v1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcnk;)V

    .line 355
    :goto_1
    invoke-static {v0}, Lcom/twitter/util/ui/a;->a(Landroid/widget/ListView;)V

    goto :goto_0

    .line 350
    :cond_1
    iget v1, v1, Lcnk;->d:I

    invoke-virtual {v0, v2, v1}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto :goto_1

    .line 353
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->g()V

    goto :goto_1
.end method

.method public a(Lcbi;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<TT;>;Z)V"
        }
    .end annotation

    .prologue
    .line 510
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcbi;)V

    .line 511
    return-void
.end method

.method protected a(Lcnk;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 876
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Lcom/twitter/app/common/list/l;->a(II)V

    .line 877
    return-void
.end method

.method protected final a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 0

    .prologue
    .line 947
    iput-object p1, p0, Lcom/twitter/app/common/list/TwitterListFragment;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 948
    return-void
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 3

    .prologue
    .line 763
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->H()Lcom/twitter/app/common/list/i;

    move-result-object v1

    .line 764
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->a(Ljava/lang/String;)Lcom/twitter/app/common/list/l$d;

    move-result-object v0

    .line 765
    invoke-virtual {v1}, Lcom/twitter/app/common/list/i;->q()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/app/common/list/l$d;->a(I)Lcom/twitter/app/common/list/l$d;

    move-result-object v0

    .line 766
    invoke-virtual {v1}, Lcom/twitter/app/common/list/i;->r()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/app/common/list/l$d;->b(I)Lcom/twitter/app/common/list/l$d;

    move-result-object v0

    const v2, 0x7f040176

    .line 767
    invoke-virtual {v0, v2}, Lcom/twitter/app/common/list/l$d;->c(I)Lcom/twitter/app/common/list/l$d;

    move-result-object v0

    .line 768
    invoke-virtual {v1}, Lcom/twitter/app/common/list/i;->s()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/app/common/list/l$d;->e(I)Lcom/twitter/app/common/list/l$d;

    move-result-object v2

    .line 769
    invoke-virtual {p1}, Lcom/twitter/app/common/list/l$d;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0400e5

    :goto_0
    invoke-virtual {v2, v0}, Lcom/twitter/app/common/list/l$d;->f(I)Lcom/twitter/app/common/list/l$d;

    move-result-object v0

    .line 771
    invoke-virtual {v1}, Lcom/twitter/app/common/list/i;->v()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/app/common/list/l$d;->a(Z)Lcom/twitter/app/common/list/l$d;

    move-result-object v0

    .line 772
    invoke-virtual {v1}, Lcom/twitter/app/common/list/i;->p()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/app/common/list/l$d;->i(I)Lcom/twitter/app/common/list/l$d;

    move-result-object v0

    .line 773
    invoke-virtual {v1}, Lcom/twitter/app/common/list/i;->t()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/app/common/list/l$d;->j(I)Lcom/twitter/app/common/list/l$d;

    move-result-object v0

    .line 774
    invoke-virtual {v1}, Lcom/twitter/app/common/list/i;->u()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/app/common/list/l$d;->k(I)Lcom/twitter/app/common/list/l$d;

    move-result-object v0

    .line 775
    invoke-virtual {v1}, Lcom/twitter/app/common/list/i;->o()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l$d;->b(Z)Lcom/twitter/app/common/list/l$d;

    .line 777
    invoke-static {}, Lbpu;->a()Lbpu;

    move-result-object v0

    invoke-virtual {v0}, Lbpu;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 778
    const v0, 0x7f0403df

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    .line 779
    invoke-virtual {p1}, Lcom/twitter/app/common/list/l$d;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f040175

    :goto_1
    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->h(I)V

    .line 783
    :goto_2
    return-void

    .line 769
    :cond_0
    const v0, 0x7f0400e3

    goto :goto_0

    .line 779
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 781
    :cond_2
    const v0, 0x7f04033f

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    goto :goto_2
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 2
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1062
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/abs/AbsFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 1063
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1065
    if-ne p3, v1, :cond_3

    .line 1066
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->w()V

    .line 1071
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->f()Lcom/twitter/app/common/list/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/d;->a()I

    move-result v0

    if-eq v0, v1, :cond_1

    .line 1072
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->ak()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p3, :cond_1

    .line 1073
    invoke-direct {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->k()V

    .line 1077
    :cond_1
    const/4 v0, 0x4

    if-ne p3, v0, :cond_2

    .line 1078
    invoke-direct {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->f()Lcom/twitter/app/common/list/d;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/list/d;->a(Lcom/twitter/library/service/s;)V

    .line 1080
    :cond_2
    return-void

    .line 1067
    :cond_3
    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    .line 1068
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aM()V

    goto :goto_0
.end method

.method protected a(Ljava/util/List;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/av/r;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 603
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->m:Lcom/twitter/library/av/b;

    if-nez v0, :cond_1

    .line 609
    :cond_0
    :goto_0
    return-void

    .line 607
    :cond_1
    new-instance v0, Lcom/twitter/library/av/s;

    invoke-direct {v0, p1}, Lcom/twitter/library/av/s;-><init>(Ljava/util/List;)V

    .line 608
    iget-object v1, p0, Lcom/twitter/app/common/list/TwitterListFragment;->m:Lcom/twitter/library/av/b;

    invoke-virtual {v1, v0, p2}, Lcom/twitter/library/av/b;->a(Lcom/twitter/library/av/p;I)V

    goto :goto_0
.end method

.method protected a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 374
    if-eqz p1, :cond_0

    .line 375
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->L()V

    .line 377
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->n:Laoy;

    if-eqz v0, :cond_3

    .line 378
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->n:Laoy;

    instance-of v0, v0, Laoz;

    if-eqz v0, :cond_1

    .line 379
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->n:Laoy;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    invoke-interface {v0}, Laoz;->a()V

    .line 381
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aN()V

    .line 385
    :cond_2
    :goto_0
    return-void

    .line 382
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 383
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/app/common/list/TwitterListFragment;->e:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v3, v1, v2}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0
.end method

.method public a(Lcmm;)Z
    .locals 1

    .prologue
    .line 1007
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 988
    const/4 v0, 0x0

    return v0
.end method

.method protected final aA()Z
    .locals 1

    .prologue
    .line 633
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->p:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 634
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->p:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 636
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final aB()Lcjt;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcjt",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 644
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->j()Lcjt;

    move-result-object v0

    return-object v0
.end method

.method protected aC()Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 655
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 656
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->az()Lcjr;

    move-result-object v0

    .line 657
    instance-of v1, v0, Lcbq;

    if-eqz v1, :cond_0

    .line 658
    invoke-virtual {v0}, Lcjr;->g()Lcbi;

    move-result-object v0

    check-cast v0, Lcbg;

    .line 659
    if-eqz v0, :cond_2

    .line 660
    invoke-interface {v0}, Lcbg;->a()Landroid/database/Cursor;

    move-result-object v0

    .line 669
    :goto_0
    return-object v0

    .line 663
    :cond_0
    invoke-virtual {v0}, Lcjr;->g()Lcbi;

    move-result-object v0

    .line 664
    instance-of v1, v0, Lcbe;

    if-eqz v1, :cond_2

    .line 665
    invoke-virtual {v0}, Lcbi;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lauv;->a()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    :cond_1
    check-cast v0, Lcbe;

    invoke-virtual {v0}, Lcbe;->a()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 669
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected aD()V
    .locals 2

    .prologue
    .line 889
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 890
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->f:Lcnl;

    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/app/common/list/l;->z()Lcnk;

    move-result-object v1

    invoke-interface {v0, v1}, Lcnl;->a(Lcnk;)V

    .line 892
    :cond_0
    return-void
.end method

.method public final aE()Z
    .locals 1

    .prologue
    .line 934
    iget-boolean v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->j:Z

    return v0
.end method

.method protected aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 1

    .prologue
    .line 961
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->g:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    return-object v0
.end method

.method protected aG()Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 1

    .prologue
    .line 978
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->Z:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    return-object v0
.end method

.method public aH()Lcmt;
    .locals 1

    .prologue
    .line 983
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->h:Lcmt;

    return-object v0
.end method

.method public aI()I
    .locals 1

    .prologue
    .line 1011
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aA()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aB()Lcjt;

    move-result-object v0

    invoke-interface {v0}, Lcjt;->a()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected aJ()Lcom/twitter/metrics/j;
    .locals 1

    .prologue
    .line 1015
    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v0

    return-object v0
.end method

.method public aK()Lcom/twitter/app/common/base/TwitterFragmentActivity;
    .locals 1

    .prologue
    .line 1144
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->k:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    return-object v0
.end method

.method public aL()V
    .locals 1

    .prologue
    .line 1153
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1154
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->x()V

    .line 1156
    :cond_0
    return-void
.end method

.method protected aM()V
    .locals 2

    .prologue
    .line 1175
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->d:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/app/common/list/TwitterListFragment$6;

    invoke-direct {v1, p0}, Lcom/twitter/app/common/list/TwitterListFragment$6;-><init>(Lcom/twitter/app/common/list/TwitterListFragment;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1183
    return-void
.end method

.method protected aN()V
    .locals 0

    .prologue
    .line 1216
    return-void
.end method

.method public a_(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1003
    return-void
.end method

.method protected aa_()Lcom/twitter/app/common/list/b;
    .locals 1

    .prologue
    .line 465
    new-instance v0, Lcom/twitter/app/common/list/TwitterListFragment$a;

    invoke-direct {v0, p0}, Lcom/twitter/app/common/list/TwitterListFragment$a;-><init>(Lcom/twitter/app/common/list/TwitterListFragment;)V

    return-object v0
.end method

.method public aj()Z
    .locals 1

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->ae()Z

    move-result v0

    return v0
.end method

.method public ak()Z
    .locals 1

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aA()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aB()Lcjt;

    move-result-object v0

    invoke-interface {v0}, Lcjt;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public al()Lcom/twitter/app/common/list/l;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/app/common/list/l",
            "<TT;TA;>;"
        }
    .end annotation

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->af()Laog;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/l;

    return-object v0
.end method

.method public am()I
    .locals 1

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->n()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public an()Z
    .locals 1

    .prologue
    .line 210
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-boolean v0, v0, Lcom/twitter/app/common/list/l;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected ao()V
    .locals 0

    .prologue
    .line 238
    return-void
.end method

.method protected final ap()V
    .locals 2

    .prologue
    .line 245
    invoke-direct {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->f()Lcom/twitter/app/common/list/d;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/d;->a(I)V

    .line 246
    return-void
.end method

.method protected aq()Z
    .locals 1

    .prologue
    .line 253
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->Z()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected ar()Z
    .locals 1

    .prologue
    .line 315
    const/4 v0, 0x0

    return v0
.end method

.method protected ar_()V
    .locals 0

    .prologue
    .line 391
    return-void
.end method

.method protected as()V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 360
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->L()V

    .line 361
    return-void
.end method

.method protected at()V
    .locals 1

    .prologue
    .line 367
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Z)V

    .line 368
    return-void
.end method

.method protected au()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 397
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->Y()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 398
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v0

    .line 399
    instance-of v2, v0, Lcom/twitter/util/android/d;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/twitter/util/android/d;

    invoke-virtual {v0}, Lcom/twitter/util/android/d;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 401
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 399
    goto :goto_0

    :cond_1
    move v0, v1

    .line 401
    goto :goto_0
.end method

.method protected av()Lcom/twitter/app/common/list/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/app/common/list/d",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 473
    new-instance v0, Lcom/twitter/app/common/list/d;

    invoke-direct {v0, p0}, Lcom/twitter/app/common/list/d;-><init>(Lcom/twitter/app/common/list/d$a;)V

    return-object v0
.end method

.method public final aw()Z
    .locals 1

    .prologue
    .line 525
    iget-boolean v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->i:Z

    return v0
.end method

.method protected ax()V
    .locals 3

    .prologue
    .line 566
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 567
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v1, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    .line 568
    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v0

    .line 569
    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    .line 570
    :goto_0
    invoke-virtual {v1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v1

    sub-int/2addr v0, v1

    .line 569
    invoke-direct {p0, v0}, Lcom/twitter/app/common/list/TwitterListFragment;->c(I)V

    .line 572
    :cond_0
    return-void

    .line 570
    :cond_1
    invoke-virtual {v1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    goto :goto_0
.end method

.method protected final ay()Z
    .locals 1

    .prologue
    .line 615
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->p:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 616
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->p:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 618
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final az()Lcjr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TA;"
        }
    .end annotation

    .prologue
    .line 626
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->h()Lcjr;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcmr;)I
    .locals 1

    .prologue
    .line 994
    const/4 v0, 0x2

    return v0
.end method

.method protected b(Lank;)Lcom/twitter/app/common/list/j;
    .locals 2

    .prologue
    .line 695
    invoke-static {}, Lcom/twitter/app/common/list/f;->a()Lcom/twitter/app/common/list/f$a;

    move-result-object v0

    .line 696
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/f$a;->a(Lamu;)Lcom/twitter/app/common/list/f$a;

    move-result-object v0

    .line 697
    invoke-virtual {v0}, Lcom/twitter/app/common/list/f$a;->a()Lcom/twitter/app/common/list/j;

    move-result-object v0

    .line 695
    return-object v0
.end method

.method protected b()V
    .locals 2

    .prologue
    .line 261
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->b()V

    .line 265
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->w_()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/l;

    .line 266
    iget v0, v0, Lcom/twitter/app/common/abs/l;->c:I

    invoke-direct {p0, v0}, Lcom/twitter/app/common/list/TwitterListFragment;->e(I)V

    goto :goto_0

    .line 269
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->g()V

    .line 270
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->Y:Lcom/twitter/app/common/list/a;

    invoke-virtual {v0}, Lcom/twitter/app/common/list/a;->a()Z

    .line 271
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->W:Lcom/twitter/android/av/j;

    invoke-virtual {v0}, Lcom/twitter/android/av/j;->e()V

    .line 273
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->ak()Z

    move-result v0

    if-nez v0, :cond_1

    .line 275
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->p()V

    .line 277
    :cond_1
    invoke-direct {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->f()Lcom/twitter/app/common/list/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/d;->d()V

    .line 278
    return-void
.end method

.method public b(Lcbi;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 414
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->ak()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcbi;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415
    invoke-direct {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->k()V

    .line 419
    :goto_0
    return-void

    .line 417
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/list/l;->a(Lcbi;)V

    goto :goto_0
.end method

.method protected b(Lcnk;)V
    .locals 3

    .prologue
    .line 1164
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/list/TwitterListFragment;->d(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1165
    iget-wide v0, p1, Lcnk;->c:J

    invoke-virtual {p0, v0, v1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(J)I

    move-result v0

    .line 1166
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    iget v2, p1, Lcnk;->e:I

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/app/common/list/l;->c(Z)Z

    .line 1168
    :cond_0
    return-void

    .line 1166
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Lcom/twitter/library/service/s;II)V
    .locals 0

    .prologue
    .line 1054
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/abs/AbsFragment;->b(Lcom/twitter/library/service/s;II)V

    .line 1056
    invoke-direct {p0, p3}, Lcom/twitter/app/common/list/TwitterListFragment;->e(I)V

    .line 1057
    return-void
.end method

.method protected synthetic c(Lank;)Lcom/twitter/app/common/abs/c;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->b(Lank;)Lcom/twitter/app/common/list/j;

    move-result-object v0

    return-object v0
.end method

.method public c(Lcmr;)V
    .locals 0

    .prologue
    .line 999
    return-void
.end method

.method protected c_(I)Z
    .locals 1

    .prologue
    .line 835
    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->d(I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic d(Lank;)Lann;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->b(Lank;)Lcom/twitter/app/common/list/j;

    move-result-object v0

    return-object v0
.end method

.method public final d(Z)V
    .locals 0

    .prologue
    .line 943
    iput-boolean p1, p0, Lcom/twitter/app/common/list/TwitterListFragment;->j:Z

    .line 944
    return-void
.end method

.method protected e(Z)V
    .locals 2

    .prologue
    .line 1039
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1041
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1042
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 1043
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/j;

    .line 1044
    if-eqz v0, :cond_0

    .line 1045
    invoke-interface {v0, p1}, Lcom/twitter/android/client/j;->a(Z)V

    goto :goto_0

    .line 1047
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 1050
    :cond_1
    return-void
.end method

.method protected f(Lank;)Lcom/twitter/app/common/list/k;
    .locals 4

    .prologue
    .line 703
    new-instance v1, Lcom/twitter/app/common/list/l$d;

    invoke-direct {v1}, Lcom/twitter/app/common/list/l$d;-><init>()V

    .line 704
    invoke-virtual {p0, v1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 705
    invoke-static {}, Lcom/twitter/app/common/list/g;->c()Lcom/twitter/app/common/list/g$a;

    move-result-object v2

    new-instance v3, Lant;

    .line 706
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/base/BaseFragmentActivity;

    invoke-direct {v3, v0, v1}, Lant;-><init>(Landroid/app/Activity;Laod;)V

    invoke-virtual {v2, v3}, Lcom/twitter/app/common/list/g$a;->a(Lant;)Lcom/twitter/app/common/list/g$a;

    move-result-object v0

    .line 707
    invoke-virtual {v0}, Lcom/twitter/app/common/list/g$a;->a()Lcom/twitter/app/common/list/k;

    move-result-object v0

    .line 705
    return-object v0
.end method

.method protected synthetic g(Lank;)Lcom/twitter/app/common/abs/d;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->f(Lank;)Lcom/twitter/app/common/list/k;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 3

    .prologue
    .line 880
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 881
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->f:Lcnl;

    invoke-interface {v0}, Lcnl;->a()Lcnk;

    move-result-object v0

    .line 882
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    iget v2, v0, Lcnk;->e:I

    iget v0, v0, Lcnk;->d:I

    invoke-virtual {v1, v2, v0}, Lcom/twitter/app/common/list/l;->a(II)V

    .line 884
    :cond_0
    return-void
.end method

.method public g(I)V
    .locals 1

    .prologue
    .line 204
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/list/l;->a(I)V

    .line 207
    :cond_0
    return-void
.end method

.method public final g(J)V
    .locals 5

    .prologue
    .line 910
    iget-wide v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->a_:J

    .line 911
    iput-wide p1, p0, Lcom/twitter/app/common/list/TwitterListFragment;->a_:J

    .line 912
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aj()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 913
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/twitter/app/common/list/l;->c(Z)Z

    .line 915
    :cond_0
    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/twitter/app/common/list/TwitterListFragment;->a(JJ)V

    .line 916
    iget-boolean v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->j:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 917
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->at()V

    .line 919
    :cond_2
    return-void
.end method

.method protected synthetic h(Lank;)Lans;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->f(Lank;)Lcom/twitter/app/common/list/k;

    move-result-object v0

    return-object v0
.end method

.method protected h(I)V
    .locals 1

    .prologue
    .line 557
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->V:Z

    if-eqz v0, :cond_0

    .line 558
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->ax()V

    .line 560
    :cond_0
    return-void
.end method

.method protected i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1099
    const-string/jumbo v0, "unknown"

    return-object v0
.end method

.method protected j()Landroid/support/v4/content/Loader;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 331
    const/4 v0, 0x0

    return-object v0
.end method

.method protected l()V
    .locals 0

    .prologue
    .line 822
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 531
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 532
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aK()Lcom/twitter/app/common/base/TwitterFragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 533
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aK()Lcom/twitter/app/common/base/TwitterFragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->F()Lcmt;

    move-result-object v0

    .line 534
    invoke-virtual {v0, p0}, Lcmt;->a(Lcms;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 535
    invoke-virtual {v0, p0}, Lcmt;->a(Lcmr$a;)V

    .line 537
    :cond_0
    iput-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->h:Lcmt;

    .line 539
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->V:Z

    if-eqz v0, :cond_2

    .line 540
    new-instance v0, Lcom/twitter/library/av/b;

    iget-object v1, p0, Lcom/twitter/app/common/list/TwitterListFragment;->T:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/twitter/library/av/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->m:Lcom/twitter/library/av/b;

    .line 544
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->K()Laoy;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->n:Laoy;

    .line 548
    new-instance v0, Lcom/twitter/app/common/list/a;

    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aa_()Lcom/twitter/app/common/list/b;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/twitter/app/common/list/a;-><init>(Lcom/twitter/app/common/list/b;Lcom/twitter/app/common/list/c;)V

    iput-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->Y:Lcom/twitter/app/common/list/a;

    .line 549
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 1134
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onAttach(Landroid/app/Activity;)V

    .line 1136
    instance-of v0, p1, Lcom/twitter/app/common/base/TwitterFragmentActivity;

    if-eqz v0, :cond_0

    .line 1137
    check-cast p1, Lcom/twitter/app/common/base/TwitterFragmentActivity;

    iput-object p1, p0, Lcom/twitter/app/common/list/TwitterListFragment;->k:Lcom/twitter/app/common/base/TwitterFragmentActivity;

    .line 1139
    :cond_0
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->U:Z

    .line 1140
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 441
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 443
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aK()Lcom/twitter/app/common/base/TwitterFragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 444
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aK()Lcom/twitter/app/common/base/TwitterFragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->K()Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->i:Z

    .line 446
    if-eqz p1, :cond_1

    .line 447
    iget-object v1, p0, Lcom/twitter/app/common/list/TwitterListFragment;->f:Lcnl;

    const-string/jumbo v0, "scroll_position"

    sget-object v2, Lcnk;->b:Lcom/twitter/util/serialization/l;

    sget-object v3, Lcnk;->a:Lcnk;

    invoke-static {p1, v0, v2, v3}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/util/serialization/l;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnk;

    invoke-interface {v1, v0}, Lcnl;->a(Lcnk;)V

    .line 451
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->H()Lcom/twitter/app/common/list/i;

    move-result-object v0

    .line 452
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/app/common/list/i;->a(J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/app/common/list/TwitterListFragment;->a_:J

    .line 453
    invoke-virtual {v0}, Lcom/twitter/app/common/list/i;->w()Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->Z:Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    .line 456
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->K_()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->V:Z

    .line 457
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->av()Lcom/twitter/app/common/list/d;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->a:Lcom/twitter/app/common/list/d;

    .line 458
    return-void

    .line 444
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 787
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->m:Lcom/twitter/library/av/b;

    if-eqz v0, :cond_0

    .line 788
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->m:Lcom/twitter/library/av/b;

    invoke-virtual {v0}, Lcom/twitter/library/av/b;->a()V

    .line 791
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->W:Lcom/twitter/android/av/j;

    if-eqz v0, :cond_1

    .line 792
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->W:Lcom/twitter/android/av/j;

    invoke-virtual {v0}, Lcom/twitter/android/av/j;->f()V

    .line 795
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->X:Lcom/twitter/android/revenue/o;

    if-eqz v0, :cond_2

    .line 796
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->X:Lcom/twitter/android/revenue/o;

    invoke-virtual {v0}, Lcom/twitter/android/revenue/o;->a()V

    .line 798
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->h:Lcmt;

    invoke-virtual {v0, p0}, Lcmt;->b(Lcms;)V

    .line 799
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->d:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 800
    invoke-direct {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->f()Lcom/twitter/app/common/list/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/d;->f()V

    .line 801
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->onDestroy()V

    .line 802
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 806
    invoke-super {p0, p1}, Lcom/twitter/app/common/abs/AbsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 807
    const-string/jumbo v0, "scroll_position"

    iget-object v1, p0, Lcom/twitter/app/common/list/TwitterListFragment;->f:Lcnl;

    invoke-interface {v1}, Lcnl;->a()Lcnk;

    move-result-object v1

    sget-object v2, Lcnk;->b:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1, v2}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/os/Bundle;

    .line 809
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 282
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->onStop()V

    .line 283
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->W:Lcom/twitter/android/av/j;

    invoke-virtual {v0}, Lcom/twitter/android/av/j;->d()V

    .line 284
    return-void
.end method

.method protected p()V
    .locals 4

    .prologue
    .line 322
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->n:Laoy;

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->n:Laoy;

    iget-object v1, p0, Lcom/twitter/app/common/list/TwitterListFragment;->o:Laow;

    invoke-interface {v0, v1}, Laoy;->a(Laow;)V

    .line 327
    :goto_0
    return-void

    .line 325
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/app/common/list/TwitterListFragment;->e:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0
.end method

.method protected q_()V
    .locals 1

    .prologue
    .line 291
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aD()V

    .line 293
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment;->W:Lcom/twitter/android/av/j;

    invoke-virtual {v0}, Lcom/twitter/android/av/j;->c()V

    .line 294
    invoke-direct {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->f()Lcom/twitter/app/common/list/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/d;->e()V

    .line 295
    invoke-super {p0}, Lcom/twitter/app/common/abs/AbsFragment;->q_()V

    .line 296
    return-void
.end method

.method public r_()V
    .locals 0

    .prologue
    .line 491
    return-void
.end method

.method public s_()V
    .locals 0

    .prologue
    .line 496
    return-void
.end method

.method public t_()V
    .locals 0

    .prologue
    .line 501
    return-void
.end method

.method public u_()V
    .locals 0

    .prologue
    .line 505
    invoke-virtual {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->L()V

    .line 506
    return-void
.end method
