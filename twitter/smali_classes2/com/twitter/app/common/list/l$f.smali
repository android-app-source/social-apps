.class Lcom/twitter/app/common/list/l$f;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/refresh/widget/RefreshableListView$e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/common/list/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "f"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/common/list/l;

.field private b:Landroid/view/View;


# direct methods
.method private constructor <init>(Lcom/twitter/app/common/list/l;)V
    .locals 0

    .prologue
    .line 857
    iput-object p1, p0, Lcom/twitter/app/common/list/l$f;->a:Lcom/twitter/app/common/list/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/app/common/list/l;Lcom/twitter/app/common/list/l$1;)V
    .locals 0

    .prologue
    .line 857
    invoke-direct {p0, p1}, Lcom/twitter/app/common/list/l$f;-><init>(Lcom/twitter/app/common/list/l;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 879
    iget-object v0, p0, Lcom/twitter/app/common/list/l$f;->a:Lcom/twitter/app/common/list/l;

    invoke-static {v0}, Lcom/twitter/app/common/list/l;->d(Lcom/twitter/app/common/list/l;)Lcom/twitter/refresh/widget/RefreshableListView$e;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 880
    iget-object v0, p0, Lcom/twitter/app/common/list/l$f;->a:Lcom/twitter/app/common/list/l;

    invoke-static {v0}, Lcom/twitter/app/common/list/l;->d(Lcom/twitter/app/common/list/l;)Lcom/twitter/refresh/widget/RefreshableListView$e;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/refresh/widget/RefreshableListView$e;->a()V

    .line 882
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 886
    iget-object v0, p0, Lcom/twitter/app/common/list/l$f;->a:Lcom/twitter/app/common/list/l;

    invoke-static {v0}, Lcom/twitter/app/common/list/l;->d(Lcom/twitter/app/common/list/l;)Lcom/twitter/refresh/widget/RefreshableListView$e;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 887
    iget-object v0, p0, Lcom/twitter/app/common/list/l$f;->a:Lcom/twitter/app/common/list/l;

    invoke-static {v0}, Lcom/twitter/app/common/list/l;->d(Lcom/twitter/app/common/list/l;)Lcom/twitter/refresh/widget/RefreshableListView$e;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/refresh/widget/RefreshableListView$e;->a()V

    .line 889
    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 862
    iget-object v0, p0, Lcom/twitter/app/common/list/l$f;->a:Lcom/twitter/app/common/list/l;

    invoke-static {v0}, Lcom/twitter/app/common/list/l;->d(Lcom/twitter/app/common/list/l;)Lcom/twitter/refresh/widget/RefreshableListView$e;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 863
    iget-object v0, p0, Lcom/twitter/app/common/list/l$f;->a:Lcom/twitter/app/common/list/l;

    invoke-static {v0}, Lcom/twitter/app/common/list/l;->d(Lcom/twitter/app/common/list/l;)Lcom/twitter/refresh/widget/RefreshableListView$e;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/refresh/widget/RefreshableListView$e;->b()V

    .line 865
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/list/l$f;->a:Lcom/twitter/app/common/list/l;

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    .line 866
    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 867
    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    if-eqz v0, :cond_2

    .line 868
    iget-object v0, p0, Lcom/twitter/app/common/list/l$f;->b:Landroid/view/View;

    if-nez v0, :cond_1

    .line 869
    iget-object v0, p0, Lcom/twitter/app/common/list/l$f;->a:Lcom/twitter/app/common/list/l;

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->aN_()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f130797

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/list/l$f;->b:Landroid/view/View;

    .line 871
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/common/list/l$f;->b:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 872
    iget-object v0, p0, Lcom/twitter/app/common/list/l$f;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 875
    :cond_2
    return-void
.end method
