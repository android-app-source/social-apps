.class public Lcom/twitter/app/common/list/l$d;
.super Laod;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/common/list/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/app/common/list/l$d",
        "<TT;>;>",
        "Laod;"
    }
.end annotation


# instance fields
.field private b:Ljava/lang/String;

.field private c:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field private d:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field private e:I
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field

.field private f:I
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field

.field private g:I
    .annotation build Landroid/support/annotation/IdRes;
    .end annotation
.end field

.field private h:I
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field

.field private i:I
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field

.field private j:I
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field

.field private k:Z

.field private l:I

.field private m:I

.field private n:I

.field private o:Z

.field private p:Lcom/twitter/app/common/list/h;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 641
    invoke-direct {p0}, Laod;-><init>()V

    .line 642
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/app/common/list/l$d;->b:Ljava/lang/String;

    .line 660
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/common/list/l$d;->k:Z

    .line 665
    sget-object v0, Lcom/twitter/app/common/list/h;->a:Lcom/twitter/app/common/list/h;

    iput-object v0, p0, Lcom/twitter/app/common/list/l$d;->p:Lcom/twitter/app/common/list/h;

    return-void
.end method


# virtual methods
.method public a(I)Lcom/twitter/app/common/list/l$d;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 686
    iput p1, p0, Lcom/twitter/app/common/list/l$d;->c:I

    .line 687
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/l$d;

    return-object v0
.end method

.method public a(Lcom/twitter/app/common/list/h;)Lcom/twitter/app/common/list/l$d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/app/common/list/h;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 825
    iput-object p1, p0, Lcom/twitter/app/common/list/l$d;->p:Lcom/twitter/app/common/list/h;

    .line 826
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/l$d;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/app/common/list/l$d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 675
    iput-object p1, p0, Lcom/twitter/app/common/list/l$d;->b:Ljava/lang/String;

    .line 676
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/l$d;

    return-object v0
.end method

.method public a(Z)Lcom/twitter/app/common/list/l$d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TT;"
        }
    .end annotation

    .prologue
    .line 771
    iput-boolean p1, p0, Lcom/twitter/app/common/list/l$d;->k:Z

    .line 772
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/l$d;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 670
    iget-object v0, p0, Lcom/twitter/app/common/list/l$d;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b()I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 681
    iget v0, p0, Lcom/twitter/app/common/list/l$d;->c:I

    return v0
.end method

.method public b(I)Lcom/twitter/app/common/list/l$d;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 697
    iput p1, p0, Lcom/twitter/app/common/list/l$d;->d:I

    .line 698
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/l$d;

    return-object v0
.end method

.method public b(Z)Lcom/twitter/app/common/list/l$d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TT;"
        }
    .end annotation

    .prologue
    .line 814
    iput-boolean p1, p0, Lcom/twitter/app/common/list/l$d;->o:Z

    .line 815
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/l$d;

    return-object v0
.end method

.method public c()I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 692
    iget v0, p0, Lcom/twitter/app/common/list/l$d;->d:I

    return v0
.end method

.method public c(I)Lcom/twitter/app/common/list/l$d;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 712
    iput p1, p0, Lcom/twitter/app/common/list/l$d;->e:I

    .line 713
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/l$d;

    return-object v0
.end method

.method public d(I)Lcom/twitter/app/common/list/l$d;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 723
    iput p1, p0, Lcom/twitter/app/common/list/l$d;->f:I

    .line 724
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/l$d;

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 702
    iget v0, p0, Lcom/twitter/app/common/list/l$d;->c:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/app/common/list/l$d;->d:I

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 707
    iget v0, p0, Lcom/twitter/app/common/list/l$d;->e:I

    return v0
.end method

.method public e(I)Lcom/twitter/app/common/list/l$d;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 734
    iput p1, p0, Lcom/twitter/app/common/list/l$d;->g:I

    .line 735
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/l$d;

    return-object v0
.end method

.method public f()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 718
    iget v0, p0, Lcom/twitter/app/common/list/l$d;->f:I

    return v0
.end method

.method public f(I)Lcom/twitter/app/common/list/l$d;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 745
    iput p1, p0, Lcom/twitter/app/common/list/l$d;->h:I

    .line 746
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/l$d;

    return-object v0
.end method

.method public g()I
    .locals 1
    .annotation build Landroid/support/annotation/IdRes;
    .end annotation

    .prologue
    .line 729
    iget v0, p0, Lcom/twitter/app/common/list/l$d;->g:I

    return v0
.end method

.method public g(I)V
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    .line 754
    iput p1, p0, Lcom/twitter/app/common/list/l$d;->i:I

    .line 755
    return-void
.end method

.method public h()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 740
    iget v0, p0, Lcom/twitter/app/common/list/l$d;->h:I

    return v0
.end method

.method public h(I)V
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    .line 762
    iput p1, p0, Lcom/twitter/app/common/list/l$d;->j:I

    .line 763
    return-void
.end method

.method public i()I
    .locals 1

    .prologue
    .line 750
    iget v0, p0, Lcom/twitter/app/common/list/l$d;->i:I

    return v0
.end method

.method public i(I)Lcom/twitter/app/common/list/l$d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 784
    iput p1, p0, Lcom/twitter/app/common/list/l$d;->l:I

    .line 785
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/l$d;

    return-object v0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 758
    iget v0, p0, Lcom/twitter/app/common/list/l$d;->j:I

    return v0
.end method

.method public j(I)Lcom/twitter/app/common/list/l$d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 794
    iput p1, p0, Lcom/twitter/app/common/list/l$d;->m:I

    .line 795
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/l$d;

    return-object v0
.end method

.method public k(I)Lcom/twitter/app/common/list/l$d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 804
    iput p1, p0, Lcom/twitter/app/common/list/l$d;->n:I

    .line 805
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/l$d;

    return-object v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 766
    iget-boolean v0, p0, Lcom/twitter/app/common/list/l$d;->k:Z

    return v0
.end method

.method public l()I
    .locals 1

    .prologue
    .line 776
    iget v0, p0, Lcom/twitter/app/common/list/l$d;->l:I

    return v0
.end method

.method public m()I
    .locals 1

    .prologue
    .line 789
    iget v0, p0, Lcom/twitter/app/common/list/l$d;->m:I

    return v0
.end method

.method public n()I
    .locals 1

    .prologue
    .line 799
    iget v0, p0, Lcom/twitter/app/common/list/l$d;->n:I

    return v0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 809
    iget-boolean v0, p0, Lcom/twitter/app/common/list/l$d;->o:Z

    return v0
.end method

.method public p()Lcom/twitter/app/common/list/h;
    .locals 1

    .prologue
    .line 820
    iget-object v0, p0, Lcom/twitter/app/common/list/l$d;->p:Lcom/twitter/app/common/list/h;

    return-object v0
.end method
