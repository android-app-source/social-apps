.class public final Lcom/twitter/app/common/list/m;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "A:",
        "Lcjr",
        "<TT;>;>",
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lcom/twitter/app/common/list/l",
        "<TT;TA;>;>;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcsd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcsd",
            "<",
            "Lcom/twitter/app/common/list/l",
            "<TT;TA;>;>;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/list/l$d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/twitter/app/common/list/m;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/app/common/list/m;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcsd;Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcsd",
            "<",
            "Lcom/twitter/app/common/list/l",
            "<TT;TA;>;>;",
            "Lcta",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/common/list/l$d;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    sget-boolean v0, Lcom/twitter/app/common/list/m;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 28
    :cond_0
    iput-object p1, p0, Lcom/twitter/app/common/list/m;->b:Lcsd;

    .line 29
    sget-boolean v0, Lcom/twitter/app/common/list/m;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 30
    :cond_1
    iput-object p2, p0, Lcom/twitter/app/common/list/m;->c:Lcta;

    .line 31
    sget-boolean v0, Lcom/twitter/app/common/list/m;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 32
    :cond_2
    iput-object p3, p0, Lcom/twitter/app/common/list/m;->d:Lcta;

    .line 33
    return-void
.end method

.method public static a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "A:",
            "Lcjr",
            "<TT;>;>(",
            "Lcsd",
            "<",
            "Lcom/twitter/app/common/list/l",
            "<TT;TA;>;>;",
            "Lcta",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/common/list/l$d;",
            ">;)",
            "Ldagger/internal/c",
            "<",
            "Lcom/twitter/app/common/list/l",
            "<TT;TA;>;>;"
        }
    .end annotation

    .prologue
    .line 46
    new-instance v0, Lcom/twitter/app/common/list/m;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/app/common/list/m;-><init>(Lcsd;Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/app/common/list/l;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/app/common/list/l",
            "<TT;TA;>;"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v2, p0, Lcom/twitter/app/common/list/m;->b:Lcsd;

    new-instance v3, Lcom/twitter/app/common/list/l;

    iget-object v0, p0, Lcom/twitter/app/common/list/m;->c:Lcta;

    .line 39
    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/twitter/app/common/list/m;->d:Lcta;

    invoke-interface {v1}, Lcta;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/app/common/list/l$d;

    invoke-direct {v3, v0, v1}, Lcom/twitter/app/common/list/l;-><init>(Landroid/view/LayoutInflater;Lcom/twitter/app/common/list/l$d;)V

    .line 37
    invoke-static {v2, v3}, Ldagger/internal/MembersInjectors;->a(Lcsd;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/l;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/twitter/app/common/list/m;->a()Lcom/twitter/app/common/list/l;

    move-result-object v0

    return-object v0
.end method
