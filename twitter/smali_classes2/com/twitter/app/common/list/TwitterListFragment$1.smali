.class Lcom/twitter/app/common/list/TwitterListFragment$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/common/list/TwitterListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/common/list/TwitterListFragment;


# direct methods
.method constructor <init>(Lcom/twitter/app/common/list/TwitterListFragment;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/twitter/app/common/list/TwitterListFragment$1;->a:Lcom/twitter/app/common/list/TwitterListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 115
    if-nez p2, :cond_0

    .line 116
    const/4 v0, 0x0

    .line 121
    :goto_0
    iget-object v1, p0, Lcom/twitter/app/common/list/TwitterListFragment$1;->a:Lcom/twitter/app/common/list/TwitterListFragment;

    invoke-static {v1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/app/common/list/TwitterListFragment;)Lcom/twitter/app/common/list/d;

    move-result-object v1

    .line 122
    invoke-virtual {v1}, Lcom/twitter/app/common/list/d;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 123
    invoke-virtual {v1, v0}, Lcom/twitter/app/common/list/d;->a(Lcbi;)V

    .line 131
    :goto_1
    return-void

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment$1;->a:Lcom/twitter/app/common/list/TwitterListFragment;

    invoke-static {v0, p2}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/app/common/list/TwitterListFragment;Landroid/database/Cursor;)Lcbi;

    move-result-object v0

    goto :goto_0

    .line 125
    :cond_1
    if-nez v0, :cond_2

    .line 126
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment$1;->a:Lcom/twitter/app/common/list/TwitterListFragment;

    invoke-virtual {v0}, Lcom/twitter/app/common/list/TwitterListFragment;->L()V

    goto :goto_1

    .line 128
    :cond_2
    iget-object v1, p0, Lcom/twitter/app/common/list/TwitterListFragment$1;->a:Lcom/twitter/app/common/list/TwitterListFragment;

    invoke-virtual {v1, v0}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcbi;)V

    goto :goto_1
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment$1;->a:Lcom/twitter/app/common/list/TwitterListFragment;

    invoke-virtual {v0}, Lcom/twitter/app/common/list/TwitterListFragment;->j()Landroid/support/v4/content/Loader;

    move-result-object v0

    return-object v0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 106
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/common/list/TwitterListFragment$1;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment$1;->a:Lcom/twitter/app/common/list/TwitterListFragment;

    invoke-static {v0}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/app/common/list/TwitterListFragment;)Lcom/twitter/app/common/list/d;

    move-result-object v0

    .line 136
    invoke-virtual {v0}, Lcom/twitter/app/common/list/d;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/d;->a(Lcbi;)V

    .line 141
    :goto_0
    return-void

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/list/TwitterListFragment$1;->a:Lcom/twitter/app/common/list/TwitterListFragment;

    invoke-virtual {v0}, Lcom/twitter/app/common/list/TwitterListFragment;->as()V

    goto :goto_0
.end method
