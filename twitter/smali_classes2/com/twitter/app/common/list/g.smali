.class public final Lcom/twitter/app/common/list/g;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/list/k;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/common/list/g$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laod;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/list/l$d;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/list/l",
            "<",
            "Ljava/lang/Object;",
            "Lcjr",
            "<",
            "Ljava/lang/Object;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/twitter/app/common/list/g;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/app/common/list/g;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/twitter/app/common/list/g$a;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    sget-boolean v0, Lcom/twitter/app/common/list/g;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 42
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/app/common/list/g;->a(Lcom/twitter/app/common/list/g$a;)V

    .line 43
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/app/common/list/g$a;Lcom/twitter/app/common/list/g$1;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/twitter/app/common/list/g;-><init>(Lcom/twitter/app/common/list/g$a;)V

    return-void
.end method

.method private a(Lcom/twitter/app/common/list/g$a;)V
    .locals 3

    .prologue
    .line 52
    .line 54
    invoke-static {p1}, Lcom/twitter/app/common/list/g$a;->a(Lcom/twitter/app/common/list/g$a;)Lant;

    move-result-object v0

    invoke-static {v0}, Lanu;->a(Lant;)Ldagger/internal/c;

    move-result-object v0

    .line 53
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/list/g;->b:Lcta;

    .line 56
    iget-object v0, p0, Lcom/twitter/app/common/list/g;->b:Lcta;

    .line 58
    invoke-static {v0}, Lanx;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 57
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/list/g;->c:Lcta;

    .line 62
    invoke-static {p1}, Lcom/twitter/app/common/list/g$a;->a(Lcom/twitter/app/common/list/g$a;)Lant;

    move-result-object v0

    invoke-static {v0}, Laoc;->a(Lant;)Ldagger/internal/c;

    move-result-object v0

    .line 61
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/list/g;->d:Lcta;

    .line 64
    iget-object v0, p0, Lcom/twitter/app/common/list/g;->d:Lcta;

    .line 65
    invoke-static {v0}, Laoj;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/list/g;->e:Lcta;

    .line 70
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/common/list/g;->c:Lcta;

    iget-object v2, p0, Lcom/twitter/app/common/list/g;->e:Lcta;

    .line 69
    invoke-static {v0, v1, v2}, Lcom/twitter/app/common/list/m;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 68
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/list/g;->f:Lcta;

    .line 74
    iget-object v0, p0, Lcom/twitter/app/common/list/g;->f:Lcta;

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/list/g;->g:Lcta;

    .line 75
    return-void
.end method

.method public static c()Lcom/twitter/app/common/list/g$a;
    .locals 2

    .prologue
    .line 46
    new-instance v0, Lcom/twitter/app/common/list/g$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/app/common/list/g$a;-><init>(Lcom/twitter/app/common/list/g$1;)V

    return-object v0
.end method


# virtual methods
.method public a()Laog;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/app/common/list/g;->g:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laog;

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    invoke-static {}, Ldagger/internal/f;->a()Ldagger/internal/c;

    move-result-object v0

    invoke-interface {v0}, Ldagger/internal/c;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method
