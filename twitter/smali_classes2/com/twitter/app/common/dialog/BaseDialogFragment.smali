.class public Lcom/twitter/app/common/dialog/BaseDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/base/e;
.implements Lcom/twitter/app/common/util/k;


# instance fields
.field private final a:Lcwv;

.field protected b:Landroid/content/Context;

.field private final c:Lcom/twitter/app/common/util/e;

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Lcom/twitter/app/common/dialog/b$a;

.field private h:Lcom/twitter/app/common/dialog/b$b;

.field private i:Lcom/twitter/app/common/dialog/b$c;

.field private j:Lcom/twitter/app/common/dialog/b$d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 54
    new-instance v0, Lcwv;

    invoke-direct {v0}, Lcwv;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a:Lcwv;

    .line 55
    new-instance v0, Lcom/twitter/app/common/util/e;

    invoke-direct {v0}, Lcom/twitter/app/common/util/e;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->c:Lcom/twitter/app/common/util/e;

    return-void
.end method

.method protected static varargs a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<",
            "L:Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<T",
            "L;",
            ">;[",
            "Ljava/lang/Object;",
            ")T",
            "L;"
        }
    .end annotation

    .prologue
    .line 328
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p1, v0

    .line 329
    invoke-virtual {p0, v2}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 330
    invoke-static {v2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 333
    :goto_1
    return-object v0

    .line 328
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 333
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 450
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c()Lcom/twitter/app/common/base/e;
    .locals 2

    .prologue
    .line 438
    invoke-virtual {p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 439
    instance-of v1, v0, Lcom/twitter/app/common/base/e;

    if-eqz v1, :cond_0

    .line 440
    check-cast v0, Lcom/twitter/app/common/base/e;

    return-object v0

    .line 441
    :cond_0
    if-nez v0, :cond_1

    .line 442
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "The fragment is not attached."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 444
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "The parent activity does not implement Retainer."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/twitter/app/common/dialog/BaseDialogFragment;",
            ">(",
            "Landroid/support/v4/app/Fragment;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 223
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 224
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/dialog/BaseDialogFragment;

    return-object v0
.end method

.method public a(Lcom/twitter/app/common/dialog/b$a;)Lcom/twitter/app/common/dialog/BaseDialogFragment;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/twitter/app/common/dialog/BaseDialogFragment;",
            ">(",
            "Lcom/twitter/app/common/dialog/b$a;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 201
    iput-object p1, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->g:Lcom/twitter/app/common/dialog/b$a;

    .line 202
    new-instance v0, Lcom/twitter/app/common/dialog/a$a;

    invoke-virtual {p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->d()Lcom/twitter/app/common/dialog/a;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/app/common/dialog/a$a;-><init>(Lcom/twitter/app/common/dialog/a;)V

    invoke-virtual {v0}, Lcom/twitter/app/common/dialog/a$a;->f()Lcom/twitter/app/common/dialog/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/dialog/a$a;->e()Lcom/twitter/app/common/dialog/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/a;)V

    .line 203
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/dialog/BaseDialogFragment;

    return-object v0
.end method

.method public a(Lcom/twitter/app/common/dialog/b$b;)Lcom/twitter/app/common/dialog/BaseDialogFragment;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/twitter/app/common/dialog/BaseDialogFragment;",
            ">(",
            "Lcom/twitter/app/common/dialog/b$b;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 186
    iput-object p1, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->h:Lcom/twitter/app/common/dialog/b$b;

    .line 187
    new-instance v0, Lcom/twitter/app/common/dialog/a$a;

    invoke-virtual {p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->d()Lcom/twitter/app/common/dialog/a;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/app/common/dialog/a$a;-><init>(Lcom/twitter/app/common/dialog/a;)V

    .line 188
    invoke-virtual {v0}, Lcom/twitter/app/common/dialog/a$a;->h()Lcom/twitter/app/common/dialog/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/dialog/a$a;->e()Lcom/twitter/app/common/dialog/a;

    move-result-object v0

    .line 187
    invoke-virtual {p0, v0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/a;)V

    .line 189
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/dialog/BaseDialogFragment;

    return-object v0
.end method

.method public a(Lcom/twitter/app/common/dialog/b$c;)Lcom/twitter/app/common/dialog/BaseDialogFragment;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/twitter/app/common/dialog/BaseDialogFragment;",
            ">(",
            "Lcom/twitter/app/common/dialog/b$c;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 215
    iput-object p1, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->i:Lcom/twitter/app/common/dialog/b$c;

    .line 216
    new-instance v0, Lcom/twitter/app/common/dialog/a$a;

    invoke-virtual {p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->d()Lcom/twitter/app/common/dialog/a;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/app/common/dialog/a$a;-><init>(Lcom/twitter/app/common/dialog/a;)V

    .line 217
    invoke-virtual {v0}, Lcom/twitter/app/common/dialog/a$a;->g()Lcom/twitter/app/common/dialog/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/dialog/a$a;->e()Lcom/twitter/app/common/dialog/a;

    move-result-object v0

    .line 216
    invoke-virtual {p0, v0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/a;)V

    .line 218
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/dialog/BaseDialogFragment;

    return-object v0
.end method

.method public a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/twitter/app/common/dialog/BaseDialogFragment;",
            ">(",
            "Lcom/twitter/app/common/dialog/b$d;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 179
    iput-object p1, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->j:Lcom/twitter/app/common/dialog/b$d;

    .line 180
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/dialog/BaseDialogFragment;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 408
    invoke-direct {p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->c()Lcom/twitter/app/common/base/e;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/twitter/app/common/base/e;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/support/v4/app/FragmentManager;)V
    .locals 1

    .prologue
    .line 275
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 276
    return-void
.end method

.method protected a(Lcom/twitter/app/common/dialog/a;)V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p1, Lcom/twitter/app/common/dialog/a;->b:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 174
    return-void
.end method

.method public a(Lcom/twitter/app/common/util/d;)V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->c:Lcom/twitter/app/common/util/e;

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/util/e;->a(Lcom/twitter/app/common/util/d;)V

    .line 159
    return-void
.end method

.method protected final b(I)Landroid/view/View;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param

    .prologue
    .line 236
    invoke-virtual {p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string/jumbo v1, "Dialog has not been created yet."

    invoke-static {v0, v1}, Lcom/twitter/util/g;->a(ZLjava/lang/String;)Z

    .line 237
    invoke-virtual {p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 236
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcom/twitter/app/common/util/d;)V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->c:Lcom/twitter/app/common/util/e;

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/util/e;->b(Lcom/twitter/app/common/util/d;)Z

    .line 164
    return-void
.end method

.method public b_(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 419
    invoke-direct {p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->c()Lcom/twitter/app/common/base/e;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/app/common/base/e;->b_(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final c(I)V
    .locals 3

    .prologue
    .line 264
    iget-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->j:Lcom/twitter/app/common/dialog/b$d;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->j:Lcom/twitter/app/common/dialog/b$d;

    invoke-virtual {p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->d()Lcom/twitter/app/common/dialog/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/app/common/dialog/a;->J()I

    move-result v2

    invoke-interface {v0, v1, v2, p1}, Lcom/twitter/app/common/dialog/b$d;->a(Landroid/content/DialogInterface;II)V

    .line 267
    :cond_0
    return-void
.end method

.method public d()Lcom/twitter/app/common/dialog/a;
    .locals 1

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/common/dialog/a;->d(Landroid/os/Bundle;)Lcom/twitter/app/common/dialog/a;

    move-result-object v0

    return-object v0
.end method

.method public f_()Z
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->d:Z

    return v0
.end method

.method public g_()Z
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->e:Z

    return v0
.end method

.method public isDestroyed()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->f:Z

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 368
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 369
    iget-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->h:Lcom/twitter/app/common/dialog/b$b;

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->h:Lcom/twitter/app/common/dialog/b$b;

    invoke-virtual {p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->d()Lcom/twitter/app/common/dialog/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/app/common/dialog/a;->J()I

    move-result v2

    invoke-interface {v0, v1, v2, p1}, Lcom/twitter/app/common/dialog/b$b;->a(Landroid/app/Dialog;ILandroid/os/Bundle;)V

    .line 372
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 7
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 299
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 300
    invoke-virtual {p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 301
    invoke-virtual {p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->d()Lcom/twitter/app/common/dialog/a;

    move-result-object v2

    .line 302
    iget-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->j:Lcom/twitter/app/common/dialog/b$d;

    if-nez v0, :cond_0

    .line 303
    const-class v0, Lcom/twitter/app/common/dialog/b$d;

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v1, v3, v4

    aput-object p1, v3, v5

    invoke-static {v0, v3}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/dialog/b$d;

    iput-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->j:Lcom/twitter/app/common/dialog/b$d;

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->g:Lcom/twitter/app/common/dialog/b$a;

    if-nez v0, :cond_1

    invoke-virtual {v2}, Lcom/twitter/app/common/dialog/a;->K()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 307
    const-class v0, Lcom/twitter/app/common/dialog/b$a;

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v1, v3, v4

    aput-object p1, v3, v5

    invoke-static {v0, v3}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/dialog/b$a;

    iput-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->g:Lcom/twitter/app/common/dialog/b$a;

    .line 310
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->i:Lcom/twitter/app/common/dialog/b$c;

    if-nez v0, :cond_2

    invoke-virtual {v2}, Lcom/twitter/app/common/dialog/a;->L()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 311
    const-class v0, Lcom/twitter/app/common/dialog/b$c;

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v1, v3, v4

    aput-object p1, v3, v5

    invoke-static {v0, v3}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/dialog/b$c;

    iput-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->i:Lcom/twitter/app/common/dialog/b$c;

    .line 314
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->h:Lcom/twitter/app/common/dialog/b$b;

    if-nez v0, :cond_3

    invoke-virtual {v2}, Lcom/twitter/app/common/dialog/a;->M()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 315
    const-class v0, Lcom/twitter/app/common/dialog/b$b;

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v1, v2, v4

    aput-object p1, v2, v5

    invoke-static {v0, v2}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/dialog/b$b;

    iput-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->h:Lcom/twitter/app/common/dialog/b$b;

    .line 318
    :cond_3
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 381
    iget-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->g:Lcom/twitter/app/common/dialog/b$a;

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->g:Lcom/twitter/app/common/dialog/b$a;

    invoke-virtual {p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->d()Lcom/twitter/app/common/dialog/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/app/common/dialog/a;->J()I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/twitter/app/common/dialog/b$a;->a(Landroid/content/DialogInterface;I)V

    .line 384
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 385
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 106
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 107
    iget-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->c:Lcom/twitter/app/common/util/e;

    invoke-virtual {v0, p0, p1}, Lcom/twitter/app/common/util/e;->a(Landroid/support/v4/app/Fragment;Landroid/content/res/Configuration;)V

    .line 108
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->c:Lcom/twitter/app/common/util/e;

    invoke-virtual {v0, p0, p1}, Lcom/twitter/app/common/util/e;->a(Landroid/support/v4/app/Fragment;Landroid/os/Bundle;)V

    .line 74
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 75
    invoke-virtual {p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->b:Landroid/content/Context;

    .line 77
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 350
    new-instance v0, Landroid/app/Dialog;

    invoke-virtual {p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->getTheme()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 351
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 352
    invoke-virtual {v0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    sget v3, Lakl$a;->dialogLayoutId:I

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 353
    iget v2, v1, Landroid/util/TypedValue;->resourceId:I

    if-eqz v2, :cond_0

    .line 354
    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(I)V

    .line 356
    :cond_0
    return-object v0
.end method

.method public onDestroy()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a:Lcwv;

    invoke-virtual {v0}, Lcwv;->B_()V

    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->f:Z

    .line 130
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDestroy()V

    .line 131
    iget-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->c:Lcom/twitter/app/common/util/e;

    invoke-virtual {v0, p0}, Lcom/twitter/app/common/util/e;->g(Landroid/support/v4/app/Fragment;)V

    .line 132
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 394
    iget-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->i:Lcom/twitter/app/common/dialog/b$c;

    if-eqz v0, :cond_0

    .line 395
    iget-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->i:Lcom/twitter/app/common/dialog/b$c;

    invoke-virtual {p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->d()Lcom/twitter/app/common/dialog/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/app/common/dialog/a;->J()I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/twitter/app/common/dialog/b$c;->a(Landroid/content/DialogInterface;I)V

    .line 397
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 398
    return-void
.end method

.method public onPause()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->e:Z

    .line 99
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onPause()V

    .line 100
    iget-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->c:Lcom/twitter/app/common/util/e;

    invoke-virtual {v0, p0}, Lcom/twitter/app/common/util/e;->e(Landroid/support/v4/app/Fragment;)V

    .line 101
    return-void
.end method

.method public onResume()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->c:Lcom/twitter/app/common/util/e;

    invoke-virtual {v0, p0}, Lcom/twitter/app/common/util/e;->b(Landroid/support/v4/app/Fragment;)V

    .line 91
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onResume()V

    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->e:Z

    .line 93
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 121
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 122
    iget-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->c:Lcom/twitter/app/common/util/e;

    invoke-virtual {v0, p0, p1}, Lcom/twitter/app/common/util/e;->b(Landroid/support/v4/app/Fragment;Landroid/os/Bundle;)V

    .line 123
    return-void
.end method

.method public onStart()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->c:Lcom/twitter/app/common/util/e;

    invoke-virtual {v0, p0}, Lcom/twitter/app/common/util/e;->a(Landroid/support/v4/app/Fragment;)V

    .line 83
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onStart()V

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->d:Z

    .line 85
    return-void
.end method

.method public onStop()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 113
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->d:Z

    .line 114
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onStop()V

    .line 115
    iget-object v0, p0, Lcom/twitter/app/common/dialog/BaseDialogFragment;->c:Lcom/twitter/app/common/util/e;

    invoke-virtual {v0, p0}, Lcom/twitter/app/common/util/e;->f(Landroid/support/v4/app/Fragment;)V

    .line 116
    return-void
.end method

.method public show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 281
    :try_start_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 287
    :goto_0
    return-void

    .line 282
    :catch_0
    move-exception v0

    .line 285
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
