.class public abstract Lcom/twitter/app/common/timeline/c;
.super Lcom/twitter/app/common/list/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/common/timeline/c$a;
    }
.end annotation


# instance fields
.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Lcom/twitter/model/core/TwitterUser;


# direct methods
.method protected constructor <init>(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/twitter/app/common/list/i;-><init>(Landroid/os/Bundle;)V

    .line 29
    iget-object v0, p0, Lcom/twitter/app/common/timeline/c;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "timeline_arg_timeline_tag"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/timeline/c;->e:Ljava/lang/String;

    .line 30
    iget-object v0, p0, Lcom/twitter/app/common/timeline/c;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "timeline_arg_scribe_section"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/timeline/c;->f:Ljava/lang/String;

    .line 31
    iget-object v0, p0, Lcom/twitter/app/common/timeline/c;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "timeline_arg_scribe_page"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/timeline/c;->g:Ljava/lang/String;

    .line 32
    iget-object v0, p0, Lcom/twitter/app/common/timeline/c;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "timeline_arg_profile_user"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    iput-object v0, p0, Lcom/twitter/app/common/timeline/c;->h:Lcom/twitter/model/core/TwitterUser;

    .line 33
    return-void
.end method


# virtual methods
.method public abstract a()Z
.end method

.method public abstract b()Lbgw;
.end method

.method public abstract c()I
.end method

.method public abstract d()I
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method
