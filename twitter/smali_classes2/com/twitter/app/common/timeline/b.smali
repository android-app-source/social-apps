.class public final Lcom/twitter/app/common/timeline/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/timeline/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/common/timeline/b$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcnz;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/metrics/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/twitter/app/common/timeline/b;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/app/common/timeline/b;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/twitter/app/common/timeline/b$a;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    sget-boolean v0, Lcom/twitter/app/common/timeline/b;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 29
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/app/common/timeline/b;->a(Lcom/twitter/app/common/timeline/b$a;)V

    .line 30
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/app/common/timeline/b$a;Lcom/twitter/app/common/timeline/b$1;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/twitter/app/common/timeline/b;-><init>(Lcom/twitter/app/common/timeline/b$a;)V

    return-void
.end method

.method private a(Lcom/twitter/app/common/timeline/b$a;)V
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/twitter/app/common/timeline/b$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/app/common/timeline/b$1;-><init>(Lcom/twitter/app/common/timeline/b;Lcom/twitter/app/common/timeline/b$a;)V

    iput-object v0, p0, Lcom/twitter/app/common/timeline/b;->b:Lcta;

    .line 52
    iget-object v0, p0, Lcom/twitter/app/common/timeline/b;->b:Lcta;

    .line 54
    invoke-static {v0}, Lapd;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 53
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/common/timeline/b;->c:Lcta;

    .line 56
    return-void
.end method

.method public static c()Lcom/twitter/app/common/timeline/b$a;
    .locals 2

    .prologue
    .line 33
    new-instance v0, Lcom/twitter/app/common/timeline/b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/app/common/timeline/b$a;-><init>(Lcom/twitter/app/common/timeline/b$1;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/android/metrics/b;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/twitter/app/common/timeline/b;->c:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/metrics/b;

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    invoke-static {}, Ldagger/internal/f;->a()Ldagger/internal/c;

    move-result-object v0

    invoke-interface {v0}, Ldagger/internal/c;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method
