.class public Lcom/twitter/app/common/timeline/TimelineFragment$e;
.super Lcom/twitter/android/ct;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/common/timeline/TimelineFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "e"
.end annotation


# instance fields
.field private a:Landroid/view/animation/TranslateAnimation;

.field private final k:J

.field private final l:Lcom/twitter/android/timeline/be;

.field private final m:I

.field private n:Lcom/twitter/android/util/v;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Lcom/twitter/android/ck;JLcom/twitter/android/timeline/be;I)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1557
    invoke-static {v3, p3, v3, v3}, Lbxc;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbxc;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/ct;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Lbxc;Lcom/twitter/android/ck;)V

    .line 1559
    iput-wide p5, p0, Lcom/twitter/app/common/timeline/TimelineFragment$e;->k:J

    .line 1560
    iput-object p7, p0, Lcom/twitter/app/common/timeline/TimelineFragment$e;->l:Lcom/twitter/android/timeline/be;

    .line 1561
    iput p8, p0, Lcom/twitter/app/common/timeline/TimelineFragment$e;->m:I

    .line 1562
    return-void
.end method


# virtual methods
.method public a(Lbxg;)V
    .locals 4

    .prologue
    .line 1568
    invoke-virtual {p1}, Lbxg;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/twitter/app/common/timeline/TimelineFragment$e;->k:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1569
    invoke-super {p0, p1}, Lcom/twitter/android/ct;->a(Lbxg;)V

    .line 1573
    :goto_0
    return-void

    .line 1571
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment$e;->b()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/util/v;)V
    .locals 0

    .prologue
    .line 1633
    iput-object p1, p0, Lcom/twitter/app/common/timeline/TimelineFragment$e;->n:Lcom/twitter/android/util/v;

    .line 1634
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/q;)V
    .locals 4

    .prologue
    .line 1577
    iget-wide v0, p2, Lcom/twitter/model/core/q;->c:J

    iget-wide v2, p0, Lcom/twitter/app/common/timeline/TimelineFragment$e;->k:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1578
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ct;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/q;)V

    .line 1582
    :goto_0
    return-void

    .line 1580
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/common/timeline/TimelineFragment$e;->b()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/media/EditableMedia;Lcom/twitter/library/widget/TweetView;)V
    .locals 1

    .prologue
    .line 1623
    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->y:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1624
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment$e;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 1625
    if-eqz v0, :cond_0

    .line 1627
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 1626
    invoke-static {v0, p1}, Lcom/twitter/android/widget/ConfirmCancelPendingTweetDialog;->a(Landroid/support/v4/app/FragmentManager;Lcom/twitter/model/core/Tweet;)Lcom/twitter/android/widget/ConfirmCancelPendingTweetDialog;

    .line 1630
    :cond_0
    return-void
.end method

.method protected b()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1588
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment$e;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 1589
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/app/common/timeline/TimelineFragment$e;->c:Landroid/content/Context;

    if-eqz v1, :cond_1

    .line 1590
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v1

    .line 1591
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment$e;->a:Landroid/view/animation/TranslateAnimation;

    .line 1592
    if-nez v0, :cond_0

    .line 1593
    iget-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment$e;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1594
    const v2, 0x7f0f000d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 1595
    const v3, 0x7f0e00f4

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    .line 1596
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    neg-float v4, v3

    invoke-direct {v0, v4, v6, v6, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1598
    int-to-long v4, v2

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 1599
    new-instance v4, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v4}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    invoke-virtual {v0, v4}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1600
    new-instance v4, Lcom/twitter/app/common/timeline/TimelineFragment$e$1;

    invoke-direct {v4, p0, v1, v0}, Lcom/twitter/app/common/timeline/TimelineFragment$e$1;-><init>(Lcom/twitter/app/common/timeline/TimelineFragment$e;Landroid/view/View;Landroid/view/animation/TranslateAnimation;)V

    .line 1608
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    neg-float v3, v3

    invoke-direct {v0, v6, v3, v6, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1609
    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 1610
    new-instance v2, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v2}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v2}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1611
    invoke-virtual {v0, v4}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1612
    iput-object v0, p0, Lcom/twitter/app/common/timeline/TimelineFragment$e;->a:Landroid/view/animation/TranslateAnimation;

    .line 1614
    :cond_0
    if-eqz v1, :cond_1

    .line 1615
    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1618
    :cond_1
    return-void
.end method
