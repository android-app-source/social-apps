.class public abstract Lcom/twitter/app/common/timeline/c$a;
.super Lcom/twitter/app/common/list/i$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/common/timeline/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/app/common/timeline/c$a",
        "<TT;>;>",
        "Lcom/twitter/app/common/list/i$a",
        "<TT;>;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/twitter/app/common/list/i$a;-><init>(Landroid/os/Bundle;)V

    .line 39
    return-void
.end method

.method protected constructor <init>(Lcom/twitter/app/common/timeline/c;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/twitter/app/common/list/i$a;-><init>(Lcom/twitter/app/common/list/i;)V

    .line 43
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/app/common/timeline/c$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/core/TwitterUser;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/twitter/app/common/timeline/c$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "timeline_arg_profile_user"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 66
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/timeline/c$a;

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/app/common/timeline/c$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/app/common/timeline/c$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "timeline_arg_timeline_tag"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/timeline/c$a;

    return-object v0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/app/common/timeline/c$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/twitter/app/common/timeline/c$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "timeline_arg_scribe_section"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/timeline/c$a;

    return-object v0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/app/common/timeline/c$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/twitter/app/common/timeline/c$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "timeline_arg_scribe_page"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/timeline/c$a;

    return-object v0
.end method
