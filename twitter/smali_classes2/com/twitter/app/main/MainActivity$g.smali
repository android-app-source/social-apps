.class Lcom/twitter/app/main/MainActivity$g;
.super Lcom/twitter/library/client/g;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/main/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "g"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/main/MainActivity;


# direct methods
.method constructor <init>(Lcom/twitter/app/main/MainActivity;)V
    .locals 0

    .prologue
    .line 1896
    iput-object p1, p0, Lcom/twitter/app/main/MainActivity$g;->a:Lcom/twitter/app/main/MainActivity;

    invoke-direct {p0}, Lcom/twitter/library/client/g;-><init>()V

    .line 1897
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;)V
    .locals 2

    .prologue
    .line 1901
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$g;->a:Lcom/twitter/app/main/MainActivity;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/app/main/MainActivity;->a(Lcom/twitter/app/main/MainActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 1902
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Z)V
    .locals 1

    .prologue
    .line 1907
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$g;->a:Lcom/twitter/app/main/MainActivity;

    iget-object v0, v0, Lcom/twitter/app/main/MainActivity;->h:Lcom/twitter/app/main/MainActivity$a;

    invoke-virtual {v0, p1}, Lcom/twitter/app/main/MainActivity$a;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1908
    return-void
.end method

.method public d(Lcom/twitter/library/client/Session;)V
    .locals 3

    .prologue
    .line 1912
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$g;->a:Lcom/twitter/app/main/MainActivity;

    invoke-static {v0}, Lcom/twitter/app/main/MainActivity;->d(Lcom/twitter/app/main/MainActivity;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/library/client/Session;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1913
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$g;->a:Lcom/twitter/app/main/MainActivity;

    invoke-static {v0}, Lcom/twitter/app/main/MainActivity;->e(Lcom/twitter/app/main/MainActivity;)V

    .line 1914
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$g;->a:Lcom/twitter/app/main/MainActivity;

    invoke-static {v0}, Lcom/twitter/app/main/MainActivity;->f(Lcom/twitter/app/main/MainActivity;)V

    .line 1915
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$g;->a:Lcom/twitter/app/main/MainActivity;

    invoke-static {v0}, Lcom/twitter/app/main/MainActivity;->g(Lcom/twitter/app/main/MainActivity;)Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 1916
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity$g;->a:Lcom/twitter/app/main/MainActivity;

    invoke-virtual {v1}, Lcom/twitter/app/main/MainActivity;->F()Lcmt;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v2

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcmt;->a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/UserSettings;)V

    .line 1918
    :cond_0
    return-void
.end method
