.class final Lcom/twitter/app/main/MainActivity$a;
.super Landroid/os/Handler;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/main/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# static fields
.field public static final a:[I


# instance fields
.field private final b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2023
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/twitter/app/main/MainActivity$a;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x2
        0x4
    .end array-data
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2028
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 2029
    iput-object p1, p0, Lcom/twitter/app/main/MainActivity$a;->b:Landroid/content/Context;

    .line 2030
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/app/main/MainActivity$i;IIJ)V
    .locals 9

    .prologue
    .line 2095
    new-instance v1, Lcom/twitter/library/api/search/l;

    move-object v2, p0

    move-object v3, p1

    move v4, p3

    move v5, p4

    move-wide v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/twitter/library/api/search/l;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;IIJ)V

    .line 2097
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 2098
    return-void
.end method


# virtual methods
.method public varargs a(JILcom/twitter/app/main/MainActivity$i;[I)V
    .locals 5

    .prologue
    .line 2084
    array-length v1, p5

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget v2, p5, v0

    .line 2085
    invoke-virtual {p0, v2}, Lcom/twitter/app/main/MainActivity$a;->hasMessages(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2086
    invoke-virtual {p0, v2}, Lcom/twitter/app/main/MainActivity$a;->removeMessages(I)V

    .line 2088
    :cond_0
    const/4 v3, -0x1

    invoke-virtual {p0, v2, p3, v3, p4}, Lcom/twitter/app/main/MainActivity$a;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {p0, v2, p1, p2}, Lcom/twitter/app/main/MainActivity$a;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 2084
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2091
    :cond_1
    return-void
.end method

.method public varargs a(JLcom/twitter/app/main/MainActivity$i;[I)V
    .locals 7

    .prologue
    .line 2079
    const/4 v4, -0x1

    move-object v1, p0

    move-wide v2, p1

    move-object v5, p3

    move-object v6, p4

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/app/main/MainActivity$a;->a(JILcom/twitter/app/main/MainActivity$i;[I)V

    .line 2080
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2034
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity$a;->b:Landroid/content/Context;

    .line 2035
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    .line 2036
    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 2037
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    .line 2038
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 2075
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 2040
    :pswitch_1
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lcom/twitter/app/main/MainActivity$i;

    .line 2041
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 2042
    const/4 v5, -0x1

    if-le v0, v5, :cond_1

    int-to-long v6, v0

    .line 2044
    :goto_1
    invoke-static {}, Lcom/twitter/android/client/w;->a()I

    move-result v5

    .line 2045
    invoke-static/range {v1 .. v7}, Lcom/twitter/app/main/MainActivity$a;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/app/main/MainActivity$i;IIJ)V

    goto :goto_0

    .line 2043
    :cond_1
    invoke-static {}, Lcom/twitter/android/client/w;->b()J

    move-result-wide v6

    goto :goto_1

    .line 2050
    :pswitch_2
    const-string/jumbo v3, "saved_searches_ttl_hours"

    invoke-static {v3, v4}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v3

    int-to-long v4, v3

    const-wide/32 v6, 0x36ee80

    mul-long/2addr v4, v6

    .line 2052
    new-instance v3, Lcom/twitter/library/api/search/c;

    invoke-direct {v3, v1, v2, v4, v5}, Lcom/twitter/library/api/search/c;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    invoke-virtual {v0, v3, v8}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    goto :goto_0

    .line 2056
    :pswitch_3
    invoke-static {v1, v2, v5}, Lbfd;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Z)Lber;

    move-result-object v1

    .line 2058
    if-eqz v1, :cond_0

    .line 2059
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    goto :goto_0

    .line 2064
    :pswitch_4
    new-instance v1, Lbcw;

    iget-object v3, p0, Lcom/twitter/app/main/MainActivity$a;->b:Landroid/content/Context;

    invoke-direct {v1, v3, v2}, Lbcw;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 2065
    invoke-virtual {v1, v5}, Lbcw;->a(I)Lbcw;

    move-result-object v1

    .line 2066
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lbcw;->a(J)Lbcw;

    move-result-object v1

    .line 2067
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbcw;->a(Ljava/lang/String;)Lbcw;

    move-result-object v1

    const/16 v2, 0x64

    .line 2068
    invoke-virtual {v1, v2}, Lbcw;->c(I)Lbcw;

    move-result-object v1

    .line 2064
    invoke-virtual {v0, v1, v8}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    goto :goto_0

    .line 2038
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
