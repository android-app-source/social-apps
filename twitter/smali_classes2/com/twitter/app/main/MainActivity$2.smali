.class Lcom/twitter/app/main/MainActivity$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/main/MainActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/main/MainActivity;


# direct methods
.method constructor <init>(Lcom/twitter/app/main/MainActivity;)V
    .locals 0

    .prologue
    .line 478
    iput-object p1, p0, Lcom/twitter/app/main/MainActivity$2;->a:Lcom/twitter/app/main/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 481
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$2;->a:Lcom/twitter/app/main/MainActivity;

    iget-object v0, v0, Lcom/twitter/app/main/MainActivity;->k:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 482
    if-ne p3, v0, :cond_0

    .line 483
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity$2;->a:Lcom/twitter/app/main/MainActivity;

    iget-object v2, p0, Lcom/twitter/app/main/MainActivity$2;->a:Lcom/twitter/app/main/MainActivity;

    iget-object v2, v2, Lcom/twitter/app/main/MainActivity;->l:Lcom/twitter/app/main/MainActivity$f;

    invoke-virtual {v2, v0}, Lcom/twitter/app/main/MainActivity$f;->a(I)Lcom/twitter/library/client/m;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/twitter/app/main/MainActivity;->a(Lcom/twitter/app/main/MainActivity;Lcom/twitter/library/client/m;)V

    .line 484
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$2;->a:Lcom/twitter/app/main/MainActivity;

    invoke-virtual {v0}, Lcom/twitter/app/main/MainActivity;->s()V

    .line 488
    :goto_0
    return-void

    .line 486
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$2;->a:Lcom/twitter/app/main/MainActivity;

    iget-object v0, v0, Lcom/twitter/app/main/MainActivity;->k:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0
.end method
