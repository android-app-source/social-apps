.class Lcom/twitter/app/main/MainActivity$e;
.super Lcno$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/main/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "e"
.end annotation


# instance fields
.field final a:Landroid/net/Uri;

.field final b:Landroid/support/v4/view/ViewPager;

.field final c:Lcom/twitter/internal/android/widget/DockLayout;

.field final d:Lcom/twitter/android/AbsPagesAdapter;

.field final synthetic e:Lcom/twitter/app/main/MainActivity;


# direct methods
.method constructor <init>(Lcom/twitter/app/main/MainActivity;Landroid/net/Uri;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/DockLayout;Lcom/twitter/android/AbsPagesAdapter;)V
    .locals 0

    .prologue
    .line 2249
    iput-object p1, p0, Lcom/twitter/app/main/MainActivity$e;->e:Lcom/twitter/app/main/MainActivity;

    invoke-direct {p0}, Lcno$a;-><init>()V

    .line 2250
    iput-object p2, p0, Lcom/twitter/app/main/MainActivity$e;->a:Landroid/net/Uri;

    .line 2251
    iput-object p3, p0, Lcom/twitter/app/main/MainActivity$e;->b:Landroid/support/v4/view/ViewPager;

    .line 2252
    iput-object p5, p0, Lcom/twitter/app/main/MainActivity$e;->d:Lcom/twitter/android/AbsPagesAdapter;

    .line 2253
    iput-object p4, p0, Lcom/twitter/app/main/MainActivity$e;->c:Lcom/twitter/internal/android/widget/DockLayout;

    .line 2254
    return-void
.end method


# virtual methods
.method public a(Lcno;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2258
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$e;->d:Lcom/twitter/android/AbsPagesAdapter;

    iget-object v1, p0, Lcom/twitter/app/main/MainActivity$e;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/AbsPagesAdapter;->a(I)Lcom/twitter/library/client/m;

    move-result-object v0

    .line 2259
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/app/main/MainActivity$e;->a:Landroid/net/Uri;

    iget-object v2, v0, Lcom/twitter/library/client/m;->a:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v0, v0, Lcom/twitter/library/client/m;->i:I

    if-lez v0, :cond_0

    .line 2260
    sget-object v0, Lcom/twitter/app/main/MainActivity;->b:Landroid/net/Uri;

    iget-object v1, p0, Lcom/twitter/app/main/MainActivity$e;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2261
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$e;->e:Lcom/twitter/app/main/MainActivity;

    invoke-static {v0}, Lcom/twitter/app/main/MainActivity;->l(Lcom/twitter/app/main/MainActivity;)Lcom/twitter/library/client/p;

    move-result-object v0

    new-instance v1, Lbfw;

    iget-object v2, p0, Lcom/twitter/app/main/MainActivity$e;->e:Lcom/twitter/app/main/MainActivity;

    iget-object v3, p0, Lcom/twitter/app/main/MainActivity$e;->e:Lcom/twitter/app/main/MainActivity;

    .line 2262
    invoke-static {v3}, Lcom/twitter/app/main/MainActivity;->k(Lcom/twitter/app/main/MainActivity;)Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {v1, v2, v3, v4}, Lbfw;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;I)V

    .line 2261
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 2263
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$e;->e:Lcom/twitter/app/main/MainActivity;

    invoke-static {v0, v4}, Lcom/twitter/app/main/MainActivity;->b(Lcom/twitter/app/main/MainActivity;I)V

    .line 2267
    :cond_0
    invoke-super {p0, p1}, Lcno$a;->a(Lcno;)V

    .line 2268
    return-void
.end method
