.class Lcom/twitter/app/main/MainActivity$i;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/main/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "i"
.end annotation


# instance fields
.field private final a:Lcom/twitter/app/main/MainActivity$a;


# direct methods
.method constructor <init>(Lcom/twitter/app/main/MainActivity$a;)V
    .locals 0

    .prologue
    .line 1925
    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    .line 1926
    iput-object p1, p0, Lcom/twitter/app/main/MainActivity$i;->a:Lcom/twitter/app/main/MainActivity$a;

    .line 1927
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 1921
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/app/main/MainActivity$i;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 6

    .prologue
    .line 1931
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 1932
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1933
    :cond_0
    instance-of v0, p1, Lcom/twitter/library/api/search/l;

    if-eqz v0, :cond_1

    .line 1934
    check-cast p1, Lcom/twitter/library/api/search/l;

    .line 1936
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$i;->a:Lcom/twitter/app/main/MainActivity$a;

    const-wide/32 v2, 0x927c0

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/twitter/library/api/search/l;->e()I

    move-result v5

    aput v5, v1, v4

    invoke-virtual {v0, v2, v3, p0, v1}, Lcom/twitter/app/main/MainActivity$a;->a(JLcom/twitter/app/main/MainActivity$i;[I)V

    .line 1939
    :cond_1
    return-void
.end method
