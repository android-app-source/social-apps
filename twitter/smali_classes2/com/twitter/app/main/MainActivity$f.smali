.class Lcom/twitter/app/main/MainActivity$f;
.super Lcom/twitter/android/AbsPagesAdapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/main/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "f"
.end annotation


# instance fields
.field final synthetic g:Lcom/twitter/app/main/MainActivity;

.field private final h:Lcom/twitter/internal/android/widget/DockLayout;

.field private i:I


# direct methods
.method constructor <init>(Lcom/twitter/app/main/MainActivity;Lcom/twitter/app/main/MainActivity;Ljava/util/List;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/HorizontalListView;Lcom/twitter/android/at;Lcom/twitter/internal/android/widget/DockLayout;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/app/main/MainActivity;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;",
            "Landroid/support/v4/view/ViewPager;",
            "Lcom/twitter/internal/android/widget/HorizontalListView;",
            "Lcom/twitter/android/at;",
            "Lcom/twitter/internal/android/widget/DockLayout;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2113
    iput-object p1, p0, Lcom/twitter/app/main/MainActivity$f;->g:Lcom/twitter/app/main/MainActivity;

    .line 2114
    invoke-virtual {p2}, Lcom/twitter/app/main/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    move-object v0, p0

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/AbsPagesAdapter;-><init>(Landroid/support/v4/app/FragmentActivity;Landroid/support/v4/app/FragmentManager;Ljava/util/List;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/HorizontalListView;Lcom/twitter/android/at;)V

    .line 2115
    iput-object p7, p0, Lcom/twitter/app/main/MainActivity$f;->h:Lcom/twitter/internal/android/widget/DockLayout;

    .line 2116
    new-instance v0, Lcom/twitter/app/main/MainActivity$f$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/app/main/MainActivity$f$1;-><init>(Lcom/twitter/app/main/MainActivity$f;Lcom/twitter/app/main/MainActivity;)V

    invoke-virtual {p0, v0}, Lcom/twitter/app/main/MainActivity$f;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2128
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/main/MainActivity$f;I)I
    .locals 0

    .prologue
    .line 2107
    iput p1, p0, Lcom/twitter/app/main/MainActivity$f;->f:I

    return p1
.end method

.method private a(Lcom/twitter/app/common/list/TwitterListFragment;)Z
    .locals 4

    .prologue
    .line 2180
    const/4 v1, 0x0

    .line 2181
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/m;

    .line 2182
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    iget-object v0, v0, Lcom/twitter/library/client/m;->b:Ljava/lang/Class;

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2183
    const/4 v0, 0x1

    .line 2187
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/app/main/MainActivity$f;I)I
    .locals 0

    .prologue
    .line 2107
    iput p1, p0, Lcom/twitter/app/main/MainActivity$f;->f:I

    return p1
.end method


# virtual methods
.method public e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2137
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$f;->b:Ljava/util/List;

    iget-object v1, p0, Lcom/twitter/app/main/MainActivity$f;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/m;

    iget-object v0, v0, Lcom/twitter/library/client/m;->e:Ljava/lang/String;

    return-object v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2192
    invoke-super {p0, p1, p2}, Lcom/twitter/android/AbsPagesAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/twitter/app/common/list/TwitterListFragment;

    .line 2193
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$f;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/twitter/library/client/m;

    .line 2194
    invoke-virtual {v2, v6}, Lcom/twitter/library/client/m;->a(Lcom/twitter/app/common/base/BaseFragment;)V

    .line 2195
    new-instance v0, Lcom/twitter/app/main/MainActivity$e;

    iget-object v1, p0, Lcom/twitter/app/main/MainActivity$f;->g:Lcom/twitter/app/main/MainActivity;

    iget-object v2, v2, Lcom/twitter/library/client/m;->a:Landroid/net/Uri;

    iget-object v3, p0, Lcom/twitter/app/main/MainActivity$f;->g:Lcom/twitter/app/main/MainActivity;

    iget-object v3, v3, Lcom/twitter/app/main/MainActivity;->k:Landroid/support/v4/view/ViewPager;

    iget-object v4, p0, Lcom/twitter/app/main/MainActivity$f;->h:Lcom/twitter/internal/android/widget/DockLayout;

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/app/main/MainActivity$e;-><init>(Lcom/twitter/app/main/MainActivity;Landroid/net/Uri;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/DockLayout;Lcom/twitter/android/AbsPagesAdapter;)V

    invoke-virtual {v6, v0}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcno$c;)Lcom/twitter/app/common/list/TwitterListFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/main/MainActivity$f;->g:Lcom/twitter/app/main/MainActivity;

    iget-object v1, v1, Lcom/twitter/app/main/MainActivity;->g:Lcom/twitter/android/q;

    .line 2196
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/refresh/widget/RefreshableListView$e;)Lcom/twitter/app/common/list/TwitterListFragment;

    .line 2197
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$f;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 2200
    invoke-virtual {p0, p2}, Lcom/twitter/app/main/MainActivity$f;->onPageSelected(I)V

    .line 2202
    :cond_0
    return-object v6
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0

    .prologue
    .line 2237
    invoke-super {p0, p1}, Lcom/twitter/android/AbsPagesAdapter;->onPageScrollStateChanged(I)V

    .line 2238
    iput p1, p0, Lcom/twitter/app/main/MainActivity$f;->i:I

    .line 2239
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2225
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/AbsPagesAdapter;->onPageScrolled(IFI)V

    .line 2226
    iget v2, p0, Lcom/twitter/app/main/MainActivity$f;->i:I

    if-ne v2, v0, :cond_0

    .line 2227
    if-nez p1, :cond_1

    move v2, v0

    .line 2228
    :goto_0
    if-nez p3, :cond_2

    .line 2229
    :goto_1
    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 2230
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$f;->g:Lcom/twitter/app/main/MainActivity;

    invoke-virtual {v0}, Lcom/twitter/app/main/MainActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->f()Z

    .line 2233
    :cond_0
    return-void

    :cond_1
    move v2, v1

    .line 2227
    goto :goto_0

    :cond_2
    move v0, v1

    .line 2228
    goto :goto_1
.end method

.method public onPageSelected(I)V
    .locals 2

    .prologue
    .line 2207
    invoke-super {p0, p1}, Lcom/twitter/android/AbsPagesAdapter;->onPageSelected(I)V

    .line 2209
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$f;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/m;

    .line 2210
    iget v1, p0, Lcom/twitter/app/main/MainActivity$f;->f:I

    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity$f;->c(I)Lcom/twitter/library/client/m;

    move-result-object v1

    .line 2212
    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity$f;->a(Lcom/twitter/library/client/m;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2213
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity$f;->g:Lcom/twitter/app/main/MainActivity;

    invoke-virtual {v1, v0}, Lcom/twitter/app/main/MainActivity;->c(Lcom/twitter/library/client/m;)V

    .line 2216
    :cond_0
    invoke-virtual {p0, v0}, Lcom/twitter/app/main/MainActivity$f;->b(Lcom/twitter/library/client/m;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2217
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity$f;->g:Lcom/twitter/app/main/MainActivity;

    iget-object v0, v0, Lcom/twitter/library/client/m;->a:Landroid/net/Uri;

    invoke-virtual {v1, v0}, Lcom/twitter/app/main/MainActivity;->b(Landroid/net/Uri;)V

    .line 2218
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$f;->g:Lcom/twitter/app/main/MainActivity;

    invoke-virtual {v0}, Lcom/twitter/app/main/MainActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->h()V

    .line 2219
    iput p1, p0, Lcom/twitter/app/main/MainActivity$f;->f:I

    .line 2221
    :cond_1
    return-void
.end method

.method public restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 9

    .prologue
    .line 2147
    check-cast p1, Lcom/twitter/android/AbsPagesAdapter$PageSavedState;

    .line 2148
    iget-object v8, p1, Lcom/twitter/android/AbsPagesAdapter$PageSavedState;->a:[Ljava/lang/String;

    .line 2150
    const/4 v0, 0x0

    move v7, v0

    :goto_0
    array-length v0, v8

    if-ge v7, v0, :cond_2

    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v7, v0, :cond_2

    .line 2151
    aget-object v0, v8, v7

    .line 2152
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity$f;->c:Landroid/support/v4/app/FragmentManager;

    .line 2153
    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/twitter/app/common/list/TwitterListFragment;

    .line 2154
    if-eqz v6, :cond_0

    .line 2155
    invoke-direct {p0, v6}, Lcom/twitter/app/main/MainActivity$f;->a(Lcom/twitter/app/common/list/TwitterListFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2156
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$f;->b:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/twitter/library/client/m;

    .line 2157
    invoke-virtual {v2, v6}, Lcom/twitter/library/client/m;->a(Lcom/twitter/app/common/base/BaseFragment;)V

    .line 2158
    new-instance v0, Lcom/twitter/app/main/MainActivity$e;

    iget-object v1, p0, Lcom/twitter/app/main/MainActivity$f;->g:Lcom/twitter/app/main/MainActivity;

    iget-object v2, v2, Lcom/twitter/library/client/m;->a:Landroid/net/Uri;

    iget-object v3, p0, Lcom/twitter/app/main/MainActivity$f;->g:Lcom/twitter/app/main/MainActivity;

    iget-object v3, v3, Lcom/twitter/app/main/MainActivity;->k:Landroid/support/v4/view/ViewPager;

    iget-object v4, p0, Lcom/twitter/app/main/MainActivity$f;->h:Lcom/twitter/internal/android/widget/DockLayout;

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/app/main/MainActivity$e;-><init>(Lcom/twitter/app/main/MainActivity;Landroid/net/Uri;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/DockLayout;Lcom/twitter/android/AbsPagesAdapter;)V

    invoke-virtual {v6, v0}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcno$c;)Lcom/twitter/app/common/list/TwitterListFragment;

    .line 2159
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$f;->h:Lcom/twitter/internal/android/widget/DockLayout;

    if-eqz v0, :cond_0

    .line 2160
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$f;->g:Lcom/twitter/app/main/MainActivity;

    iget-object v0, v0, Lcom/twitter/app/main/MainActivity;->g:Lcom/twitter/android/q;

    invoke-virtual {v6, v0}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/refresh/widget/RefreshableListView$e;)Lcom/twitter/app/common/list/TwitterListFragment;

    .line 2150
    :cond_0
    :goto_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 2171
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$f;->c:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 2172
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Removed un-used fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2173
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 2172
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2177
    :cond_2
    return-void
.end method

.method public saveState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 2142
    new-instance v0, Lcom/twitter/android/AbsPagesAdapter$PageSavedState;

    iget-object v1, p0, Lcom/twitter/app/main/MainActivity$f;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Lcom/twitter/android/AbsPagesAdapter$PageSavedState;-><init>(Ljava/util/List;)V

    return-object v0
.end method
