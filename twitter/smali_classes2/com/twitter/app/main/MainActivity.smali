.class public Lcom/twitter/app/main/MainActivity;
.super Lcom/twitter/android/ActivityWithProgress;
.source "Twttr"

# interfaces
.implements Landroid/accounts/OnAccountsUpdateListener;
.implements Lcom/twitter/android/ax;
.implements Lcom/twitter/android/dogfood/a$b;
.implements Lcom/twitter/android/geo/a$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/main/MainActivity$c;,
        Lcom/twitter/app/main/MainActivity$d;,
        Lcom/twitter/app/main/MainActivity$e;,
        Lcom/twitter/app/main/MainActivity$f;,
        Lcom/twitter/app/main/MainActivity$a;,
        Lcom/twitter/app/main/MainActivity$h;,
        Lcom/twitter/app/main/MainActivity$b;,
        Lcom/twitter/app/main/MainActivity$i;,
        Lcom/twitter/app/main/MainActivity$g;
    }
.end annotation


# static fields
.field public static final b:Landroid/net/Uri;

.field public static final c:Landroid/net/Uri;

.field public static final d:Landroid/net/Uri;

.field public static final e:Landroid/net/Uri;

.field public static final f:Landroid/net/Uri;

.field private static final m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static n:I

.field private static o:I


# instance fields
.field private A:Ljava/lang/String;

.field private B:Lcom/twitter/app/main/MainActivity$d;

.field private C:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;"
        }
    .end annotation
.end field

.field private D:Lcom/twitter/android/util/m;

.field private E:Lcom/twitter/library/service/t;

.field private K:Lcom/twitter/app/main/MainActivity$i;

.field private L:Lcom/twitter/app/main/MainActivity$g;

.field private M:Lcom/twitter/android/client/l;

.field private N:Lcom/twitter/android/geo/a;

.field private O:Z

.field private P:Lrx/j;

.field private Q:Lcom/twitter/app/main/b;

.field private R:Lahm;

.field private S:Lcom/twitter/app/main/a;

.field private T:Lcmq;

.field private U:Lcmq;

.field private V:Lcmq;

.field private W:Lcmq;

.field private X:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcmq;",
            ">;"
        }
    .end annotation
.end field

.field private Y:Lcom/twitter/app/lists/d;

.field g:Lcom/twitter/android/q;

.field h:Lcom/twitter/app/main/MainActivity$a;

.field i:Ljava/lang/String;

.field j:I

.field k:Landroid/support/v4/view/ViewPager;

.field l:Lcom/twitter/app/main/MainActivity$f;

.field private final p:Lcom/twitter/app/main/MainActivity$h;

.field private final q:Lcom/twitter/util/android/f;

.field private final r:Lcoj$a;

.field private s:J

.field private t:I

.field private u:Landroid/content/SharedPreferences;

.field private v:Lcom/twitter/android/client/i;

.field private w:Lcom/twitter/library/client/e;

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 225
    const-string/jumbo v0, "twitter://timeline/home"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/app/main/MainActivity;->b:Landroid/net/Uri;

    .line 226
    const-string/jumbo v0, "twitter://notifications"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/app/main/MainActivity;->c:Landroid/net/Uri;

    .line 227
    const-string/jumbo v0, "twitter://dms"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/app/main/MainActivity;->d:Landroid/net/Uri;

    .line 228
    const-string/jumbo v0, "twitter://moments"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/app/main/MainActivity;->e:Landroid/net/Uri;

    .line 229
    const-string/jumbo v0, "twitter://news"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/app/main/MainActivity;->f:Landroid/net/Uri;

    .line 282
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/twitter/app/main/MainActivity;->m:Ljava/util/Map;

    .line 285
    sput v1, Lcom/twitter/app/main/MainActivity;->n:I

    .line 286
    sput v1, Lcom/twitter/app/main/MainActivity;->o:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 219
    invoke-direct {p0}, Lcom/twitter/android/ActivityWithProgress;-><init>()V

    .line 297
    new-instance v0, Lcom/twitter/app/main/MainActivity$h;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/app/main/MainActivity$h;-><init>(Lcom/twitter/app/main/MainActivity;Lcom/twitter/app/main/MainActivity$1;)V

    iput-object v0, p0, Lcom/twitter/app/main/MainActivity;->p:Lcom/twitter/app/main/MainActivity$h;

    .line 300
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/main/MainActivity;->q:Lcom/twitter/util/android/f;

    .line 302
    new-instance v0, Lcom/twitter/app/main/MainActivity$1;

    invoke-direct {v0, p0}, Lcom/twitter/app/main/MainActivity$1;-><init>(Lcom/twitter/app/main/MainActivity;)V

    iput-object v0, p0, Lcom/twitter/app/main/MainActivity;->r:Lcoj$a;

    return-void
.end method

.method private B()V
    .locals 4

    .prologue
    .line 863
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 885
    :cond_0
    :goto_0
    return-void

    .line 868
    :cond_1
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 869
    invoke-static {v0, v1}, Lbqa;->c(J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 872
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v2

    invoke-virtual {v2}, Lcqq;->b()Lcqs;

    move-result-object v2

    .line 873
    invoke-static {}, Lcom/twitter/android/twogday/b;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 874
    invoke-static {v2}, Lcom/twitter/android/twogday/b;->a(Lcqs;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 875
    invoke-static {}, Lcom/twitter/android/twogday/b;->b()V

    .line 876
    invoke-static {p0, v0, v1}, Lcom/twitter/android/twogday/TwoGDayStartOverlay;->a(Landroid/support/v4/app/FragmentActivity;J)V

    goto :goto_0

    .line 879
    :cond_2
    invoke-static {v2}, Lcom/twitter/android/twogday/b;->a(Lcqs;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 880
    invoke-static {}, Lcom/twitter/library/network/s;->a()Lcom/twitter/library/network/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/s;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 881
    invoke-static {}, Lcom/twitter/android/twogday/b;->c()V

    .line 882
    invoke-static {p0, v0, v1}, Lcom/twitter/android/twogday/TwoGDayEndOverlay;->a(Landroid/support/v4/app/FragmentActivity;J)V

    goto :goto_0
.end method

.method private C()V
    .locals 6

    .prologue
    .line 1090
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 1091
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    .line 1092
    if-eqz v1, :cond_0

    .line 1093
    iget-object v2, p0, Lcom/twitter/app/main/MainActivity;->D:Lcom/twitter/android/util/m;

    invoke-virtual {v1}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/twitter/android/util/m;->a(J)V

    .line 1094
    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->ac()V

    .line 1097
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->P()Lcom/twitter/android/search/SearchSuggestionController;

    move-result-object v2

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Lcom/twitter/android/search/SearchSuggestionController;->b(Ljava/lang/CharSequence;)V

    .line 1099
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->F()Lcmt;

    move-result-object v2

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcmt;->a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/UserSettings;)V

    .line 1100
    return-void
.end method

.method private D()V
    .locals 3

    .prologue
    .line 1103
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1105
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->D:Lcom/twitter/android/util/m;

    invoke-virtual {v0}, Lcom/twitter/android/util/m;->a()I

    move-result v0

    .line 1106
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v1

    .line 1107
    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 1108
    sget-object v0, Lcom/twitter/app/main/MainActivity;->e:Landroid/net/Uri;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1112
    :cond_0
    :goto_0
    sget-object v0, Lcom/twitter/app/main/MainActivity;->c:Landroid/net/Uri;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1113
    sget-object v0, Lcom/twitter/app/main/MainActivity;->d:Landroid/net/Uri;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1116
    sget-object v0, Lcom/twitter/app/main/MainActivity;->b:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/twitter/app/main/MainActivity;->c(Landroid/net/Uri;)V

    .line 1117
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1118
    invoke-direct {p0, v0}, Lcom/twitter/app/main/MainActivity;->c(Landroid/net/Uri;)V

    goto :goto_1

    .line 1109
    :cond_1
    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 1110
    sget-object v0, Lcom/twitter/app/main/MainActivity;->f:Landroid/net/Uri;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1120
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->l:Lcom/twitter/app/main/MainActivity$f;

    invoke-virtual {v0}, Lcom/twitter/app/main/MainActivity$f;->notifyDataSetChanged()V

    .line 1121
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1850
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/app/main/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x4000000

    .line 1851
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 1852
    if-eqz p1, :cond_0

    .line 1853
    const-string/jumbo v1, "page"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1855
    :cond_0
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/app/TaskStackBuilder;
    .locals 3

    .prologue
    .line 373
    invoke-static {p0}, Landroid/support/v4/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/support/v4/app/TaskStackBuilder;

    move-result-object v0

    .line 374
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/app/main/MainActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "AbsFragmentActivity_account_name"

    .line 375
    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 376
    invoke-virtual {v0, v1}, Landroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/support/v4/app/TaskStackBuilder;

    .line 377
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/app/main/MainActivity;)Lcom/twitter/android/composer/ComposerDockLayout;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->I:Lcom/twitter/android/composer/ComposerDockLayout;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/app/main/MainActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 219
    iput-object p1, p0, Lcom/twitter/app/main/MainActivity;->A:Ljava/lang/String;

    return-object p1
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 1876
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->U:Lcmq;

    if-eqz v0, :cond_0

    .line 1877
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->U:Lcmq;

    invoke-virtual {v0, p1}, Lcmq;->a(I)V

    .line 1879
    :cond_0
    return-void
.end method

.method private a(II)V
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/AnimRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/AnimRes;
        .end annotation
    .end param

    .prologue
    .line 1772
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/app/main/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1773
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->l:Lcom/twitter/app/main/MainActivity$f;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->l:Lcom/twitter/app/main/MainActivity$f;

    invoke-virtual {v1}, Lcom/twitter/app/main/MainActivity$f;->c()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1774
    const-string/jumbo v1, "page"

    iget-object v2, p0, Lcom/twitter/app/main/MainActivity;->l:Lcom/twitter/app/main/MainActivity$f;

    invoke-virtual {v2}, Lcom/twitter/app/main/MainActivity$f;->c()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1776
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->finish()V

    .line 1777
    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/main/MainActivity;->overridePendingTransition(II)V

    .line 1778
    invoke-virtual {p0, v0}, Lcom/twitter/app/main/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 1779
    return-void
.end method

.method private a(J)V
    .locals 3

    .prologue
    const/4 v2, 0x5

    .line 919
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->h:Lcom/twitter/app/main/MainActivity$a;

    .line 920
    invoke-virtual {v0, v2}, Lcom/twitter/app/main/MainActivity$a;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 921
    invoke-virtual {v0, v2}, Lcom/twitter/app/main/MainActivity$a;->removeMessages(I)V

    .line 923
    :cond_0
    invoke-virtual {v0, v2}, Lcom/twitter/app/main/MainActivity$a;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/app/main/MainActivity$a;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 924
    return-void
.end method

.method private a(JZ)V
    .locals 3

    .prologue
    .line 1054
    if-eqz p3, :cond_0

    .line 1055
    invoke-direct {p0, p1, p2}, Lcom/twitter/app/main/MainActivity;->d(J)V

    .line 1061
    :goto_0
    return-void

    .line 1057
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1058
    const-string/jumbo v1, "activity"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1059
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/twitter/library/platform/TwitterDataSyncService;->a(Landroid/content/Context;Landroid/os/Bundle;Lcom/twitter/library/client/Session;)V

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1840
    invoke-static {p0, p1}, Lcom/twitter/app/main/MainActivity;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 1841
    instance-of v0, p0, Lcom/twitter/app/common/base/TwitterFragmentActivity;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 1842
    check-cast v0, Lcom/twitter/app/common/base/TwitterFragmentActivity;

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->e(Landroid/content/Intent;)V

    .line 1846
    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 1847
    return-void

    .line 1844
    :cond_0
    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1291
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/twitter/app/main/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1292
    return-void
.end method

.method public static a(Landroid/content/Intent;Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 382
    invoke-static {p1, p2}, Lcom/twitter/app/main/MainActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/app/TaskStackBuilder;

    move-result-object v0

    .line 383
    invoke-virtual {v0, p0}, Landroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/support/v4/app/TaskStackBuilder;

    .line 384
    invoke-virtual {v0}, Landroid/support/v4/app/TaskStackBuilder;->startActivities()V

    .line 385
    return-void
.end method

.method private a(Landroid/net/Uri;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1128
    new-instance v0, Lcom/twitter/app/home/c$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/app/home/c$a;-><init>(Landroid/os/Bundle;)V

    iget v1, p0, Lcom/twitter/app/main/MainActivity;->t:I

    .line 1129
    invoke-virtual {v0, v1}, Lcom/twitter/app/home/c$a;->e(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/home/c$a;

    .line 1130
    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->w()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/home/c$a;->f(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/home/c$a;

    .line 1132
    sget-object v1, Lcom/twitter/app/main/MainActivity;->b:Landroid/net/Uri;

    invoke-virtual {p1, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1133
    const v1, 0x7f0a0379

    invoke-virtual {v0, v1}, Lcom/twitter/app/home/c$a;->b(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/app/home/c$a;

    const v2, 0x7f0a037a

    .line 1134
    invoke-virtual {v1, v2}, Lcom/twitter/app/home/c$a;->c(I)Lcom/twitter/app/common/list/i$a;

    .line 1136
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1137
    if-eqz v1, :cond_0

    const-string/jumbo v2, "ref_event"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1138
    const-string/jumbo v2, "ref_event"

    const-string/jumbo v3, "ref_event"

    .line 1139
    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1138
    invoke-virtual {v0, v2, v1}, Lcom/twitter/app/home/c$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/app/common/base/b$a;

    .line 1142
    :cond_0
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->C:Ljava/util/List;

    new-instance v2, Lcom/twitter/library/client/m$a;

    sget-object v3, Lcom/twitter/app/main/MainActivity;->b:Landroid/net/Uri;

    const-class v4, Lcom/twitter/app/home/HomeTimelineFragment;

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/client/m$a;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    .line 1143
    invoke-virtual {v0}, Lcom/twitter/app/home/c$a;->a()Lcom/twitter/app/home/c;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/library/client/m$a;->a(Lcom/twitter/app/common/base/b;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    const-string/jumbo v2, "home"

    .line 1144
    invoke-virtual {v0, v2}, Lcom/twitter/library/client/m$a;->a(Ljava/lang/String;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    const v2, 0x7f0a0410

    .line 1145
    invoke-virtual {p0, v2}, Lcom/twitter/app/main/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/client/m$a;->a(Ljava/lang/CharSequence;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    const v2, 0x7f020297

    .line 1146
    invoke-virtual {v0, v2}, Lcom/twitter/library/client/m$a;->a(I)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 1147
    invoke-virtual {v0, v5}, Lcom/twitter/library/client/m$a;->a(Z)Lcom/twitter/library/client/m$a;

    move-result-object v0

    const v2, 0x7f130043

    .line 1148
    invoke-virtual {v0, v2}, Lcom/twitter/library/client/m$a;->b(I)Lcom/twitter/library/client/m$a;

    move-result-object v0

    const-string/jumbo v2, "nav_item_tag_home"

    .line 1149
    invoke-virtual {v0, v2}, Lcom/twitter/library/client/m$a;->a(Ljava/lang/Object;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 1150
    invoke-virtual {v0}, Lcom/twitter/library/client/m$a;->a()Lcom/twitter/library/client/m;

    move-result-object v0

    .line 1142
    invoke-interface {v1, p2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1182
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->B:Lcom/twitter/app/main/MainActivity$d;

    invoke-virtual {v0}, Lcom/twitter/app/main/MainActivity$d;->notifyDataSetChanged()V

    .line 1183
    return-void

    .line 1151
    :cond_2
    sget-object v1, Lcom/twitter/app/main/MainActivity;->d:Landroid/net/Uri;

    invoke-virtual {p1, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1152
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->C:Ljava/util/List;

    new-instance v2, Lcom/twitter/app/dm/DMActivity$a;

    invoke-direct {v2}, Lcom/twitter/app/dm/DMActivity$a;-><init>()V

    invoke-virtual {v0}, Lcom/twitter/app/home/c$a;->a()Lcom/twitter/app/home/c;

    move-result-object v0

    invoke-virtual {v2, p0, p1, v0}, Lcom/twitter/app/dm/DMActivity$a;->a(Landroid/content/Context;Landroid/net/Uri;Lcom/twitter/app/common/base/b;)Lcom/twitter/library/client/m;

    move-result-object v0

    invoke-interface {v1, p2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 1153
    :cond_3
    sget-object v1, Lcom/twitter/app/main/MainActivity;->c:Landroid/net/Uri;

    invoke-virtual {p1, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1154
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1155
    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->w()I

    move-result v1

    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->y()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/app/home/c$a;->f(I)Lcom/twitter/app/common/list/i$a;

    .line 1156
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->C:Ljava/util/List;

    new-instance v2, Lcom/twitter/android/notificationtimeline/g;

    invoke-direct {v2}, Lcom/twitter/android/notificationtimeline/g;-><init>()V

    invoke-virtual {v0}, Lcom/twitter/app/home/c$a;->a()Lcom/twitter/app/home/c;

    move-result-object v0

    invoke-virtual {v2, p0, p1, v0}, Lcom/twitter/android/notificationtimeline/g;->a(Landroid/content/Context;Landroid/net/Uri;Lcom/twitter/app/common/base/b;)Lcom/twitter/library/client/m;

    move-result-object v0

    invoke-interface {v1, p2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 1158
    :cond_4
    sget-object v1, Lcom/twitter/app/main/MainActivity;->e:Landroid/net/Uri;

    invoke-virtual {p1, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1159
    const-string/jumbo v1, "show_category_pills"

    .line 1160
    invoke-static {}, Lbsd;->g()Z

    move-result v2

    .line 1159
    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/home/c$a;->a(Ljava/lang/String;Z)Lcom/twitter/app/common/base/b$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/app/home/c$a;

    const-string/jumbo v2, "guide_type"

    .line 1161
    invoke-virtual {v1, v2, v5}, Lcom/twitter/app/home/c$a;->a(Ljava/lang/String;I)Lcom/twitter/app/common/base/b$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/app/home/c$a;

    const-string/jumbo v2, "home_view_tag"

    const-string/jumbo v3, "nav_item_tag_home"

    .line 1162
    invoke-virtual {v1, v2, v3}, Lcom/twitter/app/home/c$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/app/common/base/b$a;

    .line 1163
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->C:Ljava/util/List;

    new-instance v2, Lcom/twitter/library/client/m$a;

    const-class v3, Lcom/twitter/android/moments/ui/guide/MomentsGuideFragment;

    invoke-direct {v2, p1, v3}, Lcom/twitter/library/client/m$a;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    .line 1164
    invoke-virtual {v0}, Lcom/twitter/app/home/c$a;->a()Lcom/twitter/app/home/c;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/library/client/m$a;->a(Lcom/twitter/app/common/base/b;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    const-string/jumbo v2, "moments"

    .line 1165
    invoke-virtual {v0, v2}, Lcom/twitter/library/client/m$a;->a(Ljava/lang/String;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 1166
    invoke-static {}, Lwk;->d()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/twitter/app/main/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/client/m$a;->a(Ljava/lang/CharSequence;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 1167
    invoke-static {}, Lwk;->e()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/twitter/app/main/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/client/m$a;->b(Ljava/lang/CharSequence;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 1168
    invoke-static {}, Lwk;->c()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/client/m$a;->a(I)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 1169
    invoke-virtual {v0, v5}, Lcom/twitter/library/client/m$a;->a(Z)Lcom/twitter/library/client/m$a;

    move-result-object v0

    const v2, 0x7f1307a8

    .line 1170
    invoke-virtual {v0, v2}, Lcom/twitter/library/client/m$a;->b(I)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 1171
    invoke-virtual {v0}, Lcom/twitter/library/client/m$a;->a()Lcom/twitter/library/client/m;

    move-result-object v0

    .line 1163
    invoke-interface {v1, p2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1172
    :cond_5
    sget-object v1, Lcom/twitter/app/main/MainActivity;->f:Landroid/net/Uri;

    invoke-virtual {p1, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1173
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->C:Ljava/util/List;

    new-instance v2, Lcom/twitter/library/client/m$a;

    const-class v3, Lcom/twitter/android/news/CategorizedNewsFragment;

    invoke-direct {v2, p1, v3}, Lcom/twitter/library/client/m$a;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    .line 1174
    invoke-virtual {v0}, Lcom/twitter/app/home/c$a;->a()Lcom/twitter/app/home/c;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/library/client/m$a;->a(Lcom/twitter/app/common/base/b;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    const-string/jumbo v2, "news"

    .line 1175
    invoke-virtual {v0, v2}, Lcom/twitter/library/client/m$a;->a(Ljava/lang/String;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    const v2, 0x7f0a05ca

    .line 1176
    invoke-virtual {p0, v2}, Lcom/twitter/app/main/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/client/m$a;->a(Ljava/lang/CharSequence;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    const v2, 0x7f02029b

    .line 1177
    invoke-virtual {v0, v2}, Lcom/twitter/library/client/m$a;->a(I)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 1178
    invoke-virtual {v0, v5}, Lcom/twitter/library/client/m$a;->a(Z)Lcom/twitter/library/client/m$a;

    move-result-object v0

    const v2, 0x7f1307a9

    .line 1179
    invoke-virtual {v0, v2}, Lcom/twitter/library/client/m$a;->b(I)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 1180
    invoke-virtual {v0}, Lcom/twitter/library/client/m$a;->a()Lcom/twitter/library/client/m;

    move-result-object v0

    .line 1173
    invoke-interface {v1, p2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private a(Lcmm;II)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1064
    if-nez p1, :cond_1

    .line 1087
    :cond_0
    :goto_0
    return-void

    .line 1068
    :cond_1
    instance-of v0, p1, Lcom/twitter/ui/widget/b;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 1069
    check-cast v0, Lcom/twitter/ui/widget/b;

    .line 1070
    invoke-interface {v0, p2}, Lcom/twitter/ui/widget/b;->setBadgeMode(I)V

    .line 1071
    invoke-interface {v0, p3}, Lcom/twitter/ui/widget/b;->setBadgeNumber(I)V

    .line 1075
    :cond_2
    instance-of v0, p1, Lazv;

    if-eqz v0, :cond_0

    .line 1076
    check-cast p1, Lazv;

    .line 1077
    invoke-virtual {p1}, Lazv;->k()Ljava/lang/CharSequence;

    move-result-object v1

    .line 1078
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1079
    if-gtz p3, :cond_4

    const-string/jumbo v0, ""

    .line 1081
    :goto_1
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1082
    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1085
    :cond_3
    :goto_2
    invoke-virtual {p1, v0}, Lazv;->b(Ljava/lang/CharSequence;)Lazv;

    goto :goto_0

    .line 1079
    :cond_4
    const/high16 v0, 0x7f0c0000

    new-array v3, v6, [Ljava/lang/Object;

    .line 1080
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v2, v0, p3, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1082
    :cond_5
    const v3, 0x7f0a002e

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    aput-object v0, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method static synthetic a(Lcom/twitter/app/main/MainActivity;I)V
    .locals 0

    .prologue
    .line 219
    invoke-direct {p0, p1}, Lcom/twitter/app/main/MainActivity;->b(I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/main/MainActivity;II)V
    .locals 0

    .prologue
    .line 219
    invoke-direct {p0, p1, p2}, Lcom/twitter/app/main/MainActivity;->a(II)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/main/MainActivity;JZ)V
    .locals 1

    .prologue
    .line 219
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/app/main/MainActivity;->a(JZ)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/main/MainActivity;Lcom/twitter/library/client/m;)V
    .locals 0

    .prologue
    .line 219
    invoke-direct {p0, p1}, Lcom/twitter/app/main/MainActivity;->d(Lcom/twitter/library/client/m;)V

    return-void
.end method

.method private a(Lcom/twitter/library/client/navigation/g;)V
    .locals 6

    .prologue
    .line 1475
    invoke-virtual {p1}, Lcom/twitter/library/client/navigation/g;->f()Ljava/util/List;

    move-result-object v0

    .line 1477
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/BadgeableUserImageView;

    .line 1478
    invoke-virtual {v0}, Lcom/twitter/media/ui/image/BadgeableUserImageView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/account/UserAccount;

    .line 1479
    if-eqz v1, :cond_0

    iget-object v3, v1, Lcom/twitter/model/account/UserAccount;->b:Lcom/twitter/model/core/TwitterUser;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/twitter/app/main/MainActivity;->X:Ljava/util/Map;

    if-eqz v3, :cond_0

    .line 1480
    iget-object v3, p0, Lcom/twitter/app/main/MainActivity;->X:Ljava/util/Map;

    iget-object v1, v1, Lcom/twitter/model/account/UserAccount;->b:Lcom/twitter/model/core/TwitterUser;

    iget-wide v4, v1, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcmq;

    .line 1481
    if-eqz v1, :cond_0

    .line 1482
    new-instance v3, Lcmo;

    invoke-direct {v3, v0}, Lcmo;-><init>(Lcom/twitter/ui/widget/b;)V

    invoke-virtual {v1, v3}, Lcmq;->a(Lcmp;)V

    goto :goto_0

    .line 1486
    :cond_1
    return-void
.end method

.method public static a(Lcom/twitter/library/client/v;Landroid/content/Intent;Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 389
    invoke-virtual {p0, p3}, Lcom/twitter/library/client/v;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 390
    invoke-virtual {p0, v0}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/Session;)V

    .line 391
    invoke-static {p1, p2, p3}, Lcom/twitter/app/main/MainActivity;->a(Landroid/content/Intent;Landroid/content/Context;Ljava/lang/String;)V

    .line 392
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/main/MainActivity;Z)Z
    .locals 0

    .prologue
    .line 219
    iput-boolean p1, p0, Lcom/twitter/app/main/MainActivity;->z:Z

    return p1
.end method

.method private ab()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1186
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->D:Lcom/twitter/android/util/m;

    invoke-virtual {v0}, Lcom/twitter/android/util/m;->a()I

    move-result v0

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/twitter/app/main/MainActivity;->e:Landroid/net/Uri;

    .line 1187
    invoke-direct {p0, v0}, Lcom/twitter/app/main/MainActivity;->f(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/twitter/app/main/MainActivity;->f:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/twitter/app/main/MainActivity;->f(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1188
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1189
    sget-object v0, Lcom/twitter/app/main/MainActivity;->e:Landroid/net/Uri;

    invoke-direct {p0, v0, v1}, Lcom/twitter/app/main/MainActivity;->a(Landroid/net/Uri;I)V

    .line 1190
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->l:Lcom/twitter/app/main/MainActivity$f;

    invoke-virtual {v0}, Lcom/twitter/app/main/MainActivity$f;->notifyDataSetChanged()V

    .line 1192
    :cond_0
    return-void
.end method

.method private ac()V
    .locals 2

    .prologue
    .line 1195
    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->D()V

    .line 1196
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->k:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1197
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->l:Lcom/twitter/app/main/MainActivity$f;

    invoke-virtual {v0}, Lcom/twitter/app/main/MainActivity$f;->d()V

    .line 1200
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->u:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "tag"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1201
    return-void
.end method

.method private ad()V
    .locals 1

    .prologue
    .line 1279
    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->ae()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/app/main/MainActivity;->a(Landroid/content/Intent;)V

    .line 1280
    return-void
.end method

.method private ae()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1284
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/AccountsDialogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "page"

    sget-object v2, Lcom/twitter/app/main/MainActivity;->b:Landroid/net/Uri;

    .line 1285
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "AccountsDialogActivity_account_name"

    .line 1287
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    .line 1286
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1284
    return-object v0
.end method

.method private af()V
    .locals 4

    .prologue
    .line 1328
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    const-string/jumbo v2, "japan_news_android_periodic_sync_enabled"

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcoj;->a(JLjava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1330
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/app/main/MainActivity;->p:Lcom/twitter/app/main/MainActivity$h;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 1333
    :cond_0
    return-void
.end method

.method private ag()V
    .locals 3

    .prologue
    .line 1710
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->c()Lcmr;

    move-result-object v1

    .line 1711
    instance-of v0, v1, Lcom/twitter/library/client/navigation/g;

    if-eqz v0, :cond_0

    .line 1713
    invoke-static {p0}, Lcom/twitter/library/util/d;->b(Landroid/content/Context;)V

    .line 1715
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->I:Lcom/twitter/android/composer/ComposerDockLayout;

    invoke-virtual {v0}, Lcom/twitter/android/composer/ComposerDockLayout;->b()V

    move-object v0, v1

    .line 1717
    check-cast v0, Lcom/twitter/library/client/navigation/g;

    new-instance v2, Lcom/twitter/app/main/MainActivity$6;

    invoke-direct {v2, p0}, Lcom/twitter/app/main/MainActivity$6;-><init>(Lcom/twitter/app/main/MainActivity;)V

    .line 1718
    invoke-virtual {v0, v2}, Lcom/twitter/library/client/navigation/g;->a(Landroid/support/v4/widget/DrawerLayout$SimpleDrawerListener;)V

    .line 1724
    invoke-interface {v1}, Lcmr;->e()Z

    .line 1728
    :goto_0
    return-void

    .line 1726
    :cond_0
    const-string/jumbo v0, "Theme switching is only supported within Modern Android"

    invoke-static {v0}, Lcom/twitter/util/f;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private ah()V
    .locals 3

    .prologue
    .line 1731
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 1732
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->F()Lcmt;

    move-result-object v1

    invoke-virtual {v1}, Lcmt;->c()Lcmr;

    move-result-object v1

    .line 1733
    if-eqz v1, :cond_0

    .line 1734
    const v2, 0x7f1308a4

    invoke-interface {v1, v2}, Lcmr;->b(I)Lcmm;

    move-result-object v1

    .line 1735
    if-eqz v1, :cond_0

    .line 1736
    invoke-static {v0}, Lcom/twitter/android/ads/c;->c(Lcom/twitter/model/core/TwitterUser;)Z

    move-result v0

    invoke-interface {v1, v0}, Lcmm;->f(Z)Lcmm;

    .line 1739
    :cond_0
    return-void
.end method

.method private ai()V
    .locals 3

    .prologue
    .line 1742
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 1743
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->F()Lcmt;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v2

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcmt;->a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/UserSettings;)V

    .line 1744
    return-void
.end method

.method static synthetic b(Lcom/twitter/app/main/MainActivity;)Lcom/twitter/android/composer/ComposerDockLayout;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->I:Lcom/twitter/android/composer/ComposerDockLayout;

    return-object v0
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 1882
    iget v0, p0, Lcom/twitter/app/main/MainActivity;->j:I

    if-eq v0, p1, :cond_0

    .line 1883
    iput p1, p0, Lcom/twitter/app/main/MainActivity;->j:I

    .line 1885
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->h()V

    .line 1887
    :cond_0
    return-void
.end method

.method private b(J)V
    .locals 3

    .prologue
    const/4 v2, 0x6

    .line 927
    invoke-static {}, Lcom/twitter/app/lists/e;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 928
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->h:Lcom/twitter/app/main/MainActivity$a;

    .line 929
    invoke-virtual {v0, v2}, Lcom/twitter/app/main/MainActivity$a;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 930
    invoke-virtual {v0, v2}, Lcom/twitter/app/main/MainActivity$a;->removeMessages(I)V

    .line 932
    :cond_0
    invoke-virtual {v0, v2}, Lcom/twitter/app/main/MainActivity$a;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/app/main/MainActivity$a;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 934
    :cond_1
    return-void
.end method

.method private b(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 1336
    const-string/jumbo v0, "twitter"

    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1337
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 1339
    const-string/jumbo v1, "timeline"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1340
    sget-object v0, Lcom/twitter/app/main/MainActivity;->b:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/twitter/app/main/MainActivity;->d(Landroid/net/Uri;)V

    .line 1355
    :goto_0
    const-string/jumbo v0, "scroll_to_top"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/main/MainActivity;->y:Z

    .line 1356
    return-void

    .line 1343
    :cond_0
    sget-object v0, Lcom/twitter/app/main/MainActivity;->b:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/twitter/app/main/MainActivity;->d(Landroid/net/Uri;)V

    goto :goto_0

    .line 1346
    :cond_1
    const-string/jumbo v0, "page"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1347
    if-nez v0, :cond_2

    .line 1349
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->u:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "tag"

    sget-object v2, Lcom/twitter/app/main/MainActivity;->b:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/app/main/MainActivity;->d(Landroid/net/Uri;)V

    goto :goto_0

    .line 1352
    :cond_2
    invoke-direct {p0, v0}, Lcom/twitter/app/main/MainActivity;->d(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/app/main/MainActivity;I)V
    .locals 0

    .prologue
    .line 219
    invoke-direct {p0, p1}, Lcom/twitter/app/main/MainActivity;->a(I)V

    return-void
.end method

.method static synthetic c(Lcom/twitter/app/main/MainActivity;)Lcom/twitter/android/composer/ComposerDockLayout;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->I:Lcom/twitter/android/composer/ComposerDockLayout;

    return-object v0
.end method

.method private c(J)V
    .locals 3

    .prologue
    .line 937
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->h:Lcom/twitter/app/main/MainActivity$a;

    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->K:Lcom/twitter/app/main/MainActivity$i;

    sget-object v2, Lcom/twitter/app/main/MainActivity$a;->a:[I

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/twitter/app/main/MainActivity$a;->a(JLcom/twitter/app/main/MainActivity$i;[I)V

    .line 938
    return-void
.end method

.method private c(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 1124
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/twitter/app/main/MainActivity;->a(Landroid/net/Uri;I)V

    .line 1125
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1761
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    .line 1763
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->L:Lcom/twitter/app/main/MainActivity$g;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/u;)V

    .line 1765
    invoke-virtual {v0, p1}, Lcom/twitter/library/client/v;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 1766
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/Session;)V

    .line 1767
    const v0, 0x7f050055

    const v1, 0x7f050056

    invoke-direct {p0, v0, v1}, Lcom/twitter/app/main/MainActivity;->a(II)V

    .line 1768
    return-void
.end method

.method static synthetic d(Lcom/twitter/app/main/MainActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private d(J)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1859
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/twitter/library/platform/TwitterDataSyncService;->a(Landroid/content/Context;J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1860
    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    .line 1861
    const-string/jumbo v1, "activity"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1862
    const-string/jumbo v1, "user_settings_sync"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1863
    const-string/jumbo v1, "live_addressbook_sync"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1864
    const-string/jumbo v1, "messages"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1865
    const-string/jumbo v1, "show_notif"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1866
    const-string/jumbo v1, "news"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1867
    const-string/jumbo v1, "moments"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1868
    invoke-static {p1, p2}, Lcom/twitter/library/util/b;->a(J)Lakm;

    move-result-object v1

    .line 1869
    if-eqz v1, :cond_0

    .line 1870
    invoke-virtual {v1}, Lakm;->a()Landroid/accounts/Account;

    move-result-object v1

    invoke-static {p0, v0, v3, v1}, Lcom/twitter/library/platform/TwitterDataSyncService;->a(Landroid/content/Context;Landroid/os/Bundle;ZLandroid/accounts/Account;)Z

    .line 1873
    :cond_0
    return-void
.end method

.method private d(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1359
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->l:Lcom/twitter/app/main/MainActivity$f;

    invoke-virtual {v0, p1}, Lcom/twitter/app/main/MainActivity$f;->a(Landroid/net/Uri;)I

    move-result v0

    .line 1360
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1361
    invoke-static {}, Lcom/twitter/android/metrics/LaunchTracker;->a()Lcom/twitter/android/metrics/LaunchTracker;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/twitter/android/metrics/LaunchTracker;->a(Landroid/net/Uri;)V

    .line 1362
    invoke-virtual {p0, p1}, Lcom/twitter/app/main/MainActivity;->b(Landroid/net/Uri;)V

    .line 1363
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->k:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1365
    :cond_0
    return-void
.end method

.method private d(Lcom/twitter/library/client/m;)V
    .locals 5

    .prologue
    .line 1532
    invoke-static {p1}, Lcom/twitter/app/main/MainActivity;->e(Lcom/twitter/library/client/m;)Ljava/lang/String;

    move-result-object v0

    .line 1533
    if-eqz v0, :cond_0

    .line 1534
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "home"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "navigation_bar"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, ""

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object v0, v2, v3

    const/4 v0, 0x4

    const-string/jumbo v3, "tab_tap_scroll_to_top"

    aput-object v3, v2, v0

    .line 1535
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1534
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1537
    :cond_0
    return-void
.end method

.method private static e(Lcom/twitter/library/client/m;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1542
    sget-object v0, Lcom/twitter/app/main/MainActivity;->e:Landroid/net/Uri;

    iget-object v1, p0, Lcom/twitter/library/client/m;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1543
    const-string/jumbo v0, "moments"

    .line 1555
    :goto_0
    return-object v0

    .line 1544
    :cond_0
    sget-object v0, Lcom/twitter/app/main/MainActivity;->f:Landroid/net/Uri;

    iget-object v1, p0, Lcom/twitter/library/client/m;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1545
    const-string/jumbo v0, "news_menu_item"

    goto :goto_0

    .line 1546
    :cond_1
    sget-object v0, Lcom/twitter/app/main/MainActivity;->c:Landroid/net/Uri;

    iget-object v1, p0, Lcom/twitter/library/client/m;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1547
    const-string/jumbo v0, "notifications_menu_item"

    goto :goto_0

    .line 1548
    :cond_2
    sget-object v0, Lcom/twitter/app/main/MainActivity;->d:Landroid/net/Uri;

    iget-object v1, p0, Lcom/twitter/library/client/m;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1549
    const-string/jumbo v0, "messages_menu_item"

    goto :goto_0

    .line 1550
    :cond_3
    sget-object v0, Lcom/twitter/app/main/MainActivity;->b:Landroid/net/Uri;

    iget-object v1, p0, Lcom/twitter/library/client/m;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1551
    const-string/jumbo v0, "home_menu_item"

    goto :goto_0

    .line 1553
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lcom/twitter/app/main/MainActivity;)V
    .locals 0

    .prologue
    .line 219
    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->ai()V

    return-void
.end method

.method static synthetic f(Lcom/twitter/app/main/MainActivity;)V
    .locals 0

    .prologue
    .line 219
    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->ah()V

    return-void
.end method

.method private f(Landroid/net/Uri;)Z
    .locals 3

    .prologue
    .line 1829
    const/4 v1, 0x0

    .line 1830
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/m;

    .line 1831
    iget-object v0, v0, Lcom/twitter/library/client/m;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1832
    const/4 v0, 0x1

    .line 1836
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method static synthetic g(Lcom/twitter/app/main/MainActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/app/main/MainActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/app/main/MainActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/app/main/MainActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/app/main/MainActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/app/main/MainActivity;)Lcom/twitter/library/client/p;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->G:Lcom/twitter/library/client/p;

    return-object v0
.end method

.method static synthetic m(Lcom/twitter/app/main/MainActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/app/main/MainActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private t()V
    .locals 6

    .prologue
    .line 578
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v0

    invoke-virtual {v0}, Lakn;->c()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/util/b;->a(Ljava/util/List;Lcom/twitter/library/client/v;)Ljava/util/List;

    move-result-object v0

    .line 581
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 583
    invoke-static {v0, v1}, Lcpt;->a(Ljava/lang/Iterable;I)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/account/UserAccount;

    .line 584
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/twitter/model/account/UserAccount;->b:Lcom/twitter/model/core/TwitterUser;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/app/main/MainActivity;->X:Ljava/util/Map;

    if-eqz v2, :cond_0

    .line 585
    iget-object v2, p0, Lcom/twitter/app/main/MainActivity;->X:Ljava/util/Map;

    iget-object v3, v0, Lcom/twitter/model/account/UserAccount;->b:Lcom/twitter/model/core/TwitterUser;

    iget-wide v4, v3, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v0, v0, Lcom/twitter/model/account/UserAccount;->b:Lcom/twitter/model/core/TwitterUser;

    iget-wide v4, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 586
    invoke-static {p0, v4, v5}, Lcom/twitter/library/media/widget/b;->a(Landroid/content/Context;J)Lcmq;

    move-result-object v0

    .line 585
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 589
    :cond_1
    return-void
.end method

.method private u()Lcom/twitter/library/service/s;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 600
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 601
    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 614
    :cond_0
    :goto_0
    return-object v1

    .line 604
    :cond_1
    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v2

    .line 605
    if-eqz v2, :cond_0

    iget-wide v4, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-gtz v0, :cond_2

    iget-object v0, v2, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 608
    :cond_2
    sget-object v0, Lcom/twitter/app/main/MainActivity;->m:Ljava/util/Map;

    iget-wide v4, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 609
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v4

    .line 610
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long v6, v4, v6

    const-wide/32 v8, 0xea60

    cmp-long v0, v6, v8

    if-lez v0, :cond_0

    .line 611
    :cond_3
    sget-object v0, Lcom/twitter/app/main/MainActivity;->m:Ljava/util/Map;

    iget-wide v6, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 612
    new-instance v1, Lbio;

    iget-wide v4, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-object v6, v2, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lbio;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;)V

    goto :goto_0
.end method

.method private v()I
    .locals 2

    .prologue
    .line 619
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e030a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method private w()I
    .locals 2

    .prologue
    .line 623
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e030b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method private y()I
    .locals 2

    .prologue
    .line 627
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e020c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method private z()V
    .locals 2

    .prologue
    .line 841
    iget-boolean v0, p0, Lcom/twitter/app/main/MainActivity;->z:Z

    if-nez v0, :cond_1

    .line 842
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->P:Lrx/j;

    if-eqz v0, :cond_0

    .line 843
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->P:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 845
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/twitter/library/provider/h;->c(J)Lrx/c;

    move-result-object v0

    .line 846
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/main/MainActivity$3;

    invoke-direct {v1, p0}, Lcom/twitter/app/main/MainActivity$3;-><init>(Lcom/twitter/app/main/MainActivity;)V

    .line 847
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/main/MainActivity;->P:Lrx/j;

    .line 856
    :cond_1
    return-void
.end method


# virtual methods
.method public A()Z
    .locals 1

    .prologue
    .line 641
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic a(Lcom/twitter/library/client/m;)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 219
    invoke-virtual {p0, p1}, Lcom/twitter/app/main/MainActivity;->b(Lcom/twitter/library/client/m;)Lcom/twitter/app/common/list/TwitterListFragment;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    .line 397
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ActivityWithProgress;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    .line 398
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(I)V

    .line 399
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->b(Z)V

    .line 400
    const v1, 0x7f040192

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 401
    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 1891
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/main/MainActivity;->O:Z

    .line 1892
    return-void
.end method

.method public a(Landroid/net/Uri;IZ)V
    .locals 3

    .prologue
    .line 1750
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/m;

    .line 1751
    iget-object v2, v0, Lcom/twitter/library/client/m;->a:Landroid/net/Uri;

    invoke-virtual {p1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, v0, Lcom/twitter/library/client/m;->i:I

    if-eq v2, p2, :cond_0

    .line 1752
    iput p2, v0, Lcom/twitter/library/client/m;->i:I

    .line 1753
    iput-boolean p3, v0, Lcom/twitter/library/client/m;->h:Z

    .line 1754
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->B:Lcom/twitter/app/main/MainActivity$d;

    invoke-virtual {v0}, Lcom/twitter/app/main/MainActivity$d;->notifyDataSetChanged()V

    .line 1758
    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/library/service/s;I)V
    .locals 6

    .prologue
    .line 1038
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ActivityWithProgress;->a(Lcom/twitter/library/service/s;I)V

    .line 1041
    iget-object v0, p1, Lcom/twitter/library/service/s;->e:Ljava/lang/String;

    sget-object v1, Lcom/twitter/library/api/activity/FetchActivityTimeline;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1042
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v1

    .line 1043
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 1044
    if-eqz v1, :cond_0

    iget-wide v2, v1, Lcom/twitter/library/service/v;->c:J

    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    if-eqz v0, :cond_0

    .line 1045
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1051
    :cond_0
    :goto_0
    return-void

    .line 1049
    :cond_1
    iget-wide v0, v1, Lcom/twitter/library/service/v;->c:J

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/app/main/MainActivity;->a(JZ)V

    goto :goto_0
.end method

.method public a(Lcmm;)Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 1570
    invoke-interface {p1}, Lcmm;->a()I

    move-result v1

    .line 1571
    sparse-switch v1, :sswitch_data_0

    .line 1674
    invoke-super {p0, p1}, Lcom/twitter/android/ActivityWithProgress;->a(Lcmm;)Z

    move-result v0

    .line 1678
    :cond_0
    :goto_0
    return v0

    .line 1573
    :sswitch_0
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "home"

    aput-object v3, v2, v4

    const-string/jumbo v3, "navigation_bar"

    aput-object v3, v2, v0

    const/4 v3, 0x2

    const-string/jumbo v4, "overflow"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string/jumbo v4, ""

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "click"

    aput-object v4, v2, v3

    .line 1574
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    .line 1573
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 1575
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->F()Lcmt;

    move-result-object v1

    invoke-virtual {v1}, Lcmt;->c()Lcmr;

    move-result-object v1

    .line 1576
    if-eqz v1, :cond_0

    .line 1577
    invoke-interface {v1}, Lcmr;->b()Z

    goto :goto_0

    .line 1582
    :sswitch_1
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->n()V

    goto :goto_0

    .line 1586
    :sswitch_2
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->l()V

    goto :goto_0

    .line 1590
    :sswitch_3
    const-string/jumbo v1, "night_mode_switch"

    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->b(Ljava/lang/String;)V

    .line 1591
    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->ag()V

    goto :goto_0

    .line 1595
    :sswitch_4
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/people/PeopleDiscoveryActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 1596
    const-string/jumbo v1, "peopleplus_overflow_item"

    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 1600
    :sswitch_5
    invoke-static {p0}, Lcom/twitter/android/qrcodes/a;->b(Landroid/content/Context;)V

    .line 1601
    const-string/jumbo v1, "qr_code"

    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 1605
    :sswitch_6
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "user_id"

    .line 1606
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    .line 1605
    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 1607
    const-string/jumbo v1, "me_overflow_item"

    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1611
    :sswitch_7
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lcom/twitter/android/moments/ui/guide/ac;->a(Landroid/app/Activity;J)V

    .line 1612
    const-string/jumbo v1, "moments_overflow_item"

    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1616
    :sswitch_8
    invoke-static {}, Lcom/twitter/app/lists/a;->a()Lcom/twitter/app/lists/a;

    move-result-object v1

    .line 1617
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/app/lists/a;->b(J)Lcom/twitter/app/lists/a;

    move-result-object v1

    .line 1618
    invoke-virtual {v1, p0}, Lcom/twitter/app/lists/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 1616
    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 1619
    const-string/jumbo v1, "lists_overflow_item"

    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1623
    :sswitch_9
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/app/lists/ListCreateEditActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 1624
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v3, "me:lists:list:new_list:create"

    aput-object v3, v2, v4

    .line 1625
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    .line 1624
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 1629
    :sswitch_a
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->Y:Lcom/twitter/app/lists/d;

    invoke-static {p1}, Lcom/twitter/app/lists/d;->a(Lcmm;)V

    .line 1630
    const-string/jumbo v1, "lists_overflow_item"

    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1634
    :sswitch_b
    new-instance v1, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;

    invoke-direct {v1, p0}, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/twitter/android/highlights/HighlightsStoriesActivity$b;->a()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 1635
    const-string/jumbo v1, "highlights_overflow_item"

    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1639
    :sswitch_c
    invoke-static {p0}, Lcom/twitter/android/ads/AdsCompanionWebViewActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 1640
    const-string/jumbo v1, "open_ads_companion"

    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1644
    :sswitch_d
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/news/NewsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 1645
    const-string/jumbo v1, "news_overflow_item"

    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1650
    :sswitch_e
    invoke-super {p0, p1}, Lcom/twitter/android/ActivityWithProgress;->a(Lcmm;)Z

    .line 1651
    const-string/jumbo v1, "settings_overflow_item"

    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1656
    :sswitch_f
    invoke-super {p0, p1}, Lcom/twitter/android/ActivityWithProgress;->a(Lcmm;)Z

    .line 1657
    const-string/jumbo v1, "help_overflow_item"

    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1662
    :sswitch_10
    invoke-super {p0, p1}, Lcom/twitter/android/ActivityWithProgress;->a(Lcmm;)Z

    .line 1663
    const-string/jumbo v1, "search_menu_item"

    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1667
    :sswitch_11
    new-instance v1, Lcom/twitter/app/users/f;

    invoke-direct {v1}, Lcom/twitter/app/users/f;-><init>()V

    const/16 v2, 0x12

    .line 1668
    invoke-virtual {v1, v2}, Lcom/twitter/app/users/f;->a(I)Lcom/twitter/app/users/f;

    move-result-object v1

    .line 1669
    invoke-virtual {v1, p0}, Lcom/twitter/app/users/f;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 1667
    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 1670
    const-string/jumbo v1, "pending_followers_item"

    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1571
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f13000c -> :sswitch_1
        0x7f13002c -> :sswitch_9
        0x7f13002d -> :sswitch_0
        0x7f130057 -> :sswitch_6
        0x7f13005b -> :sswitch_2
        0x7f1302d3 -> :sswitch_e
        0x7f1307a5 -> :sswitch_b
        0x7f1307ac -> :sswitch_5
        0x7f13088d -> :sswitch_10
        0x7f13089e -> :sswitch_d
        0x7f13089f -> :sswitch_7
        0x7f1308a0 -> :sswitch_8
        0x7f1308a1 -> :sswitch_4
        0x7f1308a2 -> :sswitch_a
        0x7f1308a3 -> :sswitch_11
        0x7f1308a4 -> :sswitch_c
        0x7f1308a5 -> :sswitch_3
        0x7f1308cf -> :sswitch_f
    .end sparse-switch
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 1369
    invoke-super {p0, p1}, Lcom/twitter/android/ActivityWithProgress;->a(Lcmr;)Z

    .line 1370
    const v0, 0x7f14002a

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 1371
    const v0, 0x7f140015

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 1372
    const v0, 0x7f140014

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 1373
    const/4 v0, 0x1

    return v0
.end method

.method public a_(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1561
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/v;->a(Landroid/content/Context;)Lcom/twitter/library/util/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/util/v;->a()V

    .line 1562
    if-eqz p1, :cond_0

    .line 1563
    invoke-direct {p0, p1}, Lcom/twitter/app/main/MainActivity;->c(Ljava/lang/String;)V

    .line 1565
    :cond_0
    return-void
.end method

.method public b(Lcmr;)I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    .line 1379
    invoke-super {p0, p1}, Lcom/twitter/android/ActivityWithProgress;->b(Lcmr;)I

    .line 1380
    invoke-interface {p1}, Lcmr;->k()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 1382
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->R:Lahm;

    invoke-virtual {v1, v0}, Lahm;->a(Lcom/twitter/internal/android/widget/ToolBar;)V

    .line 1385
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->Y:Lcom/twitter/app/lists/d;

    invoke-virtual {v0, p1}, Lcom/twitter/app/lists/d;->a(Lcmr;)V

    .line 1391
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->D:Lcom/twitter/android/util/m;

    invoke-virtual {v0}, Lcom/twitter/android/util/m;->b()V

    .line 1392
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->D:Lcom/twitter/android/util/m;

    invoke-virtual {v0, p1}, Lcom/twitter/android/util/m;->a(Lcmr;)V

    .line 1396
    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->ab()V

    .line 1401
    const v0, 0x7f1308a7

    invoke-interface {p1, v0}, Lcmr;->b(I)Lcmm;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/b;

    .line 1402
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->T:Lcmq;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1403
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->T:Lcmq;

    new-instance v2, Lcmo;

    invoke-direct {v2, v0}, Lcmo;-><init>(Lcom/twitter/ui/widget/b;)V

    invoke-virtual {v1, v2}, Lcmq;->a(Lcmp;)V

    .line 1406
    :cond_0
    const v0, 0x7f130043

    invoke-interface {p1, v0}, Lcmr;->b(I)Lcmm;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/b;

    .line 1407
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->U:Lcmq;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 1408
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->U:Lcmq;

    new-instance v2, Lcmo;

    invoke-direct {v2, v0}, Lcmo;-><init>(Lcom/twitter/ui/widget/b;)V

    invoke-virtual {v1, v2}, Lcmq;->a(Lcmp;)V

    .line 1411
    :cond_1
    const v0, 0x7f1308a6

    invoke-interface {p1, v0}, Lcmr;->b(I)Lcmm;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/b;

    .line 1412
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->V:Lcmq;

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 1413
    invoke-interface {v0, v4}, Lcom/twitter/ui/widget/b;->setBadgeMode(I)V

    .line 1414
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->V:Lcmq;

    new-instance v2, Lcmo;

    invoke-direct {v2, v0}, Lcmo;-><init>(Lcom/twitter/ui/widget/b;)V

    invoke-virtual {v1, v2}, Lcmq;->a(Lcmp;)V

    .line 1417
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->X:Ljava/util/Map;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->X:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    instance-of v0, p1, Lcom/twitter/library/client/navigation/g;

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 1419
    check-cast v0, Lcom/twitter/library/client/navigation/g;

    invoke-direct {p0, v0}, Lcom/twitter/app/main/MainActivity;->a(Lcom/twitter/library/client/navigation/g;)V

    .line 1422
    :cond_3
    const v0, 0x7f1308a3

    invoke-interface {p1, v0}, Lcmr;->b(I)Lcmm;

    move-result-object v1

    .line 1423
    const-string/jumbo v0, "android_pending_followers_navigation_5203"

    invoke-static {v0}, Lcoi;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->W:Lcmq;

    if-eqz v0, :cond_4

    if-eqz v1, :cond_4

    .line 1425
    iget-object v2, p0, Lcom/twitter/app/main/MainActivity;->W:Lcmq;

    new-instance v3, Ladl;

    .line 1426
    invoke-interface {v1}, Lcmm;->c()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/FullBadgeView;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/b;

    invoke-direct {v3, v0, v1}, Ladl;-><init>(Lcom/twitter/ui/widget/b;Lcmm;)V

    .line 1425
    invoke-virtual {v2, v3}, Lcmq;->a(Lcmp;)V

    .line 1434
    :cond_4
    const v0, 0x7f1307a9

    invoke-interface {p1, v0}, Lcmr;->b(I)Lcmm;

    move-result-object v0

    iget v1, p0, Lcom/twitter/app/main/MainActivity;->j:I

    invoke-direct {p0, v0, v5, v1}, Lcom/twitter/app/main/MainActivity;->a(Lcmm;II)V

    .line 1437
    const v0, 0x7f1307a8

    invoke-interface {p1, v0}, Lcmr;->b(I)Lcmm;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v5, v1}, Lcom/twitter/app/main/MainActivity;->a(Lcmm;II)V

    .line 1443
    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->ah()V

    .line 1446
    const v0, 0x7f1308a5

    invoke-interface {p1, v0}, Lcmr;->b(I)Lcmm;

    move-result-object v1

    .line 1447
    if-eqz v1, :cond_5

    .line 1448
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Lcmm;->f(Z)Lcmm;

    .line 1449
    invoke-interface {v1}, Lcmm;->e()Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Landroid/support/v7/widget/SwitchCompat;

    if-eqz v0, :cond_5

    .line 1450
    invoke-interface {v1}, Lcmm;->e()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SwitchCompat;

    .line 1451
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1452
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/library/util/d;->a(Landroid/content/res/Resources;)Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/SwitchCompat;->setChecked(Z)V

    .line 1453
    new-instance v2, Lcom/twitter/app/main/MainActivity$4;

    invoke-direct {v2, p0, v1}, Lcom/twitter/app/main/MainActivity$4;-><init>(Lcom/twitter/app/main/MainActivity;Lcmm;)V

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1463
    :cond_5
    const v0, 0x7f1307ac

    invoke-interface {p1, v0}, Lcmr;->b(I)Lcmm;

    move-result-object v0

    .line 1464
    if-eqz v0, :cond_6

    .line 1465
    invoke-static {}, Lcom/twitter/android/qrcodes/a;->a()Z

    move-result v1

    invoke-interface {v0, v1}, Lcmm;->f(Z)Lcmm;

    .line 1468
    :cond_6
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 1469
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->F()Lcmt;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v2

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcmt;->a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/UserSettings;)V

    .line 1471
    return v4
.end method

.method public b(Lcom/twitter/library/client/m;)Lcom/twitter/app/common/list/TwitterListFragment;
    .locals 1

    .prologue
    .line 900
    if-nez p1, :cond_0

    .line 901
    const/4 v0, 0x0

    .line 904
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/library/client/m;->a(Landroid/support/v4/app/FragmentManager;)Lcom/twitter/app/common/base/BaseFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/list/TwitterListFragment;

    goto :goto_0
.end method

.method public b(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1782
    sget-object v0, Lcom/twitter/app/main/MainActivity;->e:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1783
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Lzp;->a(J)V

    .line 1786
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->l:Lcom/twitter/app/main/MainActivity$f;

    .line 1787
    invoke-virtual {v0, p1}, Lcom/twitter/app/main/MainActivity$f;->a(Landroid/net/Uri;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/main/MainActivity$f;->getPageTitle(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 1788
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->P()Lcom/twitter/android/search/SearchSuggestionController;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/app/main/MainActivity$f;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/search/SearchSuggestionController;->a(Ljava/lang/String;)Lcom/twitter/android/search/SearchSuggestionController;

    .line 1790
    invoke-virtual {p0, p1}, Lcom/twitter/app/main/MainActivity;->e(Landroid/net/Uri;)V

    .line 1792
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->R:Lahm;

    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lahm;->a(Landroid/net/Uri;Lcom/twitter/internal/android/widget/ToolBar;)V

    .line 1793
    return-void
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 10

    .prologue
    .line 406
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ActivityWithProgress;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 408
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 410
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/app/main/MainActivity;->d(Landroid/content/Intent;)V

    .line 412
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->P()Lcom/twitter/android/search/SearchSuggestionController;

    move-result-object v0

    invoke-static {p0, v0}, Lahm;->a(Landroid/content/Context;Lcom/twitter/android/search/SearchSuggestionController;)Lahm;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/main/MainActivity;->R:Lahm;

    .line 413
    invoke-static {p0}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;)Lcom/twitter/android/client/l;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/main/MainActivity;->M:Lcom/twitter/android/client/l;

    .line 415
    new-instance v0, Lcom/twitter/app/main/MainActivity$a;

    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/twitter/app/main/MainActivity$a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/app/main/MainActivity;->h:Lcom/twitter/app/main/MainActivity$a;

    .line 416
    new-instance v0, Lcom/twitter/app/main/MainActivity$i;

    iget-object v2, p0, Lcom/twitter/app/main/MainActivity;->h:Lcom/twitter/app/main/MainActivity$a;

    invoke-direct {v0, v2}, Lcom/twitter/app/main/MainActivity$i;-><init>(Lcom/twitter/app/main/MainActivity$a;)V

    iput-object v0, p0, Lcom/twitter/app/main/MainActivity;->K:Lcom/twitter/app/main/MainActivity$i;

    .line 418
    const v0, 0x7f130378

    invoke-virtual {p0, v0}, Lcom/twitter/app/main/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/twitter/app/main/MainActivity;->k:Landroid/support/v4/view/ViewPager;

    .line 419
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->k:Landroid/support/v4/view/ViewPager;

    const v2, 0x7f0e0286

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 420
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->k:Landroid/support/v4/view/ViewPager;

    const v1, 0x7f1100ca

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setPageMarginDrawable(I)V

    .line 421
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->k:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 423
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/app/main/MainActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/main/MainActivity;->u:Landroid/content/SharedPreferences;

    .line 426
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->u:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "version_code"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/twitter/app/main/MainActivity;->o:I

    .line 427
    const-class v0, Lcom/twitter/app/main/MainActivity;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 429
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, p0, v1, v2}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    .line 430
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/main/MainActivity;->x:Z

    .line 432
    new-instance v0, Lcom/twitter/app/main/MainActivity$b;

    invoke-direct {v0, p0}, Lcom/twitter/app/main/MainActivity$b;-><init>(Lcom/twitter/app/main/MainActivity;)V

    iput-object v0, p0, Lcom/twitter/app/main/MainActivity;->w:Lcom/twitter/library/client/e;

    .line 433
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->w:Lcom/twitter/library/client/e;

    invoke-static {v0}, Lcom/twitter/android/client/AppBroadcastReceiver;->a(Lcom/twitter/library/client/e;)V

    .line 435
    new-instance v0, Lcom/twitter/app/main/MainActivity$g;

    invoke-direct {v0, p0}, Lcom/twitter/app/main/MainActivity$g;-><init>(Lcom/twitter/app/main/MainActivity;)V

    iput-object v0, p0, Lcom/twitter/app/main/MainActivity;->L:Lcom/twitter/app/main/MainActivity$g;

    .line 436
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->L:Lcom/twitter/app/main/MainActivity$g;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/u;)V

    .line 438
    new-instance v0, Lcom/twitter/app/main/MainActivity$c;

    invoke-direct {v0, p0}, Lcom/twitter/app/main/MainActivity$c;-><init>(Lcom/twitter/app/main/MainActivity;)V

    iput-object v0, p0, Lcom/twitter/app/main/MainActivity;->E:Lcom/twitter/library/service/t;

    .line 439
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->G:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->E:Lcom/twitter/library/service/t;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/t;)V

    .line 441
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/main/MainActivity;->C:Ljava/util/List;

    .line 442
    new-instance v0, Lcom/twitter/app/main/MainActivity$d;

    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->C:Ljava/util/List;

    invoke-direct {v0, v1}, Lcom/twitter/app/main/MainActivity$d;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/twitter/app/main/MainActivity;->B:Lcom/twitter/app/main/MainActivity$d;

    .line 444
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 445
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    .line 446
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/main/MainActivity;->i:Ljava/lang/String;

    .line 448
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-static {p0, v1, v8, v9}, Lwl;->a(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;J)Lwl;

    move-result-object v1

    .line 450
    new-instance v2, Lcom/twitter/android/client/i;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 451
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-direct {v2, p0, v0, v3, v1}, Lcom/twitter/android/client/i;-><init>(Landroid/content/Context;Lcom/twitter/model/core/TwitterUser;Landroid/support/v4/app/FragmentManager;Lwl;)V

    iput-object v2, p0, Lcom/twitter/app/main/MainActivity;->v:Lcom/twitter/android/client/i;

    .line 453
    if-nez p1, :cond_0

    .line 457
    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->u()Lcom/twitter/library/service/s;

    move-result-object v0

    .line 458
    if-eqz v0, :cond_0

    .line 459
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->G:Lcom/twitter/library/client/p;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 463
    :cond_0
    new-instance v0, Lcom/twitter/android/util/m;

    invoke-direct {v0, v8, v9}, Lcom/twitter/android/util/m;-><init>(J)V

    iput-object v0, p0, Lcom/twitter/app/main/MainActivity;->D:Lcom/twitter/android/util/m;

    .line 465
    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->w()I

    move-result v0

    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->v()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/twitter/app/main/MainActivity;->t:I

    .line 467
    const v0, 0x7f1302e3

    invoke-virtual {p0, v0}, Lcom/twitter/app/main/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/twitter/internal/android/widget/DockLayout;

    .line 468
    if-eqz v7, :cond_1

    .line 469
    new-instance v0, Lcom/twitter/android/ay;

    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v1

    iget v2, p0, Lcom/twitter/app/main/MainActivity;->t:I

    invoke-direct {v0, p0, v1, v2}, Lcom/twitter/android/ay;-><init>(Lcom/twitter/android/ax;Lcom/twitter/internal/android/widget/ToolBar;I)V

    invoke-virtual {v7, v0}, Lcom/twitter/internal/android/widget/DockLayout;->a(Lcom/twitter/internal/android/widget/DockLayout$a;)V

    .line 473
    :cond_1
    const v0, 0x7f13037a

    invoke-virtual {p0, v0}, Lcom/twitter/app/main/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/twitter/internal/android/widget/HorizontalListView;

    .line 474
    new-instance v0, Lcom/twitter/app/main/MainActivity$f;

    iget-object v3, p0, Lcom/twitter/app/main/MainActivity;->C:Ljava/util/List;

    iget-object v4, p0, Lcom/twitter/app/main/MainActivity;->k:Landroid/support/v4/view/ViewPager;

    iget-object v6, p0, Lcom/twitter/app/main/MainActivity;->B:Lcom/twitter/app/main/MainActivity$d;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/app/main/MainActivity$f;-><init>(Lcom/twitter/app/main/MainActivity;Lcom/twitter/app/main/MainActivity;Ljava/util/List;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/HorizontalListView;Lcom/twitter/android/at;Lcom/twitter/internal/android/widget/DockLayout;)V

    iput-object v0, p0, Lcom/twitter/app/main/MainActivity;->l:Lcom/twitter/app/main/MainActivity$f;

    .line 475
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->k:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->l:Lcom/twitter/app/main/MainActivity$f;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 477
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->B:Lcom/twitter/app/main/MainActivity$d;

    invoke-virtual {v5, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 478
    new-instance v0, Lcom/twitter/app/main/MainActivity$2;

    invoke-direct {v0, p0}, Lcom/twitter/app/main/MainActivity$2;-><init>(Lcom/twitter/app/main/MainActivity;)V

    invoke-virtual {v5, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 491
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->c()Lcmr;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/navigation/g;

    .line 492
    if-eqz v0, :cond_2

    .line 493
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->B:Lcom/twitter/app/main/MainActivity$d;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/navigation/g;->a(Landroid/widget/BaseAdapter;)V

    .line 496
    :cond_2
    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->D()V

    .line 498
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 499
    invoke-static {p0}, Lcom/twitter/android/ae;->c(Landroid/app/Activity;)Z

    move-result v2

    .line 502
    const-string/jumbo v0, "notif_triggered_intent"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 503
    const-string/jumbo v0, "notif_triggered_intent"

    const/4 v3, 0x0

    .line 504
    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 507
    const-string/jumbo v3, "notif_triggered_intent"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 512
    :goto_0
    if-nez v0, :cond_3

    if-nez v2, :cond_9

    .line 513
    :cond_3
    invoke-direct {p0, v1}, Lcom/twitter/app/main/MainActivity;->b(Landroid/content/Intent;)V

    .line 519
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->o()V

    .line 521
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->j()V

    .line 523
    if-nez p1, :cond_a

    .line 524
    invoke-static {p0}, Lcom/twitter/android/client/z;->a(Landroid/content/Context;)Lcom/twitter/android/client/z;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a([I)V

    .line 526
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 528
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/dogfood/c;->a(Landroid/content/Context;)V

    .line 535
    :cond_4
    :goto_2
    invoke-static {p0}, Lcom/twitter/util/ui/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    if-nez p1, :cond_5

    .line 536
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v8, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "app::::explorebytouch_enabled"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 541
    :cond_5
    new-instance v0, Lcom/twitter/android/geo/a;

    const-string/jumbo v1, "main_activity_location_dialog"

    iget-object v2, p0, Lcom/twitter/app/main/MainActivity;->q:Lcom/twitter/util/android/f;

    const/4 v3, 0x3

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/twitter/android/geo/a;-><init>(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Lcom/twitter/util/android/f;I)V

    iput-object v0, p0, Lcom/twitter/app/main/MainActivity;->N:Lcom/twitter/android/geo/a;

    .line 544
    invoke-static {}, Lbgd;->a()Lbgd;

    move-result-object v0

    invoke-virtual {v0}, Lbgd;->b()V

    .line 547
    new-instance v0, Lcmw;

    new-instance v1, Lado;

    new-instance v2, Lauh;

    .line 548
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-direct {v2, v3}, Lauh;-><init>(Landroid/content/ContentResolver;)V

    invoke-direct {v1, v2}, Lado;-><init>(Lauj;)V

    invoke-direct {v0, v1}, Lcmw;-><init>(Lcmv;)V

    .line 549
    new-instance v1, Lcmq;

    invoke-direct {v1, v0, v8, v9}, Lcmq;-><init>(Lcmu;J)V

    iput-object v1, p0, Lcom/twitter/app/main/MainActivity;->T:Lcmq;

    .line 551
    new-instance v0, Lcmw;

    new-instance v1, Ladq;

    new-instance v2, Lauh;

    .line 552
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-direct {v2, v3}, Lauh;-><init>(Landroid/content/ContentResolver;)V

    invoke-direct {v1, v2}, Ladq;-><init>(Lauj;)V

    invoke-direct {v0, v1}, Lcmw;-><init>(Lcmv;)V

    .line 553
    new-instance v1, Lcmq;

    invoke-direct {v1, v0, v8, v9}, Lcmq;-><init>(Lcmu;J)V

    iput-object v1, p0, Lcom/twitter/app/main/MainActivity;->U:Lcmq;

    .line 555
    new-instance v0, Lcmw;

    new-instance v1, Ladn;

    new-instance v2, Lauh;

    .line 556
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-direct {v2, v3}, Lauh;-><init>(Landroid/content/ContentResolver;)V

    invoke-direct {v1, v2}, Ladn;-><init>(Lauj;)V

    invoke-direct {v0, v1}, Lcmw;-><init>(Lcmv;)V

    .line 557
    new-instance v1, Lcmq;

    invoke-direct {v1, v0, v8, v9}, Lcmq;-><init>(Lcmu;J)V

    iput-object v1, p0, Lcom/twitter/app/main/MainActivity;->V:Lcmq;

    .line 559
    const-string/jumbo v0, "android_avatar_badging_5571"

    invoke-static {v0}, Lcoi;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 560
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/main/MainActivity;->X:Ljava/util/Map;

    .line 561
    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->t()V

    .line 564
    :cond_6
    const-string/jumbo v0, "android_pending_followers_navigation_5203"

    invoke-static {v0}, Lcoi;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 565
    new-instance v0, Lcmw;

    new-instance v1, Ladp;

    new-instance v2, Lauh;

    .line 566
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-direct {v2, v3}, Lauh;-><init>(Landroid/content/ContentResolver;)V

    invoke-direct {v1, v2}, Ladp;-><init>(Lauj;)V

    invoke-direct {v0, v1}, Lcmw;-><init>(Lcmv;)V

    .line 567
    new-instance v1, Lcmq;

    invoke-direct {v1, v0, v8, v9}, Lcmq;-><init>(Lcmu;J)V

    iput-object v1, p0, Lcom/twitter/app/main/MainActivity;->W:Lcmq;

    .line 570
    :cond_7
    new-instance v0, Lcom/twitter/app/main/b;

    const v1, 0x7f130476

    invoke-virtual {p0, v1}, Lcom/twitter/app/main/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/app/main/b;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/twitter/app/main/MainActivity;->Q:Lcom/twitter/app/main/b;

    .line 571
    new-instance v1, Lcom/twitter/app/main/a;

    const v0, 0x7f130478

    invoke-virtual {p0, v0}, Lcom/twitter/app/main/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {v1, v0}, Lcom/twitter/app/main/a;-><init>(Landroid/view/ViewGroup;)V

    iput-object v1, p0, Lcom/twitter/app/main/MainActivity;->S:Lcom/twitter/app/main/a;

    .line 573
    invoke-static {p0}, Lcom/twitter/app/lists/d;->a(Lcom/twitter/app/common/base/TwitterFragmentActivity;)Lcom/twitter/app/lists/d;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/main/MainActivity;->Y:Lcom/twitter/app/lists/d;

    .line 574
    return-void

    .line 509
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 515
    :cond_9
    sget-object v0, Lcom/twitter/app/main/MainActivity;->b:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/twitter/app/main/MainActivity;->d(Landroid/net/Uri;)V

    goto/16 :goto_1

    .line 531
    :cond_a
    const-string/jumbo v0, "alreadyCheckedExpiredDrafts"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/main/MainActivity;->z:Z

    goto/16 :goto_2
.end method

.method protected b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1509
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "home"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "navigation_bar"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object p1, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    .line 1510
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1509
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1511
    return-void
.end method

.method protected c(Lcom/twitter/library/client/m;)V
    .locals 1

    .prologue
    .line 1520
    invoke-static {p1}, Lcom/twitter/app/main/MainActivity;->e(Lcom/twitter/library/client/m;)Ljava/lang/String;

    move-result-object v0

    .line 1521
    if-eqz v0, :cond_0

    .line 1522
    invoke-virtual {p0, v0}, Lcom/twitter/app/main/MainActivity;->b(Ljava/lang/String;)V

    .line 1524
    :cond_0
    return-void
.end method

.method protected d()V
    .locals 3

    .prologue
    .line 972
    invoke-super {p0}, Lcom/twitter/android/ActivityWithProgress;->d()V

    .line 973
    invoke-static {p0}, Lcom/twitter/library/platform/notifications/PushRegistration;->b(Landroid/content/Context;)V

    .line 974
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->L:Lcom/twitter/app/main/MainActivity$g;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/u;)V

    .line 975
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->w:Lcom/twitter/library/client/e;

    if-eqz v0, :cond_0

    .line 976
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->w:Lcom/twitter/library/client/e;

    invoke-static {v0}, Lcom/twitter/android/client/AppBroadcastReceiver;->b(Lcom/twitter/library/client/e;)V

    .line 978
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->G:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->E:Lcom/twitter/library/service/t;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->b(Lcom/twitter/library/service/t;)V

    .line 979
    iget-boolean v0, p0, Lcom/twitter/app/main/MainActivity;->x:Z

    if-eqz v0, :cond_1

    .line 980
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/accounts/AccountManager;->removeOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;)V

    .line 982
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->c()Lcmr;

    move-result-object v0

    .line 983
    if-eqz v0, :cond_2

    .line 984
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcmr;->a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/UserSettings;)V

    .line 988
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->T:Lcmq;

    if-eqz v0, :cond_3

    .line 989
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->T:Lcmq;

    invoke-virtual {v0}, Lcmq;->a()V

    .line 991
    :cond_3
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->U:Lcmq;

    if-eqz v0, :cond_4

    .line 992
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->U:Lcmq;

    invoke-virtual {v0}, Lcmq;->a()V

    .line 994
    :cond_4
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->V:Lcmq;

    if-eqz v0, :cond_5

    .line 995
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->V:Lcmq;

    invoke-virtual {v0}, Lcmq;->a()V

    .line 997
    :cond_5
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->W:Lcmq;

    if-eqz v0, :cond_6

    .line 998
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->W:Lcmq;

    invoke-virtual {v0}, Lcmq;->a()V

    .line 1001
    :cond_6
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->X:Ljava/util/Map;

    if-eqz v0, :cond_8

    .line 1002
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->X:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmq;

    .line 1003
    invoke-virtual {v0}, Lcmq;->a()V

    goto :goto_0

    .line 1005
    :cond_7
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->X:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1008
    :cond_8
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->Y:Lcom/twitter/app/lists/d;

    invoke-virtual {v0}, Lcom/twitter/app/lists/d;->a()V

    .line 1009
    return-void
.end method

.method public e_()V
    .locals 2

    .prologue
    .line 1683
    invoke-super {p0}, Lcom/twitter/android/ActivityWithProgress;->e_()V

    .line 1684
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->I:Lcom/twitter/android/composer/ComposerDockLayout;

    invoke-virtual {v0}, Lcom/twitter/android/composer/ComposerDockLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/main/MainActivity$5;

    invoke-direct {v1, p0}, Lcom/twitter/app/main/MainActivity$5;-><init>(Lcom/twitter/app/main/MainActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1701
    return-void
.end method

.method public i()Lcom/twitter/app/common/list/TwitterListFragment;
    .locals 2

    .prologue
    .line 909
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->l:Lcom/twitter/app/main/MainActivity$f;

    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->k:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/main/MainActivity$f;->a(I)Lcom/twitter/library/client/m;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/main/MainActivity;->b(Lcom/twitter/library/client/m;)Lcom/twitter/app/common/list/TwitterListFragment;

    move-result-object v0

    return-object v0
.end method

.method j()V
    .locals 2

    .prologue
    .line 913
    const-wide/16 v0, 0x7530

    invoke-direct {p0, v0, v1}, Lcom/twitter/app/main/MainActivity;->c(J)V

    .line 914
    const-wide/16 v0, 0x3a98

    invoke-direct {p0, v0, v1}, Lcom/twitter/app/main/MainActivity;->a(J)V

    .line 915
    const-wide/16 v0, 0x1388

    invoke-direct {p0, v0, v1}, Lcom/twitter/app/main/MainActivity;->b(J)V

    .line 916
    return-void
.end method

.method public j_()Lcom/twitter/android/AbsPagesAdapter;
    .locals 1

    .prologue
    .line 2104
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->l:Lcom/twitter/app/main/MainActivity$f;

    return-object v0
.end method

.method public k_()V
    .locals 2

    .prologue
    .line 1705
    invoke-super {p0}, Lcom/twitter/android/ActivityWithProgress;->k_()V

    .line 1706
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->I:Lcom/twitter/android/composer/ComposerDockLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/ComposerDockLayout;->setTopLocked(Z)V

    .line 1707
    return-void
.end method

.method public l()V
    .locals 3

    .prologue
    .line 1295
    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->ae()Landroid/content/Intent;

    move-result-object v0

    .line 1296
    const-string/jumbo v1, "AccountsDialogActivity_new_account"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1297
    invoke-direct {p0, v0}, Lcom/twitter/app/main/MainActivity;->a(Landroid/content/Intent;)V

    .line 1298
    return-void
.end method

.method public n()V
    .locals 3

    .prologue
    .line 1301
    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->ae()Landroid/content/Intent;

    move-result-object v0

    .line 1302
    const-string/jumbo v1, "AccountsDialogActivity_add_account"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1303
    invoke-direct {p0, v0}, Lcom/twitter/app/main/MainActivity;->a(Landroid/content/Intent;)V

    .line 1304
    return-void
.end method

.method o()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1310
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->V:Lcmq;

    if-eqz v0, :cond_0

    .line 1311
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->V:Lcmq;

    invoke-virtual {v0, v2}, Lcmq;->a(I)V

    .line 1313
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->T:Lcmq;

    if-eqz v0, :cond_1

    .line 1314
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->T:Lcmq;

    invoke-virtual {v0, v2}, Lcmq;->a(I)V

    .line 1316
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->W:Lcmq;

    if-eqz v0, :cond_2

    .line 1317
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->W:Lcmq;

    invoke-virtual {v0, v2}, Lcmq;->a(I)V

    .line 1320
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/m;

    .line 1321
    iput v2, v0, Lcom/twitter/library/client/m;->i:I

    goto :goto_0

    .line 1323
    :cond_3
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->B:Lcom/twitter/app/main/MainActivity$d;

    invoke-virtual {v0}, Lcom/twitter/app/main/MainActivity$d;->notifyDataSetChanged()V

    .line 1324
    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->af()V

    .line 1325
    return-void
.end method

.method public onAccountsUpdated([Landroid/accounts/Account;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 1205
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->isDestroyed()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1232
    :cond_0
    :goto_0
    return-void

    .line 1213
    :cond_1
    array-length v6, p1

    move v3, v0

    move v1, v0

    move v2, v0

    move-object v0, v4

    :goto_1
    if-ge v3, v6, :cond_4

    aget-object v5, p1, v3

    .line 1214
    sget-object v7, Lcom/twitter/library/util/b;->a:Ljava/lang/String;

    iget-object v8, v5, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1215
    add-int/lit8 v2, v2, 0x1

    .line 1216
    if-nez v0, :cond_2

    move-object v0, v5

    .line 1219
    :cond_2
    if-nez v1, :cond_3

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v7, p0, Lcom/twitter/app/main/MainActivity;->i:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1220
    const/4 v1, 0x1

    .line 1213
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1226
    :cond_4
    if-nez v1, :cond_0

    if-lez v2, :cond_0

    .line 1229
    iput-object v4, p0, Lcom/twitter/app/main/MainActivity;->i:Ljava/lang/String;

    .line 1230
    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->C()V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 1236
    packed-switch p1, :pswitch_data_0

    .line 1272
    :pswitch_0
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/ActivityWithProgress;->onActivityResult(IILandroid/content/Intent;)V

    .line 1276
    :cond_0
    :goto_0
    return-void

    .line 1238
    :pswitch_1
    if-ne p2, v6, :cond_0

    .line 1241
    const-string/jumbo v0, "account"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/account/UserAccount;

    .line 1242
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->i:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->i:Ljava/lang/String;

    iget-object v2, v0, Lcom/twitter/model/account/UserAccount;->a:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1245
    iget-object v0, v0, Lcom/twitter/model/account/UserAccount;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/app/main/MainActivity;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 1251
    :pswitch_2
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1252
    const-string/jumbo v0, "is_last"

    invoke-virtual {p3, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1253
    invoke-static {p0}, Lcom/twitter/android/DispatchActivity;->a(Landroid/app/Activity;)V

    goto :goto_0

    .line 1258
    :cond_1
    invoke-static {}, Lcom/twitter/library/util/b;->a()I

    move-result v0

    if-le v0, v6, :cond_0

    .line 1259
    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->ad()V

    goto :goto_0

    .line 1267
    :pswitch_3
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->h:Lcom/twitter/app/main/MainActivity$a;

    const-wide/16 v2, 0x0

    iget-object v5, p0, Lcom/twitter/app/main/MainActivity;->K:Lcom/twitter/app/main/MainActivity$i;

    new-array v6, v6, [I

    const/4 v0, 0x2

    aput v0, v6, v4

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/app/main/MainActivity$a;->a(JILcom/twitter/app/main/MainActivity$i;[I)V

    goto :goto_0

    .line 1236
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onAttachedToWindow()V
    .locals 2

    .prologue
    .line 1807
    invoke-super {p0}, Lcom/twitter/android/ActivityWithProgress;->onAttachedToWindow()V

    .line 1808
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 1809
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setFormat(I)V

    .line 1810
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 1013
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1015
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->g()Z

    .line 1027
    :goto_0
    return-void

    .line 1018
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->l:Lcom/twitter/app/main/MainActivity$f;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->P()Lcom/twitter/android/search/SearchSuggestionController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/search/SearchSuggestionController;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1020
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->l:Lcom/twitter/app/main/MainActivity$f;

    sget-object v1, Lcom/twitter/app/main/MainActivity;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/twitter/app/main/MainActivity$f;->a(Landroid/net/Uri;)I

    move-result v0

    .line 1021
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->k:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-eq v1, v0, :cond_1

    .line 1022
    sget-object v0, Lcom/twitter/app/main/MainActivity;->b:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/twitter/app/main/MainActivity;->d(Landroid/net/Uri;)V

    goto :goto_0

    .line 1026
    :cond_1
    invoke-super {p0}, Lcom/twitter/android/ActivityWithProgress;->onBackPressed()V

    goto :goto_0
.end method

.method public onContentChanged()V
    .locals 1

    .prologue
    .line 656
    invoke-super {p0}, Lcom/twitter/android/ActivityWithProgress;->onContentChanged()V

    .line 657
    const v0, 0x7f130378

    invoke-virtual {p0, v0}, Lcom/twitter/app/main/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/twitter/app/main/MainActivity;->k:Landroid/support/v4/view/ViewPager;

    .line 658
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 1031
    invoke-super {p0, p1}, Lcom/twitter/android/ActivityWithProgress;->onNewIntent(Landroid/content/Intent;)V

    .line 1032
    invoke-virtual {p0, p1}, Lcom/twitter/app/main/MainActivity;->setIntent(Landroid/content/Intent;)V

    .line 1033
    invoke-direct {p0, p1}, Lcom/twitter/app/main/MainActivity;->b(Landroid/content/Intent;)V

    .line 1034
    return-void
.end method

.method protected onPause()V
    .locals 4

    .prologue
    .line 942
    invoke-super {p0}, Lcom/twitter/android/ActivityWithProgress;->onPause()V

    .line 943
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->r:Lcoj$a;

    invoke-static {v0}, Lcoj;->b(Lcoj$a;)V

    .line 946
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->u:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 947
    const-string/jumbo v0, "ver"

    const/4 v2, 0x6

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 948
    const-string/jumbo v0, "version_code"

    sget v2, Lcom/twitter/app/main/MainActivity;->o:I

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 949
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->l:Lcom/twitter/app/main/MainActivity$f;

    invoke-virtual {v0}, Lcom/twitter/app/main/MainActivity$f;->c()Landroid/net/Uri;

    move-result-object v0

    .line 950
    const-string/jumbo v2, "tag"

    if-eqz v0, :cond_1

    .line 951
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 950
    :goto_0
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 952
    const-string/jumbo v0, "st"

    iget-wide v2, p0, Lcom/twitter/app/main/MainActivity;->s:J

    invoke-interface {v1, v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 953
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 956
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->c()Lcmr;

    move-result-object v0

    .line 957
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcmr;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 958
    invoke-interface {v0}, Lcmr;->e()Z

    .line 960
    :cond_0
    return-void

    .line 951
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 2

    .prologue
    .line 826
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 827
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    const-string/jumbo v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-virtual {v0, v1, p2, p3}, Lcom/twitter/util/android/f;->a(Ljava/lang/String;[Ljava/lang/String;[I)Z

    move-result v0

    .line 829
    if-nez v0, :cond_0

    .line 830
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbqg;->a(Z)V

    .line 833
    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 632
    invoke-super {p0, p1}, Lcom/twitter/android/ActivityWithProgress;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 633
    const-string/jumbo v0, "currentTab"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 634
    if-eqz v0, :cond_0

    .line 635
    invoke-direct {p0, v0}, Lcom/twitter/app/main/MainActivity;->d(Landroid/net/Uri;)V

    .line 637
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const/4 v10, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 694
    invoke-super {p0}, Lcom/twitter/android/ActivityWithProgress;->onResume()V

    .line 695
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->A:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 696
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->A:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/app/main/MainActivity;->c(Ljava/lang/String;)V

    .line 697
    iput-object v10, p0, Lcom/twitter/app/main/MainActivity;->A:Ljava/lang/String;

    .line 700
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->r:Lcoj$a;

    invoke-static {v0}, Lcoj;->a(Lcoj$a;)V

    .line 703
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v3

    .line 704
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->i:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 705
    invoke-virtual {v3}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/twitter/library/client/v;->d(Ljava/lang/String;)V

    .line 707
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->v:Lcom/twitter/android/client/i;

    invoke-virtual {v0}, Lcom/twitter/android/client/i;->a()V

    .line 709
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 711
    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 712
    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 714
    sget v4, Lcom/twitter/app/main/MainActivity;->o:I

    if-nez v4, :cond_7

    invoke-static {}, Lcom/twitter/android/ContactsUploadService;->a()J

    move-result-wide v4

    cmp-long v4, v4, v12

    if-nez v4, :cond_7

    .line 717
    iget-object v4, p0, Lcom/twitter/app/main/MainActivity;->M:Lcom/twitter/android/client/l;

    invoke-virtual {v4}, Lcom/twitter/android/client/l;->a()V

    .line 728
    :cond_2
    :goto_0
    sput v0, Lcom/twitter/app/main/MainActivity;->o:I

    .line 729
    const-class v0, Lcom/twitter/app/main/MainActivity;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 735
    :goto_1
    sget v0, Lcom/twitter/app/main/MainActivity;->n:I

    if-nez v0, :cond_d

    .line 736
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->u:Landroid/content/SharedPreferences;

    const-string/jumbo v4, "ver"

    invoke-interface {v0, v4, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 737
    if-nez v0, :cond_9

    .line 738
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->R()Lbqn;

    move-result-object v0

    invoke-virtual {v0}, Lbqn;->c()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 739
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->p()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 740
    const-string/jumbo v0, "debug_prefs"

    invoke-virtual {p0, v0, v2}, Lcom/twitter/app/main/MainActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 742
    const-string/jumbo v4, "suppress_location_dialogs"

    invoke-interface {v0, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_c

    move v0, v1

    .line 762
    :goto_2
    const/4 v4, 0x6

    sput v4, Lcom/twitter/app/main/MainActivity;->n:I

    .line 763
    const-class v4, Lcom/twitter/app/main/MainActivity;

    invoke-static {v4}, Lcru;->a(Ljava/lang/Class;)V

    .line 765
    :goto_3
    const-string/jumbo v4, "location_fatigue"

    iget-wide v6, p0, Lcom/twitter/app/main/MainActivity;->F:J

    .line 766
    invoke-static {p0, v4, v6, v7}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v4

    .line 767
    if-eqz v0, :cond_b

    invoke-virtual {v4}, Lcom/twitter/android/util/h;->a()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 768
    invoke-virtual {v4}, Lcom/twitter/android/util/h;->b()V

    .line 769
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->N:Lcom/twitter/android/geo/a;

    invoke-virtual {v0, p0}, Lcom/twitter/android/geo/a;->a(Lcom/twitter/android/geo/a$a;)V

    .line 770
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->N:Lcom/twitter/android/geo/a;

    const/4 v4, 0x5

    invoke-virtual {v0, v4}, Lcom/twitter/android/geo/a;->a(I)V

    .line 780
    :goto_4
    iget-boolean v0, p0, Lcom/twitter/app/main/MainActivity;->O:Z

    if-eqz v0, :cond_3

    .line 781
    iput-boolean v2, p0, Lcom/twitter/app/main/MainActivity;->O:Z

    .line 782
    invoke-static {}, Lbqg;->a()Lbqg;

    move-result-object v0

    invoke-virtual {v0}, Lbqg;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 783
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->q:Lcom/twitter/util/android/f;

    const/4 v4, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v5, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v5, v1, v2

    invoke-virtual {v0, v4, p0, v1}, Lcom/twitter/util/android/f;->a(ILandroid/app/Activity;[Ljava/lang/String;)V

    .line 789
    :cond_3
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->u:Landroid/content/SharedPreferences;

    .line 790
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v4

    .line 793
    const-string/jumbo v1, "st"

    invoke-interface {v0, v1, v12, v13}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/app/main/MainActivity;->s:J

    .line 794
    invoke-virtual {v3}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 795
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-wide v6, p0, Lcom/twitter/app/main/MainActivity;->s:J

    const-wide/32 v8, 0x36ee80

    add-long/2addr v6, v8

    cmp-long v1, v6, v4

    if-gez v1, :cond_5

    .line 796
    :cond_4
    invoke-static {p0}, Lcom/twitter/android/client/y;->a(Landroid/content/Context;)Lcom/twitter/android/client/y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/client/y;->a()V

    .line 797
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    .line 798
    invoke-static {p0, v0}, Lbbg;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lbbg;

    move-result-object v3

    .line 797
    invoke-virtual {v1, v3, v10}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 799
    iput-wide v4, p0, Lcom/twitter/app/main/MainActivity;->s:J

    .line 802
    :cond_5
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/twitter/app/main/MainActivity;->d(J)V

    .line 804
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 805
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/vineloops/a;->a(Landroid/content/Context;Lcom/twitter/library/client/p;)Lcom/twitter/library/vineloops/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/vineloops/a;->a()V

    .line 807
    invoke-static {}, Lcom/twitter/library/client/d;->a()Lcom/twitter/library/client/d;

    move-result-object v1

    new-instance v3, Lcom/twitter/library/client/AppResumeEvent;

    invoke-direct {v3, v0}, Lcom/twitter/library/client/AppResumeEvent;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Lcom/twitter/library/client/d;->a(Ljava/lang/Object;)V

    .line 809
    iget-boolean v0, p0, Lcom/twitter/app/main/MainActivity;->y:Z

    if-eqz v0, :cond_6

    .line 810
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->s()V

    .line 811
    iput-boolean v2, p0, Lcom/twitter/app/main/MainActivity;->y:Z

    .line 814
    :cond_6
    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->B()V

    .line 815
    return-void

    .line 718
    :cond_7
    :try_start_1
    sget v4, Lcom/twitter/app/main/MainActivity;->o:I

    if-le v0, v4, :cond_2

    .line 720
    const-string/jumbo v4, "legacy_deciders_find_friends_interval_sec"

    const v5, 0xed4e00

    .line 721
    invoke-static {v4, v5}, Lcoj;->a(Ljava/lang/String;I)I

    move-result v4

    int-to-long v4, v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    .line 723
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v6

    .line 724
    invoke-static {}, Lcom/twitter/android/ContactsUploadService;->a()J

    move-result-wide v8

    add-long/2addr v4, v8

    cmp-long v4, v6, v4

    if-lez v4, :cond_2

    .line 725
    iget-object v4, p0, Lcom/twitter/app/main/MainActivity;->M:Lcom/twitter/android/client/l;

    invoke-virtual {v4}, Lcom/twitter/android/client/l;->a()V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 730
    :catch_0
    move-exception v0

    goto/16 :goto_1

    :cond_8
    move v0, v1

    .line 746
    goto/16 :goto_2

    .line 749
    :cond_9
    if-eq v0, v1, :cond_a

    const/4 v4, 0x2

    if-ne v0, v4, :cond_c

    .line 757
    :cond_a
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->u:Landroid/content/SharedPreferences;

    const-string/jumbo v4, "suppress_location_dialogs"

    invoke-interface {v0, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_c

    .line 758
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->R()Lbqn;

    move-result-object v0

    invoke-virtual {v0}, Lbqn;->c()Z

    move-result v0

    if-eqz v0, :cond_c

    move v0, v1

    .line 759
    goto/16 :goto_2

    .line 775
    :cond_b
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->D:Lcom/twitter/android/util/m;

    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/app/main/MainActivity;->v:Lcom/twitter/android/client/i;

    invoke-virtual {v0, v4, v5}, Lcom/twitter/android/util/m;->a(Lcom/twitter/internal/android/widget/ToolBar;Lcom/twitter/android/client/i;)V

    goto/16 :goto_4

    :cond_c
    move v0, v2

    goto/16 :goto_2

    :cond_d
    move v0, v2

    goto/16 :goto_3
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 646
    invoke-super {p0, p1}, Lcom/twitter/android/ActivityWithProgress;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 647
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->l:Lcom/twitter/app/main/MainActivity$f;

    invoke-virtual {v0}, Lcom/twitter/app/main/MainActivity$f;->c()Landroid/net/Uri;

    move-result-object v0

    .line 648
    if-eqz v0, :cond_0

    .line 649
    const-string/jumbo v1, "currentTab"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 651
    :cond_0
    const-string/jumbo v0, "alreadyCheckedExpiredDrafts"

    iget-boolean v1, p0, Lcom/twitter/app/main/MainActivity;->z:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 652
    return-void
.end method

.method public onSearchRequested()Z
    .locals 2

    .prologue
    .line 1814
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/twitter/app/main/MainActivity;->c(J)V

    .line 1815
    invoke-super {p0}, Lcom/twitter/android/ActivityWithProgress;->onSearchRequested()Z

    move-result v0

    return v0
.end method

.method protected onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 662
    invoke-super {p0}, Lcom/twitter/android/ActivityWithProgress;->onStart()V

    .line 663
    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->af()V

    .line 664
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->O()Lcom/twitter/metrics/j;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/twitter/android/metrics/a;->a(Lcom/twitter/metrics/j;J)Lcom/twitter/android/metrics/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/metrics/a;->j()V

    .line 666
    invoke-static {}, Lbuo;->a()Lbuo;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/app/main/MainActivity;->F:J

    invoke-virtual {v0, v2, v3}, Lbuo;->a(J)V

    .line 667
    invoke-static {p0}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 668
    invoke-static {p0}, Lcom/google/android/gcm/b;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 669
    invoke-static {v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 670
    invoke-static {p0}, Lcom/twitter/library/platform/notifications/PushRegistration;->a(Landroid/content/Context;)V

    .line 681
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->a:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "has_completed_signin_flow"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 682
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "has_completed_signin_flow"

    .line 683
    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 684
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 688
    :cond_1
    invoke-direct {p0}, Lcom/twitter/app/main/MainActivity;->z()V

    .line 689
    return-void

    .line 671
    :cond_2
    iget-wide v2, p0, Lcom/twitter/app/main/MainActivity;->F:J

    invoke-static {p0, v2, v3}, Lcom/twitter/library/platform/notifications/PushRegistration;->a(Landroid/content/Context;J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 672
    new-instance v0, Lbbf;

    .line 673
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lbbf;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 674
    invoke-virtual {v0, v4}, Lbbf;->d(I)Lcom/twitter/library/service/j;

    move-result-object v0

    check-cast v0, Lbbf;

    .line 675
    iput-object v1, v0, Lbbf;->a:Ljava/lang/String;

    .line 676
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity;->G:Lcom/twitter/library/client/p;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    goto :goto_0
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 964
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->P:Lrx/j;

    if-eqz v0, :cond_0

    .line 965
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->P:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 967
    :cond_0
    invoke-super {p0}, Lcom/twitter/android/ActivityWithProgress;->onStop()V

    .line 968
    return-void
.end method

.method public q()Lcom/twitter/app/main/b;
    .locals 1

    .prologue
    .line 1797
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->Q:Lcom/twitter/app/main/b;

    return-object v0
.end method

.method public r()Lcom/twitter/app/main/a;
    .locals 1

    .prologue
    .line 1802
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->S:Lcom/twitter/app/main/a;

    return-object v0
.end method

.method public s()V
    .locals 1

    .prologue
    .line 1819
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->i()Lcom/twitter/app/common/list/TwitterListFragment;

    move-result-object v0

    .line 1820
    if-eqz v0, :cond_0

    .line 1821
    invoke-virtual {v0}, Lcom/twitter/app/common/list/TwitterListFragment;->aL()V

    .line 1822
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->I:Lcom/twitter/android/composer/ComposerDockLayout;

    if-eqz v0, :cond_0

    .line 1823
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity;->I:Lcom/twitter/android/composer/ComposerDockLayout;

    invoke-virtual {v0}, Lcom/twitter/android/composer/ComposerDockLayout;->b()V

    .line 1826
    :cond_0
    return-void
.end method

.method public x()Ljava/lang/String;
    .locals 2

    .prologue
    .line 890
    invoke-virtual {p0}, Lcom/twitter/app/main/MainActivity;->i()Lcom/twitter/app/common/list/TwitterListFragment;

    move-result-object v0

    .line 891
    instance-of v1, v0, Lcom/twitter/android/dogfood/a$b;

    if-eqz v1, :cond_0

    .line 892
    check-cast v0, Lcom/twitter/android/dogfood/a$b;

    invoke-interface {v0}, Lcom/twitter/android/dogfood/a$b;->x()Ljava/lang/String;

    move-result-object v0

    .line 894
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
