.class Lcom/twitter/app/main/MainActivity$c;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/main/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/main/MainActivity;


# direct methods
.method constructor <init>(Lcom/twitter/app/main/MainActivity;)V
    .locals 0

    .prologue
    .line 2304
    iput-object p1, p0, Lcom/twitter/app/main/MainActivity$c;->a:Lcom/twitter/app/main/MainActivity;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    .line 2305
    return-void
.end method

.method private a(Lcom/twitter/library/api/upload/w;Lcom/twitter/async/service/j;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/api/upload/w;",
            "Lcom/twitter/async/service/j",
            "<",
            "Lcom/twitter/library/service/u;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 2330
    invoke-virtual {p2}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 2332
    invoke-virtual {p1}, Lcom/twitter/library/api/upload/w;->L()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 2345
    :cond_0
    :goto_0
    return-void

    .line 2335
    :pswitch_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    .line 2337
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$c;->a:Lcom/twitter/app/main/MainActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/app/main/MainActivity;->b(Lcom/twitter/app/main/MainActivity;I)V

    goto :goto_0

    .line 2332
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 2303
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/app/main/MainActivity$c;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 3

    .prologue
    .line 2309
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$c;->a:Lcom/twitter/app/main/MainActivity;

    invoke-static {v0}, Lcom/twitter/app/main/MainActivity;->m(Lcom/twitter/app/main/MainActivity;)Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 2310
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    .line 2311
    if-eqz v0, :cond_2

    invoke-virtual {v0, v2}, Lcom/twitter/library/service/v;->a(Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 2313
    :goto_0
    instance-of v0, p1, Lbfr;

    if-nez v0, :cond_0

    instance-of v0, p1, Lbgt;

    if-eqz v0, :cond_3

    .line 2314
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2315
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$c;->a:Lcom/twitter/app/main/MainActivity;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/twitter/app/main/MainActivity$c;->a:Lcom/twitter/app/main/MainActivity;

    invoke-static {v2}, Lcom/twitter/app/main/MainActivity;->n(Lcom/twitter/app/main/MainActivity;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/twitter/library/platform/TwitterDataSyncService;->a(Landroid/content/Context;Landroid/os/Bundle;Lcom/twitter/library/client/Session;)V

    .line 2326
    :cond_1
    :goto_1
    return-void

    .line 2311
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 2317
    :cond_3
    instance-of v0, p1, Lcom/twitter/library/api/upload/w;

    if-eqz v0, :cond_4

    move-object v0, p1

    .line 2318
    check-cast v0, Lcom/twitter/library/api/upload/w;

    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v2

    invoke-direct {p0, v0, v2, v1}, Lcom/twitter/app/main/MainActivity$c;->a(Lcom/twitter/library/api/upload/w;Lcom/twitter/async/service/j;Z)V

    goto :goto_1

    .line 2319
    :cond_4
    instance-of v0, p1, Lbbg;

    if-eqz v0, :cond_1

    .line 2320
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 2321
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2322
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$c;->a:Lcom/twitter/app/main/MainActivity;

    invoke-virtual {v0}, Lcom/twitter/app/main/MainActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcmt;->a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/account/UserSettings;)V

    .line 2323
    iget-object v0, p0, Lcom/twitter/app/main/MainActivity$c;->a:Lcom/twitter/app/main/MainActivity;

    invoke-virtual {v0}, Lcom/twitter/app/main/MainActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->h()V

    goto :goto_1
.end method
