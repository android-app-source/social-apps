.class public Lcom/twitter/app/main/b;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/twitter/app/main/b;->a:Landroid/content/Context;

    .line 19
    iput-object p2, p0, Lcom/twitter/app/main/b;->b:Landroid/view/View;

    .line 20
    return-void
.end method


# virtual methods
.method public a(IIILandroid/view/View$OnClickListener;)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/app/main/b;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/app/main/b;->b:Landroid/view/View;

    invoke-static {v0, v1, p1, p2}, Lcom/twitter/ui/widget/f;->a(Landroid/content/Context;Landroid/view/View;II)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 44
    if-lez p3, :cond_0

    if-eqz p4, :cond_0

    .line 45
    invoke-virtual {v0, p3, p4}, Landroid/support/design/widget/Snackbar;->setAction(ILandroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    .line 47
    :cond_0
    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->show()V

    .line 48
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/app/main/b;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/app/main/b;->b:Landroid/view/View;

    invoke-static {v0, v1, p1, p2}, Lcom/twitter/ui/widget/f;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;I)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->show()V

    .line 58
    return-void
.end method
