.class Lcom/twitter/app/main/MainActivity$5;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/main/MainActivity;->e_()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/main/MainActivity;


# direct methods
.method constructor <init>(Lcom/twitter/app/main/MainActivity;)V
    .locals 0

    .prologue
    .line 1684
    iput-object p1, p0, Lcom/twitter/app/main/MainActivity$5;->a:Lcom/twitter/app/main/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1692
    iget-object v2, p0, Lcom/twitter/app/main/MainActivity$5;->a:Lcom/twitter/app/main/MainActivity;

    invoke-virtual {v2}, Lcom/twitter/app/main/MainActivity;->P()Lcom/twitter/android/search/SearchSuggestionController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/search/SearchSuggestionController;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1693
    iget-object v2, p0, Lcom/twitter/app/main/MainActivity$5;->a:Lcom/twitter/app/main/MainActivity;

    invoke-static {v2}, Lcom/twitter/app/main/MainActivity;->a(Lcom/twitter/app/main/MainActivity;)Lcom/twitter/android/composer/ComposerDockLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/composer/ComposerDockLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1694
    iget-object v2, p0, Lcom/twitter/app/main/MainActivity$5;->a:Lcom/twitter/app/main/MainActivity;

    invoke-static {v2}, Lcom/twitter/app/main/MainActivity;->b(Lcom/twitter/app/main/MainActivity;)Lcom/twitter/android/composer/ComposerDockLayout;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/twitter/android/composer/ComposerDockLayout;->a(I)V

    .line 1695
    iget-object v1, p0, Lcom/twitter/app/main/MainActivity$5;->a:Lcom/twitter/app/main/MainActivity;

    invoke-static {v1}, Lcom/twitter/app/main/MainActivity;->c(Lcom/twitter/app/main/MainActivity;)Lcom/twitter/android/composer/ComposerDockLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/ComposerDockLayout;->setTopLocked(Z)V

    .line 1698
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
