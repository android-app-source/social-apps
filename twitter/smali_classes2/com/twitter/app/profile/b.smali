.class public final Lcom/twitter/app/profile/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/profile/f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/profile/b$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcnz;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/metrics/j;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/metrics/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/twitter/app/profile/b;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/app/profile/b;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/twitter/app/profile/b$a;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    sget-boolean v0, Lcom/twitter/app/profile/b;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 33
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/app/profile/b;->a(Lcom/twitter/app/profile/b$a;)V

    .line 34
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/app/profile/b$a;Lcom/twitter/app/profile/b$1;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/twitter/app/profile/b;-><init>(Lcom/twitter/app/profile/b$a;)V

    return-void
.end method

.method private a(Lcom/twitter/app/profile/b$a;)V
    .locals 2

    .prologue
    .line 43
    new-instance v0, Lcom/twitter/app/profile/b$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/app/profile/b$1;-><init>(Lcom/twitter/app/profile/b;Lcom/twitter/app/profile/b$a;)V

    iput-object v0, p0, Lcom/twitter/app/profile/b;->b:Lcta;

    .line 56
    new-instance v0, Lcom/twitter/app/profile/b$2;

    invoke-direct {v0, p0, p1}, Lcom/twitter/app/profile/b$2;-><init>(Lcom/twitter/app/profile/b;Lcom/twitter/app/profile/b$a;)V

    iput-object v0, p0, Lcom/twitter/app/profile/b;->c:Lcta;

    .line 69
    iget-object v0, p0, Lcom/twitter/app/profile/b;->b:Lcta;

    iget-object v1, p0, Lcom/twitter/app/profile/b;->c:Lcta;

    .line 71
    invoke-static {v0, v1}, Lasg;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 70
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/profile/b;->d:Lcta;

    .line 73
    return-void
.end method

.method public static c()Lcom/twitter/app/profile/b$a;
    .locals 2

    .prologue
    .line 37
    new-instance v0, Lcom/twitter/app/profile/b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/app/profile/b$a;-><init>(Lcom/twitter/app/profile/b$1;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/android/metrics/b;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/app/profile/b;->d:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/metrics/b;

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    invoke-static {}, Ldagger/internal/f;->a()Ldagger/internal/c;

    move-result-object v0

    invoke-interface {v0}, Ldagger/internal/c;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method
