.class final Lcom/twitter/app/profile/ProfileTimelinesFragment$a;
.super Lcom/twitter/app/common/timeline/TimelineFragment$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/profile/ProfileTimelinesFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/support/v4/app/Fragment;


# direct methods
.method constructor <init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/revenue/c;Lcom/twitter/android/timeline/be;I)V
    .locals 0

    .prologue
    .line 531
    invoke-direct/range {p0 .. p5}, Lcom/twitter/app/common/timeline/TimelineFragment$a;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/revenue/c;Lcom/twitter/android/timeline/be;I)V

    .line 532
    iput-object p1, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment$a;->a:Landroid/support/v4/app/Fragment;

    .line 533
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/TweetActionType;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/library/widget/h;ZLcom/twitter/android/timeline/bk;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 539
    invoke-super/range {p0 .. p8}, Lcom/twitter/app/common/timeline/TimelineFragment$a;->a(Lcom/twitter/model/core/TweetActionType;Lcom/twitter/model/core/Tweet;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeItem;Lcom/twitter/library/widget/h;ZLcom/twitter/android/timeline/bk;Ljava/lang/String;)V

    .line 541
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment$a;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/android/ProfileActivity;

    if-eqz v0, :cond_0

    .line 542
    sget-object v0, Lcom/twitter/app/profile/ProfileTimelinesFragment$1;->a:[I

    invoke-virtual {p1}, Lcom/twitter/model/core/TweetActionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 559
    :cond_0
    :goto_0
    return-void

    .line 550
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment$a;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ProfileActivity;

    .line 551
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/ProfileActivity;->a(Z)V

    goto :goto_0

    .line 542
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
