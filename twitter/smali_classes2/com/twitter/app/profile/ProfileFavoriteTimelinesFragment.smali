.class public Lcom/twitter/app/profile/ProfileFavoriteTimelinesFragment;
.super Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;
.source "Twttr"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic H()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileFavoriteTimelinesFragment;->s()Lcom/twitter/app/profile/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic I()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileFavoriteTimelinesFragment;->s()Lcom/twitter/app/profile/d;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 42
    invoke-super {p0, p1, p2}, Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;->a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 43
    if-eqz v1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/profile/ProfileFavoriteTimelinesFragment;->a:Z

    if-eqz v0, :cond_0

    .line 44
    const v0, 0x7f130030

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    .line 45
    const v2, 0x7f0a0373

    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/TypefacesTextView;->setText(I)V

    .line 47
    :cond_0
    return-object v1
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 2

    .prologue
    .line 52
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 53
    iget-boolean v0, p0, Lcom/twitter/app/profile/ProfileFavoriteTimelinesFragment;->a:Z

    if-eqz v0, :cond_0

    .line 54
    const v0, 0x7f040398

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    move-result-object v0

    const v1, 0x7f0402c9

    .line 55
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l$d;->f(I)Lcom/twitter/app/common/list/l$d;

    .line 56
    invoke-static {}, Lbpu;->a()Lbpu;

    move-result-object v0

    invoke-virtual {v0}, Lbpu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    const v0, 0x7f0403df

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    .line 60
    :cond_0
    return-void
.end method

.method protected i(Lank;)Lanm;
    .locals 2

    .prologue
    .line 29
    invoke-static {}, Lcom/twitter/app/profile/a;->c()Lcom/twitter/app/profile/a$a;

    move-result-object v0

    .line 30
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/profile/a$a;->a(Lamu;)Lcom/twitter/app/profile/a$a;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lcom/twitter/app/profile/a$a;->a()Lcom/twitter/app/profile/c;

    move-result-object v0

    .line 29
    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/twitter/app/profile/ProfileFavoriteTimelinesFragment;->a:Z

    invoke-static {v0}, Lcom/twitter/android/profiles/v;->a(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lcom/twitter/app/common/timeline/c;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileFavoriteTimelinesFragment;->s()Lcom/twitter/app/profile/d;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileFavoriteTimelinesFragment;->s()Lcom/twitter/app/profile/d;

    move-result-object v0

    const-string/jumbo v1, "is_me"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/profile/d;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/profile/ProfileFavoriteTimelinesFragment;->a:Z

    .line 37
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;->onCreate(Landroid/os/Bundle;)V

    .line 38
    return-void
.end method

.method public s()Lcom/twitter/app/profile/d;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileFavoriteTimelinesFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/profile/d;->a(Landroid/os/Bundle;)Lcom/twitter/app/profile/d;

    move-result-object v0

    return-object v0
.end method
