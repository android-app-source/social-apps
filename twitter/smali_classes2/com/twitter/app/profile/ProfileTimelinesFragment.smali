.class public Lcom/twitter/app/profile/ProfileTimelinesFragment;
.super Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/profiles/t$a;
.implements Lcom/twitter/android/util/v$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/profile/ProfileTimelinesFragment$a;,
        Lcom/twitter/app/profile/ProfileTimelinesFragment$b;
    }
.end annotation


# instance fields
.field private a:Z

.field private b:Lcom/twitter/android/widget/t;

.field private c:Lcom/twitter/android/profiles/q;

.field private d:Lcom/twitter/android/profiles/m;

.field private e:Z

.field private f:Z

.field private t:Z

.field private u:J

.field private v:Lcom/twitter/android/widget/t;

.field private w:Landroid/widget/TextView;

.field private x:Lcom/twitter/ui/widget/TwitterButton;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;-><init>()V

    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 466
    const/4 v0, 0x4

    .line 467
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    .line 468
    iget-boolean v3, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->a:Z

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->b:Lcom/twitter/android/widget/t;

    if-eqz v3, :cond_1

    .line 469
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 472
    :cond_0
    invoke-static {p1}, Lcom/twitter/android/cv;->d(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_7

    move v3, v2

    .line 477
    :goto_0
    if-eqz v3, :cond_1

    .line 478
    const/16 v0, 0xd

    .line 482
    :cond_1
    iget-wide v6, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->u:J

    cmp-long v3, v6, v8

    if-lez v3, :cond_5

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 485
    :cond_2
    iget-wide v6, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->u:J

    invoke-static {p1, v6, v7}, Lcom/twitter/android/cv;->a(Landroid/database/Cursor;J)Z

    move-result v3

    if-eqz v3, :cond_3

    move v1, v2

    .line 488
    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    if-lt v3, v0, :cond_2

    .line 489
    :cond_4
    if-eqz v1, :cond_5

    .line 492
    add-int/lit8 v0, v0, 0x1

    .line 495
    :cond_5
    invoke-interface {p1, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 498
    iget-wide v2, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->u:J

    cmp-long v1, v2, v8

    if-lez v1, :cond_6

    .line 499
    add-int/lit8 v0, v0, -0x1

    .line 501
    :cond_6
    iget-object v1, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->v:Lcom/twitter/android/widget/t;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/t;->a(I)V

    .line 502
    return-void

    .line 476
    :cond_7
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    move v3, v1

    goto :goto_0
.end method

.method private aU()V
    .locals 4

    .prologue
    .line 398
    iget-wide v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->a_:J

    .line 399
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->aJ()Lcom/twitter/metrics/j;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/metrics/d;->a(JLcom/twitter/metrics/j;Z)Lcom/twitter/android/metrics/d;

    move-result-object v0

    .line 401
    if-eqz v0, :cond_0

    .line 402
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/android/metrics/d;->a(I)V

    .line 405
    :cond_0
    return-void
.end method

.method private aV()V
    .locals 2

    .prologue
    .line 408
    iget-boolean v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->a:Z

    if-eqz v0, :cond_0

    .line 409
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/twitter/android/profilecompletionmodule/t;->a(Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->t:Z

    .line 410
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->w:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 411
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->x:Lcom/twitter/ui/widget/TwitterButton;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 412
    iget-boolean v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->t:Z

    if-eqz v0, :cond_1

    .line 413
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->w:Landroid/widget/TextView;

    const v1, 0x7f0a0377

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 414
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->x:Lcom/twitter/ui/widget/TwitterButton;

    const v1, 0x7f0a0376

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setText(I)V

    .line 420
    :cond_0
    :goto_0
    return-void

    .line 416
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->w:Landroid/widget/TextView;

    const v1, 0x7f0a0378

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 417
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->x:Lcom/twitter/ui/widget/TwitterButton;

    const v1, 0x7f0a0375

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setText(I)V

    goto :goto_0
.end method

.method private s()V
    .locals 1

    .prologue
    .line 378
    invoke-direct {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->d:Lcom/twitter/android/profiles/m;

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->d:Lcom/twitter/android/profiles/m;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/m;->b()V

    .line 381
    :cond_0
    return-void
.end method

.method private t()V
    .locals 1

    .prologue
    .line 384
    invoke-direct {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->c:Lcom/twitter/android/profiles/q;

    if-eqz v0, :cond_0

    .line 385
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->f:Z

    .line 386
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->e:Z

    .line 387
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->c:Lcom/twitter/android/profiles/q;

    invoke-interface {v0}, Lcom/twitter/android/profiles/q;->n()V

    .line 391
    :goto_0
    return-void

    .line 389
    :cond_0
    invoke-direct {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->aU()V

    goto :goto_0
.end method

.method private x()Z
    .locals 1

    .prologue
    .line 394
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cv;

    invoke-virtual {v0}, Lcom/twitter/android/cv;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected B()V
    .locals 4

    .prologue
    .line 166
    invoke-super {p0}, Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;->B()V

    .line 167
    iget-wide v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->a_:J

    .line 168
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->aJ()Lcom/twitter/metrics/j;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/metrics/d;->a(JLcom/twitter/metrics/j;Z)Lcom/twitter/android/metrics/d;

    move-result-object v0

    .line 170
    if-eqz v0, :cond_0

    .line 171
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/metrics/d;->a(I)V

    .line 174
    :cond_0
    return-void
.end method

.method protected C()V
    .locals 18

    .prologue
    .line 232
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->a_:J

    .line 233
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->aJ()Lcom/twitter/metrics/j;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Lcom/twitter/android/metrics/d;->a(JLcom/twitter/metrics/j;Z)Lcom/twitter/android/metrics/d;

    move-result-object v7

    .line 234
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v12

    .line 235
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->az()Lcjr;

    move-result-object v2

    move-object/from16 v17, v2

    check-cast v17, Lcom/twitter/android/cv;

    .line 236
    instance-of v2, v12, Lcom/twitter/android/profiles/t$b;

    if-eqz v2, :cond_4

    move-object v2, v12

    .line 237
    check-cast v2, Lcom/twitter/android/profiles/t$b;

    invoke-interface {v2}, Lcom/twitter/android/profiles/t$b;->c()Lcom/twitter/android/profiles/t;

    move-result-object v5

    .line 238
    invoke-virtual {v5}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v2

    .line 239
    if-eqz v2, :cond_0

    .line 240
    iget-wide v2, v2, Lcom/twitter/model/core/TwitterUser;->L:J

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->u:J

    .line 242
    :cond_0
    move-object/from16 v0, v17

    instance-of v2, v0, Lcom/twitter/android/cf;

    if-eqz v2, :cond_1

    move-object/from16 v2, v17

    .line 243
    check-cast v2, Lcom/twitter/android/cf;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->u:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/cf;->a(Ljava/util/Set;)V

    .line 246
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->c:Lcom/twitter/android/profiles/q;

    if-eqz v2, :cond_2

    .line 248
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->c:Lcom/twitter/android/profiles/q;

    invoke-interface {v2}, Lcom/twitter/android/profiles/q;->p()V

    .line 250
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v6

    .line 251
    new-instance v2, Lcom/twitter/android/profiles/z;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v4

    const/4 v8, 0x1

    invoke-direct/range {v2 .. v8}, Lcom/twitter/android/profiles/z;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/v;Lcom/twitter/android/profiles/t;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/metrics/d;Z)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->c:Lcom/twitter/android/profiles/q;

    .line 253
    new-instance v7, Lcom/twitter/android/profiles/m;

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v9

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v10

    .line 254
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v11

    .line 255
    invoke-static {v12}, Lcom/twitter/android/client/k;->a(Landroid/content/Context;)Lcom/twitter/android/client/k;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/client/k;->a()Z

    move-result v13

    .line 256
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->D()Lcom/twitter/app/common/timeline/TimelineFragment$e;

    move-result-object v15

    move-object v8, v12

    move-object v12, v5

    move-object v14, v6

    move-object/from16 v16, p0

    invoke-direct/range {v7 .. v16}, Lcom/twitter/android/profiles/m;-><init>(Landroid/app/Activity;Lcom/twitter/library/client/v;Landroid/support/v4/app/LoaderManager;Lcom/twitter/library/client/p;Lcom/twitter/android/profiles/t;ZLcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/ct;Lcom/twitter/android/av;)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->d:Lcom/twitter/android/profiles/m;

    .line 258
    new-instance v2, Lcom/twitter/android/widget/an$a;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->c:Lcom/twitter/android/profiles/q;

    .line 259
    invoke-interface {v3}, Lcom/twitter/android/profiles/q;->b()Lcom/twitter/android/bh;

    move-result-object v3

    const/4 v4, 0x4

    move-object/from16 v0, v17

    invoke-direct {v2, v0, v3, v4}, Lcom/twitter/android/widget/an$a;-><init>(Landroid/widget/BaseAdapter;Landroid/widget/BaseAdapter;I)V

    const/4 v3, 0x1

    .line 260
    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/an$a;->a(Z)Lcom/twitter/android/widget/t$a;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/widget/an$a;

    .line 261
    invoke-virtual {v2}, Lcom/twitter/android/widget/an$a;->b()Lcom/twitter/android/widget/an;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->v:Lcom/twitter/android/widget/t;

    .line 262
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->v:Lcom/twitter/android/widget/t;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->b:Lcom/twitter/android/widget/t;

    .line 263
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->d:Lcom/twitter/android/profiles/m;

    invoke-virtual {v2}, Lcom/twitter/android/profiles/m;->a()Lcom/twitter/android/cv;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 264
    new-instance v2, Lcom/twitter/android/widget/t$a;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->v:Lcom/twitter/android/widget/t;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->d:Lcom/twitter/android/profiles/m;

    .line 265
    invoke-virtual {v4}, Lcom/twitter/android/profiles/m;->a()Lcom/twitter/android/cv;

    move-result-object v4

    const/4 v6, 0x0

    invoke-direct {v2, v3, v4, v6}, Lcom/twitter/android/widget/t$a;-><init>(Landroid/widget/BaseAdapter;Landroid/widget/BaseAdapter;I)V

    const/4 v3, 0x1

    .line 266
    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/t$a;->a(Z)Lcom/twitter/android/widget/t$a;

    move-result-object v2

    const v3, 0x7f0400ab

    .line 267
    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/t$a;->a(I)Lcom/twitter/android/widget/t$a;

    move-result-object v2

    .line 268
    invoke-virtual {v2}, Lcom/twitter/android/widget/t$a;->a()Lcom/twitter/android/widget/t;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->b:Lcom/twitter/android/widget/t;

    .line 270
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->b:Lcom/twitter/android/widget/t;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 271
    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Lcom/twitter/android/profiles/t;->a(Lcom/twitter/android/profiles/t$a;)V

    .line 275
    :goto_0
    return-void

    .line 273
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method protected G()I
    .locals 1

    .prologue
    .line 462
    const/16 v0, 0x14

    return v0
.end method

.method public synthetic H()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->h()Lcom/twitter/app/profile/e;

    move-result-object v0

    return-object v0
.end method

.method public synthetic I()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->h()Lcom/twitter/app/profile/e;

    move-result-object v0

    return-object v0
.end method

.method protected K_()Z
    .locals 1

    .prologue
    .line 227
    invoke-static {}, Lcom/twitter/library/av/v;->a()Z

    move-result v0

    return v0
.end method

.method public M()V
    .locals 1

    .prologue
    .line 286
    invoke-super {p0}, Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;->M()V

    .line 287
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->c:Lcom/twitter/android/profiles/q;

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->c:Lcom/twitter/android/profiles/q;

    invoke-interface {v0}, Lcom/twitter/android/profiles/q;->m()V

    .line 290
    :cond_0
    invoke-direct {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->s()V

    .line 291
    return-void
.end method

.method protected N_()Z
    .locals 1

    .prologue
    .line 161
    invoke-static {}, Lcom/twitter/android/revenue/k;->a()Z

    move-result v0

    return v0
.end method

.method protected O()Lcom/twitter/android/ck;
    .locals 6

    .prologue
    .line 521
    new-instance v0, Lcom/twitter/app/profile/ProfileTimelinesFragment$a;

    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->o:Lcom/twitter/android/revenue/c;

    iget-object v4, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->j:Lcom/twitter/android/timeline/be;

    iget v5, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->q:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/app/profile/ProfileTimelinesFragment$a;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/android/revenue/c;Lcom/twitter/android/timeline/be;I)V

    return-object v0
.end method

.method public a(J)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 307
    iget-object v1, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->b:Lcom/twitter/android/widget/t;

    invoke-virtual {v1}, Lcom/twitter/android/widget/t;->getCount()I

    move-result v2

    move v1, v0

    .line 308
    :goto_0
    if-ge v1, v2, :cond_0

    .line 309
    iget-object v3, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->b:Lcom/twitter/android/widget/t;

    invoke-virtual {v3, v1}, Lcom/twitter/android/widget/t;->getItemId(I)J

    move-result-wide v4

    cmp-long v3, v4, p1

    if-nez v3, :cond_1

    .line 310
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    add-int/2addr v0, v1

    .line 313
    :cond_0
    return v0

    .line 308
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 135
    invoke-super {p0, p1, p2}, Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;->a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 136
    if-eqz v1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->a:Z

    if-eqz v0, :cond_0

    .line 137
    const v0, 0x7f130030

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TypefacesTextView;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->w:Landroid/widget/TextView;

    .line 138
    const v0, 0x7f130367

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->x:Lcom/twitter/ui/widget/TwitterButton;

    .line 140
    invoke-direct {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->aV()V

    .line 141
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->x:Lcom/twitter/ui/widget/TwitterButton;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/twitter/ui/widget/TwitterButton;->setVisibility(I)V

    .line 142
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->x:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    :cond_0
    return-object v1
.end method

.method protected a(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/android/ct;Z)Lcom/twitter/android/cv;
    .locals 1

    .prologue
    .line 509
    const/4 v0, 0x1

    invoke-super {p0, p1, p2, p3, v0}, Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;->a(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/android/ct;ZZ)Lcom/twitter/android/cf;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6

    .prologue
    .line 336
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->b:Lcom/twitter/android/widget/t;

    .line 337
    invoke-virtual {p1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v1

    sub-int v1, p3, v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/t;->b(I)Lcom/twitter/util/collection/Pair;

    move-result-object v1

    .line 338
    if-eqz v1, :cond_0

    .line 339
    invoke-virtual {v1}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    .line 340
    invoke-virtual {v1}, Lcom/twitter/util/collection/Pair;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 341
    iget-object v1, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->c:Lcom/twitter/android/profiles/q;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->c:Lcom/twitter/android/profiles/q;

    invoke-interface {v1}, Lcom/twitter/android/profiles/q;->b()Lcom/twitter/android/bh;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 342
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->c:Lcom/twitter/android/profiles/q;

    move-object v1, p1

    move-object v2, p2

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/twitter/android/profiles/q;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 349
    :cond_0
    :goto_0
    return-void

    .line 343
    :cond_1
    iget-object v1, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->d:Lcom/twitter/android/profiles/m;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->d:Lcom/twitter/android/profiles/m;

    invoke-virtual {v1}, Lcom/twitter/android/profiles/m;->a()Lcom/twitter/android/cv;

    move-result-object v1

    if-ne v0, v1, :cond_2

    .line 344
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->d:Lcom/twitter/android/profiles/m;

    move-object v1, p1

    move-object v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/profiles/m;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    goto :goto_0

    .line 346
    :cond_2
    invoke-super/range {p0 .. p5}, Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;->a(Landroid/widget/ListView;Landroid/view/View;IJ)V

    goto :goto_0
.end method

.method protected a(Lcbi;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/android/timeline/bk;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 359
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;->a(Lcbi;)V

    .line 360
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cv;

    invoke-virtual {v0}, Lcom/twitter/android/cv;->f()Lcom/twitter/android/timeline/bl;

    move-result-object v0

    .line 361
    if-nez v0, :cond_3

    const/4 v0, 0x0

    move-object v2, v0

    .line 362
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 363
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cv;

    invoke-virtual {v0}, Lcom/twitter/android/cv;->getCount()I

    move-result v3

    .line 364
    instance-of v0, v1, Lcom/twitter/app/profile/ProfileTimelinesFragment$b;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 365
    check-cast v0, Lcom/twitter/app/profile/ProfileTimelinesFragment$b;

    invoke-interface {v0, v3}, Lcom/twitter/app/profile/ProfileTimelinesFragment$b;->c(I)V

    .line 367
    :cond_0
    if-nez v3, :cond_1

    .line 368
    invoke-direct {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->aV()V

    .line 370
    :cond_1
    invoke-direct {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->t()V

    .line 371
    invoke-direct {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->s()V

    .line 372
    if-eqz v2, :cond_2

    .line 373
    invoke-direct {p0, v2}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->a(Landroid/database/Cursor;)V

    .line 375
    :cond_2
    return-void

    .line 361
    :cond_3
    invoke-virtual {v0}, Lcom/twitter/android/timeline/bl;->a()Landroid/database/Cursor;

    move-result-object v0

    move-object v2, v0

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/profiles/t;)V
    .locals 6

    .prologue
    .line 178
    invoke-virtual {p1}, Lcom/twitter/android/profiles/t;->a()Lcom/twitter/model/core/TwitterUser;

    move-result-object v1

    .line 179
    if-eqz v1, :cond_3

    iget-wide v2, v1, Lcom/twitter/model/core/TwitterUser;->L:J

    iget-wide v4, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->u:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_3

    .line 180
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 181
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cv;

    .line 182
    instance-of v2, v0, Lcom/twitter/android/cf;

    if-eqz v2, :cond_1

    .line 183
    iget-wide v2, v1, Lcom/twitter/model/core/TwitterUser;->L:J

    iput-wide v2, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->u:J

    .line 184
    check-cast v0, Lcom/twitter/android/cf;

    iget-wide v2, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->u:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/cf;->a(Ljava/util/Set;)V

    .line 185
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 186
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cv;

    invoke-virtual {v0}, Lcom/twitter/android/cv;->f()Lcom/twitter/android/timeline/bl;

    move-result-object v0

    .line 187
    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 188
    :goto_0
    if-eqz v0, :cond_0

    .line 189
    invoke-direct {p0, v0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->a(Landroid/database/Cursor;)V

    .line 191
    :cond_0
    iget v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->L:I

    invoke-virtual {p0, v0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->b(I)V

    .line 197
    :cond_1
    :goto_1
    return-void

    .line 187
    :cond_2
    invoke-virtual {v0}, Lcom/twitter/android/timeline/bl;->a()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 194
    :cond_3
    iget-boolean v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->a:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cv;

    invoke-virtual {v0}, Lcom/twitter/android/cv;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 195
    invoke-direct {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->aV()V

    goto :goto_1
.end method

.method public a(Lcom/twitter/android/util/v;)V
    .locals 0

    .prologue
    .line 223
    return-void
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 2

    .prologue
    .line 149
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 150
    iget-boolean v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->a:Z

    if-eqz v0, :cond_0

    .line 151
    const v0, 0x7f040398

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    move-result-object v0

    const v1, 0x7f0402c9

    .line 152
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l$d;->f(I)Lcom/twitter/app/common/list/l$d;

    .line 153
    invoke-static {}, Lbpu;->a()Lbpu;

    move-result-object v0

    invoke-virtual {v0}, Lbpu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    const v0, 0x7f0403df

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    .line 157
    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 1

    .prologue
    .line 424
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 425
    invoke-direct {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->t()V

    .line 427
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 428
    return-void
.end method

.method protected b()V
    .locals 1

    .prologue
    .line 295
    invoke-super {p0}, Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;->b()V

    .line 296
    iget-boolean v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->f:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->c:Lcom/twitter/android/profiles/q;

    if-eqz v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->c:Lcom/twitter/android/profiles/q;

    invoke-interface {v0}, Lcom/twitter/android/profiles/q;->n()V

    .line 299
    :cond_0
    invoke-direct {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->s()V

    .line 300
    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 212
    iget-boolean v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->a:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "profile_self"

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "profile_other"

    goto :goto_0
.end method

.method public h()Lcom/twitter/app/profile/e;
    .locals 1

    .prologue
    .line 515
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/profile/e;->d(Landroid/os/Bundle;)Lcom/twitter/app/profile/e;

    move-result-object v0

    return-object v0
.end method

.method protected i(I)Lajc$a;
    .locals 2

    .prologue
    .line 452
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;->i(I)Lajc$a;

    move-result-object v1

    .line 453
    iget-boolean v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 454
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cv;

    invoke-virtual {v0}, Lcom/twitter/android/cv;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 455
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lajc$a;->a(Z)Lajc$a;

    .line 457
    :cond_0
    return-object v1
.end method

.method protected i(Lank;)Lanm;
    .locals 2

    .prologue
    .line 111
    invoke-static {}, Lcom/twitter/app/profile/b;->c()Lcom/twitter/app/profile/b$a;

    move-result-object v0

    .line 112
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/profile/b$a;->a(Lamu;)Lcom/twitter/app/profile/b$a;

    move-result-object v0

    .line 113
    invoke-virtual {v0}, Lcom/twitter/app/profile/b$a;->a()Lcom/twitter/app/profile/f;

    move-result-object v0

    .line 111
    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 354
    iget-boolean v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->a:Z

    invoke-static {v0}, Lcom/twitter/android/profiles/v;->a(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic k()Lcom/twitter/app/common/timeline/c;
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->h()Lcom/twitter/app/profile/e;

    move-result-object v0

    return-object v0
.end method

.method public m()Z
    .locals 2

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "profile_self"

    invoke-static {v0, v1}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    const-string/jumbo v0, "android_umf_request_profile_self"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    .line 204
    :goto_0
    return v0

    :cond_0
    const-string/jumbo v0, "android_umf_request_profile_other"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x1

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 319
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f130367

    if-ne v0, v1, :cond_0

    .line 320
    iget-boolean v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->t:Z

    if-eqz v0, :cond_1

    .line 321
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "profile"

    aput-object v2, v1, v3

    const-string/jumbo v2, "edit_profile_flow"

    aput-object v2, v1, v4

    const/4 v2, 0x0

    aput-object v2, v1, v5

    const-string/jumbo v2, "timeline"

    aput-object v2, v1, v6

    const-string/jumbo v2, "launch"

    aput-object v2, v1, v7

    .line 323
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 322
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 324
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string/jumbo v1, "profile"

    invoke-static {v0, v1}, Lcom/twitter/android/profilecompletionmodule/ProfileCompletionFlowActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->startActivity(Landroid/content/Intent;)V

    .line 332
    :cond_0
    :goto_0
    return-void

    .line 326
    :cond_1
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "profile"

    aput-object v2, v1, v3

    const-string/jumbo v2, "compose"

    aput-object v2, v1, v4

    const/4 v2, 0x0

    aput-object v2, v1, v5

    const-string/jumbo v2, "timeline"

    aput-object v2, v1, v6

    const-string/jumbo v2, "launch"

    aput-object v2, v1, v7

    .line 328
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 327
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 329
    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 118
    invoke-virtual {p0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->h()Lcom/twitter/app/profile/e;

    move-result-object v2

    .line 119
    const-string/jumbo v3, "is_me"

    invoke-virtual {v2, v3, v0}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->a:Z

    .line 120
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;->onCreate(Landroid/os/Bundle;)V

    .line 121
    const-string/jumbo v3, "statuses_count"

    invoke-virtual {v2, v3}, Lcom/twitter/app/common/list/i;->b(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    :cond_0
    iput-boolean v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->f:Z

    .line 123
    iget-boolean v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->a:Z

    if-eqz v0, :cond_1

    .line 124
    new-instance v0, Lcom/twitter/ui/view/h$a;

    invoke-direct {v0}, Lcom/twitter/ui/view/h$a;-><init>()V

    .line 125
    invoke-virtual {v0, v1}, Lcom/twitter/ui/view/h$a;->b(Z)Lcom/twitter/ui/view/h$a;

    move-result-object v0

    .line 126
    invoke-virtual {v0, v1}, Lcom/twitter/ui/view/h$a;->f(Z)Lcom/twitter/ui/view/h$a;

    move-result-object v0

    .line 127
    invoke-virtual {v0, v1}, Lcom/twitter/ui/view/h$a;->h(Z)Lcom/twitter/ui/view/h$a;

    move-result-object v0

    .line 128
    invoke-virtual {v0}, Lcom/twitter/ui/view/h$a;->a()Lcom/twitter/ui/view/h;

    move-result-object v0

    .line 129
    invoke-virtual {p0, v0}, Lcom/twitter/app/profile/ProfileTimelinesFragment;->a(Lcom/twitter/ui/view/h;)V

    .line 131
    :cond_1
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->c:Lcom/twitter/android/profiles/q;

    if-eqz v0, :cond_0

    .line 433
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->c:Lcom/twitter/android/profiles/q;

    invoke-interface {v0}, Lcom/twitter/android/profiles/q;->p()V

    .line 435
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->d:Lcom/twitter/android/profiles/m;

    if-eqz v0, :cond_1

    .line 436
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->d:Lcom/twitter/android/profiles/m;

    invoke-virtual {v0}, Lcom/twitter/android/profiles/m;->c()V

    .line 438
    :cond_1
    invoke-super {p0}, Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;->onDestroy()V

    .line 439
    return-void
.end method

.method public q_()V
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->c:Lcom/twitter/android/profiles/q;

    if-eqz v0, :cond_0

    .line 444
    iget-object v0, p0, Lcom/twitter/app/profile/ProfileTimelinesFragment;->c:Lcom/twitter/android/profiles/q;

    invoke-interface {v0}, Lcom/twitter/android/profiles/q;->q()V

    .line 446
    :cond_0
    invoke-super {p0}, Lcom/twitter/android/widget/ScrollingHeaderTimelineFragment;->q_()V

    .line 447
    return-void
.end method
