.class public Lcom/twitter/app/drafts/j;
.super Lcom/twitter/app/common/list/l;
.source "Twttr"

# interfaces
.implements Lala;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/drafts/j$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/list/l",
        "<",
        "Lcom/twitter/model/drafts/a;",
        "Lcom/twitter/app/drafts/c;",
        ">;",
        "Lala;"
    }
.end annotation


# instance fields
.field private final e:Lrx/j;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Lcom/twitter/app/drafts/j$a;Lcom/twitter/app/drafts/d;Lakr;Laon;Lcom/twitter/app/drafts/h;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/LayoutInflater;",
            "Lcom/twitter/app/drafts/j$a;",
            "Lcom/twitter/app/drafts/d;",
            "Lakr",
            "<",
            "Lcom/twitter/android/composer/a;",
            ">;",
            "Laon;",
            "Lcom/twitter/app/drafts/h;",
            ")V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/twitter/app/common/list/l;-><init>(Landroid/view/LayoutInflater;Lcom/twitter/app/common/list/l$d;)V

    .line 57
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 58
    new-instance v0, Lcom/twitter/app/drafts/c;

    invoke-direct {v0, v6}, Lcom/twitter/app/drafts/c;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/twitter/app/drafts/j;->a(Lcjr;)V

    .line 59
    iget-object v7, p0, Lcom/twitter/app/drafts/j;->a:Landroid/widget/ListView;

    new-instance v0, Lcom/twitter/app/drafts/j$1;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p5

    move-object v4, p2

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/twitter/app/drafts/j$1;-><init>(Lcom/twitter/app/drafts/j;Lakr;Laon;Lcom/twitter/app/drafts/j$a;Lcom/twitter/app/drafts/h;)V

    invoke-virtual {v7, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 67
    iget-object v7, p0, Lcom/twitter/app/drafts/j;->a:Landroid/widget/ListView;

    new-instance v0, Lcom/twitter/app/drafts/j$2;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p5

    move-object v4, p2

    move-object v5, p6

    invoke-direct/range {v0 .. v6}, Lcom/twitter/app/drafts/j$2;-><init>(Lcom/twitter/app/drafts/j;Lakr;Laon;Lcom/twitter/app/drafts/j$a;Lcom/twitter/app/drafts/h;Landroid/content/Context;)V

    invoke-virtual {v7, v0}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 100
    invoke-interface {p3}, Lcom/twitter/app/drafts/d;->a()Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/drafts/j$3;

    invoke-direct {v1, p0}, Lcom/twitter/app/drafts/j$3;-><init>(Lcom/twitter/app/drafts/j;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/drafts/j;->e:Lrx/j;

    .line 107
    return-void
.end method

.method static a(JLandroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/p;)V
    .locals 8

    .prologue
    .line 139
    new-instance v1, Lcom/twitter/android/composer/v;

    const/4 v6, 0x0

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p0

    invoke-direct/range {v1 .. v6}, Lcom/twitter/android/composer/v;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JZ)V

    .line 140
    sget-object v0, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;->g:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/v;->a(Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)V

    .line 141
    invoke-virtual {p4, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 142
    return-void
.end method

.method static a(Lakr;Lcom/twitter/model/drafts/a;Laon;Lcom/twitter/app/drafts/j$a;Lcom/twitter/app/drafts/h;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lakr",
            "<",
            "Lcom/twitter/android/composer/a;",
            ">;",
            "Lcom/twitter/model/drafts/a;",
            "Laon;",
            "Lcom/twitter/app/drafts/j$a;",
            "Lcom/twitter/app/drafts/h;",
            ")V"
        }
    .end annotation

    .prologue
    .line 123
    invoke-virtual {p3}, Lcom/twitter/app/drafts/j$a;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 125
    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/a;->a(Lcom/twitter/model/drafts/a;)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 126
    invoke-virtual {p3}, Lcom/twitter/app/drafts/j$a;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->b(Ljava/lang/String;)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 124
    invoke-virtual {p0, v0}, Lakr;->a(Lako;)V

    .line 127
    invoke-virtual {p4}, Lcom/twitter/app/drafts/h;->b()V

    .line 131
    :goto_0
    return-void

    .line 129
    :cond_0
    sget-object v0, Lcom/twitter/model/drafts/a;->a:Lcom/twitter/util/serialization/b;

    invoke-interface {p2, p1, v0}, Laon;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/app/common/di/scope/InjectionScope;)V
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/twitter/app/common/di/scope/InjectionScope;->e:Lcom/twitter/app/common/di/scope/InjectionScope;

    if-ne p1, v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/twitter/app/drafts/j;->e:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 113
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/app/drafts/j;->a(Lcbi;)V

    .line 115
    :cond_0
    return-void
.end method
