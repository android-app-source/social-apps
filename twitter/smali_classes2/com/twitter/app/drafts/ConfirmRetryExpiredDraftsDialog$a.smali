.class Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog$a;
.super Lcom/twitter/async/service/AsyncOperation;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/async/service/AsyncOperation",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/library/client/Session;

.field private final c:[J


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/twitter/library/client/Session;[J)V
    .locals 1

    .prologue
    .line 128
    const-class v0, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog$a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/async/service/AsyncOperation;-><init>(Ljava/lang/String;)V

    .line 129
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog$a;->a:Ljava/lang/ref/WeakReference;

    .line 130
    iput-object p2, p0, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog$a;->b:Lcom/twitter/library/client/Session;

    .line 131
    iput-object p3, p0, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog$a;->c:[J

    .line 132
    sget-object v0, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;->g:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    invoke-virtual {p0, v0}, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog$a;->a(Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)V

    .line 133
    return-void
.end method


# virtual methods
.method protected a()Ljava/lang/Void;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 137
    iget-object v0, p0, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 138
    if-nez v0, :cond_1

    .line 156
    :cond_0
    :goto_0
    return-object v8

    .line 142
    :cond_1
    instance-of v1, v0, Lcom/twitter/app/common/util/i;

    if-eqz v1, :cond_2

    move-object v1, v0

    check-cast v1, Lcom/twitter/app/common/util/i;

    invoke-interface {v1}, Lcom/twitter/app/common/util/i;->isDestroyed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 145
    :cond_2
    iget-object v1, p0, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog$a;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/twitter/library/provider/h;->a(J)Lcom/twitter/library/provider/h;

    move-result-object v3

    .line 146
    iget-object v4, p0, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog$a;->c:[J

    array-length v5, v4

    move v1, v2

    :goto_1
    if-ge v1, v5, :cond_3

    aget-wide v6, v4, v1

    .line 147
    invoke-static {v6, v7, v2}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadManager;->a(JZ)Lrx/g;

    .line 148
    invoke-virtual {v3, v6, v7, v2, v8}, Lcom/twitter/library/provider/h;->a(JILaut;)Z

    .line 146
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 150
    :cond_3
    sget-object v1, Lcom/twitter/util/concurrent/f;->a:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog$a$1;

    invoke-direct {v2, p0, v0}, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog$a$1;-><init>(Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog$a;Landroid/app/Activity;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected b()Ljava/lang/Void;
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x0

    return-object v0
.end method

.method protected synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog$a;->b()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic d()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog$a;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
