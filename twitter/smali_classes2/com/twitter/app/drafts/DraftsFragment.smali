.class public Lcom/twitter/app/drafts/DraftsFragment;
.super Lcom/twitter/app/common/list/TwitterListFragment;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/list/TwitterListFragment",
        "<",
        "Lcom/twitter/model/drafts/a;",
        "Lcjr",
        "<",
        "Lcom/twitter/model/drafts/a;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/twitter/app/common/list/TwitterListFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lank;)Lcom/twitter/app/drafts/f;
    .locals 3

    .prologue
    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/twitter/app/drafts/DraftsFragment;->H()Lcom/twitter/app/common/list/i;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/drafts/g;->a(Lcom/twitter/app/common/list/i;)Lcom/twitter/app/drafts/g;

    move-result-object v0

    .line 23
    invoke-static {}, Lcom/twitter/app/drafts/a;->a()Lcom/twitter/app/drafts/a$a;

    move-result-object v1

    .line 24
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/app/drafts/a$a;->a(Lamu;)Lcom/twitter/app/drafts/a$a;

    move-result-object v1

    new-instance v2, Lano;

    invoke-direct {v2, v0}, Lano;-><init>(Lanr;)V

    .line 25
    invoke-virtual {v1, v2}, Lcom/twitter/app/drafts/a$a;->a(Lano;)Lcom/twitter/app/drafts/a$a;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lcom/twitter/app/drafts/a$a;->a()Lcom/twitter/app/drafts/f;

    move-result-object v0

    .line 23
    return-object v0
.end method

.method protected synthetic b(Lank;)Lcom/twitter/app/common/list/j;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/twitter/app/drafts/DraftsFragment;->a(Lank;)Lcom/twitter/app/drafts/f;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic c(Lank;)Lcom/twitter/app/common/abs/c;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/twitter/app/drafts/DraftsFragment;->a(Lank;)Lcom/twitter/app/drafts/f;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic d(Lank;)Lann;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/twitter/app/drafts/DraftsFragment;->a(Lank;)Lcom/twitter/app/drafts/f;

    move-result-object v0

    return-object v0
.end method

.method protected e(Lank;)Lcom/twitter/app/drafts/l;
    .locals 4

    .prologue
    .line 32
    .line 33
    invoke-virtual {p0}, Lcom/twitter/app/drafts/DraftsFragment;->H()Lcom/twitter/app/common/list/i;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/drafts/g;->a(Lcom/twitter/app/common/list/i;)Lcom/twitter/app/drafts/g;

    move-result-object v0

    .line 34
    new-instance v1, Lcom/twitter/app/drafts/j$a;

    invoke-direct {v1}, Lcom/twitter/app/drafts/j$a;-><init>()V

    iget-boolean v2, v0, Lcom/twitter/app/drafts/g;->b:Z

    .line 35
    invoke-virtual {v1, v2}, Lcom/twitter/app/drafts/j$a;->c(Z)Lcom/twitter/app/drafts/j$a;

    move-result-object v1

    iget-object v0, v0, Lcom/twitter/app/drafts/g;->a:Ljava/lang/String;

    .line 36
    invoke-virtual {v1, v0}, Lcom/twitter/app/drafts/j$a;->b(Ljava/lang/String;)Lcom/twitter/app/drafts/j$a;

    move-result-object v2

    .line 37
    invoke-virtual {p0, v2}, Lcom/twitter/app/drafts/DraftsFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 39
    invoke-virtual {p0}, Lcom/twitter/app/drafts/DraftsFragment;->ah()Lann;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/drafts/f;

    .line 40
    new-instance v3, Lant;

    .line 41
    invoke-virtual {p0}, Lcom/twitter/app/drafts/DraftsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/twitter/app/common/base/BaseFragmentActivity;

    invoke-direct {v3, v1, p1, v2}, Lant;-><init>(Landroid/app/Activity;Lank;Laod;)V

    .line 40
    invoke-interface {v0, v3}, Lcom/twitter/app/drafts/f;->a(Lant;)Lcom/twitter/app/drafts/l;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f(Lank;)Lcom/twitter/app/common/list/k;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/twitter/app/drafts/DraftsFragment;->e(Lank;)Lcom/twitter/app/drafts/l;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic g(Lank;)Lcom/twitter/app/common/abs/d;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/twitter/app/drafts/DraftsFragment;->e(Lank;)Lcom/twitter/app/drafts/l;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic h(Lank;)Lans;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/twitter/app/drafts/DraftsFragment;->e(Lank;)Lcom/twitter/app/drafts/l;

    move-result-object v0

    return-object v0
.end method
