.class public final Lcom/twitter/app/drafts/a$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/drafts/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private a:Lano;

.field private b:Lamu;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/app/drafts/a$1;)V
    .locals 0

    .prologue
    .line 187
    invoke-direct {p0}, Lcom/twitter/app/drafts/a$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/drafts/a$a;)Lamu;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/twitter/app/drafts/a$a;->b:Lamu;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/app/drafts/a$a;)Lano;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/twitter/app/drafts/a$a;->a:Lano;

    return-object v0
.end method


# virtual methods
.method public a(Lamu;)Lcom/twitter/app/drafts/a$a;
    .locals 1

    .prologue
    .line 211
    invoke-static {p1}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamu;

    iput-object v0, p0, Lcom/twitter/app/drafts/a$a;->b:Lamu;

    .line 212
    return-object p0
.end method

.method public a(Lano;)Lcom/twitter/app/drafts/a$a;
    .locals 1

    .prologue
    .line 206
    invoke-static {p1}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lano;

    iput-object v0, p0, Lcom/twitter/app/drafts/a$a;->a:Lano;

    .line 207
    return-object p0
.end method

.method public a()Lcom/twitter/app/drafts/f;
    .locals 3

    .prologue
    .line 195
    iget-object v0, p0, Lcom/twitter/app/drafts/a$a;->a:Lano;

    if-nez v0, :cond_0

    .line 196
    new-instance v0, Lano;

    invoke-direct {v0}, Lano;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/drafts/a$a;->a:Lano;

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/drafts/a$a;->b:Lamu;

    if-nez v0, :cond_1

    .line 199
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lamu;

    .line 200
    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_1
    new-instance v0, Lcom/twitter/app/drafts/a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/app/drafts/a;-><init>(Lcom/twitter/app/drafts/a$a;Lcom/twitter/app/drafts/a$1;)V

    return-object v0
.end method
