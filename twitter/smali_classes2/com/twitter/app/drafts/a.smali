.class public final Lcom/twitter/app/drafts/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/drafts/f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/drafts/a$b;,
        Lcom/twitter/app/drafts/a$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/database/schema/DraftsSchema;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lauj",
            "<",
            "Lcom/twitter/database/model/f;",
            "Lcbi",
            "<",
            "Lcom/twitter/model/drafts/a;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laqf;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/drafts/e;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/v;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/drafts/h;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lanr;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/drafts/g;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/drafts/d;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/abs/j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/twitter/app/drafts/a;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/app/drafts/a;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/twitter/app/drafts/a$a;)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    sget-boolean v0, Lcom/twitter/app/drafts/a;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 81
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/app/drafts/a;->a(Lcom/twitter/app/drafts/a$a;)V

    .line 82
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/app/drafts/a$a;Lcom/twitter/app/drafts/a$1;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/twitter/app/drafts/a;-><init>(Lcom/twitter/app/drafts/a$a;)V

    return-void
.end method

.method public static a()Lcom/twitter/app/drafts/a$a;
    .locals 2

    .prologue
    .line 85
    new-instance v0, Lcom/twitter/app/drafts/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/app/drafts/a$a;-><init>(Lcom/twitter/app/drafts/a$1;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/app/drafts/a;)Lcta;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/app/drafts/a;->j:Lcta;

    return-object v0
.end method

.method private a(Lcom/twitter/app/drafts/a$a;)V
    .locals 3

    .prologue
    .line 91
    new-instance v0, Lcom/twitter/app/drafts/a$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/app/drafts/a$1;-><init>(Lcom/twitter/app/drafts/a;Lcom/twitter/app/drafts/a$a;)V

    iput-object v0, p0, Lcom/twitter/app/drafts/a;->b:Lcta;

    .line 104
    iget-object v0, p0, Lcom/twitter/app/drafts/a;->b:Lcta;

    .line 106
    invoke-static {v0}, Laqi;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 105
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/drafts/a;->c:Lcta;

    .line 108
    iget-object v0, p0, Lcom/twitter/app/drafts/a;->c:Lcta;

    .line 109
    invoke-static {v0}, Laqg;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/drafts/a;->d:Lcta;

    .line 111
    iget-object v0, p0, Lcom/twitter/app/drafts/a;->d:Lcta;

    .line 112
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/drafts/a;->e:Lcta;

    .line 114
    new-instance v0, Lcom/twitter/app/drafts/a$2;

    invoke-direct {v0, p0, p1}, Lcom/twitter/app/drafts/a$2;-><init>(Lcom/twitter/app/drafts/a;Lcom/twitter/app/drafts/a$a;)V

    iput-object v0, p0, Lcom/twitter/app/drafts/a;->f:Lcta;

    .line 127
    iget-object v0, p0, Lcom/twitter/app/drafts/a;->f:Lcta;

    .line 128
    invoke-static {v0}, Lcom/twitter/app/drafts/i;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/drafts/a;->g:Lcta;

    .line 133
    invoke-static {p1}, Lcom/twitter/app/drafts/a$a;->b(Lcom/twitter/app/drafts/a$a;)Lano;

    move-result-object v0

    .line 132
    invoke-static {v0}, Lanp;->a(Lano;)Ldagger/internal/c;

    move-result-object v0

    .line 131
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/drafts/a;->h:Lcta;

    .line 135
    iget-object v0, p0, Lcom/twitter/app/drafts/a;->h:Lcta;

    .line 137
    invoke-static {v0}, Laqj;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 136
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/drafts/a;->i:Lcta;

    .line 140
    iget-object v0, p0, Lcom/twitter/app/drafts/a;->e:Lcta;

    iget-object v1, p0, Lcom/twitter/app/drafts/a;->g:Lcta;

    iget-object v2, p0, Lcom/twitter/app/drafts/a;->i:Lcta;

    .line 142
    invoke-static {v0, v1, v2}, Laqk;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 141
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/drafts/a;->j:Lcta;

    .line 147
    iget-object v0, p0, Lcom/twitter/app/drafts/a;->j:Lcta;

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/drafts/a;->k:Lcta;

    .line 149
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 150
    invoke-static {v0, v1}, Ldagger/internal/f;->a(II)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/drafts/a;->k:Lcta;

    .line 151
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    .line 152
    invoke-virtual {v0}, Ldagger/internal/f$a;->a()Ldagger/internal/f;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/drafts/a;->l:Lcta;

    .line 154
    new-instance v0, Lcom/twitter/app/drafts/a$3;

    invoke-direct {v0, p0, p1}, Lcom/twitter/app/drafts/a$3;-><init>(Lcom/twitter/app/drafts/a;Lcom/twitter/app/drafts/a$a;)V

    iput-object v0, p0, Lcom/twitter/app/drafts/a;->m:Lcta;

    .line 167
    iget-object v0, p0, Lcom/twitter/app/drafts/a;->m:Lcta;

    .line 169
    invoke-static {v0}, Lcom/twitter/app/common/abs/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 168
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/drafts/a;->n:Lcta;

    .line 170
    return-void
.end method

.method static synthetic b(Lcom/twitter/app/drafts/a;)Lcta;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/app/drafts/a;->g:Lcta;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/app/drafts/a;)Lcta;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/twitter/app/drafts/a;->k:Lcta;

    return-object v0
.end method


# virtual methods
.method public a(Lant;)Lcom/twitter/app/drafts/l;
    .locals 2

    .prologue
    .line 184
    new-instance v0, Lcom/twitter/app/drafts/a$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/twitter/app/drafts/a$b;-><init>(Lcom/twitter/app/drafts/a;Lant;Lcom/twitter/app/drafts/a$1;)V

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 174
    iget-object v0, p0, Lcom/twitter/app/drafts/a;->l:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public c()Lcom/twitter/app/common/abs/j;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/twitter/app/drafts/a;->n:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/j;

    return-object v0
.end method
