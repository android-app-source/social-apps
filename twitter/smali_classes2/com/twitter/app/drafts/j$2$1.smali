.class Lcom/twitter/app/drafts/j$2$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/dialog/b$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/drafts/j$2;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/widget/AdapterView;

.field final synthetic b:I

.field final synthetic c:[I

.field final synthetic d:Lcom/twitter/app/drafts/j$2;


# direct methods
.method constructor <init>(Lcom/twitter/app/drafts/j$2;Landroid/widget/AdapterView;I[I)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/twitter/app/drafts/j$2$1;->d:Lcom/twitter/app/drafts/j$2;

    iput-object p2, p0, Lcom/twitter/app/drafts/j$2$1;->a:Landroid/widget/AdapterView;

    iput p3, p0, Lcom/twitter/app/drafts/j$2$1;->b:I

    iput-object p4, p0, Lcom/twitter/app/drafts/j$2$1;->c:[I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;II)V
    .locals 6

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/app/drafts/j$2$1;->a:Landroid/widget/AdapterView;

    iget v1, p0, Lcom/twitter/app/drafts/j$2$1;->b:I

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/a;

    .line 85
    iget-object v1, p0, Lcom/twitter/app/drafts/j$2$1;->c:[I

    aget v1, v1, p3

    const v2, 0x7f0a0321

    if-ne v1, v2, :cond_1

    .line 86
    iget-object v1, p0, Lcom/twitter/app/drafts/j$2$1;->d:Lcom/twitter/app/drafts/j$2;

    iget-object v1, v1, Lcom/twitter/app/drafts/j$2;->a:Lakr;

    iget-object v2, p0, Lcom/twitter/app/drafts/j$2$1;->d:Lcom/twitter/app/drafts/j$2;

    iget-object v2, v2, Lcom/twitter/app/drafts/j$2;->b:Laon;

    iget-object v3, p0, Lcom/twitter/app/drafts/j$2$1;->d:Lcom/twitter/app/drafts/j$2;

    iget-object v3, v3, Lcom/twitter/app/drafts/j$2;->c:Lcom/twitter/app/drafts/j$a;

    iget-object v4, p0, Lcom/twitter/app/drafts/j$2$1;->d:Lcom/twitter/app/drafts/j$2;

    iget-object v4, v4, Lcom/twitter/app/drafts/j$2;->d:Lcom/twitter/app/drafts/h;

    invoke-static {v1, v0, v2, v3, v4}, Lcom/twitter/app/drafts/j;->a(Lakr;Lcom/twitter/model/drafts/a;Laon;Lcom/twitter/app/drafts/j$a;Lcom/twitter/app/drafts/h;)V

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 87
    :cond_1
    iget-object v1, p0, Lcom/twitter/app/drafts/j$2$1;->c:[I

    aget v1, v1, p3

    const v2, 0x7f0a0320

    if-ne v1, v2, :cond_0

    .line 88
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    .line 89
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 90
    iget-wide v4, v0, Lcom/twitter/model/drafts/a;->b:J

    iget-object v0, p0, Lcom/twitter/app/drafts/j$2$1;->d:Lcom/twitter/app/drafts/j$2;

    iget-object v0, v0, Lcom/twitter/app/drafts/j$2;->e:Landroid/content/Context;

    invoke-static {v4, v5, v0, v2, v1}, Lcom/twitter/app/drafts/j;->a(JLandroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/p;)V

    goto :goto_0
.end method
