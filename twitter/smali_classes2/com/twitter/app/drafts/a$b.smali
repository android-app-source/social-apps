.class final Lcom/twitter/app/drafts/a$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/drafts/l;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/drafts/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/drafts/a;

.field private final b:Lant;

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laod;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/drafts/j$a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/base/BaseFragmentActivity;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lakr",
            "<",
            "Lcom/twitter/android/composer/a;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laol;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laon;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/drafts/j;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;>;"
        }
    .end annotation
.end field

.field private n:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/twitter/app/drafts/a;Lant;)V
    .locals 1

    .prologue
    .line 243
    iput-object p1, p0, Lcom/twitter/app/drafts/a$b;->a:Lcom/twitter/app/drafts/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244
    invoke-static {p2}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lant;

    iput-object v0, p0, Lcom/twitter/app/drafts/a$b;->b:Lant;

    .line 245
    invoke-direct {p0}, Lcom/twitter/app/drafts/a$b;->c()V

    .line 246
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/app/drafts/a;Lant;Lcom/twitter/app/drafts/a$1;)V
    .locals 0

    .prologue
    .line 216
    invoke-direct {p0, p1, p2}, Lcom/twitter/app/drafts/a$b;-><init>(Lcom/twitter/app/drafts/a;Lant;)V

    return-void
.end method

.method private c()V
    .locals 7

    .prologue
    .line 251
    iget-object v0, p0, Lcom/twitter/app/drafts/a$b;->b:Lant;

    .line 253
    invoke-static {v0}, Lanu;->a(Lant;)Ldagger/internal/c;

    move-result-object v0

    .line 252
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/drafts/a$b;->c:Lcta;

    .line 255
    iget-object v0, p0, Lcom/twitter/app/drafts/a$b;->c:Lcta;

    .line 257
    invoke-static {v0}, Lanx;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 256
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/drafts/a$b;->d:Lcta;

    .line 259
    iget-object v0, p0, Lcom/twitter/app/drafts/a$b;->b:Lant;

    .line 261
    invoke-static {v0}, Laoc;->a(Lant;)Ldagger/internal/c;

    move-result-object v0

    .line 260
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/drafts/a$b;->e:Lcta;

    .line 263
    iget-object v0, p0, Lcom/twitter/app/drafts/a$b;->e:Lcta;

    .line 265
    invoke-static {v0}, Laqm;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 264
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/drafts/a$b;->f:Lcta;

    .line 267
    iget-object v0, p0, Lcom/twitter/app/drafts/a$b;->c:Lcta;

    .line 269
    invoke-static {v0}, Lanv;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 268
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/drafts/a$b;->g:Lcta;

    .line 272
    iget-object v0, p0, Lcom/twitter/app/drafts/a$b;->g:Lcta;

    .line 274
    invoke-static {v0}, Laqd;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 273
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/drafts/a$b;->h:Lcta;

    .line 277
    iget-object v0, p0, Lcom/twitter/app/drafts/a$b;->c:Lcta;

    .line 278
    invoke-static {v0}, Laom;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/drafts/a$b;->i:Lcta;

    .line 280
    iget-object v0, p0, Lcom/twitter/app/drafts/a$b;->i:Lcta;

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/drafts/a$b;->j:Lcta;

    .line 285
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/drafts/a$b;->d:Lcta;

    iget-object v2, p0, Lcom/twitter/app/drafts/a$b;->f:Lcta;

    iget-object v3, p0, Lcom/twitter/app/drafts/a$b;->a:Lcom/twitter/app/drafts/a;

    .line 288
    invoke-static {v3}, Lcom/twitter/app/drafts/a;->a(Lcom/twitter/app/drafts/a;)Lcta;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/app/drafts/a$b;->h:Lcta;

    iget-object v5, p0, Lcom/twitter/app/drafts/a$b;->j:Lcta;

    iget-object v6, p0, Lcom/twitter/app/drafts/a$b;->a:Lcom/twitter/app/drafts/a;

    .line 291
    invoke-static {v6}, Lcom/twitter/app/drafts/a;->b(Lcom/twitter/app/drafts/a;)Lcta;

    move-result-object v6

    .line 284
    invoke-static/range {v0 .. v6}, Lcom/twitter/app/drafts/k;->a(Lcsd;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 283
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/drafts/a$b;->k:Lcta;

    .line 293
    iget-object v0, p0, Lcom/twitter/app/drafts/a$b;->k:Lcta;

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/drafts/a$b;->l:Lcta;

    .line 295
    const/4 v0, 0x2

    const/4 v1, 0x0

    .line 296
    invoke-static {v0, v1}, Ldagger/internal/f;->a(II)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/drafts/a$b;->a:Lcom/twitter/app/drafts/a;

    .line 297
    invoke-static {v1}, Lcom/twitter/app/drafts/a;->c(Lcom/twitter/app/drafts/a;)Lcta;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/drafts/a$b;->l:Lcta;

    .line 298
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    .line 299
    invoke-virtual {v0}, Ldagger/internal/f$a;->a()Ldagger/internal/f;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/drafts/a$b;->m:Lcta;

    .line 301
    iget-object v0, p0, Lcom/twitter/app/drafts/a$b;->k:Lcta;

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/drafts/a$b;->n:Lcta;

    .line 302
    return-void
.end method


# virtual methods
.method public a()Laog;
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/twitter/app/drafts/a$b;->n:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laog;

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 306
    iget-object v0, p0, Lcom/twitter/app/drafts/a$b;->m:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method
