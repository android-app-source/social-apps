.class public Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;
.super Lcom/twitter/app/common/dialog/BaseDialogFragment;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog$a;
    }
.end annotation


# static fields
.field private static final a:[J


# instance fields
.field private c:[J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    new-array v0, v0, [J

    sput-object v0, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;->a:[J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;-><init>()V

    .line 40
    sget-object v0, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;->a:[J

    iput-object v0, p0, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;->c:[J

    return-void
.end method

.method private a([J)Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;->c:[J

    .line 57
    return-object p0
.end method

.method public static a(Landroid/support/v4/app/FragmentManager;[J)V
    .locals 2

    .prologue
    .line 48
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 49
    new-instance v0, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;

    invoke-direct {v0}, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;-><init>()V

    .line 50
    invoke-direct {v0, p1}, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;->a([J)Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;

    move-result-object v0

    const-string/jumbo v1, "ConfirmRestartExpiredDrafts"

    .line 51
    invoke-virtual {v0, p0, v1}, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 52
    return-void
.end method


# virtual methods
.method c()V
    .locals 5

    .prologue
    .line 100
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog$a;

    .line 101
    invoke-virtual {p0}, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 102
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;->c:[J

    invoke-direct {v1, v2, v3, v4}, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog$a;-><init>(Landroid/app/Activity;Lcom/twitter/library/client/Session;[J)V

    .line 100
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 104
    return-void
.end method

.method e()V
    .locals 9

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 111
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v2

    .line 112
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 113
    iget-object v4, p0, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;->c:[J

    array-length v5, v4

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v5, :cond_0

    aget-wide v6, v4, v0

    .line 114
    new-instance v8, Lcom/twitter/android/client/tweetuploadmanager/b;

    invoke-direct {v8, v1, v3, v6, v7}, Lcom/twitter/android/client/tweetuploadmanager/b;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    .line 115
    invoke-virtual {v2, v8}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 113
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 117
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 63
    if-nez p1, :cond_0

    sget-object v0, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;->a:[J

    .line 65
    :goto_0
    iput-object v0, p0, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;->c:[J

    .line 68
    invoke-virtual {p0}, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0997

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;->c:[J

    array-length v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 69
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 70
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0779

    new-instance v2, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog$2;

    invoke-direct {v2, p0}, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog$2;-><init>(Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;)V

    .line 71
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00dc

    new-instance v2, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog$1;

    invoke-direct {v2, p0}, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog$1;-><init>(Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;)V

    .line 79
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 69
    return-object v0

    .line 63
    :cond_0
    const-string/jumbo v0, "expiredDraftIds"

    .line 66
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    sget-object v1, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;->a:[J

    .line 65
    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [J

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 92
    invoke-super {p0, p1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 93
    const-string/jumbo v0, "expiredDraftIds"

    iget-object v1, p0, Lcom/twitter/app/drafts/ConfirmRetryExpiredDraftsDialog;->c:[J

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 94
    return-void
.end method
