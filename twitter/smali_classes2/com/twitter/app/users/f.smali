.class public Lcom/twitter/app/users/f;
.super Lcom/twitter/app/common/base/h;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/base/h",
        "<",
        "Lcom/twitter/app/users/f;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/twitter/app/common/base/h;-><init>()V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/twitter/app/common/base/h;-><init>(Landroid/content/Intent;)V

    .line 52
    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/twitter/app/users/f;
    .locals 1

    .prologue
    .line 209
    new-instance v0, Lcom/twitter/app/users/f;

    invoke-direct {v0, p0}, Lcom/twitter/app/users/f;-><init>(Landroid/content/Intent;)V

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 4

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/app/users/f;->d:Landroid/content/Intent;

    const-string/jumbo v1, "owner_id"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 204
    const-class v0, Lcom/twitter/app/users/UsersActivity;

    invoke-virtual {p0, p1, v0}, Lcom/twitter/app/users/f;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lcom/twitter/app/users/f;
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/app/users/f;->d:Landroid/content/Intent;

    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 67
    return-object p0
.end method

.method public a(J)Lcom/twitter/app/users/f;
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/app/users/f;->d:Landroid/content/Intent;

    const-string/jumbo v1, "owner_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 57
    return-object p0
.end method

.method public a(Lcom/twitter/android/UsersAdapter$CheckboxConfig;)Lcom/twitter/app/users/f;
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/twitter/app/users/f;->d:Landroid/content/Intent;

    const-string/jumbo v1, "checkbox_config"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 129
    return-object p0
.end method

.method public a(Lcom/twitter/model/util/FriendshipCache;)Lcom/twitter/app/users/f;
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/twitter/app/users/f;->d:Landroid/content/Intent;

    const-string/jumbo v1, "friendship_cache"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 118
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/app/users/f;
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/app/users/f;->d:Landroid/content/Intent;

    const-string/jumbo v1, "category"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 85
    return-object p0
.end method

.method public a(Z)Lcom/twitter/app/users/f;
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/app/users/f;->d:Landroid/content/Intent;

    const-string/jumbo v1, "follow"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 141
    return-object p0
.end method

.method public a([J)Lcom/twitter/app/users/f;
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/twitter/app/users/f;->d:Landroid/content/Intent;

    const-string/jumbo v1, "user_ids"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 107
    return-object p0
.end method

.method public b()I
    .locals 3

    .prologue
    .line 73
    iget-object v0, p0, Lcom/twitter/app/users/f;->d:Landroid/content/Intent;

    const-string/jumbo v1, "type"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public b(J)Lcom/twitter/app/users/f;
    .locals 3

    .prologue
    .line 78
    iget-object v0, p0, Lcom/twitter/app/users/f;->d:Landroid/content/Intent;

    const-string/jumbo v1, "tag"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 79
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/app/users/f;
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/twitter/app/users/f;->d:Landroid/content/Intent;

    const-string/jumbo v1, "username"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 135
    return-object p0
.end method

.method public b(Z)Lcom/twitter/app/users/f;
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/twitter/app/users/f;->d:Landroid/content/Intent;

    const-string/jumbo v1, "hide_bio"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 151
    return-object p0
.end method

.method public c(J)Lcom/twitter/app/users/f;
    .locals 3

    .prologue
    .line 197
    iget-object v0, p0, Lcom/twitter/app/users/f;->d:Landroid/content/Intent;

    const-string/jumbo v1, "target_session_owner_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 198
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/app/users/f;
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/twitter/app/users/f;->d:Landroid/content/Intent;

    const-string/jumbo v1, "owner_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 157
    return-object p0
.end method

.method public c(Z)Lcom/twitter/app/users/f;
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/twitter/app/users/f;->d:Landroid/content/Intent;

    const-string/jumbo v1, "fetch_always"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 169
    return-object p0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/twitter/app/users/f;->d:Landroid/content/Intent;

    const-string/jumbo v1, "category_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/app/users/f;
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/twitter/app/users/f;->d:Landroid/content/Intent;

    const-string/jumbo v1, "follow_request_sender"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 175
    return-object p0
.end method

.method public d()[J
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/twitter/app/users/f;->d:Landroid/content/Intent;

    const-string/jumbo v1, "user_ids"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/twitter/android/UsersAdapter$CheckboxConfig;
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/twitter/app/users/f;->d:Landroid/content/Intent;

    const-string/jumbo v1, "checkbox_config"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    return-object v0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/app/users/f;
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lcom/twitter/app/users/f;->d:Landroid/content/Intent;

    const-string/jumbo v1, "scribe_page"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 186
    return-object p0
.end method

.method public e(Z)Lcom/twitter/app/users/f;
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/twitter/app/users/f;->d:Landroid/content/Intent;

    const-string/jumbo v1, "show_category_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 192
    return-object p0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/twitter/app/users/f;->d:Landroid/content/Intent;

    const-string/jumbo v1, "follow_request_sender"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
