.class public abstract Lcom/twitter/app/users/InjectableUsersFragment;
.super Lcom/twitter/app/users/UsersFragment;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/app/users/b;",
        ">",
        "Lcom/twitter/app/users/UsersFragment;"
    }
.end annotation


# instance fields
.field protected a:Lcom/twitter/app/users/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/twitter/app/users/UsersFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public b()V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0}, Lcom/twitter/app/users/UsersFragment;->b()V

    .line 46
    iget-object v0, p0, Lcom/twitter/app/users/InjectableUsersFragment;->a:Lcom/twitter/app/users/b;

    invoke-virtual {v0}, Lcom/twitter/app/users/b;->a()V

    .line 47
    return-void
.end method

.method k(I)Lcom/twitter/android/ag;
    .locals 9
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 64
    new-instance v0, Lcom/twitter/android/ag;

    invoke-virtual {p0}, Lcom/twitter/app/users/InjectableUsersFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v4, p0, Lcom/twitter/app/users/InjectableUsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    .line 65
    invoke-virtual {p0}, Lcom/twitter/app/users/InjectableUsersFragment;->C()Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    move-result-object v5

    invoke-virtual {p0}, Lcom/twitter/app/users/InjectableUsersFragment;->Q()Z

    move-result v6

    invoke-virtual {p0}, Lcom/twitter/app/users/InjectableUsersFragment;->aO()Z

    move-result v7

    new-instance v8, Lcom/twitter/android/ag$a;

    invoke-direct {v8}, Lcom/twitter/android/ag$a;-><init>()V

    move v2, p1

    move-object v3, p0

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/ag;-><init>(Landroid/content/Context;ILcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/android/UsersAdapter$CheckboxConfig;ZZLcom/twitter/android/ag$b;)V

    .line 64
    return-object v0
.end method

.method protected l(I)V
    .locals 7
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x1

    .line 54
    invoke-virtual {p0, p1}, Lcom/twitter/app/users/InjectableUsersFragment;->k(I)Lcom/twitter/android/ag;

    move-result-object v0

    .line 55
    invoke-virtual {v0, p0}, Lcom/twitter/android/UsersAdapter;->a(Lcom/twitter/android/av;)V

    .line 56
    new-instance v1, Lcom/twitter/android/widget/ap;

    const/4 v2, 0x0

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/widget/BaseAdapter;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/app/users/InjectableUsersFragment;->a:Lcom/twitter/app/users/b;

    .line 57
    invoke-virtual {v5}, Lcom/twitter/app/users/b;->c()Lcom/twitter/android/UsersAdapter;

    move-result-object v5

    aput-object v5, v3, v4

    aput-object v0, v3, v6

    const v4, 0x7f04008c

    invoke-direct {v1, v2, v3, v4, v6}, Lcom/twitter/android/widget/ap;-><init>([I[Landroid/widget/BaseAdapter;II)V

    iput-object v1, p0, Lcom/twitter/app/users/InjectableUsersFragment;->p:Lcom/twitter/android/widget/ap;

    .line 59
    invoke-virtual {p0}, Lcom/twitter/app/users/InjectableUsersFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/users/InjectableUsersFragment;->p:Lcom/twitter/android/widget/ap;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/app/common/list/l;->a(Lcjr;Landroid/widget/ListAdapter;)V

    .line 60
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 24
    invoke-super {p0, p1}, Lcom/twitter/app/users/UsersFragment;->onCreate(Landroid/os/Bundle;)V

    .line 25
    invoke-virtual {p0}, Lcom/twitter/app/users/InjectableUsersFragment;->q()Lcom/twitter/app/users/b;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/users/InjectableUsersFragment;->a:Lcom/twitter/app/users/b;

    .line 27
    if-eqz p1, :cond_0

    .line 28
    iget-object v1, p0, Lcom/twitter/app/users/InjectableUsersFragment;->a:Lcom/twitter/app/users/b;

    invoke-virtual {v1, p1}, Lcom/twitter/app/users/b;->b(Landroid/os/Bundle;)V

    .line 29
    const-string/jumbo v1, "fetched"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 33
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/twitter/app/users/InjectableUsersFragment;->a:Lcom/twitter/app/users/b;

    invoke-virtual {v1, v0}, Lcom/twitter/app/users/b;->a(Z)V

    .line 34
    return-void

    .line 29
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/twitter/app/users/UsersFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 39
    const-string/jumbo v0, "fetched"

    iget-object v1, p0, Lcom/twitter/app/users/InjectableUsersFragment;->a:Lcom/twitter/app/users/b;

    invoke-virtual {v1}, Lcom/twitter/app/users/b;->c()Lcom/twitter/android/UsersAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/UsersAdapter;->k()Lcjt;

    move-result-object v1

    invoke-interface {v1}, Lcjt;->b()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 40
    iget-object v0, p0, Lcom/twitter/app/users/InjectableUsersFragment;->a:Lcom/twitter/app/users/b;

    invoke-virtual {v0, p1}, Lcom/twitter/app/users/b;->a(Landroid/os/Bundle;)V

    .line 41
    return-void
.end method

.method public abstract q()Lcom/twitter/app/users/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method
