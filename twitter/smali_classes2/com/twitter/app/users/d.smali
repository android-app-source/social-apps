.class public Lcom/twitter/app/users/d;
.super Lckb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lckb",
        "<",
        "Lcom/twitter/android/timeline/co;",
        "Lcom/twitter/app/users/e;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:Lcom/twitter/app/users/c;

.field private final c:Lcom/twitter/model/util/FriendshipCache;

.field private final d:J


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Lcom/twitter/app/users/c;Lcom/twitter/model/util/FriendshipCache;J)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lckb;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/twitter/app/users/d;->a:Landroid/view/LayoutInflater;

    .line 29
    iput-object p2, p0, Lcom/twitter/app/users/d;->b:Lcom/twitter/app/users/c;

    .line 30
    iput-object p3, p0, Lcom/twitter/app/users/d;->c:Lcom/twitter/model/util/FriendshipCache;

    .line 31
    iput-wide p4, p0, Lcom/twitter/app/users/d;->d:J

    .line 32
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;)Lcom/twitter/app/users/e;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/twitter/app/users/d;->a:Landroid/view/LayoutInflater;

    invoke-static {v0, p1}, Lcom/twitter/app/users/e;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/twitter/app/users/e;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lckb$a;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lcom/twitter/app/users/e;

    check-cast p2, Lcom/twitter/android/timeline/co;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/users/d;->a(Lcom/twitter/app/users/e;Lcom/twitter/android/timeline/co;)V

    return-void
.end method

.method public a(Lcom/twitter/app/users/e;Lcom/twitter/android/timeline/co;)V
    .locals 6

    .prologue
    .line 43
    invoke-virtual {p1}, Lcom/twitter/app/users/e;->a()Lcom/twitter/ui/user/UserSocialView;

    move-result-object v0

    .line 44
    iget-object v1, p2, Lcom/twitter/android/timeline/co;->a:Lcom/twitter/model/core/TwitterUser;

    .line 45
    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setUser(Lcom/twitter/model/core/TwitterUser;)V

    .line 46
    iget-object v2, v1, Lcom/twitter/model/core/TwitterUser;->f:Ljava/lang/String;

    iget-object v3, v1, Lcom/twitter/model/core/TwitterUser;->C:Lcom/twitter/model/core/v;

    invoke-virtual {v0, v2, v3}, Lcom/twitter/ui/user/UserSocialView;->a(Ljava/lang/String;Lcom/twitter/model/core/v;)V

    .line 47
    iget-object v2, p2, Lcom/twitter/android/timeline/co;->h:Lcom/twitter/model/core/TwitterSocialProof;

    invoke-static {}, Lcom/twitter/util/z;->g()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/twitter/ui/user/UserSocialView;->a(Lcom/twitter/model/core/TwitterSocialProof;Z)V

    .line 48
    iget-object v2, p0, Lcom/twitter/app/users/d;->c:Lcom/twitter/model/util/FriendshipCache;

    if-eqz v2, :cond_0

    .line 49
    iget-object v2, p0, Lcom/twitter/app/users/d;->c:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v2, v1}, Lcom/twitter/model/util/FriendshipCache;->a(Lcom/twitter/model/core/TwitterUser;)V

    .line 50
    iget-object v2, p0, Lcom/twitter/app/users/d;->c:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v1}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/twitter/model/util/FriendshipCache;->j(J)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setIsFollowing(Z)V

    .line 52
    :cond_0
    iget-wide v2, p0, Lcom/twitter/app/users/d;->d:J

    iget-object v1, p2, Lcom/twitter/android/timeline/co;->a:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v1}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 53
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setFollowVisibility(I)V

    .line 61
    :goto_0
    return-void

    .line 56
    :cond_1
    iget-object v1, p0, Lcom/twitter/app/users/d;->b:Lcom/twitter/app/users/c;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setActionButtonClickListener(Lcom/twitter/ui/user/BaseUserView$a;)V

    .line 57
    iget-object v1, p0, Lcom/twitter/app/users/d;->b:Lcom/twitter/app/users/c;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->setFollowVisibility(I)V

    .line 59
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserSocialView;->c(Z)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/timeline/co;)Z
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 18
    check-cast p1, Lcom/twitter/android/timeline/co;

    invoke-virtual {p0, p1}, Lcom/twitter/app/users/d;->a(Lcom/twitter/android/timeline/co;)Z

    move-result v0

    return v0
.end method

.method public synthetic b(Landroid/view/ViewGroup;)Lckb$a;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/twitter/app/users/d;->a(Landroid/view/ViewGroup;)Lcom/twitter/app/users/e;

    move-result-object v0

    return-object v0
.end method
