.class public abstract Lcom/twitter/app/users/g$a;
.super Lcom/twitter/app/common/list/i$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/users/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<U:",
        "Lcom/twitter/app/users/g$a",
        "<TU;>;>",
        "Lcom/twitter/app/common/list/i$a",
        "<TU;>;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/twitter/app/common/list/i$a;-><init>()V

    .line 180
    return-void
.end method

.method protected constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 183
    invoke-direct {p0, p1}, Lcom/twitter/app/common/list/i$a;-><init>(Landroid/os/Bundle;)V

    .line 184
    return-void
.end method

.method protected constructor <init>(Lcom/twitter/app/users/g;)V
    .locals 0

    .prologue
    .line 187
    invoke-direct {p0, p1}, Lcom/twitter/app/common/list/i$a;-><init>(Lcom/twitter/app/common/list/i;)V

    .line 188
    return-void
.end method


# virtual methods
.method public a(I)Lcom/twitter/app/users/g$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TU;"
        }
    .end annotation

    .prologue
    .line 198
    iget-object v0, p0, Lcom/twitter/app/users/g$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "fast_follow"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 199
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$a;

    return-object v0
.end method

.method public a(J)Lcom/twitter/app/users/g$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TU;"
        }
    .end annotation

    .prologue
    .line 216
    iget-object v0, p0, Lcom/twitter/app/users/g$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "tag"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 217
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$a;

    return-object v0
.end method

.method public a(Lcom/twitter/model/util/FriendshipCache;)Lcom/twitter/app/users/g$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/util/FriendshipCache;",
            ")TU;"
        }
    .end annotation

    .prologue
    .line 246
    iget-object v0, p0, Lcom/twitter/app/users/g$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "friendship_cache"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 247
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$a;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/app/users/g$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TU;"
        }
    .end annotation

    .prologue
    .line 300
    iget-object v0, p0, Lcom/twitter/app/users/g$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "follow_request_sender"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$a;

    return-object v0
.end method

.method public a(Z)Lcom/twitter/app/users/g$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TU;"
        }
    .end annotation

    .prologue
    .line 192
    iget-object v0, p0, Lcom/twitter/app/users/g$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "follow"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 193
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$a;

    return-object v0
.end method

.method public a()Lcom/twitter/app/users/g;
    .locals 2

    .prologue
    .line 337
    new-instance v0, Lcom/twitter/app/users/g;

    iget-object v1, p0, Lcom/twitter/app/users/g$a;->a:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Lcom/twitter/app/users/g;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method

.method public synthetic b()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/twitter/app/users/g$a;->a()Lcom/twitter/app/users/g;

    move-result-object v0

    return-object v0
.end method

.method public b(J)Lcom/twitter/app/users/g$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TU;"
        }
    .end annotation

    .prologue
    .line 324
    iget-object v0, p0, Lcom/twitter/app/users/g$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "target_session_owner_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 325
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$a;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/app/users/g$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TU;"
        }
    .end annotation

    .prologue
    .line 318
    iget-object v0, p0, Lcom/twitter/app/users/g$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "scribe_page"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$a;

    return-object v0
.end method

.method public b(Z)Lcom/twitter/app/users/g$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TU;"
        }
    .end annotation

    .prologue
    .line 234
    iget-object v0, p0, Lcom/twitter/app/users/g$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "follow"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 235
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$a;

    return-object v0
.end method

.method public synthetic c()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/twitter/app/users/g$a;->a()Lcom/twitter/app/users/g;

    move-result-object v0

    return-object v0
.end method

.method public c(Z)Lcom/twitter/app/users/g$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TU;"
        }
    .end annotation

    .prologue
    .line 282
    iget-object v0, p0, Lcom/twitter/app/users/g$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "fetch_always"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 283
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$a;

    return-object v0
.end method

.method public g(I)Lcom/twitter/app/users/g$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TU;"
        }
    .end annotation

    .prologue
    .line 204
    iget-object v0, p0, Lcom/twitter/app/users/g$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "followers_count"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 205
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$a;

    return-object v0
.end method

.method public g(Z)Lcom/twitter/app/users/g$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TU;"
        }
    .end annotation

    .prologue
    .line 288
    iget-object v0, p0, Lcom/twitter/app/users/g$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "hide_bio"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 289
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$a;

    return-object v0
.end method

.method public h(I)Lcom/twitter/app/users/g$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TU;"
        }
    .end annotation

    .prologue
    .line 210
    iget-object v0, p0, Lcom/twitter/app/users/g$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 211
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$a;

    return-object v0
.end method

.method public h(Z)Lcom/twitter/app/users/g$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TU;"
        }
    .end annotation

    .prologue
    .line 294
    iget-object v0, p0, Lcom/twitter/app/users/g$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "is_hidden"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 295
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$a;

    return-object v0
.end method

.method public i(I)Lcom/twitter/app/users/g$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TU;"
        }
    .end annotation

    .prologue
    .line 264
    iget-object v0, p0, Lcom/twitter/app/users/g$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "limit"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 265
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$a;

    return-object v0
.end method
