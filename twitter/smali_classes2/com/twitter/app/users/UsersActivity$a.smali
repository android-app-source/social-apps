.class Lcom/twitter/app/users/UsersActivity$a;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/users/UsersActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field public final j:Z

.field public final k:I


# direct methods
.method constructor <init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 232
    invoke-direct {p0, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;-><init>(Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 233
    invoke-virtual {p0, v1}, Lcom/twitter/app/users/UsersActivity$a;->c(Z)V

    .line 234
    invoke-virtual {p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 235
    invoke-static {v2}, Lcom/twitter/app/users/f;->a(Landroid/content/Intent;)Lcom/twitter/app/users/f;

    move-result-object v2

    .line 236
    invoke-virtual {v2}, Lcom/twitter/app/users/f;->b()I

    move-result v3

    iput v3, p0, Lcom/twitter/app/users/UsersActivity$a;->k:I

    .line 237
    iget v3, p0, Lcom/twitter/app/users/UsersActivity$a;->k:I

    sparse-switch v3, :sswitch_data_0

    .line 285
    invoke-virtual {p0, v1}, Lcom/twitter/app/users/UsersActivity$a;->a(Z)V

    .line 286
    invoke-virtual {v2}, Lcom/twitter/app/users/f;->d()[J

    move-result-object v2

    if-nez v2, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/app/users/UsersActivity$a;->j:Z

    .line 290
    :goto_1
    return-void

    .line 239
    :sswitch_0
    invoke-virtual {p0, v1}, Lcom/twitter/app/users/UsersActivity$a;->a(Z)V

    .line 240
    iput-boolean v1, p0, Lcom/twitter/app/users/UsersActivity$a;->j:Z

    goto :goto_1

    .line 248
    :sswitch_1
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_2
    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersActivity$a;->a(Z)V

    .line 249
    iput-boolean v1, p0, Lcom/twitter/app/users/UsersActivity$a;->j:Z

    .line 250
    invoke-virtual {p0, v1}, Lcom/twitter/app/users/UsersActivity$a;->d(I)V

    goto :goto_1

    :cond_0
    move v0, v1

    .line 248
    goto :goto_2

    .line 255
    :sswitch_2
    invoke-virtual {p0, v1}, Lcom/twitter/app/users/UsersActivity$a;->c(Z)V

    .line 256
    invoke-virtual {p0, v1}, Lcom/twitter/app/users/UsersActivity$a;->d(Z)V

    .line 257
    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersActivity$a;->a(Z)V

    .line 258
    iput-boolean v0, p0, Lcom/twitter/app/users/UsersActivity$a;->j:Z

    .line 259
    invoke-virtual {p0, v1}, Lcom/twitter/app/users/UsersActivity$a;->d(I)V

    goto :goto_1

    .line 263
    :sswitch_3
    iput-boolean v0, p0, Lcom/twitter/app/users/UsersActivity$a;->j:Z

    goto :goto_1

    .line 267
    :sswitch_4
    invoke-virtual {p0, v1}, Lcom/twitter/app/users/UsersActivity$a;->d(I)V

    .line 268
    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersActivity$a;->b(Z)V

    .line 269
    iput-boolean v1, p0, Lcom/twitter/app/users/UsersActivity$a;->j:Z

    goto :goto_1

    .line 273
    :sswitch_5
    invoke-virtual {p0, v1}, Lcom/twitter/app/users/UsersActivity$a;->a(Z)V

    .line 274
    invoke-virtual {p0, v1}, Lcom/twitter/app/users/UsersActivity$a;->d(I)V

    .line 275
    iput-boolean v1, p0, Lcom/twitter/app/users/UsersActivity$a;->j:Z

    goto :goto_1

    .line 279
    :sswitch_6
    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersActivity$a;->a(Z)V

    .line 280
    invoke-virtual {p0, v1}, Lcom/twitter/app/users/UsersActivity$a;->d(I)V

    .line 281
    iput-boolean v1, p0, Lcom/twitter/app/users/UsersActivity$a;->j:Z

    goto :goto_1

    :cond_1
    move v0, v1

    .line 286
    goto :goto_0

    .line 237
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_2
        0x6 -> :sswitch_0
        0x7 -> :sswitch_1
        0x8 -> :sswitch_1
        0xa -> :sswitch_1
        0xd -> :sswitch_1
        0x12 -> :sswitch_3
        0x14 -> :sswitch_1
        0x1a -> :sswitch_2
        0x29 -> :sswitch_4
        0x2a -> :sswitch_6
        0x2c -> :sswitch_5
    .end sparse-switch
.end method
