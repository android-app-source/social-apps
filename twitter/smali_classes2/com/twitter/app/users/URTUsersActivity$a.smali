.class public Lcom/twitter/app/users/URTUsersActivity$a;
.super Lcom/twitter/app/common/base/h;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/users/URTUsersActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/base/h",
        "<",
        "Lcom/twitter/app/users/URTUsersActivity$a;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:J


# direct methods
.method private constructor <init>(J)V
    .locals 3

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/twitter/app/common/base/h;-><init>()V

    .line 60
    iput-wide p1, p0, Lcom/twitter/app/users/URTUsersActivity$a;->a:J

    .line 61
    iget-object v0, p0, Lcom/twitter/app/users/URTUsersActivity$a;->d:Landroid/content/Intent;

    const-string/jumbo v1, "content_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 62
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/URTUsersActivity$a;->d(Z)Lcom/twitter/app/common/base/h;

    .line 63
    return-void
.end method

.method synthetic constructor <init>(JLcom/twitter/app/users/URTUsersActivity$1;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/twitter/app/users/URTUsersActivity$a;-><init>(J)V

    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/twitter/app/users/URTUsersActivity$a;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/users/URTUsersActivity$a;->a(Landroid/os/Bundle;)Lcom/twitter/app/users/URTUsersActivity$a;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/os/Bundle;)Lcom/twitter/app/users/URTUsersActivity$a;
    .locals 4

    .prologue
    .line 74
    const-string/jumbo v0, "content_id"

    const-wide/16 v2, -0x1

    invoke-virtual {p0, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 75
    new-instance v2, Lcom/twitter/app/users/URTUsersActivity$a;

    invoke-direct {v2, v0, v1}, Lcom/twitter/app/users/URTUsersActivity$a;-><init>(J)V

    return-object v2
.end method
