.class public abstract Lcom/twitter/app/users/FollowActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/app/users/UsersFragment$c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/users/FollowActivity$a;,
        Lcom/twitter/app/users/FollowActivity$b;
    }
.end annotation


# instance fields
.field protected a:Lcom/twitter/app/users/UsersFragment;

.field protected b:Lcom/twitter/app/users/FollowActivity$a;

.field protected c:Z

.field private d:I

.field private e:I

.field private f:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/users/FollowActivity;->c:Z

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/app/users/FollowActivity;->d:I

    .line 46
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/app/users/FollowActivity;->e:I

    return-void
.end method

.method private a(II)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 132
    if-eq p2, v0, :cond_0

    .line 133
    iput p2, p0, Lcom/twitter/app/users/FollowActivity;->e:I

    .line 134
    if-eq p1, v0, :cond_0

    .line 135
    iget v0, p0, Lcom/twitter/app/users/FollowActivity;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/app/users/FollowActivity;->d:I

    .line 138
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/users/FollowActivity;II)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/twitter/app/users/FollowActivity;->a(II)V

    return-void
.end method

.method private s()V
    .locals 3

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/twitter/app/users/FollowActivity;->o()Landroid/content/BroadcastReceiver;

    move-result-object v0

    .line 103
    if-eqz v0, :cond_0

    .line 104
    iput-object v0, p0, Lcom/twitter/app/users/FollowActivity;->f:Landroid/content/BroadcastReceiver;

    .line 105
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/users/FollowActivity;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0}, Lcom/twitter/app/users/FollowActivity;->q()Landroid/content/IntentFilter;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 107
    :cond_0
    return-void
.end method

.method private t()F
    .locals 4

    .prologue
    .line 205
    iget v0, p0, Lcom/twitter/app/users/FollowActivity;->e:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/users/FollowActivity;->a:Lcom/twitter/app/users/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/app/users/UsersFragment;->O()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/app/users/FollowActivity;->d:I

    int-to-float v0, v0

    iget v1, p0, Lcom/twitter/app/users/FollowActivity;->e:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 209
    :goto_0
    iget-object v1, p0, Lcom/twitter/app/users/FollowActivity;->a:Lcom/twitter/app/users/UsersFragment;

    invoke-virtual {v1}, Lcom/twitter/app/users/UsersFragment;->M()Z

    move-result v1

    if-nez v1, :cond_1

    .line 218
    :goto_1
    return v0

    .line 205
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 213
    :cond_1
    iget-object v1, p0, Lcom/twitter/app/users/FollowActivity;->a:Lcom/twitter/app/users/UsersFragment;

    invoke-virtual {v1}, Lcom/twitter/app/users/UsersFragment;->G()I

    move-result v1

    .line 214
    iget-object v2, p0, Lcom/twitter/app/users/FollowActivity;->a:Lcom/twitter/app/users/UsersFragment;

    invoke-virtual {v2}, Lcom/twitter/app/users/UsersFragment;->N()I

    move-result v2

    int-to-float v2, v2

    int-to-float v3, v1

    div-float/2addr v2, v3

    .line 218
    int-to-float v1, v1

    div-float/2addr v0, v1

    add-float/2addr v0, v2

    goto :goto_1
.end method

.method private u()Z
    .locals 2

    .prologue
    .line 229
    invoke-direct {p0}, Lcom/twitter/app/users/FollowActivity;->t()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-virtual {p0, p1}, Lcom/twitter/app/users/FollowActivity;->a(Landroid/os/Bundle;)Lcom/twitter/app/users/FollowActivity$a;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/FollowActivity$a;

    iput-object v0, p0, Lcom/twitter/app/users/FollowActivity;->b:Lcom/twitter/app/users/FollowActivity$a;

    .line 55
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 56
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(I)V

    .line 57
    iget-object v0, p0, Lcom/twitter/app/users/FollowActivity;->b:Lcom/twitter/app/users/FollowActivity$a;

    iget v0, v0, Lcom/twitter/app/users/FollowActivity$a;->b:I

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 58
    const/16 v0, 0xa

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(I)V

    .line 60
    return-object p2
.end method

.method protected abstract a(Landroid/os/Bundle;)Lcom/twitter/app/users/FollowActivity$a;
.end method

.method protected a(Lcom/twitter/app/users/g;)Lcom/twitter/app/users/UsersFragment;
    .locals 1

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/twitter/app/users/FollowActivity;->i()Lcom/twitter/app/users/UsersFragment;

    move-result-object v0

    .line 164
    invoke-virtual {v0, p1}, Lcom/twitter/app/users/UsersFragment;->a(Lcom/twitter/app/common/base/b;)V

    .line 165
    invoke-virtual {v0, p0}, Lcom/twitter/app/users/UsersFragment;->a(Lcom/twitter/app/users/UsersFragment$c;)V

    .line 166
    return-object v0
.end method

.method protected a(Landroid/content/Intent;)Lcom/twitter/app/users/g;
    .locals 2

    .prologue
    .line 176
    .line 177
    invoke-static {p1}, Lcom/twitter/app/users/g$b;->a(Landroid/content/Intent;)Lcom/twitter/app/users/g$b;

    move-result-object v0

    const/4 v1, 0x0

    .line 178
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/g$b;->e(Z)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$b;

    iget-object v1, p0, Lcom/twitter/app/users/FollowActivity;->b:Lcom/twitter/app/users/FollowActivity$a;

    iget-object v1, v1, Lcom/twitter/app/users/FollowActivity$a;->a:Ljava/lang/String;

    .line 179
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/g$b;->b(Ljava/lang/String;)Lcom/twitter/app/users/g$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$b;

    const v1, 0x7f0a036e

    .line 180
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/g$b;->b(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$b;

    const/16 v1, 0x3e8

    .line 181
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/g$b;->i(I)Lcom/twitter/app/users/g$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$b;

    const/16 v1, 0x1c

    .line 182
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/g$b;->h(I)Lcom/twitter/app/users/g$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$b;

    const/4 v1, 0x1

    .line 183
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/g$b;->b(Z)Lcom/twitter/app/users/g$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$b;

    invoke-virtual {v0}, Lcom/twitter/app/users/g$b;->a()Lcom/twitter/app/users/g;

    move-result-object v0

    .line 176
    return-object v0
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 3

    .prologue
    const v2, 0x7f1302e4

    .line 79
    if-nez p1, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/twitter/app/users/FollowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/FollowActivity;->a(Landroid/content/Intent;)Lcom/twitter/app/users/g;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/FollowActivity;->a(Lcom/twitter/app/users/g;)Lcom/twitter/app/users/UsersFragment;

    move-result-object v0

    .line 81
    invoke-virtual {p0}, Lcom/twitter/app/users/FollowActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 82
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 83
    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 84
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 94
    :goto_0
    iput-object v0, p0, Lcom/twitter/app/users/FollowActivity;->a:Lcom/twitter/app/users/UsersFragment;

    .line 96
    const v0, 0x7f13038d

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/FollowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 98
    invoke-direct {p0}, Lcom/twitter/app/users/FollowActivity;->s()V

    .line 99
    return-void

    .line 86
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/users/FollowActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 87
    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/UsersFragment;

    .line 88
    invoke-virtual {v0, p0}, Lcom/twitter/app/users/UsersFragment;->a(Lcom/twitter/app/users/UsersFragment$c;)V

    .line 89
    const-string/jumbo v1, "page_count"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/twitter/app/users/FollowActivity;->d:I

    .line 90
    const-string/jumbo v1, "page_total"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/twitter/app/users/FollowActivity;->e:I

    .line 91
    const-string/jumbo v1, "should_show_loading_in_title"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/app/users/FollowActivity;->c:Z

    goto :goto_0
.end method

.method protected d()V
    .locals 2

    .prologue
    .line 250
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d()V

    .line 251
    iget-object v0, p0, Lcom/twitter/app/users/FollowActivity;->f:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 252
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/users/FollowActivity;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 254
    :cond_0
    return-void
.end method

.method protected i()Lcom/twitter/app/users/UsersFragment;
    .locals 1

    .prologue
    .line 171
    new-instance v0, Lcom/twitter/app/users/UsersFragment;

    invoke-direct {v0}, Lcom/twitter/app/users/UsersFragment;-><init>()V

    return-object v0
.end method

.method public j()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 143
    invoke-virtual {p0}, Lcom/twitter/app/users/FollowActivity;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    iput-boolean v4, p0, Lcom/twitter/app/users/FollowActivity;->c:Z

    .line 147
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/users/FollowActivity;->n()V

    .line 148
    invoke-virtual {p0}, Lcom/twitter/app/users/FollowActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->h()V

    .line 151
    iget v0, p0, Lcom/twitter/app/users/FollowActivity;->d:I

    if-ne v0, v5, :cond_1

    .line 152
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/users/FollowActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/app/users/FollowActivity;->b:Lcom/twitter/app/users/FollowActivity$a;

    iget-object v2, v2, Lcom/twitter/app/users/FollowActivity$a;->a:Ljava/lang/String;

    aput-object v2, v1, v4

    const-string/jumbo v2, "follow_friends:stream::results"

    aput-object v2, v1, v5

    .line 153
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 152
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 155
    :cond_1
    return-void
.end method

.method protected l()Z
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/twitter/app/users/FollowActivity;->a:Lcom/twitter/app/users/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/app/users/UsersFragment;->M()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/app/users/FollowActivity;->e:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/twitter/app/users/FollowActivity;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected n()V
    .locals 6

    .prologue
    .line 187
    iget-boolean v0, p0, Lcom/twitter/app/users/FollowActivity;->c:Z

    if-eqz v0, :cond_0

    .line 188
    iget v0, p0, Lcom/twitter/app/users/FollowActivity;->e:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/app/users/FollowActivity;->a:Lcom/twitter/app/users/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/app/users/UsersFragment;->M()Z

    move-result v0

    if-nez v0, :cond_1

    .line 189
    const v0, 0x7f0a04b6

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/FollowActivity;->setTitle(I)V

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 191
    :cond_1
    const v0, 0x7f0a04b8

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 192
    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v3

    invoke-direct {p0}, Lcom/twitter/app/users/FollowActivity;->t()F

    move-result v4

    float-to-double v4, v4

    invoke-virtual {v3, v4, v5}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 191
    invoke-virtual {p0, v0, v1}, Lcom/twitter/app/users/FollowActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/FollowActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected o()Landroid/content/BroadcastReceiver;
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    .line 69
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/users/FollowActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/app/users/FollowActivity;->b:Lcom/twitter/app/users/FollowActivity$a;

    iget-object v3, v3, Lcom/twitter/app/users/FollowActivity$a;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 70
    invoke-virtual {p0}, Lcom/twitter/app/users/FollowActivity;->r()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "back_button:click"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 69
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 71
    iget-object v0, p0, Lcom/twitter/app/users/FollowActivity;->b:Lcom/twitter/app/users/FollowActivity$a;

    iget-boolean v0, v0, Lcom/twitter/app/users/FollowActivity$a;->c:Z

    if-eqz v0, :cond_0

    .line 72
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onBackPressed()V

    .line 74
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 259
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 121
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 122
    const-string/jumbo v0, "page_count"

    iget v1, p0, Lcom/twitter/app/users/FollowActivity;->d:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 123
    const-string/jumbo v0, "page_total"

    iget v1, p0, Lcom/twitter/app/users/FollowActivity;->e:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 124
    const-string/jumbo v0, "should_show_loading_in_title"

    iget-boolean v1, p0, Lcom/twitter/app/users/FollowActivity;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 125
    return-void
.end method

.method protected q()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 116
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "upload_success_broadcast"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method protected r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/twitter/app/users/FollowActivity;->a:Lcom/twitter/app/users/UsersFragment;

    iget v0, v0, Lcom/twitter/app/users/UsersFragment;->t:I

    sparse-switch v0, :sswitch_data_0

    .line 243
    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    .line 237
    :sswitch_0
    const-string/jumbo v0, "follow_friends"

    goto :goto_0

    .line 240
    :sswitch_1
    const-string/jumbo v0, "follow_interest_suggestions"

    goto :goto_0

    .line 234
    nop

    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_0
        0x1c -> :sswitch_0
        0x20 -> :sswitch_1
    .end sparse-switch
.end method
