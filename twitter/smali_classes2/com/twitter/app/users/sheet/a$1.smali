.class Lcom/twitter/app/users/sheet/a$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/users/sheet/a;-><init>(Landroid/content/Context;Lcom/twitter/app/users/sheet/a$b;Lcom/twitter/model/util/FriendshipCache;Landroid/widget/ListView;ILcom/twitter/media/ui/image/config/CommonRoundingStrategy;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/users/sheet/a;


# direct methods
.method constructor <init>(Lcom/twitter/app/users/sheet/a;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/twitter/app/users/sheet/a$1;->a:Lcom/twitter/app/users/sheet/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 77
    instance-of v0, p2, Lcom/twitter/ui/user/UserView;

    if-eqz v0, :cond_0

    .line 78
    invoke-static {p2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/user/UserView;

    .line 79
    iget-object v1, p0, Lcom/twitter/app/users/sheet/a$1;->a:Lcom/twitter/app/users/sheet/a;

    invoke-virtual {v0}, Lcom/twitter/ui/user/UserView;->getUserId()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/twitter/ui/user/UserView;->getUserName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/twitter/ui/user/UserView;->getPromotedContent()Lcgi;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/twitter/app/users/sheet/a;->a(JLjava/lang/String;Lcgi;)V

    .line 81
    :cond_0
    return-void
.end method
