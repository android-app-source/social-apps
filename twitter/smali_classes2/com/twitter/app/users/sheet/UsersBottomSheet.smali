.class public Lcom/twitter/app/users/sheet/UsersBottomSheet;
.super Landroid/support/design/widget/BottomSheetDialogFragment;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/users/sheet/UsersBottomSheet$b;,
        Lcom/twitter/app/users/sheet/UsersBottomSheet$a;
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/app/users/sheet/a;

.field private b:Lcom/twitter/app/users/sheet/b;

.field private c:[J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/support/design/widget/BottomSheetDialogFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/twitter/app/users/sheet/UsersBottomSheet$a;)Lcom/twitter/app/users/sheet/UsersBottomSheet;
    .locals 4

    .prologue
    .line 50
    new-instance v0, Lcom/twitter/app/users/sheet/UsersBottomSheet;

    invoke-direct {v0}, Lcom/twitter/app/users/sheet/UsersBottomSheet;-><init>()V

    .line 52
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 53
    const-string/jumbo v2, "arg_action_res_id"

    invoke-static {p0}, Lcom/twitter/app/users/sheet/UsersBottomSheet$a;->a(Lcom/twitter/app/users/sheet/UsersBottomSheet$a;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 54
    invoke-static {p0}, Lcom/twitter/app/users/sheet/UsersBottomSheet$a;->b(Lcom/twitter/app/users/sheet/UsersBottomSheet$a;)Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 55
    const-string/jumbo v2, "arg_avatar_rounding_strategy"

    invoke-static {p0}, Lcom/twitter/app/users/sheet/UsersBottomSheet$a;->b(Lcom/twitter/app/users/sheet/UsersBottomSheet$a;)Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 57
    :cond_0
    const-string/jumbo v2, "arg_request_code_open_profile"

    invoke-static {p0}, Lcom/twitter/app/users/sheet/UsersBottomSheet$a;->c(Lcom/twitter/app/users/sheet/UsersBottomSheet$a;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 58
    const-string/jumbo v2, "arg_title"

    invoke-static {p0}, Lcom/twitter/app/users/sheet/UsersBottomSheet$a;->d(Lcom/twitter/app/users/sheet/UsersBottomSheet$a;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/sheet/UsersBottomSheet;->setArguments(Landroid/os/Bundle;)V

    .line 62
    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 137
    const v0, 0x7f130164

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/users/sheet/UsersBottomSheet$3;

    invoke-direct {v1, p0}, Lcom/twitter/app/users/sheet/UsersBottomSheet$3;-><init>(Lcom/twitter/app/users/sheet/UsersBottomSheet;)V

    .line 138
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    const v0, 0x7f13011b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 145
    invoke-virtual {p0}, Lcom/twitter/app/users/sheet/UsersBottomSheet;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "arg_title"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 120
    new-instance v1, Lcom/twitter/app/users/sheet/UsersBottomSheet$b;

    const/4 v0, 0x0

    invoke-direct {v1, p2, v0}, Lcom/twitter/app/users/sheet/UsersBottomSheet$b;-><init>(Landroid/view/View;Lcom/twitter/app/users/sheet/UsersBottomSheet$1;)V

    .line 121
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 122
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;

    .line 123
    invoke-virtual {v0, v1}, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;->setBehavior(Landroid/support/design/widget/CoordinatorLayout$Behavior;)V

    .line 124
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/support/design/widget/BottomSheetBehavior;->setHideable(Z)V

    .line 126
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v2, Lcom/twitter/app/users/sheet/UsersBottomSheet$2;

    invoke-direct {v2, p0, v1}, Lcom/twitter/app/users/sheet/UsersBottomSheet$2;-><init>(Lcom/twitter/app/users/sheet/UsersBottomSheet;Landroid/support/design/widget/BottomSheetBehavior;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 134
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 101
    iget-object v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet;->a:Lcom/twitter/app/users/sheet/a;

    if-nez v0, :cond_1

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    const-string/jumbo v0, "user_id"

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 106
    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    const-string/jumbo v2, "friendship"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 107
    const-string/jumbo v2, "friendship"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 108
    iget-object v3, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet;->a:Lcom/twitter/app/users/sheet/a;

    invoke-virtual {v3, v0, v1, v2}, Lcom/twitter/app/users/sheet/a;->a(JI)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/app/users/sheet/b;)V
    .locals 1

    .prologue
    .line 94
    iput-object p1, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet;->b:Lcom/twitter/app/users/sheet/b;

    .line 95
    iget-object v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet;->a:Lcom/twitter/app/users/sheet/a;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet;->a:Lcom/twitter/app/users/sheet/a;

    invoke-virtual {v0, p1}, Lcom/twitter/app/users/sheet/a;->a(Lcom/twitter/app/users/sheet/b;)V

    .line 98
    :cond_0
    return-void
.end method

.method public a([J)V
    .locals 2

    .prologue
    .line 113
    iput-object p1, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet;->c:[J

    .line 114
    iget-object v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet;->a:Lcom/twitter/app/users/sheet/a;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/users/sheet/UsersBottomSheet;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet;->a:Lcom/twitter/app/users/sheet/a;

    invoke-virtual {p0}, Lcom/twitter/app/users/sheet/UsersBottomSheet;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/twitter/app/users/sheet/a;->a([JLandroid/support/v4/app/LoaderManager;)V

    .line 117
    :cond_0
    return-void
.end method

.method public setupDialog(Landroid/app/Dialog;I)V
    .locals 9

    .prologue
    .line 67
    invoke-super {p0, p1, p2}, Landroid/support/design/widget/BottomSheetDialogFragment;->setupDialog(Landroid/app/Dialog;I)V

    .line 69
    invoke-virtual {p0}, Lcom/twitter/app/users/sheet/UsersBottomSheet;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040432

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 70
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 71
    const v1, 0x7f1304ae

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ListView;

    .line 72
    invoke-direct {p0, v0, v4}, Lcom/twitter/app/users/sheet/UsersBottomSheet;->a(Landroid/view/View;Landroid/view/View;)V

    .line 73
    invoke-direct {p0, v0}, Lcom/twitter/app/users/sheet/UsersBottomSheet;->a(Landroid/view/View;)V

    .line 76
    invoke-virtual {p0}, Lcom/twitter/app/users/sheet/UsersBottomSheet;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "arg_avatar_rounding_strategy"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    .line 78
    new-instance v0, Lcom/twitter/app/users/sheet/a;

    invoke-virtual {p0}, Lcom/twitter/app/users/sheet/UsersBottomSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/twitter/app/users/sheet/UsersBottomSheet$1;

    invoke-direct {v2, p0}, Lcom/twitter/app/users/sheet/UsersBottomSheet$1;-><init>(Lcom/twitter/app/users/sheet/UsersBottomSheet;)V

    new-instance v3, Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {v3}, Lcom/twitter/model/util/FriendshipCache;-><init>()V

    .line 86
    invoke-virtual {p0}, Lcom/twitter/app/users/sheet/UsersBottomSheet;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string/jumbo v7, "arg_action_res_id"

    const v8, 0x7f0200b0

    invoke-virtual {v5, v7, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    invoke-direct/range {v0 .. v6}, Lcom/twitter/app/users/sheet/a;-><init>(Landroid/content/Context;Lcom/twitter/app/users/sheet/a$b;Lcom/twitter/model/util/FriendshipCache;Landroid/widget/ListView;ILcom/twitter/media/ui/image/config/CommonRoundingStrategy;)V

    iput-object v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet;->a:Lcom/twitter/app/users/sheet/a;

    .line 87
    iget-object v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet;->a:Lcom/twitter/app/users/sheet/a;

    iget-object v1, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet;->b:Lcom/twitter/app/users/sheet/b;

    invoke-virtual {v0, v1}, Lcom/twitter/app/users/sheet/a;->a(Lcom/twitter/app/users/sheet/b;)V

    .line 88
    iget-object v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet;->c:[J

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet;->c:[J

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/sheet/UsersBottomSheet;->a([J)V

    .line 91
    :cond_0
    return-void
.end method
