.class Lcom/twitter/app/users/sheet/UsersBottomSheet$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/users/sheet/UsersBottomSheet;->a(Landroid/view/View;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/support/design/widget/BottomSheetBehavior;

.field final synthetic b:Lcom/twitter/app/users/sheet/UsersBottomSheet;


# direct methods
.method constructor <init>(Lcom/twitter/app/users/sheet/UsersBottomSheet;Landroid/support/design/widget/BottomSheetBehavior;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet$2;->b:Lcom/twitter/app/users/sheet/UsersBottomSheet;

    iput-object p2, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet$2;->a:Landroid/support/design/widget/BottomSheetBehavior;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet$2;->b:Lcom/twitter/app/users/sheet/UsersBottomSheet;

    invoke-virtual {v0}, Lcom/twitter/app/users/sheet/UsersBottomSheet;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 130
    int-to-float v0, v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 131
    iget-object v1, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet$2;->a:Landroid/support/design/widget/BottomSheetBehavior;

    invoke-virtual {v1, v0}, Landroid/support/design/widget/BottomSheetBehavior;->setPeekHeight(I)V

    .line 132
    return-void
.end method
