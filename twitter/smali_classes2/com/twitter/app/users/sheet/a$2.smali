.class Lcom/twitter/app/users/sheet/a$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/users/sheet/a;->a([JLandroid/support/v4/app/LoaderManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/twitter/model/core/TwitterUser;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/users/sheet/a;


# direct methods
.method constructor <init>(Lcom/twitter/app/users/sheet/a;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/twitter/app/users/sheet/a$2;->a:Lcom/twitter/app/users/sheet/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/content/Loader;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;>;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a$2;->a:Lcom/twitter/app/users/sheet/a;

    invoke-static {v0}, Lcom/twitter/app/users/sheet/a;->c(Lcom/twitter/app/users/sheet/a;)Lcom/twitter/android/UsersAdapter;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/android/UsersAdapter;->a(Ljava/util/List;)V

    .line 104
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 93
    new-instance v0, Lcom/twitter/library/api/aa;

    iget-object v1, p0, Lcom/twitter/app/users/sheet/a$2;->a:Lcom/twitter/app/users/sheet/a;

    invoke-static {v1}, Lcom/twitter/app/users/sheet/a;->a(Lcom/twitter/app/users/sheet/a;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/users/sheet/a$2;->a:Lcom/twitter/app/users/sheet/a;

    invoke-static {v2}, Lcom/twitter/app/users/sheet/a;->b(Lcom/twitter/app/users/sheet/a;)[J

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/api/aa;-><init>(Landroid/content/Context;[J)V

    return-object v0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 90
    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/users/sheet/a$2;->a(Landroid/support/v4/content/Loader;Ljava/util/List;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a$2;->a:Lcom/twitter/app/users/sheet/a;

    invoke-static {v0}, Lcom/twitter/app/users/sheet/a;->c(Lcom/twitter/app/users/sheet/a;)Lcom/twitter/android/UsersAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->k()Lcjt;

    move-result-object v0

    invoke-static {}, Lcbi;->f()Lcbi;

    move-result-object v1

    invoke-interface {v0, v1}, Lcjt;->a(Lcbi;)Lcbi;

    .line 99
    return-void
.end method
