.class public Lcom/twitter/app/users/sheet/a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/users/sheet/a$b;,
        Lcom/twitter/app/users/sheet/a$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/app/users/sheet/a$b;

.field private final c:Lcom/twitter/library/client/Session;

.field private final d:Lcom/twitter/library/client/p;

.field private final e:Lcom/twitter/model/util/FriendshipCache;

.field private final f:Lcom/twitter/android/UsersAdapter;

.field private g:[J

.field private h:Lcom/twitter/app/users/sheet/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/app/users/sheet/a$b;Lcom/twitter/model/util/FriendshipCache;Landroid/widget/ListView;ILcom/twitter/media/ui/image/config/CommonRoundingStrategy;)V
    .locals 1
    .param p5    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/twitter/app/users/sheet/a;->a:Landroid/content/Context;

    .line 67
    iput-object p2, p0, Lcom/twitter/app/users/sheet/a;->b:Lcom/twitter/app/users/sheet/a$b;

    .line 68
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/sheet/a;->c:Lcom/twitter/library/client/Session;

    .line 69
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/sheet/a;->d:Lcom/twitter/library/client/p;

    .line 70
    iput-object p3, p0, Lcom/twitter/app/users/sheet/a;->e:Lcom/twitter/model/util/FriendshipCache;

    .line 72
    invoke-direct {p0, p3, p5, p6}, Lcom/twitter/app/users/sheet/a;->a(Lcom/twitter/model/util/FriendshipCache;ILcom/twitter/media/ui/image/config/CommonRoundingStrategy;)Lcom/twitter/android/UsersAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/sheet/a;->f:Lcom/twitter/android/UsersAdapter;

    .line 74
    new-instance v0, Lcom/twitter/app/users/sheet/a$1;

    invoke-direct {v0, p0}, Lcom/twitter/app/users/sheet/a$1;-><init>(Lcom/twitter/app/users/sheet/a;)V

    invoke-virtual {p4, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 83
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a;->f:Lcom/twitter/android/UsersAdapter;

    invoke-virtual {p4, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 84
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/users/sheet/a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a;->a:Landroid/content/Context;

    return-object v0
.end method

.method private a(Lcom/twitter/model/util/FriendshipCache;ILcom/twitter/media/ui/image/config/CommonRoundingStrategy;)Lcom/twitter/android/UsersAdapter;
    .locals 8
    .param p2    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 116
    new-instance v0, Lcom/twitter/app/users/sheet/a$a;

    iget-object v1, p0, Lcom/twitter/app/users/sheet/a;->a:Landroid/content/Context;

    new-instance v3, Lcom/twitter/app/users/sheet/a$3;

    invoke-direct {v3, p0}, Lcom/twitter/app/users/sheet/a$3;-><init>(Lcom/twitter/app/users/sheet/a;)V

    move v2, p2

    move-object v4, p1

    move-object v6, p3

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/app/users/sheet/a$a;-><init>(Landroid/content/Context;ILcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/android/UsersAdapter$CheckboxConfig;Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;Lcom/twitter/app/users/sheet/a$1;)V

    return-object v0
.end method

.method private a(JLcgi;)V
    .locals 7

    .prologue
    .line 147
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a;->e:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/model/util/FriendshipCache;->b(J)V

    .line 148
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a;->d:Lcom/twitter/library/client/p;

    new-instance v1, Lbhq;

    iget-object v2, p0, Lcom/twitter/app/users/sheet/a;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/app/users/sheet/a;->c:Lcom/twitter/library/client/Session;

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lbhq;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    new-instance v2, Lcom/twitter/app/users/sheet/a$4;

    invoke-direct {v2, p0, p1, p2}, Lcom/twitter/app/users/sheet/a$4;-><init>(Lcom/twitter/app/users/sheet/a;J)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 160
    return-void
.end method

.method private b(JLcgi;)V
    .locals 7

    .prologue
    .line 163
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a;->e:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/model/util/FriendshipCache;->c(J)V

    .line 164
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a;->d:Lcom/twitter/library/client/p;

    new-instance v1, Lbhs;

    iget-object v2, p0, Lcom/twitter/app/users/sheet/a;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/app/users/sheet/a;->c:Lcom/twitter/library/client/Session;

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lbhs;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    new-instance v2, Lcom/twitter/app/users/sheet/a$5;

    invoke-direct {v2, p0, p1, p2}, Lcom/twitter/app/users/sheet/a$5;-><init>(Lcom/twitter/app/users/sheet/a;J)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 176
    return-void
.end method

.method static synthetic b(Lcom/twitter/app/users/sheet/a;)[J
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a;->g:[J

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/app/users/sheet/a;)Lcom/twitter/android/UsersAdapter;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a;->f:Lcom/twitter/android/UsersAdapter;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/app/users/sheet/a;)Lcom/twitter/model/util/FriendshipCache;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a;->e:Lcom/twitter/model/util/FriendshipCache;

    return-object v0
.end method


# virtual methods
.method a(JI)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 192
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a;->e:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/model/util/FriendshipCache;->a(JI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a;->e:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/model/util/FriendshipCache;->b(JI)V

    .line 194
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a;->f:Lcom/twitter/android/UsersAdapter;

    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->notifyDataSetChanged()V

    .line 196
    :cond_0
    return-void
.end method

.method a(JLjava/lang/String;Lcgi;)V
    .locals 11
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 180
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a;->h:Lcom/twitter/app/users/sheet/b;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a;->h:Lcom/twitter/app/users/sheet/b;

    invoke-interface {v0}, Lcom/twitter/app/users/sheet/b;->x()V

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a;->e:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/model/util/FriendshipCache;->j(J)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, -0x1

    .line 185
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 184
    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 186
    iget-object v10, p0, Lcom/twitter/app/users/sheet/a;->b:Lcom/twitter/app/users/sheet/a$b;

    iget-object v1, p0, Lcom/twitter/app/users/sheet/a;->a:Landroid/content/Context;

    .line 187
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v8, v6

    move-object v9, v6

    invoke-static/range {v1 .. v9}, Lcom/twitter/android/ProfileActivity;->a(Landroid/content/Context;JLjava/lang/String;Lcgi;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;ILcom/twitter/library/api/PromotedEvent;Lcom/twitter/model/timeline/r;)Landroid/content/Intent;

    move-result-object v0

    .line 186
    invoke-interface {v10, v0}, Lcom/twitter/app/users/sheet/a$b;->a(Landroid/content/Intent;)V

    .line 188
    return-void
.end method

.method a(JZLcgi;Ljava/lang/String;)V
    .locals 7
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 135
    if-eqz p3, :cond_1

    .line 136
    invoke-direct {p0, p1, p2, p4}, Lcom/twitter/app/users/sheet/a;->b(JLcgi;)V

    .line 141
    :goto_0
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a;->h:Lcom/twitter/app/users/sheet/b;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a;->h:Lcom/twitter/app/users/sheet/b;

    move v1, p3

    move-wide v2, p1

    move-object v4, p5

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/twitter/app/users/sheet/b;->a(ZJLjava/lang/String;Lcgi;)V

    .line 144
    :cond_0
    return-void

    .line 138
    :cond_1
    invoke-direct {p0, p1, p2, p4}, Lcom/twitter/app/users/sheet/a;->a(JLcgi;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/app/users/sheet/b;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/twitter/app/users/sheet/a;->h:Lcom/twitter/app/users/sheet/b;

    .line 111
    return-void
.end method

.method public a([JLandroid/support/v4/app/LoaderManager;)V
    .locals 3

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a;->g:[J

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/users/sheet/a;->g:[J

    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v0

    if-nez v0, :cond_1

    .line 88
    :cond_0
    iput-object p1, p0, Lcom/twitter/app/users/sheet/a;->g:[J

    .line 89
    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/twitter/app/users/sheet/a$2;

    invoke-direct {v2, p0}, Lcom/twitter/app/users/sheet/a$2;-><init>(Lcom/twitter/app/users/sheet/a;)V

    invoke-virtual {p2, v0, v1, v2}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 107
    :cond_1
    return-void
.end method
