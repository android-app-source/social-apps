.class public Lcom/twitter/app/users/sheet/UsersBottomSheet$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/users/sheet/UsersBottomSheet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;
    }
.end annotation


# instance fields
.field private final a:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field private final b:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

.field private final c:I

.field private final d:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;)V
    .locals 1

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    invoke-static {p1}, Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;->a(Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet$a;->a:I

    .line 157
    invoke-static {p1}, Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;->b(Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;)Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet$a;->b:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    .line 158
    invoke-static {p1}, Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;->c(Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;)I

    move-result v0

    iput v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet$a;->c:I

    .line 159
    invoke-static {p1}, Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;->d(Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet$a;->d:Ljava/lang/String;

    .line 160
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;Lcom/twitter/app/users/sheet/UsersBottomSheet$1;)V
    .locals 0

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/twitter/app/users/sheet/UsersBottomSheet$a;-><init>(Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/users/sheet/UsersBottomSheet$a;)I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet$a;->a:I

    return v0
.end method

.method static synthetic b(Lcom/twitter/app/users/sheet/UsersBottomSheet$a;)Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet$a;->b:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/app/users/sheet/UsersBottomSheet$a;)I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet$a;->c:I

    return v0
.end method

.method static synthetic d(Lcom/twitter/app/users/sheet/UsersBottomSheet$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet$a;->d:Ljava/lang/String;

    return-object v0
.end method
