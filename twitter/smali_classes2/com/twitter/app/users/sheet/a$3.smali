.class Lcom/twitter/app/users/sheet/a$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/ui/user/BaseUserView$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/users/sheet/a;->a(Lcom/twitter/model/util/FriendshipCache;ILcom/twitter/media/ui/image/config/CommonRoundingStrategy;)Lcom/twitter/android/UsersAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/ui/user/BaseUserView$a",
        "<",
        "Lcom/twitter/ui/user/UserView;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/users/sheet/a;


# direct methods
.method constructor <init>(Lcom/twitter/app/users/sheet/a;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/twitter/app/users/sheet/a$3;->a:Lcom/twitter/app/users/sheet/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/ui/user/BaseUserView;JII)V
    .locals 6

    .prologue
    .line 117
    move-object v1, p1

    check-cast v1, Lcom/twitter/ui/user/UserView;

    move-object v0, p0

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/app/users/sheet/a$3;->a(Lcom/twitter/ui/user/UserView;JII)V

    return-void
.end method

.method public a(Lcom/twitter/ui/user/UserView;JII)V
    .locals 8

    .prologue
    .line 120
    const v0, 0x7f130003

    if-ne p4, v0, :cond_0

    .line 121
    iget-object v1, p0, Lcom/twitter/app/users/sheet/a$3;->a:Lcom/twitter/app/users/sheet/a;

    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->j()Z

    move-result v4

    .line 122
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getPromotedContent()Lcgi;

    move-result-object v5

    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/db;

    iget-object v6, v0, Lcom/twitter/android/db;->g:Ljava/lang/String;

    move-wide v2, p2

    .line 121
    invoke-virtual/range {v1 .. v6}, Lcom/twitter/app/users/sheet/a;->a(JZLcgi;Ljava/lang/String;)V

    .line 127
    :goto_0
    return-void

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a$3;->a:Lcom/twitter/app/users/sheet/a;

    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getUserName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getPromotedContent()Lcgi;

    move-result-object v2

    invoke-virtual {v0, p2, p3, v1, v2}, Lcom/twitter/app/users/sheet/a;->a(JLjava/lang/String;Lcgi;)V

    goto :goto_0
.end method
