.class Lcom/twitter/app/users/sheet/a$a;
.super Lcom/twitter/android/UsersAdapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/users/sheet/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;


# direct methods
.method private constructor <init>(Landroid/content/Context;ILcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/android/UsersAdapter$CheckboxConfig;Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;)V
    .locals 0
    .param p2    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserView;",
            ">;",
            "Lcom/twitter/model/util/FriendshipCache;",
            "Lcom/twitter/android/UsersAdapter$CheckboxConfig;",
            "Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 206
    invoke-direct/range {p0 .. p5}, Lcom/twitter/android/UsersAdapter;-><init>(Landroid/content/Context;ILcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/android/UsersAdapter$CheckboxConfig;)V

    .line 207
    iput-object p6, p0, Lcom/twitter/app/users/sheet/a$a;->a:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    .line 208
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;ILcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/android/UsersAdapter$CheckboxConfig;Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;Lcom/twitter/app/users/sheet/a$1;)V
    .locals 0

    .prologue
    .line 198
    invoke-direct/range {p0 .. p6}, Lcom/twitter/app/users/sheet/a$a;-><init>(Landroid/content/Context;ILcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/android/UsersAdapter$CheckboxConfig;Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;I)V
    .locals 2

    .prologue
    .line 212
    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/UsersAdapter;->a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 213
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a$a;->a:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    if-eqz v0, :cond_0

    .line 214
    invoke-virtual {p0, p1}, Lcom/twitter/app/users/sheet/a$a;->a(Landroid/view/View;)Lcom/twitter/ui/user/UserView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/ui/user/UserView;->getImageView()Lcom/twitter/media/ui/image/UserImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/users/sheet/a$a;->a:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setRoundingStrategy(Lcom/twitter/media/ui/image/config/g;)V

    .line 216
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 198
    check-cast p3, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/app/users/sheet/a$a;->a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;I)V

    return-void
.end method
