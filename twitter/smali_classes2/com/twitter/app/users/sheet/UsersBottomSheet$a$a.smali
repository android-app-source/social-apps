.class public final Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/users/sheet/UsersBottomSheet$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/app/users/sheet/UsersBottomSheet$a;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field private b:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

.field private c:I

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 162
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;)I
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;->a:I

    return v0
.end method

.method static synthetic b(Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;)Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;->b:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;)I
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;->c:I

    return v0
.end method

.method static synthetic d(Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;->c:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;
    .locals 0

    .prologue
    .line 170
    iput p1, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;->a:I

    .line 171
    return-object p0
.end method

.method public a(Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;)Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;->b:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    .line 177
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;->d:Ljava/lang/String;

    .line 189
    return-object p0
.end method

.method public b(I)Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;
    .locals 0

    .prologue
    .line 182
    iput p1, p0, Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;->c:I

    .line 183
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;->e()Lcom/twitter/app/users/sheet/UsersBottomSheet$a;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/app/users/sheet/UsersBottomSheet$a;
    .locals 2

    .prologue
    .line 200
    new-instance v0, Lcom/twitter/app/users/sheet/UsersBottomSheet$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/app/users/sheet/UsersBottomSheet$a;-><init>(Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;Lcom/twitter/app/users/sheet/UsersBottomSheet$1;)V

    return-object v0
.end method
