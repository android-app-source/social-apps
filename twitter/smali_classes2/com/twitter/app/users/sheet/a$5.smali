.class Lcom/twitter/app/users/sheet/a$5;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/users/sheet/a;->b(JLcgi;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Lcom/twitter/app/users/sheet/a;


# direct methods
.method constructor <init>(Lcom/twitter/app/users/sheet/a;J)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/twitter/app/users/sheet/a$5;->b:Lcom/twitter/app/users/sheet/a;

    iput-wide p2, p0, Lcom/twitter/app/users/sheet/a$5;->a:J

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 166
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lcom/twitter/app/users/sheet/a$5;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 4

    .prologue
    .line 169
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 170
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a$5;->b:Lcom/twitter/app/users/sheet/a;

    invoke-static {v0}, Lcom/twitter/app/users/sheet/a;->d(Lcom/twitter/app/users/sheet/a;)Lcom/twitter/model/util/FriendshipCache;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/app/users/sheet/a$5;->a:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->b(J)V

    .line 172
    iget-object v0, p0, Lcom/twitter/app/users/sheet/a$5;->b:Lcom/twitter/app/users/sheet/a;

    invoke-static {v0}, Lcom/twitter/app/users/sheet/a;->c(Lcom/twitter/app/users/sheet/a;)Lcom/twitter/android/UsersAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->notifyDataSetChanged()V

    .line 174
    :cond_1
    return-void
.end method
