.class Lcom/twitter/app/users/UsersFragment$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/ui/user/BaseUserView$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/users/UsersFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/ui/user/BaseUserView$a",
        "<",
        "Lcom/twitter/ui/user/UserApprovalView;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/users/UsersFragment;


# direct methods
.method private constructor <init>(Lcom/twitter/app/users/UsersFragment;)V
    .locals 0

    .prologue
    .line 1981
    iput-object p1, p0, Lcom/twitter/app/users/UsersFragment$a;->a:Lcom/twitter/app/users/UsersFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/app/users/UsersFragment;Lcom/twitter/app/users/UsersFragment$1;)V
    .locals 0

    .prologue
    .line 1981
    invoke-direct {p0, p1}, Lcom/twitter/app/users/UsersFragment$a;-><init>(Lcom/twitter/app/users/UsersFragment;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/ui/user/BaseUserView;JII)V
    .locals 6

    .prologue
    .line 1981
    move-object v1, p1

    check-cast v1, Lcom/twitter/ui/user/UserApprovalView;

    move-object v0, p0

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/app/users/UsersFragment$a;->a(Lcom/twitter/ui/user/UserApprovalView;JII)V

    return-void
.end method

.method public a(Lcom/twitter/ui/user/UserApprovalView;JII)V
    .locals 8

    .prologue
    .line 1984
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserApprovalView;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/twitter/android/db;

    .line 1985
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserApprovalView;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1986
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment$a;->a:Lcom/twitter/app/users/UsersFragment;

    invoke-virtual {p1}, Lcom/twitter/ui/user/UserApprovalView;->getPromotedContent()Lcgi;

    move-result-object v4

    move-wide v2, p2

    move v6, p5

    invoke-static/range {v1 .. v6}, Lcom/twitter/app/users/UsersFragment;->a(Lcom/twitter/app/users/UsersFragment;JLcgi;Lcom/twitter/android/db;I)V

    .line 1987
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment$a;->a:Lcom/twitter/app/users/UsersFragment;

    iget-object v0, v0, Lcom/twitter/app/users/UsersFragment;->n:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2009
    :goto_0
    return-void

    .line 1988
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserApprovalView;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1989
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment$a;->a:Lcom/twitter/app/users/UsersFragment;

    iget-object v0, v0, Lcom/twitter/app/users/UsersFragment;->n:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1990
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment$a;->a:Lcom/twitter/app/users/UsersFragment;

    invoke-static {v0}, Lcom/twitter/app/users/UsersFragment;->a(Lcom/twitter/app/users/UsersFragment;)Lcom/twitter/library/client/p;

    move-result-object v0

    new-instance v1, Lbia;

    iget-object v2, p0, Lcom/twitter/app/users/UsersFragment$a;->a:Lcom/twitter/app/users/UsersFragment;

    invoke-virtual {v2}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/users/UsersFragment$a;->a:Lcom/twitter/app/users/UsersFragment;

    invoke-virtual {v3}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v3

    const/4 v6, 0x1

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Lbia;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JI)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 1995
    :goto_1
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment$a;->a:Lcom/twitter/app/users/UsersFragment;

    iget-object v0, v0, Lcom/twitter/app/users/UsersFragment;->n:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1996
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment$a;->a:Lcom/twitter/app/users/UsersFragment;

    invoke-virtual {v1}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "follower_requests::::accept"

    aput-object v3, v1, v2

    .line 1997
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1996
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 1993
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment$a;->a:Lcom/twitter/app/users/UsersFragment;

    iget-object v3, v5, Lcom/twitter/android/db;->g:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/twitter/ui/user/UserApprovalView;->getPromotedContent()Lcgi;

    move-result-object v4

    move-wide v1, p2

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/app/users/UsersFragment;->a(JLjava/lang/String;Lcgi;Lcom/twitter/android/db;I)V

    goto :goto_1

    .line 1998
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserApprovalView;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1999
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment$a;->a:Lcom/twitter/app/users/UsersFragment;

    iget-object v0, v0, Lcom/twitter/app/users/UsersFragment;->n:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2000
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment$a;->a:Lcom/twitter/app/users/UsersFragment;

    invoke-static {v0}, Lcom/twitter/app/users/UsersFragment;->b(Lcom/twitter/app/users/UsersFragment;)Lcom/twitter/library/client/p;

    move-result-object v0

    new-instance v1, Lbia;

    iget-object v2, p0, Lcom/twitter/app/users/UsersFragment$a;->a:Lcom/twitter/app/users/UsersFragment;

    invoke-virtual {v2}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/users/UsersFragment$a;->a:Lcom/twitter/app/users/UsersFragment;

    invoke-virtual {v3}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v3

    const/4 v6, 0x2

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Lbia;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JI)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 2003
    :cond_3
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment$a;->a:Lcom/twitter/app/users/UsersFragment;

    iget-object v0, v0, Lcom/twitter/app/users/UsersFragment;->n:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2004
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment$a;->a:Lcom/twitter/app/users/UsersFragment;

    invoke-virtual {v1}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "follower_requests::::deny"

    aput-object v3, v1, v2

    .line 2005
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 2004
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 2007
    :cond_4
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment$a;->a:Lcom/twitter/app/users/UsersFragment;

    iget-object v0, v0, Lcom/twitter/app/users/UsersFragment;->n:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method
