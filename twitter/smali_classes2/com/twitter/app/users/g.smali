.class public Lcom/twitter/app/users/g;
.super Lcom/twitter/app/common/list/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/users/g$b;,
        Lcom/twitter/app/users/g$a;
    }
.end annotation


# direct methods
.method protected constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/twitter/app/common/list/i;-><init>(Landroid/os/Bundle;)V

    .line 63
    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lcom/twitter/app/users/g;
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lcom/twitter/app/users/g;

    invoke-direct {v0, p0}, Lcom/twitter/app/users/g;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public A()Ljava/lang/String;
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/twitter/app/users/g;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "owner_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public B()I
    .locals 3

    .prologue
    .line 136
    iget-object v0, p0, Lcom/twitter/app/users/g;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "category_position"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public C()Z
    .locals 3

    .prologue
    .line 140
    iget-object v0, p0, Lcom/twitter/app/users/g;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "fetch_always"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public D()Z
    .locals 3

    .prologue
    .line 144
    iget-object v0, p0, Lcom/twitter/app/users/g;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "hide_bio"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public E()Z
    .locals 3

    .prologue
    .line 148
    iget-object v0, p0, Lcom/twitter/app/users/g;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "is_hidden"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public F()Ljava/lang/String;
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/twitter/app/users/g;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "follow_request_sender"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public G()Z
    .locals 3

    .prologue
    .line 157
    iget-object v0, p0, Lcom/twitter/app/users/g;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "disable_toast_error_messages"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public H()Z
    .locals 3

    .prologue
    .line 161
    iget-object v0, p0, Lcom/twitter/app/users/g;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "show_category_name"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public I()Ljava/lang/String;
    .locals 3

    .prologue
    .line 166
    iget-object v0, p0, Lcom/twitter/app/users/g;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "scribe_page"

    const-string/jumbo v2, "unknown"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public J()J
    .locals 4

    .prologue
    .line 170
    iget-object v0, p0, Lcom/twitter/app/users/g;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "target_session_owner_id"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method K()Lcom/twitter/android/UsersAdapter$CheckboxConfig;
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/twitter/app/users/g;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "checkbox_config"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    return-object v0
.end method

.method public a()Lcom/twitter/app/users/g$a;
    .locals 1

    .prologue
    .line 73
    new-instance v0, Lcom/twitter/app/users/g$b;

    invoke-direct {v0, p0}, Lcom/twitter/app/users/g$b;-><init>(Lcom/twitter/app/users/g;)V

    return-object v0
.end method

.method public b()Z
    .locals 3

    .prologue
    .line 77
    iget-object v0, p0, Lcom/twitter/app/users/g;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "follow"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public c()I
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/app/users/g;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "fast_follow"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public d()I
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/twitter/app/users/g;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "followers_count"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public e()I
    .locals 3

    .prologue
    .line 91
    iget-object v0, p0, Lcom/twitter/app/users/g;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "type"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public f()J
    .locals 4

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/app/users/g;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "tag"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public g()[J
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/twitter/app/users/g;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "user_ids"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    return-object v0
.end method

.method public synthetic h()Lcom/twitter/app/common/list/i$a;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/twitter/app/users/g;->a()Lcom/twitter/app/users/g$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lcom/twitter/app/common/base/b$a;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/twitter/app/users/g;->a()Lcom/twitter/app/users/g$a;

    move-result-object v0

    return-object v0
.end method

.method public j()Z
    .locals 3

    .prologue
    .line 108
    iget-object v0, p0, Lcom/twitter/app/users/g;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "hide_contacts_import_cta"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public k()Lcom/twitter/model/util/FriendshipCache;
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/twitter/app/users/g;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "friendship_cache"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/util/FriendshipCache;

    return-object v0
.end method

.method public x()Ljava/lang/String;
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/twitter/app/users/g;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "category"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public y()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    iget-object v0, p0, Lcom/twitter/app/users/g;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "explore_email_users"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public z()I
    .locals 3

    .prologue
    .line 127
    iget-object v0, p0, Lcom/twitter/app/users/g;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "limit"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method
