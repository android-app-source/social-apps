.class public Lcom/twitter/app/users/UsersFragment;
.super Lcom/twitter/android/widget/ScrollingHeaderListFragment;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/twitter/android/av;
.implements Lcom/twitter/android/cd;
.implements Lcom/twitter/android/widget/ad;
.implements Lcom/twitter/app/common/dialog/b$d;
.implements Lcom/twitter/ui/user/BaseUserView$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/users/UsersFragment$d;,
        Lcom/twitter/app/users/UsersFragment$b;,
        Lcom/twitter/app/users/UsersFragment$c;,
        Lcom/twitter/app/users/UsersFragment$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/widget/ScrollingHeaderListFragment",
        "<",
        "Landroid/database/Cursor;",
        "Lcom/twitter/android/UsersAdapter;",
        ">;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/twitter/android/av",
        "<",
        "Lcom/twitter/ui/user/BaseUserView;",
        "Lcgi;",
        ">;",
        "Lcom/twitter/android/cd;",
        "Lcom/twitter/android/widget/ad;",
        "Lcom/twitter/app/common/dialog/b$d;",
        "Lcom/twitter/ui/user/BaseUserView$a",
        "<",
        "Lcom/twitter/ui/user/UserView;",
        ">;"
    }
.end annotation


# instance fields
.field A:Ljava/lang/String;

.field B:Ljava/lang/String;

.field C:[Ljava/lang/String;

.field private D:[J

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private G:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation
.end field

.field private H:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private I:I

.field private J:I

.field private K:Z

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private aa:Z

.field private ab:Z

.field private ac:Z

.field private ad:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field

.field private ae:Ljava/lang/String;

.field private af:Z

.field private ag:Z

.field private ah:I

.field private ai:I

.field private aj:Z

.field private ak:Z

.field private al:Lcom/twitter/app/users/UsersFragment$d;

.field private am:I

.field private an:I

.field private ao:Landroid/widget/TextView;

.field private ap:Landroid/widget/TextView;

.field private aq:Z

.field private ar:Z

.field private as:Z

.field private at:Z

.field protected b:Z

.field protected c:I

.field final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field e:J

.field f:Ljava/lang/String;

.field g:Lcgi;

.field h:Lcom/twitter/ui/user/UserView;

.field i:I

.field j:J

.field k:Z

.field l:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field m:Lcom/twitter/model/util/FriendshipCache;

.field n:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field o:Lcom/twitter/app/users/UsersFragment$c;

.field p:Lcom/twitter/android/widget/ap;

.field q:Lcom/twitter/android/bz;

.field r:Lcom/twitter/android/UsersAdapter$CheckboxConfig;

.field s:I

.field t:I

.field u:I

.field v:Z

.field w:Z

.field x:Z

.field y:Landroid/net/Uri;

.field z:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 136
    invoke-direct {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;-><init>()V

    .line 227
    iput-boolean v2, p0, Lcom/twitter/app/users/UsersFragment;->b:Z

    .line 235
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->d:Ljava/util/Map;

    .line 248
    iput-boolean v2, p0, Lcom/twitter/app/users/UsersFragment;->k:Z

    .line 254
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->l:Ljava/util/HashSet;

    .line 261
    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/app/users/UsersFragment;->s:I

    .line 274
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->a:Ljava/util/Set;

    .line 281
    iput v1, p0, Lcom/twitter/app/users/UsersFragment;->J:I

    .line 282
    iput-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->K:Z

    .line 283
    iput-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->aa:Z

    .line 284
    iput-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->ab:Z

    .line 287
    const-string/jumbo v0, "unknown"

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->ae:Ljava/lang/String;

    .line 294
    iput v1, p0, Lcom/twitter/app/users/UsersFragment;->ah:I

    .line 295
    iput v1, p0, Lcom/twitter/app/users/UsersFragment;->ai:I

    return-void
.end method

.method private a(IZ)Lcom/twitter/android/bj;
    .locals 8
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 2403
    new-instance v0, Lcom/twitter/android/bj;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v4, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    .line 2404
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->C()Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    move-result-object v6

    const/4 v7, 0x0

    move v2, p1

    move-object v3, p0

    move v5, p2

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/bj;-><init>(Landroid/content/Context;ILcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/model/util/FriendshipCache;ZLcom/twitter/android/UsersAdapter$CheckboxConfig;Z)V

    .line 2405
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->Q()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/bj;->e(Z)V

    .line 2406
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->aO()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/bj;->f(Z)V

    .line 2407
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/bj;->a(Z)V

    .line 2408
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->aQ()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/bj;->c(Z)V

    .line 2409
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/app/users/UsersFragment;)Lcom/twitter/library/client/p;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->S:Lcom/twitter/library/client/p;

    return-object v0
.end method

.method private a(JILjava/lang/CharSequence;Ljava/lang/String;Lcgi;Ljava/lang/String;I)V
    .locals 9

    .prologue
    .line 1199
    invoke-virtual {p0, p1, p2, p4, p6}, Lcom/twitter/app/users/UsersFragment;->a(JLjava/lang/CharSequence;Lcgi;)Landroid/content/Intent;

    move-result-object v0

    .line 1200
    if-eqz p6, :cond_0

    .line 1201
    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->d:Lcom/twitter/library/api/PromotedEvent;

    invoke-static {v1, p6}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v1

    invoke-virtual {v1}, Lbsq$a;->a()Lbsq;

    move-result-object v1

    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    :cond_0
    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move-object v5, p6

    move-object v6, p5

    move-object/from16 v7, p7

    move/from16 v8, p8

    .line 1203
    invoke-virtual/range {v1 .. v8}, Lcom/twitter/app/users/UsersFragment;->b(JILcgi;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1204
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/app/users/UsersFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1205
    return-void
.end method

.method private a(JLcgi;Lcom/twitter/android/db;I)V
    .locals 15

    .prologue
    .line 1404
    move-object/from16 v0, p4

    iget-wide v2, v0, Lcom/twitter/android/db;->d:J

    .line 1405
    move-object/from16 v0, p4

    iget-object v12, v0, Lcom/twitter/android/db;->g:Ljava/lang/String;

    .line 1406
    move-object/from16 v0, p4

    iget v13, v0, Lcom/twitter/android/db;->f:I

    .line 1407
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->D()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1408
    iget-object v4, p0, Lcom/twitter/app/users/UsersFragment;->d:Ljava/util/Map;

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v4, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1410
    :cond_0
    new-instance v3, Lbhq;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    .line 1411
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v5

    move-wide/from16 v6, p1

    move-object/from16 v8, p3

    invoke-direct/range {v3 .. v8}, Lbhq;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    .line 1412
    const/16 v2, 0x9

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v2, v4}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 1413
    iget-object v2, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1}, Lcom/twitter/model/util/FriendshipCache;->b(J)V

    .line 1414
    const-string/jumbo v9, "follow"

    move-object/from16 v0, p4

    iget-object v10, v0, Lcom/twitter/android/db;->j:Ljava/lang/String;

    move-object/from16 v0, p4

    iget v11, v0, Lcom/twitter/android/db;->i:I

    move-object v3, p0

    move-wide/from16 v4, p1

    move/from16 v6, p5

    move-object/from16 v7, p3

    move-object v8, v12

    invoke-virtual/range {v3 .. v11}, Lcom/twitter/app/users/UsersFragment;->a(JILcgi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1416
    invoke-static {v13}, Lcom/twitter/model/core/g;->c(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1417
    const-string/jumbo v9, "follow_back"

    move-object/from16 v0, p4

    iget-object v10, v0, Lcom/twitter/android/db;->j:Ljava/lang/String;

    move-object/from16 v0, p4

    iget v11, v0, Lcom/twitter/android/db;->i:I

    move-object v3, p0

    move-wide/from16 v4, p1

    move/from16 v6, p5

    move-object/from16 v7, p3

    move-object v8, v12

    invoke-virtual/range {v3 .. v11}, Lcom/twitter/app/users/UsersFragment;->a(JILcgi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1420
    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/twitter/app/users/UsersFragment;->w:Z

    .line 1421
    return-void
.end method

.method private a(JLcom/twitter/ui/user/UserView;I)V
    .locals 7

    .prologue
    .line 1358
    invoke-virtual {p3}, Lcom/twitter/ui/user/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/db;

    .line 1359
    new-instance v1, Lcom/twitter/android/widget/aj$b;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    .line 1360
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a09e2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/aj$b;->a(Ljava/lang/String;)Lcom/twitter/android/widget/aj$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/aj$b;

    .line 1361
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a09e4

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 1363
    invoke-virtual {p3}, Lcom/twitter/ui/user/UserView;->getBestName()Ljava/lang/CharSequence;

    move-result-object v6

    aput-object v6, v4, v5

    .line 1361
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/aj$b;->a(Ljava/lang/CharSequence;)Lcom/twitter/android/widget/aj$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/aj$b;

    const v2, 0x7f0a0a40

    .line 1365
    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/aj$b;

    const v2, 0x7f0a05e0

    .line 1366
    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/aj$b;

    .line 1367
    invoke-virtual {v1}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/PromptDialogFragment;

    .line 1368
    iput-wide p1, p0, Lcom/twitter/app/users/UsersFragment;->e:J

    .line 1369
    invoke-virtual {p3}, Lcom/twitter/ui/user/UserView;->getPromotedContent()Lcgi;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/app/users/UsersFragment;->g:Lcgi;

    .line 1370
    if-eqz v0, :cond_0

    .line 1371
    iget-object v0, v0, Lcom/twitter/android/db;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->f:Ljava/lang/String;

    .line 1373
    :cond_0
    iput-object p3, p0, Lcom/twitter/app/users/UsersFragment;->h:Lcom/twitter/ui/user/UserView;

    .line 1374
    iput p4, p0, Lcom/twitter/app/users/UsersFragment;->i:I

    .line 1375
    invoke-virtual {v1, p0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 1376
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 1377
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/users/UsersFragment;JLcgi;Lcom/twitter/android/db;I)V
    .locals 1

    .prologue
    .line 136
    invoke-direct/range {p0 .. p5}, Lcom/twitter/app/users/UsersFragment;->a(JLcgi;Lcom/twitter/android/db;I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/users/UsersFragment;Lcom/twitter/library/service/s;)V
    .locals 0

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/twitter/app/users/UsersFragment;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/users/UsersFragment;Z)V
    .locals 0

    .prologue
    .line 136
    invoke-virtual {p0, p1}, Lcom/twitter/app/users/UsersFragment;->a(Z)V

    return-void
.end method

.method private a(Lcom/twitter/library/service/s;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1919
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 1920
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    .line 1921
    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->p(I)Z

    .line 1922
    iget-object v0, p1, Lcom/twitter/library/service/s;->o:Landroid/os/Bundle;

    .line 1923
    const-string/jumbo v1, "num_users"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 1925
    invoke-virtual {p0, v4}, Lcom/twitter/app/users/UsersFragment;->a(Z)V

    .line 1928
    iget-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->aa:Z

    if-nez v0, :cond_0

    .line 1929
    iput-boolean v5, p0, Lcom/twitter/app/users/UsersFragment;->aa:Z

    .line 1930
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->aT()V

    .line 1933
    :cond_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 1934
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    .line 1935
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->i()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const-string/jumbo v3, "follow_friends::reverse_lookup:count"

    aput-object v3, v2, v5

    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    int-to-long v2, v1

    .line 1936
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1933
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1937
    return-void
.end method

.method private a(Lcom/twitter/ui/user/UserView;I)V
    .locals 3

    .prologue
    .line 1264
    const v0, 0x7f130003

    if-ne p2, v0, :cond_0

    .line 1267
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->i()V

    .line 1268
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x4

    .line 1269
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getBestName()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1268
    invoke-static {v0, v1, v2}, Lcom/twitter/android/al;->a(Landroid/support/v4/app/FragmentActivity;ILjava/lang/String;)V

    .line 1273
    :cond_0
    return-void
.end method

.method private aU()Z
    .locals 3

    .prologue
    .line 1497
    const-string/jumbo v0, "spam_filtered_follows_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->D:[J

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->D:[J

    array-length v0, v0

    iget v1, p0, Lcom/twitter/app/users/UsersFragment;->an:I

    iget v2, p0, Lcom/twitter/app/users/UsersFragment;->am:I

    sub-int/2addr v1, v2

    if-lt v0, v1, :cond_1

    :cond_0
    const-string/jumbo v0, "spam_filtered_follows_always_force_enabled"

    .line 1499
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 1497
    :goto_0
    return v0

    .line 1499
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aV()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1630
    const/4 v0, 0x0

    .line 1633
    iget v2, p0, Lcom/twitter/app/users/UsersFragment;->u:I

    and-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_0

    .line 1634
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->E()V

    .line 1635
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->u:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/app/users/UsersFragment;->u:I

    move v0, v1

    .line 1643
    :goto_0
    invoke-static {}, Lcom/twitter/android/ContactsUploadService;->c()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1644
    invoke-virtual {p0, v1}, Lcom/twitter/app/users/UsersFragment;->b(Z)V

    .line 1648
    :goto_1
    return v0

    .line 1638
    :cond_0
    iput-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->aa:Z

    .line 1639
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->G()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2}, Lcom/twitter/app/users/UsersFragment;->q(I)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1646
    goto :goto_1
.end method

.method private aW()Z
    .locals 2

    .prologue
    .line 1978
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->ah:I

    iget v1, p0, Lcom/twitter/app/users/UsersFragment;->ai:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aX()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2306
    iget v1, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    packed-switch v1, :pswitch_data_0

    .line 2398
    :goto_0
    :pswitch_0
    return-void

    .line 2308
    :pswitch_1
    const-string/jumbo v1, "following"

    move-object v2, v1

    move-object v1, v0

    .line 2393
    :goto_1
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const/4 v3, 0x5

    .line 2394
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(I)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-wide v4, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    .line 2395
    invoke-virtual {v0, v4, v5}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a(J)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 2396
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 2397
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 2393
    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    goto :goto_0

    .line 2313
    :pswitch_2
    const-string/jumbo v1, "follower"

    move-object v2, v1

    move-object v1, v0

    .line 2315
    goto :goto_1

    .line 2318
    :pswitch_3
    const-string/jumbo v1, "followers"

    .line 2319
    const-string/jumbo v0, "vit_verified_followers"

    move-object v2, v1

    move-object v1, v0

    .line 2320
    goto :goto_1

    .line 2323
    :pswitch_4
    const-string/jumbo v1, "blocked"

    move-object v2, v1

    move-object v1, v0

    .line 2325
    goto :goto_1

    .line 2328
    :pswitch_5
    const-string/jumbo v1, "blocked_imported"

    move-object v2, v1

    move-object v1, v0

    .line 2330
    goto :goto_1

    .line 2333
    :pswitch_6
    const-string/jumbo v1, "muted"

    move-object v2, v1

    move-object v1, v0

    .line 2335
    goto :goto_1

    .line 2338
    :pswitch_7
    const-string/jumbo v1, "muted_automated"

    move-object v2, v1

    move-object v1, v0

    .line 2340
    goto :goto_1

    .line 2343
    :pswitch_8
    const-string/jumbo v1, "list"

    .line 2344
    const-string/jumbo v0, "users"

    move-object v2, v1

    move-object v1, v0

    .line 2345
    goto :goto_1

    .line 2348
    :pswitch_9
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->ad:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 2349
    const-string/jumbo v1, "explore_email"

    .line 2350
    const-string/jumbo v0, "category"

    move-object v2, v1

    move-object v1, v0

    goto :goto_1

    .line 2352
    :cond_0
    const-string/jumbo v1, "category"

    move-object v2, v1

    move-object v1, v0

    .line 2355
    goto :goto_1

    .line 2358
    :pswitch_a
    const-string/jumbo v1, "categories"

    move-object v2, v1

    move-object v1, v0

    .line 2360
    goto :goto_1

    .line 2364
    :pswitch_b
    const-string/jumbo v1, "matches"

    move-object v2, v1

    move-object v1, v0

    .line 2366
    goto :goto_1

    .line 2369
    :pswitch_c
    const-string/jumbo v1, "similar_to"

    move-object v2, v1

    move-object v1, v0

    .line 2371
    goto :goto_1

    .line 2374
    :pswitch_d
    const-string/jumbo v1, "favorited_by"

    move-object v2, v1

    move-object v1, v0

    .line 2376
    goto :goto_1

    .line 2379
    :pswitch_e
    const-string/jumbo v1, "retweeted_by"

    move-object v2, v1

    move-object v1, v0

    .line 2381
    goto/16 :goto_1

    .line 2384
    :pswitch_f
    const-string/jumbo v1, "friendships"

    move-object v2, v1

    move-object v1, v0

    .line 2386
    goto/16 :goto_1

    .line 2306
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_f
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_b
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method static synthetic b(Lcom/twitter/app/users/UsersFragment;)Lcom/twitter/library/client/p;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->S:Lcom/twitter/library/client/p;

    return-object v0
.end method

.method private b(JLcgi;Lcom/twitter/android/db;I)V
    .locals 11

    .prologue
    .line 1425
    iget-object v0, p4, Lcom/twitter/android/db;->g:Ljava/lang/String;

    .line 1426
    new-instance v1, Lbes;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v3

    const/4 v7, 0x1

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v1 .. v7}, Lbes;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;I)V

    .line 1428
    const/16 v2, 0x14

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v2, v3}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 1429
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v1, p1, p2}, Lcom/twitter/model/util/FriendshipCache;->h(J)V

    .line 1430
    const-string/jumbo v7, "block"

    iget-object v8, p4, Lcom/twitter/android/db;->j:Ljava/lang/String;

    iget v9, p4, Lcom/twitter/android/db;->i:I

    move-object v1, p0

    move-wide v2, p1

    move/from16 v4, p5

    move-object v5, p3

    move-object v6, v0

    invoke-virtual/range {v1 .. v9}, Lcom/twitter/app/users/UsersFragment;->a(JILcgi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1432
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->w:Z

    .line 1433
    return-void
.end method

.method private b(Lcom/twitter/library/service/s;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2517
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 2518
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v1

    .line 2519
    invoke-virtual {p0, v1}, Lcom/twitter/app/users/UsersFragment;->p(I)Z

    .line 2520
    check-cast p1, Lbhz;

    .line 2521
    iget v1, p1, Lbhz;->l:I

    .line 2523
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2524
    iget-wide v2, p1, Lbhz;->b:J

    .line 2525
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_4

    if-lez v1, :cond_4

    .line 2526
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->k(J)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2545
    :cond_0
    :goto_0
    return-void

    .line 2529
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2530
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UsersAdapter;

    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->notifyDataSetChanged()V

    .line 2535
    :cond_2
    :goto_1
    const/16 v0, 0x14

    if-ge v1, v0, :cond_3

    .line 2536
    iput-boolean v6, p0, Lcom/twitter/app/users/UsersFragment;->k:Z

    .line 2541
    :cond_3
    :goto_2
    iget-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->ab:Z

    if-nez v0, :cond_0

    .line 2542
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->ab:Z

    .line 2543
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->aT()V

    goto :goto_0

    .line 2533
    :cond_4
    invoke-virtual {p0, v6}, Lcom/twitter/app/users/UsersFragment;->a(Z)V

    goto :goto_1

    .line 2539
    :cond_5
    const v0, 0x7f0a09ec

    invoke-direct {p0, v0}, Lcom/twitter/app/users/UsersFragment;->j(I)V

    goto :goto_2
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 1623
    invoke-virtual {p0, v3}, Lcom/twitter/app/users/UsersFragment;->d(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1624
    new-instance v0, Lbca;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Lbca;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    const/16 v1, 0x1b

    invoke-virtual {p0, v0, v1, v3}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 1627
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/twitter/app/users/UsersFragment;)J
    .locals 2

    .prologue
    .line 136
    iget-wide v0, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    return-wide v0
.end method

.method private c(JLcgi;Lcom/twitter/android/db;I)V
    .locals 11

    .prologue
    .line 1437
    iget-object v0, p4, Lcom/twitter/android/db;->g:Ljava/lang/String;

    .line 1438
    new-instance v1, Lbes;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v3

    const/4 v7, 0x3

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v1 .. v7}, Lbes;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;I)V

    .line 1440
    const/16 v2, 0x15

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v2, v3}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 1441
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v1, p1, p2}, Lcom/twitter/model/util/FriendshipCache;->i(J)V

    .line 1442
    const-string/jumbo v7, "unblock"

    iget-object v8, p4, Lcom/twitter/android/db;->j:Ljava/lang/String;

    iget v9, p4, Lcom/twitter/android/db;->i:I

    move-object v1, p0

    move-wide v2, p1

    move/from16 v4, p5

    move-object v5, p3

    move-object v6, v0

    invoke-virtual/range {v1 .. v9}, Lcom/twitter/app/users/UsersFragment;->a(JILcgi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1444
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->w:Z

    .line 1445
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2195
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 2196
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object v4, v1, v2

    const/4 v2, 0x2

    aput-object v4, v1, v2

    const/4 v2, 0x3

    aput-object v4, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "impression"

    aput-object v3, v1, v2

    .line 2197
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 2198
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 2199
    new-instance v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 2200
    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    iput-wide v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->a:J

    .line 2201
    iget-object v2, p0, Lcom/twitter/app/users/UsersFragment;->E:Ljava/lang/String;

    iput-object v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->b:Ljava/lang/String;

    .line 2202
    iget v2, p0, Lcom/twitter/app/users/UsersFragment;->I:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    .line 2203
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 2204
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 2205
    return-void
.end method

.method private static c(I)Z
    .locals 1

    .prologue
    .line 463
    const/16 v0, 0xa

    if-eq p0, v0, :cond_0

    const/4 v0, 0x6

    if-eq p0, v0, :cond_0

    const/16 v0, 0x1c

    if-eq p0, v0, :cond_0

    const/16 v0, 0x20

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/twitter/app/users/UsersFragment;)J
    .locals 2

    .prologue
    .line 136
    iget-wide v0, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    return-wide v0
.end method

.method private d(JLcgi;Lcom/twitter/android/db;I)V
    .locals 11

    .prologue
    .line 1449
    iget-object v6, p4, Lcom/twitter/android/db;->g:Ljava/lang/String;

    .line 1450
    new-instance v0, Lbfa;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lbfa;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 1451
    invoke-virtual {v0, p1, p2}, Lbfa;->a(J)Lbeq;

    .line 1452
    const/16 v1, 0x18

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 1453
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/model/util/FriendshipCache;->f(J)V

    .line 1454
    const-string/jumbo v7, "mute"

    iget-object v8, p4, Lcom/twitter/android/db;->j:Ljava/lang/String;

    iget v9, p4, Lcom/twitter/android/db;->i:I

    move-object v1, p0

    move-wide v2, p1

    move/from16 v4, p5

    move-object v5, p3

    invoke-virtual/range {v1 .. v9}, Lcom/twitter/app/users/UsersFragment;->a(JILcgi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1455
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->w:Z

    .line 1456
    return-void
.end method

.method static synthetic e(Lcom/twitter/app/users/UsersFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->E:Ljava/lang/String;

    return-object v0
.end method

.method private e(JLcgi;Lcom/twitter/android/db;I)V
    .locals 11

    .prologue
    .line 1460
    iget-object v6, p4, Lcom/twitter/android/db;->g:Ljava/lang/String;

    .line 1461
    new-instance v0, Lbff;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lbff;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 1462
    invoke-virtual {v0, p1, p2}, Lbff;->a(J)Lbeq;

    .line 1463
    const/16 v1, 0x19

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 1464
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/model/util/FriendshipCache;->g(J)V

    .line 1465
    const-string/jumbo v7, "unmute"

    iget-object v8, p4, Lcom/twitter/android/db;->j:Ljava/lang/String;

    iget v9, p4, Lcom/twitter/android/db;->i:I

    move-object v1, p0

    move-wide v2, p1

    move/from16 v4, p5

    move-object v5, p3

    invoke-virtual/range {v1 .. v9}, Lcom/twitter/app/users/UsersFragment;->a(JILcgi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1467
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->w:Z

    .line 1468
    return-void
.end method

.method static synthetic f(Lcom/twitter/app/users/UsersFragment;)Z
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->aa:Z

    return v0
.end method

.method private j(I)V
    .locals 2

    .prologue
    .line 2876
    iget-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->as:Z

    if-nez v0, :cond_0

    .line 2877
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2879
    :cond_0
    return-void
.end method

.method private k()Z
    .locals 2

    .prologue
    .line 322
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->c(Lcom/twitter/library/client/Session;)Z

    move-result v0

    return v0
.end method

.method private q()Z
    .locals 1

    .prologue
    .line 551
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->am:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public A()Lcom/twitter/model/util/FriendshipCache;
    .locals 1

    .prologue
    .line 1593
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    return-object v0
.end method

.method protected B()V
    .locals 1

    .prologue
    .line 1618
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->K:Z

    .line 1619
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->aT()V

    .line 1620
    return-void
.end method

.method public C()Lcom/twitter/android/UsersAdapter$CheckboxConfig;
    .locals 1

    .prologue
    .line 1831
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->r:Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    return-object v0
.end method

.method D()Z
    .locals 2

    .prologue
    .line 1862
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    const/16 v1, 0x14

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public E()V
    .locals 1

    .prologue
    .line 1867
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->aa:Z

    .line 1868
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->G()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->q(I)V

    .line 1869
    return-void
.end method

.method protected F()V
    .locals 1

    .prologue
    .line 1904
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->Y()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1915
    :cond_0
    :goto_0
    return-void

    .line 1907
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->o:Lcom/twitter/app/users/UsersFragment$c;

    if-eqz v0, :cond_0

    .line 1908
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->ak()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1913
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->o:Lcom/twitter/app/users/UsersFragment$c;

    invoke-interface {v0}, Lcom/twitter/app/users/UsersFragment$c;->j()V

    goto :goto_0
.end method

.method public G()I
    .locals 1

    .prologue
    .line 1958
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->ai:I

    return v0
.end method

.method public synthetic H()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->s()Lcom/twitter/app/users/g;

    move-result-object v0

    return-object v0
.end method

.method public H_()V
    .locals 1

    .prologue
    .line 1544
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->m(I)Z

    .line 1545
    return-void
.end method

.method public synthetic I()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->s()Lcom/twitter/app/users/g;

    move-result-object v0

    return-object v0
.end method

.method public M()Z
    .locals 1

    .prologue
    .line 1966
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->ai:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public N()I
    .locals 1

    .prologue
    .line 1970
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->ah:I

    return v0
.end method

.method public O()Z
    .locals 1

    .prologue
    .line 1974
    iget-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->K:Z

    return v0
.end method

.method P()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2133
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    sparse-switch v0, :sswitch_data_0

    .line 2192
    :goto_0
    return-void

    .line 2135
    :sswitch_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 2136
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "following::::impression"

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 2140
    :sswitch_1
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 2141
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "followers::::impression"

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 2145
    :sswitch_2
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    .line 2146
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->i()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    const-string/jumbo v2, "address_book"

    aput-object v2, v1, v5

    const-string/jumbo v2, "::impression"

    aput-object v2, v1, v6

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 2145
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 2150
    :sswitch_3
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v6, [Ljava/lang/String;

    .line 2151
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->i()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    const-string/jumbo v2, "follow_friends:::impression"

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 2150
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 2155
    :sswitch_4
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "similar_to::::impression"

    aput-object v2, v1, v4

    .line 2156
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 2155
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 2160
    :sswitch_5
    const-string/jumbo v0, "category"

    invoke-direct {p0, v0}, Lcom/twitter/app/users/UsersFragment;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2164
    :sswitch_6
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "follower_requests::::impression"

    aput-object v2, v1, v4

    .line 2165
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 2164
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 2169
    :sswitch_7
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 2170
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "blocked::::impression"

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 2174
    :sswitch_8
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "blocked_imported::::impression"

    aput-object v2, v1, v4

    .line 2175
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 2174
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 2179
    :sswitch_9
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 2180
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "muted::::impression"

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 2184
    :sswitch_a
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 2185
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "muted_automated::::impression"

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 2184
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 2133
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_7
        0x6 -> :sswitch_5
        0x7 -> :sswitch_3
        0xa -> :sswitch_4
        0x12 -> :sswitch_6
        0x1a -> :sswitch_9
        0x1c -> :sswitch_2
        0x25 -> :sswitch_8
        0x26 -> :sswitch_a
    .end sparse-switch
.end method

.method protected Q()Z
    .locals 2

    .prologue
    .line 2422
    iget-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->ak:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public V_()V
    .locals 1

    .prologue
    .line 1602
    invoke-super {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->V_()V

    .line 1603
    const-string/jumbo v0, "get_newer"

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->d(Ljava/lang/String;)V

    .line 1604
    return-void
.end method

.method protected a(JLjava/lang/CharSequence;Lcgi;)Landroid/content/Intent;
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x0

    .line 1134
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "user_id"

    .line 1135
    invoke-virtual {v0, v2, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "type"

    iget v3, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    .line 1136
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    .line 1137
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    .line 1138
    if-eqz v0, :cond_0

    .line 1139
    const-string/jumbo v3, "association"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1142
    :cond_0
    invoke-static {p3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1144
    invoke-interface {p3, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v3, 0x40

    if-ne v0, v3, :cond_4

    .line 1145
    const/4 v0, 0x1

    .line 1149
    :goto_0
    const-string/jumbo v3, "screen_name"

    .line 1150
    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    move-result v4

    invoke-interface {p3, v0, v4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1149
    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 1153
    :cond_1
    const/16 v0, 0x12

    iget v3, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    if-ne v0, v3, :cond_6

    .line 1154
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->n:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1155
    if-eqz v0, :cond_5

    .line 1156
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1190
    :cond_2
    :goto_1
    if-eqz p4, :cond_3

    .line 1191
    const-string/jumbo v0, "pc"

    invoke-static {p4}, Lcgi;->a(Lcgi;)[B

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1193
    :cond_3
    return-object v2

    :cond_4
    move v0, v1

    .line 1147
    goto :goto_0

    .line 1158
    :pswitch_0
    const-string/jumbo v0, "friendship"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 1162
    :pswitch_1
    const-string/jumbo v0, "friendship"

    const/4 v1, 0x3

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 1167
    :pswitch_2
    const-string/jumbo v0, "friendship"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 1175
    :cond_5
    const-string/jumbo v0, "friendship"

    const/16 v1, 0x20

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 1177
    :cond_6
    invoke-direct {p0}, Lcom/twitter/app/users/UsersFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1180
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/model/util/FriendshipCache;->j(J)Ljava/lang/Integer;

    move-result-object v0

    .line 1181
    if-eqz v0, :cond_7

    .line 1182
    const-string/jumbo v1, "friendship"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_1

    .line 1183
    :cond_7
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    if-eq v5, v0, :cond_8

    const/16 v0, 0x25

    iget v1, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    if-ne v0, v1, :cond_9

    .line 1184
    :cond_8
    const-string/jumbo v0, "friendship"

    const/4 v1, 0x4

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 1185
    :cond_9
    const/16 v0, 0x1a

    iget v1, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    if-eq v0, v1, :cond_a

    const/16 v0, 0x26

    iget v1, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    if-ne v0, v1, :cond_2

    .line 1186
    :cond_a
    const-string/jumbo v0, "friendship"

    const/16 v1, 0x2000

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 1156
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 471
    invoke-super {p0, p1, p2}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v4

    .line 472
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    .line 473
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->s()Lcom/twitter/app/users/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/app/users/g;->r()I

    move-result v1

    .line 474
    iget-object v3, v0, Lcom/twitter/app/common/list/l;->b:Landroid/view/View;

    if-eqz v3, :cond_0

    .line 475
    iget-object v0, v0, Lcom/twitter/app/common/list/l;->b:Landroid/view/View;

    const v3, 0x7f13002f

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 476
    if-eqz v0, :cond_0

    .line 479
    sparse-switch v1, :sswitch_data_0

    move v1, v2

    move v3, v2

    .line 497
    :goto_0
    if-lez v3, :cond_0

    if-lez v1, :cond_0

    .line 498
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    .line 499
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    const v7, 0x7f1100c9

    invoke-static {v6, v1, v7}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;II)Lcom/twitter/ui/view/a;

    move-result-object v1

    aput-object v1, v5, v2

    .line 501
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->T:Landroid/content/Context;

    .line 502
    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "{{}}"

    invoke-static {v5, v1, v2}, Lcom/twitter/library/util/af;->a([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 501
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 503
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    .line 504
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 508
    :cond_0
    return-object v4

    .line 481
    :sswitch_0
    const v3, 0x7f0a036c

    .line 482
    const v1, 0x7f0a0bc9

    .line 483
    goto :goto_0

    .line 486
    :sswitch_1
    const v3, 0x7f0a0371

    .line 487
    const v1, 0x7f0a0bc6

    .line 488
    goto :goto_0

    .line 479
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0a036c -> :sswitch_0
        0x7f0a0371 -> :sswitch_1
    .end sparse-switch
.end method

.method a(JILcgi;Ljava/lang/String;Ljava/lang/String;I)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 5

    .prologue
    .line 2120
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 2121
    invoke-static {v0, p1, p2, p4, p5}, Lcom/twitter/library/scribe/c;->b(Lcom/twitter/analytics/feature/model/ClientEventLog;JLcgi;Ljava/lang/String;)V

    .line 2122
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p6, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 2123
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 2122
    return-object v0
.end method

.method a(JILcgi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 13

    .prologue
    .line 2097
    iget v2, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    .line 2098
    invoke-virtual {p0, v2}, Lcom/twitter/app/users/UsersFragment;->i(I)Ljava/lang/String;

    move-result-object v2

    .line 2097
    move-object/from16 v0, p7

    invoke-static {v0, v2}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 2099
    if-eqz v9, :cond_1

    move-object v3, p0

    move-wide v4, p1

    move/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v10, p6

    move/from16 v11, p8

    .line 2100
    invoke-virtual/range {v3 .. v11}, Lcom/twitter/app/users/UsersFragment;->b(JILcgi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v2

    .line 2102
    iget v3, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    const/16 v4, 0xa

    if-ne v3, v4, :cond_0

    .line 2103
    iget-wide v4, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->l(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 2105
    :cond_0
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 2107
    :cond_1
    return-void
.end method

.method a(JLjava/lang/String;Lcgi;Lcom/twitter/android/db;I)V
    .locals 13

    .prologue
    .line 1381
    const/4 v2, 0x0

    .line 1382
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->D()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1383
    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/users/UsersFragment;->b(J)Ljava/lang/Long;

    .line 1384
    const/4 v2, 0x1

    .line 1390
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    .line 1391
    new-instance v3, Lbhs;

    .line 1392
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v5

    move-wide v6, p1

    move-object/from16 v8, p4

    invoke-direct/range {v3 .. v8}, Lbhs;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    const/16 v2, 0xb

    const/4 v4, 0x0

    .line 1391
    invoke-virtual {p0, v3, v2, v4}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 1395
    :cond_1
    iget-object v2, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v2, p1, p2}, Lcom/twitter/model/util/FriendshipCache;->c(J)V

    .line 1396
    if-eqz p5, :cond_3

    move-object/from16 v0, p5

    iget-object v10, v0, Lcom/twitter/android/db;->j:Ljava/lang/String;

    .line 1397
    :goto_1
    if-eqz p5, :cond_4

    move-object/from16 v0, p5

    iget v11, v0, Lcom/twitter/android/db;->i:I

    .line 1398
    :goto_2
    const-string/jumbo v9, "unfollow"

    move-object v3, p0

    move-wide v4, p1

    move/from16 v6, p6

    move-object/from16 v7, p4

    move-object/from16 v8, p3

    invoke-virtual/range {v3 .. v11}, Lcom/twitter/app/users/UsersFragment;->a(JILcgi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1399
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/twitter/app/users/UsersFragment;->w:Z

    .line 1400
    return-void

    .line 1385
    :cond_2
    iget-object v3, p0, Lcom/twitter/app/users/UsersFragment;->l:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1388
    const/4 v2, 0x1

    goto :goto_0

    .line 1396
    :cond_3
    const/4 v10, 0x0

    goto :goto_1

    .line 1397
    :cond_4
    const/4 v11, -0x1

    goto :goto_2
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 11

    .prologue
    .line 1550
    packed-switch p2, :pswitch_data_0

    .line 1590
    :cond_0
    :goto_0
    return-void

    .line 1552
    :pswitch_0
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    .line 1553
    new-instance v1, Lbda;

    .line 1554
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 1555
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    iget-wide v6, p0, Lcom/twitter/app/users/UsersFragment;->e:J

    iget-wide v8, p0, Lcom/twitter/app/users/UsersFragment;->j:J

    const/4 v10, 0x4

    invoke-direct/range {v1 .. v10}, Lbda;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJJI)V

    .line 1560
    const/16 v0, 0x8

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 1562
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "me:lists:list:people:remove"

    aput-object v3, v1, v2

    .line 1563
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1562
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 1568
    :pswitch_1
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    .line 1569
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 1574
    :pswitch_2
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    .line 1575
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->h:Lcom/twitter/ui/user/UserView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->h:Lcom/twitter/ui/user/UserView;

    .line 1576
    invoke-virtual {v0}, Lcom/twitter/ui/user/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/db;

    move-object v6, v0

    .line 1577
    :goto_1
    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->e:J

    iget-object v4, p0, Lcom/twitter/app/users/UsersFragment;->f:Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/app/users/UsersFragment;->g:Lcgi;

    iget v7, p0, Lcom/twitter/app/users/UsersFragment;->i:I

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, Lcom/twitter/app/users/UsersFragment;->a(JLjava/lang/String;Lcgi;Lcom/twitter/android/db;I)V

    .line 1579
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->h:Lcom/twitter/ui/user/UserView;

    if-eqz v0, :cond_0

    .line 1580
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->h:Lcom/twitter/ui/user/UserView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserView;->setIsFollowing(Z)V

    .line 1581
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->h:Lcom/twitter/ui/user/UserView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/user/UserView;->b(Z)V

    goto :goto_0

    .line 1576
    :cond_1
    const/4 v6, 0x0

    goto :goto_1

    .line 1550
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1077
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1097
    :cond_0
    :goto_0
    return-void

    .line 1079
    :pswitch_0
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->q:Lcom/twitter/android/bz;

    if-eqz v0, :cond_0

    .line 1080
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->q:Lcom/twitter/android/bz;

    iget v1, p0, Lcom/twitter/app/users/UsersFragment;->s:I

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/bz;->a(II)V

    goto :goto_0

    .line 1085
    :pswitch_1
    if-eqz p2, :cond_0

    .line 1086
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 1087
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 1088
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->i()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "follow_friends:not_followed::followable"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 1089
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1086
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 1077
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1946
    return-void
.end method

.method public a(Landroid/view/View;II)V
    .locals 0

    .prologue
    .line 1952
    return-void
.end method

.method protected a(Landroid/view/View;JI)V
    .locals 10

    .prologue
    .line 1243
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/db;

    .line 1244
    iget-object v1, v0, Lcom/twitter/android/db;->c:Lcom/twitter/ui/user/BaseUserView;

    invoke-virtual {v1}, Lcom/twitter/ui/user/BaseUserView;->getUserName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, Lcom/twitter/android/db;->g:Ljava/lang/String;

    iget-object v1, v0, Lcom/twitter/android/db;->c:Lcom/twitter/ui/user/BaseUserView;

    .line 1245
    invoke-virtual {v1}, Lcom/twitter/ui/user/BaseUserView;->getPromotedContent()Lcgi;

    move-result-object v7

    iget-object v8, v0, Lcom/twitter/android/db;->j:Ljava/lang/String;

    iget v9, v0, Lcom/twitter/android/db;->i:I

    move-object v1, p0

    move-wide v2, p2

    move v4, p4

    .line 1244
    invoke-direct/range {v1 .. v9}, Lcom/twitter/app/users/UsersFragment;->a(JILjava/lang/CharSequence;Ljava/lang/String;Lcgi;Ljava/lang/String;I)V

    .line 1246
    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 136
    check-cast p1, Lcom/twitter/ui/user/BaseUserView;

    check-cast p2, Lcgi;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/app/users/UsersFragment;->a(Lcom/twitter/ui/user/BaseUserView;Lcgi;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1209
    invoke-virtual {p1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v2

    .line 1210
    invoke-virtual {p1}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v4

    .line 1211
    sub-int v3, p3, v2

    .line 1212
    iget-object v5, p0, Lcom/twitter/app/users/UsersFragment;->p:Lcom/twitter/android/widget/ap;

    invoke-virtual {v5, v3}, Lcom/twitter/android/widget/ap;->b(I)I

    move-result v3

    .line 1214
    iget v5, p0, Lcom/twitter/app/users/UsersFragment;->s:I

    if-eq v3, v5, :cond_0

    if-lez v2, :cond_2

    if-ge p3, v2, :cond_2

    :cond_0
    move v3, v0

    .line 1216
    :goto_0
    if-lez v4, :cond_3

    .line 1217
    invoke-virtual {p1}, Landroid/widget/ListView;->getCount()I

    move-result v2

    sub-int/2addr v2, v4

    add-int/lit8 v2, v2, -0x1

    if-le p3, v2, :cond_3

    move v2, v0

    .line 1219
    :goto_1
    if-eqz v3, :cond_4

    .line 1220
    invoke-virtual/range {p0 .. p5}, Lcom/twitter/app/users/UsersFragment;->b(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 1239
    :cond_1
    :goto_2
    return-void

    :cond_2
    move v3, v1

    .line 1214
    goto :goto_0

    :cond_3
    move v2, v1

    .line 1217
    goto :goto_1

    .line 1224
    :cond_4
    if-eqz v2, :cond_5

    .line 1225
    invoke-virtual/range {p0 .. p5}, Lcom/twitter/app/users/UsersFragment;->c(Landroid/widget/ListView;Landroid/view/View;IJ)V

    goto :goto_2

    .line 1229
    :cond_5
    iget-object v2, p0, Lcom/twitter/app/users/UsersFragment;->r:Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    if-nez v2, :cond_6

    .line 1230
    invoke-virtual {p0, p2, p4, p5, p3}, Lcom/twitter/app/users/UsersFragment;->a(Landroid/view/View;JI)V

    goto :goto_2

    .line 1232
    :cond_6
    check-cast p2, Lcom/twitter/ui/user/UserView;

    .line 1233
    iget-object v2, p2, Lcom/twitter/ui/user/UserView;->s:Landroid/widget/CheckBox;

    .line 1234
    invoke-virtual {v2}, Landroid/widget/CheckBox;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1235
    iget-object v3, p2, Lcom/twitter/ui/user/UserView;->s:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-nez v2, :cond_7

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1236
    invoke-virtual {p0, p2, p4, p5, p3}, Lcom/twitter/app/users/UsersFragment;->d(Lcom/twitter/ui/user/UserView;JI)V

    goto :goto_2

    :cond_7
    move v0, v1

    .line 1235
    goto :goto_3
.end method

.method protected a(Lcbi;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1101
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->a(Lcbi;)V

    .line 1102
    iget-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->af:Z

    if-nez v0, :cond_3

    .line 1106
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    const/16 v1, 0x1c

    if-eq v0, v1, :cond_1

    .line 1107
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UsersAdapter;

    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    const/16 v1, 0x20

    if-ne v0, v1, :cond_2

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->v:Z

    if-nez v0, :cond_2

    .line 1111
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->m(I)Z

    .line 1113
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->af:Z

    .line 1115
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->F()V

    .line 1116
    return-void
.end method

.method a(Lcom/twitter/app/users/UsersFragment$c;)V
    .locals 0

    .prologue
    .line 1872
    iput-object p1, p0, Lcom/twitter/app/users/UsersFragment;->o:Lcom/twitter/app/users/UsersFragment$c;

    .line 1873
    return-void
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 11

    .prologue
    .line 2557
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 2558
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->ay()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2817
    :cond_0
    :goto_0
    return-void

    .line 2561
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 2562
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->az()Lcjr;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/UsersAdapter;

    .line 2563
    packed-switch p2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 2700
    :pswitch_1
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/twitter/app/users/UsersFragment;->p(I)Z

    .line 2701
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 2702
    const/16 v0, 0x12

    iget v1, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    if-ne v0, v1, :cond_0

    .line 2703
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->a(Z)V

    goto :goto_0

    .line 2572
    :pswitch_2
    const/16 v1, 0xd

    if-eq p2, v1, :cond_2

    const/4 v1, 0x3

    if-ne p2, v1, :cond_3

    .line 2576
    :cond_2
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->ao:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/twitter/app/users/UsersFragment;->q()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2577
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->ao:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2578
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->aq:Z

    .line 2581
    :cond_3
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v1

    .line 2582
    invoke-virtual {p0, v1}, Lcom/twitter/app/users/UsersFragment;->p(I)Z

    .line 2583
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2584
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->a(Z)V

    .line 2586
    :cond_4
    const/16 v0, 0x191

    if-ne v1, v0, :cond_5

    .line 2587
    const v0, 0x7f0a06fe

    invoke-direct {p0, v0}, Lcom/twitter/app/users/UsersFragment;->j(I)V

    goto :goto_0

    .line 2588
    :cond_5
    const/16 v0, 0xc8

    if-eq v1, v0, :cond_6

    .line 2589
    const v0, 0x7f0a09ec

    invoke-direct {p0, v0}, Lcom/twitter/app/users/UsersFragment;->j(I)V

    goto :goto_0

    .line 2590
    :cond_6
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    if-eqz v0, :cond_0

    .line 2591
    check-cast p1, Lbhn;

    .line 2592
    invoke-virtual {p1}, Lbhn;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 2593
    iget-object v2, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v4

    iget v0, v0, Lcom/twitter/model/core/TwitterUser;->U:I

    invoke-virtual {v2, v4, v5, v0}, Lcom/twitter/model/util/FriendshipCache;->b(JI)V

    goto :goto_1

    .line 2599
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/twitter/app/users/UsersFragment;->b(Lcom/twitter/library/service/s;)V

    goto/16 :goto_0

    .line 2603
    :pswitch_4
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    .line 2604
    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->p(I)Z

    .line 2605
    const/16 v1, 0xc8

    if-ne v0, v1, :cond_7

    .line 2606
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/twitter/app/users/UsersFragment;->a(Z)V

    .line 2608
    :cond_7
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 2609
    const v0, 0x7f0a09ec

    invoke-direct {p0, v0}, Lcom/twitter/app/users/UsersFragment;->j(I)V

    goto/16 :goto_0

    .line 2614
    :pswitch_5
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    .line 2615
    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->p(I)Z

    .line 2616
    const/16 v1, 0xc8

    if-ne v0, v1, :cond_8

    .line 2617
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->a(Z)V

    goto/16 :goto_0

    .line 2619
    :cond_8
    const v0, 0x7f0a09f7

    invoke-direct {p0, v0}, Lcom/twitter/app/users/UsersFragment;->j(I)V

    goto/16 :goto_0

    .line 2624
    :pswitch_6
    invoke-direct {p0, p1}, Lcom/twitter/app/users/UsersFragment;->a(Lcom/twitter/library/service/s;)V

    goto/16 :goto_0

    .line 2628
    :pswitch_7
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/service/s;)Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 2629
    if-eqz v2, :cond_0

    .line 2632
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/twitter/app/users/UsersFragment;->p(I)Z

    .line 2633
    check-cast p1, Lbhq;

    invoke-virtual {p1}, Lbhq;->t()J

    move-result-wide v8

    .line 2634
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_9

    .line 2635
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, v8, v9}, Lcom/twitter/model/util/FriendshipCache;->c(J)V

    .line 2636
    invoke-virtual {v1}, Lcom/twitter/android/UsersAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 2637
    :cond_9
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2638
    invoke-virtual {p0, v8, v9}, Lcom/twitter/app/users/UsersFragment;->b(J)Ljava/lang/Long;

    move-result-object v0

    .line 2639
    if-eqz v0, :cond_0

    .line 2640
    iget-boolean v3, p0, Lcom/twitter/app/users/UsersFragment;->b:Z

    if-eqz v3, :cond_a

    .line 2641
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v8, v9, v4, v5}, Lcom/twitter/android/UsersAdapter;->a(JJ)V

    .line 2643
    :cond_a
    new-instance v0, Lbig;

    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->T:Landroid/content/Context;

    iget-wide v3, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    iget v5, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    iget-wide v6, p0, Lcom/twitter/app/users/UsersFragment;->j:J

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, Lbig;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JIJJLjava/lang/Integer;)V

    .line 2646
    const/16 v1, 0x12

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto/16 :goto_0

    .line 2653
    :pswitch_8
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/service/s;)Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 2654
    if-eqz v2, :cond_0

    .line 2657
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/twitter/app/users/UsersFragment;->p(I)Z

    .line 2658
    check-cast p1, Lbhs;

    invoke-virtual {p1}, Lbhs;->g()J

    move-result-wide v2

    .line 2659
    iget-object v4, p0, Lcom/twitter/app/users/UsersFragment;->d:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2660
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2661
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->b(J)V

    .line 2662
    invoke-virtual {v1}, Lcom/twitter/android/UsersAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 2667
    :pswitch_9
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    .line 2668
    check-cast p1, Lbij;

    .line 2669
    invoke-virtual {p1}, Lbij;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/service/v;

    .line 2670
    iget-wide v4, v2, Lcom/twitter/library/service/v;->c:J

    invoke-static {v3, v4, v5}, Lcom/twitter/library/platform/notifications/PushRegistration;->b(Landroid/content/Context;J)Z

    move-result v4

    .line 2672
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v5

    .line 2673
    invoke-virtual {p0, v5}, Lcom/twitter/app/users/UsersFragment;->p(I)Z

    .line 2674
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_b

    if-nez v4, :cond_0

    .line 2677
    :cond_b
    const/16 v0, 0x3e9

    if-eq v5, v0, :cond_c

    if-nez v4, :cond_e

    .line 2678
    :cond_c
    invoke-static {v3}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;)Lcom/twitter/android/client/l;

    move-result-object v1

    iget-wide v2, v2, Lcom/twitter/library/service/v;->c:J

    if-nez v4, :cond_d

    const/4 v0, 0x1

    :goto_2
    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/twitter/android/client/l;->a(JZZ)V

    goto/16 :goto_0

    :cond_d
    const/4 v0, 0x0

    goto :goto_2

    .line 2681
    :cond_e
    iget-object v0, p1, Lbij;->b:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v2

    .line 2682
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->k(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2683
    iget-object v0, p1, Lbij;->b:Lcom/twitter/model/core/TwitterUser;

    iget v0, v0, Lcom/twitter/model/core/TwitterUser;->U:I

    invoke-static {v0}, Lcom/twitter/model/core/g;->i(I)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2684
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->d(J)V

    .line 2688
    :goto_3
    invoke-virtual {v1}, Lcom/twitter/android/UsersAdapter;->notifyDataSetChanged()V

    .line 2689
    const v0, 0x7f0a0260

    invoke-direct {p0, v0}, Lcom/twitter/app/users/UsersFragment;->j(I)V

    goto/16 :goto_0

    .line 2686
    :cond_f
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->e(J)V

    goto :goto_3

    .line 2695
    :pswitch_a
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->a(Z)V

    goto/16 :goto_0

    .line 2706
    :cond_10
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2707
    const v0, 0x7f0a041f

    invoke-direct {p0, v0}, Lcom/twitter/app/users/UsersFragment;->j(I)V

    goto/16 :goto_0

    .line 2714
    :pswitch_b
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    .line 2715
    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->p(I)Z

    .line 2717
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->v:Z

    .line 2718
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_11

    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->ad:Ljava/util/ArrayList;

    if-eqz v0, :cond_12

    .line 2719
    :cond_11
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->a(Z)V

    .line 2721
    :cond_12
    iget-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->at:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2722
    check-cast p1, Lcom/twitter/library/api/f;

    .line 2723
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/library/api/f;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2728
    :pswitch_c
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    .line 2729
    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->p(I)Z

    .line 2730
    check-cast p1, Lbig;

    .line 2732
    invoke-virtual {p1}, Lbig;->g()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 2733
    if-eqz v0, :cond_15

    invoke-virtual {p1}, Lbig;->h()Z

    move-result v0

    if-eqz v0, :cond_15

    const/4 v0, 0x1

    .line 2734
    :goto_4
    invoke-virtual {p1}, Lbig;->M()Lcom/twitter/library/service/v;

    move-result-object v2

    .line 2735
    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/library/service/v;->a(Lcom/twitter/library/client/Session;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2736
    if-eqz v0, :cond_13

    .line 2737
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/twitter/app/users/UsersFragment;->a(Z)V

    .line 2740
    :cond_13
    invoke-virtual {p1}, Lbig;->s()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/UsersAdapter;->a(J)Ljava/lang/Long;

    move-result-object v2

    .line 2741
    if-eqz v2, :cond_0

    if-eqz v0, :cond_14

    .line 2742
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/twitter/android/da;->a(Landroid/widget/ListView;J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2743
    :cond_14
    invoke-virtual {p1}, Lbig;->s()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/UsersAdapter;->b(J)Ljava/lang/Long;

    goto/16 :goto_0

    .line 2733
    :cond_15
    const/4 v0, 0x0

    goto :goto_4

    .line 2749
    :pswitch_d
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/service/s;)Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 2750
    if-eqz v2, :cond_0

    .line 2753
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/twitter/app/users/UsersFragment;->p(I)Z

    .line 2754
    check-cast p1, Lbes;

    iget-wide v8, p1, Lbes;->b:J

    .line 2755
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_16

    .line 2756
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, v8, v9}, Lcom/twitter/model/util/FriendshipCache;->i(J)V

    .line 2757
    invoke-virtual {v1}, Lcom/twitter/android/UsersAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 2758
    :cond_16
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2759
    invoke-virtual {p0, v8, v9}, Lcom/twitter/app/users/UsersFragment;->b(J)Ljava/lang/Long;

    move-result-object v0

    .line 2760
    if-eqz v0, :cond_0

    .line 2761
    iget-boolean v3, p0, Lcom/twitter/app/users/UsersFragment;->b:Z

    if-eqz v3, :cond_17

    .line 2762
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v8, v9, v4, v5}, Lcom/twitter/android/UsersAdapter;->a(JJ)V

    .line 2764
    :cond_17
    new-instance v0, Lbig;

    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->T:Landroid/content/Context;

    iget-wide v3, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    iget v5, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    iget-wide v6, p0, Lcom/twitter/app/users/UsersFragment;->j:J

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, Lbig;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JIJJLjava/lang/Integer;)V

    .line 2767
    const/16 v1, 0x12

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto/16 :goto_0

    .line 2774
    :pswitch_e
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/service/s;)Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 2775
    if-eqz v2, :cond_0

    .line 2778
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/twitter/app/users/UsersFragment;->p(I)Z

    .line 2779
    check-cast p1, Lbes;

    iget-wide v2, p1, Lbes;->b:J

    .line 2780
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2781
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->h(J)V

    .line 2782
    invoke-virtual {v1}, Lcom/twitter/android/UsersAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 2788
    :pswitch_f
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/service/s;)Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 2789
    if-eqz v2, :cond_0

    .line 2792
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/twitter/app/users/UsersFragment;->p(I)Z

    .line 2793
    check-cast p1, Lbeq;

    invoke-virtual {p1}, Lbeq;->g()J

    move-result-wide v2

    .line 2794
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2795
    const/16 v0, 0x18

    if-ne p2, v0, :cond_18

    .line 2796
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->g(J)V

    .line 2800
    :goto_5
    invoke-virtual {v1}, Lcom/twitter/android/UsersAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 2798
    :cond_18
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/util/FriendshipCache;->f(J)V

    goto :goto_5

    .line 2805
    :pswitch_10
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->p(I)Z

    .line 2806
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->a(Z)V

    .line 2807
    iget-object v0, p1, Lcom/twitter/library/service/s;->o:Landroid/os/Bundle;

    const-string/jumbo v1, "next_cursor"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2808
    if-eqz v0, :cond_0

    .line 2809
    invoke-direct {p0, v0}, Lcom/twitter/app/users/UsersFragment;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2563
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_2
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_2
        :pswitch_d
        :pswitch_e
        :pswitch_2
        :pswitch_2
        :pswitch_f
        :pswitch_f
        :pswitch_2
        :pswitch_10
    .end packed-switch
.end method

.method public bridge synthetic a(Lcom/twitter/ui/user/BaseUserView;JII)V
    .locals 6

    .prologue
    .line 136
    move-object v1, p1

    check-cast v1, Lcom/twitter/ui/user/UserView;

    move-object v0, p0

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/app/users/UsersFragment;->a(Lcom/twitter/ui/user/UserView;JII)V

    return-void
.end method

.method public a(Lcom/twitter/ui/user/BaseUserView;Lcgi;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1473
    invoke-virtual {p1}, Lcom/twitter/ui/user/BaseUserView;->getUserId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 1474
    invoke-virtual {p1}, Lcom/twitter/ui/user/BaseUserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/db;

    .line 1475
    if-eqz v0, :cond_0

    iget v2, v0, Lcom/twitter/android/db;->i:I

    invoke-static {v2}, Lcom/twitter/app/users/UsersFragment;->c(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/app/users/UsersFragment;->H:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1476
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 1477
    invoke-virtual {p1}, Lcom/twitter/ui/user/BaseUserView;->getPromotedContent()Lcgi;

    move-result-object v1

    iget-object v0, v0, Lcom/twitter/android/db;->g:Ljava/lang/String;

    const/4 v4, 0x0

    .line 1476
    invoke-static {v2, v3, v1, v0, v4}, Lcom/twitter/library/scribe/b;->a(JLcgi;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 1478
    const-string/jumbo v1, "position"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->g:I

    .line 1479
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->G:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1481
    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->a:Ljava/util/Set;

    iget-object v1, p2, Lcgi;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1482
    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->a:Lcom/twitter/library/api/PromotedEvent;

    invoke-static {v0, p2}, Lbsq;->a(Lcom/twitter/library/api/PromotedEvent;Lcgi;)Lbsq$a;

    move-result-object v0

    invoke-virtual {v0}, Lbsq$a;->a()Lbsq;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1484
    :cond_1
    instance-of v0, p1, Lcom/twitter/ui/user/UserView;

    if-eqz v0, :cond_2

    .line 1485
    check-cast p1, Lcom/twitter/ui/user/UserView;

    .line 1486
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getUserId()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->e:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 1487
    iput-object p1, p0, Lcom/twitter/app/users/UsersFragment;->h:Lcom/twitter/ui/user/UserView;

    .line 1490
    :cond_2
    return-void
.end method

.method public a(Lcom/twitter/ui/user/UserView;JI)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 1296
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 1297
    iput-wide p2, p0, Lcom/twitter/app/users/UsersFragment;->e:J

    .line 1298
    iput p4, p0, Lcom/twitter/app/users/UsersFragment;->i:I

    .line 1299
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    invoke-direct {v0, v2}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a09f6

    .line 1300
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a09f5

    .line 1301
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0a40

    .line 1302
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a05e0

    .line 1303
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 1304
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 1305
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 1306
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 1319
    :goto_0
    return-void

    .line 1308
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/twitter/android/db;

    .line 1311
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1312
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserView;->b(Z)V

    .line 1313
    invoke-direct {p0, p2, p3, p1, p4}, Lcom/twitter/app/users/UsersFragment;->a(JLcom/twitter/ui/user/UserView;I)V

    goto :goto_0

    .line 1315
    :cond_1
    invoke-virtual {p1, v2}, Lcom/twitter/ui/user/UserView;->b(Z)V

    .line 1316
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getPromotedContent()Lcgi;

    move-result-object v4

    move-object v1, p0

    move-wide v2, p2

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/twitter/app/users/UsersFragment;->a(JLcgi;Lcom/twitter/android/db;I)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/ui/user/UserView;JII)V
    .locals 2

    .prologue
    .line 1280
    iget-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->U:Z

    if-eqz v0, :cond_1

    .line 1281
    invoke-direct {p0, p1, p4}, Lcom/twitter/app/users/UsersFragment;->a(Lcom/twitter/ui/user/UserView;I)V

    .line 1293
    :cond_0
    :goto_0
    return-void

    .line 1282
    :cond_1
    const v0, 0x7f130003

    if-ne p4, v0, :cond_2

    .line 1283
    invoke-virtual {p0, p1, p2, p3, p5}, Lcom/twitter/app/users/UsersFragment;->a(Lcom/twitter/ui/user/UserView;JI)V

    goto :goto_0

    .line 1284
    :cond_2
    const v0, 0x7f130013

    if-ne p4, v0, :cond_3

    .line 1285
    invoke-virtual {p0, p1, p2, p3, p5}, Lcom/twitter/app/users/UsersFragment;->b(Lcom/twitter/ui/user/UserView;JI)V

    goto :goto_0

    .line 1286
    :cond_3
    const v0, 0x7f130056

    if-ne p4, v0, :cond_4

    .line 1287
    invoke-virtual {p0, p1, p2, p3, p5}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/ui/user/UserView;JI)V

    goto :goto_0

    .line 1288
    :cond_4
    const v0, 0x7f130096

    if-ne p4, v0, :cond_5

    .line 1289
    invoke-virtual {p0, p1, p2, p3, p5}, Lcom/twitter/app/users/UsersFragment;->d(Lcom/twitter/ui/user/UserView;JI)V

    goto :goto_0

    .line 1290
    :cond_5
    const v0, 0x7f130097

    if-ne p4, v0, :cond_0

    .line 1291
    invoke-virtual {p0, p1, p2, p3, p5}, Lcom/twitter/app/users/UsersFragment;->a(Landroid/view/View;JI)V

    goto :goto_0
.end method

.method protected aO()Z
    .locals 2

    .prologue
    .line 2428
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    const/16 v1, 0x25

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected aP()Z
    .locals 2

    .prologue
    .line 2433
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    const/16 v1, 0x1a

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    const/16 v1, 0x26

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected aQ()Z
    .locals 2

    .prologue
    .line 2438
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 2439
    invoke-static {}, Lcom/twitter/android/revenue/k;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2438
    :goto_0
    return v0

    .line 2439
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected aR()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2458
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    sparse-switch v0, :sswitch_data_0

    .line 2468
    const/4 v0, 0x0

    .line 2472
    :goto_0
    return-object v0

    .line 2460
    :sswitch_0
    const-string/jumbo v0, "similar_to::stream::results"

    goto :goto_0

    .line 2464
    :sswitch_1
    const-string/jumbo v0, "category:who_to_follow:::results"

    goto :goto_0

    .line 2458
    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_1
        0xa -> :sswitch_0
    .end sparse-switch
.end method

.method protected aS()V
    .locals 6

    .prologue
    .line 2488
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->G:Ljava/util/List;

    .line 2489
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2490
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->aR()Ljava/lang/String;

    move-result-object v0

    .line 2491
    if-eqz v0, :cond_0

    .line 2492
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 2493
    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 2494
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(Ljava/util/List;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    .line 2495
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->l(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 2492
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 2497
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2499
    :cond_1
    return-void
.end method

.method protected aT()V
    .locals 3

    .prologue
    .line 2548
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->ah:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/app/users/UsersFragment;->ah:I

    .line 2549
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->F()V

    .line 2550
    invoke-direct {p0}, Lcom/twitter/app/users/UsersFragment;->aW()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2551
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 2553
    :cond_0
    return-void
.end method

.method public a_(II)I
    .locals 1

    .prologue
    .line 1941
    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected ar_()V
    .locals 1

    .prologue
    .line 1124
    invoke-super {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->ar_()V

    .line 1125
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->ak()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1126
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->p()V

    .line 1130
    :cond_0
    :goto_0
    return-void

    .line 1127
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UsersAdapter;

    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1128
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->m(I)Z

    goto :goto_0
.end method

.method b(JILcgi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 9

    .prologue
    .line 2113
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p6, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "user"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p7, v0, v1

    invoke-static {v0}, Lcom/twitter/analytics/model/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    move/from16 v8, p8

    invoke-virtual/range {v1 .. v8}, Lcom/twitter/app/users/UsersFragment;->a(JILcgi;Ljava/lang/String;Ljava/lang/String;I)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    return-object v0
.end method

.method b(J)Ljava/lang/Long;
    .locals 3

    .prologue
    .line 1855
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1856
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UsersAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/UsersAdapter;->b(J)Ljava/lang/Long;

    .line 1858
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->d:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public b()V
    .locals 7

    .prologue
    const/4 v1, 0x3

    const/4 v6, 0x1

    .line 824
    invoke-super {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->b()V

    .line 825
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 826
    iget-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->ac:Z

    if-eqz v0, :cond_3

    .line 827
    iget-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->ag:Z

    if-eqz v0, :cond_2

    .line 828
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->p()V

    .line 840
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 841
    if-eqz v0, :cond_1

    iget-wide v2, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-wide v4, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    iget-boolean v0, v0, Lcom/twitter/model/core/TwitterUser;->l:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    if-ne v0, v6, :cond_1

    .line 842
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v6, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 844
    :cond_1
    return-void

    .line 830
    :cond_2
    invoke-virtual {p0, v1}, Lcom/twitter/app/users/UsersFragment;->m(I)Z

    goto :goto_0

    .line 832
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->ak()Z

    move-result v0

    if-nez v0, :cond_4

    .line 834
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->p()V

    goto :goto_0

    .line 835
    :cond_4
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UsersAdapter;

    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 836
    invoke-virtual {p0, v1}, Lcom/twitter/app/users/UsersFragment;->m(I)Z

    goto :goto_0
.end method

.method b(JILcgi;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 9

    .prologue
    .line 2251
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    .line 2252
    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->e(I)Ljava/lang/String;

    move-result-object v0

    .line 2251
    invoke-static {p6, v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2253
    if-eqz v0, :cond_1

    .line 2254
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":user:profile_click"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    move/from16 v8, p7

    .line 2255
    invoke-virtual/range {v1 .. v8}, Lcom/twitter/app/users/UsersFragment;->a(JILcgi;Ljava/lang/String;Ljava/lang/String;I)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 2256
    iget v1, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_0

    .line 2257
    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->l(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 2259
    :cond_0
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 2261
    :cond_1
    return-void
.end method

.method protected b(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 1249
    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    .line 1250
    instance-of v1, v0, Landroid/content/Intent;

    if-eqz v1, :cond_0

    .line 1251
    check-cast v0, Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->startActivity(Landroid/content/Intent;)V

    .line 1253
    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/ui/user/UserView;JI)V
    .locals 8

    .prologue
    .line 1322
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/twitter/android/db;

    .line 1325
    iget-object v0, p1, Lcom/twitter/ui/user/UserView;->r:Lcom/twitter/ui/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/ActionButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1326
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getPromotedContent()Lcgi;

    move-result-object v4

    move-object v1, p0

    move-wide v2, p2

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/twitter/app/users/UsersFragment;->c(JLcgi;Lcom/twitter/android/db;I)V

    .line 1327
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->aO()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1328
    iget-object v0, p1, Lcom/twitter/ui/user/UserView;->r:Lcom/twitter/ui/widget/ActionButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/ActionButton;->setVisibility(I)V

    .line 1329
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/ui/user/UserView;->setFollowVisibility(I)V

    .line 1334
    :cond_0
    :goto_0
    return-void

    .line 1332
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getPromotedContent()Lcgi;

    move-result-object v4

    move-object v1, p0

    move-wide v2, p2

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/twitter/app/users/UsersFragment;->b(JLcgi;Lcom/twitter/android/db;I)V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 1607
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->a(Z)V

    .line 1609
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->K:Z

    if-nez v0, :cond_0

    .line 1610
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->B()V

    .line 1612
    :cond_0
    return-void
.end method

.method public bm_()V
    .locals 1

    .prologue
    .line 1877
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->x:Z

    .line 1878
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UsersAdapter;

    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->notifyDataSetChanged()V

    .line 1879
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->P()V

    .line 1880
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 1884
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->x:Z

    .line 1885
    return-void
.end method

.method protected c(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 1256
    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    .line 1257
    instance-of v1, v0, Landroid/content/Intent;

    if-eqz v1, :cond_0

    .line 1258
    check-cast v0, Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->startActivity(Landroid/content/Intent;)V

    .line 1260
    :cond_0
    return-void
.end method

.method public c(Lcom/twitter/ui/user/UserView;JI)V
    .locals 8

    .prologue
    .line 1337
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/twitter/android/db;

    .line 1338
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->b()Z

    move-result v0

    .line 1339
    if-eqz v0, :cond_0

    .line 1340
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getPromotedContent()Lcgi;

    move-result-object v4

    move-object v1, p0

    move-wide v2, p2

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/twitter/app/users/UsersFragment;->e(JLcgi;Lcom/twitter/android/db;I)V

    .line 1344
    :goto_0
    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->aP()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/twitter/ui/user/UserView;->a(ZZ)V

    .line 1345
    return-void

    .line 1342
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getPromotedContent()Lcgi;

    move-result-object v4

    move-object v1, p0

    move-wide v2, p2

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/twitter/app/users/UsersFragment;->d(JLcgi;Lcom/twitter/android/db;I)V

    goto :goto_0

    .line 1344
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public d(Lcom/twitter/ui/user/UserView;JI)V
    .locals 2

    .prologue
    .line 1348
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->r:Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    if-eqz v0, :cond_0

    .line 1349
    iget-object v0, p1, Lcom/twitter/ui/user/UserView;->s:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->r:Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    iget-boolean v1, v1, Lcom/twitter/android/UsersAdapter$CheckboxConfig;->a:Z

    if-eq v0, v1, :cond_1

    .line 1350
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->r:Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    iget-object v0, v0, Lcom/twitter/android/UsersAdapter$CheckboxConfig;->b:Ljava/util/List;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1355
    :cond_0
    :goto_0
    return-void

    .line 1352
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->r:Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    iget-object v0, v0, Lcom/twitter/android/UsersAdapter$CheckboxConfig;->b:Ljava/util/List;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method d(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2209
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    sparse-switch v0, :sswitch_data_0

    .line 2223
    const/4 v0, 0x0

    .line 2228
    :goto_0
    if-eqz v0, :cond_0

    .line 2229
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    aput-object p1, v2, v0

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 2231
    :cond_0
    return-void

    .line 2211
    :sswitch_0
    const-string/jumbo v0, "followers:::"

    goto :goto_0

    .line 2215
    :sswitch_1
    const-string/jumbo v0, "following:::"

    goto :goto_0

    .line 2219
    :sswitch_2
    const-string/jumbo v0, "category:::user"

    goto :goto_0

    .line 2209
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_0
        0x6 -> :sswitch_2
    .end sparse-switch
.end method

.method e(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2265
    sparse-switch p1, :sswitch_data_0

    .line 2298
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2267
    :sswitch_0
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->ad:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 2268
    const-string/jumbo v0, "explore_email:category:"

    goto :goto_0

    .line 2270
    :cond_0
    const-string/jumbo v0, "category::"

    goto :goto_0

    .line 2274
    :sswitch_1
    const-string/jumbo v0, "similar_to::"

    goto :goto_0

    .line 2277
    :sswitch_2
    const-string/jumbo v0, "followers::"

    goto :goto_0

    .line 2280
    :sswitch_3
    const-string/jumbo v0, "blocked::"

    goto :goto_0

    .line 2283
    :sswitch_4
    const-string/jumbo v0, "blocked_imported::"

    goto :goto_0

    .line 2286
    :sswitch_5
    const-string/jumbo v0, "muted::"

    goto :goto_0

    .line 2289
    :sswitch_6
    const-string/jumbo v0, "muted_automated::"

    goto :goto_0

    .line 2292
    :sswitch_7
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/users/AddressbookContactsFragment;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2295
    :sswitch_8
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/users/AddressbookContactsFragment;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2265
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x6 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_5
        0x1c -> :sswitch_8
        0x25 -> :sswitch_4
        0x26 -> :sswitch_6
        0x29 -> :sswitch_7
    .end sparse-switch
.end method

.method protected i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->ae:Ljava/lang/String;

    return-object v0
.end method

.method i(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2026
    sparse-switch p1, :sswitch_data_0

    .line 2071
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2028
    :sswitch_0
    const-string/jumbo v0, "following:following:"

    goto :goto_0

    .line 2031
    :sswitch_1
    const-string/jumbo v0, "followers:followers:"

    goto :goto_0

    .line 2034
    :sswitch_2
    const-string/jumbo v0, "favorited_by::"

    goto :goto_0

    .line 2037
    :sswitch_3
    const-string/jumbo v0, "retweeted_by::"

    goto :goto_0

    .line 2040
    :sswitch_4
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/users/AddressbookContactsFragment;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2043
    :sswitch_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":follow_friends:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2046
    :sswitch_6
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->ad:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 2047
    const-string/jumbo v0, "explore_email:category:"

    goto :goto_0

    .line 2049
    :cond_0
    const-string/jumbo v0, "category::"

    goto :goto_0

    .line 2053
    :sswitch_7
    const-string/jumbo v0, "similar_to::"

    goto :goto_0

    .line 2056
    :sswitch_8
    const-string/jumbo v0, "blocked::"

    goto :goto_0

    .line 2059
    :sswitch_9
    const-string/jumbo v0, "blocked_imported::"

    goto :goto_0

    .line 2062
    :sswitch_a
    const-string/jumbo v0, "muted::"

    goto :goto_0

    .line 2065
    :sswitch_b
    const-string/jumbo v0, "muted_automated::"

    goto :goto_0

    .line 2068
    :sswitch_c
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/users/AddressbookContactsFragment;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2026
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_8
        0x6 -> :sswitch_6
        0x7 -> :sswitch_5
        0xa -> :sswitch_7
        0xb -> :sswitch_2
        0xc -> :sswitch_3
        0x1a -> :sswitch_a
        0x1c -> :sswitch_4
        0x25 -> :sswitch_9
        0x26 -> :sswitch_b
        0x29 -> :sswitch_c
    .end sparse-switch
.end method

.method protected j()Landroid/support/v4/content/Loader;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v8, 0x0

    .line 1001
    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->j:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 1002
    const-string/jumbo v1, "user_groups_type=? AND user_groups_tag=?"

    iput-object v1, p0, Lcom/twitter/app/users/UsersFragment;->B:Ljava/lang/String;

    .line 1003
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    iget v2, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    iput-object v1, p0, Lcom/twitter/app/users/UsersFragment;->C:[Ljava/lang/String;

    .line 1023
    :goto_0
    new-instance v0, Lcom/twitter/util/android/d;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    .line 1024
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/users/UsersFragment;->z:[Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/app/users/UsersFragment;->B:Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/app/users/UsersFragment;->C:[Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/app/users/UsersFragment;->A:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1026
    invoke-virtual {v0, v8}, Lcom/twitter/util/android/d;->a(Z)Lcom/twitter/util/android/d;

    move-result-object v0

    .line 1023
    return-object v0

    .line 1004
    :cond_0
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->D:[J

    if-eqz v1, :cond_2

    .line 1005
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->D:[J

    .line 1006
    array-length v2, v1

    .line 1007
    new-array v3, v2, [Ljava/lang/String;

    iput-object v3, p0, Lcom/twitter/app/users/UsersFragment;->C:[Ljava/lang/String;

    .line 1009
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "users_user_id"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, " IN (?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1010
    iget-object v4, p0, Lcom/twitter/app/users/UsersFragment;->C:[Ljava/lang/String;

    aget-wide v6, v1, v8

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 1011
    :goto_1
    if-ge v0, v2, :cond_1

    .line 1012
    const-string/jumbo v4, ", ?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1013
    iget-object v4, p0, Lcom/twitter/app/users/UsersFragment;->C:[Ljava/lang/String;

    aget-wide v6, v1, v0

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    .line 1011
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1015
    :cond_1
    const-string/jumbo v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1016
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->B:Ljava/lang/String;

    goto :goto_0

    .line 1018
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->z()V

    goto :goto_0
.end method

.method protected l()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1508
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->aC()Landroid/database/Cursor;

    move-result-object v0

    .line 1509
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->moveToLast()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1510
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->an()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 1511
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    const/16 v2, 0x190

    if-ge v1, v2, :cond_0

    .line 1512
    iget-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->k:Z

    if-eqz v1, :cond_0

    .line 1513
    invoke-virtual {p0, v3}, Lcom/twitter/app/users/UsersFragment;->m(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1515
    const-string/jumbo v1, "get_older"

    invoke-virtual {p0, v1}, Lcom/twitter/app/users/UsersFragment;->d(Ljava/lang/String;)V

    .line 1519
    :cond_0
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->ao:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/twitter/app/users/UsersFragment;->q()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1521
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v1, v3, :cond_3

    .line 1522
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->ao:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1523
    iput-boolean v3, p0, Lcom/twitter/app/users/UsersFragment;->aq:Z

    .line 1530
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->ap:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/twitter/app/users/UsersFragment;->aU()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1531
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v3, :cond_4

    .line 1532
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->ap:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1533
    iput-boolean v3, p0, Lcom/twitter/app/users/UsersFragment;->ar:Z

    .line 1540
    :cond_2
    :goto_1
    return-void

    .line 1525
    :cond_3
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->ao:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1526
    iput-boolean v4, p0, Lcom/twitter/app/users/UsersFragment;->aq:Z

    goto :goto_0

    .line 1535
    :cond_4
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->ap:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1536
    iput-boolean v4, p0, Lcom/twitter/app/users/UsersFragment;->ar:Z

    goto :goto_1
.end method

.method protected l(I)V
    .locals 10
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 731
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 732
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 733
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 735
    iget v4, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    sparse-switch v4, :sswitch_data_0

    .line 813
    invoke-virtual {p0, p1}, Lcom/twitter/app/users/UsersFragment;->r(I)Lcom/twitter/android/UsersAdapter;

    move-result-object v0

    .line 814
    invoke-virtual {v0, p0}, Lcom/twitter/android/UsersAdapter;->a(Lcom/twitter/android/av;)V

    .line 815
    new-instance v1, Lcom/twitter/android/widget/ap;

    new-array v2, v8, [Landroid/widget/BaseAdapter;

    aput-object v0, v2, v9

    invoke-direct {v1, v2, v8}, Lcom/twitter/android/widget/ap;-><init>([Landroid/widget/BaseAdapter;I)V

    iput-object v1, p0, Lcom/twitter/app/users/UsersFragment;->p:Lcom/twitter/android/widget/ap;

    .line 816
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/users/UsersFragment;->p:Lcom/twitter/android/widget/ap;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/app/common/list/l;->a(Lcjr;Landroid/widget/ListAdapter;)V

    .line 820
    :goto_0
    return-void

    .line 738
    :sswitch_0
    invoke-direct {p0, p1, v9}, Lcom/twitter/app/users/UsersFragment;->a(IZ)Lcom/twitter/android/bj;

    move-result-object v0

    .line 739
    iget-wide v4, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    cmp-long v1, v4, v2

    if-nez v1, :cond_0

    .line 740
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->aP()Z

    move-result v1

    invoke-virtual {v0, v8, v1}, Lcom/twitter/android/bj;->a(ZZ)V

    .line 742
    :cond_0
    invoke-virtual {v0, p0}, Lcom/twitter/android/bj;->a(Lcom/twitter/android/av;)V

    .line 743
    new-instance v1, Lcom/twitter/android/widget/ap;

    new-array v2, v8, [Landroid/widget/BaseAdapter;

    aput-object v0, v2, v9

    invoke-direct {v1, v2, v8}, Lcom/twitter/android/widget/ap;-><init>([Landroid/widget/BaseAdapter;I)V

    iput-object v1, p0, Lcom/twitter/app/users/UsersFragment;->p:Lcom/twitter/android/widget/ap;

    .line 744
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/users/UsersFragment;->p:Lcom/twitter/android/widget/ap;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/app/common/list/l;->a(Lcjr;Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 749
    :sswitch_1
    invoke-direct {p0, p1, v8}, Lcom/twitter/app/users/UsersFragment;->a(IZ)Lcom/twitter/android/bj;

    move-result-object v0

    .line 750
    invoke-virtual {v0, p0}, Lcom/twitter/android/bj;->a(Lcom/twitter/android/av;)V

    .line 751
    new-instance v1, Lcom/twitter/android/widget/ap;

    new-array v2, v8, [Landroid/widget/BaseAdapter;

    aput-object v0, v2, v9

    invoke-direct {v1, v2, v8}, Lcom/twitter/android/widget/ap;-><init>([Landroid/widget/BaseAdapter;I)V

    iput-object v1, p0, Lcom/twitter/app/users/UsersFragment;->p:Lcom/twitter/android/widget/ap;

    .line 752
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/users/UsersFragment;->p:Lcom/twitter/android/widget/ap;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/app/common/list/l;->a(Lcjr;Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 756
    :sswitch_2
    new-instance v0, Lcom/twitter/android/aa;

    new-instance v2, Lcom/twitter/app/users/UsersFragment$a;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/twitter/app/users/UsersFragment$a;-><init>(Lcom/twitter/app/users/UsersFragment;Lcom/twitter/app/users/UsersFragment$1;)V

    iget-object v3, p0, Lcom/twitter/app/users/UsersFragment;->n:Ljava/util/HashMap;

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/aa;-><init>(Landroid/content/Context;Lcom/twitter/ui/user/BaseUserView$a;Ljava/util/Map;)V

    .line 758
    new-instance v1, Lcom/twitter/android/widget/ap;

    new-array v2, v8, [Landroid/widget/BaseAdapter;

    aput-object v0, v2, v9

    invoke-direct {v1, v2, v8}, Lcom/twitter/android/widget/ap;-><init>([Landroid/widget/BaseAdapter;I)V

    iput-object v1, p0, Lcom/twitter/app/users/UsersFragment;->p:Lcom/twitter/android/widget/ap;

    .line 759
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/users/UsersFragment;->p:Lcom/twitter/android/widget/ap;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/app/common/list/l;->a(Lcjr;Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 764
    :sswitch_3
    invoke-direct {p0, p1, v9}, Lcom/twitter/app/users/UsersFragment;->a(IZ)Lcom/twitter/android/bj;

    move-result-object v2

    .line 765
    invoke-virtual {v2, p0}, Lcom/twitter/android/UsersAdapter;->a(Lcom/twitter/android/av;)V

    .line 767
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 769
    if-eqz v0, :cond_1

    iget-wide v4, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-wide v6, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_1

    .line 770
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->aP()Z

    move-result v3

    invoke-virtual {v2, v8, v3}, Lcom/twitter/android/UsersAdapter;->a(ZZ)V

    .line 774
    :cond_1
    if-eqz v0, :cond_2

    iget v3, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    if-ne v3, v8, :cond_2

    iget-wide v4, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-wide v6, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-boolean v0, v0, Lcom/twitter/model/core/TwitterUser;->l:Z

    if-eqz v0, :cond_2

    .line 776
    new-instance v0, Lcom/twitter/app/users/f;

    invoke-direct {v0}, Lcom/twitter/app/users/f;-><init>()V

    const/16 v3, 0x12

    .line 777
    invoke-virtual {v0, v3}, Lcom/twitter/app/users/f;->a(I)Lcom/twitter/app/users/f;

    move-result-object v0

    .line 778
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/f;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 779
    new-instance v3, Lcom/twitter/android/bz;

    new-instance v4, Lcom/twitter/android/ca;

    const v5, 0x7f0a03a7

    .line 780
    invoke-virtual {v1, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1, v0}, Lcom/twitter/android/ca;-><init>(Ljava/lang/String;Landroid/content/Intent;)V

    invoke-direct {v3, v4}, Lcom/twitter/android/bz;-><init>(Lcom/twitter/android/ca;)V

    .line 782
    new-instance v0, Lcom/twitter/android/widget/ap;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    aput-object v3, v1, v9

    aput-object v2, v1, v8

    invoke-direct {v0, v1, v8}, Lcom/twitter/android/widget/ap;-><init>([Landroid/widget/BaseAdapter;I)V

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->p:Lcom/twitter/android/widget/ap;

    .line 784
    iput v9, p0, Lcom/twitter/app/users/UsersFragment;->s:I

    .line 785
    iput-object v3, p0, Lcom/twitter/app/users/UsersFragment;->q:Lcom/twitter/android/bz;

    .line 789
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->p:Lcom/twitter/android/widget/ap;

    invoke-virtual {v0, v2, v1}, Lcom/twitter/app/common/list/l;->a(Lcjr;Landroid/widget/ListAdapter;)V

    goto/16 :goto_0

    .line 787
    :cond_2
    new-instance v0, Lcom/twitter/android/widget/ap;

    new-array v1, v8, [Landroid/widget/BaseAdapter;

    aput-object v2, v1, v9

    invoke-direct {v0, v1, v8}, Lcom/twitter/android/widget/ap;-><init>([Landroid/widget/BaseAdapter;I)V

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->p:Lcom/twitter/android/widget/ap;

    goto :goto_1

    .line 793
    :sswitch_4
    invoke-virtual {p0, p1}, Lcom/twitter/app/users/UsersFragment;->r(I)Lcom/twitter/android/UsersAdapter;

    move-result-object v0

    .line 794
    invoke-virtual {v0, v8}, Lcom/twitter/android/UsersAdapter;->d(Z)V

    .line 795
    invoke-virtual {v0, p0}, Lcom/twitter/android/UsersAdapter;->a(Lcom/twitter/android/av;)V

    .line 796
    new-instance v1, Lcom/twitter/android/widget/ap;

    new-array v2, v8, [Landroid/widget/BaseAdapter;

    aput-object v0, v2, v9

    invoke-direct {v1, v2, v8}, Lcom/twitter/android/widget/ap;-><init>([Landroid/widget/BaseAdapter;I)V

    iput-object v1, p0, Lcom/twitter/app/users/UsersFragment;->p:Lcom/twitter/android/widget/ap;

    .line 797
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/users/UsersFragment;->p:Lcom/twitter/android/widget/ap;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/app/common/list/l;->a(Lcjr;Landroid/widget/ListAdapter;)V

    goto/16 :goto_0

    .line 804
    :sswitch_5
    invoke-virtual {p0, p1}, Lcom/twitter/app/users/UsersFragment;->r(I)Lcom/twitter/android/UsersAdapter;

    move-result-object v0

    .line 805
    invoke-virtual {v0, v8}, Lcom/twitter/android/UsersAdapter;->d(Z)V

    .line 806
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->aP()Z

    move-result v1

    invoke-virtual {v0, v8, v1}, Lcom/twitter/android/UsersAdapter;->a(ZZ)V

    .line 807
    invoke-virtual {v0, p0}, Lcom/twitter/android/UsersAdapter;->a(Lcom/twitter/android/av;)V

    .line 808
    new-instance v1, Lcom/twitter/android/widget/ap;

    new-array v2, v8, [Landroid/widget/BaseAdapter;

    aput-object v0, v2, v9

    invoke-direct {v1, v2, v8}, Lcom/twitter/android/widget/ap;-><init>([Landroid/widget/BaseAdapter;I)V

    iput-object v1, p0, Lcom/twitter/app/users/UsersFragment;->p:Lcom/twitter/android/widget/ap;

    .line 809
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/users/UsersFragment;->p:Lcom/twitter/android/widget/ap;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/app/common/list/l;->a(Lcjr;Landroid/widget/ListAdapter;)V

    goto/16 :goto_0

    .line 735
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_3
        0x2 -> :sswitch_5
        0x4 -> :sswitch_4
        0xa -> :sswitch_1
        0x10 -> :sswitch_0
        0x12 -> :sswitch_2
        0x14 -> :sswitch_1
        0x1a -> :sswitch_5
        0x1d -> :sswitch_3
        0x25 -> :sswitch_5
        0x26 -> :sswitch_5
    .end sparse-switch
.end method

.method protected m(I)Z
    .locals 1

    .prologue
    .line 1657
    invoke-virtual {p0, p1}, Lcom/twitter/app/users/UsersFragment;->d(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/twitter/app/users/UsersFragment;->n(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected n(I)Z
    .locals 11

    .prologue
    const/4 v10, 0x7

    const/4 v5, 0x6

    const/4 v4, 0x3

    const/4 v9, 0x1

    const/4 v0, 0x0

    .line 1661
    iget v1, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    packed-switch v1, :pswitch_data_0

    .line 1788
    :pswitch_0
    new-instance v1, Lbhn;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lbhn;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;I)V

    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->F:Ljava/lang/String;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    .line 1790
    invoke-virtual {v1, v0, v2, v3}, Lbhn;->a(Ljava/lang/String;J)Lbhn;

    move-result-object v0

    .line 1791
    invoke-virtual {p0, p1}, Lcom/twitter/app/users/UsersFragment;->o(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbhn;->c(I)Lbhn;

    move-result-object v0

    const/4 v1, 0x4

    .line 1788
    invoke-virtual {p0, v0, v1, p1}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    :cond_0
    :goto_0
    move v0, v9

    .line 1795
    :cond_1
    :goto_1
    return v0

    .line 1663
    :pswitch_1
    new-instance v0, Lbhn;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2, v9}, Lbhn;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;I)V

    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->F:Ljava/lang/String;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    .line 1665
    invoke-virtual {v0, v1, v2, v3}, Lbhn;->a(Ljava/lang/String;J)Lbhn;

    move-result-object v0

    .line 1666
    invoke-virtual {p0, p1}, Lcom/twitter/app/users/UsersFragment;->o(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbhn;->c(I)Lbhn;

    move-result-object v0

    .line 1663
    invoke-virtual {p0, v0, v4, p1}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 1668
    new-instance v0, Lbib;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 1669
    invoke-virtual {p0, p1}, Lcom/twitter/app/users/UsersFragment;->o(I)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lbib;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;I)V

    .line 1668
    invoke-virtual {p0, v0, v9, p1}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto :goto_0

    .line 1674
    :pswitch_2
    new-instance v0, Lbhn;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v2

    const/16 v3, 0x1d

    invoke-direct {v0, v1, v2, v3}, Lbhn;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;I)V

    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->F:Ljava/lang/String;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    .line 1676
    invoke-virtual {v0, v1, v2, v3}, Lbhn;->a(Ljava/lang/String;J)Lbhn;

    move-result-object v0

    .line 1677
    invoke-virtual {p0, p1}, Lcom/twitter/app/users/UsersFragment;->o(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbhn;->c(I)Lbhn;

    move-result-object v0

    const/16 v1, 0xd

    .line 1674
    invoke-virtual {p0, v0, v1, p1}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto :goto_0

    .line 1682
    :pswitch_3
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->ad:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    .line 1683
    new-instance v1, Lcom/twitter/app/users/UsersFragment$b;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/users/UsersFragment;->ad:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/twitter/app/users/UsersFragment$b;-><init>(Lcom/twitter/app/users/UsersFragment;Landroid/support/v4/app/FragmentActivity;Ljava/util/List;Lcom/twitter/app/users/UsersFragment$1;)V

    new-array v0, v0, [Ljava/lang/Void;

    .line 1684
    invoke-virtual {v1, v0}, Lcom/twitter/app/users/UsersFragment$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 1686
    :cond_2
    new-instance v1, Lcom/twitter/library/api/f;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    iget-object v6, p0, Lcom/twitter/app/users/UsersFragment;->E:Ljava/lang/String;

    const/4 v7, -0x1

    invoke-direct/range {v1 .. v7}, Lcom/twitter/library/api/f;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;I)V

    const/16 v0, 0xf

    invoke-virtual {p0, v1, v0, p1}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto/16 :goto_0

    .line 1692
    :pswitch_4
    new-instance v0, Lbcz;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget v3, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    iget-wide v4, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    iget-wide v6, p0, Lcom/twitter/app/users/UsersFragment;->j:J

    .line 1693
    invoke-virtual {p0, p1}, Lcom/twitter/app/users/UsersFragment;->o(I)I

    move-result v8

    invoke-direct/range {v0 .. v8}, Lbcz;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;IJJI)V

    .line 1692
    invoke-virtual {p0, v0, v10, p1}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto/16 :goto_0

    .line 1697
    :pswitch_5
    new-instance v0, Lbcz;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget v3, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    iget-wide v4, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    iget-wide v6, p0, Lcom/twitter/app/users/UsersFragment;->j:J

    .line 1698
    invoke-virtual {p0, p1}, Lcom/twitter/app/users/UsersFragment;->o(I)I

    move-result v8

    invoke-direct/range {v0 .. v8}, Lbcz;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;IJJI)V

    .line 1697
    invoke-virtual {p0, v0, v10, p1}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto/16 :goto_0

    .line 1703
    :pswitch_6
    new-instance v1, Lbhz;

    .line 1704
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget v4, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    invoke-direct {v1, v2, v3, v4}, Lbhz;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;I)V

    .line 1705
    iput v0, v1, Lbhz;->c:I

    .line 1706
    iput v5, v1, Lbhz;->j:I

    .line 1707
    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    iput-wide v2, v1, Lbhz;->b:J

    .line 1708
    invoke-virtual {p0, v1, v5, p1}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto/16 :goto_0

    .line 1712
    :pswitch_7
    invoke-direct {p0}, Lcom/twitter/app/users/UsersFragment;->aW()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1713
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->F()V

    goto/16 :goto_1

    .line 1716
    :cond_3
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/twitter/app/users/UsersFragment;->b(Ljava/lang/String;)V

    .line 1717
    if-ne p1, v4, :cond_0

    .line 1718
    invoke-direct {p0}, Lcom/twitter/app/users/UsersFragment;->aV()Z

    move-result v1

    .line 1719
    if-nez v1, :cond_0

    .line 1720
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->F()V

    goto/16 :goto_1

    .line 1727
    :pswitch_8
    if-ne p1, v4, :cond_1

    .line 1728
    invoke-direct {p0}, Lcom/twitter/app/users/UsersFragment;->aV()Z

    goto/16 :goto_0

    .line 1740
    :pswitch_9
    new-instance v0, Lbho;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lbho;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 1741
    iget-object v1, v0, Lbho;->a:Lbic$a;

    iget-object v2, p0, Lcom/twitter/app/users/UsersFragment;->D:[J

    invoke-virtual {v1, v2}, Lbic$a;->a([J)Lbic$a;

    move-result-object v1

    iget v2, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    .line 1742
    invoke-virtual {v1, v2}, Lbic$a;->a(I)Lbic$a;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->j:J

    .line 1743
    invoke-virtual {v1, v2, v3}, Lbic$a;->a(J)Lbic$a;

    .line 1745
    const/16 v1, 0xe

    invoke-virtual {p0, v0, v1, p1}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto/16 :goto_0

    .line 1749
    :pswitch_a
    new-instance v0, Lbib;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 1750
    invoke-virtual {p0, p1}, Lcom/twitter/app/users/UsersFragment;->o(I)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lbib;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;I)V

    .line 1749
    invoke-virtual {p0, v0, v9, p1}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto/16 :goto_0

    .line 1755
    :pswitch_b
    new-instance v0, Lbhn;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v2

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lbhn;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;I)V

    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->F:Ljava/lang/String;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    .line 1757
    invoke-virtual {v0, v1, v2, v3}, Lbhn;->a(Ljava/lang/String;J)Lbhn;

    move-result-object v0

    .line 1758
    invoke-virtual {p0, p1}, Lcom/twitter/app/users/UsersFragment;->o(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbhn;->c(I)Lbhn;

    move-result-object v0

    const/4 v1, 0x5

    .line 1755
    invoke-virtual {p0, v0, v1, p1}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto/16 :goto_0

    .line 1763
    :pswitch_c
    new-instance v0, Lbhn;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Lbhn;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;I)V

    .line 1764
    invoke-virtual {p0, p1}, Lcom/twitter/app/users/UsersFragment;->o(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbhn;->c(I)Lbhn;

    move-result-object v0

    const/16 v1, 0x13

    .line 1763
    invoke-virtual {p0, v0, v1, p1}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto/16 :goto_0

    .line 1769
    :pswitch_d
    new-instance v0, Lbhn;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v2

    const/16 v3, 0x25

    invoke-direct {v0, v1, v2, v3}, Lbhn;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;I)V

    .line 1770
    invoke-virtual {p0, p1}, Lcom/twitter/app/users/UsersFragment;->o(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbhn;->c(I)Lbhn;

    move-result-object v0

    const/16 v1, 0x16

    .line 1769
    invoke-virtual {p0, v0, v1, p1}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto/16 :goto_0

    .line 1775
    :pswitch_e
    new-instance v0, Lbhn;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v2

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lbhn;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;I)V

    .line 1776
    invoke-virtual {p0, p1}, Lcom/twitter/app/users/UsersFragment;->o(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbhn;->c(I)Lbhn;

    move-result-object v0

    const/16 v1, 0x17

    .line 1775
    invoke-virtual {p0, v0, v1, p1}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto/16 :goto_0

    .line 1781
    :pswitch_f
    new-instance v0, Lbhn;

    .line 1782
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v2

    const/16 v3, 0x26

    invoke-direct {v0, v1, v2, v3}, Lbhn;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;I)V

    .line 1783
    invoke-virtual {p0, p1}, Lcom/twitter/app/users/UsersFragment;->o(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbhn;->c(I)Lbhn;

    move-result-object v0

    const/16 v1, 0x1a

    .line 1781
    invoke-virtual {p0, v0, v1, p1}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto/16 :goto_0

    .line 1661
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_c
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_9
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_e
        :pswitch_0
        :pswitch_7
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_d
        :pswitch_f
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_9
        :pswitch_9
    .end packed-switch
.end method

.method protected o(I)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1806
    packed-switch p1, :pswitch_data_0

    .line 1824
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fetch type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1812
    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->aC()Landroid/database/Cursor;

    move-result-object v1

    .line 1813
    if-eqz v1, :cond_0

    .line 1814
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 1815
    const/4 v0, 0x1

    .line 1820
    :cond_0
    :pswitch_1
    return v0

    .line 1806
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/16 v2, 0x8

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 529
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 530
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->y()V

    .line 531
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    const v3, 0x7f13043c

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->ao:Landroid/widget/TextView;

    .line 532
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->ao:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/app/users/UsersFragment;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 533
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->ao:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0041

    iget v5, p0, Lcom/twitter/app/users/UsersFragment;->am:I

    new-array v6, v8, [Ljava/lang/Object;

    iget v7, p0, Lcom/twitter/app/users/UsersFragment;->am:I

    .line 534
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 533
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 535
    iget-object v3, p0, Lcom/twitter/app/users/UsersFragment;->ao:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->aq:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 538
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    const v3, 0x7f13043d

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->ap:Landroid/widget/TextView;

    .line 539
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->ap:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/twitter/app/users/UsersFragment;->aU()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 541
    new-array v0, v8, [Ljava/lang/Object;

    .line 542
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a0bc8

    const v5, 0x7f1100c9

    const-class v6, Lcom/twitter/android/WebViewActivity;

    invoke-static {v3, v4, v5, v6}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;IILjava/lang/Class;)Lcom/twitter/ui/view/a;

    move-result-object v3

    aput-object v3, v0, v1

    .line 544
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a03a1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 545
    iget-object v4, p0, Lcom/twitter/app/users/UsersFragment;->ap:Landroid/widget/TextView;

    const-string/jumbo v5, "{{}}"

    invoke-static {v0, v3, v5}, Lcom/twitter/library/util/af;->a([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 546
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->ap:Landroid/widget/TextView;

    iget-boolean v3, p0, Lcom/twitter/app/users/UsersFragment;->ar:Z

    if-eqz v3, :cond_3

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 548
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 535
    goto :goto_0

    :cond_3
    move v1, v2

    .line 546
    goto :goto_1
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x1

    .line 924
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->ay()Z

    move-result v0

    if-nez v0, :cond_1

    .line 997
    :cond_0
    :goto_0
    return-void

    .line 927
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 932
    :pswitch_0
    const/4 v0, -0x1

    if-ne v0, p2, :cond_0

    if-eqz p3, :cond_0

    invoke-direct {p0}, Lcom/twitter/app/users/UsersFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 933
    const-string/jumbo v0, "user_id"

    invoke-virtual {p3, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 934
    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    const-string/jumbo v0, "friendship"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 935
    const-string/jumbo v0, "friendship"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 937
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UsersAdapter;

    .line 938
    const/16 v1, 0x12

    iget v5, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    if-ne v1, v5, :cond_5

    .line 939
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->n:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 940
    if-eqz v1, :cond_2

    .line 941
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    :pswitch_1
    goto :goto_0

    .line 943
    :pswitch_2
    invoke-static {v4}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 944
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->n:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 945
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 944
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 946
    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 951
    :pswitch_3
    invoke-static {v4}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 952
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->n:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 953
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 952
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 954
    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 965
    :cond_2
    invoke-static {v4}, Lcom/twitter/model/core/g;->c(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 966
    invoke-static {v4}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 967
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->n:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 968
    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 970
    :cond_3
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->n:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 971
    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 974
    :cond_4
    invoke-static {v4}, Lcom/twitter/model/core/g;->j(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 977
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->n:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 978
    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 982
    :cond_5
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    .line 983
    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/model/util/FriendshipCache;->a(JI)Z

    move-result v5

    if-nez v5, :cond_0

    .line 984
    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/model/util/FriendshipCache;->b(JI)V

    .line 985
    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->notifyDataSetChanged()V

    .line 986
    iput-boolean v6, p0, Lcom/twitter/app/users/UsersFragment;->w:Z

    goto/16 :goto_0

    .line 927
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 941
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 333
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 335
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->s()Lcom/twitter/app/users/g;

    move-result-object v0

    .line 338
    invoke-virtual {v0}, Lcom/twitter/app/users/g;->e()I

    move-result v1

    iput v1, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    .line 339
    invoke-virtual {v0}, Lcom/twitter/app/users/g;->y()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/users/UsersFragment;->ad:Ljava/util/ArrayList;

    .line 341
    invoke-direct {p0}, Lcom/twitter/app/users/UsersFragment;->aX()V

    .line 342
    new-instance v1, Lcom/twitter/android/widget/ai;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/twitter/android/widget/ai;-><init>(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 343
    invoke-virtual {v0}, Lcom/twitter/app/users/g;->x()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/users/UsersFragment;->E:Ljava/lang/String;

    .line 345
    invoke-virtual {v0}, Lcom/twitter/app/users/g;->f()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->j:J

    .line 346
    invoke-virtual {v0}, Lcom/twitter/app/users/g;->g()[J

    move-result-object v1

    .line 347
    if-eqz v1, :cond_0

    array-length v2, v1

    if-lez v2, :cond_0

    .line 348
    iput-object v1, p0, Lcom/twitter/app/users/UsersFragment;->D:[J

    .line 350
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/app/users/g;->A()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/users/UsersFragment;->F:Ljava/lang/String;

    .line 351
    invoke-virtual {v0}, Lcom/twitter/app/users/g;->B()I

    move-result v1

    iput v1, p0, Lcom/twitter/app/users/UsersFragment;->I:I

    .line 352
    invoke-virtual {v0}, Lcom/twitter/app/users/g;->z()I

    move-result v1

    iput v1, p0, Lcom/twitter/app/users/UsersFragment;->c:I

    .line 354
    invoke-virtual {v0}, Lcom/twitter/app/users/g;->C()Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->ac:Z

    .line 356
    invoke-virtual {v0}, Lcom/twitter/app/users/g;->D()Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->ak:Z

    .line 358
    invoke-virtual {v0}, Lcom/twitter/app/users/g;->c()I

    move-result v1

    iput v1, p0, Lcom/twitter/app/users/UsersFragment;->am:I

    .line 360
    invoke-virtual {v0}, Lcom/twitter/app/users/g;->d()I

    move-result v1

    iput v1, p0, Lcom/twitter/app/users/UsersFragment;->an:I

    .line 362
    invoke-virtual {v0}, Lcom/twitter/app/users/g;->G()Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->as:Z

    .line 364
    invoke-virtual {v0}, Lcom/twitter/app/users/g;->H()Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->at:Z

    .line 366
    invoke-virtual {v0}, Lcom/twitter/app/users/g;->I()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/users/UsersFragment;->ae:Ljava/lang/String;

    .line 368
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->t()V

    .line 370
    if-eqz p1, :cond_9

    .line 371
    const-string/jumbo v0, "state_load_flags"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/app/users/UsersFragment;->u:I

    .line 372
    const-string/jumbo v0, "state_dialog_user"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/app/users/UsersFragment;->e:J

    .line 373
    const-string/jumbo v0, "state_dialog_pc"

    .line 374
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 373
    invoke-static {v0}, Lcgi;->a([B)Lcgi;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->g:Lcgi;

    .line 375
    const-string/jumbo v0, "state_dialog_user_token"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->f:Ljava/lang/String;

    .line 376
    const-string/jumbo v0, "state_dialog_user_position"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/app/users/UsersFragment;->i:I

    .line 377
    const-string/jumbo v0, "state_pending_follows"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->l:Ljava/util/HashSet;

    .line 379
    const-string/jumbo v0, "state_lookup_complete_pages"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/app/users/UsersFragment;->J:I

    .line 380
    const-string/jumbo v0, "state_lookup_complete"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->K:Z

    .line 381
    const-string/jumbo v0, "state_reverse_lookup_complete"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->aa:Z

    .line 382
    const-string/jumbo v0, "state_wtf_complete"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->ab:Z

    .line 383
    const-string/jumbo v0, "state_completed_components"

    .line 384
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/app/users/UsersFragment;->ah:I

    .line 385
    const-string/jumbo v0, "state_total_progress_components"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/app/users/UsersFragment;->ai:I

    .line 387
    const-string/jumbo v0, "state_user_ids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->D:[J

    .line 388
    const-string/jumbo v0, "state_friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 389
    const-string/jumbo v0, "state_friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/util/FriendshipCache;

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    .line 394
    :goto_0
    const-string/jumbo v0, "state_incoming_friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 396
    const-string/jumbo v0, "state_incoming_friendship_cache"

    .line 398
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 399
    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->n:Ljava/util/HashMap;

    .line 403
    :goto_1
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    const/16 v1, 0x20

    if-ne v0, v1, :cond_2

    :cond_1
    const-string/jumbo v0, "state_fetched_category_users"

    .line 405
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 406
    const-string/jumbo v0, "state_fetched_category_users"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->v:Z

    .line 408
    :cond_2
    const-string/jumbo v0, "state_loader_initialized"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 409
    const-string/jumbo v0, "state_loader_initialized"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->ag:Z

    .line 412
    :cond_3
    const-string/jumbo v0, "state_hide_contacts_import_cta"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->aj:Z

    .line 413
    const-string/jumbo v0, "is_hidden"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->x:Z

    .line 414
    const-string/jumbo v0, "fast_follow_visible"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->aq:Z

    .line 415
    const-string/jumbo v0, "filtered_follow_visible"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->ar:Z

    .line 416
    const-string/jumbo v0, "data_was_fetched"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->af:Z

    .line 417
    const-string/jumbo v0, "checked_users"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->r:Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    .line 434
    :goto_2
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    invoke-static {v0}, Lcom/twitter/app/users/UsersFragment;->c(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 435
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->G:Ljava/util/List;

    .line 436
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->H:Ljava/util/Set;

    .line 439
    :cond_4
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    const/16 v1, 0x1c

    if-ne v0, v1, :cond_5

    .line 440
    new-instance v0, Lcom/twitter/app/users/UsersFragment$d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/app/users/UsersFragment$d;-><init>(Lcom/twitter/app/users/UsersFragment;Lcom/twitter/app/users/UsersFragment$1;)V

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->al:Lcom/twitter/app/users/UsersFragment$d;

    .line 441
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->S:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->al:Lcom/twitter/app/users/UsersFragment$d;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/t;)V

    .line 444
    :cond_5
    if-nez p1, :cond_6

    iget-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->x:Z

    if-nez v0, :cond_6

    .line 445
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->P()V

    .line 447
    :cond_6
    return-void

    .line 392
    :cond_7
    new-instance v0, Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/model/util/FriendshipCache;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    goto/16 :goto_0

    .line 401
    :cond_8
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->n:Ljava/util/HashMap;

    goto/16 :goto_1

    .line 419
    :cond_9
    iput v4, p0, Lcom/twitter/app/users/UsersFragment;->u:I

    .line 420
    invoke-virtual {v0}, Lcom/twitter/app/users/g;->k()Lcom/twitter/model/util/FriendshipCache;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    .line 421
    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    if-nez v1, :cond_a

    .line 422
    new-instance v1, Lcom/twitter/model/util/FriendshipCache;

    invoke-direct {v1}, Lcom/twitter/model/util/FriendshipCache;-><init>()V

    iput-object v1, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    .line 424
    :cond_a
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/twitter/app/users/UsersFragment;->l:Ljava/util/HashSet;

    .line 426
    iget v1, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    const/16 v2, 0x12

    if-ne v1, v2, :cond_b

    .line 427
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/twitter/app/users/UsersFragment;->n:Ljava/util/HashMap;

    .line 430
    :cond_b
    invoke-virtual {v0}, Lcom/twitter/app/users/g;->j()Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->aj:Z

    .line 431
    invoke-virtual {v0}, Lcom/twitter/app/users/g;->E()Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->x:Z

    .line 432
    invoke-virtual {v0}, Lcom/twitter/app/users/g;->K()Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->r:Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    goto :goto_2
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 1031
    packed-switch p1, :pswitch_data_0

    move-object v0, v4

    .line 1046
    :goto_0
    return-object v0

    .line 1033
    :pswitch_0
    sget-object v0, Lcom/twitter/database/schema/a$y;->u:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 1036
    new-instance v0, Lcom/twitter/util/android/d;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lbun;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1039
    :pswitch_1
    new-instance v0, Lcom/twitter/util/android/d;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    .line 1040
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/users/UsersFragment;->z:[Ljava/lang/String;

    const-string/jumbo v4, "(users_friendship IS NULL OR (users_friendship & 1 == 0)) AND user_groups_user_id!=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    iget-wide v6, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    .line 1042
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    iget-object v6, p0, Lcom/twitter/app/users/UsersFragment;->A:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1043
    invoke-virtual {v0, v8}, Lcom/twitter/util/android/d;->a(Z)Lcom/twitter/util/android/d;

    move-result-object v0

    goto :goto_0

    .line 1031
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 848
    invoke-super {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->onDestroy()V

    .line 849
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->al:Lcom/twitter/app/users/UsersFragment$d;

    if-eqz v0, :cond_0

    .line 850
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->S:Lcom/twitter/library/client/p;

    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->al:Lcom/twitter/app/users/UsersFragment$d;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->b(Lcom/twitter/library/service/t;)V

    .line 852
    :cond_0
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 136
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/users/UsersFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1120
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 879
    invoke-super {p0, p1}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 880
    const-string/jumbo v0, "state_dialog_user"

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->e:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 881
    const-string/jumbo v0, "state_dialog_user_position"

    iget v1, p0, Lcom/twitter/app/users/UsersFragment;->i:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 882
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->g:Lcgi;

    if-eqz v0, :cond_0

    .line 883
    const-string/jumbo v0, "state_dialog_pc"

    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->g:Lcgi;

    invoke-static {v1}, Lcgi;->a(Lcgi;)[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 885
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 886
    const-string/jumbo v0, "state_dialog_user_token"

    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 888
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->l:Ljava/util/HashSet;

    if-eqz v0, :cond_2

    .line 889
    const-string/jumbo v0, "state_pending_follows"

    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->l:Ljava/util/HashSet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 891
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->D:[J

    if-eqz v0, :cond_3

    .line 892
    const-string/jumbo v0, "state_user_ids"

    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->D:[J

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 894
    :cond_3
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0}, Lcom/twitter/model/util/FriendshipCache;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 895
    const-string/jumbo v0, "state_friendship_cache"

    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 897
    :cond_4
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->n:Ljava/util/HashMap;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->n:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 898
    const-string/jumbo v0, "state_incoming_friendship_cache"

    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->n:Ljava/util/HashMap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 900
    :cond_5
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_6

    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    const/16 v1, 0x20

    if-ne v0, v1, :cond_7

    .line 902
    :cond_6
    const-string/jumbo v0, "state_fetched_category_users"

    iget-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->v:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 904
    :cond_7
    const-string/jumbo v0, "state_load_flags"

    iget v1, p0, Lcom/twitter/app/users/UsersFragment;->u:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 905
    iget-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->ac:Z

    if-eqz v0, :cond_8

    .line 906
    const-string/jumbo v0, "state_loader_initialized"

    iget-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->ag:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 908
    :cond_8
    const-string/jumbo v0, "state_lookup_complete_pages"

    iget v1, p0, Lcom/twitter/app/users/UsersFragment;->J:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 909
    const-string/jumbo v0, "state_lookup_complete"

    iget-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->K:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 910
    const-string/jumbo v0, "state_reverse_lookup_complete"

    iget-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->aa:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 911
    const-string/jumbo v0, "state_wtf_complete"

    iget-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->ab:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 912
    const-string/jumbo v0, "state_completed_components"

    iget v1, p0, Lcom/twitter/app/users/UsersFragment;->ah:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 913
    const-string/jumbo v0, "state_total_progress_components"

    iget v1, p0, Lcom/twitter/app/users/UsersFragment;->ai:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 914
    const-string/jumbo v0, "state_hide_contacts_import_cta"

    iget-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->aj:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 915
    const-string/jumbo v0, "is_hidden"

    iget-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->x:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 916
    const-string/jumbo v0, "fast_follow_visible"

    iget-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->aq:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 917
    const-string/jumbo v0, "filtered_follow_visible"

    iget-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->ar:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 918
    const-string/jumbo v0, "data_was_fetched"

    iget-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->af:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 919
    const-string/jumbo v0, "checked_users"

    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->r:Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 920
    return-void
.end method

.method public onStop()V
    .locals 12

    .prologue
    .line 856
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 857
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 858
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/lang/Long;

    .line 859
    new-instance v0, Lbig;

    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->T:Landroid/content/Context;

    iget-wide v3, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    iget v5, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    iget-wide v6, p0, Lcom/twitter/app/users/UsersFragment;->j:J

    .line 861
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, Lbig;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JIJJLjava/lang/Integer;)V

    .line 862
    const/16 v1, 0x12

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v3}, Lcom/twitter/app/users/UsersFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto :goto_0

    .line 865
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 867
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 868
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UsersAdapter;

    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->f()V

    .line 870
    :cond_2
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    invoke-static {v0}, Lcom/twitter/app/users/UsersFragment;->c(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 871
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->aS()V

    .line 874
    :cond_3
    invoke-super {p0}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->onStop()V

    .line 875
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 513
    invoke-super {p0, p1, p2}, Lcom/twitter/android/widget/ScrollingHeaderListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 514
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    .line 515
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setHeaderDividersEnabled(Z)V

    .line 516
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->u()Landroid/view/ViewGroup;

    move-result-object v1

    const-string/jumbo v2, "UsersFragment"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 517
    return-void
.end method

.method protected p(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1895
    iget-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->ag:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/twitter/app/users/UsersFragment;->ac:Z

    if-eqz v1, :cond_0

    const/16 v1, 0xc8

    if-ne p1, v1, :cond_0

    .line 1896
    iput-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->ag:Z

    .line 1897
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->p()V

    .line 1900
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q(I)V
    .locals 0

    .prologue
    .line 1962
    iput p1, p0, Lcom/twitter/app/users/UsersFragment;->ai:I

    .line 1963
    return-void
.end method

.method r(I)Lcom/twitter/android/UsersAdapter;
    .locals 6
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 2414
    new-instance v0, Lcom/twitter/android/UsersAdapter;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v4, p0, Lcom/twitter/app/users/UsersFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    iget-object v5, p0, Lcom/twitter/app/users/UsersFragment;->r:Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    move v2, p1

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/UsersAdapter;-><init>(Landroid/content/Context;ILcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/android/UsersAdapter$CheckboxConfig;)V

    .line 2416
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->Q()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/UsersAdapter;->e(Z)V

    .line 2417
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->aO()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/UsersAdapter;->f(Z)V

    .line 2418
    return-object v0
.end method

.method r()Lcom/twitter/library/client/Session;
    .locals 4

    .prologue
    .line 317
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    .line 318
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->s()Lcom/twitter/app/users/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/app/users/g;->J()J

    move-result-wide v2

    .line 317
    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/client/v;->a(J)Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method public s()Lcom/twitter/app/users/g;
    .locals 1

    .prologue
    .line 328
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/users/g;->a(Landroid/os/Bundle;)Lcom/twitter/app/users/g;

    move-result-object v0

    return-object v0
.end method

.method protected t()V
    .locals 2

    .prologue
    .line 450
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    const/16 v1, 0x1c

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->G()I

    move-result v0

    if-nez v0, :cond_0

    .line 452
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->q(I)V

    .line 454
    :cond_0
    return-void
.end method

.method protected u()Landroid/view/ViewGroup;
    .locals 4

    .prologue
    .line 521
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 522
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 524
    return-object v0
.end method

.method protected v()Z
    .locals 1

    .prologue
    .line 555
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/users/UsersFragment;->U:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->s()Lcom/twitter/app/users/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/users/g;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected w()I
    .locals 6
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 560
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 561
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 562
    iget v1, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    sparse-switch v1, :sswitch_data_0

    .line 574
    :cond_0
    :goto_0
    return v0

    .line 564
    :sswitch_0
    const v0, 0x7f02034b

    goto :goto_0

    .line 567
    :sswitch_1
    const v0, 0x7f0200b0

    goto :goto_0

    .line 562
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x12 -> :sswitch_1
    .end sparse-switch
.end method

.method protected x()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 578
    sget-object v0, Lbun;->a:[Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->z:[Ljava/lang/String;

    .line 579
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getChoiceMode()I

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "LOWER(users_username) ASC"

    :goto_0
    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->A:Ljava/lang/String;

    .line 583
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    packed-switch v0, :pswitch_data_0

    .line 707
    :pswitch_0
    sget-object v0, Lcom/twitter/database/schema/a$y;->k:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    .line 711
    :cond_0
    :goto_1
    return-void

    .line 579
    :cond_1
    const-string/jumbo v0, "_id ASC"

    goto :goto_0

    .line 585
    :pswitch_1
    sget-object v0, Lcom/twitter/database/schema/a$y;->h:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    .line 586
    sget-object v0, Lbun;->b:[Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->z:[Ljava/lang/String;

    goto :goto_1

    .line 590
    :pswitch_2
    sget-object v0, Lcom/twitter/database/schema/a$y;->i:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    .line 591
    sget-object v0, Lbun;->b:[Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->z:[Ljava/lang/String;

    goto :goto_1

    .line 595
    :pswitch_3
    sget-object v0, Lcom/twitter/database/schema/a$y;->j:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    .line 596
    sget-object v0, Lbun;->b:[Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->z:[Ljava/lang/String;

    goto :goto_1

    .line 600
    :pswitch_4
    sget-object v0, Lcom/twitter/database/schema/a$y;->d:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    goto :goto_1

    .line 604
    :pswitch_5
    sget-object v0, Lcom/twitter/database/schema/a$y;->e:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    goto :goto_1

    .line 608
    :pswitch_6
    sget-object v0, Lcom/twitter/database/schema/a$y;->f:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    goto :goto_1

    .line 612
    :pswitch_7
    sget-object v0, Lcom/twitter/database/schema/a$y;->g:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    goto :goto_1

    .line 616
    :pswitch_8
    sget-object v0, Lcom/twitter/database/schema/a$y;->w:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    .line 617
    sget-object v0, Lbun;->b:[Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->z:[Ljava/lang/String;

    goto :goto_1

    .line 621
    :pswitch_9
    iget-wide v0, p0, Lcom/twitter/app/users/UsersFragment;->j:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 622
    sget-object v0, Lcom/twitter/database/schema/a$y;->c:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    goto/16 :goto_1

    .line 627
    :pswitch_a
    sget-object v0, Lcom/twitter/database/schema/a$y;->o:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    goto/16 :goto_1

    .line 631
    :pswitch_b
    sget-object v0, Lcom/twitter/database/schema/a$y;->b:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    goto/16 :goto_1

    .line 635
    :pswitch_c
    sget-object v0, Lcom/twitter/database/schema/a$y;->l:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    .line 636
    const-string/jumbo v0, "(users_friendship & 1) ASC, LOWER(users_name) ASC"

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->A:Ljava/lang/String;

    goto/16 :goto_1

    .line 640
    :pswitch_d
    sget-object v0, Lcom/twitter/database/schema/a$y;->v:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    .line 641
    const-string/jumbo v0, "LOWER(TRIM(users_name)) COLLATE UNICODE"

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->A:Ljava/lang/String;

    goto/16 :goto_1

    .line 645
    :pswitch_e
    sget-object v0, Lcom/twitter/database/schema/a$y;->p:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    goto/16 :goto_1

    .line 649
    :pswitch_f
    sget-object v0, Lcom/twitter/database/schema/a$y;->q:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    goto/16 :goto_1

    .line 653
    :pswitch_10
    sget-object v0, Lcom/twitter/database/schema/a$y;->s:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    .line 654
    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    goto/16 :goto_1

    .line 658
    :pswitch_11
    sget-object v0, Lcom/twitter/database/schema/a$y;->r:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    goto/16 :goto_1

    .line 662
    :pswitch_12
    sget-object v0, Lcom/twitter/database/schema/a$y;->t:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    goto/16 :goto_1

    .line 666
    :pswitch_13
    sget-object v0, Lcom/twitter/database/schema/a$y;->u:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    .line 667
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->s()Lcom/twitter/app/users/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/users/g;->F()Ljava/lang/String;

    move-result-object v0

    .line 668
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 669
    const-string/jumbo v1, "CASE users_username WHEN \"%s\" THEN -1 ELSE LOWER(users_username) END ASC"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->A:Ljava/lang/String;

    goto/16 :goto_1

    .line 672
    :cond_2
    const-string/jumbo v0, "LOWER(users_username) ASC"

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->A:Ljava/lang/String;

    goto/16 :goto_1

    .line 677
    :pswitch_14
    sget-object v0, Lcom/twitter/database/schema/a$y;->x:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    .line 679
    sget-object v0, Lbun;->b:[Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->z:[Ljava/lang/String;

    goto/16 :goto_1

    .line 683
    :pswitch_15
    sget-object v0, Lcom/twitter/database/schema/a$y;->m:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    .line 684
    sget-object v0, Lbun;->b:[Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->z:[Ljava/lang/String;

    .line 685
    const-string/jumbo v0, "users_friendship_time DESC"

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->A:Ljava/lang/String;

    goto/16 :goto_1

    .line 689
    :pswitch_16
    sget-object v0, Lcom/twitter/database/schema/a$y;->z:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    .line 690
    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 692
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "ownerId"

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    .line 693
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 694
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    goto/16 :goto_1

    .line 698
    :pswitch_17
    sget-object v0, Lcom/twitter/database/schema/a$y;->A:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    goto/16 :goto_1

    .line 702
    :pswitch_18
    sget-object v0, Lcom/twitter/database/schema/a$y;->B:Landroid/net/Uri;

    .line 703
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 702
    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    goto/16 :goto_1

    .line 583
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_9
        :pswitch_b
        :pswitch_a
        :pswitch_c
        :pswitch_e
        :pswitch_0
        :pswitch_8
        :pswitch_11
        :pswitch_12
        :pswitch_f
        :pswitch_0
        :pswitch_0
        :pswitch_15
        :pswitch_0
        :pswitch_13
        :pswitch_0
        :pswitch_14
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_d
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_16
        :pswitch_18
        :pswitch_0
        :pswitch_17
        :pswitch_10
    .end packed-switch
.end method

.method protected y()V
    .locals 3

    .prologue
    .line 714
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->x()V

    .line 715
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 716
    iget-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "limit"

    iget v2, p0, Lcom/twitter/app/users/UsersFragment;->c:I

    .line 717
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 716
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 717
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->y:Landroid/net/Uri;

    .line 721
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 722
    const v0, 0x7f0200b0

    .line 727
    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/app/users/UsersFragment;->l(I)V

    .line 728
    return-void

    .line 724
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersFragment;->w()I

    move-result v0

    goto :goto_0
.end method

.method protected z()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 1052
    iget v0, p0, Lcom/twitter/app/users/UsersFragment;->t:I

    packed-switch v0, :pswitch_data_0

    .line 1067
    iput-object v1, p0, Lcom/twitter/app/users/UsersFragment;->B:Ljava/lang/String;

    .line 1068
    iput-object v1, p0, Lcom/twitter/app/users/UsersFragment;->C:[Ljava/lang/String;

    .line 1072
    :goto_0
    return-void

    .line 1054
    :pswitch_0
    const-string/jumbo v0, "(users_friendship IS NULL OR (users_friendship & 1 == 0)) AND user_groups_user_id!=?"

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->B:Ljava/lang/String;

    .line 1055
    new-array v0, v2, [Ljava/lang/String;

    iget-wide v2, p0, Lcom/twitter/app/users/UsersFragment;->a_:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->C:[Ljava/lang/String;

    goto :goto_0

    .line 1059
    :pswitch_1
    const-string/jumbo v0, "user_groups_type=? AND user_groups_tag=?"

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->B:Ljava/lang/String;

    .line 1060
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x6

    .line 1061
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/twitter/app/users/UsersFragment;->E:Ljava/lang/String;

    .line 1062
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/twitter/app/users/UsersFragment;->C:[Ljava/lang/String;

    goto :goto_0

    .line 1052
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
