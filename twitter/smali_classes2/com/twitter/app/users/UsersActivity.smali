.class public Lcom/twitter/app/users/UsersActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/users/UsersActivity$a;
    }
.end annotation


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method

.method static a(I)I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 147
    sparse-switch p0, :sswitch_data_0

    .line 195
    const v0, 0x7f0a09f4

    .line 199
    :goto_0
    return v0

    .line 149
    :sswitch_0
    const v0, 0x7f0a06c9

    .line 150
    goto :goto_0

    .line 153
    :sswitch_1
    const v0, 0x7f0a06c8

    .line 154
    goto :goto_0

    .line 157
    :sswitch_2
    const v0, 0x7f0a020e

    .line 158
    goto :goto_0

    .line 161
    :sswitch_3
    const v0, 0x7f0a0a3e

    .line 162
    goto :goto_0

    .line 166
    :sswitch_4
    const v0, 0x7f0a0483

    .line 167
    goto :goto_0

    .line 170
    :sswitch_5
    const v0, 0x7f0a07b0

    .line 171
    goto :goto_0

    .line 174
    :sswitch_6
    const v0, 0x7f0a03a7

    .line 175
    goto :goto_0

    .line 178
    :sswitch_7
    const v0, 0x7f0a00a7

    .line 179
    goto :goto_0

    .line 182
    :sswitch_8
    const v0, 0x7f0a05b7

    .line 183
    goto :goto_0

    .line 186
    :sswitch_9
    const v0, 0x7f0a074c

    .line 187
    goto :goto_0

    .line 191
    :sswitch_a
    const v0, 0x7f0a0218

    .line 192
    goto :goto_0

    .line 147
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_7
        0x7 -> :sswitch_2
        0xa -> :sswitch_3
        0xb -> :sswitch_4
        0xc -> :sswitch_5
        0x12 -> :sswitch_6
        0x1a -> :sswitch_8
        0x29 -> :sswitch_9
        0x2a -> :sswitch_a
        0x2c -> :sswitch_a
        0x2d -> :sswitch_4
    .end sparse-switch
.end method

.method private i()V
    .locals 4

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f1302e4

    .line 210
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/UsersFragment;

    .line 211
    invoke-virtual {v0}, Lcom/twitter/app/users/UsersFragment;->A()Lcom/twitter/model/util/FriendshipCache;

    move-result-object v1

    .line 212
    invoke-virtual {v0}, Lcom/twitter/app/users/UsersFragment;->C()Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    move-result-object v0

    .line 213
    new-instance v2, Lcom/twitter/app/users/f;

    invoke-direct {v2}, Lcom/twitter/app/users/f;-><init>()V

    .line 214
    invoke-virtual {v1}, Lcom/twitter/model/util/FriendshipCache;->a()Z

    move-result v3

    if-nez v3, :cond_0

    .line 215
    invoke-virtual {v2, v1}, Lcom/twitter/app/users/f;->a(Lcom/twitter/model/util/FriendshipCache;)Lcom/twitter/app/users/f;

    .line 217
    :cond_0
    if-eqz v0, :cond_1

    .line 218
    invoke-virtual {v2, v0}, Lcom/twitter/app/users/f;->a(Lcom/twitter/android/UsersAdapter$CheckboxConfig;)Lcom/twitter/app/users/f;

    .line 221
    :cond_1
    invoke-virtual {v1}, Lcom/twitter/model/util/FriendshipCache;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v0, :cond_3

    .line 222
    :cond_2
    const/4 v0, -0x1

    invoke-virtual {v2, p0}, Lcom/twitter/app/users/f;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/app/users/UsersActivity;->setResult(ILandroid/content/Intent;)V

    .line 224
    :cond_3
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lcom/twitter/app/users/UsersActivity$a;

    invoke-direct {v0, p0, p2}, Lcom/twitter/app/users/UsersActivity$a;-><init>(Lcom/twitter/app/common/base/TwitterFragmentActivity;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    return-object v0
.end method

.method public a(Lcmm;)Z
    .locals 2

    .prologue
    .line 135
    invoke-interface {p1}, Lcmm;->a()I

    move-result v0

    const v1, 0x7f130164

    if-ne v0, v1, :cond_0

    .line 136
    invoke-direct {p0}, Lcom/twitter/app/users/UsersActivity;->i()V

    .line 137
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersActivity;->finish()V

    .line 138
    const/4 v0, 0x1

    .line 140
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmm;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/twitter/app/users/UsersActivity;->a:Z

    if-eqz v0, :cond_0

    .line 126
    const v0, 0x7f14002c

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 127
    const/4 v0, 0x1

    .line 129
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmr;)Z

    move-result v0

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 51
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 52
    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 53
    check-cast p2, Lcom/twitter/app/users/UsersActivity$a;

    .line 54
    invoke-static {v3}, Lcom/twitter/app/users/f;->a(Landroid/content/Intent;)Lcom/twitter/app/users/f;

    move-result-object v5

    .line 55
    invoke-virtual {v5}, Lcom/twitter/app/users/f;->e()Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    move-result-object v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/app/users/UsersActivity;->a:Z

    .line 57
    if-nez p1, :cond_2

    .line 59
    invoke-static {v3}, Lcom/twitter/app/users/g$b;->a(Landroid/content/Intent;)Lcom/twitter/app/users/g$b;

    move-result-object v0

    iget-boolean v6, p2, Lcom/twitter/app/users/UsersActivity$a;->j:Z

    .line 60
    invoke-virtual {v0, v6}, Lcom/twitter/app/users/g$b;->e(Z)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$b;

    .line 62
    iget v6, p2, Lcom/twitter/app/users/UsersActivity$a;->k:I

    sparse-switch v6, :sswitch_data_0

    .line 88
    :goto_1
    const-string/jumbo v6, "fast_followers_count"

    const/4 v7, -0x1

    .line 89
    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 88
    invoke-virtual {v0, v6}, Lcom/twitter/app/users/g$b;->a(I)Lcom/twitter/app/users/g$a;

    .line 92
    const-string/jumbo v6, "followers_count"

    .line 93
    invoke-virtual {v3, v6, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 92
    invoke-virtual {v0, v3}, Lcom/twitter/app/users/g$b;->g(I)Lcom/twitter/app/users/g$a;

    .line 96
    invoke-virtual {v5}, Lcom/twitter/app/users/f;->f()Ljava/lang/String;

    move-result-object v3

    .line 97
    if-eqz v4, :cond_0

    if-eqz v3, :cond_0

    .line 98
    invoke-virtual {v0, v3}, Lcom/twitter/app/users/g$b;->a(Ljava/lang/String;)Lcom/twitter/app/users/g$a;

    .line 101
    :cond_0
    new-instance v3, Lcom/twitter/app/users/UsersFragment;

    invoke-direct {v3}, Lcom/twitter/app/users/UsersFragment;-><init>()V

    .line 102
    invoke-static {}, Lcom/twitter/android/al;->a()Z

    move-result v4

    if-nez v4, :cond_1

    move v2, v1

    :cond_1
    invoke-virtual {v3, v2}, Lcom/twitter/app/users/UsersFragment;->d(Z)V

    .line 103
    invoke-virtual {v0}, Lcom/twitter/app/users/g$b;->a()Lcom/twitter/app/users/g;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/twitter/app/users/UsersFragment;->a(Lcom/twitter/app/common/base/b;)V

    .line 104
    invoke-virtual {p0}, Lcom/twitter/app/users/UsersActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v2, 0x7f1302e4

    .line 106
    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 107
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 111
    :cond_2
    iget v0, p2, Lcom/twitter/app/users/UsersActivity$a;->k:I

    .line 112
    const/4 v2, 0x6

    if-ne v0, v2, :cond_5

    .line 113
    invoke-virtual {v5}, Lcom/twitter/app/users/f;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/twitter/app/users/UsersActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 117
    :goto_2
    if-ne v0, v1, :cond_3

    .line 118
    invoke-virtual {v5}, Lcom/twitter/app/users/f;->a()J

    move-result-wide v0

    .line 119
    invoke-static {p0, v0, v1}, Lcom/twitter/android/util/i;->a(Lcom/twitter/app/common/base/TwitterFragmentActivity;J)V

    .line 121
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 55
    goto :goto_0

    .line 64
    :sswitch_0
    const v6, 0x7f0a037b

    invoke-virtual {v0, v6}, Lcom/twitter/app/users/g$b;->c(I)Lcom/twitter/app/common/list/i$a;

    goto :goto_1

    .line 68
    :sswitch_1
    const v6, 0x7f0a036d

    invoke-virtual {v0, v6}, Lcom/twitter/app/users/g$b;->c(I)Lcom/twitter/app/common/list/i$a;

    goto :goto_1

    .line 72
    :sswitch_2
    const v6, 0x7f0a0370

    invoke-virtual {v0, v6}, Lcom/twitter/app/users/g$b;->c(I)Lcom/twitter/app/common/list/i$a;

    goto :goto_1

    .line 76
    :sswitch_3
    const v6, 0x7f0a036b

    invoke-virtual {v0, v6}, Lcom/twitter/app/users/g$b;->c(I)Lcom/twitter/app/common/list/i$a;

    goto :goto_1

    .line 80
    :sswitch_4
    const v6, 0x7f0a0372

    invoke-virtual {v0, v6}, Lcom/twitter/app/users/g$b;->c(I)Lcom/twitter/app/common/list/i$a;

    goto/16 :goto_1

    .line 115
    :cond_5
    invoke-static {v0}, Lcom/twitter/app/users/UsersActivity;->a(I)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/twitter/app/users/UsersActivity;->setTitle(I)V

    goto :goto_2

    .line 62
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_3
        0x7 -> :sswitch_1
        0xa -> :sswitch_0
        0x12 -> :sswitch_2
        0x1a -> :sswitch_4
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 204
    invoke-direct {p0}, Lcom/twitter/app/users/UsersActivity;->i()V

    .line 205
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onBackPressed()V

    .line 206
    return-void
.end method
