.class final Lcom/twitter/app/users/FollowActivity$a$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/users/FollowActivity$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/app/users/FollowActivity$a;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:I
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field

.field private c:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 294
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 295
    const-string/jumbo v0, "unknown"

    iput-object v0, p0, Lcom/twitter/app/users/FollowActivity$a$a;->a:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/users/FollowActivity$a$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/twitter/app/users/FollowActivity$a$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/app/users/FollowActivity$a$a;)I
    .locals 1

    .prologue
    .line 294
    iget v0, p0, Lcom/twitter/app/users/FollowActivity$a$a;->b:I

    return v0
.end method

.method static synthetic c(Lcom/twitter/app/users/FollowActivity$a$a;)Z
    .locals 1

    .prologue
    .line 294
    iget-boolean v0, p0, Lcom/twitter/app/users/FollowActivity$a$a;->c:Z

    return v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/twitter/app/users/FollowActivity$a$a;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Lcom/twitter/app/users/FollowActivity$a$a;
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    .line 307
    iput p1, p0, Lcom/twitter/app/users/FollowActivity$a$a;->b:I

    .line 308
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/app/users/FollowActivity$a$a;
    .locals 0

    .prologue
    .line 301
    iput-object p1, p0, Lcom/twitter/app/users/FollowActivity$a$a;->a:Ljava/lang/String;

    .line 302
    return-object p0
.end method

.method public a(Z)Lcom/twitter/app/users/FollowActivity$a$a;
    .locals 0

    .prologue
    .line 313
    iput-boolean p1, p0, Lcom/twitter/app/users/FollowActivity$a$a;->c:Z

    .line 314
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 294
    invoke-virtual {p0}, Lcom/twitter/app/users/FollowActivity$a$a;->e()Lcom/twitter/app/users/FollowActivity$a;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/app/users/FollowActivity$a;
    .locals 2

    .prologue
    .line 325
    new-instance v0, Lcom/twitter/app/users/FollowActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/app/users/FollowActivity$a;-><init>(Lcom/twitter/app/users/FollowActivity$a$a;Lcom/twitter/app/users/FollowActivity$1;)V

    return-object v0
.end method
