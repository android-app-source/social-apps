.class public abstract Lcom/twitter/app/users/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/twitter/library/client/s;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/twitter/android/UsersAdapter;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/twitter/library/client/s;"
    }
.end annotation


# instance fields
.field protected final a:J

.field protected final b:Landroid/support/v4/app/FragmentActivity;

.field protected final c:Lcom/twitter/android/UsersAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field protected final d:Lcom/twitter/model/util/FriendshipCache;

.field protected final e:I

.field private final f:Lcom/twitter/library/client/p;

.field private g:Landroid/net/Uri;

.field private h:[Ljava/lang/String;

.field private i:Z


# direct methods
.method protected constructor <init>(Landroid/support/v4/app/FragmentActivity;ILcom/twitter/model/util/FriendshipCache;JLcom/twitter/ui/user/BaseUserView$a;I)V
    .locals 2
    .param p2    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentActivity;",
            "I",
            "Lcom/twitter/model/util/FriendshipCache;",
            "J",
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserView;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/twitter/app/users/b;->b:Landroid/support/v4/app/FragmentActivity;

    .line 53
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/b;->f:Lcom/twitter/library/client/p;

    .line 54
    iput-wide p4, p0, Lcom/twitter/app/users/b;->a:J

    .line 55
    invoke-virtual {p0, p2, p6}, Lcom/twitter/app/users/b;->b(ILcom/twitter/ui/user/BaseUserView$a;)Lcom/twitter/android/UsersAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/b;->c:Lcom/twitter/android/UsersAdapter;

    .line 56
    iput-object p3, p0, Lcom/twitter/app/users/b;->d:Lcom/twitter/model/util/FriendshipCache;

    .line 57
    iput p7, p0, Lcom/twitter/app/users/b;->e:I

    .line 58
    invoke-direct {p0}, Lcom/twitter/app/users/b;->d()V

    .line 59
    return-void
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 134
    iget-object v0, p0, Lcom/twitter/app/users/b;->f:Lcom/twitter/library/client/p;

    invoke-virtual {p0, p1}, Lcom/twitter/app/users/b;->a(I)Lbhz;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;ILcom/twitter/library/client/s;)Z

    .line 135
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 62
    sget-object v0, Lbun;->b:[Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/app/users/b;->h:[Ljava/lang/String;

    .line 63
    iget v0, p0, Lcom/twitter/app/users/b;->e:I

    packed-switch v0, :pswitch_data_0

    .line 74
    sget-object v0, Lcom/twitter/database/schema/a$y;->k:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/b;->a:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "ownerId"

    iget-wide v2, p0, Lcom/twitter/app/users/b;->a:J

    .line 76
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 75
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/b;->g:Landroid/net/Uri;

    .line 81
    :goto_0
    return-void

    .line 65
    :pswitch_0
    sget-object v0, Lcom/twitter/database/schema/a$y;->z:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/twitter/app/users/b;->a:J

    .line 66
    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "ownerId"

    iget-wide v2, p0, Lcom/twitter/app/users/b;->a:J

    .line 69
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 70
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/b;->g:Landroid/net/Uri;

    goto :goto_0

    .line 63
    nop

    :pswitch_data_0
    .packed-switch 0x29
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method abstract a(I)Lbhz;
.end method

.method public abstract a()V
.end method

.method public a(ILandroid/os/Bundle;Lcom/twitter/library/service/s;)V
    .locals 0

    .prologue
    .line 153
    return-void
.end method

.method public a(ILcom/twitter/library/service/s;)V
    .locals 0

    .prologue
    .line 149
    return-void
.end method

.method public abstract a(Landroid/os/Bundle;)V
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 107
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-nez v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/twitter/app/users/b;->c:Lcom/twitter/android/UsersAdapter;

    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->k()Lcjt;

    move-result-object v0

    new-instance v1, Lcbe;

    invoke-direct {v1, p2}, Lcbe;-><init>(Landroid/database/Cursor;)V

    invoke-interface {v0, v1}, Lcjt;->a(Lcbi;)Lcbi;

    .line 109
    iget-object v0, p0, Lcom/twitter/app/users/b;->c:Lcom/twitter/android/UsersAdapter;

    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->notifyDataSetChanged()V

    .line 111
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 118
    if-eqz p1, :cond_0

    .line 119
    invoke-direct {p0, v2}, Lcom/twitter/app/users/b;->b(I)V

    .line 123
    :goto_0
    return-void

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/users/b;->b:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0
.end method

.method protected b()I
    .locals 1

    .prologue
    .line 90
    const/16 v0, 0x32

    return v0
.end method

.method public abstract b(ILcom/twitter/ui/user/BaseUserView$a;)Lcom/twitter/android/UsersAdapter;
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserView;",
            ">;)TT;"
        }
    .end annotation
.end method

.method public b(ILcom/twitter/library/service/s;)V
    .locals 3

    .prologue
    .line 139
    if-nez p1, :cond_0

    .line 140
    iget-boolean v0, p0, Lcom/twitter/app/users/b;->i:Z

    if-nez v0, :cond_0

    .line 141
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/users/b;->i:Z

    .line 142
    iget-object v0, p0, Lcom/twitter/app/users/b;->b:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 145
    :cond_0
    return-void
.end method

.method public abstract b(Landroid/os/Bundle;)V
.end method

.method public c()Lcom/twitter/android/UsersAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 127
    iget-object v0, p0, Lcom/twitter/app/users/b;->c:Lcom/twitter/android/UsersAdapter;

    return-object v0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 99
    if-nez p1, :cond_0

    .line 100
    new-instance v0, Lcom/twitter/util/android/d;

    iget-object v1, p0, Lcom/twitter/app/users/b;->b:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/twitter/app/users/b;->g:Landroid/net/Uri;

    iget-object v3, p0, Lcom/twitter/app/users/b;->h:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v4

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 32
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/users/b;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 115
    return-void
.end method
