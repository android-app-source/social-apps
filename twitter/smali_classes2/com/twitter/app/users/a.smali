.class public Lcom/twitter/app/users/a;
.super Lcom/twitter/app/users/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/users/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/users/b",
        "<",
        "Lcom/twitter/android/addressbook/a;",
        ">;"
    }
.end annotation


# instance fields
.field private final f:Lcom/twitter/app/users/a$a;

.field private final g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/util/FriendshipCache;JLcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/app/users/a$a;Lcom/twitter/android/addressbook/a$a;Ljava/lang/String;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentActivity;",
            "Lcom/twitter/model/util/FriendshipCache;",
            "J",
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserView;",
            ">;",
            "Lcom/twitter/app/users/a$a;",
            "Lcom/twitter/android/addressbook/a$a;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 35
    const v4, 0x7f0200b0

    const/16 v9, 0x29

    move-object v2, p0

    move-object v3, p1

    move-object v5, p2

    move-wide v6, p3

    move-object/from16 v8, p5

    invoke-direct/range {v2 .. v9}, Lcom/twitter/app/users/b;-><init>(Landroid/support/v4/app/FragmentActivity;ILcom/twitter/model/util/FriendshipCache;JLcom/twitter/ui/user/BaseUserView$a;I)V

    .line 37
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/twitter/app/users/a;->g:Ljava/lang/String;

    .line 38
    invoke-virtual {p0}, Lcom/twitter/app/users/a;->c()Lcom/twitter/android/UsersAdapter;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/addressbook/a;

    move-object/from16 v0, p7

    invoke-virtual {v2, v0}, Lcom/twitter/android/addressbook/a;->a(Lcom/twitter/android/addressbook/a$a;)V

    .line 39
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/twitter/app/users/a;->f:Lcom/twitter/app/users/a$a;

    .line 40
    return-void
.end method


# virtual methods
.method a(I)Lbhz;
    .locals 4

    .prologue
    .line 62
    new-instance v0, Lbhz;

    iget-object v1, p0, Lcom/twitter/app/users/a;->b:Landroid/support/v4/app/FragmentActivity;

    .line 63
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget v3, p0, Lcom/twitter/app/users/a;->e:I

    invoke-direct {v0, v1, v2, v3}, Lbhz;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;I)V

    .line 64
    iput p1, v0, Lbhz;->c:I

    .line 65
    invoke-virtual {p0}, Lcom/twitter/app/users/a;->b()I

    move-result v1

    iput v1, v0, Lbhz;->j:I

    .line 68
    return-object v0
.end method

.method public a(ILcom/twitter/ui/user/BaseUserView$a;)Lcom/twitter/android/addressbook/a;
    .locals 9
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/twitter/ui/user/BaseUserView$a",
            "<",
            "Lcom/twitter/ui/user/UserView;",
            ">;)",
            "Lcom/twitter/android/addressbook/a;"
        }
    .end annotation

    .prologue
    .line 75
    new-instance v0, Lcom/twitter/android/addressbook/a;

    iget-object v1, p0, Lcom/twitter/app/users/a;->b:Landroid/support/v4/app/FragmentActivity;

    iget-object v4, p0, Lcom/twitter/app/users/a;->d:Lcom/twitter/model/util/FriendshipCache;

    const/4 v5, 0x5

    iget-wide v6, p0, Lcom/twitter/app/users/a;->a:J

    iget-object v8, p0, Lcom/twitter/app/users/a;->g:Ljava/lang/String;

    move v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/addressbook/a;-><init>(Landroid/content/Context;ILcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/model/util/FriendshipCache;IJLjava/lang/String;)V

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/app/users/a;->c:Lcom/twitter/android/UsersAdapter;

    check-cast v0, Lcom/twitter/android/addressbook/a;

    invoke-virtual {v0}, Lcom/twitter/android/addressbook/a;->a()V

    .line 57
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 44
    const-string/jumbo v1, "state_should_show_retry_prompt"

    invoke-virtual {p0}, Lcom/twitter/app/users/a;->c()Lcom/twitter/android/UsersAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/addressbook/a;

    invoke-virtual {v0}, Lcom/twitter/android/addressbook/a;->h()Z

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 45
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 81
    invoke-super {p0, p1, p2}, Lcom/twitter/app/users/b;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    .line 83
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/twitter/app/users/a;->f:Lcom/twitter/app/users/a$a;

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/twitter/app/users/a$a;->c(I)V

    .line 86
    :cond_0
    return-void
.end method

.method public synthetic b(ILcom/twitter/ui/user/BaseUserView$a;)Lcom/twitter/android/UsersAdapter;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/users/a;->a(ILcom/twitter/ui/user/BaseUserView$a;)Lcom/twitter/android/addressbook/a;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 49
    const-string/jumbo v0, "state_should_show_retry_prompt"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    invoke-virtual {p0}, Lcom/twitter/app/users/a;->c()Lcom/twitter/android/UsersAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/addressbook/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/addressbook/a;->a(Z)V

    .line 52
    :cond_0
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 21
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/users/a;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method
