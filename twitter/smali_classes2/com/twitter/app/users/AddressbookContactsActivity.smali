.class public Lcom/twitter/app/users/AddressbookContactsActivity;
.super Lcom/twitter/app/users/FollowActivity;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/users/AddressbookContactsActivity$a;
    }
.end annotation


# instance fields
.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/twitter/app/users/FollowActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/users/AddressbookContactsActivity;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/twitter/app/users/AddressbookContactsActivity;->t()V

    return-void
.end method

.method private s()Z
    .locals 3

    .prologue
    .line 151
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 152
    invoke-static {p0}, Lbmp;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0, v0, v1}, Lbmp;->b(Landroid/content/Context;J)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private t()V
    .locals 2

    .prologue
    .line 156
    const v0, 0x7f0a004a

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 157
    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsActivity;->finish()V

    .line 158
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    .line 38
    invoke-super {p0, p1, p2}, Lcom/twitter/app/users/FollowActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    .line 39
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->b(I)V

    .line 40
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->b(Z)V

    .line 41
    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)Lcom/twitter/app/users/FollowActivity$a;
    .locals 3

    .prologue
    .line 47
    new-instance v0, Lcom/twitter/app/users/FollowActivity$a$a;

    invoke-direct {v0}, Lcom/twitter/app/users/FollowActivity$a$a;-><init>()V

    .line 48
    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "scribe_page_term"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/users/FollowActivity$a$a;->a(Ljava/lang/String;)Lcom/twitter/app/users/FollowActivity$a$a;

    move-result-object v0

    const v1, 0x7f0400fa

    .line 49
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/FollowActivity$a$a;->a(I)Lcom/twitter/app/users/FollowActivity$a$a;

    move-result-object v0

    const/4 v1, 0x1

    .line 50
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/FollowActivity$a$a;->a(Z)Lcom/twitter/app/users/FollowActivity$a$a;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lcom/twitter/app/users/FollowActivity$a$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/FollowActivity$a;

    .line 47
    return-object v0
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 56
    invoke-direct {p0}, Lcom/twitter/app/users/AddressbookContactsActivity;->s()Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsActivity;->finish()V

    .line 71
    :goto_0
    return-void

    .line 61
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/twitter/app/users/FollowActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 63
    if-eqz p1, :cond_1

    const-string/jumbo v0, "has_scribed_people_discovery_all_contacts_impression"

    .line 64
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/app/users/AddressbookContactsActivity;->d:Z

    .line 66
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 68
    new-instance v2, Lbsv;

    new-instance v3, Lcom/twitter/util/a;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v3, p0, v4, v5}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    invoke-direct {v2, v3}, Lbsv;-><init>(Lcom/twitter/util/a;)V

    .line 69
    invoke-virtual {v2, v1}, Lbsv;->a(Z)V

    .line 70
    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsActivity;->n()V

    goto :goto_0

    .line 64
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected i()Lcom/twitter/app/users/UsersFragment;
    .locals 3

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/users/g$b;->a(Landroid/content/Intent;)Lcom/twitter/app/users/g$b;

    move-result-object v0

    .line 77
    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "scribe_page_term"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/users/g$b;->b(Ljava/lang/String;)Lcom/twitter/app/users/g$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$b;

    .line 78
    invoke-virtual {v0}, Lcom/twitter/app/users/g$b;->a()Lcom/twitter/app/users/g;

    move-result-object v0

    .line 80
    new-instance v1, Lcom/twitter/app/users/AddressbookContactsFragment;

    invoke-direct {v1}, Lcom/twitter/app/users/AddressbookContactsFragment;-><init>()V

    .line 81
    invoke-virtual {v1, v0}, Lcom/twitter/app/users/AddressbookContactsFragment;->a(Lcom/twitter/app/common/base/b;)V

    .line 82
    return-object v1
.end method

.method public j()V
    .locals 5

    .prologue
    .line 96
    invoke-super {p0}, Lcom/twitter/app/users/FollowActivity;->j()V

    .line 98
    iget-object v0, p0, Lcom/twitter/app/users/AddressbookContactsActivity;->a:Lcom/twitter/app/users/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/app/users/UsersFragment;->aI()I

    move-result v0

    .line 99
    iget-boolean v1, p0, Lcom/twitter/app/users/AddressbookContactsActivity;->d:Z

    if-nez v1, :cond_0

    if-lez v0, :cond_0

    .line 103
    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "scribe_page_term"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 104
    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 103
    invoke-static {v1, v2, v3}, Lcom/twitter/android/people/ai;->a(Ljava/lang/String;J)Lcom/twitter/android/people/ai;

    move-result-object v1

    .line 105
    const-string/jumbo v2, "all_contacts"

    const-string/jumbo v3, ""

    const-string/jumbo v4, "impression"

    .line 106
    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/people/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    int-to-long v2, v0

    .line 108
    invoke-virtual {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 105
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/users/AddressbookContactsActivity;->d:Z

    .line 111
    :cond_0
    return-void
.end method

.method protected l()Z
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x1

    return v0
.end method

.method protected n()V
    .locals 1

    .prologue
    .line 127
    const v0, 0x7f0a020c

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/AddressbookContactsActivity;->setTitle(I)V

    .line 128
    return-void
.end method

.method protected o()Landroid/content/BroadcastReceiver;
    .locals 1

    .prologue
    .line 133
    new-instance v0, Lcom/twitter/app/users/AddressbookContactsActivity$a;

    invoke-direct {v0, p0}, Lcom/twitter/app/users/AddressbookContactsActivity$a;-><init>(Lcom/twitter/app/users/AddressbookContactsActivity;)V

    return-object v0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 87
    invoke-super {p0}, Lcom/twitter/app/users/FollowActivity;->onResume()V

    .line 88
    invoke-direct {p0}, Lcom/twitter/app/users/AddressbookContactsActivity;->s()Z

    move-result v0

    if-nez v0, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsActivity;->finish()V

    .line 91
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 120
    invoke-super {p0, p1}, Lcom/twitter/app/users/FollowActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 121
    const-string/jumbo v0, "has_scribed_people_discovery_all_contacts_impression"

    iget-boolean v1, p0, Lcom/twitter/app/users/AddressbookContactsActivity;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 123
    return-void
.end method

.method protected q()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 139
    invoke-super {p0}, Lcom/twitter/app/users/FollowActivity;->q()Landroid/content/IntentFilter;

    move-result-object v0

    .line 140
    const-string/jumbo v1, "live_sync_opt_in_failure_broadcast"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 141
    return-object v0
.end method
