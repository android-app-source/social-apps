.class public Lcom/twitter/app/users/e;
.super Lckb$a;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/ui/user/UserSocialView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p2}, Lckb$a;-><init>(Landroid/view/View;)V

    .line 28
    const v0, 0x7f1303ca

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/user/UserSocialView;

    iput-object v0, p0, Lcom/twitter/app/users/e;->a:Lcom/twitter/ui/user/UserSocialView;

    .line 29
    iget-object v0, p0, Lcom/twitter/app/users/e;->a:Lcom/twitter/ui/user/UserSocialView;

    invoke-static {p1, v0}, Lcom/twitter/android/widget/WhoToFollowUsersView;->a(Landroid/content/Context;Lcom/twitter/ui/user/UserSocialView;)V

    .line 30
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/twitter/app/users/e;
    .locals 3

    .prologue
    .line 22
    const v0, 0x7f040135

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 23
    new-instance v1, Lcom/twitter/app/users/e;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/twitter/app/users/e;-><init>(Landroid/content/Context;Landroid/view/View;)V

    return-object v1
.end method


# virtual methods
.method public a()Lcom/twitter/ui/user/UserSocialView;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/app/users/e;->a:Lcom/twitter/ui/user/UserSocialView;

    return-object v0
.end method
