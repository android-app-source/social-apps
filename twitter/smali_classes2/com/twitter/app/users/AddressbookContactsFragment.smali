.class public Lcom/twitter/app/users/AddressbookContactsFragment;
.super Lcom/twitter/app/users/InjectableUsersFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/addressbook/a$a;
.implements Lcom/twitter/app/users/a$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/users/InjectableUsersFragment",
        "<",
        "Lcom/twitter/app/users/a;",
        ">;",
        "Lcom/twitter/android/addressbook/a$a;",
        "Lcom/twitter/app/users/a$a;"
    }
.end annotation


# instance fields
.field private D:Lcom/twitter/android/people/ai;

.field private E:Z

.field private F:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/twitter/app/users/InjectableUsersFragment;-><init>()V

    return-void
.end method

.method private b(II)I
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/twitter/app/users/AddressbookContactsFragment;->a:Lcom/twitter/app/users/b;

    check-cast v0, Lcom/twitter/app/users/a;

    invoke-virtual {v0}, Lcom/twitter/app/users/a;->c()Lcom/twitter/android/UsersAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/addressbook/a;

    invoke-virtual {v0, p1}, Lcom/twitter/android/addressbook/a;->a(I)I

    move-result v1

    .line 181
    const/16 v0, 0x27

    if-ne p2, v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/twitter/app/users/AddressbookContactsFragment;->a:Lcom/twitter/app/users/b;

    check-cast v0, Lcom/twitter/app/users/a;

    invoke-virtual {v0}, Lcom/twitter/app/users/a;->c()Lcom/twitter/android/UsersAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/addressbook/a;

    invoke-virtual {v0}, Lcom/twitter/android/addressbook/a;->getCount()I

    move-result v0

    .line 184
    sub-int v0, v1, v0

    .line 187
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 159
    const-string/jumbo v0, "%s:address_book:all_contacts"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 164
    const-string/jumbo v0, "%s:address_book:active_contacts"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a(JILcgi;Ljava/lang/String;Ljava/lang/String;I)Lcom/twitter/analytics/feature/model/ClientEventLog;
    .locals 7

    .prologue
    .line 171
    invoke-direct {p0, p3, p7}, Lcom/twitter/app/users/AddressbookContactsFragment;->b(II)I

    move-result v6

    .line 172
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsFragment;->r()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    move-wide v2, p1

    move-object v4, p4

    move-object v5, p5

    .line 173
    invoke-static/range {v1 .. v6}, Lcom/twitter/library/scribe/c;->b(Lcom/twitter/analytics/feature/model/ClientEventLog;JLcgi;Ljava/lang/String;I)V

    .line 174
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p6, v0, v2

    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 175
    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 174
    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/AddressbookContactsFragment;->n(I)Z

    .line 121
    return-void
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0, p1}, Lcom/twitter/app/users/InjectableUsersFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 66
    const v0, 0x7f0a0050

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->a(I)Lcom/twitter/app/common/list/l$d;

    .line 67
    const v0, 0x7f0a004f

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->b(I)Lcom/twitter/app/common/list/l$d;

    .line 68
    const v0, 0x7f04002e

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->f(I)Lcom/twitter/app/common/list/l$d;

    .line 69
    return-void
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 141
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/users/InjectableUsersFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 142
    packed-switch p2, :pswitch_data_0

    .line 155
    :goto_0
    return-void

    .line 144
    :pswitch_0
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 145
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    iput-boolean v1, p0, Lcom/twitter/app/users/AddressbookContactsFragment;->F:Z

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/users/AddressbookContactsFragment;->a:Lcom/twitter/app/users/b;

    check-cast v0, Lcom/twitter/app/users/a;

    invoke-virtual {v0}, Lcom/twitter/app/users/a;->c()Lcom/twitter/android/UsersAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/addressbook/a;

    iget-boolean v2, p0, Lcom/twitter/app/users/AddressbookContactsFragment;->F:Z

    if-nez v2, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/twitter/android/addressbook/a;->a(Z)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 142
    :pswitch_data_0
    .packed-switch 0x1b
        :pswitch_0
    .end packed-switch
.end method

.method public b()V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0}, Lcom/twitter/app/users/InjectableUsersFragment;->b()V

    .line 74
    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UsersAdapter;

    invoke-virtual {v0}, Lcom/twitter/android/UsersAdapter;->notifyDataSetChanged()V

    .line 75
    return-void
.end method

.method public c(I)V
    .locals 4

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/twitter/app/users/AddressbookContactsFragment;->E:Z

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/twitter/app/users/AddressbookContactsFragment;->D:Lcom/twitter/android/people/ai;

    const-string/jumbo v1, "active_contacts"

    const-string/jumbo v2, ""

    const-string/jumbo v3, "impression"

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/people/ai;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    int-to-long v2, p1

    .line 96
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b(J)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 94
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 97
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/users/AddressbookContactsFragment;->E:Z

    .line 99
    :cond_0
    return-void
.end method

.method e(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsFragment;->s()Lcom/twitter/app/users/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/users/g;->I()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/users/AddressbookContactsFragment;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method i(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsFragment;->s()Lcom/twitter/app/users/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/users/g;->I()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/users/AddressbookContactsFragment;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method j(I)V
    .locals 1

    .prologue
    .line 113
    if-nez p1, :cond_0

    .line 114
    iget-object v0, p0, Lcom/twitter/app/users/AddressbookContactsFragment;->D:Lcom/twitter/android/people/ai;

    invoke-virtual {v0}, Lcom/twitter/android/people/ai;->b()V

    .line 116
    :cond_0
    return-void
.end method

.method k(I)Lcom/twitter/android/ag;
    .locals 10
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 126
    new-instance v0, Lcom/twitter/android/ag;

    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v4, p0, Lcom/twitter/app/users/AddressbookContactsFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    .line 127
    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsFragment;->C()Lcom/twitter/android/UsersAdapter$CheckboxConfig;

    move-result-object v5

    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsFragment;->Q()Z

    move-result v6

    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsFragment;->aO()Z

    move-result v7

    .line 128
    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsFragment;->s()Lcom/twitter/app/users/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/app/users/g;->I()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/app/users/AddressbookContactsFragment;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/twitter/app/users/AddressbookContactsFragment$2;

    invoke-direct {v9, p0}, Lcom/twitter/app/users/AddressbookContactsFragment$2;-><init>(Lcom/twitter/app/users/AddressbookContactsFragment;)V

    move v2, p1

    move-object v3, p0

    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/ag;-><init>(Landroid/content/Context;ILcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/android/UsersAdapter$CheckboxConfig;ZZLjava/lang/String;Lcom/twitter/android/ag$b;)V

    .line 126
    return-object v0
.end method

.method public k()Lcom/twitter/app/users/a;
    .locals 10

    .prologue
    .line 80
    new-instance v1, Lcom/twitter/app/users/a;

    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/users/AddressbookContactsFragment;->m:Lcom/twitter/model/util/FriendshipCache;

    iget-wide v4, p0, Lcom/twitter/app/users/AddressbookContactsFragment;->a_:J

    .line 81
    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsFragment;->s()Lcom/twitter/app/users/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/users/g;->I()Ljava/lang/String;

    move-result-object v9

    move-object v6, p0

    move-object v7, p0

    move-object v8, p0

    invoke-direct/range {v1 .. v9}, Lcom/twitter/app/users/a;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/twitter/model/util/FriendshipCache;JLcom/twitter/ui/user/BaseUserView$a;Lcom/twitter/app/users/a$a;Lcom/twitter/android/addressbook/a$a;Ljava/lang/String;)V

    .line 80
    return-object v1
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/twitter/app/users/InjectableUsersFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 49
    if-eqz p1, :cond_0

    .line 50
    const-string/jumbo v0, "state_has_scribed_impression"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/users/AddressbookContactsFragment;->E:Z

    .line 53
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsFragment;->s()Lcom/twitter/app/users/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/users/g;->I()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 52
    invoke-static {v0, v2, v3}, Lcom/twitter/android/people/ai;->a(Ljava/lang/String;J)Lcom/twitter/android/people/ai;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/users/AddressbookContactsFragment;->D:Lcom/twitter/android/people/ai;

    .line 55
    new-instance v0, Lcom/twitter/app/users/AddressbookContactsFragment$1;

    invoke-direct {v0, p0}, Lcom/twitter/app/users/AddressbookContactsFragment$1;-><init>(Lcom/twitter/app/users/AddressbookContactsFragment;)V

    invoke-virtual {p0, v0}, Lcom/twitter/app/users/AddressbookContactsFragment;->a(Lcno$c;)Lcom/twitter/app/common/list/TwitterListFragment;

    .line 61
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 86
    invoke-super {p0, p1}, Lcom/twitter/app/users/InjectableUsersFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 87
    const-string/jumbo v0, "state_has_scribed_impression"

    iget-boolean v1, p0, Lcom/twitter/app/users/AddressbookContactsFragment;->E:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 88
    return-void
.end method

.method public synthetic q()Lcom/twitter/app/users/b;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/twitter/app/users/AddressbookContactsFragment;->k()Lcom/twitter/app/users/a;

    move-result-object v0

    return-object v0
.end method
