.class public Lcom/twitter/app/users/URTUsersActivity;
.super Lcom/twitter/android/ListFragmentActivity;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/users/URTUsersActivity$a;,
        Lcom/twitter/app/users/URTUsersActivity$b;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/twitter/android/ListFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Intent;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/android/ListFragmentActivity$a;
    .locals 4

    .prologue
    .line 35
    new-instance v1, Lcom/twitter/app/users/MomentLikesFragment;

    invoke-direct {v1}, Lcom/twitter/app/users/MomentLikesFragment;-><init>()V

    .line 36
    new-instance v0, Lcom/twitter/app/users/MomentLikesFragment$a$a;

    .line 37
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/twitter/app/users/MomentLikesFragment$a$a;-><init>(Landroid/os/Bundle;)V

    .line 38
    invoke-static {p1}, Lcom/twitter/app/users/URTUsersActivity$a;->a(Landroid/content/Intent;)Lcom/twitter/app/users/URTUsersActivity$a;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/app/users/URTUsersActivity$a;->a:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/app/users/MomentLikesFragment$a$a;->c(Ljava/lang/String;)Lcom/twitter/app/common/timeline/c$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/MomentLikesFragment$a$a;

    const v2, 0x7f0a0484

    .line 39
    invoke-virtual {v0, v2}, Lcom/twitter/app/users/MomentLikesFragment$a$a;->c(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/MomentLikesFragment$a$a;

    .line 40
    invoke-virtual {v0}, Lcom/twitter/app/users/MomentLikesFragment$a$a;->a()Lcom/twitter/app/users/MomentLikesFragment$a;

    move-result-object v0

    .line 36
    invoke-virtual {v1, v0}, Lcom/twitter/app/users/MomentLikesFragment;->a(Lcom/twitter/app/common/base/b;)V

    .line 41
    new-instance v0, Lcom/twitter/android/ListFragmentActivity$a;

    invoke-direct {v0, v1}, Lcom/twitter/android/ListFragmentActivity$a;-><init>(Lcom/twitter/app/common/list/TwitterListFragment;)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    .line 26
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ListFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    .line 27
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 28
    return-object v0
.end method

.method protected a(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/twitter/app/users/URTUsersActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0483

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
