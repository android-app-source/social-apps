.class public Lcom/twitter/app/users/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/ui/user/BaseUserView$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/twitter/ui/user/BaseUserView$a",
        "<",
        "Lcom/twitter/ui/user/UserView;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/library/client/p;

.field private final b:Lcom/twitter/library/client/Session;

.field private final c:Lcom/twitter/model/util/FriendshipCache;

.field private final d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;


# direct methods
.method public constructor <init>(Lcom/twitter/library/client/p;Lcom/twitter/library/client/Session;Lcom/twitter/model/util/FriendshipCache;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/twitter/app/users/c;->a:Lcom/twitter/library/client/p;

    .line 41
    iput-object p2, p0, Lcom/twitter/app/users/c;->b:Lcom/twitter/library/client/Session;

    .line 42
    iput-object p3, p0, Lcom/twitter/app/users/c;->c:Lcom/twitter/model/util/FriendshipCache;

    .line 43
    iput-object p4, p0, Lcom/twitter/app/users/c;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 44
    return-void
.end method

.method private a(Lcom/twitter/ui/user/UserView;JLjava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 98
    iget-object v0, p0, Lcom/twitter/app/users/c;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    new-instance v1, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    .line 99
    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 100
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1, p2, p3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 101
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x2

    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getScribeComponent()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x3

    aput-object p4, v2, v0

    const/4 v0, 0x4

    aput-object p5, v2, v0

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/app/users/c;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 102
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 103
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getScribeItem()Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 100
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 104
    return-void
.end method


# virtual methods
.method a(Landroid/content/Context;J)V
    .locals 8
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 86
    new-instance v1, Lbhs;

    iget-object v3, p0, Lcom/twitter/app/users/c;->b:Lcom/twitter/library/client/Session;

    const/4 v6, 0x0

    move-object v2, p1

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Lbhs;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    .line 87
    iget-object v0, p0, Lcom/twitter/app/users/c;->a:Lcom/twitter/library/client/p;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 88
    return-void
.end method

.method protected a(Landroid/content/Context;JLjava/lang/String;Lcom/twitter/model/timeline/r;)V
    .locals 8

    .prologue
    .line 81
    const/4 v5, 0x0

    iget-object v6, p0, Lcom/twitter/app/users/c;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v7, p5

    invoke-static/range {v1 .. v7}, Lcom/twitter/android/ProfileActivity;->a(Landroid/content/Context;JLjava/lang/String;Lcgi;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/model/timeline/r;)V

    .line 82
    return-void
.end method

.method public bridge synthetic a(Lcom/twitter/ui/user/BaseUserView;JII)V
    .locals 6

    .prologue
    .line 27
    move-object v1, p1

    check-cast v1, Lcom/twitter/ui/user/UserView;

    move-object v0, p0

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/app/users/c;->a(Lcom/twitter/ui/user/UserView;JII)V

    return-void
.end method

.method public a(Lcom/twitter/ui/user/UserView;JII)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 52
    iget-object v0, p0, Lcom/twitter/app/users/c;->c:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/model/util/FriendshipCache;->j(J)Ljava/lang/Integer;

    move-result-object v0

    .line 53
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/model/core/g;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/twitter/app/users/c;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const-string/jumbo v5, "unfollow"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/app/users/c;->a(Lcom/twitter/ui/user/UserView;JLjava/lang/String;Ljava/lang/String;)V

    .line 55
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/twitter/app/users/c;->a(Landroid/content/Context;J)V

    .line 56
    iget-object v0, p0, Lcom/twitter/app/users/c;->c:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/model/util/FriendshipCache;->c(J)V

    .line 62
    :goto_0
    return-void

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/users/c;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const-string/jumbo v5, "follow"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/app/users/c;->a(Lcom/twitter/ui/user/UserView;JLjava/lang/String;Ljava/lang/String;)V

    .line 59
    invoke-virtual {p1}, Lcom/twitter/ui/user/UserView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/twitter/app/users/c;->b(Landroid/content/Context;J)V

    .line 60
    iget-object v0, p0, Lcom/twitter/app/users/c;->c:Lcom/twitter/model/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/model/util/FriendshipCache;->b(J)V

    goto :goto_0
.end method

.method b(Landroid/content/Context;J)V
    .locals 8
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 92
    new-instance v1, Lbhq;

    iget-object v3, p0, Lcom/twitter/app/users/c;->b:Lcom/twitter/library/client/Session;

    const/4 v6, 0x0

    move-object v2, p1

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Lbhq;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcgi;)V

    .line 93
    iget-object v0, p0, Lcom/twitter/app/users/c;->a:Lcom/twitter/library/client/p;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 94
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    .line 70
    move-object v1, p1

    check-cast v1, Lcom/twitter/ui/user/UserView;

    .line 71
    invoke-virtual {v1}, Lcom/twitter/ui/user/UserView;->getUserId()J

    move-result-wide v8

    .line 72
    invoke-virtual {v1}, Lcom/twitter/ui/user/UserView;->getUserName()Ljava/lang/String;

    move-result-object v7

    .line 73
    invoke-virtual {v1}, Lcom/twitter/ui/user/UserView;->getScribeItem()Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 74
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->av:Lcom/twitter/model/timeline/r;

    move-object v6, v0

    .line 75
    :goto_0
    iget-object v0, p0, Lcom/twitter/app/users/c;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const-string/jumbo v4, "user"

    const-string/jumbo v5, "profile_click"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/app/users/c;->a(Lcom/twitter/ui/user/UserView;JLjava/lang/String;Ljava/lang/String;)V

    .line 76
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object v0, p0

    move-wide v2, v8

    move-object v4, v7

    move-object v5, v6

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/app/users/c;->a(Landroid/content/Context;JLjava/lang/String;Lcom/twitter/model/timeline/r;)V

    .line 77
    return-void

    .line 74
    :cond_0
    const/4 v0, 0x0

    move-object v6, v0

    goto :goto_0
.end method
