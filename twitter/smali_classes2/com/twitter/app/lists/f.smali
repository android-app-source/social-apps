.class public Lcom/twitter/app/lists/f;
.super Lcom/twitter/app/common/list/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/lists/f$a;
    }
.end annotation


# direct methods
.method protected constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/twitter/app/common/list/i;-><init>(Landroid/os/Bundle;)V

    .line 30
    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lcom/twitter/app/lists/f;
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/twitter/app/lists/f;

    invoke-direct {v0, p0}, Lcom/twitter/app/lists/f;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/app/lists/f$a;
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/twitter/app/lists/f$a;

    invoke-direct {v0, p0}, Lcom/twitter/app/lists/f$a;-><init>(Lcom/twitter/app/lists/f;)V

    return-object v0
.end method

.method public b()Z
    .locals 3

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/app/lists/f;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "is_me"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 3

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/app/lists/f;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "force_restart"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/app/lists/f;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "screen_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Z
    .locals 3

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/app/lists/f;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "is_pick_list"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public synthetic h()Lcom/twitter/app/common/list/i$a;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/twitter/app/lists/f;->a()Lcom/twitter/app/lists/f$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic i()Lcom/twitter/app/common/base/b$a;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/twitter/app/lists/f;->a()Lcom/twitter/app/lists/f$a;

    move-result-object v0

    return-object v0
.end method
