.class public Lcom/twitter/app/lists/ListTabActivity;
.super Lcom/twitter/android/AbsTabbedPageFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/AbsPagesAdapter$a;
.implements Lcom/twitter/app/common/dialog/b$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/lists/ListTabActivity$a;
    }
.end annotation


# static fields
.field public static final c:[Ljava/lang/String;

.field private static final q:Landroid/net/Uri;

.field private static final r:Landroid/net/Uri;


# instance fields
.field d:J

.field e:J

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field h:J

.field i:Ljava/lang/String;

.field j:I

.field k:Ljava/lang/String;

.field l:Ljava/lang/String;

.field m:Z

.field n:Lcom/twitter/util/a;

.field o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;"
        }
    .end annotation
.end field

.field p:I

.field private s:Lcom/twitter/app/lists/ListTabActivity$a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 97
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "ev_content"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/app/lists/ListTabActivity;->c:[Ljava/lang/String;

    .line 109
    const-string/jumbo v0, "lists://tweets"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/app/lists/ListTabActivity;->q:Landroid/net/Uri;

    .line 110
    const-string/jumbo v0, "lists://members"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/app/lists/ListTabActivity;->r:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/twitter/android/AbsTabbedPageFragmentActivity;-><init>()V

    .line 134
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/app/lists/ListTabActivity;->j:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/lists/ListTabActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 188
    const v0, 0x7f1302e3

    invoke-virtual {p0, v0}, Lcom/twitter/app/lists/ListTabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/DockLayout;

    .line 189
    const v1, 0x7f1303ef

    invoke-virtual {p0, v1}, Lcom/twitter/app/lists/ListTabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    .line 191
    if-eqz p1, :cond_0

    .line 192
    invoke-virtual {v0, v3}, Lcom/twitter/internal/android/widget/DockLayout;->setVisibility(I)V

    .line 193
    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 198
    :goto_0
    return-void

    .line 195
    :cond_0
    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/DockLayout;->setVisibility(I)V

    .line 196
    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/app/lists/ListTabActivity;Lcom/twitter/library/service/s;I)Z
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/lists/ListTabActivity;->b(Lcom/twitter/library/service/s;I)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/app/lists/ListTabActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private l()V
    .locals 3

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->j()V

    .line 202
    iget-object v0, p0, Lcom/twitter/app/lists/ListTabActivity;->o:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/twitter/app/lists/ListTabActivity;->a(Ljava/util/List;)V

    .line 203
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->j_()Lcom/twitter/android/AbsPagesAdapter;

    move-result-object v0

    .line 204
    if-eqz v0, :cond_0

    .line 205
    invoke-virtual {v0, p0}, Lcom/twitter/android/AbsPagesAdapter;->a(Lcom/twitter/android/AbsPagesAdapter$a;)V

    .line 207
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 208
    const-string/jumbo v1, "tab"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "list_members"

    const-string/jumbo v2, "tab"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 209
    iget-object v0, p0, Lcom/twitter/app/lists/ListTabActivity;->a:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 211
    :cond_1
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    .line 149
    invoke-super {p0, p1, p2}, Lcom/twitter/android/AbsTabbedPageFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    .line 150
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(Z)V

    .line 151
    const v0, 0x7f040174

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 152
    return-object p2
.end method

.method public a(I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 512
    if-nez p1, :cond_1

    .line 513
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 514
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "list:tweets:::impression"

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 515
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->P()Lcom/twitter/android/search/SearchSuggestionController;

    move-result-object v0

    const-string/jumbo v1, "tweets"

    invoke-virtual {v0, v1}, Lcom/twitter/android/search/SearchSuggestionController;->b(Ljava/lang/String;)Lcom/twitter/android/search/SearchSuggestionController;

    .line 521
    :cond_0
    :goto_0
    return-void

    .line 516
    :cond_1
    if-ne p1, v4, :cond_0

    .line 517
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 518
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "list:members:::impression"

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 519
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->P()Lcom/twitter/android/search/SearchSuggestionController;

    move-result-object v0

    const-string/jumbo v1, "members"

    invoke-virtual {v0, v1}, Lcom/twitter/android/search/SearchSuggestionController;->b(Ljava/lang/String;)Lcom/twitter/android/search/SearchSuggestionController;

    goto :goto_0
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 498
    if-ne p2, v8, :cond_0

    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    .line 499
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    .line 500
    new-instance v1, Lbcv;

    .line 501
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-wide v4, p0, Lcom/twitter/app/lists/ListTabActivity;->d:J

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lbcv;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJ)V

    .line 502
    invoke-virtual {p0, v1, v8}, Lcom/twitter/app/lists/ListTabActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 503
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v8, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "me:lists:list::delete"

    aput-object v3, v1, v2

    .line 504
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 503
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 505
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->finish()V

    .line 507
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/service/s;I)V
    .locals 8

    .prologue
    const/16 v3, 0x194

    const/16 v2, 0xc8

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 339
    invoke-super {p0, p1, p2}, Lcom/twitter/android/AbsTabbedPageFragmentActivity;->a(Lcom/twitter/library/service/s;I)V

    .line 340
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 341
    packed-switch p2, :pswitch_data_0

    .line 418
    :cond_0
    :goto_0
    return-void

    .line 343
    :pswitch_0
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 344
    const v0, 0x7f0a0495

    invoke-static {p0, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 345
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 351
    :pswitch_1
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 352
    iput v6, p0, Lcom/twitter/app/lists/ListTabActivity;->j:I

    .line 353
    const v0, 0x7f0a049f

    .line 354
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 355
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "list::::subscribe"

    aput-object v3, v2, v7

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 359
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->F()Lcmt;

    move-result-object v1

    invoke-virtual {v1}, Lcmt;->h()V

    .line 360
    invoke-static {p0, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 357
    :cond_1
    const v0, 0x7f0a0491

    goto :goto_1

    .line 365
    :pswitch_2
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 366
    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/app/lists/ListTabActivity;->j:I

    .line 367
    const v0, 0x7f0a04a1

    .line 368
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "list::::unsubscribe"

    aput-object v3, v2, v7

    .line 369
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    .line 368
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 373
    :goto_2
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->F()Lcmt;

    move-result-object v1

    invoke-virtual {v1}, Lcmt;->h()V

    .line 374
    invoke-static {p0, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 371
    :cond_2
    const v0, 0x7f0a049e

    goto :goto_2

    .line 378
    :pswitch_3
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v1

    if-eq v1, v2, :cond_3

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 379
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->i()V

    goto/16 :goto_0

    .line 384
    :pswitch_4
    invoke-direct {p0, v7}, Lcom/twitter/app/lists/ListTabActivity;->a(Z)V

    move-object v1, p1

    .line 385
    check-cast v1, Lbdb;

    iget-object v1, v1, Lbdb;->a:Lcom/twitter/model/core/aa;

    .line 386
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v2

    if-eqz v2, :cond_7

    if-eqz v1, :cond_7

    .line 387
    iget-object v0, v1, Lcom/twitter/model/core/aa;->n:Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_4

    .line 388
    iget-object v0, v1, Lcom/twitter/model/core/aa;->n:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/app/lists/ListTabActivity;->e:J

    .line 389
    iget-object v0, v1, Lcom/twitter/model/core/aa;->n:Lcom/twitter/model/core/TwitterUser;

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/app/lists/ListTabActivity;->f:Ljava/lang/String;

    .line 390
    iget-object v0, v1, Lcom/twitter/model/core/aa;->n:Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/lists/ListTabActivity;->g:Ljava/lang/String;

    .line 392
    :cond_4
    invoke-virtual {v1}, Lcom/twitter/model/core/aa;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/app/lists/ListTabActivity;->d:J

    .line 393
    iget-object v0, v1, Lcom/twitter/model/core/aa;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/app/lists/ListTabActivity;->k:Ljava/lang/String;

    .line 394
    iget-object v0, v1, Lcom/twitter/model/core/aa;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/app/lists/ListTabActivity;->i:Ljava/lang/String;

    .line 395
    iget-object v0, v1, Lcom/twitter/model/core/aa;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/app/lists/ListTabActivity;->l:Ljava/lang/String;

    .line 396
    iget v0, p0, Lcom/twitter/app/lists/ListTabActivity;->p:I

    if-ne v0, v6, :cond_6

    .line 397
    iget-object v1, p0, Lcom/twitter/app/lists/ListTabActivity;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/app/lists/ListTabActivity;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/app/lists/ListTabActivity;->k:Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/app/lists/ListTabActivity;->i:Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/app/lists/ListTabActivity;->l:Ljava/lang/String;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->M()Lcom/twitter/library/service/v;

    move-result-object v0

    .line 399
    if-eqz v0, :cond_5

    iget-wide v0, v0, Lcom/twitter/library/service/v;->c:J

    .line 400
    :goto_3
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v2, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v0, v6, [Ljava/lang/String;

    const-string/jumbo v1, "list::list::share"

    aput-object v1, v0, v7

    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto/16 :goto_0

    .line 399
    :cond_5
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    goto :goto_3

    .line 402
    :cond_6
    invoke-direct {p0}, Lcom/twitter/app/lists/ListTabActivity;->l()V

    .line 403
    iget-object v0, p0, Lcom/twitter/app/lists/ListTabActivity;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 404
    iget-object v0, p0, Lcom/twitter/app/lists/ListTabActivity;->i:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/app/lists/ListTabActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 407
    :cond_7
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 408
    const v0, 0x7f0a048b

    invoke-static {p0, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 409
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 410
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->finish()V

    goto/16 :goto_0

    .line 341
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public a(Lcmm;)Z
    .locals 11

    .prologue
    const/4 v7, 0x0

    const/4 v10, 0x5

    const/4 v6, 0x1

    .line 248
    invoke-interface {p1}, Lcmm;->a()I

    move-result v0

    .line 249
    const v1, 0x7f1308a9

    if-ne v0, v1, :cond_0

    .line 250
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 251
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/app/lists/ListCreateEditActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 252
    const-string/jumbo v2, "list_id"

    iget-wide v4, p0, Lcom/twitter/app/lists/ListTabActivity;->d:J

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 253
    const-string/jumbo v2, "name"

    const-string/jumbo v3, "list_name"

    .line 254
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 253
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 255
    const-string/jumbo v2, "description"

    const-string/jumbo v3, "list_description"

    .line 256
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 255
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 257
    const-string/jumbo v2, "full_name"

    const-string/jumbo v3, "list_fullname"

    .line 258
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 257
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 259
    const-string/jumbo v2, "is_private"

    const-string/jumbo v3, "is_private"

    .line 260
    invoke-virtual {v0, v3, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 259
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 261
    invoke-virtual {p0, v1, v6}, Lcom/twitter/app/lists/ListTabActivity;->startActivityForResult(Landroid/content/Intent;I)V

    move v0, v6

    .line 304
    :goto_0
    return v0

    .line 263
    :cond_0
    const v1, 0x7f1308aa

    if-ne v0, v1, :cond_1

    .line 264
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    invoke-direct {v0, v6}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a0496

    .line 265
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0497

    .line 266
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0a40

    .line 267
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a05e0

    .line 268
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 269
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 270
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    move v0, v6

    .line 271
    goto :goto_0

    .line 272
    :cond_1
    const v1, 0x7f1308ac

    if-ne v0, v1, :cond_3

    .line 273
    new-instance v1, Lbda;

    .line 274
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 275
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/app/lists/ListTabActivity;->h:J

    .line 277
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/twitter/app/lists/ListTabActivity;->d:J

    invoke-direct/range {v1 .. v10}, Lbda;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJJI)V

    .line 280
    const/4 v0, 0x3

    invoke-virtual {p0, v1, v0}, Lcom/twitter/app/lists/ListTabActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 304
    :cond_2
    :goto_1
    invoke-super {p0, p1}, Lcom/twitter/android/AbsTabbedPageFragmentActivity;->a(Lcmm;)Z

    move-result v0

    goto :goto_0

    .line 281
    :cond_3
    const v1, 0x7f1308ab

    if-ne v0, v1, :cond_4

    .line 282
    new-instance v1, Lbct;

    .line 283
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 284
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/app/lists/ListTabActivity;->h:J

    .line 286
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/twitter/app/lists/ListTabActivity;->d:J

    invoke-direct/range {v1 .. v10}, Lbct;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJJI)V

    .line 289
    const/4 v0, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/twitter/app/lists/ListTabActivity;->b(Lcom/twitter/library/service/s;I)Z

    goto :goto_1

    .line 290
    :cond_4
    const v1, 0x7f1308bc

    if-ne v0, v1, :cond_2

    .line 291
    iget-object v0, p0, Lcom/twitter/app/lists/ListTabActivity;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/app/lists/ListTabActivity;->k:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/app/lists/ListTabActivity;->g:Ljava/lang/String;

    .line 292
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 293
    iget-object v1, p0, Lcom/twitter/app/lists/ListTabActivity;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/app/lists/ListTabActivity;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/app/lists/ListTabActivity;->k:Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/app/lists/ListTabActivity;->i:Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/app/lists/ListTabActivity;->l:Ljava/lang/String;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "list::list::share"

    aput-object v2, v1, v7

    .line 296
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 295
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_1

    .line 298
    :cond_5
    iput v6, p0, Lcom/twitter/app/lists/ListTabActivity;->p:I

    .line 299
    new-instance v1, Lbdb;

    .line 300
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/app/lists/ListTabActivity;->d:J

    iget-object v6, p0, Lcom/twitter/app/lists/ListTabActivity;->f:Ljava/lang/String;

    iget-object v7, p0, Lcom/twitter/app/lists/ListTabActivity;->k:Ljava/lang/String;

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lbdb;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;Ljava/lang/String;)V

    .line 299
    invoke-virtual {p0, v1, v10}, Lcom/twitter/app/lists/ListTabActivity;->b(Lcom/twitter/library/service/s;I)Z

    goto/16 :goto_1
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 224
    invoke-super {p0, p1}, Lcom/twitter/android/AbsTabbedPageFragmentActivity;->a(Lcmr;)Z

    .line 225
    const v0, 0x7f140017

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 226
    const v0, 0x7f14002f

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 227
    const/4 v0, 0x1

    return v0
.end method

.method public b(Lcmr;)I
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 233
    invoke-super {p0, p1}, Lcom/twitter/android/AbsTabbedPageFragmentActivity;->b(Lcmr;)I

    .line 234
    invoke-interface {p1}, Lcmr;->k()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 235
    iget-wide v4, p0, Lcom/twitter/app/lists/ListTabActivity;->e:J

    .line 236
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-nez v1, :cond_0

    move v1, v2

    .line 237
    :goto_0
    const v4, 0x7f1308a9

    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v4

    invoke-virtual {v4, v1}, Lazv;->b(Z)Lazv;

    .line 238
    const v4, 0x7f1308aa

    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v4

    invoke-virtual {v4, v1}, Lazv;->b(Z)Lazv;

    .line 239
    const v4, 0x7f1308ab

    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v5

    if-nez v1, :cond_1

    iget v4, p0, Lcom/twitter/app/lists/ListTabActivity;->j:I

    if-ne v4, v8, :cond_1

    move v4, v2

    :goto_1
    invoke-virtual {v5, v4}, Lazv;->b(Z)Lazv;

    .line 240
    const v4, 0x7f1308ac

    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v4

    if-nez v1, :cond_2

    iget v1, p0, Lcom/twitter/app/lists/ListTabActivity;->j:I

    if-ne v1, v2, :cond_2

    move v1, v2

    :goto_2
    invoke-virtual {v4, v1}, Lazv;->b(Z)Lazv;

    .line 241
    const v1, 0x7f1308bc

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/app/lists/ListTabActivity;->m:Z

    if-nez v1, :cond_3

    :goto_3
    invoke-virtual {v0, v2}, Lazv;->b(Z)Lazv;

    .line 242
    return v8

    :cond_0
    move v1, v3

    .line 236
    goto :goto_0

    :cond_1
    move v4, v3

    .line 239
    goto :goto_1

    :cond_2
    move v1, v3

    .line 240
    goto :goto_2

    :cond_3
    move v2, v3

    .line 241
    goto :goto_3
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 157
    invoke-super {p0, p1, p2}, Lcom/twitter/android/AbsTabbedPageFragmentActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 158
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 160
    new-instance v1, Lcom/twitter/app/lists/ListTabActivity$a;

    invoke-direct {v1, p0, p0}, Lcom/twitter/app/lists/ListTabActivity$a;-><init>(Lcom/twitter/app/lists/ListTabActivity;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/twitter/app/lists/ListTabActivity;->s:Lcom/twitter/app/lists/ListTabActivity$a;

    .line 162
    const-string/jumbo v1, "owner_id"

    invoke-virtual {v0, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/app/lists/ListTabActivity;->h:J

    .line 163
    const-string/jumbo v1, "creator_full_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/lists/ListTabActivity;->g:Ljava/lang/String;

    .line 164
    const-string/jumbo v1, "is_private"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/app/lists/ListTabActivity;->m:Z

    .line 165
    const-string/jumbo v1, "screen_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/lists/ListTabActivity;->f:Ljava/lang/String;

    .line 166
    const-string/jumbo v1, "slug"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/lists/ListTabActivity;->k:Ljava/lang/String;

    .line 168
    const-string/jumbo v1, "creator_id"

    invoke-virtual {v0, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/app/lists/ListTabActivity;->e:J

    .line 169
    const-string/jumbo v1, "list_id"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/app/lists/ListTabActivity;->d:J

    .line 171
    iget-wide v2, p0, Lcom/twitter/app/lists/ListTabActivity;->d:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    iget-wide v2, p0, Lcom/twitter/app/lists/ListTabActivity;->e:J

    cmp-long v1, v2, v4

    if-gtz v1, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/twitter/app/lists/ListTabActivity;->f:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/app/lists/ListTabActivity;->k:Ljava/lang/String;

    .line 172
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 173
    new-instance v1, Lbdb;

    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/app/lists/ListTabActivity;->d:J

    iget-object v6, p0, Lcom/twitter/app/lists/ListTabActivity;->f:Ljava/lang/String;

    iget-object v7, p0, Lcom/twitter/app/lists/ListTabActivity;->k:Ljava/lang/String;

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lbdb;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x5

    invoke-virtual {p0, v1, v2}, Lcom/twitter/app/lists/ListTabActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 179
    :goto_0
    const-string/jumbo v1, "list_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/lists/ListTabActivity;->i:Ljava/lang/String;

    .line 180
    iget-object v0, p0, Lcom/twitter/app/lists/ListTabActivity;->i:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 181
    iget-object v0, p0, Lcom/twitter/app/lists/ListTabActivity;->i:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/app/lists/ListTabActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 184
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->P()Lcom/twitter/android/search/SearchSuggestionController;

    move-result-object v0

    const-string/jumbo v1, "list"

    invoke-virtual {v0, v1}, Lcom/twitter/android/search/SearchSuggestionController;->a(Ljava/lang/String;)Lcom/twitter/android/search/SearchSuggestionController;

    .line 185
    return-void

    .line 176
    :cond_2
    invoke-direct {p0}, Lcom/twitter/app/lists/ListTabActivity;->l()V

    goto :goto_0
.end method

.method protected e()Lcom/twitter/util/a;
    .locals 4

    .prologue
    .line 215
    iget-object v0, p0, Lcom/twitter/app/lists/ListTabActivity;->n:Lcom/twitter/util/a;

    if-nez v0, :cond_0

    .line 216
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 217
    new-instance v1, Lcom/twitter/util/a;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const-string/jumbo v0, "lists_prefs"

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    iput-object v1, p0, Lcom/twitter/app/lists/ListTabActivity;->n:Lcom/twitter/util/a;

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/lists/ListTabActivity;->n:Lcom/twitter/util/a;

    return-object v0
.end method

.method i()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 424
    sget-object v0, Lcom/twitter/database/schema/a$n;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iget-wide v4, p0, Lcom/twitter/app/lists/ListTabActivity;->d:J

    .line 425
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "ownerId"

    .line 427
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 426
    invoke-virtual {v0, v1, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 428
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 429
    iget-object v0, p0, Lcom/twitter/app/lists/ListTabActivity;->s:Lcom/twitter/app/lists/ListTabActivity$a;

    const/4 v1, 0x2

    sget-object v4, Lcom/twitter/app/lists/ListTabActivity;->c:[Ljava/lang/String;

    const-string/jumbo v5, "list_mapping_user_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-wide v8, p0, Lcom/twitter/app/lists/ListTabActivity;->h:J

    .line 432
    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    move-object v7, v2

    .line 429
    invoke-virtual/range {v0 .. v7}, Lcom/twitter/app/lists/ListTabActivity$a;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    return-void
.end method

.method j()V
    .locals 14

    .prologue
    const/4 v2, 0x0

    const-wide/16 v12, 0x0

    const/4 v7, 0x0

    const/4 v1, 0x1

    .line 437
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    .line 438
    iget-object v0, p0, Lcom/twitter/app/lists/ListTabActivity;->o:Ljava/util/List;

    if-nez v0, :cond_0

    .line 439
    iget-wide v8, p0, Lcom/twitter/app/lists/ListTabActivity;->h:J

    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v10

    cmp-long v0, v8, v10

    if-nez v0, :cond_2

    move v3, v1

    .line 441
    :goto_0
    new-instance v0, Lcom/twitter/android/timeline/q$a;

    .line 442
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/twitter/android/timeline/q$a;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {p0, v0}, Lcom/twitter/app/lists/ListTabActivity;->a(Lcom/twitter/app/common/list/i$a;)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/q$a;

    iget-wide v8, p0, Lcom/twitter/app/lists/ListTabActivity;->e:J

    .line 443
    invoke-virtual {v0, v8, v9}, Lcom/twitter/android/timeline/q$a;->c(J)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/q$a;

    const v4, 0x7f0a048a

    .line 444
    invoke-virtual {v0, v4}, Lcom/twitter/android/timeline/q$a;->b(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/q$a;

    if-eqz v3, :cond_3

    const v4, 0x7f0a062a

    .line 445
    :goto_1
    invoke-virtual {v0, v4}, Lcom/twitter/android/timeline/q$a;->c(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/q$a;

    const v4, 0x7f130088

    .line 447
    invoke-virtual {v0, v4}, Lcom/twitter/android/timeline/q$a;->d(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/q$a;

    iget-wide v8, p0, Lcom/twitter/app/lists/ListTabActivity;->d:J

    const-wide/16 v10, -0x1

    cmp-long v4, v8, v10

    if-nez v4, :cond_4

    move-object v4, v2

    .line 449
    :goto_2
    invoke-virtual {v0, v4}, Lcom/twitter/android/timeline/q$a;->c(Ljava/lang/String;)Lcom/twitter/app/common/timeline/c$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/q$a;

    .line 450
    invoke-virtual {v0}, Lcom/twitter/android/timeline/q$a;->a()Lcom/twitter/android/timeline/q;

    move-result-object v4

    .line 453
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/users/g$b;->a(Landroid/content/Intent;)Lcom/twitter/app/users/g$b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/lists/ListTabActivity;->a(Lcom/twitter/app/common/list/i$a;)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$b;

    iget-wide v8, p0, Lcom/twitter/app/lists/ListTabActivity;->e:J

    .line 454
    invoke-virtual {v0, v8, v9}, Lcom/twitter/app/users/g$b;->c(J)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$b;

    const v6, 0x7f0a0488

    .line 455
    invoke-virtual {v0, v6}, Lcom/twitter/app/users/g$b;->b(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$b;

    if-eqz v3, :cond_5

    const v3, 0x7f0a0629

    .line 456
    :goto_3
    invoke-virtual {v0, v3}, Lcom/twitter/app/users/g$b;->c(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$b;

    const v3, 0x7f130099

    .line 458
    invoke-virtual {v0, v3}, Lcom/twitter/app/users/g$b;->d(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$b;

    const-string/jumbo v3, "type"

    const/4 v6, 0x4

    .line 459
    invoke-virtual {v5, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/twitter/app/users/g$b;->h(I)Lcom/twitter/app/users/g$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$b;

    iget-wide v8, p0, Lcom/twitter/app/lists/ListTabActivity;->d:J

    .line 461
    invoke-virtual {v0, v8, v9}, Lcom/twitter/app/users/g$b;->a(J)Lcom/twitter/app/users/g$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$b;

    const-string/jumbo v3, "follow"

    .line 462
    invoke-virtual {v5, v3, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/twitter/app/users/g$b;->a(Z)Lcom/twitter/app/users/g$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/g$b;

    .line 463
    invoke-virtual {v0}, Lcom/twitter/app/users/g$b;->a()Lcom/twitter/app/users/g;

    move-result-object v0

    .line 465
    const/4 v3, 0x2

    new-array v3, v3, [Lcom/twitter/library/client/m;

    new-instance v5, Lcom/twitter/library/client/m$a;

    sget-object v6, Lcom/twitter/app/lists/ListTabActivity;->q:Landroid/net/Uri;

    const-class v8, Lcom/twitter/app/lists/ListTimelineFragment;

    invoke-direct {v5, v6, v8}, Lcom/twitter/library/client/m$a;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    .line 467
    invoke-virtual {v5, v4}, Lcom/twitter/library/client/m$a;->a(Lcom/twitter/app/common/base/b;)Lcom/twitter/library/client/m$a;

    move-result-object v4

    const v5, 0x7f0a06ed

    .line 468
    invoke-virtual {p0, v5}, Lcom/twitter/app/lists/ListTabActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/twitter/library/client/m$a;->a(Ljava/lang/CharSequence;)Lcom/twitter/library/client/m$a;

    move-result-object v4

    .line 469
    invoke-virtual {v4}, Lcom/twitter/library/client/m$a;->a()Lcom/twitter/library/client/m;

    move-result-object v4

    aput-object v4, v3, v7

    new-instance v4, Lcom/twitter/library/client/m$a;

    sget-object v5, Lcom/twitter/app/lists/ListTabActivity;->r:Landroid/net/Uri;

    const-class v6, Lcom/twitter/app/users/UsersFragment;

    invoke-direct {v4, v5, v6}, Lcom/twitter/library/client/m$a;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    const v5, 0x7f0a094e

    .line 471
    invoke-virtual {p0, v5}, Lcom/twitter/app/lists/ListTabActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/twitter/library/client/m$a;->a(Ljava/lang/CharSequence;)Lcom/twitter/library/client/m$a;

    move-result-object v4

    .line 472
    invoke-virtual {v4, v0}, Lcom/twitter/library/client/m$a;->a(Lcom/twitter/app/common/base/b;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 473
    invoke-virtual {v0}, Lcom/twitter/library/client/m$a;->a()Lcom/twitter/library/client/m;

    move-result-object v0

    aput-object v0, v3, v1

    .line 465
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/lists/ListTabActivity;->o:Ljava/util/List;

    .line 476
    :cond_0
    iget-wide v4, p0, Lcom/twitter/app/lists/ListTabActivity;->d:J

    cmp-long v0, v4, v12

    if-lez v0, :cond_6

    iget-wide v4, p0, Lcom/twitter/app/lists/ListTabActivity;->h:J

    cmp-long v0, v4, v12

    if-lez v0, :cond_6

    .line 478
    iget-object v0, p0, Lcom/twitter/app/lists/ListTabActivity;->s:Lcom/twitter/app/lists/ListTabActivity$a;

    sget-object v3, Lcom/twitter/database/schema/a$n;->b:Landroid/net/Uri;

    .line 479
    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/app/lists/ListTabActivity;->d:J

    .line 480
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string/jumbo v4, "ownerId"

    .line 482
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v5

    .line 483
    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    .line 482
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    .line 481
    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    .line 484
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/twitter/app/lists/ListTabActivity;->c:[Ljava/lang/String;

    const-string/jumbo v5, "list_mapping_user_id=?"

    new-array v6, v1, [Ljava/lang/String;

    iget-wide v8, p0, Lcom/twitter/app/lists/ListTabActivity;->h:J

    .line 487
    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    move-object v7, v2

    .line 478
    invoke-virtual/range {v0 .. v7}, Lcom/twitter/app/lists/ListTabActivity$a;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    :cond_1
    :goto_4
    return-void

    :cond_2
    move v3, v7

    .line 439
    goto/16 :goto_0

    .line 444
    :cond_3
    const v4, 0x7f0a0489

    goto/16 :goto_1

    .line 447
    :cond_4
    iget-wide v8, p0, Lcom/twitter/app/lists/ListTabActivity;->d:J

    .line 449
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_2

    .line 455
    :cond_5
    const v3, 0x7f0a0487

    goto/16 :goto_3

    .line 489
    :cond_6
    iget-wide v2, p0, Lcom/twitter/app/lists/ListTabActivity;->d:J

    cmp-long v0, v2, v12

    if-lez v0, :cond_7

    iget-wide v2, p0, Lcom/twitter/app/lists/ListTabActivity;->e:J

    cmp-long v0, v2, v12

    if-gtz v0, :cond_1

    .line 490
    :cond_7
    const v0, 0x7f0a049c

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 491
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_4
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 309
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/AbsTabbedPageFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 310
    if-nez p2, :cond_0

    .line 334
    :goto_0
    return-void

    .line 314
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 316
    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 317
    const-string/jumbo v1, "name"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 319
    const-string/jumbo v2, "list_name"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 320
    const-string/jumbo v2, "list_description"

    const-string/jumbo v3, "description"

    .line 321
    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 320
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 322
    const-string/jumbo v2, "list_fullname"

    const-string/jumbo v3, "full_name"

    .line 323
    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 322
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 324
    const-string/jumbo v2, "is_private"

    const-string/jumbo v3, "is_private"

    const/4 v4, 0x0

    .line 325
    invoke-virtual {p3, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 324
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 327
    invoke-virtual {p0, v1}, Lcom/twitter/app/lists/ListTabActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 314
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
