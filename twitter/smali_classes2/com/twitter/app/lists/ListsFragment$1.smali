.class Lcom/twitter/app/lists/ListsFragment$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/lists/ListsFragment;->k()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/lists/ListsFragment;


# direct methods
.method constructor <init>(Lcom/twitter/app/lists/ListsFragment;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/twitter/app/lists/ListsFragment$1;->a:Lcom/twitter/app/lists/ListsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 86
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "me:lists:list:new_list:create"

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>([Ljava/lang/String;)V

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 87
    iget-object v0, p0, Lcom/twitter/app/lists/ListsFragment$1;->a:Lcom/twitter/app/lists/ListsFragment;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/app/lists/ListsFragment$1;->a:Lcom/twitter/app/lists/ListsFragment;

    invoke-virtual {v2}, Lcom/twitter/app/lists/ListsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/app/lists/ListCreateEditActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lcom/twitter/app/lists/ListsFragment;->startActivity(Landroid/content/Intent;)V

    .line 88
    return-void
.end method
