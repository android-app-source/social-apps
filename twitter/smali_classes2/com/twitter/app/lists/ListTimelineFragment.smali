.class public Lcom/twitter/app/lists/ListTimelineFragment;
.super Lcom/twitter/app/common/timeline/TimelineFragment;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;-><init>()V

    return-void
.end method

.method private k(I)J
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    .line 42
    packed-switch p1, :pswitch_data_0

    .line 53
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fetch type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :pswitch_1
    const-wide/16 v0, 0x0

    .line 50
    :goto_0
    return-wide v0

    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTimelineFragment;->s()Lcom/twitter/android/timeline/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/timeline/q;->d()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/twitter/app/lists/ListTimelineFragment;->l(I)J

    move-result-wide v0

    goto :goto_0

    .line 42
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private l(I)J
    .locals 2

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTimelineFragment;->N()Lcom/twitter/android/timeline/bl;

    move-result-object v0

    .line 60
    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {v0, p1}, Lcom/twitter/android/timeline/bl;->j(I)J

    move-result-wide v0

    .line 63
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private m(I)J
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    const-wide/16 v0, 0x0

    .line 74
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTimelineFragment;->s()Lcom/twitter/android/timeline/q;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/timeline/q;->d()I

    move-result v3

    .line 75
    packed-switch p1, :pswitch_data_0

    .line 103
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fetch type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTimelineFragment;->N()Lcom/twitter/android/timeline/bl;

    move-result-object v4

    .line 79
    invoke-static {v4}, Lcbk;->a(Lcbi;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 80
    invoke-virtual {v4}, Lcom/twitter/android/timeline/bl;->be_()I

    move-result v5

    .line 81
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v5, :cond_0

    .line 88
    invoke-virtual {v4, v2}, Lcom/twitter/android/timeline/bl;->g(I)I

    move-result v6

    if-ne v6, v3, :cond_1

    .line 89
    invoke-virtual {v4, v2}, Lcom/twitter/android/timeline/bl;->e(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 90
    invoke-virtual {v4, v2}, Lcom/twitter/android/timeline/bl;->f(I)Z

    move-result v6

    if-nez v6, :cond_1

    .line 91
    invoke-virtual {v4, v2}, Lcom/twitter/android/timeline/bl;->b(I)J

    move-result-wide v0

    .line 100
    :cond_0
    :pswitch_2
    return-wide v0

    .line 81
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 75
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public synthetic H()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTimelineFragment;->s()Lcom/twitter/android/timeline/q;

    move-result-object v0

    return-object v0
.end method

.method public synthetic I()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTimelineFragment;->s()Lcom/twitter/android/timeline/q;

    move-result-object v0

    return-object v0
.end method

.method protected i(I)Lajc$a;
    .locals 4

    .prologue
    .line 29
    invoke-super {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->i(I)Lajc$a;

    move-result-object v0

    .line 30
    invoke-direct {p0, p1}, Lcom/twitter/app/lists/ListTimelineFragment;->m(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lajc$a;->a(J)Lajc$a;

    move-result-object v0

    .line 31
    invoke-direct {p0, p1}, Lcom/twitter/app/lists/ListTimelineFragment;->k(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lajc$a;->b(J)Lajc$a;

    move-result-object v0

    .line 29
    return-object v0
.end method

.method public synthetic k()Lcom/twitter/app/common/timeline/c;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTimelineFragment;->s()Lcom/twitter/android/timeline/q;

    move-result-object v0

    return-object v0
.end method

.method public s()Lcom/twitter/android/timeline/q;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListTimelineFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/timeline/q;->a(Landroid/os/Bundle;)Lcom/twitter/android/timeline/q;

    move-result-object v0

    return-object v0
.end method
