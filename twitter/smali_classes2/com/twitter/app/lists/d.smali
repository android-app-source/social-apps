.class public Lcom/twitter/app/lists/d;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Laqq;

.field private final c:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Laqq;Z)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/twitter/app/lists/d;->a:Landroid/content/Context;

    .line 51
    iput-object p2, p0, Lcom/twitter/app/lists/d;->b:Laqq;

    .line 52
    iput-boolean p3, p0, Lcom/twitter/app/lists/d;->c:Z

    .line 53
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/model/core/aa;)Landroid/content/Intent;
    .locals 6

    .prologue
    .line 98
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/app/lists/ListTabActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 99
    const-string/jumbo v1, "owner_id"

    iget-wide v2, p1, Lcom/twitter/model/core/aa;->f:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "creator_id"

    iget-wide v4, p1, Lcom/twitter/model/core/aa;->g:J

    .line 100
    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "list_id"

    iget-wide v4, p1, Lcom/twitter/model/core/aa;->e:J

    .line 101
    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "list_name"

    iget-object v3, p1, Lcom/twitter/model/core/aa;->h:Ljava/lang/String;

    .line 102
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "list_description"

    iget-object v3, p1, Lcom/twitter/model/core/aa;->j:Ljava/lang/String;

    .line 103
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "list_fullname"

    iget-object v3, p1, Lcom/twitter/model/core/aa;->i:Ljava/lang/String;

    .line 104
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "is_private"

    iget-boolean v3, p1, Lcom/twitter/model/core/aa;->b:Z

    .line 105
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 106
    return-object v0
.end method

.method public static a(Lcom/twitter/app/common/base/TwitterFragmentActivity;)Lcom/twitter/app/lists/d;
    .locals 10

    .prologue
    .line 40
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 41
    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v0

    const-class v1, Laxs;

    invoke-interface {v0, v1}, Lcom/twitter/database/schema/TwitterSchema;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v6

    .line 42
    new-instance v7, Laqq;

    .line 43
    invoke-virtual {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v8

    const v9, 0x4f287

    new-instance v0, Laqr;

    const/4 v1, 0x0

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Laqr;-><init>(ZJJ)V

    move-object v1, v7

    move-object v2, p0

    move-object v3, v8

    move v4, v9

    move-object v5, v0

    invoke-direct/range {v1 .. v6}, Laqq;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;ILaqr;Lcom/twitter/database/model/k;)V

    .line 45
    new-instance v0, Lcom/twitter/app/lists/d;

    invoke-static {}, Lcom/twitter/app/lists/e;->a()Z

    move-result v1

    invoke-direct {v0, p0, v7, v1}, Lcom/twitter/app/lists/d;-><init>(Landroid/content/Context;Laqq;Z)V

    return-object v0
.end method

.method private a(Lcbi;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/model/core/aa;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcmm;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v9, 0x3e8

    const/4 v8, 0x2

    .line 85
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 86
    invoke-virtual {p1}, Lcbi;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/aa;

    .line 87
    new-instance v3, Lcmx;

    iget-object v4, p0, Lcom/twitter/app/lists/d;->a:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/twitter/model/core/aa;->a()J

    move-result-wide v6

    long-to-int v5, v6

    invoke-direct {v3, v4, v5, v8, v9}, Lcmx;-><init>(Landroid/content/Context;III)V

    iget-object v4, v0, Lcom/twitter/model/core/aa;->h:Ljava/lang/String;

    .line 88
    invoke-virtual {v3, v4}, Lcmx;->a(Ljava/lang/CharSequence;)Lcmx;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/app/lists/d;->a:Landroid/content/Context;

    .line 89
    invoke-static {v4, v0}, Lcom/twitter/app/lists/d;->a(Landroid/content/Context;Lcom/twitter/model/core/aa;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcmx;->a(Landroid/content/Intent;)Lcmx;

    move-result-object v0

    .line 87
    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 91
    :cond_0
    new-instance v0, Lcmx;

    iget-object v2, p0, Lcom/twitter/app/lists/d;->a:Landroid/content/Context;

    const v3, 0x7f13002c

    invoke-direct {v0, v2, v3, v8, v9}, Lcmx;-><init>(Landroid/content/Context;III)V

    const v2, 0x7f0a0494

    .line 92
    invoke-virtual {v0, v2}, Lcmx;->a(I)Lcmx;

    move-result-object v0

    .line 91
    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 93
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/app/lists/d;Lcbi;)Ljava/util/List;
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/twitter/app/lists/d;->a(Lcbi;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcmm;)V
    .locals 1

    .prologue
    .line 77
    instance-of v0, p0, Lcmx;

    if-eqz v0, :cond_0

    .line 78
    check-cast p0, Lcmx;

    .line 79
    invoke-virtual {p0}, Lcmx;->j()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcmx;->c(Z)Lcmx;

    .line 81
    :cond_0
    return-void

    .line 79
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/twitter/app/lists/d;->b:Laqq;

    invoke-virtual {v0}, Laqq;->b()V

    .line 74
    return-void
.end method

.method public a(Lcmr;)V
    .locals 3

    .prologue
    .line 56
    const v0, 0x7f1308a0

    invoke-interface {p1, v0}, Lcmr;->b(I)Lcmm;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmm;

    iget-boolean v1, p0, Lcom/twitter/app/lists/d;->c:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-interface {v0, v1}, Lcmm;->f(Z)Lcmm;

    .line 57
    const v0, 0x7f1308a2

    .line 58
    invoke-interface {p1, v0}, Lcmr;->b(I)Lcmm;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmm;

    .line 59
    iget-boolean v1, p0, Lcom/twitter/app/lists/d;->c:Z

    invoke-interface {v0, v1}, Lcmm;->f(Z)Lcmm;

    .line 60
    iget-boolean v1, p0, Lcom/twitter/app/lists/d;->c:Z

    if-eqz v1, :cond_0

    .line 61
    iget-object v1, p0, Lcom/twitter/app/lists/d;->b:Laqq;

    new-instance v2, Lcom/twitter/app/lists/d$1;

    invoke-direct {v2, p0, v0}, Lcom/twitter/app/lists/d$1;-><init>(Lcom/twitter/app/lists/d;Lcmm;)V

    invoke-virtual {v1, v2}, Laqq;->a(Laow;)V

    .line 70
    :cond_0
    return-void

    .line 56
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
