.class public Lcom/twitter/app/lists/ListsActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/lists/g;


# instance fields
.field private a:J

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(ZI)I
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    .line 85
    packed-switch p1, :pswitch_data_0

    .line 100
    const v0, 0x7f0a0326

    :goto_0
    return v0

    .line 87
    :pswitch_0
    if-eqz p0, :cond_0

    .line 88
    const v0, 0x7f0a049d

    goto :goto_0

    .line 90
    :cond_0
    const v0, 0x7f0a06ea

    goto :goto_0

    .line 94
    :pswitch_1
    const v0, 0x7f0a06e9

    goto :goto_0

    .line 97
    :pswitch_2
    const v0, 0x7f0a06e8

    goto :goto_0

    .line 85
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(I)V

    .line 51
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->b(Z)V

    .line 52
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 53
    return-object p2
.end method

.method public a(J)V
    .locals 5

    .prologue
    .line 137
    new-instance v0, Lcom/twitter/app/lists/b;

    iget-wide v2, p0, Lcom/twitter/app/lists/ListsActivity;->a:J

    invoke-direct {v0, p1, p2, v2, v3}, Lcom/twitter/app/lists/b;-><init>(JJ)V

    invoke-virtual {v0}, Lcom/twitter/app/lists/b;->a()Landroid/content/Intent;

    move-result-object v0

    .line 138
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/app/lists/ListsActivity;->setResult(ILandroid/content/Intent;)V

    .line 139
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsActivity;->finish()V

    .line 140
    return-void
.end method

.method public a(Lcmm;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 124
    invoke-interface {p1}, Lcmm;->a()I

    move-result v1

    .line 125
    const v2, 0x7f13088b

    if-ne v1, v2, :cond_0

    .line 126
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/app/lists/ListCreateEditActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/twitter/app/lists/ListsActivity;->startActivity(Landroid/content/Intent;)V

    .line 127
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "me:lists:list:new_list:create"

    aput-object v4, v2, v3

    .line 128
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    .line 127
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 131
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmm;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 115
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmr;)Z

    .line 116
    iget-boolean v0, p0, Lcom/twitter/app/lists/ListsActivity;->b:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "android_fab_new_list_moment_collection_5378"

    invoke-static {v0}, Lcoi;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 117
    const v0, 0x7f140009

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 119
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 8

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 59
    invoke-static {v1}, Lcom/twitter/app/lists/a;->a(Landroid/content/Intent;)Lcom/twitter/app/lists/a;

    move-result-object v2

    .line 60
    invoke-virtual {v2}, Lcom/twitter/app/lists/a;->c()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/app/lists/ListsActivity;->a:J

    .line 61
    invoke-virtual {v2}, Lcom/twitter/app/lists/a;->b()J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/app/lists/ListsActivity;->b:Z

    .line 62
    invoke-virtual {v2}, Lcom/twitter/app/lists/a;->f()Z

    move-result v0

    invoke-virtual {v2}, Lcom/twitter/app/lists/a;->g()I

    move-result v3

    invoke-static {v0, v3}, Lcom/twitter/app/lists/ListsActivity;->a(ZI)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/lists/ListsActivity;->setTitle(I)V

    .line 64
    if-nez p1, :cond_0

    .line 65
    new-instance v3, Lcom/twitter/app/lists/ListsFragment;

    invoke-direct {v3}, Lcom/twitter/app/lists/ListsFragment;-><init>()V

    .line 66
    invoke-static {v1}, Lcom/twitter/app/lists/f$a;->a(Landroid/content/Intent;)Lcom/twitter/app/lists/f$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/app/lists/ListsActivity;->b:Z

    .line 67
    invoke-virtual {v0, v1}, Lcom/twitter/app/lists/f$a;->a(Z)Lcom/twitter/app/lists/f$a;

    move-result-object v0

    .line 68
    invoke-virtual {v2}, Lcom/twitter/app/lists/a;->e()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/lists/f$a;->b(Z)Lcom/twitter/app/lists/f$a;

    move-result-object v0

    .line 69
    invoke-virtual {v2}, Lcom/twitter/app/lists/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/lists/f$a;->a(Ljava/lang/String;)Lcom/twitter/app/lists/f$a;

    move-result-object v0

    const v1, 0x7f0a05e3

    .line 70
    invoke-virtual {v0, v1}, Lcom/twitter/app/lists/f$a;->b(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/lists/f$a;

    iget-boolean v1, p0, Lcom/twitter/app/lists/ListsActivity;->b:Z

    if-eqz v1, :cond_2

    const v1, 0x7f0a05e6

    .line 71
    :goto_1
    invoke-virtual {v0, v1}, Lcom/twitter/app/lists/f$a;->c(I)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/lists/f$a;

    .line 73
    invoke-virtual {v0}, Lcom/twitter/app/lists/f$a;->a()Lcom/twitter/app/lists/f;

    move-result-object v0

    .line 66
    invoke-virtual {v3, v0}, Lcom/twitter/app/lists/ListsFragment;->a(Lcom/twitter/app/common/base/b;)V

    .line 74
    invoke-virtual {v3, p0}, Lcom/twitter/app/lists/ListsFragment;->a(Lcom/twitter/app/lists/g;)V

    .line 76
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f1302e4

    .line 78
    invoke-virtual {v0, v1, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 81
    :cond_0
    return-void

    .line 61
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 70
    :cond_2
    const v1, 0x7f0a05e2

    goto :goto_1
.end method

.method protected onStart()V
    .locals 4

    .prologue
    .line 108
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onStart()V

    .line 109
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/twitter/app/lists/ListsActivity;->b:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "own_lists"

    :goto_0
    aput-object v0, v2, v3

    const/4 v0, 0x1

    const-string/jumbo v3, ":::impression"

    aput-object v3, v2, v0

    .line 110
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 109
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 111
    return-void

    .line 109
    :cond_0
    const-string/jumbo v0, "lists"

    goto :goto_0
.end method
