.class public Lcom/twitter/app/lists/ListsFragment;
.super Lcom/twitter/app/common/list/TwitterListFragment;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/list/TwitterListFragment",
        "<",
        "Lcom/twitter/model/core/aa;",
        "Lcom/twitter/app/lists/c;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Z

.field private b:Z

.field private c:Ljava/lang/String;

.field private d:Lcom/twitter/app/lists/g;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/twitter/app/common/list/TwitterListFragment;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/model/core/aa;)V
    .locals 6

    .prologue
    .line 226
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/app/lists/ListTabActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 227
    const-string/jumbo v1, "owner_id"

    iget-wide v2, p1, Lcom/twitter/model/core/aa;->f:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "creator_id"

    iget-wide v4, p1, Lcom/twitter/model/core/aa;->g:J

    .line 228
    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "list_id"

    iget-wide v4, p1, Lcom/twitter/model/core/aa;->e:J

    .line 229
    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "list_name"

    iget-object v3, p1, Lcom/twitter/model/core/aa;->h:Ljava/lang/String;

    .line 230
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "list_description"

    iget-object v3, p1, Lcom/twitter/model/core/aa;->j:Ljava/lang/String;

    .line 231
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "list_fullname"

    iget-object v3, p1, Lcom/twitter/model/core/aa;->i:Ljava/lang/String;

    .line 232
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "is_private"

    iget-boolean v3, p1, Lcom/twitter/model/core/aa;->b:Z

    .line 233
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 234
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 235
    return-void
.end method

.method private c(I)I
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SwitchIntDef"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 175
    packed-switch p1, :pswitch_data_0

    .line 188
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fetch type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    move v0, v1

    .line 184
    :goto_0
    return v0

    .line 181
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/lists/c;

    invoke-virtual {v0}, Lcom/twitter/app/lists/c;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 182
    goto :goto_0

    .line 184
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 175
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private k()V
    .locals 2

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f13043e

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/DefaultFab;

    .line 80
    if-eqz v0, :cond_0

    .line 81
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/DefaultFab;->setVisibility(I)V

    .line 82
    new-instance v1, Lcom/twitter/app/lists/ListsFragment$1;

    invoke-direct {v1, p0}, Lcom/twitter/app/lists/ListsFragment$1;-><init>(Lcom/twitter/app/lists/ListsFragment;)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/DefaultFab;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    :cond_0
    return-void
.end method


# virtual methods
.method public synthetic H()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsFragment;->f()Lcom/twitter/app/lists/f;

    move-result-object v0

    return-object v0
.end method

.method public H_()V
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/app/lists/ListsFragment;->b(I)Z

    .line 127
    return-void
.end method

.method public synthetic I()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsFragment;->f()Lcom/twitter/app/lists/f;

    move-result-object v0

    return-object v0
.end method

.method protected K()Laoy;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laoy",
            "<",
            "Lcbi",
            "<",
            "Lcom/twitter/model/core/aa;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 106
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    .line 107
    new-instance v0, Laqq;

    .line 109
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x0

    .line 111
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsFragment;->f()Lcom/twitter/app/lists/f;

    move-result-object v4

    invoke-static {v4, v6, v7}, Laqr;->a(Lcom/twitter/app/lists/f;J)Laqr;

    move-result-object v4

    .line 112
    invoke-static {v6, v7}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v5

    const-class v6, Laxs;

    invoke-interface {v5, v6}, Lcom/twitter/database/schema/TwitterSchema;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Laqq;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;ILaqr;Lcom/twitter/database/model/k;)V

    .line 107
    return-object v0
.end method

.method public a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 4

    .prologue
    .line 200
    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/aa;

    .line 201
    invoke-virtual {p1}, Landroid/widget/ListView;->getChoiceMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 211
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/twitter/app/lists/ListsFragment;->a(Landroid/content/Context;Lcom/twitter/model/core/aa;)V

    .line 215
    :cond_0
    :goto_0
    return-void

    .line 204
    :pswitch_0
    iget-object v1, p0, Lcom/twitter/app/lists/ListsFragment;->d:Lcom/twitter/app/lists/g;

    if-eqz v1, :cond_0

    .line 205
    iget-object v1, p0, Lcom/twitter/app/lists/ListsFragment;->d:Lcom/twitter/app/lists/g;

    invoke-virtual {v0}, Lcom/twitter/model/core/aa;->a()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/twitter/app/lists/g;->a(J)V

    goto :goto_0

    .line 201
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Lcbi;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/model/core/aa;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 117
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcbi;)V

    .line 118
    iget-boolean v0, p0, Lcom/twitter/app/lists/ListsFragment;->a:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcbi;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 119
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/app/lists/ListsFragment;->a:Z

    .line 120
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/app/lists/ListsFragment;->b(I)Z

    .line 122
    :cond_1
    return-void
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 1

    .prologue
    .line 95
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 96
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsFragment;->f()Lcom/twitter/app/lists/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/lists/f;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "android_fab_new_list_moment_collection_5378"

    .line 97
    invoke-static {v0}, Lcoi;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    const v0, 0x7f040177

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->c(I)Lcom/twitter/app/common/list/l$d;

    .line 100
    :cond_0
    return-void
.end method

.method a(Lcom/twitter/app/lists/g;)V
    .locals 0

    .prologue
    .line 194
    iput-object p1, p0, Lcom/twitter/app/lists/ListsFragment;->d:Lcom/twitter/app/lists/g;

    .line 195
    return-void
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 219
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 220
    if-ne p2, v2, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v0

    if-nez v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/twitter/app/lists/ListsFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a0499

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 223
    :cond_0
    return-void
.end method

.method protected b(I)Z
    .locals 7

    .prologue
    const/16 v6, 0x64

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 139
    invoke-virtual {p0, p1}, Lcom/twitter/app/lists/ListsFragment;->c_(I)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 163
    :goto_0
    return v0

    .line 143
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsFragment;->f()Lcom/twitter/app/lists/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/lists/f;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "own_lists"

    :goto_1
    const/4 v3, 0x0

    invoke-static {v0, v3, p1}, Lcom/twitter/app/lists/ListsFragment;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 145
    iget-boolean v3, p0, Lcom/twitter/app/lists/ListsFragment;->b:Z

    if-eqz v3, :cond_2

    .line 146
    new-instance v3, Lbcx;

    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lbcx;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 147
    invoke-virtual {v3, v1}, Lbcx;->a(I)Lbcx;

    move-result-object v1

    .line 148
    invoke-direct {p0, p1}, Lcom/twitter/app/lists/ListsFragment;->c(I)I

    move-result v3

    invoke-virtual {v1, v3}, Lbcx;->c(I)Lbcx;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/app/lists/ListsFragment;->c:Ljava/lang/String;

    .line 149
    invoke-virtual {v1, v3}, Lbcx;->a(Ljava/lang/String;)Lbcx;

    move-result-object v1

    iget-wide v4, p0, Lcom/twitter/app/lists/ListsFragment;->a_:J

    .line 150
    invoke-virtual {v1, v4, v5}, Lbcx;->a(J)Lbcx;

    move-result-object v1

    .line 151
    invoke-virtual {v1, v6}, Lbcx;->d(I)Lbcx;

    move-result-object v1

    const-string/jumbo v3, "scribe_event"

    .line 152
    invoke-virtual {v1, v3, v0}, Lbcx;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/s;

    move-result-object v0

    .line 146
    invoke-virtual {p0, v0, v2, p1}, Lcom/twitter/app/lists/ListsFragment;->c(Lcom/twitter/library/service/s;II)Z

    :goto_2
    move v0, v2

    .line 163
    goto :goto_0

    .line 143
    :cond_1
    const-string/jumbo v0, "lists"

    goto :goto_1

    .line 155
    :cond_2
    new-instance v1, Lbcw;

    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lbcw;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 156
    invoke-direct {p0, p1}, Lcom/twitter/app/lists/ListsFragment;->c(I)I

    move-result v3

    invoke-virtual {v1, v3}, Lbcw;->a(I)Lbcw;

    move-result-object v1

    iget-wide v4, p0, Lcom/twitter/app/lists/ListsFragment;->a_:J

    .line 157
    invoke-virtual {v1, v4, v5}, Lbcw;->a(J)Lbcw;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/app/lists/ListsFragment;->c:Ljava/lang/String;

    .line 158
    invoke-virtual {v1, v3}, Lbcw;->a(Ljava/lang/String;)Lbcw;

    move-result-object v1

    .line 159
    invoke-virtual {v1, v6}, Lbcw;->c(I)Lbcw;

    move-result-object v1

    const-string/jumbo v3, "scribe_event"

    .line 160
    invoke-virtual {v1, v3, v0}, Lbcw;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/s;

    move-result-object v0

    .line 155
    invoke-virtual {p0, v0, v2, p1}, Lcom/twitter/app/lists/ListsFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto :goto_2
.end method

.method public f()Lcom/twitter/app/lists/f;
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/lists/f;->a(Landroid/os/Bundle;)Lcom/twitter/app/lists/f;

    move-result-object v0

    return-object v0
.end method

.method protected l()V
    .locals 2

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/lists/c;

    invoke-virtual {v0}, Lcom/twitter/app/lists/c;->g()Lcbi;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laqq$a;

    .line 132
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsFragment;->an()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Laqq$a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 133
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/app/lists/ListsFragment;->b(I)Z

    .line 135
    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 74
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/lists/c;

    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/app/lists/c;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l;->a(Lcjr;)V

    .line 75
    invoke-direct {p0}, Lcom/twitter/app/lists/ListsFragment;->k()V

    .line 76
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 65
    invoke-virtual {p0}, Lcom/twitter/app/lists/ListsFragment;->f()Lcom/twitter/app/lists/f;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lcom/twitter/app/lists/f;->c()Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/app/lists/ListsFragment;->a:Z

    .line 67
    invoke-virtual {v0}, Lcom/twitter/app/lists/f;->e()Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/app/lists/ListsFragment;->b:Z

    .line 68
    invoke-virtual {v0}, Lcom/twitter/app/lists/f;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/lists/ListsFragment;->c:Ljava/lang/String;

    .line 69
    return-void
.end method
