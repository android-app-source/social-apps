.class final Lcom/twitter/app/home/HomeTimelineFragment$b;
.super Lcom/twitter/app/common/timeline/TimelineFragment$e;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/home/HomeTimelineFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/home/HomeTimelineFragment;


# direct methods
.method constructor <init>(Lcom/twitter/app/home/HomeTimelineFragment;Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Lcom/twitter/android/ck;Lcom/twitter/android/timeline/be;I)V
    .locals 10

    .prologue
    .line 164
    iput-object p1, p0, Lcom/twitter/app/home/HomeTimelineFragment$b;->a:Lcom/twitter/app/home/HomeTimelineFragment;

    .line 165
    const-wide/16 v6, -0x1

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v8, p6

    move/from16 v9, p7

    invoke-direct/range {v1 .. v9}, Lcom/twitter/app/common/timeline/TimelineFragment$e;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Lcom/twitter/android/ck;JLcom/twitter/android/timeline/be;I)V

    .line 167
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/ad;)V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->ad:Lcom/twitter/model/timeline/r;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/twitter/model/core/Tweet;->ad:Lcom/twitter/model/timeline/r;

    iget-object v0, v0, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/app/home/HomeTimelineFragment$b;->a:Lcom/twitter/app/home/HomeTimelineFragment;

    invoke-virtual {v1}, Lcom/twitter/app/home/HomeTimelineFragment;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/core/Tweet;->ad:Lcom/twitter/model/timeline/r;

    iget-object v1, v1, Lcom/twitter/model/timeline/r;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":link:open_link"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, p1, p2, v0}, Lcom/twitter/app/common/timeline/TimelineFragment$e;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/ad;Ljava/lang/String;)V

    .line 179
    :goto_0
    return-void

    .line 177
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/timeline/TimelineFragment$e;->a(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/ad;)V

    goto :goto_0
.end method
