.class Lcom/twitter/app/home/HomeTimelineFragment$a;
.super Lcpb;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/home/HomeTimelineFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V
    .locals 5

    .prologue
    .line 955
    invoke-direct {p0}, Lcpb;-><init>()V

    .line 956
    if-nez p1, :cond_1

    .line 957
    const-string/jumbo v0, "ActiveSessionUserMissing.account_name"

    const-string/jumbo v1, "Missing account"

    invoke-virtual {p0, v0, v1}, Lcom/twitter/app/home/HomeTimelineFragment$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 958
    const-string/jumbo v0, "ActiveSessionUserMissing.reason"

    const-string/jumbo v1, "Context missing"

    invoke-virtual {p0, v0, v1}, Lcom/twitter/app/home/HomeTimelineFragment$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 959
    const-string/jumbo v0, "ActiveSessionUserMissing.additional_info"

    const-string/jumbo v1, "Fragment may have been detached or destroyed"

    invoke-virtual {p0, v0, v1}, Lcom/twitter/app/home/HomeTimelineFragment$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 974
    :cond_0
    :goto_0
    const-string/jumbo v0, "ActiveSessionUserMissing.login_status"

    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->b()Lcom/twitter/library/client/Session$LoginStatus;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/app/home/HomeTimelineFragment$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 975
    return-void

    .line 962
    :cond_1
    invoke-static {}, Lakn;->a()Lakn;

    move-result-object v0

    new-instance v1, Lcnz;

    invoke-direct {v1, p3, p4}, Lcnz;-><init>(J)V

    invoke-virtual {v0, v1}, Lakn;->a(Lcnz;)Lakm;

    move-result-object v0

    .line 963
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lakm;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 964
    :cond_2
    const-string/jumbo v0, "ActiveSessionUserMissing.account_name"

    const-string/jumbo v1, "Missing account"

    invoke-virtual {p0, v0, v1}, Lcom/twitter/app/home/HomeTimelineFragment$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 965
    const-string/jumbo v0, "ActiveSessionUserMissing.reason"

    const-string/jumbo v1, "Cannot find account"

    invoke-virtual {p0, v0, v1}, Lcom/twitter/app/home/HomeTimelineFragment$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 966
    const-string/jumbo v0, "ActiveSessionUserMissing.additional_info"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No matching user id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/app/home/HomeTimelineFragment$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    goto :goto_0

    .line 967
    :cond_3
    invoke-virtual {v0}, Lakm;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 968
    const-string/jumbo v1, "ActiveSessionUserMissing.account_name"

    invoke-virtual {v0}, Lakm;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/twitter/app/home/HomeTimelineFragment$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 969
    const-string/jumbo v1, "ActiveSessionUserMissing.reason"

    const-string/jumbo v2, "Session name mismatch"

    invoke-virtual {p0, v1, v2}, Lcom/twitter/app/home/HomeTimelineFragment$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    .line 970
    const-string/jumbo v1, "ActiveSessionUserMissing.additional_info"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Account name: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lakm;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ", Session name: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 971
    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 970
    invoke-virtual {p0, v1, v0}, Lcom/twitter/app/home/HomeTimelineFragment$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcpb;

    goto/16 :goto_0
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcom/twitter/app/home/HomeTimelineFragment$1;)V
    .locals 1

    .prologue
    .line 947
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/app/home/HomeTimelineFragment$a;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    return-void
.end method
