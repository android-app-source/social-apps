.class public Lcom/twitter/app/home/HomeTimelineFragment;
.super Lcom/twitter/app/common/timeline/TimelineFragment;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/dogfood/a$b;
.implements Lcom/twitter/android/timeline/br$a;
.implements Lcom/twitter/android/util/v$a;
.implements Lcom/twitter/android/util/v$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/home/HomeTimelineFragment$a;,
        Lcom/twitter/app/home/HomeTimelineFragment$b;
    }
.end annotation


# static fields
.field private static a:Ljava/lang/Runnable;


# instance fields
.field private b:Lcom/twitter/app/home/d;

.field private c:Z

.field private d:Lcom/twitter/android/timeline/br;

.field private e:J

.field private f:Lcom/twitter/android/widget/a;

.field private t:Z

.field private u:Z

.field private v:I

.field private w:Laot;

.field private x:Z

.field private y:Lcnl;

.field private z:Lcom/twitter/android/metrics/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/home/HomeTimelineFragment;)Lcom/twitter/android/timeline/br;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->d:Lcom/twitter/android/timeline/br;

    return-object v0
.end method

.method static a(Landroid/content/Context;Lcom/twitter/library/client/Session;J)Z
    .locals 8
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 598
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    if-nez v0, :cond_0

    .line 601
    if-eqz p0, :cond_1

    .line 602
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 604
    :goto_0
    new-instance v1, Lcom/twitter/app/home/HomeTimelineFragment$a;

    const/4 v6, 0x0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Lcom/twitter/app/home/HomeTimelineFragment$a;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcom/twitter/app/home/HomeTimelineFragment$1;)V

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "TLN-2544"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 605
    invoke-virtual {v1, v0}, Lcom/twitter/app/home/HomeTimelineFragment$a;->a(Ljava/lang/Throwable;)Lcpb;

    move-result-object v0

    .line 606
    invoke-static {v0}, Lcpd;->c(Lcpb;)V

    .line 607
    const/4 v0, 0x0

    .line 609
    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    move-object v2, p0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/app/home/HomeTimelineFragment;Z)Z
    .locals 0

    .prologue
    .line 119
    iput-boolean p1, p0, Lcom/twitter/app/home/HomeTimelineFragment;->t:Z

    return p1
.end method

.method private aX()V
    .locals 4

    .prologue
    .line 264
    const-string/jumbo v0, "app_rating_prompt_enable"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 265
    const-string/jumbo v0, "app_rating_prompt_show_now"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 266
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/util/AppRatingPromptHelper$a;

    invoke-direct {v1}, Lcom/twitter/android/util/AppRatingPromptHelper$a;-><init>()V

    invoke-static {v0, v1}, Lcom/twitter/android/util/AppRatingPromptHelper;->a(Landroid/content/Context;Lcom/twitter/android/util/AppRatingPromptHelper$a;)I

    move-result v0

    const/4 v1, 0x7

    if-lt v0, v1, :cond_1

    .line 268
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->f:Lcom/twitter/android/widget/a;

    if-nez v0, :cond_1

    .line 269
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 270
    new-instance v1, Lcom/twitter/android/widget/a;

    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v0, v2, v3}, Lcom/twitter/android/widget/a;-><init>(Landroid/content/Context;J)V

    iput-object v1, p0, Lcom/twitter/app/home/HomeTimelineFragment;->f:Lcom/twitter/android/widget/a;

    .line 271
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->f:Lcom/twitter/android/widget/a;

    invoke-virtual {v0}, Lcom/twitter/android/widget/a;->f()V

    .line 275
    :cond_1
    return-void
.end method

.method private aY()V
    .locals 4

    .prologue
    .line 433
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->aJ()Lcom/twitter/metrics/j;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/twitter/android/metrics/e;->a(Lcom/twitter/metrics/j;J)Lcom/twitter/android/metrics/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->z:Lcom/twitter/android/metrics/e;

    .line 434
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->z:Lcom/twitter/android/metrics/e;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/e;->i()V

    .line 435
    return-void
.end method

.method static synthetic b(Lcom/twitter/app/home/HomeTimelineFragment;)J
    .locals 2

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->aQ()J

    move-result-wide v0

    return-wide v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 935
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->b:Lcom/twitter/app/home/d;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/home/d;

    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p1}, Lcom/twitter/app/home/d;->a(JLjava/lang/String;)Z

    .line 936
    return-void
.end method

.method static synthetic b(Lcom/twitter/app/home/HomeTimelineFragment;Z)Z
    .locals 0

    .prologue
    .line 119
    iput-boolean p1, p0, Lcom/twitter/app/home/HomeTimelineFragment;->x:Z

    return p1
.end method

.method private c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 939
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->b:Lcom/twitter/app/home/d;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/home/d;

    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p1}, Lcom/twitter/app/home/d;->b(JLjava/lang/String;)Z

    .line 940
    return-void
.end method


# virtual methods
.method protected A()I
    .locals 1

    .prologue
    .line 774
    iget v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->v:I

    return v0
.end method

.method protected B()V
    .locals 1

    .prologue
    .line 388
    invoke-super {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->B()V

    .line 389
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->z:Lcom/twitter/android/metrics/e;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/e;->j()V

    .line 390
    return-void
.end method

.method protected D()Lcom/twitter/app/common/timeline/TimelineFragment$e;
    .locals 8

    .prologue
    .line 428
    new-instance v0, Lcom/twitter/app/home/HomeTimelineFragment$b;

    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->aO()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/twitter/app/home/HomeTimelineFragment;->H:Lcom/twitter/android/ck;

    .line 429
    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/twitter/android/ck;

    iget-object v6, p0, Lcom/twitter/app/home/HomeTimelineFragment;->j:Lcom/twitter/android/timeline/be;

    iget v7, p0, Lcom/twitter/app/home/HomeTimelineFragment;->q:I

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/app/home/HomeTimelineFragment$b;-><init>(Lcom/twitter/app/home/HomeTimelineFragment;Landroid/support/v4/app/Fragment;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;Lcom/twitter/android/ck;Lcom/twitter/android/timeline/be;I)V

    .line 428
    return-object v0
.end method

.method public synthetic H()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->aV()Lcom/twitter/app/home/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic I()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->aV()Lcom/twitter/app/home/c;

    move-result-object v0

    return-object v0
.end method

.method protected K_()Z
    .locals 1

    .prologue
    .line 764
    invoke-static {}, Lcom/twitter/library/av/v;->a()Z

    move-result v0

    return v0
.end method

.method public L()V
    .locals 1

    .prologue
    .line 259
    invoke-super {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->L()V

    .line 260
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->d:Lcom/twitter/android/timeline/br;

    invoke-interface {v0}, Lcom/twitter/android/timeline/br;->j()V

    .line 261
    return-void
.end method

.method protected N_()Z
    .locals 1

    .prologue
    .line 769
    invoke-static {}, Lcom/twitter/android/revenue/k;->a()Z

    move-result v0

    return v0
.end method

.method public V_()V
    .locals 4

    .prologue
    .line 985
    invoke-super {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->V_()V

    .line 986
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 987
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "home::::pull_to_refresh"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 989
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->aR()V

    .line 990
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    .line 439
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 440
    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 442
    invoke-static {p2}, Lank;->a(Landroid/os/Bundle;)Lank;

    move-result-object v2

    .line 443
    const v0, 0x7f130011

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/NewItemBannerView;

    .line 444
    iget v3, p0, Lcom/twitter/app/home/HomeTimelineFragment;->q:I

    invoke-static {v3, v0, p0, v2}, Lcom/twitter/android/timeline/bs;->a(ILcom/twitter/android/widget/NewItemBannerView;Lcom/twitter/android/timeline/br$a;Lank;)Lcom/twitter/android/timeline/br;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->d:Lcom/twitter/android/timeline/br;

    .line 446
    new-instance v0, Lcom/twitter/app/home/HomeTimelineFragment$3;

    invoke-direct {v0, p0}, Lcom/twitter/app/home/HomeTimelineFragment$3;-><init>(Lcom/twitter/app/home/HomeTimelineFragment;)V

    invoke-virtual {p0, v0}, Lcom/twitter/app/home/HomeTimelineFragment;->a(Lcno$c;)Lcom/twitter/app/common/list/TwitterListFragment;

    .line 462
    new-instance v0, Laqo;

    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v2, v4, v5}, Laqo;-><init>(Ljava/lang/String;J)V

    invoke-virtual {p0, v0}, Lcom/twitter/app/home/HomeTimelineFragment;->a(Lcno$c;)Lcom/twitter/app/common/list/TwitterListFragment;

    .line 463
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->d:Lcom/twitter/android/timeline/br;

    invoke-interface {v0}, Lcom/twitter/android/timeline/br;->e()V

    .line 464
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->ag()Lanh;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/app/home/HomeTimelineFragment;->d:Lcom/twitter/android/timeline/br;

    invoke-virtual {v0, v2}, Lanh;->a(Ljava/lang/Object;)Lanh;

    .line 466
    const v0, 0x7f130367

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    .line 467
    if-eqz v0, :cond_0

    .line 468
    invoke-virtual {v0, p0}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 470
    :cond_0
    return-object v1
.end method

.method protected a(JJ)V
    .locals 1

    .prologue
    .line 580
    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(JJ)V

    .line 581
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->z:Lcom/twitter/android/metrics/e;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/e;->k()V

    .line 582
    return-void
.end method

.method public a(Landroid/view/View;Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 630
    const-string/jumbo v0, "position"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 631
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->N()Lcom/twitter/android/timeline/bl;

    move-result-object v2

    .line 632
    const/4 v0, 0x0

    .line 634
    if-eqz v2, :cond_0

    .line 635
    invoke-virtual {v2, v1}, Lcom/twitter/android/timeline/bl;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/timeline/bk;

    .line 639
    :cond_0
    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    .line 643
    iget-object v3, p0, Lcom/twitter/app/home/HomeTimelineFragment;->m:Lcom/twitter/android/cp;

    invoke-virtual {v3, v0, p2, p3}, Lcom/twitter/android/cp;->a(Lcom/twitter/android/timeline/bk;Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)Z

    .line 646
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Landroid/view/View;Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)V

    .line 652
    iget-boolean v3, p0, Lcom/twitter/app/home/HomeTimelineFragment;->t:Z

    if-nez v3, :cond_2

    .line 653
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->aU()Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/app/home/HomeTimelineFragment;->t:Z

    .line 657
    :cond_2
    if-eqz v0, :cond_4

    .line 658
    invoke-virtual {v2}, Lcom/twitter/android/timeline/bl;->be_()I

    move-result v0

    .line 659
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->A()I

    move-result v3

    if-ge v0, v3, :cond_4

    sub-int v3, v0, v1

    const/16 v4, 0x14

    if-gt v3, v4, :cond_4

    .line 661
    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v0}, Lcom/twitter/android/timeline/bl;->h(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 662
    iget v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->J:I

    invoke-virtual {v2, v0}, Lcom/twitter/android/timeline/bl;->j(I)J

    move-result-wide v4

    .line 665
    iget-wide v6, p0, Lcom/twitter/app/home/HomeTimelineFragment;->e:J

    cmp-long v0, v4, v6

    if-eqz v0, :cond_3

    invoke-virtual {p0, v8}, Lcom/twitter/app/home/HomeTimelineFragment;->c(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 668
    iput-wide v4, p0, Lcom/twitter/app/home/HomeTimelineFragment;->e:J

    .line 672
    :cond_3
    invoke-virtual {v2, v1}, Lcom/twitter/android/timeline/bl;->f_(I)Z

    .line 676
    :cond_4
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->p:Lbwb;

    if-eqz v0, :cond_5

    invoke-static {p2}, Lbwi;->a(Lcom/twitter/model/core/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 679
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->p:Lbwb;

    invoke-interface {v0, v8}, Lbwb;->a(I)I

    .line 681
    :cond_5
    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 119
    check-cast p2, Lcom/twitter/model/core/Tweet;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/app/home/HomeTimelineFragment;->a(Landroid/view/View;Lcom/twitter/model/core/Tweet;Landroid/os/Bundle;)V

    return-void
.end method

.method protected a(Lcbi;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/android/timeline/bk;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 554
    iget-boolean v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->c:Z

    if-eqz v0, :cond_1

    .line 555
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->z()Lcnk;

    move-result-object v0

    .line 556
    invoke-virtual {p0, p1}, Lcom/twitter/app/home/HomeTimelineFragment;->b(Lcbi;)V

    .line 557
    invoke-virtual {p0, v0}, Lcom/twitter/app/home/HomeTimelineFragment;->b(Lcnk;)V

    .line 562
    :goto_0
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->z:Lcom/twitter/android/metrics/e;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/e;->aR_()V

    .line 564
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->aC()Landroid/database/Cursor;

    move-result-object v0

    .line 565
    if-eqz v0, :cond_0

    .line 566
    invoke-interface {v0}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 567
    iget-object v1, p0, Lcom/twitter/app/home/HomeTimelineFragment;->o:Lcom/twitter/android/revenue/c;

    const-string/jumbo v2, "ad_slots_count"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/revenue/c;->a(I)V

    .line 569
    iget-object v1, p0, Lcom/twitter/app/home/HomeTimelineFragment;->p:Lbwb;

    if-eqz v1, :cond_0

    .line 570
    const-string/jumbo v1, "realtime_ad_count"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 571
    if-lez v0, :cond_0

    .line 572
    iget-object v1, p0, Lcom/twitter/app/home/HomeTimelineFragment;->p:Lbwb;

    invoke-interface {v1, v0}, Lbwb;->a(I)I

    .line 576
    :cond_0
    return-void

    .line 559
    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Lcbi;)V

    goto :goto_0
.end method

.method public a(Lcbi;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/android/timeline/bk;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 913
    if-nez p2, :cond_1

    .line 919
    iget-boolean v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->x:Z

    if-eqz v0, :cond_0

    .line 920
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->t()V

    .line 921
    const-string/jumbo v0, "start_at_top"

    .line 929
    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/twitter/app/home/HomeTimelineFragment;->x:Z

    .line 930
    invoke-direct {p0, v0}, Lcom/twitter/app/home/HomeTimelineFragment;->c(Ljava/lang/String;)V

    .line 931
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Lcbi;Z)V

    .line 932
    return-void

    .line 924
    :cond_0
    const-string/jumbo v0, "last_reading_position"

    goto :goto_0

    .line 927
    :cond_1
    const-string/jumbo v0, "timeout"

    goto :goto_0
.end method

.method protected a(Lcnk;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 728
    invoke-super {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Lcnk;)V

    .line 729
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog$a;

    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog$a;-><init>(J)V

    .line 730
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->i()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v5, "position_restore_failure"

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/analytics/feature/model/ClientEventLog$a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/ClientEventLog$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog$a;->a()Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v0

    .line 729
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 731
    return-void
.end method

.method public a(Lcno;IIIZ)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 786
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 788
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cv;

    .line 789
    iget-boolean v1, p0, Lcom/twitter/app/home/HomeTimelineFragment;->u:Z

    if-eqz v1, :cond_0

    instance-of v1, v0, Lcom/twitter/android/cf;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 790
    check-cast v1, Lcom/twitter/android/cf;

    invoke-virtual {v1}, Lcom/twitter/android/cf;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 792
    sub-int v1, p2, p3

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v1, v1, -0x2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 794
    add-int/lit8 v3, p2, 0x1

    add-int/lit8 v3, v3, 0x2

    .line 795
    invoke-virtual {v0}, Lcom/twitter/android/cv;->getCount()I

    move-result v4

    .line 794
    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    move v3, v1

    .line 796
    :goto_0
    if-ge v3, v4, :cond_3

    move-object v1, v0

    .line 797
    check-cast v1, Lcom/twitter/android/cf;

    invoke-virtual {v1, v3}, Lcom/twitter/android/cf;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 798
    const/4 v1, 0x1

    .line 802
    :goto_1
    if-nez v1, :cond_0

    .line 803
    check-cast v0, Lcom/twitter/android/cf;

    invoke-virtual {v0, v2}, Lcom/twitter/android/cf;->b(Z)V

    .line 804
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->T:Landroid/content/Context;

    .line 805
    invoke-static {v0}, Lcom/twitter/library/client/c;->a(Landroid/content/Context;)Lcom/twitter/library/client/c;

    move-result-object v0

    .line 806
    const-string/jumbo v1, "optin"

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/c;->b(Ljava/lang/String;)V

    .line 808
    const-string/jumbo v1, "optin"

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/c;->a(Ljava/lang/String;)V

    .line 815
    :cond_0
    invoke-interface {p1}, Lcno;->a()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/widget/ag;->a(Landroid/view/View;)Lcom/twitter/android/widget/af;

    move-result-object v0

    .line 816
    if-eqz v0, :cond_1

    .line 818
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/app/common/list/l;->f()Lcno;

    move-result-object v1

    invoke-interface {v1}, Lcno;->e()I

    move-result v1

    .line 817
    invoke-interface {v0, p2, v1}, Lcom/twitter/android/widget/af;->a(II)V

    .line 821
    :cond_1
    invoke-super/range {p0 .. p5}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Lcno;IIIZ)V

    .line 822
    return-void

    .line 796
    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method public a(Lcom/twitter/android/util/v;)V
    .locals 0

    .prologue
    .line 422
    invoke-virtual {p1, p0}, Lcom/twitter/android/util/v;->a(Lcom/twitter/android/util/v$a;)V

    .line 423
    return-void
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 1

    .prologue
    .line 475
    invoke-super {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 476
    const v0, 0x7f04015f

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->c(I)Lcom/twitter/app/common/list/l$d;

    .line 477
    const v0, 0x7f0400e2

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->f(I)Lcom/twitter/app/common/list/l$d;

    .line 478
    invoke-static {}, Lbpu;->a()Lbpu;

    move-result-object v0

    invoke-virtual {v0}, Lbpu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 479
    const v0, 0x7f0403e0

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    .line 480
    const v0, 0x7f040127

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->h(I)V

    .line 484
    :goto_0
    return-void

    .line 482
    :cond_0
    const v0, 0x7f040340

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 334
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 336
    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    .line 337
    const-string/jumbo v0, "home:refresh"

    .line 338
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->aJ()Lcom/twitter/metrics/j;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/app/home/HomeTimelineFragment;->a_:J

    sget-object v6, Lcom/twitter/metrics/g;->n:Lcom/twitter/metrics/g$b;

    .line 337
    invoke-static {v0, v3, v4, v5, v6}, Lcom/twitter/metrics/e;->a(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;)Lcom/twitter/metrics/e;

    move-result-object v0

    .line 340
    invoke-virtual {v0}, Lcom/twitter/metrics/e;->j()V

    .line 343
    :cond_0
    instance-of v0, p1, Lbfr;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 344
    check-cast v0, Lbfr;

    .line 345
    invoke-virtual {v0}, Lbfr;->G()I

    move-result v3

    .line 346
    invoke-virtual {v0}, Lbfr;->H()Lcom/twitter/model/timeline/u;

    move-result-object v4

    .line 347
    if-ne p3, v7, :cond_6

    move v0, v1

    .line 348
    :goto_0
    iget-object v5, p0, Lcom/twitter/app/home/HomeTimelineFragment;->d:Lcom/twitter/android/timeline/br;

    invoke-interface {v5, v3, v0, v4}, Lcom/twitter/android/timeline/br;->a(IZLcom/twitter/model/timeline/u;)V

    .line 351
    :cond_1
    instance-of v0, p1, Lbgt;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 352
    check-cast v0, Lbgt;

    .line 353
    invoke-virtual {v0}, Lbgt;->G()I

    move-result v3

    .line 354
    invoke-virtual {v0}, Lbgt;->H()Lcom/twitter/model/timeline/u;

    move-result-object v4

    .line 355
    if-ne p3, v7, :cond_7

    move v0, v1

    .line 356
    :goto_1
    iget-object v5, p0, Lcom/twitter/app/home/HomeTimelineFragment;->d:Lcom/twitter/android/timeline/br;

    invoke-interface {v5, v3, v0, v4}, Lcom/twitter/android/timeline/br;->a(IZLcom/twitter/model/timeline/u;)V

    .line 359
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 360
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 361
    const/4 v0, 0x3

    if-eq p3, v0, :cond_3

    if-ne p3, v7, :cond_4

    .line 364
    :cond_3
    invoke-static {v4, v5}, Lbpy;->a(J)V

    .line 367
    :cond_4
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->z:Lcom/twitter/android/metrics/e;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/e;->aT_()V

    .line 368
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->z:Lcom/twitter/android/metrics/e;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/e;->j()V

    .line 370
    if-nez p2, :cond_5

    .line 371
    iput-boolean v2, p0, Lcom/twitter/app/home/HomeTimelineFragment;->c:Z

    .line 375
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->T()Z

    move-result v0

    if-nez v0, :cond_5

    if-ne p3, v1, :cond_5

    invoke-virtual {p1}, Lcom/twitter/library/service/s;->N()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 376
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->e:J

    .line 379
    :cond_5
    return-void

    :cond_6
    move v0, v2

    .line 347
    goto :goto_0

    :cond_7
    move v0, v2

    .line 355
    goto :goto_1
.end method

.method public a(JLcom/twitter/model/core/Tweet;Ljava/lang/Runnable;)Z
    .locals 1

    .prologue
    .line 752
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->c:Z

    .line 754
    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/app/common/timeline/TimelineFragment;->a(JLcom/twitter/model/core/Tweet;Ljava/lang/Runnable;)Z

    move-result v0

    return v0
.end method

.method public a(Lcom/twitter/model/timeline/n;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 489
    if-eqz p1, :cond_5

    .line 490
    invoke-virtual {p1}, Lcom/twitter/model/timeline/n;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 495
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 496
    invoke-static {}, Lcom/twitter/android/util/x;->a()Lcom/twitter/android/util/x;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/twitter/android/util/x;->a(Lcom/twitter/model/timeline/n;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 497
    invoke-static {p1, v0}, Lcom/twitter/android/widget/ReviewPhoneOverlayPrompt;->a(Lcom/twitter/model/timeline/n;Landroid/support/v4/app/FragmentManager;)V

    move v0, v2

    .line 523
    :goto_0
    return v0

    .line 499
    :cond_0
    invoke-static {p1}, Lcom/twitter/android/util/ah;->a(Lcom/twitter/model/timeline/n;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 500
    invoke-static {p1, v0}, Lcom/twitter/android/widget/VerifyPhoneOverlayPrompt;->a(Lcom/twitter/model/timeline/n;Landroid/support/v4/app/FragmentManager;)V

    move v0, v2

    .line 501
    goto :goto_0

    .line 502
    :cond_1
    invoke-static {p1}, Lcom/twitter/android/util/w;->a(Lcom/twitter/model/timeline/n;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 503
    invoke-static {p1, v0}, Lcom/twitter/android/widget/ReviewEmailOverlayPrompt;->a(Lcom/twitter/model/timeline/n;Landroid/support/v4/app/FragmentManager;)V

    move v0, v2

    .line 504
    goto :goto_0

    .line 505
    :cond_2
    invoke-static {p1}, Lcom/twitter/android/util/c;->a(Lcom/twitter/model/timeline/n;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 506
    invoke-static {p1, v0}, Lcom/twitter/android/widget/ConfirmEmailOverlayPrompt;->a(Lcom/twitter/model/timeline/n;Landroid/support/v4/app/FragmentManager;)V

    move v0, v2

    .line 507
    goto :goto_0

    .line 508
    :cond_3
    invoke-static {p1}, Lcom/twitter/android/util/ag;->a(Lcom/twitter/model/timeline/n;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 509
    invoke-static {p1, v0}, Lcom/twitter/android/widget/TypoEmailOverlayPrompt;->a(Lcom/twitter/model/timeline/n;Landroid/support/v4/app/FragmentManager;)V

    move v0, v2

    .line 510
    goto :goto_0

    .line 513
    :cond_4
    invoke-virtual {p1}, Lcom/twitter/model/timeline/n;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 515
    iget-object v0, p1, Lcom/twitter/model/timeline/n;->t:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 516
    iget-object v1, p1, Lcom/twitter/model/timeline/n;->u:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 517
    invoke-static {v0, v1}, Lcoi;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    .line 519
    goto :goto_0

    .line 523
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected aD()V
    .locals 2

    .prologue
    .line 718
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    .line 719
    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->j()Lcjt;

    move-result-object v1

    invoke-interface {v1}, Lcjt;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 720
    iget-object v1, p0, Lcom/twitter/app/home/HomeTimelineFragment;->y:Lcnl;

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->z()Lcnk;

    move-result-object v0

    invoke-interface {v1, v0}, Lcnl;->a(Lcnk;)V

    .line 722
    :cond_0
    return-void
.end method

.method public aD_()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1003
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 1004
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->i()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v4, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "new_tweet_prompt"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object v4, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "show"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1003
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1005
    return-void
.end method

.method public aE_()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1010
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 1011
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->i()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v4, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "new_tweet_prompt"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object v4, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "dismiss"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1010
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1012
    return-void
.end method

.method protected aP_()V
    .locals 1

    .prologue
    .line 215
    invoke-super {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->aP_()V

    .line 218
    sget-object v0, Lcom/twitter/app/home/HomeTimelineFragment;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 219
    sget-object v0, Lcom/twitter/app/home/HomeTimelineFragment;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 221
    :cond_0
    return-void
.end method

.method aU()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 734
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->aj()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/app/common/list/l;->f()Lcno;

    move-result-object v1

    invoke-interface {v1}, Lcno;->a()Landroid/view/ViewGroup;

    move-result-object v1

    .line 735
    :goto_0
    if-eqz v1, :cond_0

    .line 736
    invoke-static {v1}, Lcom/twitter/android/widget/ag;->a(Landroid/view/View;)Lcom/twitter/android/widget/af;

    move-result-object v2

    .line 737
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lez v1, :cond_2

    if-eqz v2, :cond_2

    .line 738
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/app/common/list/l;->z()Lcnk;

    move-result-object v1

    iget v1, v1, Lcnk;->e:I

    .line 739
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/app/common/list/l;->f()Lcno;

    move-result-object v3

    invoke-interface {v3}, Lcno;->e()I

    move-result v3

    .line 738
    invoke-interface {v2, v1, v3}, Lcom/twitter/android/widget/af;->a(II)V

    .line 744
    :cond_0
    :goto_1
    return v0

    .line 734
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 742
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public aV()Lcom/twitter/app/home/c;
    .locals 1

    .prologue
    .line 780
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/home/c;->a(Landroid/os/Bundle;)Lcom/twitter/app/home/c;

    move-result-object v0

    return-object v0
.end method

.method public aW()V
    .locals 1

    .prologue
    .line 979
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->d:Lcom/twitter/android/timeline/br;

    invoke-interface {v0}, Lcom/twitter/android/timeline/br;->m()V

    .line 980
    return-void
.end method

.method protected aa_()Lcom/twitter/app/common/list/b;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 533
    new-instance v0, Lcom/twitter/app/common/list/e;

    new-instance v1, Lcom/twitter/app/home/HomeTimelineFragment$4;

    invoke-direct {v1, p0}, Lcom/twitter/app/home/HomeTimelineFragment$4;-><init>(Lcom/twitter/app/home/HomeTimelineFragment;)V

    invoke-direct {v0, v1, v2, v2, p0}, Lcom/twitter/app/common/list/e;-><init>(Lcom/twitter/util/object/j;ZZLcom/twitter/app/common/list/e$a;)V

    return-object v0
.end method

.method protected ao()V
    .locals 2

    .prologue
    .line 839
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 840
    invoke-static {}, Lcom/twitter/app/home/a;->a()Lcom/twitter/app/home/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/app/home/a;->a_(Lcom/twitter/library/client/Session;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 841
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Lbpn;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 842
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->ap()V

    .line 844
    :cond_0
    return-void
.end method

.method protected ar()Z
    .locals 1

    .prologue
    .line 685
    invoke-super {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->ar()Z

    move-result v0

    .line 687
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->aR()V

    .line 688
    return v0
.end method

.method protected av()Lcom/twitter/app/common/list/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/app/common/list/d",
            "<",
            "Lcom/twitter/android/timeline/bk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 857
    new-instance v0, Lcom/twitter/app/home/HomeTimelineFragment$5;

    invoke-direct {v0, p0, p0}, Lcom/twitter/app/home/HomeTimelineFragment$5;-><init>(Lcom/twitter/app/home/HomeTimelineFragment;Lcom/twitter/app/common/list/d$a;)V

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 209
    invoke-super {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->b()V

    .line 210
    const-string/jumbo v0, "app_graph_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->u:Z

    .line 211
    return-void
.end method

.method protected b(Lcom/twitter/library/service/s;II)V
    .locals 5

    .prologue
    .line 243
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/timeline/TimelineFragment;->b(Lcom/twitter/library/service/s;II)V

    .line 244
    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    .line 245
    const-string/jumbo v0, "home:refresh"

    .line 246
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->aJ()Lcom/twitter/metrics/j;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/app/home/HomeTimelineFragment;->a_:J

    sget-object v4, Lcom/twitter/metrics/g;->n:Lcom/twitter/metrics/g$b;

    .line 245
    invoke-static {v0, v1, v2, v3, v4}, Lcom/twitter/metrics/e;->a(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;)Lcom/twitter/metrics/e;

    move-result-object v0

    .line 247
    invoke-virtual {v0}, Lcom/twitter/metrics/e;->i()V

    .line 249
    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/model/timeline/n;)V
    .locals 0

    .prologue
    .line 528
    return-void
.end method

.method protected c_(I)Z
    .locals 4

    .prologue
    .line 593
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/app/home/HomeTimelineFragment;->a_:J

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/app/home/HomeTimelineFragment;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;J)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->c_(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 412
    const-string/jumbo v0, "home_timeline"

    return-object v0
.end method

.method protected g()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 699
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->y:Lcnl;

    invoke-interface {v0}, Lcnl;->a()Lcnk;

    move-result-object v0

    .line 700
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    .line 701
    invoke-virtual {v0}, Lcnk;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 702
    iget-wide v2, v0, Lcnk;->c:J

    invoke-virtual {p0, v2, v3}, Lcom/twitter/app/home/HomeTimelineFragment;->a(J)I

    move-result v2

    .line 703
    iget-object v3, v1, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 704
    iget v0, v0, Lcnk;->d:I

    invoke-virtual {v1, v2, v0}, Lcom/twitter/app/common/list/l;->a(II)V

    .line 709
    :cond_0
    :goto_0
    return-void

    .line 707
    :cond_1
    invoke-virtual {v1, v3, v3}, Lcom/twitter/app/common/list/l;->a(II)V

    goto :goto_0
.end method

.method protected i(I)Lajc$a;
    .locals 2

    .prologue
    .line 395
    invoke-super {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->i(I)Lajc$a;

    move-result-object v0

    .line 396
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 397
    if-eqz v1, :cond_0

    .line 398
    invoke-static {v1}, Lcom/twitter/util/ui/k;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lajc$a;->c(Ljava/lang/String;)Lajc$a;

    .line 400
    :cond_0
    return-object v0
.end method

.method protected j()Landroid/support/v4/content/Loader;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 544
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->z:Lcom/twitter/android/metrics/e;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/e;->aQ_()V

    .line 545
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    .line 546
    new-instance v1, Lcom/twitter/app/home/b;

    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->y()Laiz;

    move-result-object v0

    invoke-static {v0}, Laja;->a(Laiz;)Lapb;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/app/home/HomeTimelineFragment;->o:Lcom/twitter/android/revenue/c;

    .line 547
    invoke-static {v6, v7}, Lash;->a(J)Lapb;

    move-result-object v5

    const-string/jumbo v0, "ad_formats_ad_slots_android_4189"

    const-string/jumbo v8, "ad_slots"

    .line 548
    invoke-static {v0, v8}, Lcoi;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    invoke-direct/range {v1 .. v8}, Lcom/twitter/app/home/b;-><init>(Landroid/content/Context;Lapb;Lcom/twitter/android/revenue/c;Lapb;JZ)V

    .line 546
    return-object v1
.end method

.method public synthetic k()Lcom/twitter/app/common/timeline/c;
    .locals 1

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->aV()Lcom/twitter/app/home/c;

    move-result-object v0

    return-object v0
.end method

.method k(I)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 759
    iput p1, p0, Lcom/twitter/app/home/HomeTimelineFragment;->v:I

    .line 760
    return-void
.end method

.method protected l()V
    .locals 4

    .prologue
    .line 827
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->aC()Landroid/database/Cursor;

    move-result-object v0

    .line 828
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->moveToLast()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 829
    const/16 v1, 0x10

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->A()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 830
    :cond_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 831
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "home::::bottom"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 834
    :cond_1
    invoke-super {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->l()V

    .line 835
    return-void
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 405
    const/4 v0, 0x1

    return v0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 417
    const/4 v0, 0x0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 279
    invoke-super {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 280
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->f()Lcno;

    move-result-object v0

    invoke-interface {v0}, Lcno;->a()Landroid/view/ViewGroup;

    move-result-object v2

    .line 281
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0a040f

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 283
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/cv;

    .line 284
    instance-of v1, v0, Lcom/twitter/android/widget/ad;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 285
    check-cast v1, Lcom/twitter/android/widget/ad;

    .line 287
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f130422

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/twitter/app/home/HomeTimelineFragment$1;

    invoke-direct {v4, p0}, Lcom/twitter/app/home/HomeTimelineFragment$1;-><init>(Lcom/twitter/app/home/HomeTimelineFragment;)V

    .line 286
    invoke-static {v1, v2, v3, v4}, Lcom/twitter/android/widget/ag;->a(Lcom/twitter/android/widget/ad;Landroid/view/ViewGroup;Landroid/view/View;Lcom/twitter/android/widget/ae;)V

    .line 317
    :cond_0
    new-instance v1, Lcom/twitter/app/home/HomeTimelineFragment$2;

    invoke-direct {v1, p0}, Lcom/twitter/app/home/HomeTimelineFragment$2;-><init>(Lcom/twitter/app/home/HomeTimelineFragment;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/cv;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 324
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->w:Laot;

    if-eqz v0, :cond_1

    .line 325
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->w:Laot;

    invoke-virtual {v0}, Laot;->b()V

    .line 327
    :cond_1
    new-instance v0, Laot;

    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x1

    new-instance v3, Lash;

    .line 328
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-direct {v3, v4, v6, v7}, Lash;-><init>(Landroid/content/Context;J)V

    invoke-direct {v0, v1, v2, v3}, Laot;-><init>(Landroid/support/v4/app/LoaderManager;ILcom/twitter/util/object/j;)V

    iput-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->w:Laot;

    .line 329
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->w:Laot;

    iget-object v1, p0, Lcom/twitter/app/home/HomeTimelineFragment;->o:Lcom/twitter/android/revenue/c;

    invoke-virtual {v0, v1}, Laot;->a(Laow;)V

    .line 330
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 184
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f130367

    if-ne v0, v1, :cond_0

    .line 185
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/people/PeopleDiscoveryActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/app/home/HomeTimelineFragment;->startActivity(Landroid/content/Intent;)V

    .line 187
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->aY()V

    .line 192
    invoke-super {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->onCreate(Landroid/os/Bundle;)V

    .line 193
    if-eqz p1, :cond_0

    .line 194
    const-string/jumbo v0, "timeline_view_limit"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/home/HomeTimelineFragment;->k(I)V

    .line 198
    :goto_0
    invoke-direct {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->aX()V

    .line 199
    new-instance v0, Lcom/twitter/ui/view/h$a;

    invoke-direct {v0}, Lcom/twitter/ui/view/h$a;-><init>()V

    const/4 v1, 0x1

    .line 200
    invoke-virtual {v0, v1}, Lcom/twitter/ui/view/h$a;->h(Z)Lcom/twitter/ui/view/h$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/ui/view/h$a;->a()Lcom/twitter/ui/view/h;

    move-result-object v0

    .line 199
    invoke-virtual {p0, v0}, Lcom/twitter/app/home/HomeTimelineFragment;->a(Lcom/twitter/ui/view/h;)V

    .line 201
    new-instance v0, Lcom/twitter/app/home/d;

    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->i()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/app/home/d;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->b:Lcom/twitter/app/home/d;

    .line 203
    new-instance v0, Lcnq;

    new-instance v1, Lcom/twitter/util/a;

    .line 204
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    const-string/jumbo v3, "timeline"

    invoke-direct {v1, v2, v4, v5, v3}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcnq;-><init>(Lcom/twitter/util/a;I)V

    iput-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->y:Lcnl;

    .line 205
    return-void

    .line 196
    :cond_0
    iget v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->q:I

    invoke-static {v0}, Lbpv;->b(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/home/HomeTimelineFragment;->k(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 621
    invoke-super {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->onDestroy()V

    .line 623
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->w:Laot;

    if-eqz v0, :cond_0

    .line 624
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->w:Laot;

    invoke-virtual {v0}, Laot;->b()V

    .line 626
    :cond_0
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 614
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->d:Lcom/twitter/android/timeline/br;

    invoke-interface {v0}, Lcom/twitter/android/timeline/br;->f()V

    .line 615
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->ag()Lanh;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/home/HomeTimelineFragment;->d:Lcom/twitter/android/timeline/br;

    invoke-virtual {v0, v1}, Lanh;->b(Ljava/lang/Object;)Lanh;

    .line 616
    invoke-super {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->onDestroyView()V

    .line 617
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 253
    invoke-super {p0, p1}, Lcom/twitter/app/common/timeline/TimelineFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 254
    const-string/jumbo v0, "timeline_view_limit"

    iget v1, p0, Lcom/twitter/app/home/HomeTimelineFragment;->v:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 255
    return-void
.end method

.method protected q_()V
    .locals 5

    .prologue
    .line 225
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->d:Lcom/twitter/android/timeline/br;

    invoke-interface {v0}, Lcom/twitter/android/timeline/br;->h()V

    .line 226
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->z:Lcom/twitter/android/metrics/e;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/e;->k()V

    .line 227
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->f:Lcom/twitter/android/widget/a;

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->f:Lcom/twitter/android/widget/a;

    invoke-virtual {v0}, Lcom/twitter/android/widget/a;->a()V

    .line 229
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->f:Lcom/twitter/android/widget/a;

    .line 232
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/app/home/HomeTimelineFragment;->d(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 233
    const-string/jumbo v0, "home:refresh"

    .line 234
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->aJ()Lcom/twitter/metrics/j;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/app/home/HomeTimelineFragment;->a_:J

    sget-object v4, Lcom/twitter/metrics/g;->n:Lcom/twitter/metrics/g$b;

    .line 233
    invoke-static {v0, v1, v2, v3, v4}, Lcom/twitter/metrics/e;->a(Ljava/lang/String;Lcom/twitter/metrics/j;JLcom/twitter/metrics/g$b;)Lcom/twitter/metrics/e;

    move-result-object v0

    .line 236
    invoke-virtual {v0}, Lcom/twitter/metrics/e;->k()V

    .line 238
    :cond_1
    invoke-super {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->q_()V

    .line 239
    return-void
.end method

.method public r_()V
    .locals 1

    .prologue
    .line 885
    const-string/jumbo v0, "activate"

    invoke-direct {p0, v0}, Lcom/twitter/app/home/HomeTimelineFragment;->b(Ljava/lang/String;)V

    .line 886
    return-void
.end method

.method public s()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 995
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->x()V

    .line 996
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 997
    invoke-virtual {p0}, Lcom/twitter/app/home/HomeTimelineFragment;->i()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v4, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "new_tweet_prompt"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object v4, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 996
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 998
    return-void
.end method

.method public s_()V
    .locals 1

    .prologue
    .line 890
    const-string/jumbo v0, "focus"

    invoke-direct {p0, v0}, Lcom/twitter/app/home/HomeTimelineFragment;->b(Ljava/lang/String;)V

    .line 891
    return-void
.end method

.method t()V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 713
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->y:Lcnl;

    invoke-interface {v0}, Lcnl;->b()V

    .line 714
    return-void
.end method

.method public t_()V
    .locals 1

    .prologue
    .line 895
    const-string/jumbo v0, "unfocus"

    invoke-direct {p0, v0}, Lcom/twitter/app/home/HomeTimelineFragment;->c(Ljava/lang/String;)V

    .line 896
    return-void
.end method

.method public u_()V
    .locals 1

    .prologue
    .line 900
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->x:Z

    .line 901
    invoke-super {p0}, Lcom/twitter/app/common/timeline/TimelineFragment;->u_()V

    .line 902
    return-void
.end method

.method public x()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 587
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->T:Landroid/content/Context;

    invoke-static {v0}, Lbgd;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 588
    :goto_0
    const-string/jumbo v3, "PTR Override: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 587
    goto :goto_0
.end method

.method protected z()V
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lcom/twitter/app/home/HomeTimelineFragment;->z:Lcom/twitter/android/metrics/e;

    invoke-virtual {v0}, Lcom/twitter/android/metrics/e;->aS_()V

    .line 384
    return-void
.end method
