.class public Lcom/twitter/app/home/a;
.super Lcom/twitter/library/client/g;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/app/home/a;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StaticFieldLeak"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    sput-object v0, Lcom/twitter/app/home/a;->a:Lcom/twitter/app/home/a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/twitter/library/client/g;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/twitter/app/home/a;->b:Landroid/content/Context;

    .line 39
    return-void
.end method

.method public static declared-synchronized a()Lcom/twitter/app/home/a;
    .locals 3

    .prologue
    .line 43
    const-class v1, Lcom/twitter/app/home/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/app/home/a;->a:Lcom/twitter/app/home/a;

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "Instance has not been set"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 46
    :cond_0
    :try_start_1
    sget-object v0, Lcom/twitter/app/home/a;->a:Lcom/twitter/app/home/a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method public static a(Lcom/twitter/app/home/a;)V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/twitter/app/home/a;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 51
    sput-object p0, Lcom/twitter/app/home/a;->a:Lcom/twitter/app/home/a;

    .line 52
    return-void
.end method

.method private static a(Lcom/twitter/util/a;)V
    .locals 2

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v1, "home_jump_to_top_permitted"

    .line 121
    invoke-virtual {v0, v1}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;)Lcom/twitter/util/a$a;

    move-result-object v0

    .line 122
    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 123
    return-void
.end method

.method private e(Lcom/twitter/library/client/Session;)Lcom/twitter/util/a;
    .locals 4

    .prologue
    .line 106
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    new-instance v0, Lcom/twitter/util/a;

    iget-object v1, p0, Lcom/twitter/app/home/a;->b:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    .line 109
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/twitter/app/home/a;->e(Lcom/twitter/library/client/Session;)Lcom/twitter/util/a;

    move-result-object v0

    .line 114
    if-eqz v0, :cond_0

    .line 115
    invoke-static {v0}, Lcom/twitter/app/home/a;->a(Lcom/twitter/util/a;)V

    .line 117
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;J)V
    .locals 4

    .prologue
    .line 62
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 63
    invoke-static {v0, v1}, Lbpn;->b(J)F

    move-result v0

    .line 64
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v1, p2, v2

    if-ltz v1, :cond_0

    .line 65
    long-to-float v1, p2

    const v2, 0x476a6000    # 60000.0f

    mul-float/2addr v0, v2

    cmpl-float v0, v1, v0

    if-ltz v0, :cond_0

    .line 66
    invoke-direct {p0, p1}, Lcom/twitter/app/home/a;->e(Lcom/twitter/library/client/Session;)Lcom/twitter/util/a;

    move-result-object v0

    .line 67
    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {v0}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v0

    const-string/jumbo v1, "home_jump_to_top_permitted"

    const/4 v2, 0x1

    .line 69
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;Z)Lcom/twitter/util/a$a;

    move-result-object v0

    .line 70
    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 74
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Z)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/twitter/app/home/a;->f(Lcom/twitter/library/client/Session;)V

    .line 80
    return-void
.end method

.method public a_(Lcom/twitter/library/client/Session;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 95
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 96
    invoke-direct {p0, p1}, Lcom/twitter/app/home/a;->e(Lcom/twitter/library/client/Session;)Lcom/twitter/util/a;

    move-result-object v1

    .line 97
    if-eqz v1, :cond_0

    const-string/jumbo v2, "home_jump_to_top_permitted"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/util/a;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 98
    invoke-static {v1}, Lcom/twitter/app/home/a;->a(Lcom/twitter/util/a;)V

    .line 99
    const/4 v0, 0x1

    .line 101
    :cond_0
    return v0
.end method
