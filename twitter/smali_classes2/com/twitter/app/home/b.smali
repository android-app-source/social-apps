.class public Lcom/twitter/app/home/b;
.super Lcom/twitter/util/android/d;
.source "Twttr"


# instance fields
.field private final a:Lapb;

.field private final b:Lcom/twitter/android/revenue/c;

.field private final c:J

.field private final d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lapb;Lcom/twitter/android/revenue/c;Lapb;JZ)V
    .locals 7

    .prologue
    .line 24
    iget-object v2, p2, Lapb;->d:Landroid/net/Uri;

    iget-object v3, p2, Lapb;->e:[Ljava/lang/String;

    iget-object v4, p2, Lapb;->a:Ljava/lang/String;

    iget-object v5, p2, Lapb;->b:[Ljava/lang/String;

    iget-object v6, p2, Lapb;->c:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    iput-object p3, p0, Lcom/twitter/app/home/b;->b:Lcom/twitter/android/revenue/c;

    .line 26
    iput-object p4, p0, Lcom/twitter/app/home/b;->a:Lapb;

    .line 27
    iput-wide p5, p0, Lcom/twitter/app/home/b;->c:J

    .line 28
    iput-boolean p7, p0, Lcom/twitter/app/home/b;->d:Z

    .line 29
    return-void
.end method


# virtual methods
.method public loadInBackground()Landroid/database/Cursor;
    .locals 4

    .prologue
    .line 33
    invoke-super {p0}, Lcom/twitter/util/android/d;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    .line 34
    iget-boolean v1, p0, Lcom/twitter/app/home/b;->d:Z

    if-eqz v1, :cond_0

    .line 35
    iget-object v1, p0, Lcom/twitter/app/home/b;->b:Lcom/twitter/android/revenue/c;

    iget-wide v2, p0, Lcom/twitter/app/home/b;->c:J

    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/home/b;->a:Lapb;

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/revenue/c;->a(Lcom/twitter/library/provider/t;Lapb;)V

    .line 37
    :cond_0
    return-object v0
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/twitter/app/home/b;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
