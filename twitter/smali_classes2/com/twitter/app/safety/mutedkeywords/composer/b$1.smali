.class Lcom/twitter/app/safety/mutedkeywords/composer/b$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lasv$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/safety/mutedkeywords/composer/b;->a(Lcgm;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/safety/mutedkeywords/composer/b;


# direct methods
.method constructor <init>(Lcom/twitter/app/safety/mutedkeywords/composer/b;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/twitter/app/safety/mutedkeywords/composer/b$1;->a:Lcom/twitter/app/safety/mutedkeywords/composer/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcgm;)V
    .locals 4

    .prologue
    .line 66
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/composer/b$1;->a:Lcom/twitter/app/safety/mutedkeywords/composer/b;

    invoke-static {v1}, Lcom/twitter/app/safety/mutedkeywords/composer/b;->a(Lcom/twitter/app/safety/mutedkeywords/composer/b;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "settings"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "notifications"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "mute_keyword"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "add"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "mute"

    aput-object v3, v1, v2

    .line 67
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 66
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 69
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/b$1;->a:Lcom/twitter/app/safety/mutedkeywords/composer/b;

    invoke-static {v0, p1}, Lcom/twitter/app/safety/mutedkeywords/composer/b;->a(Lcom/twitter/app/safety/mutedkeywords/composer/b;Lcgm;)V

    .line 70
    return-void
.end method

.method public a(Lcgm;Lbfb;)V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/b$1;->a:Lcom/twitter/app/safety/mutedkeywords/composer/b;

    invoke-static {v0, p2}, Lcom/twitter/app/safety/mutedkeywords/composer/b;->a(Lcom/twitter/app/safety/mutedkeywords/composer/b;Lbfb;)V

    .line 75
    return-void
.end method
