.class public Lcom/twitter/app/safety/mutedkeywords/composer/e;
.super Laog;
.source "Twttr"

# interfaces
.implements Lala;
.implements Lasl$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/safety/mutedkeywords/composer/e$a;
    }
.end annotation


# static fields
.field private static final a:Lasi;

.field private static final b:Lasj;


# instance fields
.field private final c:Lcom/twitter/app/safety/mutedkeywords/composer/b;

.field private final d:Lcom/twitter/app/safety/mutedkeywords/composer/e$a;

.field private final e:Lasl;

.field private final f:Lml;

.field private final g:Lnl;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56
    new-instance v0, Lasi;

    const/16 v1, 0x8c

    invoke-direct {v0, v1}, Lasi;-><init>(I)V

    sput-object v0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->a:Lasi;

    .line 58
    new-instance v0, Lasj;

    invoke-direct {v0}, Lasj;-><init>()V

    sput-object v0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->b:Lasj;

    return-void
.end method

.method public constructor <init>(Landroid/view/LayoutInflater;Lcom/twitter/app/safety/mutedkeywords/composer/b;Lcom/twitter/library/client/Session;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 72
    invoke-direct {p0}, Laog;-><init>()V

    .line 65
    new-instance v0, Lnl;

    invoke-direct {v0}, Lnl;-><init>()V

    .line 66
    invoke-virtual {v0, v7}, Lnl;->b(Z)Lnl;

    move-result-object v0

    .line 67
    invoke-virtual {v0, v7}, Lnl;->a(Z)Lnl;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->g:Lnl;

    .line 73
    iput-object p2, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->c:Lcom/twitter/app/safety/mutedkeywords/composer/b;

    .line 76
    const v0, 0x7f040222

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 77
    invoke-virtual {p0, v0}, Lcom/twitter/app/safety/mutedkeywords/composer/e;->a(Landroid/view/View;)V

    .line 78
    invoke-static {v0}, Lcom/twitter/app/safety/mutedkeywords/composer/e;->b(Landroid/view/View;)Lcom/twitter/app/safety/mutedkeywords/composer/e$a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->d:Lcom/twitter/app/safety/mutedkeywords/composer/e$a;

    .line 79
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->d:Lcom/twitter/app/safety/mutedkeywords/composer/e$a;

    iget-object v0, v0, Lcom/twitter/app/safety/mutedkeywords/composer/e$a;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 82
    new-instance v1, Lasm;

    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v2

    invoke-direct {v1, v2}, Lasm;-><init>(Lcrr;)V

    .line 83
    new-instance v2, Lasn;

    invoke-direct {v2}, Lasn;-><init>()V

    .line 85
    new-instance v3, Lasl;

    invoke-direct {v3}, Lasl;-><init>()V

    iget-object v4, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->d:Lcom/twitter/app/safety/mutedkeywords/composer/e$a;

    iget-object v4, v4, Lcom/twitter/app/safety/mutedkeywords/composer/e$a;->b:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    const v5, 0x7f0a08c0

    .line 86
    invoke-virtual {v3, v4, v1, v5}, Lasl;->a(Lcom/twitter/ui/widget/TwitterEditText;Lask;I)Lasl;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->d:Lcom/twitter/app/safety/mutedkeywords/composer/e$a;

    iget-object v3, v3, Lcom/twitter/app/safety/mutedkeywords/composer/e$a;->b:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    sget-object v4, Lcom/twitter/app/safety/mutedkeywords/composer/e;->a:Lasi;

    const v5, 0x7f0a05bb

    .line 89
    invoke-virtual {v1, v3, v4, v5}, Lasl;->a(Lcom/twitter/ui/widget/TwitterEditText;Lask;I)Lasl;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->d:Lcom/twitter/app/safety/mutedkeywords/composer/e$a;

    iget-object v3, v3, Lcom/twitter/app/safety/mutedkeywords/composer/e$a;->b:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    sget-object v4, Lcom/twitter/app/safety/mutedkeywords/composer/e;->b:Lasj;

    const v5, 0x7f0a05bc

    .line 92
    invoke-virtual {v1, v3, v4, v5}, Lasl;->a(Lcom/twitter/ui/widget/TwitterEditText;Lask;I)Lasl;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->d:Lcom/twitter/app/safety/mutedkeywords/composer/e$a;

    iget-object v3, v3, Lcom/twitter/app/safety/mutedkeywords/composer/e$a;->b:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    .line 95
    invoke-virtual {v1, v3, v2, v6}, Lasl;->a(Lcom/twitter/ui/widget/TwitterEditText;Lask;I)Lasl;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->e:Lasl;

    .line 99
    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->e:Lasl;

    invoke-virtual {v1, p0}, Lasl;->a(Lasl$e;)V

    .line 100
    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->e:Lasl;

    new-instance v2, Lcom/twitter/app/safety/mutedkeywords/composer/e$1;

    invoke-direct {v2, p0}, Lcom/twitter/app/safety/mutedkeywords/composer/e$1;-><init>(Lcom/twitter/app/safety/mutedkeywords/composer/e;)V

    invoke-virtual {v1, v2}, Lasl;->a(Lasl$a;)V

    .line 107
    new-array v1, v7, [Ljava/lang/Object;

    const v2, 0x7f0a0bca

    const v3, 0x7f1100c9

    const v4, 0x7f1100c8

    const-class v5, Lcom/twitter/android/WebViewActivity;

    .line 108
    invoke-static {v0, v2, v3, v4, v5}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;IIILjava/lang/Class;)Lcom/twitter/ui/view/a;

    move-result-object v2

    aput-object v2, v1, v6

    .line 113
    invoke-direct {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/e;->j()V

    .line 115
    iget-object v2, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->d:Lcom/twitter/app/safety/mutedkeywords/composer/e$a;

    iget-object v2, v2, Lcom/twitter/app/safety/mutedkeywords/composer/e$a;->d:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/twitter/ui/view/g;->a(Landroid/widget/TextView;)V

    .line 116
    iget-object v2, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->d:Lcom/twitter/app/safety/mutedkeywords/composer/e$a;

    iget-object v2, v2, Lcom/twitter/app/safety/mutedkeywords/composer/e$a;->d:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->d:Lcom/twitter/app/safety/mutedkeywords/composer/e$a;

    iget-object v3, v3, Lcom/twitter/app/safety/mutedkeywords/composer/e$a;->d:Landroid/widget/TextView;

    .line 117
    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "{{}}"

    invoke-static {v1, v3, v4}, Lcom/twitter/library/util/af;->a([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    new-instance v1, Lml;

    invoke-direct {v1, v0}, Lml;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->f:Lml;

    .line 120
    invoke-direct {p0, v0, p3}, Lcom/twitter/app/safety/mutedkeywords/composer/e;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 121
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/safety/mutedkeywords/composer/e;)Lasl;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->e:Lasl;

    return-object v0
.end method

.method private a(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 3

    .prologue
    .line 227
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->d:Lcom/twitter/app/safety/mutedkeywords/composer/e$a;

    iget-object v0, v0, Lcom/twitter/app/safety/mutedkeywords/composer/e$a;->b:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    .line 228
    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->g:Lnl;

    invoke-virtual {v0, v1}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->setTokenizer(Lnj;)V

    .line 229
    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->d:Lcom/twitter/app/safety/mutedkeywords/composer/e$a;

    iget-object v1, v1, Lcom/twitter/app/safety/mutedkeywords/composer/e$a;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->setListView(Landroid/widget/ListView;)V

    .line 230
    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->f:Lml;

    invoke-virtual {v0, v1}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->setAdapter(Lmq;)V

    .line 231
    new-instance v1, Lcom/twitter/app/safety/mutedkeywords/composer/e$3;

    invoke-direct {v1, p0}, Lcom/twitter/app/safety/mutedkeywords/composer/e$3;-><init>(Lcom/twitter/app/safety/mutedkeywords/composer/e;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->setSuggestionListener(Lcom/twitter/android/autocomplete/SuggestionEditText$e;)V

    .line 251
    new-instance v1, Lcom/twitter/app/safety/mutedkeywords/composer/e$4;

    invoke-direct {v1, p0}, Lcom/twitter/app/safety/mutedkeywords/composer/e$4;-><init>(Lcom/twitter/app/safety/mutedkeywords/composer/e;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->setSuggestionStringConverter(Lcom/twitter/android/autocomplete/SuggestionEditText$f;)V

    .line 260
    invoke-static {}, Lbph;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 261
    new-instance v1, Lmv;

    new-instance v2, Lcom/twitter/app/safety/mutedkeywords/composer/i;

    invoke-direct {v2, p1, p2}, Lcom/twitter/app/safety/mutedkeywords/composer/i;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-direct {v1, p1, v2}, Lmv;-><init>(Landroid/content/Context;Lnd;)V

    .line 267
    invoke-virtual {v0, v1}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->setSuggestionProvider(Lna;)V

    .line 269
    :cond_0
    return-void
.end method

.method private static b(Landroid/view/View;)Lcom/twitter/app/safety/mutedkeywords/composer/e$a;
    .locals 4

    .prologue
    .line 218
    const v0, 0x7f130557

    .line 219
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    .line 220
    const v1, 0x7f130559

    .line 221
    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 222
    const v2, 0x7f130558

    invoke-virtual {p0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 223
    new-instance v3, Lcom/twitter/app/safety/mutedkeywords/composer/e$a;

    invoke-direct {v3, p0, v0, v1, v2}, Lcom/twitter/app/safety/mutedkeywords/composer/e$a;-><init>(Landroid/view/View;Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;Landroid/widget/ListView;Landroid/widget/TextView;)V

    return-object v3
.end method

.method static synthetic i()Lasj;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->b:Lasj;

    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 124
    invoke-static {}, Lbph;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/e;->aN_()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0}, Lcom/twitter/util/s;->c(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 127
    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->c:Lcom/twitter/app/safety/mutedkeywords/composer/b;

    invoke-virtual {v1, v0}, Lcom/twitter/app/safety/mutedkeywords/composer/b;->a(Ljava/lang/String;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/safety/mutedkeywords/composer/e$2;

    invoke-direct {v1, p0}, Lcom/twitter/app/safety/mutedkeywords/composer/e$2;-><init>(Lcom/twitter/app/safety/mutedkeywords/composer/e;)V

    .line 128
    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/h;)Lrx/j;

    .line 144
    :cond_0
    return-void
.end method

.method private k()V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->e:Lasl;

    invoke-virtual {v0}, Lasl;->a()V

    .line 172
    return-void
.end method

.method private l()Lcgm;
    .locals 11

    .prologue
    .line 190
    invoke-direct {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/e;->m()Ljava/lang/String;

    move-result-object v5

    .line 191
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->c:Lcom/twitter/app/safety/mutedkeywords/composer/b;

    invoke-virtual {v0}, Lcom/twitter/app/safety/mutedkeywords/composer/b;->b()Lcgm;

    move-result-object v0

    .line 192
    if-eqz v0, :cond_0

    .line 193
    new-instance v1, Lcgm;

    iget-wide v2, v0, Lcgm;->b:J

    iget-object v4, v0, Lcgm;->c:Ljava/lang/String;

    iget-wide v6, v0, Lcgm;->e:J

    iget-wide v8, v0, Lcgm;->f:J

    iget-boolean v10, v0, Lcgm;->g:Z

    invoke-direct/range {v1 .. v10}, Lcgm;-><init>(JLjava/lang/String;Ljava/lang/String;JJZ)V

    .line 196
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcgm;

    invoke-direct {v1, v5}, Lcgm;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->d:Lcom/twitter/app/safety/mutedkeywords/composer/e$a;

    iget-object v0, v0, Lcom/twitter/app/safety/mutedkeywords/composer/e$a;->b:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/app/common/di/scope/InjectionScope;)V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/e;->k()V

    .line 153
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->d:Lcom/twitter/app/safety/mutedkeywords/composer/e$a;

    iget-object v0, v0, Lcom/twitter/app/safety/mutedkeywords/composer/e$a;->b:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    invoke-virtual {v0, p1}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->setError(Ljava/lang/CharSequence;)V

    .line 186
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->c:Lcom/twitter/app/safety/mutedkeywords/composer/b;

    invoke-virtual {v0, p1}, Lcom/twitter/app/safety/mutedkeywords/composer/b;->a(Z)V

    .line 177
    return-void
.end method

.method protected a(I)Z
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 157
    const/4 v1, 0x5

    if-ne p1, v1, :cond_1

    .line 158
    invoke-virtual {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/e;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 159
    invoke-virtual {p0, v0}, Lcom/twitter/app/safety/mutedkeywords/composer/e;->b(Z)V

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->c:Lcom/twitter/app/safety/mutedkeywords/composer/b;

    invoke-virtual {v0}, Lcom/twitter/app/safety/mutedkeywords/composer/b;->d()V

    .line 162
    const/4 v0, 0x1

    .line 164
    :cond_1
    return v0
.end method

.method protected b(Z)V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 212
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->d:Lcom/twitter/app/safety/mutedkeywords/composer/e$a;

    iget-object v0, v0, Lcom/twitter/app/safety/mutedkeywords/composer/e$a;->b:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->d:Lcom/twitter/app/safety/mutedkeywords/composer/e$a;

    iget-object v1, v1, Lcom/twitter/app/safety/mutedkeywords/composer/e$a;->b:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    invoke-static {v0, v1, p1}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    .line 214
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->d:Lcom/twitter/app/safety/mutedkeywords/composer/e$a;

    iget-object v0, v0, Lcom/twitter/app/safety/mutedkeywords/composer/e$a;->b:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->b()V

    .line 148
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/e;->l()Lcgm;

    move-result-object v0

    .line 181
    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/composer/e;->c:Lcom/twitter/app/safety/mutedkeywords/composer/b;

    invoke-virtual {v1, v0}, Lcom/twitter/app/safety/mutedkeywords/composer/b;->a(Lcgm;)V

    .line 182
    return-void
.end method

.method protected h()Z
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/e;->aN_()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/d;->f(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
