.class public final Lcom/twitter/app/safety/mutedkeywords/composer/a$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/safety/mutedkeywords/composer/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private a:Lamu;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/app/safety/mutedkeywords/composer/a$1;)V
    .locals 0

    .prologue
    .line 214
    invoke-direct {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/a$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/safety/mutedkeywords/composer/a$a;)Lamu;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a$a;->a:Lamu;

    return-object v0
.end method


# virtual methods
.method public a(Lamu;)Lcom/twitter/app/safety/mutedkeywords/composer/a$a;
    .locals 1

    .prologue
    .line 250
    invoke-static {p1}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamu;

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a$a;->a:Lamu;

    .line 251
    return-object p0
.end method

.method public a()Lcom/twitter/app/safety/mutedkeywords/composer/d;
    .locals 3

    .prologue
    .line 220
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a$a;->a:Lamu;

    if-nez v0, :cond_0

    .line 221
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lamu;

    .line 222
    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 224
    :cond_0
    new-instance v0, Lcom/twitter/app/safety/mutedkeywords/composer/a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/app/safety/mutedkeywords/composer/a;-><init>(Lcom/twitter/app/safety/mutedkeywords/composer/a$a;Lcom/twitter/app/safety/mutedkeywords/composer/a$1;)V

    return-object v0
.end method
