.class public Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/safety/mutedkeywords/composer/b$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity$b;,
        Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity$a;
    }
.end annotation


# instance fields
.field private a:Lakq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lakq",
            "<",
            "Lcgm;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/twitter/app/common/dialog/ProgressDialogFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method

.method private l()Lcom/twitter/app/safety/mutedkeywords/composer/b;
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->X()Lann;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/safety/mutedkeywords/composer/d;

    .line 112
    invoke-interface {v0}, Lcom/twitter/app/safety/mutedkeywords/composer/d;->d()Lcom/twitter/app/safety/mutedkeywords/composer/b;

    move-result-object v0

    return-object v0
.end method

.method private n()V
    .locals 1

    .prologue
    .line 154
    const v0, 0x7f0a0a35

    invoke-virtual {p0, v0}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->a(I)V

    .line 155
    invoke-direct {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->o()Lcom/twitter/app/safety/mutedkeywords/composer/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/safety/mutedkeywords/composer/e;->g()V

    .line 156
    return-void
.end method

.method private o()Lcom/twitter/app/safety/mutedkeywords/composer/e;
    .locals 1

    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->Z()Laog;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/safety/mutedkeywords/composer/e;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 46
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(I)V

    .line 47
    invoke-virtual {p2, v2}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 48
    const/16 v0, 0xc

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(I)V

    .line 49
    invoke-virtual {p2, v2}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(Z)V

    .line 50
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 51
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lank;)Lcom/twitter/app/common/base/j;
    .locals 2

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->X()Lann;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/safety/mutedkeywords/composer/d;

    .line 66
    new-instance v1, Lant;

    invoke-direct {v1, p0, p1}, Lant;-><init>(Landroid/app/Activity;Lank;)V

    invoke-interface {v0, v1}, Lcom/twitter/app/safety/mutedkeywords/composer/d;->a(Lant;)Lcom/twitter/app/safety/mutedkeywords/composer/g;

    move-result-object v0

    return-object v0
.end method

.method protected a(I)V
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    .line 160
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->b:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    if-nez v0, :cond_0

    .line 161
    invoke-static {p1}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->a(I)Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->b:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    .line 162
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->b:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->setRetainInstance(Z)V

    .line 163
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->b:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    invoke-virtual {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 165
    :cond_0
    return-void
.end method

.method public a(Lbfb;)V
    .locals 2

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->j()V

    .line 124
    invoke-direct {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->o()Lcom/twitter/app/safety/mutedkeywords/composer/e;

    move-result-object v0

    iget-object v1, p1, Lbfb;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/app/safety/mutedkeywords/composer/e;->a(Ljava/lang/String;)V

    .line 125
    return-void
.end method

.method public a(Lcgm;)V
    .locals 2

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->j()V

    .line 118
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->a:Lakq;

    const/4 v1, -0x1

    invoke-virtual {v0, v1, p1}, Lakq;->a(ILjava/lang/Object;)V

    .line 119
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->d()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 99
    if-eqz v0, :cond_0

    .line 100
    const v1, 0x7f13089c

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    invoke-virtual {v0, p1}, Lazv;->c(Z)Lazv;

    .line 102
    :cond_0
    return-void
.end method

.method public a(Lcmm;)Z
    .locals 2

    .prologue
    .line 143
    invoke-interface {p1}, Lcmm;->a()I

    move-result v0

    .line 144
    const v1, 0x7f13089c

    if-ne v0, v1, :cond_1

    .line 145
    invoke-direct {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->n()V

    .line 150
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 146
    :cond_1
    const v1, 0x7f130043

    if-ne v0, v1, :cond_0

    .line 147
    invoke-virtual {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    .line 148
    invoke-virtual {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->p()V

    goto :goto_0
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 129
    const v0, 0x7f14002e

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 130
    const/4 v0, 0x1

    return v0
.end method

.method public b(Lcmr;)I
    .locals 2

    .prologue
    .line 136
    invoke-interface {p1}, Lcmr;->k()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 137
    const v1, 0x7f13089c

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lazv;->c(Z)Lazv;

    .line 138
    const/4 v0, 0x2

    return v0
.end method

.method protected synthetic b(Lank;)Lcom/twitter/app/common/abs/b;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->a(Lank;)Lcom/twitter/app/common/base/j;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 2

    .prologue
    .line 71
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V

    .line 72
    const v0, 0x7f0a05ba

    invoke-virtual {p0, v0}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->setTitle(I)V

    .line 73
    new-instance v0, Lakq;

    new-instance v1, Lcom/twitter/app/safety/mutedkeywords/composer/h;

    invoke-direct {v1}, Lcom/twitter/app/safety/mutedkeywords/composer/h;-><init>()V

    invoke-direct {v0, p0, v1}, Lakq;-><init>(Landroid/app/Activity;Lakw;)V

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->a:Lakq;

    .line 74
    invoke-direct {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->l()Lcom/twitter/app/safety/mutedkeywords/composer/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/app/safety/mutedkeywords/composer/b;->a(Lcom/twitter/app/safety/mutedkeywords/composer/b$b;)Lcom/twitter/app/safety/mutedkeywords/composer/b;

    .line 75
    return-void
.end method

.method protected synthetic c(Lank;)Lans;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->a(Lank;)Lcom/twitter/app/common/base/j;

    move-result-object v0

    return-object v0
.end method

.method protected d(Lank;)Lcom/twitter/app/safety/mutedkeywords/composer/d;
    .locals 2

    .prologue
    .line 57
    invoke-static {}, Lcom/twitter/app/safety/mutedkeywords/composer/a;->a()Lcom/twitter/app/safety/mutedkeywords/composer/a$a;

    move-result-object v0

    .line 58
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/safety/mutedkeywords/composer/a$a;->a(Lamu;)Lcom/twitter/app/safety/mutedkeywords/composer/a$a;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lcom/twitter/app/safety/mutedkeywords/composer/a$a;->a()Lcom/twitter/app/safety/mutedkeywords/composer/d;

    move-result-object v0

    .line 57
    return-object v0
.end method

.method protected d()V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->l()Lcom/twitter/app/safety/mutedkeywords/composer/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/safety/mutedkeywords/composer/b;->a()V

    .line 80
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d()V

    .line 81
    return-void
.end method

.method protected synthetic e(Lank;)Lcom/twitter/app/common/base/i;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->d(Lank;)Lcom/twitter/app/safety/mutedkeywords/composer/d;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f(Lank;)Lcom/twitter/app/common/abs/a;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->d(Lank;)Lcom/twitter/app/safety/mutedkeywords/composer/d;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic g(Lank;)Lann;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->d(Lank;)Lcom/twitter/app/safety/mutedkeywords/composer/d;

    move-result-object v0

    return-object v0
.end method

.method public i()V
    .locals 0

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->n()V

    .line 107
    return-void
.end method

.method protected j()V
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->b:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->b:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/twitter/app/common/dialog/ProgressDialogFragment;->dismissAllowingStateLoss()V

    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->b:Lcom/twitter/app/common/dialog/ProgressDialogFragment;

    .line 172
    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 93
    invoke-direct {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity;->o()Lcom/twitter/app/safety/mutedkeywords/composer/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/safety/mutedkeywords/composer/e;->f()V

    .line 94
    return-void
.end method
