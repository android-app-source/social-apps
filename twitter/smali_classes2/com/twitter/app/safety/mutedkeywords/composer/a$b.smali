.class final Lcom/twitter/app/safety/mutedkeywords/composer/a$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/safety/mutedkeywords/composer/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/safety/mutedkeywords/composer/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/safety/mutedkeywords/composer/a;

.field private final b:Lant;

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/composer/e;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/twitter/app/safety/mutedkeywords/composer/a;Lant;)V
    .locals 1

    .prologue
    .line 267
    iput-object p1, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a$b;->a:Lcom/twitter/app/safety/mutedkeywords/composer/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 268
    invoke-static {p2}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lant;

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a$b;->b:Lant;

    .line 269
    invoke-direct {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/a$b;->c()V

    .line 270
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/app/safety/mutedkeywords/composer/a;Lant;Lcom/twitter/app/safety/mutedkeywords/composer/a$1;)V
    .locals 0

    .prologue
    .line 255
    invoke-direct {p0, p1, p2}, Lcom/twitter/app/safety/mutedkeywords/composer/a$b;-><init>(Lcom/twitter/app/safety/mutedkeywords/composer/a;Lant;)V

    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 275
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a$b;->b:Lant;

    .line 277
    invoke-static {v0}, Lanu;->a(Lant;)Ldagger/internal/c;

    move-result-object v0

    .line 276
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a$b;->c:Lcta;

    .line 279
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a$b;->c:Lcta;

    .line 281
    invoke-static {v0}, Lanx;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 280
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a$b;->d:Lcta;

    .line 286
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a$b;->d:Lcta;

    iget-object v2, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a$b;->a:Lcom/twitter/app/safety/mutedkeywords/composer/a;

    .line 290
    invoke-static {v2}, Lcom/twitter/app/safety/mutedkeywords/composer/a;->a(Lcom/twitter/app/safety/mutedkeywords/composer/a;)Lcta;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a$b;->a:Lcom/twitter/app/safety/mutedkeywords/composer/a;

    .line 291
    invoke-static {v3}, Lcom/twitter/app/safety/mutedkeywords/composer/a;->b(Lcom/twitter/app/safety/mutedkeywords/composer/a;)Lcta;

    move-result-object v3

    .line 285
    invoke-static {v0, v1, v2, v3}, Lcom/twitter/app/safety/mutedkeywords/composer/f;->a(Lcsd;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 284
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a$b;->e:Lcta;

    .line 293
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a$b;->e:Lcta;

    .line 294
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a$b;->f:Lcta;

    .line 295
    return-void
.end method


# virtual methods
.method public a()Laog;
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a$b;->f:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laog;

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 299
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a$b;->a:Lcom/twitter/app/safety/mutedkeywords/composer/a;

    invoke-static {v0}, Lcom/twitter/app/safety/mutedkeywords/composer/a;->c(Lcom/twitter/app/safety/mutedkeywords/composer/a;)Lcta;

    move-result-object v0

    .line 300
    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 299
    return-object v0
.end method
