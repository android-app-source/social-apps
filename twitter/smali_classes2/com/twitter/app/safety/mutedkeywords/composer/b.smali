.class public Lcom/twitter/app/safety/mutedkeywords/composer/b;
.super Lasv;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/safety/mutedkeywords/composer/b$a;,
        Lcom/twitter/app/safety/mutedkeywords/composer/b$c;,
        Lcom/twitter/app/safety/mutedkeywords/composer/b$b;
    }
.end annotation


# instance fields
.field private d:Lcgm;

.field private e:Z

.field private f:Lcom/twitter/app/safety/mutedkeywords/composer/b$b;


# direct methods
.method public constructor <init>(Lasw;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lasv;-><init>(Lasw;Lcom/twitter/library/client/Session;)V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/b;->e:Z

    .line 34
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/safety/mutedkeywords/composer/b;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/b;->b:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method private a(Lbfb;)V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/b;->f:Lcom/twitter/app/safety/mutedkeywords/composer/b$b;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/b;->f:Lcom/twitter/app/safety/mutedkeywords/composer/b$b;

    invoke-interface {v0, p1}, Lcom/twitter/app/safety/mutedkeywords/composer/b$b;->a(Lbfb;)V

    .line 124
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/safety/mutedkeywords/composer/b;Lbfb;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/twitter/app/safety/mutedkeywords/composer/b;->a(Lbfb;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/safety/mutedkeywords/composer/b;Lcgm;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/twitter/app/safety/mutedkeywords/composer/b;->b(Lcgm;)V

    return-void
.end method

.method private b(Lcgm;)V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/b;->f:Lcom/twitter/app/safety/mutedkeywords/composer/b$b;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/b;->f:Lcom/twitter/app/safety/mutedkeywords/composer/b$b;

    invoke-interface {v0, p1}, Lcom/twitter/app/safety/mutedkeywords/composer/b$b;->a(Lcgm;)V

    .line 113
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/app/safety/mutedkeywords/composer/b$b;)Lcom/twitter/app/safety/mutedkeywords/composer/b;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/twitter/app/safety/mutedkeywords/composer/b;->f:Lcom/twitter/app/safety/mutedkeywords/composer/b$b;

    .line 39
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/g",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcgl;",
            "Lcom/twitter/model/core/y;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/b;->a:Lasw;

    invoke-virtual {v0, p1}, Lasw;->a(Ljava/lang/String;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/b;->f:Lcom/twitter/app/safety/mutedkeywords/composer/b$b;

    .line 44
    return-void
.end method

.method public a(Lcgm;)V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/twitter/app/safety/mutedkeywords/composer/b$1;

    invoke-direct {v0, p0}, Lcom/twitter/app/safety/mutedkeywords/composer/b$1;-><init>(Lcom/twitter/app/safety/mutedkeywords/composer/b;)V

    invoke-virtual {p0, p1, v0}, Lcom/twitter/app/safety/mutedkeywords/composer/b;->a(Lcgm;Lasv$a;)V

    .line 77
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/twitter/app/safety/mutedkeywords/composer/b;->e:Z

    .line 90
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/b;->f:Lcom/twitter/app/safety/mutedkeywords/composer/b$b;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/b;->f:Lcom/twitter/app/safety/mutedkeywords/composer/b$b;

    invoke-interface {v0, p1}, Lcom/twitter/app/safety/mutedkeywords/composer/b$b;->a(Z)V

    .line 93
    :cond_0
    return-void
.end method

.method public b()Lcgm;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/b;->d:Lcgm;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/b;->e:Z

    return v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/twitter/app/safety/mutedkeywords/composer/b;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/b;->f:Lcom/twitter/app/safety/mutedkeywords/composer/b$b;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/b;->f:Lcom/twitter/app/safety/mutedkeywords/composer/b$b;

    invoke-interface {v0}, Lcom/twitter/app/safety/mutedkeywords/composer/b$b;->i()V

    .line 101
    :cond_0
    return-void
.end method
