.class public final Lcom/twitter/app/safety/mutedkeywords/composer/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/safety/mutedkeywords/composer/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/safety/mutedkeywords/composer/a$b;,
        Lcom/twitter/app/safety/mutedkeywords/composer/a$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lass;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laso;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lawb;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/String;",
            "Lcgl;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lasw;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/composer/b;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/abs/j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/twitter/app/safety/mutedkeywords/composer/a;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/twitter/app/safety/mutedkeywords/composer/a$a;)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    sget-boolean v0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 73
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/app/safety/mutedkeywords/composer/a;->a(Lcom/twitter/app/safety/mutedkeywords/composer/a$a;)V

    .line 74
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/app/safety/mutedkeywords/composer/a$a;Lcom/twitter/app/safety/mutedkeywords/composer/a$1;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/twitter/app/safety/mutedkeywords/composer/a;-><init>(Lcom/twitter/app/safety/mutedkeywords/composer/a$a;)V

    return-void
.end method

.method public static a()Lcom/twitter/app/safety/mutedkeywords/composer/a$a;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Lcom/twitter/app/safety/mutedkeywords/composer/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/app/safety/mutedkeywords/composer/a$a;-><init>(Lcom/twitter/app/safety/mutedkeywords/composer/a$1;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/app/safety/mutedkeywords/composer/a;)Lcta;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->j:Lcta;

    return-object v0
.end method

.method private a(Lcom/twitter/app/safety/mutedkeywords/composer/a$a;)V
    .locals 3

    .prologue
    .line 83
    new-instance v0, Lcom/twitter/app/safety/mutedkeywords/composer/a$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/app/safety/mutedkeywords/composer/a$1;-><init>(Lcom/twitter/app/safety/mutedkeywords/composer/a;Lcom/twitter/app/safety/mutedkeywords/composer/a$a;)V

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->b:Lcta;

    .line 96
    new-instance v0, Lcom/twitter/app/safety/mutedkeywords/composer/a$2;

    invoke-direct {v0, p0, p1}, Lcom/twitter/app/safety/mutedkeywords/composer/a$2;-><init>(Lcom/twitter/app/safety/mutedkeywords/composer/a;Lcom/twitter/app/safety/mutedkeywords/composer/a$a;)V

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->c:Lcta;

    .line 112
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->b:Lcta;

    iget-object v2, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->c:Lcta;

    .line 111
    invoke-static {v0, v1, v2}, Lasu;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 110
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->d:Lcta;

    .line 116
    new-instance v0, Lcom/twitter/app/safety/mutedkeywords/composer/a$3;

    invoke-direct {v0, p0, p1}, Lcom/twitter/app/safety/mutedkeywords/composer/a$3;-><init>(Lcom/twitter/app/safety/mutedkeywords/composer/a;Lcom/twitter/app/safety/mutedkeywords/composer/a$a;)V

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->e:Lcta;

    .line 132
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->e:Lcta;

    iget-object v2, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->c:Lcta;

    .line 131
    invoke-static {v0, v1, v2}, Lasp;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 130
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->f:Lcta;

    .line 136
    new-instance v0, Lcom/twitter/app/safety/mutedkeywords/composer/a$4;

    invoke-direct {v0, p0, p1}, Lcom/twitter/app/safety/mutedkeywords/composer/a$4;-><init>(Lcom/twitter/app/safety/mutedkeywords/composer/a;Lcom/twitter/app/safety/mutedkeywords/composer/a$a;)V

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->g:Lcta;

    .line 149
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->g:Lcta;

    .line 152
    invoke-static {v0}, Lasr;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 150
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->h:Lcta;

    .line 154
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->d:Lcta;

    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->f:Lcta;

    iget-object v2, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->h:Lcta;

    .line 156
    invoke-static {v0, v1, v2}, Lasx;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 155
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->i:Lcta;

    .line 164
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->i:Lcta;

    iget-object v2, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->c:Lcta;

    .line 163
    invoke-static {v0, v1, v2}, Lcom/twitter/app/safety/mutedkeywords/composer/c;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 162
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->j:Lcta;

    .line 168
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->j:Lcta;

    .line 169
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->k:Lcta;

    .line 171
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 172
    invoke-static {v0, v1}, Ldagger/internal/f;->a(II)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->k:Lcta;

    .line 173
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    .line 174
    invoke-virtual {v0}, Ldagger/internal/f$a;->a()Ldagger/internal/f;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->l:Lcta;

    .line 176
    new-instance v0, Lcom/twitter/app/safety/mutedkeywords/composer/a$5;

    invoke-direct {v0, p0, p1}, Lcom/twitter/app/safety/mutedkeywords/composer/a$5;-><init>(Lcom/twitter/app/safety/mutedkeywords/composer/a;Lcom/twitter/app/safety/mutedkeywords/composer/a$a;)V

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->m:Lcta;

    .line 189
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->m:Lcta;

    .line 191
    invoke-static {v0}, Lcom/twitter/app/common/abs/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 190
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->n:Lcta;

    .line 192
    return-void
.end method

.method static synthetic b(Lcom/twitter/app/safety/mutedkeywords/composer/a;)Lcta;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->c:Lcta;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/app/safety/mutedkeywords/composer/a;)Lcta;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->l:Lcta;

    return-object v0
.end method


# virtual methods
.method public a(Lant;)Lcom/twitter/app/safety/mutedkeywords/composer/g;
    .locals 2

    .prologue
    .line 211
    new-instance v0, Lcom/twitter/app/safety/mutedkeywords/composer/a$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/twitter/app/safety/mutedkeywords/composer/a$b;-><init>(Lcom/twitter/app/safety/mutedkeywords/composer/a;Lant;Lcom/twitter/app/safety/mutedkeywords/composer/a$1;)V

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 196
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->l:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public c()Lcom/twitter/app/common/abs/j;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->n:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/j;

    return-object v0
.end method

.method public d()Lcom/twitter/app/safety/mutedkeywords/composer/b;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/composer/a;->j:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/safety/mutedkeywords/composer/b;

    return-object v0
.end method
