.class public Lcom/twitter/app/safety/mutedkeywords/composer/h;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laku;
.implements Lakw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Laku",
        "<",
        "Lcgm;",
        ">;",
        "Lakw",
        "<",
        "Lcgm;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Intent;)Lcgm;
    .locals 2

    .prologue
    .line 19
    if-nez p1, :cond_0

    .line 20
    const/4 v0, 0x0

    .line 22
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "extra_keyword"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    sget-object v1, Lcgm;->a:Lcom/twitter/util/serialization/i;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgm;

    goto :goto_0
.end method

.method public a(Landroid/content/Intent;Lcgm;)V
    .locals 2

    .prologue
    .line 27
    if-nez p1, :cond_0

    .line 31
    :goto_0
    return-void

    .line 30
    :cond_0
    const-string/jumbo v0, "extra_keyword"

    sget-object v1, Lcgm;->a:Lcom/twitter/util/serialization/i;

    invoke-static {p2, v1}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    goto :goto_0
.end method

.method public bridge synthetic a(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 13
    check-cast p2, Lcgm;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/safety/mutedkeywords/composer/h;->a(Landroid/content/Intent;Lcgm;)V

    return-void
.end method

.method public synthetic b(Landroid/content/Intent;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lcom/twitter/app/safety/mutedkeywords/composer/h;->a(Landroid/content/Intent;)Lcgm;

    move-result-object v0

    return-object v0
.end method
