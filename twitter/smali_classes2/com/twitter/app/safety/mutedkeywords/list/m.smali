.class public Lcom/twitter/app/safety/mutedkeywords/list/m;
.super Laog;
.source "Twttr"

# interfaces
.implements Lakv;
.implements Lcom/twitter/app/safety/mutedkeywords/list/l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laog;",
        "Lakv",
        "<",
        "Lcgm;",
        ">;",
        "Lcom/twitter/app/safety/mutedkeywords/list/l;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/app/safety/mutedkeywords/list/j;

.field private final b:Lcom/twitter/app/safety/mutedkeywords/list/e;

.field private final c:Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity$b;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Lcom/twitter/app/safety/mutedkeywords/list/j;Lcom/twitter/app/safety/mutedkeywords/list/u;Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity$b;Lank;Landroid/support/v4/app/FragmentManager;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 53
    invoke-direct {p0}, Laog;-><init>()V

    .line 54
    const v0, 0x7f040226

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 56
    const v0, 0x7f1304c9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 57
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 58
    new-instance v3, Landroid/support/v7/widget/LinearLayoutManager;

    const/4 v4, 0x1

    invoke-direct {v3, v2, v4, v5}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 59
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 60
    const v4, 0x7f0206f2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 61
    new-instance v4, Lcom/twitter/app/safety/mutedkeywords/list/i;

    invoke-direct {v4, v3}, Lcom/twitter/app/safety/mutedkeywords/list/i;-><init>(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 63
    invoke-virtual {p0, v1}, Lcom/twitter/app/safety/mutedkeywords/list/m;->a(Landroid/view/View;)V

    .line 65
    new-instance v1, Lcom/twitter/app/safety/mutedkeywords/list/b;

    invoke-direct {v1, v2}, Lcom/twitter/app/safety/mutedkeywords/list/b;-><init>(Landroid/content/Context;)V

    .line 66
    new-instance v2, Lcom/twitter/app/safety/mutedkeywords/list/e;

    invoke-direct {v2}, Lcom/twitter/app/safety/mutedkeywords/list/e;-><init>()V

    iput-object v2, p0, Lcom/twitter/app/safety/mutedkeywords/list/m;->b:Lcom/twitter/app/safety/mutedkeywords/list/e;

    .line 67
    iget-object v2, p0, Lcom/twitter/app/safety/mutedkeywords/list/m;->b:Lcom/twitter/app/safety/mutedkeywords/list/e;

    invoke-virtual {v2, v1}, Lcom/twitter/app/safety/mutedkeywords/list/e;->a(Lcom/twitter/app/safety/mutedkeywords/list/b;)V

    .line 68
    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/list/m;->b:Lcom/twitter/app/safety/mutedkeywords/list/e;

    new-instance v2, Lcom/twitter/app/safety/mutedkeywords/list/m$1;

    invoke-direct {v2, p0}, Lcom/twitter/app/safety/mutedkeywords/list/m$1;-><init>(Lcom/twitter/app/safety/mutedkeywords/list/m;)V

    invoke-virtual {v1, v2}, Lcom/twitter/app/safety/mutedkeywords/list/e;->a(Lcom/twitter/app/safety/mutedkeywords/list/e$a;)V

    .line 76
    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/list/m;->b:Lcom/twitter/app/safety/mutedkeywords/list/e;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 78
    iput-object p2, p0, Lcom/twitter/app/safety/mutedkeywords/list/m;->a:Lcom/twitter/app/safety/mutedkeywords/list/j;

    .line 79
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/m;->a:Lcom/twitter/app/safety/mutedkeywords/list/j;

    invoke-virtual {v0, p0}, Lcom/twitter/app/safety/mutedkeywords/list/j;->a(Lcom/twitter/app/safety/mutedkeywords/list/l;)V

    .line 81
    invoke-direct {p0}, Lcom/twitter/app/safety/mutedkeywords/list/m;->f()V

    .line 83
    iput-object p4, p0, Lcom/twitter/app/safety/mutedkeywords/list/m;->c:Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity$b;

    .line 84
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/m;->c:Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity$b;

    invoke-virtual {v0, p0}, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity$b;->a(Lakv;)V

    .line 86
    invoke-virtual {p0}, Lcom/twitter/app/safety/mutedkeywords/list/m;->aN_()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f13055d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 87
    new-instance v1, Lcom/twitter/app/safety/mutedkeywords/list/m$2;

    invoke-direct {v1, p0}, Lcom/twitter/app/safety/mutedkeywords/list/m$2;-><init>(Lcom/twitter/app/safety/mutedkeywords/list/m;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    invoke-virtual {p3, p5, p6}, Lcom/twitter/app/safety/mutedkeywords/list/u;->a(Lank;Landroid/support/v4/app/FragmentManager;)V

    .line 94
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/safety/mutedkeywords/list/m;)Lcom/twitter/app/safety/mutedkeywords/list/j;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/m;->a:Lcom/twitter/app/safety/mutedkeywords/list/j;

    return-object v0
.end method

.method private a(ILcgm;I)V
    .locals 5
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 137
    invoke-virtual {p0, p2, p3}, Lcom/twitter/app/safety/mutedkeywords/list/m;->c(Lcgm;I)V

    .line 138
    invoke-virtual {p0}, Lcom/twitter/app/safety/mutedkeywords/list/m;->aN_()Landroid/view/View;

    move-result-object v0

    .line 139
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 140
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p2, Lcgm;->d:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 141
    const/4 v3, -0x1

    .line 142
    invoke-static {v1, v0, v2, v3}, Lcom/twitter/ui/widget/f;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;I)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 143
    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->show()V

    .line 144
    return-void
.end method

.method private a(Lbfb;I)V
    .locals 2

    .prologue
    .line 147
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/twitter/app/safety/mutedkeywords/list/m;->a(Lbfb;IILandroid/view/View$OnClickListener;)V

    .line 148
    return-void
.end method

.method private a(Lbfb;IILandroid/view/View$OnClickListener;)V
    .locals 4
    .param p3    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 159
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p2}, Lcom/twitter/app/safety/mutedkeywords/list/m;->c(Lcgm;I)V

    .line 160
    invoke-virtual {p0}, Lcom/twitter/app/safety/mutedkeywords/list/m;->aN_()Landroid/view/View;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 162
    iget-object v2, p1, Lbfb;->a:Ljava/lang/String;

    const/4 v3, -0x2

    invoke-static {v1, v0, v2, v3}, Lcom/twitter/ui/widget/f;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;I)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 164
    if-lez p3, :cond_0

    if-eqz p4, :cond_0

    .line 165
    invoke-virtual {v0, p3, p4}, Landroid/support/design/widget/Snackbar;->setAction(ILandroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    .line 168
    :cond_0
    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->show()V

    .line 169
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/safety/mutedkeywords/list/m;Lbfb;IILandroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/app/safety/mutedkeywords/list/m;->a(Lbfb;IILandroid/view/View$OnClickListener;)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/m;->a:Lcom/twitter/app/safety/mutedkeywords/list/j;

    new-instance v1, Lcom/twitter/app/safety/mutedkeywords/list/m$3;

    invoke-direct {v1, p0}, Lcom/twitter/app/safety/mutedkeywords/list/m$3;-><init>(Lcom/twitter/app/safety/mutedkeywords/list/m;)V

    invoke-virtual {v0, p1, v1}, Lcom/twitter/app/safety/mutedkeywords/list/j;->a(ZLasv$b;)V

    .line 202
    return-void
.end method

.method static synthetic b(Lcom/twitter/app/safety/mutedkeywords/list/m;)Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity$b;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/m;->c:Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity$b;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/app/safety/mutedkeywords/list/m;)Lcom/twitter/app/safety/mutedkeywords/list/e;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/m;->b:Lcom/twitter/app/safety/mutedkeywords/list/e;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/app/safety/mutedkeywords/list/m;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/twitter/app/safety/mutedkeywords/list/m;->g()V

    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/app/safety/mutedkeywords/list/m;->a(Z)V

    .line 173
    return-void
.end method

.method private g()V
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/app/safety/mutedkeywords/list/m;->a(Z)V

    .line 177
    return-void
.end method


# virtual methods
.method public a(ILcgm;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 98
    if-eq p1, v1, :cond_0

    .line 103
    :goto_0
    return-void

    .line 101
    :cond_0
    invoke-direct {p0}, Lcom/twitter/app/safety/mutedkeywords/list/m;->g()V

    .line 102
    invoke-static {p2}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgm;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/app/safety/mutedkeywords/list/m;->a(Lcgm;I)V

    goto :goto_0
.end method

.method public bridge synthetic a(ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 39
    check-cast p2, Lcgm;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/safety/mutedkeywords/list/m;->a(ILcgm;)V

    return-void
.end method

.method public a(Lcgm;I)V
    .locals 1

    .prologue
    .line 113
    const v0, 0x7f0a05b5

    invoke-direct {p0, v0, p1, p2}, Lcom/twitter/app/safety/mutedkeywords/list/m;->a(ILcgm;I)V

    .line 114
    return-void
.end method

.method public a(Lcgm;Lbfb;I)V
    .locals 0

    .prologue
    .line 118
    invoke-direct {p0, p2, p3}, Lcom/twitter/app/safety/mutedkeywords/list/m;->a(Lbfb;I)V

    .line 119
    return-void
.end method

.method public am_()V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/m;->a:Lcom/twitter/app/safety/mutedkeywords/list/j;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/app/safety/mutedkeywords/list/j;->a(Lcom/twitter/app/safety/mutedkeywords/list/l;)V

    .line 108
    invoke-super {p0}, Laog;->am_()V

    .line 109
    return-void
.end method

.method public b(Lcgm;I)V
    .locals 1

    .prologue
    .line 123
    const v0, 0x7f0a09bb

    invoke-direct {p0, v0, p1, p2}, Lcom/twitter/app/safety/mutedkeywords/list/m;->a(ILcgm;I)V

    .line 124
    return-void
.end method

.method public b(Lcgm;Lbfb;I)V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0, p2, p3}, Lcom/twitter/app/safety/mutedkeywords/list/m;->a(Lbfb;I)V

    .line 129
    return-void
.end method

.method public c(Lcgm;I)V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/m;->b:Lcom/twitter/app/safety/mutedkeywords/list/e;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/app/safety/mutedkeywords/list/e;->a(Lcgm;I)V

    .line 134
    return-void
.end method
