.class public Lcom/twitter/app/safety/mutedkeywords/list/u;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/dialog/b$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/safety/mutedkeywords/list/u$a;
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/app/safety/mutedkeywords/list/u$a;

.field private b:Lcgm;

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b(Landroid/content/Context;J)Lcom/twitter/android/util/h;
    .locals 8

    .prologue
    .line 86
    new-instance v0, Lcom/twitter/android/util/h;

    const-string/jumbo v2, "muted_keyword_delete_confirmation"

    const/4 v3, 0x3

    const-wide/16 v4, 0x0

    move-object v1, p1

    move-wide v6, p2

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/util/h;-><init>(Landroid/content/Context;Ljava/lang/String;IJJ)V

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;II)V
    .locals 3

    .prologue
    .line 75
    const/4 v0, 0x1

    if-eq v0, p2, :cond_1

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/u;->a:Lcom/twitter/app/safety/mutedkeywords/list/u$a;

    if-eqz v0, :cond_0

    .line 79
    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/list/u;->a:Lcom/twitter/app/safety/mutedkeywords/list/u$a;

    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/u;->b:Lcgm;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgm;

    iget v2, p0, Lcom/twitter/app/safety/mutedkeywords/list/u;->c:I

    invoke-interface {v1, v0, v2, p3}, Lcom/twitter/app/safety/mutedkeywords/list/u$a;->a(Lcgm;II)V

    goto :goto_0
.end method

.method public a(Lank;Landroid/support/v4/app/FragmentManager;)V
    .locals 1

    .prologue
    .line 40
    if-eqz p1, :cond_0

    .line 41
    const-string/jumbo v0, "tag_unmute_confirmation_dialog"

    .line 42
    invoke-virtual {p2, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PromptDialogFragment;

    .line 43
    if-eqz v0, :cond_0

    .line 44
    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    .line 47
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/app/safety/mutedkeywords/list/u$a;Lcgm;ILandroid/support/v4/app/FragmentManager;)V
    .locals 2

    .prologue
    .line 58
    iput-object p2, p0, Lcom/twitter/app/safety/mutedkeywords/list/u;->b:Lcgm;

    .line 59
    iput p3, p0, Lcom/twitter/app/safety/mutedkeywords/list/u;->c:I

    .line 60
    iput-object p1, p0, Lcom/twitter/app/safety/mutedkeywords/list/u;->a:Lcom/twitter/app/safety/mutedkeywords/list/u$a;

    .line 61
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a09b6

    .line 63
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a09b3

    .line 64
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a09b5

    .line 65
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a09b4

    .line 66
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 67
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 68
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PromptDialogFragment;

    .line 69
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->setCancelable(Z)V

    .line 70
    const-string/jumbo v1, "tag_unmute_confirmation_dialog"

    invoke-virtual {v0, p4, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method public a(Landroid/content/Context;J)Z
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/app/safety/mutedkeywords/list/u;->b(Landroid/content/Context;J)Lcom/twitter/android/util/h;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lcom/twitter/android/util/h;->a()Z

    move-result v1

    .line 52
    invoke-virtual {v0}, Lcom/twitter/android/util/h;->b()V

    .line 53
    return v1
.end method
