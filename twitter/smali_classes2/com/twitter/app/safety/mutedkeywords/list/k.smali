.class public final Lcom/twitter/app/safety/mutedkeywords/list/k;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lcom/twitter/app/safety/mutedkeywords/list/j;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcsd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcsd",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/list/j;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lasw;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/list/u;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/twitter/app/safety/mutedkeywords/list/k;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/app/safety/mutedkeywords/list/k;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcsd;Lcta;Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcsd",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/list/j;",
            ">;",
            "Lcta",
            "<",
            "Lasw;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/list/u;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    sget-boolean v0, Lcom/twitter/app/safety/mutedkeywords/list/k;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 32
    :cond_0
    iput-object p1, p0, Lcom/twitter/app/safety/mutedkeywords/list/k;->b:Lcsd;

    .line 33
    sget-boolean v0, Lcom/twitter/app/safety/mutedkeywords/list/k;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 34
    :cond_1
    iput-object p2, p0, Lcom/twitter/app/safety/mutedkeywords/list/k;->c:Lcta;

    .line 35
    sget-boolean v0, Lcom/twitter/app/safety/mutedkeywords/list/k;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 36
    :cond_2
    iput-object p3, p0, Lcom/twitter/app/safety/mutedkeywords/list/k;->d:Lcta;

    .line 37
    sget-boolean v0, Lcom/twitter/app/safety/mutedkeywords/list/k;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 38
    :cond_3
    iput-object p4, p0, Lcom/twitter/app/safety/mutedkeywords/list/k;->e:Lcta;

    .line 39
    return-void
.end method

.method public static a(Lcsd;Lcta;Lcta;Lcta;)Ldagger/internal/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcsd",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/list/j;",
            ">;",
            "Lcta",
            "<",
            "Lasw;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/list/u;",
            ">;)",
            "Ldagger/internal/c",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/list/j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    new-instance v0, Lcom/twitter/app/safety/mutedkeywords/list/k;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/twitter/app/safety/mutedkeywords/list/k;-><init>(Lcsd;Lcta;Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/app/safety/mutedkeywords/list/j;
    .locals 5

    .prologue
    .line 43
    iget-object v3, p0, Lcom/twitter/app/safety/mutedkeywords/list/k;->b:Lcsd;

    new-instance v4, Lcom/twitter/app/safety/mutedkeywords/list/j;

    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/k;->c:Lcta;

    .line 46
    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lasw;

    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/list/k;->d:Lcta;

    invoke-interface {v1}, Lcta;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/app/safety/mutedkeywords/list/k;->e:Lcta;

    invoke-interface {v2}, Lcta;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/app/safety/mutedkeywords/list/u;

    invoke-direct {v4, v0, v1, v2}, Lcom/twitter/app/safety/mutedkeywords/list/j;-><init>(Lasw;Lcom/twitter/library/client/Session;Lcom/twitter/app/safety/mutedkeywords/list/u;)V

    .line 43
    invoke-static {v3, v4}, Ldagger/internal/MembersInjectors;->a(Lcsd;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/safety/mutedkeywords/list/j;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/twitter/app/safety/mutedkeywords/list/k;->a()Lcom/twitter/app/safety/mutedkeywords/list/j;

    move-result-object v0

    return-object v0
.end method
