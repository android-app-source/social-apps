.class public final Lcom/twitter/app/safety/mutedkeywords/list/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/safety/mutedkeywords/list/r;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/safety/mutedkeywords/list/a$b;,
        Lcom/twitter/app/safety/mutedkeywords/list/a$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lass;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laso;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lawb;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/String;",
            "Lcgl;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lasw;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/list/u;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/list/j;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;>;"
        }
    .end annotation
.end field

.field private n:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/abs/j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/twitter/app/safety/mutedkeywords/list/a;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/app/safety/mutedkeywords/list/a;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/twitter/app/safety/mutedkeywords/list/a$a;)V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    sget-boolean v0, Lcom/twitter/app/safety/mutedkeywords/list/a;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 82
    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/app/safety/mutedkeywords/list/a;->a(Lcom/twitter/app/safety/mutedkeywords/list/a$a;)V

    .line 83
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/app/safety/mutedkeywords/list/a$a;Lcom/twitter/app/safety/mutedkeywords/list/a$1;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/twitter/app/safety/mutedkeywords/list/a;-><init>(Lcom/twitter/app/safety/mutedkeywords/list/a$a;)V

    return-void
.end method

.method public static a()Lcom/twitter/app/safety/mutedkeywords/list/a$a;
    .locals 2

    .prologue
    .line 86
    new-instance v0, Lcom/twitter/app/safety/mutedkeywords/list/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/app/safety/mutedkeywords/list/a$a;-><init>(Lcom/twitter/app/safety/mutedkeywords/list/a$1;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/app/safety/mutedkeywords/list/a;)Lcta;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->k:Lcta;

    return-object v0
.end method

.method private a(Lcom/twitter/app/safety/mutedkeywords/list/a$a;)V
    .locals 4

    .prologue
    .line 92
    new-instance v0, Lcom/twitter/app/safety/mutedkeywords/list/a$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/app/safety/mutedkeywords/list/a$1;-><init>(Lcom/twitter/app/safety/mutedkeywords/list/a;Lcom/twitter/app/safety/mutedkeywords/list/a$a;)V

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->b:Lcta;

    .line 105
    new-instance v0, Lcom/twitter/app/safety/mutedkeywords/list/a$2;

    invoke-direct {v0, p0, p1}, Lcom/twitter/app/safety/mutedkeywords/list/a$2;-><init>(Lcom/twitter/app/safety/mutedkeywords/list/a;Lcom/twitter/app/safety/mutedkeywords/list/a$a;)V

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->c:Lcta;

    .line 121
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->b:Lcta;

    iget-object v2, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->c:Lcta;

    .line 120
    invoke-static {v0, v1, v2}, Lasu;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 119
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->d:Lcta;

    .line 125
    new-instance v0, Lcom/twitter/app/safety/mutedkeywords/list/a$3;

    invoke-direct {v0, p0, p1}, Lcom/twitter/app/safety/mutedkeywords/list/a$3;-><init>(Lcom/twitter/app/safety/mutedkeywords/list/a;Lcom/twitter/app/safety/mutedkeywords/list/a$a;)V

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->e:Lcta;

    .line 141
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->e:Lcta;

    iget-object v2, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->c:Lcta;

    .line 140
    invoke-static {v0, v1, v2}, Lasp;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 139
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->f:Lcta;

    .line 145
    new-instance v0, Lcom/twitter/app/safety/mutedkeywords/list/a$4;

    invoke-direct {v0, p0, p1}, Lcom/twitter/app/safety/mutedkeywords/list/a$4;-><init>(Lcom/twitter/app/safety/mutedkeywords/list/a;Lcom/twitter/app/safety/mutedkeywords/list/a$a;)V

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->g:Lcta;

    .line 158
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->g:Lcta;

    .line 161
    invoke-static {v0}, Lasr;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 159
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->h:Lcta;

    .line 163
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->d:Lcta;

    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->f:Lcta;

    iget-object v2, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->h:Lcta;

    .line 165
    invoke-static {v0, v1, v2}, Lasx;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 164
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->i:Lcta;

    .line 171
    invoke-static {}, Lcom/twitter/app/safety/mutedkeywords/list/v;->c()Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->j:Lcta;

    .line 176
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->i:Lcta;

    iget-object v2, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->c:Lcta;

    iget-object v3, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->j:Lcta;

    .line 175
    invoke-static {v0, v1, v2, v3}, Lcom/twitter/app/safety/mutedkeywords/list/k;->a(Lcsd;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 174
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->k:Lcta;

    .line 181
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->k:Lcta;

    .line 182
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->l:Lcta;

    .line 184
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 185
    invoke-static {v0, v1}, Ldagger/internal/f;->a(II)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->l:Lcta;

    .line 186
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    .line 187
    invoke-virtual {v0}, Ldagger/internal/f$a;->a()Ldagger/internal/f;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->m:Lcta;

    .line 189
    new-instance v0, Lcom/twitter/app/safety/mutedkeywords/list/a$5;

    invoke-direct {v0, p0, p1}, Lcom/twitter/app/safety/mutedkeywords/list/a$5;-><init>(Lcom/twitter/app/safety/mutedkeywords/list/a;Lcom/twitter/app/safety/mutedkeywords/list/a$a;)V

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->n:Lcta;

    .line 202
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->n:Lcta;

    .line 204
    invoke-static {v0}, Lcom/twitter/app/common/abs/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 203
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->o:Lcta;

    .line 205
    return-void
.end method

.method static synthetic b(Lcom/twitter/app/safety/mutedkeywords/list/a;)Lcta;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->j:Lcta;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/app/safety/mutedkeywords/list/a;)Lcta;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->m:Lcta;

    return-object v0
.end method


# virtual methods
.method public a(Lant;)Lcom/twitter/app/safety/mutedkeywords/list/o;
    .locals 2

    .prologue
    .line 224
    new-instance v0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/twitter/app/safety/mutedkeywords/list/a$b;-><init>(Lcom/twitter/app/safety/mutedkeywords/list/a;Lant;Lcom/twitter/app/safety/mutedkeywords/list/a$1;)V

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->m:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public c()Lcom/twitter/app/common/abs/j;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a;->o:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/j;

    return-object v0
.end method
