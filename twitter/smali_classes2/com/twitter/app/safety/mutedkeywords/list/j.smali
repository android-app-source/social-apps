.class public Lcom/twitter/app/safety/mutedkeywords/list/j;
.super Lasv;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/safety/mutedkeywords/list/u$a;


# instance fields
.field private d:Lcom/twitter/app/safety/mutedkeywords/list/l;

.field private final e:Lcom/twitter/app/safety/mutedkeywords/list/u;


# direct methods
.method public constructor <init>(Lasw;Lcom/twitter/library/client/Session;Lcom/twitter/app/safety/mutedkeywords/list/u;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lasv;-><init>(Lasw;Lcom/twitter/library/client/Session;)V

    .line 36
    iput-object p3, p0, Lcom/twitter/app/safety/mutedkeywords/list/j;->e:Lcom/twitter/app/safety/mutedkeywords/list/u;

    .line 37
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/safety/mutedkeywords/list/j;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/j;->b:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method private a(Lcgm;I)V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/twitter/app/safety/mutedkeywords/list/j$1;

    invoke-direct {v0, p0, p2}, Lcom/twitter/app/safety/mutedkeywords/list/j$1;-><init>(Lcom/twitter/app/safety/mutedkeywords/list/j;I)V

    invoke-virtual {p0, p1, v0}, Lcom/twitter/app/safety/mutedkeywords/list/j;->b(Lcgm;Lasv$a;)V

    .line 92
    return-void
.end method

.method private a(Lcgm;ILandroid/support/v4/app/FragmentManager;)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/j;->e:Lcom/twitter/app/safety/mutedkeywords/list/u;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/twitter/app/safety/mutedkeywords/list/u;->a(Lcom/twitter/app/safety/mutedkeywords/list/u$a;Lcgm;ILandroid/support/v4/app/FragmentManager;)V

    .line 69
    return-void
.end method

.method private a(Landroid/content/Context;)Z
    .locals 4

    .prologue
    .line 63
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/j;->e:Lcom/twitter/app/safety/mutedkeywords/list/u;

    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/list/j;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, p1, v2, v3}, Lcom/twitter/app/safety/mutedkeywords/list/u;->a(Landroid/content/Context;J)Z

    move-result v0

    return v0
.end method

.method private static b(Landroid/content/Context;)Landroid/support/v4/app/FragmentManager;
    .locals 1

    .prologue
    .line 96
    instance-of v0, p0, Landroid/support/v7/app/AppCompatActivity;

    if-eqz v0, :cond_0

    .line 97
    check-cast p0, Landroid/support/v7/app/AppCompatActivity;

    .line 98
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 100
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/app/safety/mutedkeywords/list/j;)Lcom/twitter/app/safety/mutedkeywords/list/l;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/j;->d:Lcom/twitter/app/safety/mutedkeywords/list/l;

    return-object v0
.end method

.method private b(Landroid/view/View;Lcgm;I)V
    .locals 2

    .prologue
    .line 105
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 106
    invoke-direct {p0, v0}, Lcom/twitter/app/safety/mutedkeywords/list/j;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 107
    invoke-static {v0}, Lcom/twitter/app/safety/mutedkeywords/list/j;->b(Landroid/content/Context;)Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 108
    if-eqz v0, :cond_0

    .line 109
    invoke-direct {p0, p2, p3, v0}, Lcom/twitter/app/safety/mutedkeywords/list/j;->a(Lcgm;ILandroid/support/v4/app/FragmentManager;)V

    .line 114
    :goto_0
    return-void

    .line 113
    :cond_0
    invoke-direct {p0, p2, p3}, Lcom/twitter/app/safety/mutedkeywords/list/j;->a(Lcgm;I)V

    goto :goto_0
.end method

.method private b(Lcgm;I)V
    .locals 1

    .prologue
    .line 118
    new-instance v0, Lcom/twitter/app/safety/mutedkeywords/list/j$2;

    invoke-direct {v0, p0, p2}, Lcom/twitter/app/safety/mutedkeywords/list/j$2;-><init>(Lcom/twitter/app/safety/mutedkeywords/list/j;I)V

    invoke-virtual {p0, p1, v0}, Lcom/twitter/app/safety/mutedkeywords/list/j;->a(Lcgm;Lasv$a;)V

    .line 138
    return-void
.end method

.method static synthetic c(Lcom/twitter/app/safety/mutedkeywords/list/j;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/j;->b:Lcom/twitter/library/client/Session;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/View;Lcgm;I)V
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p2, Lcgm;->g:Z

    if-eqz v0, :cond_0

    .line 45
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/app/safety/mutedkeywords/list/j;->b(Landroid/view/View;Lcgm;I)V

    .line 49
    :goto_0
    return-void

    .line 47
    :cond_0
    invoke-direct {p0, p2, p3}, Lcom/twitter/app/safety/mutedkeywords/list/j;->b(Lcgm;I)V

    goto :goto_0
.end method

.method public a(Lcgm;II)V
    .locals 2

    .prologue
    .line 53
    const/4 v0, -0x1

    if-eq v0, p3, :cond_1

    .line 54
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/j;->d:Lcom/twitter/app/safety/mutedkeywords/list/l;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/j;->d:Lcom/twitter/app/safety/mutedkeywords/list/l;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p2}, Lcom/twitter/app/safety/mutedkeywords/list/l;->c(Lcgm;I)V

    .line 60
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/twitter/app/safety/mutedkeywords/list/j;->a(Lcgm;I)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/app/safety/mutedkeywords/list/l;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/twitter/app/safety/mutedkeywords/list/j;->d:Lcom/twitter/app/safety/mutedkeywords/list/l;

    .line 41
    return-void
.end method
