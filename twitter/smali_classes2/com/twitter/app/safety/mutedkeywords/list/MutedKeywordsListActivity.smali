.class public Lcom/twitter/app/safety/mutedkeywords/list/MutedKeywordsListActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/safety/mutedkeywords/list/MutedKeywordsListActivity$b;,
        Lcom/twitter/app/safety/mutedkeywords/list/MutedKeywordsListActivity$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method

.method private i()V
    .locals 5

    .prologue
    .line 66
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 67
    const-string/jumbo v1, "muted_keywords_prompt"

    .line 68
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 67
    invoke-static {p0, v1, v2, v3}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lcom/twitter/android/util/h;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 70
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/safety/mutedkeywords/list/MutedKeywordsListActivity;->F:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "settings"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "notifications"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "mute_keyword"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "interstitial"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "impression"

    aput-object v4, v2, v3

    .line 71
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    .line 70
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 72
    invoke-virtual {p0}, Lcom/twitter/app/safety/mutedkeywords/list/MutedKeywordsListActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/widget/MutedKeywordsEducationOverlay;->b(Landroid/support/v4/app/FragmentManager;)V

    .line 73
    invoke-virtual {v0}, Lcom/twitter/android/util/h;->b()V

    .line 75
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    const/16 v0, 0xc

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(I)V

    .line 32
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 33
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(Z)V

    .line 34
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(Z)V

    .line 35
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 36
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic a(Lank;)Lcom/twitter/app/common/base/j;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/twitter/app/safety/mutedkeywords/list/MutedKeywordsListActivity;->h(Lank;)Lcom/twitter/app/safety/mutedkeywords/list/o;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic b(Lank;)Lcom/twitter/app/common/abs/b;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/twitter/app/safety/mutedkeywords/list/MutedKeywordsListActivity;->h(Lank;)Lcom/twitter/app/safety/mutedkeywords/list/o;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic c(Lank;)Lans;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/twitter/app/safety/mutedkeywords/list/MutedKeywordsListActivity;->h(Lank;)Lcom/twitter/app/safety/mutedkeywords/list/o;

    move-result-object v0

    return-object v0
.end method

.method protected d(Lank;)Lcom/twitter/app/safety/mutedkeywords/list/r;
    .locals 2

    .prologue
    .line 42
    invoke-static {}, Lcom/twitter/app/safety/mutedkeywords/list/a;->a()Lcom/twitter/app/safety/mutedkeywords/list/a$a;

    move-result-object v0

    .line 43
    invoke-static {}, Lamu;->av()Lamu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/safety/mutedkeywords/list/a$a;->a(Lamu;)Lcom/twitter/app/safety/mutedkeywords/list/a$a;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lcom/twitter/app/safety/mutedkeywords/list/a$a;->a()Lcom/twitter/app/safety/mutedkeywords/list/r;

    move-result-object v0

    .line 42
    return-object v0
.end method

.method protected synthetic e(Lank;)Lcom/twitter/app/common/base/i;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/twitter/app/safety/mutedkeywords/list/MutedKeywordsListActivity;->d(Lank;)Lcom/twitter/app/safety/mutedkeywords/list/r;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f(Lank;)Lcom/twitter/app/common/abs/a;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/twitter/app/safety/mutedkeywords/list/MutedKeywordsListActivity;->d(Lank;)Lcom/twitter/app/safety/mutedkeywords/list/r;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic g(Lank;)Lann;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/twitter/app/safety/mutedkeywords/list/MutedKeywordsListActivity;->d(Lank;)Lcom/twitter/app/safety/mutedkeywords/list/r;

    move-result-object v0

    return-object v0
.end method

.method protected h(Lank;)Lcom/twitter/app/safety/mutedkeywords/list/o;
    .locals 2

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/twitter/app/safety/mutedkeywords/list/MutedKeywordsListActivity;->X()Lann;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/safety/mutedkeywords/list/r;

    .line 51
    new-instance v1, Lant;

    invoke-direct {v1, p0, p1}, Lant;-><init>(Landroid/app/Activity;Lank;)V

    invoke-interface {v0, v1}, Lcom/twitter/app/safety/mutedkeywords/list/r;->a(Lant;)Lcom/twitter/app/safety/mutedkeywords/list/o;

    move-result-object v0

    return-object v0
.end method

.method protected onStart()V
    .locals 4

    .prologue
    .line 57
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onStart()V

    .line 58
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/safety/mutedkeywords/list/MutedKeywordsListActivity;->F:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "settings"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "notifications"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "mute_keyword"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "list"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "impression"

    aput-object v3, v1, v2

    .line 59
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 58
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 61
    invoke-direct {p0}, Lcom/twitter/app/safety/mutedkeywords/list/MutedKeywordsListActivity;->i()V

    .line 62
    return-void
.end method
