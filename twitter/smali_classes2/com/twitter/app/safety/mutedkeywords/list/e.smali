.class public Lcom/twitter/app/safety/mutedkeywords/list/e;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/safety/mutedkeywords/list/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/twitter/app/safety/mutedkeywords/list/h;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcbi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcbi",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/list/f;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/twitter/app/safety/mutedkeywords/list/b;

.field private c:Lcom/twitter/app/safety/mutedkeywords/list/e$a;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 38
    new-instance v0, Lcbl$a;

    invoke-direct {v0}, Lcbl$a;-><init>()V

    new-instance v1, Lcom/twitter/app/safety/mutedkeywords/list/s;

    invoke-direct {v1}, Lcom/twitter/app/safety/mutedkeywords/list/s;-><init>()V

    .line 39
    invoke-virtual {v0, v1}, Lcbl$a;->a(Ljava/lang/Object;)Lcbl$a;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lcbl$a;->a()Lcbl;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/e;->a:Lcbi;

    .line 41
    return-void
.end method

.method private a(I)Lcom/twitter/app/safety/mutedkeywords/list/f;
    .locals 1

    .prologue
    .line 93
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/e;->a:Lcbi;

    invoke-virtual {v0}, Lcbi;->be_()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/e;->a:Lcbi;

    invoke-virtual {v0, p1}, Lcbi;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/safety/mutedkeywords/list/f;

    .line 98
    :goto_0
    return-object v0

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/e;->a:Lcbi;

    invoke-virtual {v0}, Lcbi;->be_()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 96
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/e;->b:Lcom/twitter/app/safety/mutedkeywords/list/b;

    goto :goto_0

    .line 98
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/app/safety/mutedkeywords/list/e;Lcom/twitter/app/safety/mutedkeywords/list/h;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/twitter/app/safety/mutedkeywords/list/e;->a(Lcom/twitter/app/safety/mutedkeywords/list/h;Landroid/view/View;)V

    return-void
.end method

.method private a(Lcom/twitter/app/safety/mutedkeywords/list/h;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 147
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/e;->c:Lcom/twitter/app/safety/mutedkeywords/list/e$a;

    if-eqz v0, :cond_0

    .line 148
    invoke-virtual {p1}, Lcom/twitter/app/safety/mutedkeywords/list/h;->getAdapterPosition()I

    move-result v1

    .line 149
    invoke-direct {p0, v1}, Lcom/twitter/app/safety/mutedkeywords/list/e;->a(I)Lcom/twitter/app/safety/mutedkeywords/list/f;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/safety/mutedkeywords/list/g;

    .line 150
    iget-object v2, p0, Lcom/twitter/app/safety/mutedkeywords/list/e;->c:Lcom/twitter/app/safety/mutedkeywords/list/e$a;

    invoke-virtual {v0}, Lcom/twitter/app/safety/mutedkeywords/list/g;->b()Lcgm;

    move-result-object v0

    invoke-interface {v2, p2, v0, v1}, Lcom/twitter/app/safety/mutedkeywords/list/e$a;->a(Landroid/view/View;Lcgm;I)V

    .line 152
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Lcom/twitter/app/safety/mutedkeywords/list/h;
    .locals 2

    .prologue
    .line 104
    packed-switch p2, :pswitch_data_0

    .line 125
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Invalid view type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :pswitch_0
    invoke-static {p1}, Lcom/twitter/app/safety/mutedkeywords/list/d;->a(Landroid/view/ViewGroup;)Lcom/twitter/app/safety/mutedkeywords/list/d;

    move-result-object v0

    .line 107
    new-instance v1, Lcom/twitter/app/safety/mutedkeywords/list/e$1;

    invoke-direct {v1, p0, v0}, Lcom/twitter/app/safety/mutedkeywords/list/e$1;-><init>(Lcom/twitter/app/safety/mutedkeywords/list/e;Lcom/twitter/app/safety/mutedkeywords/list/d;)V

    invoke-virtual {v0, v1}, Lcom/twitter/app/safety/mutedkeywords/list/d;->a(Landroid/view/View$OnClickListener;)V

    .line 129
    :goto_0
    return-object v0

    .line 117
    :pswitch_1
    invoke-static {p1}, Lcom/twitter/app/safety/mutedkeywords/list/c;->a(Landroid/view/ViewGroup;)Lcom/twitter/app/safety/mutedkeywords/list/c;

    move-result-object v0

    goto :goto_0

    .line 121
    :pswitch_2
    invoke-static {p1}, Lcom/twitter/app/safety/mutedkeywords/list/t;->a(Landroid/view/ViewGroup;)Lcom/twitter/app/safety/mutedkeywords/list/t;

    move-result-object v0

    goto :goto_0

    .line 104
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lcbi;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/list/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/e;->a:Lcbi;

    .line 45
    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    iput-object p1, p0, Lcom/twitter/app/safety/mutedkeywords/list/e;->a:Lcbi;

    .line 47
    invoke-virtual {p0}, Lcom/twitter/app/safety/mutedkeywords/list/e;->notifyDataSetChanged()V

    .line 49
    :cond_0
    return-void
.end method

.method public a(Lcgm;I)V
    .locals 3

    .prologue
    .line 58
    if-ltz p2, :cond_1

    invoke-virtual {p0}, Lcom/twitter/app/safety/mutedkeywords/list/e;->getItemCount()I

    move-result v0

    if-ge p2, v0, :cond_1

    .line 59
    if-eqz p1, :cond_0

    .line 60
    invoke-direct {p0, p2}, Lcom/twitter/app/safety/mutedkeywords/list/e;->a(I)Lcom/twitter/app/safety/mutedkeywords/list/f;

    move-result-object v0

    .line 61
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/app/safety/mutedkeywords/list/f;->a()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 62
    invoke-direct {p0, p2}, Lcom/twitter/app/safety/mutedkeywords/list/e;->a(I)Lcom/twitter/app/safety/mutedkeywords/list/f;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/safety/mutedkeywords/list/g;

    .line 63
    invoke-virtual {v0}, Lcom/twitter/app/safety/mutedkeywords/list/g;->b()Lcgm;

    move-result-object v1

    iget-object v1, v1, Lcgm;->d:Ljava/lang/String;

    iget-object v2, p1, Lcgm;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    invoke-virtual {v0, p1}, Lcom/twitter/app/safety/mutedkeywords/list/g;->a(Lcgm;)V

    .line 68
    :cond_0
    invoke-virtual {p0, p2}, Lcom/twitter/app/safety/mutedkeywords/list/e;->notifyItemChanged(I)V

    .line 70
    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/app/safety/mutedkeywords/list/b;)V
    .locals 1

    .prologue
    .line 73
    iput-object p1, p0, Lcom/twitter/app/safety/mutedkeywords/list/e;->b:Lcom/twitter/app/safety/mutedkeywords/list/b;

    .line 74
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/e;->a:Lcbi;

    invoke-virtual {v0}, Lcbi;->be_()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/safety/mutedkeywords/list/e;->notifyItemInserted(I)V

    .line 75
    return-void
.end method

.method public a(Lcom/twitter/app/safety/mutedkeywords/list/e$a;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/twitter/app/safety/mutedkeywords/list/e;->c:Lcom/twitter/app/safety/mutedkeywords/list/e$a;

    .line 79
    return-void
.end method

.method public a(Lcom/twitter/app/safety/mutedkeywords/list/h;I)V
    .locals 1

    .prologue
    .line 134
    invoke-direct {p0, p2}, Lcom/twitter/app/safety/mutedkeywords/list/e;->a(I)Lcom/twitter/app/safety/mutedkeywords/list/f;

    move-result-object v0

    .line 135
    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {p1, p2, v0}, Lcom/twitter/app/safety/mutedkeywords/list/h;->a(ILcom/twitter/app/safety/mutedkeywords/list/f;)V

    .line 139
    :cond_0
    return-void
.end method

.method public getItemCount()I
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/e;->a:Lcbi;

    invoke-virtual {v0}, Lcbi;->be_()I

    move-result v1

    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/e;->b:Lcom/twitter/app/safety/mutedkeywords/list/b;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 3

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/twitter/app/safety/mutedkeywords/list/e;->a(I)Lcom/twitter/app/safety/mutedkeywords/list/f;

    move-result-object v0

    .line 84
    if-eqz v0, :cond_0

    .line 85
    invoke-virtual {v0}, Lcom/twitter/app/safety/mutedkeywords/list/f;->a()I

    move-result v0

    return v0

    .line 87
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Position for view type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " out of bounds (0, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/safety/mutedkeywords/list/e;->a:Lcbi;

    invoke-virtual {v2}, Lcbi;->be_()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") in MutedKeywordAdapter"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lcom/twitter/app/safety/mutedkeywords/list/h;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/safety/mutedkeywords/list/e;->a(Lcom/twitter/app/safety/mutedkeywords/list/h;I)V

    return-void
.end method

.method public synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/safety/mutedkeywords/list/e;->a(Landroid/view/ViewGroup;I)Lcom/twitter/app/safety/mutedkeywords/list/h;

    move-result-object v0

    return-object v0
.end method
