.class public Lcom/twitter/app/safety/mutedkeywords/list/d;
.super Lcom/twitter/app/safety/mutedkeywords/list/h;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/safety/mutedkeywords/list/h",
        "<",
        "Lcom/twitter/app/safety/mutedkeywords/list/g;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Landroid/widget/ImageView;


# direct methods
.method protected constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/twitter/app/safety/mutedkeywords/list/h;-><init>(Landroid/view/View;)V

    .line 24
    const v0, 0x7f13055b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/d;->a:Landroid/widget/TextView;

    .line 25
    const v0, 0x7f13055c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/d;->b:Landroid/widget/ImageView;

    .line 26
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)Lcom/twitter/app/safety/mutedkeywords/list/d;
    .locals 3

    .prologue
    .line 30
    .line 31
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040224

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 32
    new-instance v1, Lcom/twitter/app/safety/mutedkeywords/list/d;

    invoke-direct {v1, v0}, Lcom/twitter/app/safety/mutedkeywords/list/d;-><init>(Landroid/view/View;)V

    return-object v1
.end method


# virtual methods
.method public bridge synthetic a(ILcom/twitter/app/safety/mutedkeywords/list/f;)V
    .locals 0

    .prologue
    .line 18
    check-cast p2, Lcom/twitter/app/safety/mutedkeywords/list/g;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/safety/mutedkeywords/list/d;->a(ILcom/twitter/app/safety/mutedkeywords/list/g;)V

    return-void
.end method

.method public a(ILcom/twitter/app/safety/mutedkeywords/list/g;)V
    .locals 3

    .prologue
    .line 37
    invoke-virtual {p2}, Lcom/twitter/app/safety/mutedkeywords/list/g;->b()Lcgm;

    move-result-object v0

    .line 38
    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/list/d;->a:Landroid/widget/TextView;

    iget-object v2, v0, Lcgm;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/list/d;->b:Landroid/widget/ImageView;

    iget-boolean v0, v0, Lcgm;->g:Z

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setActivated(Z)V

    .line 40
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/d;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    return-void
.end method
