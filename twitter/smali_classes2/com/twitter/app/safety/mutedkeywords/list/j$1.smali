.class Lcom/twitter/app/safety/mutedkeywords/list/j$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lasv$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/safety/mutedkeywords/list/j;->a(Lcgm;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/twitter/app/safety/mutedkeywords/list/j;


# direct methods
.method constructor <init>(Lcom/twitter/app/safety/mutedkeywords/list/j;I)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/twitter/app/safety/mutedkeywords/list/j$1;->b:Lcom/twitter/app/safety/mutedkeywords/list/j;

    iput p2, p0, Lcom/twitter/app/safety/mutedkeywords/list/j$1;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcgm;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 76
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/list/j$1;->b:Lcom/twitter/app/safety/mutedkeywords/list/j;

    invoke-static {v1}, Lcom/twitter/app/safety/mutedkeywords/list/j;->a(Lcom/twitter/app/safety/mutedkeywords/list/j;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "settings"

    aput-object v2, v1, v4

    const/4 v2, 0x1

    const-string/jumbo v3, "notifications"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "mute_keyword"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "list"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "unmute"

    aput-object v3, v1, v2

    .line 77
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 76
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 79
    iput-boolean v4, p1, Lcgm;->g:Z

    .line 80
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/j$1;->b:Lcom/twitter/app/safety/mutedkeywords/list/j;

    invoke-static {v0}, Lcom/twitter/app/safety/mutedkeywords/list/j;->b(Lcom/twitter/app/safety/mutedkeywords/list/j;)Lcom/twitter/app/safety/mutedkeywords/list/l;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/j$1;->b:Lcom/twitter/app/safety/mutedkeywords/list/j;

    invoke-static {v0}, Lcom/twitter/app/safety/mutedkeywords/list/j;->b(Lcom/twitter/app/safety/mutedkeywords/list/j;)Lcom/twitter/app/safety/mutedkeywords/list/l;

    move-result-object v0

    iget v1, p0, Lcom/twitter/app/safety/mutedkeywords/list/j$1;->a:I

    invoke-interface {v0, p1, v1}, Lcom/twitter/app/safety/mutedkeywords/list/l;->b(Lcgm;I)V

    .line 83
    :cond_0
    return-void
.end method

.method public a(Lcgm;Lbfb;)V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/j$1;->b:Lcom/twitter/app/safety/mutedkeywords/list/j;

    invoke-static {v0}, Lcom/twitter/app/safety/mutedkeywords/list/j;->b(Lcom/twitter/app/safety/mutedkeywords/list/j;)Lcom/twitter/app/safety/mutedkeywords/list/l;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/j$1;->b:Lcom/twitter/app/safety/mutedkeywords/list/j;

    invoke-static {v0}, Lcom/twitter/app/safety/mutedkeywords/list/j;->b(Lcom/twitter/app/safety/mutedkeywords/list/j;)Lcom/twitter/app/safety/mutedkeywords/list/l;

    move-result-object v0

    iget v1, p0, Lcom/twitter/app/safety/mutedkeywords/list/j$1;->a:I

    invoke-interface {v0, p1, p2, v1}, Lcom/twitter/app/safety/mutedkeywords/list/l;->b(Lcgm;Lbfb;I)V

    .line 90
    :cond_0
    return-void
.end method
