.class public final Lcom/twitter/app/safety/mutedkeywords/list/n;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lcom/twitter/app/safety/mutedkeywords/list/m;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcsd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcsd",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/list/m;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/list/j;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/list/u;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity$b;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lank;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/support/v4/app/FragmentManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/twitter/app/safety/mutedkeywords/list/n;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/app/safety/mutedkeywords/list/n;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcsd;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcsd",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/list/m;",
            ">;",
            "Lcta",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/list/j;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/list/u;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity$b;",
            ">;",
            "Lcta",
            "<",
            "Lank;",
            ">;",
            "Lcta",
            "<",
            "Landroid/support/v4/app/FragmentManager;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    sget-boolean v0, Lcom/twitter/app/safety/mutedkeywords/list/n;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 41
    :cond_0
    iput-object p1, p0, Lcom/twitter/app/safety/mutedkeywords/list/n;->b:Lcsd;

    .line 42
    sget-boolean v0, Lcom/twitter/app/safety/mutedkeywords/list/n;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 43
    :cond_1
    iput-object p2, p0, Lcom/twitter/app/safety/mutedkeywords/list/n;->c:Lcta;

    .line 44
    sget-boolean v0, Lcom/twitter/app/safety/mutedkeywords/list/n;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 45
    :cond_2
    iput-object p3, p0, Lcom/twitter/app/safety/mutedkeywords/list/n;->d:Lcta;

    .line 46
    sget-boolean v0, Lcom/twitter/app/safety/mutedkeywords/list/n;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 47
    :cond_3
    iput-object p4, p0, Lcom/twitter/app/safety/mutedkeywords/list/n;->e:Lcta;

    .line 48
    sget-boolean v0, Lcom/twitter/app/safety/mutedkeywords/list/n;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 49
    :cond_4
    iput-object p5, p0, Lcom/twitter/app/safety/mutedkeywords/list/n;->f:Lcta;

    .line 50
    sget-boolean v0, Lcom/twitter/app/safety/mutedkeywords/list/n;->a:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 51
    :cond_5
    iput-object p6, p0, Lcom/twitter/app/safety/mutedkeywords/list/n;->g:Lcta;

    .line 52
    sget-boolean v0, Lcom/twitter/app/safety/mutedkeywords/list/n;->a:Z

    if-nez v0, :cond_6

    if-nez p7, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 53
    :cond_6
    iput-object p7, p0, Lcom/twitter/app/safety/mutedkeywords/list/n;->h:Lcta;

    .line 54
    return-void
.end method

.method public static a(Lcsd;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcsd",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/list/m;",
            ">;",
            "Lcta",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/list/j;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/list/u;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity$b;",
            ">;",
            "Lcta",
            "<",
            "Lank;",
            ">;",
            "Lcta",
            "<",
            "Landroid/support/v4/app/FragmentManager;",
            ">;)",
            "Ldagger/internal/c",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/list/m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    new-instance v0, Lcom/twitter/app/safety/mutedkeywords/list/n;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/twitter/app/safety/mutedkeywords/list/n;-><init>(Lcsd;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/app/safety/mutedkeywords/list/m;
    .locals 8

    .prologue
    .line 58
    iget-object v7, p0, Lcom/twitter/app/safety/mutedkeywords/list/n;->b:Lcsd;

    new-instance v0, Lcom/twitter/app/safety/mutedkeywords/list/m;

    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/list/n;->c:Lcta;

    .line 61
    invoke-interface {v1}, Lcta;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iget-object v2, p0, Lcom/twitter/app/safety/mutedkeywords/list/n;->d:Lcta;

    .line 62
    invoke-interface {v2}, Lcta;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/app/safety/mutedkeywords/list/j;

    iget-object v3, p0, Lcom/twitter/app/safety/mutedkeywords/list/n;->e:Lcta;

    .line 63
    invoke-interface {v3}, Lcta;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/app/safety/mutedkeywords/list/u;

    iget-object v4, p0, Lcom/twitter/app/safety/mutedkeywords/list/n;->f:Lcta;

    .line 64
    invoke-interface {v4}, Lcta;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity$b;

    iget-object v5, p0, Lcom/twitter/app/safety/mutedkeywords/list/n;->g:Lcta;

    .line 65
    invoke-interface {v5}, Lcta;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lank;

    iget-object v6, p0, Lcom/twitter/app/safety/mutedkeywords/list/n;->h:Lcta;

    .line 66
    invoke-interface {v6}, Lcta;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/support/v4/app/FragmentManager;

    invoke-direct/range {v0 .. v6}, Lcom/twitter/app/safety/mutedkeywords/list/m;-><init>(Landroid/view/LayoutInflater;Lcom/twitter/app/safety/mutedkeywords/list/j;Lcom/twitter/app/safety/mutedkeywords/list/u;Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity$b;Lank;Landroid/support/v4/app/FragmentManager;)V

    .line 58
    invoke-static {v7, v0}, Ldagger/internal/MembersInjectors;->a(Lcsd;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/safety/mutedkeywords/list/m;

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/twitter/app/safety/mutedkeywords/list/n;->a()Lcom/twitter/app/safety/mutedkeywords/list/m;

    move-result-object v0

    return-object v0
.end method
