.class public Lcom/twitter/app/safety/mutedkeywords/list/c;
.super Lcom/twitter/app/safety/mutedkeywords/list/h;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/safety/mutedkeywords/list/h",
        "<",
        "Lcom/twitter/app/safety/mutedkeywords/list/b;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Landroid/widget/TextView;


# direct methods
.method protected constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/twitter/app/safety/mutedkeywords/list/h;-><init>(Landroid/view/View;)V

    .line 21
    const v0, 0x7f13055a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/c;->a:Landroid/widget/TextView;

    .line 22
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)Lcom/twitter/app/safety/mutedkeywords/list/c;
    .locals 3

    .prologue
    .line 26
    .line 28
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040223

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 29
    new-instance v1, Lcom/twitter/app/safety/mutedkeywords/list/c;

    invoke-direct {v1, v0}, Lcom/twitter/app/safety/mutedkeywords/list/c;-><init>(Landroid/view/View;)V

    return-object v1
.end method


# virtual methods
.method public a(ILcom/twitter/app/safety/mutedkeywords/list/b;)V
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/c;->a:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/twitter/app/safety/mutedkeywords/list/b;->a:Landroid/text/Spanned;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 35
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/c;->a:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/twitter/ui/view/g;->a(Landroid/widget/TextView;)V

    .line 36
    return-void
.end method

.method public bridge synthetic a(ILcom/twitter/app/safety/mutedkeywords/list/f;)V
    .locals 0

    .prologue
    .line 16
    check-cast p2, Lcom/twitter/app/safety/mutedkeywords/list/b;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/safety/mutedkeywords/list/c;->a(ILcom/twitter/app/safety/mutedkeywords/list/b;)V

    return-void
.end method
