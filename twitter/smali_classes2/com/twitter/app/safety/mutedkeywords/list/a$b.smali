.class final Lcom/twitter/app/safety/mutedkeywords/list/a$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/safety/mutedkeywords/list/o;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/safety/mutedkeywords/list/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/safety/mutedkeywords/list/a;

.field private final b:Lant;

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/base/BaseFragmentActivity;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/composer/MutedKeywordComposerActivity$b;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lank;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/support/v4/app/FragmentManager;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/safety/mutedkeywords/list/m;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/twitter/app/safety/mutedkeywords/list/a;Lant;)V
    .locals 1

    .prologue
    .line 288
    iput-object p1, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->a:Lcom/twitter/app/safety/mutedkeywords/list/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 289
    invoke-static {p2}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lant;

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->b:Lant;

    .line 290
    invoke-direct {p0}, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->c()V

    .line 291
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/app/safety/mutedkeywords/list/a;Lant;Lcom/twitter/app/safety/mutedkeywords/list/a$1;)V
    .locals 0

    .prologue
    .line 268
    invoke-direct {p0, p1, p2}, Lcom/twitter/app/safety/mutedkeywords/list/a$b;-><init>(Lcom/twitter/app/safety/mutedkeywords/list/a;Lant;)V

    return-void
.end method

.method private c()V
    .locals 7

    .prologue
    .line 296
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->b:Lant;

    .line 298
    invoke-static {v0}, Lanu;->a(Lant;)Ldagger/internal/c;

    move-result-object v0

    .line 297
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->c:Lcta;

    .line 300
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->c:Lcta;

    .line 302
    invoke-static {v0}, Lanx;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 301
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->d:Lcta;

    .line 304
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->c:Lcta;

    .line 306
    invoke-static {v0}, Lanv;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 305
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->e:Lcta;

    .line 309
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->e:Lcta;

    .line 311
    invoke-static {v0}, Lcom/twitter/app/safety/mutedkeywords/list/q;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 310
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->f:Lcta;

    .line 314
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->b:Lant;

    .line 316
    invoke-static {v0}, Laoa;->a(Lant;)Ldagger/internal/c;

    move-result-object v0

    .line 315
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->g:Lcta;

    .line 318
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->e:Lcta;

    .line 320
    invoke-static {v0}, Laob;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 319
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->h:Lcta;

    .line 326
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->d:Lcta;

    iget-object v2, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->a:Lcom/twitter/app/safety/mutedkeywords/list/a;

    .line 328
    invoke-static {v2}, Lcom/twitter/app/safety/mutedkeywords/list/a;->a(Lcom/twitter/app/safety/mutedkeywords/list/a;)Lcta;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->a:Lcom/twitter/app/safety/mutedkeywords/list/a;

    .line 329
    invoke-static {v3}, Lcom/twitter/app/safety/mutedkeywords/list/a;->b(Lcom/twitter/app/safety/mutedkeywords/list/a;)Lcta;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->f:Lcta;

    iget-object v5, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->g:Lcta;

    iget-object v6, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->h:Lcta;

    .line 325
    invoke-static/range {v0 .. v6}, Lcom/twitter/app/safety/mutedkeywords/list/n;->a(Lcsd;Lcta;Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 324
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->i:Lcta;

    .line 334
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->i:Lcta;

    .line 335
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->j:Lcta;

    .line 336
    return-void
.end method


# virtual methods
.method public a()Laog;
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->j:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laog;

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 340
    iget-object v0, p0, Lcom/twitter/app/safety/mutedkeywords/list/a$b;->a:Lcom/twitter/app/safety/mutedkeywords/list/a;

    invoke-static {v0}, Lcom/twitter/app/safety/mutedkeywords/list/a;->c(Lcom/twitter/app/safety/mutedkeywords/list/a;)Lcta;

    move-result-object v0

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method
