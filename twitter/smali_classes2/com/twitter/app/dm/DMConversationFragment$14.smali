.class Lcom/twitter/app/dm/DMConversationFragment$14;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/dialog/b$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/dm/DMConversationFragment;->a(Lcmm;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Landroid/support/v4/app/FragmentActivity;

.field final synthetic c:Lcom/twitter/app/dm/DMConversationFragment;


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/DMConversationFragment;Ljava/util/List;Landroid/support/v4/app/FragmentActivity;)V
    .locals 0

    .prologue
    .line 1930
    iput-object p1, p0, Lcom/twitter/app/dm/DMConversationFragment$14;->c:Lcom/twitter/app/dm/DMConversationFragment;

    iput-object p2, p0, Lcom/twitter/app/dm/DMConversationFragment$14;->a:Ljava/util/List;

    iput-object p3, p0, Lcom/twitter/app/dm/DMConversationFragment$14;->b:Landroid/support/v4/app/FragmentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;II)V
    .locals 2

    .prologue
    .line 1933
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$14;->a:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1934
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1935
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$14;->c:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v0}, Lcom/twitter/app/dm/DMConversationFragment;->o(Lcom/twitter/app/dm/DMConversationFragment;)V

    .line 1959
    :cond_0
    :goto_0
    return-void

    .line 1936
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 1937
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$14;->c:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v0}, Lcom/twitter/app/dm/DMConversationFragment;->p(Lcom/twitter/app/dm/DMConversationFragment;)Lcom/twitter/android/media/selection/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/media/selection/c;->b()V

    goto :goto_0

    .line 1938
    :cond_2
    if-nez v0, :cond_3

    .line 1939
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$14;->c:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-virtual {v0}, Lcom/twitter/app/dm/DMConversationFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment$14;->c:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v1}, Lcom/twitter/app/dm/DMConversationFragment;->q(Lcom/twitter/app/dm/DMConversationFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/app/dm/DMConversationFragment;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 1940
    :cond_3
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 1941
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/16 v1, 0x30c

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a02a1

    .line 1942
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a02a2

    .line 1943
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a00f6

    .line 1944
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 1945
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/dm/DMConversationFragment$14$1;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/DMConversationFragment$14$1;-><init>(Lcom/twitter/app/dm/DMConversationFragment$14;)V

    .line 1946
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment$14;->b:Landroid/support/v4/app/FragmentActivity;

    .line 1957
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0
.end method
