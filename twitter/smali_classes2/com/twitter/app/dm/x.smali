.class public Lcom/twitter/app/dm/x;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {}, Lcom/twitter/util/collection/o;->f()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/x;->a:Ljava/lang/Iterable;

    .line 23
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/x;->b:Ljava/util/List;

    return-void
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/twitter/app/dm/x;->d()Z

    move-result v1

    .line 60
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/x;->b:Ljava/util/List;

    :goto_0
    iput-object v0, p0, Lcom/twitter/app/dm/x;->a:Ljava/lang/Iterable;

    .line 61
    return v1

    .line 60
    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/o;->f()Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method private d()Z
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/app/dm/x;->b:Ljava/util/List;

    new-instance v1, Lcom/twitter/app/dm/x$1;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/x$1;-><init>(Lcom/twitter/app/dm/x;)V

    invoke-static {v0, v1}, Lcpt;->b(Ljava/lang/Iterable;Lcpv;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()Lcom/twitter/model/dms/n;
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/twitter/app/dm/x;->a:Ljava/lang/Iterable;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    sget-object v0, Lcom/twitter/model/dms/n;->a:Lcom/twitter/model/dms/n;

    .line 43
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/model/dms/n;

    iget-object v1, p0, Lcom/twitter/app/dm/x;->a:Ljava/lang/Iterable;

    invoke-direct {v0, v1}, Lcom/twitter/model/dms/n;-><init>(Ljava/lang/Iterable;)V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 31
    const-string/jumbo v0, "state_participants"

    .line 32
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/x;->b:Ljava/util/List;

    .line 33
    invoke-direct {p0}, Lcom/twitter/app/dm/x;->c()Z

    move-result v0

    return v0
.end method

.method public a(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 26
    iput-object p1, p0, Lcom/twitter/app/dm/x;->b:Ljava/util/List;

    .line 27
    invoke-direct {p0}, Lcom/twitter/app/dm/x;->c()Z

    move-result v0

    return v0
.end method

.method public b()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 48
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 49
    const-string/jumbo v1, "state_participants"

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/twitter/app/dm/x;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 50
    return-object v0
.end method
