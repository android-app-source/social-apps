.class public Lcom/twitter/app/dm/n;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/n$a;,
        Lcom/twitter/app/dm/n$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/app/dm/n$b;

.field private final b:Lcom/twitter/app/dm/n$a;

.field private final c:Lcom/twitter/android/media/selection/a;

.field private d:Landroid/net/Uri;

.field private e:Landroid/net/Uri;

.field private f:Z

.field private g:Lcom/twitter/android/media/selection/MediaAttachment;

.field private final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/app/dm/n$b;Lcom/twitter/app/dm/n$a;Lcom/twitter/android/media/selection/a;Lcom/twitter/android/media/selection/MediaAttachment;Landroid/net/Uri;Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/app/dm/n$b;",
            "Lcom/twitter/app/dm/n$a;",
            "Lcom/twitter/android/media/selection/a;",
            "Lcom/twitter/android/media/selection/MediaAttachment;",
            "Landroid/net/Uri;",
            "Ljava/util/Collection",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/twitter/app/dm/n;->a:Lcom/twitter/app/dm/n$b;

    .line 47
    iput-object p2, p0, Lcom/twitter/app/dm/n;->b:Lcom/twitter/app/dm/n$a;

    .line 48
    iput-object p3, p0, Lcom/twitter/app/dm/n;->c:Lcom/twitter/android/media/selection/a;

    .line 49
    invoke-static {p6}, Lcom/twitter/util/collection/MutableSet;->a(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/n;->h:Ljava/util/Set;

    .line 51
    invoke-virtual {p0, p4}, Lcom/twitter/app/dm/n;->b(Lcom/twitter/android/media/selection/MediaAttachment;)V

    .line 52
    invoke-virtual {p0, p5}, Lcom/twitter/app/dm/n;->d(Landroid/net/Uri;)V

    .line 53
    return-void
.end method

.method private y()V
    .locals 0

    .prologue
    .line 264
    invoke-virtual {p0}, Lcom/twitter/app/dm/n;->u()V

    .line 265
    invoke-virtual {p0}, Lcom/twitter/app/dm/n;->w()V

    .line 266
    return-void
.end method

.method private z()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/twitter/app/dm/n;->g:Lcom/twitter/android/media/selection/MediaAttachment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/n;->g:Lcom/twitter/android/media/selection/MediaAttachment;

    invoke-virtual {v0}, Lcom/twitter/android/media/selection/MediaAttachment;->a()Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/twitter/app/dm/n;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/twitter/app/dm/n;->d:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/n;->a(Landroid/net/Uri;)V

    .line 88
    :cond_0
    return-void
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/twitter/app/dm/n;->b:Lcom/twitter/app/dm/n$a;

    iget-object v1, p0, Lcom/twitter/app/dm/n;->c:Lcom/twitter/android/media/selection/a;

    invoke-interface {v0, p1, p2, p3, v1}, Lcom/twitter/app/dm/n$a;->a(IILandroid/content/Intent;Lcom/twitter/android/media/selection/a;)V

    .line 140
    return-void
.end method

.method public a(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/twitter/app/dm/n;->b:Lcom/twitter/app/dm/n$a;

    iget-object v1, p0, Lcom/twitter/app/dm/n;->c:Lcom/twitter/android/media/selection/a;

    invoke-interface {v0, p1, v1}, Lcom/twitter/app/dm/n$a;->a(Landroid/net/Uri;Lcom/twitter/android/media/selection/a;)V

    .line 82
    return-void
.end method

.method public a(Lcom/twitter/android/media/selection/MediaAttachment;)V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/twitter/app/dm/n;->b:Lcom/twitter/app/dm/n$a;

    iget-object v1, p0, Lcom/twitter/app/dm/n;->c:Lcom/twitter/android/media/selection/a;

    invoke-interface {v0, p1, v1}, Lcom/twitter/app/dm/n$a;->a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/media/selection/a;)V

    .line 78
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 135
    iput-boolean p1, p0, Lcom/twitter/app/dm/n;->f:Z

    .line 136
    return-void
.end method

.method public a(ZI)V
    .locals 2

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/twitter/app/dm/n;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 57
    if-eqz p1, :cond_1

    .line 58
    invoke-virtual {p0}, Lcom/twitter/app/dm/n;->a()V

    .line 63
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/app/dm/n;->s()Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/n;->d(Lcom/twitter/android/media/selection/MediaAttachment;)V

    .line 74
    :cond_0
    :goto_1
    return-void

    .line 60
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/dm/n;->b:Lcom/twitter/app/dm/n$a;

    invoke-interface {v0, p2}, Lcom/twitter/app/dm/n$a;->b(I)V

    goto :goto_0

    .line 69
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/app/dm/n;->l()Lcom/twitter/android/media/selection/MediaAttachment;

    move-result-object v0

    .line 70
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/dm/n;->f()Z

    move-result v1

    if-nez v1, :cond_0

    .line 71
    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/n;->a(Lcom/twitter/android/media/selection/MediaAttachment;)V

    goto :goto_1
.end method

.method public b()V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/app/dm/n;->b:Lcom/twitter/app/dm/n$a;

    invoke-interface {v0}, Lcom/twitter/app/dm/n$a;->M()V

    .line 96
    return-void
.end method

.method public b(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/twitter/app/dm/n;->b:Lcom/twitter/app/dm/n$a;

    iget-object v1, p0, Lcom/twitter/app/dm/n;->c:Lcom/twitter/android/media/selection/a;

    invoke-interface {v0, p1, v1}, Lcom/twitter/app/dm/n$a;->b(Landroid/net/Uri;Lcom/twitter/android/media/selection/a;)V

    .line 92
    return-void
.end method

.method public b(Lcom/twitter/android/media/selection/MediaAttachment;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/twitter/app/dm/n;->g:Lcom/twitter/android/media/selection/MediaAttachment;

    .line 165
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/twitter/app/dm/n;->x()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/app/dm/n;->s()Z

    move-result v0

    if-nez v0, :cond_1

    .line 100
    invoke-direct {p0}, Lcom/twitter/app/dm/n;->z()Landroid/net/Uri;

    move-result-object v0

    .line 101
    if-eqz v0, :cond_1

    .line 102
    invoke-virtual {p0}, Lcom/twitter/app/dm/n;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    iget-object v1, p0, Lcom/twitter/app/dm/n;->h:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 107
    :cond_0
    iget-object v1, p0, Lcom/twitter/app/dm/n;->b:Lcom/twitter/app/dm/n$a;

    invoke-interface {v1, v0}, Lcom/twitter/app/dm/n$a;->a(Landroid/net/Uri;)V

    .line 108
    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/n;->b(Landroid/net/Uri;)V

    .line 111
    :cond_1
    return-void
.end method

.method public c(Lcom/twitter/android/media/selection/MediaAttachment;)V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/twitter/app/dm/n;->a:Lcom/twitter/app/dm/n$b;

    invoke-interface {v0}, Lcom/twitter/app/dm/n$b;->bj_()V

    .line 220
    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/n;->b(Lcom/twitter/android/media/selection/MediaAttachment;)V

    .line 222
    iget-object v0, p0, Lcom/twitter/app/dm/n;->a:Lcom/twitter/app/dm/n$b;

    invoke-interface {v0}, Lcom/twitter/app/dm/n$b;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 225
    invoke-direct {p0}, Lcom/twitter/app/dm/n;->y()V

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/n;->g:Lcom/twitter/android/media/selection/MediaAttachment;

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/n;->d(Lcom/twitter/android/media/selection/MediaAttachment;)V

    .line 229
    return-void
.end method

.method public c(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/twitter/app/dm/n;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public d(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/twitter/app/dm/n;->d:Landroid/net/Uri;

    .line 144
    return-void
.end method

.method public d(Lcom/twitter/android/media/selection/MediaAttachment;)V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/twitter/app/dm/n;->b:Lcom/twitter/app/dm/n$a;

    invoke-interface {v0, p1}, Lcom/twitter/app/dm/n$a;->b(Lcom/twitter/android/media/selection/MediaAttachment;)V

    .line 237
    return-void
.end method

.method public d()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 114
    iget-object v1, p0, Lcom/twitter/app/dm/n;->g:Lcom/twitter/android/media/selection/MediaAttachment;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/app/dm/n;->g:Lcom/twitter/android/media/selection/MediaAttachment;

    iget v1, v1, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127
    iget-object v0, p0, Lcom/twitter/app/dm/n;->h:Ljava/util/Set;

    return-object v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/twitter/app/dm/n;->f:Z

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/twitter/app/dm/n;->d:Landroid/net/Uri;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/twitter/app/dm/n;->d:Landroid/net/Uri;

    return-object v0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/twitter/app/dm/n;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/n;->e:Landroid/net/Uri;

    iget-object v1, p0, Lcom/twitter/app/dm/n;->d:Landroid/net/Uri;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/twitter/app/dm/n;->d:Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/app/dm/n;->e:Landroid/net/Uri;

    .line 161
    return-void
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/twitter/app/dm/n;->g:Lcom/twitter/android/media/selection/MediaAttachment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/n;->g:Lcom/twitter/android/media/selection/MediaAttachment;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/selection/MediaAttachment;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Lcom/twitter/android/media/selection/MediaAttachment;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/twitter/app/dm/n;->g:Lcom/twitter/android/media/selection/MediaAttachment;

    return-object v0
.end method

.method public m()Lcom/twitter/model/media/EditableMedia;
    .locals 2

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/twitter/app/dm/n;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/n;->g:Lcom/twitter/android/media/selection/MediaAttachment;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/selection/MediaAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()V
    .locals 1

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/twitter/app/dm/n;->t()V

    .line 183
    invoke-virtual {p0}, Lcom/twitter/app/dm/n;->v()V

    .line 185
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/n;->d(Lcom/twitter/android/media/selection/MediaAttachment;)V

    .line 186
    invoke-virtual {p0}, Lcom/twitter/app/dm/n;->r()V

    .line 187
    return-void
.end method

.method public o()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 192
    invoke-virtual {p0}, Lcom/twitter/app/dm/n;->c()V

    .line 193
    invoke-virtual {p0}, Lcom/twitter/app/dm/n;->b()V

    .line 197
    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/n;->b(Lcom/twitter/android/media/selection/MediaAttachment;)V

    .line 198
    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/n;->d(Landroid/net/Uri;)V

    .line 199
    invoke-virtual {p0}, Lcom/twitter/app/dm/n;->j()V

    .line 200
    return-void
.end method

.method public p()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 203
    invoke-direct {p0}, Lcom/twitter/app/dm/n;->z()Landroid/net/Uri;

    move-result-object v0

    .line 204
    invoke-virtual {p0, v1}, Lcom/twitter/app/dm/n;->b(Lcom/twitter/android/media/selection/MediaAttachment;)V

    .line 205
    invoke-virtual {p0, v1}, Lcom/twitter/app/dm/n;->d(Landroid/net/Uri;)V

    .line 206
    if-eqz v0, :cond_0

    .line 207
    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/n;->b(Landroid/net/Uri;)V

    .line 209
    :cond_0
    return-void
.end method

.method public q()V
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lcom/twitter/app/dm/n;->g:Lcom/twitter/android/media/selection/MediaAttachment;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/twitter/app/dm/n;->g:Lcom/twitter/android/media/selection/MediaAttachment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/selection/MediaAttachment;->a(Lcom/twitter/android/media/selection/MediaAttachment;)V

    .line 215
    :cond_0
    return-void
.end method

.method public r()V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/twitter/app/dm/n;->a:Lcom/twitter/app/dm/n$b;

    invoke-interface {v0}, Lcom/twitter/app/dm/n$b;->d()V

    .line 233
    return-void
.end method

.method public s()Z
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/twitter/app/dm/n;->a:Lcom/twitter/app/dm/n$b;

    invoke-interface {v0}, Lcom/twitter/app/dm/n$b;->f()Z

    move-result v0

    return v0
.end method

.method public t()V
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/twitter/app/dm/n;->a:Lcom/twitter/app/dm/n$b;

    invoke-interface {v0}, Lcom/twitter/app/dm/n$b;->g()V

    .line 245
    return-void
.end method

.method public u()V
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/twitter/app/dm/n;->a:Lcom/twitter/app/dm/n$b;

    invoke-interface {v0}, Lcom/twitter/app/dm/n$b;->h()V

    .line 249
    return-void
.end method

.method public v()V
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/twitter/app/dm/n;->a:Lcom/twitter/app/dm/n$b;

    invoke-interface {v0}, Lcom/twitter/app/dm/n$b;->i()V

    .line 253
    return-void
.end method

.method public w()V
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/twitter/app/dm/n;->a:Lcom/twitter/app/dm/n$b;

    invoke-interface {v0}, Lcom/twitter/app/dm/n$b;->j()V

    .line 257
    return-void
.end method

.method public x()Z
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/twitter/app/dm/n;->g:Lcom/twitter/android/media/selection/MediaAttachment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/n;->g:Lcom/twitter/android/media/selection/MediaAttachment;

    invoke-virtual {v0}, Lcom/twitter/android/media/selection/MediaAttachment;->c()Lcom/twitter/model/media/MediaSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/media/MediaSource;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
