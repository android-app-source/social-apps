.class public Lcom/twitter/app/dm/aa;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/aa$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/app/dm/ae;

.field private final b:Lcom/twitter/app/dm/ae;

.field private final c:Lcom/twitter/app/dm/ae;

.field private final d:Lcom/twitter/app/dm/ae;


# direct methods
.method private constructor <init>(Lcom/twitter/app/dm/aa$a;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    invoke-static {p1}, Lcom/twitter/app/dm/aa$a;->a(Lcom/twitter/app/dm/aa$a;)Lcom/twitter/app/dm/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/aa;->b:Lcom/twitter/app/dm/ae;

    .line 17
    invoke-static {p1}, Lcom/twitter/app/dm/aa$a;->b(Lcom/twitter/app/dm/aa$a;)Lcom/twitter/app/dm/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/aa;->d:Lcom/twitter/app/dm/ae;

    .line 18
    invoke-static {p1}, Lcom/twitter/app/dm/aa$a;->c(Lcom/twitter/app/dm/aa$a;)Lcom/twitter/app/dm/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/aa;->a:Lcom/twitter/app/dm/ae;

    .line 19
    invoke-static {p1}, Lcom/twitter/app/dm/aa$a;->d(Lcom/twitter/app/dm/aa$a;)Lcom/twitter/app/dm/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/aa;->c:Lcom/twitter/app/dm/ae;

    .line 20
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/app/dm/aa$a;Lcom/twitter/app/dm/aa$1;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/aa;-><init>(Lcom/twitter/app/dm/aa$a;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/app/dm/ab;)V
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p1, Lcom/twitter/app/dm/ab;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lcom/twitter/app/dm/ab;->f:Z

    if-nez v0, :cond_0

    .line 24
    iget-object v0, p0, Lcom/twitter/app/dm/aa;->c:Lcom/twitter/app/dm/ae;

    invoke-interface {v0, p1}, Lcom/twitter/app/dm/ae;->a(Lcom/twitter/app/dm/ab;)V

    .line 28
    :goto_0
    return-void

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/aa;->c:Lcom/twitter/app/dm/ae;

    invoke-interface {v0, p1}, Lcom/twitter/app/dm/ae;->b(Lcom/twitter/app/dm/ab;)V

    goto :goto_0
.end method

.method public b(Lcom/twitter/app/dm/ab;)V
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p1, Lcom/twitter/app/dm/ab;->d:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p1, Lcom/twitter/app/dm/ab;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lcom/twitter/app/dm/ab;->a:Z

    if-eqz v0, :cond_2

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/aa;->b:Lcom/twitter/app/dm/ae;

    invoke-interface {v0, p1}, Lcom/twitter/app/dm/ae;->b(Lcom/twitter/app/dm/ab;)V

    .line 33
    iget-boolean v0, p1, Lcom/twitter/app/dm/ab;->a:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p1, Lcom/twitter/app/dm/ab;->c:Z

    if-eqz v0, :cond_1

    .line 34
    iget-object v0, p0, Lcom/twitter/app/dm/aa;->d:Lcom/twitter/app/dm/ae;

    invoke-interface {v0, p1}, Lcom/twitter/app/dm/ae;->a(Lcom/twitter/app/dm/ab;)V

    .line 35
    iget-object v0, p0, Lcom/twitter/app/dm/aa;->a:Lcom/twitter/app/dm/ae;

    invoke-interface {v0, p1}, Lcom/twitter/app/dm/ae;->b(Lcom/twitter/app/dm/ab;)V

    .line 45
    :goto_0
    return-void

    .line 37
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/dm/aa;->d:Lcom/twitter/app/dm/ae;

    invoke-interface {v0, p1}, Lcom/twitter/app/dm/ae;->b(Lcom/twitter/app/dm/ab;)V

    .line 38
    iget-object v0, p0, Lcom/twitter/app/dm/aa;->a:Lcom/twitter/app/dm/ae;

    invoke-interface {v0, p1}, Lcom/twitter/app/dm/ae;->a(Lcom/twitter/app/dm/ab;)V

    goto :goto_0

    .line 41
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/dm/aa;->b:Lcom/twitter/app/dm/ae;

    invoke-interface {v0, p1}, Lcom/twitter/app/dm/ae;->a(Lcom/twitter/app/dm/ab;)V

    .line 42
    iget-object v0, p0, Lcom/twitter/app/dm/aa;->a:Lcom/twitter/app/dm/ae;

    invoke-interface {v0, p1}, Lcom/twitter/app/dm/ae;->b(Lcom/twitter/app/dm/ab;)V

    .line 43
    iget-object v0, p0, Lcom/twitter/app/dm/aa;->d:Lcom/twitter/app/dm/ae;

    invoke-interface {v0, p1}, Lcom/twitter/app/dm/ae;->b(Lcom/twitter/app/dm/ab;)V

    goto :goto_0
.end method
