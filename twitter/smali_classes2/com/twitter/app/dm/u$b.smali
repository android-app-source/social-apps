.class Lcom/twitter/app/dm/u$b;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/dm/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/twitter/model/core/TwitterUser;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/dm/u;

.field private final b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/app/dm/u$a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:J


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/u;Ljava/util/Collection;Lcom/twitter/app/dm/u$a;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/twitter/app/dm/u$a;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 50
    iput-object p1, p0, Lcom/twitter/app/dm/u$b;->a:Lcom/twitter/app/dm/u;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 51
    iput-object p2, p0, Lcom/twitter/app/dm/u$b;->b:Ljava/util/Collection;

    .line 52
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/app/dm/u$b;->c:Ljava/lang/ref/WeakReference;

    .line 53
    iput-wide p4, p0, Lcom/twitter/app/dm/u$b;->d:J

    .line 54
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/twitter/app/dm/u$b;->d:J

    invoke-static {v0, v1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/u$b;->b:Ljava/util/Collection;

    .line 59
    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->e(Ljava/util/Collection;)[J

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/t;->a([J)Ljava/util/List;

    move-result-object v0

    .line 58
    return-object v0
.end method

.method protected a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    if-eqz p1, :cond_0

    .line 65
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 66
    iget-object v2, p0, Lcom/twitter/app/dm/u$b;->a:Lcom/twitter/app/dm/u;

    invoke-static {v2}, Lcom/twitter/app/dm/u;->a(Lcom/twitter/app/dm/u;)Ljava/util/Map;

    move-result-object v2

    iget-wide v4, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/u$b;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/u$a;

    .line 70
    if-eqz v0, :cond_1

    .line 71
    iget-object v1, p0, Lcom/twitter/app/dm/u$b;->a:Lcom/twitter/app/dm/u;

    invoke-static {v1}, Lcom/twitter/app/dm/u;->a(Lcom/twitter/app/dm/u;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/app/dm/u$a;->a(Ljava/util/Map;)V

    .line 73
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/dm/u$b;->a:Lcom/twitter/app/dm/u;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/app/dm/u;->a(Lcom/twitter/app/dm/u;Z)Z

    .line 74
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 45
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/u$b;->a([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 45
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/u$b;->a(Ljava/util/List;)V

    return-void
.end method
