.class public Lcom/twitter/app/dm/l;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lcom/twitter/app/dm/h$a;

    invoke-direct {v0}, Lcom/twitter/app/dm/h$a;-><init>()V

    invoke-virtual {v0}, Lcom/twitter/app/dm/h$a;->e()Lcom/twitter/app/dm/h;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/h;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/app/dm/b;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 82
    const-class v0, Lcom/twitter/app/dm/DMActivity;

    invoke-static {p0, v0, p1}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Ljava/lang/Class;Lcom/twitter/app/dm/b;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/app/dm/h;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/twitter/app/dm/DMActivity;

    invoke-static {p0, v0, p1}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Ljava/lang/Class;Lcom/twitter/app/dm/b;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/app/dm/j;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/j;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/app/dm/j;Z)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/twitter/app/dm/DMActivity;

    invoke-static {p0, v0, p1, p2}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Ljava/lang/Class;Lcom/twitter/app/dm/b;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/app/dm/r;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 66
    const-class v0, Lcom/twitter/app/dm/ShareViaDMActivity;

    invoke-static {p0, v0, p1}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Ljava/lang/Class;Lcom/twitter/app/dm/b;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/Class;Lcom/twitter/app/dm/b;)Landroid/content/Intent;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/app/common/base/TwitterFragmentActivity;",
            ">;",
            "Lcom/twitter/app/dm/b;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 109
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Ljava/lang/Class;Lcom/twitter/app/dm/b;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/Class;Lcom/twitter/app/dm/b;Z)Landroid/content/Intent;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/app/common/base/TwitterFragmentActivity;",
            ">;",
            "Lcom/twitter/app/dm/b;",
            "Z)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 116
    new-instance v0, Lcom/twitter/app/common/base/h;

    invoke-direct {v0}, Lcom/twitter/app/common/base/h;-><init>()V

    .line 117
    invoke-virtual {v0, p3}, Lcom/twitter/app/common/base/h;->d(Z)Lcom/twitter/app/common/base/h;

    move-result-object v0

    .line 118
    invoke-virtual {v0, p0, p1}, Lcom/twitter/app/common/base/h;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 119
    invoke-virtual {p2}, Lcom/twitter/app/dm/b;->n()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 116
    return-object v0
.end method

.method public static b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 74
    new-instance v0, Lcom/twitter/app/dm/r$a;

    invoke-direct {v0}, Lcom/twitter/app/dm/r$a;-><init>()V

    invoke-virtual {v0}, Lcom/twitter/app/dm/r$a;->e()Lcom/twitter/app/dm/r;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/r;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;Lcom/twitter/app/dm/b;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 96
    const-class v0, Lcom/twitter/app/dm/RootDMActivity;

    invoke-static {p0, v0, p1}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Ljava/lang/Class;Lcom/twitter/app/dm/b;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static c(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lcom/twitter/app/dm/b$b;

    invoke-direct {v0}, Lcom/twitter/app/dm/b$b;-><init>()V

    invoke-virtual {v0}, Lcom/twitter/app/dm/b$b;->a()Lcom/twitter/app/dm/b;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/b;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
