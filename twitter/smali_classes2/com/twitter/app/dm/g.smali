.class public Lcom/twitter/app/dm/g;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/autocomplete/SuggestionEditText$c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/twitter/android/autocomplete/SuggestionEditText$c",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/Session;

.field private final c:Lcom/twitter/app/dm/g$a;

.field private final d:Lcom/twitter/app/dm/widget/DMRecipientSearch;

.field private final e:Lcom/twitter/android/autocomplete/SuggestionEditText;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/autocomplete/SuggestionEditText",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/twitter/android/suggestionselection/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/suggestionselection/a",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final g:I

.field private final h:Z

.field private final i:Z

.field private final j:Z

.field private k:Ljava/lang/String;

.field private l:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/app/dm/g$a;Lcom/twitter/app/dm/widget/DMRecipientSearch;Lcom/twitter/android/autocomplete/SuggestionEditText;Lcom/twitter/android/suggestionselection/a;ZZZI)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/library/client/Session;",
            "Lcom/twitter/app/dm/g$a;",
            "Lcom/twitter/app/dm/widget/DMRecipientSearch;",
            "Lcom/twitter/android/autocomplete/SuggestionEditText",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/twitter/android/suggestionselection/a",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;ZZZI)V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lcom/twitter/app/dm/g;->a:Landroid/content/Context;

    .line 69
    iput-object p2, p0, Lcom/twitter/app/dm/g;->b:Lcom/twitter/library/client/Session;

    .line 70
    iput-object p3, p0, Lcom/twitter/app/dm/g;->c:Lcom/twitter/app/dm/g$a;

    .line 71
    iput-object p4, p0, Lcom/twitter/app/dm/g;->d:Lcom/twitter/app/dm/widget/DMRecipientSearch;

    .line 72
    iput-object p5, p0, Lcom/twitter/app/dm/g;->e:Lcom/twitter/android/autocomplete/SuggestionEditText;

    .line 73
    iput-object p6, p0, Lcom/twitter/app/dm/g;->f:Lcom/twitter/android/suggestionselection/a;

    .line 75
    iput-boolean p7, p0, Lcom/twitter/app/dm/g;->h:Z

    .line 76
    iput-boolean p8, p0, Lcom/twitter/app/dm/g;->i:Z

    .line 77
    iput-boolean p9, p0, Lcom/twitter/app/dm/g;->j:Z

    .line 78
    iput p10, p0, Lcom/twitter/app/dm/g;->g:I

    .line 80
    const/4 v0, 0x1

    invoke-virtual {p5, v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setLongClickable(Z)V

    .line 81
    invoke-virtual {p5, p0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setQueryTransformer(Lcom/twitter/android/autocomplete/SuggestionEditText$c;)V

    .line 83
    invoke-virtual {p0}, Lcom/twitter/app/dm/g;->a()V

    .line 84
    return-void
.end method

.method private a(J)I
    .locals 5

    .prologue
    .line 216
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v0

    .line 217
    invoke-direct {p0}, Lcom/twitter/app/dm/g;->g()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Iterable;)Lcom/twitter/util/collection/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/g;->f:Lcom/twitter/android/suggestionselection/a;

    .line 218
    invoke-virtual {v1}, Lcom/twitter/android/suggestionselection/a;->d()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Iterable;)Lcom/twitter/util/collection/o;

    move-result-object v0

    .line 219
    const-wide/16 v2, -0x1

    cmp-long v1, p1, v2

    if-eqz v1, :cond_0

    .line 220
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 222
    :cond_0
    iget-object v1, p0, Lcom/twitter/app/dm/g;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/o;->d(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 223
    invoke-virtual {v0}, Lcom/twitter/util/collection/o;->h()I

    move-result v0

    return v0
.end method

.method private a(JLcom/twitter/model/core/TwitterUser;)V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p3, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/app/dm/g;->a(JLjava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/twitter/app/dm/g;->c:Lcom/twitter/app/dm/g$a;

    invoke-interface {v0, p1, p2, p3}, Lcom/twitter/app/dm/g$a;->a(JLcom/twitter/model/core/TwitterUser;)V

    .line 143
    return-void
.end method

.method private a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/dms/t;)V
    .locals 3

    .prologue
    .line 180
    iget-boolean v0, p2, Lcom/twitter/model/dms/t;->a:Z

    if-eqz v0, :cond_0

    .line 181
    iget-wide v0, p1, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-object v2, p1, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    invoke-static {v2}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/app/dm/g;->a(JLjava/lang/String;)V

    .line 190
    :goto_0
    iget-object v0, p0, Lcom/twitter/app/dm/g;->c:Lcom/twitter/app/dm/g$a;

    invoke-interface {v0, p1, p2}, Lcom/twitter/app/dm/g$a;->a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/dms/t;)V

    .line 191
    return-void

    .line 183
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/twitter/app/dm/g;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a029a

    .line 184
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0616

    const/4 v2, 0x0

    .line 185
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 186
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 187
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private a(Lcom/twitter/model/dms/q;)V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/twitter/app/dm/g;->c:Lcom/twitter/app/dm/g$a;

    invoke-interface {v0, p1}, Lcom/twitter/app/dm/g$a;->a(Lcom/twitter/model/dms/q;)V

    .line 147
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 150
    new-instance v0, Lcom/twitter/library/api/dm/i;

    iget-object v1, p0, Lcom/twitter/app/dm/g;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/app/dm/g;->b:Lcom/twitter/library/client/Session;

    .line 151
    invoke-static {p1}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/dm/i;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/util/Collection;)V

    .line 152
    iget-object v1, v0, Lcom/twitter/library/api/dm/i;->d:Ljava/lang/String;

    iput-object v1, p0, Lcom/twitter/app/dm/g;->k:Ljava/lang/String;

    .line 153
    iget-object v1, p0, Lcom/twitter/app/dm/g;->c:Lcom/twitter/app/dm/g$a;

    invoke-interface {v1, v0}, Lcom/twitter/app/dm/g$a;->a(Lcom/twitter/library/api/dm/i;)V

    .line 154
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 116
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/app/dm/g;->k:Ljava/lang/String;

    .line 117
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 207
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/g;->f:Lcom/twitter/android/suggestionselection/a;

    .line 208
    invoke-virtual {v1}, Lcom/twitter/android/suggestionselection/a;->d()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Iterable;)Lcom/twitter/util/collection/o;

    move-result-object v0

    .line 209
    invoke-direct {p0}, Lcom/twitter/app/dm/g;->g()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Iterable;)Lcom/twitter/util/collection/o;

    move-result-object v0

    .line 210
    invoke-virtual {v0}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 211
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/app/dm/g;->l:Z

    .line 212
    iget-object v0, p0, Lcom/twitter/app/dm/g;->e:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->b()V

    .line 213
    return-void

    .line 211
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 227
    invoke-direct {p0}, Lcom/twitter/app/dm/g;->g()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 232
    iget-object v0, p0, Lcom/twitter/app/dm/g;->f:Lcom/twitter/android/suggestionselection/a;

    invoke-virtual {v0}, Lcom/twitter/android/suggestionselection/a;->b()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/twitter/app/dm/g;->l:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/dm/g;->h:Z

    if-nez v0, :cond_0

    .line 92
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/app/dm/g;->j:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/app/dm/g;->i:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 94
    :goto_0
    invoke-static {p1, v0}, Lmx;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 92
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a()V
    .locals 8
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 101
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    .line 102
    new-instance v2, Lcom/twitter/util/a;

    iget-object v3, p0, Lcom/twitter/app/dm/g;->a:Landroid/content/Context;

    iget-object v4, p0, Lcom/twitter/app/dm/g;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    const-string/jumbo v6, "dm"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    .line 103
    const-string/jumbo v3, "followers_timestamp"

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/twitter/util/a;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    const-wide/32 v6, 0x5265c00

    add-long/2addr v4, v6

    cmp-long v3, v4, v0

    if-gez v3, :cond_0

    .line 104
    invoke-virtual {v2}, Lcom/twitter/util/a;->a()Lcom/twitter/util/a$a;

    move-result-object v2

    const-string/jumbo v3, "followers_timestamp"

    invoke-virtual {v2, v3, v0, v1}, Lcom/twitter/util/a$a;->a(Ljava/lang/String;J)Lcom/twitter/util/a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/a$a;->apply()V

    .line 105
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    new-instance v1, Lbhn;

    iget-object v2, p0, Lcom/twitter/app/dm/g;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/app/dm/g;->b:Lcom/twitter/library/client/Session;

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4}, Lbhn;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;I)V

    const/16 v2, 0x190

    .line 107
    invoke-virtual {v1, v2}, Lbhn;->a(I)Lbhn;

    move-result-object v1

    .line 106
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;)Ljava/lang/String;

    .line 109
    :cond_0
    return-void
.end method

.method a(JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 194
    invoke-direct {p0, p1, p2}, Lcom/twitter/app/dm/g;->a(J)I

    move-result v0

    iget v1, p0, Lcom/twitter/app/dm/g;->g:I

    if-gt v0, v1, :cond_1

    .line 195
    iget-object v0, p0, Lcom/twitter/app/dm/g;->f:Lcom/twitter/android/suggestionselection/a;

    invoke-virtual {v0}, Lcom/twitter/android/suggestionselection/a;->d()Ljava/util/Set;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/twitter/app/dm/g;->f:Lcom/twitter/android/suggestionselection/a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/android/suggestionselection/a;->a(JLjava/lang/String;)V

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/g;->c:Lcom/twitter/app/dm/g$a;

    invoke-interface {v0}, Lcom/twitter/app/dm/g$a;->c()V

    .line 199
    invoke-direct {p0}, Lcom/twitter/app/dm/g;->e()V

    .line 200
    iget-object v0, p0, Lcom/twitter/app/dm/g;->d:Lcom/twitter/app/dm/widget/DMRecipientSearch;

    invoke-direct {p0}, Lcom/twitter/app/dm/g;->f()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/widget/DMRecipientSearch;->a(Z)V

    .line 204
    :goto_0
    return-void

    .line 202
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/dm/g;->a:Landroid/content/Context;

    const v1, 0x7f0a02f4

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/api/dm/i;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 157
    iget-object v0, p1, Lcom/twitter/library/api/dm/i;->d:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/app/dm/g;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    invoke-virtual {p1}, Lcom/twitter/library/api/dm/i;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 159
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 160
    iget-object v0, p1, Lcom/twitter/library/api/dm/i;->a:Lcom/twitter/model/dms/ah;

    iget-object v0, v0, Lcom/twitter/model/dms/ah;->c:Ljava/util/List;

    .line 161
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 162
    iget-object v0, p0, Lcom/twitter/app/dm/g;->a:Landroid/content/Context;

    const v1, 0x7f0a0298

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 177
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    iget-object v1, p1, Lcom/twitter/library/api/dm/i;->a:Lcom/twitter/model/dms/ah;

    iget-object v2, v1, Lcom/twitter/model/dms/ah;->b:Ljava/util/Map;

    .line 165
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 166
    iget-object v1, v0, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    .line 167
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/t;

    .line 168
    :goto_2
    if-eqz v1, :cond_2

    .line 169
    invoke-direct {p0, v0, v1}, Lcom/twitter/app/dm/g;->a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/dms/t;)V

    goto :goto_1

    .line 167
    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    .line 174
    :cond_4
    iget-object v0, p0, Lcom/twitter/app/dm/g;->a:Landroid/content/Context;

    const v1, 0x7f0a0260

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public a(JLjava/lang/Object;)Z
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/twitter/app/dm/g;->c:Lcom/twitter/app/dm/g$a;

    invoke-interface {v0, p3, p1, p2}, Lcom/twitter/app/dm/g$a;->a(Ljava/lang/Object;J)V

    .line 129
    instance-of v0, p3, Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_1

    .line 130
    check-cast p3, Lcom/twitter/model/core/TwitterUser;

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/app/dm/g;->a(JLcom/twitter/model/core/TwitterUser;)V

    .line 137
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 131
    :cond_1
    instance-of v0, p3, Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 132
    check-cast p3, Ljava/lang/String;

    invoke-direct {p0, p3}, Lcom/twitter/app/dm/g;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 133
    :cond_2
    instance-of v0, p3, Lcom/twitter/model/dms/q;

    if-eqz v0, :cond_0

    .line 134
    check-cast p3, Lcom/twitter/model/dms/q;

    invoke-direct {p0, p3}, Lcom/twitter/app/dm/g;->a(Lcom/twitter/model/dms/q;)V

    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/twitter/app/dm/g;->d()V

    .line 113
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 120
    invoke-direct {p0}, Lcom/twitter/app/dm/g;->e()V

    .line 121
    iget-object v0, p0, Lcom/twitter/app/dm/g;->c:Lcom/twitter/app/dm/g$a;

    invoke-interface {v0}, Lcom/twitter/app/dm/g$a;->c()V

    .line 122
    iget-object v0, p0, Lcom/twitter/app/dm/g;->d:Lcom/twitter/app/dm/widget/DMRecipientSearch;

    invoke-direct {p0}, Lcom/twitter/app/dm/g;->f()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/widget/DMRecipientSearch;->a(Z)V

    .line 123
    return-void
.end method
