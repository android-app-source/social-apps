.class public Lcom/twitter/app/dm/t;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/app/dm/t;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/t;->b:Ljava/util/Map;

    .line 25
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/t;->c:Ljava/util/Map;

    .line 24
    return-void
.end method

.method public static declared-synchronized a()Lcom/twitter/app/dm/t;
    .locals 2

    .prologue
    .line 29
    const-class v1, Lcom/twitter/app/dm/t;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/app/dm/t;->a:Lcom/twitter/app/dm/t;

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Lcom/twitter/app/dm/t;

    invoke-direct {v0}, Lcom/twitter/app/dm/t;-><init>()V

    sput-object v0, Lcom/twitter/app/dm/t;->a:Lcom/twitter/app/dm/t;

    .line 31
    const-class v0, Lcom/twitter/app/dm/t;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 33
    :cond_0
    sget-object v0, Lcom/twitter/app/dm/t;->a:Lcom/twitter/app/dm/t;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/app/dm/t;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;Lcom/twitter/library/api/dm/q;Lcom/twitter/model/drafts/DraftAttachment;)V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/twitter/app/dm/t;->b:Ljava/util/Map;

    iget-object v1, p2, Lcom/twitter/library/api/dm/q;->d:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    if-eqz p3, :cond_0

    .line 45
    iget-object v0, p0, Lcom/twitter/app/dm/t;->c:Ljava/util/Map;

    invoke-interface {v0, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    :cond_0
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/app/dm/t;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    .line 63
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/twitter/model/drafts/DraftAttachment;->b(Lcom/twitter/model/drafts/DraftAttachment;)Lrx/g;

    goto :goto_0

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/t;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 66
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/app/dm/t;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    .line 56
    if-eqz v0, :cond_0

    .line 57
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/DraftAttachment;->b(Lcom/twitter/model/drafts/DraftAttachment;)Lrx/g;

    .line 59
    :cond_0
    return-void
.end method
