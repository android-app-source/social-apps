.class Lcom/twitter/app/dm/DMConversationFragment$18;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/widget/AttachmentMediaView$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/dm/DMConversationFragment;->b(Lcom/twitter/android/media/selection/MediaAttachment;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/dm/DMConversationFragment;


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/DMConversationFragment;)V
    .locals 0

    .prologue
    .line 3076
    iput-object p1, p0, Lcom/twitter/app/dm/DMConversationFragment$18;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 3079
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$18;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v0}, Lcom/twitter/app/dm/DMConversationFragment;->w(Lcom/twitter/app/dm/DMConversationFragment;)V

    .line 3080
    return-void
.end method

.method public a(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/widget/AttachmentMediaView;)V
    .locals 5

    .prologue
    .line 3084
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$18;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v0}, Lcom/twitter/app/dm/DMConversationFragment;->x(Lcom/twitter/app/dm/DMConversationFragment;)Lcom/twitter/app/dm/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/dm/n;->l()Lcom/twitter/android/media/selection/MediaAttachment;

    move-result-object v0

    .line 3085
    if-eqz v0, :cond_0

    iget v1, v0, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    if-nez v1, :cond_0

    .line 3086
    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment$18;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v1}, Lcom/twitter/app/dm/DMConversationFragment;->y(Lcom/twitter/app/dm/DMConversationFragment;)Lcom/twitter/android/media/selection/c;

    move-result-object v1

    const/4 v2, 0x2

    .line 3087
    invoke-virtual {v0, v2}, Lcom/twitter/android/media/selection/MediaAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableMedia;

    invoke-virtual {v0}, Lcom/twitter/model/media/EditableMedia;->b()Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/app/dm/DMConversationFragment$18;->a:Lcom/twitter/app/dm/DMConversationFragment;

    const/4 v4, 0x1

    .line 3086
    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/model/media/EditableMedia;Landroid/view/View;Lcom/twitter/android/media/selection/a;Z)V

    .line 3090
    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/widget/AttachmentMediaView;)V
    .locals 4

    .prologue
    .line 3095
    instance-of v0, p1, Lcom/twitter/model/media/EditableImage;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 3096
    check-cast v0, Lcom/twitter/model/media/EditableImage;

    .line 3097
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment$18;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-virtual {v2}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/AltTextActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "editable_image"

    .line 3098
    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    .line 3099
    iget-object v2, v0, Lcom/twitter/model/media/EditableImage;->i:Ljava/lang/String;

    invoke-static {v2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3100
    const-string/jumbo v2, "alt_text"

    iget-object v0, v0, Lcom/twitter/model/media/EditableImage;->i:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3102
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$18;->a:Lcom/twitter/app/dm/DMConversationFragment;

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/dm/DMConversationFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3104
    :cond_1
    return-void
.end method

.method public c(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/widget/AttachmentMediaView;)V
    .locals 0

    .prologue
    .line 3108
    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/dm/DMConversationFragment$18;->a(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/widget/AttachmentMediaView;)V

    .line 3109
    return-void
.end method

.method public d(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/widget/AttachmentMediaView;)V
    .locals 4

    .prologue
    .line 3114
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$18;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v0}, Lcom/twitter/app/dm/DMConversationFragment;->x(Lcom/twitter/app/dm/DMConversationFragment;)Lcom/twitter/app/dm/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/dm/n;->l()Lcom/twitter/android/media/selection/MediaAttachment;

    move-result-object v0

    .line 3115
    if-eqz v0, :cond_0

    iget v1, v0, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    if-nez v1, :cond_0

    .line 3116
    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment$18;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v1}, Lcom/twitter/app/dm/DMConversationFragment;->y(Lcom/twitter/app/dm/DMConversationFragment;)Lcom/twitter/android/media/selection/c;

    move-result-object v1

    const/4 v2, 0x2

    .line 3117
    invoke-virtual {v0, v2}, Lcom/twitter/android/media/selection/MediaAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableMedia;

    invoke-virtual {v0}, Lcom/twitter/model/media/EditableMedia;->b()Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment$18;->a:Lcom/twitter/app/dm/DMConversationFragment;

    const/4 v3, 0x3

    .line 3116
    invoke-virtual {v1, v0, v2, v3}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/model/media/EditableMedia;Lcom/twitter/android/media/selection/a;I)V

    .line 3120
    :cond_0
    return-void
.end method
