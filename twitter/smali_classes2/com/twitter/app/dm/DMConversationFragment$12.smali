.class Lcom/twitter/app/dm/DMConversationFragment$12;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/dm/DMConversationFragment;->R()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Ljava/lang/ref/WeakReference",
        "<",
        "Lcom/twitter/app/dm/ag;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/dm/DMConversationFragment;


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/DMConversationFragment;)V
    .locals 0

    .prologue
    .line 639
    iput-object p1, p0, Lcom/twitter/app/dm/DMConversationFragment$12;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 639
    check-cast p1, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/DMConversationFragment$12;->a(Ljava/lang/ref/WeakReference;)V

    return-void
.end method

.method public a(Ljava/lang/ref/WeakReference;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/app/dm/ag;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 642
    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/ag;

    .line 643
    if-eqz v0, :cond_1

    .line 644
    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment$12;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v1}, Lcom/twitter/app/dm/DMConversationFragment;->b(Lcom/twitter/app/dm/DMConversationFragment;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v1

    .line 645
    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment$12;->a:Lcom/twitter/app/dm/DMConversationFragment;

    .line 646
    invoke-static {v2}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcom/twitter/app/dm/DMConversationFragment;)[J

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/util/collection/CollectionUtils;->a([J)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/provider/t;->a(Ljava/util/List;)Ljava/util/Map;

    move-result-object v1

    .line 647
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    .line 649
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/core/TwitterUser;

    .line 650
    iget-object v3, v1, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    .line 651
    if-eqz v3, :cond_0

    .line 652
    invoke-virtual {v1}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5, v3}, Lcom/twitter/app/dm/ag;->a(JLjava/lang/String;)V

    goto :goto_0

    .line 656
    :cond_1
    return-void
.end method
