.class public Lcom/twitter/app/dm/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/dm/ae;


# instance fields
.field private final b:Landroid/view/View;

.field private final c:Landroid/view/View;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/twitter/app/dm/a;->c:Landroid/view/View;

    .line 19
    const v0, 0x7f1307ee

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/a;->b:Landroid/view/View;

    .line 20
    return-void
.end method

.method private a()I
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/app/dm/a;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Lcom/twitter/app/dm/ab;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 25
    iget-boolean v0, p0, Lcom/twitter/app/dm/a;->d:Z

    if-nez v0, :cond_0

    .line 26
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>()V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages"

    aput-object v3, v1, v2

    const-string/jumbo v2, "share_tweet_user_select"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string/jumbo v3, "add_participant_warning"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const/4 v3, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "impression"

    aput-object v3, v1, v2

    .line 27
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 26
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 29
    iput-boolean v4, p0, Lcom/twitter/app/dm/a;->d:Z

    .line 31
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/a;->b:Landroid/view/View;

    const/16 v1, 0x12c

    invoke-static {v0, v1}, Lcom/twitter/app/dm/e;->a(Landroid/view/View;I)V

    .line 32
    return-void
.end method

.method public b(Lcom/twitter/app/dm/ab;)V
    .locals 3

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/app/dm/a;->b:Landroid/view/View;

    const/16 v1, 0x12c

    invoke-direct {p0}, Lcom/twitter/app/dm/a;->a()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/twitter/app/dm/e;->b(Landroid/view/View;II)V

    .line 37
    return-void
.end method
