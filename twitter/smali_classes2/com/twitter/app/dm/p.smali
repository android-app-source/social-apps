.class public Lcom/twitter/app/dm/p;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcpm;

.field private final d:J


# direct methods
.method public constructor <init>(Lcpm;J)V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/p;->a:Ljava/util/Set;

    .line 18
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/p;->b:Ljava/util/Set;

    .line 24
    iput-object p1, p0, Lcom/twitter/app/dm/p;->c:Lcpm;

    .line 25
    iput-wide p2, p0, Lcom/twitter/app/dm/p;->d:J

    .line 26
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/dms/e;)V
    .locals 5

    .prologue
    .line 30
    iget-object v0, p0, Lcom/twitter/app/dm/p;->a:Ljava/util/Set;

    iget-wide v2, p1, Lcom/twitter/model/dms/e;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/twitter/app/dm/p;->c:Lcpm;

    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/p;->d:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "messages:thread::shared_tweet_dm:impression"

    aput-object v4, v2, v3

    .line 32
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    .line 31
    invoke-virtual {v0, v1}, Lcpm;->b(Lcpk;)V

    .line 33
    iget-object v0, p0, Lcom/twitter/app/dm/p;->a:Ljava/util/Set;

    iget-wide v2, p1, Lcom/twitter/model/dms/e;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 35
    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/model/dms/e;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 39
    iget-object v0, p0, Lcom/twitter/app/dm/p;->b:Ljava/util/Set;

    iget-wide v2, p1, Lcom/twitter/model/dms/e;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/model/dms/e;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    invoke-virtual {p1}, Lcom/twitter/model/dms/e;->y()I

    move-result v0

    if-ne v0, v6, :cond_1

    const-string/jumbo v0, "large_emoji_message"

    .line 41
    :goto_0
    iget-object v1, p0, Lcom/twitter/app/dm/p;->c:Lcpm;

    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, p0, Lcom/twitter/app/dm/p;->d:J

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "messages"

    aput-object v5, v3, v4

    const-string/jumbo v4, "thread"

    aput-object v4, v3, v6

    const/4 v4, 0x2

    const/4 v5, 0x0

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aput-object v0, v3, v4

    const/4 v0, 0x4

    const-string/jumbo v4, "impression"

    aput-object v4, v3, v0

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcpm;->b(Lcpk;)V

    .line 43
    iget-object v0, p0, Lcom/twitter/app/dm/p;->b:Ljava/util/Set;

    iget-wide v2, p1, Lcom/twitter/model/dms/e;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 45
    :cond_0
    return-void

    .line 40
    :cond_1
    const-string/jumbo v0, "medium_emoji_message"

    goto :goto_0
.end method
