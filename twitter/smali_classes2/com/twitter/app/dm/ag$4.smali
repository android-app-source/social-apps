.class Lcom/twitter/app/dm/ag$4;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/dm/ag;->e(J)Lrx/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/d",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/dm/ag;


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/ag;)V
    .locals 0

    .prologue
    .line 476
    iput-object p1, p0, Lcom/twitter/app/dm/ag$4;->a:Lcom/twitter/app/dm/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Long;)Ljava/lang/Boolean;
    .locals 6

    .prologue
    .line 479
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 480
    iget-object v0, p0, Lcom/twitter/app/dm/ag$4;->a:Lcom/twitter/app/dm/ag;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/app/dm/ag;->d(J)Ljava/lang/Long;

    move-result-object v0

    .line 481
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/app/dm/ag$4;->a:Lcom/twitter/app/dm/ag;

    .line 482
    invoke-static {v1}, Lcom/twitter/app/dm/ag;->b(Lcom/twitter/app/dm/ag;)Lcqt;

    move-result-object v1

    invoke-interface {v1}, Lcqt;->b()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {}, Lcom/twitter/app/dm/ag;->k()J

    move-result-wide v4

    add-long/2addr v0, v4

    cmp-long v0, v2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    .line 481
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 482
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 476
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/ag$4;->a(Ljava/lang/Long;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
