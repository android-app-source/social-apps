.class public Lcom/twitter/app/dm/DMConversationFragment$c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lbrp;
.implements Lcom/twitter/android/media/selection/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/dm/DMConversationFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/dm/DMConversationFragment;


# direct methods
.method public constructor <init>(Lcom/twitter/app/dm/DMConversationFragment;)V
    .locals 0

    .prologue
    .line 3006
    iput-object p1, p0, Lcom/twitter/app/dm/DMConversationFragment$c;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 1

    .prologue
    .line 3053
    add-int/lit16 v0, p1, 0x7530

    return v0
.end method

.method public a(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 3042
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$c;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-virtual {p0, p2}, Lcom/twitter/app/dm/DMConversationFragment$c;->a(I)I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/twitter/app/dm/DMConversationFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3043
    return-void
.end method

.method public a(Lcom/twitter/android/media/selection/b;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 3016
    invoke-virtual {p1}, Lcom/twitter/android/media/selection/b;->c()Lcom/twitter/android/media/selection/MediaAttachment;

    move-result-object v0

    .line 3017
    if-nez v0, :cond_1

    .line 3038
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 3020
    :cond_1
    iget v1, v0, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    packed-switch v1, :pswitch_data_0

    .line 3034
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$c;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v0}, Lcom/twitter/app/dm/DMConversationFragment;->v(Lcom/twitter/app/dm/DMConversationFragment;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a028d

    invoke-static {v0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 3022
    :pswitch_1
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/selection/MediaAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 3023
    if-eqz v0, :cond_0

    .line 3024
    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment$c;->a:Lcom/twitter/app/dm/DMConversationFragment;

    new-instance v2, Lcom/twitter/library/api/dm/w;

    iget-object v3, p0, Lcom/twitter/app/dm/DMConversationFragment$c;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v3}, Lcom/twitter/app/dm/DMConversationFragment;->u(Lcom/twitter/app/dm/DMConversationFragment;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/app/dm/DMConversationFragment$c;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v4}, Lcom/twitter/app/dm/DMConversationFragment;->s(Lcom/twitter/app/dm/DMConversationFragment;)Lcom/twitter/library/client/Session;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/app/dm/DMConversationFragment$c;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v5}, Lcom/twitter/app/dm/DMConversationFragment;->a(Lcom/twitter/app/dm/DMConversationFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/twitter/library/api/dm/w;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Lcom/twitter/model/media/EditableMedia;)V

    const/16 v0, 0xf

    invoke-static {v1, v2, v0, v6}, Lcom/twitter/app/dm/DMConversationFragment;->b(Lcom/twitter/app/dm/DMConversationFragment;Lcom/twitter/library/service/s;II)Z

    goto :goto_0

    .line 3020
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/twitter/android/media/selection/MediaAttachment;)Z
    .locals 1

    .prologue
    .line 3011
    const/4 v0, 0x1

    return v0
.end method

.method public b(I)I
    .locals 1

    .prologue
    .line 3064
    add-int/lit16 v0, p1, -0x7530

    return v0
.end method
