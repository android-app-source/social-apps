.class public Lcom/twitter/app/dm/q;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/q$a;,
        Lcom/twitter/app/dm/q$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lbta;

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/app/dm/q$b;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/String;

.field private final e:Landroid/net/Uri;

.field private final f:Z


# direct methods
.method private constructor <init>(Lcom/twitter/app/dm/q$a;)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 28
    invoke-static {p1}, Lcom/twitter/app/dm/q$a;->a(Lcom/twitter/app/dm/q$a;)Lbta;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/q;->a:Lbta;

    .line 29
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, Lcom/twitter/app/dm/q$a;->b(Lcom/twitter/app/dm/q$a;)Lcom/twitter/app/dm/q$b;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/app/dm/q;->b:Ljava/lang/ref/WeakReference;

    .line 30
    invoke-static {p1}, Lcom/twitter/app/dm/q$a;->c(Lcom/twitter/app/dm/q$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/q;->d:Ljava/lang/String;

    .line 31
    invoke-static {p1}, Lcom/twitter/app/dm/q$a;->d(Lcom/twitter/app/dm/q$a;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/q;->e:Landroid/net/Uri;

    .line 32
    invoke-static {p1}, Lcom/twitter/app/dm/q$a;->e(Lcom/twitter/app/dm/q$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/q;->f:Z

    .line 33
    invoke-static {p1}, Lcom/twitter/app/dm/q$a;->f(Lcom/twitter/app/dm/q$a;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/q;->c:Ljava/util/Set;

    .line 34
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/app/dm/q$a;Lcom/twitter/app/dm/q$1;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/q;-><init>(Lcom/twitter/app/dm/q$a;)V

    return-void
.end method


# virtual methods
.method protected final varargs a([Ljava/lang/Void;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/app/dm/q;->a:Lbta;

    iget-object v1, p0, Lcom/twitter/app/dm/q;->a:Lbta;

    iget-object v2, p0, Lcom/twitter/app/dm/q;->c:Ljava/util/Set;

    .line 40
    invoke-virtual {v1, v2}, Lbta;->a(Ljava/util/Set;)Ljava/lang/Iterable;

    move-result-object v1

    const/4 v2, 0x1

    .line 39
    invoke-virtual {v0, v1, v2}, Lbta;->a(Ljava/lang/Iterable;Z)Lcbi;

    move-result-object v1

    .line 41
    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/q;

    .line 42
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 43
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/model/dms/q;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/app/dm/q;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/q$b;

    .line 49
    if-eqz v0, :cond_0

    .line 50
    iget-object v2, p0, Lcom/twitter/app/dm/q;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/app/dm/q;->c:Ljava/util/Set;

    iget-object v4, p0, Lcom/twitter/app/dm/q;->e:Landroid/net/Uri;

    iget-boolean v5, p0, Lcom/twitter/app/dm/q;->f:Z

    move-object v1, p1

    invoke-interface/range {v0 .. v5}, Lcom/twitter/app/dm/q$b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Landroid/net/Uri;Z)V

    .line 53
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/q;->a([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/q;->a(Ljava/lang/String;)V

    return-void
.end method
