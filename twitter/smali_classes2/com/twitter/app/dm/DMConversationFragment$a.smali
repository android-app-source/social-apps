.class Lcom/twitter/app/dm/DMConversationFragment$a;
.super Lape;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/dm/DMConversationFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/dm/DMConversationFragment;


# direct methods
.method private constructor <init>(Lcom/twitter/app/dm/DMConversationFragment;)V
    .locals 0

    .prologue
    .line 3270
    iput-object p1, p0, Lcom/twitter/app/dm/DMConversationFragment$a;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-direct {p0}, Lape;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/app/dm/DMConversationFragment;Lcom/twitter/app/dm/DMConversationFragment$1;)V
    .locals 0

    .prologue
    .line 3270
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/DMConversationFragment$a;-><init>(Lcom/twitter/app/dm/DMConversationFragment;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/customerservice/network/FeedbackRequestParams;)V
    .locals 4

    .prologue
    .line 3292
    new-instance v0, Lcom/twitter/library/customerservice/network/a;

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment$a;->a:Lcom/twitter/app/dm/DMConversationFragment;

    .line 3293
    invoke-static {v1}, Lcom/twitter/app/dm/DMConversationFragment;->D(Lcom/twitter/app/dm/DMConversationFragment;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment$a;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v2}, Lcom/twitter/app/dm/DMConversationFragment;->s(Lcom/twitter/app/dm/DMConversationFragment;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Lcom/twitter/library/customerservice/network/a;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/customerservice/network/FeedbackRequestParams;)V

    .line 3294
    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment$a;->a:Lcom/twitter/app/dm/DMConversationFragment;

    const/16 v2, 0x11

    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, Lcom/twitter/app/dm/DMConversationFragment;->d(Lcom/twitter/app/dm/DMConversationFragment;Lcom/twitter/library/service/s;II)Z

    .line 3295
    return-void
.end method

.method public a(Lcom/twitter/library/customerservice/network/FeedbackRequestParams;I)V
    .locals 4

    .prologue
    .line 3273
    new-instance v0, Lcom/twitter/library/customerservice/network/b;

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment$a;->a:Lcom/twitter/app/dm/DMConversationFragment;

    .line 3274
    invoke-static {v1}, Lcom/twitter/app/dm/DMConversationFragment;->B(Lcom/twitter/app/dm/DMConversationFragment;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment$a;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v2}, Lcom/twitter/app/dm/DMConversationFragment;->s(Lcom/twitter/app/dm/DMConversationFragment;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1, p2}, Lcom/twitter/library/customerservice/network/b;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/customerservice/network/FeedbackRequestParams;I)V

    .line 3275
    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment$a;->a:Lcom/twitter/app/dm/DMConversationFragment;

    const/16 v2, 0x10

    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcom/twitter/app/dm/DMConversationFragment;Lcom/twitter/library/service/s;II)Z

    .line 3276
    return-void
.end method

.method public a(Lcom/twitter/library/customerservice/network/FeedbackRequestParams;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 3283
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$a;->a:Lcom/twitter/app/dm/DMConversationFragment;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment$a;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v2}, Lcom/twitter/app/dm/DMConversationFragment;->C(Lcom/twitter/app/dm/DMConversationFragment;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "feedback_associated_user_name_key"

    .line 3284
    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "feedback_associated_score_description_key"

    .line 3285
    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "feedback_request_params"

    .line 3286
    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "feedback_scribe_component"

    .line 3287
    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 3283
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/DMConversationFragment;->startActivity(Landroid/content/Intent;)V

    .line 3288
    return-void
.end method
