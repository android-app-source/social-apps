.class public Lcom/twitter/app/dm/i;
.super Lcjr;
.source "Twttr"

# interfaces
.implements Landroid/widget/AbsListView$RecyclerListener;
.implements Laow;
.implements Lcne;
.implements Lcom/twitter/app/dm/aj;
.implements Lcom/twitter/app/dm/m$a;
.implements Lcom/twitter/app/dm/v;
.implements Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$a;
.implements Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/i$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcjr",
        "<",
        "Lcom/twitter/model/dms/m;",
        ">;",
        "Landroid/widget/AbsListView$RecyclerListener;",
        "Laow",
        "<",
        "Ljava/util/List",
        "<",
        "Lcdu;",
        ">;>;",
        "Lcne;",
        "Lcom/twitter/app/dm/aj;",
        "Lcom/twitter/app/dm/m$a;",
        "Lcom/twitter/app/dm/v;",
        "Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$a;",
        "Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$b;"
    }
.end annotation


# static fields
.field private static final a:Ljava/text/SimpleDateFormat;

.field private static final b:Ljava/text/SimpleDateFormat;

.field private static final c:Ljava/text/SimpleDateFormat;


# instance fields
.field private A:Lcom/twitter/app/dm/ag;

.field private B:Lyj;

.field private final C:I

.field private final D:I

.field private E:Lcom/twitter/model/dms/n;

.field private final F:Z

.field private final G:Z

.field private H:J

.field private I:I

.field private final J:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private K:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/app/dm/widget/SentMessageBylineView;",
            ">;"
        }
    .end annotation
.end field

.field private final L:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private M:Z

.field private final N:Lapl;

.field private final O:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcdu;",
            ">;"
        }
    .end annotation
.end field

.field private final P:Lcom/twitter/app/dm/u;

.field private final Q:Lcom/twitter/app/dm/p;

.field private final R:Lcom/twitter/app/dm/o;

.field private S:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/dms/c;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/content/Context;

.field private final e:Lcom/twitter/library/network/t;

.field private final f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final g:Lcom/twitter/app/dm/f;

.field private final h:Lcom/twitter/app/dm/m;

.field private final i:Lcom/twitter/android/bn;

.field private final j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Z

.field private final m:Z

.field private final n:Z

.field private final o:J

.field private p:Z

.field private q:Z

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private final u:Z

.field private final v:Z

.field private final w:Lape;

.field private final x:Lcom/twitter/library/card/m;

.field private y:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcaq;",
            ">;"
        }
    .end annotation
.end field

.field private final z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 129
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0}, Ljava/text/SimpleDateFormat;-><init>()V

    sput-object v0, Lcom/twitter/app/dm/i;->a:Ljava/text/SimpleDateFormat;

    .line 131
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0}, Ljava/text/SimpleDateFormat;-><init>()V

    sput-object v0, Lcom/twitter/app/dm/i;->b:Ljava/text/SimpleDateFormat;

    .line 133
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0}, Ljava/text/SimpleDateFormat;-><init>()V

    sput-object v0, Lcom/twitter/app/dm/i;->c:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/app/dm/i$a;)V
    .locals 6

    .prologue
    .line 193
    invoke-static {p1}, Lcom/twitter/app/dm/i$a;->a(Lcom/twitter/app/dm/i$a;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcjr;-><init>(Landroid/content/Context;)V

    .line 144
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/i;->j:Ljava/util/Set;

    .line 145
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/i;->k:Ljava/util/Set;

    .line 174
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/app/dm/i;->H:J

    .line 175
    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/app/dm/i;->I:I

    .line 177
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/i;->J:Ljava/util/Set;

    .line 178
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/app/dm/i;->K:Ljava/lang/ref/WeakReference;

    .line 181
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/i;->L:Ljava/util/Map;

    .line 186
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/i;->O:Ljava/util/Map;

    .line 190
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/i;->S:Ljava/util/Map;

    .line 194
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 195
    invoke-static {p1}, Lcom/twitter/app/dm/i$a;->a(Lcom/twitter/app/dm/i$a;)Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/dm/i;->d:Landroid/content/Context;

    .line 196
    new-instance v1, Lcom/twitter/library/network/t;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->h()Lcom/twitter/model/account/OAuthToken;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/library/network/t;-><init>(Lcom/twitter/model/account/OAuthToken;)V

    iput-object v1, p0, Lcom/twitter/app/dm/i;->e:Lcom/twitter/library/network/t;

    .line 197
    invoke-static {p1}, Lcom/twitter/app/dm/i$a;->b(Lcom/twitter/app/dm/i$a;)Lcom/twitter/app/dm/f;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/dm/i;->g:Lcom/twitter/app/dm/f;

    .line 198
    invoke-static {p1}, Lcom/twitter/app/dm/i$a;->c(Lcom/twitter/app/dm/i$a;)Lcom/twitter/android/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/dm/i;->i:Lcom/twitter/android/bn;

    .line 199
    invoke-static {p1}, Lcom/twitter/app/dm/i$a;->d(Lcom/twitter/app/dm/i$a;)Lcom/twitter/app/dm/m;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/dm/i;->h:Lcom/twitter/app/dm/m;

    .line 200
    iget-object v1, p0, Lcom/twitter/app/dm/i;->h:Lcom/twitter/app/dm/m;

    invoke-virtual {v1, p0}, Lcom/twitter/app/dm/m;->a(Lcom/twitter/app/dm/m$a;)V

    .line 201
    invoke-static {p1}, Lcom/twitter/app/dm/i$a;->e(Lcom/twitter/app/dm/i$a;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/dm/i;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 203
    invoke-static {p1}, Lcom/twitter/app/dm/i$a;->f(Lcom/twitter/app/dm/i$a;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/app/dm/i;->u:Z

    .line 204
    invoke-static {p1}, Lcom/twitter/app/dm/i$a;->g(Lcom/twitter/app/dm/i$a;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/app/dm/i;->v:Z

    .line 205
    invoke-static {p1}, Lcom/twitter/app/dm/i$a;->h(Lcom/twitter/app/dm/i$a;)Lape;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/dm/i;->w:Lape;

    .line 206
    invoke-static {p1}, Lcom/twitter/app/dm/i$a;->i(Lcom/twitter/app/dm/i$a;)Lcom/twitter/library/card/m;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/dm/i;->x:Lcom/twitter/library/card/m;

    .line 208
    invoke-static {p1}, Lcom/twitter/app/dm/i$a;->j(Lcom/twitter/app/dm/i$a;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/app/dm/i;->l:Z

    .line 209
    invoke-static {p1}, Lcom/twitter/app/dm/i$a;->k(Lcom/twitter/app/dm/i$a;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/app/dm/i;->m:Z

    .line 210
    invoke-static {p1}, Lcom/twitter/app/dm/i$a;->l(Lcom/twitter/app/dm/i$a;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/app/dm/i;->n:Z

    .line 211
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/app/dm/i;->o:J

    .line 214
    sget-object v1, Lcom/twitter/app/dm/i;->a:Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lcom/twitter/app/dm/i;->d:Landroid/content/Context;

    const v3, 0x7f0a0b76

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 215
    sget-object v1, Lcom/twitter/app/dm/i;->b:Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lcom/twitter/app/dm/i;->d:Landroid/content/Context;

    const v3, 0x7f0a0b73

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 216
    sget-object v1, Lcom/twitter/app/dm/i;->c:Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lcom/twitter/app/dm/i;->d:Landroid/content/Context;

    const v3, 0x7f0a0b75

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 218
    invoke-static {p1}, Lcom/twitter/app/dm/i$a;->m(Lcom/twitter/app/dm/i$a;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/app/dm/i;->F:Z

    .line 219
    invoke-static {p1}, Lcom/twitter/app/dm/i$a;->n(Lcom/twitter/app/dm/i$a;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/app/dm/i;->G:Z

    .line 221
    invoke-static {}, Lcom/twitter/library/dm/d;->r()Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/app/dm/i;->z:Z

    .line 223
    iget-object v1, p0, Lcom/twitter/app/dm/i;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 224
    const v2, 0x7f0e056a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/twitter/app/dm/i;->C:I

    .line 225
    iget v2, p0, Lcom/twitter/app/dm/i;->C:I

    const v3, 0x7f0e0076

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v1, v2

    iput v1, p0, Lcom/twitter/app/dm/i;->D:I

    .line 227
    iget-boolean v1, p0, Lcom/twitter/app/dm/i;->z:Z

    if-eqz v1, :cond_0

    .line 228
    new-instance v1, Lyj;

    new-instance v2, Laug;

    new-instance v3, Lwx;

    .line 229
    invoke-virtual {p0}, Lcom/twitter/app/dm/i;->j()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lwx;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-direct {v2, v3}, Laug;-><init>(Lauj;)V

    new-instance v0, Laug;

    new-instance v3, Lww;

    iget-wide v4, p0, Lcom/twitter/app/dm/i;->o:J

    .line 230
    invoke-static {v4, v5}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v4

    invoke-direct {v3, v4}, Lww;-><init>(Lcom/twitter/library/provider/t;)V

    invoke-direct {v0, v3}, Laug;-><init>(Lauj;)V

    invoke-direct {v1, v2, v0}, Lyj;-><init>(Lauj;Lauj;)V

    iput-object v1, p0, Lcom/twitter/app/dm/i;->B:Lyj;

    .line 233
    :cond_0
    invoke-static {p1}, Lcom/twitter/app/dm/i$a;->o(Lcom/twitter/app/dm/i$a;)Lapl;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/i;->N:Lapl;

    .line 234
    new-instance v0, Lcom/twitter/app/dm/u;

    iget-wide v2, p0, Lcom/twitter/app/dm/i;->o:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/app/dm/u;-><init>(J)V

    iput-object v0, p0, Lcom/twitter/app/dm/i;->P:Lcom/twitter/app/dm/u;

    .line 235
    new-instance v0, Lcom/twitter/app/dm/p;

    invoke-static {}, Lcpm;->a()Lcpm;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/app/dm/i;->o:J

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/app/dm/p;-><init>(Lcpm;J)V

    iput-object v0, p0, Lcom/twitter/app/dm/i;->Q:Lcom/twitter/app/dm/p;

    .line 236
    new-instance v0, Lcom/twitter/app/dm/o;

    invoke-direct {v0}, Lcom/twitter/app/dm/o;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/dm/i;->R:Lcom/twitter/app/dm/o;

    .line 237
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/app/dm/i$a;Lcom/twitter/app/dm/i$1;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/i;-><init>(Lcom/twitter/app/dm/i$a;)V

    return-void
.end method

.method private a(ILandroid/view/ViewGroup;Z)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/twitter/app/dm/i;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private a(Lcom/twitter/model/dms/c;Lapn$b;)Lapn$a;
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 468
    invoke-virtual {p1}, Lcom/twitter/model/dms/c;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 570
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 472
    :pswitch_1
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e;

    .line 473
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/i;->b(Lcom/twitter/model/dms/c;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 474
    invoke-static {p2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lapt$a;

    .line 475
    new-instance v2, Laqb$a;

    invoke-direct {v2}, Laqb$a;-><init>()V

    .line 476
    invoke-virtual {v2, v1}, Laqb$a;->a(Lapn$b;)Lapn$a;

    move-result-object v1

    check-cast v1, Laqb$a;

    .line 477
    invoke-virtual {v1, v0}, Laqb$a;->a(Lcom/twitter/model/dms/c;)Lapn$a;

    move-result-object v0

    check-cast v0, Laqb$a;

    .line 478
    invoke-virtual {v0, p0}, Laqb$a;->a(Lcom/twitter/app/dm/aj;)Laqb$a;

    move-result-object v0

    goto :goto_0

    .line 481
    :cond_0
    iget-wide v6, p0, Lcom/twitter/app/dm/i;->o:J

    invoke-virtual {p1, v6, v7}, Lcom/twitter/model/dms/c;->b(J)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 482
    invoke-static {p2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laqa$c;

    .line 483
    new-instance v2, Laqa$a;

    invoke-direct {v2}, Laqa$a;-><init>()V

    iget-boolean v5, p0, Lcom/twitter/app/dm/i;->G:Z

    .line 484
    invoke-virtual {v2, v5}, Laqa$a;->f(Z)Laqa$a;

    move-result-object v5

    iget v2, p0, Lcom/twitter/app/dm/i;->I:I

    .line 485
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v5, v2}, Laqa$a;->a(I)Laqa$a;

    move-result-object v2

    iget-object v5, p0, Lcom/twitter/app/dm/i;->E:Lcom/twitter/model/dms/n;

    .line 486
    invoke-virtual {v2, v5}, Laqa$a;->a(Lcom/twitter/model/dms/n;)Laqa$a;

    move-result-object v5

    iget-wide v6, p0, Lcom/twitter/app/dm/i;->H:J

    .line 487
    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->q()J

    move-result-wide v8

    cmp-long v2, v6, v8

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    invoke-virtual {v5, v2}, Laqa$a;->g(Z)Laqa$a;

    move-result-object v2

    .line 488
    invoke-virtual {v2, v1}, Laqa$a;->a(Lapn$b;)Lapn$a;

    move-result-object v1

    check-cast v1, Lapv$a;

    .line 497
    :goto_2
    iget-boolean v2, p0, Lcom/twitter/app/dm/i;->v:Z

    if-eqz v2, :cond_3

    invoke-static {v0}, Lcom/twitter/app/dm/i;->b(Lcom/twitter/model/dms/e;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 499
    :goto_3
    invoke-virtual {v1, v0}, Lapv$a;->a(Lcom/twitter/model/dms/c;)Lapn$a;

    move-result-object v1

    check-cast v1, Lapv$a;

    .line 500
    invoke-virtual {v1, p0}, Lapv$a;->a(Lcom/twitter/app/dm/v;)Lapv$a;

    move-result-object v1

    iget-boolean v2, p0, Lcom/twitter/app/dm/i;->p:Z

    .line 501
    invoke-virtual {v1, v2}, Lapv$a;->b(Z)Lapv$a;

    move-result-object v1

    iget-boolean v2, p0, Lcom/twitter/app/dm/i;->m:Z

    .line 502
    invoke-virtual {v1, v2}, Lapv$a;->c(Z)Lapv$a;

    move-result-object v1

    .line 503
    invoke-virtual {v1, v3}, Lapv$a;->d(Z)Lapv$a;

    move-result-object v1

    iget-boolean v2, p0, Lcom/twitter/app/dm/i;->n:Z

    .line 504
    invoke-virtual {v1, v2}, Lapv$a;->e(Z)Lapv$a;

    move-result-object v1

    .line 505
    invoke-virtual {v1, p0}, Lapv$a;->a(Lcne;)Lapv$a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/dm/i;->g:Lcom/twitter/app/dm/f;

    .line 506
    invoke-virtual {v1, v2}, Lapv$a;->a(Lcom/twitter/app/dm/f;)Lapv$a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/dm/i;->i:Lcom/twitter/android/bn;

    .line 507
    invoke-virtual {v1, v2}, Lapv$a;->a(Lcom/twitter/android/bn;)Lapv$a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/dm/i;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 508
    invoke-virtual {v1, v2}, Lapv$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lapv$a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/dm/i;->e:Lcom/twitter/library/network/t;

    .line 509
    invoke-virtual {v1, v2}, Lapv$a;->a(Lcom/twitter/library/network/t;)Lapv$a;

    move-result-object v1

    sget-object v2, Lcom/twitter/app/dm/i;->a:Ljava/text/SimpleDateFormat;

    .line 510
    invoke-virtual {v1, v2}, Lapv$a;->a(Ljava/text/SimpleDateFormat;)Lapv$a;

    move-result-object v1

    sget-object v2, Lcom/twitter/app/dm/i;->b:Ljava/text/SimpleDateFormat;

    .line 511
    invoke-virtual {v1, v2}, Lapv$a;->b(Ljava/text/SimpleDateFormat;)Lapv$a;

    move-result-object v1

    sget-object v2, Lcom/twitter/app/dm/i;->c:Ljava/text/SimpleDateFormat;

    .line 512
    invoke-virtual {v1, v2}, Lapv$a;->c(Ljava/text/SimpleDateFormat;)Lapv$a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/dm/i;->Q:Lcom/twitter/app/dm/p;

    .line 513
    invoke-virtual {v1, v2}, Lapv$a;->a(Lcom/twitter/app/dm/p;)Lapv$a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/dm/i;->R:Lcom/twitter/app/dm/o;

    .line 514
    invoke-virtual {v1, v2}, Lapv$a;->a(Lcom/twitter/app/dm/o;)Lapv$a;

    move-result-object v1

    .line 515
    invoke-direct {p0, v0}, Lcom/twitter/app/dm/i;->a(Lcom/twitter/model/dms/e;)Lcdu;

    move-result-object v0

    invoke-virtual {v1, v0}, Lapv$a;->a(Lcdu;)Lapv$a;

    move-result-object v0

    goto/16 :goto_0

    :cond_1
    move v2, v4

    .line 487
    goto :goto_1

    .line 490
    :cond_2
    invoke-static {p2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lapz$b;

    .line 491
    new-instance v2, Lapz$a;

    invoke-direct {v2}, Lapz$a;-><init>()V

    iget-object v5, p0, Lcom/twitter/app/dm/i;->A:Lcom/twitter/app/dm/ag;

    .line 492
    invoke-virtual {v2, v5}, Lapz$a;->a(Lcom/twitter/app/dm/ag;)Lapz$a;

    move-result-object v2

    iget-object v5, p0, Lcom/twitter/app/dm/i;->N:Lapl;

    .line 493
    invoke-virtual {v2, v5}, Lapz$a;->a(Lapl;)Lapz$a;

    move-result-object v2

    .line 494
    invoke-virtual {v2, v1}, Lapz$a;->a(Lapn$b;)Lapn$a;

    move-result-object v1

    check-cast v1, Lapv$a;

    goto/16 :goto_2

    :cond_3
    move v3, v4

    .line 497
    goto/16 :goto_3

    .line 519
    :pswitch_2
    invoke-static {p2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapr$b;

    .line 520
    new-instance v1, Lapr$a;

    invoke-direct {v1}, Lapr$a;-><init>()V

    .line 521
    invoke-virtual {v1, v0}, Lapr$a;->a(Lapn$b;)Lapn$a;

    move-result-object v0

    check-cast v0, Lapr$a;

    .line 522
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/c;

    invoke-virtual {v0, v1}, Lapr$a;->a(Lcom/twitter/model/dms/c;)Lapn$a;

    move-result-object v0

    check-cast v0, Lapr$a;

    iget-object v1, p0, Lcom/twitter/app/dm/i;->s:Ljava/lang/String;

    .line 523
    invoke-virtual {v0, v1}, Lapr$a;->a(Ljava/lang/String;)Lapr$a;

    move-result-object v0

    goto/16 :goto_0

    .line 526
    :pswitch_3
    invoke-static {p2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapu$b;

    .line 527
    new-instance v1, Lapu$a;

    invoke-direct {v1}, Lapu$a;-><init>()V

    .line 528
    invoke-virtual {v1, v0}, Lapu$a;->a(Lapn$b;)Lapn$a;

    move-result-object v0

    check-cast v0, Lapu$a;

    .line 529
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/c;

    invoke-virtual {v0, v1}, Lapu$a;->a(Lcom/twitter/model/dms/c;)Lapn$a;

    move-result-object v0

    check-cast v0, Lapu$a;

    iget-object v1, p0, Lcom/twitter/app/dm/i;->P:Lcom/twitter/app/dm/u;

    .line 530
    invoke-virtual {v0, v1}, Lapu$a;->a(Lcom/twitter/app/dm/u;)Lapu$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/app/dm/i;->l:Z

    .line 531
    invoke-virtual {v0, v1}, Lapu$a;->b(Z)Lapu$a;

    move-result-object v0

    goto/16 :goto_0

    .line 534
    :pswitch_4
    invoke-static {p2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapt$a;

    .line 535
    new-instance v1, Lapw$a;

    invoke-direct {v1}, Lapw$a;-><init>()V

    .line 536
    invoke-virtual {v1, v0}, Lapw$a;->a(Lapn$b;)Lapn$a;

    move-result-object v0

    check-cast v0, Lapw$a;

    .line 537
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/c;

    invoke-virtual {v0, v1}, Lapw$a;->a(Lcom/twitter/model/dms/c;)Lapn$a;

    move-result-object v0

    goto/16 :goto_0

    .line 540
    :pswitch_5
    invoke-static {p2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapt$a;

    .line 541
    new-instance v1, Lapx$a;

    invoke-direct {v1}, Lapx$a;-><init>()V

    .line 542
    invoke-virtual {v1, v0}, Lapx$a;->a(Lapn$b;)Lapn$a;

    move-result-object v0

    check-cast v0, Lapx$a;

    .line 543
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/c;

    invoke-virtual {v0, v1}, Lapx$a;->a(Lcom/twitter/model/dms/c;)Lapn$a;

    move-result-object v0

    goto/16 :goto_0

    .line 546
    :pswitch_6
    invoke-static {p2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapt$a;

    .line 547
    new-instance v1, Lapq$a;

    invoke-direct {v1}, Lapq$a;-><init>()V

    .line 548
    invoke-virtual {v1, v0}, Lapq$a;->a(Lapn$b;)Lapn$a;

    move-result-object v0

    check-cast v0, Lapq$a;

    .line 549
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/c;

    invoke-virtual {v0, v1}, Lapq$a;->a(Lcom/twitter/model/dms/c;)Lapn$a;

    move-result-object v0

    goto/16 :goto_0

    .line 552
    :pswitch_7
    invoke-static {p2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapt$a;

    .line 553
    new-instance v1, Lapy$a;

    invoke-direct {v1}, Lapy$a;-><init>()V

    .line 554
    invoke-virtual {v1, v0}, Lapy$a;->a(Lapn$b;)Lapn$a;

    move-result-object v0

    check-cast v0, Lapy$a;

    .line 555
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/c;

    invoke-virtual {v0, v1}, Lapy$a;->a(Lcom/twitter/model/dms/c;)Lapn$a;

    move-result-object v0

    goto/16 :goto_0

    .line 558
    :pswitch_8
    invoke-static {p2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapt$a;

    .line 559
    new-instance v1, Lapp$a;

    invoke-direct {v1}, Lapp$a;-><init>()V

    .line 560
    invoke-virtual {v1, v0}, Lapp$a;->a(Lapn$b;)Lapn$a;

    move-result-object v0

    check-cast v0, Lapp$a;

    .line 561
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/c;

    invoke-virtual {v0, v1}, Lapp$a;->a(Lcom/twitter/model/dms/c;)Lapn$a;

    move-result-object v0

    goto/16 :goto_0

    .line 564
    :pswitch_9
    invoke-static {p2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapt$a;

    .line 565
    new-instance v1, Lapo$a;

    invoke-direct {v1}, Lapo$a;-><init>()V

    .line 566
    invoke-virtual {v1, v0}, Lapo$a;->a(Lapn$b;)Lapn$a;

    move-result-object v0

    check-cast v0, Lapo$a;

    .line 567
    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/c;

    invoke-virtual {v0, v1}, Lapo$a;->a(Lcom/twitter/model/dms/c;)Lapn$a;

    move-result-object v0

    goto/16 :goto_0

    .line 468
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_7
        :pswitch_1
        :pswitch_2
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private a(Lcom/twitter/model/dms/e;)Lcdu;
    .locals 8

    .prologue
    .line 577
    invoke-virtual {p1}, Lcom/twitter/model/dms/e;->F()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 578
    invoke-virtual {p1}, Lcom/twitter/model/dms/e;->u()Lcbx;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcch;

    .line 579
    iget-object v0, p0, Lcom/twitter/app/dm/i;->O:Ljava/util/Map;

    iget-wide v4, v2, Lcch;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 581
    iget-object v0, p0, Lcom/twitter/app/dm/i;->O:Ljava/util/Map;

    iget-wide v2, v2, Lcch;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdu;

    .line 588
    :goto_0
    return-object v0

    .line 584
    :cond_0
    new-instance v6, Lbrm;

    invoke-virtual {p0}, Lcom/twitter/app/dm/i;->j()Landroid/content/Context;

    move-result-object v7

    new-instance v0, Lbrn;

    const/4 v1, 0x1

    iget-wide v2, v2, Lcch;->c:J

    iget-wide v4, p0, Lcom/twitter/app/dm/i;->o:J

    invoke-direct/range {v0 .. v5}, Lbrn;-><init>(IJJ)V

    invoke-direct {v6, v7, v0}, Lbrm;-><init>(Landroid/content/Context;Lbrn;)V

    .line 585
    invoke-virtual {v6, p0}, Lbrm;->a(Laow;)V

    .line 588
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(Lcbi;)Ljava/util/Map;
    .locals 6
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/model/dms/m;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/dms/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 448
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v2

    .line 449
    if-eqz p0, :cond_2

    .line 450
    invoke-virtual {p0}, Lcbi;->i()Ljava/util/ListIterator;

    move-result-object v3

    .line 451
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 452
    invoke-interface {v3}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/m;

    .line 453
    invoke-interface {v3}, Ljava/util/ListIterator;->nextIndex()I

    move-result v1

    .line 454
    invoke-virtual {p0}, Lcbi;->be_()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 455
    invoke-virtual {p0, v1}, Lcbi;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/dms/m;

    .line 456
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/twitter/model/dms/m;->c()Lcom/twitter/model/dms/c;

    move-result-object v1

    .line 457
    :goto_1
    invoke-virtual {v0}, Lcom/twitter/model/dms/m;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 456
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 462
    :cond_2
    return-object v2
.end method

.method private a(Landroid/view/View;Z)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const v3, 0x7f13032f

    .line 612
    iget-object v0, p0, Lcom/twitter/app/dm/i;->A:Lcom/twitter/app/dm/ag;

    if-nez v0, :cond_1

    .line 656
    :cond_0
    :goto_0
    return-void

    .line 618
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/dm/i;->A:Lcom/twitter/app/dm/ag;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ag;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 619
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 622
    :cond_2
    const/4 v0, 0x0

    .line 625
    if-eqz p2, :cond_5

    iget-object v1, p0, Lcom/twitter/app/dm/i;->A:Lcom/twitter/app/dm/ag;

    invoke-virtual {v1}, Lcom/twitter/app/dm/ag;->e()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 627
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 628
    if-nez v1, :cond_5

    .line 631
    const v1, 0x7f0400cf

    move-object v0, p1

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v1, v0, v2}, Lcom/twitter/app/dm/i;->a(ILandroid/view/ViewGroup;Z)Landroid/view/ViewGroup;

    .line 635
    :goto_1
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 636
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/i;->B:Lyj;

    if-eqz v0, :cond_0

    .line 637
    if-eqz p2, :cond_4

    .line 638
    if-eqz v2, :cond_3

    .line 640
    const v0, 0x7f130323

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 641
    iget-object v3, p0, Lcom/twitter/app/dm/i;->d:Landroid/content/Context;

    const v4, 0x7f020126

    .line 642
    invoke-static {v3, v4}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 643
    iget-object v4, p0, Lcom/twitter/app/dm/i;->d:Landroid/content/Context;

    const v5, 0x7f110087

    invoke-static {v4, v5}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v4

    invoke-static {v3, v4}, Lcnd;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    .line 644
    invoke-virtual {v0, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 647
    iget v3, p0, Lcom/twitter/app/dm/i;->D:I

    iget v4, p0, Lcom/twitter/app/dm/i;->C:I

    iget v5, p0, Lcom/twitter/app/dm/i;->C:I

    iget v6, p0, Lcom/twitter/app/dm/i;->C:I

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/view/View;->setPadding(IIII)V

    .line 650
    :cond_3
    iget-object v0, p0, Lcom/twitter/app/dm/i;->A:Lcom/twitter/app/dm/ag;

    .line 651
    invoke-virtual {p0}, Lcom/twitter/app/dm/i;->j()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/app/dm/i;->B:Lyj;

    move-object v5, p0

    .line 650
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/app/dm/ag;->a(Landroid/view/View;ZLandroid/content/Context;Lyj;Lcne;)V

    goto :goto_0

    .line 653
    :cond_4
    check-cast p1, Landroid/view/ViewGroup;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0

    :cond_5
    move v2, v0

    goto :goto_1
.end method

.method private b(Lcom/twitter/model/dms/c;)Z
    .locals 4

    .prologue
    .line 768
    invoke-virtual {p1}, Lcom/twitter/model/dms/c;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/i;->j:Ljava/util/Set;

    iget-wide v2, p1, Lcom/twitter/model/dms/c;->e:J

    .line 769
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 768
    :goto_0
    return v0

    .line 769
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/twitter/model/dms/e;)Z
    .locals 4

    .prologue
    .line 901
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 902
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->u()Lcbx;

    move-result-object v0

    check-cast v0, Lcbz;

    .line 903
    :goto_0
    if-nez v0, :cond_1

    .line 904
    const/4 v0, 0x0

    .line 907
    :goto_1
    return v0

    .line 902
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 907
    :cond_1
    invoke-static {}, Lcom/twitter/library/card/ae;->b()Lcom/twitter/library/card/ae;

    move-result-object v1

    invoke-virtual {v0}, Lcbz;->k()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->e:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 908
    invoke-virtual {v0}, Lcbz;->j()Lcax;

    move-result-object v0

    invoke-virtual {v0}, Lcax;->K()Lcar;

    move-result-object v0

    .line 907
    invoke-virtual {v1, v2, v3, v0}, Lcom/twitter/library/card/ae;->a(Ljava/lang/String;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcar;)Z

    move-result v0

    goto :goto_1
.end method

.method static c(Ljava/lang/String;)J
    .locals 5
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 726
    const-wide v0, 0x80000001L

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v2, v0

    .line 727
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "getItemIdForSentMessageRequestId("

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, ") returned a non-positive id: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/f;->a(ZLjava/lang/String;)Z

    .line 729
    return-wide v2

    .line 727
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(Lcom/twitter/model/dms/c;)I
    .locals 2

    .prologue
    const/4 v0, 0x2

    .line 738
    invoke-virtual {p1}, Lcom/twitter/model/dms/c;->o()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 754
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/i;->b(Lcom/twitter/model/dms/c;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 760
    :goto_0
    :pswitch_1
    return v0

    .line 740
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 751
    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 757
    :cond_0
    iget-wide v0, p0, Lcom/twitter/app/dm/i;->o:J

    invoke-virtual {p1, v0, v1}, Lcom/twitter/model/dms/c;->b(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 758
    const/4 v0, 0x1

    goto :goto_0

    .line 760
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 738
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Lcom/twitter/model/dms/m;)I
    .locals 1

    .prologue
    .line 734
    invoke-virtual {p1}, Lcom/twitter/model/dms/m;->c()Lcom/twitter/model/dms/c;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/i;->a(Lcom/twitter/model/dms/c;)I

    move-result v0

    return v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 109
    check-cast p1, Lcom/twitter/model/dms/m;

    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/i;->a(Lcom/twitter/model/dms/m;)I

    move-result v0

    return v0
.end method

.method protected a(Landroid/content/Context;Lcom/twitter/model/dms/m;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 357
    invoke-virtual {p2}, Lcom/twitter/model/dms/m;->c()Lcom/twitter/model/dms/c;

    move-result-object v0

    .line 358
    const v1, 0x7f0400c8

    const/4 v2, 0x0

    invoke-direct {p0, v1, p3, v2}, Lcom/twitter/app/dm/i;->a(ILandroid/view/ViewGroup;Z)Landroid/view/ViewGroup;

    move-result-object v2

    .line 361
    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/i;->a(Lcom/twitter/model/dms/c;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 395
    const/4 v0, 0x0

    .line 400
    :goto_0
    return-object v0

    .line 363
    :pswitch_0
    new-instance v0, Lapz$b;

    const v1, 0x7f0400cc

    .line 364
    invoke-direct {p0, v1, v2, v3}, Lcom/twitter/app/dm/i;->a(ILandroid/view/ViewGroup;Z)Landroid/view/ViewGroup;

    move-result-object v1

    iget-boolean v3, p0, Lcom/twitter/app/dm/i;->u:Z

    invoke-direct {v0, v1, v3}, Lapz$b;-><init>(Landroid/view/View;Z)V

    .line 399
    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    move-object v0, v2

    .line 400
    goto :goto_0

    .line 368
    :pswitch_1
    const v0, 0x7f0400cd

    invoke-direct {p0, v0, v2, v3}, Lcom/twitter/app/dm/i;->a(ILandroid/view/ViewGroup;Z)Landroid/view/ViewGroup;

    move-result-object v3

    .line 370
    iget-boolean v0, p0, Lcom/twitter/app/dm/i;->G:Z

    if-eqz v0, :cond_0

    .line 371
    new-instance v0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;

    invoke-direct {v0, p1, p0, p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;-><init>(Landroid/content/Context;Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$a;Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$b;)V

    move-object v1, v0

    .line 375
    :goto_2
    const v0, 0x7f13030d

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 376
    new-instance v0, Laqa$c;

    invoke-direct {v0, v3, v1}, Laqa$c;-><init>(Landroid/view/View;Lcom/twitter/app/dm/widget/SentMessageBylineView;)V

    goto :goto_1

    .line 373
    :cond_0
    new-instance v0, Lcom/twitter/app/dm/widget/SentMessageBylineView;

    invoke-direct {v0, p1, p0}, Lcom/twitter/app/dm/widget/SentMessageBylineView;-><init>(Landroid/content/Context;Lcom/twitter/app/dm/widget/SentMessageBylineView$a;)V

    move-object v1, v0

    goto :goto_2

    .line 380
    :pswitch_2
    new-instance v0, Lapt$a;

    const v1, 0x7f0400b2

    .line 381
    invoke-direct {p0, v1, v2, v3}, Lcom/twitter/app/dm/i;->a(ILandroid/view/ViewGroup;Z)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-direct {v0, v1}, Lapt$a;-><init>(Landroid/view/View;)V

    goto :goto_1

    .line 385
    :pswitch_3
    new-instance v0, Lapu$b;

    const v1, 0x7f0400bc

    .line 386
    invoke-direct {p0, v1, v2, v3}, Lcom/twitter/app/dm/i;->a(ILandroid/view/ViewGroup;Z)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-direct {v0, v1}, Lapu$b;-><init>(Landroid/view/View;)V

    goto :goto_1

    .line 390
    :pswitch_4
    new-instance v0, Lapr$b;

    const v1, 0x7f0400b8

    .line 391
    invoke-direct {p0, v1, v2, v3}, Lcom/twitter/app/dm/i;->a(ILandroid/view/ViewGroup;Z)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-direct {v0, v1}, Lapr$b;-><init>(Landroid/view/View;)V

    goto :goto_1

    .line 361
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 109
    check-cast p2, Lcom/twitter/model/dms/m;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/app/dm/i;->a(Landroid/content/Context;Lcom/twitter/model/dms/m;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method a()V
    .locals 1

    .prologue
    .line 271
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/dm/i;->M:Z

    .line 272
    return-void
.end method

.method public a(I)V
    .locals 4

    .prologue
    .line 297
    iput p1, p0, Lcom/twitter/app/dm/i;->I:I

    .line 298
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/i;->o:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:thread::read_receipts:expand"

    aput-object v3, v1, v2

    .line 299
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 298
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 300
    return-void
.end method

.method public a(J)V
    .locals 5

    .prologue
    .line 872
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/i;->o:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:thread:rtf_message::open"

    aput-object v3, v1, v2

    .line 873
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 872
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 874
    iget-object v0, p0, Lcom/twitter/app/dm/i;->j:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 875
    invoke-virtual {p0}, Lcom/twitter/app/dm/i;->notifyDataSetChanged()V

    .line 876
    return-void
.end method

.method public a(JZ)V
    .locals 2

    .prologue
    .line 276
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/app/dm/i;->K:Ljava/lang/ref/WeakReference;

    .line 277
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/app/dm/i;->H:J

    .line 278
    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/app/dm/i;->I:I

    .line 279
    return-void
.end method

.method public a(JZLcom/twitter/app/dm/widget/SentMessageBylineView;)V
    .locals 5

    .prologue
    .line 285
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/app/dm/i;->K:Ljava/lang/ref/WeakReference;

    .line 286
    iput-wide p1, p0, Lcom/twitter/app/dm/i;->H:J

    .line 288
    iget-object v0, p0, Lcom/twitter/app/dm/i;->i:Lcom/twitter/android/bn;

    invoke-interface {v0, p4}, Lcom/twitter/android/bn;->a(Landroid/view/View;)V

    .line 290
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/i;->o:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:thread::read_receipts:impression"

    aput-object v3, v1, v2

    .line 291
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 290
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 292
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 889
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/dm/i;->G:Z

    if-eqz v0, :cond_0

    .line 890
    const-string/jumbo v0, "state_state_shown_message_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/app/dm/i;->H:J

    .line 891
    iget-object v1, p0, Lcom/twitter/app/dm/i;->J:Ljava/util/Set;

    const-string/jumbo v0, "state_draft_message_request_ids"

    .line 892
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 894
    iget-boolean v0, p0, Lcom/twitter/app/dm/i;->F:Z

    if-eqz v0, :cond_0

    .line 895
    const-string/jumbo v0, "state_revealed_seen_by_pages"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/app/dm/i;->I:I

    .line 898
    :cond_0
    return-void
.end method

.method protected a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/model/dms/m;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 412
    invoke-virtual {p3}, Lcom/twitter/model/dms/m;->c()Lcom/twitter/model/dms/c;

    move-result-object v4

    .line 413
    iget-object v0, p0, Lcom/twitter/app/dm/i;->S:Ljava/util/Map;

    invoke-virtual {p3}, Lcom/twitter/model/dms/m;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/c;

    .line 414
    if-nez v0, :cond_3

    move v2, v1

    .line 416
    :goto_0
    invoke-direct {p0, v4}, Lcom/twitter/app/dm/i;->b(Lcom/twitter/model/dms/c;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/twitter/app/dm/i;->k:Ljava/util/Set;

    iget-wide v6, v4, Lcom/twitter/model/dms/c;->e:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 417
    new-instance v5, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v6, p0, Lcom/twitter/app/dm/i;->o:J

    invoke-direct {v5, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v6, "messages:thread:rtf_message::impression"

    aput-object v6, v1, v3

    .line 418
    invoke-virtual {v5, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    check-cast v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v6, v4, Lcom/twitter/model/dms/c;->e:J

    iget-wide v8, v4, Lcom/twitter/model/dms/c;->g:J

    .line 419
    invoke-virtual {v1, v6, v7, v8, v9}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(JJ)Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-result-object v1

    .line 417
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 420
    iget-object v1, p0, Lcom/twitter/app/dm/i;->k:Ljava/util/Set;

    iget-wide v6, v4, Lcom/twitter/model/dms/c;->e:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 424
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lapn$b;

    .line 423
    invoke-direct {p0, v4, v1}, Lcom/twitter/app/dm/i;->a(Lcom/twitter/model/dms/c;Lapn$b;)Lapn$a;

    move-result-object v1

    .line 425
    if-eqz v1, :cond_1

    .line 427
    invoke-virtual {p0}, Lcom/twitter/app/dm/i;->j()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1, v3}, Lapn$a;->a(Landroid/content/Context;)Lapn$a;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/app/dm/i;->h:Lcom/twitter/app/dm/m;

    .line 428
    invoke-virtual {v1, v3}, Lapn$a;->a(Lcom/twitter/app/dm/m;)Lapn$a;

    move-result-object v1

    .line 429
    invoke-virtual {v1, p3}, Lapn$a;->a(Lcom/twitter/model/dms/m;)Lapn$a;

    move-result-object v1

    .line 430
    invoke-virtual {v1, v2}, Lapn$a;->a(Z)Lapn$a;

    move-result-object v1

    .line 431
    invoke-virtual {v1, v0}, Lapn$a;->b(Lcom/twitter/model/dms/c;)Lapn$a;

    move-result-object v0

    .line 432
    invoke-virtual {v0}, Lapn$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapn;

    .line 433
    invoke-virtual {v0}, Lapn;->a()V

    .line 436
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/app/dm/i;->z:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/app/dm/i;->A:Lcom/twitter/app/dm/ag;

    if-eqz v0, :cond_2

    .line 437
    invoke-direct {p0, p1, v2}, Lcom/twitter/app/dm/i;->a(Landroid/view/View;Z)V

    .line 439
    :cond_2
    return-void

    :cond_3
    move v2, v3

    .line 414
    goto :goto_0
.end method

.method protected bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 109
    check-cast p3, Lcom/twitter/model/dms/m;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/app/dm/i;->a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/model/dms/m;)V

    return-void
.end method

.method public a(Lcax;)V
    .locals 0

    .prologue
    .line 786
    return-void
.end method

.method public a(Lcom/twitter/app/dm/ag;)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Lcom/twitter/app/dm/i;->A:Lcom/twitter/app/dm/ag;

    .line 268
    return-void
.end method

.method public a(Lcom/twitter/app/dm/widget/SentMessageBylineView;)V
    .locals 4

    .prologue
    .line 310
    iget-object v0, p0, Lcom/twitter/app/dm/i;->K:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/widget/SentMessageBylineView;

    .line 311
    if-eqz v0, :cond_0

    if-eq v0, p1, :cond_0

    .line 312
    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 313
    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 314
    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->g()V

    .line 319
    :cond_0
    :goto_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/i;->o:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:thread::message:click"

    aput-object v3, v1, v2

    .line 320
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 319
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 321
    return-void

    .line 316
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->getMessageId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->a(J)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/MediaEntity;)V
    .locals 0

    .prologue
    .line 790
    return-void
.end method

.method public a(Lcom/twitter/model/core/ad;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 686
    iget-object v1, p0, Lcom/twitter/app/dm/i;->d:Landroid/content/Context;

    iget-wide v4, p0, Lcom/twitter/app/dm/i;->o:J

    const-string/jumbo v6, "messages:thread:::open_link"

    iget-object v8, p0, Lcom/twitter/app/dm/i;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-object v3, p1

    move-object v7, v2

    move-object v9, v2

    invoke-static/range {v1 .. v9}, Lcom/twitter/android/client/OpenUriHelper;->a(Landroid/content/Context;Lcom/twitter/library/client/BrowserDataSource;Lcom/twitter/model/core/ad;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)V

    .line 688
    return-void
.end method

.method public a(Lcom/twitter/model/core/b;)V
    .locals 2

    .prologue
    .line 670
    iget-object v0, p0, Lcom/twitter/app/dm/i;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/app/dm/i;->d:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/twitter/android/t;->a(Landroid/content/Context;Lcom/twitter/model/core/b;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 671
    return-void
.end method

.method public a(Lcom/twitter/model/core/h;)V
    .locals 2

    .prologue
    .line 665
    iget-object v0, p0, Lcom/twitter/app/dm/i;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/app/dm/i;->d:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/twitter/android/t;->a(Landroid/content/Context;Lcom/twitter/model/core/h;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 666
    return-void
.end method

.method public a(Lcom/twitter/model/core/q;)V
    .locals 4

    .prologue
    .line 675
    iget-object v0, p0, Lcom/twitter/app/dm/i;->d:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/app/dm/i;->d:Landroid/content/Context;

    const-class v3, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "screen_name"

    iget-object v3, p1, Lcom/twitter/model/core/q;->j:Ljava/lang/String;

    .line 676
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 675
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 677
    return-void
.end method

.method public a(Lcom/twitter/model/dms/n;)V
    .locals 1

    .prologue
    .line 261
    iget-boolean v0, p0, Lcom/twitter/app/dm/i;->F:Z

    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 262
    iput-object p1, p0, Lcom/twitter/app/dm/i;->E:Lcom/twitter/model/dms/n;

    .line 263
    invoke-virtual {p0}, Lcom/twitter/app/dm/i;->notifyDataSetChanged()V

    .line 264
    return-void
.end method

.method public a(Lcom/twitter/model/geo/TwitterPlace;)V
    .locals 0

    .prologue
    .line 681
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/twitter/app/dm/i;->s:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 255
    iput-object p1, p0, Lcom/twitter/app/dm/i;->s:Ljava/lang/String;

    .line 256
    invoke-virtual {p0}, Lcom/twitter/app/dm/i;->notifyDataSetChanged()V

    .line 258
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/twitter/model/drafts/DraftAttachment;)V
    .locals 1

    .prologue
    .line 849
    iget-object v0, p0, Lcom/twitter/app/dm/i;->L:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 850
    iget-object v0, p0, Lcom/twitter/app/dm/i;->L:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 852
    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcdu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 593
    if-nez p1, :cond_1

    .line 608
    :cond_0
    :goto_0
    return-void

    .line 597
    :cond_1
    const/4 v0, 0x0

    .line 598
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdu;

    .line 599
    iget-object v3, p0, Lcom/twitter/app/dm/i;->O:Ljava/util/Map;

    iget-wide v4, v0, Lcdu;->h:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 600
    iget-object v1, p0, Lcom/twitter/app/dm/i;->O:Ljava/util/Map;

    iget-wide v4, v0, Lcdu;->h:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 601
    const/4 v0, 0x1

    :goto_2
    move v1, v0

    .line 603
    goto :goto_1

    .line 605
    :cond_2
    if-eqz v1, :cond_0

    .line 606
    invoke-virtual {p0}, Lcom/twitter/app/dm/i;->notifyDataSetChanged()V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public a(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcaq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 781
    iput-object p1, p0, Lcom/twitter/app/dm/i;->y:Ljava/util/Map;

    .line 782
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 240
    iget-boolean v0, p0, Lcom/twitter/app/dm/i;->p:Z

    if-eq v0, p1, :cond_0

    .line 241
    iput-boolean p1, p0, Lcom/twitter/app/dm/i;->p:Z

    .line 242
    invoke-virtual {p0}, Lcom/twitter/app/dm/i;->notifyDataSetChanged()V

    .line 244
    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/model/dms/m;)Landroid/view/View;
    .locals 6

    .prologue
    .line 809
    iget-boolean v0, p0, Lcom/twitter/app/dm/i;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/i;->y:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 810
    iget-object v0, p0, Lcom/twitter/app/dm/i;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/app/dm/i;->y:Ljava/util/Map;

    iget-object v3, p0, Lcom/twitter/app/dm/i;->w:Lape;

    iget-boolean v4, p0, Lcom/twitter/app/dm/i;->q:Z

    iget-boolean v5, p0, Lcom/twitter/app/dm/i;->p:Z

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lapf;->a(Landroid/content/Context;Lcom/twitter/model/dms/m;Ljava/util/Map;Lape;ZZ)Landroid/view/View;

    move-result-object v0

    .line 814
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 325
    invoke-virtual {p0}, Lcom/twitter/app/dm/i;->notifyDataSetChanged()V

    .line 326
    return-void
.end method

.method public b(J)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 660
    iget-object v1, p0, Lcom/twitter/app/dm/i;->d:Landroid/content/Context;

    move-wide v2, p1

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-static/range {v1 .. v7}, Lcom/twitter/android/ProfileActivity;->a(Landroid/content/Context;JLjava/lang/String;Lcgi;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/model/timeline/r;)V

    .line 661
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/twitter/app/dm/i;->J:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 305
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 247
    iget-boolean v0, p0, Lcom/twitter/app/dm/i;->q:Z

    if-eq v0, p1, :cond_0

    .line 248
    iput-boolean p1, p0, Lcom/twitter/app/dm/i;->q:Z

    .line 249
    invoke-virtual {p0}, Lcom/twitter/app/dm/i;->notifyDataSetChanged()V

    .line 251
    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/model/core/ad;)Z
    .locals 1

    .prologue
    .line 692
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic b_(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 109
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/i;->a(Ljava/util/List;)V

    return-void
.end method

.method public c(Lcom/twitter/model/dms/m;)Landroid/view/View;
    .locals 3

    .prologue
    .line 820
    iget-boolean v0, p0, Lcom/twitter/app/dm/i;->v:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/dm/i;->u:Z

    if-eqz v0, :cond_0

    .line 821
    iget-object v0, p0, Lcom/twitter/app/dm/i;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/app/dm/i;->x:Lcom/twitter/library/card/m;

    iget-object v2, p0, Lcom/twitter/app/dm/i;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-static {v0, p1, v1, v2}, Lapf;->a(Landroid/content/Context;Lcom/twitter/model/dms/m;Lcom/twitter/library/card/m;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Landroid/view/View;

    move-result-object v0

    .line 824
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 834
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/app/dm/i;->t:Ljava/lang/String;

    .line 835
    return-void
.end method

.method public d()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 880
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 881
    const-string/jumbo v1, "state_state_shown_message_id"

    iget-wide v2, p0, Lcom/twitter/app/dm/i;->H:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 882
    const-string/jumbo v1, "state_revealed_seen_by_pages"

    iget v2, p0, Lcom/twitter/app/dm/i;->I:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 883
    const-string/jumbo v1, "state_draft_message_request_ids"

    iget-object v2, p0, Lcom/twitter/app/dm/i;->J:Ljava/util/Set;

    .line 884
    invoke-static {v2}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/Set;)Ljava/io/Serializable;

    move-result-object v2

    .line 883
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 885
    return-object v0
.end method

.method public d(J)V
    .locals 0

    .prologue
    .line 794
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 797
    iput-object p1, p0, Lcom/twitter/app/dm/i;->t:Ljava/lang/String;

    .line 798
    return-void
.end method

.method public e(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 829
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/i;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 839
    iget-object v0, p0, Lcom/twitter/app/dm/i;->J:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public g(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 844
    iget-object v0, p0, Lcom/twitter/app/dm/i;->J:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 845
    return-void
.end method

.method public getItemId(I)J
    .locals 4

    .prologue
    .line 702
    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/i;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/m;

    .line 703
    if-nez v0, :cond_0

    .line 704
    const-wide/16 v0, -0x1

    .line 716
    :goto_0
    return-wide v0

    .line 707
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/model/dms/m;->c()Lcom/twitter/model/dms/c;

    move-result-object v1

    .line 708
    invoke-virtual {v1}, Lcom/twitter/model/dms/c;->n()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lcom/twitter/app/dm/i;->o:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/model/dms/c;->b(J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 709
    check-cast v1, Lcom/twitter/model/dms/e;

    invoke-virtual {v1}, Lcom/twitter/model/dms/e;->v()Ljava/lang/String;

    move-result-object v1

    .line 710
    if-eqz v1, :cond_1

    .line 712
    invoke-static {v1}, Lcom/twitter/app/dm/i;->c(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    .line 716
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/model/dms/m;->a()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 330
    const/4 v0, 0x5

    return v0
.end method

.method public h(Ljava/lang/String;)Lcom/twitter/model/drafts/DraftAttachment;
    .locals 1

    .prologue
    .line 857
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/i;->L:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 862
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/i;->L:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 863
    iget-object v0, p0, Lcom/twitter/app/dm/i;->L:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/DraftAttachment;

    .line 864
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/DraftAttachment;->b(Lcom/twitter/model/drafts/DraftAttachment;)Lrx/g;

    .line 865
    invoke-virtual {p0}, Lcom/twitter/app/dm/i;->notifyDataSetChanged()V

    .line 867
    :cond_0
    return-void
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 335
    iget-boolean v0, p0, Lcom/twitter/app/dm/i;->M:Z

    if-eqz v0, :cond_0

    .line 336
    invoke-virtual {p0}, Lcom/twitter/app/dm/i;->g()Lcbi;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/dm/i;->a(Lcbi;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/i;->S:Ljava/util/Map;

    .line 337
    invoke-super {p0}, Lcjr;->notifyDataSetChanged()V

    .line 339
    :cond_0
    return-void
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 343
    if-nez p1, :cond_1

    .line 351
    :cond_0
    :goto_0
    return-void

    .line 347
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 348
    instance-of v0, v0, Lapn$b;

    if-eqz v0, :cond_0

    .line 349
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapn$b;

    invoke-virtual {v0}, Lapn$b;->a()V

    goto :goto_0
.end method
