.class Lcom/twitter/app/dm/DMComposeFragment$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/dm/DMComposeFragment;->h()Landroid/text/TextWatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/dm/DMComposeFragment;


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/DMComposeFragment;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/twitter/app/dm/DMComposeFragment$1;->a:Lcom/twitter/app/dm/DMComposeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 178
    iget-object v0, p0, Lcom/twitter/app/dm/DMComposeFragment$1;->a:Lcom/twitter/app/dm/DMComposeFragment;

    invoke-static {v0}, Lcom/twitter/app/dm/DMComposeFragment;->a(Lcom/twitter/app/dm/DMComposeFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/app/dm/DMComposeFragment$1;->a:Lcom/twitter/app/dm/DMComposeFragment;

    invoke-virtual {v1}, Lcom/twitter/app/dm/DMComposeFragment;->v()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 179
    iget-object v0, p0, Lcom/twitter/app/dm/DMComposeFragment$1;->a:Lcom/twitter/app/dm/DMComposeFragment;

    invoke-static {v0}, Lcom/twitter/app/dm/DMComposeFragment;->b(Lcom/twitter/app/dm/DMComposeFragment;)V

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/DMComposeFragment$1;->a:Lcom/twitter/app/dm/DMComposeFragment;

    iget-object v0, v0, Lcom/twitter/app/dm/DMComposeFragment;->f:Lcom/twitter/app/dm/g;

    if-eqz v0, :cond_1

    .line 183
    iget-object v0, p0, Lcom/twitter/app/dm/DMComposeFragment$1;->a:Lcom/twitter/app/dm/DMComposeFragment;

    iget-object v0, v0, Lcom/twitter/app/dm/DMComposeFragment;->f:Lcom/twitter/app/dm/g;

    invoke-virtual {v0}, Lcom/twitter/app/dm/g;->c()V

    .line 185
    :cond_1
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lcom/twitter/app/dm/DMComposeFragment$1;->a:Lcom/twitter/app/dm/DMComposeFragment;

    iget-object v1, p0, Lcom/twitter/app/dm/DMComposeFragment$1;->a:Lcom/twitter/app/dm/DMComposeFragment;

    invoke-virtual {v1}, Lcom/twitter/app/dm/DMComposeFragment;->v()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/app/dm/DMComposeFragment;->a(Lcom/twitter/app/dm/DMComposeFragment;I)I

    .line 166
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/twitter/app/dm/DMComposeFragment$1;->a:Lcom/twitter/app/dm/DMComposeFragment;

    iget-object v0, v0, Lcom/twitter/app/dm/DMComposeFragment;->f:Lcom/twitter/app/dm/g;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/twitter/app/dm/DMComposeFragment$1;->a:Lcom/twitter/app/dm/DMComposeFragment;

    iget-object v0, v0, Lcom/twitter/app/dm/DMComposeFragment;->f:Lcom/twitter/app/dm/g;

    invoke-virtual {v0}, Lcom/twitter/app/dm/g;->b()V

    .line 173
    :cond_0
    return-void
.end method
