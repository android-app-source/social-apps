.class Lcom/twitter/app/dm/ShareViaDMComposeFragment$4;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/dm/ShareViaDMComposeFragment;->h()Landroid/text/TextWatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/text/TextWatcher;

.field final synthetic b:Lcom/twitter/app/dm/ShareViaDMComposeFragment;


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/ShareViaDMComposeFragment;Landroid/text/TextWatcher;)V
    .locals 0

    .prologue
    .line 462
    iput-object p1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment$4;->b:Lcom/twitter/app/dm/ShareViaDMComposeFragment;

    iput-object p2, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment$4;->a:Landroid/text/TextWatcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 475
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment$4;->a:Landroid/text/TextWatcher;

    invoke-interface {v0, p1}, Landroid/text/TextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    .line 476
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment$4;->b:Lcom/twitter/app/dm/ShareViaDMComposeFragment;

    invoke-static {v0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->i(Lcom/twitter/app/dm/ShareViaDMComposeFragment;)Lcom/twitter/app/dm/ac;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment$4;->b:Lcom/twitter/app/dm/ShareViaDMComposeFragment;

    invoke-virtual {v1}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->v()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/ac;->a(Ljava/util/Set;)V

    .line 477
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment$4;->b:Lcom/twitter/app/dm/ShareViaDMComposeFragment;

    invoke-static {v0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->d(Lcom/twitter/app/dm/ShareViaDMComposeFragment;)Lcom/twitter/app/dm/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment$4;->b:Lcom/twitter/app/dm/ShareViaDMComposeFragment;

    invoke-static {v1}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->c(Lcom/twitter/app/dm/ShareViaDMComposeFragment;)Lcom/twitter/app/dm/ab;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/aa;->b(Lcom/twitter/app/dm/ab;)V

    .line 478
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment$4;->a:Landroid/text/TextWatcher;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/text/TextWatcher;->beforeTextChanged(Ljava/lang/CharSequence;III)V

    .line 466
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 470
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment$4;->a:Landroid/text/TextWatcher;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/text/TextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 471
    return-void
.end method
