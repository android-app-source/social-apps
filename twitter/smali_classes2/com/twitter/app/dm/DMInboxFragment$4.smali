.class Lcom/twitter/app/dm/DMInboxFragment$4;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/dm/DMInboxFragment;->a(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/dm/DMInboxFragment;


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/DMInboxFragment;)V
    .locals 0

    .prologue
    .line 220
    iput-object p1, p0, Lcom/twitter/app/dm/DMInboxFragment$4;->a:Lcom/twitter/app/dm/DMInboxFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 224
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/app/dm/DMInboxFragment$4;->a:Lcom/twitter/app/dm/DMInboxFragment;

    invoke-static {v1}, Lcom/twitter/app/dm/DMInboxFragment;->d(Lcom/twitter/app/dm/DMInboxFragment;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:inbox:user_list:import:click"

    aput-object v3, v1, v2

    .line 225
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 224
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 226
    iget-object v0, p0, Lcom/twitter/app/dm/DMInboxFragment$4;->a:Lcom/twitter/app/dm/DMInboxFragment;

    invoke-static {v0}, Lcom/twitter/app/dm/DMInboxFragment;->c(Lcom/twitter/app/dm/DMInboxFragment;)V

    .line 227
    return-void
.end method
