.class Lcom/twitter/app/dm/DMConversationFragment$19;
.super Lcom/twitter/library/network/livepipeline/g;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/dm/DMConversationFragment;->R()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/network/livepipeline/g",
        "<",
        "Lcom/twitter/model/livepipeline/g;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/dm/DMConversationFragment;


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/DMConversationFragment;)V
    .locals 0

    .prologue
    .line 671
    iput-object p1, p0, Lcom/twitter/app/dm/DMConversationFragment$19;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-direct {p0}, Lcom/twitter/library/network/livepipeline/g;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/model/livepipeline/d;)V
    .locals 0

    .prologue
    .line 671
    check-cast p1, Lcom/twitter/model/livepipeline/g;

    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/DMConversationFragment$19;->a(Lcom/twitter/model/livepipeline/g;)V

    return-void
.end method

.method public a(Lcom/twitter/model/livepipeline/g;)V
    .locals 4

    .prologue
    .line 674
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$19;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v0}, Lcom/twitter/app/dm/DMConversationFragment;->d(Lcom/twitter/app/dm/DMConversationFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$19;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v0}, Lcom/twitter/app/dm/DMConversationFragment;->e(Lcom/twitter/app/dm/DMConversationFragment;)Lcom/twitter/app/dm/ag;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 675
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$19;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v0}, Lcom/twitter/app/dm/DMConversationFragment;->e(Lcom/twitter/app/dm/DMConversationFragment;)Lcom/twitter/app/dm/ag;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/model/livepipeline/g;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/app/dm/ag;->b(J)V

    .line 676
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$19;->a:Lcom/twitter/app/dm/DMConversationFragment;

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment$19;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v1}, Lcom/twitter/app/dm/DMConversationFragment;->e(Lcom/twitter/app/dm/DMConversationFragment;)Lcom/twitter/app/dm/ag;

    move-result-object v1

    iget-object v2, p1, Lcom/twitter/model/livepipeline/g;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/app/dm/ag;->a(J)Lrx/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/DMConversationFragment;->a(Lrx/j;)V

    .line 679
    :cond_0
    const-string/jumbo v0, "LivePipeline"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "User "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/twitter/model/livepipeline/g;->b:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " is typing..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    return-void
.end method
