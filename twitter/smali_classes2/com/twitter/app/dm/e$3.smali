.class final Lcom/twitter/app/dm/e$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/dm/e;->a(Landroid/view/View;IFLcom/twitter/util/ui/c;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:F

.field final synthetic c:I


# direct methods
.method constructor <init>(Landroid/view/View;FI)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/twitter/app/dm/e$3;->a:Landroid/view/View;

    iput p2, p0, Lcom/twitter/app/dm/e$3;->b:F

    iput p3, p0, Lcom/twitter/app/dm/e$3;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3

    .prologue
    .line 106
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 107
    iget-object v1, p0, Lcom/twitter/app/dm/e$3;->a:Landroid/view/View;

    iget v2, p0, Lcom/twitter/app/dm/e$3;->b:F

    sub-float/2addr v2, v0

    invoke-virtual {v1, v2}, Landroid/view/View;->setX(F)V

    .line 108
    iget-object v1, p0, Lcom/twitter/app/dm/e$3;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v2, p0, Lcom/twitter/app/dm/e$3;->c:I

    float-to-int v0, v0

    add-int/2addr v0, v2

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 109
    iget-object v0, p0, Lcom/twitter/app/dm/e$3;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 110
    return-void
.end method
