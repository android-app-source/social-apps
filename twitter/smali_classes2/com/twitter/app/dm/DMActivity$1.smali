.class Lcom/twitter/app/dm/DMActivity$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/dm/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/dm/DMActivity;->b(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/app/ProgressDialog;

.field final synthetic b:Lcom/twitter/app/dm/j;

.field final synthetic c:Lcom/twitter/app/dm/DMActivity;


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/DMActivity;Landroid/app/ProgressDialog;Lcom/twitter/app/dm/j;)V
    .locals 0

    .prologue
    .line 335
    iput-object p1, p0, Lcom/twitter/app/dm/DMActivity$1;->c:Lcom/twitter/app/dm/DMActivity;

    iput-object p2, p0, Lcom/twitter/app/dm/DMActivity$1;->a:Landroid/app/ProgressDialog;

    iput-object p3, p0, Lcom/twitter/app/dm/DMActivity$1;->b:Lcom/twitter/app/dm/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/TwitterUser;Z)V
    .locals 6

    .prologue
    .line 338
    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity$1;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity$1;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 342
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity$1;->c:Lcom/twitter/app/dm/DMActivity;

    invoke-virtual {v0}, Lcom/twitter/app/dm/DMActivity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity$1;->c:Lcom/twitter/app/dm/DMActivity;

    invoke-virtual {v0}, Lcom/twitter/app/dm/DMActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 352
    :cond_1
    :goto_0
    return-void

    .line 346
    :cond_2
    if-eqz p2, :cond_1

    .line 347
    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v4

    .line 348
    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity$1;->c:Lcom/twitter/app/dm/DMActivity;

    iget-object v1, p0, Lcom/twitter/app/dm/DMActivity$1;->c:Lcom/twitter/app/dm/DMActivity;

    invoke-static {v1}, Lcom/twitter/app/dm/DMActivity;->a(Lcom/twitter/app/dm/DMActivity;)J

    move-result-wide v2

    invoke-static {v2, v3, v4, v5}, Lcom/twitter/library/dm/e;->a(JJ)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [J

    const/4 v3, 0x0

    aput-wide v4, v2, v3

    iget-object v3, p0, Lcom/twitter/app/dm/DMActivity$1;->b:Lcom/twitter/app/dm/j;

    .line 349
    invoke-virtual {v3}, Lcom/twitter/app/dm/j;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/app/dm/DMActivity$1;->b:Lcom/twitter/app/dm/j;

    .line 350
    invoke-virtual {v4}, Lcom/twitter/app/dm/j;->k()Landroid/net/Uri;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/app/dm/DMActivity$1;->b:Lcom/twitter/app/dm/j;

    invoke-virtual {v5}, Lcom/twitter/app/dm/j;->z()Ljava/lang/String;

    move-result-object v5

    .line 348
    invoke-static/range {v0 .. v5}, Lcom/twitter/app/dm/DMActivity;->a(Lcom/twitter/app/dm/DMActivity;Ljava/lang/String;[JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    goto :goto_0
.end method
