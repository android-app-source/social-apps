.class Lcom/twitter/app/dm/DMConversationFragment$6;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lapj$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/dm/DMConversationFragment;->aU()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/dm/DMConversationFragment;


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/DMConversationFragment;)V
    .locals 0

    .prologue
    .line 1358
    iput-object p1, p0, Lcom/twitter/app/dm/DMConversationFragment$6;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcbi;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1361
    new-instance v0, Lcom/twitter/app/dm/DMConversationFragment$6$1;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/DMConversationFragment$6$1;-><init>(Lcom/twitter/app/dm/DMConversationFragment$6;)V

    invoke-static {p1, v0}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;Lcpv;)Ljava/util/List;

    move-result-object v0

    .line 1369
    new-instance v1, Lcom/twitter/app/dm/DMConversationFragment$6$2;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/DMConversationFragment$6$2;-><init>(Lcom/twitter/app/dm/DMConversationFragment$6;)V

    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;Lcpv;)Ljava/util/List;

    move-result-object v1

    .line 1378
    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment$6;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v2}, Lcom/twitter/app/dm/DMConversationFragment;->l(Lcom/twitter/app/dm/DMConversationFragment;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1379
    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment$6;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v2, v1}, Lcom/twitter/app/dm/DMConversationFragment;->a(Lcom/twitter/app/dm/DMConversationFragment;Ljava/util/List;)V

    .line 1385
    :goto_0
    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment$6;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v1, v0}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcom/twitter/app/dm/DMConversationFragment;Ljava/util/List;)V

    .line 1386
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$6;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-virtual {v0}, Lcom/twitter/app/dm/DMConversationFragment;->aH()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->h()V

    .line 1388
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$6;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v0, p1}, Lcom/twitter/app/dm/DMConversationFragment;->a(Lcom/twitter/app/dm/DMConversationFragment;Lcbi;)V

    .line 1392
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$6;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v0, v3}, Lcom/twitter/app/dm/DMConversationFragment;->b(Lcom/twitter/app/dm/DMConversationFragment;Z)Z

    .line 1393
    return-void

    .line 1381
    :cond_0
    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment$6;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v1, v3}, Lcom/twitter/app/dm/DMConversationFragment;->a(Lcom/twitter/app/dm/DMConversationFragment;Z)V

    .line 1382
    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment$6;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v1, v0}, Lcom/twitter/app/dm/DMConversationFragment;->b(Lcom/twitter/app/dm/DMConversationFragment;Ljava/util/List;)V

    goto :goto_0
.end method
