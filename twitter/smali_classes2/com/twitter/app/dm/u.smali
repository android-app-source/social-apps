.class public Lcom/twitter/app/dm/u;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/u$a;,
        Lcom/twitter/app/dm/u$b;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field

.field private final b:J

.field private c:Z


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-wide p1, p0, Lcom/twitter/app/dm/u;->b:J

    .line 32
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/u;->a:Ljava/util/Map;

    .line 33
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/u;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/twitter/app/dm/u;->a:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/app/dm/u;Z)Z
    .locals 0

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/twitter/app/dm/u;->c:Z

    return p1
.end method


# virtual methods
.method public a(Ljava/util/Collection;Lcom/twitter/app/dm/u$a;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/twitter/app/dm/u$a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/app/dm/u;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    .line 37
    if-eqz v0, :cond_1

    .line 38
    iget-object v0, p0, Lcom/twitter/app/dm/u;->a:Ljava/util/Map;

    invoke-interface {p2, v0}, Lcom/twitter/app/dm/u$a;->a(Ljava/util/Map;)V

    .line 43
    :cond_0
    :goto_0
    return-void

    .line 39
    :cond_1
    iget-boolean v0, p0, Lcom/twitter/app/dm/u;->c:Z

    if-nez v0, :cond_0

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/dm/u;->c:Z

    .line 41
    new-instance v0, Lcom/twitter/app/dm/u$b;

    iget-wide v4, p0, Lcom/twitter/app/dm/u;->b:J

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/app/dm/u$b;-><init>(Lcom/twitter/app/dm/u;Ljava/util/Collection;Lcom/twitter/app/dm/u$a;J)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/u$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
