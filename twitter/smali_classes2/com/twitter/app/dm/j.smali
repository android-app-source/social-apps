.class public Lcom/twitter/app/dm/j;
.super Lcom/twitter/app/dm/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/j$a;
    }
.end annotation


# direct methods
.method protected constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/b;-><init>(Landroid/os/Bundle;)V

    .line 32
    return-void
.end method

.method public static d(Landroid/os/Bundle;)Lcom/twitter/app/dm/j;
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/twitter/app/dm/j;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/j;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public A()Z
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/app/dm/j;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "is_from_notification"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public B()Z
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/twitter/app/dm/j;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "is_from_direct_share"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public C()Z
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/twitter/app/dm/j;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "is_group"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public D()Z
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/app/dm/j;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "should_show_verified_badge"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public f()[J
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/twitter/app/dm/j;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "participant_ids"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/app/dm/j;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "recipient_screen_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/twitter/app/dm/j;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "conversation_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public k()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/app/dm/j;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "media_uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/j;->c:Landroid/os/Bundle;

    const-string/jumbo v2, "android.intent.extra.STREAM"

    .line 57
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .line 56
    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    return-object v0
.end method

.method public x()Ljava/lang/String;
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/twitter/app/dm/j;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public y()Ljava/lang/String;
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/app/dm/j;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "subtitle"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public z()Ljava/lang/String;
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/app/dm/j;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "welcome_message_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 73
    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/j;->j(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 76
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
