.class public Lcom/twitter/app/dm/ag;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/dm/g;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/ag$b;,
        Lcom/twitter/app/dm/ag$a;
    }
.end annotation


# static fields
.field private static final a:J

.field private static final b:Ljava/lang/Integer;

.field private static final c:Ljava/lang/Integer;


# instance fields
.field private final d:Lcom/twitter/app/dm/ag$a;

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Laut;

.field private final g:Ljava/lang/String;

.field private final h:Lcqt;

.field private final i:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcom/twitter/app/dm/af;

.field private l:Lcom/twitter/app/dm/ai;

.field private m:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 60
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/twitter/app/dm/ag;->a:J

    .line 63
    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/twitter/app/dm/ag;->b:Ljava/lang/Integer;

    .line 65
    sget-object v0, Lcom/twitter/app/dm/ag;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/twitter/app/dm/ag;->c:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Laut;Ljava/lang/String;Lcom/twitter/app/dm/af;Lcom/twitter/app/dm/ag$a;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-object p1, p0, Lcom/twitter/app/dm/ag;->f:Laut;

    .line 86
    iput-object p2, p0, Lcom/twitter/app/dm/ag;->g:Ljava/lang/String;

    .line 90
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/app/dm/ag;->e:Ljava/util/Map;

    .line 91
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/app/dm/ag;->j:Ljava/util/Set;

    .line 92
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->a()Lcqt;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/ag;->h:Lcqt;

    .line 93
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    sget-object v1, Lcom/twitter/app/dm/ag;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/app/dm/ag;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 94
    iput-object p3, p0, Lcom/twitter/app/dm/ag;->k:Lcom/twitter/app/dm/af;

    .line 96
    if-eqz p4, :cond_0

    .line 97
    iput-object p4, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    .line 101
    :goto_0
    return-void

    .line 99
    :cond_0
    new-instance v0, Lcom/twitter/app/dm/ag$a;

    invoke-direct {v0}, Lcom/twitter/app/dm/ag$a;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    goto :goto_0
.end method

.method private a(JLandroid/view/ViewGroup;Lyj;ZLcne;)Landroid/view/View;
    .locals 3

    .prologue
    .line 349
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04041c

    const/4 v2, 0x0

    .line 350
    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    .line 352
    invoke-direct {p0, p1, p2, p4, v0}, Lcom/twitter/app/dm/ag;->a(JLyj;Lcom/twitter/media/ui/image/UserImageView;)V

    .line 354
    if-eqz p5, :cond_0

    .line 355
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p3, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 360
    :goto_0
    iget-object v1, p0, Lcom/twitter/app/dm/ag;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setTag(Ljava/lang/Object;)V

    .line 361
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setClickable(Z)V

    .line 362
    new-instance v1, Lcom/twitter/app/dm/ag$2;

    invoke-direct {v1, p0, p6, p1, p2}, Lcom/twitter/app/dm/ag$2;-><init>(Lcom/twitter/app/dm/ag;Lcne;J)V

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 369
    return-object v0

    .line 357
    :cond_0
    invoke-virtual {p3, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private a(Landroid/view/ViewGroup;)Landroid/widget/TextView;
    .locals 3

    .prologue
    .line 321
    sget-object v0, Lcom/twitter/app/dm/ag;->c:Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 322
    iget-object v1, p0, Lcom/twitter/app/dm/ag;->j:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 323
    if-eqz v0, :cond_0

    .line 324
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 325
    const/4 v0, 0x0

    .line 339
    :cond_0
    :goto_0
    return-object v0

    .line 328
    :cond_1
    if-nez v0, :cond_2

    .line 329
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04041d

    const/4 v2, 0x0

    .line 330
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 331
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 332
    sget-object v1, Lcom/twitter/app/dm/ag;->c:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 336
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/dm/ag;->j:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/app/dm/ag;)Lcom/twitter/app/dm/ag$a;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    return-object v0
.end method

.method private a(Lyj;Landroid/view/ViewGroup;Lcne;)Lcom/twitter/util/collection/q;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lyj;",
            "Landroid/view/ViewGroup;",
            "Lcne;",
            ")",
            "Lcom/twitter/util/collection/q",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 251
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    move v8, v0

    .line 253
    :goto_0
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v9

    .line 254
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v10

    .line 255
    invoke-virtual {p0}, Lcom/twitter/app/dm/ag;->g()Ljava/util/List;

    move-result-object v0

    .line 257
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 258
    invoke-virtual {p0, v0, v1}, Lcom/twitter/app/dm/ag;->h(J)Ljava/lang/Object;

    move-result-object v2

    .line 259
    sget-object v3, Lcom/twitter/app/dm/ag;->c:Ljava/lang/Integer;

    if-eq v2, v3, :cond_4

    .line 262
    invoke-virtual {p2, v2}, Landroid/view/ViewGroup;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v2

    .line 263
    iget-object v3, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    invoke-virtual {v3}, Lcom/twitter/app/dm/ag$a;->c()Ljava/util/Set;

    move-result-object v3

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 264
    invoke-virtual {v10, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 265
    iget-object v2, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    invoke-virtual {v2}, Lcom/twitter/app/dm/ag$a;->c()Ljava/util/Set;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 269
    :goto_2
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 271
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->j:Ljava/util/Set;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 272
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->j:Ljava/util/Set;

    .line 273
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v6, 0x1

    :goto_3
    move-object v1, p0

    move-object v4, p2

    move-object v5, p1

    move-object/from16 v7, p3

    .line 272
    invoke-direct/range {v1 .. v7}, Lcom/twitter/app/dm/ag;->a(JLandroid/view/ViewGroup;Lyj;ZLcne;)Landroid/view/View;

    move-result-object v0

    .line 274
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v3, v0}, Lcom/twitter/app/dm/ag;->a(JLjava/lang/Object;)V

    goto :goto_1

    .line 251
    :cond_1
    const/4 v0, 0x0

    move v8, v0

    goto/16 :goto_0

    .line 267
    :cond_2
    invoke-virtual {v9, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_2

    .line 273
    :cond_3
    const/4 v6, 0x0

    goto :goto_3

    .line 277
    :cond_4
    iget-object v2, p0, Lcom/twitter/app/dm/ag;->j:Ljava/util/Set;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 278
    iget-object v2, p0, Lcom/twitter/app/dm/ag;->e:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 282
    :cond_5
    invoke-static {}, Lcom/twitter/util/collection/h;->f()Lcom/twitter/util/collection/h;

    move-result-object v11

    .line 283
    invoke-virtual {p0}, Lcom/twitter/app/dm/ag;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_4
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 284
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v1, 0x4

    if-ge v0, v1, :cond_6

    .line 285
    const/4 v6, 0x0

    move-object v1, p0

    move-object v4, p2

    move-object v5, p1

    move-object/from16 v7, p3

    .line 286
    invoke-direct/range {v1 .. v7}, Lcom/twitter/app/dm/ag;->a(JLandroid/view/ViewGroup;Lyj;ZLcne;)Landroid/view/View;

    move-result-object v0

    .line 288
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 289
    invoke-virtual {p0, v2, v3, v1}, Lcom/twitter/app/dm/ag;->a(JLjava/lang/Object;)V

    .line 290
    invoke-virtual {v11, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_4

    .line 294
    :cond_6
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->j:Ljava/util/Set;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 298
    :cond_7
    invoke-direct {p0, p2}, Lcom/twitter/app/dm/ag;->a(Landroid/view/ViewGroup;)Landroid/widget/TextView;

    move-result-object v1

    .line 300
    if-eqz v1, :cond_9

    .line 302
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 303
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v4, v5, v0}, Lcom/twitter/app/dm/ag;->a(JLjava/lang/Object;)V

    goto :goto_5

    .line 306
    :cond_8
    if-nez v8, :cond_9

    .line 307
    invoke-virtual {v11, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 311
    :cond_9
    new-instance v0, Lcom/twitter/util/collection/q;

    invoke-virtual {v11}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v9}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v2

    .line 312
    invoke-virtual {v10}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/util/collection/q;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 311
    return-object v0
.end method

.method private a(JLyj;Lcom/twitter/media/ui/image/UserImageView;)V
    .locals 3

    .prologue
    .line 378
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ag$a;->b()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 379
    if-eqz v0, :cond_0

    .line 380
    invoke-virtual {p4, v0}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;)Z

    .line 395
    :goto_0
    return-void

    .line 382
    :cond_0
    invoke-virtual {p3, p1, p2}, Lyj;->a(J)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/dm/ag$3;

    invoke-direct {v1, p0, p4}, Lcom/twitter/app/dm/ag$3;-><init>(Lcom/twitter/app/dm/ag;Lcom/twitter/media/ui/image/UserImageView;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/app/dm/ag;)Lcqt;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->h:Lcqt;

    return-object v0
.end method

.method static synthetic k()J
    .locals 2

    .prologue
    .line 57
    sget-wide v0, Lcom/twitter/app/dm/ag;->a:J

    return-wide v0
.end method


# virtual methods
.method public a(Landroid/view/View;)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->l:Lcom/twitter/app/dm/ai;

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->l:Lcom/twitter/app/dm/ai;

    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/ai;->a(Landroid/view/View;)Landroid/animation/AnimatorSet;

    move-result-object v0

    .line 238
    :goto_0
    return-object v0

    .line 235
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)Lrx/j;
    .locals 3

    .prologue
    .line 193
    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/dm/ag;->e(J)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/dm/ag$1;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/ag$1;-><init>(Lcom/twitter/app/dm/ag;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    return-object v0
.end method

.method public a(JJ)V
    .locals 3

    .prologue
    .line 432
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ag$a;->a()Ljava/util/Map;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 433
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ag$a;->c()Ljava/util/Set;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 434
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ag$a;->d()Ljava/util/Set;

    move-result-object v0

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 435
    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/dm/ag;->c(J)V

    .line 437
    :cond_0
    return-void
.end method

.method protected a(JLjava/lang/Object;)V
    .locals 3

    .prologue
    .line 534
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 535
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->e:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536
    return-void
.end method

.method public a(JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 166
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ag$a;->b()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    return-void
.end method

.method public a(Landroid/view/View;ZLandroid/content/Context;Lyj;Lcne;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 109
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 111
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/ag;->l:Lcom/twitter/app/dm/ai;

    if-nez v0, :cond_5

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->l:Lcom/twitter/app/dm/ai;

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->l:Lcom/twitter/app/dm/ai;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ai;->e()V

    .line 115
    :cond_1
    new-instance v0, Lcom/twitter/app/dm/ai;

    invoke-direct {v0, p1, p3}, Lcom/twitter/app/dm/ai;-><init>(Landroid/view/View;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/app/dm/ag;->l:Lcom/twitter/app/dm/ai;

    .line 118
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 119
    :goto_0
    iget-object v3, p0, Lcom/twitter/app/dm/ag;->e:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 120
    iget-object v3, p0, Lcom/twitter/app/dm/ag;->j:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    move v3, v0

    .line 125
    :goto_1
    const v0, 0x7f130330

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 127
    invoke-virtual {p0}, Lcom/twitter/app/dm/ag;->e()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 130
    invoke-virtual {p0}, Lcom/twitter/app/dm/ag;->h()Z

    move-result v4

    if-nez v4, :cond_6

    .line 132
    :goto_2
    invoke-direct {p0, p4, v0, p5}, Lcom/twitter/app/dm/ag;->a(Lyj;Landroid/view/ViewGroup;Lcne;)Lcom/twitter/util/collection/q;

    move-result-object v2

    .line 133
    iget-object v0, v2, Lcom/twitter/util/collection/q;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 134
    iget-object v0, v2, Lcom/twitter/util/collection/q;->b:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    .line 135
    iget-object v0, v2, Lcom/twitter/util/collection/q;->c:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 137
    if-eqz v3, :cond_7

    .line 138
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->l:Lcom/twitter/app/dm/ai;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ai;->d()V

    .line 157
    :cond_2
    :goto_3
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->k:Lcom/twitter/app/dm/af;

    if-eqz v0, :cond_3

    .line 158
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->k:Lcom/twitter/app/dm/af;

    iget-object v1, p0, Lcom/twitter/app/dm/ag;->e:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/twitter/app/dm/af;->a(I)V

    .line 160
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 118
    goto :goto_0

    :cond_5
    move v3, v2

    .line 122
    goto :goto_1

    :cond_6
    move v1, v2

    .line 130
    goto :goto_2

    .line 139
    :cond_7
    if-eqz v1, :cond_8

    .line 140
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->l:Lcom/twitter/app/dm/ai;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ai;->a()V

    goto :goto_3

    .line 142
    :cond_8
    iget-object v1, p0, Lcom/twitter/app/dm/ag;->l:Lcom/twitter/app/dm/ai;

    invoke-virtual {v1, v5, v0}, Lcom/twitter/app/dm/ai;->a(Ljava/util/List;Ljava/util/List;)V

    .line 143
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->l:Lcom/twitter/app/dm/ai;

    invoke-virtual {v0, v4}, Lcom/twitter/app/dm/ai;->a(Ljava/util/List;)V

    goto :goto_3

    .line 145
    :cond_9
    invoke-virtual {p0}, Lcom/twitter/app/dm/ag;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 147
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 148
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 149
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ag$a;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 150
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->l:Lcom/twitter/app/dm/ai;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ai;->b()V

    goto :goto_3

    .line 152
    :cond_a
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ag$a;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 153
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->l:Lcom/twitter/app/dm/ai;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ai;->c()V

    goto :goto_3
.end method

.method public a(Ljava/util/Collection;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 441
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 442
    iget-object v2, v0, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    .line 443
    if-eqz v2, :cond_0

    .line 444
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5, v2}, Lcom/twitter/app/dm/ag;->a(JLjava/lang/String;)V

    goto :goto_0

    .line 447
    :cond_1
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->l:Lcom/twitter/app/dm/ai;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->l:Lcom/twitter/app/dm/ai;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->l:Lcom/twitter/app/dm/ai;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ai;->e()V

    .line 184
    :cond_0
    return-void
.end method

.method public b(J)V
    .locals 5

    .prologue
    .line 399
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 400
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ag$a;->a()Ljava/util/Map;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    .line 401
    iget-object v1, p0, Lcom/twitter/app/dm/ag;->h:Lcqt;

    invoke-interface {v1}, Lcqt;->b()J

    move-result-wide v2

    .line 402
    iget-object v1, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    invoke-virtual {v1}, Lcom/twitter/app/dm/ag$a;->a()Ljava/util/Map;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    if-nez v0, :cond_0

    .line 405
    invoke-virtual {p0}, Lcom/twitter/app/dm/ag;->i()V

    .line 408
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->k:Lcom/twitter/app/dm/af;

    if-eqz v0, :cond_1

    .line 409
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->k:Lcom/twitter/app/dm/af;

    invoke-interface {v0}, Lcom/twitter/app/dm/af;->a()V

    .line 411
    :cond_1
    return-void
.end method

.method public c()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lrx/j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 210
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 211
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ag$a;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 212
    invoke-virtual {p0, v4, v5}, Lcom/twitter/app/dm/ag;->a(J)Lrx/j;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 214
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public c(J)V
    .locals 3

    .prologue
    .line 415
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 416
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ag$a;->a()Ljava/util/Map;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    .line 417
    if-eqz v0, :cond_0

    .line 418
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ag$a;->a()Ljava/util/Map;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 420
    invoke-virtual {p0}, Lcom/twitter/app/dm/ag;->i()V

    .line 422
    :cond_0
    return-void
.end method

.method public d()Lcom/twitter/app/dm/ag$a;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    return-object v0
.end method

.method protected d(J)Ljava/lang/Long;
    .locals 3

    .prologue
    .line 462
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 463
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ag$a;->a()Ljava/util/Map;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    .line 464
    invoke-virtual {v0}, Lcom/twitter/app/dm/ag$a;->a()Ljava/util/Map;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 463
    :goto_0
    return-object v0

    .line 464
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method e(J)Lrx/c;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/c",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 473
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    .line 474
    invoke-virtual {p0}, Lcom/twitter/app/dm/ag;->j()J

    move-result-wide v2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lrx/c;->b(JLjava/util/concurrent/TimeUnit;)Lrx/c;

    move-result-object v0

    .line 475
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/dm/ag$4;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/ag$4;-><init>(Lcom/twitter/app/dm/ag;)V

    .line 476
    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 473
    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 453
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 454
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ag$a;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected f()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 492
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 493
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    .line 494
    invoke-virtual {v1}, Lcom/twitter/app/dm/ag$a;->a()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/ag;->e:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->d(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 493
    return-object v0
.end method

.method public f(J)Z
    .locals 3

    .prologue
    .line 519
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ag$a;->d()Ljava/util/Set;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected g()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 502
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 503
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/ag;->e:Ljava/util/Map;

    .line 504
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    invoke-virtual {v1}, Lcom/twitter/app/dm/ag$a;->a()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->d(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 503
    return-object v0
.end method

.method public g(J)V
    .locals 3

    .prologue
    .line 527
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->d:Lcom/twitter/app/dm/ag$a;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ag$a;->d()Ljava/util/Set;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 528
    return-void
.end method

.method protected h(J)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 543
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 544
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->e:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 511
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 512
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected i()V
    .locals 5

    .prologue
    .line 551
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->f:Laut;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/net/Uri;

    const/4 v2, 0x0

    sget-object v3, Lcom/twitter/database/schema/a$i;->a:Landroid/net/Uri;

    iget-object v4, p0, Lcom/twitter/app/dm/ag;->g:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Laut;->a([Landroid/net/Uri;)V

    .line 552
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->f:Laut;

    invoke-virtual {v0}, Laut;->a()V

    .line 553
    return-void
.end method

.method protected j()J
    .locals 2

    .prologue
    .line 556
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->m:Ljava/lang/Long;

    if-nez v0, :cond_0

    sget-wide v0, Lcom/twitter/app/dm/ag;->a:J

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/ag;->m:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method
