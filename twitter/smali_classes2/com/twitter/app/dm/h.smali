.class public Lcom/twitter/app/dm/h;
.super Lcom/twitter/app/dm/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/h$a;
    }
.end annotation


# direct methods
.method protected constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/b;-><init>(Landroid/os/Bundle;)V

    .line 17
    return-void
.end method

.method public static d(Landroid/os/Bundle;)Lcom/twitter/app/dm/h;
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/twitter/app/dm/h;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/h;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public f()Z
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Lcom/twitter/app/dm/h;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "is_sharing_external_content"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lcom/twitter/app/dm/h;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "is_forwarding_message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public j()Z
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/app/dm/h;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "should_add_participants_to_existing_conversation"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/twitter/app/dm/h;->c:Landroid/os/Bundle;

    const-string/jumbo v1, "should_go_back_to_source_activity"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
