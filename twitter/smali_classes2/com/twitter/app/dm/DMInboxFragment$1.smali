.class Lcom/twitter/app/dm/DMInboxFragment$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/dm/DMInboxFragment;->a(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/dm/DMInboxFragment;


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/DMInboxFragment;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lcom/twitter/app/dm/DMInboxFragment$1;->a:Lcom/twitter/app/dm/DMInboxFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 175
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/provider/f;

    .line 176
    if-eqz v0, :cond_0

    .line 177
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v2, p0, Lcom/twitter/app/dm/DMInboxFragment$1;->a:Lcom/twitter/app/dm/DMInboxFragment;

    invoke-static {v2}, Lcom/twitter/app/dm/DMInboxFragment;->a(Lcom/twitter/app/dm/DMInboxFragment;)J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "messages:inbox:user_list:user:select"

    aput-object v4, v2, v3

    .line 178
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    .line 177
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 179
    iget-object v1, p0, Lcom/twitter/app/dm/DMInboxFragment$1;->a:Lcom/twitter/app/dm/DMInboxFragment;

    iget-object v2, p0, Lcom/twitter/app/dm/DMInboxFragment$1;->a:Lcom/twitter/app/dm/DMInboxFragment;

    invoke-virtual {v2}, Lcom/twitter/app/dm/DMInboxFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/twitter/app/dm/j$a;

    invoke-direct {v3}, Lcom/twitter/app/dm/j$a;-><init>()V

    iget-wide v4, v0, Lcom/twitter/android/provider/f;->a:J

    .line 181
    invoke-virtual {v3, v4, v5}, Lcom/twitter/app/dm/j$a;->a(J)Lcom/twitter/app/dm/j$a;

    move-result-object v0

    .line 182
    invoke-virtual {v0}, Lcom/twitter/app/dm/j$a;->e()Lcom/twitter/app/dm/j;

    move-result-object v0

    .line 179
    invoke-static {v2, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/j;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/app/dm/DMInboxFragment;->startActivity(Landroid/content/Intent;)V

    .line 184
    :cond_0
    return-void
.end method
