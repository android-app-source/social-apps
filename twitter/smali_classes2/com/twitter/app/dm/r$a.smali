.class public Lcom/twitter/app/dm/r$a;
.super Lcom/twitter/app/dm/b$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/dm/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/dm/b$a",
        "<",
        "Lcom/twitter/app/dm/r$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 71
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/twitter/app/dm/b$a;-><init>(Landroid/os/Bundle;I)V

    .line 72
    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/twitter/app/dm/b;
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/twitter/app/dm/r$a;->e()Lcom/twitter/app/dm/r;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lcom/twitter/app/dm/r$a;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 103
    iget-object v0, p0, Lcom/twitter/app/dm/r$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "title_text_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 104
    return-object p0
.end method

.method public a(Lcom/twitter/model/core/r;)Lcom/twitter/app/dm/r$a;
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Lcom/twitter/app/dm/r$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "quoted_tweet"

    sget-object v2, Lcom/twitter/model/core/r;->a:Lcom/twitter/util/serialization/l;

    .line 77
    invoke-static {p1, v2}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    .line 76
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 78
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/app/dm/r$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)",
            "Lcom/twitter/app/dm/r$a;"
        }
    .end annotation

    .prologue
    .line 83
    if-eqz p1, :cond_0

    .line 84
    iget-object v0, p0, Lcom/twitter/app/dm/r$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "recipients"

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 86
    :cond_0
    return-object p0
.end method

.method public synthetic b()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/twitter/app/dm/r$a;->e()Lcom/twitter/app/dm/r;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/app/dm/r$a;
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/twitter/app/dm/r$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "hint_text"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    return-object p0
.end method

.method public synthetic c()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/twitter/app/dm/r$a;->e()Lcom/twitter/app/dm/r;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/app/dm/r$a;
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/twitter/app/dm/r$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "scribe_component"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    return-object p0
.end method

.method public c(Z)Lcom/twitter/app/dm/r$a;
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/twitter/app/dm/r$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "is_from_message_me_card"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 92
    return-object p0
.end method

.method public e()Lcom/twitter/app/dm/r;
    .locals 2

    .prologue
    .line 116
    new-instance v0, Lcom/twitter/app/dm/r;

    iget-object v1, p0, Lcom/twitter/app/dm/r$a;->a:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Lcom/twitter/app/dm/r;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method
