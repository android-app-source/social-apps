.class public abstract Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;
.super Lcom/twitter/android/widget/PromptDialogFragment;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/dialog/BaseDMMessageDialog$a;
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/model/dms/e;

.field private c:[I

.field private d:Lcom/twitter/app/dm/dialog/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/twitter/android/widget/PromptDialogFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;)Lcom/twitter/app/dm/dialog/a;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->d:Lcom/twitter/app/dm/dialog/a;

    return-object v0
.end method

.method private a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 221
    new-instance v0, Lcom/twitter/app/dm/h$a;

    invoke-direct {v0}, Lcom/twitter/app/dm/h$a;-><init>()V

    .line 222
    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/h$a;->i(Z)Lcom/twitter/app/dm/h$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->a:Lcom/twitter/model/dms/e;

    .line 223
    invoke-virtual {v1}, Lcom/twitter/model/dms/e;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/h$a;->a(Ljava/lang/String;)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/h$a;

    .line 224
    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/h$a;->c(Z)Lcom/twitter/app/dm/h$a;

    move-result-object v1

    .line 225
    if-eqz p1, :cond_0

    .line 226
    const-string/jumbo v0, "android.intent.extra.STREAM"

    invoke-virtual {v1, v0, p1}, Lcom/twitter/app/dm/h$a;->a(Ljava/lang/String;Landroid/os/Parcelable;)Lcom/twitter/app/common/base/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/h$a;

    .line 227
    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/h$a;->g(Z)Lcom/twitter/app/dm/h$a;

    .line 229
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1}, Lcom/twitter/app/dm/h$a;->e()Lcom/twitter/app/dm/h;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/h;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->startActivity(Landroid/content/Intent;)V

    .line 230
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->a(Landroid/net/Uri;)V

    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    .line 101
    iget-object v0, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->d:Lcom/twitter/app/dm/dialog/a;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->d:Lcom/twitter/app/dm/dialog/a;

    iget-object v1, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->a:Lcom/twitter/model/dms/e;

    invoke-virtual {v1}, Lcom/twitter/model/dms/e;->a()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/twitter/app/dm/dialog/a;->d(J)V

    .line 104
    :cond_0
    return-void
.end method

.method private f()V
    .locals 4

    .prologue
    .line 107
    iget-object v0, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->d:Lcom/twitter/app/dm/dialog/a;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->d:Lcom/twitter/app/dm/dialog/a;

    iget-object v1, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->a:Lcom/twitter/model/dms/e;

    invoke-virtual {v1}, Lcom/twitter/model/dms/e;->a()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/twitter/app/dm/dialog/a;->c(J)V

    .line 110
    :cond_0
    return-void
.end method

.method private g()V
    .locals 6

    .prologue
    .line 114
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 115
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 116
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "messages:thread::message:delete_dm"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 118
    invoke-virtual {p0}, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 119
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v2

    .line 121
    iget-object v3, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->a:Lcom/twitter/model/dms/e;

    invoke-virtual {v3}, Lcom/twitter/model/dms/e;->d()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 122
    new-instance v3, Lcom/twitter/app/dm/s;

    iget-object v4, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->a:Lcom/twitter/model/dms/e;

    .line 123
    invoke-virtual {v4}, Lcom/twitter/model/dms/e;->a()J

    move-result-wide v4

    invoke-direct {v3, v1, v0, v4, v5}, Lcom/twitter/app/dm/s;-><init>(Landroid/content/ContextWrapper;Lcom/twitter/library/client/Session;J)V

    .line 122
    invoke-virtual {v2, v3}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 124
    iget-object v0, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->a:Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->v()Ljava/lang/String;

    move-result-object v0

    .line 125
    iget-object v1, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->d:Lcom/twitter/app/dm/dialog/a;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 126
    iget-object v1, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->d:Lcom/twitter/app/dm/dialog/a;

    invoke-interface {v1, v0}, Lcom/twitter/app/dm/dialog/a;->d(Ljava/lang/String;)V

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    new-instance v3, Lcom/twitter/library/api/dm/m;

    iget-object v4, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->a:Lcom/twitter/model/dms/e;

    .line 130
    invoke-virtual {v4}, Lcom/twitter/model/dms/e;->a()J

    move-result-wide v4

    invoke-direct {v3, v1, v0, v4, v5}, Lcom/twitter/library/api/dm/m;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    new-instance v0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog$1;

    invoke-direct {v0, p0, v1}, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog$1;-><init>(Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;Landroid/content/ContextWrapper;)V

    .line 129
    invoke-virtual {v2, v3, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    goto :goto_0
.end method

.method private h()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 149
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 150
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 151
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v5, [Ljava/lang/String;

    const-string/jumbo v3, "messages:thread::message:forward_dm"

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 153
    iget-object v1, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->a:Lcom/twitter/model/dms/e;

    invoke-virtual {v1}, Lcom/twitter/model/dms/e;->z()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 154
    new-instance v1, Lcom/twitter/library/network/t;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->h()Lcom/twitter/model/account/OAuthToken;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/twitter/library/network/t;-><init>(Lcom/twitter/model/account/OAuthToken;)V

    .line 155
    iget-object v0, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->a:Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->u()Lcbx;

    move-result-object v0

    check-cast v0, Lccf;

    .line 156
    new-instance v2, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog$a;

    invoke-direct {v2, p0}, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog$a;-><init>(Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;)V

    new-array v3, v5, [Lcom/twitter/media/request/a;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccf;

    invoke-static {v0}, Lcom/twitter/media/util/k;->a(Lccf;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    .line 157
    invoke-virtual {v0, v1}, Lcom/twitter/media/request/a$a;->a(Ljava/lang/Object;)Lcom/twitter/media/request/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/a$a;

    invoke-virtual {v0}, Lcom/twitter/media/request/a$a;->a()Lcom/twitter/media/request/a;

    move-result-object v0

    aput-object v0, v3, v4

    .line 156
    invoke-virtual {v2, v3}, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 161
    :goto_0
    return-void

    .line 159
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method private i()V
    .locals 4

    .prologue
    .line 165
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 166
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v2, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v3, "messages:thread::message:cancel_dm"

    aput-object v3, v0, v1

    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 168
    iget-object v0, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->a:Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->v()Ljava/lang/String;

    move-result-object v0

    .line 169
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->d:Lcom/twitter/app/dm/dialog/a;

    if-eqz v1, :cond_0

    .line 170
    iget-object v1, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->d:Lcom/twitter/app/dm/dialog/a;

    iget-object v2, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->a:Lcom/twitter/model/dms/e;

    invoke-virtual {v2}, Lcom/twitter/model/dms/e;->q()J

    move-result-wide v2

    invoke-interface {v1, v2, v3, v0}, Lcom/twitter/app/dm/dialog/a;->a(JLjava/lang/String;)V

    .line 172
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/app/FragmentManager;)V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->c:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->c:[I

    array-length v0, v0

    if-nez v0, :cond_1

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 217
    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0
.end method

.method protected abstract a(Lcom/twitter/model/dms/e;)V
.end method

.method protected a(Lcom/twitter/model/dms/e;[ILcom/twitter/app/dm/dialog/a;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->a:Lcom/twitter/model/dms/e;

    .line 177
    iput-object p2, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->c:[I

    .line 178
    iput-object p3, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->d:Lcom/twitter/app/dm/dialog/a;

    .line 179
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 203
    invoke-super {p0, p1}, Lcom/twitter/android/widget/PromptDialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 204
    invoke-virtual {p0}, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 205
    iget-object v1, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->d:Lcom/twitter/app/dm/dialog/a;

    if-nez v1, :cond_0

    .line 206
    const-class v1, Lcom/twitter/app/dm/dialog/a;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    aput-object p1, v2, v0

    invoke-static {v1, v2}, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/dialog/a;

    iput-object v0, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->d:Lcom/twitter/app/dm/dialog/a;

    .line 208
    :cond_0
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 65
    iget-object v0, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->c:[I

    aget v0, v0, p2

    sparse-switch v0, :sswitch_data_0

    .line 97
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/twitter/android/widget/PromptDialogFragment;->onClick(Landroid/content/DialogInterface;I)V

    .line 98
    return-void

    .line 68
    :sswitch_0
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 69
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v2, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v3, "messages:thread::message:copy"

    aput-object v3, v0, v1

    invoke-virtual {v2, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 70
    iget-object v0, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->a:Lcom/twitter/model/dms/e;

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->a(Lcom/twitter/model/dms/e;)V

    goto :goto_0

    .line 74
    :sswitch_1
    invoke-direct {p0}, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->i()V

    goto :goto_0

    .line 78
    :sswitch_2
    invoke-direct {p0}, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->g()V

    goto :goto_0

    .line 82
    :sswitch_3
    invoke-direct {p0}, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->f()V

    goto :goto_0

    .line 86
    :sswitch_4
    invoke-direct {p0}, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->e()V

    goto :goto_0

    .line 90
    :sswitch_5
    invoke-direct {p0}, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->h()V

    goto :goto_0

    .line 65
    :sswitch_data_0
    .sparse-switch
        0x7f0a00f8 -> :sswitch_1
        0x7f0a021e -> :sswitch_0
        0x7f0a021f -> :sswitch_0
        0x7f0a0263 -> :sswitch_2
        0x7f0a029d -> :sswitch_4
        0x7f0a03b3 -> :sswitch_5
        0x7f0a0779 -> :sswitch_3
    .end sparse-switch
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 186
    if-eqz p1, :cond_0

    .line 187
    const-string/jumbo v0, "message"

    sget-object v1, Lcom/twitter/model/dms/e;->b:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e;

    iput-object v0, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->a:Lcom/twitter/model/dms/e;

    .line 189
    const-string/jumbo v0, "dialog_items"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->c:[I

    .line 191
    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/widget/PromptDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 196
    const-string/jumbo v0, "message"

    iget-object v1, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->a:Lcom/twitter/model/dms/e;

    sget-object v2, Lcom/twitter/model/dms/e;->b:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v0, v1, v2}, Lcom/twitter/util/v;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Landroid/os/Bundle;

    .line 197
    const-string/jumbo v0, "dialog_items"

    iget-object v1, p0, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;->c:[I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 198
    invoke-super {p0, p1}, Lcom/twitter/android/widget/PromptDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 199
    return-void
.end method
