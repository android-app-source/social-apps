.class public Lcom/twitter/app/dm/dialog/ReportConversationDialog;
.super Lcom/twitter/android/widget/PromptDialogFragment;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/dialog/ReportConversationDialog$a;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Lcom/twitter/app/dm/dialog/ReportConversationDialog$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/twitter/android/widget/PromptDialogFragment;-><init>()V

    return-void
.end method

.method public static a(IZLjava/lang/String;Ljava/lang/String;Lcom/twitter/app/dm/dialog/ReportConversationDialog$a;)Lcom/twitter/app/dm/dialog/ReportConversationDialog;
    .locals 2

    .prologue
    .line 41
    new-instance v0, Lcom/twitter/app/dm/dialog/f$b;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/dialog/f$b;-><init>(I)V

    const v1, 0x7f0a0769

    .line 42
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/dialog/f$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/dialog/f$b;

    const v1, 0x7f0b0023

    .line 43
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/dialog/f$b;->c(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/dialog/f$b;

    .line 44
    invoke-virtual {v0}, Lcom/twitter/app/dm/dialog/f$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/dialog/ReportConversationDialog;

    .line 45
    invoke-direct {v0, p2, p3, p1, p4}, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->a(Ljava/lang/String;Ljava/lang/String;ZLcom/twitter/app/dm/dialog/ReportConversationDialog$a;)V

    .line 46
    return-object v0
.end method

.method private a(Landroid/content/Context;Lcom/twitter/library/api/dm/ReportConversationRequest$Type;Lcom/twitter/library/client/Session;)V
    .locals 3

    .prologue
    .line 59
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    .line 60
    new-instance v1, Lcom/twitter/library/api/dm/ReportConversationRequest;

    iget-object v2, p0, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->a:Ljava/lang/String;

    invoke-direct {v1, p1, p3, v2, p2}, Lcom/twitter/library/api/dm/ReportConversationRequest;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Lcom/twitter/library/api/dm/ReportConversationRequest$Type;)V

    new-instance v2, Lcom/twitter/app/dm/dialog/ReportConversationDialog$1;

    invoke-direct {v2, p0, p1}, Lcom/twitter/app/dm/dialog/ReportConversationDialog$1;-><init>(Lcom/twitter/app/dm/dialog/ReportConversationDialog;Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 72
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;ZLcom/twitter/app/dm/dialog/ReportConversationDialog$a;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->a:Ljava/lang/String;

    .line 52
    iput-object p2, p0, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->c:Ljava/lang/String;

    .line 53
    iput-boolean p3, p0, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->d:Z

    .line 54
    iput-object p4, p0, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->e:Lcom/twitter/app/dm/dialog/ReportConversationDialog$a;

    .line 55
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/dialog/ReportConversationDialog;)Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->d:Z

    return v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 117
    invoke-super {p0, p1}, Lcom/twitter/android/widget/PromptDialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 118
    invoke-virtual {p0}, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 119
    iget-object v1, p0, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->e:Lcom/twitter/app/dm/dialog/ReportConversationDialog$a;

    if-nez v1, :cond_0

    .line 120
    const-class v1, Lcom/twitter/app/dm/dialog/ReportConversationDialog$a;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    aput-object p1, v2, v0

    invoke-static {v1, v2}, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/dialog/ReportConversationDialog$a;

    iput-object v0, p0, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->e:Lcom/twitter/app/dm/dialog/ReportConversationDialog$a;

    .line 122
    :cond_0
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 79
    invoke-virtual {p0}, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 80
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 82
    if-nez p2, :cond_1

    .line 83
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "messages"

    aput-object v4, v3, v7

    iget-object v4, p0, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->c:Ljava/lang/String;

    aput-object v4, v3, v6

    const-string/jumbo v4, "thread"

    aput-object v4, v3, v8

    const-string/jumbo v4, "spam"

    aput-object v4, v3, v9

    const-string/jumbo v4, "report_as_spam"

    aput-object v4, v3, v10

    .line 84
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 83
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 85
    sget-object v2, Lcom/twitter/library/api/dm/ReportConversationRequest$Type;->a:Lcom/twitter/library/api/dm/ReportConversationRequest$Type;

    invoke-direct {p0, v0, v2, v1}, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->a(Landroid/content/Context;Lcom/twitter/library/api/dm/ReportConversationRequest$Type;Lcom/twitter/library/client/Session;)V

    .line 86
    iget-object v0, p0, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->e:Lcom/twitter/app/dm/dialog/ReportConversationDialog$a;

    invoke-interface {v0}, Lcom/twitter/app/dm/dialog/ReportConversationDialog$a;->D()V

    .line 93
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/twitter/android/widget/PromptDialogFragment;->onClick(Landroid/content/DialogInterface;I)V

    .line 94
    return-void

    .line 87
    :cond_1
    if-ne p2, v6, :cond_0

    .line 88
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "messages"

    aput-object v4, v3, v7

    iget-object v4, p0, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->c:Ljava/lang/String;

    aput-object v4, v3, v6

    const-string/jumbo v4, "thread"

    aput-object v4, v3, v8

    const-string/jumbo v4, "abusive"

    aput-object v4, v3, v9

    const-string/jumbo v4, "report_as_spam"

    aput-object v4, v3, v10

    .line 89
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 88
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 90
    sget-object v2, Lcom/twitter/library/api/dm/ReportConversationRequest$Type;->b:Lcom/twitter/library/api/dm/ReportConversationRequest$Type;

    invoke-direct {p0, v0, v2, v1}, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->a(Landroid/content/Context;Lcom/twitter/library/api/dm/ReportConversationRequest$Type;Lcom/twitter/library/client/Session;)V

    .line 91
    iget-object v0, p0, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->e:Lcom/twitter/app/dm/dialog/ReportConversationDialog$a;

    invoke-interface {v0}, Lcom/twitter/app/dm/dialog/ReportConversationDialog$a;->F()V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 99
    if-eqz p1, :cond_0

    .line 100
    const-string/jumbo v0, "conversation_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->a:Ljava/lang/String;

    .line 101
    const-string/jumbo v0, "scribe_section"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->c:Ljava/lang/String;

    .line 102
    const-string/jumbo v0, "is_group"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->d:Z

    .line 104
    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/widget/PromptDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 109
    invoke-super {p0, p1}, Lcom/twitter/android/widget/PromptDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 110
    const-string/jumbo v0, "conversation_id"

    iget-object v1, p0, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const-string/jumbo v0, "scribe_section"

    iget-object v1, p0, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string/jumbo v0, "is_group"

    iget-boolean v1, p0, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 113
    return-void
.end method
