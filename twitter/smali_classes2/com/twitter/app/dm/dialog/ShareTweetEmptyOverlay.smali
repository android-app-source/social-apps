.class public Lcom/twitter/app/dm/dialog/ShareTweetEmptyOverlay;
.super Lcom/twitter/android/dialog/TakeoverDialogFragment;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/twitter/model/core/r;Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/twitter/app/dm/dialog/g$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/app/dm/dialog/g$b;-><init>(I)V

    .line 22
    invoke-virtual {v0, p0}, Lcom/twitter/app/dm/dialog/g$b;->a(Lcom/twitter/model/core/r;)Lcom/twitter/app/dm/dialog/g$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/dialog/g$b;

    const v1, 0x7f0a08ac

    .line 23
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/dialog/g$b;->b(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/dialog/g$b;

    const v1, 0x7f0a08ab

    .line 24
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/dialog/g$b;->d(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/dialog/g$b;

    const v1, 0x7f020574

    .line 25
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/dialog/g$b;->a(I)Lcom/twitter/android/dialog/f$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/dialog/g$b;

    .line 26
    invoke-virtual {v0}, Lcom/twitter/app/dm/dialog/g$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 27
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 28
    return-void
.end method


# virtual methods
.method public synthetic b()Lcom/twitter/android/dialog/g;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/twitter/app/dm/dialog/ShareTweetEmptyOverlay;->m()Lcom/twitter/app/dm/dialog/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()Lcom/twitter/android/dialog/f;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/twitter/app/dm/dialog/ShareTweetEmptyOverlay;->m()Lcom/twitter/app/dm/dialog/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()Lcom/twitter/app/common/dialog/a;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/twitter/app/dm/dialog/ShareTweetEmptyOverlay;->m()Lcom/twitter/app/dm/dialog/g;

    move-result-object v0

    return-object v0
.end method

.method protected h()V
    .locals 3

    .prologue
    .line 33
    invoke-super {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->h()V

    .line 34
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "messages:share_tweet_no_followers::impression"

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/dialog/ShareTweetEmptyOverlay;->a([Ljava/lang/String;)V

    .line 35
    return-void
.end method

.method protected i()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 48
    invoke-super {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->i()V

    .line 49
    invoke-virtual {p0}, Lcom/twitter/app/dm/dialog/ShareTweetEmptyOverlay;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/app/dm/dialog/ShareTweetEmptyOverlay;->m()Lcom/twitter/app/dm/dialog/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/app/dm/dialog/g;->a()Lcom/twitter/model/core/r;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Lcom/twitter/model/core/r;Z)V

    .line 50
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "messages:share_tweet_no_followers::share_tweet"

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/dialog/ShareTweetEmptyOverlay;->a([Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method protected k()V
    .locals 3

    .prologue
    .line 40
    invoke-super {p0}, Lcom/twitter/android/dialog/TakeoverDialogFragment;->k()V

    .line 41
    invoke-virtual {p0}, Lcom/twitter/app/dm/dialog/ShareTweetEmptyOverlay;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 42
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "messages:share_tweet_no_followers::cancel"

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/dialog/ShareTweetEmptyOverlay;->a([Ljava/lang/String;)V

    .line 43
    return-void
.end method

.method public m()Lcom/twitter/app/dm/dialog/g;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/twitter/app/dm/dialog/ShareTweetEmptyOverlay;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/dm/dialog/g;->a(Landroid/os/Bundle;)Lcom/twitter/app/dm/dialog/g;

    move-result-object v0

    return-object v0
.end method
