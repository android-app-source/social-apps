.class public Lcom/twitter/app/dm/dialog/DMTweetDialog;
.super Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;-><init>()V

    return-void
.end method

.method public static a(ILcom/twitter/model/dms/e;Lcom/twitter/app/dm/dialog/a;)Lcom/twitter/app/dm/dialog/DMTweetDialog;
    .locals 2

    .prologue
    .line 20
    invoke-static {p1}, Lcom/twitter/app/dm/dialog/DMTweetDialog;->b(Lcom/twitter/model/dms/e;)[I

    move-result-object v0

    .line 21
    invoke-static {p0, v0}, Lcom/twitter/app/dm/dialog/DMTweetDialog;->a(I[I)Lcom/twitter/app/dm/dialog/DMTweetDialog;

    move-result-object v1

    .line 22
    invoke-virtual {v1, p1, v0, p2}, Lcom/twitter/app/dm/dialog/DMTweetDialog;->a(Lcom/twitter/model/dms/e;[ILcom/twitter/app/dm/dialog/a;)V

    .line 23
    return-object v1
.end method

.method private static a(I[I)Lcom/twitter/app/dm/dialog/DMTweetDialog;
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/twitter/app/dm/dialog/c$a;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/dialog/c$a;-><init>(I)V

    .line 29
    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/dialog/c$a;->a([I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 30
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/dialog/DMTweetDialog;

    .line 28
    return-object v0
.end method

.method static b(Lcom/twitter/model/dms/e;)[I
    .locals 6

    .prologue
    const v5, 0x7f0a0263

    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 35
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    const v1, 0x7f0a021f

    .line 36
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 37
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast p0, Lcom/twitter/model/dms/r;

    invoke-virtual {p0}, Lcom/twitter/model/dms/r;->f()Z

    move-result v1

    if-nez v1, :cond_0

    .line 38
    new-array v1, v2, [Ljava/lang/Integer;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0x7f0a0779

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->b([Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 42
    :goto_0
    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->d(Ljava/util/Collection;)[I

    move-result-object v0

    return-object v0

    .line 40
    :cond_0
    new-array v1, v2, [Ljava/lang/Integer;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0x7f0a029d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->b([Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0
.end method


# virtual methods
.method protected a(Lcom/twitter/model/dms/e;)V
    .locals 6

    .prologue
    .line 47
    invoke-virtual {p1}, Lcom/twitter/model/dms/e;->u()Lcbx;

    move-result-object v0

    check-cast v0, Lcci;

    iget-object v0, v0, Lcci;->d:Lcom/twitter/model/core/r;

    .line 48
    const v1, 0x7f0a0c3c

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, v0, Lcom/twitter/model/core/r;->d:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, v0, Lcom/twitter/model/core/r;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/twitter/app/dm/dialog/DMTweetDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 49
    invoke-virtual {p0}, Lcom/twitter/app/dm/dialog/DMTweetDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 50
    return-void
.end method
