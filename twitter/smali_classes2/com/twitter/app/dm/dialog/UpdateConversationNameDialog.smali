.class public Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;
.super Lcom/twitter/android/widget/PromptDialogFragment;
.source "Twttr"


# instance fields
.field private a:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/twitter/android/widget/PromptDialogFragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lcom/twitter/app/dm/dialog/h$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/app/dm/dialog/h$b;-><init>(I)V

    const v1, 0x7f0a0616

    .line 40
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/dialog/h$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/dialog/h$b;

    const v1, 0x7f0a00f6

    .line 41
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/dialog/h$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/dialog/h$b;

    const v1, 0x7f0400b7

    .line 42
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/dialog/h$b;->g(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/dialog/h$b;

    .line 43
    invoke-virtual {v0}, Lcom/twitter/app/dm/dialog/h$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;

    .line 44
    invoke-direct {v0}, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;->e()V

    .line 45
    invoke-virtual {v0, p0, p1}, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;->c:Ljava/lang/String;

    return-object v0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog$1;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog$1;-><init>(Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;)V

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;->a(Lcom/twitter/app/common/dialog/b$b;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    .line 59
    new-instance v0, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog$2;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog$2;-><init>(Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;)V

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    .line 97
    return-void
.end method


# virtual methods
.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 100
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;->a:Ljava/lang/String;

    .line 101
    iput-object p2, p0, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;->c:Ljava/lang/String;

    .line 102
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 107
    if-eqz p1, :cond_0

    .line 108
    const-string/jumbo v0, "extra_old_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;->a:Ljava/lang/String;

    .line 109
    const-string/jumbo v0, "extra_conversation_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;->c:Ljava/lang/String;

    .line 111
    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/widget/PromptDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 116
    invoke-super {p0, p1}, Lcom/twitter/android/widget/PromptDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 117
    const-string/jumbo v0, "extra_old_name"

    iget-object v1, p0, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string/jumbo v0, "extra_conversation_id"

    iget-object v1, p0, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    return-void
.end method
