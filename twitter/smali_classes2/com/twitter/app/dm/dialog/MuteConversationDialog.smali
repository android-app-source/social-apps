.class public Lcom/twitter/app/dm/dialog/MuteConversationDialog;
.super Lcom/twitter/android/widget/PromptDialogFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/client/s;


# instance fields
.field private a:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/twitter/android/widget/PromptDialogFragment;-><init>()V

    return-void
.end method

.method private static a(I)Lcom/twitter/app/dm/dialog/MuteConversationDialog;
    .locals 2

    .prologue
    .line 52
    new-instance v0, Lcom/twitter/app/dm/dialog/e$b;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/dialog/e$b;-><init>(I)V

    .line 53
    invoke-static {}, Lcom/twitter/app/dm/dialog/MuteConversationDialog;->e()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/dialog/e$b;->a([I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/dialog/e$b;

    .line 54
    invoke-virtual {v0}, Lcom/twitter/app/dm/dialog/e$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/dialog/MuteConversationDialog;

    .line 52
    return-object v0
.end method

.method public static a(ILjava/lang/String;Ljava/lang/String;)Lcom/twitter/app/dm/dialog/MuteConversationDialog;
    .locals 1

    .prologue
    .line 40
    invoke-static {p0}, Lcom/twitter/app/dm/dialog/MuteConversationDialog;->a(I)Lcom/twitter/app/dm/dialog/MuteConversationDialog;

    move-result-object v0

    .line 41
    invoke-direct {v0, p1, p2}, Lcom/twitter/app/dm/dialog/MuteConversationDialog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 46
    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/dialog/MuteConversationDialog;->a:Ljava/lang/String;

    .line 47
    iput-object p2, p0, Lcom/twitter/app/dm/dialog/MuteConversationDialog;->c:Ljava/lang/String;

    .line 48
    return-void
.end method

.method private static e()[I
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 4
        0x7f0a05a6
        0x7f0a05aa
        0x7f0a05a8
        0x7f0a05b0
    .end array-data
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;Lcom/twitter/library/service/s;)V
    .locals 0

    .prologue
    .line 118
    return-void
.end method

.method public a(ILcom/twitter/library/service/s;)V
    .locals 0

    .prologue
    .line 114
    return-void
.end method

.method public b(ILcom/twitter/library/service/s;)V
    .locals 3

    .prologue
    .line 122
    invoke-virtual {p2}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 123
    packed-switch p1, :pswitch_data_0

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 128
    :pswitch_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/twitter/app/dm/dialog/MuteConversationDialog;->b:Landroid/content/Context;

    const v1, 0x7f0a02bb

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 123
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 13

    .prologue
    const/4 v11, 0x3

    const/4 v5, 0x2

    const/4 v8, 0x0

    const/4 v4, 0x1

    .line 71
    invoke-virtual {p0}, Lcom/twitter/app/dm/dialog/MuteConversationDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 72
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v12

    .line 74
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 76
    packed-switch p2, :pswitch_data_0

    .line 102
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-direct {v0, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v4, [Ljava/lang/String;

    const-string/jumbo v5, "messages:inbox::thread:mute_dm_thread_forever"

    aput-object v5, v3, v8

    .line 103
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 102
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 104
    new-instance v0, Lcom/twitter/library/api/dm/u;

    iget-object v3, p0, Lcom/twitter/app/dm/dialog/MuteConversationDialog;->a:Ljava/lang/String;

    move v5, v8

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/dm/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;ZI)V

    invoke-virtual {v12, v0, v11, p0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;ILcom/twitter/library/client/s;)Z

    .line 110
    :goto_0
    return-void

    .line 78
    :pswitch_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-direct {v0, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v4, [Ljava/lang/String;

    const-string/jumbo v5, "messages:inbox::thread:mute_dm_thread_1h"

    aput-object v5, v3, v8

    .line 79
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 78
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 80
    new-instance v0, Lcom/twitter/library/api/dm/u;

    iget-object v3, p0, Lcom/twitter/app/dm/dialog/MuteConversationDialog;->a:Ljava/lang/String;

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/dm/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;ZI)V

    invoke-virtual {v12, v0, v8, p0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;ILcom/twitter/library/client/s;)Z

    goto :goto_0

    .line 86
    :pswitch_1
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-direct {v0, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v4, [Ljava/lang/String;

    const-string/jumbo v6, "messages:inbox::thread:mute_dm_thread_8h"

    aput-object v6, v3, v8

    .line 87
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 86
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 88
    new-instance v0, Lcom/twitter/library/api/dm/u;

    iget-object v3, p0, Lcom/twitter/app/dm/dialog/MuteConversationDialog;->a:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/dm/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;ZI)V

    invoke-virtual {v12, v0, v4, p0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;ILcom/twitter/library/client/s;)Z

    goto :goto_0

    .line 94
    :pswitch_2
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-direct {v0, v6, v7}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v4, [Ljava/lang/String;

    const-string/jumbo v6, "messages:inbox::thread:mute_dm_thread_1w"

    aput-object v6, v3, v8

    .line 95
    invoke-virtual {v0, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 94
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 96
    new-instance v6, Lcom/twitter/library/api/dm/u;

    iget-object v9, p0, Lcom/twitter/app/dm/dialog/MuteConversationDialog;->a:Ljava/lang/String;

    move-object v7, v1

    move-object v8, v2

    move v10, v4

    invoke-direct/range {v6 .. v11}, Lcom/twitter/library/api/dm/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;ZI)V

    invoke-virtual {v12, v6, v5, p0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;ILcom/twitter/library/client/s;)Z

    goto :goto_0

    .line 76
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 142
    if-eqz p1, :cond_0

    .line 143
    const-string/jumbo v0, "conversation_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/dialog/MuteConversationDialog;->a:Ljava/lang/String;

    .line 144
    const-string/jumbo v0, "scribe_section"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/dialog/MuteConversationDialog;->c:Ljava/lang/String;

    .line 146
    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/widget/PromptDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 151
    invoke-super {p0, p1}, Lcom/twitter/android/widget/PromptDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 152
    const-string/jumbo v0, "conversation_id"

    iget-object v1, p0, Lcom/twitter/app/dm/dialog/MuteConversationDialog;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const-string/jumbo v0, "scribe_section"

    iget-object v1, p0, Lcom/twitter/app/dm/dialog/MuteConversationDialog;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    return-void
.end method
