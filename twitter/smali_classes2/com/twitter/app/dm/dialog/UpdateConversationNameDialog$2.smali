.class Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/dialog/b$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;->e()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog$2;->a:Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;II)V
    .locals 7

    .prologue
    .line 62
    const/4 v0, -0x1

    if-eq p3, v0, :cond_1

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog$2;->a:Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;

    invoke-virtual {v0}, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const v1, 0x7f1302ee

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 67
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 68
    iget-object v1, p0, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog$2;->a:Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;

    invoke-static {v1, v0}, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;->a(Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog$2;->a:Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;

    invoke-static {v1}, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;->a(Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 70
    iget-object v1, p0, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog$2;->a:Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;

    invoke-virtual {v1}, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 71
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 72
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "messages:thread::edit_name:save"

    aput-object v6, v4, v5

    .line 73
    invoke-virtual {v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v3

    .line 72
    invoke-static {v3}, Lcpm;->a(Lcpk;)V

    .line 74
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v3

    new-instance v4, Lcom/twitter/library/api/dm/v;

    iget-object v5, p0, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog$2;->a:Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;

    .line 75
    invoke-static {v5}, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;->b(Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v1, v2, v5, v0}, Lcom/twitter/library/api/dm/v;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog$2$1;

    invoke-direct {v0, p0, v1}, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog$2$1;-><init>(Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog$2;Landroid/content/Context;)V

    .line 74
    invoke-virtual {v3, v4, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    goto :goto_0
.end method
