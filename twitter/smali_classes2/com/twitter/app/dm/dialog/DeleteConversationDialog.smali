.class public Lcom/twitter/app/dm/dialog/DeleteConversationDialog;
.super Lcom/twitter/android/widget/PromptDialogFragment;
.source "Twttr"


# instance fields
.field private a:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/twitter/android/widget/PromptDialogFragment;-><init>()V

    return-void
.end method

.method private static a(IZ)Lcom/twitter/app/dm/dialog/DeleteConversationDialog;
    .locals 3

    .prologue
    .line 52
    if-eqz p1, :cond_0

    .line 53
    const v0, 0x7f0a0542

    move v1, v0

    .line 58
    :goto_0
    new-instance v0, Lcom/twitter/app/dm/dialog/d$b;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/dialog/d$b;-><init>(I)V

    const v2, 0x7f0a0547

    .line 59
    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/dialog/d$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/dialog/d$b;

    const v2, 0x7f0a054a

    .line 60
    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/dialog/d$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/dialog/d$b;

    .line 61
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/dialog/d$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/dialog/d$b;

    const v1, 0x7f0a00f6

    .line 62
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/dialog/d$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/dialog/d$b;

    .line 63
    invoke-virtual {v0}, Lcom/twitter/app/dm/dialog/d$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/dialog/DeleteConversationDialog;

    .line 58
    return-object v0

    .line 55
    :cond_0
    const v0, 0x7f0a0541

    move v1, v0

    goto :goto_0
.end method

.method public static a(IZLjava/lang/String;Ljava/lang/String;)Lcom/twitter/app/dm/dialog/DeleteConversationDialog;
    .locals 1

    .prologue
    .line 38
    invoke-static {p0, p1}, Lcom/twitter/app/dm/dialog/DeleteConversationDialog;->a(IZ)Lcom/twitter/app/dm/dialog/DeleteConversationDialog;

    move-result-object v0

    .line 39
    invoke-direct {v0, p2, p3, p1}, Lcom/twitter/app/dm/dialog/DeleteConversationDialog;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 40
    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 44
    invoke-static {p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/dialog/DeleteConversationDialog;->a:Ljava/lang/String;

    .line 45
    iput-object p2, p0, Lcom/twitter/app/dm/dialog/DeleteConversationDialog;->c:Ljava/lang/String;

    .line 46
    iput-boolean p3, p0, Lcom/twitter/app/dm/dialog/DeleteConversationDialog;->d:Z

    .line 47
    return-void
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 67
    const/4 v0, -0x1

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 74
    invoke-static {p2}, Lcom/twitter/app/dm/dialog/DeleteConversationDialog;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {p0}, Lcom/twitter/app/dm/dialog/DeleteConversationDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 76
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    .line 77
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v2

    .line 79
    new-instance v3, Lcom/twitter/library/api/dm/l;

    iget-object v4, p0, Lcom/twitter/app/dm/dialog/DeleteConversationDialog;->a:Ljava/lang/String;

    invoke-direct {v3, v0, v1, v4, v5}, Lcom/twitter/library/api/dm/l;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Z)V

    new-instance v4, Lcom/twitter/app/dm/dialog/DeleteConversationDialog$1;

    invoke-direct {v4, p0, v0}, Lcom/twitter/app/dm/dialog/DeleteConversationDialog$1;-><init>(Lcom/twitter/app/dm/dialog/DeleteConversationDialog;Landroid/content/Context;)V

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 90
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 91
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v2, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const-string/jumbo v0, "messages"

    aput-object v0, v1, v5

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/twitter/app/dm/dialog/DeleteConversationDialog;->c:Ljava/lang/String;

    aput-object v3, v1, v0

    const/4 v0, 0x2

    const/4 v3, 0x0

    aput-object v3, v1, v0

    const/4 v0, 0x3

    const-string/jumbo v3, "thread"

    aput-object v3, v1, v0

    const/4 v3, 0x4

    iget-boolean v0, p0, Lcom/twitter/app/dm/dialog/DeleteConversationDialog;->d:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "leave_group"

    :goto_0
    aput-object v0, v1, v3

    invoke-virtual {v2, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 94
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/twitter/android/widget/PromptDialogFragment;->onClick(Landroid/content/DialogInterface;I)V

    .line 95
    return-void

    .line 91
    :cond_1
    const-string/jumbo v0, "delete_thread"

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 100
    if-eqz p1, :cond_0

    .line 101
    const-string/jumbo v0, "conversation_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/dialog/DeleteConversationDialog;->a:Ljava/lang/String;

    .line 102
    const-string/jumbo v0, "scribe_section"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/dialog/DeleteConversationDialog;->c:Ljava/lang/String;

    .line 103
    const-string/jumbo v0, "is_group"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/dialog/DeleteConversationDialog;->d:Z

    .line 105
    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/widget/PromptDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 110
    invoke-super {p0, p1}, Lcom/twitter/android/widget/PromptDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 111
    const-string/jumbo v0, "conversation_id"

    iget-object v1, p0, Lcom/twitter/app/dm/dialog/DeleteConversationDialog;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string/jumbo v0, "scribe_section"

    iget-object v1, p0, Lcom/twitter/app/dm/dialog/DeleteConversationDialog;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const-string/jumbo v0, "is_group"

    iget-boolean v1, p0, Lcom/twitter/app/dm/dialog/DeleteConversationDialog;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 114
    return-void
.end method
