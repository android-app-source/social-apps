.class public Lcom/twitter/app/dm/dialog/DMMessageDialog;
.super Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/twitter/app/dm/dialog/BaseDMMessageDialog;-><init>()V

    return-void
.end method

.method public static a(ILcom/twitter/model/dms/e;Lcom/twitter/app/dm/dialog/a;)Lcom/twitter/app/dm/dialog/DMMessageDialog;
    .locals 2

    .prologue
    .line 19
    invoke-static {p1}, Lcom/twitter/app/dm/dialog/DMMessageDialog;->b(Lcom/twitter/model/dms/e;)[I

    move-result-object v0

    .line 20
    invoke-static {p0, v0}, Lcom/twitter/app/dm/dialog/DMMessageDialog;->a(I[I)Lcom/twitter/app/dm/dialog/DMMessageDialog;

    move-result-object v1

    .line 21
    invoke-virtual {v1, p1, v0, p2}, Lcom/twitter/app/dm/dialog/DMMessageDialog;->a(Lcom/twitter/model/dms/e;[ILcom/twitter/app/dm/dialog/a;)V

    .line 22
    return-object v1
.end method

.method private static a(I[I)Lcom/twitter/app/dm/dialog/DMMessageDialog;
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/twitter/app/dm/dialog/b$a;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/dialog/b$a;-><init>(I)V

    .line 28
    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/dialog/b$a;->a([I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 29
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/dialog/DMMessageDialog;

    .line 27
    return-object v0
.end method

.method static b(Lcom/twitter/model/dms/e;)[I
    .locals 7

    .prologue
    const/4 v1, 0x0

    const v6, 0x7f0a0263

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 34
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 35
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0a021e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move-result-object v2

    .line 36
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, p0

    .line 37
    check-cast v0, Lcom/twitter/model/dms/r;

    invoke-virtual {v0}, Lcom/twitter/model/dms/r;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 38
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a00f8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :cond_0
    invoke-virtual {v2, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 49
    :goto_1
    invoke-virtual {v2}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->d(Ljava/util/Collection;)[I

    move-result-object v0

    return-object v0

    :cond_1
    move-object v0, v1

    .line 35
    goto :goto_0

    .line 40
    :cond_2
    new-array v0, v5, [Ljava/lang/Integer;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    const v1, 0x7f0a0779

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->b([Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_1

    .line 43
    :cond_3
    const-string/jumbo v0, "dm_forwarding_enabled"

    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 44
    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->z()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->s()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/twitter/model/dms/e;->E()Z

    move-result v0

    if-nez v0, :cond_5

    .line 45
    :cond_4
    const v0, 0x7f0a03b3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 47
    :cond_5
    new-array v0, v5, [Ljava/lang/Integer;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    const v1, 0x7f0a029d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/h;->b([Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_1
.end method


# virtual methods
.method protected a(Lcom/twitter/model/dms/e;)V
    .locals 2

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/twitter/app/dm/dialog/DMMessageDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/model/dms/e;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/util/af;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 57
    return-void
.end method
