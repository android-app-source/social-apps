.class public Lcom/twitter/app/dm/DMDirectShareChooser;
.super Landroid/service/chooser/ChooserTargetService;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x17
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/service/chooser/ChooserTargetService;-><init>()V

    return-void
.end method

.method private a()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMDirectShareChooser;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02036d

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 153
    if-nez p1, :cond_0

    .line 154
    const/4 v0, 0x0

    .line 166
    :goto_0
    return-object v0

    .line 156
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 157
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 159
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, p1}, Landroid/support/v4/graphics/drawable/RoundedBitmapDrawableFactory;->create(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)Landroid/support/v4/graphics/drawable/RoundedBitmapDrawable;

    move-result-object v2

    .line 160
    invoke-virtual {v2, v4}, Landroid/support/v4/graphics/drawable/RoundedBitmapDrawable;->setCircular(Z)V

    .line 161
    invoke-virtual {v2, v4}, Landroid/support/v4/graphics/drawable/RoundedBitmapDrawable;->setAntiAlias(Z)V

    .line 162
    invoke-virtual {v2, v3, v3, v0, v1}, Landroid/support/v4/graphics/drawable/RoundedBitmapDrawable;->setBounds(IIII)V

    .line 163
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 164
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 165
    invoke-virtual {v2, v1}, Landroid/support/v4/graphics/drawable/RoundedBitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method private a(Lcom/twitter/media/request/a$a;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMDirectShareChooser;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/media/manager/g;->a(Landroid/content/Context;)Lcom/twitter/library/media/manager/g;

    move-result-object v1

    .line 139
    invoke-virtual {v1, p1}, Lcom/twitter/library/media/manager/g;->b(Lcom/twitter/media/request/a$a;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 140
    if-eqz v0, :cond_0

    .line 147
    :goto_0
    return-object v0

    .line 143
    :cond_0
    invoke-virtual {v1, p1}, Lcom/twitter/library/media/manager/g;->d(Lcom/twitter/media/request/a$a;)Ljava/io/File;

    move-result-object v0

    .line 144
    if-eqz v0, :cond_1

    .line 145
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 147
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 121
    const/4 v0, -0x3

    .line 122
    invoke-static {p1, v0}, Lcom/twitter/media/manager/UserImageRequest;->a(Ljava/lang/String;I)Lcom/twitter/media/request/a$a;

    move-result-object v0

    .line 123
    invoke-direct {p0, v0}, Lcom/twitter/app/dm/DMDirectShareChooser;->a(Lcom/twitter/media/request/a$a;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 124
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMDirectShareChooser;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/twitter/app/dm/DMDirectShareChooser;->a(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 129
    invoke-static {p1}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/dm/DMGroupAvatarImageVariant;->e:Lcom/twitter/media/request/a$c;

    .line 130
    invoke-virtual {v0, v1}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/media/request/a$c;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    .line 131
    invoke-static {p2, p3}, Lcom/twitter/util/math/Size;->a(II)Lcom/twitter/util/math/Size;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/util/math/Size;)Lcom/twitter/media/request/a$a;

    move-result-object v0

    .line 132
    invoke-direct {p0, v0}, Lcom/twitter/app/dm/DMDirectShareChooser;->a(Lcom/twitter/media/request/a$a;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 133
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMDirectShareChooser;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/twitter/app/dm/DMDirectShareChooser;->a(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private b()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMDirectShareChooser;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02036e

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private static b(J)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/provider/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    const/4 v0, 0x4

    invoke-static {p0, p1, v0}, Lcom/twitter/android/provider/d;->a(JI)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(J)Ljava/util/List;
    .locals 17
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Landroid/service/chooser/ChooserTarget;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    new-instance v6, Landroid/content/ComponentName;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/dm/DMDirectShareChooser;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/twitter/app/dm/DMActivity;

    .line 77
    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v6, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const/4 v2, 0x4

    invoke-static {v2}, Lcom/twitter/util/collection/h;->a(I)Lcom/twitter/util/collection/h;

    move-result-object v8

    .line 79
    invoke-direct/range {p0 .. p0}, Lcom/twitter/app/dm/DMDirectShareChooser;->b()Landroid/graphics/Bitmap;

    move-result-object v9

    .line 80
    invoke-direct/range {p0 .. p0}, Lcom/twitter/app/dm/DMDirectShareChooser;->a()Landroid/graphics/Bitmap;

    move-result-object v10

    .line 81
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    .line 82
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    .line 83
    const v5, 0x3f7d70a4    # 0.99f

    .line 84
    invoke-static/range {p1 .. p2}, Lcom/twitter/app/dm/DMDirectShareChooser;->b(J)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/provider/c;

    .line 85
    new-instance v7, Lcom/twitter/app/dm/j$a;

    invoke-direct {v7}, Lcom/twitter/app/dm/j$a;-><init>()V

    .line 88
    instance-of v3, v2, Lcom/twitter/library/provider/d;

    if-eqz v3, :cond_0

    .line 89
    check-cast v2, Lcom/twitter/library/provider/d;

    iget-object v2, v2, Lcom/twitter/library/provider/d;->b:Lcom/twitter/model/core/TwitterUser;

    .line 90
    iget-wide v14, v2, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-virtual {v7, v14, v15}, Lcom/twitter/app/dm/j$a;->a(J)Lcom/twitter/app/dm/j$a;

    .line 91
    iget-object v3, v2, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    .line 92
    iget-object v2, v2, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    .line 93
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/twitter/app/dm/DMDirectShareChooser;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 92
    invoke-static {v2, v9}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    .line 104
    :goto_1
    invoke-static {v2}, Landroid/graphics/drawable/Icon;->createWithBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/drawable/Icon;

    move-result-object v4

    .line 105
    const/4 v2, 0x1

    invoke-virtual {v7, v2}, Lcom/twitter/app/dm/j$a;->g(Z)Lcom/twitter/app/dm/j$a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/app/dm/j$a;->e()Lcom/twitter/app/dm/j;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/app/dm/j;->n()Landroid/os/Bundle;

    move-result-object v7

    .line 107
    new-instance v2, Landroid/service/chooser/ChooserTarget;

    invoke-direct/range {v2 .. v7}, Landroid/service/chooser/ChooserTarget;-><init>(Ljava/lang/CharSequence;Landroid/graphics/drawable/Icon;FLandroid/content/ComponentName;Landroid/os/Bundle;)V

    invoke-virtual {v8, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 108
    const v2, 0x3d4ccccd    # 0.05f

    sub-float/2addr v5, v2

    .line 109
    goto :goto_0

    .line 95
    :cond_0
    invoke-virtual {v2}, Lcom/twitter/library/provider/c;->c()Lcom/twitter/model/dms/q;

    move-result-object v2

    .line 96
    iget-object v3, v2, Lcom/twitter/model/dms/q;->b:Ljava/lang/String;

    invoke-virtual {v7, v3}, Lcom/twitter/app/dm/j$a;->c(Ljava/lang/String;)Lcom/twitter/app/dm/j$a;

    .line 97
    new-instance v3, Lcom/twitter/library/dm/b;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/dm/DMDirectShareChooser;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    move-wide/from16 v0, p1

    invoke-direct {v3, v2, v4, v0, v1}, Lcom/twitter/library/dm/b;-><init>(Lcom/twitter/model/dms/q;Landroid/content/Context;J)V

    invoke-virtual {v3}, Lcom/twitter/library/dm/b;->a()Ljava/lang/String;

    move-result-object v3

    .line 98
    iget-object v2, v2, Lcom/twitter/model/dms/q;->e:Ljava/lang/String;

    .line 99
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v11, v12}, Lcom/twitter/app/dm/DMDirectShareChooser;->a(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 98
    invoke-static {v2, v10}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    goto :goto_1

    .line 110
    :cond_1
    invoke-virtual {v8}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    return-object v2
.end method

.method public onGetChooserTargets(Landroid/content/ComponentName;Landroid/content/IntentFilter;)Ljava/util/List;
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ComponentName;",
            "Landroid/content/IntentFilter;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/service/chooser/ChooserTarget;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_0

    .line 65
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    .line 70
    :goto_0
    return-object v0

    .line 67
    :cond_0
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 68
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v2, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "messages:direct_share_service:::impression"

    aput-object v5, v3, v4

    .line 69
    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v2

    .line 68
    invoke-static {v2}, Lcpm;->a(Lcpk;)V

    .line 70
    invoke-virtual {p0, v0, v1}, Lcom/twitter/app/dm/DMDirectShareChooser;->a(J)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
