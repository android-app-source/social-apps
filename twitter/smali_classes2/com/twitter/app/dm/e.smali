.class public Lcom/twitter/app/dm/e;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Landroid/content/Context;)Landroid/view/animation/LayoutAnimationController;
    .locals 4

    .prologue
    .line 65
    const v0, 0x7f05002f

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 66
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 67
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 68
    new-instance v1, Landroid/view/animation/LayoutAnimationController;

    const v2, 0x3e19999a    # 0.15f

    invoke-direct {v1, v0, v2}, Landroid/view/animation/LayoutAnimationController;-><init>(Landroid/view/animation/Animation;F)V

    return-object v1
.end method

.method public static a(Landroid/view/View;)V
    .locals 5

    .prologue
    const v4, 0x3f8147ae    # 1.01f

    .line 72
    invoke-virtual {p0}, Landroid/view/View;->getPivotX()F

    move-result v0

    .line 73
    const/16 v1, 0x12c

    new-instance v2, Landroid/view/animation/AnticipateOvershootInterpolator;

    const/high16 v3, 0x437a0000    # 250.0f

    invoke-direct {v2, v3}, Landroid/view/animation/AnticipateOvershootInterpolator;-><init>(F)V

    invoke-static {p0, v4, v4, v1, v2}, Lcom/twitter/util/e;->a(Landroid/view/View;FFILandroid/view/animation/Interpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Lcom/twitter/app/dm/e$2;

    invoke-direct {v2, p0, v0}, Lcom/twitter/app/dm/e$2;-><init>(Landroid/view/View;F)V

    .line 74
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 88
    return-void
.end method

.method public static a(Landroid/view/View;I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 31
    const/high16 v1, 0x3f800000    # 1.0f

    new-instance v5, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    move-object v0, p0

    move v3, v2

    move v4, p1

    invoke-static/range {v0 .. v5}, Lcom/twitter/util/e;->a(Landroid/view/View;FIIILandroid/view/animation/Interpolator;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/dm/e$1;

    invoke-direct {v1}, Lcom/twitter/app/dm/e$1;-><init>()V

    .line 32
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setListener(Landroid/support/v4/view/ViewPropertyAnimatorListener;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->start()V

    .line 40
    return-void
.end method

.method public static a(Landroid/view/View;IFLcom/twitter/util/ui/c;)V
    .locals 6

    .prologue
    .line 98
    invoke-virtual {p0}, Landroid/view/View;->getX()F

    move-result v0

    .line 99
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 100
    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v2, v3

    const/4 v3, 0x1

    aput p2, v2, v3

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 101
    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 102
    int-to-long v4, p1

    invoke-virtual {v2, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 103
    new-instance v3, Lcom/twitter/app/dm/e$3;

    invoke-direct {v3, p0, v0, v1}, Lcom/twitter/app/dm/e$3;-><init>(Landroid/view/View;FI)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 112
    if-eqz p3, :cond_0

    .line 113
    invoke-virtual {v2, p3}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 115
    :cond_0
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 116
    return-void
.end method

.method public static a(Landroid/view/View;II)V
    .locals 3

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    new-instance v2, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-static {p0, p1, v0, v1, v2}, Lcom/twitter/app/dm/e;->a(Landroid/view/View;IIILandroid/view/animation/Interpolator;)V

    .line 44
    return-void
.end method

.method private static a(Landroid/view/View;IIILandroid/view/animation/Interpolator;)V
    .locals 6

    .prologue
    .line 52
    const/4 v1, 0x0

    move-object v0, p0

    move v2, p2

    move v3, p3

    move v4, p1

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lcom/twitter/util/e;->a(Landroid/view/View;FIIILandroid/view/animation/Interpolator;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->start()V

    .line 53
    return-void
.end method

.method public static b(Landroid/view/View;II)V
    .locals 3

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    mul-int/lit8 v1, v1, -0x1

    new-instance v2, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-static {p0, p1, v0, v1, v2}, Lcom/twitter/app/dm/e;->a(Landroid/view/View;IIILandroid/view/animation/Interpolator;)V

    .line 48
    return-void
.end method

.method public static c(Landroid/view/View;II)V
    .locals 3

    .prologue
    .line 56
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/4 v1, 0x0

    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-static {p0, p1, v0, v1, v2}, Lcom/twitter/app/dm/e;->a(Landroid/view/View;IIILandroid/view/animation/Interpolator;)V

    .line 57
    return-void
.end method

.method public static d(Landroid/view/View;II)V
    .locals 3

    .prologue
    .line 60
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    mul-int/lit8 v0, v0, -0x1

    const/4 v1, 0x0

    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-static {p0, p1, v0, v1, v2}, Lcom/twitter/app/dm/e;->a(Landroid/view/View;IIILandroid/view/animation/Interpolator;)V

    .line 61
    return-void
.end method
