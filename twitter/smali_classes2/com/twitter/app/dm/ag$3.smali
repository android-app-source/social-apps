.class Lcom/twitter/app/dm/ag$3;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/dm/ag;->a(JLyj;Lcom/twitter/media/ui/image/UserImageView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcqw",
        "<",
        "Lcom/twitter/util/collection/k",
        "<",
        "Lcom/twitter/model/core/TwitterUser;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/media/ui/image/UserImageView;

.field final synthetic b:Lcom/twitter/app/dm/ag;


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/ag;Lcom/twitter/media/ui/image/UserImageView;)V
    .locals 0

    .prologue
    .line 383
    iput-object p1, p0, Lcom/twitter/app/dm/ag$3;->b:Lcom/twitter/app/dm/ag;

    iput-object p2, p0, Lcom/twitter/app/dm/ag$3;->a:Lcom/twitter/media/ui/image/UserImageView;

    invoke-direct {p0}, Lcqw;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/util/collection/k;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 386
    invoke-virtual {p1}, Lcom/twitter/util/collection/k;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 387
    invoke-virtual {p1}, Lcom/twitter/util/collection/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 388
    iget-object v1, v0, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    .line 389
    iget-object v2, p0, Lcom/twitter/app/dm/ag$3;->b:Lcom/twitter/app/dm/ag;

    invoke-static {v2}, Lcom/twitter/app/dm/ag;->a(Lcom/twitter/app/dm/ag;)Lcom/twitter/app/dm/ag$a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/app/dm/ag$a;->b()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v2

    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 390
    iget-object v0, p0, Lcom/twitter/app/dm/ag$3;->a:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;)Z

    .line 392
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 383
    check-cast p1, Lcom/twitter/util/collection/k;

    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/ag$3;->a(Lcom/twitter/util/collection/k;)V

    return-void
.end method
