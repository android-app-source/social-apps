.class public Lcom/twitter/app/dm/widget/DMAvatar;
.super Landroid/widget/RelativeLayout;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/widget/DMAvatar$Location;
    }
.end annotation


# instance fields
.field private a:I

.field private b:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/app/dm/widget/DMAvatar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/app/dm/widget/DMAvatar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 65
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 67
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMAvatar;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/bi$a;->DMAvatar:[I

    invoke-virtual {v0, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 68
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/app/dm/widget/DMAvatar;->a:I

    .line 69
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 70
    return-void
.end method

.method private a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/media/ui/image/UserImageView;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 165
    new-instance v1, Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMAvatar;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/media/ui/image/UserImageView;-><init>(Landroid/content/Context;)V

    .line 167
    if-eqz p1, :cond_1

    .line 168
    invoke-virtual {v1, p1}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/model/core/TwitterUser;)Z

    .line 169
    invoke-virtual {p1}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v0

    .line 174
    :goto_0
    iget v2, p0, Lcom/twitter/app/dm/widget/DMAvatar;->a:I

    invoke-virtual {v1, v2}, Lcom/twitter/media/ui/image/UserImageView;->setSize(I)V

    .line 176
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 177
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMAvatar;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a041c

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 180
    :cond_0
    sget-object v0, Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;->c:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;->setRoundingStrategy(Lcom/twitter/media/ui/image/config/g;)V

    .line 181
    return-object v1

    .line 172
    :cond_1
    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/model/core/TwitterUser;)Z

    goto :goto_0
.end method

.method private a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/app/dm/widget/DMAvatar$Location;II)Lcom/twitter/media/ui/image/UserImageView;
    .locals 6

    .prologue
    const/4 v3, -0x2

    .line 187
    new-instance v1, Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMAvatar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;-><init>(Landroid/content/Context;)V

    .line 188
    invoke-virtual {v1, p1}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/model/core/TwitterUser;)Z

    .line 189
    invoke-virtual {v1, p3, p4}, Lcom/twitter/media/ui/image/UserImageView;->a(II)V

    .line 190
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 192
    iget-object v3, p2, Lcom/twitter/app/dm/widget/DMAvatar$Location;->layoutRules:[I

    array-length v4, v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    aget v5, v3, v0

    .line 193
    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 192
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 195
    :cond_0
    invoke-virtual {v1, v2}, Lcom/twitter/media/ui/image/UserImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 197
    invoke-virtual {p2}, Lcom/twitter/app/dm/widget/DMAvatar$Location;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 198
    sget-object v0, Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;->b:Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;->setScaleType(Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;)V

    .line 201
    :cond_1
    const/4 v0, 0x2

    invoke-static {v1, v0}, Lcom/twitter/util/ui/a;->a(Landroid/view/View;I)V

    .line 203
    iget-object v0, p2, Lcom/twitter/app/dm/widget/DMAvatar$Location;->roundingStrategy:Lcom/twitter/media/ui/image/config/g;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;->setRoundingStrategy(Lcom/twitter/media/ui/image/config/g;)V

    .line 204
    return-object v1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 100
    new-instance v0, Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMAvatar;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/twitter/media/ui/image/RichImageView;

    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMAvatar;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/twitter/media/ui/image/RichImageView;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1, v2, v4}, Lcom/twitter/media/ui/image/MediaImageView;-><init>(Landroid/content/Context;Landroid/widget/ImageView;Z)V

    .line 101
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    iget v2, p0, Lcom/twitter/app/dm/widget/DMAvatar;->a:I

    iget v3, p0, Lcom/twitter/app/dm/widget/DMAvatar;->a:I

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 102
    sget-object v1, Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;->b:Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setScaleType(Lcom/twitter/media/ui/image/BaseMediaImageView$ScaleType;)V

    .line 103
    sget-object v1, Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;->c:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setRoundingStrategy(Lcom/twitter/media/ui/image/config/g;)V

    .line 104
    invoke-static {p2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMAvatar;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a041c

    new-array v3, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 108
    :cond_0
    invoke-static {p1}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/dm/DMGroupAvatarImageVariant;->e:Lcom/twitter/media/request/a$c;

    .line 109
    invoke-virtual {v1, v2}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/media/request/a$c;)Lcom/twitter/media/request/a$a;

    move-result-object v1

    .line 108
    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 110
    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/widget/DMAvatar;->addView(Landroid/view/View;)V

    .line 111
    return-void
.end method

.method private a(Ljava/util/List;II)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 158
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    sget-object v1, Lcom/twitter/app/dm/widget/DMAvatar$Location;->c:Lcom/twitter/app/dm/widget/DMAvatar$Location;

    invoke-direct {p0, v0, v1, p2, p2}, Lcom/twitter/app/dm/widget/DMAvatar;->a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/app/dm/widget/DMAvatar$Location;II)Lcom/twitter/media/ui/image/UserImageView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/widget/DMAvatar;->addView(Landroid/view/View;)V

    .line 159
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    sget-object v1, Lcom/twitter/app/dm/widget/DMAvatar$Location;->d:Lcom/twitter/app/dm/widget/DMAvatar$Location;

    invoke-direct {p0, v0, v1, p2, p2}, Lcom/twitter/app/dm/widget/DMAvatar;->a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/app/dm/widget/DMAvatar$Location;II)Lcom/twitter/media/ui/image/UserImageView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/widget/DMAvatar;->addView(Landroid/view/View;)V

    .line 160
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    sget-object v1, Lcom/twitter/app/dm/widget/DMAvatar$Location;->a:Lcom/twitter/app/dm/widget/DMAvatar$Location;

    invoke-direct {p0, v0, v1, p2, p3}, Lcom/twitter/app/dm/widget/DMAvatar;->a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/app/dm/widget/DMAvatar$Location;II)Lcom/twitter/media/ui/image/UserImageView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/widget/DMAvatar;->addView(Landroid/view/View;)V

    .line 161
    return-void
.end method

.method private a(Ljava/util/List;ZLjava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 115
    if-eqz p2, :cond_3

    .line 117
    iget v0, p0, Lcom/twitter/app/dm/widget/DMAvatar;->a:I

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMAvatar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e01e1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int v1, v0, v1

    .line 119
    iget v2, p0, Lcom/twitter/app/dm/widget/DMAvatar;->a:I

    .line 121
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 122
    invoke-static {p1, v4, v5}, Lcom/twitter/library/dm/e;->a(Ljava/util/List;J)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x2

    if-le v0, v3, :cond_2

    .line 123
    invoke-direct {p0, p1, v1, v2}, Lcom/twitter/app/dm/widget/DMAvatar;->a(Ljava/util/List;II)V

    .line 135
    :cond_0
    :goto_0
    invoke-static {p3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMAvatar;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a041c

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/widget/DMAvatar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 145
    :cond_1
    :goto_1
    return-void

    .line 125
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 126
    if-lez v3, :cond_0

    .line 127
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    sget-object v4, Lcom/twitter/app/dm/widget/DMAvatar$Location;->a:Lcom/twitter/app/dm/widget/DMAvatar$Location;

    invoke-direct {p0, v0, v4, v1, v2}, Lcom/twitter/app/dm/widget/DMAvatar;->a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/app/dm/widget/DMAvatar$Location;II)Lcom/twitter/media/ui/image/UserImageView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/widget/DMAvatar;->addView(Landroid/view/View;)V

    .line 129
    if-le v3, v6, :cond_0

    .line 130
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    sget-object v3, Lcom/twitter/app/dm/widget/DMAvatar$Location;->b:Lcom/twitter/app/dm/widget/DMAvatar$Location;

    invoke-direct {p0, v0, v3, v1, v2}, Lcom/twitter/app/dm/widget/DMAvatar;->a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/app/dm/widget/DMAvatar$Location;II)Lcom/twitter/media/ui/image/UserImageView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/widget/DMAvatar;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 139
    :cond_3
    invoke-static {p1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/app/dm/widget/DMAvatar;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/media/ui/image/UserImageView;

    move-result-object v0

    .line 140
    iget-object v1, p0, Lcom/twitter/app/dm/widget/DMAvatar;->b:Landroid/view/View$OnClickListener;

    if-eqz v1, :cond_4

    .line 141
    iget-object v1, p0, Lcom/twitter/app/dm/widget/DMAvatar;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    :cond_4
    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/widget/DMAvatar;->addView(Landroid/view/View;)V

    goto :goto_1
.end method


# virtual methods
.method public setConversation(Lcom/twitter/model/dms/q;)V
    .locals 4

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMAvatar;->removeAllViews()V

    .line 89
    new-instance v0, Lcom/twitter/library/dm/b;

    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMAvatar;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 90
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/twitter/library/dm/b;-><init>(Lcom/twitter/model/dms/q;Landroid/content/Context;J)V

    .line 91
    invoke-virtual {v0}, Lcom/twitter/library/dm/b;->a()Ljava/lang/String;

    move-result-object v0

    .line 92
    iget-object v1, p1, Lcom/twitter/model/dms/q;->e:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 93
    iget-object v1, p1, Lcom/twitter/model/dms/q;->e:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/twitter/app/dm/widget/DMAvatar;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    :goto_0
    return-void

    .line 95
    :cond_0
    iget-object v1, p1, Lcom/twitter/model/dms/q;->i:Ljava/util/List;

    iget-boolean v2, p1, Lcom/twitter/model/dms/q;->h:Z

    invoke-direct {p0, v1, v2, v0}, Lcom/twitter/app/dm/widget/DMAvatar;->a(Ljava/util/List;ZLjava/lang/String;)V

    goto :goto_0
.end method

.method public setOneOnOneClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/twitter/app/dm/widget/DMAvatar;->b:Landroid/view/View$OnClickListener;

    .line 84
    return-void
.end method

.method public setSize(I)V
    .locals 1

    .prologue
    .line 73
    iput p1, p0, Lcom/twitter/app/dm/widget/DMAvatar;->a:I

    .line 74
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMAvatar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 75
    if-eqz v0, :cond_0

    .line 76
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 77
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 78
    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/widget/DMAvatar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 80
    :cond_0
    return-void
.end method
