.class Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/dm/widget/DMConversationMessageComposer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field final synthetic b:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

.field private final c:Landroid/view/View;

.field private final d:Landroid/view/View;

.field private final e:I

.field private final f:I

.field private final g:I

.field private h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private j:Z


# direct methods
.method private constructor <init>(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;)V
    .locals 2

    .prologue
    .line 625
    iput-object p1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 626
    const v0, 0x7f1304b3

    invoke-virtual {p1, v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->c:Landroid/view/View;

    .line 627
    const v0, 0x7f1304b6

    invoke-virtual {p1, v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->d:Landroid/view/View;

    .line 628
    iget-object v0, p1, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->getInputType()I

    move-result v0

    iput v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->e:I

    .line 629
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->f:I

    .line 630
    invoke-virtual {p1}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a06bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->i:Ljava/lang/String;

    .line 631
    iget-object v0, p1, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->getMaxLines()I

    move-result v0

    iput v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->g:I

    .line 632
    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;Lcom/twitter/app/dm/widget/DMConversationMessageComposer$1;)V
    .locals 0

    .prologue
    .line 610
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;-><init>(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 610
    iput-object p1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->h:Ljava/lang/String;

    return-object p1
.end method

.method private a(Lccu;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 635
    iput-boolean v3, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->j:Z

    .line 636
    invoke-virtual {p1}, Lccu;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->a:Ljava/lang/String;

    .line 637
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-static {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->f(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 638
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-static {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->g(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 640
    iget-object v0, p1, Lccu;->c:Lccs;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccs;

    .line 642
    iget-object v4, v0, Lccs;->e:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_0
    move v1, v2

    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 655
    :goto_1
    iget-object v1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual {v1}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->p()V

    .line 656
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->e()V

    .line 657
    new-instance v1, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c$1;

    invoke-direct {v1, p0, v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c$1;-><init>(Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;Lccs;)V

    invoke-direct {p0, v1}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->a(Lcom/twitter/util/ui/c;)V

    .line 672
    return-void

    .line 642
    :sswitch_0
    const-string/jumbo v3, "default"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :sswitch_1
    const-string/jumbo v1, "number"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v3

    goto :goto_0

    .line 644
    :pswitch_0
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->d()V

    goto :goto_1

    .line 648
    :pswitch_1
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->c()V

    goto :goto_1

    .line 642
    :sswitch_data_0
    .sparse-switch
        -0x3da724b7 -> :sswitch_1
        0x5c13d641 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;Z)V
    .locals 0

    .prologue
    .line 610
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->a(Z)V

    return-void
.end method

.method private a(Lcom/twitter/util/ui/c;)V
    .locals 3

    .prologue
    .line 741
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    .line 742
    iget-object v1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->d:Landroid/view/View;

    const/16 v2, 0x96

    .line 743
    invoke-static {v1, v2, v0, p1}, Lcom/twitter/app/dm/e;->a(Landroid/view/View;IFLcom/twitter/util/ui/c;)V

    .line 744
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 675
    iget-boolean v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->j:Z

    if-eqz v0, :cond_1

    .line 676
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    iget-object v0, v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    iget-object v1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setHintText(Ljava/lang/String;)V

    .line 677
    new-instance v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c$2;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c$2;-><init>(Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;)V

    invoke-direct {p0, v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b(Lcom/twitter/util/ui/c;)V

    .line 685
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->f()V

    .line 686
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-static {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->f(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 687
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-static {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->g(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 688
    if-eqz p1, :cond_0

    .line 689
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->a:Ljava/lang/String;

    .line 691
    :cond_0
    iput-boolean v2, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->j:Z

    .line 693
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;)Z
    .locals 1

    .prologue
    .line 610
    iget-boolean v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->j:Z

    return v0
.end method

.method static synthetic a(Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;Lccu;)Z
    .locals 1

    .prologue
    .line 610
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b(Lccu;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;)Landroid/view/View;
    .locals 1

    .prologue
    .line 610
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->d:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;Lccu;)V
    .locals 0

    .prologue
    .line 610
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->a(Lccu;)V

    return-void
.end method

.method private b(Lcom/twitter/util/ui/c;)V
    .locals 3

    .prologue
    .line 747
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    .line 748
    iget-object v1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->d:Landroid/view/View;

    const/16 v2, 0x96

    neg-float v0, v0

    .line 749
    invoke-static {v1, v2, v0, p1}, Lcom/twitter/app/dm/e;->a(Landroid/view/View;IFLcom/twitter/util/ui/c;)V

    .line 750
    return-void
.end method

.method private b(Lccu;)Z
    .locals 2

    .prologue
    .line 727
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 728
    invoke-virtual {p1}, Lccu;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/util/y;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 727
    :goto_0
    return v0

    .line 728
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;)I
    .locals 1

    .prologue
    .line 610
    iget v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->f:I

    return v0
.end method

.method private c()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 696
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    iget-object v0, v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->getInputType()I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 697
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    iget-object v0, v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setInputType(I)V

    .line 701
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    iget-object v0, v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setSingleLine(Z)V

    .line 704
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    iget-object v0, v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    new-instance v1, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c$3;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c$3;-><init>(Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setEditTextOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 716
    :cond_0
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 719
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    iget-object v0, v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->getInputType()I

    move-result v0

    iget v1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->e:I

    if-eq v0, v1, :cond_0

    .line 721
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    iget-object v0, v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    iget v1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->e:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setInputType(I)V

    .line 722
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    iget-object v0, v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setEditTextOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 724
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;)V
    .locals 0

    .prologue
    .line 610
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->d()V

    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 732
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->c:Landroid/view/View;

    const/16 v1, 0x96

    iget-object v2, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->c:Landroid/view/View;

    .line 733
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 732
    invoke-static {v0, v1, v2}, Lcom/twitter/app/dm/e;->d(Landroid/view/View;II)V

    .line 734
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 737
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->c:Landroid/view/View;

    const/16 v1, 0x96

    invoke-static {v0, v1}, Lcom/twitter/app/dm/e;->a(Landroid/view/View;I)V

    .line 738
    return-void
.end method


# virtual methods
.method a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 755
    iget-boolean v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->j:Z

    if-eqz v0, :cond_0

    .line 756
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    iget-object v0, v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0, v2}, Lcom/twitter/android/composer/TweetBox;->setHintText(Ljava/lang/String;)V

    .line 757
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    iget-object v0, v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    iget v1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->g:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setMaxLines(I)V

    .line 758
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    iget-object v0, v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0, v2}, Lcom/twitter/android/composer/TweetBox;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 760
    :cond_0
    return-void
.end method

.method b()V
    .locals 2

    .prologue
    .line 763
    iget-boolean v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->j:Z

    if-eqz v0, :cond_0

    .line 764
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    iget-object v0, v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setMaxLines(I)V

    .line 765
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    iget-object v0, v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 766
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    iget-object v0, v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    iget-object v1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setHintText(Ljava/lang/String;)V

    .line 768
    :cond_0
    return-void
.end method
