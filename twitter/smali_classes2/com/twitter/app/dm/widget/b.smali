.class public abstract Lcom/twitter/app/dm/widget/b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/widget/b$a;
    }
.end annotation


# instance fields
.field final a:Landroid/view/View;

.field final b:Landroid/support/design/widget/BottomSheetBehavior;

.field private final c:Landroid/view/View;

.field private final d:Landroid/view/animation/Animation;

.field private final e:Landroid/view/animation/Animation;

.field private final f:Landroid/view/animation/Animation;

.field private final g:Landroid/view/animation/Animation;

.field private h:Landroid/widget/ViewSwitcher;

.field private i:Lcom/twitter/internal/android/widget/ToolBar;

.field private j:Lcom/twitter/app/dm/widget/b$a;


# direct methods
.method protected constructor <init>(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/twitter/app/dm/widget/b;->c:Landroid/view/View;

    .line 50
    invoke-static {p1}, Landroid/support/design/widget/BottomSheetBehavior;->from(Landroid/view/View;)Landroid/support/design/widget/BottomSheetBehavior;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/widget/b;->b:Landroid/support/design/widget/BottomSheetBehavior;

    .line 51
    iput-object p2, p0, Lcom/twitter/app/dm/widget/b;->a:Landroid/view/View;

    .line 53
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 54
    const v1, 0x7f05005b

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/dm/widget/b;->d:Landroid/view/animation/Animation;

    .line 55
    const v1, 0x7f05005c

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/dm/widget/b;->e:Landroid/view/animation/Animation;

    .line 56
    const v1, 0x7f05005a

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/dm/widget/b;->f:Landroid/view/animation/Animation;

    .line 57
    const v1, 0x7f05005d

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/widget/b;->g:Landroid/view/animation/Animation;

    .line 58
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/widget/b;)Lcom/twitter/app/dm/widget/b$a;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b;->j:Lcom/twitter/app/dm/widget/b$a;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/app/dm/widget/b;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/b;->g()V

    return-void
.end method

.method static synthetic c(Lcom/twitter/app/dm/widget/b;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/b;->h()V

    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b;->h:Landroid/widget/ViewSwitcher;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/widget/b;->h:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/widget/b;->i:Lcom/twitter/internal/android/widget/ToolBar;

    if-eq v0, v1, :cond_0

    .line 145
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b;->h:Landroid/widget/ViewSwitcher;

    iget-object v1, p0, Lcom/twitter/app/dm/widget/b;->d:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 146
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b;->h:Landroid/widget/ViewSwitcher;

    iget-object v1, p0, Lcom/twitter/app/dm/widget/b;->g:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 147
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b;->h:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->showNext()V

    .line 149
    :cond_0
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b;->h:Landroid/widget/ViewSwitcher;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/widget/b;->h:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/widget/b;->i:Lcom/twitter/internal/android/widget/ToolBar;

    if-ne v0, v1, :cond_0

    .line 153
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b;->h:Landroid/widget/ViewSwitcher;

    iget-object v1, p0, Lcom/twitter/app/dm/widget/b;->f:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 154
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b;->h:Landroid/widget/ViewSwitcher;

    iget-object v1, p0, Lcom/twitter/app/dm/widget/b;->e:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 155
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b;->h:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->showNext()V

    .line 157
    :cond_0
    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 183
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b;->c:Landroid/view/View;

    .line 184
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;

    .line 185
    invoke-virtual {v0}, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;->getBehavior()Landroid/support/design/widget/CoordinatorLayout$Behavior;

    move-result-object v1

    if-nez v1, :cond_0

    .line 186
    iget-object v1, p0, Lcom/twitter/app/dm/widget/b;->b:Landroid/support/design/widget/BottomSheetBehavior;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;->setBehavior(Landroid/support/design/widget/CoordinatorLayout$Behavior;)V

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 190
    invoke-static {v0}, Lcom/twitter/util/d;->f(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 191
    iget-object v1, p0, Lcom/twitter/app/dm/widget/b;->b:Landroid/support/design/widget/BottomSheetBehavior;

    .line 192
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0e00f3

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 191
    invoke-virtual {v1, v0}, Landroid/support/design/widget/BottomSheetBehavior;->setPeekHeight(I)V

    .line 197
    :goto_0
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b;->b:Landroid/support/design/widget/BottomSheetBehavior;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomSheetBehavior;->setHideable(Z)V

    .line 198
    return-void

    .line 194
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b;->b:Landroid/support/design/widget/BottomSheetBehavior;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomSheetBehavior;->setPeekHeight(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/widget/ViewSwitcher;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/twitter/app/dm/widget/b;->h:Landroid/widget/ViewSwitcher;

    .line 122
    return-void
.end method

.method public a(Lcom/twitter/app/dm/widget/b$a;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/twitter/app/dm/widget/b;->j:Lcom/twitter/app/dm/widget/b$a;

    .line 71
    return-void
.end method

.method public a(Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 2

    .prologue
    .line 125
    iput-object p1, p0, Lcom/twitter/app/dm/widget/b;->i:Lcom/twitter/internal/android/widget/ToolBar;

    .line 126
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b;->i:Lcom/twitter/internal/android/widget/ToolBar;

    new-instance v1, Lcom/twitter/app/dm/widget/b$2;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/widget/b$2;-><init>(Lcom/twitter/app/dm/widget/b;)V

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setOnToolBarItemSelectedListener(Lcmr$a;)V

    .line 140
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b;->i:Lcom/twitter/internal/android/widget/ToolBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setVisibility(I)V

    .line 141
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b;->b:Landroid/support/design/widget/BottomSheetBehavior;

    invoke-virtual {v0}, Landroid/support/design/widget/BottomSheetBehavior;->getState()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b;->b:Landroid/support/design/widget/BottomSheetBehavior;

    invoke-virtual {v0}, Landroid/support/design/widget/BottomSheetBehavior;->getState()I

    move-result v0

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b;->b:Landroid/support/design/widget/BottomSheetBehavior;

    new-instance v1, Lcom/twitter/app/dm/widget/b$1;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/widget/b$1;-><init>(Lcom/twitter/app/dm/widget/b;)V

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomSheetBehavior;->setBottomSheetCallback(Landroid/support/design/widget/BottomSheetBehavior$BottomSheetCallback;)V

    .line 118
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/b;->i()V

    .line 161
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b;->b:Landroid/support/design/widget/BottomSheetBehavior;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomSheetBehavior;->setState(I)V

    .line 162
    return-void
.end method

.method public e()V
    .locals 2
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/b;->i()V

    .line 167
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b;->b:Landroid/support/design/widget/BottomSheetBehavior;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomSheetBehavior;->setState(I)V

    .line 168
    return-void
.end method

.method public f()V
    .locals 2
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 175
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/b;->i()V

    .line 176
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b;->b:Landroid/support/design/widget/BottomSheetBehavior;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomSheetBehavior;->setState(I)V

    .line 177
    return-void
.end method
