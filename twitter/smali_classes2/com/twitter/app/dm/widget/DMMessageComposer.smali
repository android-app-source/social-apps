.class public abstract Lcom/twitter/app/dm/widget/DMMessageComposer;
.super Landroid/widget/RelativeLayout;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/composer/TweetBox$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/widget/DMMessageComposer$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<",
        "L::Lcom/twitter/app/dm/widget/DMMessageComposer$a;",
        ">",
        "Landroid/widget/RelativeLayout;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/twitter/android/composer/TweetBox$b;"
    }
.end annotation


# instance fields
.field final a:Landroid/view/ViewGroup;

.field final b:Lcom/twitter/android/composer/TweetBox;

.field final c:Landroid/widget/Button;

.field final d:Z

.field final e:J

.field f:Lcom/twitter/app/dm/widget/DMMessageComposer$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "T",
            "L;"
        }
    .end annotation
.end field

.field final g:Landroid/content/Context;

.field private h:Lcom/twitter/app/dm/ah;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/app/dm/widget/DMMessageComposer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    iput-object p1, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->g:Landroid/content/Context;

    .line 52
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->e:J

    .line 53
    invoke-static {}, Lcom/twitter/library/dm/d;->f()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->d:Z

    .line 55
    const v0, 0x7f0400b1

    invoke-static {p1, v0, p0}, Lcom/twitter/app/dm/widget/DMMessageComposer;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 56
    const v0, 0x7f1304af

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/widget/DMMessageComposer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->a:Landroid/view/ViewGroup;

    .line 58
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->a:Landroid/view/ViewGroup;

    const v1, 0x7f1304b7

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/composer/TweetBox;

    iput-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    .line 59
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0, p0}, Lcom/twitter/android/composer/TweetBox;->setTweetBoxListener(Lcom/twitter/android/composer/TweetBox$b;)V

    .line 60
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setMaxChars(I)V

    .line 61
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMMessageComposer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a06ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setImeActionLabel(Ljava/lang/CharSequence;)V

    .line 62
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setAttachmentsUseSecureUrls(Z)V

    .line 63
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setSession(Lcom/twitter/library/client/Session;)V

    .line 65
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->a:Landroid/view/ViewGroup;

    const v1, 0x7f1302e6

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->c:Landroid/widget/Button;

    .line 66
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->c:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMMessageComposer;->p()V

    .line 139
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->h:Lcom/twitter/app/dm/ah;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->h:Lcom/twitter/app/dm/ah;

    invoke-interface {v0}, Lcom/twitter/app/dm/ah;->G()V

    .line 143
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 156
    return-void
.end method

.method public a(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->f:Lcom/twitter/app/dm/widget/DMMessageComposer$a;

    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMMessageComposer;->getMessageText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/app/dm/widget/DMMessageComposer$a;->c(Ljava/lang/String;)V

    .line 152
    return-void
.end method

.method public bk_()V
    .locals 0

    .prologue
    .line 147
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 160
    return-void
.end method

.method public getMessageText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->getText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->a(Z)V

    .line 84
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 103
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 104
    const v1, 0x7f1302e6

    if-ne v0, v1, :cond_0

    .line 105
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->getText()Ljava/lang/String;

    move-result-object v0

    .line 106
    iget-boolean v1, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->d:Z

    if-eqz v1, :cond_1

    const-string/jumbo v1, "/shrug"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 107
    iget-object v1, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->f:Lcom/twitter/app/dm/widget/DMMessageComposer$a;

    const-string/jumbo v2, "/shrug"

    const-string/jumbo v3, "\u00af\\\\_(\u30c4)_/\u00af"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/twitter/app/dm/widget/DMMessageComposer$a;->c(Ljava/lang/String;)V

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 109
    :cond_1
    iget-object v1, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->f:Lcom/twitter/app/dm/widget/DMMessageComposer$a;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/twitter/app/dm/widget/DMMessageComposer$a;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method abstract p()V
.end method

.method public s()V
    .locals 3

    .prologue
    .line 75
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    const-string/jumbo v1, ""

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/composer/TweetBox;->a(Ljava/lang/String;[I)V

    .line 76
    return-void
.end method

.method public setListener(Lcom/twitter/app/dm/widget/DMMessageComposer$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(T",
            "L;",
            ")V"
        }
    .end annotation

    .prologue
    .line 123
    iput-object p1, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->f:Lcom/twitter/app/dm/widget/DMMessageComposer$a;

    .line 124
    return-void
.end method

.method public setTypingEventProducer(Lcom/twitter/app/dm/ah;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->h:Lcom/twitter/app/dm/ah;

    .line 128
    return-void
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->c()Z

    move-result v0

    return v0
.end method

.method public u()V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->a(Z)V

    .line 88
    return-void
.end method

.method public v()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->f()V

    .line 92
    return-void
.end method

.method public w()V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->g()V

    .line 96
    return-void
.end method

.method public x()V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->a:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 116
    return-void
.end method

.method public y()V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMMessageComposer;->a:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 120
    return-void
.end method
