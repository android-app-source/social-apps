.class public Lcom/twitter/app/dm/widget/DMConversationMessageComposer;
.super Lcom/twitter/app/dm/widget/DMMessageComposer;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/widget/InlineComposerMediaScrollView$a;
.implements Lcom/twitter/app/dm/DMEmojiComposeView$a;
.implements Lcom/twitter/app/dm/n$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/widget/DMConversationMessageComposer$d;,
        Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;,
        Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;,
        Lcom/twitter/app/dm/widget/DMConversationMessageComposer$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/dm/widget/DMMessageComposer",
        "<",
        "Lcom/twitter/app/dm/widget/DMConversationMessageComposer$d;",
        ">;",
        "Lcom/twitter/android/media/widget/InlineComposerMediaScrollView$a;",
        "Lcom/twitter/app/dm/DMEmojiComposeView$a;",
        "Lcom/twitter/app/dm/n$b;"
    }
.end annotation


# instance fields
.field private final h:Lcom/twitter/app/dm/DMEmojiComposeView;

.field private final i:Landroid/view/View;

.field private final j:Landroid/view/View;

.field private final k:Landroid/widget/ImageView;

.field private final l:Landroid/view/View;

.field private final m:Lcom/twitter/android/media/widget/InlineComposerMediaLayout;

.field private final n:Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;

.field private final o:Lcom/twitter/android/media/widget/AttachmentMediaView;

.field private final p:Lcom/twitter/android/card/j;

.field private final q:Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;

.field private final r:Z

.field private s:Lapk;

.field private final t:Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;

.field private u:Z

.field private v:Z

.field private final w:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 91
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 94
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/app/dm/widget/DMMessageComposer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 96
    invoke-static {}, Lcom/twitter/library/dm/d;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 97
    const v0, 0x7f1301d4

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/card/CardPreviewView;

    .line 98
    invoke-static {}, Lcom/twitter/android/card/k;->a()Lcom/twitter/android/card/k;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    sget-object v4, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->f:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/card/k;->a(Landroid/app/Activity;Lcom/twitter/android/card/i;Lcax;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Z)Lcom/twitter/android/card/j;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->p:Lcom/twitter/android/card/j;

    .line 100
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->p:Lcom/twitter/android/card/j;

    invoke-virtual {v2, v0}, Lcom/twitter/android/card/CardPreviewView;->setController(Lcom/twitter/android/card/j;)V

    .line 101
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->p:Lcom/twitter/android/card/j;

    new-instance v1, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$b;

    invoke-direct {v1, p0, v3}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$b;-><init>(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;Lcom/twitter/app/dm/widget/DMConversationMessageComposer$1;)V

    invoke-interface {v0, v1}, Lcom/twitter/android/card/j;->a(Lcom/twitter/android/card/j$a;)V

    .line 102
    new-instance v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;

    .line 103
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, p0, p1, v4, v5}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;-><init>(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;Landroid/content/Context;J)V

    iput-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->q:Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;

    .line 109
    :goto_0
    invoke-static {}, Lcom/twitter/library/dm/d;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;

    invoke-direct {v0, p0, v3}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;-><init>(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;Lcom/twitter/app/dm/widget/DMConversationMessageComposer$1;)V

    :goto_1
    iput-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->t:Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;

    .line 112
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a06bf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setHintText(Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    new-instance v1, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$1;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$1;-><init>(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/TweetBox;->setTextWatcher(Landroid/text/TextWatcher;)V

    .line 126
    invoke-static {p1}, Lcom/twitter/util/ui/k;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->w:Z

    .line 128
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->a:Landroid/view/ViewGroup;

    const v1, 0x7f1302e8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->i:Landroid/view/View;

    .line 129
    const v0, 0x7f1302ea

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/DMEmojiComposeView;

    iput-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->h:Lcom/twitter/app/dm/DMEmojiComposeView;

    .line 131
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->a:Landroid/view/ViewGroup;

    const v1, 0x7f1302e7

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->j:Landroid/view/View;

    .line 132
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->j:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->a:Landroid/view/ViewGroup;

    const v1, 0x7f1304b4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->k:Landroid/widget/ImageView;

    .line 135
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    invoke-static {}, Lcom/twitter/android/util/j;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->a:Landroid/view/ViewGroup;

    const v1, 0x7f1304b5

    .line 137
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    :cond_0
    iput-object v3, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->l:Landroid/view/View;

    .line 140
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->a:Landroid/view/ViewGroup;

    const v1, 0x7f1304b0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/InlineComposerMediaLayout;

    iput-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->m:Lcom/twitter/android/media/widget/InlineComposerMediaLayout;

    .line 141
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->m:Lcom/twitter/android/media/widget/InlineComposerMediaLayout;

    const v1, 0x7f1304b1

    .line 142
    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/InlineComposerMediaLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;

    iput-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->n:Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;

    .line 143
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->n:Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;

    invoke-virtual {v0, p0}, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->setActionListener(Lcom/twitter/android/media/widget/InlineComposerMediaScrollView$a;)V

    .line 144
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->n:Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;

    const v1, 0x7f130392

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/widget/AttachmentMediaView;

    iput-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->o:Lcom/twitter/android/media/widget/AttachmentMediaView;

    .line 146
    invoke-static {}, Lcom/twitter/library/dm/d;->m()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->r:Z

    .line 147
    return-void

    .line 105
    :cond_1
    iput-object v3, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->p:Lcom/twitter/android/card/j;

    .line 106
    iput-object v3, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->q:Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;

    goto/16 :goto_0

    :cond_2
    move-object v0, v3

    .line 109
    goto/16 :goto_1
.end method

.method private A()V
    .locals 0

    .prologue
    .line 520
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->h()V

    .line 521
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->j()V

    .line 522
    return-void
.end method

.method private B()Z
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->t:Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->t:Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;

    invoke-static {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->a(Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private C()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 559
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->B()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 560
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->f:Lcom/twitter/app/dm/widget/DMMessageComposer$a;

    check-cast v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$d;

    iget-object v1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->t:Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;

    iget-object v1, v1, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->a:Ljava/lang/String;

    .line 561
    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    .line 562
    invoke-virtual {v3}, Lcom/twitter/android/composer/TweetBox;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 560
    invoke-interface {v0, v1, v3}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->t:Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;

    invoke-static {v0, v2}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->a(Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;Z)V

    move v0, v2

    .line 566
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;)Lcom/twitter/android/card/j;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->p:Lcom/twitter/android/card/j;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;)Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->q:Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->A()V

    return-void
.end method

.method private c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 417
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "tombstone://card"

    .line 418
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 417
    :goto_0
    return v0

    .line 418
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Z)V
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->h:Lcom/twitter/app/dm/DMEmojiComposeView;

    invoke-virtual {v0, p0}, Lcom/twitter/app/dm/DMEmojiComposeView;->setListener(Lcom/twitter/app/dm/DMEmojiComposeView$a;)V

    .line 220
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$3;

    invoke-direct {v1, p0, p1}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$3;-><init>(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;Z)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 233
    iput-boolean p1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->u:Z

    .line 234
    return-void
.end method

.method static synthetic d(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;)Z
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->B()Z

    move-result v0

    return v0
.end method

.method static synthetic e(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->z()V

    return-void
.end method

.method private e(Z)V
    .locals 2

    .prologue
    .line 366
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->s:Lapk;

    if-eqz v0, :cond_0

    .line 367
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->s:Lapk;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lapk;->a(Z)V

    .line 368
    if-eqz p1, :cond_0

    .line 369
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->s:Lapk;

    invoke-virtual {v0}, Lapk;->c()V

    .line 372
    :cond_0
    return-void
.end method

.method static synthetic f(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;)Landroid/view/View;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->i:Landroid/view/View;

    return-object v0
.end method

.method private f(Z)V
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->t:Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;

    if-eqz v0, :cond_0

    .line 376
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->t:Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;

    invoke-static {v0, p1}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->a(Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;Z)V

    .line 378
    :cond_0
    return-void
.end method

.method static synthetic g(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;)Landroid/view/View;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->j:Landroid/view/View;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;)Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->v:Z

    return v0
.end method

.method private z()V
    .locals 0

    .prologue
    .line 515
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->g()V

    .line 516
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->i()V

    .line 517
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/media/selection/MediaAttachment;)Lcom/twitter/android/media/widget/AttachmentMediaView;
    .locals 2

    .prologue
    .line 476
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->m:Lcom/twitter/android/media/widget/InlineComposerMediaLayout;

    sget-object v1, Lcom/twitter/android/composer/ComposerType;->c:Lcom/twitter/android/composer/ComposerType;

    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/media/widget/InlineComposerMediaLayout;->a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/composer/ComposerType;)Lcom/twitter/android/media/widget/AttachmentMediaView;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 4

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->l()V

    .line 302
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->e:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:thread:dm_compose_bar:suggestions:cancel"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 304
    return-void
.end method

.method public a(Landroid/support/v4/app/FragmentManager;)V
    .locals 4

    .prologue
    .line 183
    invoke-static {}, Lcom/twitter/library/dm/d;->n()Z

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 184
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->g:Landroid/content/Context;

    const-string/jumbo v1, "fatigue_standalone_stickers_tooltip"

    iget-wide v2, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->e:J

    .line 185
    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/twitter/android/util/h;

    move-result-object v0

    .line 186
    invoke-virtual {v0}, Lcom/twitter/android/util/h;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 187
    iget-object v1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->g:Landroid/content/Context;

    const v2, 0x7f1302e8

    invoke-static {v1, v2}, Lcom/twitter/ui/widget/Tooltip;->a(Landroid/content/Context;I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v1

    const v2, 0x7f0a02ef

    .line 188
    invoke-virtual {v1, v2}, Lcom/twitter/ui/widget/Tooltip$a;->a(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v1

    const/4 v2, 0x0

    .line 189
    invoke-virtual {v1, v2}, Lcom/twitter/ui/widget/Tooltip$a;->c(I)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v1

    new-instance v2, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$2;

    invoke-direct {v2, p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$2;-><init>(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;)V

    .line 190
    invoke-virtual {v1, v2}, Lcom/twitter/ui/widget/Tooltip$a;->a(Lcom/twitter/ui/widget/Tooltip$c;)Lcom/twitter/ui/widget/Tooltip$a;

    move-result-object v1

    const-string/jumbo v2, "tag_standalone_stickers_tooltip"

    .line 204
    invoke-virtual {v1, p1, v2}, Lcom/twitter/ui/widget/Tooltip$a;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)Lcom/twitter/ui/widget/Tooltip;

    .line 205
    invoke-virtual {v0}, Lcom/twitter/android/util/h;->b()V

    .line 207
    :cond_0
    return-void
.end method

.method public a(Lcck;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 328
    invoke-virtual {p1}, Lcck;->a()Ljava/lang/String;

    move-result-object v3

    const/4 v2, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v2, :pswitch_data_0

    .line 358
    :cond_1
    :goto_1
    return-void

    .line 328
    :sswitch_0
    const-string/jumbo v4, "options"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v2, v1

    goto :goto_0

    :sswitch_1
    const-string/jumbo v4, "text_input"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v2, v0

    goto :goto_0

    .line 330
    :pswitch_0
    iget-object v2, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->s:Lapk;

    if-eqz v2, :cond_1

    .line 331
    invoke-direct {p0, v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->f(Z)V

    .line 332
    check-cast p1, Lccr;

    .line 333
    iget-object v2, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->s:Lapk;

    invoke-virtual {v2, p1}, Lapk;->b(Lccr;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 334
    :goto_2
    if-eqz v0, :cond_5

    .line 335
    iget-boolean v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->v:Z

    if-eqz v0, :cond_2

    .line 336
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->u()V

    .line 338
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->s:Lapk;

    invoke-virtual {v0, p1}, Lapk;->a(Lccr;)V

    .line 342
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->f:Lcom/twitter/app/dm/widget/DMMessageComposer$a;

    check-cast v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$d;

    invoke-interface {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$d;->k()V

    goto :goto_1

    :cond_4
    move v0, v1

    .line 333
    goto :goto_2

    .line 339
    :cond_5
    iget-boolean v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->v:Z

    if-nez v0, :cond_3

    .line 340
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->s:Lapk;

    invoke-virtual {v0, p1}, Lapk;->a(Lccr;)V

    goto :goto_3

    .line 347
    :pswitch_1
    check-cast p1, Lccu;

    .line 348
    iget-object v1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->t:Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->t:Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;

    invoke-static {v1, p1}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->a(Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;Lccu;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 349
    invoke-direct {p0, v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->e(Z)V

    .line 350
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->t:Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;

    invoke-static {v0, p1}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b(Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;Lccu;)V

    goto :goto_1

    .line 328
    :sswitch_data_0
    .sparse-switch
        -0x7dc155c8 -> :sswitch_1
        -0x4a797962 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;)V
    .locals 3

    .prologue
    .line 281
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->g:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    .line 286
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 291
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->n()V

    .line 292
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->f:Lcom/twitter/app/dm/widget/DMMessageComposer$a;

    check-cast v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$d;

    invoke-interface {v0, p1}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$d;->c(Ljava/lang/String;)V

    .line 293
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->l()V

    .line 294
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->e:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:thread:dm_compose_bar:suggestions:send_dm"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 296
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 151
    if-eqz p2, :cond_0

    .line 152
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->m()V

    .line 156
    :goto_0
    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b(Ljava/lang/String;)V

    .line 157
    return-void

    .line 154
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->u()V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 274
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->C()Z

    move-result v0

    if-nez v0, :cond_0

    .line 275
    invoke-super {p0}, Lcom/twitter/app/dm/widget/DMMessageComposer;->b()V

    .line 277
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 166
    invoke-static {p1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0, p1, v4}, Lcom/twitter/android/composer/TweetBox;->a(Ljava/lang/String;[I)V

    .line 170
    invoke-static {}, Lcom/twitter/library/dm/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, " https://twitter.com/i/moments/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    const-string/jumbo v0, " https://twitter.com/i/moments/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 172
    array-length v1, v0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 173
    iget-object v1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v1, p1, v4}, Lcom/twitter/android/composer/TweetBox;->a(Ljava/lang/String;[I)V

    .line 174
    iget-object v1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v1, v3, v0}, Lcom/twitter/android/composer/TweetBox;->b(II)Z

    .line 175
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->m()V

    .line 179
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->p()V

    .line 180
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->i:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    iget-boolean v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->r:Z

    if-nez v0, :cond_0

    .line 213
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->d(Z)V

    .line 215
    :cond_0
    return-void
.end method

.method public bj_()V
    .locals 2

    .prologue
    .line 485
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->m:Lcom/twitter/android/media/widget/InlineComposerMediaLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/InlineComposerMediaLayout;->setVisibility(I)V

    .line 486
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->v()V

    .line 487
    return-void
.end method

.method public c(Z)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 381
    iget-object v3, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    iget-boolean v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->w:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    invoke-virtual {v3, v0}, Lcom/twitter/android/composer/TweetBox;->setCursorVisible(Z)V

    .line 384
    iget-boolean v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->v:Z

    if-eq v0, p1, :cond_1

    .line 385
    if-eqz p1, :cond_3

    .line 386
    invoke-direct {p0, v1}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->e(Z)V

    .line 387
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->f:Lcom/twitter/app/dm/widget/DMMessageComposer$a;

    check-cast v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$d;

    invoke-interface {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$d;->k()V

    .line 396
    :cond_1
    :goto_1
    iput-boolean p1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->v:Z

    .line 397
    return-void

    :cond_2
    move v0, v1

    .line 381
    goto :goto_0

    .line 389
    :cond_3
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->clearFocus()V

    .line 390
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->s:Lapk;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->s:Lapk;

    invoke-virtual {v0}, Lapk;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 391
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->s:Lapk;

    invoke-virtual {v0, v2, v1}, Lapk;->a(ZZ)V

    goto :goto_1
.end method

.method public d()V
    .locals 2

    .prologue
    .line 491
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->m:Lcom/twitter/android/media/widget/InlineComposerMediaLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/InlineComposerMediaLayout;->setVisibility(I)V

    .line 492
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 496
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->m:Lcom/twitter/android/media/widget/InlineComposerMediaLayout;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/InlineComposerMediaLayout;->isShown()Z

    move-result v0

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->o:Lcom/twitter/android/media/widget/AttachmentMediaView;

    invoke-virtual {v0}, Lcom/twitter/android/media/widget/AttachmentMediaView;->h()Z

    move-result v0

    return v0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 452
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->k:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 453
    return-void
.end method

.method public getCardUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 405
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->p:Lcom/twitter/android/card/j;

    if-eqz v0, :cond_0

    .line 406
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->p:Lcom/twitter/android/card/j;

    invoke-interface {v0}, Lcom/twitter/android/card/j;->b()Ljava/lang/String;

    move-result-object v0

    .line 407
    invoke-direct {p0, v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 411
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()V
    .locals 2

    .prologue
    .line 457
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->k:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 458
    return-void
.end method

.method public i()V
    .locals 2

    .prologue
    .line 462
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->l:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 463
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->l:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 465
    :cond_0
    return-void
.end method

.method public j()V
    .locals 2

    .prologue
    .line 469
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->l:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 470
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->l:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 472
    :cond_0
    return-void
.end method

.method public k()V
    .locals 5

    .prologue
    const/16 v2, 0x12c

    const/4 v4, 0x1

    .line 308
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->h:Lcom/twitter/app/dm/DMEmojiComposeView;

    invoke-static {v0, v2}, Lcom/twitter/app/dm/e;->a(Landroid/view/View;I)V

    .line 309
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->a:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->getWidth()I

    move-result v1

    invoke-static {v0, v2, v1}, Lcom/twitter/app/dm/e;->d(Landroid/view/View;II)V

    .line 310
    iput-boolean v4, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->u:Z

    .line 311
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->e:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:thread:dm_compose_bar:suggestions:impression"

    aput-object v3, v1, v2

    .line 312
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 311
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 313
    return-void
.end method

.method public l()V
    .locals 3

    .prologue
    const/16 v2, 0x12c

    .line 316
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->h:Lcom/twitter/app/dm/DMEmojiComposeView;

    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->getWidth()I

    move-result v1

    invoke-static {v0, v2, v1}, Lcom/twitter/app/dm/e;->c(Landroid/view/View;II)V

    .line 317
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->a:Landroid/view/ViewGroup;

    invoke-static {v0, v2}, Lcom/twitter/app/dm/e;->a(Landroid/view/View;I)V

    .line 318
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->u:Z

    .line 319
    return-void
.end method

.method public m()V
    .locals 1

    .prologue
    .line 323
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->e(Z)V

    .line 324
    invoke-super {p0}, Lcom/twitter/app/dm/widget/DMMessageComposer;->m()V

    .line 325
    return-void
.end method

.method public n()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 361
    invoke-direct {p0, v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->e(Z)V

    .line 362
    invoke-direct {p0, v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->f(Z)V

    .line 363
    return-void
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 400
    iget-boolean v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->u:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 238
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 239
    sparse-switch v0, :sswitch_data_0

    .line 267
    invoke-super {p0, p1}, Lcom/twitter/app/dm/widget/DMMessageComposer;->onClick(Landroid/view/View;)V

    .line 270
    :cond_0
    :goto_0
    return-void

    .line 241
    :sswitch_0
    iget-boolean v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->r:Z

    if-eqz v0, :cond_1

    .line 242
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->f:Lcom/twitter/app/dm/widget/DMMessageComposer$a;

    check-cast v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$d;

    invoke-interface {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$d;->A()V

    goto :goto_0

    .line 244
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->k()V

    goto :goto_0

    .line 249
    :sswitch_1
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->f:Lcom/twitter/app/dm/widget/DMMessageComposer$a;

    check-cast v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$d;

    invoke-interface {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$d;->y()V

    goto :goto_0

    .line 253
    :sswitch_2
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->f:Lcom/twitter/app/dm/widget/DMMessageComposer$a;

    check-cast v0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$d;

    invoke-interface {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$d;->z()V

    goto :goto_0

    .line 257
    :sswitch_3
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->f(Z)V

    goto :goto_0

    .line 261
    :sswitch_4
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->C()Z

    move-result v0

    if-nez v0, :cond_0

    .line 262
    invoke-super {p0, p1}, Lcom/twitter/app/dm/widget/DMMessageComposer;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 239
    :sswitch_data_0
    .sparse-switch
        0x7f1302e6 -> :sswitch_4
        0x7f1302e7 -> :sswitch_3
        0x7f1302e8 -> :sswitch_0
        0x7f1304b4 -> :sswitch_1
        0x7f1304b5 -> :sswitch_2
    .end sparse-switch
.end method

.method p()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 423
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v0}, Lcom/twitter/android/composer/TweetBox;->p()Z

    move-result v0

    .line 424
    iget-object v1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->c:Landroid/widget/Button;

    iget-object v2, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b:Lcom/twitter/android/composer/TweetBox;

    invoke-virtual {v2}, Lcom/twitter/android/composer/TweetBox;->p()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 425
    if-nez v0, :cond_2

    .line 426
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->c:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 427
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->B()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 428
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->j:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 429
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->t:Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->b()V

    .line 441
    :cond_0
    :goto_0
    return-void

    .line 431
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->i:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 434
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->c:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 435
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->i:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 436
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->B()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 437
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->t:Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$c;->a()V

    .line 438
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->j:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public q()V
    .locals 2

    .prologue
    .line 444
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->l:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 445
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->l:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 446
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->l:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 448
    :cond_0
    return-void
.end method

.method public r()V
    .locals 2

    .prologue
    .line 505
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->n:Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/InlineComposerMediaScrollView;->setVisibility(I)V

    .line 506
    return-void
.end method

.method public setAssociation(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 1

    .prologue
    .line 509
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->q:Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 510
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->q:Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;

    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 512
    :cond_0
    return-void
.end method

.method public setQuickReplyDrawerHost(Lapk;)V
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->s:Lapk;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string/jumbo v1, "Attempting to set a new host on the Message Composer. Something\'s fishy!"

    invoke-static {v0, v1}, Lcom/twitter/util/f;->a(ZLjava/lang/String;)Z

    .line 162
    iput-object p1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->s:Lapk;

    .line 163
    return-void

    .line 160
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
