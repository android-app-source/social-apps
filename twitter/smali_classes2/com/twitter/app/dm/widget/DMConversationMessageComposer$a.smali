.class Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/dm/widget/DMConversationMessageComposer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

.field private final b:Landroid/content/Context;

.field private final c:J

.field private d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;Landroid/content/Context;J)V
    .locals 1

    .prologue
    .line 574
    iput-object p1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;->a:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 575
    iput-object p2, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;->b:Landroid/content/Context;

    .line 576
    iput-wide p3, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;->c:J

    .line 577
    return-void
.end method


# virtual methods
.method a()V
    .locals 4

    .prologue
    .line 584
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;->a:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-static {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->a(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;)Lcom/twitter/android/card/j;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 585
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;->c:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 586
    iget-object v1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;->a:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    .line 587
    invoke-static {v2}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->a(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;)Lcom/twitter/android/card/j;

    move-result-object v2

    invoke-interface {v2}, Lcom/twitter/android/card/j;->d()Lcax;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;->a:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    .line 588
    invoke-static {v3}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->a(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;)Lcom/twitter/android/card/j;

    move-result-object v3

    invoke-interface {v3}, Lcom/twitter/android/card/j;->c()Ljava/lang/String;

    move-result-object v3

    .line 587
    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/scribe/c;->b(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcax;Ljava/lang/String;)V

    .line 589
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:thread:dm_compose_bar:platform_card_preview:impression"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 590
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 589
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 592
    :cond_0
    return-void
.end method

.method a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V
    .locals 0

    .prologue
    .line 580
    iput-object p1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 581
    return-void
.end method

.method b()V
    .locals 4

    .prologue
    .line 595
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;->a:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-static {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->a(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;)Lcom/twitter/android/card/j;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 596
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;->c:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 597
    iget-object v1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;->a:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    .line 598
    invoke-static {v2}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->a(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;)Lcom/twitter/android/card/j;

    move-result-object v2

    invoke-interface {v2}, Lcom/twitter/android/card/j;->d()Lcax;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;->a:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    .line 599
    invoke-static {v3}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->a(Lcom/twitter/app/dm/widget/DMConversationMessageComposer;)Lcom/twitter/android/card/j;

    move-result-object v3

    invoke-interface {v3}, Lcom/twitter/android/card/j;->c()Ljava/lang/String;

    move-result-object v3

    .line 598
    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/scribe/c;->b(Lcom/twitter/analytics/feature/model/ClientEventLog;Landroid/content/Context;Lcax;Ljava/lang/String;)V

    .line 600
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:thread:dm_compose_bar:platform_card_preview:dismiss"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/app/dm/widget/DMConversationMessageComposer$a;->d:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 601
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeAssociation;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 600
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 603
    :cond_0
    return-void
.end method
