.class Lcom/twitter/app/dm/widget/b$1;
.super Landroid/support/design/widget/BottomSheetBehavior$BottomSheetCallback;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/dm/widget/b;->c()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/dm/widget/b;


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/widget/b;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/twitter/app/dm/widget/b$1;->a:Lcom/twitter/app/dm/widget/b;

    invoke-direct {p0}, Landroid/support/design/widget/BottomSheetBehavior$BottomSheetCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSlide(Landroid/view/View;F)V
    .locals 2

    .prologue
    .line 112
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    .line 113
    iget-object v1, p0, Lcom/twitter/app/dm/widget/b$1;->a:Lcom/twitter/app/dm/widget/b;

    iget-object v1, v1, Lcom/twitter/app/dm/widget/b;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 114
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 115
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b$1;->a:Lcom/twitter/app/dm/widget/b;

    iget-object v0, v0, Lcom/twitter/app/dm/widget/b;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 116
    return-void
.end method

.method public onStateChanged(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b$1;->a:Lcom/twitter/app/dm/widget/b;

    iget-object v0, v0, Lcom/twitter/app/dm/widget/b;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 78
    const/4 v1, 0x5

    if-ne p2, v1, :cond_2

    .line 79
    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 83
    :goto_0
    iget-object v1, p0, Lcom/twitter/app/dm/widget/b$1;->a:Lcom/twitter/app/dm/widget/b;

    iget-object v1, v1, Lcom/twitter/app/dm/widget/b;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 85
    const/4 v0, 0x3

    if-ne p2, v0, :cond_3

    .line 86
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b$1;->a:Lcom/twitter/app/dm/widget/b;

    invoke-static {v0}, Lcom/twitter/app/dm/widget/b;->a(Lcom/twitter/app/dm/widget/b;)Lcom/twitter/app/dm/widget/b$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b$1;->a:Lcom/twitter/app/dm/widget/b;

    invoke-static {v0}, Lcom/twitter/app/dm/widget/b;->a(Lcom/twitter/app/dm/widget/b;)Lcom/twitter/app/dm/widget/b$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/app/dm/widget/b$a;->a()V

    .line 92
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;

    .line 93
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;->setBehavior(Landroid/support/design/widget/CoordinatorLayout$Behavior;)V

    .line 95
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b$1;->a:Lcom/twitter/app/dm/widget/b;

    invoke-static {v0}, Lcom/twitter/app/dm/widget/b;->b(Lcom/twitter/app/dm/widget/b;)V

    .line 100
    :goto_1
    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    .line 101
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b$1;->a:Lcom/twitter/app/dm/widget/b;

    iget-object v0, v0, Lcom/twitter/app/dm/widget/b;->b:Landroid/support/design/widget/BottomSheetBehavior;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomSheetBehavior;->setHideable(Z)V

    .line 102
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b$1;->a:Lcom/twitter/app/dm/widget/b;

    invoke-static {v0}, Lcom/twitter/app/dm/widget/b;->a(Lcom/twitter/app/dm/widget/b;)Lcom/twitter/app/dm/widget/b$a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 103
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b$1;->a:Lcom/twitter/app/dm/widget/b;

    invoke-static {v0}, Lcom/twitter/app/dm/widget/b;->a(Lcom/twitter/app/dm/widget/b;)Lcom/twitter/app/dm/widget/b$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/app/dm/widget/b$a;->b()V

    .line 106
    :cond_1
    return-void

    .line 81
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0

    .line 97
    :cond_3
    iget-object v0, p0, Lcom/twitter/app/dm/widget/b$1;->a:Lcom/twitter/app/dm/widget/b;

    invoke-static {v0}, Lcom/twitter/app/dm/widget/b;->c(Lcom/twitter/app/dm/widget/b;)V

    goto :goto_1
.end method
