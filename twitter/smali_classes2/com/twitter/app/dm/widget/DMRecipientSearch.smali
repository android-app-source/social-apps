.class public abstract Lcom/twitter/app/dm/widget/DMRecipientSearch;
.super Landroid/widget/RelativeLayout;
.source "Twttr"


# static fields
.field private static final a:Landroid/graphics/Typeface;


# instance fields
.field private b:Lcom/twitter/android/autocomplete/SuggestionEditText;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/android/autocomplete/SuggestionEditText",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/graphics/drawable/Drawable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    const-string/jumbo v0, "sans-serif-light"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/twitter/app/dm/widget/DMRecipientSearch;->a:Landroid/graphics/Typeface;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .param p4    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    invoke-static {p1, p4, p0}, Lcom/twitter/app/dm/widget/DMRecipientSearch;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 29
    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 41
    if-eqz p1, :cond_0

    move-object v0, v1

    .line 42
    :goto_0
    iget-object v2, p0, Lcom/twitter/app/dm/widget/DMRecipientSearch;->b:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v2, v0, v1, v1, v1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 43
    return-void

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMRecipientSearch;->c:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public getSuggestionEditText()Lcom/twitter/android/autocomplete/SuggestionEditText;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/android/autocomplete/SuggestionEditText",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMRecipientSearch;->b:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/autocomplete/SuggestionEditText;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 33
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 35
    const v0, 0x7f13029e

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/widget/DMRecipientSearch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/autocomplete/SuggestionEditText;

    iput-object v0, p0, Lcom/twitter/app/dm/widget/DMRecipientSearch;->b:Lcom/twitter/android/autocomplete/SuggestionEditText;

    .line 36
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMRecipientSearch;->b:Lcom/twitter/android/autocomplete/SuggestionEditText;

    sget-object v1, Lcom/twitter/app/dm/widget/DMRecipientSearch;->a:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Lcom/twitter/android/autocomplete/SuggestionEditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 37
    iget-object v0, p0, Lcom/twitter/app/dm/widget/DMRecipientSearch;->b:Lcom/twitter/android/autocomplete/SuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/SuggestionEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/twitter/app/dm/widget/DMRecipientSearch;->c:Landroid/graphics/drawable/Drawable;

    .line 38
    return-void
.end method
