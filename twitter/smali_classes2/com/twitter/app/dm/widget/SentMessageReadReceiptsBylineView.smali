.class public Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;
.super Lcom/twitter/app/dm/widget/SentMessageBylineView;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$b;,
        Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$a;
    }
.end annotation


# static fields
.field private static final e:Landroid/view/animation/Interpolator;


# instance fields
.field private final f:Landroid/view/View;

.field private final g:Landroid/view/View;

.field private final h:Landroid/widget/ImageView;

.field private final i:Landroid/widget/TextView;

.field private final j:Landroid/view/View;

.field private final k:Landroid/widget/TextView;

.field private final l:Lcom/twitter/ui/widget/CircularProgressIndicator;

.field private final m:Landroid/widget/ImageView;

.field private final n:Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$b;

.field private o:Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$b;

.field private final p:Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$a;

.field private final q:Ljava/lang/String;

.field private r:Lcom/twitter/model/dms/n;

.field private s:Lcom/twitter/model/dms/e;

.field private t:Z

.field private u:Z

.field private v:I

.field private w:Z

.field private final x:Lcom/twitter/app/dm/widget/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Landroid/support/v4/view/animation/FastOutSlowInInterpolator;

    invoke-direct {v0}, Landroid/support/v4/view/animation/FastOutSlowInInterpolator;-><init>()V

    sput-object v0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->e:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$a;Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$b;)V
    .locals 3

    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, Lcom/twitter/app/dm/widget/SentMessageBylineView;-><init>(Landroid/content/Context;Lcom/twitter/app/dm/widget/SentMessageBylineView$a;)V

    .line 78
    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->v:I

    .line 88
    const v0, 0x7f13030d

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 90
    const v1, 0x7f130312

    .line 91
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 90
    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iput-object v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->f:Landroid/view/View;

    .line 92
    iget-object v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->f:Landroid/view/View;

    const v2, 0x7f130313

    .line 93
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iput-object v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->g:Landroid/view/View;

    .line 94
    iget-object v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->g:Landroid/view/View;

    const v2, 0x7f130314

    .line 95
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 94
    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->h:Landroid/widget/ImageView;

    .line 97
    const v1, 0x7f130315

    invoke-virtual {p0, v1}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 98
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    const v2, 0x7f130316

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->i:Landroid/widget/TextView;

    .line 100
    iget-object v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->i:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 102
    const v1, 0x7f13030e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->j:Landroid/view/View;

    .line 103
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->j:Landroid/view/View;

    const v1, 0x7f13030f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->k:Landroid/widget/TextView;

    .line 104
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->j:Landroid/view/View;

    const v1, 0x7f130310

    .line 105
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/CircularProgressIndicator;

    iput-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->l:Lcom/twitter/ui/widget/CircularProgressIndicator;

    .line 106
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->j:Landroid/view/View;

    const v1, 0x7f130311

    .line 107
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->m:Landroid/widget/ImageView;

    .line 109
    iput-object p3, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->n:Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$b;

    .line 110
    iput-object p2, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->p:Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$a;

    .line 111
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a048c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->q:Ljava/lang/String;

    .line 113
    invoke-static {}, Lcom/twitter/app/dm/widget/a;->a()Lcom/twitter/app/dm/widget/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->x:Lcom/twitter/app/dm/widget/a;

    .line 114
    return-void
.end method

.method private A()V
    .locals 4

    .prologue
    .line 650
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->getMessageId()J

    move-result-wide v0

    .line 651
    iget-object v2, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->n:Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$b;

    iget-boolean v3, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->c:Z

    invoke-interface {v2, v0, v1, v3, p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$b;->a(JZLcom/twitter/app/dm/widget/SentMessageBylineView;)V

    .line 652
    iget-object v2, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->o:Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$b;

    iget-boolean v3, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->c:Z

    invoke-interface {v2, v0, v1, v3, p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$b;->a(JZLcom/twitter/app/dm/widget/SentMessageBylineView;)V

    .line 653
    return-void
.end method

.method private B()V
    .locals 4

    .prologue
    .line 656
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->getMessageId()J

    move-result-wide v0

    .line 657
    iget-object v2, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->n:Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$b;

    iget-boolean v3, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->c:Z

    invoke-interface {v2, v0, v1, v3}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$b;->a(JZ)V

    .line 658
    iget-object v2, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->o:Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$b;

    iget-boolean v3, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->c:Z

    invoke-interface {v2, v0, v1, v3}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$b;->a(JZ)V

    .line 659
    return-void
.end method

.method private C()V
    .locals 1

    .prologue
    .line 662
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->setVisibility(I)V

    .line 663
    return-void
.end method

.method private a(Landroid/view/View;IFIILandroid/animation/AnimatorListenerAdapter;)Landroid/animation/AnimatorSet;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 423
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p3

    .line 425
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 426
    int-to-long v2, p2

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 427
    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 428
    sget-object v2, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->e:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 429
    new-array v2, v7, [Landroid/animation/Animator;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v7, [F

    aput v0, v4, v5

    aput p3, v4, v6

    .line 430
    invoke-static {p1, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    aput-object v0, v2, v5

    sget-object v0, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v3, v7, [F

    int-to-float v4, p4

    aput v4, v3, v5

    int-to-float v4, p5

    aput v4, v3, v6

    .line 431
    invoke-static {p1, v0, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    aput-object v0, v2, v6

    .line 429
    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 433
    if-eqz p6, :cond_0

    .line 434
    invoke-virtual {v1, p6}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 437
    :cond_0
    return-object v1
.end method

.method static synthetic a(Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->k()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->c(Ljava/util/List;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;Z)Z
    .locals 0

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->t:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;)Lcom/twitter/app/dm/widget/a;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->x:Lcom/twitter/app/dm/widget/a;

    return-object v0
.end method

.method private b(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 335
    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->a(Ljava/util/List;)Landroid/text/Spanned;

    move-result-object v0

    .line 336
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->i:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 338
    iget-object v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->i:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 340
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 8

    .prologue
    const v7, 0x7f130026

    const v6, 0x7f130023

    .line 301
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->s:Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->q()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->g:Landroid/view/View;

    .line 302
    invoke-virtual {v1, v6}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 303
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->x:Lcom/twitter/app/dm/widget/a;

    iget-object v2, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->s:Lcom/twitter/model/dms/e;

    invoke-virtual {v2}, Lcom/twitter/model/dms/e;->q()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/app/dm/widget/a;->c(J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 324
    :goto_0
    return-void

    .line 308
    :cond_0
    iget-object v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->h:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 310
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->g:Landroid/view/View;

    .line 311
    invoke-virtual {v2, v7}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 312
    iget-object v2, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->g:Landroid/view/View;

    iget-object v3, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->s:Lcom/twitter/model/dms/e;

    invoke-virtual {v3}, Lcom/twitter/model/dms/e;->q()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 313
    iget-object v2, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->g:Landroid/view/View;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 315
    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    if-nez v1, :cond_1

    .line 316
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->n()V

    goto :goto_0

    .line 318
    :cond_1
    if-eqz p1, :cond_2

    .line 319
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->k()V

    goto :goto_0

    .line 321
    :cond_2
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->j()V

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->j:Landroid/view/View;

    return-object v0
.end method

.method private c(Z)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 618
    if-eqz p1, :cond_0

    .line 619
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0274

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 621
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/CharSequence;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a0275

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "..."

    aput-object v2, v0, v1

    invoke-static {v0}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private c(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 379
    iget v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->v:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->v:I

    .line 380
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->p:Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$a;

    iget v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->v:I

    invoke-interface {v0, v1}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$a;->a(I)V

    .line 381
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->i:Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->a(Ljava/util/List;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 382
    return-void
.end method

.method static synthetic d(Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->r()V

    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 224
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->s:Lcom/twitter/model/dms/e;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/r;

    .line 225
    invoke-virtual {v0}, Lcom/twitter/model/dms/r;->f()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->s:Lcom/twitter/model/dms/e;

    invoke-virtual {v3}, Lcom/twitter/model/dms/e;->D()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 226
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 227
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->s:Lcom/twitter/model/dms/e;

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/r;

    invoke-virtual {v0}, Lcom/twitter/model/dms/r;->G()I

    move-result v0

    .line 228
    if-lez v0, :cond_0

    .line 229
    iget-object v2, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->l:Lcom/twitter/ui/widget/CircularProgressIndicator;

    invoke-virtual {v2, v0}, Lcom/twitter/ui/widget/CircularProgressIndicator;->a(I)V

    .line 230
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->l:Lcom/twitter/ui/widget/CircularProgressIndicator;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/CircularProgressIndicator;->setVisibility(I)V

    .line 242
    :goto_0
    return-void

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->l:Lcom/twitter/ui/widget/CircularProgressIndicator;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/CircularProgressIndicator;->a()V

    .line 233
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->l:Lcom/twitter/ui/widget/CircularProgressIndicator;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/CircularProgressIndicator;->setVisibility(I)V

    goto :goto_0

    .line 236
    :cond_1
    iget-object v3, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->l:Lcom/twitter/ui/widget/CircularProgressIndicator;

    invoke-virtual {v3}, Lcom/twitter/ui/widget/CircularProgressIndicator;->a()V

    .line 237
    iget-object v3, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->l:Lcom/twitter/ui/widget/CircularProgressIndicator;

    invoke-virtual {v3, v2}, Lcom/twitter/ui/widget/CircularProgressIndicator;->setVisibility(I)V

    .line 239
    invoke-virtual {v0}, Lcom/twitter/model/dms/r;->g()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    const/4 v0, 0x1

    .line 240
    :goto_1
    iget-object v3, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->m:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    :goto_2
    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 239
    goto :goto_1

    :cond_3
    move v1, v2

    .line 240
    goto :goto_2
.end method

.method private i()V
    .locals 1

    .prologue
    .line 245
    iget-boolean v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->b:Z

    if-eqz v0, :cond_1

    .line 246
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->e()V

    .line 247
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->A()V

    .line 252
    :goto_0
    iget-boolean v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->b:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->u:Z

    if-eqz v0, :cond_2

    .line 253
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->x()V

    .line 258
    :goto_1
    iget-boolean v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->c:Z

    if-eqz v0, :cond_0

    .line 259
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->e()V

    .line 260
    iget-boolean v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->b:Z

    if-eqz v0, :cond_3

    .line 261
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->C()V

    .line 266
    :cond_0
    :goto_2
    return-void

    .line 249
    :cond_1
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->u()V

    goto :goto_0

    .line 255
    :cond_2
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->y()V

    goto :goto_1

    .line 263
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->g()V

    goto :goto_2
.end method

.method private j()V
    .locals 3

    .prologue
    .line 327
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->h:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f020372

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 328
    return-void
.end method

.method private k()V
    .locals 3

    .prologue
    .line 331
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->h:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f020371

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 332
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 395
    .line 396
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->f:Landroid/view/View;

    new-instance v1, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$2;

    invoke-direct {v1, p0, p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$2;-><init>(Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;Lcom/twitter/app/dm/widget/SentMessageBylineView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 402
    return-void
.end method

.method private m()V
    .locals 2

    .prologue
    .line 405
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 418
    :goto_0
    return-void

    .line 409
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->t:Z

    .line 411
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 413
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->w()V

    goto :goto_0

    .line 416
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->s:Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->q()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->a(J)V

    goto :goto_0
.end method

.method private n()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x64

    const/4 v6, 0x2

    .line 441
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->s:Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->a()J

    move-result-wide v0

    .line 443
    iget-object v2, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->h:Landroid/widget/ImageView;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v6, [F

    fill-array-data v4, :array_0

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 445
    invoke-virtual {v2, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 447
    iget-object v3, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->h:Landroid/widget/ImageView;

    sget-object v4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v5, v6, [F

    fill-array-data v5, :array_1

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 449
    const-wide/16 v4, 0x32

    invoke-virtual {v3, v4, v5}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 450
    invoke-virtual {v3, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 451
    new-instance v4, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$3;

    invoke-direct {v4, p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$3;-><init>(Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;)V

    invoke-virtual {v3, v4}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 458
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    .line 459
    new-array v5, v6, [Landroid/animation/Animator;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v2, 0x1

    aput-object v3, v5, v2

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 460
    new-instance v2, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$4;

    invoke-direct {v2, p0, v0, v1}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$4;-><init>(Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;J)V

    invoke-virtual {v4, v2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 467
    iget-object v2, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->x:Lcom/twitter/app/dm/widget/a;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/app/dm/widget/a;->a(J)V

    .line 468
    invoke-virtual {v4}, Landroid/animation/AnimatorSet;->start()V

    .line 469
    return-void

    .line 443
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 447
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private o()Landroid/animation/AnimatorListenerAdapter;
    .locals 1

    .prologue
    .line 473
    new-instance v0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$5;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$5;-><init>(Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;)V

    return-object v0
.end method

.method private p()Landroid/animation/AnimatorListenerAdapter;
    .locals 1

    .prologue
    .line 485
    new-instance v0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$6;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$6;-><init>(Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;)V

    return-object v0
.end method

.method private q()Landroid/animation/AnimatorListenerAdapter;
    .locals 1

    .prologue
    .line 497
    new-instance v0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$7;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$7;-><init>(Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;)V

    return-object v0
.end method

.method private r()V
    .locals 2

    .prologue
    .line 508
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->x:Lcom/twitter/app/dm/widget/a;

    iget-object v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->s:Lcom/twitter/model/dms/e;

    invoke-virtual {v1}, Lcom/twitter/model/dms/e;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/widget/a;->b(Ljava/lang/String;)V

    .line 509
    iget-object v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->p:Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$a;

    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->s:Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->v()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$a;->b(Ljava/lang/String;)V

    .line 510
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->b()V

    .line 511
    return-void
.end method

.method private s()Z
    .locals 1

    .prologue
    .line 514
    iget-boolean v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->t:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private t()Z
    .locals 2

    .prologue
    .line 518
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->x:Lcom/twitter/app/dm/widget/a;

    iget-object v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->s:Lcom/twitter/model/dms/e;

    invoke-virtual {v1}, Lcom/twitter/model/dms/e;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/widget/a;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private u()V
    .locals 2

    .prologue
    .line 535
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->d:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 537
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->a:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 538
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->a:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 539
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->a:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 541
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->y()V

    .line 542
    return-void
.end method

.method private v()V
    .locals 14

    .prologue
    const/16 v2, 0x12c

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 545
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v9

    .line 547
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->j:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setTranslationY(F)V

    .line 548
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->j:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setAlpha(F)V

    .line 549
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->j:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 550
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->k:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->s:Lcom/twitter/model/dms/e;

    invoke-virtual {v1}, Lcom/twitter/model/dms/e;->D()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->c(Z)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 551
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->m:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 554
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->u()V

    .line 555
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->j()V

    .line 556
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 557
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->f:Landroid/view/View;

    int-to-float v1, v9

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 558
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 559
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->f:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 561
    new-instance v12, Landroid/animation/AnimatorSet;

    invoke-direct {v12}, Landroid/animation/AnimatorSet;-><init>()V

    .line 562
    const/4 v0, 0x2

    new-array v13, v0, [Landroid/animation/Animator;

    iget-object v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->j:Landroid/view/View;

    neg-int v5, v9

    .line 564
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->q()Landroid/animation/AnimatorListenerAdapter;

    move-result-object v6

    move-object v0, p0

    .line 563
    invoke-direct/range {v0 .. v6}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->a(Landroid/view/View;IFIILandroid/animation/AnimatorListenerAdapter;)Landroid/animation/AnimatorSet;

    move-result-object v0

    aput-object v0, v13, v4

    const/4 v0, 0x1

    iget-object v6, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->f:Landroid/view/View;

    const/4 v11, 0x0

    move-object v5, p0

    move v7, v2

    move v10, v4

    .line 565
    invoke-direct/range {v5 .. v11}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->a(Landroid/view/View;IFIILandroid/animation/AnimatorListenerAdapter;)Landroid/animation/AnimatorSet;

    move-result-object v1

    aput-object v1, v13, v0

    .line 562
    invoke-virtual {v12, v13}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 569
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->x:Lcom/twitter/app/dm/widget/a;

    iget-object v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->s:Lcom/twitter/model/dms/e;

    invoke-virtual {v1}, Lcom/twitter/model/dms/e;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/widget/a;->a(Ljava/lang/String;)V

    .line 570
    invoke-virtual {v12}, Landroid/animation/AnimatorSet;->start()V

    .line 571
    return-void
.end method

.method private w()V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 574
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v7

    .line 576
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->d:Landroid/widget/TextView;

    int-to-float v1, v7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 577
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setAlpha(F)V

    .line 578
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 580
    new-instance v8, Landroid/animation/AnimatorSet;

    invoke-direct {v8}, Landroid/animation/AnimatorSet;-><init>()V

    .line 581
    const/4 v0, 0x2

    new-array v9, v0, [Landroid/animation/Animator;

    iget-object v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->a:Landroid/widget/TextView;

    neg-int v5, v7

    .line 583
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->o()Landroid/animation/AnimatorListenerAdapter;

    move-result-object v6

    move-object v0, p0

    move v4, v2

    .line 582
    invoke-direct/range {v0 .. v6}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->a(Landroid/view/View;IFIILandroid/animation/AnimatorListenerAdapter;)Landroid/animation/AnimatorSet;

    move-result-object v0

    aput-object v0, v9, v2

    const/4 v10, 0x1

    iget-object v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->d:Landroid/widget/TextView;

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    move-object v0, p0

    move v4, v7

    move v5, v2

    .line 584
    invoke-direct/range {v0 .. v6}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->a(Landroid/view/View;IFIILandroid/animation/AnimatorListenerAdapter;)Landroid/animation/AnimatorSet;

    move-result-object v0

    aput-object v0, v9, v10

    .line 581
    invoke-virtual {v8, v9}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 587
    invoke-virtual {v8}, Landroid/animation/AnimatorSet;->start()V

    .line 589
    iget-boolean v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->u:Z

    if-eqz v0, :cond_0

    .line 590
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->x()V

    .line 592
    :cond_0
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->A()V

    .line 593
    return-void
.end method

.method private x()V
    .locals 2

    .prologue
    .line 632
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->i:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 633
    return-void
.end method

.method private y()V
    .locals 2

    .prologue
    .line 636
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->i:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 637
    return-void
.end method

.method private z()V
    .locals 1

    .prologue
    .line 640
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 641
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->g()V

    .line 642
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->B()V

    .line 647
    :goto_0
    return-void

    .line 644
    :cond_0
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->C()V

    .line 645
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->A()V

    goto :goto_0
.end method


# virtual methods
.method a(Ljava/util/List;)Landroid/text/Spanned;
    .locals 8
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/text/Spanned;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 345
    iget v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->v:I

    mul-int/lit8 v0, v0, 0xa

    .line 347
    invoke-static {p1, v0}, Lcpt;->a(Ljava/lang/Iterable;I)Ljava/lang/Iterable;

    move-result-object v1

    .line 348
    iget-object v2, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->q:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    .line 349
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    sub-int v0, v2, v0

    .line 351
    if-lez v0, :cond_0

    .line 352
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0006

    new-array v4, v7, [Ljava/lang/Object;

    .line 354
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    .line 352
    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 356
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    .line 357
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f11018b

    invoke-static {v3, v4}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 359
    iget-object v3, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->i:Landroid/widget/TextView;

    new-instance v4, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$1;

    invoke-direct {v4, p0, p1}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$1;-><init>(Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;Ljava/util/List;)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 366
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 367
    invoke-virtual {v3, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    const-string/jumbo v3, " "

    .line 368
    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    new-array v3, v7, [Landroid/text/style/ForegroundColorSpan;

    aput-object v2, v3, v6

    const-string/jumbo v2, "{{}}"

    .line 369
    invoke-static {v3, v0, v2}, Lcom/twitter/library/util/af;->a([Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 374
    :goto_0
    return-object v0

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->i:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 374
    new-instance v0, Landroid/text/SpannedString;

    invoke-direct {v0, v1}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(J)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 597
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    neg-int v7, v0

    .line 599
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->a:Landroid/widget/TextView;

    int-to-float v1, v7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 600
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setAlpha(F)V

    .line 601
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 603
    new-instance v8, Landroid/animation/AnimatorSet;

    invoke-direct {v8}, Landroid/animation/AnimatorSet;-><init>()V

    .line 604
    const/4 v0, 0x2

    new-array v9, v0, [Landroid/animation/Animator;

    iget-object v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->d:Landroid/widget/TextView;

    neg-int v5, v7

    .line 606
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->p()Landroid/animation/AnimatorListenerAdapter;

    move-result-object v6

    move-object v0, p0

    move v4, v2

    .line 605
    invoke-direct/range {v0 .. v6}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->a(Landroid/view/View;IFIILandroid/animation/AnimatorListenerAdapter;)Landroid/animation/AnimatorSet;

    move-result-object v0

    aput-object v0, v9, v2

    const/4 v10, 0x1

    iget-object v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->a:Landroid/widget/TextView;

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    move-object v0, p0

    move v4, v7

    move v5, v2

    .line 607
    invoke-direct/range {v0 .. v6}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->a(Landroid/view/View;IFIILandroid/animation/AnimatorListenerAdapter;)Landroid/animation/AnimatorSet;

    move-result-object v0

    aput-object v0, v9, v10

    .line 604
    invoke-virtual {v8, v9}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 610
    invoke-virtual {v8}, Landroid/animation/AnimatorSet;->start()V

    .line 612
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->y()V

    .line 613
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->B()V

    .line 614
    return-void
.end method

.method public a(Lcom/twitter/app/dm/widget/SentMessageBylineView;)V
    .locals 1

    .prologue
    .line 287
    invoke-super {p0, p1}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->a(Lcom/twitter/app/dm/widget/SentMessageBylineView;)V

    .line 288
    iget-boolean v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->c:Z

    if-eqz v0, :cond_0

    .line 289
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->z()V

    .line 293
    :goto_0
    return-void

    .line 291
    :cond_0
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->m()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/dms/n;Lcom/twitter/model/dms/e;ZIZLcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$b;Z)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 120
    invoke-super {p0, p3, p5}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->a(ZZ)V

    .line 121
    iput-object p1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->r:Lcom/twitter/model/dms/n;

    .line 122
    iput-object p2, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->s:Lcom/twitter/model/dms/e;

    .line 123
    if-le p4, v0, :cond_1

    :goto_0
    iput p4, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->v:I

    .line 124
    iput-object p6, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->o:Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$b;

    .line 125
    iput-boolean p7, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->w:Z

    .line 127
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->y()V

    .line 132
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->t()Z

    move-result v0

    if-nez v0, :cond_0

    .line 133
    if-eqz p3, :cond_2

    .line 134
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->e()V

    .line 140
    :goto_1
    if-eqz p5, :cond_3

    .line 141
    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->g()V

    .line 146
    :cond_0
    :goto_2
    return-void

    :cond_1
    move p4, v0

    .line 123
    goto :goto_0

    .line 136
    :cond_2
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->u()V

    goto :goto_1

    .line 143
    :cond_3
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->C()V

    goto :goto_2
.end method

.method public b()V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 150
    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v0

    invoke-virtual {v0}, Lcof;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->s:Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->d()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 154
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->b()V

    .line 156
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->t()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 204
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 151
    goto :goto_0

    .line 161
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->j:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 162
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 163
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 165
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 166
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->s:Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->q()J

    move-result-wide v6

    .line 170
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->r:Lcom/twitter/model/dms/n;

    if-eqz v0, :cond_4

    .line 171
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->r:Lcom/twitter/model/dms/n;

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/twitter/model/dms/n;->a(JJ)Ljava/util/List;

    move-result-object v3

    .line 172
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->r:Lcom/twitter/model/dms/n;

    invoke-virtual {v0, v6, v7}, Lcom/twitter/model/dms/n;->a(J)I

    move-result v0

    .line 173
    if-lez v0, :cond_3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v0, :cond_3

    move v0, v1

    .line 179
    :goto_2
    invoke-direct {p0, v0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->b(Z)V

    .line 181
    iput-boolean v2, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->u:Z

    .line 182
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 184
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->d:Landroid/widget/TextView;

    const v1, 0x7f0a02f2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 185
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->y()V

    .line 202
    :goto_3
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->l()V

    .line 203
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->i()V

    goto :goto_1

    :cond_3
    move v0, v2

    .line 173
    goto :goto_2

    .line 175
    :cond_4
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    move-object v3, v0

    move v0, v2

    .line 176
    goto :goto_2

    .line 186
    :cond_5
    if-eqz v0, :cond_7

    .line 187
    iget-boolean v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->w:Z

    if-eqz v0, :cond_6

    .line 189
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->d:Landroid/widget/TextView;

    const v1, 0x7f0a02f1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    .line 192
    :cond_6
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->d:Landroid/widget/TextView;

    const v1, 0x7f0a02f0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    .line 196
    :cond_7
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0007

    .line 197
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    new-array v7, v1, [Ljava/lang/Object;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v2

    .line 196
    invoke-virtual {v4, v5, v6, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 198
    invoke-direct {p0, v3}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->b(Ljava/util/List;)V

    .line 199
    iput-boolean v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->u:Z

    goto :goto_3
.end method

.method public c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 208
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->s:Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->d()Z

    move-result v0

    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 209
    invoke-super {p0}, Lcom/twitter/app/dm/widget/SentMessageBylineView;->c()V

    .line 211
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 213
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->j:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 214
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->j:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 215
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->j:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 217
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->k:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->s:Lcom/twitter/model/dms/e;

    invoke-virtual {v1}, Lcom/twitter/model/dms/e;->D()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->c(Z)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 220
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->h()V

    .line 221
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->s:Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 278
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 283
    :goto_1
    return-void

    .line 277
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 282
    :cond_1
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->v()V

    goto :goto_1
.end method

.method public e()V
    .locals 2

    .prologue
    .line 523
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->a:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 525
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 526
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->d:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 527
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 529
    iget-boolean v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->u:Z

    if-eqz v0, :cond_0

    .line 530
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->x()V

    .line 532
    :cond_0
    return-void
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 628
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isShown()Z

    move-result v0

    return v0
.end method

.method public g()V
    .locals 4

    .prologue
    .line 667
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->setVisibility(I)V

    .line 668
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->o:Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$b;

    invoke-virtual {p0}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->getMessageId()J

    move-result-wide v2

    iget-boolean v1, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->c:Z

    invoke-interface {v0, v2, v3, v1}, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView$b;->a(JZ)V

    .line 669
    return-void
.end method

.method getLayoutResId()I
    .locals 1

    .prologue
    .line 297
    const v0, 0x7f0400ca

    return v0
.end method

.method public getMessageId()J
    .locals 2

    .prologue
    .line 386
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->s:Lcom/twitter/model/dms/e;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e;

    invoke-virtual {v0}, Lcom/twitter/model/dms/e;->q()J

    move-result-wide v0

    return-wide v0
.end method

.method public setDraftStatusText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/twitter/app/dm/widget/SentMessageReadReceiptsBylineView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 392
    return-void
.end method
