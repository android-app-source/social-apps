.class public Lcom/twitter/app/dm/widget/c;
.super Lcom/twitter/app/dm/widget/b;
.source "Twttr"

# interfaces
.implements Laow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/dm/widget/b;",
        "Laow",
        "<",
        "Lcdv;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

.field private final d:J

.field private final e:I

.field private f:Lcom/twitter/android/media/stickers/data/a;

.field private g:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>(Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;Landroid/view/View;JIILcom/twitter/android/media/stickers/data/a;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 52
    invoke-direct {p0, p1, p2}, Lcom/twitter/app/dm/widget/b;-><init>(Landroid/view/View;Landroid/view/View;)V

    .line 53
    iput-wide p3, p0, Lcom/twitter/app/dm/widget/c;->d:J

    .line 54
    iput-object p7, p0, Lcom/twitter/app/dm/widget/c;->f:Lcom/twitter/android/media/stickers/data/a;

    .line 55
    iput p6, p0, Lcom/twitter/app/dm/widget/c;->e:I

    .line 56
    iput-object p1, p0, Lcom/twitter/app/dm/widget/c;->c:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    .line 58
    iget-object v0, p0, Lcom/twitter/app/dm/widget/c;->c:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;->setShouldShowRecentlyUsedFirstIfExists(Z)V

    .line 61
    if-eq p5, v1, :cond_0

    const/4 v0, 0x2

    if-eq p5, v0, :cond_0

    .line 63
    new-instance v0, Lcom/twitter/app/dm/widget/c$1;

    invoke-direct {v0, p0, p5}, Lcom/twitter/app/dm/widget/c$1;-><init>(Lcom/twitter/app/dm/widget/c;I)V

    invoke-virtual {p2, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 71
    :cond_0
    const/4 v0, 0x5

    if-eq p5, v0, :cond_1

    .line 72
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/c;->i()V

    .line 74
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/widget/c;)Lcom/twitter/android/media/stickers/data/a;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/app/dm/widget/c;->f:Lcom/twitter/android/media/stickers/data/a;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/app/dm/widget/c;)Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/twitter/app/dm/widget/c;->c:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    return-object v0
.end method

.method private i()V
    .locals 6

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/app/dm/widget/c;->f:Lcom/twitter/android/media/stickers/data/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/widget/c;->f:Lcom/twitter/android/media/stickers/data/a;

    .line 96
    :goto_0
    iput-object v0, p0, Lcom/twitter/app/dm/widget/c;->f:Lcom/twitter/android/media/stickers/data/a;

    .line 99
    iget-object v0, p0, Lcom/twitter/app/dm/widget/c;->c:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    new-instance v1, Lcom/twitter/app/dm/widget/c$2;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/widget/c$2;-><init>(Lcom/twitter/app/dm/widget/c;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;->setRetryStickerCatalogListener(Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView$a;)V

    .line 106
    iget-object v0, p0, Lcom/twitter/app/dm/widget/c;->f:Lcom/twitter/android/media/stickers/data/a;

    new-instance v1, Lcom/twitter/app/dm/widget/c$3;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/widget/c$3;-><init>(Lcom/twitter/app/dm/widget/c;)V

    invoke-virtual {v0, p0, v1}, Lcom/twitter/android/media/stickers/data/a;->a(Laow;Lcom/twitter/android/media/stickers/data/a$b;)V

    .line 113
    return-void

    .line 95
    :cond_0
    new-instance v0, Lcom/twitter/android/media/stickers/data/a;

    iget-object v1, p0, Lcom/twitter/app/dm/widget/c;->a:Landroid/view/View;

    .line 96
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/app/dm/widget/c;->d:J

    new-instance v4, Lbrn;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Lbrn;-><init>(I)V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/media/stickers/data/a;-><init>(Landroid/content/Context;JLbrn;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lcdu;)V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/twitter/app/dm/widget/c;->f:Lcom/twitter/android/media/stickers/data/a;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/twitter/app/dm/widget/c;->f:Lcom/twitter/android/media/stickers/data/a;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/stickers/data/a;->a(Lcdu;)V

    .line 122
    :cond_0
    return-void
.end method

.method public a(Lcdv;)V
    .locals 3

    .prologue
    .line 153
    iget-object v0, p0, Lcom/twitter/app/dm/widget/c;->c:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    iget v1, p0, Lcom/twitter/app/dm/widget/c;->e:I

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;->a(Lcdv;IZ)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/widget/c;->g:Landroid/support/v4/view/ViewPager;

    .line 154
    return-void
.end method

.method public a(Lcom/twitter/android/media/imageeditor/stickers/c$b;)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/twitter/app/dm/widget/c;->c:Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;->setStickerSelectedListener(Lcom/twitter/android/media/imageeditor/stickers/c$b;)V

    .line 89
    return-void
.end method

.method public synthetic b_(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lcdv;

    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/widget/c;->a(Lcdv;)V

    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/twitter/app/dm/widget/c;->f:Lcom/twitter/android/media/stickers/data/a;

    if-nez v0, :cond_0

    .line 127
    invoke-direct {p0}, Lcom/twitter/app/dm/widget/c;->i()V

    .line 129
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/dm/widget/b;->d()V

    .line 130
    return-void
.end method

.method public e()V
    .locals 4

    .prologue
    .line 145
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/widget/c;->d:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:thread:standalone_sticker_catalog::cancel"

    aput-object v3, v1, v2

    .line 146
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 145
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 148
    invoke-super {p0}, Lcom/twitter/app/dm/widget/b;->e()V

    .line 149
    return-void
.end method

.method public g()I
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/app/dm/widget/c;->g:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/widget/c;->g:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/widget/c;->a(Lcom/twitter/android/media/imageeditor/stickers/c$b;)V

    .line 137
    iget-object v0, p0, Lcom/twitter/app/dm/widget/c;->f:Lcom/twitter/android/media/stickers/data/a;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/twitter/app/dm/widget/c;->f:Lcom/twitter/android/media/stickers/data/a;

    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/data/a;->b()V

    .line 140
    :cond_0
    return-void
.end method
