.class public Lcom/twitter/app/dm/ShareViaDMActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"


# instance fields
.field private a:Z

.field private b:Lcom/twitter/model/core/r;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/TwitterUser;",
            ">;"
        }
    .end annotation
.end field

.field private d:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Lcom/twitter/app/dm/ShareViaDMComposeFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    :goto_0
    return-void

    .line 58
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/app/dm/ShareViaDMActivity;->a:Z

    if-nez v0, :cond_1

    .line 59
    invoke-direct {p0}, Lcom/twitter/app/dm/ShareViaDMActivity;->j()V

    goto :goto_0

    .line 61
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v1, "share_via_dm_fragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 62
    instance-of v1, v0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;

    if-eqz v1, :cond_2

    .line 63
    check-cast v0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;

    iput-object v0, p0, Lcom/twitter/app/dm/ShareViaDMActivity;->i:Lcom/twitter/app/dm/ShareViaDMComposeFragment;

    goto :goto_0

    .line 65
    :cond_2
    invoke-direct {p0}, Lcom/twitter/app/dm/ShareViaDMActivity;->j()V

    goto :goto_0
.end method

.method private j()V
    .locals 4

    .prologue
    .line 71
    new-instance v0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;

    invoke-direct {v0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/dm/ShareViaDMActivity;->i:Lcom/twitter/app/dm/ShareViaDMComposeFragment;

    .line 72
    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMActivity;->i:Lcom/twitter/app/dm/ShareViaDMComposeFragment;

    new-instance v0, Lcom/twitter/app/dm/r$a;

    invoke-direct {v0}, Lcom/twitter/app/dm/r$a;-><init>()V

    iget-object v2, p0, Lcom/twitter/app/dm/ShareViaDMActivity;->b:Lcom/twitter/model/core/r;

    .line 73
    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/r$a;->a(Lcom/twitter/model/core/r;)Lcom/twitter/app/dm/r$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/app/dm/ShareViaDMActivity;->c:Ljava/util/List;

    .line 74
    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/r$a;->a(Ljava/util/List;)Lcom/twitter/app/dm/r$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/app/dm/ShareViaDMActivity;->g:Ljava/lang/String;

    .line 75
    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/r$a;->a(Ljava/lang/String;)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/r$a;

    iget-boolean v2, p0, Lcom/twitter/app/dm/ShareViaDMActivity;->f:Z

    .line 76
    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/r$a;->c(Z)Lcom/twitter/app/dm/r$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/app/dm/ShareViaDMActivity;->e:Ljava/lang/String;

    .line 77
    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/r$a;->b(Ljava/lang/String;)Lcom/twitter/app/dm/r$a;

    move-result-object v0

    iget v2, p0, Lcom/twitter/app/dm/ShareViaDMActivity;->d:I

    .line 78
    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/r$a;->a(I)Lcom/twitter/app/dm/r$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/app/dm/ShareViaDMActivity;->h:Ljava/lang/String;

    .line 79
    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/r$a;->c(Ljava/lang/String;)Lcom/twitter/app/dm/r$a;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lcom/twitter/app/dm/r$a;->e()Lcom/twitter/app/dm/r;

    move-result-object v0

    .line 72
    invoke-virtual {v1, v0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->a(Lcom/twitter/app/common/base/b;)V

    .line 81
    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f13043b

    iget-object v2, p0, Lcom/twitter/app/dm/ShareViaDMActivity;->i:Lcom/twitter/app/dm/ShareViaDMComposeFragment;

    const-string/jumbo v3, "share_via_dm_fragment"

    .line 82
    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 84
    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->h()V

    .line 85
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 108
    if-eqz v0, :cond_0

    .line 109
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    .line 111
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 116
    const v0, 0x7f0403b0

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 117
    const/16 v0, 0xc

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(I)V

    .line 118
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(Z)V

    .line 119
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 120
    return-object p2
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 2

    .prologue
    .line 37
    new-instance v1, Lcom/twitter/app/dm/r;

    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/twitter/app/dm/r;-><init>(Landroid/content/Intent;)V

    .line 38
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/app/dm/ShareViaDMActivity;->a:Z

    .line 39
    invoke-virtual {v1}, Lcom/twitter/app/dm/r;->f()Lcom/twitter/model/core/r;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/ShareViaDMActivity;->b:Lcom/twitter/model/core/r;

    .line 40
    invoke-virtual {v1}, Lcom/twitter/app/dm/r;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/ShareViaDMActivity;->c:Ljava/util/List;

    .line 41
    invoke-virtual {v1}, Lcom/twitter/app/dm/r;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/ShareViaDMActivity;->g:Ljava/lang/String;

    .line 42
    invoke-virtual {v1}, Lcom/twitter/app/dm/r;->j()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/ShareViaDMActivity;->f:Z

    .line 43
    invoke-virtual {v1}, Lcom/twitter/app/dm/r;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/ShareViaDMActivity;->e:Ljava/lang/String;

    .line 44
    invoke-virtual {v1}, Lcom/twitter/app/dm/r;->x()I

    move-result v0

    iput v0, p0, Lcom/twitter/app/dm/ShareViaDMActivity;->d:I

    .line 45
    invoke-virtual {v1}, Lcom/twitter/app/dm/r;->y()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/ShareViaDMActivity;->h:Ljava/lang/String;

    .line 47
    invoke-direct {p0}, Lcom/twitter/app/dm/ShareViaDMActivity;->i()V

    .line 50
    invoke-static {}, Lcom/twitter/library/dm/d;->a()Z

    .line 51
    return-void

    .line 38
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x1

    return v0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 93
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onBackPressed()V

    .line 94
    const v0, 0x7f05003c

    const v1, 0x7f05003d

    invoke-virtual {p0, v0, v1}, Lcom/twitter/app/dm/ShareViaDMActivity;->overridePendingTransition(II)V

    .line 95
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMActivity;->i:Lcom/twitter/app/dm/ShareViaDMComposeFragment;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMActivity;->i:Lcom/twitter/app/dm/ShareViaDMComposeFragment;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->w()V

    .line 98
    :cond_0
    return-void
.end method

.method protected p()V
    .locals 0

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMActivity;->onBackPressed()V

    .line 103
    invoke-direct {p0}, Lcom/twitter/app/dm/ShareViaDMActivity;->l()V

    .line 104
    return-void
.end method

.method protected p_()V
    .locals 0

    .prologue
    .line 89
    return-void
.end method
