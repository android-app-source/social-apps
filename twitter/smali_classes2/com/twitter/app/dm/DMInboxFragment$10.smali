.class Lcom/twitter/app/dm/DMInboxFragment$10;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/common/dialog/b$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/dm/DMInboxFragment;->b(Lcom/twitter/model/dms/q;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Z

.field final synthetic d:Lcom/twitter/app/dm/DMInboxFragment;


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/DMInboxFragment;ZLjava/lang/String;Z)V
    .locals 0

    .prologue
    .line 596
    iput-object p1, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->d:Lcom/twitter/app/dm/DMInboxFragment;

    iput-boolean p2, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->a:Z

    iput-object p3, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->b:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;II)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 599
    packed-switch p3, :pswitch_data_0

    .line 639
    :goto_0
    return-void

    .line 601
    :pswitch_0
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->a:Z

    iget-object v2, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->b:Ljava/lang/String;

    const-string/jumbo v3, "inbox"

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/app/dm/dialog/DeleteConversationDialog;->a(IZLjava/lang/String;Ljava/lang/String;)Lcom/twitter/app/dm/dialog/DeleteConversationDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->d:Lcom/twitter/app/dm/DMInboxFragment;

    .line 603
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/dialog/DeleteConversationDialog;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->d:Lcom/twitter/app/dm/DMInboxFragment;

    .line 604
    invoke-virtual {v1}, Lcom/twitter/app/dm/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0

    .line 608
    :pswitch_1
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->c:Z

    if-eqz v0, :cond_1

    .line 609
    invoke-static {}, Lcom/twitter/library/dm/d;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 611
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->b:Ljava/lang/String;

    const-string/jumbo v2, "inbox"

    invoke-static {v0, v1, v2}, Lcom/twitter/app/dm/dialog/MuteConversationDialog;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/twitter/app/dm/dialog/MuteConversationDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->d:Lcom/twitter/app/dm/DMInboxFragment;

    .line 613
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/dialog/MuteConversationDialog;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->d:Lcom/twitter/app/dm/DMInboxFragment;

    .line 614
    invoke-virtual {v1}, Lcom/twitter/app/dm/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0

    .line 616
    :cond_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->d:Lcom/twitter/app/dm/DMInboxFragment;

    invoke-static {v1}, Lcom/twitter/app/dm/DMInboxFragment;->f(Lcom/twitter/app/dm/DMInboxFragment;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "messages:inbox::thread:mute_dm_thread"

    aput-object v2, v1, v5

    .line 617
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 616
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 618
    iget-object v0, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->d:Lcom/twitter/app/dm/DMInboxFragment;

    new-instance v1, Lcom/twitter/library/api/dm/u;

    iget-object v2, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->d:Lcom/twitter/app/dm/DMInboxFragment;

    invoke-virtual {v2}, Lcom/twitter/app/dm/DMInboxFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->d:Lcom/twitter/app/dm/DMInboxFragment;

    invoke-static {v3}, Lcom/twitter/app/dm/DMInboxFragment;->g(Lcom/twitter/app/dm/DMInboxFragment;)Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4, v6}, Lcom/twitter/library/api/dm/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Z)V

    const/4 v2, 0x3

    invoke-static {v0, v1, v2, v5}, Lcom/twitter/app/dm/DMInboxFragment;->a(Lcom/twitter/app/dm/DMInboxFragment;Lcom/twitter/library/service/s;II)Z

    .line 620
    iget-object v0, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->d:Lcom/twitter/app/dm/DMInboxFragment;

    invoke-virtual {v0}, Lcom/twitter/app/dm/DMInboxFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a02bb

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 623
    :cond_1
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->d:Lcom/twitter/app/dm/DMInboxFragment;

    invoke-static {v1}, Lcom/twitter/app/dm/DMInboxFragment;->h(Lcom/twitter/app/dm/DMInboxFragment;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "messages:inbox::thread:unmute_dm_thread"

    aput-object v2, v1, v5

    .line 624
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 623
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 625
    iget-object v0, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->d:Lcom/twitter/app/dm/DMInboxFragment;

    new-instance v1, Lcom/twitter/library/api/dm/u;

    iget-object v2, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->d:Lcom/twitter/app/dm/DMInboxFragment;

    invoke-virtual {v2}, Lcom/twitter/app/dm/DMInboxFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->d:Lcom/twitter/app/dm/DMInboxFragment;

    invoke-static {v3}, Lcom/twitter/app/dm/DMInboxFragment;->i(Lcom/twitter/app/dm/DMInboxFragment;)Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/twitter/library/api/dm/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Z)V

    const/4 v2, 0x4

    invoke-static {v0, v1, v2, v5}, Lcom/twitter/app/dm/DMInboxFragment;->b(Lcom/twitter/app/dm/DMInboxFragment;Lcom/twitter/library/service/s;II)Z

    .line 627
    iget-object v0, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->d:Lcom/twitter/app/dm/DMInboxFragment;

    invoke-virtual {v0}, Lcom/twitter/app/dm/DMInboxFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a02bc

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 632
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->d:Lcom/twitter/app/dm/DMInboxFragment;

    iget-object v1, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->b:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/twitter/app/dm/DMInboxFragment$10;->a:Z

    invoke-static {v0, v1, v2}, Lcom/twitter/app/dm/DMInboxFragment;->a(Lcom/twitter/app/dm/DMInboxFragment;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 599
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
