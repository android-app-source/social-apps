.class public abstract Lcom/twitter/app/dm/b$a;
.super Lcom/twitter/app/common/list/i$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/dm/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<U:",
        "Lcom/twitter/app/dm/b$a",
        "<TU;>;>",
        "Lcom/twitter/app/common/list/i$a",
        "<TU;>;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/twitter/app/common/list/i$a;-><init>()V

    .line 84
    return-void
.end method

.method protected constructor <init>(Landroid/os/Bundle;I)V
    .locals 2

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/twitter/app/common/list/i$a;-><init>(Landroid/os/Bundle;)V

    .line 88
    iget-object v0, p0, Lcom/twitter/app/dm/b$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "intent_type"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 89
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/twitter/app/dm/b$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TU;"
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/twitter/app/dm/b$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "initial_text"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/b$a;

    return-object v0
.end method

.method public a(Z)Lcom/twitter/app/dm/b$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TU;"
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/twitter/app/dm/b$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "is_keyboard_open"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 94
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/b$a;

    return-object v0
.end method

.method public a()Lcom/twitter/app/dm/b;
    .locals 2

    .prologue
    .line 112
    new-instance v0, Lcom/twitter/app/dm/b;

    iget-object v1, p0, Lcom/twitter/app/dm/b$a;->a:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Lcom/twitter/app/dm/b;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method

.method public synthetic b()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/twitter/app/dm/b$a;->a()Lcom/twitter/app/dm/b;

    move-result-object v0

    return-object v0
.end method

.method public b(Z)Lcom/twitter/app/dm/b$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TU;"
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lcom/twitter/app/dm/b$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "is_from_external_url"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 100
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/b$a;

    return-object v0
.end method

.method public synthetic c()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/twitter/app/dm/b$a;->a()Lcom/twitter/app/dm/b;

    move-result-object v0

    return-object v0
.end method
