.class public Lcom/twitter/app/dm/k;
.super Lcjr;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcjr",
        "<",
        "Lcom/twitter/model/dms/q;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcjr;-><init>(Landroid/content/Context;)V

    .line 24
    return-void
.end method


# virtual methods
.method a(Lcom/twitter/model/dms/q;)J
    .locals 5
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const-wide/16 v2, -0x1

    .line 58
    if-nez p1, :cond_0

    move-wide v0, v2

    .line 70
    :goto_0
    return-wide v0

    .line 62
    :cond_0
    iget-object v0, p1, Lcom/twitter/model/dms/q;->b:Ljava/lang/String;

    .line 64
    const-string/jumbo v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 65
    const-string/jumbo v1, "-"

    const/4 v4, 0x2

    invoke-virtual {v0, v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    .line 66
    const/4 v1, 0x1

    aget-object v0, v0, v1

    .line 70
    :cond_1
    invoke-static {v0, v2, v3}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method protected a(Landroid/content/Context;Lcom/twitter/model/dms/q;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/twitter/app/dm/k;->j()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400bb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 29
    new-instance v1, Laps$c;

    invoke-direct {v1, v0}, Laps$c;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 30
    return-object v0
.end method

.method protected bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 21
    check-cast p2, Lcom/twitter/model/dms/q;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/app/dm/k;->a(Landroid/content/Context;Lcom/twitter/model/dms/q;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/model/dms/q;)V
    .locals 2

    .prologue
    .line 35
    new-instance v1, Laps$a;

    invoke-direct {v1}, Laps$a;-><init>()V

    .line 36
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laps$c;

    invoke-virtual {v1, v0}, Laps$a;->a(Laps$c;)Laps$a;

    move-result-object v0

    .line 37
    invoke-virtual {v0, p3}, Laps$a;->a(Lcom/twitter/model/dms/q;)Laps$a;

    move-result-object v0

    .line 38
    invoke-virtual {p0}, Lcom/twitter/app/dm/k;->j()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Laps$a;->a(Landroid/content/Context;)Laps$a;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Laps$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laps;

    .line 40
    invoke-virtual {v0}, Laps;->a()V

    .line 41
    return-void
.end method

.method protected bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 21
    check-cast p3, Lcom/twitter/model/dms/q;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/app/dm/k;->a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/model/dms/q;)V

    return-void
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/k;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/q;

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/k;->a(Lcom/twitter/model/dms/q;)J

    move-result-wide v0

    return-wide v0
.end method
