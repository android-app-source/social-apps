.class public Lcom/twitter/app/dm/m;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/m$a;
    }
.end annotation


# instance fields
.field private final a:J

.field private b:Lcom/twitter/app/dm/m$a;

.field private c:Z

.field private d:Z

.field private e:J

.field private f:I


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/app/dm/m;-><init>(JLcom/twitter/app/common/list/i;)V

    .line 33
    return-void
.end method

.method public constructor <init>(JLcom/twitter/app/common/list/i;)V
    .locals 3

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-wide p1, p0, Lcom/twitter/app/dm/m;->a:J

    .line 38
    if-eqz p3, :cond_0

    .line 39
    const-string/jumbo v0, "last_read_marker_unread_messages_count"

    invoke-virtual {p3, v0}, Lcom/twitter/app/common/list/i;->b(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/app/dm/m;->f:I

    .line 40
    iget v0, p0, Lcom/twitter/app/dm/m;->f:I

    if-lez v0, :cond_0

    .line 41
    const-string/jumbo v0, "last_read_marker_event_id"

    invoke-virtual {p3, v0}, Lcom/twitter/app/common/list/i;->c(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/app/dm/m;->e:J

    .line 42
    const-string/jumbo v0, "last_read_marker_is_invalid"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/m;->d:Z

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/dm/m;->c:Z

    .line 46
    :cond_0
    return-void
.end method

.method private a(Lcom/twitter/model/dms/m;)Z
    .locals 1

    .prologue
    .line 118
    iget-boolean v0, p1, Lcom/twitter/model/dms/m;->e:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/model/dms/m;->c()Lcom/twitter/model/dms/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/model/dms/c;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(Ljava/util/Iterator;)I
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "Lcom/twitter/model/dms/m;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 114
    iget-wide v0, p0, Lcom/twitter/app/dm/m;->a:J

    invoke-static {p1, v0, v1}, Lcom/twitter/library/dm/e;->a(Ljava/util/Iterator;J)I

    move-result v0

    return v0
.end method

.method public a()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 123
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 124
    const-string/jumbo v1, "last_read_marker_unread_messages_count"

    iget v2, p0, Lcom/twitter/app/dm/m;->f:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 125
    const-string/jumbo v1, "last_read_marker_event_id"

    iget-wide v2, p0, Lcom/twitter/app/dm/m;->e:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 126
    const-string/jumbo v1, "last_read_marker_is_invalid"

    iget-boolean v2, p0, Lcom/twitter/app/dm/m;->d:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 127
    return-object v0
.end method

.method public a(Lcbi;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/model/dms/m;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/twitter/app/dm/m;->c:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/dm/m;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/m;->b:Lcom/twitter/app/dm/m$a;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcbi;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 53
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/dm/m;->c:Z

    .line 55
    iget v0, p0, Lcom/twitter/app/dm/m;->f:I

    if-gtz v0, :cond_0

    .line 60
    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/m;->b(Lcbi;)Lcom/twitter/model/dms/m;

    move-result-object v0

    .line 61
    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {v0}, Lcom/twitter/model/dms/m;->c()Lcom/twitter/model/dms/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/model/dms/c;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/app/dm/m;->e:J

    .line 64
    invoke-virtual {v0}, Lcom/twitter/model/dms/m;->a()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lcom/twitter/library/dm/e;->a(Lcbi;J)Ljava/util/Iterator;

    move-result-object v0

    .line 65
    if-eqz v0, :cond_2

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/m;->a(Ljava/util/Iterator;)I

    move-result v0

    :goto_1
    iput v0, p0, Lcom/twitter/app/dm/m;->f:I

    .line 66
    iget v0, p0, Lcom/twitter/app/dm/m;->f:I

    if-lez v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/twitter/app/dm/m;->b:Lcom/twitter/app/dm/m$a;

    invoke-interface {v0}, Lcom/twitter/app/dm/m$a;->b()V

    goto :goto_0

    .line 65
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Lcom/twitter/app/dm/m$a;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/twitter/app/dm/m;->b:Lcom/twitter/app/dm/m$a;

    .line 132
    return-void
.end method

.method public a(J)Z
    .locals 3

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/twitter/app/dm/m;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/app/dm/m;->e:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b(Lcbi;)Lcom/twitter/model/dms/m;
    .locals 5
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/model/dms/m;",
            ">;)",
            "Lcom/twitter/model/dms/m;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 75
    invoke-virtual {p1}, Lcbi;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v2

    .line 95
    :cond_0
    :goto_0
    return-object v0

    .line 79
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcbi;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/m;

    .line 80
    if-nez v0, :cond_2

    move-object v0, v2

    .line 81
    goto :goto_0

    .line 84
    :cond_2
    invoke-direct {p0, v0}, Lcom/twitter/app/dm/m;->a(Lcom/twitter/model/dms/m;)Z

    move-result v0

    .line 85
    invoke-virtual {p1}, Lcbi;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/m;

    .line 86
    if-eqz v1, :cond_3

    iget-boolean v4, v0, Lcom/twitter/model/dms/m;->e:Z

    if-nez v4, :cond_0

    .line 90
    :cond_3
    invoke-direct {p0, v0}, Lcom/twitter/app/dm/m;->a(Lcom/twitter/model/dms/m;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 91
    const/4 v0, 0x1

    :goto_2
    move v1, v0

    .line 93
    goto :goto_1

    :cond_4
    move-object v0, v2

    .line 95
    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/twitter/app/dm/m;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/dm/m;->d:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/app/dm/m;->f:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lcom/twitter/app/dm/m;->f:I

    return v0
.end method

.method public c(Lcbi;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/model/dms/m;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 99
    if-eqz p1, :cond_1

    .line 100
    const/4 v0, 0x0

    .line 101
    invoke-virtual {p1}, Lcbi;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/m;

    .line 102
    invoke-virtual {v0}, Lcom/twitter/model/dms/m;->a()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/twitter/app/dm/m;->e:J

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    .line 109
    :goto_1
    return v1

    .line 105
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 106
    goto :goto_0

    .line 109
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 147
    iget-boolean v0, p0, Lcom/twitter/app/dm/m;->c:Z

    return v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/dm/m;->d:Z

    .line 156
    return-void
.end method
