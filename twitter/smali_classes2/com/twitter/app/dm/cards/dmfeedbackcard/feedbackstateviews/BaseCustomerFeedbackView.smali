.class public abstract Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;
.super Landroid/widget/RelativeLayout;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView$a;
    }
.end annotation


# static fields
.field protected static final a:Landroid/graphics/Typeface;


# instance fields
.field protected final b:Lcom/twitter/app/dm/cards/dmfeedbackcard/c;

.field protected final c:Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView$a;

.field protected final d:Ljava/lang/String;

.field private final e:Lcom/twitter/app/dm/cards/dmfeedbackcard/e;

.field private final f:Ljava/lang/String;

.field private final g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    const-string/jumbo v0, "sans-serif"

    const/4 v1, 0x0

    .line 41
    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->a:Landroid/graphics/Typeface;

    .line 40
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/twitter/app/dm/cards/dmfeedbackcard/c;Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView$a;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 58
    iput-object p2, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->b:Lcom/twitter/app/dm/cards/dmfeedbackcard/c;

    .line 59
    iput-object p3, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->c:Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView$a;

    .line 60
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->b:Lcom/twitter/app/dm/cards/dmfeedbackcard/c;

    invoke-virtual {v0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/c;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/twitter/app/dm/cards/dmfeedbackcard/e;->a(J)Lcom/twitter/app/dm/cards/dmfeedbackcard/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->e:Lcom/twitter/app/dm/cards/dmfeedbackcard/e;

    .line 61
    invoke-virtual {p2}, Lcom/twitter/app/dm/cards/dmfeedbackcard/c;->q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->d:Ljava/lang/String;

    .line 62
    iput-object p4, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->f:Ljava/lang/String;

    .line 63
    invoke-static {}, Lcom/twitter/app/dm/cards/dmfeedbackcard/a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->g:Z

    .line 64
    invoke-virtual {p0, p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 65
    return-void
.end method

.method private static a(Landroid/text/SpannableString;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 104
    invoke-virtual {p1, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    :goto_0
    if-ltz v0, :cond_0

    .line 105
    new-instance v1, Lcom/twitter/android/util/NoUnderlineURLSpan;

    invoke-direct {v1, p3}, Lcom/twitter/android/util/NoUnderlineURLSpan;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v0

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v0, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 104
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, p2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    .line 107
    :cond_0
    return-void
.end method

.method private c()Landroid/text/SpannableString;
    .locals 7

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 112
    const v1, 0x7f0a0391

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 113
    const v2, 0x7f0a038e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 114
    const v3, 0x7f0a038f

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->b:Lcom/twitter/app/dm/cards/dmfeedbackcard/c;

    .line 115
    invoke-virtual {v6}, Lcom/twitter/app/dm/cards/dmfeedbackcard/c;->m()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    const/4 v5, 0x2

    aput-object v2, v4, v5

    .line 114
    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 118
    new-instance v4, Landroid/text/SpannableString;

    invoke-direct {v4, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->b:Lcom/twitter/app/dm/cards/dmfeedbackcard/c;

    .line 121
    invoke-virtual {v0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/c;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 120
    invoke-static {v4, v3, v1, v0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->a(Landroid/text/SpannableString;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const-string/jumbo v0, "https://support.twitter.com/articles/20174629"

    invoke-static {v4, v3, v2, v0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->a(Landroid/text/SpannableString;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    return-object v4
.end method

.method private d()Landroid/text/SpannableString;
    .locals 6

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 129
    const v1, 0x7f0a0391

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 130
    const v2, 0x7f0a0390

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->b:Lcom/twitter/app/dm/cards/dmfeedbackcard/c;

    .line 132
    invoke-virtual {v5}, Lcom/twitter/app/dm/cards/dmfeedbackcard/c;->m()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v1, v3, v4

    .line 130
    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 134
    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 136
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->b:Lcom/twitter/app/dm/cards/dmfeedbackcard/c;

    .line 137
    invoke-virtual {v0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/c;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 136
    invoke-static {v3, v2, v1, v0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->a(Landroid/text/SpannableString;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    return-object v3
.end method


# virtual methods
.method protected a()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 68
    const v0, 0x7f130034

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 69
    if-eqz v1, :cond_0

    .line 71
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 72
    invoke-static {}, Lcom/twitter/util/s;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 73
    const/16 v2, 0x9

    invoke-virtual {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 77
    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 79
    new-instance v0, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView$1;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView$1;-><init>(Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    :cond_0
    return-void

    .line 75
    :cond_1
    const/16 v2, 0xb

    invoke-virtual {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_0
.end method

.method protected a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 144
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->e:Lcom/twitter/app/dm/cards/dmfeedbackcard/e;

    iget-object v1, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1}, Lcom/twitter/app/dm/cards/dmfeedbackcard/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    return-void
.end method

.method protected b()V
    .locals 3

    .prologue
    .line 91
    const v0, 0x7f1302cc

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 92
    if-eqz v0, :cond_0

    .line 93
    iget-boolean v1, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->g:Z

    if-eqz v1, :cond_1

    .line 94
    invoke-direct {p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->d()Landroid/text/SpannableString;

    move-result-object v1

    .line 96
    :goto_0
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 97
    sget-object v2, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 98
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 100
    :cond_0
    return-void

    .line 94
    :cond_1
    invoke-direct {p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/feedbackstateviews/BaseCustomerFeedbackView;->c()Landroid/text/SpannableString;

    move-result-object v1

    goto :goto_0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x1

    return v0
.end method
