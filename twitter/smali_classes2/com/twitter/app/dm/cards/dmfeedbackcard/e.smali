.class public Lcom/twitter/app/dm/cards/dmfeedbackcard/e;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:J

.field private final b:J


# direct methods
.method private constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-wide p1, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/e;->a:J

    .line 17
    iput-wide p3, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/e;->b:J

    .line 18
    return-void
.end method

.method public static a(J)Lcom/twitter/app/dm/cards/dmfeedbackcard/e;
    .locals 4

    .prologue
    .line 22
    new-instance v0, Lcom/twitter/app/dm/cards/dmfeedbackcard/e;

    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3, p0, p1}, Lcom/twitter/app/dm/cards/dmfeedbackcard/e;-><init>(JJ)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 27
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/e;->a:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:thread"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    aput-object p2, v1, v2

    const/4 v2, 0x3

    aput-object p3, v1, v2

    .line 28
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 29
    iget-wide v2, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/e;->b:J

    invoke-static {v0, v2, v3, p1}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;JLjava/lang/String;)V

    .line 30
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 31
    return-void
.end method
