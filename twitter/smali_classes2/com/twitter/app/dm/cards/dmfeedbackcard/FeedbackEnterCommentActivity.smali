.class public Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/ui/widget/TwitterButton;

.field private b:Landroid/view/View;

.field private c:Landroid/widget/EditText;

.field private d:Lcom/twitter/library/customerservice/network/FeedbackRequestParams;

.field private e:Lcom/twitter/app/dm/cards/dmfeedbackcard/e;

.field private f:Z

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;Z)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 214
    if-eqz p1, :cond_0

    .line 215
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->e:Lcom/twitter/app/dm/cards/dmfeedbackcard/e;

    iget-object v1, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->g:Ljava/lang/String;

    const-string/jumbo v2, "comment_compose"

    const-string/jumbo v3, "dismiss"

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/app/dm/cards/dmfeedbackcard/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    invoke-virtual {p0, v4}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->setResult(I)V

    .line 223
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 224
    if-eqz v1, :cond_1

    .line 225
    const-string/jumbo v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 226
    if-eqz v0, :cond_1

    .line 227
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 230
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->finish()V

    .line 231
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;)Z
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->o()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;)Lcom/twitter/ui/widget/TwitterButton;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->a:Lcom/twitter/ui/widget/TwitterButton;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->b:Landroid/view/View;

    return-object v0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 182
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a038c

    .line 183
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a002b

    .line 184
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0278

    new-instance v2, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity$6;

    invoke-direct {v2, p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity$6;-><init>(Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;)V

    .line 185
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00f6

    new-instance v2, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity$5;

    invoke-direct {v2, p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity$5;-><init>(Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;)V

    .line 193
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 199
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 201
    new-instance v1, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity$7;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity$7;-><init>(Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 208
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 209
    return-void
.end method

.method private l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private n()Z
    .locals 1

    .prologue
    .line 239
    invoke-direct {p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()Z
    .locals 1

    .prologue
    .line 243
    iget-boolean v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->f:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 1

    .prologue
    .line 54
    const v0, 0x7f0400e7

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 55
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 56
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->a(Z)V

    .line 57
    return-object p2
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 64
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/app/common/abs/AbsFragmentActivity$a;)V

    .line 66
    invoke-virtual {p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 67
    const-string/jumbo v0, "feedback_associated_user_name_key"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 68
    const-string/jumbo v0, "feedback_scribe_component"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->g:Ljava/lang/String;

    .line 70
    const-string/jumbo v0, "feedback_request_params"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/customerservice/network/FeedbackRequestParams;

    iput-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->d:Lcom/twitter/library/customerservice/network/FeedbackRequestParams;

    .line 71
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->d:Lcom/twitter/library/customerservice/network/FeedbackRequestParams;

    invoke-virtual {v0}, Lcom/twitter/library/customerservice/network/FeedbackRequestParams;->d()Lcaq;

    move-result-object v0

    .line 72
    const-string/jumbo v3, "score"

    .line 73
    invoke-static {v3, v0}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    const/4 v3, -0x1

    .line 72
    invoke-static {v0, v3}, Lcom/twitter/util/y;->d(Ljava/lang/String;I)I

    .line 76
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->d:Lcom/twitter/library/customerservice/network/FeedbackRequestParams;

    invoke-virtual {v0}, Lcom/twitter/library/customerservice/network/FeedbackRequestParams;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/twitter/app/dm/cards/dmfeedbackcard/e;->a(J)Lcom/twitter/app/dm/cards/dmfeedbackcard/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->e:Lcom/twitter/app/dm/cards/dmfeedbackcard/e;

    .line 77
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->e:Lcom/twitter/app/dm/cards/dmfeedbackcard/e;

    iget-object v3, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->g:Ljava/lang/String;

    const-string/jumbo v4, "comment_compose"

    const-string/jumbo v5, "impression"

    invoke-virtual {v0, v3, v4, v5}, Lcom/twitter/app/dm/cards/dmfeedbackcard/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const v0, 0x7f0a0387

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->setTitle(I)V

    .line 85
    const v0, 0x7f130370

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->a:Lcom/twitter/ui/widget/TwitterButton;

    .line 86
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->a:Lcom/twitter/ui/widget/TwitterButton;

    .line 87
    invoke-virtual {p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0394

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 86
    invoke-virtual {v0, v3}, Lcom/twitter/ui/widget/TwitterButton;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->a:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, v6}, Lcom/twitter/ui/widget/TwitterButton;->setEnabled(Z)V

    .line 90
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->a:Lcom/twitter/ui/widget/TwitterButton;

    new-instance v3, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity$1;

    invoke-direct {v3, p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity$1;-><init>(Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;)V

    invoke-virtual {v0, v3}, Lcom/twitter/ui/widget/TwitterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    const-string/jumbo v0, "feedback_associated_score_description_key"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 99
    const v0, 0x7f13036f

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->c:Landroid/widget/EditText;

    .line 100
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->c:Landroid/widget/EditText;

    sget-object v3, Lcom/twitter/android/revenue/card/h;->b:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 101
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->c:Landroid/widget/EditText;

    .line 102
    invoke-virtual {p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0388

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    aput-object v1, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 101
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 104
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->c:Landroid/widget/EditText;

    new-instance v1, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity$2;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity$2;-><init>(Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 112
    const v0, 0x7f13036e

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->b:Landroid/view/View;

    .line 113
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->b:Landroid/view/View;

    new-instance v1, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity$3;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity$3;-><init>(Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    return-void
.end method

.method protected a(Lcom/twitter/library/service/s;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 154
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcom/twitter/library/service/s;I)V

    .line 155
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 157
    if-ne p2, v3, :cond_0

    .line 158
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 159
    iput-boolean v2, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->f:Z

    .line 160
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->a:Lcom/twitter/ui/widget/TwitterButton;

    invoke-direct {p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->o()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setEnabled(Z)V

    .line 161
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 162
    const v0, 0x7f0a0395

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 163
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    invoke-direct {p0, v2}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->a(Z)V

    goto :goto_0
.end method

.method public i()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 124
    invoke-direct {p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->l()Ljava/lang/String;

    move-result-object v0

    .line 125
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x2710

    if-gt v1, v2, :cond_0

    .line 126
    iget-object v1, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->e:Lcom/twitter/app/dm/cards/dmfeedbackcard/e;

    iget-object v2, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->g:Ljava/lang/String;

    const-string/jumbo v3, "comment_compose"

    const-string/jumbo v4, "submit"

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/app/dm/cards/dmfeedbackcard/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    iget-object v1, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->d:Lcom/twitter/library/customerservice/network/FeedbackRequestParams;

    invoke-virtual {v1, v0}, Lcom/twitter/library/customerservice/network/FeedbackRequestParams;->a(Ljava/lang/String;)Lcom/twitter/library/customerservice/network/FeedbackRequestParams;

    .line 131
    new-instance v1, Lcom/twitter/library/customerservice/network/b;

    invoke-virtual {p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->d:Lcom/twitter/library/customerservice/network/FeedbackRequestParams;

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/twitter/library/customerservice/network/b;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/customerservice/network/FeedbackRequestParams;Ljava/lang/String;)V

    .line 133
    iput-boolean v6, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->f:Z

    .line 134
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->a:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, v5}, Lcom/twitter/ui/widget/TwitterButton;->setEnabled(Z)V

    .line 135
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 136
    invoke-virtual {p0, v1, v6}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 150
    :goto_0
    return-void

    .line 138
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a038a

    .line 139
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a038b

    .line 140
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0616

    new-instance v2, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity$4;

    invoke-direct {v2, p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity$4;-><init>(Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;)V

    .line 141
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 147
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 148
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 172
    invoke-direct {p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 174
    iget-object v0, p0, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->a:Lcom/twitter/ui/widget/TwitterButton;

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/TwitterButton;->setEnabled(Z)V

    .line 175
    invoke-direct {p0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->j()V

    .line 179
    :goto_0
    return-void

    .line 177
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/app/dm/cards/dmfeedbackcard/FeedbackEnterCommentActivity;->a(Z)V

    goto :goto_0
.end method
