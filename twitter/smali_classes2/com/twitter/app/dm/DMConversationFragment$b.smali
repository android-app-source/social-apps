.class Lcom/twitter/app/dm/DMConversationFragment$b;
.super Lcom/twitter/library/network/livepipeline/a;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/dm/af;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/dm/DMConversationFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/dm/DMConversationFragment;

.field private final b:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final c:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final d:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final e:Ljava/util/concurrent/atomic/AtomicInteger;

.field private f:I


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/DMConversationFragment;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3222
    iput-object p1, p0, Lcom/twitter/app/dm/DMConversationFragment$b;->a:Lcom/twitter/app/dm/DMConversationFragment;

    .line 3223
    invoke-static {p1}, Lcom/twitter/app/dm/DMConversationFragment;->z(Lcom/twitter/app/dm/DMConversationFragment;)Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "lp:dm:::series"

    invoke-direct {p0, v0, v1}, Lcom/twitter/library/network/livepipeline/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 3224
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$b;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 3225
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$b;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 3226
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$b;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 3227
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$b;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 3228
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 3232
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$b;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 3233
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 3237
    invoke-static {}, Lcom/twitter/util/f;->a()V

    .line 3238
    iget v0, p0, Lcom/twitter/app/dm/DMConversationFragment$b;->f:I

    if-le p1, v0, :cond_0

    .line 3239
    iput p1, p0, Lcom/twitter/app/dm/DMConversationFragment$b;->f:I

    .line 3241
    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 3244
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$b;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 3245
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 3248
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$b;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 3249
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 3252
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$b;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 3253
    return-void
.end method

.method protected e()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3257
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "max_typing_users_count"

    iget v2, p0, Lcom/twitter/app/dm/DMConversationFragment$b;->f:I

    .line 3258
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "is_group_dm"

    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment$b;->a:Lcom/twitter/app/dm/DMConversationFragment;

    .line 3259
    invoke-static {v2}, Lcom/twitter/app/dm/DMConversationFragment;->A(Lcom/twitter/app/dm/DMConversationFragment;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "typing_events_received"

    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment$b;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 3260
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "new_message_events_received"

    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment$b;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 3261
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "typing_events_sent"

    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment$b;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 3262
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "new_message_events_sent"

    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment$b;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 3263
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "subscription_enabled"

    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment$b;->a:Lcom/twitter/app/dm/DMConversationFragment;

    .line 3264
    invoke-static {v2}, Lcom/twitter/app/dm/DMConversationFragment;->d(Lcom/twitter/app/dm/DMConversationFragment;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    const-string/jumbo v1, "publishing_enabled"

    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment$b;->a:Lcom/twitter/app/dm/DMConversationFragment;

    .line 3265
    invoke-static {v2}, Lcom/twitter/app/dm/DMConversationFragment;->d(Lcom/twitter/app/dm/DMConversationFragment;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    move-result-object v0

    .line 3266
    invoke-virtual {v0}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method
