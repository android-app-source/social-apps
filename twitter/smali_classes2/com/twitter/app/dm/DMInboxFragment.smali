.class public Lcom/twitter/app/dm/DMInboxFragment;
.super Lcom/twitter/app/common/list/TwitterListFragment;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation build Lcom/twitter/app/AutoSaveState;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/list/TwitterListFragment",
        "<",
        "Lcom/twitter/model/dms/q;",
        "Lcom/twitter/app/dm/k;",
        ">;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field a:Z
    .annotation build Lcom/twitter/app/SaveState;
    .end annotation
.end field

.field private c:Lcom/twitter/library/client/o;

.field private d:Landroid/widget/ListView;

.field private e:Lmu;

.field private f:Lapj;

.field private g:Z

.field private h:Lcom/twitter/ui/widget/PromptView;

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 120
    const/4 v0, 0x3

    .line 121
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const/4 v3, 0x2

    .line 122
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 120
    invoke-static {v0, v1}, Lcom/twitter/util/collection/o;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/twitter/app/dm/DMInboxFragment;->b:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/twitter/app/common/list/TwitterListFragment;-><init>()V

    .line 137
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMInboxFragment;->g:Z

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/DMInboxFragment;)J
    .locals 2

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/twitter/app/dm/DMInboxFragment;->a_:J

    return-wide v0
.end method

.method private a(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 170
    const v0, 0x7f13004d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 171
    new-instance v1, Lcom/twitter/app/dm/DMInboxFragment$1;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/DMInboxFragment$1;-><init>(Lcom/twitter/app/dm/DMInboxFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 186
    const v1, 0x7f1306bc

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 187
    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setScrollbarFadingEnabled(Z)V

    .line 189
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 191
    new-instance v1, Lcom/twitter/ui/widget/PromptView;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/ui/widget/PromptView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/twitter/app/dm/DMInboxFragment;->h:Lcom/twitter/ui/widget/PromptView;

    .line 192
    iget-object v1, p0, Lcom/twitter/app/dm/DMInboxFragment;->h:Lcom/twitter/ui/widget/PromptView;

    invoke-virtual {v1, v5}, Lcom/twitter/ui/widget/PromptView;->setIsHeader(Z)V

    .line 193
    iget-object v1, p0, Lcom/twitter/app/dm/DMInboxFragment;->h:Lcom/twitter/ui/widget/PromptView;

    invoke-virtual {v0, v1, v3, v4}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 195
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0400b9

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 196
    invoke-virtual {v0, v1, v3, v4}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 199
    iget-object v2, p0, Lcom/twitter/app/dm/DMInboxFragment;->h:Lcom/twitter/ui/widget/PromptView;

    new-instance v3, Lcom/twitter/app/dm/DMInboxFragment$3;

    invoke-direct {v3, p0}, Lcom/twitter/app/dm/DMInboxFragment$3;-><init>(Lcom/twitter/app/dm/DMInboxFragment;)V

    invoke-virtual {v2, v3}, Lcom/twitter/ui/widget/PromptView;->setOnPromptClickListener(Lcom/twitter/ui/widget/PromptView$a;)V

    .line 220
    const v2, 0x7f1302f0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/twitter/app/dm/DMInboxFragment$4;

    invoke-direct {v2, p0}, Lcom/twitter/app/dm/DMInboxFragment$4;-><init>(Lcom/twitter/app/dm/DMInboxFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 229
    iput-object v0, p0, Lcom/twitter/app/dm/DMInboxFragment;->d:Landroid/widget/ListView;

    .line 230
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/DMInboxFragment;Lcom/twitter/model/dms/q;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/DMInboxFragment;->b(Lcom/twitter/model/dms/q;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/DMInboxFragment;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Lcom/twitter/app/dm/DMInboxFragment;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method private a(Lcom/twitter/model/dms/q;)V
    .locals 6

    .prologue
    .line 577
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/dm/j$a;

    invoke-direct {v1}, Lcom/twitter/app/dm/j$a;-><init>()V

    iget-object v2, p1, Lcom/twitter/model/dms/q;->b:Ljava/lang/String;

    .line 578
    invoke-virtual {v1, v2}, Lcom/twitter/app/dm/j$a;->c(Ljava/lang/String;)Lcom/twitter/app/dm/j$a;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/dm/b;

    .line 579
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, p1, v3, v4, v5}, Lcom/twitter/library/dm/b;-><init>(Lcom/twitter/model/dms/q;Landroid/content/Context;J)V

    invoke-virtual {v2}, Lcom/twitter/library/dm/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/app/dm/j$a;->d(Ljava/lang/String;)Lcom/twitter/app/dm/j$a;

    move-result-object v1

    iget-object v2, p1, Lcom/twitter/model/dms/q;->d:Ljava/lang/String;

    .line 580
    invoke-virtual {v1, v2}, Lcom/twitter/app/dm/j$a;->e(Ljava/lang/String;)Lcom/twitter/app/dm/j$a;

    move-result-object v1

    iget-boolean v2, p1, Lcom/twitter/model/dms/q;->h:Z

    iget-object v3, p1, Lcom/twitter/model/dms/q;->i:Ljava/util/List;

    .line 581
    invoke-static {v2, v3}, Lcom/twitter/library/dm/e;->a(ZLjava/util/List;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/app/dm/j$a;->h(Z)Lcom/twitter/app/dm/j$a;

    move-result-object v1

    .line 582
    invoke-virtual {v1}, Lcom/twitter/app/dm/j$a;->e()Lcom/twitter/app/dm/j;

    move-result-object v1

    .line 577
    invoke-static {v0, v1}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/j;)Landroid/content/Intent;

    move-result-object v0

    .line 584
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->aq()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/app/common/base/h;->a(Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMInboxFragment;->startActivity(Landroid/content/Intent;)V

    .line 585
    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 647
    const/4 v0, 0x3

    const-string/jumbo v1, "inbox"

    new-instance v2, Lcom/twitter/app/dm/DMInboxFragment$2;

    invoke-direct {v2, p0, p2}, Lcom/twitter/app/dm/DMInboxFragment$2;-><init>(Lcom/twitter/app/dm/DMInboxFragment;Z)V

    invoke-static {v0, p2, p1, v1, v2}, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->a(IZLjava/lang/String;Ljava/lang/String;Lcom/twitter/app/dm/dialog/ReportConversationDialog$a;)Lcom/twitter/app/dm/dialog/ReportConversationDialog;

    move-result-object v0

    .line 675
    invoke-virtual {v0, p0}, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 676
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 677
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/DMInboxFragment;I)Z
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/DMInboxFragment;->b(I)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/twitter/app/dm/DMInboxFragment;Lcom/twitter/library/service/s;II)Z
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/app/dm/DMInboxFragment;->c(Lcom/twitter/library/service/s;II)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;ZZ)[Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 682
    invoke-static {p1}, Lcom/twitter/library/dm/e;->c(Ljava/lang/String;)Z

    move-result v3

    .line 683
    if-nez v3, :cond_2

    if-eqz p3, :cond_1

    const v0, 0x7f0a02f6

    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMInboxFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 685
    :goto_1
    if-nez v3, :cond_0

    const v0, 0x7f0a029d

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMInboxFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 686
    :cond_0
    if-eqz p2, :cond_3

    const v0, 0x7f0a054b

    :goto_2
    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMInboxFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 688
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    const/4 v2, 0x1

    aput-object v1, v3, v2

    invoke-static {v0, v3}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 693
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0

    .line 683
    :cond_1
    const v0, 0x7f0a02f7

    goto :goto_0

    :cond_2
    move-object v2, v1

    goto :goto_1

    .line 686
    :cond_3
    const v0, 0x7f0a0549

    goto :goto_2
.end method

.method static synthetic b(Lcom/twitter/app/dm/DMInboxFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/twitter/model/dms/q;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 589
    iget-object v1, p1, Lcom/twitter/model/dms/q;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/object/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 590
    iget-boolean v3, p1, Lcom/twitter/model/dms/q;->h:Z

    .line 592
    iget-boolean v1, p1, Lcom/twitter/model/dms/q;->j:Z

    if-nez v1, :cond_0

    move v1, v0

    .line 593
    :goto_0
    new-instance v4, Lcom/twitter/android/widget/aj$b;

    invoke-direct {v4, v0}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    .line 594
    invoke-direct {p0, v2, v3, v1}, Lcom/twitter/app/dm/DMInboxFragment;->a(Ljava/lang/String;ZZ)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/twitter/android/widget/aj$b;->a([Ljava/lang/CharSequence;)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 595
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PromptDialogFragment;

    .line 596
    new-instance v4, Lcom/twitter/app/dm/DMInboxFragment$10;

    invoke-direct {v4, p0, v3, v2, v1}, Lcom/twitter/app/dm/DMInboxFragment$10;-><init>(Lcom/twitter/app/dm/DMInboxFragment;ZLjava/lang/String;Z)V

    invoke-virtual {v0, v4}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    .line 642
    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 643
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 644
    return-void

    .line 592
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private b(Z)V
    .locals 1

    .prologue
    .line 463
    iput-boolean p1, p0, Lcom/twitter/app/dm/DMInboxFragment;->g:Z

    .line 464
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->aH()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->h()V

    .line 465
    return-void
.end method

.method private b(I)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 536
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 537
    sget-object v1, Lcom/twitter/app/dm/DMInboxFragment;->b:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 540
    new-instance v1, Lcom/twitter/library/api/dm/j;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/api/dm/j;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v0, 0x2

    const/4 v2, 0x3

    if-ne p1, v2, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-virtual {p0, v1, v0, p1}, Lcom/twitter/app/dm/DMInboxFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 546
    :goto_0
    return v3

    .line 543
    :cond_1
    new-instance v1, Lcom/twitter/library/api/dm/g;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/api/dm/g;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {p0, v1, v3, p1}, Lcom/twitter/app/dm/DMInboxFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/app/dm/DMInboxFragment;Lcom/twitter/library/service/s;II)Z
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/app/dm/DMInboxFragment;->c(Lcom/twitter/library/service/s;II)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/twitter/app/dm/DMInboxFragment;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/twitter/app/dm/DMInboxFragment;->t()V

    return-void
.end method

.method static synthetic d(Lcom/twitter/app/dm/DMInboxFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/app/dm/DMInboxFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/app/dm/DMInboxFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/app/dm/DMInboxFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/app/dm/DMInboxFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/app/dm/DMInboxFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private k()V
    .locals 7

    .prologue
    .line 366
    iget-object v0, p0, Lcom/twitter/app/dm/DMInboxFragment;->f:Lapj;

    if-nez v0, :cond_0

    .line 367
    new-instance v1, Lapj;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    .line 368
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    const/4 v6, 0x2

    invoke-direct/range {v1 .. v6}, Lapj;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;JI)V

    iput-object v1, p0, Lcom/twitter/app/dm/DMInboxFragment;->f:Lapj;

    .line 369
    iget-object v0, p0, Lcom/twitter/app/dm/DMInboxFragment;->f:Lapj;

    new-instance v1, Lcom/twitter/app/dm/DMInboxFragment$8;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/DMInboxFragment$8;-><init>(Lcom/twitter/app/dm/DMInboxFragment;)V

    invoke-virtual {v0, v1}, Lapj;->a(Lapj$a;)V

    .line 377
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/DMInboxFragment;->f:Lapj;

    invoke-virtual {v0}, Lapj;->a()V

    .line 378
    return-void
.end method

.method private q()V
    .locals 4

    .prologue
    .line 382
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v1, 0x7f1302f0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 386
    iget-object v1, p0, Lcom/twitter/app/dm/DMInboxFragment;->e:Lmu;

    invoke-virtual {v1}, Lmu;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 387
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 388
    const v0, 0x7f0a03a2

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMInboxFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 390
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/h;->a(Landroid/content/Context;)Lcom/twitter/library/util/g;

    move-result-object v0

    .line 391
    invoke-interface {v0}, Lcom/twitter/library/util/g;->b()Z

    move-result v0

    .line 392
    if-eqz v0, :cond_0

    .line 394
    const v0, 0x7f0a0291

    :goto_0
    move-object v3, v1

    move v1, v0

    move-object v0, v3

    .line 413
    :goto_1
    iget-object v2, p0, Lcom/twitter/app/dm/DMInboxFragment;->h:Lcom/twitter/ui/widget/PromptView;

    invoke-virtual {v2, v1}, Lcom/twitter/ui/widget/PromptView;->setTitle(I)V

    .line 414
    iget-object v1, p0, Lcom/twitter/app/dm/DMInboxFragment;->h:Lcom/twitter/ui/widget/PromptView;

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/PromptView;->setButtonText(Ljava/lang/CharSequence;)V

    .line 415
    return-void

    .line 398
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/util/t;->a(Landroid/content/Context;)Lcom/twitter/android/util/s;

    move-result-object v0

    .line 401
    invoke-interface {v0}, Lcom/twitter/android/util/s;->c()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {v0}, Lcom/twitter/android/util/s;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 402
    :cond_1
    const v0, 0x7f0a0290

    goto :goto_0

    .line 404
    :cond_2
    const v0, 0x7f0a028f

    goto :goto_0

    .line 408
    :cond_3
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 409
    const/4 v0, 0x0

    .line 410
    const v1, 0x7f0a028e

    goto :goto_1
.end method

.method private r()V
    .locals 3

    .prologue
    .line 456
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->aq()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->ad()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 457
    :cond_0
    new-instance v0, Lcom/twitter/library/api/dm/x;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/api/dm/x;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v1, 0x6

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/app/dm/DMInboxFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 460
    :cond_1
    return-void
.end method

.method private s()V
    .locals 6

    .prologue
    .line 550
    const-wide/16 v0, 0x3e8

    const-string/jumbo v2, "dm_event_api_poll_interval_inbox"

    const-wide/16 v4, 0x3c

    .line 551
    invoke-static {v2, v4, v5}, Lcoj;->a(Ljava/lang/String;J)J

    move-result-wide v2

    mul-long/2addr v0, v2

    .line 553
    new-instance v2, Lcom/twitter/library/client/o;

    new-instance v3, Lcom/twitter/app/dm/DMInboxFragment$9;

    invoke-direct {v3, p0}, Lcom/twitter/app/dm/DMInboxFragment$9;-><init>(Lcom/twitter/app/dm/DMInboxFragment;)V

    invoke-direct {v2, v3, v0, v1}, Lcom/twitter/library/client/o;-><init>(Ljava/lang/Runnable;J)V

    iput-object v2, p0, Lcom/twitter/app/dm/DMInboxFragment;->c:Lcom/twitter/library/client/o;

    .line 559
    return-void
.end method

.method private t()V
    .locals 2

    .prologue
    .line 562
    new-instance v0, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;

    invoke-direct {v0}, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/ConnectContactsUploadHelperActivity$a;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMInboxFragment;->startActivity(Landroid/content/Intent;)V

    .line 563
    return-void
.end method


# virtual methods
.method public synthetic H()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->f()Lcom/twitter/app/dm/b;

    move-result-object v0

    return-object v0
.end method

.method public H_()V
    .locals 1

    .prologue
    .line 530
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/twitter/app/dm/DMInboxFragment;->b(I)Z

    .line 531
    return-void
.end method

.method public synthetic I()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->f()Lcom/twitter/app/dm/b;

    move-result-object v0

    return-object v0
.end method

.method protected K()Laoy;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laoy",
            "<",
            "Lcbi",
            "<",
            "Lcom/twitter/model/dms/q;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 326
    new-instance v0, Lcom/twitter/app/dm/DMInboxFragment$6;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/DMInboxFragment$6;-><init>(Lcom/twitter/app/dm/DMInboxFragment;)V

    .line 334
    new-instance v1, Lcom/twitter/app/dm/DMInboxFragment$7;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/DMInboxFragment$7;-><init>(Lcom/twitter/app/dm/DMInboxFragment;)V

    .line 349
    new-instance v2, Laos;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, v0, v1}, Laos;-><init>(Landroid/support/v4/app/LoaderManager;ILcom/twitter/util/object/j;Lcbq;)V

    return-object v2
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 164
    invoke-super {p0, p1, p2}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 165
    invoke-direct {p0, v0}, Lcom/twitter/app/dm/DMInboxFragment;->a(Landroid/view/View;)V

    .line 166
    return-object v0
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 426
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 440
    :goto_0
    return-void

    .line 429
    :pswitch_0
    iget-object v1, p0, Lcom/twitter/app/dm/DMInboxFragment;->e:Lmu;

    const/4 v2, 0x0

    if-eqz p2, :cond_0

    new-instance v0, Lcbh;

    new-instance v3, Lcom/twitter/android/provider/SuggestionsProvider$k;

    invoke-direct {v3}, Lcom/twitter/android/provider/SuggestionsProvider$k;-><init>()V

    invoke-direct {v0, p2, v3}, Lcbh;-><init>(Landroid/database/Cursor;Lcbp;)V

    :goto_1
    invoke-virtual {v1, v2, v0}, Lmu;->a(Ljava/lang/Object;Lcbi;)Lcbi;

    .line 433
    invoke-direct {p0}, Lcom/twitter/app/dm/DMInboxFragment;->q()V

    goto :goto_0

    .line 432
    :cond_0
    invoke-static {}, Lcbi;->f()Lcbi;

    move-result-object v0

    goto :goto_1

    .line 426
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 1

    .prologue
    .line 567
    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/q;

    .line 568
    if-nez v0, :cond_0

    .line 573
    :goto_0
    return-void

    .line 572
    :cond_0
    invoke-direct {p0, v0}, Lcom/twitter/app/dm/DMInboxFragment;->a(Lcom/twitter/model/dms/q;)V

    goto :goto_0
.end method

.method protected a(Lcbi;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/model/dms/q;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 419
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcbi;)V

    .line 420
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/k;

    invoke-virtual {v0}, Lcom/twitter/app/dm/k;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/twitter/app/dm/DMInboxFragment;->b(Z)V

    .line 421
    invoke-direct {p0}, Lcom/twitter/app/dm/DMInboxFragment;->r()V

    .line 422
    return-void

    .line 420
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 1

    .prologue
    .line 234
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 235
    const v0, 0x7f0400ba

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->c(I)Lcom/twitter/app/common/list/l$d;

    .line 236
    return-void
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 3

    .prologue
    const v2, 0x7f0a0548

    const/4 v1, 0x1

    .line 698
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 700
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 701
    packed-switch p2, :pswitch_data_0

    .line 727
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 703
    :pswitch_1
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 704
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/dm/DMInboxFragment;->T:Landroid/content/Context;

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 709
    :pswitch_2
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 710
    check-cast p1, Lcom/twitter/library/api/dm/g;

    .line 711
    invoke-virtual {p1}, Lcom/twitter/library/api/dm/g;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMInboxFragment;->i:Z

    goto :goto_0

    .line 713
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/dm/DMInboxFragment;->T:Landroid/content/Context;

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 718
    :pswitch_3
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 719
    iget-object v0, p0, Lcom/twitter/app/dm/DMInboxFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a02b5

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 701
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected a(Z)V
    .locals 3

    .prologue
    .line 514
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Z)V

    .line 516
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 517
    invoke-direct {p0}, Lcom/twitter/app/dm/DMInboxFragment;->k()V

    .line 518
    return-void
.end method

.method public a(Lcmm;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x1

    .line 490
    invoke-interface {p1}, Lcmm;->a()I

    move-result v1

    .line 491
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 492
    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 494
    const v3, 0x7f1308b1

    if-ne v1, v3, :cond_0

    .line 495
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v3, "messages:navigation_bar::compose:click"

    aput-object v3, v2, v6

    .line 496
    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    .line 495
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 497
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/twitter/app/dm/h$a;

    invoke-direct {v2}, Lcom/twitter/app/dm/h$a;-><init>()V

    .line 499
    invoke-virtual {v2}, Lcom/twitter/app/dm/h$a;->e()Lcom/twitter/app/dm/h;

    move-result-object v2

    .line 497
    invoke-static {v1, v2}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/h;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/app/dm/DMInboxFragment;->startActivity(Landroid/content/Intent;)V

    .line 508
    :goto_0
    return v0

    .line 501
    :cond_0
    const v3, 0x7f1308b2

    if-ne v1, v3, :cond_1

    .line 502
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v3, v0, [Ljava/lang/String;

    const-string/jumbo v4, "messages:inbox::mark_all_as_read:click"

    aput-object v4, v3, v6

    .line 503
    invoke-virtual {v1, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    .line 502
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 504
    new-instance v1, Lcom/twitter/library/api/dm/n;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3, v2}, Lcom/twitter/library/api/dm/n;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v2, 0x5

    invoke-virtual {p0, v1, v2, v6}, Lcom/twitter/app/dm/DMInboxFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto :goto_0

    .line 508
    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcmm;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 469
    const v0, 0x7f14001c

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 470
    const/4 v0, 0x1

    return v0
.end method

.method public aP_()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 276
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->aP_()V

    .line 277
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->aq()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 278
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/DMInboxFragment;->a_:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "messages:inbox:::impression"

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 281
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->f()Lcom/twitter/app/dm/b;

    move-result-object v0

    const-string/jumbo v1, "ref_event"

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 282
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/twitter/app/dm/DMInboxFragment;->a:Z

    if-nez v1, :cond_1

    .line 283
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/DMInboxFragment;->a_:J

    new-array v4, v6, [Ljava/lang/String;

    const-string/jumbo v5, "messages::::impression"

    aput-object v5, v4, v7

    invoke-direct {v1, v2, v3, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    .line 284
    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 283
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 285
    iput-boolean v6, p0, Lcom/twitter/app/dm/DMInboxFragment;->a:Z

    .line 289
    :goto_0
    invoke-direct {p0}, Lcom/twitter/app/dm/DMInboxFragment;->r()V

    .line 290
    return-void

    .line 287
    :cond_1
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/DMInboxFragment;->a_:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v6, [Ljava/lang/String;

    const-string/jumbo v2, "messages::::impression"

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0
.end method

.method protected ar_()V
    .locals 1

    .prologue
    .line 522
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->ar_()V

    .line 523
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->ak()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/k;

    invoke-virtual {v0}, Lcom/twitter/app/dm/k;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 524
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/twitter/app/dm/DMInboxFragment;->b(I)Z

    .line 526
    :cond_0
    return-void
.end method

.method public b(Lcmr;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 476
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->b(Lcmr;)I

    .line 477
    invoke-interface {p1}, Lcmr;->k()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 478
    const v2, 0x7f1308b2

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v2

    .line 479
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->aq()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 481
    const v3, 0x7f1308b1

    invoke-virtual {v0, v3}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    invoke-virtual {v0, v1}, Lazv;->b(Z)Lazv;

    .line 483
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->aq()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/app/dm/DMInboxFragment;->g:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lazv;->b(Z)Lazv;

    .line 484
    const/4 v0, 0x2

    return v0

    :cond_1
    move v0, v1

    .line 483
    goto :goto_0
.end method

.method protected b()V
    .locals 3

    .prologue
    .line 294
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->b()V

    .line 295
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->ar_()V

    .line 296
    iget-object v0, p0, Lcom/twitter/app/dm/DMInboxFragment;->e:Lmu;

    invoke-virtual {v0}, Lmu;->g()Lcbi;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 299
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/DMInboxFragment;->c:Lcom/twitter/library/client/o;

    if-eqz v0, :cond_1

    .line 300
    iget-object v0, p0, Lcom/twitter/app/dm/DMInboxFragment;->c:Lcom/twitter/library/client/o;

    invoke-virtual {v0}, Lcom/twitter/library/client/o;->a()V

    .line 302
    :cond_1
    return-void
.end method

.method public f()Lcom/twitter/app/dm/b;
    .locals 1

    .prologue
    .line 739
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/dm/b;->a(Landroid/os/Bundle;)Lcom/twitter/app/dm/b;

    move-result-object v0

    return-object v0
.end method

.method protected l()V
    .locals 1

    .prologue
    .line 731
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/dm/DMInboxFragment;->i:Z

    if-eqz v0, :cond_0

    .line 732
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/app/dm/DMInboxFragment;->b(I)Z

    .line 734
    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 240
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 242
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->aq()Z

    move-result v0

    if-nez v0, :cond_0

    .line 243
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0a0406

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    .line 246
    :cond_0
    if-nez p1, :cond_1

    .line 247
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/twitter/app/dm/DMInboxFragment;->b(I)Z

    .line 250
    :cond_1
    invoke-direct {p0}, Lcom/twitter/app/dm/DMInboxFragment;->k()V

    .line 252
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/dm/k;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/app/dm/k;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l;->a(Lcjr;)V

    .line 253
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    new-instance v1, Lcom/twitter/app/dm/DMInboxFragment$5;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/DMInboxFragment$5;-><init>(Lcom/twitter/app/dm/DMInboxFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 266
    iget-object v0, p0, Lcom/twitter/app/dm/DMInboxFragment;->e:Lmu;

    if-nez v0, :cond_2

    .line 267
    new-instance v0, Lmu;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lmu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/app/dm/DMInboxFragment;->e:Lmu;

    .line 269
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/dm/DMInboxFragment;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/app/dm/DMInboxFragment;->e:Lmu;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 270
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMInboxFragment;->i:Z

    .line 271
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 145
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 147
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->aq()Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/DMInboxFragment;->a_:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:inbox:::impression"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 151
    :cond_0
    invoke-static {p0, p1}, Lcom/twitter/app/dm/DMInboxFragmentSavedState;->a(Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 153
    invoke-direct {p0}, Lcom/twitter/app/dm/DMInboxFragment;->s()V

    .line 154
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 354
    packed-switch p1, :pswitch_data_0

    move-object v0, v4

    .line 360
    :goto_0
    return-object v0

    .line 356
    :pswitch_0
    new-instance v0, Lcom/twitter/util/android/d;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMInboxFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/provider/SuggestionsProvider;->e:Landroid/net/Uri;

    sget-object v3, Lcom/twitter/android/provider/SuggestionsProvider$l;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 354
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 93
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/dm/DMInboxFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 444
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 453
    :goto_0
    return-void

    .line 446
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/app/dm/DMInboxFragment;->e:Lmu;

    const/4 v1, 0x0

    invoke-static {}, Lcbi;->f()Lcbi;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmu;->a(Ljava/lang/Object;Lcbi;)Lcbi;

    goto :goto_0

    .line 444
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 158
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 159
    new-instance v0, Lcom/twitter/app/dm/DMInboxFragmentSavedState;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/DMInboxFragmentSavedState;-><init>(Lcom/twitter/app/dm/DMInboxFragment;)V

    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/DMInboxFragmentSavedState;->a(Landroid/os/Bundle;)V

    .line 160
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 314
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->onStart()V

    .line 320
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMInboxFragment;->a(Z)V

    .line 321
    return-void
.end method

.method protected q_()V
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/twitter/app/dm/DMInboxFragment;->c:Lcom/twitter/library/client/o;

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/twitter/app/dm/DMInboxFragment;->c:Lcom/twitter/library/client/o;

    invoke-virtual {v0}, Lcom/twitter/library/client/o;->b()V

    .line 309
    :cond_0
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->q_()V

    .line 310
    return-void
.end method
