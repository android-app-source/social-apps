.class Lcom/twitter/app/dm/quickshare/d;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/twitter/model/dms/q;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/database/model/i;

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/twitter/app/dm/quickshare/c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:J


# direct methods
.method constructor <init>(Lcom/twitter/library/provider/t;Lcom/twitter/app/dm/quickshare/c;)V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 23
    invoke-virtual {p1}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/quickshare/d;->a:Lcom/twitter/database/model/i;

    .line 24
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/app/dm/quickshare/d;->b:Ljava/lang/ref/WeakReference;

    .line 25
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/app/dm/quickshare/d;->c:J

    .line 26
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/q;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    new-instance v0, Lbta;

    iget-object v1, p0, Lcom/twitter/app/dm/quickshare/d;->a:Lcom/twitter/database/model/i;

    invoke-direct {v0, v1}, Lbta;-><init>(Lcom/twitter/database/model/i;)V

    iget-wide v2, p0, Lcom/twitter/app/dm/quickshare/d;->c:J

    invoke-virtual {v0, v2, v3}, Lbta;->a(J)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/q;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 38
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/d;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/quickshare/c;

    .line 39
    if-eqz v0, :cond_0

    .line 40
    invoke-interface {v0, p1}, Lcom/twitter/app/dm/quickshare/c;->a(Ljava/util/List;)V

    .line 42
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/quickshare/d;->a([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/quickshare/d;->a(Ljava/util/List;)V

    return-void
.end method
