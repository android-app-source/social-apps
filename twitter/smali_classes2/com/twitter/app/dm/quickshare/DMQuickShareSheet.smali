.class public Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;
.super Landroid/support/design/widget/BottomSheetDialogFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/dm/quickshare/a$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/app/dm/quickshare/a;

.field private b:Lcom/twitter/app/dm/quickshare/b;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/widget/TextSwitcher;

.field private f:Landroid/view/View;

.field private g:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/support/design/widget/BottomSheetDialogFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;)Lcom/twitter/app/dm/quickshare/a;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->a:Lcom/twitter/app/dm/quickshare/a;

    return-object v0
.end method

.method private a(II)V
    .locals 5

    .prologue
    .line 263
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->d:Landroid/view/View;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 265
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->d:Landroid/view/View;

    sget-object v1, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    int-to-float v4, p1

    aput v4, v2, v3

    const/4 v3, 0x1

    int-to-float v4, p2

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 266
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 267
    new-instance v1, Landroid/support/v4/view/animation/FastOutSlowInInterpolator;

    invoke-direct {v1}, Landroid/support/v4/view/animation/FastOutSlowInInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 268
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 269
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 158
    const v0, 0x7f130302

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextSwitcher;

    iput-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->e:Landroid/widget/TextSwitcher;

    .line 159
    invoke-virtual {p0}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f110012

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    .line 160
    invoke-virtual {p0}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0047

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 161
    iget-object v2, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->e:Landroid/widget/TextSwitcher;

    new-instance v3, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$3;

    invoke-direct {v3, p0, v0, v1}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$3;-><init>(Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;IF)V

    invoke-virtual {v2, v3}, Landroid/widget/TextSwitcher;->setFactory(Landroid/widget/ViewSwitcher$ViewFactory;)V

    .line 170
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->e:Landroid/widget/TextSwitcher;

    invoke-virtual {p0}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f050030

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextSwitcher;->setInAnimation(Landroid/content/Context;I)V

    .line 171
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->e:Landroid/widget/TextSwitcher;

    invoke-virtual {p0}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f050032

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextSwitcher;->setOutAnimation(Landroid/content/Context;I)V

    .line 172
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->e:Landroid/widget/TextSwitcher;

    const v1, 0x7f0a07f9

    invoke-virtual {p0, v1}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextSwitcher;->setCurrentText(Ljava/lang/CharSequence;)V

    .line 173
    return-void
.end method

.method private a(Landroid/view/View;J)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 184
    const v0, 0x7f130303

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 185
    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v3, v3}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 186
    new-instance v1, Lcom/twitter/app/dm/quickshare/b;

    invoke-virtual {p0}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->a:Lcom/twitter/app/dm/quickshare/a;

    invoke-direct {v1, v2, p2, p3, v3}, Lcom/twitter/app/dm/quickshare/b;-><init>(Landroid/content/Context;JLcom/twitter/app/dm/quickshare/b$c;)V

    iput-object v1, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->b:Lcom/twitter/app/dm/quickshare/b;

    .line 187
    iget-object v1, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->b:Lcom/twitter/app/dm/quickshare/b;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 188
    return-void
.end method

.method static synthetic b(Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;)Landroid/view/View;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->f:Landroid/view/View;

    return-object v0
.end method

.method private b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 176
    .line 177
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Landroid/support/design/widget/BottomSheetBehavior;->from(Landroid/view/View;)Landroid/support/design/widget/BottomSheetBehavior;

    move-result-object v0

    .line 178
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomSheetBehavior;->setState(I)V

    .line 179
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomSheetBehavior;->setSkipCollapsed(Z)V

    .line 180
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomSheetBehavior;->setHideable(Z)V

    .line 181
    return-void
.end method

.method private static c(Landroid/view/View;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 306
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 307
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 308
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 311
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    .line 308
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 311
    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;)Landroid/view/View;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->d:Landroid/view/View;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;)Landroid/view/View;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->c:Landroid/view/View;

    return-object v0
.end method

.method private e()Landroid/view/View;
    .locals 3

    .prologue
    .line 285
    invoke-virtual {p0}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 286
    invoke-static {v1}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->c(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 288
    instance-of v2, v0, Landroid/support/design/widget/CoordinatorLayout;

    if-eqz v2, :cond_1

    .line 295
    :cond_0
    :goto_0
    return-object v0

    .line 291
    :cond_1
    invoke-static {v0}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->c(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 292
    instance-of v2, v0, Landroid/support/design/widget/CoordinatorLayout;

    if-nez v2, :cond_0

    move-object v0, v1

    .line 295
    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/twitter/library/provider/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 223
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->b:Lcom/twitter/app/dm/quickshare/b;

    invoke-virtual {v0}, Lcom/twitter/app/dm/quickshare/b;->a()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 249
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->e:Landroid/widget/TextSwitcher;

    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextSwitcher;->setText(Ljava/lang/CharSequence;)V

    .line 250
    return-void
.end method

.method public a(Lcom/twitter/app/dm/j;)V
    .locals 2

    .prologue
    .line 243
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 244
    invoke-static {v0, p1}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/j;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 245
    return-void
.end method

.method public a(Lcom/twitter/library/api/dm/q;)V
    .locals 2

    .prologue
    .line 273
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 274
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->c:Landroid/view/View;

    new-instance v1, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$5;

    invoke-direct {v1, p0, p1, p2}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$5;-><init>(Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 239
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/q;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 192
    new-instance v0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$4;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$4;-><init>(Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;)V

    invoke-static {p1, v0}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/List;Lcpp;)Ljava/util/List;

    move-result-object v0

    .line 211
    iget-object v1, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->b:Lcom/twitter/app/dm/quickshare/b;

    invoke-virtual {v1, v0}, Lcom/twitter/app/dm/quickshare/b;->a(Ljava/util/List;)V

    .line 212
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->g:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 259
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->a(II)V

    .line 260
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 254
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->a(II)V

    .line 255
    return-void
.end method

.method public setupDialog(Landroid/app/Dialog;I)V
    .locals 7

    .prologue
    .line 114
    invoke-super {p0, p1, p2}, Landroid/support/design/widget/BottomSheetDialogFragment;->setupDialog(Landroid/app/Dialog;I)V

    .line 116
    invoke-direct {p0}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->e()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->c:Landroid/view/View;

    .line 119
    invoke-virtual {p0}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "arg_tweet"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/r;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/model/core/r;

    .line 121
    invoke-virtual {p0}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "arg_moment"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/moments/Moment;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/model/moments/Moment;

    .line 122
    if-eqz v2, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    if-eqz v3, :cond_2

    if-nez v2, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 124
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 125
    new-instance v0, Lcom/twitter/app/dm/quickshare/a;

    invoke-virtual {p0}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/app/dm/quickshare/a;-><init>(Landroid/content/Context;Lcom/twitter/model/core/r;Lcom/twitter/model/moments/Moment;JLcom/twitter/app/dm/quickshare/a$a;)V

    iput-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->a:Lcom/twitter/app/dm/quickshare/a;

    .line 127
    invoke-virtual {p0}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0400c2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 128
    invoke-virtual {p1, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 129
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->d:Landroid/view/View;

    .line 131
    invoke-direct {p0, v1}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->a(Landroid/view/View;)V

    .line 133
    const v0, 0x7f130304

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->f:Landroid/view/View;

    .line 134
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->f:Landroid/view/View;

    const v2, 0x7f130305

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->g:Landroid/widget/EditText;

    .line 136
    invoke-direct {p0, v1}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->b(Landroid/view/View;)V

    .line 137
    invoke-direct {p0, v1, v4, v5}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->a(Landroid/view/View;J)V

    .line 139
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->f:Landroid/view/View;

    const v1, 0x7f130306

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 140
    new-instance v1, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$1;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$1;-><init>(Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->a:Lcom/twitter/app/dm/quickshare/a;

    invoke-virtual {v0}, Lcom/twitter/app/dm/quickshare/a;->a()V

    .line 149
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->f:Landroid/view/View;

    new-instance v1, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$2;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$2;-><init>(Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 155
    return-void

    .line 122
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
