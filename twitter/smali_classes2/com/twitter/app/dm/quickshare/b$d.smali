.class Lcom/twitter/app/dm/quickshare/b$d;
.super Lcom/twitter/app/dm/quickshare/b$a;
.source "Twttr"


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/dm/quickshare/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "d"
.end annotation


# instance fields
.field final a:Lcom/twitter/media/ui/image/UserImageView;

.field final b:Landroid/widget/TextView;

.field final c:Landroid/widget/TextView;

.field final d:Landroid/view/View;


# direct methods
.method private constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 217
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/app/dm/quickshare/b$a;-><init>(Landroid/view/View;Lcom/twitter/app/dm/quickshare/b$1;)V

    .line 218
    const v0, 0x7f1302fe

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/UserImageView;

    iput-object v0, p0, Lcom/twitter/app/dm/quickshare/b$d;->a:Lcom/twitter/media/ui/image/UserImageView;

    .line 219
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/b$d;->a:Lcom/twitter/media/ui/image/UserImageView;

    sget-object v1, Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;->c:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/UserImageView;->setRoundingStrategy(Lcom/twitter/media/ui/image/config/g;)V

    .line 220
    const v0, 0x7f1302ff

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/app/dm/quickshare/b$d;->b:Landroid/widget/TextView;

    .line 221
    const v0, 0x7f130301

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/app/dm/quickshare/b$d;->c:Landroid/widget/TextView;

    .line 222
    const v0, 0x7f130300

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/twitter/app/dm/quickshare/b$d;->d:Landroid/view/View;

    .line 223
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/View;Lcom/twitter/app/dm/quickshare/b$1;)V
    .locals 0

    .prologue
    .line 210
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/quickshare/b$d;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method a(F)V
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/b$d;->a:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v0, p1}, Lcom/twitter/media/ui/image/UserImageView;->setAlpha(F)V

    .line 228
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/b$d;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 229
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/b$d;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 230
    return-void
.end method
