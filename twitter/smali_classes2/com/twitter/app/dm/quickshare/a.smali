.class public Lcom/twitter/app/dm/quickshare/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/dm/q$b;
.implements Lcom/twitter/app/dm/quickshare/b$c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/quickshare/a$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/app/dm/quickshare/a$a;

.field private final c:Lcom/twitter/model/core/r;

.field private final d:Lcom/twitter/model/moments/Moment;

.field private final e:J

.field private final f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/model/core/r;Lcom/twitter/model/moments/Moment;JLcom/twitter/app/dm/quickshare/a$a;)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-static {}, Lcom/twitter/library/dm/d;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/quickshare/a;->f:Z

    .line 51
    iput-object p1, p0, Lcom/twitter/app/dm/quickshare/a;->a:Landroid/content/Context;

    .line 52
    iput-object p6, p0, Lcom/twitter/app/dm/quickshare/a;->b:Lcom/twitter/app/dm/quickshare/a$a;

    .line 53
    iput-object p2, p0, Lcom/twitter/app/dm/quickshare/a;->c:Lcom/twitter/model/core/r;

    .line 54
    iput-object p3, p0, Lcom/twitter/app/dm/quickshare/a;->d:Lcom/twitter/model/moments/Moment;

    .line 55
    iput-wide p4, p0, Lcom/twitter/app/dm/quickshare/a;->e:J

    .line 56
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/quickshare/a;)Lcom/twitter/app/dm/quickshare/a$a;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/a;->b:Lcom/twitter/app/dm/quickshare/a$a;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/twitter/library/provider/c;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 193
    invoke-static {p1}, Lcom/twitter/library/dm/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 194
    invoke-static {p2}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/c;

    .line 195
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v1

    if-ne v1, v2, :cond_0

    instance-of v1, v0, Lcom/twitter/library/provider/d;

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/twitter/util/f;->b(Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 196
    check-cast v0, Lcom/twitter/library/provider/d;

    iget-object v0, v0, Lcom/twitter/library/provider/d;->b:Lcom/twitter/model/core/TwitterUser;

    .line 204
    :goto_1
    iget-object v1, p0, Lcom/twitter/app/dm/quickshare/a;->c:Lcom/twitter/model/core/r;

    if-eqz v1, :cond_4

    .line 205
    if-nez v0, :cond_3

    .line 206
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/a;->a:Landroid/content/Context;

    const v1, 0x7f0a02c7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 215
    :goto_2
    return-object v0

    :cond_0
    move v1, v3

    .line 195
    goto :goto_0

    :cond_1
    move-object v0, v4

    .line 198
    goto :goto_1

    :cond_2
    move-object v0, v4

    .line 201
    goto :goto_1

    .line 208
    :cond_3
    iget-object v1, p0, Lcom/twitter/app/dm/quickshare/a;->a:Landroid/content/Context;

    const v4, 0x7f0a02c8

    new-array v2, v2, [Ljava/lang/Object;

    .line 209
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    .line 208
    invoke-virtual {v1, v4, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 211
    :cond_4
    iget-object v1, p0, Lcom/twitter/app/dm/quickshare/a;->d:Lcom/twitter/model/moments/Moment;

    if-eqz v1, :cond_6

    .line 212
    if-nez v0, :cond_5

    .line 213
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/a;->a:Landroid/content/Context;

    const v1, 0x7f0a02c5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 215
    :cond_5
    iget-object v1, p0, Lcom/twitter/app/dm/quickshare/a;->a:Landroid/content/Context;

    const v4, 0x7f0a02c6

    new-array v2, v2, [Ljava/lang/Object;

    .line 216
    invoke-virtual {v0}, Lcom/twitter/model/core/TwitterUser;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    .line 215
    invoke-virtual {v1, v4, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 219
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "DMQuickSharePresenter requires either a Tweet or Moment"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Ljava/util/Collection;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/twitter/library/provider/c;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 97
    invoke-static {p1}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/c;

    .line 98
    instance-of v1, v0, Lcom/twitter/library/provider/b;

    if-eqz v1, :cond_0

    .line 99
    check-cast v0, Lcom/twitter/library/provider/b;

    iget-object v0, v0, Lcom/twitter/library/provider/b;->b:Lcom/twitter/model/dms/q;

    iget-object v0, v0, Lcom/twitter/model/dms/q;->b:Ljava/lang/String;

    .line 103
    :goto_0
    return-object v0

    .line 100
    :cond_0
    instance-of v1, v0, Lcom/twitter/library/provider/d;

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 101
    iget-wide v2, p0, Lcom/twitter/app/dm/quickshare/a;->e:J

    check-cast v0, Lcom/twitter/library/provider/d;

    iget-object v0, v0, Lcom/twitter/library/provider/d;->b:Lcom/twitter/model/core/TwitterUser;

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v2, v3, v0, v1}, Lcom/twitter/library/dm/e;->a(JJ)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 103
    :cond_1
    invoke-static {}, Lcom/twitter/library/dm/e;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/twitter/library/provider/c;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    invoke-static {p1}, Lcom/twitter/library/dm/e;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    new-instance v0, Lcom/twitter/app/dm/quickshare/a$1;

    invoke-direct {v0}, Lcom/twitter/app/dm/quickshare/a$1;-><init>()V

    invoke-static {p0, v0}, Lcpt;->a(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/o;->a(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    .line 123
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 175
    new-instance v0, Lcom/twitter/library/api/dm/q$a;

    invoke-direct {v0}, Lcom/twitter/library/api/dm/q$a;-><init>()V

    iget-object v1, p0, Lcom/twitter/app/dm/quickshare/a;->a:Landroid/content/Context;

    .line 176
    invoke-virtual {v0, v1}, Lcom/twitter/library/api/dm/q$a;->a(Landroid/content/Context;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    .line 177
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/dm/q$a;->a(Lcom/twitter/library/client/Session;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    .line 178
    invoke-virtual {v0, p1}, Lcom/twitter/library/api/dm/q$a;->b(Ljava/lang/String;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    .line 179
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/dm/q$a;->c(Ljava/lang/String;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    .line 180
    invoke-virtual {v0, p2}, Lcom/twitter/library/api/dm/q$a;->d(Ljava/lang/String;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    .line 181
    invoke-virtual {v0, p3}, Lcom/twitter/library/api/dm/q$a;->a(Ljava/util/Set;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    .line 182
    iget-object v1, p0, Lcom/twitter/app/dm/quickshare/a;->c:Lcom/twitter/model/core/r;

    if-eqz v1, :cond_1

    .line 183
    iget-object v1, p0, Lcom/twitter/app/dm/quickshare/a;->c:Lcom/twitter/model/core/r;

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/dm/q$a;->a(Lcom/twitter/model/core/r;)Lcom/twitter/library/api/dm/q$a;

    .line 187
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/twitter/app/dm/quickshare/a;->b:Lcom/twitter/app/dm/quickshare/a$a;

    invoke-virtual {v0}, Lcom/twitter/library/api/dm/q$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/dm/q;

    invoke-interface {v1, v0}, Lcom/twitter/app/dm/quickshare/a$a;->a(Lcom/twitter/library/api/dm/q;)V

    .line 188
    return-void

    .line 184
    :cond_1
    iget-object v1, p0, Lcom/twitter/app/dm/quickshare/a;->d:Lcom/twitter/model/moments/Moment;

    if-eqz v1, :cond_0

    .line 185
    iget-object v1, p0, Lcom/twitter/app/dm/quickshare/a;->d:Lcom/twitter/model/moments/Moment;

    iget-object v1, v1, Lcom/twitter/model/moments/Moment;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/dm/q$a;->d(Ljava/lang/String;)Lcom/twitter/library/api/dm/q$a;

    goto :goto_0
.end method

.method private static b(Ljava/util/Collection;)I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/twitter/library/provider/c;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 154
    invoke-static {p0}, Lcom/twitter/app/dm/quickshare/a;->c(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    const v0, 0x7f0a07fb

    .line 157
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0a07f9

    goto :goto_0
.end method

.method private static c(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/twitter/library/provider/c;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 162
    invoke-static {p0}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/util/Collection;)I

    move-result v1

    .line 163
    if-nez v1, :cond_1

    .line 164
    const/4 v0, 0x0

    .line 169
    :cond_0
    :goto_0
    return v0

    .line 165
    :cond_1
    if-gt v1, v0, :cond_0

    .line 169
    invoke-static {p0}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/library/provider/b;

    goto :goto_0
.end method


# virtual methods
.method a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)Landroid/support/design/widget/Snackbar;
    .locals 3

    .prologue
    .line 130
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, p3, v1}, Lcom/twitter/ui/widget/f;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;I)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    const v1, 0x7f0a0763

    new-instance v2, Lcom/twitter/app/dm/quickshare/a$2;

    invoke-direct {v2, p0, p2}, Lcom/twitter/app/dm/quickshare/a$2;-><init>(Lcom/twitter/app/dm/quickshare/a;Ljava/lang/String;)V

    .line 131
    invoke-virtual {v0, v1, v2}, Landroid/support/design/widget/Snackbar;->setAction(ILandroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 130
    return-object v0
.end method

.method a()V
    .locals 4

    .prologue
    .line 59
    new-instance v0, Lcom/twitter/app/dm/quickshare/d;

    iget-wide v2, p0, Lcom/twitter/app/dm/quickshare/a;->e:J

    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/dm/quickshare/a;->b:Lcom/twitter/app/dm/quickshare/a$a;

    invoke-direct {v0, v1, v2}, Lcom/twitter/app/dm/quickshare/d;-><init>(Lcom/twitter/library/provider/t;Lcom/twitter/app/dm/quickshare/c;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 60
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/quickshare/d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 61
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Landroid/net/Uri;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Landroid/net/Uri;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 91
    invoke-static {}, Lcom/twitter/library/dm/e;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0, p2, p3}, Lcom/twitter/app/dm/quickshare/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 93
    return-void
.end method

.method public a(Ljava/util/Collection;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/twitter/library/provider/c;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 143
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/a;->b:Lcom/twitter/app/dm/quickshare/a$a;

    invoke-interface {v0}, Lcom/twitter/app/dm/quickshare/a$a;->d()V

    .line 149
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/a;->b:Lcom/twitter/app/dm/quickshare/a$a;

    invoke-static {p1}, Lcom/twitter/app/dm/quickshare/a;->b(Ljava/util/Collection;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/twitter/app/dm/quickshare/a$a;->a(I)V

    .line 150
    return-void

    .line 145
    :cond_1
    if-eqz p2, :cond_0

    .line 146
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/a;->b:Lcom/twitter/app/dm/quickshare/a$a;

    invoke-interface {v0}, Lcom/twitter/app/dm/quickshare/a$a;->c()V

    goto :goto_0
.end method

.method b()V
    .locals 8

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/a;->b:Lcom/twitter/app/dm/quickshare/a$a;

    invoke-interface {v0}, Lcom/twitter/app/dm/quickshare/a$a;->a()Ljava/util/Collection;

    move-result-object v1

    .line 65
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    :goto_0
    return-void

    .line 69
    :cond_0
    invoke-direct {p0, v1}, Lcom/twitter/app/dm/quickshare/a;->a(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v2

    .line 70
    invoke-static {v1, v2}, Lcom/twitter/app/dm/quickshare/a;->a(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    .line 72
    iget-boolean v3, p0, Lcom/twitter/app/dm/quickshare/a;->f:Z

    if-eqz v3, :cond_2

    invoke-static {v2}, Lcom/twitter/library/dm/e;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 73
    new-instance v3, Lcom/twitter/app/dm/q$a;

    invoke-direct {v3}, Lcom/twitter/app/dm/q$a;-><init>()V

    new-instance v4, Lbta;

    iget-wide v6, p0, Lcom/twitter/app/dm/quickshare/a;->e:J

    .line 74
    invoke-static {v6, v7}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v5

    invoke-direct {v4, v5}, Lbta;-><init>(Lcom/twitter/database/model/i;)V

    invoke-virtual {v3, v4}, Lcom/twitter/app/dm/q$a;->a(Lbta;)Lcom/twitter/app/dm/q$a;

    move-result-object v3

    if-eqz v0, :cond_1

    .line 75
    :goto_1
    invoke-virtual {v3, v0}, Lcom/twitter/app/dm/q$a;->a(Ljava/util/Set;)Lcom/twitter/app/dm/q$a;

    move-result-object v0

    .line 76
    invoke-virtual {v0, p0}, Lcom/twitter/app/dm/q$a;->a(Lcom/twitter/app/dm/q$b;)Lcom/twitter/app/dm/q$a;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/app/dm/quickshare/a;->b:Lcom/twitter/app/dm/quickshare/a$a;

    .line 77
    invoke-interface {v3}, Lcom/twitter/app/dm/quickshare/a$a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/app/dm/q$a;->a(Ljava/lang/String;)Lcom/twitter/app/dm/q$a;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, Lcom/twitter/app/dm/q$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/q;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    .line 79
    invoke-virtual {v0, v3}, Lcom/twitter/app/dm/q;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 84
    :goto_2
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/a;->b:Lcom/twitter/app/dm/quickshare/a$a;

    invoke-interface {v0}, Lcom/twitter/app/dm/quickshare/a$a;->dismiss()V

    .line 85
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/a;->b:Lcom/twitter/app/dm/quickshare/a$a;

    invoke-direct {p0, v2, v1}, Lcom/twitter/app/dm/quickshare/a;->a(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lcom/twitter/app/dm/quickshare/a$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 75
    :cond_1
    invoke-static {}, Lcom/twitter/util/collection/o;->f()Ljava/util/Set;

    move-result-object v0

    goto :goto_1

    .line 81
    :cond_2
    iget-object v3, p0, Lcom/twitter/app/dm/quickshare/a;->b:Lcom/twitter/app/dm/quickshare/a$a;

    invoke-interface {v3}, Lcom/twitter/app/dm/quickshare/a$a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3, v0}, Lcom/twitter/app/dm/quickshare/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    goto :goto_2
.end method
