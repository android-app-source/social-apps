.class public Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/support/v4/app/FragmentManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;->a:Landroid/content/Context;

    .line 80
    iput-object p2, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;->b:Landroid/support/v4/app/FragmentManager;

    .line 81
    return-void
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 105
    new-instance v0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;

    invoke-direct {v0}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;-><init>()V

    .line 107
    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->setArguments(Landroid/os/Bundle;)V

    .line 108
    iget-object v1, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;->b:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v0}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 109
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/r;)V
    .locals 3

    .prologue
    .line 93
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 94
    const-string/jumbo v1, "arg_tweet"

    sget-object v2, Lcom/twitter/model/core/r;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v2}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 95
    invoke-direct {p0, v0}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;->a(Landroid/os/Bundle;)V

    .line 96
    return-void
.end method

.method public a(Lcom/twitter/model/moments/Moment;)V
    .locals 3

    .prologue
    .line 99
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 100
    const-string/jumbo v1, "arg_moment"

    sget-object v2, Lcom/twitter/model/moments/Moment;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v2}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 101
    invoke-direct {p0, v0}, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;->a(Landroid/os/Bundle;)V

    .line 102
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 89
    invoke-static {}, Lcom/twitter/library/dm/d;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/DMQuickShareSheet$a;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/util/d;->f(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
