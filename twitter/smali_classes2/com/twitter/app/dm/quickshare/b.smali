.class public Lcom/twitter/app/dm/quickshare/b;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/quickshare/b$c;,
        Lcom/twitter/app/dm/quickshare/b$a;,
        Lcom/twitter/app/dm/quickshare/b$d;,
        Lcom/twitter/app/dm/quickshare/b$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/twitter/app/dm/quickshare/b$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:J

.field private final c:Lcom/twitter/app/dm/quickshare/b$c;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/provider/c;",
            ">;"
        }
    .end annotation
.end field

.field private e:[Z

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/library/provider/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;JLcom/twitter/app/dm/quickshare/b$c;)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 45
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/quickshare/b;->d:Ljava/util/List;

    .line 46
    const/4 v0, 0x0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/twitter/app/dm/quickshare/b;->e:[Z

    .line 47
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/quickshare/b;->f:Ljava/util/Map;

    .line 51
    iput-object p1, p0, Lcom/twitter/app/dm/quickshare/b;->a:Landroid/content/Context;

    .line 52
    iput-wide p2, p0, Lcom/twitter/app/dm/quickshare/b;->b:J

    .line 53
    iput-object p4, p0, Lcom/twitter/app/dm/quickshare/b;->c:Lcom/twitter/app/dm/quickshare/b$c;

    .line 54
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/quickshare/b;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/b;->f:Ljava/util/Map;

    return-object v0
.end method

.method private a(Lcom/twitter/app/dm/quickshare/b$b;Lcom/twitter/library/provider/b;)V
    .locals 6

    .prologue
    .line 162
    iget-object v0, p2, Lcom/twitter/library/provider/b;->b:Lcom/twitter/model/dms/q;

    .line 163
    invoke-static {p1}, Lcom/twitter/app/dm/quickshare/b$b;->a(Lcom/twitter/app/dm/quickshare/b$b;)Lcom/twitter/app/dm/widget/DMAvatar;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/app/dm/widget/DMAvatar;->setConversation(Lcom/twitter/model/dms/q;)V

    .line 164
    invoke-static {p1}, Lcom/twitter/app/dm/quickshare/b$b;->b(Lcom/twitter/app/dm/quickshare/b$b;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/dm/b;

    iget-object v3, p0, Lcom/twitter/app/dm/quickshare/b;->a:Landroid/content/Context;

    iget-wide v4, p0, Lcom/twitter/app/dm/quickshare/b;->b:J

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/twitter/library/dm/b;-><init>(Lcom/twitter/model/dms/q;Landroid/content/Context;J)V

    invoke-virtual {v2}, Lcom/twitter/library/dm/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    return-void
.end method

.method private a(Lcom/twitter/app/dm/quickshare/b$d;Lcom/twitter/library/provider/d;)V
    .locals 3

    .prologue
    .line 168
    iget-object v0, p2, Lcom/twitter/library/provider/d;->b:Lcom/twitter/model/core/TwitterUser;

    .line 169
    iget-object v1, p1, Lcom/twitter/app/dm/quickshare/b$d;->a:Lcom/twitter/media/ui/image/UserImageView;

    invoke-virtual {v1, v0}, Lcom/twitter/media/ui/image/UserImageView;->a(Lcom/twitter/model/core/TwitterUser;)Z

    .line 170
    iget-object v1, p1, Lcom/twitter/app/dm/quickshare/b$d;->b:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x40

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 172
    iget-object v2, p1, Lcom/twitter/app/dm/quickshare/b$d;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    iget-object v1, p1, Lcom/twitter/app/dm/quickshare/b$d;->d:Landroid/view/View;

    iget-boolean v0, v0, Lcom/twitter/model/core/TwitterUser;->m:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 174
    return-void

    .line 173
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 118
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/twitter/app/dm/quickshare/b;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 119
    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/quickshare/b;->b(I)Z

    move-result v1

    .line 120
    iget-object v2, p0, Lcom/twitter/app/dm/quickshare/b;->e:[Z

    aget-boolean v2, v2, v0

    if-eq v2, v1, :cond_0

    .line 121
    iget-object v2, p0, Lcom/twitter/app/dm/quickshare/b;->e:[Z

    aput-boolean v1, v2, v0

    .line 122
    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/quickshare/b;->notifyItemChanged(I)V

    .line 118
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 125
    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/twitter/app/dm/quickshare/b;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/twitter/app/dm/quickshare/b;->b()V

    return-void
.end method

.method static synthetic c(Lcom/twitter/app/dm/quickshare/b;)Lcom/twitter/app/dm/quickshare/b$c;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/b;->c:Lcom/twitter/app/dm/quickshare/b$c;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Lcom/twitter/app/dm/quickshare/b$a;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 72
    packed-switch p2, :pswitch_data_0

    .line 86
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "DMQuickShareRecipientAdapter viewType must be 0 or 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400c0

    .line 75
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 76
    new-instance v0, Lcom/twitter/app/dm/quickshare/b$b;

    invoke-direct {v0, v1, v3}, Lcom/twitter/app/dm/quickshare/b$b;-><init>(Landroid/view/View;Lcom/twitter/app/dm/quickshare/b$1;)V

    .line 91
    :goto_0
    iget-object v1, v0, Lcom/twitter/app/dm/quickshare/b$a;->itemView:Landroid/view/View;

    new-instance v2, Lcom/twitter/app/dm/quickshare/b$1;

    invoke-direct {v2, p0, v0}, Lcom/twitter/app/dm/quickshare/b$1;-><init>(Lcom/twitter/app/dm/quickshare/b;Lcom/twitter/app/dm/quickshare/b$a;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    return-object v0

    .line 80
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400c1

    .line 81
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 82
    new-instance v0, Lcom/twitter/app/dm/quickshare/b$d;

    invoke-direct {v0, v1, v3}, Lcom/twitter/app/dm/quickshare/b$d;-><init>(Landroid/view/View;Lcom/twitter/app/dm/quickshare/b$1;)V

    goto :goto_0

    .line 72
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/twitter/library/provider/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/b;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method a(I)V
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/b;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/c;

    .line 109
    invoke-virtual {v0}, Lcom/twitter/library/provider/c;->d()Ljava/lang/String;

    move-result-object v1

    .line 110
    iget-object v2, p0, Lcom/twitter/app/dm/quickshare/b;->f:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 111
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/b;->f:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    :goto_0
    return-void

    .line 113
    :cond_0
    iget-object v2, p0, Lcom/twitter/app/dm/quickshare/b;->f:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Lcom/twitter/app/dm/quickshare/b$a;I)V
    .locals 3

    .prologue
    .line 144
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/b;->d:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/c;

    .line 146
    instance-of v1, v0, Lcom/twitter/library/provider/b;

    if-eqz v1, :cond_1

    move-object v1, p1

    .line 147
    check-cast v1, Lcom/twitter/app/dm/quickshare/b$b;

    move-object v2, v0

    check-cast v2, Lcom/twitter/library/provider/b;

    invoke-direct {p0, v1, v2}, Lcom/twitter/app/dm/quickshare/b;->a(Lcom/twitter/app/dm/quickshare/b$b;Lcom/twitter/library/provider/b;)V

    .line 152
    :cond_0
    :goto_0
    invoke-static {p1}, Lcom/twitter/app/dm/quickshare/b$a;->a(Lcom/twitter/app/dm/quickshare/b$a;)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/dm/quickshare/b;->f:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/twitter/library/provider/c;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 156
    invoke-virtual {p0, p2}, Lcom/twitter/app/dm/quickshare/b;->b(I)Z

    move-result v1

    .line 157
    if-eqz v1, :cond_3

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_2
    invoke-virtual {p1, v0}, Lcom/twitter/app/dm/quickshare/b$a;->a(F)V

    .line 158
    iget-object v0, p1, Lcom/twitter/app/dm/quickshare/b$a;->itemView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 159
    return-void

    .line 148
    :cond_1
    instance-of v1, v0, Lcom/twitter/library/provider/d;

    if-eqz v1, :cond_0

    move-object v1, p1

    .line 149
    check-cast v1, Lcom/twitter/app/dm/quickshare/b$d;

    move-object v2, v0

    check-cast v2, Lcom/twitter/library/provider/d;

    invoke-direct {p0, v1, v2}, Lcom/twitter/app/dm/quickshare/b;->a(Lcom/twitter/app/dm/quickshare/b$d;Lcom/twitter/library/provider/d;)V

    goto :goto_0

    .line 152
    :cond_2
    const/16 v0, 0x8

    goto :goto_1

    .line 157
    :cond_3
    const v0, 0x3e99999a    # 0.3f

    goto :goto_2
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/provider/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 62
    iput-object p1, p0, Lcom/twitter/app/dm/quickshare/b;->d:Ljava/util/List;

    .line 63
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/twitter/app/dm/quickshare/b;->e:[Z

    .line 64
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/b;->e:[Z

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([ZZ)V

    .line 65
    invoke-virtual {p0}, Lcom/twitter/app/dm/quickshare/b;->notifyDataSetChanged()V

    .line 66
    return-void
.end method

.method b(I)Z
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 129
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/b;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->c(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/c;

    .line 130
    instance-of v1, v0, Lcom/twitter/library/provider/b;

    if-eqz v1, :cond_0

    .line 132
    invoke-virtual {v0}, Lcom/twitter/library/provider/c;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/b;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/c;

    invoke-virtual {v0}, Lcom/twitter/library/provider/c;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 138
    :goto_0
    return v0

    .line 133
    :cond_0
    instance-of v0, v0, Lcom/twitter/library/provider/d;

    if-eqz v0, :cond_1

    .line 135
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/b;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/library/provider/d;

    goto :goto_0

    .line 138
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/b;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/b;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/c;

    .line 184
    instance-of v0, v0, Lcom/twitter/library/provider/b;

    if-eqz v0, :cond_0

    .line 185
    const/4 v0, 0x0

    .line 187
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 34
    check-cast p1, Lcom/twitter/app/dm/quickshare/b$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/dm/quickshare/b;->a(Lcom/twitter/app/dm/quickshare/b$a;I)V

    return-void
.end method

.method public synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/dm/quickshare/b;->a(Landroid/view/ViewGroup;I)Lcom/twitter/app/dm/quickshare/b$a;

    move-result-object v0

    return-object v0
.end method
