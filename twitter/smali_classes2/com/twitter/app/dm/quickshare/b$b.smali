.class Lcom/twitter/app/dm/quickshare/b$b;
.super Lcom/twitter/app/dm/quickshare/b$a;
.source "Twttr"


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/dm/quickshare/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation


# instance fields
.field private final a:Lcom/twitter/app/dm/widget/DMAvatar;

.field private final b:Landroid/widget/TextView;


# direct methods
.method private constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 197
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/app/dm/quickshare/b$a;-><init>(Landroid/view/View;Lcom/twitter/app/dm/quickshare/b$1;)V

    .line 198
    const v0, 0x7f1302fc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/widget/DMAvatar;

    iput-object v0, p0, Lcom/twitter/app/dm/quickshare/b$b;->a:Lcom/twitter/app/dm/widget/DMAvatar;

    .line 199
    const v0, 0x7f130058

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/app/dm/quickshare/b$b;->b:Landroid/widget/TextView;

    .line 200
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/View;Lcom/twitter/app/dm/quickshare/b$1;)V
    .locals 0

    .prologue
    .line 192
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/quickshare/b$b;-><init>(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/quickshare/b$b;)Lcom/twitter/app/dm/widget/DMAvatar;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/b$b;->a:Lcom/twitter/app/dm/widget/DMAvatar;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/app/dm/quickshare/b$b;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/b$b;->b:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method a(F)V
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/b$b;->a:Lcom/twitter/app/dm/widget/DMAvatar;

    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/widget/DMAvatar;->setAlpha(F)V

    .line 205
    iget-object v0, p0, Lcom/twitter/app/dm/quickshare/b$b;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 206
    return-void
.end method
