.class Lcom/twitter/app/dm/DMInboxFragment$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/ui/widget/PromptView$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/dm/DMInboxFragment;->a(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/dm/DMInboxFragment;


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/DMInboxFragment;)V
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lcom/twitter/app/dm/DMInboxFragment$3;->a:Lcom/twitter/app/dm/DMInboxFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/ui/widget/PromptView;)V
    .locals 4

    .prologue
    .line 203
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/app/dm/DMInboxFragment$3;->a:Lcom/twitter/app/dm/DMInboxFragment;

    invoke-static {v1}, Lcom/twitter/app/dm/DMInboxFragment;->b(Lcom/twitter/app/dm/DMInboxFragment;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:inbox::import:click"

    aput-object v3, v1, v2

    .line 204
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 203
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 206
    iget-object v0, p0, Lcom/twitter/app/dm/DMInboxFragment$3;->a:Lcom/twitter/app/dm/DMInboxFragment;

    invoke-virtual {v0}, Lcom/twitter/app/dm/DMInboxFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/util/t;->a(Landroid/content/Context;)Lcom/twitter/android/util/s;

    move-result-object v0

    .line 207
    iget-object v1, p0, Lcom/twitter/app/dm/DMInboxFragment$3;->a:Lcom/twitter/app/dm/DMInboxFragment;

    invoke-static {v1}, Lcom/twitter/app/dm/DMInboxFragment;->c(Lcom/twitter/app/dm/DMInboxFragment;)V

    .line 210
    invoke-interface {v0}, Lcom/twitter/android/util/s;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    const-string/jumbo v0, "messages"

    iget-object v1, p0, Lcom/twitter/app/dm/DMInboxFragment$3;->a:Lcom/twitter/app/dm/DMInboxFragment;

    .line 212
    invoke-virtual {v1}, Lcom/twitter/app/dm/DMInboxFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/DeviceRegistrationService$a;->a(Ljava/lang/String;Landroid/content/Context;)Lcom/twitter/android/DeviceRegistrationService$a;

    move-result-object v0

    .line 213
    invoke-virtual {v0}, Lcom/twitter/android/DeviceRegistrationService$a;->a()V

    .line 215
    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/ui/widget/PromptView;)V
    .locals 0

    .prologue
    .line 218
    return-void
.end method
