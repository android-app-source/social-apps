.class Lcom/twitter/app/dm/DMConversationFragment$13;
.super Lcbp;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/dm/DMConversationFragment;->a(Landroid/database/Cursor;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcbp",
        "<",
        "Lcom/twitter/util/collection/Pair",
        "<",
        "Ljava/lang/Long;",
        "Lcaq;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/dm/DMConversationFragment;


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/DMConversationFragment;)V
    .locals 0

    .prologue
    .line 1777
    iput-object p1, p0, Lcom/twitter/app/dm/DMConversationFragment$13;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-direct {p0}, Lcbp;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Lcom/twitter/util/collection/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/Long;",
            "Lcaq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1781
    const-string/jumbo v0, "card_id"

    .line 1782
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1783
    const-string/jumbo v0, "card_state"

    .line 1785
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1784
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcaq;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcaq;

    .line 1788
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    if-eqz v0, :cond_0

    :goto_0
    invoke-static {v1, v0}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Lcaq;

    invoke-direct {v0}, Lcaq;-><init>()V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1777
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/DMConversationFragment$13;->a(Landroid/database/Cursor;)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    return-object v0
.end method
