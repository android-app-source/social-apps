.class public Lcom/twitter/app/dm/DMComposeFragment;
.super Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/dm/g$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/DMComposeFragment$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;",
        "Lcom/twitter/app/dm/g$a;"
    }
.end annotation


# instance fields
.field protected e:Z

.field f:Lcom/twitter/app/dm/g;

.field private g:Z

.field private h:Z

.field private i:I

.field private j:Z

.field private k:Z

.field private l:Ljava/lang/String;

.field private m:Landroid/net/Uri;

.field private n:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/DMComposeFragment;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/twitter/app/dm/DMComposeFragment;->i:I

    return v0
.end method

.method static synthetic a(Lcom/twitter/app/dm/DMComposeFragment;I)I
    .locals 0

    .prologue
    .line 48
    iput p1, p0, Lcom/twitter/app/dm/DMComposeFragment;->i:I

    return p1
.end method

.method static synthetic b(Lcom/twitter/app/dm/DMComposeFragment;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/twitter/app/dm/DMComposeFragment;->y()V

    return-void
.end method

.method private w()V
    .locals 2

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 127
    iget-boolean v1, p0, Lcom/twitter/app/dm/DMComposeFragment;->g:Z

    if-eqz v1, :cond_0

    .line 128
    const v1, 0x7f0a027c

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(I)V

    .line 136
    :goto_0
    return-void

    .line 129
    :cond_0
    iget-boolean v1, p0, Lcom/twitter/app/dm/DMComposeFragment;->e:Z

    if-eqz v1, :cond_1

    .line 130
    const v1, 0x7f0a02b8

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(I)V

    goto :goto_0

    .line 131
    :cond_1
    iget-boolean v1, p0, Lcom/twitter/app/dm/DMComposeFragment;->k:Z

    if-eqz v1, :cond_2

    .line 132
    const v1, 0x7f0a029e

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(I)V

    goto :goto_0

    .line 134
    :cond_2
    const v1, 0x7f0a02b7

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(I)V

    goto :goto_0
.end method

.method private x()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 190
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMComposeFragment;->j:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "external_share"

    .line 191
    :goto_0
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "messages"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 192
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->n()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object v1, v3, v4

    const/4 v1, 0x3

    aput-object v0, v3, v1

    const/4 v0, 0x4

    const-string/jumbo v1, "impression"

    aput-object v1, v3, v0

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 191
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 193
    return-void

    :cond_0
    move-object v0, v1

    .line 190
    goto :goto_0
.end method

.method private y()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 197
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 198
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->n()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object v4, v1, v2

    const/4 v2, 0x3

    aput-object v4, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "remove"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 197
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 199
    return-void
.end method


# virtual methods
.method public synthetic I()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->q()Lcom/twitter/app/dm/h;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->m()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/app/dm/DMComposeFragment;->a(Landroid/view/LayoutInflater;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(JLcom/twitter/model/core/TwitterUser;)V
    .locals 0

    .prologue
    .line 207
    return-void
.end method

.method public a(Lcom/twitter/library/api/dm/i;)V
    .locals 2

    .prologue
    .line 261
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/dm/DMComposeFragment$2;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/DMComposeFragment$2;-><init>(Lcom/twitter/app/dm/DMComposeFragment;)V

    invoke-virtual {v0, p1, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 275
    return-void
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/dms/t;)V
    .locals 0

    .prologue
    .line 219
    return-void
.end method

.method public a(Lcom/twitter/model/dms/q;)V
    .locals 5

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 212
    instance-of v1, v0, Lcom/twitter/app/dm/DMActivity;

    if-eqz v1, :cond_0

    .line 213
    check-cast v0, Lcom/twitter/app/dm/DMActivity;

    iget-object v1, p1, Lcom/twitter/model/dms/q;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/app/dm/DMComposeFragment;->l:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/app/dm/DMComposeFragment;->m:Landroid/net/Uri;

    .line 214
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->u()Z

    move-result v4

    .line 213
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/app/dm/DMActivity;->a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Z)V

    .line 216
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Object;J)V
    .locals 6

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/app/dm/DMComposeFragment;->d:Lcom/twitter/android/suggestionselection/a;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/android/suggestionselection/a;->a(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 236
    invoke-direct {p0}, Lcom/twitter/app/dm/DMComposeFragment;->y()V

    .line 257
    :cond_0
    :goto_0
    return-void

    .line 242
    :cond_1
    instance-of v0, p1, Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_2

    .line 243
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->o()Ljava/lang/String;

    move-result-object v1

    .line 244
    const-string/jumbo v0, "user"

    .line 255
    :goto_1
    new-instance v2, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "messages"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 256
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->n()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object v1, v3, v4

    const/4 v1, 0x3

    aput-object v0, v3, v1

    const/4 v0, 0x4

    const-string/jumbo v1, "select"

    aput-object v1, v3, v0

    invoke-virtual {v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 255
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 245
    :cond_2
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 246
    const-string/jumbo v1, "typeahead"

    .line 247
    const-string/jumbo v0, "query"

    goto :goto_1

    .line 248
    :cond_3
    instance-of v0, p1, Lcom/twitter/model/dms/q;

    if-eqz v0, :cond_0

    .line 249
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->o()Ljava/lang/String;

    move-result-object v1

    .line 250
    const-string/jumbo v0, "conversation"

    goto :goto_1
.end method

.method public bridge synthetic a(Ljava/lang/Object;JLjava/lang/Object;I)Z
    .locals 6

    .prologue
    .line 48
    move-object v1, p1

    check-cast v1, Ljava/lang/String;

    move-object v0, p0

    move-wide v2, p2

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/app/dm/DMComposeFragment;->a(Ljava/lang/String;JLjava/lang/Object;I)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;JLjava/lang/Object;I)Z
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lcom/twitter/app/dm/DMComposeFragment;->f:Lcom/twitter/app/dm/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/DMComposeFragment;->f:Lcom/twitter/app/dm/g;

    invoke-virtual {v0, p2, p3, p4}, Lcom/twitter/app/dm/g;->a(JLjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 293
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->v()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/app/dm/DMComposeFragment;->h:Z

    .line 294
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 295
    instance-of v1, v0, Lcom/twitter/app/common/base/TwitterFragmentActivity;

    if-eqz v1, :cond_0

    .line 296
    check-cast v0, Lcom/twitter/app/common/base/TwitterFragmentActivity;

    .line 297
    invoke-virtual {v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->F()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->h()V

    .line 299
    :cond_0
    return-void

    .line 293
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected e()Lna;
    .locals 6
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lna",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 91
    new-instance v1, Lnd;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "compose_message"

    invoke-direct {v1, v2, v0, v3}, Lnd;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    .line 93
    new-instance v2, Lmx;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v2, v3, v1, v4, v5}, Lmx;-><init>(Landroid/content/Context;Lnd;J)V

    return-object v2
.end method

.method protected f()Lnh;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lnh",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    new-instance v0, Lcom/twitter/app/dm/DMComposeFragment$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/app/dm/DMComposeFragment$a;-><init>(Lcom/twitter/app/dm/DMComposeFragment$1;)V

    return-object v0
.end method

.method protected g()Lmq;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lmq",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    new-instance v0, Lmm;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/dm/DMComposeFragment;->d:Lcom/twitter/android/suggestionselection/a;

    invoke-direct {v0, v1, v2}, Lmm;-><init>(Landroid/content/Context;Lcom/twitter/android/autocomplete/a;)V

    return-object v0
.end method

.method protected h()Landroid/text/TextWatcher;
    .locals 1

    .prologue
    .line 162
    new-instance v0, Lcom/twitter/app/dm/DMComposeFragment$1;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/DMComposeFragment$1;-><init>(Lcom/twitter/app/dm/DMComposeFragment;)V

    return-object v0
.end method

.method protected l()I
    .locals 1

    .prologue
    .line 110
    invoke-static {}, Lcom/twitter/library/dm/d;->k()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method protected m()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 146
    const v0, 0x7f0400b0

    return v0
.end method

.method protected n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    const-string/jumbo v0, "compose"

    return-object v0
.end method

.method protected o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    const-string/jumbo v0, "user_list"

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    .line 115
    invoke-super {p0, p1}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 116
    invoke-direct {p0}, Lcom/twitter/app/dm/DMComposeFragment;->w()V

    .line 119
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v1, 0x7f1302e9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/app/dm/widget/DMRecipientSearch;

    .line 120
    new-instance v0, Lcom/twitter/app/dm/g;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-object v5, p0, Lcom/twitter/app/dm/DMComposeFragment;->a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    iget-object v6, p0, Lcom/twitter/app/dm/DMComposeFragment;->d:Lcom/twitter/android/suggestionselection/a;

    iget-boolean v7, p0, Lcom/twitter/app/dm/DMComposeFragment;->g:Z

    iget-boolean v8, p0, Lcom/twitter/app/dm/DMComposeFragment;->j:Z

    iget-boolean v9, p0, Lcom/twitter/app/dm/DMComposeFragment;->e:Z

    .line 122
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->l()I

    move-result v10

    move-object v3, p0

    invoke-direct/range {v0 .. v10}, Lcom/twitter/app/dm/g;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/app/dm/g$a;Lcom/twitter/app/dm/widget/DMRecipientSearch;Lcom/twitter/android/autocomplete/SuggestionEditText;Lcom/twitter/android/suggestionselection/a;ZZZI)V

    iput-object v0, p0, Lcom/twitter/app/dm/DMComposeFragment;->f:Lcom/twitter/app/dm/g;

    .line 123
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 69
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->q()Lcom/twitter/app/dm/h;

    move-result-object v1

    .line 70
    invoke-virtual {v1}, Lcom/twitter/app/dm/h;->j()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMComposeFragment;->g:Z

    .line 71
    invoke-virtual {v1}, Lcom/twitter/app/dm/h;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/DMComposeFragment;->l:Ljava/lang/String;

    .line 72
    invoke-virtual {v1}, Lcom/twitter/app/dm/h;->f()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMComposeFragment;->j:Z

    .line 73
    invoke-virtual {v1}, Lcom/twitter/app/dm/h;->g()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMComposeFragment;->k:Z

    .line 74
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMComposeFragment;->j:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "android.intent.extra.STREAM"

    invoke-virtual {v1, v0}, Lcom/twitter/app/dm/h;->h(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    :goto_0
    iput-object v0, p0, Lcom/twitter/app/dm/DMComposeFragment;->m:Landroid/net/Uri;

    .line 75
    invoke-virtual {v1}, Lcom/twitter/app/dm/h;->k()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMComposeFragment;->n:Z

    .line 77
    invoke-direct {p0}, Lcom/twitter/app/dm/DMComposeFragment;->x()V

    .line 78
    return-void

    .line 74
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 82
    invoke-super {p0}, Lcom/twitter/android/suggestionselection/SuggestionSelectionListFragment;->onStart()V

    .line 83
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->j()V

    .line 84
    return-void
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/twitter/app/dm/DMComposeFragment;->a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/DMComposeFragment;->a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()Lcom/twitter/app/dm/h;
    .locals 1

    .prologue
    .line 284
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMComposeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/dm/h;->d(Landroid/os/Bundle;)Lcom/twitter/app/dm/h;

    move-result-object v0

    return-object v0
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 288
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMComposeFragment;->h:Z

    return v0
.end method

.method public s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/twitter/app/dm/DMComposeFragment;->l:Ljava/lang/String;

    return-object v0
.end method

.method public t()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/twitter/app/dm/DMComposeFragment;->m:Landroid/net/Uri;

    return-object v0
.end method

.method public u()Z
    .locals 1

    .prologue
    .line 312
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMComposeFragment;->n:Z

    return v0
.end method

.method public v()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 317
    iget-object v0, p0, Lcom/twitter/app/dm/DMComposeFragment;->d:Lcom/twitter/android/suggestionselection/a;

    invoke-virtual {v0}, Lcom/twitter/android/suggestionselection/a;->b()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
