.class public Lcom/twitter/app/dm/ak;
.super Lcom/twitter/library/service/j;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/String;

.field private final c:Lcom/twitter/model/dms/x;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Lcom/twitter/model/dms/x;)V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/twitter/app/dm/s;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/j;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 25
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/app/dm/ak;->a:Ljava/lang/ref/WeakReference;

    .line 26
    iput-object p3, p0, Lcom/twitter/app/dm/ak;->b:Ljava/lang/String;

    .line 27
    iput-object p4, p0, Lcom/twitter/app/dm/ak;->c:Lcom/twitter/model/dms/x;

    .line 28
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 3

    .prologue
    .line 32
    iget-object v0, p0, Lcom/twitter/app/dm/ak;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 33
    if-eqz v0, :cond_0

    .line 35
    invoke-virtual {p0}, Lcom/twitter/app/dm/ak;->h()Lcom/twitter/library/service/v;

    move-result-object v0

    iget-wide v0, v0, Lcom/twitter/library/service/v;->c:J

    invoke-static {v0, v1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/ak;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/app/dm/ak;->c:Lcom/twitter/model/dms/x;

    .line 36
    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/provider/t;->a(Ljava/lang/String;Lcom/twitter/model/dms/x;)V

    .line 38
    :cond_0
    return-void
.end method
