.class Lcom/twitter/app/dm/DMActivity$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/dm/DMActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/dm/DMActivity;


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/DMActivity;)V
    .locals 0

    .prologue
    .line 416
    iput-object p1, p0, Lcom/twitter/app/dm/DMActivity$2;->a:Lcom/twitter/app/dm/DMActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 420
    iget-object v1, p0, Lcom/twitter/app/dm/DMActivity$2;->a:Lcom/twitter/app/dm/DMActivity;

    invoke-static {v1}, Lcom/twitter/app/dm/DMActivity;->b(Lcom/twitter/app/dm/DMActivity;)I

    move-result v1

    const/4 v3, 0x3

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/twitter/app/dm/DMActivity$2;->a:Lcom/twitter/app/dm/DMActivity;

    .line 421
    invoke-static {v1}, Lcom/twitter/app/dm/DMActivity;->c(Lcom/twitter/app/dm/DMActivity;)Lcom/twitter/app/dm/DMConversationFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/app/dm/DMConversationFragment;->w()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v0

    .line 422
    :goto_0
    new-instance v3, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v4, p0, Lcom/twitter/app/dm/DMActivity$2;->a:Lcom/twitter/app/dm/DMActivity;

    invoke-static {v4}, Lcom/twitter/app/dm/DMActivity;->d(Lcom/twitter/app/dm/DMActivity;)Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v4, "app:twitter_service:direct_messages::discard_dm"

    aput-object v4, v0, v2

    .line 423
    invoke-virtual {v3, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 424
    invoke-static {}, Lcrr;->h()Lcrr;

    move-result-object v2

    invoke-virtual {v2}, Lcrr;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string/jumbo v2, "connected"

    :goto_1
    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->h(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    if-eqz v1, :cond_2

    const-string/jumbo v1, "has_media"

    .line 426
    :goto_2
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->k(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 422
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 428
    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity$2;->a:Lcom/twitter/app/dm/DMActivity;

    invoke-static {v0}, Lcom/twitter/app/dm/DMActivity;->e(Lcom/twitter/app/dm/DMActivity;)V

    .line 430
    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity$2;->a:Lcom/twitter/app/dm/DMActivity;

    invoke-static {v0}, Lcom/twitter/app/dm/DMActivity;->f(Lcom/twitter/app/dm/DMActivity;)V

    .line 431
    return-void

    :cond_0
    move v1, v2

    .line 421
    goto :goto_0

    .line 424
    :cond_1
    const-string/jumbo v2, "disconnected"

    goto :goto_1

    :cond_2
    const-string/jumbo v1, "no_media"

    goto :goto_2
.end method
