.class public Lcom/twitter/app/dm/DMActivity;
.super Lcom/twitter/app/common/base/TwitterFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/dm/q$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/DMActivity$a;,
        Lcom/twitter/app/dm/DMActivity$b;
    }
.end annotation


# instance fields
.field private a:Lcom/twitter/app/dm/DMActivity$b;

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:Z

.field private e:I

.field private f:Lcom/twitter/app/dm/DMConversationFragment;

.field private g:Lcom/twitter/app/dm/DMComposeFragment;

.field private h:Lcom/twitter/android/widget/DraggableDrawerLayout;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/DMActivity;)J
    .locals 2

    .prologue
    .line 64
    iget-wide v0, p0, Lcom/twitter/app/dm/DMActivity;->F:J

    return-wide v0
.end method

.method static synthetic a(Lcom/twitter/app/dm/DMActivity;Ljava/lang/String;[JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct/range {p0 .. p5}, Lcom/twitter/app/dm/DMActivity;->a(Ljava/lang/String;[JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;Landroid/net/Uri;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 182
    new-instance v0, Lcom/twitter/app/dm/j$a;

    invoke-direct {v0}, Lcom/twitter/app/dm/j$a;-><init>()V

    .line 183
    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/j$a;->c(Ljava/lang/String;)Lcom/twitter/app/dm/j$a;

    move-result-object v0

    .line 184
    invoke-static {p2}, Lcom/twitter/util/collection/CollectionUtils;->e(Ljava/util/Collection;)[J

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/j$a;->a([J)Lcom/twitter/app/dm/j$a;

    move-result-object v0

    .line 185
    invoke-static {p3}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/j$a;->a(Z)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/j$a;

    .line 186
    invoke-virtual {v0, p3}, Lcom/twitter/app/dm/j$a;->a(Ljava/lang/String;)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/j$a;

    .line 187
    invoke-virtual {v0, p4}, Lcom/twitter/app/dm/j$a;->a(Landroid/net/Uri;)Lcom/twitter/app/dm/j$a;

    move-result-object v0

    .line 188
    invoke-virtual {v0}, Lcom/twitter/app/dm/j$a;->e()Lcom/twitter/app/dm/j;

    move-result-object v0

    .line 182
    invoke-static {p0, v0, p5}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/j;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMActivity;->startActivity(Landroid/content/Intent;)V

    .line 189
    if-eqz p5, :cond_0

    .line 190
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMActivity;->finish()V

    .line 192
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;[JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 509
    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 515
    :goto_0
    return-void

    .line 512
    :cond_0
    iput-object p1, p0, Lcom/twitter/app/dm/DMActivity;->b:Ljava/lang/String;

    .line 514
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/twitter/app/dm/DMActivity;->b([JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/util/Set;Ljava/lang/String;Landroid/net/Uri;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 146
    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/dm/DMActivity;->d:Z

    if-eqz v0, :cond_0

    .line 147
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 148
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v0

    .line 149
    invoke-virtual {v0, p1}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Iterable;)Lcom/twitter/util/collection/o;

    move-result-object v0

    .line 150
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    move-result-object v0

    .line 151
    invoke-virtual {v0}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 152
    new-instance v1, Lcom/twitter/app/dm/q$a;

    invoke-direct {v1}, Lcom/twitter/app/dm/q$a;-><init>()V

    new-instance v4, Lbta;

    .line 153
    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v2

    invoke-direct {v4, v2}, Lbta;-><init>(Lcom/twitter/database/model/i;)V

    invoke-virtual {v1, v4}, Lcom/twitter/app/dm/q$a;->a(Lbta;)Lcom/twitter/app/dm/q$a;

    move-result-object v1

    .line 154
    invoke-virtual {v1, v0}, Lcom/twitter/app/dm/q$a;->a(Ljava/util/Set;)Lcom/twitter/app/dm/q$a;

    move-result-object v0

    .line 155
    invoke-virtual {v0, p0}, Lcom/twitter/app/dm/q$a;->a(Lcom/twitter/app/dm/q$b;)Lcom/twitter/app/dm/q$a;

    move-result-object v0

    .line 156
    invoke-virtual {v0, p4}, Lcom/twitter/app/dm/q$a;->a(Z)Lcom/twitter/app/dm/q$a;

    move-result-object v0

    .line 157
    invoke-virtual {v0, p2}, Lcom/twitter/app/dm/q$a;->a(Ljava/lang/String;)Lcom/twitter/app/dm/q$a;

    move-result-object v0

    .line 158
    invoke-virtual {v0, p3}, Lcom/twitter/app/dm/q$a;->a(Landroid/net/Uri;)Lcom/twitter/app/dm/q$a;

    move-result-object v0

    .line 159
    invoke-virtual {v0}, Lcom/twitter/app/dm/q$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/q;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 160
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/q;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 165
    :goto_0
    return-void

    .line 162
    :cond_0
    iget-object v1, p0, Lcom/twitter/app/dm/DMActivity;->b:Ljava/lang/String;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/app/dm/DMActivity;->a(Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;Landroid/net/Uri;Z)V

    goto :goto_0
.end method

.method private a([JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 503
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/twitter/library/dm/e;->a(J[J)Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/app/dm/DMActivity;->a(Ljava/lang/String;[JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    .line 505
    return-void
.end method

.method private a(Landroid/os/Bundle;)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 256
    invoke-static {p1}, Lcom/twitter/app/dm/j;->d(Landroid/os/Bundle;)Lcom/twitter/app/dm/j;

    move-result-object v0

    .line 257
    invoke-virtual {v0}, Lcom/twitter/app/dm/j;->j()Ljava/lang/String;

    move-result-object v1

    .line 258
    invoke-virtual {v0}, Lcom/twitter/app/dm/j;->B()Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/app/dm/DMActivity;->c:Z

    .line 259
    if-eqz v1, :cond_1

    .line 260
    invoke-virtual {v0}, Lcom/twitter/app/dm/j;->A()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 261
    const-string/jumbo v2, "dm:conversation_load"

    .line 262
    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v3

    sget-object v4, Lcom/twitter/metrics/g;->m:Lcom/twitter/metrics/g$b;

    .line 261
    invoke-static {v2, v3, v4}, Lcom/twitter/metrics/n;->b(Ljava/lang/String;Lcom/twitter/metrics/j;Lcom/twitter/metrics/g$b;)Lcom/twitter/metrics/n;

    move-result-object v2

    .line 262
    invoke-virtual {v2}, Lcom/twitter/metrics/n;->i()V

    .line 264
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/twitter/app/dm/j;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/twitter/app/dm/j;->k()Landroid/net/Uri;

    move-result-object v4

    .line 265
    invoke-virtual {v0}, Lcom/twitter/app/dm/j;->z()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    .line 264
    invoke-direct/range {v0 .. v5}, Lcom/twitter/app/dm/DMActivity;->a(Ljava/lang/String;[JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    move v0, v6

    .line 276
    :goto_0
    return v0

    .line 269
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/app/dm/j;->f()[J

    move-result-object v1

    .line 270
    if-eqz v1, :cond_2

    .line 271
    invoke-virtual {v0}, Lcom/twitter/app/dm/j;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/twitter/app/dm/j;->k()Landroid/net/Uri;

    move-result-object v3

    .line 272
    invoke-virtual {v0}, Lcom/twitter/app/dm/j;->z()Ljava/lang/String;

    move-result-object v0

    .line 271
    invoke-direct {p0, v1, v2, v3, v0}, Lcom/twitter/app/dm/DMActivity;->a([JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    move v0, v6

    .line 273
    goto :goto_0

    .line 276
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/os/Bundle;Ljava/lang/String;Z)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 286
    invoke-static {p1}, Lcom/twitter/app/dm/h;->d(Landroid/os/Bundle;)Lcom/twitter/app/dm/h;

    move-result-object v3

    .line 287
    const-string/jumbo v1, "android.intent.action.SEND"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 288
    invoke-virtual {v3}, Lcom/twitter/app/dm/h;->f()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    move v1, v2

    .line 289
    :goto_0
    invoke-virtual {v3}, Lcom/twitter/app/dm/h;->c()Ljava/lang/String;

    move-result-object v4

    .line 290
    if-nez p3, :cond_1

    invoke-virtual {v3}, Lcom/twitter/app/dm/h;->d()Z

    move-result v3

    invoke-direct {p0, v4, v3, v1}, Lcom/twitter/app/dm/DMActivity;->a(Ljava/lang/String;ZZ)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 293
    :cond_1
    new-instance v0, Lcom/twitter/app/dm/DMComposeFragment;

    invoke-direct {v0}, Lcom/twitter/app/dm/DMComposeFragment;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/dm/DMActivity;->g:Lcom/twitter/app/dm/DMComposeFragment;

    .line 294
    iget-object v3, p0, Lcom/twitter/app/dm/DMActivity;->g:Lcom/twitter/app/dm/DMComposeFragment;

    new-instance v0, Lcom/twitter/app/dm/h$a;

    invoke-direct {v0, p1}, Lcom/twitter/app/dm/h$a;-><init>(Landroid/os/Bundle;)V

    .line 295
    invoke-virtual {v0, v4}, Lcom/twitter/app/dm/h$a;->a(Ljava/lang/String;)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/h$a;

    .line 296
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/h$a;->g(Z)Lcom/twitter/app/dm/h$a;

    move-result-object v0

    .line 297
    invoke-virtual {v0}, Lcom/twitter/app/dm/h$a;->e()Lcom/twitter/app/dm/h;

    move-result-object v0

    .line 294
    invoke-virtual {v3, v0}, Lcom/twitter/app/dm/DMComposeFragment;->a(Lcom/twitter/app/common/base/b;)V

    .line 298
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 299
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f1302e4

    iget-object v3, p0, Lcom/twitter/app/dm/DMActivity;->g:Lcom/twitter/app/dm/DMComposeFragment;

    const-string/jumbo v4, "mthread"

    .line 300
    invoke-virtual {v0, v1, v3, v4}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 301
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 302
    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/app/dm/DMActivity;->e:I

    move v0, v2

    .line 306
    :cond_2
    return v0

    :cond_3
    move v1, v0

    .line 288
    goto :goto_0
.end method

.method protected static a(Lcom/twitter/app/dm/b;)Z
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 322
    invoke-virtual {p0}, Lcom/twitter/app/dm/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/dm/b;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    invoke-virtual {p0}, Lcom/twitter/app/dm/b;->n()Landroid/os/Bundle;

    move-result-object v0

    .line 323
    invoke-static {v0}, Lcom/twitter/app/dm/j;->d(Landroid/os/Bundle;)Lcom/twitter/app/dm/j;

    move-result-object v0

    .line 324
    invoke-virtual {v0}, Lcom/twitter/app/dm/j;->j()Ljava/lang/String;

    move-result-object v0

    .line 323
    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 322
    :goto_0
    return v0

    .line 323
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;ZZ)Z
    .locals 1

    .prologue
    .line 377
    invoke-static {p1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p3, :cond_0

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/app/dm/DMActivity;)I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/twitter/app/dm/DMActivity;->e:I

    return v0
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 328
    invoke-static {p1}, Lcom/twitter/app/dm/j;->d(Landroid/os/Bundle;)Lcom/twitter/app/dm/j;

    move-result-object v0

    .line 329
    invoke-virtual {v0}, Lcom/twitter/app/dm/j;->g()Ljava/lang/String;

    move-result-object v1

    .line 330
    invoke-virtual {v0}, Lcom/twitter/app/dm/j;->f()[J

    move-result-object v2

    .line 332
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v2, :cond_1

    array-length v3, v2

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 333
    :cond_0
    invoke-direct {p0}, Lcom/twitter/app/dm/DMActivity;->l()Landroid/app/ProgressDialog;

    move-result-object v3

    .line 335
    new-instance v4, Lcom/twitter/app/dm/DMActivity$1;

    invoke-direct {v4, p0, v3, v0}, Lcom/twitter/app/dm/DMActivity$1;-><init>(Lcom/twitter/app/dm/DMActivity;Landroid/app/ProgressDialog;Lcom/twitter/app/dm/j;)V

    .line 354
    invoke-virtual {v3}, Landroid/app/ProgressDialog;->show()V

    .line 355
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 356
    invoke-static {p0, v1, v4}, Lcom/twitter/app/dm/c;->a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/app/dm/c$a;)V

    .line 362
    :cond_1
    :goto_0
    return-void

    .line 359
    :cond_2
    const/4 v0, 0x0

    aget-wide v0, v2, v0

    invoke-static {p0, v0, v1, v4}, Lcom/twitter/app/dm/c;->a(Landroid/content/Context;JLcom/twitter/app/dm/c$a;)V

    goto :goto_0
.end method

.method private b([JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 5

    .prologue
    const v4, 0x7f1302e4

    const/4 v3, 0x3

    .line 519
    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity;->f:Lcom/twitter/app/dm/DMConversationFragment;

    if-nez v0, :cond_1

    .line 520
    new-instance v0, Lcom/twitter/app/dm/DMConversationFragment;

    invoke-direct {v0}, Lcom/twitter/app/dm/DMConversationFragment;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/dm/DMActivity;->f:Lcom/twitter/app/dm/DMConversationFragment;

    .line 521
    iget-object v1, p0, Lcom/twitter/app/dm/DMActivity;->f:Lcom/twitter/app/dm/DMConversationFragment;

    new-instance v0, Lcom/twitter/app/dm/j$a;

    invoke-direct {v0}, Lcom/twitter/app/dm/j$a;-><init>()V

    .line 522
    invoke-virtual {v0, p3}, Lcom/twitter/app/dm/j$a;->a(Landroid/net/Uri;)Lcom/twitter/app/dm/j$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/app/dm/DMActivity;->b:Ljava/lang/String;

    .line 523
    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/j$a;->c(Ljava/lang/String;)Lcom/twitter/app/dm/j$a;

    move-result-object v0

    .line 524
    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/j$a;->a([J)Lcom/twitter/app/dm/j$a;

    move-result-object v0

    .line 525
    invoke-static {p2}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/j$a;->a(Z)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/j$a;

    .line 526
    invoke-virtual {v0, p2}, Lcom/twitter/app/dm/j$a;->a(Ljava/lang/String;)Lcom/twitter/app/dm/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/j$a;

    iget-boolean v2, p0, Lcom/twitter/app/dm/DMActivity;->c:Z

    .line 527
    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/j$a;->g(Z)Lcom/twitter/app/dm/j$a;

    move-result-object v0

    .line 528
    invoke-virtual {v0, p4}, Lcom/twitter/app/dm/j$a;->f(Ljava/lang/String;)Lcom/twitter/app/dm/j$a;

    move-result-object v0

    const/4 v2, 0x0

    .line 529
    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/j$a;->e(Z)Lcom/twitter/app/common/list/i$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/j$a;

    .line 530
    invoke-virtual {v0}, Lcom/twitter/app/dm/j$a;->e()Lcom/twitter/app/dm/j;

    move-result-object v0

    .line 521
    invoke-virtual {v1, v0}, Lcom/twitter/app/dm/DMConversationFragment;->a(Lcom/twitter/app/common/base/b;)V

    .line 535
    :goto_0
    iget v0, p0, Lcom/twitter/app/dm/DMActivity;->e:I

    if-eq v0, v3, :cond_0

    .line 536
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 537
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 539
    iget v1, p0, Lcom/twitter/app/dm/DMActivity;->e:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 540
    iget-object v1, p0, Lcom/twitter/app/dm/DMActivity;->f:Lcom/twitter/app/dm/DMConversationFragment;

    const-string/jumbo v2, "mthread"

    invoke-virtual {v0, v4, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 545
    :goto_1
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 547
    iput v3, p0, Lcom/twitter/app/dm/DMActivity;->e:I

    .line 549
    :cond_0
    return-void

    .line 532
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity;->f:Lcom/twitter/app/dm/DMConversationFragment;

    iget-object v1, p0, Lcom/twitter/app/dm/DMActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/DMConversationFragment;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 542
    :cond_2
    iget-object v1, p0, Lcom/twitter/app/dm/DMActivity;->f:Lcom/twitter/app/dm/DMConversationFragment;

    const-string/jumbo v2, "mthread"

    invoke-virtual {v0, v4, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_1
.end method

.method static synthetic c(Lcom/twitter/app/dm/DMActivity;)Lcom/twitter/app/dm/DMConversationFragment;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity;->f:Lcom/twitter/app/dm/DMConversationFragment;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/app/dm/DMActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/app/dm/DMActivity;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/twitter/app/dm/DMActivity;->r()V

    return-void
.end method

.method static synthetic f(Lcom/twitter/app/dm/DMActivity;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/twitter/app/dm/DMActivity;->n()V

    return-void
.end method

.method private j()V
    .locals 4

    .prologue
    .line 311
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 312
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f1302e4

    new-instance v2, Lcom/twitter/app/dm/DMInboxFragment;

    invoke-direct {v2}, Lcom/twitter/app/dm/DMInboxFragment;-><init>()V

    const-string/jumbo v3, "mthread"

    .line 313
    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 314
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 315
    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/app/dm/DMActivity;->e:I

    .line 316
    return-void
.end method

.method private l()Landroid/app/ProgressDialog;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 367
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 368
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 369
    const v1, 0x7f0a04b6

    invoke-virtual {p0, v1}, Lcom/twitter/app/dm/DMActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 370
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 371
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 372
    return-object v0
.end method

.method private n()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 468
    const/4 v0, 0x0

    .line 469
    iget v2, p0, Lcom/twitter/app/dm/DMActivity;->e:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 470
    invoke-direct {p0}, Lcom/twitter/app/dm/DMActivity;->o()V

    .line 481
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMActivity;->U()Z

    move-result v1

    if-nez v1, :cond_3

    if-eqz v0, :cond_3

    .line 482
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMActivity;->G()V

    .line 486
    :goto_1
    return-void

    .line 471
    :cond_1
    iget v2, p0, Lcom/twitter/app/dm/DMActivity;->e:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    .line 472
    invoke-direct {p0}, Lcom/twitter/app/dm/DMActivity;->r()V

    .line 473
    iget-object v2, p0, Lcom/twitter/app/dm/DMActivity;->f:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-virtual {v2}, Lcom/twitter/app/dm/DMConversationFragment;->B()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 474
    invoke-direct {p0}, Lcom/twitter/app/dm/DMActivity;->o()V

    move v0, v1

    .line 475
    goto :goto_0

    :cond_2
    move v0, v1

    .line 478
    goto :goto_0

    .line 484
    :cond_3
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onBackPressed()V

    goto :goto_1
.end method

.method private o()V
    .locals 2

    .prologue
    .line 495
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 496
    if-eqz v0, :cond_0

    .line 497
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    .line 499
    :cond_0
    return-void
.end method

.method private q()Z
    .locals 2

    .prologue
    .line 555
    iget v0, p0, Lcom/twitter/app/dm/DMActivity;->e:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity;->g:Lcom/twitter/app/dm/DMComposeFragment;

    invoke-virtual {v0}, Lcom/twitter/app/dm/DMComposeFragment;->p()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/twitter/app/dm/DMActivity;->e:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity;->f:Lcom/twitter/app/dm/DMConversationFragment;

    .line 556
    invoke-virtual {v0}, Lcom/twitter/app/dm/DMConversationFragment;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 555
    :goto_0
    return v0

    .line 556
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private r()V
    .locals 4

    .prologue
    .line 560
    iget v0, p0, Lcom/twitter/app/dm/DMActivity;->e:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity;->f:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-virtual {v0}, Lcom/twitter/app/dm/DMConversationFragment;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 561
    new-instance v0, Lcom/twitter/library/api/dm/l;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/dm/DMActivity;->b:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/twitter/library/api/dm/l;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Z)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/app/dm/DMActivity;->b(Lcom/twitter/library/service/s;I)Z

    .line 564
    :cond_0
    return-void
.end method


# virtual methods
.method protected G()V
    .locals 1

    .prologue
    .line 491
    sget-object v0, Lcom/twitter/app/main/MainActivity;->d:Landroid/net/Uri;

    invoke-static {p0, v0}, Lcom/twitter/app/main/MainActivity;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    .line 492
    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)Lcom/twitter/app/common/base/TwitterFragmentActivity$a;
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 95
    const v0, 0x7f0400ac

    invoke-virtual {p2, v0}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(I)V

    .line 96
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->c(Z)V

    .line 97
    invoke-virtual {p2, v1}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->d(I)V

    .line 98
    iget v0, p0, Lcom/twitter/app/dm/DMActivity;->e:I

    if-ne v0, v2, :cond_0

    .line 99
    invoke-virtual {p2, v2}, Lcom/twitter/app/common/base/TwitterFragmentActivity$a;->b(Z)V

    .line 101
    :cond_0
    return-object p2
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Z)V
    .locals 6

    .prologue
    .line 176
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/app/dm/DMActivity;->a(Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;Landroid/net/Uri;Z)V

    .line 177
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Landroid/net/Uri;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Landroid/net/Uri;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 170
    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p2

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/twitter/app/dm/DMActivity;->a(Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;Landroid/net/Uri;Z)V

    .line 172
    return-void
.end method

.method public a(Lcmm;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 133
    invoke-interface {p1}, Lcmm;->a()I

    move-result v1

    .line 134
    const v2, 0x7f130890

    if-ne v1, v2, :cond_0

    .line 135
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 136
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v2, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "messages:compose:::next"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v1

    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 137
    iget-object v1, p0, Lcom/twitter/app/dm/DMActivity;->g:Lcom/twitter/app/dm/DMComposeFragment;

    invoke-virtual {v1}, Lcom/twitter/app/dm/DMComposeFragment;->v()Ljava/util/Set;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/dm/DMActivity;->g:Lcom/twitter/app/dm/DMComposeFragment;

    invoke-virtual {v2}, Lcom/twitter/app/dm/DMComposeFragment;->s()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/dm/DMActivity;->g:Lcom/twitter/app/dm/DMComposeFragment;

    .line 138
    invoke-virtual {v3}, Lcom/twitter/app/dm/DMComposeFragment;->t()Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/app/dm/DMActivity;->g:Lcom/twitter/app/dm/DMComposeFragment;

    invoke-virtual {v4}, Lcom/twitter/app/dm/DMComposeFragment;->u()Z

    move-result v4

    .line 137
    invoke-direct {p0, v1, v2, v3, v4}, Lcom/twitter/app/dm/DMActivity;->a(Ljava/util/Set;Ljava/lang/String;Landroid/net/Uri;Z)V

    .line 141
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmm;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lcmr;)Z
    .locals 2

    .prologue
    .line 106
    iget v0, p0, Lcom/twitter/app/dm/DMActivity;->e:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 107
    const v0, 0x7f14000d

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 109
    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->a(Lcmr;)Z

    move-result v0

    return v0
.end method

.method public b(Lcmr;)I
    .locals 3

    .prologue
    .line 115
    invoke-interface {p1}, Lcmr;->k()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 116
    const v1, 0x7f130890

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v1

    .line 117
    if-eqz v1, :cond_0

    .line 118
    iget v0, p0, Lcom/twitter/app/dm/DMActivity;->e:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity;->g:Lcom/twitter/app/dm/DMComposeFragment;

    .line 119
    invoke-virtual {v0}, Lcom/twitter/app/dm/DMComposeFragment;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 120
    :goto_0
    invoke-virtual {v1, v0}, Lazv;->b(Z)Lazv;

    .line 123
    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_0

    .line 124
    invoke-interface {p1}, Lcmr;->g()V

    .line 127
    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->b(Lcmr;)I

    move-result v0

    return v0

    .line 119
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;Lcom/twitter/app/common/base/TwitterFragmentActivity$a;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 196
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 197
    if-nez p1, :cond_5

    .line 198
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 199
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 200
    :goto_0
    invoke-static {v0}, Lcom/twitter/app/dm/b;->a(Landroid/os/Bundle;)Lcom/twitter/app/dm/b;

    move-result-object v2

    .line 205
    invoke-static {v2}, Lcom/twitter/app/dm/DMActivity;->a(Lcom/twitter/app/dm/b;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 206
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, v4}, Lcom/twitter/app/dm/DMActivity;->a(Landroid/os/Bundle;Ljava/lang/String;Z)Z

    .line 207
    invoke-direct {p0, v0}, Lcom/twitter/app/dm/DMActivity;->b(Landroid/os/Bundle;)V

    .line 233
    :cond_0
    :goto_1
    new-instance v0, Lcom/twitter/app/dm/DMActivity$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/app/dm/DMActivity$b;-><init>(Lcom/twitter/app/dm/DMActivity;Lcom/twitter/app/dm/DMActivity$1;)V

    iput-object v0, p0, Lcom/twitter/app/dm/DMActivity;->a:Lcom/twitter/app/dm/DMActivity$b;

    .line 234
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/DMActivity;->a:Lcom/twitter/app/dm/DMActivity$b;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/u;)V

    .line 236
    iget v0, p0, Lcom/twitter/app/dm/DMActivity;->e:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 237
    const v0, 0x7f1302e0

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/ToolbarWrapperLayout;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ToolbarWrapperLayout;->a()V

    .line 241
    :cond_1
    const v0, 0x7f1302de

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/DraggableDrawerLayout;

    iput-object v0, p0, Lcom/twitter/app/dm/DMActivity;->h:Lcom/twitter/android/widget/DraggableDrawerLayout;

    .line 242
    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity;->h:Lcom/twitter/android/widget/DraggableDrawerLayout;

    if-eqz v0, :cond_2

    .line 243
    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity;->h:Lcom/twitter/android/widget/DraggableDrawerLayout;

    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Z)V

    .line 247
    :cond_2
    invoke-static {}, Lcom/twitter/library/dm/d;->a()Z

    .line 248
    invoke-static {}, Lcom/twitter/library/dm/d;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMActivity;->d:Z

    .line 249
    return-void

    .line 199
    :cond_3
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto :goto_0

    .line 208
    :cond_4
    invoke-direct {p0, v0}, Lcom/twitter/app/dm/DMActivity;->a(Landroid/os/Bundle;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 209
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, v3}, Lcom/twitter/app/dm/DMActivity;->a(Landroid/os/Bundle;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    invoke-direct {p0}, Lcom/twitter/app/dm/DMActivity;->j()V

    goto :goto_1

    .line 213
    :cond_5
    const-string/jumbo v1, "optional_current_fragment"

    .line 214
    invoke-virtual {p1, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 215
    iput v1, p0, Lcom/twitter/app/dm/DMActivity;->e:I

    .line 217
    iget v1, p0, Lcom/twitter/app/dm/DMActivity;->e:I

    packed-switch v1, :pswitch_data_0

    goto :goto_1

    .line 224
    :pswitch_0
    const-string/jumbo v1, "mthread"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/DMComposeFragment;

    iput-object v0, p0, Lcom/twitter/app/dm/DMActivity;->g:Lcom/twitter/app/dm/DMComposeFragment;

    goto :goto_1

    .line 219
    :pswitch_1
    const-string/jumbo v1, "mthread"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/DMConversationFragment;

    iput-object v0, p0, Lcom/twitter/app/dm/DMActivity;->f:Lcom/twitter/app/dm/DMConversationFragment;

    .line 220
    const-string/jumbo v0, "conversation_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/DMActivity;->b:Ljava/lang/String;

    goto/16 :goto_1

    .line 217
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected d()V
    .locals 2

    .prologue
    .line 390
    invoke-direct {p0}, Lcom/twitter/app/dm/DMActivity;->r()V

    .line 391
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMActivity;->I()Lcom/twitter/library/client/v;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/DMActivity;->a:Lcom/twitter/app/dm/DMActivity$b;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/v;->b(Lcom/twitter/library/client/u;)V

    .line 392
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->d()V

    .line 393
    return-void
.end method

.method i()Lcom/twitter/android/widget/DraggableDrawerLayout;
    .locals 1

    .prologue
    .line 568
    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity;->h:Lcom/twitter/android/widget/DraggableDrawerLayout;

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 397
    iget v0, p0, Lcom/twitter/app/dm/DMActivity;->e:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 398
    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity;->f:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/app/dm/DMConversationFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 400
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 401
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 449
    invoke-direct {p0}, Lcom/twitter/app/dm/DMActivity;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMActivity;->showDialog(I)V

    .line 456
    :goto_0
    return-void

    .line 451
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity;->f:Lcom/twitter/app/dm/DMConversationFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity;->f:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-virtual {v0}, Lcom/twitter/app/dm/DMConversationFragment;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 452
    iget-object v0, p0, Lcom/twitter/app/dm/DMActivity;->f:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-virtual {v0}, Lcom/twitter/app/dm/DMConversationFragment;->s()V

    goto :goto_0

    .line 454
    :cond_1
    invoke-direct {p0}, Lcom/twitter/app/dm/DMActivity;->n()V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 412
    packed-switch p1, :pswitch_data_0

    .line 442
    :goto_0
    return-object v0

    .line 415
    :pswitch_0
    new-instance v1, Lcom/twitter/app/dm/DMActivity$2;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/DMActivity$2;-><init>(Lcom/twitter/app/dm/DMActivity;)V

    .line 434
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0a05c5

    .line 435
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a002b

    .line 436
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a0278

    .line 437
    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a00f6

    .line 438
    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 439
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 412
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 383
    invoke-super {p0}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onResume()V

    .line 384
    invoke-static {p0}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;)Lcom/twitter/android/client/l;

    move-result-object v0

    .line 385
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMActivity;->J()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    iget-object v1, p0, Lcom/twitter/app/dm/DMActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v1}, Lcom/twitter/android/client/l;->a(JLjava/lang/String;)V

    .line 386
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 405
    invoke-super {p0, p1}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 406
    const-string/jumbo v0, "conversation_id"

    iget-object v1, p0, Lcom/twitter/app/dm/DMActivity;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    const-string/jumbo v0, "optional_current_fragment"

    iget v1, p0, Lcom/twitter/app/dm/DMActivity;->e:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 408
    return-void
.end method

.method protected p()V
    .locals 1

    .prologue
    .line 460
    invoke-direct {p0}, Lcom/twitter/app/dm/DMActivity;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMActivity;->showDialog(I)V

    .line 465
    :goto_0
    return-void

    .line 463
    :cond_0
    invoke-direct {p0}, Lcom/twitter/app/dm/DMActivity;->n()V

    goto :goto_0
.end method
