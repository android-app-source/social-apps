.class public final Lcom/twitter/app/dm/q$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/dm/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/app/dm/q;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lbta;

.field private b:Lcom/twitter/app/dm/q$b;

.field private c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/String;

.field private e:Landroid/net/Uri;

.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/q$a;)Lbta;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/app/dm/q$a;->a:Lbta;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/app/dm/q$a;)Lcom/twitter/app/dm/q$b;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/app/dm/q$a;->b:Lcom/twitter/app/dm/q$b;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/app/dm/q$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/app/dm/q$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/app/dm/q$a;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/app/dm/q$a;->e:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/app/dm/q$a;)Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/twitter/app/dm/q$a;->f:Z

    return v0
.end method

.method static synthetic f(Lcom/twitter/app/dm/q$a;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/twitter/app/dm/q$a;->c:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/twitter/app/dm/q$a;->a:Lbta;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/q$a;->b:Lcom/twitter/app/dm/q$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/q$a;->c:Ljava/util/Set;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/net/Uri;)Lcom/twitter/app/dm/q$a;
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/twitter/app/dm/q$a;->e:Landroid/net/Uri;

    .line 100
    return-object p0
.end method

.method public a(Lbta;)Lcom/twitter/app/dm/q$a;
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/twitter/app/dm/q$a;->a:Lbta;

    .line 76
    return-object p0
.end method

.method public a(Lcom/twitter/app/dm/q$b;)Lcom/twitter/app/dm/q$a;
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/twitter/app/dm/q$a;->b:Lcom/twitter/app/dm/q$b;

    .line 82
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/app/dm/q$a;
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/twitter/app/dm/q$a;->d:Ljava/lang/String;

    .line 94
    return-object p0
.end method

.method public a(Ljava/util/Set;)Lcom/twitter/app/dm/q$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/twitter/app/dm/q$a;"
        }
    .end annotation

    .prologue
    .line 87
    iput-object p1, p0, Lcom/twitter/app/dm/q$a;->c:Ljava/util/Set;

    .line 88
    return-object p0
.end method

.method public a(Z)Lcom/twitter/app/dm/q$a;
    .locals 0

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/twitter/app/dm/q$a;->f:Z

    .line 106
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/twitter/app/dm/q$a;->e()Lcom/twitter/app/dm/q;

    move-result-object v0

    return-object v0
.end method

.method protected e()Lcom/twitter/app/dm/q;
    .locals 2

    .prologue
    .line 112
    new-instance v0, Lcom/twitter/app/dm/q;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/app/dm/q;-><init>(Lcom/twitter/app/dm/q$a;Lcom/twitter/app/dm/q$1;)V

    return-object v0
.end method
