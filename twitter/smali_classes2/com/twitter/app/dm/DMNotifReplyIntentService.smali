.class public Lcom/twitter/app/dm/DMNotifReplyIntentService;
.super Landroid/app/IntentService;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    const-string/jumbo v0, "ReplyIntent"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 32
    return-void
.end method

.method private static a(Landroid/os/Bundle;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 55
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a()Landroid/app/NotificationManager;
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMNotifReplyIntentService;->getApplication()Landroid/app/Application;

    move-result-object v0

    const-string/jumbo v1, "notification"

    invoke-virtual {v0, v1}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    return-object v0
.end method

.method a(JLjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 62
    new-instance v0, Lcom/twitter/library/api/dm/q$a;

    invoke-direct {v0}, Lcom/twitter/library/api/dm/q$a;-><init>()V

    .line 63
    invoke-virtual {v0, p0}, Lcom/twitter/library/api/dm/q$a;->a(Landroid/content/Context;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    .line 64
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/dm/q$a;->a(Lcom/twitter/library/client/Session;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    .line 65
    invoke-virtual {v0, p3}, Lcom/twitter/library/api/dm/q$a;->b(Ljava/lang/String;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    .line 66
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/dm/q$a;->c(Ljava/lang/String;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    .line 67
    invoke-virtual {v0, p4}, Lcom/twitter/library/api/dm/q$a;->d(Ljava/lang/String;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lcom/twitter/library/api/dm/q$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/dm/q;

    .line 69
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 70
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p1, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:notifications:::send_dm"

    aput-object v3, v1, v2

    .line 71
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 70
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 72
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 36
    if-eqz p1, :cond_1

    .line 37
    invoke-static {p1}, Landroid/support/v4/app/RemoteInput;->getResultsFromIntent(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v0

    .line 38
    const-string/jumbo v1, "extra_conversation_id"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 39
    invoke-static {v0, v1}, Lcom/twitter/app/dm/DMNotifReplyIntentService;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 40
    const-string/jumbo v2, "extra_notification_reply"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 41
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 42
    const-string/jumbo v2, "extra_owner_id"

    const-wide/16 v4, -0x1

    invoke-virtual {p1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 43
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v3, v1, v0}, Lcom/twitter/app/dm/DMNotifReplyIntentService;->a(JLjava/lang/String;Ljava/lang/String;)V

    .line 45
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMNotifReplyIntentService;->a()Landroid/app/NotificationManager;

    move-result-object v0

    .line 46
    if-eqz v0, :cond_1

    .line 47
    const-string/jumbo v1, "extra_notification_id"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 48
    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 52
    :cond_1
    return-void
.end method
