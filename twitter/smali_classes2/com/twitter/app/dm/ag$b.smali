.class Lcom/twitter/app/dm/ag$b;
.super Lcom/twitter/util/serialization/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/dm/ag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/serialization/i",
        "<",
        "Lcom/twitter/app/dm/ag$a;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 609
    invoke-direct {p0}, Lcom/twitter/util/serialization/i;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/app/dm/ag$1;)V
    .locals 0

    .prologue
    .line 609
    invoke-direct {p0}, Lcom/twitter/app/dm/ag$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/app/dm/ag$a;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 629
    sget-object v0, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    sget-object v1, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    .line 630
    invoke-static {v0, v1}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    .line 629
    invoke-virtual {p1, v0}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 631
    sget-object v1, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    sget-object v2, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    .line 632
    invoke-static {v1, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    .line 631
    invoke-virtual {p1, v1}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 633
    sget-object v2, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    .line 634
    invoke-static {v2}, Lcom/twitter/util/collection/d;->b(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 633
    invoke-virtual {p1, v2}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    .line 635
    sget-object v3, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    .line 636
    invoke-static {v3}, Lcom/twitter/util/collection/d;->b(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v3

    .line 635
    invoke-virtual {p1, v3}, Lcom/twitter/util/serialization/n;->a(Lcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Set;

    .line 638
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 640
    new-instance v4, Lcom/twitter/app/dm/ag$a;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/twitter/app/dm/ag$a;-><init>(Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;)V

    move-object v0, v4

    .line 643
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/app/dm/ag$a;

    invoke-direct {v0}, Lcom/twitter/app/dm/ag$a;-><init>()V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/util/serialization/o;Lcom/twitter/app/dm/ag$a;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 614
    .line 615
    invoke-virtual {p2}, Lcom/twitter/app/dm/ag$a;->a()Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    sget-object v2, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    .line 616
    invoke-static {v1, v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v1

    .line 615
    invoke-virtual {p1, v0, v1}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 617
    invoke-virtual {p2}, Lcom/twitter/app/dm/ag$a;->b()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    sget-object v2, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    sget-object v3, Lcom/twitter/util/serialization/f;->i:Lcom/twitter/util/serialization/l;

    .line 618
    invoke-static {v2, v3}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 617
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 619
    invoke-virtual {p2}, Lcom/twitter/app/dm/ag$a;->c()Ljava/util/Set;

    move-result-object v1

    sget-object v2, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    .line 620
    invoke-static {v2}, Lcom/twitter/util/collection/d;->b(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 619
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    move-result-object v0

    .line 621
    invoke-virtual {p2}, Lcom/twitter/app/dm/ag$a;->d()Ljava/util/Set;

    move-result-object v1

    sget-object v2, Lcom/twitter/util/serialization/f;->f:Lcom/twitter/util/serialization/l;

    .line 622
    invoke-static {v2}, Lcom/twitter/util/collection/d;->b(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 621
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/serialization/o;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/o;

    .line 623
    return-void
.end method

.method protected synthetic a_(Lcom/twitter/util/serialization/o;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 609
    check-cast p2, Lcom/twitter/app/dm/ag$a;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/dm/ag$b;->a(Lcom/twitter/util/serialization/o;Lcom/twitter/app/dm/ag$a;)V

    return-void
.end method

.method protected synthetic b(Lcom/twitter/util/serialization/n;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 609
    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/dm/ag$b;->a(Lcom/twitter/util/serialization/n;I)Lcom/twitter/app/dm/ag$a;

    move-result-object v0

    return-object v0
.end method
