.class public Lcom/twitter/app/dm/ShareViaDMComposeFragment;
.super Lcom/twitter/app/dm/DMComposeFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/autocomplete/b$d;
.implements Lcom/twitter/android/provider/a$a;
.implements Lcom/twitter/app/dm/q$b;
.implements Lcom/twitter/app/dm/widget/DMMessageComposer$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/dm/DMComposeFragment;",
        "Lcom/twitter/android/autocomplete/b$d",
        "<",
        "Lcom/twitter/android/provider/d$b;",
        "Lcom/twitter/library/provider/c;",
        ">;",
        "Lcom/twitter/android/provider/a$a;",
        "Lcom/twitter/app/dm/q$b;",
        "Lcom/twitter/app/dm/widget/DMMessageComposer$a;"
    }
.end annotation


# instance fields
.field private g:Lcom/twitter/app/dm/ac;

.field private h:Lcom/twitter/app/dm/widget/ShareViaDMMessageComposer;

.field private i:Lcom/twitter/library/view/QuoteView;

.field private j:Lcom/twitter/app/dm/y;

.field private k:Z

.field private l:Z

.field private m:Lcom/twitter/android/provider/d;

.field private n:Lcom/twitter/android/provider/d$b;

.field private o:Z

.field private p:Z

.field private q:Lcom/twitter/app/dm/aa;

.field private r:Lcom/twitter/model/core/r;

.field private s:Z

.field private t:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field private u:Ljava/lang/String;

.field private v:Lcom/twitter/app/dm/r;

.field private w:Lbta;

.field private x:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/twitter/app/dm/DMComposeFragment;-><init>()V

    return-void
.end method

.method private A()V
    .locals 2

    .prologue
    .line 389
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->h:Lcom/twitter/app/dm/widget/ShareViaDMMessageComposer;

    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->g:Lcom/twitter/app/dm/ac;

    invoke-virtual {v1}, Lcom/twitter/app/dm/ac;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/widget/ShareViaDMMessageComposer;->setHasValidRecipients(Z)V

    .line 390
    return-void
.end method

.method private B()Z
    .locals 1

    .prologue
    .line 393
    invoke-direct {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->C()Z

    move-result v0

    return v0
.end method

.method private C()Z
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    invoke-virtual {v0}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private D()V
    .locals 2

    .prologue
    .line 401
    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->Y()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 402
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->j:Lcom/twitter/app/dm/y;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->j:Lcom/twitter/app/dm/y;

    invoke-virtual {v0}, Lcom/twitter/app/dm/y;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 403
    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 408
    :cond_0
    :goto_0
    return-void

    .line 405
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->t:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    goto :goto_0
.end method

.method private E()Lcom/twitter/app/dm/ab;
    .locals 2

    .prologue
    .line 412
    new-instance v0, Lcom/twitter/app/dm/ab$a;

    invoke-direct {v0}, Lcom/twitter/app/dm/ab$a;-><init>()V

    .line 413
    invoke-direct {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->C()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/ab$a;->a(Z)Lcom/twitter/app/dm/ab$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->d:Lcom/twitter/android/suggestionselection/a;

    .line 414
    invoke-virtual {v1}, Lcom/twitter/android/suggestionselection/a;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/ab$a;->c(Z)Lcom/twitter/app/dm/ab$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->k:Z

    .line 415
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/ab$a;->e(Z)Lcom/twitter/app/dm/ab$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->g:Lcom/twitter/app/dm/ac;

    .line 416
    invoke-virtual {v1}, Lcom/twitter/app/dm/ac;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/ab$a;->b(Z)Lcom/twitter/app/dm/ab$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->g:Lcom/twitter/app/dm/ac;

    .line 417
    invoke-virtual {v1}, Lcom/twitter/app/dm/ac;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/ab$a;->d(Z)Lcom/twitter/app/dm/ab$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->o:Z

    .line 418
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/ab$a;->f(Z)Lcom/twitter/app/dm/ab$a;

    move-result-object v0

    .line 419
    invoke-virtual {v0}, Lcom/twitter/app/dm/ab$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/ab;

    .line 412
    return-object v0
.end method

.method private F()Z
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->g:Lcom/twitter/app/dm/ac;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ac;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->d:Lcom/twitter/android/suggestionselection/a;

    invoke-virtual {v0}, Lcom/twitter/android/suggestionselection/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->g:Lcom/twitter/app/dm/ac;

    .line 484
    invoke-virtual {v0}, Lcom/twitter/app/dm/ac;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 483
    :goto_0
    return v0

    .line 484
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/app/dm/ShareViaDMComposeFragment;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->b:Landroid/widget/ListView;

    return-object v0
.end method

.method private a(Lcom/twitter/app/dm/widget/DMMessageComposer$a;Lcom/twitter/model/core/r;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->h:Lcom/twitter/app/dm/widget/ShareViaDMMessageComposer;

    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/widget/ShareViaDMMessageComposer;->setListener(Lcom/twitter/app/dm/widget/DMMessageComposer$a;)V

    .line 253
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->h:Lcom/twitter/app/dm/widget/ShareViaDMMessageComposer;

    invoke-virtual {v0, p2, p3, p4}, Lcom/twitter/app/dm/widget/ShareViaDMMessageComposer;->a(Lcom/twitter/model/core/r;Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 340
    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 342
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->S:Lcom/twitter/library/client/p;

    new-instance v2, Lcom/twitter/library/api/dm/t;

    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->g:Lcom/twitter/app/dm/ac;

    .line 343
    invoke-virtual {v4}, Lcom/twitter/app/dm/ac;->e()Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v1, v3, v4}, Lcom/twitter/library/api/dm/t;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/util/Collection;)V

    .line 342
    invoke-virtual {v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 344
    new-instance v0, Lcom/twitter/library/api/dm/q$a;

    invoke-direct {v0}, Lcom/twitter/library/api/dm/q$a;-><init>()V

    .line 345
    invoke-virtual {v0, v1}, Lcom/twitter/library/api/dm/q$a;->a(Landroid/content/Context;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    .line 346
    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/api/dm/q$a;->a(Lcom/twitter/library/client/Session;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    .line 347
    invoke-virtual {v0, p1}, Lcom/twitter/library/api/dm/q$a;->b(Ljava/lang/String;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    .line 348
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/api/dm/q$a;->c(Ljava/lang/String;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    .line 349
    invoke-virtual {v0, p2}, Lcom/twitter/library/api/dm/q$a;->d(Ljava/lang/String;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->r:Lcom/twitter/model/core/r;

    .line 350
    invoke-virtual {v0, v2}, Lcom/twitter/library/api/dm/q$a;->a(Lcom/twitter/model/core/r;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->g:Lcom/twitter/app/dm/ac;

    .line 351
    invoke-virtual {v2}, Lcom/twitter/app/dm/ac;->f()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/api/dm/q$a;->a(Ljava/util/Set;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    .line 352
    invoke-virtual {v0}, Lcom/twitter/library/api/dm/q$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/dm/q;

    .line 353
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v2

    new-instance v3, Lcom/twitter/app/dm/ShareViaDMComposeFragment$3;

    invoke-direct {v3, p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment$3;-><init>(Lcom/twitter/app/dm/ShareViaDMComposeFragment;)V

    invoke-virtual {v2, v0, v3}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 364
    invoke-direct {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->z()V

    .line 366
    const v0, 0x7f0a07ff

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 367
    invoke-static {v1}, Lcom/twitter/library/dm/e;->c(Landroid/content/Context;)V

    .line 369
    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 370
    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 371
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/ShareViaDMComposeFragment;Z)Z
    .locals 0

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->o:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/app/dm/ShareViaDMComposeFragment;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->b:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/app/dm/ShareViaDMComposeFragment;)Lcom/twitter/app/dm/ab;
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->E()Lcom/twitter/app/dm/ab;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/app/dm/ShareViaDMComposeFragment;)Lcom/twitter/app/dm/aa;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->q:Lcom/twitter/app/dm/aa;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/app/dm/ShareViaDMComposeFragment;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->y()V

    return-void
.end method

.method static synthetic f(Lcom/twitter/app/dm/ShareViaDMComposeFragment;)Lcom/twitter/app/dm/y;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->j:Lcom/twitter/app/dm/y;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/app/dm/ShareViaDMComposeFragment;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->D()V

    return-void
.end method

.method static synthetic h(Lcom/twitter/app/dm/ShareViaDMComposeFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->T:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/app/dm/ShareViaDMComposeFragment;)Lcom/twitter/app/dm/ac;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->g:Lcom/twitter/app/dm/ac;

    return-object v0
.end method

.method private x()V
    .locals 2

    .prologue
    .line 126
    new-instance v0, Lcom/twitter/android/autocomplete/b;

    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->m:Lcom/twitter/android/provider/d;

    invoke-direct {v0, v1, p0}, Lcom/twitter/android/autocomplete/b;-><init>(Lna;Lcom/twitter/android/autocomplete/b$d;)V

    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->n:Lcom/twitter/android/provider/d$b;

    invoke-virtual {v0, v1}, Lcom/twitter/android/autocomplete/b;->a(Ljava/lang/Object;)V

    .line 127
    return-void
.end method

.method private y()V
    .locals 3

    .prologue
    .line 295
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    .line 296
    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->c:Lmq;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->c:Lmq;

    invoke-virtual {v1, v0}, Lmq;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 297
    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->b:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 299
    :cond_0
    return-void
.end method

.method private z()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 375
    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 376
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "messages:share_tweet_conversation"

    aput-object v5, v1, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->u:Ljava/lang/String;

    aput-object v5, v1, v4

    const/4 v4, 0x2

    const-string/jumbo v5, ":send_tweet_dm"

    aput-object v5, v1, v4

    .line 377
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 378
    iget-boolean v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->s:Z

    if-eqz v1, :cond_0

    .line 379
    invoke-static {v0, v2, v3, v6, v6}, Lcom/twitter/library/scribe/c;->b(Lcom/twitter/analytics/feature/model/ClientEventLog;JLcgi;Ljava/lang/String;)V

    .line 380
    const-string/jumbo v1, "2586390716:message_me"

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->k(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 381
    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->r:Lcom/twitter/model/core/r;

    iget-object v1, v1, Lcom/twitter/model/core/r;->k:Lcax;

    if-eqz v1, :cond_0

    .line 382
    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->r:Lcom/twitter/model/core/r;

    iget-object v1, v1, Lcom/twitter/model/core/r;->k:Lcax;

    invoke-virtual {v1}, Lcax;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->f(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 385
    :cond_0
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 386
    return-void
.end method


# virtual methods
.method protected a(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 4

    .prologue
    .line 137
    invoke-super {p0, p1}, Lcom/twitter/app/dm/DMComposeFragment;->a(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v1

    .line 139
    const v0, 0x7f13027b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/view/QuoteView;

    iput-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->i:Lcom/twitter/library/view/QuoteView;

    .line 140
    const v0, 0x7f1307f2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/widget/ShareViaDMMessageComposer;

    iput-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->h:Lcom/twitter/app/dm/widget/ShareViaDMMessageComposer;

    .line 142
    iget-boolean v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->l:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/d;->f(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->s:Z

    if-nez v0, :cond_0

    .line 143
    new-instance v2, Lcom/twitter/app/dm/y;

    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const v0, 0x7f1307ec

    .line 144
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {v2, v3, v0}, Lcom/twitter/app/dm/y;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    iput-object v2, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->j:Lcom/twitter/app/dm/y;

    .line 147
    :cond_0
    return-object v1
.end method

.method public a(JLcom/twitter/model/core/TwitterUser;)V
    .locals 4

    .prologue
    .line 435
    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->g:Lcom/twitter/app/dm/ac;

    const/4 v0, 0x1

    new-array v2, v0, [Lcom/twitter/library/provider/c;

    const/4 v3, 0x0

    new-instance v0, Lcom/twitter/library/provider/d$a;

    invoke-direct {v0}, Lcom/twitter/library/provider/d$a;-><init>()V

    .line 436
    invoke-virtual {v0, p3}, Lcom/twitter/library/provider/d$a;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/library/provider/d$a;

    move-result-object v0

    .line 437
    invoke-virtual {v0}, Lcom/twitter/library/provider/d$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/c;

    aput-object v0, v2, v3

    .line 435
    invoke-virtual {v1, v2}, Lcom/twitter/app/dm/ac;->a([Lcom/twitter/library/provider/c;)V

    .line 438
    return-void
.end method

.method public a(Lcom/twitter/android/provider/d$b;Lcbi;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/provider/d$b;",
            "Lcbi",
            "<",
            "Lcom/twitter/library/provider/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 514
    iput-boolean v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->k:Z

    .line 515
    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->Y()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 516
    invoke-virtual {p2}, Lcbi;->be_()I

    move-result v1

    if-nez v1, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->p:Z

    .line 517
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->b:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/app/dm/e;->a(Landroid/content/Context;)Landroid/view/animation/LayoutAnimationController;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V

    .line 518
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->q:Lcom/twitter/app/dm/aa;

    invoke-direct {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->E()Lcom/twitter/app/dm/ab;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/aa;->b(Lcom/twitter/app/dm/ab;)V

    .line 520
    :cond_0
    return-void

    .line 516
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/TwitterUser;Lcom/twitter/model/dms/t;)V
    .locals 4

    .prologue
    .line 453
    iget-boolean v0, p2, Lcom/twitter/model/dms/t;->a:Z

    if-eqz v0, :cond_0

    .line 454
    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->g:Lcom/twitter/app/dm/ac;

    const/4 v0, 0x1

    new-array v2, v0, [Lcom/twitter/library/provider/c;

    const/4 v3, 0x0

    new-instance v0, Lcom/twitter/library/provider/d$a;

    invoke-direct {v0}, Lcom/twitter/library/provider/d$a;-><init>()V

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/d$a;->a(Lcom/twitter/model/core/TwitterUser;)Lcom/twitter/library/provider/d$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/provider/d$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/c;

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/app/dm/ac;->a([Lcom/twitter/library/provider/c;)V

    .line 456
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/model/dms/q;)V
    .locals 8

    .prologue
    .line 442
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->f:Lcom/twitter/app/dm/g;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/g;

    const-wide/16 v2, -0x1

    new-instance v1, Lcom/twitter/library/dm/b;

    .line 444
    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-direct {v1, p1, v4, v6, v7}, Lcom/twitter/library/dm/b;-><init>(Lcom/twitter/model/dms/q;Landroid/content/Context;J)V

    invoke-virtual {v1}, Lcom/twitter/library/dm/b;->a()Ljava/lang/String;

    move-result-object v1

    .line 442
    invoke-virtual {v0, v2, v3, v1}, Lcom/twitter/app/dm/g;->a(JLjava/lang/String;)V

    .line 446
    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->g:Lcom/twitter/app/dm/ac;

    const/4 v0, 0x1

    new-array v2, v0, [Lcom/twitter/library/provider/c;

    const/4 v3, 0x0

    new-instance v0, Lcom/twitter/library/provider/b$a;

    invoke-direct {v0}, Lcom/twitter/library/provider/b$a;-><init>()V

    .line 447
    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/b$a;->a(Lcom/twitter/model/dms/q;)Lcom/twitter/library/provider/b$a;

    move-result-object v0

    .line 448
    invoke-virtual {v0}, Lcom/twitter/library/provider/b$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/c;

    aput-object v0, v2, v3

    .line 446
    invoke-virtual {v1, v2}, Lcom/twitter/app/dm/ac;->a([Lcom/twitter/library/provider/c;)V

    .line 449
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lcbi;)V
    .locals 0

    .prologue
    .line 68
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->a(Ljava/lang/String;Lcbi;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lcbi;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcbi",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 284
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->q:Lcom/twitter/app/dm/aa;

    invoke-direct {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->E()Lcom/twitter/app/dm/ab;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/aa;->a(Lcom/twitter/app/dm/ab;)V

    .line 285
    invoke-direct {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->A()V

    .line 286
    invoke-direct {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->D()V

    .line 287
    invoke-direct {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->o:Z

    if-eqz v0, :cond_0

    .line 288
    invoke-direct {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->y()V

    .line 292
    :goto_0
    return-void

    .line 290
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/twitter/app/dm/DMComposeFragment;->a(Ljava/lang/Object;Lcbi;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Landroid/net/Uri;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Landroid/net/Uri;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 311
    invoke-direct {p0, p1, p2}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->B()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;JLjava/lang/Object;I)Z
    .locals 6

    .prologue
    .line 68
    move-object v1, p1

    check-cast v1, Ljava/lang/String;

    move-object v0, p0

    move-wide v2, p2

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->a(Ljava/lang/String;JLjava/lang/Object;I)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;JLjava/lang/Object;I)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 424
    invoke-super/range {p0 .. p5}, Lcom/twitter/app/dm/DMComposeFragment;->a(Ljava/lang/String;JLjava/lang/Object;I)Z

    .line 426
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->g:Lcom/twitter/app/dm/ac;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ac;->d()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 427
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->h:Lcom/twitter/app/dm/widget/ShareViaDMMessageComposer;

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/ShareViaDMMessageComposer;->m()V

    .line 429
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->q:Lcom/twitter/app/dm/aa;

    invoke-direct {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->E()Lcom/twitter/app/dm/ab;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/aa;->b(Lcom/twitter/app/dm/ab;)V

    .line 430
    return v2
.end method

.method public b()V
    .locals 1

    .prologue
    .line 258
    invoke-super {p0}, Lcom/twitter/app/dm/DMComposeFragment;->b()V

    .line 259
    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/model/core/TwitterUser;

    move-result-object v0

    .line 260
    if-eqz v0, :cond_0

    iget v0, v0, Lcom/twitter/model/core/TwitterUser;->R:I

    if-nez v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->r:Lcom/twitter/model/core/r;

    invoke-static {v0, p0}, Lcom/twitter/app/dm/dialog/ShareTweetEmptyOverlay;->a(Lcom/twitter/model/core/r;Landroid/support/v4/app/Fragment;)V

    .line 263
    :cond_0
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;Lcbi;)V
    .locals 0

    .prologue
    .line 68
    check-cast p1, Lcom/twitter/android/provider/d$b;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->a(Lcom/twitter/android/provider/d$b;Lcbi;)V

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 316
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->g:Lcom/twitter/app/dm/ac;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ac;->g()Lcom/twitter/model/dms/q;

    move-result-object v0

    .line 317
    if-eqz v0, :cond_0

    .line 318
    iget-object v0, v0, Lcom/twitter/model/dms/q;->b:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    :goto_0
    return-void

    .line 319
    :cond_0
    invoke-static {}, Lcom/twitter/library/dm/d;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 322
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->g:Lcom/twitter/app/dm/ac;

    .line 323
    invoke-virtual {v1}, Lcom/twitter/app/dm/ac;->f()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Iterable;)Lcom/twitter/util/collection/o;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->x:J

    .line 324
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    move-result-object v0

    .line 325
    invoke-virtual {v0}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 326
    new-instance v1, Lcom/twitter/app/dm/q$a;

    invoke-direct {v1}, Lcom/twitter/app/dm/q$a;-><init>()V

    iget-object v2, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->w:Lbta;

    .line 327
    invoke-virtual {v1, v2}, Lcom/twitter/app/dm/q$a;->a(Lbta;)Lcom/twitter/app/dm/q$a;

    move-result-object v1

    .line 328
    invoke-virtual {v1, v0}, Lcom/twitter/app/dm/q$a;->a(Ljava/util/Set;)Lcom/twitter/app/dm/q$a;

    move-result-object v0

    .line 329
    invoke-virtual {v0, p0}, Lcom/twitter/app/dm/q$a;->a(Lcom/twitter/app/dm/q$b;)Lcom/twitter/app/dm/q$a;

    move-result-object v0

    .line 330
    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/q$a;->a(Ljava/lang/String;)Lcom/twitter/app/dm/q$a;

    move-result-object v0

    .line 331
    invoke-virtual {v0}, Lcom/twitter/app/dm/q$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/q;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 332
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/q;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 335
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public d()V
    .locals 3

    .prologue
    .line 273
    invoke-super {p0}, Lcom/twitter/app/dm/DMComposeFragment;->d()V

    .line 277
    iget-boolean v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->p:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->Y()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 278
    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/twitter/util/ui/k;->b(Landroid/content/Context;Landroid/view/View;Z)V

    .line 280
    :cond_0
    return-void
.end method

.method protected e()Lna;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lna",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    new-instance v0, Lcom/twitter/android/provider/d;

    invoke-direct {v0}, Lcom/twitter/android/provider/d;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->m:Lcom/twitter/android/provider/d;

    .line 118
    new-instance v1, Lcom/twitter/android/provider/d$b;

    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->r:Lcom/twitter/model/core/r;

    iget-wide v2, v0, Lcom/twitter/model/core/r;->e:J

    iget-wide v4, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->x:J

    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->r:Lcom/twitter/model/core/r;

    iget-wide v6, v0, Lcom/twitter/model/core/r;->b:J

    const/4 v8, 0x6

    invoke-direct/range {v1 .. v8}, Lcom/twitter/android/provider/d$b;-><init>(JJJI)V

    iput-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->n:Lcom/twitter/android/provider/d$b;

    .line 121
    new-instance v0, Lcom/twitter/android/provider/a;

    invoke-super {p0}, Lcom/twitter/app/dm/DMComposeFragment;->e()Lna;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->m:Lcom/twitter/android/provider/d;

    iget-object v3, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->n:Lcom/twitter/android/provider/d$b;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/twitter/android/provider/a;-><init>(Lna;Lcom/twitter/android/provider/d;Lcom/twitter/android/provider/d$b;Lcom/twitter/android/provider/a$a;)V

    return-object v0
.end method

.method protected h()Landroid/text/TextWatcher;
    .locals 2

    .prologue
    .line 461
    invoke-super {p0}, Lcom/twitter/app/dm/DMComposeFragment;->h()Landroid/text/TextWatcher;

    move-result-object v0

    .line 462
    new-instance v1, Lcom/twitter/app/dm/ShareViaDMComposeFragment$4;

    invoke-direct {v1, p0, v0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment$4;-><init>(Lcom/twitter/app/dm/ShareViaDMComposeFragment;Landroid/text/TextWatcher;)V

    return-object v1
.end method

.method protected i()Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->s:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected k()Z
    .locals 1

    .prologue
    .line 489
    const/4 v0, 0x0

    return v0
.end method

.method protected m()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 158
    const v0, 0x7f0403b1

    return v0
.end method

.method protected n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 268
    const-string/jumbo v0, "share_tweet_user_select"

    return-object v0
.end method

.method protected o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 495
    invoke-direct {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496
    const-string/jumbo v0, "suggested"

    .line 498
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/twitter/app/dm/DMComposeFragment;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 163
    invoke-super {p0, p1}, Lcom/twitter/app/dm/DMComposeFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 169
    if-nez p1, :cond_3

    .line 172
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->v:Lcom/twitter/app/dm/r;

    invoke-virtual {v0}, Lcom/twitter/app/dm/r;->g()Ljava/util/List;

    move-result-object v3

    .line 173
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->v:Lcom/twitter/app/dm/r;

    invoke-virtual {v0}, Lcom/twitter/app/dm/r;->c()Ljava/lang/String;

    move-result-object v2

    .line 174
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->v:Lcom/twitter/app/dm/r;

    invoke-virtual {v0}, Lcom/twitter/app/dm/r;->k()Ljava/lang/String;

    move-result-object v0

    move-object v4, v3

    move-object v5, v1

    move-object v3, v2

    move-object v2, v0

    .line 184
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 185
    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->j:Lcom/twitter/app/dm/y;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->j:Lcom/twitter/app/dm/y;

    .line 188
    :goto_1
    new-instance v6, Lcom/twitter/app/dm/aa$a;

    invoke-direct {v6}, Lcom/twitter/app/dm/aa$a;-><init>()V

    new-instance v7, Lcom/twitter/app/dm/ad;

    invoke-direct {v7, v0}, Lcom/twitter/app/dm/ad;-><init>(Landroid/view/View;)V

    .line 189
    invoke-virtual {v6, v7}, Lcom/twitter/app/dm/aa$a;->a(Lcom/twitter/app/dm/ae;)Lcom/twitter/app/dm/aa$a;

    move-result-object v6

    new-instance v7, Lcom/twitter/app/dm/a;

    invoke-direct {v7, v0}, Lcom/twitter/app/dm/a;-><init>(Landroid/view/View;)V

    .line 190
    invoke-virtual {v6, v7}, Lcom/twitter/app/dm/aa$a;->d(Lcom/twitter/app/dm/ae;)Lcom/twitter/app/dm/aa$a;

    move-result-object v6

    new-instance v7, Lcom/twitter/app/dm/w;

    invoke-direct {v7, v0}, Lcom/twitter/app/dm/w;-><init>(Landroid/view/View;)V

    .line 191
    invoke-virtual {v6, v7}, Lcom/twitter/app/dm/aa$a;->b(Lcom/twitter/app/dm/ae;)Lcom/twitter/app/dm/aa$a;

    move-result-object v6

    .line 192
    invoke-virtual {v6, v1}, Lcom/twitter/app/dm/aa$a;->c(Lcom/twitter/app/dm/ae;)Lcom/twitter/app/dm/aa$a;

    move-result-object v1

    .line 193
    invoke-virtual {v1}, Lcom/twitter/app/dm/aa$a;->q()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/app/dm/aa;

    iput-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->q:Lcom/twitter/app/dm/aa;

    .line 195
    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/util/d;->f(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 196
    invoke-static {v0}, Lcom/twitter/app/dm/z;->a(Landroid/view/View;)V

    .line 199
    :cond_0
    new-instance v0, Lcom/twitter/app/dm/ac;

    invoke-direct {v0}, Lcom/twitter/app/dm/ac;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->g:Lcom/twitter/app/dm/ac;

    .line 200
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->r:Lcom/twitter/model/core/r;

    invoke-direct {p0, p0, v0, v3, v2}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->a(Lcom/twitter/app/dm/widget/DMMessageComposer$a;Lcom/twitter/model/core/r;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    invoke-direct {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->x()V

    .line 202
    invoke-direct {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->A()V

    .line 204
    if-eqz v5, :cond_1

    .line 205
    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->g:Lcom/twitter/app/dm/ac;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/twitter/library/provider/c;

    invoke-interface {v5, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/provider/c;

    invoke-virtual {v1, v0}, Lcom/twitter/app/dm/ac;->a([Lcom/twitter/library/provider/c;)V

    .line 208
    :cond_1
    invoke-direct {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->F()Z

    move-result v0

    if-nez v0, :cond_2

    .line 209
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/dm/ShareViaDMComposeFragment$1;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment$1;-><init>(Lcom/twitter/app/dm/ShareViaDMComposeFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 220
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->i:Lcom/twitter/library/view/QuoteView;

    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->r:Lcom/twitter/model/core/r;

    invoke-virtual {v0, v1}, Lcom/twitter/library/view/QuoteView;->setQuoteData(Lcom/twitter/model/core/r;)V

    .line 222
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    new-instance v1, Lcom/twitter/app/dm/ShareViaDMComposeFragment$2;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment$2;-><init>(Lcom/twitter/app/dm/ShareViaDMComposeFragment;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 239
    invoke-static {v4}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 240
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/model/core/TwitterUser;

    .line 241
    const-string/jumbo v1, ""

    invoke-virtual {v4}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v2

    const/4 v5, -0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->a(Ljava/lang/String;JLjava/lang/Object;I)Z

    goto :goto_2

    .line 177
    :cond_3
    const-string/jumbo v0, "suggestions"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    sget-object v2, Lcom/twitter/library/provider/c;->c:Lcom/twitter/util/serialization/l;

    .line 178
    invoke-static {v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 177
    invoke-static {v0, v2}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v0

    .line 181
    goto/16 :goto_0

    .line 185
    :cond_4
    sget-object v1, Lcom/twitter/app/dm/ae;->a:Lcom/twitter/app/dm/ae;

    goto/16 :goto_1

    .line 243
    :cond_5
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->a:Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/autocomplete/ListViewSuggestionEditText;->setEnabled(Z)V

    .line 246
    :cond_6
    invoke-direct {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->D()V

    .line 247
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 100
    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/dm/e;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->l:Z

    .line 102
    new-instance v0, Lcom/twitter/app/dm/r;

    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/twitter/app/dm/r;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->v:Lcom/twitter/app/dm/r;

    .line 103
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->v:Lcom/twitter/app/dm/r;

    invoke-virtual {v0}, Lcom/twitter/app/dm/r;->f()Lcom/twitter/model/core/r;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->r:Lcom/twitter/model/core/r;

    .line 104
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->v:Lcom/twitter/app/dm/r;

    invoke-virtual {v0}, Lcom/twitter/app/dm/r;->j()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->s:Z

    .line 105
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->r:Lcom/twitter/model/core/r;

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->e:Z

    .line 106
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->v:Lcom/twitter/app/dm/r;

    invoke-virtual {v0}, Lcom/twitter/app/dm/r;->x()I

    move-result v0

    iput v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->t:I

    .line 107
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->v:Lcom/twitter/app/dm/r;

    invoke-virtual {v0}, Lcom/twitter/app/dm/r;->y()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->u:Ljava/lang/String;

    .line 108
    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->x:J

    .line 109
    new-instance v0, Lbta;

    iget-wide v2, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->x:J

    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v1

    invoke-direct {v0, v1}, Lbta;-><init>(Lcom/twitter/database/model/i;)V

    iput-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->w:Lbta;

    .line 111
    invoke-super {p0, p1}, Lcom/twitter/app/dm/DMComposeFragment;->onCreate(Landroid/os/Bundle;)V

    .line 112
    return-void

    :cond_0
    move v0, v2

    .line 100
    goto :goto_0

    :cond_1
    move v1, v2

    .line 105
    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 303
    invoke-super {p0, p1}, Lcom/twitter/app/dm/DMComposeFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 304
    const-string/jumbo v0, "suggestions"

    iget-object v1, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->g:Lcom/twitter/app/dm/ac;

    invoke-virtual {v1}, Lcom/twitter/app/dm/ac;->c()Ljava/util/List;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/c;->c:Lcom/twitter/util/serialization/l;

    .line 305
    invoke-static {v2}, Lcom/twitter/util/collection/d;->a(Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/l;

    move-result-object v2

    .line 304
    invoke-static {v1, v2}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 306
    return-void
.end method

.method public w()V
    .locals 5

    .prologue
    .line 505
    iget-object v0, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->g:Lcom/twitter/app/dm/ac;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ac;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "cancel_with_selection"

    .line 507
    :goto_0
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "messages"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 508
    invoke-virtual {p0}, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->n()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/twitter/app/dm/ShareViaDMComposeFragment;->u:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const/4 v4, 0x0

    aput-object v4, v2, v3

    const/4 v3, 0x4

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 507
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 509
    return-void

    .line 505
    :cond_0
    const-string/jumbo v0, "cancel_without_selection"

    goto :goto_0
.end method
