.class Lcom/twitter/app/dm/DMConversationFragment$21;
.super Lcom/twitter/library/network/livepipeline/g;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/dm/DMConversationFragment;->R()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/network/livepipeline/g",
        "<",
        "Lcom/twitter/model/livepipeline/b;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/dm/DMConversationFragment;


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/DMConversationFragment;)V
    .locals 0

    .prologue
    .line 694
    iput-object p1, p0, Lcom/twitter/app/dm/DMConversationFragment$21;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-direct {p0}, Lcom/twitter/library/network/livepipeline/g;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/livepipeline/b;)V
    .locals 2

    .prologue
    .line 697
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$21;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-virtual {v0}, Lcom/twitter/app/dm/DMConversationFragment;->ac()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 698
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$21;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v0}, Lcom/twitter/app/dm/DMConversationFragment;->d(Lcom/twitter/app/dm/DMConversationFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 699
    const-string/jumbo v0, "LivePipeline"

    const-string/jumbo v1, "Fetching new messages..."

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$21;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v0}, Lcom/twitter/app/dm/DMConversationFragment;->f(Lcom/twitter/app/dm/DMConversationFragment;)Lcom/twitter/library/client/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/o;->a()V

    .line 702
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$21;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v0}, Lcom/twitter/app/dm/DMConversationFragment;->g(Lcom/twitter/app/dm/DMConversationFragment;)V

    .line 704
    :cond_1
    return-void
.end method

.method public bridge synthetic a(Lcom/twitter/model/livepipeline/d;)V
    .locals 0

    .prologue
    .line 694
    check-cast p1, Lcom/twitter/model/livepipeline/b;

    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/DMConversationFragment$21;->a(Lcom/twitter/model/livepipeline/b;)V

    return-void
.end method
