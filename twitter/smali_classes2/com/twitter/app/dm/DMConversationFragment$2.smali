.class Lcom/twitter/app/dm/DMConversationFragment$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/media/imageeditor/stickers/c$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/dm/DMConversationFragment;->a(Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/dm/DMConversationFragment;


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/DMConversationFragment;)V
    .locals 0

    .prologue
    .line 1012
    iput-object p1, p0, Lcom/twitter/app/dm/DMConversationFragment$2;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcdu;ILandroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    .line 1017
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$2;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v0}, Lcom/twitter/app/dm/DMConversationFragment;->k(Lcom/twitter/app/dm/DMConversationFragment;)Lcom/twitter/app/dm/widget/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/widget/c;->a(Lcdu;)V

    .line 1018
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment$2;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v1}, Lcom/twitter/app/dm/DMConversationFragment;->b(Lcom/twitter/app/dm/DMConversationFragment;)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:thread:::send_standalone_sticker_dm"

    aput-object v3, v1, v2

    .line 1019
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1018
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1020
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$2;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v0, p1}, Lcom/twitter/app/dm/DMConversationFragment;->a(Lcom/twitter/app/dm/DMConversationFragment;Lcdu;)V

    .line 1021
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$2;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v0}, Lcom/twitter/app/dm/DMConversationFragment;->k(Lcom/twitter/app/dm/DMConversationFragment;)Lcom/twitter/app/dm/widget/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/c;->f()V

    .line 1022
    return-void
.end method
