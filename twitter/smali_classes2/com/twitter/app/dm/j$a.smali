.class public Lcom/twitter/app/dm/j$a;
.super Lcom/twitter/app/dm/b$a;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/dm/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/dm/b$a",
        "<",
        "Lcom/twitter/app/dm/j$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 97
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/twitter/app/dm/b$a;-><init>(Landroid/os/Bundle;I)V

    .line 98
    return-void
.end method

.method private f()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 171
    iget-object v2, p0, Lcom/twitter/app/dm/j$a;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "participant_ids"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v2

    .line 172
    if-eqz v2, :cond_2

    .line 173
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 175
    invoke-static {v2}, Lcom/twitter/util/collection/CollectionUtils;->a([J)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lcom/twitter/app/dm/j$a$1;

    invoke-direct {v3, p0, v4, v5}, Lcom/twitter/app/dm/j$a$1;-><init>(Lcom/twitter/app/dm/j$a;J)V

    invoke-static {v2, v3}, Lcpt;->a(Ljava/lang/Iterable;Lcpv;)Ljava/lang/Iterable;

    move-result-object v2

    .line 174
    invoke-static {v2}, Lcpt;->b(Ljava/lang/Iterable;)I

    move-result v2

    .line 181
    if-le v2, v0, :cond_1

    .line 186
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/twitter/app/dm/j$a;->a:Landroid/os/Bundle;

    const-string/jumbo v2, "is_group"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 187
    return-void

    :cond_1
    move v0, v1

    .line 181
    goto :goto_0

    .line 183
    :cond_2
    iget-object v2, p0, Lcom/twitter/app/dm/j$a;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "conversation_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 184
    if-eqz v2, :cond_3

    const-string/jumbo v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public synthetic a()Lcom/twitter/app/dm/b;
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/twitter/app/dm/j$a;->e()Lcom/twitter/app/dm/j;

    move-result-object v0

    return-object v0
.end method

.method public a(J)Lcom/twitter/app/dm/j$a;
    .locals 3

    .prologue
    .line 105
    const/4 v0, 0x1

    new-array v0, v0, [J

    const/4 v1, 0x0

    aput-wide p1, v0, v1

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/j$a;->a([J)Lcom/twitter/app/dm/j$a;

    .line 106
    return-object p0
.end method

.method public a(Landroid/net/Uri;)Lcom/twitter/app/dm/j$a;
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/twitter/app/dm/j$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "media_uri"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 130
    return-object p0
.end method

.method public a([J)Lcom/twitter/app/dm/j$a;
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/twitter/app/dm/j$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "participant_ids"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 112
    return-object p0
.end method

.method public synthetic b()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/twitter/app/dm/j$a;->e()Lcom/twitter/app/dm/j;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/app/dm/j$a;
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/twitter/app/dm/j$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "recipient_screen_name"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    return-object p0
.end method

.method public synthetic c()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/twitter/app/dm/j$a;->e()Lcom/twitter/app/dm/j;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/app/dm/j$a;
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/twitter/app/dm/j$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "conversation_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    return-object p0
.end method

.method public c(Z)Lcom/twitter/app/dm/j$a;
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/twitter/app/dm/j$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "is_from_notification"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 154
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/app/dm/j$a;
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/twitter/app/dm/j$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/app/dm/j$a;
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/twitter/app/dm/j$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "subtitle"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    return-object p0
.end method

.method public e()Lcom/twitter/app/dm/j;
    .locals 2

    .prologue
    .line 192
    invoke-direct {p0}, Lcom/twitter/app/dm/j$a;->f()V

    .line 193
    new-instance v0, Lcom/twitter/app/dm/j;

    iget-object v1, p0, Lcom/twitter/app/dm/j$a;->a:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Lcom/twitter/app/dm/j;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method

.method public f(Ljava/lang/String;)Lcom/twitter/app/dm/j$a;
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lcom/twitter/app/dm/j$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "welcome_message_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    return-object p0
.end method

.method public g(Z)Lcom/twitter/app/dm/j$a;
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lcom/twitter/app/dm/j$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "is_from_direct_share"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 160
    return-object p0
.end method

.method public h(Z)Lcom/twitter/app/dm/j$a;
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lcom/twitter/app/dm/j$a;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "should_show_verified_badge"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 166
    return-object p0
.end method
