.class public Lcom/twitter/app/dm/DMNotifMuteIntentService;
.super Landroid/app/IntentService;
.source "Twttr"


# instance fields
.field private final a:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    const-string/jumbo v0, "MuteIntent"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 29
    invoke-static {}, Lcom/twitter/library/dm/d;->d()I

    move-result v0

    iput v0, p0, Lcom/twitter/app/dm/DMNotifMuteIntentService;->a:I

    .line 33
    return-void
.end method

.method private a()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/twitter/app/dm/DMNotifMuteIntentService;->a:I

    packed-switch v0, :pswitch_data_0

    .line 75
    :pswitch_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 65
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 68
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 71
    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    .line 63
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 83
    iget v0, p0, Lcom/twitter/app/dm/DMNotifMuteIntentService;->a:I

    packed-switch v0, :pswitch_data_0

    .line 94
    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMNotifMuteIntentService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a05a7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 85
    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMNotifMuteIntentService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a05b1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 88
    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMNotifMuteIntentService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a05a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 91
    :pswitch_3
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMNotifMuteIntentService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a05ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 83
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method a(JLjava/lang/String;)V
    .locals 7
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 50
    new-instance v0, Lcom/twitter/library/api/dm/u;

    .line 51
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {p0}, Lcom/twitter/app/dm/DMNotifMuteIntentService;->a()I

    move-result v5

    move-object v1, p0

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/dm/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;ZI)V

    .line 52
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 53
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v0, p1, p2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:notifications:::mute_dm"

    aput-object v3, v1, v2

    .line 54
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 53
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 55
    new-instance v0, Lcom/twitter/util/android/g;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMNotifMuteIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/util/android/g;-><init>(Landroid/content/Context;)V

    .line 56
    invoke-direct {p0}, Lcom/twitter/app/dm/DMNotifMuteIntentService;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/twitter/util/android/g;->a(Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 58
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 37
    if-eqz p1, :cond_1

    const-string/jumbo v0, "extra_conversation_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 38
    :goto_0
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 39
    const-string/jumbo v1, "extra_owner_id"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 40
    invoke-virtual {p0, v2, v3, v0}, Lcom/twitter/app/dm/DMNotifMuteIntentService;->a(JLjava/lang/String;)V

    .line 41
    const-string/jumbo v0, "extra_notification_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v0

    .line 42
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMNotifMuteIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;)Lcom/twitter/android/client/l;

    move-result-object v1

    invoke-virtual {v1, v0, v2, v3}, Lcom/twitter/android/client/l;->a([IJ)V

    .line 44
    :cond_0
    return-void

    .line 37
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
