.class Lcom/twitter/app/dm/DMConversationFragment$25;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/app/dm/widget/b$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/app/dm/DMConversationFragment;->a(Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/app/dm/DMConversationFragment;


# direct methods
.method constructor <init>(Lcom/twitter/app/dm/DMConversationFragment;)V
    .locals 0

    .prologue
    .line 997
    iput-object p1, p0, Lcom/twitter/app/dm/DMConversationFragment$25;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 1001
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment$25;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v1}, Lcom/twitter/app/dm/DMConversationFragment;->b(Lcom/twitter/app/dm/DMConversationFragment;)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:thread:standalone_sticker_catalog::impression"

    aput-object v3, v1, v2

    .line 1002
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1001
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1003
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 1007
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$25;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v0}, Lcom/twitter/app/dm/DMConversationFragment;->i(Lcom/twitter/app/dm/DMConversationFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1008
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment$25;->a:Lcom/twitter/app/dm/DMConversationFragment;

    invoke-static {v0}, Lcom/twitter/app/dm/DMConversationFragment;->j(Lcom/twitter/app/dm/DMConversationFragment;)V

    .line 1010
    :cond_0
    return-void
.end method
