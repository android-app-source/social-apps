.class public Lcom/twitter/app/dm/DMConversationFragment;
.super Lcom/twitter/app/common/list/TwitterListFragment;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lapl;
.implements Lbrp;
.implements Lcom/twitter/android/bn;
.implements Lcom/twitter/android/client/m;
.implements Lcom/twitter/android/media/selection/a;
.implements Lcom/twitter/app/common/dialog/b$a;
.implements Lcom/twitter/app/common/dialog/b$d;
.implements Lcom/twitter/app/dm/ah;
.implements Lcom/twitter/app/dm/dialog/ReportConversationDialog$a;
.implements Lcom/twitter/app/dm/dialog/a;
.implements Lcom/twitter/app/dm/f;
.implements Lcom/twitter/app/dm/n$a;
.implements Lcom/twitter/app/dm/widget/DMConversationMessageComposer$d;
.implements Lcom/twitter/app/users/sheet/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/DMConversationFragment$a;,
        Lcom/twitter/app/dm/DMConversationFragment$b;,
        Lcom/twitter/app/dm/DMConversationFragment$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/app/common/list/TwitterListFragment",
        "<",
        "Lcom/twitter/model/dms/m;",
        "Lcom/twitter/app/dm/i;",
        ">;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lapl;",
        "Lbrp;",
        "Lcom/twitter/android/bn;",
        "Lcom/twitter/android/client/m;",
        "Lcom/twitter/android/media/selection/a;",
        "Lcom/twitter/app/common/dialog/b$a;",
        "Lcom/twitter/app/common/dialog/b$d;",
        "Lcom/twitter/app/dm/ah;",
        "Lcom/twitter/app/dm/dialog/ReportConversationDialog$a;",
        "Lcom/twitter/app/dm/dialog/a;",
        "Lcom/twitter/app/dm/f;",
        "Lcom/twitter/app/dm/n$a;",
        "Lcom/twitter/app/dm/widget/DMConversationMessageComposer$d;",
        "Lcom/twitter/app/users/sheet/b;"
    }
.end annotation


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Z

.field private H:Z

.field private I:Landroid/view/View;

.field private J:Z

.field private K:Lcom/twitter/app/dm/m;

.field private L:Z

.field private M:Z

.field private N:Lcom/twitter/library/client/o;

.field private O:Z

.field private P:Z

.field private Q:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lcom/twitter/android/widget/PromptDialogFragment;

.field private aA:Z

.field private aB:Z

.field private aC:Lcom/twitter/app/dm/widget/c;

.field private aD:Landroid/view/View;

.field private aE:Landroid/widget/TextView;

.field private aF:Landroid/widget/TextView;

.field private aG:Landroid/view/View;

.field private aH:Lcom/twitter/app/users/sheet/UsersBottomSheet;

.field private aa:Lcom/twitter/android/widget/NewItemBannerView;

.field private ab:Z

.field private ac:Z

.field private ad:Z

.field private ae:Z

.field private af:Z

.field private ag:Z

.field private ah:Z

.field private ai:Z

.field private aj:Z

.field private ak:Lape;

.field private al:Lcom/twitter/library/card/m;

.field private am:Z

.field private an:Z

.field private ao:Lcom/twitter/app/dm/ag;

.field private ap:Lcom/twitter/app/dm/ag$a;

.field private aq:Lcom/twitter/library/network/livepipeline/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/library/network/livepipeline/g",
            "<",
            "Lcom/twitter/model/livepipeline/g;",
            ">;"
        }
    .end annotation
.end field

.field private ar:Lcom/twitter/library/network/livepipeline/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/library/network/livepipeline/g",
            "<",
            "Lcom/twitter/model/livepipeline/b;",
            ">;"
        }
    .end annotation
.end field

.field private as:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private at:Lcom/twitter/app/dm/DMConversationFragment$b;

.field private au:Z

.field private av:Landroid/os/Handler;

.field private aw:Lcom/twitter/app/dm/x;

.field private ax:Z

.field private ay:Z

.field private az:Z

.field private b:J

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Lcom/twitter/library/client/Session;

.field private f:J

.field private g:Lcom/twitter/android/media/selection/c;

.field private h:Lcom/twitter/app/dm/DMConversationFragment$c;

.field private i:Z

.field private j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

.field private k:Landroid/app/ProgressDialog;

.field private l:Lcom/twitter/app/dm/n;

.field private m:Lcom/twitter/android/media/selection/c;

.field private n:Lapj;

.field private o:Z

.field private p:[J

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Lcom/twitter/app/dm/t;

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 235
    invoke-direct {p0}, Lcom/twitter/app/common/list/TwitterListFragment;-><init>()V

    .line 348
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->b:J

    return-void
.end method

.method static synthetic A(Lcom/twitter/app/dm/DMConversationFragment;)Z
    .locals 1

    .prologue
    .line 235
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->x:Z

    return v0
.end method

.method static synthetic B(Lcom/twitter/app/dm/DMConversationFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic C(Lcom/twitter/app/dm/DMConversationFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic D(Lcom/twitter/app/dm/DMConversationFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    return-object v0
.end method

.method private N()V
    .locals 3

    .prologue
    .line 582
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    const-string/jumbo v1, "Coming soon! If you are not in debug build file a bug with aawasthi@twitter.com"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 583
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 584
    return-void
.end method

.method private O()V
    .locals 1

    .prologue
    .line 591
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aC:Lcom/twitter/app/dm/widget/c;

    if-eqz v0, :cond_0

    .line 592
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->u()V

    .line 593
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aC:Lcom/twitter/app/dm/widget/c;

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/c;->d()V

    .line 595
    :cond_0
    return-void
.end method

.method private P()V
    .locals 1

    .prologue
    .line 604
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aC:Lcom/twitter/app/dm/widget/c;

    if-eqz v0, :cond_0

    .line 605
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aC:Lcom/twitter/app/dm/widget/c;

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/c;->f()V

    .line 607
    :cond_0
    return-void
.end method

.method private Q()V
    .locals 1

    .prologue
    .line 617
    invoke-static {}, Lcom/twitter/library/dm/d;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/dm/e;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 618
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->ak()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 619
    new-instance v0, Lcom/twitter/app/dm/DMConversationFragment$b;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/DMConversationFragment$b;-><init>(Lcom/twitter/app/dm/DMConversationFragment;)V

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->at:Lcom/twitter/app/dm/DMConversationFragment$b;

    .line 620
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->R()V

    .line 621
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->S()V

    .line 622
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->an:Z

    .line 624
    :cond_0
    return-void
.end method

.method private R()V
    .locals 5

    .prologue
    .line 629
    new-instance v0, Lcom/twitter/app/dm/ag;

    new-instance v1, Laut;

    .line 630
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, v2}, Laut;-><init>(Landroid/content/ContentResolver;)V

    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/app/dm/DMConversationFragment;->at:Lcom/twitter/app/dm/DMConversationFragment$b;

    iget-object v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->ap:Lcom/twitter/app/dm/ag$a;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/app/dm/ag;-><init>(Laut;Ljava/lang/String;Lcom/twitter/app/dm/af;Lcom/twitter/app/dm/ag$a;)V

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ao:Lcom/twitter/app/dm/ag;

    .line 636
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->p:[J

    if-eqz v0, :cond_0

    .line 638
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->ao:Lcom/twitter/app/dm/ag;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-static {v0}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/dm/DMConversationFragment$12;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/DMConversationFragment$12;-><init>(Lcom/twitter/app/dm/DMConversationFragment;)V

    .line 639
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 660
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ap:Lcom/twitter/app/dm/ag$a;

    if-eqz v0, :cond_2

    .line 661
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ao:Lcom/twitter/app/dm/ag;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ag;->c()Ljava/util/List;

    move-result-object v0

    .line 662
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/j;

    .line 663
    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->a(Lrx/j;)V

    goto :goto_0

    .line 665
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ap:Lcom/twitter/app/dm/ag$a;

    .line 668
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/i;

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->ao:Lcom/twitter/app/dm/ag;

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/i;->a(Lcom/twitter/app/dm/ag;)V

    .line 671
    new-instance v0, Lcom/twitter/app/dm/DMConversationFragment$19;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/DMConversationFragment$19;-><init>(Lcom/twitter/app/dm/DMConversationFragment;)V

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aq:Lcom/twitter/library/network/livepipeline/g;

    .line 683
    invoke-static {}, Lcom/twitter/library/network/livepipeline/b;->a()Lcom/twitter/library/network/livepipeline/b;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    .line 684
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/livepipeline/b;->a(Ljava/lang/String;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/dm/DMConversationFragment$20;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/DMConversationFragment$20;-><init>(Lcom/twitter/app/dm/DMConversationFragment;)V

    .line 685
    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 690
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->aq:Lcom/twitter/library/network/livepipeline/g;

    .line 691
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    .line 683
    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->a(Lrx/j;)V

    .line 694
    new-instance v0, Lcom/twitter/app/dm/DMConversationFragment$21;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/DMConversationFragment$21;-><init>(Lcom/twitter/app/dm/DMConversationFragment;)V

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ar:Lcom/twitter/library/network/livepipeline/g;

    .line 707
    invoke-static {}, Lcom/twitter/library/network/livepipeline/b;->a()Lcom/twitter/library/network/livepipeline/b;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    .line 708
    invoke-virtual {v0, v1}, Lcom/twitter/library/network/livepipeline/b;->b(Ljava/lang/String;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/dm/DMConversationFragment$22;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/DMConversationFragment$22;-><init>(Lcom/twitter/app/dm/DMConversationFragment;)V

    .line 709
    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 714
    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->ar:Lcom/twitter/library/network/livepipeline/g;

    .line 715
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    .line 707
    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->a(Lrx/j;)V

    .line 716
    return-void
.end method

.method private S()V
    .locals 4

    .prologue
    .line 719
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->as:Lrx/subjects/PublishSubject;

    .line 720
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->as:Lrx/subjects/PublishSubject;

    const-wide/16 v2, 0x7d0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 721
    invoke-virtual {v0, v2, v3, v1}, Lrx/subjects/PublishSubject;->d(JLjava/util/concurrent/TimeUnit;)Lrx/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/dm/DMConversationFragment$23;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/DMConversationFragment$23;-><init>(Lcom/twitter/app/dm/DMConversationFragment;)V

    .line 722
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    .line 720
    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->a(Lrx/j;)V

    .line 729
    return-void
.end method

.method private T()V
    .locals 1

    .prologue
    .line 732
    invoke-static {}, Lcom/twitter/library/dm/d;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 733
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->U()V

    .line 734
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aO()V

    .line 735
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->br()V

    .line 736
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->an:Z

    .line 738
    :cond_0
    return-void
.end method

.method private U()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 741
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ao:Lcom/twitter/app/dm/ag;

    if-eqz v0, :cond_0

    .line 742
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ao:Lcom/twitter/app/dm/ag;

    invoke-virtual {v0}, Lcom/twitter/app/dm/ag;->b()V

    .line 743
    iput-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->ao:Lcom/twitter/app/dm/ag;

    .line 746
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 747
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/i;

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/i;->a(Lcom/twitter/app/dm/ag;)V

    .line 750
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aq:Lcom/twitter/library/network/livepipeline/g;

    if-eqz v0, :cond_2

    .line 751
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aq:Lcom/twitter/library/network/livepipeline/g;

    invoke-virtual {v0}, Lcom/twitter/library/network/livepipeline/g;->B_()V

    .line 752
    iput-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->aq:Lcom/twitter/library/network/livepipeline/g;

    .line 755
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ar:Lcom/twitter/library/network/livepipeline/g;

    if-eqz v0, :cond_3

    .line 756
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ar:Lcom/twitter/library/network/livepipeline/g;

    invoke-virtual {v0}, Lcom/twitter/library/network/livepipeline/g;->B_()V

    .line 757
    iput-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->ar:Lcom/twitter/library/network/livepipeline/g;

    .line 759
    :cond_3
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/android/media/selection/MediaAttachment;Ljava/lang/String;Lccl;Lcdu;)Lcom/twitter/library/api/dm/q;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2185
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/twitter/android/media/selection/MediaAttachment;->d()Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v0

    move-object v1, v0

    .line 2187
    :goto_0
    new-instance v0, Lcom/twitter/library/api/dm/q$a;

    invoke-direct {v0}, Lcom/twitter/library/api/dm/q$a;-><init>()V

    .line 2188
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/library/api/dm/q$a;->a(Landroid/content/Context;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    .line 2189
    invoke-virtual {v0, v3}, Lcom/twitter/library/api/dm/q$a;->a(Lcom/twitter/library/client/Session;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    .line 2190
    invoke-virtual {v0, v3}, Lcom/twitter/library/api/dm/q$a;->b(Ljava/lang/String;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    .line 2191
    invoke-virtual {v0, p1}, Lcom/twitter/library/api/dm/q$a;->c(Ljava/lang/String;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    .line 2192
    invoke-virtual {v0, p2}, Lcom/twitter/library/api/dm/q$a;->d(Ljava/lang/String;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    .line 2193
    invoke-virtual {v0, v1}, Lcom/twitter/library/api/dm/q$a;->a(Lcom/twitter/model/drafts/DraftAttachment;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    .line 2194
    invoke-virtual {v0, p4}, Lcom/twitter/library/api/dm/q$a;->a(Ljava/lang/String;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    .line 2195
    invoke-virtual {v0, p5}, Lcom/twitter/library/api/dm/q$a;->a(Lccl;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    .line 2196
    invoke-virtual {v0, p6}, Lcom/twitter/library/api/dm/q$a;->a(Lcdu;)Lcom/twitter/library/api/dm/q$a;

    move-result-object v0

    .line 2197
    invoke-virtual {v0}, Lcom/twitter/library/api/dm/q$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/dm/q;

    .line 2199
    iget-object v3, p0, Lcom/twitter/app/dm/DMConversationFragment;->u:Lcom/twitter/app/dm/t;

    iget-object v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    .line 2200
    invoke-virtual {v4}, Lcom/twitter/app/dm/n;->x()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2199
    :goto_1
    invoke-virtual {v3, p1, v0, v1}, Lcom/twitter/app/dm/t;->a(Ljava/lang/String;Lcom/twitter/library/api/dm/q;Lcom/twitter/model/drafts/DraftAttachment;)V

    .line 2202
    return-object v0

    :cond_0
    move-object v1, v2

    .line 2185
    goto :goto_0

    :cond_1
    move-object v1, v2

    .line 2200
    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/app/dm/DMConversationFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    return-object v0
.end method

.method private a(Landroid/database/Cursor;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcaq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1774
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v1

    .line 1775
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ai:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1776
    new-instance v0, Lcbh;

    new-instance v2, Lcom/twitter/app/dm/DMConversationFragment$13;

    invoke-direct {v2, p0}, Lcom/twitter/app/dm/DMConversationFragment$13;-><init>(Lcom/twitter/app/dm/DMConversationFragment;)V

    invoke-direct {v0, p1, v2}, Lcbh;-><init>(Landroid/database/Cursor;Lcbp;)V

    .line 1792
    invoke-virtual {v0}, Lcbh;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/Pair;

    .line 1793
    invoke-virtual {v0}, Lcom/twitter/util/collection/Pair;->a()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0}, Lcom/twitter/util/collection/Pair;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    goto :goto_0

    .line 1796
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method private a(JI)V
    .locals 3

    .prologue
    .line 2795
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->i:Z

    if-nez v0, :cond_0

    const/4 v0, 0x2

    if-ne p3, v0, :cond_1

    .line 2796
    :cond_0
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/16 v1, 0x308

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a0272

    .line 2797
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a06be

    .line 2798
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0779

    .line 2799
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a00f6

    .line 2800
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 2801
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/dm/DMConversationFragment$17;

    invoke-direct {v1, p0, p1, p2}, Lcom/twitter/app/dm/DMConversationFragment$17;-><init>(Lcom/twitter/app/dm/DMConversationFragment;J)V

    .line 2802
    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 2810
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 2811
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->i:Z

    .line 2813
    :cond_1
    return-void
.end method

.method private a(JZ)V
    .locals 7

    .prologue
    .line 2407
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->v:Z

    .line 2408
    new-instance v1, Lcom/twitter/library/api/dm/s;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    move-wide v4, p1

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/api/dm/s;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JZ)V

    const/4 v0, 0x2

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 2410
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2974
    sget-object v0, Lcom/twitter/library/dm/DMGroupAvatarImageVariant;->d:Lcom/twitter/library/dm/DMGroupAvatarImageVariant;

    invoke-static {p1, v0}, Lcom/twitter/library/dm/DMGroupAvatarImageVariant;->a(Ljava/lang/String;Lcom/twitter/library/dm/DMGroupAvatarImageVariant;)Ljava/lang/String;

    move-result-object v0

    .line 2975
    new-instance v1, Lcom/twitter/app/common/base/h;

    invoke-direct {v1}, Lcom/twitter/app/common/base/h;-><init>()V

    const/4 v2, 0x1

    .line 2976
    invoke-virtual {v1, v2}, Lcom/twitter/app/common/base/h;->d(Z)Lcom/twitter/app/common/base/h;

    move-result-object v1

    const-class v2, Lcom/twitter/android/ImageActivity;

    .line 2977
    invoke-virtual {v1, p0, v2}, Lcom/twitter/app/common/base/h;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    .line 2978
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "image_url"

    .line 2979
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2980
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 2981
    return-void
.end method

.method private a(Landroid/os/Bundle;Lcom/twitter/model/account/UserSettings;)V
    .locals 3

    .prologue
    .line 1097
    new-instance v0, Lcom/twitter/app/dm/i$a;

    invoke-direct {v0}, Lcom/twitter/app/dm/i$a;-><init>()V

    .line 1098
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/i$a;->a(Landroid/content/Context;)Lcom/twitter/app/dm/i$a;

    move-result-object v1

    .line 1099
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v1, v0}, Lcom/twitter/app/dm/i$a;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/app/dm/i$a;

    move-result-object v0

    .line 1100
    invoke-virtual {v0, p0}, Lcom/twitter/app/dm/i$a;->a(Lcom/twitter/app/dm/f;)Lcom/twitter/app/dm/i$a;

    move-result-object v0

    .line 1101
    invoke-virtual {v0, p0}, Lcom/twitter/app/dm/i$a;->a(Lcom/twitter/android/bn;)Lcom/twitter/app/dm/i$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->K:Lcom/twitter/app/dm/m;

    .line 1102
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/i$a;->a(Lcom/twitter/app/dm/m;)Lcom/twitter/app/dm/i$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->aj:Z

    .line 1103
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/i$a;->b(Z)Lcom/twitter/app/dm/i$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->ai:Z

    .line 1104
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/i$a;->a(Z)Lcom/twitter/app/dm/i$a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->ax:Z

    .line 1105
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/i$a;->c(Z)Lcom/twitter/app/dm/i$a;

    move-result-object v0

    .line 1106
    invoke-static {}, Lcom/twitter/library/dm/d;->h()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/i$a;->d(Z)Lcom/twitter/app/dm/i$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->ak:Lape;

    .line 1107
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/i$a;->a(Lape;)Lcom/twitter/app/dm/i$a;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->al:Lcom/twitter/library/card/m;

    .line 1108
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/i$a;->a(Lcom/twitter/library/card/m;)Lcom/twitter/app/dm/i$a;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/account/UserSettings;->j:Z

    .line 1109
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/i$a;->e(Z)Lcom/twitter/app/dm/i$a;

    move-result-object v0

    iget-boolean v1, p2, Lcom/twitter/model/account/UserSettings;->k:Z

    .line 1110
    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/i$a;->f(Z)Lcom/twitter/app/dm/i$a;

    move-result-object v0

    .line 1111
    invoke-static {}, Lcom/twitter/library/dm/d;->o()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/i$a;->g(Z)Lcom/twitter/app/dm/i$a;

    move-result-object v0

    .line 1112
    invoke-virtual {v0, p0}, Lcom/twitter/app/dm/i$a;->a(Lapl;)Lcom/twitter/app/dm/i$a;

    move-result-object v0

    .line 1113
    invoke-virtual {v0}, Lcom/twitter/app/dm/i$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/i;

    .line 1114
    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->x:Z

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/i;->a(Z)V

    .line 1115
    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->y:Z

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/i;->b(Z)V

    .line 1116
    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/i;->a(Landroid/os/Bundle;)V

    .line 1118
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aQ()V

    .line 1120
    new-instance v1, Lcom/twitter/app/dm/widget/ReadOnlyConversationFooterView;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/app/dm/widget/ReadOnlyConversationFooterView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->I:Landroid/view/View;

    .line 1122
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/app/common/list/l;->a(Lcjr;)V

    .line 1123
    return-void
.end method

.method private a(Landroid/view/View;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1038
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    if-ge v0, p2, :cond_2

    move v0, v1

    .line 1039
    :goto_0
    if-eqz v0, :cond_3

    .line 1042
    iget-boolean v3, p0, Lcom/twitter/app/dm/DMConversationFragment;->G:Z

    if-nez v3, :cond_1

    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bj()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bg()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1047
    :cond_0
    invoke-direct {p0, v2}, Lcom/twitter/app/dm/DMConversationFragment;->g(Z)V

    .line 1049
    :cond_1
    iput-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->G:Z

    .line 1055
    :goto_1
    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual {v1, v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->c(Z)V

    .line 1056
    return-void

    :cond_2
    move v0, v2

    .line 1038
    goto :goto_0

    .line 1052
    :cond_3
    iput-boolean v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->G:Z

    goto :goto_1
.end method

.method private a(Lcbi;J)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/model/dms/m;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 2462
    .line 2463
    invoke-static {p1, p2, p3}, Lcom/twitter/library/dm/e;->a(Lcbi;J)Ljava/util/Iterator;

    move-result-object v0

    .line 2465
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2466
    :cond_0
    const/4 v0, 0x0

    .line 2471
    :goto_0
    if-lez v0, :cond_1

    add-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->e(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2472
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aa:Lcom/twitter/android/widget/NewItemBannerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/NewItemBannerView;->c()Z

    .line 2474
    :cond_1
    return-void

    .line 2468
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 2469
    iget-wide v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    invoke-static {v0, v2, v3}, Lcom/twitter/library/dm/e;->a(Ljava/util/Iterator;J)I

    move-result v0

    goto :goto_0
.end method

.method private a(Lcdu;)V
    .locals 2

    .prologue
    .line 2109
    const-string/jumbo v0, ""

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1}, Lcom/twitter/app/dm/DMConversationFragment;->a(Ljava/lang/String;Lccl;Lcdu;)V

    .line 2110
    return-void
.end method

.method private a(Lcno;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2293
    if-eqz p2, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ad:Z

    .line 2294
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->t:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ad:Z

    if-eqz v0, :cond_0

    .line 2295
    iput-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->t:Z

    .line 2297
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->W:Lcom/twitter/android/av/j;

    if-eqz v0, :cond_1

    .line 2298
    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->W:Lcom/twitter/android/av/j;

    invoke-direct {p0, v2}, Lcom/twitter/app/dm/DMConversationFragment;->e(I)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    :goto_1
    invoke-virtual {v1, v0}, Lcom/twitter/android/av/j;->a(I)V

    .line 2301
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->W:Lcom/twitter/android/av/j;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/av/j;->a(Lcno;I)V

    .line 2303
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 2293
    goto :goto_0

    .line 2298
    :cond_3
    const/4 v0, 0x3

    goto :goto_1
.end method

.method private a(Lcno;III)V
    .locals 2

    .prologue
    .line 2307
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ab:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->L:Z

    if-eqz v0, :cond_0

    if-eq p3, p4, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ad:Z

    if-eqz v0, :cond_1

    :cond_0
    const/16 v0, 0xa

    if-ge p2, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    .line 2309
    invoke-static {v0}, Lcom/twitter/library/dm/e;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->q:Z

    if-nez v0, :cond_1

    .line 2311
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aR()V

    .line 2314
    :cond_1
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bg()Z

    move-result v0

    if-eqz v0, :cond_2

    if-lez p2, :cond_2

    sub-int v0, p4, p2

    sub-int/2addr v0, p3

    if-nez v0, :cond_2

    .line 2318
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ac:Z

    .line 2321
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aa:Lcom/twitter/android/widget/NewItemBannerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/NewItemBannerView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bi()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2322
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aa:Lcom/twitter/android/widget/NewItemBannerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/NewItemBannerView;->b()Z

    .line 2325
    :cond_3
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->W:Lcom/twitter/android/av/j;

    if-eqz v0, :cond_4

    .line 2326
    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->W:Lcom/twitter/android/av/j;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->e(I)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x4

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/android/av/j;->a(I)V

    .line 2329
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->W:Lcom/twitter/android/av/j;

    invoke-virtual {v0, p1}, Lcom/twitter/android/av/j;->a(Lcno;)V

    .line 2331
    :cond_4
    return-void

    .line 2326
    :cond_5
    const/4 v0, 0x3

    goto :goto_0
.end method

.method private a(Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;II)V
    .locals 9

    .prologue
    .line 991
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1302eb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 992
    new-instance v1, Lcom/twitter/app/dm/widget/c;

    iget-wide v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    const-string/jumbo v0, "sticker_repo"

    .line 994
    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->b_(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/android/media/stickers/data/a;

    move-object v2, p1

    move v6, p2

    move v7, p3

    invoke-direct/range {v1 .. v8}, Lcom/twitter/app/dm/widget/c;-><init>(Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;Landroid/view/View;JIILcom/twitter/android/media/stickers/data/a;)V

    iput-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->aC:Lcom/twitter/app/dm/widget/c;

    .line 996
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aC:Lcom/twitter/app/dm/widget/c;

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/c;->c()V

    .line 997
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aC:Lcom/twitter/app/dm/widget/c;

    new-instance v1, Lcom/twitter/app/dm/DMConversationFragment$25;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/DMConversationFragment$25;-><init>(Lcom/twitter/app/dm/DMConversationFragment;)V

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/widget/c;->a(Lcom/twitter/app/dm/widget/b$a;)V

    .line 1012
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aC:Lcom/twitter/app/dm/widget/c;

    new-instance v1, Lcom/twitter/app/dm/DMConversationFragment$2;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/DMConversationFragment$2;-><init>(Lcom/twitter/app/dm/DMConversationFragment;)V

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/widget/c;->a(Lcom/twitter/android/media/imageeditor/stickers/c$b;)V

    .line 1025
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aC:Lcom/twitter/app/dm/widget/c;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a02f3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/app/dm/DMConversationFragment;->a(Lcom/twitter/app/dm/widget/b;Ljava/lang/String;)V

    .line 1026
    return-void
.end method

.method private a(Lcom/twitter/android/media/selection/MediaAttachment;Landroid/net/Uri;Ljava/lang/String;ZLjava/util/Collection;Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/media/selection/MediaAttachment;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Collection",
            "<",
            "Landroid/net/Uri;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1192
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->P:Z

    if-eqz v0, :cond_0

    .line 1193
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->q()V

    .line 1196
    :cond_0
    new-instance v0, Lcom/twitter/android/media/selection/c;

    .line 1197
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string/jumbo v3, "dm_composition"

    sget-object v4, Lcom/twitter/media/model/MediaType;->h:Ljava/util/EnumSet;

    const/4 v5, 0x1

    sget-object v6, Lcom/twitter/android/composer/ComposerType;->c:Lcom/twitter/android/composer/ComposerType;

    iget-object v7, p0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    move-object v2, p0

    move-object v8, p0

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/media/selection/c;-><init>(Landroid/content/Context;Lbrp;Ljava/lang/String;Ljava/util/EnumSet;ILcom/twitter/android/composer/ComposerType;Lcom/twitter/library/client/Session;Lcom/twitter/app/common/util/k;)V

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->m:Lcom/twitter/android/media/selection/c;

    .line 1206
    new-instance v0, Lcom/twitter/app/dm/n;

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    move-object v2, p0

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/twitter/app/dm/n;-><init>(Lcom/twitter/app/dm/n$b;Lcom/twitter/app/dm/n$a;Lcom/twitter/android/media/selection/a;Lcom/twitter/android/media/selection/MediaAttachment;Landroid/net/Uri;Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    .line 1209
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual {v0, p6}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b(Z)V

    .line 1210
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual {v0, p3, p4}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->a(Ljava/lang/String;Z)V

    .line 1212
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Landroid/content/Context;)Z

    move-result v1

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/twitter/app/dm/n;->a(ZI)V

    .line 1214
    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/DMConversationFragment;I)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/DMConversationFragment;->c(I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/DMConversationFragment;JZ)V
    .locals 1

    .prologue
    .line 235
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/app/dm/DMConversationFragment;->a(JZ)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/DMConversationFragment;Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0, p1, p2}, Lcom/twitter/app/dm/DMConversationFragment;->a(Landroid/view/View;I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/DMConversationFragment;Lcbi;)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcbi;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/DMConversationFragment;Lcdu;)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/DMConversationFragment;->a(Lcdu;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/DMConversationFragment;Lcno;I)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0, p1, p2}, Lcom/twitter/app/dm/DMConversationFragment;->a(Lcno;I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/DMConversationFragment;Lcno;III)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/app/dm/DMConversationFragment;->a(Lcno;III)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/DMConversationFragment;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/DMConversationFragment;->b(Ljava/util/List;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/DMConversationFragment;Z)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/DMConversationFragment;->b(Z)V

    return-void
.end method

.method private a(Lcom/twitter/app/dm/widget/b;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1029
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bh()Lcom/twitter/app/dm/DMActivity;

    move-result-object v0

    const v1, 0x7f1302e1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/DMActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewSwitcher;

    .line 1030
    invoke-virtual {p1, v0}, Lcom/twitter/app/dm/widget/b;->a(Landroid/widget/ViewSwitcher;)V

    .line 1032
    const v1, 0x7f1302e2

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 1033
    invoke-virtual {v0, p2}, Lcom/twitter/internal/android/widget/ToolBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 1034
    invoke-virtual {p1, v0}, Lcom/twitter/app/dm/widget/b;->a(Lcom/twitter/internal/android/widget/ToolBar;)V

    .line 1035
    return-void
.end method

.method private a(Ljava/lang/String;Lccl;Lcdu;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x1

    .line 2115
    if-nez p2, :cond_0

    .line 2117
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->q()V

    .line 2119
    :cond_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "messages:thread:::send_dm"

    aput-object v2, v1, v7

    .line 2120
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    .line 2121
    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->ae:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->af:Z

    if-nez v1, :cond_1

    .line 2122
    new-instance v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 2123
    const-string/jumbo v2, "direct_share"

    iput-object v2, v1, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    .line 2124
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    .line 2126
    :cond_1
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 2127
    iput-boolean v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->af:Z

    .line 2129
    iput-boolean v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->v:Z

    .line 2131
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->K:Lcom/twitter/app/dm/m;

    invoke-virtual {v0}, Lcom/twitter/app/dm/m;->e()V

    .line 2133
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {v0}, Lcom/twitter/app/dm/n;->j()V

    .line 2136
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {v0}, Lcom/twitter/app/dm/n;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {v0}, Lcom/twitter/app/dm/n;->k()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2157
    :goto_0
    return-void

    .line 2140
    :cond_2
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2141
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->Q:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2142
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aJ()Lcom/twitter/metrics/j;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/twitter/android/metrics/c;->a(Ljava/lang/String;Lcom/twitter/metrics/j;)Lcom/twitter/android/metrics/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/metrics/c;->i()V

    .line 2144
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/i;

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/i;->d(Ljava/lang/String;)V

    .line 2146
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {v0}, Lcom/twitter/app/dm/n;->l()Lcom/twitter/android/media/selection/MediaAttachment;

    move-result-object v3

    .line 2147
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    .line 2148
    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->getCardUrl()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v2, p1

    move-object v5, p2

    move-object v6, p3

    .line 2147
    invoke-direct/range {v0 .. v6}, Lcom/twitter/app/dm/DMConversationFragment;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/android/media/selection/MediaAttachment;Ljava/lang/String;Lccl;Lcdu;)Lcom/twitter/library/api/dm/q;

    move-result-object v0

    .line 2149
    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1, v7}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 2150
    iget-wide v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    sget-object v1, Lcom/twitter/android/composer/ComposerType;->c:Lcom/twitter/android/composer/ComposerType;

    if-eqz v3, :cond_3

    .line 2151
    invoke-virtual {v3}, Lcom/twitter/android/media/selection/MediaAttachment;->d()Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v0

    .line 2150
    :goto_1
    invoke-static {v4, v5, v1, v0}, Lcom/twitter/android/composer/p;->a(JLcom/twitter/android/composer/ComposerType;Lcom/twitter/model/drafts/DraftAttachment;)V

    .line 2153
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {v0}, Lcom/twitter/app/dm/n;->o()V

    .line 2154
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->s()V

    .line 2155
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aS()V

    .line 2156
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bp()V

    goto :goto_0

    .line 2151
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1423
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aw:Lcom/twitter/app/dm/x;

    if-eqz v0, :cond_0

    .line 1424
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aw:Lcom/twitter/app/dm/x;

    .line 1425
    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/x;->a(Ljava/util/List;)Z

    move-result v0

    .line 1426
    if-eqz v0, :cond_0

    .line 1427
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/i;

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->aw:Lcom/twitter/app/dm/x;

    invoke-virtual {v1}, Lcom/twitter/app/dm/x;->a()Lcom/twitter/model/dms/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/i;->a(Lcom/twitter/model/dms/n;)V

    .line 1430
    :cond_0
    return-void
.end method

.method private a(ZZJZ)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1688
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->v:Z

    if-eqz v0, :cond_1

    .line 1689
    iput-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->v:Z

    .line 1690
    invoke-direct {p0, v1}, Lcom/twitter/app/dm/DMConversationFragment;->g(Z)V

    .line 1699
    :cond_0
    :goto_0
    return-void

    .line 1691
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->K:Lcom/twitter/app/dm/m;

    invoke-virtual {v0}, Lcom/twitter/app/dm/m;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p2, :cond_2

    .line 1692
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/i;

    invoke-virtual {v0}, Lcom/twitter/app/dm/i;->g()Lcbi;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->d(Lcbi;)V

    goto :goto_0

    .line 1693
    :cond_2
    if-eqz p1, :cond_4

    invoke-direct {p0, p3, p4}, Lcom/twitter/app/dm/DMConversationFragment;->e(J)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aY()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1694
    :cond_3
    invoke-direct {p0, v1}, Lcom/twitter/app/dm/DMConversationFragment;->g(Z)V

    goto :goto_0

    .line 1695
    :cond_4
    if-eqz p5, :cond_0

    .line 1697
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aW()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/app/dm/DMConversationFragment;Lcom/twitter/library/service/s;II)Z
    .locals 1

    .prologue
    .line 235
    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcom/twitter/library/service/s;II)Z

    move-result v0

    return v0
.end method

.method private aO()V
    .locals 1

    .prologue
    .line 762
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->as:Lrx/subjects/PublishSubject;

    if-eqz v0, :cond_0

    .line 763
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->as:Lrx/subjects/PublishSubject;

    invoke-virtual {v0}, Lrx/subjects/PublishSubject;->by_()V

    .line 764
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->as:Lrx/subjects/PublishSubject;

    .line 766
    :cond_0
    return-void
.end method

.method private aP()V
    .locals 2

    .prologue
    .line 975
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v1, "TAG_USERS_BOTTOM_SHEET"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 976
    instance-of v1, v0, Lcom/twitter/app/users/sheet/UsersBottomSheet;

    if-eqz v1, :cond_0

    .line 977
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/sheet/UsersBottomSheet;

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aH:Lcom/twitter/app/users/sheet/UsersBottomSheet;

    .line 987
    :goto_0
    return-void

    .line 979
    :cond_0
    new-instance v0, Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;

    invoke-direct {v0}, Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;-><init>()V

    const v1, 0x7f0200b0

    .line 981
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;->a(I)Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;->c:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    .line 982
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;->a(Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;)Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;

    move-result-object v0

    const/4 v1, 0x7

    .line 983
    invoke-virtual {v0, v1}, Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;->b(I)Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;

    move-result-object v0

    const v1, 0x7f0a02a3

    .line 984
    invoke-virtual {p0, v1}, Lcom/twitter/app/dm/DMConversationFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;->a(Ljava/lang/String;)Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;

    move-result-object v0

    .line 985
    invoke-virtual {v0}, Lcom/twitter/app/users/sheet/UsersBottomSheet$a$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/users/sheet/UsersBottomSheet$a;

    .line 979
    invoke-static {v0}, Lcom/twitter/app/users/sheet/UsersBottomSheet;->a(Lcom/twitter/app/users/sheet/UsersBottomSheet$a;)Lcom/twitter/app/users/sheet/UsersBottomSheet;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aH:Lcom/twitter/app/users/sheet/UsersBottomSheet;

    goto :goto_0
.end method

.method private aQ()V
    .locals 2

    .prologue
    .line 1126
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/common/list/l;->f()Lcno;

    move-result-object v0

    .line 1127
    new-instance v1, Lcom/twitter/app/dm/DMConversationFragment$3;

    invoke-direct {v1, p0, v0}, Lcom/twitter/app/dm/DMConversationFragment$3;-><init>(Lcom/twitter/app/dm/DMConversationFragment;Lcno;)V

    invoke-interface {v0, v1}, Lcno;->a(Lcno$b;)V

    .line 1139
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/dm/DMConversationFragment$4;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/DMConversationFragment$4;-><init>(Lcom/twitter/app/dm/DMConversationFragment;)V

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/list/l;->a(Lcno$c;)V

    .line 1151
    return-void
.end method

.method private aR()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1163
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ag:Z

    if-eqz v0, :cond_0

    .line 1178
    :goto_0
    return-void

    .line 1167
    :cond_0
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->r:Z

    if-nez v0, :cond_1

    .line 1168
    iput-boolean v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->r:Z

    .line 1169
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ad:Z

    if-nez v0, :cond_1

    .line 1170
    iput-boolean v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->s:Z

    .line 1174
    :cond_1
    new-instance v0, Lcom/twitter/library/api/dm/e;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    iget-object v3, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/dm/e;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    .line 1175
    invoke-direct {p0, v4}, Lcom/twitter/app/dm/DMConversationFragment;->h(Z)V

    .line 1176
    iput-boolean v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->q:Z

    .line 1177
    const/4 v1, 0x6

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto :goto_0
.end method

.method private aS()V
    .locals 1

    .prologue
    .line 1181
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {v0}, Lcom/twitter/app/dm/n;->n()V

    .line 1182
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->w()V

    .line 1183
    return-void
.end method

.method private aT()V
    .locals 4

    .prologue
    .line 1218
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:thread:dm_compose_bar:media:dismiss"

    aput-object v3, v1, v2

    .line 1219
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1218
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1220
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {v0}, Lcom/twitter/app/dm/n;->c()V

    .line 1221
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {v0}, Lcom/twitter/app/dm/n;->q()V

    .line 1222
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bf()V

    .line 1223
    return-void
.end method

.method private aU()V
    .locals 7

    .prologue
    .line 1355
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->n:Lapj;

    if-nez v0, :cond_0

    .line 1356
    new-instance v1, Lapj;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    const/4 v6, 0x2

    invoke-direct/range {v1 .. v6}, Lapj;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;JI)V

    iput-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->n:Lapj;

    .line 1358
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->n:Lapj;

    new-instance v1, Lcom/twitter/app/dm/DMConversationFragment$6;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/DMConversationFragment$6;-><init>(Lcom/twitter/app/dm/DMConversationFragment;)V

    invoke-virtual {v0, v1}, Lapj;->a(Lapj$a;)V

    .line 1397
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->n:Lapj;

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lapj;->a(Ljava/lang/String;)V

    .line 1398
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->n:Lapj;

    invoke-virtual {v0}, Lapj;->a()V

    .line 1399
    return-void
.end method

.method private aV()V
    .locals 1

    .prologue
    .line 1681
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->d:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bd()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1682
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/i;

    invoke-virtual {v0}, Lcom/twitter/app/dm/i;->notifyDataSetChanged()V

    .line 1684
    :cond_0
    return-void
.end method

.method private aW()V
    .locals 2

    .prologue
    .line 1702
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    new-instance v1, Lcom/twitter/app/dm/DMConversationFragment$10;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/DMConversationFragment$10;-><init>(Lcom/twitter/app/dm/DMConversationFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 1708
    return-void
.end method

.method private aX()J
    .locals 2

    .prologue
    .line 1728
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/i;

    invoke-virtual {v0}, Lcom/twitter/app/dm/i;->g()Lcbi;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->d(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/m;

    .line 1729
    if-nez v0, :cond_0

    .line 1730
    const-wide/16 v0, 0x0

    .line 1733
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {v0}, Lcom/twitter/model/dms/m;->a()J

    move-result-wide v0

    goto :goto_0
.end method

.method private aY()Z
    .locals 1

    .prologue
    .line 1737
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ao:Lcom/twitter/app/dm/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ao:Lcom/twitter/app/dm/ag;

    .line 1738
    invoke-virtual {v0}, Lcom/twitter/app/dm/ag;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ao:Lcom/twitter/app/dm/ag;

    .line 1739
    invoke-virtual {v0}, Lcom/twitter/app/dm/ag;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1737
    :goto_0
    return v0

    .line 1739
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aZ()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1755
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->x:Z

    if-nez v0, :cond_1

    .line 1756
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->y:Z

    if-eqz v0, :cond_2

    .line 1757
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->y()V

    .line 1758
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->J:Z

    if-nez v0, :cond_0

    .line 1759
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->I:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 1761
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->J:Z

    .line 1770
    :cond_1
    :goto_0
    return-void

    .line 1763
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->x()V

    .line 1764
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aj()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1765
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->I:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    .line 1767
    :cond_3
    iput-boolean v3, p0, Lcom/twitter/app/dm/DMConversationFragment;->J:Z

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/app/dm/DMConversationFragment;)J
    .locals 2

    .prologue
    .line 235
    iget-wide v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    return-wide v0
.end method

.method private b(Landroid/view/View;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1059
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->H:Z

    if-eqz v0, :cond_0

    .line 1060
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    if-ge v0, p2, :cond_1

    const/4 v0, 0x1

    .line 1061
    :goto_0
    if-eqz v0, :cond_0

    .line 1066
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aW()V

    .line 1067
    iput-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->H:Z

    .line 1070
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 1060
    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/app/dm/DMConversationFragment;Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0, p1, p2}, Lcom/twitter/app/dm/DMConversationFragment;->b(Landroid/view/View;I)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/app/dm/DMConversationFragment;Lcbi;)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/DMConversationFragment;->d(Lcbi;)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/app/dm/DMConversationFragment;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/DMConversationFragment;->a(Ljava/util/List;)V

    return-void
.end method

.method private b(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1445
    new-instance v0, Lcom/twitter/app/dm/DMConversationFragment$7;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/DMConversationFragment$7;-><init>(Lcom/twitter/app/dm/DMConversationFragment;)V

    invoke-static {p1, v0}, Lcpt;->a(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 1455
    new-instance v1, Lbim;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    invoke-direct {v1, v2, v3, v0}, Lbim;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/util/Collection;)V

    const/16 v0, 0xd

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 1457
    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 1402
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->z:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1403
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aE:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1405
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/twitter/app/dm/DMConversationFragment;Lcom/twitter/library/service/s;II)Z
    .locals 1

    .prologue
    .line 235
    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcom/twitter/library/service/s;II)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/app/dm/DMConversationFragment;Z)Z
    .locals 0

    .prologue
    .line 235
    iput-boolean p1, p0, Lcom/twitter/app/dm/DMConversationFragment;->o:Z

    return p1
.end method

.method private ba()V
    .locals 5

    .prologue
    .line 1805
    new-instance v0, Lcom/twitter/app/dm/d;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    iget-object v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/app/dm/d;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1806
    return-void
.end method

.method private bb()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2276
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/twitter/android/media/camera/e;->a(Landroid/app/Activity;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2277
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->g:Lcom/twitter/android/media/selection/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, v2}, Lcom/twitter/android/media/selection/c;->a(ZII)V

    .line 2284
    :goto_0
    return-void

    .line 2280
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string/jumbo v1, "messages:thread::thread"

    invoke-static {v0, v2, v1}, Lcom/twitter/android/media/camera/e;->a(Landroid/app/Activity;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0xe

    .line 2279
    invoke-virtual {p0, v0, v1}, Lcom/twitter/app/dm/DMConversationFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private bc()V
    .locals 3

    .prologue
    .line 2287
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/WebViewActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v1, 0x7f0a0c3d

    .line 2288
    invoke-virtual {p0, v1}, Lcom/twitter/app/dm/DMConversationFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 2289
    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->startActivity(Landroid/content/Intent;)V

    .line 2290
    return-void
.end method

.method private bd()Z
    .locals 1

    .prologue
    .line 2443
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->ak()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/i;

    invoke-virtual {v0}, Lcom/twitter/app/dm/i;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private be()V
    .locals 2

    .prologue
    .line 2447
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v1, 0x7f130011

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/NewItemBannerView;

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aa:Lcom/twitter/android/widget/NewItemBannerView;

    .line 2448
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aa:Lcom/twitter/android/widget/NewItemBannerView;

    const v1, 0x7f0a02b9

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/NewItemBannerView;->setText(I)V

    .line 2449
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aa:Lcom/twitter/android/widget/NewItemBannerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/NewItemBannerView;->a()V

    .line 2450
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aa:Lcom/twitter/android/widget/NewItemBannerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/NewItemBannerView;->setShouldThrottleShowing(Z)V

    .line 2451
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aa:Lcom/twitter/android/widget/NewItemBannerView;

    new-instance v1, Lcom/twitter/app/dm/DMConversationFragment$15;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/DMConversationFragment$15;-><init>(Lcom/twitter/app/dm/DMConversationFragment;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/NewItemBannerView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2458
    return-void
.end method

.method private bf()V
    .locals 1

    .prologue
    .line 2729
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {v0}, Lcom/twitter/app/dm/n;->p()V

    .line 2730
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aS()V

    .line 2731
    return-void
.end method

.method private bg()Z
    .locals 1

    .prologue
    .line 2734
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->K:Lcom/twitter/app/dm/m;

    invoke-virtual {v0}, Lcom/twitter/app/dm/m;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ac:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private bh()Lcom/twitter/app/dm/DMActivity;
    .locals 2

    .prologue
    .line 2817
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-class v1, Lcom/twitter/app/dm/DMActivity;

    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/DMActivity;

    return-object v0
.end method

.method private bi()Z
    .locals 1

    .prologue
    .line 2867
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->e(I)Z

    move-result v0

    return v0
.end method

.method private bj()Z
    .locals 1

    .prologue
    .line 2871
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->e(I)Z

    move-result v0

    return v0
.end method

.method private bk()I
    .locals 3

    .prologue
    .line 2879
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    .line 2880
    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v2

    sub-int/2addr v1, v2

    .line 2881
    invoke-virtual {v0}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v0

    .line 2882
    sub-int v0, v1, v0

    return v0
.end method

.method private bl()V
    .locals 1

    .prologue
    .line 2932
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bh()Lcom/twitter/app/dm/DMActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/app/dm/DMActivity;->p()V

    .line 2933
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bm()V

    .line 2934
    return-void
.end method

.method private bm()V
    .locals 1

    .prologue
    .line 2937
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ag:Z

    .line 2938
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    if-eqz v0, :cond_0

    .line 2939
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->u()V

    .line 2941
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2942
    if-eqz v0, :cond_1

    .line 2943
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2945
    :cond_1
    return-void
.end method

.method private bn()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2984
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2985
    invoke-static {v0}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2986
    new-instance v1, Lcom/twitter/android/GalleryGridActivity$b;

    invoke-direct {v1}, Lcom/twitter/android/GalleryGridActivity$b;-><init>()V

    .line 2987
    invoke-virtual {v1, v0}, Lcom/twitter/android/GalleryGridActivity$b;->a(Landroid/content/Context;)Lcom/twitter/android/GalleryGridActivity$b;

    move-result-object v0

    const-string/jumbo v1, "dm_composition"

    .line 2988
    invoke-virtual {v0, v1}, Lcom/twitter/android/GalleryGridActivity$b;->a(Ljava/lang/String;)Lcom/twitter/android/GalleryGridActivity$b;

    move-result-object v0

    const-string/jumbo v1, "messages:thread:dm_compose_bar:media"

    .line 2989
    invoke-virtual {v0, v1}, Lcom/twitter/android/GalleryGridActivity$b;->b(Ljava/lang/String;)Lcom/twitter/android/GalleryGridActivity$b;

    move-result-object v0

    .line 2990
    invoke-virtual {v0, v2}, Lcom/twitter/android/GalleryGridActivity$b;->a(Z)Lcom/twitter/android/GalleryGridActivity$b;

    move-result-object v0

    .line 2991
    invoke-virtual {v0, v2}, Lcom/twitter/android/GalleryGridActivity$b;->a(I)Lcom/twitter/android/GalleryGridActivity$b;

    move-result-object v0

    .line 2992
    invoke-virtual {v0}, Lcom/twitter/android/GalleryGridActivity$b;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    const/4 v1, 0x5

    .line 2986
    invoke-virtual {p0, v0, v1}, Lcom/twitter/app/dm/DMConversationFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2997
    :goto_0
    return-void

    .line 2995
    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->b(I)V

    goto :goto_0
.end method

.method private bo()V
    .locals 1

    .prologue
    .line 3188
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->at:Lcom/twitter/app/dm/DMConversationFragment$b;

    if-eqz v0, :cond_0

    .line 3189
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->at:Lcom/twitter/app/dm/DMConversationFragment$b;

    invoke-virtual {v0}, Lcom/twitter/app/dm/DMConversationFragment$b;->b()V

    .line 3191
    :cond_0
    return-void
.end method

.method private bp()V
    .locals 1

    .prologue
    .line 3194
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->at:Lcom/twitter/app/dm/DMConversationFragment$b;

    if-eqz v0, :cond_0

    .line 3195
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->at:Lcom/twitter/app/dm/DMConversationFragment$b;

    invoke-virtual {v0}, Lcom/twitter/app/dm/DMConversationFragment$b;->d()V

    .line 3197
    :cond_0
    return-void
.end method

.method private bq()V
    .locals 1

    .prologue
    .line 3200
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->at:Lcom/twitter/app/dm/DMConversationFragment$b;

    if-eqz v0, :cond_0

    .line 3201
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->at:Lcom/twitter/app/dm/DMConversationFragment$b;

    invoke-virtual {v0}, Lcom/twitter/app/dm/DMConversationFragment$b;->c()V

    .line 3203
    :cond_0
    return-void
.end method

.method private br()V
    .locals 1

    .prologue
    .line 3206
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->at:Lcom/twitter/app/dm/DMConversationFragment$b;

    if-eqz v0, :cond_0

    .line 3207
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->at:Lcom/twitter/app/dm/DMConversationFragment$b;

    invoke-virtual {v0}, Lcom/twitter/app/dm/DMConversationFragment$b;->j()V

    .line 3209
    :cond_0
    return-void
.end method

.method private c(I)V
    .locals 3

    .prologue
    .line 2334
    .line 2335
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bd()Z

    move-result v0

    if-eqz v0, :cond_1

    if-ltz p1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/i;

    invoke-virtual {v0}, Lcom/twitter/app/dm/i;->getCount()I

    move-result v0

    if-ge v0, p1, :cond_1

    .line 2336
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/i;

    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/i;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/m;

    .line 2337
    :goto_0
    if-eqz v0, :cond_0

    .line 2338
    invoke-virtual {v0}, Lcom/twitter/model/dms/m;->c()Lcom/twitter/model/dms/c;

    move-result-object v0

    .line 2339
    invoke-virtual {v0}, Lcom/twitter/model/dms/c;->o()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2340
    const/16 v1, 0x304

    .line 2341
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/e;

    .line 2340
    invoke-static {v1, v0, p0}, Lcom/twitter/app/dm/dialog/DMMessageDialog;->a(ILcom/twitter/model/dms/e;Lcom/twitter/app/dm/dialog/a;)Lcom/twitter/app/dm/dialog/DMMessageDialog;

    move-result-object v0

    .line 2342
    invoke-virtual {v0, p0}, Lcom/twitter/app/dm/dialog/DMMessageDialog;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 2343
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 2346
    :cond_0
    return-void

    .line 2336
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lcbi;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1434
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->o:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ao:Lcom/twitter/app/dm/ag;

    if-eqz v0, :cond_1

    .line 1435
    invoke-virtual {p1}, Lcbi;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/Participant;

    .line 1436
    iget-object v0, v0, Lcom/twitter/model/dms/Participant;->g:Lcom/twitter/model/core/TwitterUser;

    .line 1437
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 1438
    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->ao:Lcom/twitter/app/dm/ag;

    iget-wide v4, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    iget-object v0, v0, Lcom/twitter/model/core/TwitterUser;->d:Ljava/lang/String;

    invoke-virtual {v2, v4, v5, v0}, Lcom/twitter/app/dm/ag;->a(JLjava/lang/String;)V

    goto :goto_0

    .line 1442
    :cond_1
    return-void
.end method

.method private c(Lcom/twitter/android/media/selection/MediaAttachment;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2755
    iget v0, p1, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    packed-switch v0, :pswitch_data_0

    .line 2777
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/n;->a(Z)V

    .line 2778
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bf()V

    .line 2779
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->Y()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2781
    iget v0, p1, Lcom/twitter/android/media/selection/MediaAttachment;->a:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 2783
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lwe;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 2787
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2792
    :cond_0
    :goto_1
    return-void

    .line 2757
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {p1}, Lcom/twitter/android/media/selection/MediaAttachment;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/n;->c(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2758
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {v0, v2}, Lcom/twitter/app/dm/n;->a(Z)V

    .line 2759
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {v0}, Lcom/twitter/app/dm/n;->i()Z

    move-result v0

    .line 2760
    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/twitter/app/dm/n;->d(Landroid/net/Uri;)V

    .line 2761
    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {v1, p1}, Lcom/twitter/app/dm/n;->c(Lcom/twitter/android/media/selection/MediaAttachment;)V

    .line 2762
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bj()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2763
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aW()V

    .line 2765
    :cond_1
    if-eqz v0, :cond_0

    .line 2767
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->getMessageText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->c(Ljava/lang/String;)V

    goto :goto_1

    .line 2773
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/n;->c(Lcom/twitter/android/media/selection/MediaAttachment;)V

    goto :goto_1

    .line 2785
    :cond_2
    const v0, 0x7f0a04b5

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2755
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic c(Lcom/twitter/app/dm/DMConversationFragment;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/DMConversationFragment;->c(Ljava/util/List;)V

    return-void
.end method

.method static synthetic c(Lcom/twitter/app/dm/DMConversationFragment;Z)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/DMConversationFragment;->g(Z)V

    return-void
.end method

.method private c(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/dms/Participant;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1659
    if-nez p1, :cond_1

    .line 1675
    :cond_0
    :goto_0
    return-void

    .line 1662
    :cond_1
    new-instance v0, Lcom/twitter/app/dm/DMConversationFragment$9;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/DMConversationFragment$9;-><init>(Lcom/twitter/app/dm/DMConversationFragment;)V

    .line 1663
    invoke-static {p1, v0}, Lcpt;->a(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v0

    .line 1662
    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->e(Ljava/util/Collection;)[J

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->p:[J

    .line 1671
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->p:[J

    if-eqz v0, :cond_0

    .line 1672
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aH:Lcom/twitter/app/users/sheet/UsersBottomSheet;

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->p:[J

    invoke-virtual {v0, v1}, Lcom/twitter/app/users/sheet/UsersBottomSheet;->a([J)V

    .line 1673
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->C:Z

    goto :goto_0
.end method

.method private c(Z)V
    .locals 2

    .prologue
    .line 1417
    if-eqz p1, :cond_0

    .line 1418
    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->aG:Landroid/view/View;

    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ah:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1420
    :cond_0
    return-void

    .line 1418
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/app/dm/DMConversationFragment;Lcom/twitter/library/service/s;II)Z
    .locals 1

    .prologue
    .line 235
    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcom/twitter/library/service/s;II)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/twitter/app/dm/DMConversationFragment;)[J
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->p:[J

    return-object v0
.end method

.method private d(Lcbi;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/model/dms/m;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2424
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->K:Lcom/twitter/app/dm/m;

    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/m;->c(Lcbi;)I

    move-result v0

    .line 2425
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aj()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2426
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getView()Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2440
    :cond_0
    :goto_0
    return-void

    .line 2430
    :cond_1
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aI()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 2433
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-double v2, v1

    const-wide v4, 0x3fc999999999999aL    # 0.2

    mul-double/2addr v2, v4

    double-to-int v1, v2

    .line 2434
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v2

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v0, v1}, Lcom/twitter/app/common/list/l;->a(II)V

    .line 2439
    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->M:Z

    goto :goto_0

    .line 2436
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->g(Z)V

    goto :goto_1
.end method

.method static synthetic d(Lcom/twitter/app/dm/DMConversationFragment;Z)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0, p1}, Lcom/twitter/app/dm/DMConversationFragment;->i(Z)V

    return-void
.end method

.method static synthetic d(Lcom/twitter/app/dm/DMConversationFragment;)Z
    .locals 1

    .prologue
    .line 235
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->am:Z

    return v0
.end method

.method static synthetic d(Lcom/twitter/app/dm/DMConversationFragment;Lcom/twitter/library/service/s;II)Z
    .locals 1

    .prologue
    .line 235
    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcom/twitter/library/service/s;II)Z

    move-result v0

    return v0
.end method

.method static synthetic e(Lcom/twitter/app/dm/DMConversationFragment;)Lcom/twitter/app/dm/ag;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ao:Lcom/twitter/app/dm/ag;

    return-object v0
.end method

.method private e(I)Z
    .locals 1

    .prologue
    .line 2875
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bk()I

    move-result v0

    if-gt v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(J)Z
    .locals 3

    .prologue
    .line 1724
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aX()J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic f(Lcom/twitter/app/dm/DMConversationFragment;)Lcom/twitter/library/client/o;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->N:Lcom/twitter/library/client/o;

    return-object v0
.end method

.method private f(J)V
    .locals 3

    .prologue
    .line 1743
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/twitter/app/dm/DMConversationFragment$11;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/DMConversationFragment$11;-><init>(Lcom/twitter/app/dm/DMConversationFragment;)V

    invoke-static {v0, p1, p2, v1}, Lcom/twitter/app/dm/c;->a(Landroid/content/Context;JLcom/twitter/app/dm/c$a;)V

    .line 1752
    return-void
.end method

.method private f(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1408
    invoke-static {p1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1409
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aF:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1410
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aF:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1414
    :goto_0
    return-void

    .line 1412
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aF:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private f(Z)V
    .locals 1

    .prologue
    .line 1800
    iput-boolean p1, p0, Lcom/twitter/app/dm/DMConversationFragment;->w:Z

    .line 1801
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aH()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->h()V

    .line 1802
    return-void
.end method

.method static synthetic g(Lcom/twitter/app/dm/DMConversationFragment;)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bq()V

    return-void
.end method

.method private g(Z)V
    .locals 2

    .prologue
    .line 2413
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bd()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2414
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    .line 2415
    if-eqz p1, :cond_1

    .line 2416
    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    .line 2421
    :cond_0
    :goto_0
    return-void

    .line 2418
    :cond_1
    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0
.end method

.method private h(J)V
    .locals 3

    .prologue
    .line 2375
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/16 v1, 0x302

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a0911

    .line 2376
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->a(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0b0014

    .line 2377
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->c(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 2378
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 2379
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 2380
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 2381
    iput-wide p1, p0, Lcom/twitter/app/dm/DMConversationFragment;->b:J

    .line 2382
    return-void
.end method

.method static synthetic h(Lcom/twitter/app/dm/DMConversationFragment;)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bo()V

    return-void
.end method

.method private h(Z)V
    .locals 1

    .prologue
    .line 2477
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/app/common/list/l;->a(Z)V

    .line 2478
    return-void
.end method

.method private i(Z)V
    .locals 1

    .prologue
    .line 2844
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->y:Z

    if-eq v0, p1, :cond_0

    .line 2845
    iput-boolean p1, p0, Lcom/twitter/app/dm/DMConversationFragment;->y:Z

    .line 2846
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2847
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/i;

    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/i;->b(Z)V

    .line 2850
    :cond_0
    return-void
.end method

.method static synthetic i(Lcom/twitter/app/dm/DMConversationFragment;)Z
    .locals 1

    .prologue
    .line 235
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bj()Z

    move-result v0

    return v0
.end method

.method static synthetic j(Lcom/twitter/app/dm/DMConversationFragment;)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aW()V

    return-void
.end method

.method private j(Z)V
    .locals 1

    .prologue
    .line 2856
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->x:Z

    if-eq v0, p1, :cond_0

    .line 2857
    iput-boolean p1, p0, Lcom/twitter/app/dm/DMConversationFragment;->x:Z

    .line 2858
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->ay()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2859
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/i;

    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/i;->a(Z)V

    .line 2862
    :cond_0
    return-void
.end method

.method static synthetic k(Lcom/twitter/app/dm/DMConversationFragment;)Lcom/twitter/app/dm/widget/c;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aC:Lcom/twitter/app/dm/widget/c;

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/app/dm/DMConversationFragment;)Z
    .locals 1

    .prologue
    .line 235
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->o:Z

    return v0
.end method

.method static synthetic m(Lcom/twitter/app/dm/DMConversationFragment;)Lcom/twitter/app/dm/m;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->K:Lcom/twitter/app/dm/m;

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/app/dm/DMConversationFragment;)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aZ()V

    return-void
.end method

.method static synthetic o(Lcom/twitter/app/dm/DMConversationFragment;)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bb()V

    return-void
.end method

.method static synthetic p(Lcom/twitter/app/dm/DMConversationFragment;)Lcom/twitter/android/media/selection/c;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->g:Lcom/twitter/android/media/selection/c;

    return-object v0
.end method

.method static synthetic q(Lcom/twitter/app/dm/DMConversationFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->B:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic r(Lcom/twitter/app/dm/DMConversationFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic s(Lcom/twitter/app/dm/DMConversationFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method static synthetic t(Lcom/twitter/app/dm/DMConversationFragment;)Lcom/twitter/android/widget/NewItemBannerView;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aa:Lcom/twitter/android/widget/NewItemBannerView;

    return-object v0
.end method

.method static synthetic u(Lcom/twitter/app/dm/DMConversationFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic v(Lcom/twitter/app/dm/DMConversationFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic w(Lcom/twitter/app/dm/DMConversationFragment;)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aT()V

    return-void
.end method

.method static synthetic x(Lcom/twitter/app/dm/DMConversationFragment;)Lcom/twitter/app/dm/n;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    return-object v0
.end method

.method static synthetic y(Lcom/twitter/app/dm/DMConversationFragment;)Lcom/twitter/android/media/selection/c;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->m:Lcom/twitter/android/media/selection/c;

    return-object v0
.end method

.method static synthetic z(Lcom/twitter/app/dm/DMConversationFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public A()V
    .locals 4

    .prologue
    .line 2176
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:thread:dm_compose_bar:stickers_button:click"

    aput-object v3, v1, v2

    .line 2177
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 2176
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 2178
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->O()V

    .line 2179
    return-void
.end method

.method public B()Z
    .locals 1

    .prologue
    .line 2207
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->E:Z

    return v0
.end method

.method public C()Lcom/twitter/app/dm/j;
    .locals 1

    .prologue
    .line 2823
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/app/dm/j;->d(Landroid/os/Bundle;)Lcom/twitter/app/dm/j;

    move-result-object v0

    return-object v0
.end method

.method public D()V
    .locals 1

    .prologue
    .line 2897
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->u:Lcom/twitter/app/dm/t;

    invoke-virtual {v0}, Lcom/twitter/app/dm/t;->b()V

    .line 2898
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bm()V

    .line 2899
    return-void
.end method

.method public E()V
    .locals 1

    .prologue
    .line 2909
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->K:Lcom/twitter/app/dm/m;

    invoke-virtual {v0}, Lcom/twitter/app/dm/m;->e()V

    .line 2910
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->q()V

    .line 2911
    return-void
.end method

.method public F()V
    .locals 2

    .prologue
    .line 2951
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ag:Z

    .line 2953
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->u:Lcom/twitter/app/dm/t;

    invoke-virtual {v0}, Lcom/twitter/app/dm/t;->b()V

    .line 2954
    new-instance v1, Lcom/twitter/android/widget/aj$b;

    const/16 v0, 0x30a

    invoke-direct {v1, v0}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->x:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0a0510

    .line 2955
    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0a40

    .line 2958
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a05e0

    .line 2959
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 2960
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 2961
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$a;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 2962
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 2963
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 2964
    return-void

    .line 2954
    :cond_0
    const v0, 0x7f0a0511

    goto :goto_0
.end method

.method public G()V
    .locals 2

    .prologue
    .line 3001
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->as:Lrx/subjects/PublishSubject;

    if-eqz v0, :cond_0

    .line 3002
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->as:Lrx/subjects/PublishSubject;

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject;->a(Ljava/lang/Object;)V

    .line 3004
    :cond_0
    return-void
.end method

.method public synthetic H()Lcom/twitter/app/common/list/i;
    .locals 1

    .prologue
    .line 235
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->C()Lcom/twitter/app/dm/j;

    move-result-object v0

    return-object v0
.end method

.method public synthetic I()Lcom/twitter/app/common/base/b;
    .locals 1

    .prologue
    .line 235
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->C()Lcom/twitter/app/dm/j;

    move-result-object v0

    return-object v0
.end method

.method protected K()Laoy;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laoy",
            "<",
            "Lcbi",
            "<",
            "Lcom/twitter/model/dms/m;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1317
    new-instance v0, Lcom/twitter/app/dm/DMConversationFragment$5;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/DMConversationFragment$5;-><init>(Lcom/twitter/app/dm/DMConversationFragment;)V

    .line 1328
    new-instance v1, Lapm;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    .line 1329
    invoke-static {v4, v5}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v4

    .line 1330
    invoke-virtual {v4}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v4

    const-class v5, Lawx;

    .line 1331
    invoke-interface {v4, v5}, Lcom/twitter/database/schema/TwitterSchema;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v4

    invoke-direct {v1, v2, v3, v0, v4}, Lapm;-><init>(Landroid/support/v4/app/LoaderManager;ILcom/twitter/util/object/j;Lcom/twitter/database/model/k;)V

    .line 1328
    return-object v1
.end method

.method public M()V
    .locals 1

    .prologue
    .line 3146
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->m:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v0}, Lcom/twitter/android/media/selection/c;->e()V

    .line 3147
    return-void
.end method

.method public a()V
    .locals 1

    .prologue
    .line 1712
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->g(Z)V

    .line 1713
    return-void
.end method

.method public a(IILandroid/content/Intent;Lcom/twitter/android/media/selection/a;)V
    .locals 1

    .prologue
    .line 3157
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->m:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/twitter/android/media/selection/c;->a(IILandroid/content/Intent;Lcom/twitter/android/media/selection/a;)V

    .line 3158
    return-void
.end method

.method public a(JLjava/lang/String;)V
    .locals 5

    .prologue
    .line 2915
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v0

    .line 2917
    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->u:Lcom/twitter/app/dm/t;

    invoke-virtual {v1, p3}, Lcom/twitter/app/dm/t;->b(Ljava/lang/String;)V

    .line 2918
    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->u:Lcom/twitter/app/dm/t;

    invoke-virtual {v1, p3}, Lcom/twitter/app/dm/t;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2919
    if-eqz v1, :cond_0

    .line 2920
    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/p;->a(Ljava/lang/String;Z)V

    .line 2924
    :cond_0
    new-instance v1, Lcom/twitter/app/dm/s;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    invoke-direct {v1, v2, v3, p1, p2}, Lcom/twitter/app/dm/s;-><init>(Landroid/content/ContextWrapper;Lcom/twitter/library/client/Session;J)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 2926
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->Q:Ljava/util/Set;

    invoke-interface {v0, p3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2927
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a053f

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2929
    :cond_1
    return-void
.end method

.method public a(Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 2968
    const/16 v0, 0x30a

    if-ne p2, v0, :cond_0

    .line 2969
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bm()V

    .line 2971
    :cond_0
    return-void
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v0, -0x1

    const/4 v7, 0x0

    .line 2214
    sparse-switch p2, :sswitch_data_0

    .line 2273
    :cond_0
    :goto_0
    return-void

    .line 2216
    :sswitch_0
    invoke-static {p3}, Lcom/twitter/app/dm/dialog/DeleteConversationDialog;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2217
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bl()V

    goto :goto_0

    .line 2222
    :sswitch_1
    iget-wide v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 2223
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->K:Lcom/twitter/app/dm/m;

    invoke-virtual {v0}, Lcom/twitter/app/dm/m;->e()V

    .line 2224
    packed-switch p3, :pswitch_data_0

    .line 2253
    :cond_1
    :goto_1
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->b:J

    goto :goto_0

    .line 2226
    :pswitch_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "messages:thread:message:spam:report_as_spam"

    aput-object v2, v1, v7

    .line 2227
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 2226
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 2228
    new-instance v1, Lcom/twitter/library/api/dm/ReportDMRequest;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    iget-wide v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->b:J

    sget-object v6, Lcom/twitter/library/api/dm/ReportDMRequest$Type;->a:Lcom/twitter/library/api/dm/ReportDMRequest$Type;

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/api/dm/ReportDMRequest;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcom/twitter/library/api/dm/ReportDMRequest$Type;)V

    const/4 v0, 0x3

    invoke-virtual {p0, v1, v0, v7}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto :goto_1

    .line 2233
    :pswitch_1
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "messages:thread:message:abusive:report_as_spam"

    aput-object v2, v1, v7

    .line 2234
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 2233
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 2235
    new-instance v1, Lcom/twitter/library/api/dm/ReportDMRequest;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    iget-wide v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->b:J

    sget-object v6, Lcom/twitter/library/api/dm/ReportDMRequest$Type;->b:Lcom/twitter/library/api/dm/ReportDMRequest$Type;

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/api/dm/ReportDMRequest;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcom/twitter/library/api/dm/ReportDMRequest$Type;)V

    const/4 v0, 0x4

    invoke-virtual {p0, v1, v0, v7}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 2238
    new-instance v0, Lcom/twitter/android/widget/aj$b;

    const/16 v1, 0x303

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    const v1, 0x7f0a050d

    .line 2239
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->b(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a0a40

    .line 2240
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->d(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    const v1, 0x7f0a05e0

    .line 2241
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj$b;->f(I)Lcom/twitter/android/widget/aj$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aj$b;

    .line 2242
    invoke-virtual {v0}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 2243
    invoke-virtual {v0, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 2244
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto/16 :goto_1

    .line 2257
    :sswitch_2
    if-ne p3, v0, :cond_0

    .line 2258
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bc()V

    goto/16 :goto_0

    .line 2263
    :sswitch_3
    if-ne p3, v0, :cond_2

    .line 2264
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bc()V

    .line 2266
    :cond_2
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bm()V

    goto/16 :goto_0

    .line 2214
    nop

    :sswitch_data_0
    .sparse-switch
        0x301 -> :sswitch_0
        0x302 -> :sswitch_1
        0x303 -> :sswitch_2
        0x30a -> :sswitch_3
    .end sparse-switch

    .line 2224
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1074
    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/dm/DMConversationFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1075
    return-void
.end method

.method public a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 3151
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->m:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/selection/c;->a(Landroid/net/Uri;)V

    .line 3152
    return-void
.end method

.method public a(Landroid/net/Uri;Lcom/twitter/android/media/selection/a;)V
    .locals 2

    .prologue
    .line 3136
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->m:Lcom/twitter/android/media/selection/c;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, p2}, Lcom/twitter/android/media/selection/c;->a(Landroid/net/Uri;ZLcom/twitter/android/media/selection/a;)V

    .line 3137
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1585
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1643
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1587
    :pswitch_1
    invoke-static {p2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsy;

    .line 1588
    new-instance v1, Lbsz;

    invoke-direct {v1}, Lbsz;-><init>()V

    .line 1589
    invoke-virtual {v1, v0}, Lbsz;->a(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/q;

    .line 1590
    if-eqz v0, :cond_0

    .line 1591
    iget-object v1, v0, Lcom/twitter/model/dms/q;->b:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/twitter/app/dm/DMConversationFragment;->b(Ljava/lang/String;)V

    .line 1593
    iget-object v1, v0, Lcom/twitter/model/dms/q;->m:Lcom/twitter/model/dms/x;

    .line 1594
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/twitter/model/dms/x;->a()Ljava/lang/String;

    move-result-object v1

    .line 1595
    :goto_1
    iget-object v3, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual {v3}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->t()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual {v3}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->getMessageText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1596
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1597
    iget-object v3, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual {v3, v1}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->b(Ljava/lang/String;)V

    .line 1600
    :cond_1
    iget-boolean v1, v0, Lcom/twitter/model/dms/q;->j:Z

    if-nez v1, :cond_3

    move v1, v2

    :goto_2
    invoke-direct {p0, v1}, Lcom/twitter/app/dm/DMConversationFragment;->f(Z)V

    .line 1601
    iget-object v1, v0, Lcom/twitter/model/dms/q;->d:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/twitter/app/dm/DMConversationFragment;->f(Ljava/lang/String;)V

    .line 1602
    iget-boolean v1, v0, Lcom/twitter/model/dms/q;->h:Z

    invoke-direct {p0, v1}, Lcom/twitter/app/dm/DMConversationFragment;->j(Z)V

    .line 1604
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aH()Lcmt;

    move-result-object v1

    invoke-virtual {v1}, Lcmt;->h()V

    .line 1606
    iget-object v1, v0, Lcom/twitter/model/dms/q;->c:Ljava/lang/String;

    iput-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->A:Ljava/lang/String;

    .line 1607
    new-instance v1, Lcom/twitter/library/dm/b;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    invoke-direct {v1, v0, v3, v4, v5}, Lcom/twitter/library/dm/b;-><init>(Lcom/twitter/model/dms/q;Landroid/content/Context;J)V

    invoke-virtual {v1}, Lcom/twitter/library/dm/b;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->z:Ljava/lang/String;

    .line 1608
    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->o:Z

    invoke-direct {p0, v1}, Lcom/twitter/app/dm/DMConversationFragment;->b(Z)V

    .line 1610
    iget-boolean v1, v0, Lcom/twitter/model/dms/q;->h:Z

    iget-object v3, v0, Lcom/twitter/model/dms/q;->i:Ljava/util/List;

    .line 1611
    invoke-static {v1, v3}, Lcom/twitter/library/dm/e;->a(ZLjava/util/List;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->ah:Z

    .line 1612
    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->o:Z

    invoke-direct {p0, v1}, Lcom/twitter/app/dm/DMConversationFragment;->c(Z)V

    .line 1614
    iget-object v1, v0, Lcom/twitter/model/dms/q;->e:Ljava/lang/String;

    iput-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->B:Ljava/lang/String;

    .line 1615
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->az()Lcjr;

    move-result-object v1

    check-cast v1, Lcom/twitter/app/dm/i;

    iget-object v3, p0, Lcom/twitter/app/dm/DMConversationFragment;->B:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/twitter/app/dm/i;->a(Ljava/lang/String;)V

    .line 1617
    iget-boolean v1, v0, Lcom/twitter/model/dms/q;->k:Z

    invoke-direct {p0, v1}, Lcom/twitter/app/dm/DMConversationFragment;->i(Z)V

    .line 1618
    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->O:Z

    if-nez v1, :cond_0

    .line 1619
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aZ()V

    .line 1620
    iput-boolean v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->O:Z

    .line 1621
    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->y:Z

    if-eqz v1, :cond_0

    .line 1622
    iget-object v0, v0, Lcom/twitter/model/dms/q;->i:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 1623
    if-eqz v0, :cond_4

    iget-wide v0, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    .line 1624
    :goto_3
    invoke-direct {p0, v0, v1}, Lcom/twitter/app/dm/DMConversationFragment;->f(J)V

    goto/16 :goto_0

    .line 1594
    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 1600
    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    .line 1623
    :cond_4
    const-wide/16 v0, -0x1

    goto :goto_3

    .line 1632
    :pswitch_2
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ai:Z

    if-eqz v0, :cond_0

    .line 1633
    invoke-direct {p0, p2}, Lcom/twitter/app/dm/DMConversationFragment;->a(Landroid/database/Cursor;)Ljava/util/Map;

    move-result-object v1

    .line 1634
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/i;

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/i;->a(Ljava/util/Map;)V

    .line 1635
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aV()V

    goto/16 :goto_0

    .line 1585
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public a(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 1717
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bd()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1718
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual {v1}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->getHeight()I

    move-result v3

    const/16 v4, 0x12c

    const/4 v5, -0x1

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/util/aa;->a(Landroid/widget/ListView;Landroid/view/View;IIII)V

    .line 1721
    :cond_0
    return-void
.end method

.method protected a(Lcbi;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/model/dms/m;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 1522
    iput-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->d:Z

    .line 1523
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/i;

    invoke-virtual {v0}, Lcom/twitter/app/dm/i;->a()V

    .line 1524
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bi()Z

    move-result v2

    .line 1525
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aX()J

    move-result-wide v4

    .line 1526
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->ak()Z

    move-result v0

    if-nez v0, :cond_2

    move v6, v7

    .line 1528
    :goto_0
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcbi;)V

    .line 1530
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/i;

    invoke-virtual {v0}, Lcom/twitter/app/dm/i;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1533
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/dm/e;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->D:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->q:Z

    if-nez v0, :cond_0

    .line 1535
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aR()V

    .line 1536
    iput-boolean v7, p0, Lcom/twitter/app/dm/DMConversationFragment;->D:Z

    .line 1577
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aH()Lcmt;

    move-result-object v0

    invoke-virtual {v0}, Lcmt;->h()V

    .line 1578
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->an:Z

    if-nez v0, :cond_1

    .line 1579
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->Q()V

    .line 1581
    :cond_1
    return-void

    :cond_2
    move v6, v1

    .line 1526
    goto :goto_0

    .line 1539
    :cond_3
    const-string/jumbo v0, "dm:conversation_load"

    .line 1541
    invoke-static {}, Lcom/twitter/metrics/j;->b()Lcom/twitter/metrics/j;

    move-result-object v3

    sget-object v8, Lcom/twitter/metrics/g;->m:Lcom/twitter/metrics/g$b;

    .line 1539
    invoke-static {v0, v3, v8}, Lcom/twitter/metrics/n;->b(Ljava/lang/String;Lcom/twitter/metrics/j;Lcom/twitter/metrics/g$b;)Lcom/twitter/metrics/n;

    move-result-object v0

    .line 1541
    invoke-virtual {v0}, Lcom/twitter/metrics/n;->j()V

    .line 1543
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/dm/e;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1544
    new-instance v0, Lcom/twitter/library/api/dm/o;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v8, p0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    iget-object v9, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    invoke-direct {v0, v3, v8, v9}, Lcom/twitter/library/api/dm/o;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    const/4 v3, 0x5

    invoke-virtual {p0, v0, v3, v1}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 1548
    :cond_4
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->s:Z

    if-eqz v0, :cond_6

    .line 1549
    iput-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->s:Z

    .line 1550
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->t:Z

    if-nez v0, :cond_5

    .line 1554
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->al()Lcom/twitter/app/common/list/l;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/app/common/list/l;->a:Landroid/widget/ListView;

    new-instance v1, Lcom/twitter/app/dm/DMConversationFragment$8;

    invoke-direct {v1, p0, p1}, Lcom/twitter/app/dm/DMConversationFragment$8;-><init>(Lcom/twitter/app/dm/DMConversationFragment;Lcbi;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 1573
    :cond_5
    :goto_2
    invoke-direct {p0, p1, v4, v5}, Lcom/twitter/app/dm/DMConversationFragment;->a(Lcbi;J)V

    .line 1575
    iput-boolean v7, p0, Lcom/twitter/app/dm/DMConversationFragment;->E:Z

    goto :goto_1

    .line 1567
    :cond_6
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->K:Lcom/twitter/app/dm/m;

    invoke-virtual {v0}, Lcom/twitter/app/dm/m;->d()Z

    move-result v0

    if-nez v0, :cond_7

    move v3, v7

    .line 1568
    :goto_3
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->K:Lcom/twitter/app/dm/m;

    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/m;->a(Lcbi;)V

    move-object v1, p0

    .line 1569
    invoke-direct/range {v1 .. v6}, Lcom/twitter/app/dm/DMConversationFragment;->a(ZZJZ)V

    goto :goto_2

    :cond_7
    move v3, v1

    .line 1567
    goto :goto_3
.end method

.method public a(Lcck;)V
    .locals 2

    .prologue
    .line 543
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->v:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/twitter/app/dm/DMConversationFragment;->b(Lcck;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 544
    const-string/jumbo v0, "location"

    invoke-virtual {p1}, Lcck;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 545
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->N()V

    .line 550
    :cond_0
    :goto_0
    return-void

    .line 547
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->a(Lcck;)V

    goto :goto_0
.end method

.method protected a(Lcnk;)V
    .locals 4

    .prologue
    .line 1649
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->g(Z)V

    .line 1650
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Position restoration failed due to missing item position: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p1, Lcnk;->c:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 1652
    return-void
.end method

.method public a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/media/selection/a;)V
    .locals 1

    .prologue
    .line 3131
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->m:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/media/selection/a;)V

    .line 3132
    return-void
.end method

.method public a(Lcom/twitter/android/media/selection/b;)V
    .locals 2

    .prologue
    .line 2745
    invoke-virtual {p1}, Lcom/twitter/android/media/selection/b;->c()Lcom/twitter/android/media/selection/MediaAttachment;

    move-result-object v0

    .line 2746
    if-nez v0, :cond_0

    .line 2747
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/n;->a(Z)V

    .line 2748
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bf()V

    .line 2752
    :goto_0
    return-void

    .line 2751
    :cond_0
    invoke-direct {p0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcom/twitter/android/media/selection/MediaAttachment;)V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/app/common/list/l$d;)V
    .locals 1

    .prologue
    .line 1308
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/app/common/list/l$d;)V

    .line 1309
    const v0, 0x7f0400b3

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->c(I)Lcom/twitter/app/common/list/l$d;

    .line 1310
    const v0, 0x7f040178

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->g(I)V

    .line 1311
    const v0, 0x7f0400b4

    invoke-virtual {p1, v0}, Lcom/twitter/app/common/list/l$d;->d(I)Lcom/twitter/app/common/list/l$d;

    .line 1312
    return-void
.end method

.method protected a(Lcom/twitter/library/service/s;II)V
    .locals 9

    .prologue
    const/16 v8, 0x158

    const v2, 0x7f0a027d

    const/4 v1, -0x1

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2482
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcom/twitter/library/service/s;II)V

    .line 2483
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    .line 2484
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v0}, Lcom/twitter/library/network/ab;->a(Lcom/twitter/library/service/u;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v3, v5

    .line 2486
    :goto_0
    packed-switch p2, :pswitch_data_0

    .line 2726
    :cond_0
    :goto_1
    :pswitch_0
    return-void

    :cond_1
    move v3, v4

    .line 2484
    goto :goto_0

    :pswitch_1
    move-object v1, p1

    .line 2489
    check-cast v1, Lcom/twitter/library/api/dm/SendDMRequest;

    .line 2490
    invoke-virtual {v1}, Lcom/twitter/library/api/dm/SendDMRequest;->e()Z

    move-result v6

    if-nez v6, :cond_2

    .line 2491
    invoke-virtual {v1}, Lcom/twitter/library/api/dm/SendDMRequest;->ay_()Ljava/lang/String;

    move-result-object v6

    .line 2492
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aJ()Lcom/twitter/metrics/j;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/twitter/android/metrics/c;->a(Ljava/lang/String;Lcom/twitter/metrics/j;)Lcom/twitter/android/metrics/c;

    move-result-object v6

    .line 2493
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/twitter/android/metrics/c;->a(Z)V

    .line 2496
    :cond_2
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2497
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->g()Lcom/twitter/network/l;

    move-result-object v3

    .line 2498
    if-eqz v3, :cond_5

    iget v3, v3, Lcom/twitter/network/l;->a:I

    .line 2499
    :goto_2
    packed-switch v3, :pswitch_data_1

    :pswitch_2
    goto :goto_1

    .line 2501
    :pswitch_3
    invoke-virtual {v1}, Lcom/twitter/library/api/dm/SendDMRequest;->f()Ljava/lang/String;

    move-result-object v0

    .line 2502
    if-eqz v0, :cond_3

    .line 2503
    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->b(Ljava/lang/String;)V

    .line 2505
    :cond_3
    check-cast p1, Lcom/twitter/library/api/dm/SendDMRequest;

    iget-object v1, p1, Lcom/twitter/library/api/dm/SendDMRequest;->a:Lcom/twitter/model/dms/ad;

    .line 2506
    invoke-virtual {v1}, Lcom/twitter/model/dms/ad;->g()Lcom/twitter/model/dms/s;

    move-result-object v0

    .line 2507
    if-eqz v0, :cond_4

    .line 2508
    iget-object v3, p0, Lcom/twitter/app/dm/DMConversationFragment;->Q:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/twitter/model/dms/s;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/a$c;

    iget-object v0, v0, Lcom/twitter/model/dms/a$c;->h:Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2511
    :cond_4
    iget-object v0, v1, Lcom/twitter/model/dms/ad;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2512
    invoke-virtual {v1}, Lcom/twitter/model/dms/ad;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v2

    .line 2515
    :goto_3
    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    invoke-static {v1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_5
    move v3, v4

    .line 2498
    goto :goto_2

    .line 2512
    :cond_6
    const v0, 0x7f0a027e

    goto :goto_3

    .line 2520
    :pswitch_4
    iget-boolean v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->au:Z

    if-eqz v2, :cond_0

    .line 2521
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->f()Lcom/twitter/network/HttpOperation;

    move-result-object v0

    .line 2522
    if-nez v0, :cond_7

    const/4 v0, 0x0

    .line 2524
    :goto_4
    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 2525
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    .line 2526
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->av:Landroid/os/Handler;

    new-instance v4, Lcom/twitter/app/dm/DMConversationFragment$16;

    invoke-direct {v4, p0, v1}, Lcom/twitter/app/dm/DMConversationFragment$16;-><init>(Lcom/twitter/app/dm/DMConversationFragment;Lcom/twitter/library/api/dm/SendDMRequest;)V

    const-wide/16 v6, 0x3e8

    mul-long/2addr v2, v6

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_1

    .line 2522
    :cond_7
    const-string/jumbo v2, "retry-after"

    .line 2523
    invoke-virtual {v0, v2}, Lcom/twitter/network/HttpOperation;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 2541
    :cond_8
    if-nez v3, :cond_0

    .line 2542
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 2581
    invoke-virtual {v1}, Lcom/twitter/library/api/dm/SendDMRequest;->h()J

    move-result-wide v2

    .line 2582
    invoke-virtual {v1}, Lcom/twitter/library/api/dm/SendDMRequest;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    .line 2583
    invoke-direct {p0, v2, v3, p2}, Lcom/twitter/app/dm/DMConversationFragment;->a(JI)V

    goto/16 :goto_1

    .line 2544
    :sswitch_0
    invoke-virtual {v1}, Lcom/twitter/library/api/dm/SendDMRequest;->g()Ljava/util/Set;

    move-result-object v0

    .line 2546
    const/16 v1, 0x15d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2547
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a029a

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 2548
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2549
    invoke-direct {p0, v5}, Lcom/twitter/app/dm/DMConversationFragment;->i(Z)V

    .line 2550
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aZ()V

    goto/16 :goto_1

    .line 2552
    :cond_9
    const/16 v1, 0x96

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2553
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a029b

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 2554
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2555
    invoke-direct {p0, v5}, Lcom/twitter/app/dm/DMConversationFragment;->i(Z)V

    .line 2556
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aZ()V

    goto/16 :goto_1

    .line 2558
    :cond_a
    const/16 v1, 0xe2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 2559
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a0282

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 2560
    :cond_b
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 2561
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/dialog/RateLimitDialogFragmentActivity;->a(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 2562
    :cond_c
    const/16 v1, 0x162

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2563
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a0297

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 2564
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 2566
    :cond_d
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a0293

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 2571
    :sswitch_1
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a0298

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 2572
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 2577
    :sswitch_2
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a0299

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 2593
    :pswitch_5
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez v3, :cond_0

    .line 2594
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a0540

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 2599
    :pswitch_6
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez v3, :cond_0

    .line 2600
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a028d

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 2605
    :pswitch_7
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2606
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a050f

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 2611
    :pswitch_8
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2612
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a050b

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 2617
    :pswitch_9
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aj()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 2618
    invoke-direct {p0, v4}, Lcom/twitter/app/dm/DMConversationFragment;->h(Z)V

    .line 2621
    :cond_e
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 2622
    check-cast p1, Lcom/twitter/library/api/dm/e;

    .line 2623
    invoke-virtual {p1}, Lcom/twitter/library/api/dm/e;->e()Z

    move-result v0

    if-nez v0, :cond_f

    .line 2624
    iput-boolean v5, p0, Lcom/twitter/app/dm/DMConversationFragment;->ab:Z

    .line 2637
    :cond_f
    :goto_5
    iput-boolean v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->q:Z

    goto/16 :goto_1

    .line 2627
    :cond_10
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    const/16 v1, 0x191

    if-ne v0, v1, :cond_11

    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->F:Z

    if-eqz v0, :cond_11

    .line 2630
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a0288

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2631
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bl()V

    .line 2634
    :cond_11
    iput-boolean v5, p0, Lcom/twitter/app/dm/DMConversationFragment;->ab:Z

    goto :goto_5

    .line 2641
    :pswitch_a
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2642
    invoke-direct {p0, v5}, Lcom/twitter/app/dm/DMConversationFragment;->f(Z)V

    .line 2643
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a02bc

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 2648
    :pswitch_b
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2649
    invoke-direct {p0, v4}, Lcom/twitter/app/dm/DMConversationFragment;->f(Z)V

    .line 2650
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a02bb

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 2655
    :pswitch_c
    check-cast p1, Lcom/twitter/library/api/dm/b;

    .line 2658
    iget-object v5, p0, Lcom/twitter/app/dm/DMConversationFragment;->k:Landroid/app/ProgressDialog;

    if-eqz v5, :cond_12

    .line 2659
    iget-object v5, p0, Lcom/twitter/app/dm/DMConversationFragment;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/ProgressDialog;->hide()V

    .line 2663
    :cond_12
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v5

    if-eqz v5, :cond_15

    .line 2664
    iget-object v0, p1, Lcom/twitter/library/api/dm/b;->a:Lcom/twitter/model/dms/b;

    .line 2666
    iget-object v3, v0, Lcom/twitter/model/dms/b;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_14

    move v2, v1

    .line 2696
    :cond_13
    :goto_6
    if-eq v2, v1, :cond_0

    .line 2697
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    invoke-static {v0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 2669
    :cond_14
    invoke-virtual {v0}, Lcom/twitter/model/dms/b;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_13

    .line 2672
    const v2, 0x7f0a027e

    goto :goto_6

    .line 2676
    :cond_15
    if-eqz v3, :cond_16

    move v2, v1

    .line 2677
    goto :goto_6

    .line 2678
    :cond_16
    const/16 v3, 0x193

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v5

    if-ne v3, v5, :cond_17

    .line 2679
    invoke-virtual {p1}, Lcom/twitter/library/api/dm/b;->g()[I

    move-result-object v0

    .line 2680
    invoke-static {v0, v8}, Lcom/twitter/util/collection/CollectionUtils;->a([II)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 2682
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/dialog/RateLimitDialogFragmentActivity;->a(Landroid/content/Context;)V

    move v2, v1

    goto :goto_6

    .line 2686
    :cond_17
    const/16 v3, 0x1a4

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v5

    if-eq v3, v5, :cond_18

    const/16 v3, 0x1ad

    .line 2687
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->d()I

    move-result v0

    if-ne v3, v0, :cond_13

    .line 2689
    :cond_18
    const v2, 0x7f0a027f

    goto :goto_6

    .line 2702
    :pswitch_d
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2703
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v5, v1, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto/16 :goto_1

    .line 2708
    :pswitch_e
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2710
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aV()V

    .line 2711
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    const v1, 0x7f0a0614

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 2712
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 2717
    :pswitch_f
    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_1

    .line 2486
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_1
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_d
        :pswitch_0
        :pswitch_6
        :pswitch_e
        :pswitch_f
    .end packed-switch

    .line 2499
    :pswitch_data_1
    .packed-switch 0xc8
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch

    .line 2542
    :sswitch_data_0
    .sparse-switch
        0x193 -> :sswitch_0
        0x194 -> :sswitch_1
        0x1a4 -> :sswitch_2
        0x1ad -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Ljava/lang/String;Lccp;)V
    .locals 3

    .prologue
    .line 513
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->az:Z

    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 514
    new-instance v0, Lccq$a;

    invoke-direct {v0}, Lccq$a;-><init>()V

    .line 515
    invoke-virtual {v0, p1}, Lccq$a;->a(Ljava/lang/String;)Lccl$a;

    move-result-object v0

    check-cast v0, Lccq$a;

    .line 516
    invoke-virtual {v0, p2}, Lccq$a;->a(Lccp;)Lccq$a;

    move-result-object v0

    .line 517
    invoke-virtual {v0}, Lccq$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccq;

    .line 519
    invoke-virtual {v0}, Lccq;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/twitter/app/dm/DMConversationFragment;->a(Ljava/lang/String;Lccl;Lcdu;)V

    .line 520
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 525
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aA:Z

    invoke-static {v0}, Lcom/twitter/util/f;->b(Z)Z

    .line 526
    new-instance v0, Lcct$a;

    invoke-direct {v0}, Lcct$a;-><init>()V

    .line 527
    invoke-virtual {v0, p1}, Lcct$a;->a(Ljava/lang/String;)Lccl$a;

    move-result-object v0

    check-cast v0, Lcct$a;

    .line 528
    invoke-virtual {v0, p2}, Lcct$a;->b(Ljava/lang/String;)Lcct$a;

    move-result-object v0

    .line 529
    invoke-virtual {v0}, Lcct$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcct;

    .line 531
    invoke-virtual {v0}, Lcct;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/twitter/app/dm/DMConversationFragment;->a(Ljava/lang/String;Lccl;Lcdu;)V

    .line 532
    return-void
.end method

.method protected a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1084
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Z)V

    .line 1085
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    .line 1086
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 1087
    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->ai:Z

    if-eqz v1, :cond_0

    .line 1088
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 1090
    :cond_0
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->aU()V

    .line 1091
    return-void
.end method

.method public a(ZJLjava/lang/String;Lcgi;)V
    .locals 4

    .prologue
    .line 2097
    if-nez p1, :cond_0

    .line 2098
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:view_participants:user_list:user:follow"

    aput-object v3, v1, v2

    .line 2099
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 2098
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 2101
    :cond_0
    return-void
.end method

.method public a(Lcmm;)Z
    .locals 8

    .prologue
    const/16 v7, 0x9

    const/4 v6, 0x0

    const/4 v2, 0x1

    .line 1861
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    .line 1862
    invoke-interface {p1}, Lcmm;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1997
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcmm;)Z

    move-result v0

    :goto_0
    return v0

    .line 1864
    :pswitch_0
    const/16 v0, 0x301

    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->x:Z

    iget-object v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    const-string/jumbo v5, "thread"

    invoke-static {v0, v1, v4, v5}, Lcom/twitter/app/dm/dialog/DeleteConversationDialog;->a(IZLjava/lang/String;Ljava/lang/String;)Lcom/twitter/app/dm/dialog/DeleteConversationDialog;

    move-result-object v0

    .line 1866
    invoke-virtual {v0, p0}, Lcom/twitter/app/dm/dialog/DeleteConversationDialog;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 1867
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    move v0, v2

    .line 1868
    goto :goto_0

    .line 1871
    :pswitch_1
    instance-of v0, p1, Lazv;

    if-eqz v0, :cond_1

    check-cast p1, Lazv;

    invoke-virtual {p1}, Lazv;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1872
    invoke-static {}, Lcom/twitter/library/dm/d;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1873
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    const-string/jumbo v1, "thread"

    invoke-static {v7, v0, v1}, Lcom/twitter/app/dm/dialog/MuteConversationDialog;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/twitter/app/dm/dialog/MuteConversationDialog;

    move-result-object v0

    .line 1874
    invoke-virtual {v0, p0}, Lcom/twitter/app/dm/dialog/MuteConversationDialog;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 1875
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    :goto_1
    move v0, v2

    .line 1888
    goto :goto_0

    .line 1877
    :cond_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v2, [Ljava/lang/String;

    const-string/jumbo v4, "messages:thread::thread:mute_dm_thread"

    aput-object v4, v1, v6

    .line 1878
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1877
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1879
    new-instance v0, Lcom/twitter/library/api/dm/u;

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    iget-object v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    invoke-direct {v0, v3, v1, v4, v2}, Lcom/twitter/library/api/dm/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Z)V

    invoke-virtual {p0, v0, v7, v6}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto :goto_1

    .line 1883
    :cond_1
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v2, [Ljava/lang/String;

    const-string/jumbo v4, "messages:thread::thread:unmute_dm_thread"

    aput-object v4, v1, v6

    .line 1884
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1883
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1885
    new-instance v0, Lcom/twitter/library/api/dm/u;

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    iget-object v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    invoke-direct {v0, v3, v1, v4, v6}, Lcom/twitter/library/api/dm/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Z)V

    const/16 v1, 0x8

    invoke-virtual {p0, v0, v1, v6}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcom/twitter/library/service/s;II)Z

    goto :goto_1

    .line 1891
    :pswitch_2
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v2, [Ljava/lang/String;

    const-string/jumbo v4, "messages:thread::thread:edit_name"

    aput-object v4, v1, v6

    .line 1892
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1891
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1893
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->A:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;

    move-result-object v0

    .line 1894
    invoke-virtual {v0, p0}, Lcom/twitter/app/dm/dialog/UpdateConversationNameDialog;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 1895
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    move v0, v2

    .line 1896
    goto/16 :goto_0

    .line 1899
    :pswitch_3
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v2, [Ljava/lang/String;

    const-string/jumbo v4, "messages:thread::thread:edit_photo"

    aput-object v4, v1, v6

    .line 1900
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1899
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1901
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1902
    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->B:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    .line 1904
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v4

    .line 1905
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v5

    .line 1907
    if-eqz v1, :cond_2

    .line 1908
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 1909
    const v6, 0x7f0a0a24

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 1912
    :cond_2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 1913
    const v6, 0x7f0a00bb

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 1915
    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 1916
    const v6, 0x7f0a0696

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 1918
    if-eqz v1, :cond_3

    .line 1919
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 1920
    const v1, 0x7f0a075a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 1923
    :cond_3
    invoke-virtual {v5}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1925
    new-instance v5, Lcom/twitter/android/widget/aj$b;

    const/16 v1, 0x30b

    invoke-direct {v5, v1}, Lcom/twitter/android/widget/aj$b;-><init>(I)V

    .line 1927
    invoke-virtual {v4}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v4}, Lcom/twitter/util/collection/h;->i()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/CharSequence;

    invoke-virtual {v5, v1}, Lcom/twitter/android/widget/aj$b;->a([Ljava/lang/CharSequence;)Lcom/twitter/android/widget/aj$a;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/aj$b;

    .line 1928
    invoke-virtual {v1}, Lcom/twitter/android/widget/aj$b;->i()Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v1

    .line 1929
    invoke-virtual {v1, p0}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v1

    new-instance v4, Lcom/twitter/app/dm/DMConversationFragment$14;

    invoke-direct {v4, p0, v0, v3}, Lcom/twitter/app/dm/DMConversationFragment$14;-><init>(Lcom/twitter/app/dm/DMConversationFragment;Ljava/util/List;Landroid/support/v4/app/FragmentActivity;)V

    .line 1930
    invoke-virtual {v1, v4}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Lcom/twitter/app/common/dialog/b$d;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PromptDialogFragment;

    .line 1961
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    move v0, v2

    .line 1962
    goto/16 :goto_0

    .line 1965
    :pswitch_4
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v2, [Ljava/lang/String;

    const-string/jumbo v3, "messages:thread::thread:view_participants"

    aput-object v3, v1, v6

    .line 1966
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1965
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1967
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->u()V

    .line 1968
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aH:Lcom/twitter/app/users/sheet/UsersBottomSheet;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v3, "TAG_USERS_BOTTOM_SHEET"

    invoke-virtual {v0, v1, v3}, Lcom/twitter/app/users/sheet/UsersBottomSheet;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    move v0, v2

    .line 1969
    goto/16 :goto_0

    .line 1972
    :pswitch_5
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->p:[J

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->p:[J

    array-length v0, v0

    invoke-static {}, Lcom/twitter/library/dm/d;->k()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 1973
    :cond_4
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    invoke-direct {v0, v4, v5}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v2, [Ljava/lang/String;

    const-string/jumbo v4, "messages:thread::thread:add_participants"

    aput-object v4, v1, v6

    .line 1974
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 1973
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 1975
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/app/dm/DMAddParticipantsActivity;

    invoke-direct {v0, v3, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "preselected_items"

    iget-object v3, p0, Lcom/twitter/app/dm/DMConversationFragment;->p:[J

    .line 1976
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    move-result-object v0

    .line 1975
    invoke-virtual {p0, v0, v2}, Lcom/twitter/app/dm/DMConversationFragment;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_2
    move v0, v2

    .line 1983
    goto/16 :goto_0

    .line 1981
    :cond_5
    const v0, 0x7f0a02f4

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_2

    .line 1986
    :pswitch_6
    const/16 v0, 0x309

    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->x:Z

    iget-object v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    const-string/jumbo v5, "thread"

    invoke-static {v0, v1, v4, v5, p0}, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->a(IZLjava/lang/String;Ljava/lang/String;Lcom/twitter/app/dm/dialog/ReportConversationDialog$a;)Lcom/twitter/app/dm/dialog/ReportConversationDialog;

    move-result-object v0

    .line 1988
    invoke-virtual {v0, p0}, Lcom/twitter/app/dm/dialog/ReportConversationDialog;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 1989
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    move v0, v2

    .line 1990
    goto/16 :goto_0

    .line 1993
    :pswitch_7
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->N()V

    move v0, v2

    .line 1994
    goto/16 :goto_0

    .line 1862
    nop

    :pswitch_data_0
    .packed-switch 0x7f130891
        :pswitch_5
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public a(Lcmr;)Z
    .locals 1

    .prologue
    .line 1810
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->a(Lcmr;)Z

    .line 1811
    const v0, 0x7f14000e

    invoke-interface {p1, v0}, Lcmr;->a(I)V

    .line 1812
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lcom/twitter/android/media/selection/MediaAttachment;)Z
    .locals 2

    .prologue
    .line 2739
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {p1}, Lcom/twitter/android/media/selection/MediaAttachment;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/dm/n;->d(Landroid/net/Uri;)V

    .line 2740
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lcom/twitter/library/platform/notifications/u;)Z
    .locals 4

    .prologue
    .line 2836
    iget-wide v0, p1, Lcom/twitter/library/platform/notifications/u;->c:J

    iget-wide v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->ac()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/library/platform/notifications/u;->g:Lcom/twitter/library/platform/notifications/r;

    iget-object v1, v1, Lcom/twitter/library/platform/notifications/r;->q:Ljava/lang/String;

    .line 2837
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2836
    :goto_0
    return v0

    .line 2837
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/dms/e;)Z
    .locals 2

    .prologue
    .line 2350
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->Y()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2351
    const/4 v0, 0x0

    .line 2358
    :goto_0
    return v0

    .line 2354
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/model/dms/e;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x307

    .line 2355
    :goto_1
    invoke-static {v0, p1, p0}, Lcom/twitter/app/dm/dialog/DMMessageDialog;->a(ILcom/twitter/model/dms/e;Lcom/twitter/app/dm/dialog/a;)Lcom/twitter/app/dm/dialog/DMMessageDialog;

    move-result-object v0

    .line 2356
    invoke-virtual {v0, p0}, Lcom/twitter/app/dm/dialog/DMMessageDialog;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 2357
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 2358
    const/4 v0, 0x1

    goto :goto_0

    .line 2354
    :cond_1
    const/16 v0, 0x304

    goto :goto_1
.end method

.method public a_(J)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 2387
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "messages:thread:rtf_message::report_as_spam"

    aput-object v2, v1, v7

    .line 2388
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 2387
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 2389
    new-instance v1, Lcom/twitter/library/api/dm/ReportDMRequest;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    sget-object v6, Lcom/twitter/library/api/dm/ReportDMRequest$Type;->a:Lcom/twitter/library/api/dm/ReportDMRequest$Type;

    move-wide v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/api/dm/ReportDMRequest;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcom/twitter/library/api/dm/ReportDMRequest$Type;)V

    const/4 v0, 0x3

    invoke-virtual {p0, v1, v0, v7}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 2391
    return-void
.end method

.method public b(Lcmr;)I
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1818
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->b(Lcmr;)I

    .line 1820
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bd()Z

    move-result v2

    .line 1822
    invoke-interface {p1}, Lcmr;->k()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    .line 1823
    const v1, 0x7f130896

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v5

    .line 1824
    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->x:Z

    if-eqz v1, :cond_0

    const v1, 0x7f0a054b

    .line 1826
    :goto_0
    invoke-virtual {v5, v2}, Lazv;->b(Z)Lazv;

    .line 1827
    invoke-virtual {v5, v1}, Lazv;->d(I)Lazv;

    .line 1829
    iget-boolean v5, p0, Lcom/twitter/app/dm/DMConversationFragment;->x:Z

    .line 1830
    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/library/dm/e;->c(Ljava/lang/String;)Z

    move-result v6

    .line 1832
    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->w:Z

    if-eqz v1, :cond_1

    const v1, 0x7f0a02f6

    .line 1835
    :goto_1
    const v2, 0x7f130891

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v7

    iget-boolean v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->y:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->p:[J

    if-eqz v2, :cond_2

    if-nez v6, :cond_2

    move v2, v3

    .line 1836
    :goto_2
    invoke-virtual {v7, v2}, Lazv;->b(Z)Lazv;

    .line 1838
    const v2, 0x7f130892

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v7

    if-eqz v5, :cond_3

    iget-boolean v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->C:Z

    if-eqz v2, :cond_3

    if-nez v6, :cond_3

    move v2, v3

    .line 1839
    :goto_3
    invoke-virtual {v7, v2}, Lazv;->b(Z)Lazv;

    .line 1841
    const v2, 0x7f130893

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v7

    if-eqz v5, :cond_4

    if-nez v6, :cond_4

    move v2, v3

    .line 1842
    :goto_4
    invoke-virtual {v7, v2}, Lazv;->b(Z)Lazv;

    .line 1843
    const v2, 0x7f130894

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v7

    if-eqz v5, :cond_5

    if-nez v6, :cond_5

    move v2, v3

    .line 1844
    :goto_5
    invoke-virtual {v7, v2}, Lazv;->b(Z)Lazv;

    .line 1845
    const v2, 0x7f130895

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v5

    if-nez v6, :cond_6

    move v2, v3

    .line 1846
    :goto_6
    invoke-virtual {v5, v2}, Lazv;->b(Z)Lazv;

    move-result-object v2

    .line 1847
    invoke-virtual {v2, v1}, Lazv;->d(I)Lazv;

    move-result-object v1

    iget-boolean v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->w:Z

    .line 1848
    invoke-virtual {v1, v2}, Lazv;->d(Z)V

    .line 1849
    const v1, 0x7f130898

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v1

    .line 1850
    invoke-static {}, Lcom/twitter/library/dm/d;->y()Z

    move-result v2

    invoke-virtual {v1, v2}, Lazv;->b(Z)Lazv;

    .line 1852
    const v1, 0x7f130897

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lazv;

    move-result-object v0

    if-nez v6, :cond_7

    :goto_7
    invoke-virtual {v0, v3}, Lazv;->b(Z)Lazv;

    .line 1853
    const/4 v0, 0x2

    return v0

    .line 1824
    :cond_0
    const v1, 0x7f0a0549

    goto/16 :goto_0

    .line 1832
    :cond_1
    const v1, 0x7f0a02f7

    goto/16 :goto_1

    :cond_2
    move v2, v4

    .line 1835
    goto :goto_2

    :cond_3
    move v2, v4

    .line 1838
    goto :goto_3

    :cond_4
    move v2, v4

    .line 1841
    goto :goto_4

    :cond_5
    move v2, v4

    .line 1843
    goto :goto_5

    :cond_6
    move v2, v4

    .line 1845
    goto :goto_6

    :cond_7
    move v3, v4

    .line 1852
    goto :goto_7
.end method

.method public b()V
    .locals 1

    .prologue
    .line 1461
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->b()V

    .line 1463
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->N:Lcom/twitter/library/client/o;

    invoke-virtual {v0}, Lcom/twitter/library/client/o;->a()V

    .line 1464
    return-void
.end method

.method public b(I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 3162
    new-instance v0, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    const v1, 0x7f0a03c9

    .line 3163
    invoke-virtual {p0, v1}, Lcom/twitter/app/dm/DMConversationFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v5, v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;-><init>(Ljava/lang/String;Landroid/content/Context;[Ljava/lang/String;)V

    const-string/jumbo v1, "messages:thread:dm_compose_bar:media"

    .line 3165
    invoke-virtual {v0, v1}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->f(Ljava/lang/String;)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    move-result-object v0

    .line 3166
    invoke-virtual {v0, v6}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->b(Z)Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;

    move-result-object v0

    .line 3167
    invoke-virtual {v0}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity$a;->a()Landroid/content/Intent;

    move-result-object v0

    .line 3162
    invoke-virtual {p0, v0, p1}, Lcom/twitter/app/dm/DMConversationFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3169
    return-void
.end method

.method public b(J)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 2396
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "messages:thread:rtf_message::report_as_ok"

    aput-object v2, v1, v7

    .line 2397
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 2396
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 2398
    new-instance v1, Lcom/twitter/library/api/dm/ReportDMRequest;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    sget-object v6, Lcom/twitter/library/api/dm/ReportDMRequest$Type;->c:Lcom/twitter/library/api/dm/ReportDMRequest$Type;

    move-wide v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/api/dm/ReportDMRequest;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLcom/twitter/library/api/dm/ReportDMRequest$Type;)V

    const/16 v0, 0xb

    invoke-virtual {p0, v1, v0, v7}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 2400
    return-void
.end method

.method public b(Landroid/net/Uri;Lcom/twitter/android/media/selection/a;)V
    .locals 1

    .prologue
    .line 3141
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->m:Lcom/twitter/android/media/selection/c;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/media/selection/c;->a(Landroid/net/Uri;Lcom/twitter/android/media/selection/a;)V

    .line 3142
    return-void
.end method

.method public b(Lcom/twitter/android/media/selection/MediaAttachment;)V
    .locals 2

    .prologue
    .line 3074
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->a(Lcom/twitter/android/media/selection/MediaAttachment;)Lcom/twitter/android/media/widget/AttachmentMediaView;

    move-result-object v0

    .line 3075
    if-eqz v0, :cond_0

    .line 3076
    new-instance v1, Lcom/twitter/app/dm/DMConversationFragment$18;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/DMConversationFragment$18;-><init>(Lcom/twitter/app/dm/DMConversationFragment;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/media/widget/AttachmentMediaView;->setOnAttachmentActionListener(Lcom/twitter/android/media/widget/AttachmentMediaView$a;)V

    .line 3123
    :cond_0
    if-eqz p1, :cond_1

    .line 3124
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->r()V

    .line 3126
    :cond_1
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1294
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1295
    iput-object p1, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    .line 1298
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->an:Z

    if-nez v0, :cond_0

    .line 1299
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->Q()V

    .line 1301
    :cond_0
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->ba()V

    .line 1302
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->a(Z)V

    .line 1304
    :cond_1
    return-void
.end method

.method public b(Lcck;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 559
    if-nez p1, :cond_0

    .line 575
    :goto_0
    return v0

    .line 563
    :cond_0
    invoke-virtual {p1}, Lcck;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v1, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 565
    :pswitch_0
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->az:Z

    goto :goto_0

    .line 563
    :sswitch_0
    const-string/jumbo v3, "options"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v1, v0

    goto :goto_1

    :sswitch_1
    const-string/jumbo v3, "text_input"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_2
    const-string/jumbo v3, "location"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x2

    goto :goto_1

    :sswitch_3
    const-string/jumbo v3, "unknown"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x3

    goto :goto_1

    .line 568
    :pswitch_1
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aA:Z

    goto :goto_0

    .line 571
    :pswitch_2
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aB:Z

    goto :goto_0

    .line 563
    nop

    :sswitch_data_0
    .sparse-switch
        -0x7dc155c8 -> :sswitch_1
        -0x4a797962 -> :sswitch_0
        -0x10fa53b6 -> :sswitch_3
        0x714f9fb5 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public b(Lcom/twitter/model/dms/e;)Z
    .locals 2

    .prologue
    .line 2363
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->Y()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2364
    const/4 v0, 0x0

    .line 2371
    :goto_0
    return v0

    .line 2367
    :cond_0
    invoke-virtual {p1}, Lcom/twitter/model/dms/e;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x306

    .line 2368
    :goto_1
    invoke-static {v0, p1, p0}, Lcom/twitter/app/dm/dialog/DMTweetDialog;->a(ILcom/twitter/model/dms/e;Lcom/twitter/app/dm/dialog/a;)Lcom/twitter/app/dm/dialog/DMTweetDialog;

    move-result-object v0

    .line 2369
    invoke-virtual {v0, p0}, Lcom/twitter/app/dm/dialog/DMTweetDialog;->a(Landroid/support/v4/app/Fragment;)Lcom/twitter/app/common/dialog/BaseDialogFragment;

    move-result-object v0

    .line 2370
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/app/common/dialog/BaseDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 2371
    const/4 v0, 0x1

    goto :goto_0

    .line 2367
    :cond_1
    const/16 v0, 0x305

    goto :goto_1
.end method

.method public c(J)V
    .locals 1

    .prologue
    .line 2887
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/app/dm/DMConversationFragment;->a(JZ)V

    .line 2888
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2105
    invoke-direct {p0, p1, v0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->a(Ljava/lang/String;Lccl;Lcdu;)V

    .line 2106
    return-void
.end method

.method public d()Lrn;
    .locals 1

    .prologue
    .line 2829
    sget-object v0, Lrm;->a:Lrm;

    return-object v0
.end method

.method public d(J)V
    .locals 1

    .prologue
    .line 2892
    invoke-direct {p0, p1, p2}, Lcom/twitter/app/dm/DMConversationFragment;->h(J)V

    .line 2893
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2903
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->u:Lcom/twitter/app/dm/t;

    invoke-virtual {v0, p1}, Lcom/twitter/app/dm/t;->b(Ljava/lang/String;)V

    .line 2904
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->K:Lcom/twitter/app/dm/m;

    invoke-virtual {v0}, Lcom/twitter/app/dm/m;->e()V

    .line 2905
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 507
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->H:Z

    .line 508
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 536
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 537
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->s()V

    .line 539
    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 24

    .prologue
    .line 771
    invoke-super/range {p0 .. p1}, Lcom/twitter/app/common/list/TwitterListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 773
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    .line 774
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v22

    .line 775
    if-nez v22, :cond_0

    .line 778
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/dm/DMConversationFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0a0289

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 779
    invoke-direct/range {p0 .. p0}, Lcom/twitter/app/dm/DMConversationFragment;->bl()V

    .line 972
    :goto_0
    return-void

    .line 796
    :cond_0
    if-eqz p1, :cond_a

    const/4 v4, 0x1

    :goto_1
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->L:Z

    .line 798
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/dm/DMConversationFragment;->C()Lcom/twitter/app/dm/j;

    move-result-object v23

    .line 799
    invoke-virtual/range {v23 .. v23}, Lcom/twitter/app/dm/j;->B()Z

    move-result v4

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->ae:Z

    .line 800
    invoke-virtual/range {v23 .. v23}, Lcom/twitter/app/dm/j;->a()Z

    move-result v4

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->F:Z

    .line 802
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->L:Z

    if-eqz v4, :cond_c

    .line 804
    invoke-static/range {p1 .. p1}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/Bundle;

    invoke-static {v4}, Lcom/twitter/app/common/list/i;->c(Landroid/os/Bundle;)Lcom/twitter/app/common/list/i;

    move-result-object v18

    .line 805
    const-string/jumbo v4, "conversation_id"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/twitter/app/common/list/i;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 806
    const-string/jumbo v4, "title"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/twitter/app/common/list/i;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 807
    const-string/jumbo v4, "subtitle"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/twitter/app/common/list/i;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 808
    const-string/jumbo v4, "text"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/twitter/app/common/list/i;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 809
    const-string/jumbo v4, "media_uri"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/twitter/app/common/list/i;->h(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    .line 810
    const-string/jumbo v6, "media_attachment"

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Lcom/twitter/app/common/list/i;->h(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/twitter/android/media/selection/MediaAttachment;

    .line 811
    const-string/jumbo v7, "has_sent_message"

    const/4 v8, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7, v8}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;Z)Z

    move-result v7

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/twitter/app/dm/DMConversationFragment;->af:Z

    .line 812
    const-string/jumbo v7, "should_show_verified_badge"

    const/4 v8, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7, v8}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;Z)Z

    move-result v7

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/twitter/app/dm/DMConversationFragment;->ah:Z

    .line 813
    const-string/jumbo v7, "has_loaded_entries"

    const/4 v8, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7, v8}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;Z)Z

    move-result v7

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/twitter/app/dm/DMConversationFragment;->E:Z

    .line 814
    const-string/jumbo v7, "canceled_pending_attachments"

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Lcom/twitter/app/common/list/i;->g(Ljava/lang/String;)Ljava/util/List;

    move-result-object v13

    .line 815
    const/4 v12, 0x0

    .line 816
    const-string/jumbo v7, "quick_emoji_visible"

    const/4 v8, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7, v8}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;Z)Z

    move-result v11

    .line 818
    const-string/jumbo v7, "error_dialog"

    const/4 v8, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v7, v8}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;Z)Z

    move-result v7

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/twitter/app/dm/DMConversationFragment;->i:Z

    .line 819
    new-instance v7, Lcom/twitter/app/dm/m;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    move-object/from16 v0, v18

    invoke-direct {v7, v8, v9, v0}, Lcom/twitter/app/dm/m;-><init>(JLcom/twitter/app/common/list/i;)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/twitter/app/dm/DMConversationFragment;->K:Lcom/twitter/app/dm/m;

    .line 820
    const-string/jumbo v7, "has_scrolled_to_last_read_marker"

    const/4 v8, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7, v8}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;Z)Z

    move-result v7

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/twitter/app/dm/DMConversationFragment;->M:Z

    .line 821
    const-string/jumbo v7, "report_entry_id"

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Lcom/twitter/app/common/list/i;->c(Ljava/lang/String;)J

    move-result-wide v8

    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/twitter/app/dm/DMConversationFragment;->b:J

    .line 822
    const-string/jumbo v7, "is_group_convo"

    const/4 v8, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7, v8}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;Z)Z

    move-result v7

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/twitter/app/dm/DMConversationFragment;->j(Z)V

    .line 823
    const-string/jumbo v7, "has_requested_older_messages"

    const/4 v8, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7, v8}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;Z)Z

    move-result v7

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/twitter/app/dm/DMConversationFragment;->r:Z

    .line 824
    const-string/jumbo v7, "has_user_scrolled"

    const/4 v8, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7, v8}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;Z)Z

    move-result v7

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/twitter/app/dm/DMConversationFragment;->t:Z

    .line 825
    const-string/jumbo v7, "in_flight_message_request_ids"

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Lcom/twitter/app/common/list/i;->i(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    check-cast v7, Ljava/util/Set;

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/twitter/app/dm/DMConversationFragment;->Q:Ljava/util/Set;

    .line 826
    const-string/jumbo v7, "read_only"

    const/4 v8, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7, v8}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;Z)Z

    move-result v7

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/twitter/app/dm/DMConversationFragment;->y:Z

    .line 827
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/app/dm/DMConversationFragment;->aw:Lcom/twitter/app/dm/x;

    if-eqz v7, :cond_b

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/app/dm/DMConversationFragment;->aw:Lcom/twitter/app/dm/x;

    .line 828
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/twitter/app/dm/x;->a(Landroid/os/Bundle;)Z

    move-result v7

    if-eqz v7, :cond_b

    const/4 v7, 0x1

    move v8, v7

    .line 829
    :goto_2
    const-string/jumbo v7, "sticker_sheet_state"

    const/4 v9, 0x5

    move-object/from16 v0, v18

    invoke-virtual {v0, v7, v9}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;I)I

    move-result v10

    .line 830
    const-string/jumbo v7, "sticker_tab_position"

    const/4 v9, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7, v9}, Lcom/twitter/app/common/list/i;->a(Ljava/lang/String;I)I

    move-result v9

    .line 832
    const-string/jumbo v7, "typing_data"

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Lcom/twitter/app/common/list/i;->i(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    check-cast v7, [B

    .line 833
    if-eqz v7, :cond_1

    .line 834
    sget-object v18, Lcom/twitter/app/dm/ag$a;->a:Lcom/twitter/app/dm/ag$b;

    move-object/from16 v0, v18

    invoke-static {v7, v0}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/twitter/app/dm/ag$a;

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/twitter/app/dm/DMConversationFragment;->ap:Lcom/twitter/app/dm/ag$a;

    :cond_1
    move-object/from16 v18, v13

    move-object/from16 v19, v4

    move-object/from16 v20, v6

    move-object/from16 v21, v15

    move-object/from16 v7, v17

    move v13, v9

    move v15, v8

    move-object v4, v14

    move-object/from16 v6, v16

    move/from16 v17, v12

    move v14, v10

    move/from16 v16, v11

    .line 874
    :goto_3
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    move-object v4, v5

    .line 876
    check-cast v4, Lcom/twitter/app/common/base/TwitterFragmentActivity;

    invoke-virtual {v4}, Lcom/twitter/app/common/base/TwitterFragmentActivity;->E()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v4

    .line 877
    invoke-static {v4}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/internal/android/widget/ToolBar;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/app/dm/DMConversationFragment;->aD:Landroid/view/View;

    invoke-virtual {v4, v8}, Lcom/twitter/internal/android/widget/ToolBar;->setCustomView(Landroid/view/View;)V

    .line 878
    const/4 v4, 0x0

    invoke-virtual {v5, v4}, Landroid/support/v4/app/FragmentActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 879
    invoke-static {v7}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v8

    .line 880
    if-eqz v8, :cond_2

    const v4, 0x7f0a04b6

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/twitter/app/dm/DMConversationFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    :cond_2
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/twitter/app/dm/DMConversationFragment;->z:Ljava/lang/String;

    .line 881
    if-nez v8, :cond_f

    const/4 v4, 0x1

    :goto_4
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/twitter/app/dm/DMConversationFragment;->b(Z)V

    .line 882
    if-nez v8, :cond_10

    const/4 v4, 0x1

    :goto_5
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/twitter/app/dm/DMConversationFragment;->c(Z)V

    .line 883
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/twitter/app/dm/DMConversationFragment;->f(Ljava/lang/String;)V

    .line 885
    invoke-super/range {p0 .. p0}, Lcom/twitter/app/common/list/TwitterListFragment;->p()V

    .line 886
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/dm/DMConversationFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v4

    .line 887
    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v4, v6, v7, v0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 888
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/twitter/app/dm/DMConversationFragment;->ai:Z

    if-eqz v6, :cond_3

    .line 889
    const/4 v6, 0x3

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v4, v6, v7, v0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 891
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/twitter/app/dm/DMConversationFragment;->aU()V

    .line 893
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/twitter/app/dm/DMConversationFragment;->a(Landroid/os/Bundle;Lcom/twitter/model/account/UserSettings;)V

    .line 895
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->ay:Z

    if-eqz v4, :cond_4

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->y:Z

    if-nez v4, :cond_4

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->x:Z

    if-nez v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    .line 896
    invoke-static {v4}, Lcom/twitter/library/dm/e;->c(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    invoke-static {v4}, Lcom/twitter/library/dm/e;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 897
    new-instance v4, Lcom/twitter/library/api/dm/k;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    .line 898
    invoke-virtual/range {v23 .. v23}, Lcom/twitter/app/dm/j;->z()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v6, v7, v8, v9}, Lcom/twitter/library/api/dm/k;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;)V

    .line 899
    const/16 v6, 0x12

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6, v7}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 902
    :cond_4
    new-instance v4, Lcom/twitter/app/dm/DMConversationFragment$c;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/twitter/app/dm/DMConversationFragment$c;-><init>(Lcom/twitter/app/dm/DMConversationFragment;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->h:Lcom/twitter/app/dm/DMConversationFragment$c;

    .line 903
    new-instance v4, Lcom/twitter/android/media/selection/c;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/app/dm/DMConversationFragment;->h:Lcom/twitter/app/dm/DMConversationFragment$c;

    const-string/jumbo v7, "dm_group_avatar_composition"

    sget-object v8, Lcom/twitter/media/model/MediaType;->g:Ljava/util/EnumSet;

    const/4 v9, 0x1

    sget-object v10, Lcom/twitter/android/composer/ComposerType;->c:Lcom/twitter/android/composer/ComposerType;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    move-object/from16 v12, p0

    invoke-direct/range {v4 .. v12}, Lcom/twitter/android/media/selection/c;-><init>(Landroid/content/Context;Lbrp;Ljava/lang/String;Ljava/util/EnumSet;ILcom/twitter/android/composer/ComposerType;Lcom/twitter/library/client/Session;Lcom/twitter/app/common/util/k;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->g:Lcom/twitter/android/media/selection/c;

    .line 912
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/dm/DMConversationFragment;->getView()Landroid/view/View;

    move-result-object v4

    const v5, 0x7f1302ec

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    .line 913
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/dm/DMConversationFragment;->aF()Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->setAssociation(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 914
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->setListener(Lcom/twitter/app/dm/widget/DMMessageComposer$a;)V

    .line 915
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->am:Z

    if-eqz v4, :cond_5

    .line 916
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->setTypingEventProducer(Lcom/twitter/app/dm/ah;)V

    .line 919
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/twitter/app/dm/DMConversationFragment;->be()V

    .line 921
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->y:Z

    if-eqz v4, :cond_6

    .line 922
    invoke-direct/range {p0 .. p0}, Lcom/twitter/app/dm/DMConversationFragment;->aZ()V

    .line 925
    :cond_6
    if-eqz v15, :cond_7

    .line 926
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/dm/DMConversationFragment;->az()Lcjr;

    move-result-object v4

    check-cast v4, Lcom/twitter/app/dm/i;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/app/dm/DMConversationFragment;->aw:Lcom/twitter/app/dm/x;

    invoke-virtual {v5}, Lcom/twitter/app/dm/x;->a()Lcom/twitter/model/dms/n;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/twitter/app/dm/i;->a(Lcom/twitter/model/dms/n;)V

    :cond_7
    move-object/from16 v4, p0

    move-object/from16 v5, v20

    move-object/from16 v6, v19

    move-object/from16 v7, v21

    move/from16 v8, v17

    move-object/from16 v9, v18

    move/from16 v10, v16

    .line 929
    invoke-direct/range {v4 .. v10}, Lcom/twitter/app/dm/DMConversationFragment;->a(Lcom/twitter/android/media/selection/MediaAttachment;Landroid/net/Uri;Ljava/lang/String;ZLjava/util/Collection;Z)V

    .line 932
    invoke-direct/range {p0 .. p0}, Lcom/twitter/app/dm/DMConversationFragment;->bh()Lcom/twitter/app/dm/DMActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/app/dm/DMActivity;->i()Lcom/twitter/android/widget/DraggableDrawerLayout;

    move-result-object v4

    invoke-static {v4}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/android/widget/DraggableDrawerLayout;

    .line 934
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/dm/DMConversationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 935
    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v6, v6, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 936
    const v7, 0x7f0e0514

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 937
    sub-int v5, v6, v5

    .line 938
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/dm/DMConversationFragment;->getView()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v6

    new-instance v7, Lcom/twitter/app/dm/DMConversationFragment$24;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v4, v5}, Lcom/twitter/app/dm/DMConversationFragment$24;-><init>(Lcom/twitter/app/dm/DMConversationFragment;Lcom/twitter/android/widget/DraggableDrawerLayout;I)V

    invoke-virtual {v6, v7}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 951
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/twitter/app/dm/DMConversationFragment;->az:Z

    if-eqz v5, :cond_8

    .line 952
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    new-instance v6, Lapk;

    .line 953
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/dm/DMConversationFragment;->getContext()Landroid/content/Context;

    move-result-object v7

    move-object/from16 v0, p0

    invoke-direct {v6, v7, v4, v0}, Lapk;-><init>(Landroid/content/Context;Lcom/twitter/android/widget/DraggableDrawerLayout;Lapk$a;)V

    .line 952
    invoke-virtual {v5, v6}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->setQuickReplyDrawerHost(Lapk;)V

    .line 957
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/dm/DMConversationFragment;->getView()Landroid/view/View;

    move-result-object v4

    const v5, 0x7f130317

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-static {v4}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;

    .line 958
    invoke-static {}, Lcom/twitter/library/dm/d;->m()Z

    move-result v5

    if-eqz v5, :cond_11

    .line 959
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;->setVisibility(I)V

    .line 960
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v14, v13}, Lcom/twitter/app/dm/DMConversationFragment;->a(Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;II)V

    .line 962
    invoke-static {}, Lcom/twitter/library/dm/d;->n()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 963
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/app/dm/DMConversationFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->a(Landroid/support/v4/app/FragmentManager;)V

    .line 969
    :cond_9
    :goto_6
    invoke-direct/range {p0 .. p0}, Lcom/twitter/app/dm/DMConversationFragment;->ba()V

    .line 971
    invoke-direct/range {p0 .. p0}, Lcom/twitter/app/dm/DMConversationFragment;->aP()V

    goto/16 :goto_0

    .line 796
    :cond_a
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 828
    :cond_b
    const/4 v7, 0x0

    move v8, v7

    goto/16 :goto_2

    .line 838
    :cond_c
    invoke-virtual/range {v23 .. v23}, Lcom/twitter/app/dm/j;->j()Ljava/lang/String;

    move-result-object v14

    .line 839
    invoke-virtual/range {v23 .. v23}, Lcom/twitter/app/dm/j;->C()Z

    move-result v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/twitter/app/dm/DMConversationFragment;->j(Z)V

    .line 840
    invoke-virtual/range {v23 .. v23}, Lcom/twitter/app/dm/j;->x()Ljava/lang/String;

    move-result-object v17

    .line 841
    invoke-virtual/range {v23 .. v23}, Lcom/twitter/app/dm/j;->y()Ljava/lang/String;

    move-result-object v16

    .line 842
    invoke-virtual/range {v23 .. v23}, Lcom/twitter/app/dm/j;->c()Ljava/lang/String;

    move-result-object v15

    .line 843
    invoke-virtual/range {v23 .. v23}, Lcom/twitter/app/dm/j;->k()Landroid/net/Uri;

    move-result-object v12

    .line 844
    const/4 v13, 0x0

    .line 845
    const/4 v11, 0x0

    .line 846
    invoke-virtual/range {v23 .. v23}, Lcom/twitter/app/dm/j;->b()Z

    move-result v10

    .line 847
    invoke-virtual/range {v23 .. v23}, Lcom/twitter/app/dm/j;->D()Z

    move-result v4

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->ah:Z

    .line 848
    const/4 v9, 0x0

    .line 849
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->i:Z

    .line 850
    new-instance v4, Lcom/twitter/app/dm/m;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    invoke-direct {v4, v6, v7}, Lcom/twitter/app/dm/m;-><init>(J)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->K:Lcom/twitter/app/dm/m;

    .line 851
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->af:Z

    .line 852
    const/4 v8, 0x0

    .line 853
    invoke-static {}, Lcom/twitter/util/collection/MutableSet;->a()Ljava/util/Set;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->Q:Ljava/util/Set;

    .line 854
    const/4 v7, 0x5

    .line 855
    const/4 v6, 0x0

    .line 857
    invoke-virtual/range {v23 .. v23}, Lcom/twitter/app/dm/j;->f()[J

    move-result-object v4

    .line 858
    if-eqz v4, :cond_d

    array-length v0, v4

    move/from16 v18, v0

    if-eqz v18, :cond_d

    .line 861
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/app/dm/DMConversationFragment;->S:Lcom/twitter/library/client/p;

    move-object/from16 v18, v0

    new-instance v19, Lcom/twitter/library/dm/c;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v5, v1, v14, v4}, Lcom/twitter/library/dm/c;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;[J)V

    invoke-virtual/range {v18 .. v19}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 864
    :cond_d
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/app/dm/DMConversationFragment;->ae:Z

    if-eqz v4, :cond_e

    .line 865
    new-instance v18, Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    invoke-direct/range {v18 .. v18}, Lcom/twitter/analytics/feature/model/TwitterScribeItem;-><init>()V

    .line 866
    const-string/jumbo v4, "direct_share"

    move-object/from16 v0, v18

    iput-object v4, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->v:Ljava/lang/String;

    .line 867
    new-instance v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-direct {v4, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const-string/jumbo v21, "messages:thread::external_share:impression"

    aput-object v21, v19, v20

    .line 868
    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v4

    check-cast v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v4

    .line 867
    invoke-static {v4}, Lcpm;->a(Lcpk;)V

    move-object/from16 v18, v11

    move-object/from16 v19, v12

    move-object/from16 v20, v13

    move-object v4, v14

    move-object/from16 v21, v15

    move v13, v6

    move v14, v7

    move v15, v8

    move-object/from16 v6, v16

    move-object/from16 v7, v17

    move/from16 v16, v9

    move/from16 v17, v10

    .line 869
    goto/16 :goto_3

    .line 870
    :cond_e
    new-instance v4, Lcom/twitter/analytics/feature/model/ClientEventLog;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-direct {v4, v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string/jumbo v20, "messages:thread:::impression"

    aput-object v20, v18, v19

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v4

    invoke-static {v4}, Lcpm;->a(Lcpk;)V

    move-object/from16 v18, v11

    move-object/from16 v19, v12

    move-object/from16 v20, v13

    move-object v4, v14

    move-object/from16 v21, v15

    move v13, v6

    move v14, v7

    move v15, v8

    move-object/from16 v6, v16

    move-object/from16 v7, v17

    move/from16 v16, v9

    move/from16 v17, v10

    goto/16 :goto_3

    .line 881
    :cond_f
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 882
    :cond_10
    const/4 v4, 0x0

    goto/16 :goto_5

    .line 966
    :cond_11
    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/twitter/android/media/imageeditor/stickers/StickerSelectorView;->setVisibility(I)V

    goto/16 :goto_6
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 2008
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->g:Lcom/twitter/android/media/selection/c;

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->h:Lcom/twitter/app/dm/DMConversationFragment$c;

    .line 2009
    invoke-virtual {v1, p1}, Lcom/twitter/app/dm/DMConversationFragment$c;->b(I)I

    move-result v1

    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->h:Lcom/twitter/app/dm/DMConversationFragment$c;

    .line 2008
    invoke-virtual {v0, v1, p2, p3, v2}, Lcom/twitter/android/media/selection/c;->a(IILandroid/content/Intent;Lcom/twitter/android/media/selection/a;)V

    .line 2012
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/app/dm/n;->a(IILandroid/content/Intent;)V

    .line 2014
    const/4 v0, -0x1

    if-ne p2, v0, :cond_b

    .line 2015
    if-ne p1, v3, :cond_2

    .line 2016
    const-string/jumbo v0, "user_ids"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v0

    .line 2017
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 2018
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 2019
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v2

    .line 2020
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->a([J)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Iterable;)Lcom/twitter/util/collection/o;

    move-result-object v0

    .line 2021
    iget-boolean v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->x:Z

    if-eqz v2, :cond_1

    .line 2023
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->k:Landroid/app/ProgressDialog;

    .line 2024
    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v5}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 2025
    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 2026
    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 2027
    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V

    .line 2029
    new-instance v2, Lcom/twitter/library/api/dm/b;

    iget-object v3, p0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    iget-object v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    .line 2030
    invoke-virtual {v0}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-direct {v2, v1, v3, v4, v0}, Lcom/twitter/library/api/dm/b;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/util/Set;)V

    const/16 v0, 0xa

    .line 2029
    invoke-virtual {p0, v2, v0, v5}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 2084
    :cond_0
    :goto_0
    return-void

    .line 2034
    :cond_1
    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->p:[J

    .line 2035
    invoke-static {v2}, Lcom/twitter/util/collection/CollectionUtils;->a([J)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/util/collection/o;->b(Ljava/lang/Iterable;)Lcom/twitter/util/collection/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/collection/o;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 2034
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->e(Ljava/util/Collection;)[J

    move-result-object v0

    .line 2036
    new-instance v2, Lcom/twitter/app/dm/j$a;

    invoke-direct {v2}, Lcom/twitter/app/dm/j$a;-><init>()V

    .line 2038
    invoke-virtual {v2, v0}, Lcom/twitter/app/dm/j$a;->a([J)Lcom/twitter/app/dm/j$a;

    move-result-object v0

    .line 2039
    invoke-virtual {v0}, Lcom/twitter/app/dm/j$a;->e()Lcom/twitter/app/dm/j;

    move-result-object v0

    .line 2036
    invoke-static {v1, v0}, Lcom/twitter/app/dm/l;->a(Landroid/content/Context;Lcom/twitter/app/dm/j;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->startActivity(Landroid/content/Intent;)V

    .line 2042
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bm()V

    goto :goto_0

    .line 2045
    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_4

    if-eqz p3, :cond_4

    .line 2046
    invoke-static {p3}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2047
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {v0}, Lcom/twitter/app/dm/n;->a()V

    goto :goto_0

    .line 2049
    :cond_3
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bf()V

    goto :goto_0

    .line 2051
    :cond_4
    const/16 v0, 0xe

    if-ne p1, v0, :cond_5

    .line 2052
    invoke-static {p3}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2053
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bb()V

    goto :goto_0

    .line 2054
    :cond_5
    if-ne p1, v4, :cond_7

    .line 2055
    if-eqz p3, :cond_6

    .line 2056
    invoke-static {p3}, Lcom/twitter/android/util/j;->a(Landroid/content/Intent;)Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v0

    .line 2057
    if-eqz v0, :cond_6

    .line 2058
    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    new-instance v2, Lcom/twitter/android/media/selection/MediaAttachment;

    invoke-direct {v2, v0}, Lcom/twitter/android/media/selection/MediaAttachment;-><init>(Lcom/twitter/model/drafts/DraftAttachment;)V

    invoke-virtual {v1, v2}, Lcom/twitter/app/dm/n;->a(Lcom/twitter/android/media/selection/MediaAttachment;)V

    .line 2061
    :cond_6
    sget-object v0, Lcom/twitter/android/composer/ComposerType;->c:Lcom/twitter/android/composer/ComposerType;

    invoke-static {v0, p3}, Lcom/twitter/android/util/j;->a(Lcom/twitter/android/composer/ComposerType;Landroid/content/Intent;)V

    goto :goto_0

    .line 2062
    :cond_7
    const/4 v0, 0x5

    if-ne p1, v0, :cond_8

    if-eqz p3, :cond_8

    .line 2063
    const-string/jumbo v0, "media_attachment"

    .line 2064
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/media/selection/MediaAttachment;

    .line 2065
    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {v1, v0}, Lcom/twitter/app/dm/n;->a(Lcom/twitter/android/media/selection/MediaAttachment;)V

    goto :goto_0

    .line 2066
    :cond_8
    const/4 v0, 0x4

    if-ne p1, v0, :cond_9

    .line 2067
    invoke-static {p3}, Lcom/twitter/android/runtimepermissions/PermissionRequestActivity;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2068
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bn()V

    goto/16 :goto_0

    .line 2069
    :cond_9
    const/4 v0, 0x6

    if-ne p1, v0, :cond_a

    if-eqz p3, :cond_a

    .line 2070
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {v0}, Lcom/twitter/app/dm/n;->m()Lcom/twitter/model/media/EditableMedia;

    move-result-object v0

    .line 2071
    instance-of v1, v0, Lcom/twitter/model/media/EditableImage;

    if-eqz v1, :cond_0

    .line 2072
    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/media/EditableImage;

    .line 2073
    const-string/jumbo v1, "alt_text"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/model/media/EditableImage;->i:Ljava/lang/String;

    .line 2074
    new-instance v1, Lcom/twitter/android/media/selection/MediaAttachment;

    new-instance v2, Lcom/twitter/model/drafts/DraftAttachment;

    invoke-direct {v2, v0}, Lcom/twitter/model/drafts/DraftAttachment;-><init>(Lcom/twitter/model/media/EditableMedia;)V

    invoke-direct {v1, v2}, Lcom/twitter/android/media/selection/MediaAttachment;-><init>(Lcom/twitter/model/drafts/DraftAttachment;)V

    invoke-direct {p0, v1}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcom/twitter/android/media/selection/MediaAttachment;)V

    goto/16 :goto_0

    .line 2076
    :cond_a
    const/4 v0, 0x7

    if-ne p1, v0, :cond_0

    if-eqz p3, :cond_0

    .line 2077
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aH:Lcom/twitter/app/users/sheet/UsersBottomSheet;

    invoke-virtual {v0}, Lcom/twitter/app/users/sheet/UsersBottomSheet;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2078
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aH:Lcom/twitter/app/users/sheet/UsersBottomSheet;

    invoke-virtual {v0, p3}, Lcom/twitter/app/users/sheet/UsersBottomSheet;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 2081
    :cond_b
    if-ne p1, v4, :cond_0

    .line 2082
    sget-object v0, Lcom/twitter/android/composer/ComposerType;->c:Lcom/twitter/android/composer/ComposerType;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/util/j;->a(Lcom/twitter/android/composer/ComposerType;Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 448
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 449
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;)Lcom/twitter/android/client/l;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/client/l;->a(Lcom/twitter/android/client/m;)V

    .line 450
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v4, 0x7f0400ce

    invoke-virtual {v0, v4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aD:Landroid/view/View;

    .line 451
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aD:Landroid/view/View;

    const v4, 0x7f13032c

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aE:Landroid/widget/TextView;

    .line 452
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aD:Landroid/view/View;

    const v4, 0x7f13032e

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aF:Landroid/widget/TextView;

    .line 453
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aD:Landroid/view/View;

    const v4, 0x7f13032d

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aG:Landroid/view/View;

    .line 455
    invoke-static {}, Lcom/twitter/android/util/j;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->P:Z

    .line 456
    invoke-static {}, Lcom/twitter/app/dm/t;->a()Lcom/twitter/app/dm/t;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->u:Lcom/twitter/app/dm/t;

    .line 457
    new-instance v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;-><init>()V

    const-string/jumbo v4, "messages"

    .line 458
    invoke-virtual {v0, v4}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v4, "thread"

    .line 459
    invoke-virtual {v0, v4}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const-string/jumbo v4, ""

    .line 460
    invoke-virtual {v0, v4}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->d(Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeAssociation;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 457
    invoke-virtual {p0, v0}, Lcom/twitter/app/dm/DMConversationFragment;->a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)V

    .line 462
    invoke-static {}, Lcom/twitter/library/dm/d;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ai:Z

    .line 463
    invoke-static {}, Lcom/twitter/library/dm/d;->g()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aj:Z

    .line 464
    new-instance v0, Lcom/twitter/app/dm/DMConversationFragment$a;

    invoke-direct {v0, p0, v1}, Lcom/twitter/app/dm/DMConversationFragment$a;-><init>(Lcom/twitter/app/dm/DMConversationFragment;Lcom/twitter/app/dm/DMConversationFragment$1;)V

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ak:Lape;

    .line 465
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aj:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/twitter/library/card/m;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/library/card/m;

    invoke-direct {v0}, Lcom/twitter/library/card/m;-><init>()V

    :goto_0
    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->al:Lcom/twitter/library/card/m;

    .line 467
    invoke-static {}, Lcom/twitter/library/dm/d;->r()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->am:Z

    .line 469
    invoke-static {}, Lcom/twitter/library/dm/d;->j()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->au:Z

    .line 470
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->av:Landroid/os/Handler;

    .line 472
    if-eqz p1, :cond_1

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->d:Z

    .line 474
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    .line 475
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    .line 477
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/model/account/UserSettings;

    move-result-object v0

    .line 478
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/twitter/model/account/UserSettings;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 480
    :goto_2
    invoke-static {v2}, Lcom/twitter/library/dm/d;->a(Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ax:Z

    .line 482
    const-wide/16 v4, 0x3e8

    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ax:Z

    if-eqz v0, :cond_3

    const-wide/16 v2, 0xa

    :goto_3
    mul-long/2addr v2, v4

    .line 484
    new-instance v0, Lcom/twitter/library/client/o;

    new-instance v4, Lcom/twitter/app/dm/DMConversationFragment$1;

    invoke-direct {v4, p0}, Lcom/twitter/app/dm/DMConversationFragment$1;-><init>(Lcom/twitter/app/dm/DMConversationFragment;)V

    invoke-direct {v0, v4, v2, v3}, Lcom/twitter/library/client/o;-><init>(Ljava/lang/Runnable;J)V

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->N:Lcom/twitter/library/client/o;

    .line 493
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ax:Z

    if-eqz v0, :cond_4

    .line 494
    new-instance v0, Lcom/twitter/app/dm/x;

    invoke-direct {v0}, Lcom/twitter/app/dm/x;-><init>()V

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aw:Lcom/twitter/app/dm/x;

    .line 499
    :goto_4
    invoke-static {}, Lcom/twitter/library/dm/d;->s()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ay:Z

    .line 500
    invoke-static {}, Lcom/twitter/library/dm/d;->t()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->az:Z

    .line 501
    invoke-static {}, Lcom/twitter/library/dm/d;->u()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aA:Z

    .line 502
    invoke-static {}, Lcom/twitter/library/dm/d;->x()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aB:Z

    .line 503
    return-void

    :cond_0
    move-object v0, v1

    .line 465
    goto :goto_0

    :cond_1
    move v0, v3

    .line 472
    goto :goto_1

    :cond_2
    move v2, v3

    .line 478
    goto :goto_2

    .line 482
    :cond_3
    const-wide/16 v2, 0x1e

    goto :goto_3

    .line 496
    :cond_4
    iput-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->aw:Lcom/twitter/app/dm/x;

    goto :goto_4
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1336
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    move-object v0, v6

    .line 1349
    :goto_0
    return-object v0

    .line 1338
    :pswitch_1
    new-instance v0, Laph;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    iget-object v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Laph;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    goto :goto_0

    .line 1341
    :pswitch_2
    new-instance v0, Lcom/twitter/util/android/d;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/database/schema/a$j;->a:Landroid/net/Uri;

    iget-wide v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    .line 1342
    invoke-static {v2, v4, v5}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/database/schema/a$j;->b:[Ljava/lang/String;

    const-string/jumbo v4, "card_conversation_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    aput-object v8, v5, v7

    invoke-direct/range {v0 .. v6}, Lcom/twitter/util/android/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1336
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 7

    .prologue
    .line 1240
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->av:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1241
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/l;->a(Landroid/content/Context;)Lcom/twitter/android/client/l;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/client/l;->b(Lcom/twitter/android/client/m;)V

    .line 1243
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    if-eqz v0, :cond_0

    .line 1245
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    new-instance v2, Lcom/twitter/app/dm/ak;

    .line 1246
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    iget-object v5, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    new-instance v0, Lcom/twitter/model/dms/x$a;

    invoke-direct {v0}, Lcom/twitter/model/dms/x$a;-><init>()V

    iget-object v6, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    .line 1248
    invoke-virtual {v6}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->getMessageText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/twitter/model/dms/x$a;->a(Ljava/lang/String;)Lcom/twitter/model/dms/x$a;

    move-result-object v0

    .line 1249
    invoke-virtual {v0}, Lcom/twitter/model/dms/x$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/x;

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/twitter/app/dm/ak;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Lcom/twitter/model/dms/x;)V

    .line 1245
    invoke-virtual {v1, v2}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 1252
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aC:Lcom/twitter/app/dm/widget/c;

    if-eqz v0, :cond_1

    .line 1253
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aC:Lcom/twitter/app/dm/widget/c;

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/c;->h()V

    .line 1256
    :cond_1
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->onDestroy()V

    .line 1257
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 235
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/app/dm/DMConversationFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1656
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1468
    invoke-super {p0, p1}, Lcom/twitter/app/common/list/TwitterListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1471
    const-string/jumbo v0, "media_uri"

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {v1}, Lcom/twitter/app/dm/n;->h()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1472
    const-string/jumbo v0, "media_attachment"

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {v1}, Lcom/twitter/app/dm/n;->l()Lcom/twitter/android/media/selection/MediaAttachment;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1473
    const-string/jumbo v0, "canceled_pending_attachments"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    .line 1474
    invoke-virtual {v2}, Lcom/twitter/app/dm/n;->e()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1473
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1476
    const-string/jumbo v0, "text"

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual {v1}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->getMessageText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1477
    const-string/jumbo v0, "error_dialog"

    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->i:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1478
    const-string/jumbo v0, "conversation_id"

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1479
    const-string/jumbo v0, "is_group_convo"

    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->x:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1480
    const-string/jumbo v0, "has_loaded_entries"

    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->E:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1481
    const-string/jumbo v0, "has_sent_message"

    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->af:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1482
    const-string/jumbo v0, "should_show_verified_badge"

    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->ah:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1483
    const-string/jumbo v0, "read_only"

    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->y:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1484
    const-string/jumbo v0, "report_entry_id"

    iget-wide v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->b:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1485
    const-string/jumbo v0, "quick_emoji_visible"

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual {v1}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->o()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1486
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->K:Lcom/twitter/app/dm/m;

    invoke-virtual {v0}, Lcom/twitter/app/dm/m;->a()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 1487
    const-string/jumbo v0, "has_scrolled_to_last_read_marker"

    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->M:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1488
    const-string/jumbo v0, "has_requested_older_messages"

    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->r:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1489
    const-string/jumbo v0, "has_user_scrolled"

    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->t:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1490
    const-string/jumbo v0, "in_flight_message_request_ids"

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->Q:Ljava/util/Set;

    .line 1491
    invoke-static {v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/Set;)Ljava/io/Serializable;

    move-result-object v1

    .line 1490
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1492
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->ao:Lcom/twitter/app/dm/ag;

    if-eqz v0, :cond_0

    .line 1493
    const-string/jumbo v0, "typing_data"

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->ao:Lcom/twitter/app/dm/ag;

    .line 1494
    invoke-virtual {v1}, Lcom/twitter/app/dm/ag;->d()Lcom/twitter/app/dm/ag$a;

    move-result-object v1

    sget-object v2, Lcom/twitter/app/dm/ag$a;->a:Lcom/twitter/app/dm/ag$b;

    invoke-static {v1, v2}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v1

    .line 1493
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1498
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aE:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1499
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1500
    const-string/jumbo v1, "title"

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1503
    :cond_1
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aF:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1504
    invoke-static {v0}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1505
    const-string/jumbo v1, "subtitle"

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1508
    :cond_2
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aw:Lcom/twitter/app/dm/x;

    if-eqz v0, :cond_3

    .line 1509
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aw:Lcom/twitter/app/dm/x;

    invoke-virtual {v0}, Lcom/twitter/app/dm/x;->b()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 1512
    :cond_3
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aC:Lcom/twitter/app/dm/widget/c;

    if-eqz v0, :cond_4

    .line 1513
    const-string/jumbo v0, "sticker_sheet_state"

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->aC:Lcom/twitter/app/dm/widget/c;

    invoke-virtual {v1}, Lcom/twitter/app/dm/widget/c;->b()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1514
    const-string/jumbo v0, "sticker_tab_position"

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->aC:Lcom/twitter/app/dm/widget/c;

    invoke-virtual {v1}, Lcom/twitter/app/dm/widget/c;->g()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1517
    :cond_4
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/i;

    invoke-virtual {v0}, Lcom/twitter/app/dm/i;->d()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 1518
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 1261
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->onStart()V

    .line 1262
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->Q()V

    .line 1263
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1264
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->P()V

    .line 1267
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aH:Lcom/twitter/app/users/sheet/UsersBottomSheet;

    invoke-virtual {v0, p0}, Lcom/twitter/app/users/sheet/UsersBottomSheet;->a(Lcom/twitter/app/users/sheet/b;)V

    .line 1268
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 1272
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aH:Lcom/twitter/app/users/sheet/UsersBottomSheet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/app/users/sheet/UsersBottomSheet;->a(Lcom/twitter/app/users/sheet/b;)V

    .line 1273
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->T()V

    .line 1274
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->onStop()V

    .line 1275
    return-void
.end method

.method protected p()V
    .locals 0

    .prologue
    .line 1080
    return-void
.end method

.method public q()V
    .locals 1

    .prologue
    .line 554
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->j:Lcom/twitter/app/dm/widget/DMConversationMessageComposer;

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/DMConversationMessageComposer;->n()V

    .line 555
    return-void
.end method

.method public q_()V
    .locals 1

    .prologue
    .line 1227
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->a:Lcom/twitter/android/widget/PromptDialogFragment;

    if-eqz v0, :cond_0

    .line 1228
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->a:Lcom/twitter/android/widget/PromptDialogFragment;

    invoke-virtual {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->dismiss()V

    .line 1229
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->a:Lcom/twitter/android/widget/PromptDialogFragment;

    .line 1231
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->N:Lcom/twitter/library/client/o;

    invoke-virtual {v0}, Lcom/twitter/library/client/o;->b()V

    .line 1234
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->q()V

    .line 1235
    invoke-super {p0}, Lcom/twitter/app/common/list/TwitterListFragment;->q_()V

    .line 1236
    return-void
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 587
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aC:Lcom/twitter/app/dm/widget/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aC:Lcom/twitter/app/dm/widget/c;

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()V
    .locals 1

    .prologue
    .line 598
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aC:Lcom/twitter/app/dm/widget/c;

    if-eqz v0, :cond_0

    .line 599
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->aC:Lcom/twitter/app/dm/widget/c;

    invoke-virtual {v0}, Lcom/twitter/app/dm/widget/c;->e()V

    .line 601
    :cond_0
    return-void
.end method

.method t()V
    .locals 4

    .prologue
    .line 1154
    new-instance v0, Lcom/twitter/library/api/dm/f;

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->T:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->e:Lcom/twitter/library/client/Session;

    iget-object v3, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/dm/f;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    .line 1156
    iget-boolean v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->am:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->ao:Lcom/twitter/app/dm/ag;

    if-eqz v1, :cond_0

    .line 1157
    iget-object v1, p0, Lcom/twitter/app/dm/DMConversationFragment;->ao:Lcom/twitter/app/dm/ag;

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/dm/f;->a(Lcom/twitter/library/dm/g;)V

    .line 1159
    :cond_0
    const/16 v1, 0xc

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/app/dm/DMConversationFragment;->c(Lcom/twitter/library/service/s;II)Z

    .line 1160
    return-void
.end method

.method public u()Z
    .locals 1

    .prologue
    .line 1278
    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->y:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()Z
    .locals 1

    .prologue
    .line 1282
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/dm/e;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->x:Z

    if-nez v0, :cond_1

    .line 1283
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->ak()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->az()Lcjr;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/dm/i;

    invoke-virtual {v0}, Lcom/twitter/app/dm/i;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 1282
    :goto_0
    return v0

    .line 1283
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public w()Z
    .locals 1

    .prologue
    .line 1290
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {v0}, Lcom/twitter/app/dm/n;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public x()V
    .locals 4

    .prologue
    .line 2089
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->X()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:view_participants:user_list:user:click"

    aput-object v3, v1, v2

    .line 2090
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 2089
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 2091
    return-void
.end method

.method public y()V
    .locals 0

    .prologue
    .line 2161
    invoke-direct {p0}, Lcom/twitter/app/dm/DMConversationFragment;->bn()V

    .line 2162
    return-void
.end method

.method public z()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2167
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-wide v2, p0, Lcom/twitter/app/dm/DMConversationFragment;->f:J

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:thread:dm_compose_bar:found_media:click"

    aput-object v3, v1, v2

    .line 2168
    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 2167
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 2169
    iget-object v0, p0, Lcom/twitter/app/dm/DMConversationFragment;->l:Lcom/twitter/app/dm/n;

    invoke-virtual {v0, v4}, Lcom/twitter/app/dm/n;->a(Z)V

    .line 2170
    invoke-virtual {p0}, Lcom/twitter/app/dm/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x2

    sget-object v2, Lcom/twitter/android/composer/ComposerType;->c:Lcom/twitter/android/composer/ComposerType;

    invoke-static {v0, v1, v2}, Lcom/twitter/android/util/j;->a(Landroid/app/Activity;ILcom/twitter/android/composer/ComposerType;)V

    .line 2171
    return-void
.end method
