.class final Lcom/twitter/app/dm/i$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/app/dm/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/util/object/i",
        "<",
        "Lcom/twitter/app/dm/i;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private c:Lcom/twitter/app/dm/f;

.field private d:Lcom/twitter/android/bn;

.field private e:Lcom/twitter/app/dm/m;

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Lape;

.field private k:Lcom/twitter/library/card/m;

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Lapl;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 911
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/app/dm/i$a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 911
    iget-object v0, p0, Lcom/twitter/app/dm/i$a;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/app/dm/i$a;)Lcom/twitter/app/dm/f;
    .locals 1

    .prologue
    .line 911
    iget-object v0, p0, Lcom/twitter/app/dm/i$a;->c:Lcom/twitter/app/dm/f;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/app/dm/i$a;)Lcom/twitter/android/bn;
    .locals 1

    .prologue
    .line 911
    iget-object v0, p0, Lcom/twitter/app/dm/i$a;->d:Lcom/twitter/android/bn;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/app/dm/i$a;)Lcom/twitter/app/dm/m;
    .locals 1

    .prologue
    .line 911
    iget-object v0, p0, Lcom/twitter/app/dm/i$a;->e:Lcom/twitter/app/dm/m;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/app/dm/i$a;)Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;
    .locals 1

    .prologue
    .line 911
    iget-object v0, p0, Lcom/twitter/app/dm/i$a;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/app/dm/i$a;)Z
    .locals 1

    .prologue
    .line 911
    iget-boolean v0, p0, Lcom/twitter/app/dm/i$a;->f:Z

    return v0
.end method

.method static synthetic g(Lcom/twitter/app/dm/i$a;)Z
    .locals 1

    .prologue
    .line 911
    iget-boolean v0, p0, Lcom/twitter/app/dm/i$a;->g:Z

    return v0
.end method

.method static synthetic h(Lcom/twitter/app/dm/i$a;)Lape;
    .locals 1

    .prologue
    .line 911
    iget-object v0, p0, Lcom/twitter/app/dm/i$a;->j:Lape;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/app/dm/i$a;)Lcom/twitter/library/card/m;
    .locals 1

    .prologue
    .line 911
    iget-object v0, p0, Lcom/twitter/app/dm/i$a;->k:Lcom/twitter/library/card/m;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/app/dm/i$a;)Z
    .locals 1

    .prologue
    .line 911
    iget-boolean v0, p0, Lcom/twitter/app/dm/i$a;->l:Z

    return v0
.end method

.method static synthetic k(Lcom/twitter/app/dm/i$a;)Z
    .locals 1

    .prologue
    .line 911
    iget-boolean v0, p0, Lcom/twitter/app/dm/i$a;->m:Z

    return v0
.end method

.method static synthetic l(Lcom/twitter/app/dm/i$a;)Z
    .locals 1

    .prologue
    .line 911
    iget-boolean v0, p0, Lcom/twitter/app/dm/i$a;->n:Z

    return v0
.end method

.method static synthetic m(Lcom/twitter/app/dm/i$a;)Z
    .locals 1

    .prologue
    .line 911
    iget-boolean v0, p0, Lcom/twitter/app/dm/i$a;->h:Z

    return v0
.end method

.method static synthetic n(Lcom/twitter/app/dm/i$a;)Z
    .locals 1

    .prologue
    .line 911
    iget-boolean v0, p0, Lcom/twitter/app/dm/i$a;->i:Z

    return v0
.end method

.method static synthetic o(Lcom/twitter/app/dm/i$a;)Lapl;
    .locals 1

    .prologue
    .line 911
    iget-object v0, p0, Lcom/twitter/app/dm/i$a;->o:Lapl;

    return-object v0
.end method


# virtual methods
.method public R_()Z
    .locals 1

    .prologue
    .line 1021
    iget-object v0, p0, Lcom/twitter/app/dm/i$a;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/i$a;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/i$a;->c:Lcom/twitter/app/dm/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/i$a;->d:Lcom/twitter/android/bn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/i$a;->e:Lcom/twitter/app/dm/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/app/dm/i$a;->j:Lape;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;)Lcom/twitter/app/dm/i$a;
    .locals 0

    .prologue
    .line 931
    iput-object p1, p0, Lcom/twitter/app/dm/i$a;->a:Landroid/content/Context;

    .line 932
    return-object p0
.end method

.method public a(Lape;)Lcom/twitter/app/dm/i$a;
    .locals 0

    .prologue
    .line 985
    iput-object p1, p0, Lcom/twitter/app/dm/i$a;->j:Lape;

    .line 986
    return-object p0
.end method

.method public a(Lapl;)Lcom/twitter/app/dm/i$a;
    .locals 0

    .prologue
    .line 1015
    iput-object p1, p0, Lcom/twitter/app/dm/i$a;->o:Lapl;

    .line 1016
    return-object p0
.end method

.method public a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lcom/twitter/app/dm/i$a;
    .locals 0

    .prologue
    .line 937
    iput-object p1, p0, Lcom/twitter/app/dm/i$a;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 938
    return-object p0
.end method

.method public a(Lcom/twitter/android/bn;)Lcom/twitter/app/dm/i$a;
    .locals 0

    .prologue
    .line 949
    iput-object p1, p0, Lcom/twitter/app/dm/i$a;->d:Lcom/twitter/android/bn;

    .line 950
    return-object p0
.end method

.method public a(Lcom/twitter/app/dm/f;)Lcom/twitter/app/dm/i$a;
    .locals 0

    .prologue
    .line 943
    iput-object p1, p0, Lcom/twitter/app/dm/i$a;->c:Lcom/twitter/app/dm/f;

    .line 944
    return-object p0
.end method

.method public a(Lcom/twitter/app/dm/m;)Lcom/twitter/app/dm/i$a;
    .locals 0

    .prologue
    .line 955
    iput-object p1, p0, Lcom/twitter/app/dm/i$a;->e:Lcom/twitter/app/dm/m;

    .line 956
    return-object p0
.end method

.method public a(Lcom/twitter/library/card/m;)Lcom/twitter/app/dm/i$a;
    .locals 0

    .prologue
    .line 991
    iput-object p1, p0, Lcom/twitter/app/dm/i$a;->k:Lcom/twitter/library/card/m;

    .line 992
    return-object p0
.end method

.method public a(Z)Lcom/twitter/app/dm/i$a;
    .locals 0

    .prologue
    .line 961
    iput-boolean p1, p0, Lcom/twitter/app/dm/i$a;->f:Z

    .line 962
    return-object p0
.end method

.method public b(Z)Lcom/twitter/app/dm/i$a;
    .locals 0

    .prologue
    .line 967
    iput-boolean p1, p0, Lcom/twitter/app/dm/i$a;->g:Z

    .line 968
    return-object p0
.end method

.method protected synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 911
    invoke-virtual {p0}, Lcom/twitter/app/dm/i$a;->e()Lcom/twitter/app/dm/i;

    move-result-object v0

    return-object v0
.end method

.method public c(Z)Lcom/twitter/app/dm/i$a;
    .locals 0

    .prologue
    .line 973
    iput-boolean p1, p0, Lcom/twitter/app/dm/i$a;->h:Z

    .line 974
    return-object p0
.end method

.method public d(Z)Lcom/twitter/app/dm/i$a;
    .locals 0

    .prologue
    .line 979
    iput-boolean p1, p0, Lcom/twitter/app/dm/i$a;->i:Z

    .line 980
    return-object p0
.end method

.method public e(Z)Lcom/twitter/app/dm/i$a;
    .locals 0

    .prologue
    .line 997
    iput-boolean p1, p0, Lcom/twitter/app/dm/i$a;->l:Z

    .line 998
    return-object p0
.end method

.method protected e()Lcom/twitter/app/dm/i;
    .locals 2

    .prologue
    .line 1028
    new-instance v0, Lcom/twitter/app/dm/i;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/app/dm/i;-><init>(Lcom/twitter/app/dm/i$a;Lcom/twitter/app/dm/i$1;)V

    return-object v0
.end method

.method public f(Z)Lcom/twitter/app/dm/i$a;
    .locals 0

    .prologue
    .line 1003
    iput-boolean p1, p0, Lcom/twitter/app/dm/i$a;->m:Z

    .line 1004
    return-object p0
.end method

.method public g(Z)Lcom/twitter/app/dm/i$a;
    .locals 0

    .prologue
    .line 1009
    iput-boolean p1, p0, Lcom/twitter/app/dm/i$a;->n:Z

    .line 1010
    return-object p0
.end method
