.class Lcom/twitter/app/dm/ai;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Landroid/view/animation/Interpolator;

.field private static final b:Landroid/view/animation/Interpolator;


# instance fields
.field private final c:Landroid/view/View;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/animation/AnimatorSet;

.field private f:Landroid/animation/AnimatorSet;

.field private g:Landroid/animation/Animator;

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private final o:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private final p:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private q:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/high16 v3, 0x3e800000    # 0.25f

    .line 36
    new-instance v0, Lcom/twitter/ui/anim/l;

    const/high16 v1, 0x43b10000    # 354.0f

    const/high16 v2, 0x41b00000    # 22.0f

    invoke-direct {v0, v1, v2}, Lcom/twitter/ui/anim/l;-><init>(FF)V

    sput-object v0, Lcom/twitter/app/dm/ai;->a:Landroid/view/animation/Interpolator;

    .line 37
    const v0, 0x3dcccccd    # 0.1f

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v3, v0, v3, v1}, Landroid/support/v4/view/animation/PathInterpolatorCompat;->create(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v0

    sput-object v0, Lcom/twitter/app/dm/ai;->b:Landroid/view/animation/Interpolator;

    return-void
.end method

.method constructor <init>(Landroid/view/View;Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/twitter/app/dm/ai;->c:Landroid/view/View;

    .line 66
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 67
    const v1, 0x7f0f0052

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/twitter/app/dm/ai;->h:I

    .line 68
    const v1, 0x7f0f0053

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/twitter/app/dm/ai;->i:I

    .line 69
    const v1, 0x7f0f0054

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/twitter/app/dm/ai;->j:I

    .line 70
    const v1, 0x7f0f0055

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/twitter/app/dm/ai;->k:I

    .line 71
    const v1, 0x7f0f0030

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/twitter/app/dm/ai;->l:I

    .line 72
    const v1, 0x7f0f0031

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/app/dm/ai;->m:I

    .line 73
    const v0, 0x7f110003

    invoke-static {p2, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/app/dm/ai;->n:I

    .line 74
    const v0, 0x7f110190

    invoke-static {p2, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/app/dm/ai;->o:I

    .line 75
    const v0, 0x7f11008c

    invoke-static {p2, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/app/dm/ai;->p:I

    .line 77
    iget-object v0, p0, Lcom/twitter/app/dm/ai;->c:Landroid/view/View;

    const v1, 0x7f130331

    .line 78
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/view/View;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/app/dm/ai;->c:Landroid/view/View;

    const v4, 0x7f130332

    .line 79
    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/app/dm/ai;->c:Landroid/view/View;

    const v4, 0x7f130333

    .line 80
    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v1, v2

    .line 77
    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/app/dm/ai;->d:Ljava/util/List;

    .line 81
    return-void
.end method

.method private a(Landroid/view/View;J)Landroid/animation/Animator;
    .locals 6

    .prologue
    .line 300
    iget v4, p0, Lcom/twitter/app/dm/ai;->h:I

    const/high16 v5, 0x42480000    # 50.0f

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/app/dm/ai;->a(Landroid/view/View;JIF)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/View;JIF)Landroid/animation/Animator;
    .locals 4

    .prologue
    .line 309
    sget-object v0, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p5, v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x0

    aput v3, v1, v2

    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 310
    int-to-long v2, p4

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 311
    invoke-virtual {v0, p2, p3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 312
    sget-object v1, Lcom/twitter/app/dm/ai;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 313
    new-instance v1, Lcom/twitter/app/dm/ai$5;

    invoke-direct {v1, p0, p1}, Lcom/twitter/app/dm/ai$5;-><init>(Lcom/twitter/app/dm/ai;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 319
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/app/dm/ai;)Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/twitter/app/dm/ai;->q:Z

    return v0
.end method

.method static synthetic b(Lcom/twitter/app/dm/ai;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/twitter/app/dm/ai;->o:I

    return v0
.end method

.method private b(Landroid/view/View;)Landroid/animation/Animator;
    .locals 4

    .prologue
    .line 246
    sget-object v0, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    .line 247
    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 248
    iget v1, p0, Lcom/twitter/app/dm/ai;->k:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 249
    sget-object v1, Lcom/twitter/app/dm/ai;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 250
    new-instance v1, Lcom/twitter/app/dm/ai$3;

    invoke-direct {v1, p0, p1}, Lcom/twitter/app/dm/ai$3;-><init>(Lcom/twitter/app/dm/ai;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 259
    return-object v0

    .line 246
    :array_0
    .array-data 4
        0x0
        0x43960000    # 300.0f
    .end array-data
.end method

.method private b(Landroid/view/View;J)Landroid/animation/Animator;
    .locals 10

    .prologue
    const/high16 v3, 0x3fc00000    # 1.5f

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 328
    sget-object v0, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v1, v8, [F

    aput v3, v1, v7

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 329
    sget-object v1, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v2, v8, [F

    aput v3, v2, v7

    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .line 330
    sget-object v2, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v3, v8, [F

    aput v5, v3, v7

    invoke-static {v2, v3}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    .line 331
    sget-object v3, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v4, v8, [F

    aput v5, v4, v7

    invoke-static {v3, v4}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    .line 332
    const-string/jumbo v4, "colorFilter"

    new-array v5, v9, [I

    iget v6, p0, Lcom/twitter/app/dm/ai;->o:I

    aput v6, v5, v7

    iget v6, p0, Lcom/twitter/app/dm/ai;->p:I

    aput v6, v5, v8

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    .line 334
    new-instance v5, Landroid/animation/ArgbEvaluator;

    invoke-direct {v5}, Landroid/animation/ArgbEvaluator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/animation/PropertyValuesHolder;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 336
    const/4 v5, 0x3

    new-array v5, v5, [Landroid/animation/PropertyValuesHolder;

    aput-object v2, v5, v7

    aput-object v3, v5, v8

    aput-object v4, v5, v9

    invoke-static {p1, v5}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 337
    new-array v3, v9, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v3, v7

    aput-object v1, v3, v8

    invoke-static {p1, v3}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 338
    new-instance v1, Lcom/twitter/app/dm/ai$6;

    invoke-direct {v1, p0, p1}, Lcom/twitter/app/dm/ai$6;-><init>(Lcom/twitter/app/dm/ai;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 346
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 347
    invoke-virtual {v1, p2, p3}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 348
    iget v3, p0, Lcom/twitter/app/dm/ai;->i:I

    int-to-long v4, v3

    invoke-virtual {v1, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 349
    new-array v3, v9, [Landroid/animation/Animator;

    aput-object v0, v3, v7

    aput-object v2, v3, v8

    invoke-virtual {v1, v3}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 350
    return-object v1
.end method

.method private c(Landroid/view/View;)Landroid/animation/Animator;
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 267
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 268
    sget-object v1, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v2, v4, [F

    fill-array-data v2, :array_0

    .line 269
    invoke-static {p1, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 270
    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v4, [F

    fill-array-data v3, :array_1

    invoke-static {p1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 271
    new-array v3, v4, [Landroid/animation/Animator;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    aput-object v2, v3, v1

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 272
    iget v1, p0, Lcom/twitter/app/dm/ai;->k:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 273
    sget-object v1, Lcom/twitter/app/dm/ai;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 274
    new-instance v1, Lcom/twitter/app/dm/ai$4;

    invoke-direct {v1, p0, p1}, Lcom/twitter/app/dm/ai$4;-><init>(Lcom/twitter/app/dm/ai;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 284
    return-object v0

    .line 268
    nop

    :array_0
    .array-data 4
        0x0
        -0x3e600000    # -20.0f
    .end array-data

    .line 270
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method static synthetic c(Lcom/twitter/app/dm/ai;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/twitter/app/dm/ai;->l()V

    return-void
.end method

.method private d(Landroid/view/View;)Landroid/animation/Animator;
    .locals 6

    .prologue
    .line 292
    const-wide/16 v2, 0x0

    iget v4, p0, Lcom/twitter/app/dm/ai;->j:I

    const/high16 v5, 0x43480000    # 200.0f

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/app/dm/ai;->a(Landroid/view/View;JIF)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/app/dm/ai;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/twitter/app/dm/ai;->k()V

    return-void
.end method

.method static synthetic e(Lcom/twitter/app/dm/ai;)Landroid/view/View;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/twitter/app/dm/ai;->c:Landroid/view/View;

    return-object v0
.end method

.method private f()Landroid/animation/AnimatorSet;
    .locals 3

    .prologue
    .line 189
    iget-object v0, p0, Lcom/twitter/app/dm/ai;->e:Landroid/animation/AnimatorSet;

    if-nez v0, :cond_0

    .line 190
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 191
    iget-object v1, p0, Lcom/twitter/app/dm/ai;->c:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/twitter/app/dm/ai;->d(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    .line 192
    invoke-direct {p0}, Lcom/twitter/app/dm/ai;->g()Landroid/animation/AnimatorSet;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    invoke-direct {p0}, Lcom/twitter/app/dm/ai;->h()Landroid/animation/AnimatorSet;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 193
    iput-object v0, p0, Lcom/twitter/app/dm/ai;->e:Landroid/animation/AnimatorSet;

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/ai;->e:Landroid/animation/AnimatorSet;

    return-object v0
.end method

.method private g()Landroid/animation/AnimatorSet;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 205
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 206
    const/4 v0, 0x3

    new-array v2, v0, [Landroid/animation/Animator;

    iget-object v0, p0, Lcom/twitter/app/dm/ai;->d:Ljava/util/List;

    .line 207
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const-wide/16 v4, 0x0

    invoke-direct {p0, v0, v4, v5}, Lcom/twitter/app/dm/ai;->a(Landroid/view/View;J)Landroid/animation/Animator;

    move-result-object v0

    aput-object v0, v2, v3

    iget-object v0, p0, Lcom/twitter/app/dm/ai;->d:Ljava/util/List;

    .line 208
    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget v3, p0, Lcom/twitter/app/dm/ai;->h:I

    div-int/lit8 v3, v3, 0x2

    int-to-long v4, v3

    invoke-direct {p0, v0, v4, v5}, Lcom/twitter/app/dm/ai;->a(Landroid/view/View;J)Landroid/animation/Animator;

    move-result-object v0

    aput-object v0, v2, v6

    iget-object v0, p0, Lcom/twitter/app/dm/ai;->d:Ljava/util/List;

    .line 209
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget v3, p0, Lcom/twitter/app/dm/ai;->h:I

    int-to-long v4, v3

    invoke-direct {p0, v0, v4, v5}, Lcom/twitter/app/dm/ai;->a(Landroid/view/View;J)Landroid/animation/Animator;

    move-result-object v0

    aput-object v0, v2, v7

    .line 206
    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 211
    return-object v1
.end method

.method private h()Landroid/animation/AnimatorSet;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 220
    iget-object v0, p0, Lcom/twitter/app/dm/ai;->f:Landroid/animation/AnimatorSet;

    if-nez v0, :cond_0

    .line 222
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 223
    const/4 v0, 0x3

    new-array v2, v0, [Landroid/animation/Animator;

    iget-object v0, p0, Lcom/twitter/app/dm/ai;->d:Ljava/util/List;

    .line 224
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const-wide/16 v4, 0x0

    invoke-direct {p0, v0, v4, v5}, Lcom/twitter/app/dm/ai;->b(Landroid/view/View;J)Landroid/animation/Animator;

    move-result-object v0

    aput-object v0, v2, v3

    iget-object v0, p0, Lcom/twitter/app/dm/ai;->d:Ljava/util/List;

    .line 225
    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget v3, p0, Lcom/twitter/app/dm/ai;->i:I

    div-int/lit8 v3, v3, 0x2

    int-to-long v4, v3

    invoke-direct {p0, v0, v4, v5}, Lcom/twitter/app/dm/ai;->b(Landroid/view/View;J)Landroid/animation/Animator;

    move-result-object v0

    aput-object v0, v2, v6

    iget-object v0, p0, Lcom/twitter/app/dm/ai;->d:Ljava/util/List;

    .line 226
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget v3, p0, Lcom/twitter/app/dm/ai;->i:I

    int-to-long v4, v3

    invoke-direct {p0, v0, v4, v5}, Lcom/twitter/app/dm/ai;->b(Landroid/view/View;J)Landroid/animation/Animator;

    move-result-object v0

    aput-object v0, v2, v7

    .line 223
    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 227
    new-instance v0, Lcom/twitter/app/dm/ai$2;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/ai$2;-><init>(Lcom/twitter/app/dm/ai;)V

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 236
    iput-object v1, p0, Lcom/twitter/app/dm/ai;->f:Landroid/animation/AnimatorSet;

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/ai;->f:Landroid/animation/AnimatorSet;

    return-object v0
.end method

.method private i()Landroid/animation/Animator;
    .locals 4

    .prologue
    .line 359
    iget-object v0, p0, Lcom/twitter/app/dm/ai;->g:Landroid/animation/Animator;

    if-nez v0, :cond_0

    .line 360
    iget-object v0, p0, Lcom/twitter/app/dm/ai;->c:Landroid/view/View;

    sget-object v1, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    .line 361
    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 362
    iget v1, p0, Lcom/twitter/app/dm/ai;->k:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 363
    sget-object v1, Lcom/twitter/app/dm/ai;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 364
    new-instance v1, Lcom/twitter/app/dm/ai$7;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/ai$7;-><init>(Lcom/twitter/app/dm/ai;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 371
    iput-object v0, p0, Lcom/twitter/app/dm/ai;->g:Landroid/animation/Animator;

    .line 374
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/ai;->g:Landroid/animation/Animator;

    return-object v0

    .line 360
    :array_0
    .array-data 4
        0x0
        0x43960000    # 300.0f
    .end array-data
.end method

.method private j()Landroid/animation/Animator;
    .locals 4

    .prologue
    .line 382
    iget-object v0, p0, Lcom/twitter/app/dm/ai;->c:Landroid/view/View;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 383
    iget v1, p0, Lcom/twitter/app/dm/ai;->k:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 384
    new-instance v1, Lcom/twitter/app/dm/ai$8;

    invoke-direct {v1, p0}, Lcom/twitter/app/dm/ai$8;-><init>(Lcom/twitter/app/dm/ai;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 392
    return-object v0

    .line 382
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private k()V
    .locals 5

    .prologue
    .line 396
    iget-object v0, p0, Lcom/twitter/app/dm/ai;->c:Landroid/view/View;

    const v1, 0x7f130330

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 397
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 398
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 399
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 400
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 398
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 403
    :cond_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 404
    iget-object v0, p0, Lcom/twitter/app/dm/ai;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 405
    iget-object v0, p0, Lcom/twitter/app/dm/ai;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 406
    return-void
.end method

.method private l()V
    .locals 5

    .prologue
    .line 409
    iget-object v0, p0, Lcom/twitter/app/dm/ai;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 410
    check-cast v0, Landroid/widget/ImageView;

    .line 411
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iget v3, p0, Lcom/twitter/app/dm/ai;->n:I

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 412
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 414
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)Landroid/animation/AnimatorSet;
    .locals 6

    .prologue
    const/4 v3, 0x2

    .line 164
    sget-object v0, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v1, v3, [F

    fill-array-data v1, :array_0

    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 165
    sget-object v1, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v2, v3, [F

    fill-array-data v2, :array_1

    invoke-static {p1, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 167
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 168
    new-array v3, v3, [Landroid/animation/Animator;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 169
    iget v0, p0, Lcom/twitter/app/dm/ai;->m:I

    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 170
    iget v0, p0, Lcom/twitter/app/dm/ai;->l:I

    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 172
    new-instance v0, Lcom/twitter/app/dm/ai$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/app/dm/ai$1;-><init>(Lcom/twitter/app/dm/ai;Landroid/view/View;)V

    invoke-virtual {v1, v0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 179
    return-object v2

    .line 164
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 165
    :array_1
    .array-data 4
        0x42200000    # 40.0f
        0x0
    .end array-data
.end method

.method public a()V
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/dm/ai;->q:Z

    .line 88
    invoke-direct {p0}, Lcom/twitter/app/dm/ai;->f()Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 89
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 129
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 130
    invoke-direct {p0, v0}, Lcom/twitter/app/dm/ai;->d(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0

    .line 132
    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 139
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 140
    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    invoke-direct {p0, v0}, Lcom/twitter/app/dm/ai;->b(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0

    .line 144
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 145
    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    invoke-direct {p0, v0}, Lcom/twitter/app/dm/ai;->c(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_1

    .line 148
    :cond_1
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/app/dm/ai;->q:Z

    .line 96
    invoke-direct {p0}, Lcom/twitter/app/dm/ai;->i()Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 97
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/app/dm/ai;->q:Z

    .line 104
    invoke-direct {p0}, Lcom/twitter/app/dm/ai;->j()Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 105
    return-void
.end method

.method public d()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 111
    iget-object v0, p0, Lcom/twitter/app/dm/ai;->c:Landroid/view/View;

    const v1, 0x7f130330

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    move v1, v2

    .line 112
    :goto_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 113
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 112
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/twitter/app/dm/ai;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 117
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 120
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/app/dm/ai;->q:Z

    .line 121
    invoke-direct {p0}, Lcom/twitter/app/dm/ai;->h()Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 122
    iget-object v0, p0, Lcom/twitter/app/dm/ai;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 123
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/twitter/app/dm/ai;->f:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    .line 155
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/app/dm/ai;->q:Z

    .line 157
    :cond_0
    return-void
.end method
