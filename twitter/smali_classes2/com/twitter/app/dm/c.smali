.class public Lcom/twitter/app/dm/c;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/app/dm/c$a;
    }
.end annotation


# direct methods
.method private static a(Lcom/twitter/app/dm/c$a;)Lcom/twitter/library/service/t;
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/twitter/app/dm/c$1;

    invoke-direct {v0, p0}, Lcom/twitter/app/dm/c$1;-><init>(Lcom/twitter/app/dm/c$a;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;JLcom/twitter/app/dm/c$a;)V
    .locals 5

    .prologue
    .line 22
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 23
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/api/dm/i;

    const/4 v3, 0x1

    new-array v3, v3, [J

    const/4 v4, 0x0

    aput-wide p1, v3, v4

    invoke-direct {v2, p0, v0, v3}, Lcom/twitter/library/api/dm/i;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;[J)V

    .line 24
    invoke-static {p3}, Lcom/twitter/app/dm/c;->a(Lcom/twitter/app/dm/c$a;)Lcom/twitter/library/service/t;

    move-result-object v0

    .line 23
    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 25
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/app/dm/c$a;)V
    .locals 4

    .prologue
    .line 29
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 30
    invoke-static {}, Lcom/twitter/library/client/p;->b()Lcom/twitter/library/client/p;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/api/dm/i;

    .line 31
    invoke-static {p1}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, p0, v0, v3}, Lcom/twitter/library/api/dm/i;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/util/Collection;)V

    invoke-static {p2}, Lcom/twitter/app/dm/c;->a(Lcom/twitter/app/dm/c$a;)Lcom/twitter/library/service/t;

    move-result-object v0

    .line 30
    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/client/p;->a(Lcom/twitter/library/service/s;Lcom/twitter/library/service/t;)Ljava/lang/String;

    .line 32
    return-void
.end method
