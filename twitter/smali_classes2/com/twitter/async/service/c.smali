.class public Lcom/twitter/async/service/c;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field private final d:Ljava/util/concurrent/ExecutorService;

.field private final e:Ljava/util/concurrent/ExecutorService;

.field private final f:Ljava/util/concurrent/ExecutorService;

.field private final g:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/twitter/async/service/c;->a:Ljava/util/concurrent/ExecutorService;

    .line 36
    iput-object p2, p0, Lcom/twitter/async/service/c;->b:Ljava/util/concurrent/ExecutorService;

    .line 37
    iput-object p3, p0, Lcom/twitter/async/service/c;->c:Ljava/util/concurrent/ExecutorService;

    .line 38
    iput-object p4, p0, Lcom/twitter/async/service/c;->d:Ljava/util/concurrent/ExecutorService;

    .line 39
    iput-object p5, p0, Lcom/twitter/async/service/c;->e:Ljava/util/concurrent/ExecutorService;

    .line 40
    iput-object p6, p0, Lcom/twitter/async/service/c;->f:Ljava/util/concurrent/ExecutorService;

    .line 41
    iput-object p7, p0, Lcom/twitter/async/service/c;->g:Ljava/util/concurrent/ExecutorService;

    .line 42
    return-void
.end method

.method public static declared-synchronized a()Lcom/twitter/async/service/c;
    .locals 2

    .prologue
    .line 24
    const-class v1, Lcom/twitter/async/service/c;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcot;->ak()Lcot;

    move-result-object v0

    check-cast v0, Latz;

    invoke-interface {v0}, Latz;->I()Lcom/twitter/async/service/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)Ljava/util/concurrent/ExecutorService;
    .locals 2

    .prologue
    .line 60
    sget-object v0, Lcom/twitter/async/service/c$1;->a:[I

    invoke-virtual {p1}, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 80
    iget-object v0, p0, Lcom/twitter/async/service/c;->a:Ljava/util/concurrent/ExecutorService;

    :goto_0
    return-object v0

    .line 62
    :pswitch_0
    iget-object v0, p0, Lcom/twitter/async/service/c;->b:Ljava/util/concurrent/ExecutorService;

    goto :goto_0

    .line 65
    :pswitch_1
    iget-object v0, p0, Lcom/twitter/async/service/c;->c:Ljava/util/concurrent/ExecutorService;

    goto :goto_0

    .line 68
    :pswitch_2
    iget-object v0, p0, Lcom/twitter/async/service/c;->d:Ljava/util/concurrent/ExecutorService;

    goto :goto_0

    .line 71
    :pswitch_3
    iget-object v0, p0, Lcom/twitter/async/service/c;->e:Ljava/util/concurrent/ExecutorService;

    goto :goto_0

    .line 74
    :pswitch_4
    iget-object v0, p0, Lcom/twitter/async/service/c;->g:Ljava/util/concurrent/ExecutorService;

    goto :goto_0

    .line 77
    :pswitch_5
    iget-object v0, p0, Lcom/twitter/async/service/c;->f:Ljava/util/concurrent/ExecutorService;

    goto :goto_0

    .line 60
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public b()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/twitter/async/service/c;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 87
    iget-object v0, p0, Lcom/twitter/async/service/c;->b:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 88
    iget-object v0, p0, Lcom/twitter/async/service/c;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 89
    iget-object v0, p0, Lcom/twitter/async/service/c;->d:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 90
    iget-object v0, p0, Lcom/twitter/async/service/c;->g:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 91
    return-void
.end method
