.class Lcom/twitter/async/service/AsyncService$d;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/async/service/AsyncService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "d"
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private b:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 480
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/async/service/AsyncService$d;->a:Ljava/util/Map;

    .line 484
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/async/service/AsyncService$d;->b:I

    .line 485
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 526
    monitor-enter p0

    .line 527
    :try_start_0
    iget v0, p0, Lcom/twitter/async/service/AsyncService$d;->b:I

    if-le p1, v0, :cond_0

    .line 528
    iput p1, p0, Lcom/twitter/async/service/AsyncService$d;->b:I

    .line 530
    :cond_0
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$d;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 531
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 533
    if-eqz v0, :cond_1

    .line 534
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 536
    :cond_1
    return-void

    .line 531
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(ILjava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 513
    monitor-enter p0

    .line 514
    :try_start_0
    iget v0, p0, Lcom/twitter/async/service/AsyncService$d;->b:I

    if-gt p1, v0, :cond_2

    const/4 v0, 0x1

    .line 515
    :goto_0
    if-nez v0, :cond_0

    .line 516
    iget-object v1, p0, Lcom/twitter/async/service/AsyncService$d;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 518
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 519
    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    .line 520
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    .line 522
    :cond_1
    return-void

    .line 514
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 518
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 496
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/async/service/AsyncService$d;->a(I)V

    .line 497
    return-void
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 490
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/twitter/async/service/AsyncService$d;->a(ILjava/lang/Runnable;)V

    .line 491
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 508
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/twitter/async/service/AsyncService$d;->a(I)V

    .line 509
    return-void
.end method

.method public b(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 502
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lcom/twitter/async/service/AsyncService$d;->a(ILjava/lang/Runnable;)V

    .line 503
    return-void
.end method
