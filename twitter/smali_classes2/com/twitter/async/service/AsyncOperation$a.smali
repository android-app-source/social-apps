.class Lcom/twitter/async/service/AsyncOperation$a;
.super Lcom/twitter/async/service/AsyncOperation;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/async/service/AsyncOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/async/service/AsyncOperation",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/async/service/AsyncOperation$b;

.field private final b:Lcom/twitter/async/service/AsyncOperation;


# direct methods
.method protected constructor <init>(Lcom/twitter/async/service/AsyncOperation;Landroid/util/Pair;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/AsyncOperation;",
            "Landroid/util/Pair",
            "<",
            "Lcom/twitter/async/service/AsyncOperation$b;",
            "Lcom/twitter/async/service/AsyncOperation$ExecutionClass;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 671
    const-class v0, Lcom/twitter/async/service/AsyncOperation$a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/async/service/AsyncOperation;-><init>(Ljava/lang/String;)V

    .line 672
    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/async/service/AsyncOperation$b;

    iput-object v0, p0, Lcom/twitter/async/service/AsyncOperation$a;->a:Lcom/twitter/async/service/AsyncOperation$b;

    .line 673
    iget-object v0, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 674
    iget-object v0, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    invoke-virtual {p0, v0}, Lcom/twitter/async/service/AsyncOperation$a;->a(Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)V

    .line 676
    :cond_0
    iput-object p1, p0, Lcom/twitter/async/service/AsyncOperation$a;->b:Lcom/twitter/async/service/AsyncOperation;

    .line 677
    return-void
.end method


# virtual methods
.method protected a()Ljava/lang/Void;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 682
    iget-object v0, p0, Lcom/twitter/async/service/AsyncOperation$a;->a:Lcom/twitter/async/service/AsyncOperation$b;

    iget-object v1, p0, Lcom/twitter/async/service/AsyncOperation$a;->b:Lcom/twitter/async/service/AsyncOperation;

    invoke-interface {v0, v1}, Lcom/twitter/async/service/AsyncOperation$b;->a(Lcom/twitter/async/service/AsyncOperation;)V

    .line 683
    const/4 v0, 0x0

    return-object v0
.end method

.method protected b()Ljava/lang/Void;
    .locals 1

    .prologue
    .line 689
    const/4 v0, 0x0

    return-object v0
.end method

.method protected synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 664
    invoke-virtual {p0}, Lcom/twitter/async/service/AsyncOperation$a;->b()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic d()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 664
    invoke-virtual {p0}, Lcom/twitter/async/service/AsyncOperation$a;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
