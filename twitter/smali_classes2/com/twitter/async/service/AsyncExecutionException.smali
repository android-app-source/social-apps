.class public Lcom/twitter/async/service/AsyncExecutionException;
.super Ljava/lang/RuntimeException;
.source "Twttr"


# static fields
.field private static final serialVersionUID:J = -0x2bdeb49450afb4abL


# direct methods
.method constructor <init>(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/RuntimeException;-><init>()V

    .line 13
    invoke-virtual {p0, p1}, Lcom/twitter/async/service/AsyncExecutionException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 14
    return-void
.end method
