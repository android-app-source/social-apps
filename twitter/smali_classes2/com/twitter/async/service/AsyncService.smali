.class public Lcom/twitter/async/service/AsyncService;
.super Landroid/app/Service;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/async/service/AsyncService$f;,
        Lcom/twitter/async/service/AsyncService$d;,
        Lcom/twitter/async/service/AsyncService$e;,
        Lcom/twitter/async/service/AsyncService$g;,
        Lcom/twitter/async/service/AsyncService$a;,
        Lcom/twitter/async/service/AsyncService$b;,
        Lcom/twitter/async/service/AsyncService$c;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/os/Handler;

.field private final c:Landroid/os/IBinder;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/async/service/AsyncService$b;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/twitter/async/service/c;

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/async/service/AsyncService$e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/twitter/async/service/AsyncService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/async/service/AsyncService;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/twitter/async/service/c;->a()Lcom/twitter/async/service/c;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/async/service/AsyncService;-><init>(Lcom/twitter/async/service/c;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Lcom/twitter/async/service/c;)V
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 45
    new-instance v0, Lcom/twitter/async/service/AsyncService$f;

    invoke-direct {v0, p0}, Lcom/twitter/async/service/AsyncService$f;-><init>(Lcom/twitter/async/service/AsyncService;)V

    iput-object v0, p0, Lcom/twitter/async/service/AsyncService;->c:Landroid/os/IBinder;

    .line 46
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/async/service/AsyncService;->d:Ljava/util/Map;

    .line 59
    iput-object p1, p0, Lcom/twitter/async/service/AsyncService;->e:Lcom/twitter/async/service/c;

    .line 60
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/async/service/AsyncService;->b:Landroid/os/Handler;

    .line 61
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/async/service/AsyncService;->f:Ljava/util/Set;

    .line 62
    return-void
.end method

.method static synthetic a(Lcom/twitter/async/service/AsyncService;)Lcom/twitter/async/service/c;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService;->e:Lcom/twitter/async/service/c;

    return-object v0
.end method

.method static synthetic a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 41
    invoke-static {p0, p1}, Lcom/twitter/async/service/AsyncService;->b(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/async/service/AsyncService;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService;->b:Landroid/os/Handler;

    return-object v0
.end method

.method private static b(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/lang/Runnable;",
            ")",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 174
    :try_start_0
    invoke-interface {p0, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 178
    :goto_0
    return-object v0

    .line 175
    :catch_0
    move-exception v0

    .line 176
    sget-object v1, Lcom/twitter/async/service/AsyncService;->a:Ljava/lang/String;

    const-string/jumbo v2, "Attempt to submit a job during shutdown"

    invoke-static {v1, v2, v0}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 178
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/async/service/AsyncService;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService;->d:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/async/service/AsyncService;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService;->f:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/twitter/async/service/AsyncOperation",
            "<TT;TS;>;)V"
        }
    .end annotation

    .prologue
    .line 94
    if-nez p1, :cond_1

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 98
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/async/service/AsyncOperation;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    new-instance v0, Lcom/twitter/async/service/AsyncService$b;

    invoke-direct {v0, p0, p1}, Lcom/twitter/async/service/AsyncService$b;-><init>(Lcom/twitter/async/service/AsyncService;Lcom/twitter/async/service/AsyncOperation;)V

    .line 107
    invoke-virtual {p1}, Lcom/twitter/async/service/AsyncOperation;->n()V

    .line 110
    invoke-static {v0}, Lcom/twitter/async/service/AsyncService$b;->a(Lcom/twitter/async/service/AsyncService$b;)Lcom/twitter/async/service/AsyncOperation;

    move-result-object v1

    .line 109
    invoke-virtual {p1, v1}, Lcom/twitter/async/service/AsyncOperation;->c(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/Runnable;

    move-result-object v1

    .line 112
    new-instance v2, Lcom/twitter/async/service/AsyncService$1;

    invoke-direct {v2, p0, v1, p1, v0}, Lcom/twitter/async/service/AsyncService$1;-><init>(Lcom/twitter/async/service/AsyncService;Ljava/lang/Runnable;Lcom/twitter/async/service/AsyncOperation;Lcom/twitter/async/service/AsyncService$b;)V

    invoke-virtual {v0, v2}, Lcom/twitter/async/service/AsyncService$b;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method a(Lcom/twitter/async/service/AsyncService$b;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/twitter/async/service/AsyncService$b",
            "<TS;>;)V"
        }
    .end annotation

    .prologue
    .line 153
    invoke-static {p1}, Lcom/twitter/async/service/AsyncService$b;->b(Lcom/twitter/async/service/AsyncService$b;)Lcom/twitter/async/service/AsyncOperation;

    move-result-object v0

    .line 154
    iget-object v1, p0, Lcom/twitter/async/service/AsyncService;->e:Lcom/twitter/async/service/c;

    invoke-virtual {v0}, Lcom/twitter/async/service/AsyncOperation;->j()Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/async/service/c;->a(Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    .line 158
    invoke-virtual {v0}, Lcom/twitter/async/service/AsyncOperation;->m()Lcom/twitter/async/service/b;

    move-result-object v2

    .line 159
    if-eqz v2, :cond_0

    .line 160
    const-string/jumbo v3, "blocking"

    invoke-virtual {v2, v3}, Lcom/twitter/async/service/b;->a(Ljava/lang/String;)V

    .line 163
    :cond_0
    invoke-static {v1, p1}, Lcom/twitter/async/service/AsyncService;->b(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v1

    .line 164
    invoke-virtual {v0, v1}, Lcom/twitter/async/service/AsyncOperation;->a(Ljava/util/concurrent/Future;)V

    .line 165
    return-void
.end method

.method public a(Lcom/twitter/async/service/e;)V
    .locals 3

    .prologue
    .line 188
    new-instance v0, Lcom/twitter/async/service/AsyncService$c;

    iget-object v1, p0, Lcom/twitter/async/service/AsyncService;->f:Ljava/util/Set;

    invoke-direct {v0, p1, v1}, Lcom/twitter/async/service/AsyncService$c;-><init>(Lcom/twitter/async/service/e;Ljava/util/Set;)V

    .line 190
    iget-object v1, p0, Lcom/twitter/async/service/AsyncService;->e:Lcom/twitter/async/service/c;

    sget-object v2, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;->c:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    invoke-virtual {v1, v2}, Lcom/twitter/async/service/c;->a(Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    .line 191
    invoke-static {v1, v0}, Lcom/twitter/async/service/AsyncService;->b(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 192
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService;->c:Landroid/os/IBinder;

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 72
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 73
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService;->e:Lcom/twitter/async/service/c;

    invoke-virtual {v0}, Lcom/twitter/async/service/c;->b()V

    .line 74
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 75
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    .prologue
    .line 79
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "By default, AsyncService is strictly a bound service and does not support starting with Context.startService()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
