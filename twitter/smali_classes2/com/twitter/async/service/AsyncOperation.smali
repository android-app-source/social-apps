.class public abstract Lcom/twitter/async/service/AsyncOperation;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/util/concurrent/Future;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/async/service/AsyncOperation$a;,
        Lcom/twitter/async/service/AsyncOperation$b;,
        Lcom/twitter/async/service/AsyncOperation$ExecutionClass;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Future",
        "<TS;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/twitter/async/service/AsyncOperation$b;",
            "Lcom/twitter/async/service/AsyncOperation$ExecutionClass;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Landroid/os/Handler;

.field private c:Lcom/twitter/async/service/AsyncService$e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/async/service/AsyncService$e",
            "<TS;>;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field f:Lcom/twitter/async/service/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/async/service/k",
            "<TS;>;"
        }
    .end annotation
.end field

.field private g:Ljava/util/concurrent/Future;

.field private h:I

.field private i:Lcom/twitter/async/service/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/async/service/j",
            "<TS;>;"
        }
    .end annotation
.end field

.field private j:I

.field private k:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

.field private l:Z


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 123
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {p0, p1, v0}, Lcom/twitter/async/service/AsyncOperation;-><init>(Ljava/lang/String;Landroid/os/Handler;)V

    .line 124
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/async/service/AsyncOperation;->a:Ljava/util/List;

    .line 109
    sget-object v0, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;->a:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    iput-object v0, p0, Lcom/twitter/async/service/AsyncOperation;->k:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    .line 127
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/twitter/util/y;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/async/service/AsyncOperation;->d:Ljava/lang/String;

    .line 128
    iput-object p1, p0, Lcom/twitter/async/service/AsyncOperation;->e:Ljava/lang/String;

    .line 129
    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/async/service/AsyncOperation;->h:I

    .line 130
    iput-object p2, p0, Lcom/twitter/async/service/AsyncOperation;->b:Landroid/os/Handler;

    .line 131
    return-void
.end method


# virtual methods
.method public final a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Lcom/twitter/async/service/AsyncOperation",
            "<TT;TS;>;>(",
            "Lcom/twitter/async/service/AsyncOperation$b",
            "<TT;TE;>;)TE;"
        }
    .end annotation

    .prologue
    .line 168
    sget-object v0, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;->h:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    invoke-virtual {p0, p1, v0}, Lcom/twitter/async/service/AsyncOperation;->a(Lcom/twitter/async/service/AsyncOperation$b;Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)Lcom/twitter/async/service/AsyncOperation;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/twitter/async/service/AsyncOperation$b;Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)Lcom/twitter/async/service/AsyncOperation;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Lcom/twitter/async/service/AsyncOperation",
            "<TT;TS;>;>(",
            "Lcom/twitter/async/service/AsyncOperation$b",
            "<TT;TE;>;",
            "Lcom/twitter/async/service/AsyncOperation$ExecutionClass;",
            ")TE;"
        }
    .end annotation

    .prologue
    .line 181
    invoke-virtual {p0, p1, p2}, Lcom/twitter/async/service/AsyncOperation;->b(Lcom/twitter/async/service/AsyncOperation$b;Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)Z

    .line 182
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/async/service/AsyncOperation;

    return-object v0
.end method

.method public final a(Lcom/twitter/async/service/k;)Lcom/twitter/async/service/AsyncOperation;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Lcom/twitter/async/service/AsyncOperation;",
            ">(",
            "Lcom/twitter/async/service/k",
            "<TS;>;)TE;"
        }
    .end annotation

    .prologue
    .line 242
    iput-object p1, p0, Lcom/twitter/async/service/AsyncOperation;->f:Lcom/twitter/async/service/k;

    .line 243
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/async/service/AsyncOperation;

    return-object v0
.end method

.method public a(Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)V
    .locals 2

    .prologue
    .line 145
    sget-object v0, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;->h:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    if-ne p1, v0, :cond_0

    .line 146
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Async operations should not run on the main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :cond_0
    iput-object p1, p0, Lcom/twitter/async/service/AsyncOperation;->k:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    .line 149
    return-void
.end method

.method public final a(Lcom/twitter/async/service/AsyncService;)V
    .locals 4

    .prologue
    .line 316
    iget-object v0, p0, Lcom/twitter/async/service/AsyncOperation;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 317
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    .line 318
    if-eqz v1, :cond_0

    sget-object v3, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;->h:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    if-eq v1, v3, :cond_0

    if-eqz p1, :cond_0

    .line 319
    new-instance v1, Lcom/twitter/async/service/AsyncOperation$a;

    invoke-direct {v1, p0, v0}, Lcom/twitter/async/service/AsyncOperation$a;-><init>(Lcom/twitter/async/service/AsyncOperation;Landroid/util/Pair;)V

    invoke-virtual {p1, v1}, Lcom/twitter/async/service/AsyncService;->a(Lcom/twitter/async/service/AsyncOperation;)V

    goto :goto_0

    .line 321
    :cond_0
    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/async/service/AsyncOperation$b;

    invoke-interface {v0, p0}, Lcom/twitter/async/service/AsyncOperation$b;->a(Lcom/twitter/async/service/AsyncOperation;)V

    goto :goto_0

    .line 324
    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/async/service/j;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<TS;>;)V"
        }
    .end annotation

    .prologue
    .line 430
    return-void
.end method

.method protected final a(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 257
    iget-object v0, p0, Lcom/twitter/async/service/AsyncOperation;->b:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/async/service/AsyncOperation$1;

    invoke-direct {v1, p0, p1}, Lcom/twitter/async/service/AsyncOperation$1;-><init>(Lcom/twitter/async/service/AsyncOperation;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 265
    return-void
.end method

.method protected final a(Ljava/util/concurrent/Future;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/twitter/async/service/AsyncOperation;->g:Ljava/util/concurrent/Future;

    .line 208
    return-void
.end method

.method protected final declared-synchronized a(Lcom/twitter/async/service/AsyncService$e;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/AsyncService$e",
            "<TS;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 488
    monitor-enter p0

    .line 489
    :try_start_0
    iget v2, p0, Lcom/twitter/async/service/AsyncOperation;->h:I

    packed-switch v2, :pswitch_data_0

    .line 502
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Mark RETRY_SCHEDULED. Invalid state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/twitter/async/service/AsyncOperation;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 488
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 491
    :pswitch_0
    :try_start_1
    iput-object p1, p0, Lcom/twitter/async/service/AsyncOperation;->c:Lcom/twitter/async/service/AsyncService$e;

    .line 492
    const/4 v2, 0x3

    iput v2, p0, Lcom/twitter/async/service/AsyncOperation;->h:I

    move v2, v1

    .line 505
    :goto_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 506
    if-nez v2, :cond_0

    :goto_1
    monitor-exit p0

    return v0

    :pswitch_1
    move v2, v0

    .line 499
    goto :goto_0

    :cond_0
    move v0, v1

    .line 506
    goto :goto_1

    .line 489
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/twitter/async/service/e;Lcom/twitter/async/service/j;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 615
    monitor-enter p0

    .line 616
    :try_start_0
    iget v2, p0, Lcom/twitter/async/service/AsyncOperation;->h:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    move v2, v0

    .line 617
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 619
    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/async/service/AsyncOperation;->f:Lcom/twitter/async/service/k;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/async/service/AsyncOperation;->f:Lcom/twitter/async/service/k;

    .line 621
    invoke-virtual {v2, p1, p2}, Lcom/twitter/async/service/k;->a(Lcom/twitter/async/service/e;Lcom/twitter/async/service/j;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 619
    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 616
    goto :goto_0

    .line 617
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move v0, v1

    .line 621
    goto :goto_1
.end method

.method public final b(I)Lcom/twitter/async/service/AsyncOperation;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Lcom/twitter/async/service/AsyncOperation;",
            ">(I)TE;"
        }
    .end annotation

    .prologue
    .line 136
    iput p1, p0, Lcom/twitter/async/service/AsyncOperation;->j:I

    .line 137
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/async/service/AsyncOperation;

    return-object v0
.end method

.method public b(Lcom/twitter/async/service/j;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/j",
            "<TS;>;)V"
        }
    .end annotation

    .prologue
    .line 423
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 305
    iget-object v0, p0, Lcom/twitter/async/service/AsyncOperation;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 306
    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/async/service/AsyncOperation$b;

    invoke-interface {v0, p1, p0}, Lcom/twitter/async/service/AsyncOperation$b;->a(Ljava/lang/Object;Lcom/twitter/async/service/AsyncOperation;)V

    goto :goto_0

    .line 308
    :cond_0
    return-void
.end method

.method protected final b(Lcom/twitter/async/service/AsyncOperation$b;Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)Z
    .locals 2
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Lcom/twitter/async/service/AsyncOperation",
            "<TT;TS;>;>(",
            "Lcom/twitter/async/service/AsyncOperation$b",
            "<TT;TE;>;",
            "Lcom/twitter/async/service/AsyncOperation$ExecutionClass;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 197
    monitor-enter p0

    .line 198
    :try_start_0
    iget v0, p0, Lcom/twitter/async/service/AsyncOperation;->h:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    .line 199
    iget-object v0, p0, Lcom/twitter/async/service/AsyncOperation;->a:Ljava/util/List;

    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    const/4 v0, 0x1

    monitor-exit p0

    .line 203
    :goto_0
    return v0

    .line 202
    :cond_0
    monitor-exit p0

    .line 203
    const/4 v0, 0x0

    goto :goto_0

    .line 202
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final declared-synchronized c(Lcom/twitter/async/service/j;)Lcom/twitter/async/service/AsyncOperation;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Lcom/twitter/async/service/AsyncOperation;",
            ">(",
            "Lcom/twitter/async/service/j",
            "<TS;>;)TE;"
        }
    .end annotation

    .prologue
    .line 274
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/twitter/async/service/AsyncOperation;->i:Lcom/twitter/async/service/j;

    .line 275
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/async/service/AsyncOperation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 274
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract c()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TS;"
        }
    .end annotation
.end method

.method public c(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 415
    const/4 v0, 0x0

    return-object v0
.end method

.method public final cancel(Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 545
    .line 547
    monitor-enter p0

    .line 548
    :try_start_0
    iget v2, p0, Lcom/twitter/async/service/AsyncOperation;->h:I

    packed-switch v2, :pswitch_data_0

    move v1, v0

    .line 588
    :goto_0
    if-eqz v1, :cond_0

    .line 589
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/twitter/async/service/AsyncOperation;->l:Z

    .line 591
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 592
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 594
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/async/service/AsyncOperation;->c:Lcom/twitter/async/service/AsyncService$e;

    if-eqz v0, :cond_1

    .line 596
    iget-object v0, p0, Lcom/twitter/async/service/AsyncOperation;->c:Lcom/twitter/async/service/AsyncService$e;

    invoke-virtual {v0}, Lcom/twitter/async/service/AsyncService$e;->run()V

    .line 599
    :cond_1
    return v1

    .line 554
    :pswitch_0
    const/4 v2, 0x5

    :try_start_1
    iput v2, p0, Lcom/twitter/async/service/AsyncOperation;->h:I

    goto :goto_0

    .line 592
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 564
    :pswitch_1
    :try_start_2
    iget-object v2, p0, Lcom/twitter/async/service/AsyncOperation;->g:Ljava/util/concurrent/Future;

    if-eqz v2, :cond_2

    .line 565
    iget-object v2, p0, Lcom/twitter/async/service/AsyncOperation;->g:Ljava/util/concurrent/Future;

    invoke-interface {v2, p1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 567
    :cond_2
    const/4 v2, 0x5

    iput v2, p0, Lcom/twitter/async/service/AsyncOperation;->h:I

    goto :goto_0

    .line 576
    :pswitch_2
    const/4 v0, 0x5

    iput v0, p0, Lcom/twitter/async/service/AsyncOperation;->h:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v1

    .line 577
    goto :goto_0

    .line 548
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected abstract d()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TS;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation
.end method

.method public final get()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TS;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 334
    monitor-enter p0

    .line 335
    :goto_0
    :try_start_0
    iget v0, p0, Lcom/twitter/async/service/AsyncOperation;->h:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    .line 336
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    goto :goto_0

    .line 338
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 339
    iget-object v0, p0, Lcom/twitter/async/service/AsyncOperation;->i:Lcom/twitter/async/service/j;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/async/service/AsyncOperation;->i:Lcom/twitter/async/service/j;

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TS;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x6

    .line 352
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 353
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 354
    monitor-enter p0

    .line 355
    :goto_0
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-lez v4, :cond_0

    :try_start_0
    iget v4, p0, Lcom/twitter/async/service/AsyncOperation;->h:I

    if-eq v4, v6, :cond_0

    .line 356
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V

    .line 357
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v4, v2

    .line 358
    sub-long/2addr v0, v4

    .line 359
    goto :goto_0

    .line 360
    :cond_0
    iget v0, p0, Lcom/twitter/async/service/AsyncOperation;->h:I

    if-eq v0, v6, :cond_1

    .line 361
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    const-string/jumbo v1, "Get async operation result timed out"

    invoke-direct {v0, v1}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 363
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 364
    iget-object v0, p0, Lcom/twitter/async/service/AsyncOperation;->i:Lcom/twitter/async/service/j;

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 141
    iget v0, p0, Lcom/twitter/async/service/AsyncOperation;->j:I

    return v0
.end method

.method public declared-synchronized isCancelled()Z
    .locals 1

    .prologue
    .line 372
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/twitter/async/service/AsyncOperation;->l:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isDone()Z
    .locals 2

    .prologue
    .line 380
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/twitter/async/service/AsyncOperation;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected j()Lcom/twitter/async/service/AsyncOperation$ExecutionClass;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/twitter/async/service/AsyncOperation;->k:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    return-object v0
.end method

.method public k()V
    .locals 2

    .prologue
    .line 229
    monitor-enter p0

    .line 230
    :try_start_0
    iget v0, p0, Lcom/twitter/async/service/AsyncOperation;->h:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    .line 231
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 233
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/async/service/AsyncOperation;->c:Lcom/twitter/async/service/AsyncService$e;

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/twitter/async/service/AsyncOperation;->c:Lcom/twitter/async/service/AsyncService$e;

    invoke-virtual {v0}, Lcom/twitter/async/service/AsyncService$e;->run()V

    .line 237
    :cond_0
    return-void

    .line 230
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 231
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final declared-synchronized l()Lcom/twitter/async/service/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/async/service/j",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 269
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/async/service/AsyncOperation;->i:Lcom/twitter/async/service/j;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public m()Lcom/twitter/async/service/b;
    .locals 1

    .prologue
    .line 286
    const/4 v0, 0x0

    return-object v0
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 295
    iget-object v0, p0, Lcom/twitter/async/service/AsyncOperation;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 296
    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/async/service/AsyncOperation$b;

    invoke-interface {v0, p0}, Lcom/twitter/async/service/AsyncOperation$b;->b(Lcom/twitter/async/service/AsyncOperation;)V

    goto :goto_0

    .line 298
    :cond_0
    return-void
.end method

.method protected o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 439
    const/4 v0, 0x0

    return-object v0
.end method

.method protected declared-synchronized p()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 447
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/twitter/async/service/AsyncOperation;->h:I

    if-ne v1, v0, :cond_0

    .line 448
    const/4 v1, 0x2

    iput v1, p0, Lcom/twitter/async/service/AsyncOperation;->h:I

    .line 449
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 452
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 447
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized q()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 461
    monitor-enter p0

    .line 462
    :try_start_0
    iget v2, p0, Lcom/twitter/async/service/AsyncOperation;->h:I

    packed-switch v2, :pswitch_data_0

    .line 473
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Mark RUNNING. Invalid state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/twitter/async/service/AsyncOperation;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 461
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 465
    :pswitch_1
    const/4 v2, 0x4

    :try_start_1
    iput v2, p0, Lcom/twitter/async/service/AsyncOperation;->h:I

    move v2, v1

    .line 476
    :goto_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 477
    if-nez v2, :cond_0

    :goto_1
    monitor-exit p0

    return v0

    :pswitch_2
    move v2, v0

    .line 470
    goto :goto_0

    :cond_0
    move v0, v1

    .line 477
    goto :goto_1

    .line 462
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected declared-synchronized r()V
    .locals 3

    .prologue
    .line 513
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/twitter/async/service/AsyncOperation;->h:I

    packed-switch v0, :pswitch_data_0

    .line 520
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Mark DONE. Invalid state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/twitter/async/service/AsyncOperation;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 513
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 516
    :pswitch_0
    const/4 v0, 0x6

    :try_start_1
    iput v0, p0, Lcom/twitter/async/service/AsyncOperation;->h:I

    .line 523
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 524
    monitor-exit p0

    return-void

    .line 513
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
