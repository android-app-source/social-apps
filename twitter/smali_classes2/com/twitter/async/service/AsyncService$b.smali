.class Lcom/twitter/async/service/AsyncService$b;
.super Lcom/twitter/async/service/g;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/async/service/AsyncService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/twitter/async/service/g;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/async/service/AsyncService;

.field private final b:Ljava/lang/String;

.field private final e:Lcom/twitter/async/service/AsyncOperation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/async/service/AsyncOperation",
            "<*TS;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/twitter/async/service/AsyncOperation;

.field private final g:Lcom/twitter/async/service/AsyncService$d;

.field private final h:Lcom/twitter/async/service/AsyncService$d;

.field private final i:Lcom/twitter/async/service/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/async/service/j",
            "<TS;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/twitter/async/service/AsyncService;Lcom/twitter/async/service/AsyncOperation;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/AsyncOperation",
            "<*TS;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 240
    iput-object p1, p0, Lcom/twitter/async/service/AsyncService$b;->a:Lcom/twitter/async/service/AsyncService;

    .line 241
    invoke-virtual {p2}, Lcom/twitter/async/service/AsyncOperation;->i()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/twitter/async/service/g;-><init>(I)V

    .line 246
    invoke-virtual {p2}, Lcom/twitter/async/service/AsyncOperation;->o()Ljava/lang/String;

    move-result-object v0

    .line 247
    iput-object p2, p0, Lcom/twitter/async/service/AsyncService$b;->e:Lcom/twitter/async/service/AsyncOperation;

    .line 248
    new-instance v2, Lcom/twitter/async/service/j;

    invoke-direct {v2}, Lcom/twitter/async/service/j;-><init>()V

    iput-object v2, p0, Lcom/twitter/async/service/AsyncService$b;->i:Lcom/twitter/async/service/j;

    .line 249
    iput-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->b:Ljava/lang/String;

    .line 250
    new-instance v2, Lcom/twitter/async/service/AsyncService$d;

    invoke-direct {v2}, Lcom/twitter/async/service/AsyncService$d;-><init>()V

    iput-object v2, p0, Lcom/twitter/async/service/AsyncService$b;->g:Lcom/twitter/async/service/AsyncService$d;

    .line 251
    if-eqz v0, :cond_1

    .line 252
    invoke-static {p1}, Lcom/twitter/async/service/AsyncService;->c(Lcom/twitter/async/service/AsyncService;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v0, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/async/service/AsyncService$b;

    move-object v2, v0

    .line 253
    :goto_0
    if-eqz v2, :cond_2

    iget-object v0, v2, Lcom/twitter/async/service/AsyncService$b;->e:Lcom/twitter/async/service/AsyncOperation;

    :goto_1
    iput-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->f:Lcom/twitter/async/service/AsyncOperation;

    .line 254
    if-eqz v2, :cond_0

    iget-object v1, v2, Lcom/twitter/async/service/AsyncService$b;->g:Lcom/twitter/async/service/AsyncService$d;

    :cond_0
    iput-object v1, p0, Lcom/twitter/async/service/AsyncService$b;->h:Lcom/twitter/async/service/AsyncService$d;

    .line 255
    return-void

    :cond_1
    move-object v2, v1

    .line 252
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 253
    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/async/service/AsyncService$b;)Lcom/twitter/async/service/AsyncOperation;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->f:Lcom/twitter/async/service/AsyncOperation;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/async/service/AsyncService$b;)Lcom/twitter/async/service/AsyncOperation;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->e:Lcom/twitter/async/service/AsyncOperation;

    return-object v0
.end method

.method private b()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 295
    :try_start_0
    iget-object v1, p0, Lcom/twitter/async/service/AsyncService$b;->e:Lcom/twitter/async/service/AsyncOperation;

    invoke-virtual {v1}, Lcom/twitter/async/service/AsyncOperation;->d()Ljava/lang/Object;

    move-result-object v1

    .line 296
    iget-object v2, p0, Lcom/twitter/async/service/AsyncService$b;->i:Lcom/twitter/async/service/j;

    invoke-virtual {v2, v1}, Lcom/twitter/async/service/j;->a(Ljava/lang/Object;)Lcom/twitter/async/service/j;

    .line 297
    invoke-direct {p0}, Lcom/twitter/async/service/AsyncService$b;->c()Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_0

    .line 300
    :goto_0
    return v0

    .line 297
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 298
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/async/service/AsyncService$b;)Lcom/twitter/async/service/j;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->i:Lcom/twitter/async/service/j;

    return-object v0
.end method

.method private c()Z
    .locals 4

    .prologue
    .line 306
    const/4 v0, 0x0

    .line 307
    iget-object v1, p0, Lcom/twitter/async/service/AsyncService$b;->e:Lcom/twitter/async/service/AsyncOperation;

    iget-object v1, v1, Lcom/twitter/async/service/AsyncOperation;->f:Lcom/twitter/async/service/k;

    .line 308
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/twitter/async/service/AsyncService$b;->i:Lcom/twitter/async/service/j;

    invoke-virtual {v1, v2}, Lcom/twitter/async/service/k;->a(Lcom/twitter/async/service/j;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 309
    iget-object v2, p0, Lcom/twitter/async/service/AsyncService$b;->e:Lcom/twitter/async/service/AsyncOperation;

    iget-object v3, p0, Lcom/twitter/async/service/AsyncService$b;->i:Lcom/twitter/async/service/j;

    invoke-virtual {v2, v3}, Lcom/twitter/async/service/AsyncOperation;->b(Lcom/twitter/async/service/j;)V

    .line 310
    new-instance v2, Lcom/twitter/async/service/AsyncService$e;

    iget-object v3, p0, Lcom/twitter/async/service/AsyncService$b;->a:Lcom/twitter/async/service/AsyncService;

    invoke-direct {v2, v3, p0}, Lcom/twitter/async/service/AsyncService$e;-><init>(Lcom/twitter/async/service/AsyncService;Lcom/twitter/async/service/AsyncService$b;)V

    .line 311
    iget-object v3, p0, Lcom/twitter/async/service/AsyncService$b;->e:Lcom/twitter/async/service/AsyncOperation;

    invoke-virtual {v3, v2}, Lcom/twitter/async/service/AsyncOperation;->a(Lcom/twitter/async/service/AsyncService$e;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 312
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->a:Lcom/twitter/async/service/AsyncService;

    invoke-static {v0}, Lcom/twitter/async/service/AsyncService;->d(Lcom/twitter/async/service/AsyncService;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 313
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->i:Lcom/twitter/async/service/j;

    invoke-virtual {v1, v0}, Lcom/twitter/async/service/k;->b(Lcom/twitter/async/service/j;)J

    move-result-wide v0

    .line 314
    iget-object v3, p0, Lcom/twitter/async/service/AsyncService$b;->a:Lcom/twitter/async/service/AsyncService;

    invoke-static {v3}, Lcom/twitter/async/service/AsyncService;->b(Lcom/twitter/async/service/AsyncService;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v2, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 315
    const/4 v0, 0x1

    .line 318
    :cond_0
    return v0
.end method

.method private d()V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 322
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v0

    .line 323
    invoke-static {v0}, Landroid/os/Process;->getThreadPriority(I)I

    move-result v1

    if-eq v1, v2, :cond_0

    .line 324
    invoke-static {v0, v2}, Landroid/os/Process;->setThreadPriority(II)V

    .line 326
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/twitter/async/service/AsyncService$b;)V
    .locals 0

    .prologue
    .line 232
    invoke-direct {p0}, Lcom/twitter/async/service/AsyncService$b;->d()V

    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    .line 331
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->e:Lcom/twitter/async/service/AsyncOperation;

    invoke-virtual {v0}, Lcom/twitter/async/service/AsyncOperation;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->i:Lcom/twitter/async/service/j;

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->i:Lcom/twitter/async/service/j;

    iget-object v1, p0, Lcom/twitter/async/service/AsyncService$b;->e:Lcom/twitter/async/service/AsyncOperation;

    invoke-virtual {v1}, Lcom/twitter/async/service/AsyncOperation;->c()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/async/service/j;->a(Ljava/lang/Object;)Lcom/twitter/async/service/j;

    .line 334
    :cond_0
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->e:Lcom/twitter/async/service/AsyncOperation;

    iget-object v1, p0, Lcom/twitter/async/service/AsyncService$b;->i:Lcom/twitter/async/service/j;

    invoke-virtual {v0, v1}, Lcom/twitter/async/service/AsyncOperation;->c(Lcom/twitter/async/service/j;)Lcom/twitter/async/service/AsyncOperation;

    .line 337
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->e:Lcom/twitter/async/service/AsyncOperation;

    invoke-virtual {v0}, Lcom/twitter/async/service/AsyncOperation;->r()V

    .line 339
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->e:Lcom/twitter/async/service/AsyncOperation;

    iget-object v1, p0, Lcom/twitter/async/service/AsyncService$b;->i:Lcom/twitter/async/service/j;

    invoke-virtual {v0, v1}, Lcom/twitter/async/service/AsyncOperation;->a(Lcom/twitter/async/service/j;)V

    .line 341
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->a:Lcom/twitter/async/service/AsyncService;

    invoke-static {v0}, Lcom/twitter/async/service/AsyncService;->b(Lcom/twitter/async/service/AsyncService;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/twitter/async/service/AsyncService$a;

    iget-object v2, p0, Lcom/twitter/async/service/AsyncService$b;->a:Lcom/twitter/async/service/AsyncService;

    iget-object v3, p0, Lcom/twitter/async/service/AsyncService$b;->e:Lcom/twitter/async/service/AsyncOperation;

    invoke-direct {v1, v2, v3}, Lcom/twitter/async/service/AsyncService$a;-><init>(Lcom/twitter/async/service/AsyncService;Lcom/twitter/async/service/AsyncOperation;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 343
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->g:Lcom/twitter/async/service/AsyncService$d;

    invoke-virtual {v0}, Lcom/twitter/async/service/AsyncService$d;->b()V

    .line 346
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->a:Lcom/twitter/async/service/AsyncService;

    invoke-static {v0}, Lcom/twitter/async/service/AsyncService;->c(Lcom/twitter/async/service/AsyncService;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/async/service/AsyncService$b;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p0, :cond_2

    .line 347
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->a:Lcom/twitter/async/service/AsyncService;

    invoke-static {v0}, Lcom/twitter/async/service/AsyncService;->c(Lcom/twitter/async/service/AsyncService;)Ljava/util/Map;

    move-result-object v1

    monitor-enter v1

    .line 348
    :try_start_0
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->a:Lcom/twitter/async/service/AsyncService;

    invoke-static {v0}, Lcom/twitter/async/service/AsyncService;->c(Lcom/twitter/async/service/AsyncService;)Ljava/util/Map;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/async/service/AsyncService$b;->b:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p0, :cond_1

    .line 349
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->a:Lcom/twitter/async/service/AsyncService;

    invoke-static {v0}, Lcom/twitter/async/service/AsyncService;->c(Lcom/twitter/async/service/AsyncService;)Ljava/util/Map;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/async/service/AsyncService$b;->b:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    :cond_1
    monitor-exit v1

    .line 353
    :cond_2
    return-void

    .line 351
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic e(Lcom/twitter/async/service/AsyncService$b;)Z
    .locals 1

    .prologue
    .line 232
    invoke-direct {p0}, Lcom/twitter/async/service/AsyncService$b;->b()Z

    move-result v0

    return v0
.end method

.method static synthetic f(Lcom/twitter/async/service/AsyncService$b;)V
    .locals 0

    .prologue
    .line 232
    invoke-direct {p0}, Lcom/twitter/async/service/AsyncService$b;->e()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->g:Lcom/twitter/async/service/AsyncService$d;

    invoke-virtual {v0}, Lcom/twitter/async/service/AsyncService$d;->a()V

    .line 370
    return-void
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->h:Lcom/twitter/async/service/AsyncService$d;

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->h:Lcom/twitter/async/service/AsyncService$d;

    invoke-virtual {v0, p1}, Lcom/twitter/async/service/AsyncService$d;->a(Ljava/lang/Runnable;)V

    .line 365
    :goto_0
    return-void

    .line 363
    :cond_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public b(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->h:Lcom/twitter/async/service/AsyncService$d;

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$b;->h:Lcom/twitter/async/service/AsyncService$d;

    invoke-virtual {v0, p1}, Lcom/twitter/async/service/AsyncService$d;->b(Ljava/lang/Runnable;)V

    .line 381
    :goto_0
    return-void

    .line 379
    :cond_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 387
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 392
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 259
    invoke-static {}, Lcpd;->a()Lcpd;

    move-result-object v0

    invoke-virtual {v0}, Lcpd;->b()Lcpa;

    move-result-object v0

    new-instance v1, Lcom/twitter/async/service/AsyncService$b$1;

    invoke-direct {v1, p0}, Lcom/twitter/async/service/AsyncService$b$1;-><init>(Lcom/twitter/async/service/AsyncService$b;)V

    invoke-virtual {v0, v1}, Lcpa;->a(Lcom/twitter/util/concurrent/i;)Ljava/lang/Object;

    .line 286
    return-void
.end method
