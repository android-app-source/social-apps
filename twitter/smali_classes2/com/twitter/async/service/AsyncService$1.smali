.class Lcom/twitter/async/service/AsyncService$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/async/service/AsyncService;->a(Lcom/twitter/async/service/AsyncOperation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/Runnable;

.field final synthetic b:Lcom/twitter/async/service/AsyncOperation;

.field final synthetic c:Lcom/twitter/async/service/AsyncService$b;

.field final synthetic d:Lcom/twitter/async/service/AsyncService;


# direct methods
.method constructor <init>(Lcom/twitter/async/service/AsyncService;Ljava/lang/Runnable;Lcom/twitter/async/service/AsyncOperation;Lcom/twitter/async/service/AsyncService$b;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/twitter/async/service/AsyncService$1;->d:Lcom/twitter/async/service/AsyncService;

    iput-object p2, p0, Lcom/twitter/async/service/AsyncService$1;->a:Ljava/lang/Runnable;

    iput-object p3, p0, Lcom/twitter/async/service/AsyncService$1;->b:Lcom/twitter/async/service/AsyncOperation;

    iput-object p4, p0, Lcom/twitter/async/service/AsyncService$1;->c:Lcom/twitter/async/service/AsyncService$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/async/service/AsyncService$1;Lcom/twitter/async/service/AsyncService$b;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/twitter/async/service/AsyncService$1;->a(Lcom/twitter/async/service/AsyncService$b;)V

    return-void
.end method

.method private a(Lcom/twitter/async/service/AsyncService$b;)V
    .locals 1

    .prologue
    .line 139
    new-instance v0, Lcom/twitter/async/service/AsyncService$1$2;

    invoke-direct {v0, p0, p1}, Lcom/twitter/async/service/AsyncService$1$2;-><init>(Lcom/twitter/async/service/AsyncService$1;Lcom/twitter/async/service/AsyncService$b;)V

    invoke-virtual {p1, v0}, Lcom/twitter/async/service/AsyncService$b;->b(Ljava/lang/Runnable;)V

    .line 145
    invoke-virtual {p1}, Lcom/twitter/async/service/AsyncService$b;->a()V

    .line 146
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 115
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$1;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$1;->b:Lcom/twitter/async/service/AsyncOperation;

    invoke-virtual {v0}, Lcom/twitter/async/service/AsyncOperation;->i()I

    move-result v0

    .line 117
    iget-object v1, p0, Lcom/twitter/async/service/AsyncService$1;->d:Lcom/twitter/async/service/AsyncService;

    .line 118
    invoke-static {v1}, Lcom/twitter/async/service/AsyncService;->a(Lcom/twitter/async/service/AsyncService;)Lcom/twitter/async/service/c;

    move-result-object v1

    sget-object v2, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;->c:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    invoke-virtual {v1, v2}, Lcom/twitter/async/service/c;->a(Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    .line 119
    new-instance v2, Lcom/twitter/async/service/AsyncService$1$1;

    invoke-direct {v2, p0, v0}, Lcom/twitter/async/service/AsyncService$1$1;-><init>(Lcom/twitter/async/service/AsyncService$1;I)V

    .line 132
    invoke-static {v1, v2}, Lcom/twitter/async/service/AsyncService;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 136
    :goto_0
    return-void

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$1;->c:Lcom/twitter/async/service/AsyncService$b;

    invoke-direct {p0, v0}, Lcom/twitter/async/service/AsyncService$1;->a(Lcom/twitter/async/service/AsyncService$b;)V

    goto :goto_0
.end method
