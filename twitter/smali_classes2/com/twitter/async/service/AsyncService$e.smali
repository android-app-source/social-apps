.class public Lcom/twitter/async/service/AsyncService$e;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/async/service/AsyncService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/async/service/AsyncService;

.field private b:Lcom/twitter/async/service/AsyncService$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/async/service/AsyncService$b",
            "<TS;>;"
        }
    .end annotation
.end field

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/twitter/async/service/AsyncService;Lcom/twitter/async/service/AsyncService$b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/AsyncService$b",
            "<TS;>;)V"
        }
    .end annotation

    .prologue
    .line 443
    iput-object p1, p0, Lcom/twitter/async/service/AsyncService$e;->a:Lcom/twitter/async/service/AsyncService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 444
    iput-object p2, p0, Lcom/twitter/async/service/AsyncService$e;->b:Lcom/twitter/async/service/AsyncService$b;

    .line 445
    return-void
.end method

.method static synthetic a(Lcom/twitter/async/service/AsyncService$e;)Lcom/twitter/async/service/AsyncService$b;
    .locals 1

    .prologue
    .line 439
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$e;->b:Lcom/twitter/async/service/AsyncService$b;

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 449
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 454
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 460
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$e;->a:Lcom/twitter/async/service/AsyncService;

    invoke-static {v0}, Lcom/twitter/async/service/AsyncService;->d(Lcom/twitter/async/service/AsyncService;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 461
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$e;->a:Lcom/twitter/async/service/AsyncService;

    invoke-static {v0}, Lcom/twitter/async/service/AsyncService;->b(Lcom/twitter/async/service/AsyncService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 463
    monitor-enter p0

    .line 464
    :try_start_0
    iget-boolean v0, p0, Lcom/twitter/async/service/AsyncService$e;->c:Z

    if-eqz v0, :cond_0

    .line 465
    monitor-exit p0

    .line 471
    :goto_0
    return-void

    .line 467
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/async/service/AsyncService$e;->c:Z

    .line 468
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 469
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$e;->a:Lcom/twitter/async/service/AsyncService;

    iget-object v1, p0, Lcom/twitter/async/service/AsyncService$e;->b:Lcom/twitter/async/service/AsyncService$b;

    invoke-virtual {v0, v1}, Lcom/twitter/async/service/AsyncService;->a(Lcom/twitter/async/service/AsyncService$b;)V

    .line 470
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/async/service/AsyncService$e;->b:Lcom/twitter/async/service/AsyncService$b;

    goto :goto_0

    .line 468
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
