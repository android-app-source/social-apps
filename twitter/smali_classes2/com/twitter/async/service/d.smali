.class public Lcom/twitter/async/service/d;
.super Ljava/util/concurrent/AbstractExecutorService;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field private final b:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "Lcom/twitter/async/service/g;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/concurrent/locks/ReentrantLock;

.field private final d:Ljava/util/concurrent/locks/Condition;

.field private e:Lcom/twitter/async/service/g;

.field private f:Z


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/util/concurrent/AbstractExecutorService;-><init>()V

    .line 35
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/twitter/async/service/d;->b:Ljava/util/ArrayDeque;

    .line 36
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/twitter/async/service/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    .line 37
    iget-object v0, p0, Lcom/twitter/async/service/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/async/service/d;->d:Ljava/util/concurrent/locks/Condition;

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/async/service/d;->f:Z

    .line 42
    iput-object p1, p0, Lcom/twitter/async/service/d;->a:Ljava/util/concurrent/ExecutorService;

    .line 43
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/twitter/async/service/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 97
    :try_start_0
    iget-object v0, p0, Lcom/twitter/async/service/d;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/async/service/g;

    iput-object v0, p0, Lcom/twitter/async/service/d;->e:Lcom/twitter/async/service/g;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/twitter/async/service/d;->a:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcom/twitter/async/service/d;->e:Lcom/twitter/async/service/g;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/twitter/async/service/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 103
    return-void

    .line 101
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/twitter/async/service/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 144
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    .line 145
    iget-object v2, p0, Lcom/twitter/async/service/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 148
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/async/service/d;->isTerminated()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 149
    const/4 v0, 0x1

    .line 157
    iget-object v1, p0, Lcom/twitter/async/service/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 152
    :goto_1
    return v0

    .line 151
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_1

    .line 152
    const/4 v0, 0x0

    .line 157
    iget-object v1, p0, Lcom/twitter/async/service/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_1

    .line 154
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/twitter/async/service/d;->d:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v2, v0, v1}, Ljava/util/concurrent/locks/Condition;->awaitNanos(J)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    goto :goto_0

    .line 157
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/twitter/async/service/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public execute(Ljava/lang/Runnable;)V
    .locals 4

    .prologue
    .line 64
    iget-object v1, p0, Lcom/twitter/async/service/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 67
    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/async/service/d;->isShutdown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    new-instance v1, Ljava/util/concurrent/RejectedExecutionException;

    invoke-direct {v1}, Ljava/util/concurrent/RejectedExecutionException;-><init>()V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/twitter/async/service/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1

    .line 71
    :cond_0
    :try_start_1
    move-object v0, p1

    check-cast v0, Lcom/twitter/async/service/f;

    move-object v1, v0

    .line 72
    iget-object v2, p0, Lcom/twitter/async/service/d;->b:Ljava/util/ArrayDeque;

    new-instance v3, Lcom/twitter/async/service/d$2;

    iget v1, v1, Lcom/twitter/async/service/f;->a:I

    invoke-direct {v3, p0, v1, p1}, Lcom/twitter/async/service/d$2;-><init>(Lcom/twitter/async/service/d;ILjava/lang/Runnable;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayDeque;->offer(Ljava/lang/Object;)Z

    .line 86
    iget-object v1, p0, Lcom/twitter/async/service/d;->e:Lcom/twitter/async/service/g;

    if-nez v1, :cond_1

    .line 87
    invoke-virtual {p0}, Lcom/twitter/async/service/d;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90
    :cond_1
    iget-object v1, p0, Lcom/twitter/async/service/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 92
    return-void
.end method

.method public isShutdown()Z
    .locals 1

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/twitter/async/service/d;->f:Z

    return v0
.end method

.method public isTerminated()Z
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/twitter/async/service/d;->isShutdown()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/async/service/d;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected newTaskFor(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/RunnableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Runnable;",
            "TS;)",
            "Ljava/util/concurrent/RunnableFuture",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 49
    instance-of v0, p1, Lcom/twitter/async/service/g;

    if-eqz v0, :cond_0

    .line 50
    check-cast p1, Lcom/twitter/async/service/g;

    .line 59
    :goto_0
    new-instance v0, Lcom/twitter/async/service/f;

    invoke-direct {v0, p1, p2}, Lcom/twitter/async/service/f;-><init>(Lcom/twitter/async/service/g;Ljava/lang/Object;)V

    return-object v0

    .line 52
    :cond_0
    new-instance v0, Lcom/twitter/async/service/d$1;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1, p1}, Lcom/twitter/async/service/d$1;-><init>(Lcom/twitter/async/service/d;ILjava/lang/Runnable;)V

    move-object p1, v0

    goto :goto_0
.end method

.method public shutdown()V
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/async/service/d;->f:Z

    .line 108
    return-void
.end method

.method public shutdownNow()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Lcom/twitter/async/service/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/twitter/async/service/d;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 120
    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/async/service/d;->shutdown()V

    .line 121
    :goto_0
    iget-object v1, p0, Lcom/twitter/async/service/d;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 122
    iget-object v1, p0, Lcom/twitter/async/service/d;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->poll()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 125
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/twitter/async/service/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/twitter/async/service/d;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 128
    return-object v0
.end method
