.class public Lcom/twitter/async/service/i;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private a:Lcom/twitter/async/service/AsyncService;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/async/service/AsyncOperation;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/async/service/AsyncOperation;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/async/service/AsyncOperation;",
            ">;"
        }
    .end annotation
.end field

.field private final e:[I

.field private final f:Lcom/twitter/async/service/AsyncOperation$b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/async/service/i;->b:Ljava/util/Map;

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/async/service/i;->c:Ljava/util/Map;

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/async/service/i;->d:Ljava/util/List;

    .line 26
    const/4 v0, 0x0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/twitter/async/service/i;->e:[I

    .line 31
    new-instance v0, Lcom/twitter/async/service/i$1;

    invoke-direct {v0, p0}, Lcom/twitter/async/service/i$1;-><init>(Lcom/twitter/async/service/i;)V

    iput-object v0, p0, Lcom/twitter/async/service/i;->f:Lcom/twitter/async/service/AsyncOperation$b;

    return-void
.end method

.method static synthetic a(Lcom/twitter/async/service/i;)[I
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/twitter/async/service/i;->e:[I

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/async/service/i;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/twitter/async/service/i;->b:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/async/service/i;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/twitter/async/service/i;->c:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 63
    iget-object v0, p1, Lcom/twitter/async/service/AsyncOperation;->d:Ljava/lang/String;

    .line 65
    iget-object v1, p0, Lcom/twitter/async/service/i;->f:Lcom/twitter/async/service/AsyncOperation$b;

    invoke-virtual {p1, v1}, Lcom/twitter/async/service/AsyncOperation;->a(Lcom/twitter/async/service/AsyncOperation$b;)Lcom/twitter/async/service/AsyncOperation;

    .line 66
    iget-object v1, p0, Lcom/twitter/async/service/i;->e:[I

    monitor-enter v1

    .line 67
    :try_start_0
    iget-object v2, p0, Lcom/twitter/async/service/i;->a:Lcom/twitter/async/service/AsyncService;

    if-eqz v2, :cond_0

    .line 68
    iget-object v2, p0, Lcom/twitter/async/service/i;->c:Ljava/util/Map;

    invoke-interface {v2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    iget-object v2, p0, Lcom/twitter/async/service/i;->a:Lcom/twitter/async/service/AsyncService;

    invoke-virtual {v2, p1}, Lcom/twitter/async/service/AsyncService;->a(Lcom/twitter/async/service/AsyncOperation;)V

    .line 73
    :goto_0
    monitor-exit v1

    .line 74
    return-object v0

    .line 71
    :cond_0
    iget-object v2, p0, Lcom/twitter/async/service/i;->b:Ljava/util/Map;

    invoke-interface {v2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/twitter/async/service/e;)V
    .locals 2

    .prologue
    .line 158
    iget-object v1, p0, Lcom/twitter/async/service/i;->e:[I

    monitor-enter v1

    .line 159
    :try_start_0
    iget-object v0, p0, Lcom/twitter/async/service/i;->a:Lcom/twitter/async/service/AsyncService;

    .line 160
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 162
    if-eqz v0, :cond_0

    .line 163
    invoke-virtual {v0, p1}, Lcom/twitter/async/service/AsyncService;->a(Lcom/twitter/async/service/e;)V

    .line 165
    :cond_0
    return-void

    .line 160
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 136
    iget-object v1, p0, Lcom/twitter/async/service/i;->e:[I

    monitor-enter v1

    .line 138
    :try_start_0
    iget-object v0, p0, Lcom/twitter/async/service/i;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/async/service/AsyncOperation;

    .line 139
    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {v0, p2}, Lcom/twitter/async/service/AsyncOperation;->cancel(Z)Z

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/twitter/async/service/i;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    monitor-exit v1

    .line 147
    return-void

    .line 146
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 85
    iget-object v1, p0, Lcom/twitter/async/service/i;->e:[I

    monitor-enter v1

    .line 86
    :try_start_0
    iget-object v0, p0, Lcom/twitter/async/service/i;->d:Ljava/util/List;

    iget-object v2, p0, Lcom/twitter/async/service/i;->b:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 87
    iget-object v0, p0, Lcom/twitter/async/service/i;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 88
    iget-object v0, p0, Lcom/twitter/async/service/i;->d:Ljava/util/List;

    iget-object v2, p0, Lcom/twitter/async/service/i;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 89
    iget-object v0, p0, Lcom/twitter/async/service/i;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 90
    iget-object v0, p0, Lcom/twitter/async/service/i;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/async/service/AsyncOperation;

    .line 91
    invoke-virtual {v0, p1}, Lcom/twitter/async/service/AsyncOperation;->cancel(Z)Z

    goto :goto_0

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 93
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/twitter/async/service/i;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 94
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 95
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/async/service/i;->a:Lcom/twitter/async/service/AsyncService;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 121
    iget-object v1, p0, Lcom/twitter/async/service/i;->e:[I

    monitor-enter v1

    .line 122
    :try_start_0
    iget-object v0, p0, Lcom/twitter/async/service/i;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4

    .prologue
    .line 101
    instance-of v0, p2, Lcom/twitter/async/service/AsyncService$f;

    if-eqz v0, :cond_1

    .line 102
    iget-object v1, p0, Lcom/twitter/async/service/i;->e:[I

    monitor-enter v1

    .line 103
    :try_start_0
    check-cast p2, Lcom/twitter/async/service/AsyncService$f;

    invoke-virtual {p2}, Lcom/twitter/async/service/AsyncService$f;->a()Lcom/twitter/async/service/AsyncService;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/async/service/i;->a:Lcom/twitter/async/service/AsyncService;

    .line 104
    iget-object v0, p0, Lcom/twitter/async/service/i;->c:Ljava/util/Map;

    iget-object v2, p0, Lcom/twitter/async/service/i;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 105
    iget-object v0, p0, Lcom/twitter/async/service/i;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/async/service/AsyncOperation;

    .line 106
    iget-object v3, p0, Lcom/twitter/async/service/i;->a:Lcom/twitter/async/service/AsyncService;

    invoke-virtual {v3, v0}, Lcom/twitter/async/service/AsyncService;->a(Lcom/twitter/async/service/AsyncOperation;)V

    goto :goto_0

    .line 109
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 108
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/twitter/async/service/i;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 109
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111
    :cond_1
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 115
    iget-object v1, p0, Lcom/twitter/async/service/i;->e:[I

    monitor-enter v1

    .line 116
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/twitter/async/service/i;->a:Lcom/twitter/async/service/AsyncService;

    .line 117
    monitor-exit v1

    .line 118
    return-void

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
