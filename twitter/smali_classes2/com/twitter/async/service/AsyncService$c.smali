.class Lcom/twitter/async/service/AsyncService$c;
.super Lcom/twitter/async/service/g;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/async/service/AsyncService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "c"
.end annotation


# instance fields
.field private final a:Lcom/twitter/async/service/e;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/async/service/AsyncService$e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/twitter/async/service/e;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/async/service/e;",
            "Ljava/util/Set",
            "<",
            "Lcom/twitter/async/service/AsyncService$e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 206
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/async/service/g;-><init>(I)V

    .line 207
    iput-object p1, p0, Lcom/twitter/async/service/AsyncService$c;->a:Lcom/twitter/async/service/e;

    .line 208
    iput-object p2, p0, Lcom/twitter/async/service/AsyncService$c;->b:Ljava/util/Set;

    .line 209
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 214
    iget-object v0, p0, Lcom/twitter/async/service/AsyncService$c;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/async/service/AsyncService$e;

    .line 215
    invoke-static {v0}, Lcom/twitter/async/service/AsyncService$e;->a(Lcom/twitter/async/service/AsyncService$e;)Lcom/twitter/async/service/AsyncService$b;

    move-result-object v0

    .line 216
    if-eqz v0, :cond_0

    .line 217
    invoke-static {v0}, Lcom/twitter/async/service/AsyncService$b;->b(Lcom/twitter/async/service/AsyncService$b;)Lcom/twitter/async/service/AsyncOperation;

    move-result-object v2

    .line 218
    invoke-static {v0}, Lcom/twitter/async/service/AsyncService$b;->c(Lcom/twitter/async/service/AsyncService$b;)Lcom/twitter/async/service/j;

    move-result-object v0

    .line 219
    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/twitter/async/service/AsyncService$c;->a:Lcom/twitter/async/service/e;

    .line 220
    invoke-virtual {v2, v3, v0}, Lcom/twitter/async/service/AsyncOperation;->a(Lcom/twitter/async/service/e;Lcom/twitter/async/service/j;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    invoke-virtual {v2}, Lcom/twitter/async/service/AsyncOperation;->k()V

    goto :goto_0

    .line 225
    :cond_1
    return-void
.end method
