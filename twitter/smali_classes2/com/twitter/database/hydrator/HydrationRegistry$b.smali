.class public Lcom/twitter/database/hydrator/HydrationRegistry$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/database/hydrator/HydrationRegistry$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/database/hydrator/HydrationRegistry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Class;Ljava/lang/Class;Lcbr;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<GETTER:",
            "Ljava/lang/Object;",
            "MODE",
            "L:Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TGETTER;>;",
            "Ljava/lang/Class",
            "<TMODE",
            "L;",
            ">;",
            "Lcbr",
            "<TGETTER;TMODE",
            "L;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-static {p1, p2, p3}, Lcom/twitter/database/hydrator/b;->a(Ljava/lang/Class;Ljava/lang/Class;Lcbr;)V

    .line 61
    return-void
.end method

.method public a(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Lcom/twitter/database/hydrator/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<MODE",
            "L:Ljava/lang/Object;",
            "SETTER:",
            "Ljava/lang/Object;",
            "WRITER::",
            "Lcom/twitter/database/model/m",
            "<TSETTER;>;>(",
            "Ljava/lang/Class",
            "<TMODE",
            "L;",
            ">;",
            "Ljava/lang/Class",
            "<TSETTER;>;",
            "Ljava/lang/Class",
            "<TWRITER;>;",
            "Lcom/twitter/database/hydrator/a",
            "<TMODE",
            "L;",
            "TSETTER;>;)V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-static {p1, p2, p3, p4}, Lcom/twitter/database/hydrator/b;->a(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Lcom/twitter/database/hydrator/a;)V

    .line 68
    return-void
.end method
