.class public final Lcom/twitter/database/hydrator/TwitterHydrationRegistrar;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/database/hydrator/HydrationRegistry$Registrar;


# annotations
.annotation build Lcod;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/database/hydrator/HydrationRegistry$a;)V
    .locals 4

    .prologue
    .line 47
    const-class v0, Lawu$a;

    const-class v1, Lcom/twitter/model/dms/c;

    new-instance v2, Lavl;

    invoke-direct {v2}, Lavl;-><init>()V

    invoke-interface {p1, v0, v1, v2}, Lcom/twitter/database/hydrator/HydrationRegistry$a;->a(Ljava/lang/Class;Ljava/lang/Class;Lcbr;)V

    .line 48
    const-class v0, Lawy$a;

    const-class v1, Lcom/twitter/model/dms/i;

    new-instance v2, Lavn;

    invoke-direct {v2}, Lavn;-><init>()V

    invoke-interface {p1, v0, v1, v2}, Lcom/twitter/database/hydrator/HydrationRegistry$a;->a(Ljava/lang/Class;Ljava/lang/Class;Lcbr;)V

    .line 49
    const-class v0, Lawx$a;

    const-class v1, Lcom/twitter/model/dms/m;

    new-instance v2, Lavm;

    invoke-direct {v2}, Lavm;-><init>()V

    invoke-interface {p1, v0, v1, v2}, Lcom/twitter/database/hydrator/HydrationRegistry$a;->a(Ljava/lang/Class;Ljava/lang/Class;Lcbr;)V

    .line 50
    const-class v0, Lawz$a;

    const-class v1, Lcom/twitter/model/dms/q;

    new-instance v2, Lavp;

    invoke-direct {v2}, Lavp;-><init>()V

    invoke-interface {p1, v0, v1, v2}, Lcom/twitter/database/hydrator/HydrationRegistry$a;->a(Ljava/lang/Class;Ljava/lang/Class;Lcbr;)V

    .line 51
    const-class v0, Lcom/twitter/database/schema/DraftsSchema$a$a;

    const-class v1, Lcom/twitter/model/drafts/a;

    new-instance v2, Lavt;

    invoke-direct {v2}, Lavt;-><init>()V

    invoke-interface {p1, v0, v1, v2}, Lcom/twitter/database/hydrator/HydrationRegistry$a;->a(Ljava/lang/Class;Ljava/lang/Class;Lcbr;)V

    .line 52
    const-class v0, Laxb$a;

    const-class v1, Lcfx;

    new-instance v2, Lavv;

    invoke-direct {v2}, Lavv;-><init>()V

    invoke-interface {p1, v0, v1, v2}, Lcom/twitter/database/hydrator/HydrationRegistry$a;->a(Ljava/lang/Class;Ljava/lang/Class;Lcbr;)V

    .line 53
    const-class v0, Laww$a;

    const-class v1, Lcom/twitter/model/dms/Participant;

    new-instance v2, Lavr;

    invoke-direct {v2}, Lavr;-><init>()V

    invoke-interface {p1, v0, v1, v2}, Lcom/twitter/database/hydrator/HydrationRegistry$a;->a(Ljava/lang/Class;Ljava/lang/Class;Lcbr;)V

    .line 54
    const-class v0, Laxs$a;

    const-class v1, Lcom/twitter/model/core/aa;

    new-instance v2, Lavu;

    invoke-direct {v2}, Lavu;-><init>()V

    invoke-interface {p1, v0, v1, v2}, Lcom/twitter/database/hydrator/HydrationRegistry$a;->a(Ljava/lang/Class;Ljava/lang/Class;Lcbr;)V

    .line 55
    const-class v0, Laxp$a;

    const-class v1, Lcom/twitter/model/core/TwitterUser;

    new-instance v2, Lavy;

    invoke-direct {v2}, Lavy;-><init>()V

    invoke-interface {p1, v0, v1, v2}, Lcom/twitter/database/hydrator/HydrationRegistry$a;->a(Ljava/lang/Class;Ljava/lang/Class;Lcbr;)V

    .line 56
    const-class v0, Lcec;

    const-class v1, Lazl$a;

    const-class v2, Lazl;

    new-instance v3, Lavx;

    invoke-direct {v3}, Lavx;-><init>()V

    invoke-interface {p1, v0, v1, v2, v3}, Lcom/twitter/database/hydrator/HydrationRegistry$a;->a(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Lcom/twitter/database/hydrator/a;)V

    .line 57
    const-class v0, Lcdu;

    const-class v1, Lazj$a;

    const-class v2, Lazj;

    new-instance v3, Lavw;

    invoke-direct {v3}, Lavw;-><init>()V

    invoke-interface {p1, v0, v1, v2, v3}, Lcom/twitter/database/hydrator/HydrationRegistry$a;->a(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Lcom/twitter/database/hydrator/a;)V

    .line 58
    const-class v0, Lcom/twitter/model/dms/Participant;

    const-class v1, Lawv$b$a;

    const-class v2, Lawv$b;

    new-instance v3, Lavq;

    invoke-direct {v3}, Lavq;-><init>()V

    invoke-interface {p1, v0, v1, v2, v3}, Lcom/twitter/database/hydrator/HydrationRegistry$a;->a(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Lcom/twitter/database/hydrator/a;)V

    .line 59
    const-class v0, Lcom/twitter/model/drafts/a;

    const-class v1, Lcom/twitter/database/schema/DraftsSchema$c$a;

    const-class v2, Lcom/twitter/database/schema/DraftsSchema$c;

    new-instance v3, Lavs;

    invoke-direct {v3}, Lavs;-><init>()V

    invoke-interface {p1, v0, v1, v2, v3}, Lcom/twitter/database/hydrator/HydrationRegistry$a;->a(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Lcom/twitter/database/hydrator/a;)V

    .line 60
    return-void
.end method
