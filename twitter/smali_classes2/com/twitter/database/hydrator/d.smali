.class public Lcom/twitter/database/hydrator/d;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/database/hydrator/d$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/database/model/i;",
            "Lcom/twitter/database/hydrator/d;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/twitter/database/model/i;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/twitter/database/hydrator/d;->a:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/database/model/i;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/twitter/database/hydrator/d;->b:Lcom/twitter/database/model/i;

    .line 43
    return-void
.end method

.method private a(Ljava/lang/Object;ZLcom/twitter/database/hydrator/d$a;)J
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<MODE",
            "L:Ljava/lang/Object;",
            "SETTER:",
            "Ljava/lang/Object;",
            ">(TMODE",
            "L;",
            "Z",
            "Lcom/twitter/database/hydrator/d$a",
            "<TSETTER;>;)J"
        }
    .end annotation

    .prologue
    .line 158
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 160
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/database/hydrator/b;->a(Ljava/lang/Class;)Lcom/twitter/database/hydrator/b$b;

    move-result-object v0

    .line 161
    if-nez v0, :cond_0

    .line 162
    const-wide/16 v0, -0x1

    .line 171
    :goto_0
    return-wide v0

    .line 165
    :cond_0
    iget-object v1, p0, Lcom/twitter/database/hydrator/d;->b:Lcom/twitter/database/model/i;

    iget-object v2, v0, Lcom/twitter/database/hydrator/b$b;->c:Ljava/lang/Class;

    invoke-interface {v1, v2}, Lcom/twitter/database/model/i;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v1

    invoke-interface {v1}, Lcom/twitter/database/model/m;->b()Lcom/twitter/database/model/h;

    move-result-object v1

    .line 166
    iget-object v0, v0, Lcom/twitter/database/hydrator/b$b;->a:Lcom/twitter/database/hydrator/a;

    .line 167
    iget-object v2, v1, Lcom/twitter/database/model/h;->d:Ljava/lang/Object;

    invoke-virtual {v0, p1, v2}, Lcom/twitter/database/hydrator/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    if-eqz p3, :cond_1

    .line 169
    iget-object v0, v1, Lcom/twitter/database/model/h;->d:Ljava/lang/Object;

    invoke-interface {p3, v0}, Lcom/twitter/database/hydrator/d$a;->a(Ljava/lang/Object;)V

    .line 171
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {v1}, Lcom/twitter/database/model/h;->c()J

    move-result-wide v0

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lcom/twitter/database/model/h;->b()J

    move-result-wide v0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/database/model/i;)Lcom/twitter/database/hydrator/d;
    .locals 3

    .prologue
    .line 31
    sget-object v1, Lcom/twitter/database/hydrator/d;->a:Ljava/util/Map;

    monitor-enter v1

    .line 32
    :try_start_0
    sget-object v0, Lcom/twitter/database/hydrator/d;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/database/hydrator/d;

    .line 33
    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/twitter/database/hydrator/d;

    invoke-direct {v0, p0}, Lcom/twitter/database/hydrator/d;-><init>(Lcom/twitter/database/model/i;)V

    .line 35
    sget-object v2, Lcom/twitter/database/hydrator/d;->a:Ljava/util/Map;

    invoke-interface {v2, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    :cond_0
    monitor-exit v1

    .line 38
    return-object v0

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Ljava/util/Collection;Ljava/lang/Class;ZLcom/twitter/database/hydrator/d$a;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<MODE",
            "L:Ljava/lang/Object;",
            "SETTER:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<TMODE",
            "L;",
            ">;",
            "Ljava/lang/Class",
            "<TMODE",
            "L;",
            ">;Z",
            "Lcom/twitter/database/hydrator/d$a",
            "<TSETTER;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 123
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 125
    invoke-static {p1}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 149
    :goto_0
    return v0

    .line 129
    :cond_0
    invoke-static {p2}, Lcom/twitter/database/hydrator/b;->a(Ljava/lang/Class;)Lcom/twitter/database/hydrator/b$b;

    move-result-object v2

    .line 130
    if-nez v2, :cond_1

    move v0, v1

    .line 131
    goto :goto_0

    .line 134
    :cond_1
    iget-object v3, p0, Lcom/twitter/database/hydrator/d;->b:Lcom/twitter/database/model/i;

    invoke-interface {v3}, Lcom/twitter/database/model/i;->h()Lcom/twitter/database/model/o;

    move-result-object v4

    .line 136
    :try_start_0
    iget-object v3, p0, Lcom/twitter/database/hydrator/d;->b:Lcom/twitter/database/model/i;

    iget-object v5, v2, Lcom/twitter/database/hydrator/b$b;->c:Ljava/lang/Class;

    invoke-interface {v3, v5}, Lcom/twitter/database/model/i;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v3

    invoke-interface {v3}, Lcom/twitter/database/model/m;->b()Lcom/twitter/database/model/h;

    move-result-object v5

    .line 137
    iget-object v6, v2, Lcom/twitter/database/hydrator/b$b;->a:Lcom/twitter/database/hydrator/a;

    .line 138
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 139
    iget-object v3, v5, Lcom/twitter/database/model/h;->d:Ljava/lang/Object;

    invoke-virtual {v6, v2, v3}, Lcom/twitter/database/hydrator/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    if-eqz p4, :cond_3

    .line 141
    iget-object v2, v5, Lcom/twitter/database/model/h;->d:Ljava/lang/Object;

    invoke-interface {p4, v2}, Lcom/twitter/database/hydrator/d$a;->a(Ljava/lang/Object;)V

    .line 143
    :cond_3
    if-eqz p3, :cond_4

    invoke-virtual {v5}, Lcom/twitter/database/model/h;->c()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 144
    :goto_1
    const-wide/16 v8, -0x1

    cmp-long v2, v2, v8

    if-nez v2, :cond_2

    .line 151
    invoke-interface {v4}, Lcom/twitter/database/model/o;->close()V

    move v0, v1

    .line 145
    goto :goto_0

    .line 143
    :cond_4
    :try_start_1
    invoke-virtual {v5}, Lcom/twitter/database/model/h;->b()J

    move-result-wide v2

    goto :goto_1

    .line 148
    :cond_5
    invoke-interface {v4}, Lcom/twitter/database/model/o;->a()Lcom/twitter/database/model/o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151
    invoke-interface {v4}, Lcom/twitter/database/model/o;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v4}, Lcom/twitter/database/model/o;->close()V

    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/Class;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<SETTER:",
            "Ljava/lang/Object;",
            "WRITER::",
            "Lcom/twitter/database/model/m",
            "<TSETTER;>;>(",
            "Ljava/lang/Class",
            "<TWRITER;>;)I"
        }
    .end annotation

    .prologue
    .line 47
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 48
    iget-object v0, p0, Lcom/twitter/database/hydrator/d;->b:Lcom/twitter/database/model/i;

    invoke-interface {v0, p1}, Lcom/twitter/database/model/i;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/twitter/database/model/m;->a(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/Class;Lcom/twitter/database/model/f;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<SETTER:",
            "Ljava/lang/Object;",
            "WRITER::",
            "Lcom/twitter/database/model/m",
            "<TSETTER;>;>(",
            "Ljava/lang/Class",
            "<TWRITER;>;",
            "Lcom/twitter/database/model/f;",
            ")I"
        }
    .end annotation

    .prologue
    .line 54
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 55
    if-eqz p2, :cond_0

    .line 56
    iget-object v0, p0, Lcom/twitter/database/hydrator/d;->b:Lcom/twitter/database/model/i;

    invoke-interface {v0, p1}, Lcom/twitter/database/model/i;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/database/model/f;->a:Ljava/lang/String;

    iget-object v2, p2, Lcom/twitter/database/model/f;->b:[Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/twitter/database/model/m;->a(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 58
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/twitter/database/hydrator/d;->a(Ljava/lang/Class;)I

    move-result v0

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;Lcom/twitter/database/hydrator/d$a;)J
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<MODE",
            "L:Ljava/lang/Object;",
            "SETTER:",
            "Ljava/lang/Object;",
            ">(TMODE",
            "L;",
            "Lcom/twitter/database/hydrator/d$a",
            "<TSETTER;>;)J"
        }
    .end annotation

    .prologue
    .line 64
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/database/hydrator/d;->a(Ljava/lang/Object;ZLcom/twitter/database/hydrator/d$a;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Ljava/lang/Object;)Lcom/twitter/database/model/h;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<MODE",
            "L:Ljava/lang/Object;",
            "SETTER:",
            "Ljava/lang/Object;",
            ">(TMODE",
            "L;",
            ")",
            "Lcom/twitter/database/model/h",
            "<TSETTER;>;"
        }
    .end annotation

    .prologue
    .line 107
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 109
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/database/hydrator/b;->a(Ljava/lang/Class;)Lcom/twitter/database/hydrator/b$b;

    move-result-object v1

    .line 110
    if-nez v1, :cond_0

    .line 111
    const/4 v0, 0x0

    .line 117
    :goto_0
    return-object v0

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/twitter/database/hydrator/d;->b:Lcom/twitter/database/model/i;

    iget-object v2, v1, Lcom/twitter/database/hydrator/b$b;->c:Ljava/lang/Class;

    invoke-interface {v0, v2}, Lcom/twitter/database/model/i;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/database/model/m;->b()Lcom/twitter/database/model/h;

    move-result-object v0

    .line 115
    iget-object v1, v1, Lcom/twitter/database/hydrator/b$b;->a:Lcom/twitter/database/hydrator/a;

    .line 116
    iget-object v2, v0, Lcom/twitter/database/model/h;->d:Ljava/lang/Object;

    invoke-virtual {v1, p1, v2}, Lcom/twitter/database/hydrator/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Ljava/util/Collection;Ljava/lang/Class;Lcom/twitter/database/hydrator/d$a;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<MODE",
            "L:Ljava/lang/Object;",
            "SETTER:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<TMODE",
            "L;",
            ">;",
            "Ljava/lang/Class",
            "<TMODE",
            "L;",
            ">;",
            "Lcom/twitter/database/hydrator/d$a",
            "<TSETTER;>;)Z"
        }
    .end annotation

    .prologue
    .line 96
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/twitter/database/hydrator/d;->a(Ljava/util/Collection;Ljava/lang/Class;ZLcom/twitter/database/hydrator/d$a;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/Object;Lcom/twitter/database/hydrator/d$a;)J
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<MODE",
            "L:Ljava/lang/Object;",
            "SETTER:",
            "Ljava/lang/Object;",
            ">(TMODE",
            "L;",
            "Lcom/twitter/database/hydrator/d$a",
            "<TSETTER;>;)J"
        }
    .end annotation

    .prologue
    .line 85
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/database/hydrator/d;->a(Ljava/lang/Object;ZLcom/twitter/database/hydrator/d$a;)J

    move-result-wide v0

    return-wide v0
.end method
