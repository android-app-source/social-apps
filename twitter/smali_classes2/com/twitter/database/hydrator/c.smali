.class public Lcom/twitter/database/hydrator/c;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/database/model/i;",
            "Lcom/twitter/database/hydrator/c;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/twitter/database/model/i;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/twitter/database/hydrator/c;->a:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/database/model/i;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/twitter/database/hydrator/c;->b:Lcom/twitter/database/model/i;

    .line 47
    return-void
.end method

.method private static a(Lcom/twitter/database/model/g;Ljava/lang/Class;)Lcbr;
    .locals 4

    .prologue
    .line 91
    iget-object v0, p0, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/twitter/database/hydrator/b;->a(Ljava/lang/Class;Ljava/lang/Class;)Lcbr;

    move-result-object v1

    .line 92
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Could not find hydrator to hydrate "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    .line 93
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 92
    invoke-static {v0, v2}, Lcom/twitter/util/f;->a(ZLjava/lang/String;)Z

    .line 94
    return-object v1

    .line 92
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/database/model/i;)Lcom/twitter/database/hydrator/c;
    .locals 3

    .prologue
    .line 35
    sget-object v1, Lcom/twitter/database/hydrator/c;->a:Ljava/util/Map;

    monitor-enter v1

    .line 36
    :try_start_0
    sget-object v0, Lcom/twitter/database/hydrator/c;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/database/hydrator/c;

    .line 37
    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/twitter/database/hydrator/c;

    invoke-direct {v0, p0}, Lcom/twitter/database/hydrator/c;-><init>(Lcom/twitter/database/model/i;)V

    .line 39
    sget-object v2, Lcom/twitter/database/hydrator/c;->a:Ljava/util/Map;

    invoke-interface {v2, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    :cond_0
    monitor-exit v1

    .line 42
    return-object v0

    .line 41
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(Lcom/twitter/database/model/l;Lcom/twitter/database/model/f;Ljava/lang/Class;)Lcbi;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S::",
            "Lcom/twitter/database/model/k;",
            "D:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/twitter/database/model/l;",
            "Lcom/twitter/database/model/f;",
            "Ljava/lang/Class",
            "<TD;>;)",
            "Lcbi",
            "<TD;>;"
        }
    .end annotation

    .prologue
    .line 73
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 74
    invoke-interface {p1, p2}, Lcom/twitter/database/model/l;->a(Lcom/twitter/database/model/f;)Lcom/twitter/database/model/g;

    move-result-object v1

    .line 75
    invoke-virtual {v1}, Lcom/twitter/database/model/g;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 77
    invoke-static {}, Lcbi;->f()Lcbi;

    move-result-object v0

    .line 85
    :goto_0
    return-object v0

    .line 79
    :cond_0
    invoke-static {v1, p3}, Lcom/twitter/database/hydrator/c;->a(Lcom/twitter/database/model/g;Ljava/lang/Class;)Lcbr;

    move-result-object v2

    .line 80
    if-eqz v2, :cond_1

    .line 81
    new-instance v0, Lcbm;

    new-instance v3, Lauy;

    invoke-direct {v3, v1}, Lauy;-><init>(Lcom/twitter/database/model/g;)V

    invoke-direct {v0, v3, v2}, Lcbm;-><init>(Lcbi;Lcbs;)V

    goto :goto_0

    .line 84
    :cond_1
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 85
    invoke-static {}, Lcbi;->f()Lcbi;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/Class;Lcom/twitter/database/model/f;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S::",
            "Lcom/twitter/database/model/k;",
            "D:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Lcom/twitter/database/model/f;",
            "Ljava/lang/Class",
            "<TD;>;)TD;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 53
    iget-object v1, p0, Lcom/twitter/database/hydrator/c;->b:Lcom/twitter/database/model/i;

    invoke-interface {v1, p1}, Lcom/twitter/database/model/i;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v1

    invoke-interface {v1}, Lcom/twitter/database/model/k;->f()Lcom/twitter/database/model/l;

    move-result-object v1

    invoke-interface {v1, p2}, Lcom/twitter/database/model/l;->a(Lcom/twitter/database/model/f;)Lcom/twitter/database/model/g;

    move-result-object v1

    .line 55
    :try_start_0
    invoke-virtual {v1}, Lcom/twitter/database/model/g;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 65
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 62
    :goto_0
    return-object v0

    .line 58
    :cond_0
    :try_start_1
    invoke-static {v1, p3}, Lcom/twitter/database/hydrator/c;->a(Lcom/twitter/database/model/g;Ljava/lang/Class;)Lcbr;

    move-result-object v2

    .line 59
    if-eqz v2, :cond_1

    .line 60
    iget-object v0, v1, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    invoke-virtual {v2, v0}, Lcbr;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 65
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :cond_1
    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcqc;->a(Ljava/io/Closeable;)V

    throw v0
.end method

.method public a(Ljava/lang/Class;Lcom/twitter/database/model/f;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S::",
            "Lcom/twitter/database/model/k;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Lcom/twitter/database/model/f;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 106
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 107
    iget-object v0, p0, Lcom/twitter/database/hydrator/c;->b:Lcom/twitter/database/model/i;

    invoke-interface {v0, p1}, Lcom/twitter/database/model/i;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/database/model/k;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/twitter/database/model/l;->a(Lcom/twitter/database/model/f;)Lcom/twitter/database/model/g;

    move-result-object v0

    .line 109
    :try_start_0
    invoke-virtual {v0}, Lcom/twitter/database/model/g;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 111
    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 109
    return v1

    .line 111
    :catchall_0
    move-exception v1

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    throw v1
.end method

.method public b(Ljava/lang/Class;Lcom/twitter/database/model/f;Ljava/lang/Class;)Lcbi;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S::",
            "Lcom/twitter/database/model/k;",
            "D:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Lcom/twitter/database/model/f;",
            "Ljava/lang/Class",
            "<TD;>;)",
            "Lcbi",
            "<TD;>;"
        }
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, Lcom/twitter/database/hydrator/c;->b:Lcom/twitter/database/model/i;

    invoke-interface {v0, p1}, Lcom/twitter/database/model/i;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/database/model/k;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/twitter/database/hydrator/c;->a(Lcom/twitter/database/model/l;Lcom/twitter/database/model/f;Ljava/lang/Class;)Lcbi;

    move-result-object v0

    return-object v0
.end method
