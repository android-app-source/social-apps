.class public Lcom/twitter/database/hydrator/b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/database/hydrator/b$a;,
        Lcom/twitter/database/hydrator/b$b;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/database/hydrator/b$a;",
            "Lcbr;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class;",
            "Lcom/twitter/util/collection/o",
            "<",
            "Ljava/lang/Class;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class;",
            "Lcom/twitter/database/hydrator/b$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/twitter/database/hydrator/b;->a:Ljava/util/Map;

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/twitter/database/hydrator/b;->b:Ljava/util/Map;

    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/twitter/database/hydrator/b;->c:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/Class;)Lcbr;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<GETTER:",
            "Ljava/lang/Object;",
            "MODE",
            "L:Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TGETTER;>;",
            "Ljava/lang/Class",
            "<TMODE",
            "L;",
            ">;)",
            "Lcbr",
            "<-TGETTER;TMODE",
            "L;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    invoke-static {}, Lcom/twitter/database/hydrator/HydrationRegistry;->a()V

    .line 83
    sget-object v0, Lcom/twitter/database/hydrator/b;->a:Ljava/util/Map;

    sget-object v1, Lcom/twitter/database/hydrator/b;->b:Ljava/util/Map;

    invoke-static {p0, p1, v0, v1}, Lcom/twitter/database/hydrator/b;->a(Ljava/lang/Class;Ljava/lang/Class;Ljava/util/Map;Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbr;

    return-object v0
.end method

.method public static a(Ljava/lang/Class;)Lcom/twitter/database/hydrator/b$b;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<MODE",
            "L:Ljava/lang/Object;",
            "SETTER:",
            "Ljava/lang/Object;",
            "WRITER::",
            "Lcom/twitter/database/model/m",
            "<TSETTER;>;>(",
            "Ljava/lang/Class",
            "<TMODE",
            "L;",
            ">;)",
            "Lcom/twitter/database/hydrator/b$b",
            "<TMODE",
            "L;",
            "TSETTER;TWRITER;>;"
        }
    .end annotation

    .prologue
    .line 89
    invoke-static {}, Lcom/twitter/database/hydrator/HydrationRegistry;->a()V

    .line 90
    sget-object v0, Lcom/twitter/database/hydrator/b;->c:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/database/hydrator/b$b;

    .line 91
    if-nez v0, :cond_0

    .line 92
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Could not find dehydrator to dehydrate "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 94
    :cond_0
    return-object v0
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Class;Ljava/util/Map;Ljava/util/Map;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "D:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/Class",
            "<TD;>;",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/database/hydrator/b$a;",
            "TT;>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class;",
            "Lcom/twitter/util/collection/o",
            "<",
            "Ljava/lang/Class;",
            ">;>;)TT;"
        }
    .end annotation

    .prologue
    .line 112
    .line 113
    :goto_0
    if-eqz p0, :cond_1

    .line 114
    invoke-static {p0, p1, p2, p3}, Lcom/twitter/database/hydrator/b;->b(Ljava/lang/Class;Ljava/lang/Class;Ljava/util/Map;Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_0

    .line 122
    :goto_1
    return-object v0

    .line 120
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object p0

    goto :goto_0

    .line 122
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected static a(Ljava/lang/Class;Ljava/lang/Class;Lcbr;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<GETTER:",
            "Ljava/lang/Object;",
            "MODE",
            "L:Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TGETTER;>;",
            "Ljava/lang/Class",
            "<TMODE",
            "L;",
            ">;",
            "Lcbr",
            "<TGETTER;TMODE",
            "L;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    sget-object v0, Lcom/twitter/database/hydrator/b;->a:Ljava/util/Map;

    new-instance v1, Lcom/twitter/database/hydrator/b$a;

    invoke-direct {v1, p0, p1}, Lcom/twitter/database/hydrator/b$a;-><init>(Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/twitter/database/hydrator/b;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 47
    invoke-static {}, Lcom/twitter/util/collection/o;->e()Lcom/twitter/util/collection/o;

    move-result-object v1

    .line 46
    invoke-static {v0, v1}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/o;

    .line 48
    invoke-virtual {v0, p0}, Lcom/twitter/util/collection/o;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/o;

    .line 49
    sget-object v1, Lcom/twitter/database/hydrator/b;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    const-class v0, Lcom/twitter/database/hydrator/b;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 52
    return-void
.end method

.method protected static a(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;Lcom/twitter/database/hydrator/a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<MODE",
            "L:Ljava/lang/Object;",
            "SETTER:",
            "Ljava/lang/Object;",
            "WRITER::",
            "Lcom/twitter/database/model/m",
            "<TSETTER;>;>(",
            "Ljava/lang/Class",
            "<TMODE",
            "L;",
            ">;",
            "Ljava/lang/Class",
            "<TSETTER;>;",
            "Ljava/lang/Class",
            "<TWRITER;>;",
            "Lcom/twitter/database/hydrator/a",
            "<TMODE",
            "L;",
            "TSETTER;>;)V"
        }
    .end annotation

    .prologue
    .line 57
    sget-object v0, Lcom/twitter/database/hydrator/b;->c:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Attempted to register "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 59
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " as the dehydrator for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 60
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " but there is already a dehydrator associated with this class: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v0, Lcom/twitter/database/hydrator/b;->c:Ljava/util/Map;

    .line 62
    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/database/hydrator/b$b;

    iget-object v0, v0, Lcom/twitter/database/hydrator/b$b;->a:Lcom/twitter/database/hydrator/a;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 58
    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 67
    :goto_0
    return-void

    .line 65
    :cond_0
    sget-object v0, Lcom/twitter/database/hydrator/b;->c:Ljava/util/Map;

    new-instance v1, Lcom/twitter/database/hydrator/b$b;

    invoke-direct {v1, p3, p1, p2}, Lcom/twitter/database/hydrator/b$b;-><init>(Lcom/twitter/database/hydrator/a;Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    const-class v0, Lcom/twitter/database/hydrator/b;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    goto :goto_0
.end method

.method private static b(Ljava/lang/Class;Ljava/lang/Class;Ljava/util/Map;Ljava/util/Map;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "D:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/Class",
            "<TD;>;",
            "Ljava/util/Map",
            "<",
            "Lcom/twitter/database/hydrator/b$a;",
            "TT;>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class;",
            "Lcom/twitter/util/collection/o",
            "<",
            "Ljava/lang/Class;",
            ">;>;)TT;"
        }
    .end annotation

    .prologue
    .line 139
    new-instance v0, Lcom/twitter/database/hydrator/b$a;

    invoke-direct {v0, p0, p1}, Lcom/twitter/database/hydrator/b$a;-><init>(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 140
    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 141
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 151
    :goto_0
    return-object v0

    .line 143
    :cond_0
    invoke-interface {p3, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 144
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/o;

    .line 145
    invoke-virtual {v0}, Lcom/twitter/util/collection/o;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 146
    invoke-virtual {v0, p0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 147
    new-instance v1, Lcom/twitter/database/hydrator/b$a;

    invoke-direct {v1, v0, p1}, Lcom/twitter/database/hydrator/b$a;-><init>(Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 151
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
