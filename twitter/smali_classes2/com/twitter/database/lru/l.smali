.class public interface abstract Lcom/twitter/database/lru/l;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract a()Lrx/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Lcqx;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Iterable;)Lrx/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<TK;>;)",
            "Lrx/g",
            "<",
            "Ljava/util/Map",
            "<TK;TV;>;>;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Object;)Lrx/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Lrx/g",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Object;Ljava/lang/Object;)Lrx/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)",
            "Lrx/g",
            "<TV;>;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Object;Ljava/lang/Object;J)Lrx/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;J)",
            "Lrx/g",
            "<TV;>;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Object;Lrx/functions/d;)Lrx/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Lrx/functions/d",
            "<TV;TV;>;)",
            "Lrx/g",
            "<TV;>;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/util/Map;)Lrx/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TK;TV;>;)",
            "Lrx/g",
            "<",
            "Lcqx;",
            ">;"
        }
    .end annotation
.end method

.method public abstract b(Ljava/lang/Object;)Lrx/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Lrx/g",
            "<TV;>;"
        }
    .end annotation
.end method
