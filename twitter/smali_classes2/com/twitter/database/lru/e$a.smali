.class public Lcom/twitter/database/lru/e$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/database/lru/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field a:Lcom/twitter/database/lru/k;

.field b:Ljava/lang/String;

.field c:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<TV;>;"
        }
    .end annotation
.end field

.field d:Lcom/twitter/database/lru/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/lru/f",
            "<TK;>;"
        }
    .end annotation
.end field

.field e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-static {}, Lcom/twitter/database/lru/e;->a()Lcom/twitter/database/lru/k;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/database/lru/e$a;->a:Lcom/twitter/database/lru/k;

    .line 64
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/database/lru/e$a;->b:Ljava/lang/String;

    return-void
.end method

.method public static a()Lcom/twitter/database/lru/e$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/twitter/database/lru/e$a",
            "<",
            "Ljava/lang/String;",
            "TV;>;"
        }
    .end annotation

    .prologue
    .line 71
    new-instance v0, Lcom/twitter/database/lru/e$a;

    invoke-direct {v0}, Lcom/twitter/database/lru/e$a;-><init>()V

    sget-object v1, Lcom/twitter/database/lru/f;->a:Lcom/twitter/database/lru/f;

    invoke-virtual {v0, v1}, Lcom/twitter/database/lru/e$a;->a(Lcom/twitter/database/lru/f;)Lcom/twitter/database/lru/e$a;

    move-result-object v0

    return-object v0
.end method

.method public static b()Lcom/twitter/database/lru/e$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/twitter/database/lru/e$a",
            "<",
            "Ljava/lang/Long;",
            "TV;>;"
        }
    .end annotation

    .prologue
    .line 76
    new-instance v0, Lcom/twitter/database/lru/e$a;

    invoke-direct {v0}, Lcom/twitter/database/lru/e$a;-><init>()V

    sget-object v1, Lcom/twitter/database/lru/f;->b:Lcom/twitter/database/lru/f;

    invoke-virtual {v0, v1}, Lcom/twitter/database/lru/e$a;->a(Lcom/twitter/database/lru/f;)Lcom/twitter/database/lru/e$a;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/database/lru/f;)Lcom/twitter/database/lru/e$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/database/lru/f",
            "<TK;>;)",
            "Lcom/twitter/database/lru/e$a",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 105
    iput-object p1, p0, Lcom/twitter/database/lru/e$a;->d:Lcom/twitter/database/lru/f;

    .line 106
    return-object p0
.end method

.method public a(Lcom/twitter/database/lru/k;)Lcom/twitter/database/lru/e$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/database/lru/k;",
            ")",
            "Lcom/twitter/database/lru/e$a",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 81
    iput-object p1, p0, Lcom/twitter/database/lru/e$a;->a:Lcom/twitter/database/lru/k;

    .line 82
    return-object p0
.end method

.method public a(Lcom/twitter/util/serialization/l;)Lcom/twitter/database/lru/e$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/util/serialization/l",
            "<TV;>;)",
            "Lcom/twitter/database/lru/e$a",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 93
    iput-object p1, p0, Lcom/twitter/database/lru/e$a;->c:Lcom/twitter/util/serialization/l;

    .line 94
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/database/lru/e$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/twitter/database/lru/e$a",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 87
    iput-object p1, p0, Lcom/twitter/database/lru/e$a;->b:Ljava/lang/String;

    .line 88
    return-object p0
.end method

.method public c()Lcom/twitter/database/lru/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/database/lru/e",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 111
    new-instance v0, Lcom/twitter/database/lru/e;

    invoke-direct {v0, p0}, Lcom/twitter/database/lru/e;-><init>(Lcom/twitter/database/lru/e$a;)V

    return-object v0
.end method
