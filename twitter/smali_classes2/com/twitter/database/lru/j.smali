.class public Lcom/twitter/database/lru/j;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/database/lru/i;


# instance fields
.field private final a:Lcom/twitter/database/lru/d;

.field private final b:Lcom/twitter/database/lru/h;

.field private final c:Lcqu;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/twitter/database/lru/LruPolicy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/database/lru/d;Lcom/twitter/database/lru/h;Lcqu;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/twitter/database/lru/j;->a:Lcom/twitter/database/lru/d;

    .line 30
    iput-object p2, p0, Lcom/twitter/database/lru/j;->b:Lcom/twitter/database/lru/h;

    .line 31
    iput-object p3, p0, Lcom/twitter/database/lru/j;->c:Lcqu;

    .line 32
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/database/lru/j;->d:Ljava/util/Map;

    .line 33
    invoke-direct {p0}, Lcom/twitter/database/lru/j;->b()V

    .line 34
    return-void
.end method

.method static synthetic a()J
    .locals 2

    .prologue
    .line 19
    invoke-static {}, Lcom/twitter/database/lru/j;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic a(Lcom/twitter/database/lru/j;)Lcom/twitter/database/lru/h;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/twitter/database/lru/j;->b:Lcom/twitter/database/lru/h;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lcom/twitter/database/lru/j;->c:Lcqu;

    new-instance v1, Lcom/twitter/database/lru/j$2;

    invoke-direct {v1, p0}, Lcom/twitter/database/lru/j$2;-><init>(Lcom/twitter/database/lru/j;)V

    invoke-virtual {v0, v1}, Lcqu;->a(Ljava/util/concurrent/Callable;)Lrx/a;

    move-result-object v0

    .line 189
    invoke-static {}, Lcqv;->b()Lcqv;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/a;->b(Lrx/b;)V

    .line 190
    return-void
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 161
    iget-object v0, p0, Lcom/twitter/database/lru/j;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/database/lru/LruPolicy;

    .line 162
    if-eqz v0, :cond_0

    .line 163
    iget-object v1, p0, Lcom/twitter/database/lru/j;->c:Lcqu;

    new-instance v2, Lcom/twitter/database/lru/j$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/twitter/database/lru/j$1;-><init>(Lcom/twitter/database/lru/j;Lcom/twitter/database/lru/LruPolicy;I)V

    invoke-virtual {v1, v2}, Lcqu;->a(Ljava/util/concurrent/Callable;)Lrx/a;

    move-result-object v0

    .line 176
    invoke-static {}, Lcqv;->b()Lcqv;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/a;->b(Lrx/b;)V

    .line 178
    :cond_0
    return-void
.end method

.method private static c()J
    .locals 2

    .prologue
    .line 193
    invoke-static {}, Lcqq;->d()Lcqq;

    move-result-object v0

    invoke-virtual {v0}, Lcqq;->a()Lcqt;

    move-result-object v0

    invoke-interface {v0}, Lcqt;->a()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 39
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 40
    iget-object v0, p0, Lcom/twitter/database/lru/j;->a:Lcom/twitter/database/lru/d;

    invoke-virtual {v0, p1}, Lcom/twitter/database/lru/d;->a(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public a(Ljava/util/concurrent/Callable;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;TT;)TT;"
        }
    .end annotation

    .prologue
    .line 62
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 63
    iget-object v0, p0, Lcom/twitter/database/lru/j;->b:Lcom/twitter/database/lru/h;

    invoke-virtual {v0}, Lcom/twitter/database/lru/h;->a()Lcom/twitter/database/model/o;

    move-result-object v1

    .line 65
    :try_start_0
    invoke-interface {p1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    .line 66
    invoke-interface {v1}, Lcom/twitter/database/model/o;->a()Lcom/twitter/database/model/o;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    invoke-interface {v1}, Lcom/twitter/database/model/o;->close()V

    move-object p2, v0

    .line 71
    :goto_0
    return-object p2

    .line 68
    :catch_0
    move-exception v0

    .line 70
    :try_start_1
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 73
    invoke-interface {v1}, Lcom/twitter/database/model/o;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Lcom/twitter/database/model/o;->close()V

    throw v0
.end method

.method public a(ILjava/lang/Iterable;I)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;I)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation

    .prologue
    .line 81
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 82
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v1

    .line 83
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 84
    invoke-virtual {p0, p1, v0, p3}, Lcom/twitter/database/lru/j;->a(ILjava/lang/String;I)[B

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    goto :goto_0

    .line 86
    :cond_0
    invoke-virtual {v1}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 143
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 144
    iget-object v0, p0, Lcom/twitter/database/lru/j;->b:Lcom/twitter/database/lru/h;

    invoke-virtual {v0}, Lcom/twitter/database/lru/h;->a()Lcom/twitter/database/model/o;

    move-result-object v1

    .line 146
    :try_start_0
    iget-object v0, p0, Lcom/twitter/database/lru/j;->b:Lcom/twitter/database/lru/h;

    invoke-virtual {v0, p1}, Lcom/twitter/database/lru/h;->a(I)V

    .line 147
    iget-object v0, p0, Lcom/twitter/database/lru/j;->a:Lcom/twitter/database/lru/d;

    invoke-virtual {v0, p1}, Lcom/twitter/database/lru/d;->a(I)V

    .line 148
    invoke-interface {v1}, Lcom/twitter/database/model/o;->a()Lcom/twitter/database/model/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    invoke-interface {v1}, Lcom/twitter/database/model/o;->close()V

    .line 152
    return-void

    .line 150
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Lcom/twitter/database/model/o;->close()V

    throw v0
.end method

.method public a(IILjava/util/Map;J)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[B>;J)V"
        }
    .end annotation

    .prologue
    .line 122
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 123
    iget-object v0, p0, Lcom/twitter/database/lru/j;->b:Lcom/twitter/database/lru/h;

    invoke-virtual {v0}, Lcom/twitter/database/lru/h;->a()Lcom/twitter/database/model/o;

    move-result-object v8

    .line 125
    :try_start_0
    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 126
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    move-object v1, p0

    move v2, p1

    move v4, p2

    move-wide v6, p4

    invoke-virtual/range {v1 .. v7}, Lcom/twitter/database/lru/j;->a(ILjava/lang/String;I[BJ)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 130
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Lcom/twitter/database/model/o;->close()V

    throw v0

    .line 128
    :cond_0
    :try_start_1
    invoke-interface {v8}, Lcom/twitter/database/model/o;->a()Lcom/twitter/database/model/o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 130
    invoke-interface {v8}, Lcom/twitter/database/model/o;->close()V

    .line 132
    return-void
.end method

.method public a(ILcom/twitter/database/lru/LruPolicy;)V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/twitter/database/lru/j;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    invoke-direct {p0, p1}, Lcom/twitter/database/lru/j;->b(I)V

    .line 158
    return-void
.end method

.method public a(ILjava/lang/String;)Z
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/twitter/database/lru/j;->b:Lcom/twitter/database/lru/h;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/database/lru/h;->a(ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public a(ILjava/lang/String;I)[B
    .locals 6

    .prologue
    .line 54
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 55
    invoke-static {}, Lcom/twitter/database/lru/j;->c()J

    move-result-wide v4

    .line 56
    iget-object v0, p0, Lcom/twitter/database/lru/j;->b:Lcom/twitter/database/lru/h;

    move v1, p1

    move-object v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/database/lru/h;->a(ILjava/lang/String;IJ)[B

    move-result-object v0

    return-object v0
.end method

.method public a(ILjava/lang/String;I[BJ)[B
    .locals 11

    .prologue
    .line 101
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 102
    iget-object v0, p0, Lcom/twitter/database/lru/j;->b:Lcom/twitter/database/lru/h;

    invoke-virtual {v0}, Lcom/twitter/database/lru/h;->a()Lcom/twitter/database/model/o;

    move-result-object v10

    .line 103
    invoke-static {}, Lcom/twitter/database/lru/j;->c()J

    move-result-wide v8

    .line 105
    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/database/lru/j;->a(ILjava/lang/String;I)[B

    move-result-object v0

    .line 106
    if-eqz v0, :cond_0

    .line 107
    iget-object v1, p0, Lcom/twitter/database/lru/j;->b:Lcom/twitter/database/lru/h;

    move v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-wide/from16 v6, p5

    invoke-virtual/range {v1 .. v9}, Lcom/twitter/database/lru/h;->a(ILjava/lang/String;I[BJJ)V

    .line 112
    :goto_0
    invoke-interface {v10}, Lcom/twitter/database/model/o;->a()Lcom/twitter/database/model/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    invoke-interface {v10}, Lcom/twitter/database/model/o;->close()V

    .line 113
    return-object v0

    .line 109
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/twitter/database/lru/j;->b:Lcom/twitter/database/lru/h;

    move v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-wide/from16 v6, p5

    invoke-virtual/range {v1 .. v9}, Lcom/twitter/database/lru/h;->b(ILjava/lang/String;I[BJJ)V

    .line 110
    invoke-direct {p0, p1}, Lcom/twitter/database/lru/j;->b(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 115
    :catchall_0
    move-exception v0

    invoke-interface {v10}, Lcom/twitter/database/model/o;->close()V

    throw v0
.end method
