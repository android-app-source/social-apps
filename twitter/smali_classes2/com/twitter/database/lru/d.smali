.class public Lcom/twitter/database/lru/d;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Lcom/twitter/database/model/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/model/l",
            "<",
            "Lcom/twitter/database/lru/schema/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/twitter/database/model/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/model/m",
            "<",
            "Lcom/twitter/database/lru/schema/a$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-string/jumbo v0, "category_name"

    invoke-static {v0}, Laux;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/database/lru/d;->a:Ljava/lang/String;

    .line 17
    const-string/jumbo v0, "_id"

    invoke-static {v0}, Laux;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/database/lru/d;->b:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/twitter/database/model/l;Lcom/twitter/database/model/m;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/database/model/l",
            "<",
            "Lcom/twitter/database/lru/schema/a$a;",
            ">;",
            "Lcom/twitter/database/model/m",
            "<",
            "Lcom/twitter/database/lru/schema/a$b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/twitter/database/lru/d;->c:Lcom/twitter/database/model/l;

    .line 32
    iput-object p2, p0, Lcom/twitter/database/lru/d;->d:Lcom/twitter/database/model/m;

    .line 33
    return-void
.end method

.method public static a(Lcom/twitter/database/lru/schema/LruSchema;)Lcom/twitter/database/lru/d;
    .locals 3

    .prologue
    .line 24
    new-instance v1, Lcom/twitter/database/lru/d;

    const-class v0, Lcom/twitter/database/lru/schema/a;

    .line 25
    invoke-interface {p0, v0}, Lcom/twitter/database/lru/schema/LruSchema;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    check-cast v0, Lcom/twitter/database/lru/schema/a;

    invoke-interface {v0}, Lcom/twitter/database/lru/schema/a;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    const-class v2, Lcom/twitter/database/lru/schema/a$c;

    .line 26
    invoke-interface {p0, v2}, Lcom/twitter/database/lru/schema/LruSchema;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/twitter/database/lru/d;-><init>(Lcom/twitter/database/model/l;Lcom/twitter/database/model/m;)V

    .line 24
    return-object v1
.end method


# virtual methods
.method public a(Ljava/lang/String;)I
    .locals 5

    .prologue
    .line 36
    iget-object v0, p0, Lcom/twitter/database/lru/d;->d:Lcom/twitter/database/model/m;

    invoke-interface {v0}, Lcom/twitter/database/model/m;->b()Lcom/twitter/database/model/h;

    move-result-object v1

    .line 37
    iget-object v0, p0, Lcom/twitter/database/lru/d;->c:Lcom/twitter/database/model/l;

    sget-object v2, Lcom/twitter/database/lru/d;->a:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-interface {v0, v2, v3}, Lcom/twitter/database/model/l;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/twitter/database/model/g;

    move-result-object v2

    .line 39
    :try_start_0
    invoke-virtual {v2}, Lcom/twitter/database/model/g;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    iget-object v0, v1, Lcom/twitter/database/model/h;->d:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/database/lru/schema/a$b;

    invoke-interface {v0, p1}, Lcom/twitter/database/lru/schema/a$b;->a(Ljava/lang/String;)Lcom/twitter/database/lru/schema/a$b;

    .line 41
    invoke-virtual {v1}, Lcom/twitter/database/model/h;->b()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    long-to-int v0, v0

    .line 45
    invoke-virtual {v2}, Lcom/twitter/database/model/g;->close()V

    .line 43
    :goto_0
    return v0

    :cond_0
    :try_start_1
    iget-object v0, v2, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/database/lru/schema/a$a;

    invoke-interface {v0}, Lcom/twitter/database/lru/schema/a$a;->a()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    long-to-int v0, v0

    .line 45
    invoke-virtual {v2}, Lcom/twitter/database/model/g;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/twitter/database/model/g;->close()V

    throw v0
.end method

.method public a(I)V
    .locals 5

    .prologue
    .line 50
    iget-object v0, p0, Lcom/twitter/database/lru/d;->d:Lcom/twitter/database/model/m;

    sget-object v1, Lcom/twitter/database/lru/d;->b:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/twitter/database/model/m;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    move-result v0

    .line 51
    if-gez v0, :cond_0

    .line 52
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Failed to delete category id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcqj;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    :cond_0
    return-void
.end method
