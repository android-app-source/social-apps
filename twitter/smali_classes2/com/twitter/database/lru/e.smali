.class public Lcom/twitter/database/lru/e;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/database/lru/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final f:J

.field private static final g:Lcom/twitter/database/lru/k;


# instance fields
.field public final a:Lcom/twitter/database/lru/k;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<TV;>;"
        }
    .end annotation
.end field

.field public final d:Lcom/twitter/database/lru/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/lru/f",
            "<TK;>;"
        }
    .end annotation
.end field

.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 32
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/twitter/database/lru/e;->f:J

    .line 33
    new-instance v0, Lcom/twitter/database/lru/k;

    new-instance v1, Lcom/twitter/database/lru/LruPolicy;

    sget-object v2, Lcom/twitter/database/lru/LruPolicy$Type;->a:Lcom/twitter/database/lru/LruPolicy$Type;

    const/16 v3, 0x32

    invoke-direct {v1, v2, v3}, Lcom/twitter/database/lru/LruPolicy;-><init>(Lcom/twitter/database/lru/LruPolicy$Type;I)V

    sget-wide v2, Lcom/twitter/database/lru/e;->f:J

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/database/lru/k;-><init>(Lcom/twitter/database/lru/LruPolicy;J)V

    sput-object v0, Lcom/twitter/database/lru/e;->g:Lcom/twitter/database/lru/k;

    return-void
.end method

.method constructor <init>(Lcom/twitter/database/lru/e$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/database/lru/e$a",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iget-object v0, p1, Lcom/twitter/database/lru/e$a;->a:Lcom/twitter/database/lru/k;

    iput-object v0, p0, Lcom/twitter/database/lru/e;->a:Lcom/twitter/database/lru/k;

    .line 44
    iget-object v0, p1, Lcom/twitter/database/lru/e$a;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/database/lru/e;->b:Ljava/lang/String;

    .line 45
    iget-object v0, p1, Lcom/twitter/database/lru/e$a;->c:Lcom/twitter/util/serialization/l;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/serialization/l;

    iput-object v0, p0, Lcom/twitter/database/lru/e;->c:Lcom/twitter/util/serialization/l;

    .line 46
    iget v0, p1, Lcom/twitter/database/lru/e$a;->e:I

    iput v0, p0, Lcom/twitter/database/lru/e;->e:I

    .line 47
    iget-object v0, p1, Lcom/twitter/database/lru/e$a;->d:Lcom/twitter/database/lru/f;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/database/lru/f;

    iput-object v0, p0, Lcom/twitter/database/lru/e;->d:Lcom/twitter/database/lru/f;

    .line 48
    return-void
.end method

.method static synthetic a()Lcom/twitter/database/lru/k;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/twitter/database/lru/e;->g:Lcom/twitter/database/lru/k;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 52
    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/twitter/database/lru/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/database/lru/e;->b:Ljava/lang/String;

    check-cast p1, Lcom/twitter/database/lru/e;

    iget-object v1, p1, Lcom/twitter/database/lru/e;->b:Ljava/lang/String;

    .line 53
    invoke-static {v0, v1}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 52
    :goto_0
    return v0

    .line 53
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/database/lru/e;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
