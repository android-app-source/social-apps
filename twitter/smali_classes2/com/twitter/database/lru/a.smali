.class public Lcom/twitter/database/lru/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/database/lru/l;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/database/lru/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/twitter/database/lru/l",
        "<",
        "Ljava/lang/String;",
        "TV;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/database/lru/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/lru/c",
            "<",
            "Ljava/lang/String;",
            "TV;>;"
        }
    .end annotation
.end field

.field private final b:Lrx/f;

.field private final c:Lrx/f;

.field private final d:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/database/lru/c;Lrx/f;Lrx/f;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/database/lru/c",
            "<",
            "Ljava/lang/String;",
            "TV;>;",
            "Lrx/f;",
            "Lrx/f;",
            ")V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/twitter/database/lru/a;->a:Lcom/twitter/database/lru/c;

    .line 34
    iput-object p2, p0, Lcom/twitter/database/lru/a;->b:Lrx/f;

    .line 35
    iput-object p3, p0, Lcom/twitter/database/lru/a;->c:Lrx/f;

    .line 36
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/database/lru/a;->d:Lrx/subjects/PublishSubject;

    .line 37
    return-void
.end method

.method static synthetic a(Lcom/twitter/database/lru/a;)Lcom/twitter/database/lru/c;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/twitter/database/lru/a;->a:Lcom/twitter/database/lru/c;

    return-object v0
.end method

.method private a(Ljava/util/concurrent/Callable;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)",
            "Lrx/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 210
    invoke-static {p1}, Lrx/g;->a(Ljava/util/concurrent/Callable;)Lrx/g;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/database/lru/a;->a(Lrx/g;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method private a(Lrx/g;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/g",
            "<TT;>;)",
            "Lrx/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 215
    iget-object v0, p0, Lcom/twitter/database/lru/a;->b:Lrx/f;

    invoke-virtual {p1, v0}, Lrx/g;->b(Lrx/f;)Lrx/g;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/database/lru/a;->c:Lrx/f;

    invoke-virtual {v0, v1}, Lrx/g;->a(Lrx/f;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/database/lru/a;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/twitter/database/lru/a;->c(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/Object;)Lrx/functions/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TV;)",
            "Lrx/functions/b",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 220
    new-instance v0, Lcom/twitter/database/lru/a$7;

    invoke-direct {v0, p0, p2, p1}, Lcom/twitter/database/lru/a$7;-><init>(Lcom/twitter/database/lru/a;Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/database/lru/a;)Lrx/subjects/PublishSubject;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/twitter/database/lru/a;->d:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method private c(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 231
    iget-object v0, p0, Lcom/twitter/database/lru/a;->d:Lrx/subjects/PublishSubject;

    new-instance v1, Ljava/util/AbstractMap$SimpleEntry;

    invoke-direct {v1, p1, p2}, Ljava/util/AbstractMap$SimpleEntry;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject;->a(Ljava/lang/Object;)V

    .line 232
    return-void
.end method


# virtual methods
.method public a()Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Lcqx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    new-instance v0, Lcom/twitter/database/lru/a$3;

    invoke-direct {v0, p0}, Lcom/twitter/database/lru/a$3;-><init>(Lcom/twitter/database/lru/a;)V

    invoke-direct {p0, v0}, Lcom/twitter/database/lru/a;->a(Ljava/util/concurrent/Callable;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/database/lru/a$2;

    invoke-direct {v1, p0}, Lcom/twitter/database/lru/a$2;-><init>(Lcom/twitter/database/lru/a;)V

    .line 153
    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/b;)Lrx/g;

    move-result-object v0

    .line 145
    invoke-static {v0}, Lcre;->a(Lrx/g;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Iterable;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lrx/g",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "TV;>;>;"
        }
    .end annotation

    .prologue
    .line 54
    new-instance v0, Lcom/twitter/database/lru/a$8;

    invoke-direct {v0, p0, p1}, Lcom/twitter/database/lru/a$8;-><init>(Lcom/twitter/database/lru/a;Ljava/lang/Iterable;)V

    invoke-direct {p0, v0}, Lcom/twitter/database/lru/a;->a(Ljava/util/concurrent/Callable;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/Object;)Lrx/g;
    .locals 1

    .prologue
    .line 23
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/twitter/database/lru/a;->b(Ljava/lang/String;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Lrx/g;
    .locals 1

    .prologue
    .line 23
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/database/lru/a;->a(Ljava/lang/String;Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;J)Lrx/g;
    .locals 1

    .prologue
    .line 23
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/database/lru/a;->a(Ljava/lang/String;Ljava/lang/Object;J)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Lrx/functions/d;)Lrx/g;
    .locals 1

    .prologue
    .line 23
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/database/lru/a;->a(Ljava/lang/String;Lrx/functions/d;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/g",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 42
    new-instance v0, Lcom/twitter/database/lru/a$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/database/lru/a$1;-><init>(Lcom/twitter/database/lru/a;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/twitter/database/lru/a;->a(Ljava/util/concurrent/Callable;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TV;)",
            "Lrx/g",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 78
    new-instance v0, Lcom/twitter/database/lru/a$9;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/database/lru/a$9;-><init>(Lcom/twitter/database/lru/a;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lcom/twitter/database/lru/a;->a(Ljava/util/concurrent/Callable;)Lrx/g;

    move-result-object v0

    .line 85
    invoke-direct {p0, p1, p2}, Lcom/twitter/database/lru/a;->b(Ljava/lang/String;Ljava/lang/Object;)Lrx/functions/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/b;)Lrx/g;

    move-result-object v0

    .line 78
    invoke-static {v0}, Lcre;->a(Lrx/g;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;J)Lrx/g;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TV;J)",
            "Lrx/g",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 91
    new-instance v0, Lcom/twitter/database/lru/a$10;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/database/lru/a$10;-><init>(Lcom/twitter/database/lru/a;Ljava/lang/String;Ljava/lang/Object;J)V

    invoke-direct {p0, v0}, Lcom/twitter/database/lru/a;->a(Ljava/util/concurrent/Callable;)Lrx/g;

    move-result-object v0

    .line 98
    invoke-direct {p0, p1, p2}, Lcom/twitter/database/lru/a;->b(Ljava/lang/String;Ljava/lang/Object;)Lrx/functions/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/b;)Lrx/g;

    move-result-object v0

    .line 91
    invoke-static {v0}, Lcre;->a(Lrx/g;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Lrx/functions/d;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lrx/functions/d",
            "<TV;TV;>;)",
            "Lrx/g",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 170
    new-instance v0, Lcom/twitter/database/lru/a$6;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/database/lru/a$6;-><init>(Lcom/twitter/database/lru/a;Ljava/lang/String;Lrx/functions/d;)V

    invoke-direct {p0, v0}, Lcom/twitter/database/lru/a;->a(Ljava/util/concurrent/Callable;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/database/lru/a$5;

    invoke-direct {v1, p0, p1}, Lcom/twitter/database/lru/a$5;-><init>(Lcom/twitter/database/lru/a;Ljava/lang/String;)V

    .line 191
    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/b;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/database/lru/a$4;

    invoke-direct {v1, p0}, Lcom/twitter/database/lru/a$4;-><init>(Lcom/twitter/database/lru/a;)V

    .line 199
    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    .line 170
    invoke-static {v0}, Lcre;->a(Lrx/g;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/Map;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "TV;>;)",
            "Lrx/g",
            "<",
            "Lcqx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    new-instance v0, Lcom/twitter/database/lru/a$12;

    invoke-direct {v0, p0, p1}, Lcom/twitter/database/lru/a$12;-><init>(Lcom/twitter/database/lru/a;Ljava/util/Map;)V

    invoke-direct {p0, v0}, Lcom/twitter/database/lru/a;->a(Ljava/util/concurrent/Callable;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/database/lru/a$11;

    invoke-direct {v1, p0, p1}, Lcom/twitter/database/lru/a$11;-><init>(Lcom/twitter/database/lru/a;Ljava/util/Map;)V

    .line 112
    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/b;)Lrx/g;

    move-result-object v0

    .line 104
    invoke-static {v0}, Lcre;->a(Lrx/g;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Lrx/g;
    .locals 1

    .prologue
    .line 23
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/twitter/database/lru/a;->a(Ljava/lang/String;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/g",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    new-instance v0, Lcom/twitter/database/lru/a$14;

    invoke-direct {v0, p0, p1}, Lcom/twitter/database/lru/a$14;-><init>(Lcom/twitter/database/lru/a;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/twitter/database/lru/a;->a(Ljava/util/concurrent/Callable;)Lrx/g;

    move-result-object v0

    new-instance v1, Lcom/twitter/database/lru/a$13;

    invoke-direct {v1, p0, p1}, Lcom/twitter/database/lru/a$13;-><init>(Lcom/twitter/database/lru/a;Ljava/lang/String;)V

    .line 132
    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/b;)Lrx/g;

    move-result-object v0

    .line 125
    invoke-static {v0}, Lcre;->a(Lrx/g;)Lrx/g;

    move-result-object v0

    return-object v0
.end method
