.class public Lcom/twitter/database/lru/g;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/database/lru/l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/twitter/database/lru/l",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/database/lru/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/String;",
            "TV;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/twitter/database/lru/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/lru/f",
            "<TK;>;"
        }
    .end annotation
.end field

.field private final c:Lcpp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcpp",
            "<",
            "Ljava/lang/String;",
            "TK;>;"
        }
    .end annotation
.end field

.field private final d:Lcpp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcpp",
            "<TK;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lrx/functions/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/d",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "TV;>;",
            "Ljava/util/Map",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/twitter/database/lru/l;Lcom/twitter/database/lru/f;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/String;",
            "TV;>;",
            "Lcom/twitter/database/lru/f",
            "<TK;>;)V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/twitter/database/lru/g;->a:Lcom/twitter/database/lru/l;

    .line 28
    iput-object p2, p0, Lcom/twitter/database/lru/g;->b:Lcom/twitter/database/lru/f;

    .line 29
    new-instance v0, Lcom/twitter/database/lru/g$1;

    invoke-direct {v0, p0}, Lcom/twitter/database/lru/g$1;-><init>(Lcom/twitter/database/lru/g;)V

    iput-object v0, p0, Lcom/twitter/database/lru/g;->c:Lcpp;

    .line 36
    new-instance v0, Lcom/twitter/database/lru/g$2;

    invoke-direct {v0, p0}, Lcom/twitter/database/lru/g$2;-><init>(Lcom/twitter/database/lru/g;)V

    iput-object v0, p0, Lcom/twitter/database/lru/g;->d:Lcpp;

    .line 43
    new-instance v0, Lcom/twitter/database/lru/g$3;

    invoke-direct {v0, p0}, Lcom/twitter/database/lru/g$3;-><init>(Lcom/twitter/database/lru/g;)V

    iput-object v0, p0, Lcom/twitter/database/lru/g;->e:Lrx/functions/d;

    .line 50
    return-void
.end method

.method static synthetic a(Lcom/twitter/database/lru/g;)Lcom/twitter/database/lru/f;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/twitter/database/lru/g;->b:Lcom/twitter/database/lru/f;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/database/lru/g;)Lcpp;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/twitter/database/lru/g;->c:Lcpp;

    return-object v0
.end method


# virtual methods
.method public a()Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Lcqx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lcom/twitter/database/lru/g;->a:Lcom/twitter/database/lru/l;

    invoke-interface {v0}, Lcom/twitter/database/lru/l;->a()Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Iterable;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<TK;>;)",
            "Lrx/g",
            "<",
            "Ljava/util/Map",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/database/lru/g;->d:Lcpp;

    invoke-static {p1, v0}, Lcpt;->a(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v0

    .line 62
    iget-object v1, p0, Lcom/twitter/database/lru/g;->a:Lcom/twitter/database/lru/l;

    invoke-interface {v1, v0}, Lcom/twitter/database/lru/l;->a(Ljava/lang/Iterable;)Lrx/g;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/database/lru/g;->e:Lrx/functions/d;

    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Lrx/g",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lcom/twitter/database/lru/g;->a:Lcom/twitter/database/lru/l;

    iget-object v1, p0, Lcom/twitter/database/lru/g;->b:Lcom/twitter/database/lru/f;

    invoke-interface {v1, p1}, Lcom/twitter/database/lru/f;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/database/lru/l;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)",
            "Lrx/g",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/twitter/database/lru/g;->a:Lcom/twitter/database/lru/l;

    iget-object v1, p0, Lcom/twitter/database/lru/g;->b:Lcom/twitter/database/lru/f;

    invoke-interface {v1, p1}, Lcom/twitter/database/lru/f;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/twitter/database/lru/l;->a(Ljava/lang/Object;Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;J)Lrx/g;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;J)",
            "Lrx/g",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/twitter/database/lru/g;->a:Lcom/twitter/database/lru/l;

    iget-object v1, p0, Lcom/twitter/database/lru/g;->b:Lcom/twitter/database/lru/f;

    invoke-interface {v1, p1}, Lcom/twitter/database/lru/f;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3, p4}, Lcom/twitter/database/lru/l;->a(Ljava/lang/Object;Ljava/lang/Object;J)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Lrx/functions/d;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Lrx/functions/d",
            "<TV;TV;>;)",
            "Lrx/g",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/twitter/database/lru/g;->a:Lcom/twitter/database/lru/l;

    iget-object v1, p0, Lcom/twitter/database/lru/g;->b:Lcom/twitter/database/lru/f;

    invoke-interface {v1, p1}, Lcom/twitter/database/lru/f;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/twitter/database/lru/l;->a(Ljava/lang/Object;Lrx/functions/d;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/Map;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TK;TV;>;)",
            "Lrx/g",
            "<",
            "Lcqx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/twitter/database/lru/g;->a:Lcom/twitter/database/lru/l;

    new-instance v1, Lcom/twitter/database/lru/g$4;

    invoke-direct {v1, p0}, Lcom/twitter/database/lru/g$4;-><init>(Lcom/twitter/database/lru/g;)V

    invoke-static {p1, v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/util/Map;Lcpp;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/database/lru/l;->a(Ljava/util/Map;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Lrx/g",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/twitter/database/lru/g;->a:Lcom/twitter/database/lru/l;

    iget-object v1, p0, Lcom/twitter/database/lru/g;->b:Lcom/twitter/database/lru/f;

    invoke-interface {v1, p1}, Lcom/twitter/database/lru/f;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/database/lru/l;->b(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    return-object v0
.end method
