.class public Lcom/twitter/database/lru/h;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/database/model/i;


# direct methods
.method public constructor <init>(Lcom/twitter/database/lru/schema/LruSchema;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/twitter/database/lru/h;->a:Lcom/twitter/database/model/i;

    .line 46
    return-void
.end method

.method private static a(JJ)J
    .locals 6

    .prologue
    .line 204
    const-wide v4, 0x7fffffffffffffffL

    move-wide v0, p0

    move-wide v2, p2

    invoke-static/range {v0 .. v5}, Lcom/twitter/util/math/b;->b(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(IILrx/functions/d;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lrx/functions/d",
            "<",
            "Lcom/twitter/database/lru/schema/b$a;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 162
    iget-object v1, p0, Lcom/twitter/database/lru/h;->a:Lcom/twitter/database/model/i;

    invoke-interface {v1}, Lcom/twitter/database/model/i;->h()Lcom/twitter/database/model/o;

    move-result-object v2

    .line 164
    :try_start_0
    new-instance v1, Lcom/twitter/database/model/f$a;

    invoke-direct {v1}, Lcom/twitter/database/model/f$a;-><init>()V

    const-string/jumbo v3, "category_id"

    .line 165
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v4}, Laux;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/twitter/database/model/f$a;->a(Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v1

    const-string/jumbo v3, "expiry_timestamp DESC"

    .line 166
    invoke-virtual {v1, v3}, Lcom/twitter/database/model/f$a;->b(Ljava/lang/String;)Lcom/twitter/database/model/f$a;

    move-result-object v1

    .line 167
    invoke-virtual {v1}, Lcom/twitter/database/model/f$a;->a()Lcom/twitter/database/model/f;

    move-result-object v1

    .line 168
    invoke-direct {p0}, Lcom/twitter/database/lru/h;->c()Lcom/twitter/database/model/l;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/twitter/database/model/l;->a(Lcom/twitter/database/model/f;)Lcom/twitter/database/model/g;

    move-result-object v3

    .line 169
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v4

    .line 172
    :try_start_1
    invoke-virtual {v3}, Lcom/twitter/database/model/g;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    move v1, v0

    .line 174
    iget-object v0, v3, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    invoke-interface {p3, v0}, Lrx/functions/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v1

    .line 175
    if-gt v0, p2, :cond_1

    invoke-virtual {v3}, Lcom/twitter/database/model/g;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 177
    :cond_1
    invoke-virtual {v3}, Lcom/twitter/database/model/g;->e()Z

    move-result v0

    if-nez v0, :cond_3

    .line 179
    :cond_2
    iget-object v0, v3, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/database/lru/schema/b$a;

    invoke-interface {v0}, Lcom/twitter/database/lru/schema/b$a;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 180
    invoke-virtual {v3}, Lcom/twitter/database/model/g;->d()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 184
    :cond_3
    :try_start_2
    invoke-virtual {v3}, Lcom/twitter/database/model/g;->close()V

    .line 186
    invoke-direct {p0}, Lcom/twitter/database/lru/h;->b()Lcom/twitter/database/model/m;

    move-result-object v1

    const-string/jumbo v3, "_id"

    invoke-virtual {v4}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v3, v0}, Laux;->a(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v1, v0, v3}, Lcom/twitter/database/model/m;->a(Ljava/lang/String;[Ljava/lang/String;)I

    .line 187
    invoke-interface {v2}, Lcom/twitter/database/model/o;->a()Lcom/twitter/database/model/o;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 189
    invoke-interface {v2}, Lcom/twitter/database/model/o;->close()V

    .line 191
    return-void

    .line 184
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v3}, Lcom/twitter/database/model/g;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 189
    :catchall_1
    move-exception v0

    invoke-interface {v2}, Lcom/twitter/database/model/o;->close()V

    throw v0
.end method

.method private b()Lcom/twitter/database/model/m;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/database/model/m",
            "<",
            "Lcom/twitter/database/lru/schema/b$c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 195
    iget-object v0, p0, Lcom/twitter/database/lru/h;->a:Lcom/twitter/database/model/i;

    const-class v1, Lcom/twitter/database/lru/schema/b$b;

    invoke-interface {v0, v1}, Lcom/twitter/database/model/i;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v0

    return-object v0
.end method

.method private c()Lcom/twitter/database/model/l;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/database/model/l",
            "<",
            "Lcom/twitter/database/lru/schema/b$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200
    iget-object v0, p0, Lcom/twitter/database/lru/h;->a:Lcom/twitter/database/model/i;

    const-class v1, Lcom/twitter/database/lru/schema/b;

    invoke-interface {v0, v1}, Lcom/twitter/database/model/i;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    check-cast v0, Lcom/twitter/database/lru/schema/b;

    invoke-interface {v0}, Lcom/twitter/database/lru/schema/b;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/database/model/o;
    .locals 1

    .prologue
    .line 210
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 211
    iget-object v0, p0, Lcom/twitter/database/lru/h;->a:Lcom/twitter/database/model/i;

    invoke-interface {v0}, Lcom/twitter/database/model/i;->h()Lcom/twitter/database/model/o;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 5

    .prologue
    .line 124
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 125
    invoke-direct {p0}, Lcom/twitter/database/lru/h;->b()Lcom/twitter/database/model/m;

    move-result-object v0

    const-string/jumbo v1, "category_id=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/twitter/database/model/m;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 126
    return-void
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 141
    new-instance v0, Lcom/twitter/database/lru/h$1;

    invoke-direct {v0, p0}, Lcom/twitter/database/lru/h$1;-><init>(Lcom/twitter/database/lru/h;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/database/lru/h;->a(IILrx/functions/d;)V

    .line 148
    return-void
.end method

.method public a(ILjava/lang/String;I[BJJ)V
    .locals 5

    .prologue
    .line 86
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 87
    invoke-direct {p0}, Lcom/twitter/database/lru/h;->b()Lcom/twitter/database/model/m;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/database/model/m;->b()Lcom/twitter/database/model/h;

    move-result-object v1

    .line 88
    iget-object v0, v1, Lcom/twitter/database/model/h;->d:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/database/lru/schema/b$c;

    invoke-interface {v0, p4}, Lcom/twitter/database/lru/schema/b$c;->a([B)Lcom/twitter/database/lru/schema/b$c;

    move-result-object v0

    .line 89
    invoke-static {p7, p8, p5, p6}, Lcom/twitter/database/lru/h;->a(JJ)J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/twitter/database/lru/schema/b$c;->b(J)Lcom/twitter/database/lru/schema/b$c;

    move-result-object v0

    .line 90
    invoke-interface {v0, p7, p8}, Lcom/twitter/database/lru/schema/b$c;->a(J)Lcom/twitter/database/lru/schema/b$c;

    move-result-object v0

    array-length v2, p4

    int-to-long v2, v2

    .line 91
    invoke-interface {v0, v2, v3}, Lcom/twitter/database/lru/schema/b$c;->c(J)Lcom/twitter/database/lru/schema/b$c;

    .line 92
    const-string/jumbo v0, "category_id=? AND key=? AND version=? AND expiry_timestamp>?"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    .line 93
    invoke-static {p7, p8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 92
    invoke-virtual {v1, v0, v2}, Lcom/twitter/database/model/h;->a(Ljava/lang/String;[Ljava/lang/String;)I

    .line 94
    return-void
.end method

.method public a(J)V
    .locals 5

    .prologue
    .line 130
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 131
    invoke-direct {p0}, Lcom/twitter/database/lru/h;->b()Lcom/twitter/database/model/m;

    move-result-object v0

    const-string/jumbo v1, "expiry_timestamp<?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/twitter/database/model/m;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 132
    return-void
.end method

.method public a(ILjava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 113
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 114
    invoke-direct {p0}, Lcom/twitter/database/lru/h;->b()Lcom/twitter/database/model/m;

    move-result-object v2

    const-string/jumbo v3, "category_id=? AND key=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    aput-object p2, v4, v0

    invoke-interface {v2, v3, v4}, Lcom/twitter/database/model/m;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    move-result v2

    .line 115
    if-gez v2, :cond_0

    .line 116
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Failed to delete entry with category id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " key: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcqj;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_0
    if-le v2, v0, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public a(ILjava/lang/String;IJ)[B
    .locals 6

    .prologue
    .line 51
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 52
    invoke-direct {p0}, Lcom/twitter/database/lru/h;->c()Lcom/twitter/database/model/l;

    move-result-object v0

    const-string/jumbo v1, "category_id=? AND key=? AND version=? AND expiry_timestamp>?"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 53
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    .line 52
    invoke-interface {v0, v1, v2}, Lcom/twitter/database/model/l;->a(Ljava/lang/String;[Ljava/lang/Object;)Lcom/twitter/database/model/g;

    move-result-object v1

    .line 55
    :try_start_0
    invoke-virtual {v1}, Lcom/twitter/database/model/g;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, v1, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/database/lru/schema/b$a;

    invoke-interface {v0}, Lcom/twitter/database/lru/schema/b$a;->b()[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 60
    invoke-virtual {v1}, Lcom/twitter/database/model/g;->close()V

    .line 58
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    .line 60
    invoke-virtual {v1}, Lcom/twitter/database/model/g;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/twitter/database/model/g;->close()V

    throw v0
.end method

.method public b(II)V
    .locals 1

    .prologue
    .line 151
    new-instance v0, Lcom/twitter/database/lru/h$2;

    invoke-direct {v0, p0}, Lcom/twitter/database/lru/h$2;-><init>(Lcom/twitter/database/lru/h;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/database/lru/h;->a(IILrx/functions/d;)V

    .line 158
    return-void
.end method

.method public b(ILjava/lang/String;I[BJJ)V
    .locals 5

    .prologue
    .line 99
    invoke-static {}, Lcom/twitter/util/f;->c()V

    .line 100
    invoke-direct {p0}, Lcom/twitter/database/lru/h;->b()Lcom/twitter/database/model/m;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/database/model/m;->b()Lcom/twitter/database/model/h;

    move-result-object v1

    .line 101
    iget-object v0, v1, Lcom/twitter/database/model/h;->d:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/database/lru/schema/b$c;

    invoke-interface {v0, p4}, Lcom/twitter/database/lru/schema/b$c;->a([B)Lcom/twitter/database/lru/schema/b$c;

    move-result-object v0

    .line 102
    invoke-static {p7, p8, p5, p6}, Lcom/twitter/database/lru/h;->a(JJ)J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/twitter/database/lru/schema/b$c;->b(J)Lcom/twitter/database/lru/schema/b$c;

    move-result-object v0

    .line 103
    invoke-interface {v0, p7, p8}, Lcom/twitter/database/lru/schema/b$c;->a(J)Lcom/twitter/database/lru/schema/b$c;

    move-result-object v0

    array-length v2, p4

    int-to-long v2, v2

    .line 104
    invoke-interface {v0, v2, v3}, Lcom/twitter/database/lru/schema/b$c;->c(J)Lcom/twitter/database/lru/schema/b$c;

    move-result-object v0

    .line 105
    invoke-interface {v0, p1}, Lcom/twitter/database/lru/schema/b$c;->a(I)Lcom/twitter/database/lru/schema/b$c;

    move-result-object v0

    .line 106
    invoke-interface {v0, p2}, Lcom/twitter/database/lru/schema/b$c;->a(Ljava/lang/String;)Lcom/twitter/database/lru/schema/b$c;

    move-result-object v0

    .line 107
    invoke-interface {v0, p3}, Lcom/twitter/database/lru/schema/b$c;->b(I)Lcom/twitter/database/lru/schema/b$c;

    .line 108
    invoke-virtual {v1}, Lcom/twitter/database/model/h;->b()J

    .line 109
    return-void
.end method
