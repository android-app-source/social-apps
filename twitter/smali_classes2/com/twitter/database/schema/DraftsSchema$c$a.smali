.class public interface abstract Lcom/twitter/database/schema/DraftsSchema$c$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/database/schema/DraftsSchema$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# virtual methods
.method public abstract a(I)Lcom/twitter/database/schema/DraftsSchema$c$a;
.end method

.method public abstract a(J)Lcom/twitter/database/schema/DraftsSchema$c$a;
.end method

.method public abstract a(Lcau;)Lcom/twitter/database/schema/DraftsSchema$c$a;
.end method

.method public abstract a(Lcgi;)Lcom/twitter/database/schema/DraftsSchema$c$a;
.end method

.method public abstract a(Lcom/twitter/model/core/r;)Lcom/twitter/database/schema/DraftsSchema$c$a;
.end method

.method public abstract a(Lcom/twitter/model/geo/c;)Lcom/twitter/database/schema/DraftsSchema$c$a;
.end method

.method public abstract a(Lcom/twitter/model/timeline/ap;)Lcom/twitter/database/schema/DraftsSchema$c$a;
.end method

.method public abstract a(Ljava/lang/String;)Lcom/twitter/database/schema/DraftsSchema$c$a;
.end method

.method public abstract a(Ljava/util/List;)Lcom/twitter/database/schema/DraftsSchema$c$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/drafts/DraftAttachment;",
            ">;)",
            "Lcom/twitter/database/schema/DraftsSchema$c$a;"
        }
    .end annotation
.end method

.method public abstract a(Z)Lcom/twitter/database/schema/DraftsSchema$c$a;
.end method

.method public abstract b(J)Lcom/twitter/database/schema/DraftsSchema$c$a;
.end method

.method public abstract b(Ljava/lang/String;)Lcom/twitter/database/schema/DraftsSchema$c$a;
.end method

.method public abstract b(Ljava/util/List;)Lcom/twitter/database/schema/DraftsSchema$c$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/twitter/database/schema/DraftsSchema$c$a;"
        }
    .end annotation
.end method

.method public abstract b(Z)Lcom/twitter/database/schema/DraftsSchema$c$a;
.end method

.method public abstract c(J)Lcom/twitter/database/schema/DraftsSchema$c$a;
.end method

.method public abstract c(Ljava/lang/String;)Lcom/twitter/database/schema/DraftsSchema$c$a;
.end method

.method public abstract c(Ljava/util/List;)Lcom/twitter/database/schema/DraftsSchema$c$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/twitter/database/schema/DraftsSchema$c$a;"
        }
    .end annotation
.end method

.method public abstract d(J)Lcom/twitter/database/schema/DraftsSchema$c$a;
.end method

.method public abstract d(Ljava/util/List;)Lcom/twitter/database/schema/DraftsSchema$c$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/twitter/database/schema/DraftsSchema$c$a;"
        }
    .end annotation
.end method
