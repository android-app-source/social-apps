.class public interface abstract Lcom/twitter/database/model/j;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/database/model/j$a;
    }
.end annotation


# virtual methods
.method public abstract a()Lcom/twitter/database/model/o;
.end method

.method public abstract a(Ljava/lang/Class;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Class;Lcom/twitter/database/model/ColumnDefinition;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/n;",
            ">;",
            "Lcom/twitter/database/model/ColumnDefinition;",
            ")V"
        }
    .end annotation
.end method

.method public varargs abstract a(Ljava/lang/Class;[Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/n;",
            ">;[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public varargs abstract a([Ljava/lang/String;)V
.end method

.method public abstract b()V
.end method

.method public varargs abstract b([Ljava/lang/String;)V
.end method

.method public abstract c()Lcom/twitter/database/model/i;
.end method

.method public varargs abstract c([Ljava/lang/String;)V
.end method
