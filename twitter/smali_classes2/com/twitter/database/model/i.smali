.class public interface abstract Lcom/twitter/database/model/i;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/database/model/i$a;
    }
.end annotation


# virtual methods
.method public abstract a(Ljava/lang/Class;)Lcom/twitter/database/model/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/twitter/database/model/k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public abstract b(Ljava/lang/Class;)Lcom/twitter/database/model/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<G:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/l",
            "<TG;>;>;)",
            "Lcom/twitter/database/model/l",
            "<TG;>;"
        }
    .end annotation
.end method

.method public abstract c(Ljava/lang/Class;)Lcom/twitter/database/model/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/m",
            "<TS;>;>;)",
            "Lcom/twitter/database/model/m",
            "<TS;>;"
        }
    .end annotation
.end method

.method public abstract g()V
.end method

.method public abstract h()Lcom/twitter/database/model/o;
.end method

.method public abstract j()V
.end method
