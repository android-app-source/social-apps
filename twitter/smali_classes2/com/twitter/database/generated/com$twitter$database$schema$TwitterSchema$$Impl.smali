.class public final Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;
.super Lcom/twitter/database/internal/f;
.source "Twttr"

# interfaces
.implements Lcom/twitter/database/schema/TwitterSchema;


# annotations
.annotation build Lcod;
.end annotation


# static fields
.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/l;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/l;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/m;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/m;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 114
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    .line 115
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lawd;

    const-class v2, Lcom/twitter/database/generated/i;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Layo;

    const-class v2, Lcom/twitter/database/generated/bw;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lawr;

    const-class v2, Lcom/twitter/database/generated/w;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laxf;

    const-class v2, Lcom/twitter/database/generated/am;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laxk;

    const-class v2, Lcom/twitter/database/generated/ar;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lawp;

    const-class v2, Lcom/twitter/database/generated/u;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laxp;

    const-class v2, Lcom/twitter/database/generated/aw;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laxi;

    const-class v2, Lcom/twitter/database/generated/ap;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laxh;

    const-class v2, Lcom/twitter/database/generated/ao;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Layp;

    const-class v2, Lcom/twitter/database/generated/bx;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laws;

    const-class v2, Lcom/twitter/database/generated/x;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Layr;

    const-class v2, Lcom/twitter/database/generated/bz;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lays;

    const-class v2, Lcom/twitter/database/generated/ca;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Layt;

    const-class v2, Lcom/twitter/database/generated/cb;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Layu;

    const-class v2, Lcom/twitter/database/generated/cc;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Layx;

    const-class v2, Lcom/twitter/database/generated/cf;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laxm;

    const-class v2, Lcom/twitter/database/generated/at;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laxo;

    const-class v2, Lcom/twitter/database/generated/av;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laxn;

    const-class v2, Lcom/twitter/database/generated/au;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Layl;

    const-class v2, Lcom/twitter/database/generated/bt;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lawu;

    const-class v2, Lcom/twitter/database/generated/aa;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lawv;

    const-class v2, Lcom/twitter/database/generated/ab;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laww;

    const-class v2, Lcom/twitter/database/generated/ad;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lawy;

    const-class v2, Lcom/twitter/database/generated/af;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lawx;

    const-class v2, Lcom/twitter/database/generated/ae;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laxd;

    const-class v2, Lcom/twitter/database/generated/ak;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lazn;

    const-class v2, Lcom/twitter/database/generated/cv;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lazp;

    const-class v2, Lcom/twitter/database/generated/cx;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lazo;

    const-class v2, Lcom/twitter/database/generated/cw;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lawz;

    const-class v2, Lcom/twitter/database/generated/ag;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lawi;

    const-class v2, Lcom/twitter/database/generated/n;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laxe;

    const-class v2, Lcom/twitter/database/generated/al;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laxl;

    const-class v2, Lcom/twitter/database/generated/as;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laxs;

    const-class v2, Lcom/twitter/database/generated/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Layv;

    const-class v2, Lcom/twitter/database/generated/cd;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laxy;

    const-class v2, Lcom/twitter/database/generated/be;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laxz;

    const-class v2, Lcom/twitter/database/generated/bf;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laya;

    const-class v2, Lcom/twitter/database/generated/bg;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Layb;

    const-class v2, Lcom/twitter/database/generated/bh;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Layf;

    const-class v2, Lcom/twitter/database/generated/bl;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laxx;

    const-class v2, Lcom/twitter/database/generated/bc;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Layc;

    const-class v2, Lcom/twitter/database/generated/bi;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Layd;

    const-class v2, Lcom/twitter/database/generated/bj;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laye;

    const-class v2, Lcom/twitter/database/generated/bk;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laxa;

    const-class v2, Lcom/twitter/database/generated/ah;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Layh;

    const-class v2, Lcom/twitter/database/generated/bp;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Layi;

    const-class v2, Lcom/twitter/database/generated/bq;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lawk;

    const-class v2, Lcom/twitter/database/generated/p;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lawl;

    const-class v2, Lcom/twitter/database/generated/q;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Layw;

    const-class v2, Lcom/twitter/database/generated/ce;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lawt;

    const-class v2, Lcom/twitter/database/generated/y;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lawq;

    const-class v2, Lcom/twitter/database/generated/v;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Layg;

    const-class v2, Lcom/twitter/database/generated/bn;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laym;

    const-class v2, Lcom/twitter/database/generated/bu;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lazq;

    const-class v2, Lcom/twitter/database/generated/cy;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Layy;

    const-class v2, Lcom/twitter/database/generated/cg;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Layz;

    const-class v2, Lcom/twitter/database/generated/ch;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laza;

    const-class v2, Lcom/twitter/database/generated/ci;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laxj;

    const-class v2, Lcom/twitter/database/generated/aq;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laxg;

    const-class v2, Lcom/twitter/database/generated/an;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lazk;

    const-class v2, Lcom/twitter/database/generated/cs;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lazg;

    const-class v2, Lcom/twitter/database/generated/co;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lazh;

    const-class v2, Lcom/twitter/database/generated/cp;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lazm;

    const-class v2, Lcom/twitter/database/generated/cu;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laxq;

    const-class v2, Lcom/twitter/database/generated/ax;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laxr;

    const-class v2, Lcom/twitter/database/generated/ay;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lazr;

    const-class v2, Lcom/twitter/database/generated/cz;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lazs;

    const-class v2, Lcom/twitter/database/generated/da;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lazc;

    const-class v2, Lcom/twitter/database/generated/ck;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lazd;

    const-class v2, Lcom/twitter/database/generated/cl;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laze;

    const-class v2, Lcom/twitter/database/generated/cm;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lazf;

    const-class v2, Lcom/twitter/database/generated/cn;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laxt;

    const-class v2, Lcom/twitter/database/generated/ba;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->c:Ljava/util/Map;

    .line 191
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->c:Ljava/util/Map;

    const-class v1, Layj;

    const-class v2, Lcom/twitter/database/generated/br;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->d:Ljava/util/Map;

    .line 195
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->d:Ljava/util/Map;

    const-class v1, Lawe;

    const-class v2, Lcom/twitter/database/generated/j;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->d:Ljava/util/Map;

    const-class v1, Layq;

    const-class v2, Lcom/twitter/database/generated/by;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->d:Ljava/util/Map;

    const-class v1, Lawv$b;

    const-class v2, Lcom/twitter/database/generated/ac;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->d:Ljava/util/Map;

    const-class v1, Lawj;

    const-class v2, Lcom/twitter/database/generated/o;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->d:Ljava/util/Map;

    const-class v1, Layf$b;

    const-class v2, Lcom/twitter/database/generated/bm;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->d:Ljava/util/Map;

    const-class v1, Laxx$c;

    const-class v2, Lcom/twitter/database/generated/bd;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->d:Ljava/util/Map;

    const-class v1, Layk;

    const-class v2, Lcom/twitter/database/generated/bs;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->d:Ljava/util/Map;

    const-class v1, Lawm;

    const-class v2, Lcom/twitter/database/generated/r;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->d:Ljava/util/Map;

    const-class v1, Lawt$c;

    const-class v2, Lcom/twitter/database/generated/z;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->d:Ljava/util/Map;

    const-class v1, Layg$c;

    const-class v2, Lcom/twitter/database/generated/bo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->d:Ljava/util/Map;

    const-class v1, Layn;

    const-class v2, Lcom/twitter/database/generated/bv;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->d:Ljava/util/Map;

    const-class v1, Lazb;

    const-class v2, Lcom/twitter/database/generated/cj;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->d:Ljava/util/Map;

    const-class v1, Lazj;

    const-class v2, Lcom/twitter/database/generated/cr;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->d:Ljava/util/Map;

    const-class v1, Lazl;

    const-class v2, Lcom/twitter/database/generated/ct;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->d:Ljava/util/Map;

    const-class v1, Lazi;

    const-class v2, Lcom/twitter/database/generated/cq;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->d:Ljava/util/Map;

    const-class v1, Laxu;

    const-class v2, Lcom/twitter/database/generated/bb;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    return-void
.end method

.method public constructor <init>(Lcom/twitter/database/model/a;)V
    .locals 1
    .annotation build Lcod;
    .end annotation

    .prologue
    .line 215
    invoke-direct {p0, p1}, Lcom/twitter/database/internal/f;-><init>(Lcom/twitter/database/model/a;)V

    .line 216
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/twitter/database/model/a;->a(Z)V

    .line 217
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 222
    const-string/jumbo v0, "twitter_schema"

    return-object v0
.end method

.method protected final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 228
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->b:Ljava/util/Map;

    return-object v0
.end method

.method protected final c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/l;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/l;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 234
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->c:Ljava/util/Map;

    return-object v0
.end method

.method protected final d()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/m;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/m;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 240
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$TwitterSchema$$Impl;->d:Ljava/util/Map;

    return-object v0
.end method
