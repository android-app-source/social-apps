.class public final Lcom/twitter/database/generated/f;
.super Lcom/twitter/database/internal/k;
.source "Twttr"

# interfaces
.implements Lcom/twitter/database/schema/DraftsSchema$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/database/generated/f$a;,
        Lcom/twitter/database/generated/f$b;
    }
.end annotation


# static fields
.field private static final b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final c:[Lcom/twitter/database/model/d;

.field private static final d:[Ljava/lang/String;


# instance fields
.field private final e:Lcom/twitter/database/internal/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/internal/i",
            "<",
            "Lcom/twitter/database/schema/DraftsSchema$a$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 38
    invoke-static {}, Lcom/twitter/util/collection/o;->f()Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/twitter/database/generated/f;->b:Ljava/util/Collection;

    .line 41
    new-array v0, v2, [Lcom/twitter/database/model/d;

    sput-object v0, Lcom/twitter/database/generated/f;->c:[Lcom/twitter/database/model/d;

    .line 46
    const/16 v0, 0x14

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "_id"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string/jumbo v2, "content"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "in_r_status_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "updated_at"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "sending_state"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "pc"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "quoted_tweet_data"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "media"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "prepared_media_ids"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "media_prepared_at"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "geo_tag"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "card_url"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "poll"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "reply_prefill_disabled"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "semantic_core_ids"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "tweet_preview_info"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "engagement_metadata"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "excluded_recipients"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "periscope_creator_id"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "periscope_is_live"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/database/generated/f;->d:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/database/internal/f;)V
    .locals 2
    .annotation build Lcod;
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/twitter/database/internal/k;-><init>(Lcom/twitter/database/internal/f;)V

    .line 75
    new-instance v0, Lcom/twitter/database/generated/f$b;

    iget-object v1, p0, Lcom/twitter/database/generated/f;->f_:Lcom/twitter/database/internal/f;

    invoke-direct {v0, p0, v1}, Lcom/twitter/database/generated/f$b;-><init>(Lcom/twitter/database/generated/f;Lcom/twitter/database/internal/f;)V

    iput-object v0, p0, Lcom/twitter/database/generated/f;->e:Lcom/twitter/database/internal/i;

    .line 76
    return-void
.end method

.method static synthetic g()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/twitter/database/generated/f;->d:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    const-string/jumbo v0, "drafts"

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    const-string/jumbo v0, "CREATE TABLE drafts (\n\t_id INTEGER PRIMARY KEY AUTOINCREMENT,\n\tcontent TEXT,\n\tin_r_status_id INTEGER,\n\tupdated_at INTEGER,\n\tsending_state INTEGER DEFAULT 0,\n\tpc BLOB /*NULLABLE*/,\n\tquoted_tweet_data BLOB /*NULLABLE*/,\n\tmedia BLOB /*NULLABLE*/,\n\tprepared_media_ids BLOB /*NULLABLE*/,\n\tmedia_prepared_at INTEGER,\n\tgeo_tag BLOB /*NULLABLE*/,\n\tcard_url TEXT,\n\tpoll BLOB /*NULLABLE*/,\n\treply_prefill_disabled INTEGER DEFAULT 0,\n\tsemantic_core_ids BLOB /*NULLABLE*/,\n\ttweet_preview_info BLOB /*NULLABLE*/,\n\tengagement_metadata TEXT,\n\texcluded_recipients BLOB /*NULLABLE*/,\n\tperiscope_creator_id INTEGER,\n\tperiscope_is_live INTEGER\n);"

    return-object v0
.end method

.method protected final c()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 114
    sget-object v0, Lcom/twitter/database/generated/f;->b:Ljava/util/Collection;

    return-object v0
.end method

.method public final d()[Lcom/twitter/database/model/d;
    .locals 1

    .prologue
    .line 120
    sget-object v0, Lcom/twitter/database/generated/f;->c:[Lcom/twitter/database/model/d;

    return-object v0
.end method

.method public final e()Lcom/twitter/database/internal/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/database/internal/i",
            "<",
            "Lcom/twitter/database/schema/DraftsSchema$a$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lcom/twitter/database/generated/f;->e:Lcom/twitter/database/internal/i;

    return-object v0
.end method

.method public synthetic f()Lcom/twitter/database/model/l;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/twitter/database/generated/f;->e()Lcom/twitter/database/internal/i;

    move-result-object v0

    return-object v0
.end method
