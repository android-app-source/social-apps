.class Lcom/twitter/database/generated/da$a$6;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laxp$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/database/generated/da$a;-><init>(Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/database/generated/da$a;


# direct methods
.method constructor <init>(Lcom/twitter/database/generated/da$a;)V
    .locals 0

    .prologue
    .line 1415
    iput-object p1, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public A()J
    .locals 2

    .prologue
    .line 1602
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xad

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public B()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1608
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xae

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public C()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1614
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xaf

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public D()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1635
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xb2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()J
    .locals 2

    .prologue
    .line 1422
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x93

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 1431
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x94

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1437
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x95

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1443
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x96

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1449
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x97

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/twitter/model/core/v;
    .locals 2

    .prologue
    const/16 v1, 0x98

    .line 1455
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1456
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1462
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x99

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/twitter/model/core/v;
    .locals 2

    .prologue
    const/16 v1, 0x9a

    .line 1468
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1469
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    goto :goto_0
.end method

.method public i()I
    .locals 2

    .prologue
    .line 1474
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x9b

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public j()I
    .locals 2

    .prologue
    .line 1479
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x9c

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1485
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x9d

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1494
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x9e

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()Lcom/twitter/model/profile/ExtendedProfile;
    .locals 2

    .prologue
    const/16 v1, 0x9f

    .line 1503
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1504
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/profile/ExtendedProfile;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/profile/ExtendedProfile;

    goto :goto_0
.end method

.method public n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1510
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xa0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public o()Lcom/twitter/util/collection/k;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/geo/TwitterPlace;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v1, 0xa1

    .line 1516
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1517
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/database/generated/a;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/k;

    goto :goto_0
.end method

.method public p()I
    .locals 2

    .prologue
    .line 1526
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xa2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public q()J
    .locals 2

    .prologue
    .line 1534
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xa3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public r()J
    .locals 2

    .prologue
    .line 1542
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xa4

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public s()J
    .locals 2

    .prologue
    .line 1550
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xa5

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public t()J
    .locals 2

    .prologue
    .line 1558
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xa6

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public u()J
    .locals 2

    .prologue
    .line 1566
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xa7

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public v()J
    .locals 2

    .prologue
    .line 1571
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xa8

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public w()I
    .locals 2

    .prologue
    .line 1576
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xa9

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public x()J
    .locals 2

    .prologue
    .line 1581
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xaa

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public y()J
    .locals 2

    .prologue
    .line 1589
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xab

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public z()J
    .locals 2

    .prologue
    .line 1597
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$6;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xac

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method
