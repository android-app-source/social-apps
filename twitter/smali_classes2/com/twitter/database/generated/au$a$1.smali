.class Lcom/twitter/database/generated/au$a$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laxp$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/database/generated/au$a;-><init>(Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/database/generated/au$a;


# direct methods
.method constructor <init>(Lcom/twitter/database/generated/au$a;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public A()J
    .locals 2

    .prologue
    .line 417
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public B()Ljava/lang/String;
    .locals 2

    .prologue
    .line 423
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public C()Ljava/lang/String;
    .locals 2

    .prologue
    .line 429
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public D()Ljava/lang/String;
    .locals 2

    .prologue
    .line 450
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()J
    .locals 2

    .prologue
    .line 237
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x9

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 246
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xb

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 258
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xc

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 264
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xd

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/twitter/model/core/v;
    .locals 2

    .prologue
    const/16 v1, 0xe

    .line 270
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 271
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 277
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xf

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/twitter/model/core/v;
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 283
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 284
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    goto :goto_0
.end method

.method public i()I
    .locals 2

    .prologue
    .line 289
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x11

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public j()I
    .locals 2

    .prologue
    .line 294
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x12

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x13

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 309
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x14

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()Lcom/twitter/model/profile/ExtendedProfile;
    .locals 2

    .prologue
    const/16 v1, 0x15

    .line 318
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 319
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/profile/ExtendedProfile;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/profile/ExtendedProfile;

    goto :goto_0
.end method

.method public n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 325
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public o()Lcom/twitter/util/collection/k;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/geo/TwitterPlace;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v1, 0x17

    .line 331
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 332
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/database/generated/a;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/k;

    goto :goto_0
.end method

.method public p()I
    .locals 2

    .prologue
    .line 341
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public q()J
    .locals 2

    .prologue
    .line 349
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public r()J
    .locals 2

    .prologue
    .line 357
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x1a

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public s()J
    .locals 2

    .prologue
    .line 365
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x1b

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public t()J
    .locals 2

    .prologue
    .line 373
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x1c

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public u()J
    .locals 2

    .prologue
    .line 381
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x1d

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public v()J
    .locals 2

    .prologue
    .line 386
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x1e

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public w()I
    .locals 2

    .prologue
    .line 391
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x1f

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public x()J
    .locals 2

    .prologue
    .line 396
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public y()J
    .locals 2

    .prologue
    .line 404
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public z()J
    .locals 2

    .prologue
    .line 412
    iget-object v0, p0, Lcom/twitter/database/generated/au$a$1;->a:Lcom/twitter/database/generated/au$a;

    invoke-static {v0}, Lcom/twitter/database/generated/au$a;->a(Lcom/twitter/database/generated/au$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method
