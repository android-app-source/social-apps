.class Lcom/twitter/database/generated/ay$a$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laxp$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/database/generated/ay$a;-><init>(Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/database/generated/ay$a;


# direct methods
.method constructor <init>(Lcom/twitter/database/generated/ay$a;)V
    .locals 0

    .prologue
    .line 777
    iput-object p1, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public A()J
    .locals 2

    .prologue
    .line 964
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x5e

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public B()Ljava/lang/String;
    .locals 2

    .prologue
    .line 970
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x5f

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public C()Ljava/lang/String;
    .locals 2

    .prologue
    .line 976
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x60

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public D()Ljava/lang/String;
    .locals 2

    .prologue
    .line 997
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x63

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()J
    .locals 2

    .prologue
    .line 784
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x44

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 793
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x45

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 799
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x46

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 805
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x47

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 811
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x48

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/twitter/model/core/v;
    .locals 2

    .prologue
    const/16 v1, 0x49

    .line 817
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 818
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 824
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x4a

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/twitter/model/core/v;
    .locals 2

    .prologue
    const/16 v1, 0x4b

    .line 830
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 831
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    goto :goto_0
.end method

.method public i()I
    .locals 2

    .prologue
    .line 836
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x4c

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public j()I
    .locals 2

    .prologue
    .line 841
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x4d

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 847
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x4e

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 856
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x4f

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()Lcom/twitter/model/profile/ExtendedProfile;
    .locals 2

    .prologue
    const/16 v1, 0x50

    .line 865
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 866
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/profile/ExtendedProfile;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/profile/ExtendedProfile;

    goto :goto_0
.end method

.method public n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 872
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x51

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public o()Lcom/twitter/util/collection/k;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/geo/TwitterPlace;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v1, 0x52

    .line 878
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 879
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/database/generated/a;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/k;

    goto :goto_0
.end method

.method public p()I
    .locals 2

    .prologue
    .line 888
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x53

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public q()J
    .locals 2

    .prologue
    .line 896
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public r()J
    .locals 2

    .prologue
    .line 904
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x55

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public s()J
    .locals 2

    .prologue
    .line 912
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x56

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public t()J
    .locals 2

    .prologue
    .line 920
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public u()J
    .locals 2

    .prologue
    .line 928
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x58

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public v()J
    .locals 2

    .prologue
    .line 933
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x59

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public w()I
    .locals 2

    .prologue
    .line 938
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x5a

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public x()J
    .locals 2

    .prologue
    .line 943
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x5b

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public y()J
    .locals 2

    .prologue
    .line 951
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x5c

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public z()J
    .locals 2

    .prologue
    .line 959
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$3;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method
