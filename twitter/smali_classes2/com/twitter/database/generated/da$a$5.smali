.class Lcom/twitter/database/generated/da$a$5;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laxp$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/database/generated/da$a;-><init>(Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/database/generated/da$a;


# direct methods
.method constructor <init>(Lcom/twitter/database/generated/da$a;)V
    .locals 0

    .prologue
    .line 1192
    iput-object p1, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public A()J
    .locals 2

    .prologue
    .line 1379
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x8d

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public B()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1385
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x8e

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public C()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1391
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x8f

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public D()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1412
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x92

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()J
    .locals 2

    .prologue
    .line 1199
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x73

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 1208
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x74

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1214
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x75

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1220
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x76

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1226
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x77

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/twitter/model/core/v;
    .locals 2

    .prologue
    const/16 v1, 0x78

    .line 1232
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1233
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1239
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x79

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/twitter/model/core/v;
    .locals 2

    .prologue
    const/16 v1, 0x7a

    .line 1245
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1246
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    goto :goto_0
.end method

.method public i()I
    .locals 2

    .prologue
    .line 1251
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x7b

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public j()I
    .locals 2

    .prologue
    .line 1256
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x7c

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1262
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1271
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x7e

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()Lcom/twitter/model/profile/ExtendedProfile;
    .locals 2

    .prologue
    const/16 v1, 0x7f

    .line 1280
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1281
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/profile/ExtendedProfile;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/profile/ExtendedProfile;

    goto :goto_0
.end method

.method public n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1287
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x80

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public o()Lcom/twitter/util/collection/k;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/geo/TwitterPlace;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v1, 0x81

    .line 1293
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1294
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/database/generated/a;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/k;

    goto :goto_0
.end method

.method public p()I
    .locals 2

    .prologue
    .line 1303
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x82

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public q()J
    .locals 2

    .prologue
    .line 1311
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x83

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public r()J
    .locals 2

    .prologue
    .line 1319
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x84

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public s()J
    .locals 2

    .prologue
    .line 1327
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x85

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public t()J
    .locals 2

    .prologue
    .line 1335
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x86

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public u()J
    .locals 2

    .prologue
    .line 1343
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x87

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public v()J
    .locals 2

    .prologue
    .line 1348
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x88

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public w()I
    .locals 2

    .prologue
    .line 1353
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x89

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public x()J
    .locals 2

    .prologue
    .line 1358
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x8a

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public y()J
    .locals 2

    .prologue
    .line 1366
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x8b

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public z()J
    .locals 2

    .prologue
    .line 1374
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$5;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x8c

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method
