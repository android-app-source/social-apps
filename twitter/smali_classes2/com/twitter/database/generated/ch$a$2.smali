.class Lcom/twitter/database/generated/ch$a$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laxp$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/database/generated/ch$a;-><init>(Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/database/generated/ch$a;


# direct methods
.method constructor <init>(Lcom/twitter/database/generated/ch$a;)V
    .locals 0

    .prologue
    .line 1826
    iput-object p1, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public A()J
    .locals 2

    .prologue
    .line 2013
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xc7

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public B()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2019
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xc8

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public C()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2025
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xc9

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public D()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2046
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xcc

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()J
    .locals 2

    .prologue
    .line 1833
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xad

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 1842
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xae

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1848
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xaf

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1854
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xb0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1860
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xb1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/twitter/model/core/v;
    .locals 2

    .prologue
    const/16 v1, 0xb2

    .line 1866
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1867
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1873
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xb3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/twitter/model/core/v;
    .locals 2

    .prologue
    const/16 v1, 0xb4

    .line 1879
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1880
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    goto :goto_0
.end method

.method public i()I
    .locals 2

    .prologue
    .line 1885
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xb5

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public j()I
    .locals 2

    .prologue
    .line 1890
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xb6

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1896
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xb7

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1905
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xb8

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()Lcom/twitter/model/profile/ExtendedProfile;
    .locals 2

    .prologue
    const/16 v1, 0xb9

    .line 1914
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1915
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/profile/ExtendedProfile;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/profile/ExtendedProfile;

    goto :goto_0
.end method

.method public n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1921
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xba

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public o()Lcom/twitter/util/collection/k;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/geo/TwitterPlace;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v1, 0xbb

    .line 1927
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1928
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/database/generated/a;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/k;

    goto :goto_0
.end method

.method public p()I
    .locals 2

    .prologue
    .line 1937
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xbc

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public q()J
    .locals 2

    .prologue
    .line 1945
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xbd

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public r()J
    .locals 2

    .prologue
    .line 1953
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xbe

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public s()J
    .locals 2

    .prologue
    .line 1961
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xbf

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public t()J
    .locals 2

    .prologue
    .line 1969
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xc0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public u()J
    .locals 2

    .prologue
    .line 1977
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xc1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public v()J
    .locals 2

    .prologue
    .line 1982
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xc2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public w()I
    .locals 2

    .prologue
    .line 1987
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xc3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public x()J
    .locals 2

    .prologue
    .line 1992
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xc4

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public y()J
    .locals 2

    .prologue
    .line 2000
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xc5

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public z()J
    .locals 2

    .prologue
    .line 2008
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$2;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xc6

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method
