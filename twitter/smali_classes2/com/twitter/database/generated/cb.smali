.class public final Lcom/twitter/database/generated/cb;
.super Lcom/twitter/database/internal/k;
.source "Twttr"

# interfaces
.implements Layt;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/database/generated/cb$a;,
        Lcom/twitter/database/generated/cb$b;
    }
.end annotation


# static fields
.field private static final b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final c:[Lcom/twitter/database/model/d;

.field private static final d:[Ljava/lang/String;


# instance fields
.field private final e:Lcom/twitter/database/internal/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/internal/i",
            "<",
            "Layt$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 30
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0, v4}, Ljava/util/LinkedHashSet;-><init>(I)V

    sput-object v0, Lcom/twitter/database/generated/cb;->b:Ljava/util/Collection;

    .line 33
    new-array v0, v4, [Lcom/twitter/database/model/d;

    new-instance v1, Lcom/twitter/database/model/d;

    const-string/jumbo v2, "carousel_collection_id_index"

    const-string/jumbo v3, "CREATE INDEX carousel_collection_id_index ON carousel (\n\tcollection_id\n);"

    invoke-direct {v1, v2, v3}, Lcom/twitter/database/model/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v0, v5

    sput-object v0, Lcom/twitter/database/generated/cb;->c:[Lcom/twitter/database/model/d;

    .line 40
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "_id"

    aput-object v1, v0, v5

    const-string/jumbo v1, "collection_id"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string/jumbo v2, "item_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "sort_index"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/database/generated/cb;->d:[Ljava/lang/String;

    .line 50
    sget-object v0, Lcom/twitter/database/generated/cb;->b:Ljava/util/Collection;

    const-class v1, Layu;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 51
    return-void
.end method

.method public constructor <init>(Lcom/twitter/database/internal/f;)V
    .locals 2
    .annotation build Lcod;
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/twitter/database/internal/k;-><init>(Lcom/twitter/database/internal/f;)V

    .line 59
    new-instance v0, Lcom/twitter/database/generated/cb$b;

    iget-object v1, p0, Lcom/twitter/database/generated/cb;->f_:Lcom/twitter/database/internal/f;

    invoke-direct {v0, p0, v1}, Lcom/twitter/database/generated/cb$b;-><init>(Lcom/twitter/database/generated/cb;Lcom/twitter/database/internal/f;)V

    iput-object v0, p0, Lcom/twitter/database/generated/cb;->e:Lcom/twitter/database/internal/i;

    .line 60
    return-void
.end method

.method static synthetic g()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/twitter/database/generated/cb;->d:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    const-string/jumbo v0, "carousel"

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    const-string/jumbo v0, "CREATE TABLE carousel (\n\t_id INTEGER PRIMARY KEY,\n\tcollection_id INTEGER NOT NULL,\n\titem_id INTEGER NOT NULL,\n\tsort_index INTEGER NOT NULL,\n\ttype INTEGER NOT NULL,\n\tdata BLOB\n);"

    return-object v0
.end method

.method protected final c()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 84
    sget-object v0, Lcom/twitter/database/generated/cb;->b:Ljava/util/Collection;

    return-object v0
.end method

.method public final d()[Lcom/twitter/database/model/d;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/twitter/database/generated/cb;->c:[Lcom/twitter/database/model/d;

    return-object v0
.end method

.method public final e()Lcom/twitter/database/internal/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/database/internal/i",
            "<",
            "Layt$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lcom/twitter/database/generated/cb;->e:Lcom/twitter/database/internal/i;

    return-object v0
.end method

.method public synthetic f()Lcom/twitter/database/model/l;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/twitter/database/generated/cb;->e()Lcom/twitter/database/internal/i;

    move-result-object v0

    return-object v0
.end method
