.class Lcom/twitter/database/generated/da$a$7;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laxl$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/database/generated/da$a;-><init>(Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/database/generated/da$a;


# direct methods
.method constructor <init>(Lcom/twitter/database/generated/da$a;)V
    .locals 0

    .prologue
    .line 1638
    iput-object p1, p0, Lcom/twitter/database/generated/da$a$7;->a:Lcom/twitter/database/generated/da$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 1645
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$7;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xb3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1655
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$7;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xb4

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1672
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$7;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xb6

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1690
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$7;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xb8

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1699
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$7;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xb9

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 1759
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$7;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xc0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public g()[B
    .locals 2

    .prologue
    .line 1778
    iget-object v0, p0, Lcom/twitter/database/generated/da$a$7;->a:Lcom/twitter/database/generated/da$a;

    invoke-static {v0}, Lcom/twitter/database/generated/da$a;->a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xc2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    return-object v0
.end method
