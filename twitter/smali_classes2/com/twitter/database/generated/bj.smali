.class public final Lcom/twitter/database/generated/bj;
.super Lcom/twitter/database/internal/k;
.source "Twttr"

# interfaces
.implements Layd;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/database/generated/bj$a;,
        Lcom/twitter/database/generated/bj$b;
    }
.end annotation


# static fields
.field private static final b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final c:[Lcom/twitter/database/model/d;

.field private static final d:[Ljava/lang/String;


# instance fields
.field private final e:Lcom/twitter/database/internal/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/internal/i",
            "<",
            "Layd$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 31
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0, v4}, Ljava/util/LinkedHashSet;-><init>(I)V

    sput-object v0, Lcom/twitter/database/generated/bj;->b:Ljava/util/Collection;

    .line 34
    new-array v0, v4, [Lcom/twitter/database/model/d;

    new-instance v1, Lcom/twitter/database/model/d;

    const-string/jumbo v2, "moments_pages_moment_id_index"

    const-string/jumbo v3, "CREATE INDEX moments_pages_moment_id_index ON moments_pages (\n\tmoment_id\n);"

    invoke-direct {v1, v2, v3}, Lcom/twitter/database/model/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v0, v5

    sput-object v0, Lcom/twitter/database/generated/bj;->c:[Lcom/twitter/database/model/d;

    .line 41
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "_id"

    aput-object v1, v0, v5

    const-string/jumbo v1, "moment_id"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string/jumbo v2, "page_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "tweet_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "page_number"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "last_read_timestamp"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "content_version"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "capsule_page_data"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "meta_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/database/generated/bj;->d:[Ljava/lang/String;

    .line 54
    sget-object v0, Lcom/twitter/database/generated/bj;->b:Ljava/util/Collection;

    const-class v1, Laye;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 55
    return-void
.end method

.method public constructor <init>(Lcom/twitter/database/internal/f;)V
    .locals 2
    .annotation build Lcod;
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/twitter/database/internal/k;-><init>(Lcom/twitter/database/internal/f;)V

    .line 63
    new-instance v0, Lcom/twitter/database/generated/bj$b;

    iget-object v1, p0, Lcom/twitter/database/generated/bj;->f_:Lcom/twitter/database/internal/f;

    invoke-direct {v0, p0, v1}, Lcom/twitter/database/generated/bj$b;-><init>(Lcom/twitter/database/generated/bj;Lcom/twitter/database/internal/f;)V

    iput-object v0, p0, Lcom/twitter/database/generated/bj;->e:Lcom/twitter/database/internal/i;

    .line 64
    return-void
.end method

.method static synthetic g()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/twitter/database/generated/bj;->d:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    const-string/jumbo v0, "moments_pages"

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    const-string/jumbo v0, "CREATE TABLE moments_pages (\n\t_id INTEGER PRIMARY KEY,\n\tmoment_id INTEGER,\n\tpage_id TEXT NOT NULL,\n\ttweet_id INTEGER,\n\tpage_number INTEGER DEFAULT 0,\n\tlast_read_timestamp INTEGER /*NULLABLE*/,\n\tcontent_version INTEGER,\n\tcapsule_page_data BLOB NOT NULL,\n\tmeta_type INTEGER\n);"

    return-object v0
.end method

.method protected final c()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 91
    sget-object v0, Lcom/twitter/database/generated/bj;->b:Ljava/util/Collection;

    return-object v0
.end method

.method public final d()[Lcom/twitter/database/model/d;
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/twitter/database/generated/bj;->c:[Lcom/twitter/database/model/d;

    return-object v0
.end method

.method public final e()Lcom/twitter/database/internal/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/database/internal/i",
            "<",
            "Layd$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/twitter/database/generated/bj;->e:Lcom/twitter/database/internal/i;

    return-object v0
.end method

.method public synthetic f()Lcom/twitter/database/model/l;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/twitter/database/generated/bj;->e()Lcom/twitter/database/internal/i;

    move-result-object v0

    return-object v0
.end method
