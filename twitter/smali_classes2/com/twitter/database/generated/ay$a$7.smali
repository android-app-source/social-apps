.class Lcom/twitter/database/generated/ay$a$7;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laxp$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/database/generated/ay$a;-><init>(Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/database/generated/ay$a;


# direct methods
.method constructor <init>(Lcom/twitter/database/generated/ay$a;)V
    .locals 0

    .prologue
    .line 1505
    iput-object p1, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public A()J
    .locals 2

    .prologue
    .line 1692
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xb5

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public B()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1698
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xb6

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public C()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1704
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xb7

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public D()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1725
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xba

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()J
    .locals 2

    .prologue
    .line 1512
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x9b

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 1521
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x9c

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1527
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x9d

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1533
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x9e

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1539
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x9f

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/twitter/model/core/v;
    .locals 2

    .prologue
    const/16 v1, 0xa0

    .line 1545
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1546
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1552
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xa1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/twitter/model/core/v;
    .locals 2

    .prologue
    const/16 v1, 0xa2

    .line 1558
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1559
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    goto :goto_0
.end method

.method public i()I
    .locals 2

    .prologue
    .line 1564
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xa3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public j()I
    .locals 2

    .prologue
    .line 1569
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xa4

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1575
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xa5

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1584
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xa6

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()Lcom/twitter/model/profile/ExtendedProfile;
    .locals 2

    .prologue
    const/16 v1, 0xa7

    .line 1593
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1594
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/profile/ExtendedProfile;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/profile/ExtendedProfile;

    goto :goto_0
.end method

.method public n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1600
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xa8

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public o()Lcom/twitter/util/collection/k;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/geo/TwitterPlace;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v1, 0xa9

    .line 1606
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1607
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/database/generated/a;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/k;

    goto :goto_0
.end method

.method public p()I
    .locals 2

    .prologue
    .line 1616
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xaa

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public q()J
    .locals 2

    .prologue
    .line 1624
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xab

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public r()J
    .locals 2

    .prologue
    .line 1632
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xac

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public s()J
    .locals 2

    .prologue
    .line 1640
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xad

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public t()J
    .locals 2

    .prologue
    .line 1648
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xae

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public u()J
    .locals 2

    .prologue
    .line 1656
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xaf

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public v()J
    .locals 2

    .prologue
    .line 1661
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xb0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public w()I
    .locals 2

    .prologue
    .line 1666
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xb1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public x()J
    .locals 2

    .prologue
    .line 1671
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xb2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public y()J
    .locals 2

    .prologue
    .line 1679
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xb3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public z()J
    .locals 2

    .prologue
    .line 1687
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a$7;->a:Lcom/twitter/database/generated/ay$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ay$a;->a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xb4

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method
