.class Lcom/twitter/database/generated/bi$a$7;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laxp$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/database/generated/bi$a;-><init>(Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/database/generated/bi$a;


# direct methods
.method constructor <init>(Lcom/twitter/database/generated/bi$a;)V
    .locals 0

    .prologue
    .line 985
    iput-object p1, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public A()J
    .locals 2

    .prologue
    .line 1172
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public B()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1178
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x7e

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public C()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1184
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x7f

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public D()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1205
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x82

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()J
    .locals 2

    .prologue
    .line 992
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x63

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 1001
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x64

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1007
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x65

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1013
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x66

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1019
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x67

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/twitter/model/core/v;
    .locals 2

    .prologue
    const/16 v1, 0x68

    .line 1025
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1026
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1032
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x69

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/twitter/model/core/v;
    .locals 2

    .prologue
    const/16 v1, 0x6a

    .line 1038
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1039
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    goto :goto_0
.end method

.method public i()I
    .locals 2

    .prologue
    .line 1044
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x6b

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public j()I
    .locals 2

    .prologue
    .line 1049
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x6c

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1055
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x6d

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x6e

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()Lcom/twitter/model/profile/ExtendedProfile;
    .locals 2

    .prologue
    const/16 v1, 0x6f

    .line 1073
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1074
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/profile/ExtendedProfile;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/profile/ExtendedProfile;

    goto :goto_0
.end method

.method public n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1080
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x70

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public o()Lcom/twitter/util/collection/k;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/geo/TwitterPlace;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v1, 0x71

    .line 1086
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1087
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/database/generated/a;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/k;

    goto :goto_0
.end method

.method public p()I
    .locals 2

    .prologue
    .line 1096
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x72

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public q()J
    .locals 2

    .prologue
    .line 1104
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x73

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public r()J
    .locals 2

    .prologue
    .line 1112
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x74

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public s()J
    .locals 2

    .prologue
    .line 1120
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x75

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public t()J
    .locals 2

    .prologue
    .line 1128
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x76

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public u()J
    .locals 2

    .prologue
    .line 1136
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x77

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public v()J
    .locals 2

    .prologue
    .line 1141
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x78

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public w()I
    .locals 2

    .prologue
    .line 1146
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x79

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public x()J
    .locals 2

    .prologue
    .line 1151
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x7a

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public y()J
    .locals 2

    .prologue
    .line 1159
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x7b

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public z()J
    .locals 2

    .prologue
    .line 1167
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$7;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x7c

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method
