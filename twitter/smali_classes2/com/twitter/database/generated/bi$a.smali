.class final Lcom/twitter/database/generated/bi$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Layc$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/database/generated/bi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/database/Cursor;

.field private final b:Laxz$a;

.field private final c:Laxy$a;

.field private final d:Layb$a;

.field private final e:Laxk$a;

.field private final f:Lawp$a;

.field private final g:Laxp$a;

.field private final h:Laxp$a;

.field private final i:Laxi$a;

.field private final j:Laxh$a;

.field private final k:Laxx$a;


# direct methods
.method private constructor <init>(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 520
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 521
    iput-object p1, p0, Lcom/twitter/database/generated/bi$a;->a:Landroid/database/Cursor;

    .line 522
    new-instance v0, Lcom/twitter/database/generated/bi$a$1;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/bi$a$1;-><init>(Lcom/twitter/database/generated/bi$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/bi$a;->b:Laxz$a;

    .line 610
    new-instance v0, Lcom/twitter/database/generated/bi$a$3;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/bi$a$3;-><init>(Lcom/twitter/database/generated/bi$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/bi$a;->c:Laxy$a;

    .line 732
    new-instance v0, Lcom/twitter/database/generated/bi$a$4;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/bi$a$4;-><init>(Lcom/twitter/database/generated/bi$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/bi$a;->d:Layb$a;

    .line 760
    new-instance v0, Lcom/twitter/database/generated/bi$a$5;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/bi$a$5;-><init>(Lcom/twitter/database/generated/bi$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/bi$a;->e:Laxk$a;

    .line 959
    new-instance v0, Lcom/twitter/database/generated/bi$a$6;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/bi$a$6;-><init>(Lcom/twitter/database/generated/bi$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/bi$a;->f:Lawp$a;

    .line 985
    new-instance v0, Lcom/twitter/database/generated/bi$a$7;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/bi$a$7;-><init>(Lcom/twitter/database/generated/bi$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/bi$a;->g:Laxp$a;

    .line 1208
    new-instance v0, Lcom/twitter/database/generated/bi$a$8;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/bi$a$8;-><init>(Lcom/twitter/database/generated/bi$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/bi$a;->h:Laxp$a;

    .line 1431
    new-instance v0, Lcom/twitter/database/generated/bi$a$9;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/bi$a$9;-><init>(Lcom/twitter/database/generated/bi$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/bi$a;->i:Laxi$a;

    .line 1548
    new-instance v0, Lcom/twitter/database/generated/bi$a$10;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/bi$a$10;-><init>(Lcom/twitter/database/generated/bi$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/bi$a;->j:Laxh$a;

    .line 1713
    new-instance v0, Lcom/twitter/database/generated/bi$a$2;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/bi$a$2;-><init>(Lcom/twitter/database/generated/bi$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/bi$a;->k:Laxx$a;

    .line 1738
    return-void
.end method

.method synthetic constructor <init>(Landroid/database/Cursor;Lcom/twitter/database/generated/bi$1;)V
    .locals 0

    .prologue
    .line 486
    invoke-direct {p0, p1}, Lcom/twitter/database/generated/bi$a;-><init>(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a;->a:Landroid/database/Cursor;

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 1746
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a;->a:Landroid/database/Cursor;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method
