.class final Lcom/twitter/database/generated/ao$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laxh$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/database/generated/ao;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/database/Cursor;

.field private final b:Laxk$a;

.field private final c:Lawp$a;

.field private final d:Laxp$a;

.field private final e:Laxp$a;

.field private final f:Laxi$a;


# direct methods
.method private constructor <init>(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 419
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 420
    iput-object p1, p0, Lcom/twitter/database/generated/ao$a;->a:Landroid/database/Cursor;

    .line 421
    new-instance v0, Lcom/twitter/database/generated/ao$a$1;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ao$a$1;-><init>(Lcom/twitter/database/generated/ao$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ao$a;->b:Laxk$a;

    .line 620
    new-instance v0, Lcom/twitter/database/generated/ao$a$2;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ao$a$2;-><init>(Lcom/twitter/database/generated/ao$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ao$a;->c:Lawp$a;

    .line 646
    new-instance v0, Lcom/twitter/database/generated/ao$a$3;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ao$a$3;-><init>(Lcom/twitter/database/generated/ao$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ao$a;->d:Laxp$a;

    .line 869
    new-instance v0, Lcom/twitter/database/generated/ao$a$4;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ao$a$4;-><init>(Lcom/twitter/database/generated/ao$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ao$a;->e:Laxp$a;

    .line 1092
    new-instance v0, Lcom/twitter/database/generated/ao$a$5;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ao$a$5;-><init>(Lcom/twitter/database/generated/ao$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ao$a;->f:Laxi$a;

    .line 1209
    return-void
.end method

.method synthetic constructor <init>(Landroid/database/Cursor;Lcom/twitter/database/generated/ao$1;)V
    .locals 0

    .prologue
    .line 400
    invoke-direct {p0, p1}, Lcom/twitter/database/generated/ao$a;-><init>(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/database/generated/ao$a;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/twitter/database/generated/ao$a;->a:Landroid/database/Cursor;

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 1217
    iget-object v0, p0, Lcom/twitter/database/generated/ao$a;->a:Landroid/database/Cursor;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method
