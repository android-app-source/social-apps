.class Lcom/twitter/database/generated/az$a$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laxp$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/database/generated/az$a;-><init>(Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/database/generated/az$a;


# direct methods
.method constructor <init>(Lcom/twitter/database/generated/az$a;)V
    .locals 0

    .prologue
    .line 271
    iput-object p1, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public A()J
    .locals 2

    .prologue
    .line 458
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public B()Ljava/lang/String;
    .locals 2

    .prologue
    .line 464
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public C()Ljava/lang/String;
    .locals 2

    .prologue
    .line 470
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public D()Ljava/lang/String;
    .locals 2

    .prologue
    .line 491
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x35

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()J
    .locals 2

    .prologue
    .line 278
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 299
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 305
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x1a

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/twitter/model/core/v;
    .locals 2

    .prologue
    const/16 v1, 0x1b

    .line 311
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 312
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 318
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x1c

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/twitter/model/core/v;
    .locals 2

    .prologue
    const/16 v1, 0x1d

    .line 324
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 325
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    goto :goto_0
.end method

.method public i()I
    .locals 2

    .prologue
    .line 330
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x1e

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public j()I
    .locals 2

    .prologue
    .line 335
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x1f

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 341
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 350
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()Lcom/twitter/model/profile/ExtendedProfile;
    .locals 2

    .prologue
    const/16 v1, 0x22

    .line 359
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 360
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/profile/ExtendedProfile;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/profile/ExtendedProfile;

    goto :goto_0
.end method

.method public n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 366
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public o()Lcom/twitter/util/collection/k;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/geo/TwitterPlace;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v1, 0x24

    .line 372
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 373
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/database/generated/a;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/k;

    goto :goto_0
.end method

.method public p()I
    .locals 2

    .prologue
    .line 382
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public q()J
    .locals 2

    .prologue
    .line 390
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public r()J
    .locals 2

    .prologue
    .line 398
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public s()J
    .locals 2

    .prologue
    .line 406
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public t()J
    .locals 2

    .prologue
    .line 414
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x29

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public u()J
    .locals 2

    .prologue
    .line 422
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x2a

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public v()J
    .locals 2

    .prologue
    .line 427
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x2b

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public w()I
    .locals 2

    .prologue
    .line 432
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x2c

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public x()J
    .locals 2

    .prologue
    .line 437
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x2d

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public y()J
    .locals 2

    .prologue
    .line 445
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x2e

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public z()J
    .locals 2

    .prologue
    .line 453
    iget-object v0, p0, Lcom/twitter/database/generated/az$a$2;->a:Lcom/twitter/database/generated/az$a;

    invoke-static {v0}, Lcom/twitter/database/generated/az$a;->a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x2f

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method
