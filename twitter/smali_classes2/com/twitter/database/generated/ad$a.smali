.class final Lcom/twitter/database/generated/ad$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laww$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/database/generated/ad;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/database/Cursor;

.field private final b:Laxp$a;


# direct methods
.method private constructor <init>(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 191
    iput-object p1, p0, Lcom/twitter/database/generated/ad$a;->a:Landroid/database/Cursor;

    .line 192
    new-instance v0, Lcom/twitter/database/generated/ad$a$1;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ad$a$1;-><init>(Lcom/twitter/database/generated/ad$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ad$a;->b:Laxp$a;

    .line 415
    return-void
.end method

.method synthetic constructor <init>(Landroid/database/Cursor;Lcom/twitter/database/generated/ad$1;)V
    .locals 0

    .prologue
    .line 183
    invoke-direct {p0, p1}, Lcom/twitter/database/generated/ad$a;-><init>(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/database/generated/ad$a;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/twitter/database/generated/ad$a;->a:Landroid/database/Cursor;

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 423
    iget-object v0, p0, Lcom/twitter/database/generated/ad$a;->a:Landroid/database/Cursor;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 429
    iget-object v0, p0, Lcom/twitter/database/generated/ad$a;->a:Landroid/database/Cursor;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 438
    iget-object v0, p0, Lcom/twitter/database/generated/ad$a;->a:Landroid/database/Cursor;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 446
    iget-object v0, p0, Lcom/twitter/database/generated/ad$a;->a:Landroid/database/Cursor;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 463
    iget-object v0, p0, Lcom/twitter/database/generated/ad$a;->a:Landroid/database/Cursor;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 471
    iget-object v0, p0, Lcom/twitter/database/generated/ad$a;->a:Landroid/database/Cursor;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public g()Laxp$a;
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lcom/twitter/database/generated/ad$a;->b:Laxp$a;

    return-object v0
.end method
