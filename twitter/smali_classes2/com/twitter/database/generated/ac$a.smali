.class final Lcom/twitter/database/generated/ac$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lawv$b$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/database/generated/ac;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/content/ContentValues;


# direct methods
.method constructor <init>(Landroid/content/ContentValues;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/twitter/database/generated/ac$a;->a:Landroid/content/ContentValues;

    .line 43
    return-void
.end method


# virtual methods
.method public a(I)Lawv$b$a;
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Lcom/twitter/database/generated/ac$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "participant_type"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 73
    return-object p0
.end method

.method public a(J)Lawv$b$a;
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Lcom/twitter/database/generated/ac$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "user_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 59
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lawv$b$a;
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/twitter/database/generated/ac$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "conversation_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    return-object p0
.end method

.method public b(J)Lawv$b$a;
    .locals 3

    .prologue
    .line 65
    iget-object v0, p0, Lcom/twitter/database/generated/ac$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "join_time"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 66
    return-object p0
.end method

.method public c(J)Lawv$b$a;
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lcom/twitter/database/generated/ac$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "last_read_event_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 80
    return-object p0
.end method

.method public d(J)Lawv$b$a;
    .locals 3

    .prologue
    .line 86
    iget-object v0, p0, Lcom/twitter/database/generated/ac$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "join_conversation_event_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 87
    return-object p0
.end method
