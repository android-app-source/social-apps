.class public final Lcom/twitter/database/generated/ah;
.super Lcom/twitter/database/internal/m;
.source "Twttr"

# interfaces
.implements Laxa;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/database/generated/ah$a;,
        Lcom/twitter/database/generated/ah$b;
    }
.end annotation


# static fields
.field private static final b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final c:[Ljava/lang/String;


# instance fields
.field private final d:Lcom/twitter/database/internal/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/internal/i",
            "<",
            "Laxa$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 30
    invoke-static {}, Lcom/twitter/util/collection/o;->f()Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/twitter/database/generated/ah;->b:Ljava/util/Collection;

    .line 33
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "conversation_entries_entry_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "conversation_entries_sort_entry_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "conversation_entries_conversation_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "conversation_entries_user_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "conversation_entries_created"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "conversation_entries_entry_type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "conversation_entries_data"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "conversation_entries_request_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "users_username"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "users_name"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "users_image_url"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "is_unread"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "max_sort_entry_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/database/generated/ah;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/database/internal/f;)V
    .locals 2
    .annotation build Lcod;
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/twitter/database/internal/m;-><init>(Lcom/twitter/database/internal/f;)V

    .line 56
    new-instance v0, Lcom/twitter/database/generated/ah$b;

    iget-object v1, p0, Lcom/twitter/database/generated/ah;->f_:Lcom/twitter/database/internal/f;

    invoke-direct {v0, p0, v1}, Lcom/twitter/database/generated/ah$b;-><init>(Lcom/twitter/database/generated/ah;Lcom/twitter/database/internal/f;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ah;->d:Lcom/twitter/database/internal/i;

    .line 57
    return-void
.end method

.method static synthetic e()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/twitter/database/generated/ah;->c:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    const-string/jumbo v0, "most_recent_conversation_item"

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    const-string/jumbo v0, "CREATE VIEW most_recent_conversation_item\n\tAS SELECT\n\t\tconversation._id AS _id,\n\t\tconversation.conversation_entries_entry_id AS conversation_entries_entry_id,\n\t\tconversation.conversation_entries_sort_entry_id AS conversation_entries_sort_entry_id,\n\t\tconversation.conversation_entries_conversation_id AS conversation_entries_conversation_id,\n\t\tconversation.conversation_entries_user_id AS conversation_entries_user_id,\n\t\tconversation.conversation_entries_created AS conversation_entries_created,\n\t\tconversation.conversation_entries_entry_type AS conversation_entries_entry_type,\n\t\tconversation.conversation_entries_data AS conversation_entries_data,\n\t\tconversation.conversation_entries_request_id AS conversation_entries_request_id,\n\t\tconversation.users_username AS users_username,\n\t\tconversation.users_name AS users_name,\n\t\tconversation.users_image_url AS users_image_url,\n\t\tconversation.is_unread AS is_unread,\n\t\tmax(conversation_entries_sort_entry_id) AS max_sort_entry_id\n\tFROM conversation\n\tWHERE conversation_entries_entry_type IN(0,19,1,10,8,20,17,21,22)\n\tGROUP BY conversation_entries_conversation_id;"

    return-object v0
.end method

.method protected final c()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 92
    sget-object v0, Lcom/twitter/database/generated/ah;->b:Ljava/util/Collection;

    return-object v0
.end method

.method public final d()Lcom/twitter/database/internal/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/database/internal/i",
            "<",
            "Laxa$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lcom/twitter/database/generated/ah;->d:Lcom/twitter/database/internal/i;

    return-object v0
.end method

.method public synthetic f()Lcom/twitter/database/model/l;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/twitter/database/generated/ah;->d()Lcom/twitter/database/internal/i;

    move-result-object v0

    return-object v0
.end method
