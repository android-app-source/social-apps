.class public final Lcom/twitter/database/generated/com$twitter$database$schema$GlobalSchema$$Impl;
.super Lcom/twitter/database/internal/f;
.source "Twttr"

# interfaces
.implements Lcom/twitter/database/schema/GlobalSchema;


# annotations
.annotation build Lcod;
.end annotation


# static fields
.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/l;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/l;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/m;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/m;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 36
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$GlobalSchema$$Impl;->b:Ljava/util/Map;

    .line 37
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$GlobalSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Laxb;

    const-class v2, Lcom/twitter/database/generated/ai;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$GlobalSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lawf;

    const-class v2, Lcom/twitter/database/generated/k;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$GlobalSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lawg;

    const-class v2, Lcom/twitter/database/generated/l;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$GlobalSchema$$Impl;->b:Ljava/util/Map;

    const-class v1, Lawn;

    const-class v2, Lcom/twitter/database/generated/s;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$GlobalSchema$$Impl;->c:Ljava/util/Map;

    .line 46
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$GlobalSchema$$Impl;->d:Ljava/util/Map;

    .line 47
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$GlobalSchema$$Impl;->d:Ljava/util/Map;

    const-class v1, Laxc;

    const-class v2, Lcom/twitter/database/generated/aj;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$GlobalSchema$$Impl;->d:Ljava/util/Map;

    const-class v1, Lawh;

    const-class v2, Lcom/twitter/database/generated/m;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$GlobalSchema$$Impl;->d:Ljava/util/Map;

    const-class v1, Lawo;

    const-class v2, Lcom/twitter/database/generated/t;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    return-void
.end method

.method public constructor <init>(Lcom/twitter/database/model/a;)V
    .locals 0
    .annotation build Lcod;
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/twitter/database/internal/f;-><init>(Lcom/twitter/database/model/a;)V

    .line 55
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    const-string/jumbo v0, "global"

    return-object v0
.end method

.method protected final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 66
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$GlobalSchema$$Impl;->b:Ljava/util/Map;

    return-object v0
.end method

.method protected final c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/l;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/l;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 72
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$GlobalSchema$$Impl;->c:Ljava/util/Map;

    return-object v0
.end method

.method protected final d()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/m;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/m;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 78
    sget-object v0, Lcom/twitter/database/generated/com$twitter$database$schema$GlobalSchema$$Impl;->d:Ljava/util/Map;

    return-object v0
.end method
