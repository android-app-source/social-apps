.class Lcom/twitter/database/generated/bi$a$8;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laxp$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/database/generated/bi$a;-><init>(Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/database/generated/bi$a;


# direct methods
.method constructor <init>(Lcom/twitter/database/generated/bi$a;)V
    .locals 0

    .prologue
    .line 1208
    iput-object p1, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public A()J
    .locals 2

    .prologue
    .line 1395
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x9d

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public B()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1401
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x9e

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public C()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1407
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x9f

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public D()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1428
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xa2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()J
    .locals 2

    .prologue
    .line 1215
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x83

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 1224
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x84

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1230
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x85

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1236
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x86

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1242
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x87

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/twitter/model/core/v;
    .locals 2

    .prologue
    const/16 v1, 0x88

    .line 1248
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1249
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1255
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x89

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/twitter/model/core/v;
    .locals 2

    .prologue
    const/16 v1, 0x8a

    .line 1261
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1262
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    goto :goto_0
.end method

.method public i()I
    .locals 2

    .prologue
    .line 1267
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x8b

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public j()I
    .locals 2

    .prologue
    .line 1272
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x8c

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1278
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x8d

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1287
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x8e

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()Lcom/twitter/model/profile/ExtendedProfile;
    .locals 2

    .prologue
    const/16 v1, 0x8f

    .line 1296
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1297
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/profile/ExtendedProfile;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/profile/ExtendedProfile;

    goto :goto_0
.end method

.method public n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1303
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x90

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public o()Lcom/twitter/util/collection/k;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/geo/TwitterPlace;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v1, 0x91

    .line 1309
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1310
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/database/generated/a;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/k;

    goto :goto_0
.end method

.method public p()I
    .locals 2

    .prologue
    .line 1319
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x92

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public q()J
    .locals 2

    .prologue
    .line 1327
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x93

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public r()J
    .locals 2

    .prologue
    .line 1335
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x94

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public s()J
    .locals 2

    .prologue
    .line 1343
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x95

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public t()J
    .locals 2

    .prologue
    .line 1351
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x96

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public u()J
    .locals 2

    .prologue
    .line 1359
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x97

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public v()J
    .locals 2

    .prologue
    .line 1364
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x98

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public w()I
    .locals 2

    .prologue
    .line 1369
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x99

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public x()J
    .locals 2

    .prologue
    .line 1374
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x9a

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public y()J
    .locals 2

    .prologue
    .line 1382
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x9b

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public z()J
    .locals 2

    .prologue
    .line 1390
    iget-object v0, p0, Lcom/twitter/database/generated/bi$a$8;->a:Lcom/twitter/database/generated/bi$a;

    invoke-static {v0}, Lcom/twitter/database/generated/bi$a;->a(Lcom/twitter/database/generated/bi$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x9c

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method
