.class public final Lcom/twitter/database/generated/aw;
.super Lcom/twitter/database/internal/k;
.source "Twttr"

# interfaces
.implements Laxp;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/database/generated/aw$a;,
        Lcom/twitter/database/generated/aw$b;
    }
.end annotation


# static fields
.field private static final b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final c:[Lcom/twitter/database/model/d;

.field private static final d:[Ljava/lang/String;


# instance fields
.field private final e:Lcom/twitter/database/internal/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/internal/i",
            "<",
            "Laxp$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0xa

    const/4 v2, 0x0

    .line 46
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0, v3}, Ljava/util/LinkedHashSet;-><init>(I)V

    sput-object v0, Lcom/twitter/database/generated/aw;->b:Ljava/util/Collection;

    .line 49
    new-array v0, v2, [Lcom/twitter/database/model/d;

    sput-object v0, Lcom/twitter/database/generated/aw;->c:[Lcom/twitter/database/model/d;

    .line 54
    const/16 v0, 0x20

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "_id"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string/jumbo v2, "user_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "username"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "description"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "description_entities"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "web_url"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "url_entities"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "bg_color"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "link_color"

    aput-object v2, v0, v1

    const-string/jumbo v1, "image_url"

    aput-object v1, v0, v3

    const/16 v1, 0xb

    const-string/jumbo v2, "header_url"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "extended_profile_fields"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "location"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "structured_location"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "user_flags"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "followers"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "fast_followers"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "friends"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "statuses"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "favorites"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "media_count"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "friendship"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "friendship_time"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "profile_created"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "updated"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "pinned_tweet_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "advertiser_type"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, "business_profile_state"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, "customer_service_state"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string/jumbo v2, "hash"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string/jumbo v2, "translator_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/database/generated/aw;->d:[Ljava/lang/String;

    .line 90
    sget-object v0, Lcom/twitter/database/generated/aw;->b:Ljava/util/Collection;

    const-class v1, Laxn;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 91
    sget-object v0, Lcom/twitter/database/generated/aw;->b:Ljava/util/Collection;

    const-class v1, Laxh;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 92
    sget-object v0, Lcom/twitter/database/generated/aw;->b:Ljava/util/Collection;

    const-class v1, Lazf;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 93
    sget-object v0, Lcom/twitter/database/generated/aw;->b:Ljava/util/Collection;

    const-class v1, Lazs;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 94
    sget-object v0, Lcom/twitter/database/generated/aw;->b:Ljava/util/Collection;

    const-class v1, Lawx;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 95
    sget-object v0, Lcom/twitter/database/generated/aw;->b:Ljava/util/Collection;

    const-class v1, Laye;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 96
    sget-object v0, Lcom/twitter/database/generated/aw;->b:Ljava/util/Collection;

    const-class v1, Laze;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 97
    sget-object v0, Lcom/twitter/database/generated/aw;->b:Ljava/util/Collection;

    const-class v1, Laww;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 98
    sget-object v0, Lcom/twitter/database/generated/aw;->b:Ljava/util/Collection;

    const-class v1, Layz;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 99
    sget-object v0, Lcom/twitter/database/generated/aw;->b:Ljava/util/Collection;

    const-class v1, Laxs;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 100
    return-void
.end method

.method public constructor <init>(Lcom/twitter/database/internal/f;)V
    .locals 2
    .annotation build Lcod;
    .end annotation

    .prologue
    .line 107
    invoke-direct {p0, p1}, Lcom/twitter/database/internal/k;-><init>(Lcom/twitter/database/internal/f;)V

    .line 108
    new-instance v0, Lcom/twitter/database/generated/aw$b;

    iget-object v1, p0, Lcom/twitter/database/generated/aw;->f_:Lcom/twitter/database/internal/f;

    invoke-direct {v0, p0, v1}, Lcom/twitter/database/generated/aw$b;-><init>(Lcom/twitter/database/generated/aw;Lcom/twitter/database/internal/f;)V

    iput-object v0, p0, Lcom/twitter/database/generated/aw;->e:Lcom/twitter/database/internal/i;

    .line 109
    return-void
.end method

.method static synthetic g()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/twitter/database/generated/aw;->d:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    const-string/jumbo v0, "users"

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    const-string/jumbo v0, "CREATE TABLE users (\n\t_id INTEGER PRIMARY KEY,\n\tuser_id INTEGER UNIQUE NOT NULL,\n\tusername TEXT,\n\tname TEXT,\n\tdescription TEXT,\n\tdescription_entities BLOB /*NULLABLE*/,\n\tweb_url TEXT,\n\turl_entities BLOB /*NULLABLE*/,\n\tbg_color INTEGER,\n\tlink_color INTEGER,\n\timage_url TEXT,\n\theader_url TEXT,\n\textended_profile_fields BLOB /*NULLABLE*/,\n\tlocation TEXT,\n\tstructured_location BLOB /*NULLABLE*/,\n\tuser_flags INTEGER,\n\tfollowers INTEGER,\n\tfast_followers INTEGER,\n\tfriends INTEGER,\n\tstatuses INTEGER,\n\tfavorites INTEGER,\n\tmedia_count INTEGER,\n\tfriendship INTEGER,\n\tfriendship_time INTEGER,\n\tprofile_created INTEGER,\n\tupdated INTEGER,\n\tpinned_tweet_id INTEGER,\n\tadvertiser_type TEXT,\n\tbusiness_profile_state TEXT,\n\tcustomer_service_state TEXT NOT NULL DEFAULT \'none\',\n\thash INTEGER,\n\ttranslator_type TEXT\n);"

    return-object v0
.end method

.method protected final c()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 159
    sget-object v0, Lcom/twitter/database/generated/aw;->b:Ljava/util/Collection;

    return-object v0
.end method

.method public final d()[Lcom/twitter/database/model/d;
    .locals 1

    .prologue
    .line 165
    sget-object v0, Lcom/twitter/database/generated/aw;->c:[Lcom/twitter/database/model/d;

    return-object v0
.end method

.method public final e()Lcom/twitter/database/internal/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/database/internal/i",
            "<",
            "Laxp$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 171
    iget-object v0, p0, Lcom/twitter/database/generated/aw;->e:Lcom/twitter/database/internal/i;

    return-object v0
.end method

.method public synthetic f()Lcom/twitter/database/model/l;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/twitter/database/generated/aw;->e()Lcom/twitter/database/internal/i;

    move-result-object v0

    return-object v0
.end method
