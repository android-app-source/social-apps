.class public final Lcom/twitter/database/generated/g;
.super Lcom/twitter/database/internal/i;
.source "Twttr"

# interfaces
.implements Lcom/twitter/database/schema/DraftsSchema$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/database/generated/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/database/internal/i",
        "<",
        "Lcom/twitter/database/schema/DraftsSchema$b$a;",
        ">;",
        "Lcom/twitter/database/schema/DraftsSchema$b;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 22
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "sending_state"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/database/generated/g;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/database/internal/f;)V
    .locals 0
    .annotation build Lcod;
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/twitter/database/internal/i;-><init>(Lcom/twitter/database/internal/f;)V

    .line 30
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/twitter/database/model/g;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/twitter/database/model/g",
            "<",
            "Lcom/twitter/database/schema/DraftsSchema$b$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    new-instance v1, Lcom/twitter/database/internal/a;

    new-instance v2, Lcom/twitter/database/generated/g$a;

    move-object v0, p1

    check-cast v0, Landroid/database/Cursor;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/twitter/database/generated/g$a;-><init>(Landroid/database/Cursor;Lcom/twitter/database/generated/g$1;)V

    check-cast p1, Landroid/database/Cursor;

    invoke-direct {v1, v2, p1}, Lcom/twitter/database/internal/a;-><init>(Ljava/lang/Object;Landroid/database/Cursor;)V

    return-object v1
.end method

.method public final a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/twitter/database/generated/g;->a:[Ljava/lang/String;

    return-object v0
.end method

.method protected final b()Lcom/twitter/database/internal/h;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/twitter/database/internal/h;",
            ">()TT;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/twitter/database/generated/g;->b:Lcom/twitter/database/internal/f;

    const-class v1, Lcom/twitter/database/schema/DraftsSchema$a;

    invoke-virtual {v0, v1}, Lcom/twitter/database/internal/f;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/database/internal/h;

    return-object v0
.end method
