.class final Lcom/twitter/database/generated/ay$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laxr$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/database/generated/ay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/database/Cursor;

.field private final b:Laxk$a;

.field private final c:Lawp$a;

.field private final d:Laxp$a;

.field private final e:Laxp$a;

.field private final f:Laxi$a;

.field private final g:Laxh$a;

.field private final h:Laxp$a;

.field private final i:Laxo$a;

.field private final j:Laxn$a;


# direct methods
.method private constructor <init>(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 550
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 551
    iput-object p1, p0, Lcom/twitter/database/generated/ay$a;->a:Landroid/database/Cursor;

    .line 552
    new-instance v0, Lcom/twitter/database/generated/ay$a$1;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ay$a$1;-><init>(Lcom/twitter/database/generated/ay$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ay$a;->b:Laxk$a;

    .line 751
    new-instance v0, Lcom/twitter/database/generated/ay$a$2;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ay$a$2;-><init>(Lcom/twitter/database/generated/ay$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ay$a;->c:Lawp$a;

    .line 777
    new-instance v0, Lcom/twitter/database/generated/ay$a$3;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ay$a$3;-><init>(Lcom/twitter/database/generated/ay$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ay$a;->d:Laxp$a;

    .line 1000
    new-instance v0, Lcom/twitter/database/generated/ay$a$4;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ay$a$4;-><init>(Lcom/twitter/database/generated/ay$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ay$a;->e:Laxp$a;

    .line 1223
    new-instance v0, Lcom/twitter/database/generated/ay$a$5;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ay$a$5;-><init>(Lcom/twitter/database/generated/ay$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ay$a;->f:Laxi$a;

    .line 1340
    new-instance v0, Lcom/twitter/database/generated/ay$a$6;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ay$a$6;-><init>(Lcom/twitter/database/generated/ay$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ay$a;->g:Laxh$a;

    .line 1505
    new-instance v0, Lcom/twitter/database/generated/ay$a$7;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ay$a$7;-><init>(Lcom/twitter/database/generated/ay$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ay$a;->h:Laxp$a;

    .line 1728
    new-instance v0, Lcom/twitter/database/generated/ay$a$8;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ay$a$8;-><init>(Lcom/twitter/database/generated/ay$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ay$a;->i:Laxo$a;

    .line 1812
    new-instance v0, Lcom/twitter/database/generated/ay$a$9;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ay$a$9;-><init>(Lcom/twitter/database/generated/ay$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ay$a;->j:Laxn$a;

    .line 1895
    return-void
.end method

.method synthetic constructor <init>(Landroid/database/Cursor;Lcom/twitter/database/generated/ay$1;)V
    .locals 0

    .prologue
    .line 519
    invoke-direct {p0, p1}, Lcom/twitter/database/generated/ay$a;-><init>(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/database/generated/ay$a;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 519
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a;->a:Landroid/database/Cursor;

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 1903
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a;->a:Landroid/database/Cursor;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2011
    iget-object v0, p0, Lcom/twitter/database/generated/ay$a;->a:Landroid/database/Cursor;

    const/16 v1, 0x10

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
