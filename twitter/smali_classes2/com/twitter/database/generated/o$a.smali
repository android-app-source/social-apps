.class final Lcom/twitter/database/generated/o$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lawj$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/database/generated/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/content/ContentValues;


# direct methods
.method constructor <init>(Landroid/content/ContentValues;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    .line 49
    return-void
.end method


# virtual methods
.method public a(I)Lawj$a;
    .locals 3

    .prologue
    .line 155
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "icon_identifier"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 156
    return-object p0
.end method

.method public a(J)Lawj$a;
    .locals 3

    .prologue
    .line 54
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "generic_activity_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 55
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lawj$a;
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "hash_key"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    return-object p0
.end method

.method public a(Ljava/util/List;)Lawj$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/i;",
            ">;)",
            "Lawj$a;"
        }
    .end annotation

    .prologue
    .line 89
    if-nez p1, :cond_0

    .line 90
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "display_text_bold_index_list"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 94
    :goto_0
    return-object p0

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "display_text_bold_index_list"

    sget-object v2, Lcom/twitter/database/generated/a;->b:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v2}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    goto :goto_0
.end method

.method public b(J)Lawj$a;
    .locals 3

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "created_at"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 62
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lawj$a;
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "generic_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    return-object p0
.end method

.method public b(Ljava/util/List;)Lawj$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/core/i;",
            ">;)",
            "Lawj$a;"
        }
    .end annotation

    .prologue
    .line 111
    if-nez p1, :cond_0

    .line 112
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "social_proof_bold_index_list"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 116
    :goto_0
    return-object p0

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "social_proof_bold_index_list"

    sget-object v2, Lcom/twitter/database/generated/a;->b:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v2}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Lawj$a;
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "display_text"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    return-object p0
.end method

.method public c(Ljava/util/List;)Lawj$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcaf;",
            ">;)",
            "Lawj$a;"
        }
    .end annotation

    .prologue
    .line 133
    if-nez p1, :cond_0

    .line 134
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "dismiss_options"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 138
    :goto_0
    return-object p0

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "dismiss_options"

    sget-object v2, Lcom/twitter/database/generated/a;->c:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v2}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    goto :goto_0
.end method

.method public d(Ljava/lang/String;)Lawj$a;
    .locals 2

    .prologue
    .line 100
    if-nez p1, :cond_0

    .line 101
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "social_proof_text"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 105
    :goto_0
    return-object p0

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "social_proof_text"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public d(Ljava/util/List;)Lawj$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lawj$a;"
        }
    .end annotation

    .prologue
    .line 162
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "facepile_user_ids"

    sget-object v2, Lcom/twitter/database/generated/a;->d:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v2}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 163
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lawj$a;
    .locals 2

    .prologue
    .line 122
    if-nez p1, :cond_0

    .line 123
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "dismiss_type"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 127
    :goto_0
    return-object p0

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "dismiss_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public e(Ljava/util/List;)Lawj$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lawj$a;"
        }
    .end annotation

    .prologue
    .line 169
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "context_tweet_ids"

    sget-object v2, Lcom/twitter/database/generated/a;->d:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v2}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 170
    return-object p0
.end method

.method public f(Ljava/lang/String;)Lawj$a;
    .locals 2

    .prologue
    .line 144
    if-nez p1, :cond_0

    .line 145
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "dismiss_scribe"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 149
    :goto_0
    return-object p0

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "dismiss_scribe"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public f(Ljava/util/List;)Lawj$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lawj$a;"
        }
    .end annotation

    .prologue
    .line 176
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "context_user_ids"

    sget-object v2, Lcom/twitter/database/generated/a;->d:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v2}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 177
    return-object p0
.end method

.method public g(Ljava/lang/String;)Lawj$a;
    .locals 2

    .prologue
    .line 183
    if-nez p1, :cond_0

    .line 184
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "context_alt_text"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 188
    :goto_0
    return-object p0

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "context_alt_text"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public h(Ljava/lang/String;)Lawj$a;
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "tap_through_action"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    return-object p0
.end method

.method public i(Ljava/lang/String;)Lawj$a;
    .locals 2

    .prologue
    .line 201
    if-nez p1, :cond_0

    .line 202
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "scribe_component"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 206
    :goto_0
    return-object p0

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "scribe_component"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public j(Ljava/lang/String;)Lawj$a;
    .locals 2

    .prologue
    .line 212
    if-nez p1, :cond_0

    .line 213
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "sender"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 217
    :goto_0
    return-object p0

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "sender"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public k(Ljava/lang/String;)Lawj$a;
    .locals 2

    .prologue
    .line 223
    if-nez p1, :cond_0

    .line 224
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "impression_id"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 228
    :goto_0
    return-object p0

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/o$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "impression_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
