.class final Lcom/twitter/database/generated/az$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laxs$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/database/generated/az;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/database/Cursor;

.field private final b:Laxe$a;

.field private final c:Laxp$a;


# direct methods
.method private constructor <init>(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227
    iput-object p1, p0, Lcom/twitter/database/generated/az$a;->a:Landroid/database/Cursor;

    .line 228
    new-instance v0, Lcom/twitter/database/generated/az$a$1;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/az$a$1;-><init>(Lcom/twitter/database/generated/az$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/az$a;->b:Laxe$a;

    .line 271
    new-instance v0, Lcom/twitter/database/generated/az$a$2;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/az$a$2;-><init>(Lcom/twitter/database/generated/az$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/az$a;->c:Laxp$a;

    .line 494
    return-void
.end method

.method synthetic constructor <init>(Landroid/database/Cursor;Lcom/twitter/database/generated/az$1;)V
    .locals 0

    .prologue
    .line 216
    invoke-direct {p0, p1}, Lcom/twitter/database/generated/az$a;-><init>(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/database/generated/az$a;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/twitter/database/generated/az$a;->a:Landroid/database/Cursor;

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 502
    iget-object v0, p0, Lcom/twitter/database/generated/az$a;->a:Landroid/database/Cursor;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 512
    iget-object v0, p0, Lcom/twitter/database/generated/az$a;->a:Landroid/database/Cursor;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 529
    iget-object v0, p0, Lcom/twitter/database/generated/az$a;->a:Landroid/database/Cursor;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 547
    iget-object v0, p0, Lcom/twitter/database/generated/az$a;->a:Landroid/database/Cursor;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 556
    iget-object v0, p0, Lcom/twitter/database/generated/az$a;->a:Landroid/database/Cursor;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 616
    iget-object v0, p0, Lcom/twitter/database/generated/az$a;->a:Landroid/database/Cursor;

    const/16 v1, 0xd

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public g()[B
    .locals 2

    .prologue
    .line 635
    iget-object v0, p0, Lcom/twitter/database/generated/az$a;->a:Landroid/database/Cursor;

    const/16 v1, 0xf

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    return-object v0
.end method

.method public h()Laxe$a;
    .locals 1

    .prologue
    .line 653
    iget-object v0, p0, Lcom/twitter/database/generated/az$a;->b:Laxe$a;

    return-object v0
.end method

.method public i()Laxp$a;
    .locals 1

    .prologue
    .line 663
    iget-object v0, p0, Lcom/twitter/database/generated/az$a;->c:Laxp$a;

    return-object v0
.end method
