.class final Lcom/twitter/database/generated/ch$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Layz$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/database/generated/ch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/database/Cursor;

.field private final b:Laxk$a;

.field private final c:Lawp$a;

.field private final d:Laxp$a;

.field private final e:Laxp$a;

.field private final f:Laxi$a;

.field private final g:Laxh$a;

.field private final h:Laxl$a;

.field private final i:Laxm$a;

.field private final j:Laxo$a;

.field private final k:Laxp$a;


# direct methods
.method private constructor <init>(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 573
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 574
    iput-object p1, p0, Lcom/twitter/database/generated/ch$a;->a:Landroid/database/Cursor;

    .line 575
    new-instance v0, Lcom/twitter/database/generated/ch$a$1;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ch$a$1;-><init>(Lcom/twitter/database/generated/ch$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ch$a;->b:Laxk$a;

    .line 774
    new-instance v0, Lcom/twitter/database/generated/ch$a$3;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ch$a$3;-><init>(Lcom/twitter/database/generated/ch$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ch$a;->c:Lawp$a;

    .line 800
    new-instance v0, Lcom/twitter/database/generated/ch$a$4;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ch$a$4;-><init>(Lcom/twitter/database/generated/ch$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ch$a;->d:Laxp$a;

    .line 1023
    new-instance v0, Lcom/twitter/database/generated/ch$a$5;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ch$a$5;-><init>(Lcom/twitter/database/generated/ch$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ch$a;->e:Laxp$a;

    .line 1246
    new-instance v0, Lcom/twitter/database/generated/ch$a$6;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ch$a$6;-><init>(Lcom/twitter/database/generated/ch$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ch$a;->f:Laxi$a;

    .line 1363
    new-instance v0, Lcom/twitter/database/generated/ch$a$7;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ch$a$7;-><init>(Lcom/twitter/database/generated/ch$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ch$a;->g:Laxh$a;

    .line 1528
    new-instance v0, Lcom/twitter/database/generated/ch$a$8;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ch$a$8;-><init>(Lcom/twitter/database/generated/ch$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ch$a;->h:Laxl$a;

    .line 1679
    new-instance v0, Lcom/twitter/database/generated/ch$a$9;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ch$a$9;-><init>(Lcom/twitter/database/generated/ch$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ch$a;->i:Laxm$a;

    .line 1742
    new-instance v0, Lcom/twitter/database/generated/ch$a$10;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ch$a$10;-><init>(Lcom/twitter/database/generated/ch$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ch$a;->j:Laxo$a;

    .line 1826
    new-instance v0, Lcom/twitter/database/generated/ch$a$2;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/ch$a$2;-><init>(Lcom/twitter/database/generated/ch$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ch$a;->k:Laxp$a;

    .line 2049
    return-void
.end method

.method synthetic constructor <init>(Landroid/database/Cursor;Lcom/twitter/database/generated/ch$1;)V
    .locals 0

    .prologue
    .line 539
    invoke-direct {p0, p1}, Lcom/twitter/database/generated/ch$a;-><init>(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 539
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a;->a:Landroid/database/Cursor;

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 2057
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a;->a:Landroid/database/Cursor;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method
