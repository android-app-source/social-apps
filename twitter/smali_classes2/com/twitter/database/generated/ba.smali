.class public final Lcom/twitter/database/generated/ba;
.super Lcom/twitter/database/internal/k;
.source "Twttr"

# interfaces
.implements Laxt;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/database/generated/ba$a;,
        Lcom/twitter/database/generated/ba$b;
    }
.end annotation


# static fields
.field private static final b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final c:[Lcom/twitter/database/model/d;

.field private static final d:[Ljava/lang/String;


# instance fields
.field private final e:Lcom/twitter/database/internal/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/internal/i",
            "<",
            "Laxt$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 35
    invoke-static {}, Lcom/twitter/util/collection/o;->f()Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/twitter/database/generated/ba;->b:Ljava/util/Collection;

    .line 38
    new-array v0, v5, [Lcom/twitter/database/model/d;

    new-instance v1, Lcom/twitter/database/model/d;

    const-string/jumbo v2, "live_video_events_event_id_index"

    const-string/jumbo v3, "CREATE INDEX live_video_events_event_id_index ON live_video_events (\n\tevent_id\n);"

    invoke-direct {v1, v2, v3}, Lcom/twitter/database/model/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v0, v4

    sput-object v0, Lcom/twitter/database/generated/ba;->c:[Lcom/twitter/database/model/d;

    .line 45
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "_id"

    aput-object v1, v0, v4

    const-string/jumbo v1, "event_id"

    aput-object v1, v0, v5

    const/4 v1, 0x2

    const-string/jumbo v2, "owner_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "start_time"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "end_time"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "host_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "hashtag"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "broadcast"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "semantic_core_id"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "updated_at"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "placeholder_variants"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "timelines"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "subscriptions"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "docking_enabled"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/database/generated/ba;->d:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/database/internal/f;)V
    .locals 2
    .annotation build Lcod;
    .end annotation

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/twitter/database/internal/k;-><init>(Lcom/twitter/database/internal/f;)V

    .line 69
    new-instance v0, Lcom/twitter/database/generated/ba$b;

    iget-object v1, p0, Lcom/twitter/database/generated/ba;->f_:Lcom/twitter/database/internal/f;

    invoke-direct {v0, p0, v1}, Lcom/twitter/database/generated/ba$b;-><init>(Lcom/twitter/database/generated/ba;Lcom/twitter/database/internal/f;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ba;->e:Lcom/twitter/database/internal/i;

    .line 70
    return-void
.end method

.method static synthetic g()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/twitter/database/generated/ba;->d:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    const-string/jumbo v0, "live_video_events"

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    const-string/jumbo v0, "CREATE TABLE live_video_events (\n\t_id INTEGER PRIMARY KEY,\n\tevent_id INTEGER UNIQUE ON CONFLICT REPLACE,\n\towner_id INTEGER NOT NULL,\n\tstart_time INTEGER NOT NULL,\n\tend_time INTEGER NOT NULL,\n\ttitle TEXT NOT NULL,\n\thost_name TEXT,\n\thashtag TEXT NOT NULL,\n\tbroadcast BLOB NOT NULL,\n\tsemantic_core_id TEXT,\n\tupdated_at INTEGER NOT NULL,\n\tplaceholder_variants BLOB NOT NULL,\n\ttimelines BLOB NOT NULL,\n\tsubscriptions BLOB /*NULLABLE*/,\n\tdocking_enabled INTEGER NOT NULL\n);"

    return-object v0
.end method

.method protected final c()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 103
    sget-object v0, Lcom/twitter/database/generated/ba;->b:Ljava/util/Collection;

    return-object v0
.end method

.method public final d()[Lcom/twitter/database/model/d;
    .locals 1

    .prologue
    .line 109
    sget-object v0, Lcom/twitter/database/generated/ba;->c:[Lcom/twitter/database/model/d;

    return-object v0
.end method

.method public final e()Lcom/twitter/database/internal/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/database/internal/i",
            "<",
            "Laxt$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lcom/twitter/database/generated/ba;->e:Lcom/twitter/database/internal/i;

    return-object v0
.end method

.method public synthetic f()Lcom/twitter/database/model/l;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/twitter/database/generated/ba;->e()Lcom/twitter/database/internal/i;

    move-result-object v0

    return-object v0
.end method
