.class Lcom/twitter/database/generated/ch$a$8;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laxl$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/database/generated/ch$a;-><init>(Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/database/generated/ch$a;


# direct methods
.method constructor <init>(Lcom/twitter/database/generated/ch$a;)V
    .locals 0

    .prologue
    .line 1528
    iput-object p1, p0, Lcom/twitter/database/generated/ch$a$8;->a:Lcom/twitter/database/generated/ch$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 1535
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$8;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x89

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1545
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$8;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x8a

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1562
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$8;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x8c

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1580
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$8;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x8e

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1589
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$8;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x8f

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 1649
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$8;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x96

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public g()[B
    .locals 2

    .prologue
    .line 1668
    iget-object v0, p0, Lcom/twitter/database/generated/ch$a$8;->a:Lcom/twitter/database/generated/ch$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ch$a;->a(Lcom/twitter/database/generated/ch$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x98

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    return-object v0
.end method
