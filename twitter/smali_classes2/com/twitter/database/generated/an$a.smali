.class final Lcom/twitter/database/generated/an$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laxg$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/database/generated/an;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/database/Cursor;

.field private final b:Laxk$a;

.field private final c:Lawp$a;

.field private final d:Laxp$a;

.field private final e:Laxp$a;

.field private final f:Laxi$a;

.field private final g:Laxj$a;


# direct methods
.method private constructor <init>(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 404
    iput-object p1, p0, Lcom/twitter/database/generated/an$a;->a:Landroid/database/Cursor;

    .line 405
    new-instance v0, Lcom/twitter/database/generated/an$a$1;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/an$a$1;-><init>(Lcom/twitter/database/generated/an$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/an$a;->b:Laxk$a;

    .line 604
    new-instance v0, Lcom/twitter/database/generated/an$a$2;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/an$a$2;-><init>(Lcom/twitter/database/generated/an$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/an$a;->c:Lawp$a;

    .line 630
    new-instance v0, Lcom/twitter/database/generated/an$a$3;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/an$a$3;-><init>(Lcom/twitter/database/generated/an$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/an$a;->d:Laxp$a;

    .line 853
    new-instance v0, Lcom/twitter/database/generated/an$a$4;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/an$a$4;-><init>(Lcom/twitter/database/generated/an$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/an$a;->e:Laxp$a;

    .line 1076
    new-instance v0, Lcom/twitter/database/generated/an$a$5;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/an$a$5;-><init>(Lcom/twitter/database/generated/an$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/an$a;->f:Laxi$a;

    .line 1193
    new-instance v0, Lcom/twitter/database/generated/an$a$6;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/an$a$6;-><init>(Lcom/twitter/database/generated/an$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/an$a;->g:Laxj$a;

    .line 1212
    return-void
.end method

.method synthetic constructor <init>(Landroid/database/Cursor;Lcom/twitter/database/generated/an$1;)V
    .locals 0

    .prologue
    .line 381
    invoke-direct {p0, p1}, Lcom/twitter/database/generated/an$a;-><init>(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/database/generated/an$a;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/twitter/database/generated/an$a;->a:Landroid/database/Cursor;

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 1220
    iget-object v0, p0, Lcom/twitter/database/generated/an$a;->a:Landroid/database/Cursor;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method
