.class public final Lcom/twitter/database/generated/ar;
.super Lcom/twitter/database/internal/k;
.source "Twttr"

# interfaces
.implements Laxk;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/database/generated/ar$a;,
        Lcom/twitter/database/generated/ar$b;
    }
.end annotation


# static fields
.field private static final b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final c:[Lcom/twitter/database/model/d;

.field private static final d:[Ljava/lang/String;


# instance fields
.field private final e:Lcom/twitter/database/internal/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/internal/i",
            "<",
            "Laxk$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 37
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0, v3}, Ljava/util/LinkedHashSet;-><init>(I)V

    sput-object v0, Lcom/twitter/database/generated/ar;->b:Ljava/util/Collection;

    .line 40
    new-array v0, v2, [Lcom/twitter/database/model/d;

    sput-object v0, Lcom/twitter/database/generated/ar;->c:[Lcom/twitter/database/model/d;

    .line 45
    const/16 v0, 0x1e

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "_id"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string/jumbo v2, "status_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "author_id"

    aput-object v2, v0, v1

    const-string/jumbo v1, "content"

    aput-object v1, v0, v3

    const/4 v1, 0x4

    const-string/jumbo v2, "r_content"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "source"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "created"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "in_r_user_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "in_r_status_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "in_r_screen_name"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "favorited"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "retweeted"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "favorite_count"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "retweet_count"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "view_count"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "flags"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "latitude"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "longitude"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "place_data"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "entities"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "card"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "lang"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "supplemental_language"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "quoted_tweet_data"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "quoted_tweet_id"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "withheld_scope"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "reply_count"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "conversation_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, "collection_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, "display_text_start"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/database/generated/ar;->d:[Ljava/lang/String;

    .line 79
    sget-object v0, Lcom/twitter/database/generated/ar;->b:Ljava/util/Collection;

    const-class v1, Laxh;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 80
    sget-object v0, Lcom/twitter/database/generated/ar;->b:Ljava/util/Collection;

    const-class v1, Lazs;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 81
    sget-object v0, Lcom/twitter/database/generated/ar;->b:Ljava/util/Collection;

    const-class v1, Laye;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 82
    return-void
.end method

.method public constructor <init>(Lcom/twitter/database/internal/f;)V
    .locals 2
    .annotation build Lcod;
    .end annotation

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/twitter/database/internal/k;-><init>(Lcom/twitter/database/internal/f;)V

    .line 90
    new-instance v0, Lcom/twitter/database/generated/ar$b;

    iget-object v1, p0, Lcom/twitter/database/generated/ar;->f_:Lcom/twitter/database/internal/f;

    invoke-direct {v0, p0, v1}, Lcom/twitter/database/generated/ar$b;-><init>(Lcom/twitter/database/generated/ar;Lcom/twitter/database/internal/f;)V

    iput-object v0, p0, Lcom/twitter/database/generated/ar;->e:Lcom/twitter/database/internal/i;

    .line 91
    return-void
.end method

.method static synthetic g()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/twitter/database/generated/ar;->d:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    const-string/jumbo v0, "statuses"

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    const-string/jumbo v0, "CREATE TABLE statuses (\n\t_id INTEGER PRIMARY KEY,\n\tstatus_id INTEGER UNIQUE NOT NULL,\n\tauthor_id INTEGER,\n\tcontent TEXT,\n\tr_content TEXT,\n\tsource TEXT,\n\tcreated INTEGER,\n\tin_r_user_id INTEGER,\n\tin_r_status_id INTEGER,\n\tin_r_screen_name TEXT,\n\tfavorited INTEGER,\n\tretweeted INTEGER,\n\tfavorite_count INTEGER,\n\tretweet_count INTEGER,\n\tview_count INTEGER,\n\tflags INTEGER,\n\tlatitude TEXT,\n\tlongitude TEXT,\n\tplace_data BLOB /*NULLABLE*/,\n\tentities TEXT,\n\tcard BLOB /*NULLABLE*/,\n\tlang TEXT,\n\tsupplemental_language TEXT,\n\tquoted_tweet_data BLOB,\n\tquoted_tweet_id INTEGER,\n\twithheld_scope INTEGER,\n\treply_count INTEGER,\n\tconversation_id INTEGER,\n\tcollection_id INTEGER,\n\tdisplay_text_start INTEGER\n);"

    return-object v0
.end method

.method protected final c()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 139
    sget-object v0, Lcom/twitter/database/generated/ar;->b:Ljava/util/Collection;

    return-object v0
.end method

.method public final d()[Lcom/twitter/database/model/d;
    .locals 1

    .prologue
    .line 145
    sget-object v0, Lcom/twitter/database/generated/ar;->c:[Lcom/twitter/database/model/d;

    return-object v0
.end method

.method public final e()Lcom/twitter/database/internal/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/database/internal/i",
            "<",
            "Laxk$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 151
    iget-object v0, p0, Lcom/twitter/database/generated/ar;->e:Lcom/twitter/database/internal/i;

    return-object v0
.end method

.method public synthetic f()Lcom/twitter/database/model/l;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/twitter/database/generated/ar;->e()Lcom/twitter/database/internal/i;

    move-result-object v0

    return-object v0
.end method
