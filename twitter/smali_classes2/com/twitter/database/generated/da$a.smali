.class final Lcom/twitter/database/generated/da$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lazs$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/database/generated/da;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/database/Cursor;

.field private final b:Laxk$a;

.field private final c:Laxf$a;

.field private final d:Lawp$a;

.field private final e:Laxp$a;

.field private final f:Laxp$a;

.field private final g:Laxp$a;

.field private final h:Laxl$a;

.field private final i:Lazq$a;

.field private final j:Laxi$a;


# direct methods
.method private constructor <init>(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 628
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 629
    iput-object p1, p0, Lcom/twitter/database/generated/da$a;->a:Landroid/database/Cursor;

    .line 630
    new-instance v0, Lcom/twitter/database/generated/da$a$1;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/da$a$1;-><init>(Lcom/twitter/database/generated/da$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/da$a;->b:Laxk$a;

    .line 829
    new-instance v0, Lcom/twitter/database/generated/da$a$2;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/da$a$2;-><init>(Lcom/twitter/database/generated/da$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/da$a;->c:Laxf$a;

    .line 943
    new-instance v0, Lcom/twitter/database/generated/da$a$3;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/da$a$3;-><init>(Lcom/twitter/database/generated/da$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/da$a;->d:Lawp$a;

    .line 969
    new-instance v0, Lcom/twitter/database/generated/da$a$4;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/da$a$4;-><init>(Lcom/twitter/database/generated/da$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/da$a;->e:Laxp$a;

    .line 1192
    new-instance v0, Lcom/twitter/database/generated/da$a$5;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/da$a$5;-><init>(Lcom/twitter/database/generated/da$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/da$a;->f:Laxp$a;

    .line 1415
    new-instance v0, Lcom/twitter/database/generated/da$a$6;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/da$a$6;-><init>(Lcom/twitter/database/generated/da$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/da$a;->g:Laxp$a;

    .line 1638
    new-instance v0, Lcom/twitter/database/generated/da$a$7;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/da$a$7;-><init>(Lcom/twitter/database/generated/da$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/da$a;->h:Laxl$a;

    .line 1789
    new-instance v0, Lcom/twitter/database/generated/da$a$8;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/da$a$8;-><init>(Lcom/twitter/database/generated/da$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/da$a;->i:Lazq$a;

    .line 1975
    new-instance v0, Lcom/twitter/database/generated/da$a$9;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/da$a$9;-><init>(Lcom/twitter/database/generated/da$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/da$a;->j:Laxi$a;

    .line 2092
    return-void
.end method

.method synthetic constructor <init>(Landroid/database/Cursor;Lcom/twitter/database/generated/da$1;)V
    .locals 0

    .prologue
    .line 597
    invoke-direct {p0, p1}, Lcom/twitter/database/generated/da$a;-><init>(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/database/generated/da$a;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lcom/twitter/database/generated/da$a;->a:Landroid/database/Cursor;

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 2100
    iget-object v0, p0, Lcom/twitter/database/generated/da$a;->a:Landroid/database/Cursor;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()I
    .locals 2

    .prologue
    .line 2154
    iget-object v0, p0, Lcom/twitter/database/generated/da$a;->a:Landroid/database/Cursor;

    const/16 v1, 0x9

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method
