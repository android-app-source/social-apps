.class Lcom/twitter/database/generated/ca$a$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laxp$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/database/generated/ca$a;-><init>(Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/database/generated/ca$a;


# direct methods
.method constructor <init>(Lcom/twitter/database/generated/ca$a;)V
    .locals 0

    .prologue
    .line 631
    iput-object p1, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public A()J
    .locals 2

    .prologue
    .line 818
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x50

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public B()Ljava/lang/String;
    .locals 2

    .prologue
    .line 824
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x51

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public C()Ljava/lang/String;
    .locals 2

    .prologue
    .line 830
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x52

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public D()Ljava/lang/String;
    .locals 2

    .prologue
    .line 851
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x55

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()J
    .locals 2

    .prologue
    .line 638
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x36

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 647
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x37

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 653
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x38

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 659
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x39

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 665
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x3a

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/twitter/model/core/v;
    .locals 2

    .prologue
    const/16 v1, 0x3b

    .line 671
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 672
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 678
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x3c

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/twitter/model/core/v;
    .locals 2

    .prologue
    const/16 v1, 0x3d

    .line 684
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 685
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    goto :goto_0
.end method

.method public i()I
    .locals 2

    .prologue
    .line 690
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x3e

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public j()I
    .locals 2

    .prologue
    .line 695
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x3f

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 701
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x40

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 710
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x41

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()Lcom/twitter/model/profile/ExtendedProfile;
    .locals 2

    .prologue
    const/16 v1, 0x42

    .line 719
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 720
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/profile/ExtendedProfile;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/profile/ExtendedProfile;

    goto :goto_0
.end method

.method public n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 726
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x43

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public o()Lcom/twitter/util/collection/k;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/geo/TwitterPlace;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v1, 0x44

    .line 732
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 733
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/database/generated/a;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/k;

    goto :goto_0
.end method

.method public p()I
    .locals 2

    .prologue
    .line 742
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x45

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public q()J
    .locals 2

    .prologue
    .line 750
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x46

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public r()J
    .locals 2

    .prologue
    .line 758
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x47

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public s()J
    .locals 2

    .prologue
    .line 766
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x48

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public t()J
    .locals 2

    .prologue
    .line 774
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x49

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public u()J
    .locals 2

    .prologue
    .line 782
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x4a

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public v()J
    .locals 2

    .prologue
    .line 787
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x4b

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public w()I
    .locals 2

    .prologue
    .line 792
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x4c

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public x()J
    .locals 2

    .prologue
    .line 797
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x4d

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public y()J
    .locals 2

    .prologue
    .line 805
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x4e

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public z()J
    .locals 2

    .prologue
    .line 813
    iget-object v0, p0, Lcom/twitter/database/generated/ca$a$3;->a:Lcom/twitter/database/generated/ca$a;

    invoke-static {v0}, Lcom/twitter/database/generated/ca$a;->a(Lcom/twitter/database/generated/ca$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x4f

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method
