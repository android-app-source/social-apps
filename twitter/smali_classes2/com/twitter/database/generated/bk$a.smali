.class final Lcom/twitter/database/generated/bk$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laye$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/database/generated/bk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/database/Cursor;

.field private final b:Laxy$a;

.field private final c:Laxk$a;

.field private final d:Lawp$a;

.field private final e:Laxp$a;

.field private final f:Laxp$a;

.field private final g:Laxi$a;

.field private final h:Laxh$a;

.field private final i:Laxx$a;


# direct methods
.method private constructor <init>(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 482
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 483
    iput-object p1, p0, Lcom/twitter/database/generated/bk$a;->a:Landroid/database/Cursor;

    .line 484
    new-instance v0, Lcom/twitter/database/generated/bk$a$1;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/bk$a$1;-><init>(Lcom/twitter/database/generated/bk$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/bk$a;->b:Laxy$a;

    .line 606
    new-instance v0, Lcom/twitter/database/generated/bk$a$2;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/bk$a$2;-><init>(Lcom/twitter/database/generated/bk$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/bk$a;->c:Laxk$a;

    .line 805
    new-instance v0, Lcom/twitter/database/generated/bk$a$3;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/bk$a$3;-><init>(Lcom/twitter/database/generated/bk$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/bk$a;->d:Lawp$a;

    .line 831
    new-instance v0, Lcom/twitter/database/generated/bk$a$4;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/bk$a$4;-><init>(Lcom/twitter/database/generated/bk$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/bk$a;->e:Laxp$a;

    .line 1054
    new-instance v0, Lcom/twitter/database/generated/bk$a$5;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/bk$a$5;-><init>(Lcom/twitter/database/generated/bk$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/bk$a;->f:Laxp$a;

    .line 1277
    new-instance v0, Lcom/twitter/database/generated/bk$a$6;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/bk$a$6;-><init>(Lcom/twitter/database/generated/bk$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/bk$a;->g:Laxi$a;

    .line 1394
    new-instance v0, Lcom/twitter/database/generated/bk$a$7;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/bk$a$7;-><init>(Lcom/twitter/database/generated/bk$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/bk$a;->h:Laxh$a;

    .line 1559
    new-instance v0, Lcom/twitter/database/generated/bk$a$8;

    invoke-direct {v0, p0}, Lcom/twitter/database/generated/bk$a$8;-><init>(Lcom/twitter/database/generated/bk$a;)V

    iput-object v0, p0, Lcom/twitter/database/generated/bk$a;->i:Laxx$a;

    .line 1584
    return-void
.end method

.method synthetic constructor <init>(Landroid/database/Cursor;Lcom/twitter/database/generated/bk$1;)V
    .locals 0

    .prologue
    .line 454
    invoke-direct {p0, p1}, Lcom/twitter/database/generated/bk$a;-><init>(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/database/generated/bk$a;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lcom/twitter/database/generated/bk$a;->a:Landroid/database/Cursor;

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 1592
    iget-object v0, p0, Lcom/twitter/database/generated/bk$a;->a:Landroid/database/Cursor;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1603
    iget-object v0, p0, Lcom/twitter/database/generated/bk$a;->a:Landroid/database/Cursor;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/Long;
    .locals 2

    .prologue
    const/4 v1, 0x5

    .line 1622
    iget-object v0, p0, Lcom/twitter/database/generated/bk$a;->a:Landroid/database/Cursor;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1623
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/bk$a;->a:Landroid/database/Cursor;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 1628
    iget-object v0, p0, Lcom/twitter/database/generated/bk$a;->a:Landroid/database/Cursor;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method
