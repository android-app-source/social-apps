.class final Lcom/twitter/database/generated/bb$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laxu$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/database/generated/bb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/content/ContentValues;


# direct methods
.method constructor <init>(Landroid/content/ContentValues;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/twitter/database/generated/bb$a;->a:Landroid/content/ContentValues;

    .line 49
    return-void
.end method


# virtual methods
.method public a(J)Laxu$a;
    .locals 3

    .prologue
    .line 54
    iget-object v0, p0, Lcom/twitter/database/generated/bb$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "event_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 55
    return-object p0
.end method

.method public a(Lcom/twitter/model/livevideo/a;)Laxu$a;
    .locals 3

    .prologue
    .line 107
    iget-object v0, p0, Lcom/twitter/database/generated/bb$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "broadcast"

    sget-object v2, Lcom/twitter/model/livevideo/a;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v2}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 108
    return-object p0
.end method

.method public a(Lcom/twitter/model/livevideo/d;)Laxu$a;
    .locals 3

    .prologue
    .line 146
    if-nez p1, :cond_0

    .line 147
    iget-object v0, p0, Lcom/twitter/database/generated/bb$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "subscriptions"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 151
    :goto_0
    return-object p0

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/bb$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "subscriptions"

    sget-object v2, Lcom/twitter/model/livevideo/d;->a:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v2}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Laxu$a;
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/twitter/database/generated/bb$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    return-object p0
.end method

.method public a(Ljava/util/List;)Laxu$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/card/property/ImageSpec;",
            ">;)",
            "Laxu$a;"
        }
    .end annotation

    .prologue
    .line 132
    iget-object v0, p0, Lcom/twitter/database/generated/bb$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "placeholder_variants"

    sget-object v2, Lcom/twitter/database/generated/a;->g:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v2}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 133
    return-object p0
.end method

.method public a(Z)Laxu$a;
    .locals 3

    .prologue
    .line 157
    iget-object v0, p0, Lcom/twitter/database/generated/bb$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "docking_enabled"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 158
    return-object p0
.end method

.method public b(J)Laxu$a;
    .locals 3

    .prologue
    .line 61
    iget-object v0, p0, Lcom/twitter/database/generated/bb$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "owner_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 62
    return-object p0
.end method

.method public b(Ljava/lang/String;)Laxu$a;
    .locals 2

    .prologue
    .line 89
    if-nez p1, :cond_0

    .line 90
    iget-object v0, p0, Lcom/twitter/database/generated/bb$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "host_name"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 94
    :goto_0
    return-object p0

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/bb$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "host_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Ljava/util/List;)Laxu$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/livevideo/e;",
            ">;)",
            "Laxu$a;"
        }
    .end annotation

    .prologue
    .line 139
    iget-object v0, p0, Lcom/twitter/database/generated/bb$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "timelines"

    sget-object v2, Lcom/twitter/database/generated/a;->h:Lcom/twitter/util/serialization/l;

    invoke-static {p1, v2}, Lcom/twitter/util/serialization/k;->a(Ljava/lang/Object;Lcom/twitter/util/serialization/l;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 140
    return-object p0
.end method

.method public c(J)Laxu$a;
    .locals 3

    .prologue
    .line 68
    iget-object v0, p0, Lcom/twitter/database/generated/bb$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "start_time"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 69
    return-object p0
.end method

.method public c(Ljava/lang/String;)Laxu$a;
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/twitter/database/generated/bb$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "hashtag"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    return-object p0
.end method

.method public d(J)Laxu$a;
    .locals 3

    .prologue
    .line 75
    iget-object v0, p0, Lcom/twitter/database/generated/bb$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "end_time"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 76
    return-object p0
.end method

.method public d(Ljava/lang/String;)Laxu$a;
    .locals 2

    .prologue
    .line 121
    if-nez p1, :cond_0

    .line 122
    iget-object v0, p0, Lcom/twitter/database/generated/bb$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "semantic_core_id"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 126
    :goto_0
    return-object p0

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/bb$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "semantic_core_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public e(J)Laxu$a;
    .locals 3

    .prologue
    .line 114
    iget-object v0, p0, Lcom/twitter/database/generated/bb$a;->a:Landroid/content/ContentValues;

    const-string/jumbo v1, "updated_at"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 115
    return-object p0
.end method
