.class Lcom/twitter/database/generated/cm$a$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Laxp$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/twitter/database/generated/cm$a;-><init>(Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/database/generated/cm$a;


# direct methods
.method constructor <init>(Lcom/twitter/database/generated/cm$a;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public A()J
    .locals 2

    .prologue
    .line 400
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x1f

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public B()Ljava/lang/String;
    .locals 2

    .prologue
    .line 406
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public C()Ljava/lang/String;
    .locals 2

    .prologue
    .line 412
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public D()Ljava/lang/String;
    .locals 2

    .prologue
    .line 433
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()J
    .locals 2

    .prologue
    .line 220
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 229
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 241
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x9

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/twitter/model/core/v;
    .locals 2

    .prologue
    const/16 v1, 0xa

    .line 253
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 254
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 260
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xb

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/twitter/model/core/v;
    .locals 2

    .prologue
    const/16 v1, 0xc

    .line 266
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 267
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/core/v;->b:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/v;

    goto :goto_0
.end method

.method public i()I
    .locals 2

    .prologue
    .line 272
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xd

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public j()I
    .locals 2

    .prologue
    .line 277
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xe

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 283
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0xf

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 292
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x10

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()Lcom/twitter/model/profile/ExtendedProfile;
    .locals 2

    .prologue
    const/16 v1, 0x11

    .line 301
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 302
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/model/profile/ExtendedProfile;->a:Lcom/twitter/util/serialization/b;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/profile/ExtendedProfile;

    goto :goto_0
.end method

.method public n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 308
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x12

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public o()Lcom/twitter/util/collection/k;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/geo/TwitterPlace;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v1, 0x13

    .line 314
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 315
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    sget-object v1, Lcom/twitter/database/generated/a;->a:Lcom/twitter/util/serialization/l;

    invoke-static {v0, v1}, Lcom/twitter/util/serialization/k;->a([BLcom/twitter/util/serialization/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/util/collection/k;

    goto :goto_0
.end method

.method public p()I
    .locals 2

    .prologue
    .line 324
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x14

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public q()J
    .locals 2

    .prologue
    .line 332
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x15

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public r()J
    .locals 2

    .prologue
    .line 340
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public s()J
    .locals 2

    .prologue
    .line 348
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public t()J
    .locals 2

    .prologue
    .line 356
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public u()J
    .locals 2

    .prologue
    .line 364
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public v()J
    .locals 2

    .prologue
    .line 369
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x1a

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public w()I
    .locals 2

    .prologue
    .line 374
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x1b

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public x()J
    .locals 2

    .prologue
    .line 379
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x1c

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public y()J
    .locals 2

    .prologue
    .line 387
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x1d

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public z()J
    .locals 2

    .prologue
    .line 395
    iget-object v0, p0, Lcom/twitter/database/generated/cm$a$1;->a:Lcom/twitter/database/generated/cm$a;

    invoke-static {v0}, Lcom/twitter/database/generated/cm$a;->a(Lcom/twitter/database/generated/cm$a;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x1e

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method
