.class public final Lcom/twitter/database/generated/d;
.super Lcom/twitter/database/internal/k;
.source "Twttr"

# interfaces
.implements Lcom/twitter/database/lru/schema/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/database/generated/d$a;,
        Lcom/twitter/database/generated/d$b;
    }
.end annotation


# static fields
.field private static final b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final c:[Lcom/twitter/database/model/d;

.field private static final d:[Ljava/lang/String;


# instance fields
.field private final e:Lcom/twitter/database/internal/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/database/internal/i",
            "<",
            "Lcom/twitter/database/lru/schema/b$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 28
    invoke-static {}, Lcom/twitter/util/collection/o;->f()Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/twitter/database/generated/d;->b:Ljava/util/Collection;

    .line 31
    new-array v0, v5, [Lcom/twitter/database/model/d;

    new-instance v1, Lcom/twitter/database/model/d;

    const-string/jumbo v2, "lru_key_value_table_category_version_key_index"

    const-string/jumbo v3, "CREATE INDEX lru_key_value_table_category_version_key_index ON lru_key_value_table (\n\tcategory_id,\n\tversion,\n\tkey\n);"

    invoke-direct {v1, v2, v3}, Lcom/twitter/database/model/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v0, v4

    sput-object v0, Lcom/twitter/database/generated/d;->c:[Lcom/twitter/database/model/d;

    .line 40
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "_id"

    aput-object v1, v0, v4

    const-string/jumbo v1, "category_id"

    aput-object v1, v0, v5

    const/4 v1, 0x2

    const-string/jumbo v2, "version"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "expiry_timestamp"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "last_update_timestamp"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "key"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "value"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "data_size_bytes"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/database/generated/d;->d:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/database/internal/f;)V
    .locals 2
    .annotation build Lcod;
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/twitter/database/internal/k;-><init>(Lcom/twitter/database/internal/f;)V

    .line 57
    new-instance v0, Lcom/twitter/database/generated/d$b;

    iget-object v1, p0, Lcom/twitter/database/generated/d;->f_:Lcom/twitter/database/internal/f;

    invoke-direct {v0, p0, v1}, Lcom/twitter/database/generated/d$b;-><init>(Lcom/twitter/database/generated/d;Lcom/twitter/database/internal/f;)V

    iput-object v0, p0, Lcom/twitter/database/generated/d;->e:Lcom/twitter/database/internal/i;

    .line 58
    return-void
.end method

.method static synthetic g()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/twitter/database/generated/d;->d:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    const-string/jumbo v0, "lru_key_value_table"

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    const-string/jumbo v0, "CREATE TABLE lru_key_value_table (\n\t_id INTEGER PRIMARY KEY,\n\tcategory_id INTEGER,\n\tversion INTEGER,\n\texpiry_timestamp INTEGER,\n\tlast_update_timestamp INTEGER,\n\tkey TEXT NOT NULL,\n\tvalue BLOB,\n\tdata_size_bytes INTEGER\n);"

    return-object v0
.end method

.method protected final c()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/twitter/database/model/k;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 84
    sget-object v0, Lcom/twitter/database/generated/d;->b:Ljava/util/Collection;

    return-object v0
.end method

.method public final d()[Lcom/twitter/database/model/d;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/twitter/database/generated/d;->c:[Lcom/twitter/database/model/d;

    return-object v0
.end method

.method public final e()Lcom/twitter/database/internal/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/database/internal/i",
            "<",
            "Lcom/twitter/database/lru/schema/b$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lcom/twitter/database/generated/d;->e:Lcom/twitter/database/internal/i;

    return-object v0
.end method

.method public synthetic f()Lcom/twitter/database/model/l;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/twitter/database/generated/d;->e()Lcom/twitter/database/internal/i;

    move-result-object v0

    return-object v0
.end method
