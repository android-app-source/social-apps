.class public abstract Lcom/twitter/database/internal/i;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/database/model/l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/twitter/database/model/l",
        "<TP;>;"
    }
.end annotation


# instance fields
.field protected final b:Lcom/twitter/database/internal/f;


# direct methods
.method protected constructor <init>(Lcom/twitter/database/internal/f;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/twitter/database/internal/i;->b:Lcom/twitter/database/internal/f;

    .line 22
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/database/model/f;)Lcom/twitter/database/model/g;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/database/model/f;",
            ")",
            "Lcom/twitter/database/model/g",
            "<TP;>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 35
    invoke-static {}, Lcom/twitter/util/g;->c()V

    .line 36
    if-eqz p1, :cond_0

    .line 37
    iget-object v0, p0, Lcom/twitter/database/internal/i;->b:Lcom/twitter/database/internal/f;

    invoke-virtual {v0}, Lcom/twitter/database/internal/f;->e()Lcom/twitter/database/model/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/database/internal/i;->b()Lcom/twitter/database/internal/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/database/internal/h;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/database/internal/i;->a()[Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/twitter/database/model/f;->a:Ljava/lang/String;

    iget-object v4, p1, Lcom/twitter/database/model/f;->b:[Ljava/lang/String;

    iget-object v5, p1, Lcom/twitter/database/model/f;->d:Ljava/lang/String;

    iget-object v6, p1, Lcom/twitter/database/model/f;->e:Ljava/lang/String;

    iget-object v7, p1, Lcom/twitter/database/model/f;->c:Ljava/lang/String;

    iget-object v8, p1, Lcom/twitter/database/model/f;->f:Ljava/lang/String;

    invoke-interface/range {v0 .. v8}, Lcom/twitter/database/model/a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/database/internal/i;->a(Ljava/lang/Object;)Lcom/twitter/database/model/g;

    move-result-object v0

    .line 40
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/database/internal/i;->b:Lcom/twitter/database/internal/f;

    invoke-virtual {v0}, Lcom/twitter/database/internal/f;->e()Lcom/twitter/database/model/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/database/internal/i;->b()Lcom/twitter/database/internal/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/database/internal/h;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/database/internal/i;->a()[Ljava/lang/String;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    move-object v8, v3

    invoke-interface/range {v0 .. v8}, Lcom/twitter/database/model/a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/database/internal/i;->a(Ljava/lang/Object;)Lcom/twitter/database/model/g;

    move-result-object v0

    goto :goto_0
.end method

.method public abstract a(Ljava/lang/Object;)Lcom/twitter/database/model/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/twitter/database/model/g",
            "<TP;>;"
        }
    .end annotation
.end method

.method public varargs a(Ljava/lang/String;[Ljava/lang/Object;)Lcom/twitter/database/model/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Object;",
            ")",
            "Lcom/twitter/database/model/g",
            "<TP;>;"
        }
    .end annotation

    .prologue
    .line 49
    invoke-static {}, Lcom/twitter/util/g;->c()V

    .line 50
    invoke-static {p2}, Lcom/twitter/util/collection/CollectionUtils;->e([Ljava/lang/Object;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/database/internal/i;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/twitter/database/model/g;

    move-result-object v0

    return-object v0
.end method

.method public varargs a(Ljava/lang/String;[Ljava/lang/String;)Lcom/twitter/database/model/g;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Lcom/twitter/database/model/g",
            "<TP;>;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 57
    invoke-static {}, Lcom/twitter/util/g;->c()V

    .line 58
    iget-object v0, p0, Lcom/twitter/database/internal/i;->b:Lcom/twitter/database/internal/f;

    invoke-virtual {v0}, Lcom/twitter/database/internal/f;->e()Lcom/twitter/database/model/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/database/internal/i;->b()Lcom/twitter/database/internal/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/database/internal/h;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/database/internal/i;->a()[Ljava/lang/String;

    move-result-object v2

    move-object v3, p1

    move-object v4, p2

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-interface/range {v0 .. v8}, Lcom/twitter/database/model/a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/database/internal/i;->a(Ljava/lang/Object;)Lcom/twitter/database/model/g;

    move-result-object v0

    return-object v0
.end method

.method public abstract a()[Ljava/lang/String;
.end method

.method public varargs b(Ljava/lang/String;[Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 72
    invoke-static {}, Lcom/twitter/util/g;->c()V

    .line 73
    invoke-static {p2}, Lcom/twitter/util/collection/CollectionUtils;->e([Ljava/lang/Object;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/database/internal/i;->b(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public varargs b(Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 79
    invoke-static {}, Lcom/twitter/util/g;->c()V

    .line 80
    iget-object v0, p0, Lcom/twitter/database/internal/i;->b:Lcom/twitter/database/internal/f;

    invoke-virtual {v0}, Lcom/twitter/database/internal/f;->e()Lcom/twitter/database/model/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/database/internal/i;->b()Lcom/twitter/database/internal/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/database/internal/h;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Lcom/twitter/database/model/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method protected abstract b()Lcom/twitter/database/internal/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/twitter/database/internal/h;",
            ">()TT;"
        }
    .end annotation
.end method

.method public c()Lcom/twitter/database/model/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/database/model/g",
            "<TP;>;"
        }
    .end annotation

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/database/internal/i;->a(Lcom/twitter/database/model/f;)Lcom/twitter/database/model/g;

    move-result-object v0

    return-object v0
.end method

.method public d()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcom/twitter/database/model/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/twitter/database/internal/i;->b()Lcom/twitter/database/internal/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/database/internal/h;->j()Lrx/c;

    move-result-object v0

    return-object v0
.end method
