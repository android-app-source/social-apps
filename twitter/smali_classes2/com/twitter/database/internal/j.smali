.class public abstract Lcom/twitter/database/internal/j;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/database/model/m;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/twitter/database/model/m",
        "<TS;>;"
    }
.end annotation


# instance fields
.field protected final a:Lcom/twitter/database/internal/f;


# direct methods
.method protected constructor <init>(Lcom/twitter/database/internal/f;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/twitter/database/internal/j;->a:Lcom/twitter/database/internal/f;

    .line 22
    return-void
.end method


# virtual methods
.method public varargs a(Ljava/lang/String;[Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 27
    invoke-static {p2}, Lcom/twitter/util/collection/CollectionUtils;->e([Ljava/lang/Object;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/database/internal/j;->a(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public varargs a(Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/twitter/database/internal/j;->a()Lcom/twitter/database/internal/k;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/database/internal/k;->a(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method protected abstract a()Lcom/twitter/database/internal/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/twitter/database/internal/k;",
            ">()TT;"
        }
    .end annotation
.end method

.method public c()Lcom/twitter/database/model/o;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/twitter/database/internal/j;->a:Lcom/twitter/database/internal/f;

    invoke-virtual {v0}, Lcom/twitter/database/internal/f;->h()Lcom/twitter/database/model/o;

    move-result-object v0

    return-object v0
.end method
