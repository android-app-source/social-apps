.class final enum Lcom/twitter/badge/LauncherIconBadgeUtil$Badger$1;
.super Lcom/twitter/badge/LauncherIconBadgeUtil$Badger;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/badge/LauncherIconBadgeUtil$Badger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/badge/LauncherIconBadgeUtil$Badger;-><init>(Ljava/lang/String;ILcom/twitter/badge/LauncherIconBadgeUtil$1;)V

    return-void
.end method

.method private d(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 99
    invoke-static {p1}, Lcom/twitter/badge/LauncherIconBadgeUtil$Badger$1;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 100
    if-nez v1, :cond_0

    .line 101
    const-string/jumbo v0, "unknown"

    .line 114
    :goto_0
    return-object v0

    .line 104
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 114
    const-string/jumbo v0, "unknown"

    goto :goto_0

    .line 104
    :sswitch_0
    const-string/jumbo v2, "com.sec.android.app.launcher"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string/jumbo v2, "com.sec.android.app.twlauncher"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string/jumbo v2, "com.lge.launcher"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_3
    const-string/jumbo v2, "com.lge.launcher2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x3

    goto :goto_1

    .line 107
    :pswitch_0
    const-string/jumbo v0, "samsung"

    goto :goto_0

    .line 111
    :pswitch_1
    const-string/jumbo v0, "lge"

    goto :goto_0

    .line 104
    nop

    :sswitch_data_0
    .sparse-switch
        0xb268c41 -> :sswitch_3
        0x10e03611 -> :sswitch_2
        0x1f29c336 -> :sswitch_0
        0x24621139 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method a(Landroid/content/Context;JI)V
    .locals 4

    .prologue
    .line 85
    invoke-static {p1}, Lcom/twitter/badge/LauncherIconBadgeUtil$Badger$1;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 86
    if-eqz v0, :cond_0

    .line 87
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.BADGE_COUNT_UPDATE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 88
    const-string/jumbo v2, "badge_count_package_name"

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    const-string/jumbo v2, "badge_count_class_name"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 90
    const-string/jumbo v0, "badge_count"

    invoke-virtual {v1, v0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 91
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 92
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "app"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "badge"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "update"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    .line 93
    invoke-direct {p0, p1}, Lcom/twitter/badge/LauncherIconBadgeUtil$Badger$1;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "success"

    aput-object v3, v1, v2

    invoke-direct {v0, p2, p3, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    .line 92
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 95
    :cond_0
    return-void
.end method

.method a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return v0
.end method
