.class final enum Lcom/twitter/badge/LauncherIconBadgeUtil$Badger$2;
.super Lcom/twitter/badge/LauncherIconBadgeUtil$Badger;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/badge/LauncherIconBadgeUtil$Badger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/badge/LauncherIconBadgeUtil$Badger;-><init>(Ljava/lang/String;ILcom/twitter/badge/LauncherIconBadgeUtil$1;)V

    return-void
.end method

.method private a(Landroid/content/Context;JILjava/lang/String;)V
    .locals 4

    .prologue
    .line 157
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 158
    const-string/jumbo v1, "badge_count"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 159
    const-string/jumbo v1, "package_name"

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const-string/jumbo v1, "activity_name"

    invoke-virtual {v0, v1, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "content://com.sonymobile.home.resourceprovider/badge"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 162
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "app:badge:update:sony:success"

    aput-object v3, v1, v2

    invoke-direct {v0, p2, p3, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 163
    return-void
.end method

.method private b(Landroid/content/Context;JILjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 167
    invoke-static {}, Lcom/twitter/util/android/f;->a()Lcom/twitter/util/android/f;

    move-result-object v0

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v4, "com.sonyericsson.home.permission.BROADCAST_BADGE"

    aput-object v4, v3, v2

    .line 168
    invoke-virtual {v0, p1, v3}, Lcom/twitter/util/android/f;->a(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v0, "com.sonyericsson.home.action.UPDATE_BADGE"

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 170
    const-string/jumbo v0, "com.sonyericsson.home.intent.extra.badge.PACKAGE_NAME"

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 171
    const-string/jumbo v0, "com.sonyericsson.home.intent.extra.badge.ACTIVITY_NAME"

    invoke-virtual {v3, v0, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 172
    const-string/jumbo v0, "com.sonyericsson.home.intent.extra.badge.MESSAGE"

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 173
    const-string/jumbo v4, "com.sonyericsson.home.intent.extra.badge.SHOW_MESSAGE"

    if-lez p4, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 174
    invoke-virtual {p1, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 175
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v4, "app:badge:update:sony:success"

    aput-object v4, v3, v2

    invoke-direct {v0, p2, p3, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 177
    :cond_0
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v3, "app:badge:update:sony:permissions_denied"

    aput-object v3, v1, v2

    invoke-direct {v0, p2, p3, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 178
    return-void

    :cond_1
    move v0, v2

    .line 173
    goto :goto_0
.end method

.method private d(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 181
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string/jumbo v2, "com.sonymobile.home.resourceprovider"

    .line 182
    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 181
    :cond_0
    return v0
.end method


# virtual methods
.method a(Landroid/content/Context;JI)V
    .locals 6

    .prologue
    .line 143
    invoke-static {p1}, Lcom/twitter/badge/LauncherIconBadgeUtil$Badger$2;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 144
    if-eqz v5, :cond_0

    .line 145
    invoke-direct {p0, p1}, Lcom/twitter/badge/LauncherIconBadgeUtil$Badger$2;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "oem_launcher_badge_sony_update_5383"

    const-string/jumbo v1, "enabled"

    .line 146
    invoke-static {p2, p3, v0, v1}, Lcoi;->a(JLjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move v4, p4

    .line 148
    invoke-direct/range {v0 .. v5}, Lcom/twitter/badge/LauncherIconBadgeUtil$Badger$2;->a(Landroid/content/Context;JILjava/lang/String;)V

    .line 153
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move v4, p4

    .line 150
    invoke-direct/range {v0 .. v5}, Lcom/twitter/badge/LauncherIconBadgeUtil$Badger$2;->b(Landroid/content/Context;JILjava/lang/String;)V

    goto :goto_0
.end method

.method a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 137
    const-string/jumbo v0, "com.sonyericsson.home"

    invoke-static {p1}, Lcom/twitter/badge/LauncherIconBadgeUtil$Badger$2;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
