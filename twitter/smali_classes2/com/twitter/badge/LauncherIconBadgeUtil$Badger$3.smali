.class final enum Lcom/twitter/badge/LauncherIconBadgeUtil$Badger$3;
.super Lcom/twitter/badge/LauncherIconBadgeUtil$Badger;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/twitter/badge/LauncherIconBadgeUtil$Badger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/badge/LauncherIconBadgeUtil$Badger;-><init>(Ljava/lang/String;ILcom/twitter/badge/LauncherIconBadgeUtil$1;)V

    return-void
.end method


# virtual methods
.method a(Landroid/content/Context;JI)V
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 193
    invoke-static {p1}, Lcom/twitter/badge/LauncherIconBadgeUtil$Badger$3;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 194
    if-eqz v0, :cond_0

    .line 196
    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 197
    const-string/jumbo v2, "tag"

    const-string/jumbo v3, "%s/%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 198
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 197
    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const-string/jumbo v0, "count"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 200
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "content://com.teslacoilsw.notifier/unread_count"

    .line 201
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 202
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "app:badge:update:nova:success"

    aput-object v3, v1, v2

    invoke-direct {v0, p2, p3, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    invoke-static {v0}, Lcpm;->a(Lcpk;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 203
    :catch_0
    move-exception v0

    .line 205
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    new-array v1, v8, [Ljava/lang/String;

    const-string/jumbo v2, "app:badge:update:nova:unavailable"

    aput-object v2, v1, v7

    invoke-direct {v0, p2, p3, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_0

    .line 206
    :catch_1
    move-exception v0

    .line 207
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "app:badge:update:nova:failure"

    aput-object v3, v2, v7

    invoke-direct {v1, p2, p3, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 209
    const-string/jumbo v1, "Badger.NOVA"

    const-string/jumbo v2, "Error setting badge for Nova launcher"

    invoke-static {v1, v2, v0}, Lcqj;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 216
    const-string/jumbo v0, "com.teslacoilsw.launcher"

    invoke-static {p1}, Lcom/twitter/badge/LauncherIconBadgeUtil$Badger$3;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "com.teslacoilsw.notifier"

    .line 217
    invoke-static {p1, v0}, Lcom/twitter/util/u;->c(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 216
    :goto_0
    return v0

    .line 217
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
