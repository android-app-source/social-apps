.class public Lcom/twitter/badge/LauncherIconBadgeUpdaterService;
.super Landroid/app/IntentService;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 81
    const-class v0, Lcom/twitter/badge/LauncherIconBadgeUpdaterService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 82
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/badge/LauncherIconBadgeUpdaterService;->setIntentRedelivery(Z)V

    .line 83
    return-void
.end method

.method private a(J)I
    .locals 3

    .prologue
    .line 143
    invoke-direct {p0, p1, p2}, Lcom/twitter/badge/LauncherIconBadgeUpdaterService;->b(J)I

    move-result v0

    invoke-direct {p0, p1, p2}, Lcom/twitter/badge/LauncherIconBadgeUpdaterService;->c(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method static a(JZLcom/twitter/badge/LauncherIconBadgeUtil;I)V
    .locals 6
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 132
    if-eqz p2, :cond_1

    .line 133
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v0, "app:badge:update:all"

    aput-object v0, v2, v3

    if-lez p4, :cond_0

    const-string/jumbo v0, "nonzero"

    :goto_0
    aput-object v0, v2, v4

    invoke-direct {v1, p0, p1, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    .line 134
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 135
    invoke-virtual {p3, p0, p1, p4}, Lcom/twitter/badge/LauncherIconBadgeUtil;->a(JI)V

    .line 140
    :goto_1
    return-void

    .line 133
    :cond_0
    const-string/jumbo v0, "zero"

    goto :goto_0

    .line 137
    :cond_1
    invoke-virtual {p3, p0, p1, v3}, Lcom/twitter/badge/LauncherIconBadgeUtil;->a(JI)V

    .line 138
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "app:badge:update::disabled"

    aput-object v2, v1, v3

    invoke-direct {v0, p0, p1, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 51
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 52
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/badge/LauncherIconBadgeUpdaterService;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "update"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 53
    return-void
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 43
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/badge/LauncherIconBadgeUpdaterService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "remote_update"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "badge_count"

    .line 44
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 43
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 45
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/v;)V
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lcom/twitter/badge/LauncherIconBadgeUpdaterService$1;

    invoke-direct {v0, p0, p1}, Lcom/twitter/badge/LauncherIconBadgeUpdaterService$1;-><init>(Landroid/content/Context;Lcom/twitter/library/client/v;)V

    invoke-virtual {p1, v0}, Lcom/twitter/library/client/v;->a(Lcom/twitter/library/client/u;)V

    .line 78
    return-void
.end method

.method private b(J)I
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 147
    .line 148
    invoke-virtual {p0}, Lcom/twitter/badge/LauncherIconBadgeUpdaterService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/twitter/database/schema/a$l;->a:Landroid/net/Uri;

    .line 149
    invoke-static {v1, p1, p2}, Lcom/twitter/database/schema/a;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 148
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 151
    if-eqz v1, :cond_1

    .line 152
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 155
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 157
    :goto_1
    return v0

    :cond_0
    move v0, v6

    goto :goto_0

    :cond_1
    move v0, v6

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 56
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 57
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/badge/LauncherIconBadgeUpdaterService;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "clear"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 58
    return-void
.end method

.method private c(J)I
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 161
    const/4 v6, 0x0

    .line 162
    invoke-virtual {p0}, Lcom/twitter/badge/LauncherIconBadgeUpdaterService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/provider/GlobalDatabaseProvider;->b:Landroid/net/Uri;

    .line 164
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 163
    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/j$a;->a:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 166
    if-eqz v1, :cond_1

    .line 167
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    const/4 v0, 0x3

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 170
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 172
    :goto_1
    return v0

    :cond_0
    move v0, v6

    goto :goto_0

    :cond_1
    move v0, v6

    goto :goto_1
.end method

.method private d(J)Z
    .locals 3

    .prologue
    .line 176
    new-instance v0, Lcom/twitter/util/a;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/util/a;-><init>(Landroid/content/Context;J)V

    const-string/jumbo v1, "launcher_icon_badge_enabled"

    const/4 v2, 0x1

    .line 177
    invoke-virtual {v0, v1, v2}, Lcom/twitter/util/a;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 176
    return v0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 88
    if-nez p1, :cond_1

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 92
    if-eqz v0, :cond_0

    .line 93
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    .line 94
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_2
    move v0, v2

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 122
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unknown intent action for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :sswitch_0
    const-string/jumbo v6, "update"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_1

    :sswitch_1
    const-string/jumbo v6, "clear"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v3

    goto :goto_1

    :sswitch_2
    const-string/jumbo v6, "remote_update"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    goto :goto_1

    .line 96
    :pswitch_0
    cmp-long v0, v4, v8

    if-lez v0, :cond_0

    .line 97
    invoke-direct {p0, v4, v5}, Lcom/twitter/badge/LauncherIconBadgeUpdaterService;->a(J)I

    move-result v0

    .line 98
    invoke-direct {p0, v4, v5}, Lcom/twitter/badge/LauncherIconBadgeUpdaterService;->d(J)Z

    move-result v1

    .line 99
    invoke-static {p0}, Lcom/twitter/badge/LauncherIconBadgeUtil;->a(Landroid/content/Context;)Lcom/twitter/badge/LauncherIconBadgeUtil;

    move-result-object v2

    .line 98
    invoke-static {v4, v5, v1, v2, v0}, Lcom/twitter/badge/LauncherIconBadgeUpdaterService;->a(JZLcom/twitter/badge/LauncherIconBadgeUtil;I)V

    .line 100
    const-string/jumbo v1, "BadgeUpdaterService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "UPDATE: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 105
    :pswitch_1
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    new-array v2, v3, [Ljava/lang/String;

    const-string/jumbo v3, "app:badge:::clear"

    aput-object v3, v2, v1

    invoke-direct {v0, v4, v5, v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J[Ljava/lang/String;)V

    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 106
    invoke-static {p0}, Lcom/twitter/badge/LauncherIconBadgeUtil;->a(Landroid/content/Context;)Lcom/twitter/badge/LauncherIconBadgeUtil;

    move-result-object v0

    invoke-virtual {v0, v4, v5, v1}, Lcom/twitter/badge/LauncherIconBadgeUtil;->a(JI)V

    .line 107
    const-string/jumbo v0, "BadgeUpdaterService"

    const-string/jumbo v1, "CLEAR"

    invoke-static {v0, v1}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 111
    :pswitch_2
    const-string/jumbo v0, "badge_count"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 112
    if-gez v0, :cond_3

    .line 113
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Cannot set remote badge count to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcpd;->c(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 116
    :cond_3
    cmp-long v1, v4, v8

    if-lez v1, :cond_0

    .line 117
    invoke-direct {p0, v4, v5}, Lcom/twitter/badge/LauncherIconBadgeUpdaterService;->d(J)Z

    move-result v1

    invoke-static {p0}, Lcom/twitter/badge/LauncherIconBadgeUtil;->a(Landroid/content/Context;)Lcom/twitter/badge/LauncherIconBadgeUtil;

    move-result-object v2

    invoke-static {v4, v5, v1, v2, v0}, Lcom/twitter/badge/LauncherIconBadgeUpdaterService;->a(JZLcom/twitter/badge/LauncherIconBadgeUtil;I)V

    goto/16 :goto_0

    .line 94
    :sswitch_data_0
    .sparse-switch
        -0x31ffc737 -> :sswitch_0
        0x5a5b64d -> :sswitch_1
        0x2af082e2 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
