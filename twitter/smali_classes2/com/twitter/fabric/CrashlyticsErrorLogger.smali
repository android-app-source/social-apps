.class public Lcom/twitter/fabric/CrashlyticsErrorLogger;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcpc;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/twitter/fabric/CrashlyticsErrorLogger$a;,
        Lcom/twitter/fabric/CrashlyticsErrorLogger$DroppedLogsException;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/fabric/CrashlyticsErrorLogger$a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/Thread$UncaughtExceptionHandler;

.field private c:Lcom/twitter/util/Tristate;

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/fabric/CrashlyticsErrorLogger;->a:Ljava/util/List;

    .line 37
    sget-object v0, Lcom/twitter/util/Tristate;->a:Lcom/twitter/util/Tristate;

    iput-object v0, p0, Lcom/twitter/fabric/CrashlyticsErrorLogger;->c:Lcom/twitter/util/Tristate;

    .line 42
    new-instance v0, Lio/fabric/sdk/android/c$a;

    invoke-direct {v0, p1}, Lio/fabric/sdk/android/c$a;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcof;->m()Lcof;

    move-result-object v3

    invoke-virtual {v3}, Lcof;->a()Z

    move-result v3

    invoke-virtual {v0, v3}, Lio/fabric/sdk/android/c$a;->a(Z)Lio/fabric/sdk/android/c$a;

    move-result-object v3

    const/4 v0, 0x2

    new-array v4, v0, [Lio/fabric/sdk/android/h;

    new-instance v5, Lf$a;

    invoke-direct {v5}, Lf$a;-><init>()V

    if-nez p2, :cond_0

    move v0, v1

    .line 43
    :goto_0
    invoke-virtual {v5, v0}, Lf$a;->a(Z)Lf$a;

    move-result-object v0

    invoke-virtual {v0}, Lf$a;->a()Lf;

    move-result-object v0

    aput-object v0, v4, v2

    new-instance v0, Lcom/crashlytics/android/ndk/b;

    invoke-direct {v0}, Lcom/crashlytics/android/ndk/b;-><init>()V

    aput-object v0, v4, v1

    .line 42
    invoke-virtual {v3, v4}, Lio/fabric/sdk/android/c$a;->a([Lio/fabric/sdk/android/h;)Lio/fabric/sdk/android/c$a;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lio/fabric/sdk/android/c$a;->a()Lio/fabric/sdk/android/c;

    move-result-object v0

    .line 42
    invoke-static {v0}, Lio/fabric/sdk/android/c;->a(Lio/fabric/sdk/android/c;)Lio/fabric/sdk/android/c;

    .line 46
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/fabric/CrashlyticsErrorLogger;->b:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 48
    const-string/jumbo v0, "\nGLOBAL VALUES\n"

    invoke-static {v0}, Lf;->a(Ljava/lang/String;)V

    .line 49
    return-void

    :cond_0
    move v0, v2

    .line 42
    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 4

    .prologue
    .line 111
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 112
    :goto_0
    const-string/jumbo v1, "user_name"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 113
    invoke-static {v0}, Lf;->b(Ljava/lang/String;)V

    .line 119
    :goto_1
    const-string/jumbo v1, "CrashlyticsErrorLogger"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcqj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    return-void

    .line 111
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 114
    :cond_1
    if-eqz p2, :cond_2

    .line 115
    invoke-static {p0, v0}, Lcom/twitter/fabric/CrashlyticsErrorLogger;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 117
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lf;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/16 v2, 0x400

    const/4 v1, 0x0

    .line 123
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-gt v0, v2, :cond_2

    .line 124
    :cond_0
    invoke-static {p0, p1}, Lf;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :cond_1
    return-void

    .line 126
    :cond_2
    invoke-static {p1, v2}, Lcom/twitter/util/y;->c(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v2

    move v0, v1

    .line 127
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 128
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string/jumbo v4, "%s_%02d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v1

    const/4 v6, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aget-object v4, v2, v0

    invoke-static {v3, v4}, Lf;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static a(Ljava/util/Map;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 105
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 106
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1, v0, p1}, Lcom/twitter/fabric/CrashlyticsErrorLogger;->a(Ljava/lang/String;Ljava/lang/Object;Z)V

    goto :goto_0

    .line 108
    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized a(Lcom/twitter/util/Tristate;)V
    .locals 3

    .prologue
    .line 52
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/twitter/fabric/CrashlyticsErrorLogger;->c:Lcom/twitter/util/Tristate;

    .line 53
    iget-object v0, p0, Lcom/twitter/fabric/CrashlyticsErrorLogger;->c:Lcom/twitter/util/Tristate;

    sget-object v1, Lcom/twitter/util/Tristate;->c:Lcom/twitter/util/Tristate;

    if-ne v0, v1, :cond_1

    .line 54
    iget-object v0, p0, Lcom/twitter/fabric/CrashlyticsErrorLogger;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/fabric/CrashlyticsErrorLogger$a;

    .line 55
    iget-object v2, v0, Lcom/twitter/fabric/CrashlyticsErrorLogger$a;->a:Lcpb;

    iget-object v0, v0, Lcom/twitter/fabric/CrashlyticsErrorLogger$a;->b:Lcpa$b;

    invoke-virtual {p0, v2, v0}, Lcom/twitter/fabric/CrashlyticsErrorLogger;->a(Lcpb;Lcpa$b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 52
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 57
    :cond_0
    :try_start_1
    iget v0, p0, Lcom/twitter/fabric/CrashlyticsErrorLogger;->d:I

    if-lez v0, :cond_1

    .line 58
    new-instance v0, Lcom/twitter/fabric/CrashlyticsErrorLogger$DroppedLogsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Dropped: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/twitter/fabric/CrashlyticsErrorLogger;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " logs."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/fabric/CrashlyticsErrorLogger$DroppedLogsException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 61
    :cond_1
    iget-object v0, p0, Lcom/twitter/fabric/CrashlyticsErrorLogger;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(Lcpb;Lcpa$b;)V
    .locals 2

    .prologue
    .line 71
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/fabric/CrashlyticsErrorLogger;->c:Lcom/twitter/util/Tristate;

    sget-object v1, Lcom/twitter/util/Tristate;->c:Lcom/twitter/util/Tristate;

    if-ne v0, v1, :cond_1

    .line 72
    const-string/jumbo v0, "\nNON-FATAL EXCEPTION\n"

    invoke-static {v0}, Lf;->a(Ljava/lang/String;)V

    .line 73
    invoke-virtual {p1}, Lcpb;->a()Ljava/util/Map;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/fabric/CrashlyticsErrorLogger;->a(Ljava/util/Map;Z)V

    .line 74
    iget-object v0, p2, Lcpa$b;->b:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/fabric/CrashlyticsErrorLogger;->a(Ljava/util/Map;Z)V

    .line 75
    invoke-virtual {p1}, Lcpb;->c()Ljava/lang/Throwable;

    move-result-object v0

    .line 76
    invoke-static {v0}, Lf;->a(Ljava/lang/Throwable;)V

    .line 78
    const-string/jumbo v1, "\nRECENT NON-FATAL EXCEPTION\n"

    invoke-static {v1}, Lf;->a(Ljava/lang/String;)V

    .line 79
    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lf;->a(Ljava/lang/String;)V

    .line 80
    invoke-static {v0}, Lcqj;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lf;->a(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p1}, Lcpb;->a()Ljava/util/Map;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/fabric/CrashlyticsErrorLogger;->a(Ljava/util/Map;Z)V

    .line 82
    iget-object v0, p2, Lcpa$b;->b:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/fabric/CrashlyticsErrorLogger;->a(Ljava/util/Map;Z)V

    .line 83
    const-string/jumbo v0, "\nGLOBAL VALUES\n"

    invoke-static {v0}, Lf;->a(Ljava/lang/String;)V

    .line 84
    iget-object v0, p2, Lcpa$b;->a:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/fabric/CrashlyticsErrorLogger;->a(Ljava/util/Map;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 85
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/twitter/fabric/CrashlyticsErrorLogger;->c:Lcom/twitter/util/Tristate;

    sget-object v1, Lcom/twitter/util/Tristate;->a:Lcom/twitter/util/Tristate;

    if-ne v0, v1, :cond_0

    .line 86
    iget-object v0, p0, Lcom/twitter/fabric/CrashlyticsErrorLogger;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x64

    if-lt v0, v1, :cond_2

    .line 88
    iget v0, p0, Lcom/twitter/fabric/CrashlyticsErrorLogger;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/fabric/CrashlyticsErrorLogger;->d:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 71
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 90
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/twitter/fabric/CrashlyticsErrorLogger;->a:Ljava/util/List;

    new-instance v1, Lcom/twitter/fabric/CrashlyticsErrorLogger$a;

    invoke-direct {v1, p1, p2}, Lcom/twitter/fabric/CrashlyticsErrorLogger$a;-><init>(Lcpb;Lcpa$b;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Lcom/twitter/fabric/CrashlyticsErrorLogger;->a(Ljava/lang/String;Ljava/lang/Object;Z)V

    .line 67
    return-void
.end method

.method public declared-synchronized b(Lcpb;Lcpa$b;)V
    .locals 3

    .prologue
    .line 99
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcpb;->a()Ljava/util/Map;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/fabric/CrashlyticsErrorLogger;->a(Ljava/util/Map;Z)V

    .line 100
    iget-object v0, p2, Lcpa$b;->b:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/fabric/CrashlyticsErrorLogger;->a(Ljava/util/Map;Z)V

    .line 101
    iget-object v0, p0, Lcom/twitter/fabric/CrashlyticsErrorLogger;->b:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {p1}, Lcpb;->c()Ljava/lang/Throwable;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    monitor-exit p0

    return-void

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
