.class Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;
.super Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;
.source "Twttr"

# interfaces
.implements Llx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/okhttp/v_1_5_1/internal/http/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic d:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

.field private e:I

.field private f:Z

.field private final g:Lcom/squareup/okhttp/v_1_5_1/internal/http/h;


# direct methods
.method constructor <init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;Ljava/net/CacheRequest;Lcom/squareup/okhttp/v_1_5_1/internal/http/h;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 449
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    .line 450
    invoke-direct {p0, p1, p2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;Ljava/net/CacheRequest;)V

    .line 445
    const/4 v0, -0x1

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->e:I

    .line 446
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->f:Z

    .line 451
    iput-object p3, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->g:Lcom/squareup/okhttp/v_1_5_1/internal/http/h;

    .line 452
    return-void
.end method

.method private b()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v2, -0x1

    const/4 v3, 0x1

    .line 477
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->e:I

    if-eq v0, v2, :cond_0

    .line 478
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->f(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;)Llk;

    move-result-object v0

    invoke-interface {v0, v3}, Llk;->a(Z)Ljava/lang/String;

    .line 480
    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->f(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;)Llk;

    move-result-object v0

    invoke-interface {v0, v3}, Llk;->a(Z)Ljava/lang/String;

    move-result-object v0

    .line 481
    const-string/jumbo v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 482
    if-eq v1, v2, :cond_1

    .line 483
    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 486
    :cond_1
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->e:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 490
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->e:I

    if-nez v0, :cond_2

    .line 491
    iput-boolean v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->f:Z

    .line 492
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    invoke-direct {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;-><init>()V

    .line 493
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    invoke-virtual {v1, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;)V

    .line 494
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->g:Lcom/squareup/okhttp/v_1_5_1/internal/http/h;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;->a()Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/d;)V

    .line 495
    invoke-virtual {p0, v3}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->a(Z)V

    .line 497
    :cond_2
    return-void

    .line 487
    :catch_0
    move-exception v1

    .line 488
    new-instance v1, Ljava/net/ProtocolException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Expected a hex chunk size but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public b(Llq;J)J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v0, -0x1

    .line 456
    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 457
    :cond_0
    iget-boolean v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->b:Z

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 458
    :cond_1
    iget-boolean v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->f:Z

    if-nez v2, :cond_3

    .line 472
    :cond_2
    :goto_0
    return-wide v0

    .line 460
    :cond_3
    iget v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->e:I

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->e:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_5

    .line 461
    :cond_4
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->b()V

    .line 462
    iget-boolean v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->f:Z

    if-eqz v2, :cond_2

    .line 465
    :cond_5
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    invoke-static {v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->f(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;)Llk;

    move-result-object v2

    iget v3, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->e:I

    int-to-long v4, v3

    invoke-static {p2, p3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-interface {v2, p1, v4, v5}, Llk;->b(Llq;J)J

    move-result-wide v2

    .line 466
    cmp-long v0, v2, v0

    if-nez v0, :cond_6

    .line 467
    invoke-virtual {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->a()V

    .line 468
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "unexpected end of stream"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 470
    :cond_6
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->e:I

    int-to-long v0, v0

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->e:I

    .line 471
    invoke-virtual {p0, p1, v2, v3}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->a(Llq;J)V

    move-wide v0, v2

    .line 472
    goto :goto_0
.end method

.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 505
    iget-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->b:Z

    if-eqz v0, :cond_0

    .line 510
    :goto_0
    return-void

    .line 506
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    const/16 v1, 0x64

    invoke-virtual {v0, p0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->a(Llx;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 507
    invoke-virtual {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->a()V

    .line 509
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;->b:Z

    goto :goto_0
.end method
