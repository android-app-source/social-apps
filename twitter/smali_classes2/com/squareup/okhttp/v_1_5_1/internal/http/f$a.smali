.class Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/okhttp/v_1_5_1/internal/http/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field protected final a:Ljava/io/OutputStream;

.field protected b:Z

.field final synthetic c:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

.field private final d:Ljava/net/CacheRequest;


# direct methods
.method constructor <init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;Ljava/net/CacheRequest;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 334
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 336
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/net/CacheRequest;->getBody()Ljava/io/OutputStream;

    move-result-object v1

    .line 337
    :goto_0
    if-nez v1, :cond_0

    move-object p2, v0

    .line 341
    :cond_0
    iput-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;->a:Ljava/io/OutputStream;

    .line 342
    iput-object p2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;->d:Ljava/net/CacheRequest;

    .line 343
    return-void

    :cond_1
    move-object v1, v0

    .line 336
    goto :goto_0
.end method


# virtual methods
.method protected final a()V
    .locals 2

    .prologue
    .line 386
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;->d:Ljava/net/CacheRequest;

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;->d:Ljava/net/CacheRequest;

    invoke-virtual {v0}, Ljava/net/CacheRequest;->abort()V

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->d(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;)Lcom/squareup/okhttp/v_1_5_1/c;

    move-result-object v0

    invoke-static {v0}, Llh;->a(Ljava/io/Closeable;)V

    .line 390
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;I)I

    .line 391
    return-void
.end method

.method protected final a(Llq;J)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 347
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;->a:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    .line 348
    invoke-virtual {p1}, Llq;->l()J

    move-result-wide v0

    sub-long v2, v0, p2

    iget-object v6, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;->a:Ljava/io/OutputStream;

    move-object v1, p1

    move-wide v4, p2

    invoke-static/range {v1 .. v6}, Llr;->a(Llq;JJLjava/io/OutputStream;)V

    .line 350
    :cond_0
    return-void
.end method

.method protected final a(Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 357
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->b(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;)I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    invoke-static {v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->b(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359
    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;->d:Ljava/net/CacheRequest;

    if-eqz v0, :cond_1

    .line 360
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;->a:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 363
    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    invoke-static {v0, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;I)I

    .line 364
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->c(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 365
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    invoke-static {v0, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->b(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;I)I

    .line 366
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;)Lcom/squareup/okhttp/v_1_5_1/d;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    invoke-static {v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->d(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;)Lcom/squareup/okhttp/v_1_5_1/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/d;->a(Lcom/squareup/okhttp/v_1_5_1/c;)V

    .line 371
    :cond_2
    :goto_0
    return-void

    .line 367
    :cond_3
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->c(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 368
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;I)I

    .line 369
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->d(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;)Lcom/squareup/okhttp/v_1_5_1/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/c;->close()V

    goto :goto_0
.end method
