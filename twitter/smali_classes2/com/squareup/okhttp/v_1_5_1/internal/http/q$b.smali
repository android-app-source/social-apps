.class final Lcom/squareup/okhttp/v_1_5_1/internal/http/q$b;
.super Lcom/squareup/okhttp/v_1_5_1/internal/http/b;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/okhttp/v_1_5_1/internal/http/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation


# instance fields
.field private final a:Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;


# direct methods
.method public constructor <init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;)V
    .locals 0

    .prologue
    .line 503
    invoke-direct {p0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/b;-><init>(Ljava/net/HttpURLConnection;)V

    .line 504
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$b;->a:Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;

    .line 505
    return-void
.end method


# virtual methods
.method protected a()Lcom/squareup/okhttp/v_1_5_1/e;
    .locals 1

    .prologue
    .line 508
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$b;->a:Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->f()Lcom/squareup/okhttp/v_1_5_1/e;

    move-result-object v0

    return-object v0
.end method

.method public getHostnameVerifier()Ljavax/net/ssl/HostnameVerifier;
    .locals 1

    .prologue
    .line 516
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public getSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;
    .locals 1

    .prologue
    .line 524
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setFixedLengthStreamingMode(J)V
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$b;->a:Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;->setFixedLengthStreamingMode(J)V

    .line 529
    return-void
.end method

.method public setHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V
    .locals 1

    .prologue
    .line 512
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->a()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V
    .locals 1

    .prologue
    .line 520
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->a()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method
