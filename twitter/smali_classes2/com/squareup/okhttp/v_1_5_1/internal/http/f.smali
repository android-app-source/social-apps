.class public final Lcom/squareup/okhttp/v_1_5_1/internal/http/f;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/okhttp/v_1_5_1/internal/http/f$1;,
        Lcom/squareup/okhttp/v_1_5_1/internal/http/f$f;,
        Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;,
        Lcom/squareup/okhttp/v_1_5_1/internal/http/f$e;,
        Lcom/squareup/okhttp/v_1_5_1/internal/http/f$a;,
        Lcom/squareup/okhttp/v_1_5_1/internal/http/f$b;,
        Lcom/squareup/okhttp/v_1_5_1/internal/http/f$d;
    }
.end annotation


# static fields
.field private static final g:[B

.field private static final h:[B


# instance fields
.field private final a:Lcom/squareup/okhttp/v_1_5_1/d;

.field private final b:Lcom/squareup/okhttp/v_1_5_1/c;

.field private final c:Llk;

.field private final d:Llj;

.field private e:I

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 275
    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->g:[B

    .line 278
    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->h:[B

    return-void

    .line 275
    :array_0
    .array-data 1
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x61t
        0x62t
        0x63t
        0x64t
        0x65t
        0x66t
    .end array-data

    .line 278
    :array_1
    .array-data 1
        0x30t
        0xdt
        0xat
        0xdt
        0xat
    .end array-data
.end method

.method public constructor <init>(Lcom/squareup/okhttp/v_1_5_1/d;Lcom/squareup/okhttp/v_1_5_1/c;Llk;Llj;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    .line 76
    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->f:I

    .line 80
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->a:Lcom/squareup/okhttp/v_1_5_1/d;

    .line 81
    iput-object p2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->b:Lcom/squareup/okhttp/v_1_5_1/c;

    .line 82
    iput-object p3, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->c:Llk;

    .line 83
    iput-object p4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->d:Llj;

    .line 84
    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;I)I
    .locals 0

    .prologue
    .line 57
    iput p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    return p1
.end method

.method static synthetic a(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;)Llj;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->d:Llj;

    return-object v0
.end method

.method static synthetic b(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;)I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    return v0
.end method

.method static synthetic b(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;I)I
    .locals 0

    .prologue
    .line 57
    iput p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->f:I

    return p1
.end method

.method static synthetic c(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;)I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->f:I

    return v0
.end method

.method static synthetic d(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;)Lcom/squareup/okhttp/v_1_5_1/c;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->b:Lcom/squareup/okhttp/v_1_5_1/c;

    return-object v0
.end method

.method static synthetic e(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;)Lcom/squareup/okhttp/v_1_5_1/d;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->a:Lcom/squareup/okhttp/v_1_5_1/d;

    return-object v0
.end method

.method static synthetic f(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;)Llk;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->c:Llk;

    return-object v0
.end method

.method static synthetic h()[B
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->h:[B

    return-object v0
.end method

.method static synthetic i()[B
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->g:[B

    return-object v0
.end method


# virtual methods
.method public a(J)Llw;
    .locals 3

    .prologue
    .line 198
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 199
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    .line 200
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$d;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;JLcom/squareup/okhttp/v_1_5_1/internal/http/f$1;)V

    return-object v0
.end method

.method public a(Ljava/net/CacheRequest;)Llx;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 232
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 233
    :cond_0
    const/4 v0, 0x5

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    .line 234
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$f;

    invoke-direct {v0, p0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$f;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;Ljava/net/CacheRequest;)V

    return-object v0
.end method

.method public a(Ljava/net/CacheRequest;J)Llx;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 211
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 212
    :cond_0
    const/4 v0, 0x5

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    .line 213
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$e;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$e;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;Ljava/net/CacheRequest;J)V

    return-object v0
.end method

.method public a(Ljava/net/CacheRequest;Lcom/squareup/okhttp/v_1_5_1/internal/http/h;)Llx;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 226
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 227
    :cond_0
    const/4 v0, 0x5

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    .line 228
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$c;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;Ljava/net/CacheRequest;Lcom/squareup/okhttp/v_1_5_1/internal/http/h;)V

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 91
    const/4 v0, 0x1

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->f:I

    .line 94
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    if-nez v0, :cond_0

    .line 95
    const/4 v0, 0x0

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->f:I

    .line 96
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->a:Lcom/squareup/okhttp/v_1_5_1/d;

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->b:Lcom/squareup/okhttp/v_1_5_1/c;

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/d;->a(Lcom/squareup/okhttp/v_1_5_1/c;)V

    .line 98
    :cond_0
    return-void
.end method

.method public a(Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 165
    :goto_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->c:Llk;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Llk;->a(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 166
    invoke-virtual {p1, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;->a(Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    goto :goto_0

    .line 168
    :cond_0
    return-void
.end method

.method public a(Lcom/squareup/okhttp/v_1_5_1/internal/http/d;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->d:Llj;

    invoke-interface {v0, p2}, Llj;->a(Ljava/lang/String;)Llj;

    move-result-object v0

    const-string/jumbo v1, "\r\n"

    invoke-interface {v0, v1}, Llj;->a(Ljava/lang/String;)Llj;

    .line 127
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d;->a()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 128
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->d:Llj;

    invoke-virtual {p1, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Llj;->a(Ljava/lang/String;)Llj;

    move-result-object v1

    const-string/jumbo v2, ": "

    invoke-interface {v1, v2}, Llj;->a(Ljava/lang/String;)Llj;

    move-result-object v1

    invoke-virtual {p1, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Llj;->a(Ljava/lang/String;)Llj;

    move-result-object v1

    const-string/jumbo v2, "\r\n"

    invoke-interface {v1, v2}, Llj;->a(Ljava/lang/String;)Llj;

    .line 127
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 133
    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->d:Llj;

    const-string/jumbo v1, "\r\n"

    invoke-interface {v0, v1}, Llj;->a(Ljava/lang/String;)Llj;

    .line 134
    const/4 v0, 0x1

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    .line 135
    return-void
.end method

.method public a(Lcom/squareup/okhttp/v_1_5_1/internal/http/r;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 204
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 205
    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    .line 206
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->d:Llj;

    invoke-virtual {p1, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/r;->a(Llj;)V

    .line 207
    return-void
.end method

.method public a(Llx;I)Z
    .locals 3

    .prologue
    .line 177
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->b:Lcom/squareup/okhttp/v_1_5_1/c;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/c;->c()Ljava/net/Socket;

    move-result-object v1

    .line 179
    :try_start_0
    invoke-virtual {v1}, Ljava/net/Socket;->getSoTimeout()I

    move-result v2

    .line 180
    invoke-virtual {v1, p2}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    :try_start_1
    invoke-static {p1, p2}, Llh;->a(Llx;I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 184
    :try_start_2
    invoke-virtual {v1, v2}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 187
    :goto_0
    return v0

    .line 184
    :catchall_0
    move-exception v0

    invoke-virtual {v1, v2}, Ljava/net/Socket;->setSoTimeout(I)V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 186
    :catch_0
    move-exception v0

    .line 187
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    const/4 v0, 0x2

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->f:I

    .line 108
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    if-nez v0, :cond_0

    .line 109
    const/4 v0, 0x6

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    .line 110
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->b:Lcom/squareup/okhttp/v_1_5_1/c;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/c;->close()V

    .line 112
    :cond_0
    return-void
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 116
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->d:Llj;

    invoke-interface {v0}, Llj;->a()V

    .line 121
    return-void
.end method

.method public e()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 139
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    if-eq v0, v5, :cond_0

    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 140
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->c:Llk;

    invoke-interface {v0, v5}, Llk;->a(Z)Ljava/lang/String;

    move-result-object v0

    .line 145
    new-instance v1, Lcom/squareup/okhttp/v_1_5_1/internal/http/u;

    invoke-direct {v1, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/u;-><init>(Ljava/lang/String;)V

    .line 147
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;

    invoke-direct {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/u;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;

    move-result-object v0

    .line 150
    new-instance v2, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    invoke-direct {v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;-><init>()V

    .line 151
    invoke-virtual {p0, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;)V

    .line 152
    sget-object v3, Lcom/squareup/okhttp/v_1_5_1/internal/http/l;->e:Ljava/lang/String;

    sget-object v4, Lcom/squareup/okhttp/v_1_5_1/Protocol;->c:Lcom/squareup/okhttp/v_1_5_1/Protocol;

    iget-object v4, v4, Lcom/squareup/okhttp/v_1_5_1/Protocol;->name:Lll;

    invoke-virtual {v4}, Lll;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    .line 153
    invoke-virtual {v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;->a()Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/d;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;

    .line 155
    invoke-virtual {v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/u;->c()I

    move-result v1

    const/16 v2, 0x64

    if-eq v1, v2, :cond_0

    .line 156
    const/4 v1, 0x4

    iput v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    .line 157
    return-object v0
.end method

.method public f()Llw;
    .locals 3

    .prologue
    .line 192
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 193
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e:I

    .line 194
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$b;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;Lcom/squareup/okhttp/v_1_5_1/internal/http/f$1;)V

    return-object v0
.end method

.method public g()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 221
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->a(Ljava/net/CacheRequest;J)Llx;

    .line 222
    return-void
.end method
