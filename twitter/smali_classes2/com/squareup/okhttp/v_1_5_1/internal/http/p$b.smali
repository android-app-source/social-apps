.class public Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/okhttp/v_1_5_1/internal/http/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field private a:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

.field private b:Lcom/squareup/okhttp/v_1_5_1/internal/http/u;

.field private c:Lcom/squareup/okhttp/v_1_5_1/e;

.field private d:Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

.field private e:Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;

.field private f:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 369
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    invoke-direct {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    .line 370
    return-void
.end method

.method private constructor <init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)V
    .locals 1

    .prologue
    .line 372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 373
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->b(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->a:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    .line 374
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->c(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)Lcom/squareup/okhttp/v_1_5_1/internal/http/u;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->b:Lcom/squareup/okhttp/v_1_5_1/internal/http/u;

    .line 375
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->d(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)Lcom/squareup/okhttp/v_1_5_1/e;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->c:Lcom/squareup/okhttp/v_1_5_1/e;

    .line 376
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->e(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d;->b()Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    .line 377
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->f(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->e:Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;

    .line 378
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->g(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->f:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    .line 379
    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;Lcom/squareup/okhttp/v_1_5_1/internal/http/p$1;)V
    .locals 0

    .prologue
    .line 360
    invoke-direct {p0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;)Lcom/squareup/okhttp/v_1_5_1/internal/http/n;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->a:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    return-object v0
.end method

.method static synthetic b(Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;)Lcom/squareup/okhttp/v_1_5_1/internal/http/u;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->b:Lcom/squareup/okhttp/v_1_5_1/internal/http/u;

    return-object v0
.end method

.method static synthetic c(Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;)Lcom/squareup/okhttp/v_1_5_1/e;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->c:Lcom/squareup/okhttp/v_1_5_1/e;

    return-object v0
.end method

.method static synthetic d(Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;)Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    return-object v0
.end method

.method static synthetic e(Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->e:Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;

    return-object v0
.end method

.method static synthetic f(Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->f:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/squareup/okhttp/v_1_5_1/ResponseSource;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;
    .locals 3

    .prologue
    .line 441
    sget-object v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/l;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->b:Lcom/squareup/okhttp/v_1_5_1/internal/http/u;

    invoke-virtual {v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/u;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/squareup/okhttp/v_1_5_1/e;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;
    .locals 0

    .prologue
    .line 401
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->c:Lcom/squareup/okhttp/v_1_5_1/e;

    .line 402
    return-object p0
.end method

.method public a(Lcom/squareup/okhttp/v_1_5_1/internal/http/d;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;
    .locals 1

    .prologue
    .line 430
    invoke-virtual {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d;->b()Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    .line 431
    return-object p0
.end method

.method public a(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;
    .locals 0

    .prologue
    .line 382
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->a:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    .line 383
    return-object p0
.end method

.method public a(Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;
    .locals 0

    .prologue
    .line 435
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->e:Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;

    .line 436
    return-object p0
.end method

.method public a(Lcom/squareup/okhttp/v_1_5_1/internal/http/u;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;
    .locals 2

    .prologue
    .line 387
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "statusLine == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 388
    :cond_0
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->b:Lcom/squareup/okhttp/v_1_5_1/internal/http/u;

    .line 389
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;
    .locals 2

    .prologue
    .line 394
    :try_start_0
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/u;

    invoke-direct {v0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/u;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/u;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 395
    :catch_0
    move-exception v0

    .line 396
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    .line 411
    return-object p0
.end method

.method public a()Lcom/squareup/okhttp/v_1_5_1/internal/http/p;
    .locals 2

    .prologue
    .line 450
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->a:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "request == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 451
    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->b:Lcom/squareup/okhttp/v_1_5_1/internal/http/u;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "statusLine == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 452
    :cond_1
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;Lcom/squareup/okhttp/v_1_5_1/internal/http/p$1;)V

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;->b(Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    .line 425
    return-object p0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    .line 420
    return-object p0
.end method
