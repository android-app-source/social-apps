.class public final Lcom/squareup/okhttp/v_1_5_1/internal/http/n;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/okhttp/v_1_5_1/internal/http/n$1;,
        Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;,
        Lcom/squareup/okhttp/v_1_5_1/internal/http/n$a;,
        Lcom/squareup/okhttp/v_1_5_1/internal/http/n$c;
    }
.end annotation


# instance fields
.field private final a:Ljava/net/URL;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

.field private final d:Lcom/squareup/okhttp/v_1_5_1/internal/http/n$a;

.field private final e:Ljava/lang/Object;

.field private volatile f:Lcom/squareup/okhttp/v_1_5_1/internal/http/n$c;

.field private volatile g:Ljava/net/URI;

.field private volatile h:Lcom/squareup/okhttp/v_1_5_1/b;


# direct methods
.method private constructor <init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;)Ljava/net/URL;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->a:Ljava/net/URL;

    .line 51
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->b(Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->b:Ljava/lang/String;

    .line 52
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->c(Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;)Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;->a()Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    .line 53
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->d(Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;)Lcom/squareup/okhttp/v_1_5_1/internal/http/n$a;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/n$a;

    .line 54
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->e(Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->e(Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->e:Ljava/lang/Object;

    .line 55
    return-void

    :cond_0
    move-object v0, p0

    .line 54
    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;Lcom/squareup/okhttp/v_1_5_1/internal/http/n$1;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Ljava/net/URL;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->a:Ljava/net/URL;

    return-object v0
.end method

.method static synthetic b(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Lcom/squareup/okhttp/v_1_5_1/internal/http/n$a;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/n$a;

    return-object v0
.end method

.method static synthetic d(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->e:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic e(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Lcom/squareup/okhttp/v_1_5_1/internal/http/d;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    return-object v0
.end method

.method private m()Lcom/squareup/okhttp/v_1_5_1/internal/http/n$c;
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->f:Lcom/squareup/okhttp/v_1_5_1/internal/http/n$c;

    .line 116
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$c;

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    invoke-direct {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$c;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/d;)V

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->f:Lcom/squareup/okhttp/v_1_5_1/internal/http/n$c;

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/net/URL;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->a:Ljava/net/URL;

    return-object v0
.end method

.method public b()Ljava/net/URI;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->g:Ljava/net/URI;

    .line 64
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Llg;->a()Llg;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->a:Ljava/net/URL;

    invoke-virtual {v0, v1}, Llg;->a(Ljava/net/URL;)Ljava/net/URI;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->g:Ljava/net/URI;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 65
    :catch_0
    move-exception v0

    .line 66
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public b(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->a:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->b:Ljava/lang/String;

    return-object v0
.end method

.method public e()Lcom/squareup/okhttp/v_1_5_1/internal/http/d;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    return-object v0
.end method

.method public f()Lcom/squareup/okhttp/v_1_5_1/internal/http/n$a;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/n$a;

    return-object v0
.end method

.method public g()Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;
    .locals 2

    .prologue
    .line 99
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;Lcom/squareup/okhttp/v_1_5_1/internal/http/n$1;)V

    return-object v0
.end method

.method public h()Lcom/squareup/okhttp/v_1_5_1/internal/http/d;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->m()Lcom/squareup/okhttp/v_1_5_1/internal/http/n$c;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$c;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/n$c;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->m()Lcom/squareup/okhttp/v_1_5_1/internal/http/n$c;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$c;->b(Lcom/squareup/okhttp/v_1_5_1/internal/http/n$c;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public k()Lcom/squareup/okhttp/v_1_5_1/b;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->h:Lcom/squareup/okhttp/v_1_5_1/b;

    .line 125
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/b;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/d;)Lcom/squareup/okhttp/v_1_5_1/b;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->h:Lcom/squareup/okhttp/v_1_5_1/b;

    goto :goto_0
.end method

.method public l()Z
    .locals 2

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->a()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "https"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
