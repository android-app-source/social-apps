.class public final Lcom/squareup/okhttp/v_1_5_1/internal/http/p;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/okhttp/v_1_5_1/internal/http/p$1;,
        Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;,
        Lcom/squareup/okhttp/v_1_5_1/internal/http/p$c;,
        Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

.field private final b:Lcom/squareup/okhttp/v_1_5_1/internal/http/u;

.field private final c:Lcom/squareup/okhttp/v_1_5_1/e;

.field private final d:Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

.field private final e:Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;

.field private final f:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

.field private volatile g:Lcom/squareup/okhttp/v_1_5_1/internal/http/p$c;

.field private volatile h:Lcom/squareup/okhttp/v_1_5_1/b;


# direct methods
.method private constructor <init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;)Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->a:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    .line 61
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->b(Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;)Lcom/squareup/okhttp/v_1_5_1/internal/http/u;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->b:Lcom/squareup/okhttp/v_1_5_1/internal/http/u;

    .line 62
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->c(Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;)Lcom/squareup/okhttp/v_1_5_1/e;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->c:Lcom/squareup/okhttp/v_1_5_1/e;

    .line 63
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->d(Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;)Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;->a()Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    .line 64
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->e(Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->e:Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;

    .line 65
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->f(Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->f:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    .line 66
    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;Lcom/squareup/okhttp/v_1_5_1/internal/http/p$1;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;)V

    return-void
.end method

.method static synthetic b(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)Lcom/squareup/okhttp/v_1_5_1/internal/http/n;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->a:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    return-object v0
.end method

.method static synthetic c(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)Lcom/squareup/okhttp/v_1_5_1/internal/http/u;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->b:Lcom/squareup/okhttp/v_1_5_1/internal/http/u;

    return-object v0
.end method

.method static synthetic d(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)Lcom/squareup/okhttp/v_1_5_1/e;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->c:Lcom/squareup/okhttp/v_1_5_1/e;

    return-object v0
.end method

.method static synthetic e(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)Lcom/squareup/okhttp/v_1_5_1/internal/http/d;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    return-object v0
.end method

.method static synthetic f(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->e:Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;

    return-object v0
.end method

.method static synthetic g(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->f:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    return-object v0
.end method

.method private m()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$c;
    .locals 3

    .prologue
    .line 279
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->g:Lcom/squareup/okhttp/v_1_5_1/internal/http/p$c;

    .line 280
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$c;

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$c;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/d;Lcom/squareup/okhttp/v_1_5_1/internal/http/p$1;)V

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->g:Lcom/squareup/okhttp/v_1_5_1/internal/http/p$c;

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/squareup/okhttp/v_1_5_1/internal/http/n;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->a:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 118
    if-eqz v0, :cond_0

    move-object p2, v0

    :cond_0
    return-object p2
.end method

.method public a(Lcom/squareup/okhttp/v_1_5_1/internal/http/d;Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Z
    .locals 3

    .prologue
    .line 163
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->m()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$c;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$c;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/p$c;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 164
    invoke-virtual {p1, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {p2, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-static {v2, v0}, Llh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 166
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 175
    invoke-virtual {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->c()I

    move-result v1

    const/16 v2, 0x130

    if-ne v1, v2, :cond_1

    .line 189
    :cond_0
    :goto_0
    return v0

    .line 182
    :cond_1
    invoke-direct {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->m()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$c;

    move-result-object v1

    .line 183
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->m()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$c;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$c;->a:Ljava/util/Date;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$c;->a:Ljava/util/Date;

    if-eqz v2, :cond_2

    iget-object v1, v1, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$c;->a:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->m()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$c;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$c;->a:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 189
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->b:Lcom/squareup/okhttp/v_1_5_1/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/u;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->b:Lcom/squareup/okhttp/v_1_5_1/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/u;->c()I

    move-result v0

    return v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->b:Lcom/squareup/okhttp/v_1_5_1/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/u;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->b:Lcom/squareup/okhttp/v_1_5_1/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/u;->b()I

    move-result v0

    return v0
.end method

.method public f()Lcom/squareup/okhttp/v_1_5_1/e;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->c:Lcom/squareup/okhttp/v_1_5_1/e;

    return-object v0
.end method

.method public g()Lcom/squareup/okhttp/v_1_5_1/internal/http/d;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    return-object v0
.end method

.method public h()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->e:Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;

    return-object v0
.end method

.method public i()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;
    .locals 2

    .prologue
    .line 130
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;Lcom/squareup/okhttp/v_1_5_1/internal/http/p$1;)V

    return-object v0
.end method

.method public j()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->m()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$c;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$c;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/p$c;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 154
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->m()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$c;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$c;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/p$c;)Ljava/util/Set;

    move-result-object v0

    const-string/jumbo v1, "*"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public l()Lcom/squareup/okhttp/v_1_5_1/b;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->h:Lcom/squareup/okhttp/v_1_5_1/b;

    .line 289
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/b;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/d;)Lcom/squareup/okhttp/v_1_5_1/b;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->h:Lcom/squareup/okhttp/v_1_5_1/b;

    goto :goto_0
.end method
