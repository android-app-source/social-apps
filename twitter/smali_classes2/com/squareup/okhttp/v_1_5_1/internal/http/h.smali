.class public Lcom/squareup/okhttp/v_1_5_1/internal/http/h;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field final a:Lcom/squareup/okhttp/v_1_5_1/h;

.field b:J

.field public final c:Z

.field private d:Lcom/squareup/okhttp/v_1_5_1/c;

.field private e:Lcom/squareup/okhttp/v_1_5_1/internal/http/s;

.field private f:Lcom/squareup/okhttp/v_1_5_1/j;

.field private g:Lcom/squareup/okhttp/v_1_5_1/internal/http/v;

.field private h:Z

.field private i:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

.field private j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

.field private k:Llw;

.field private l:Llj;

.field private m:Lcom/squareup/okhttp/v_1_5_1/ResponseSource;

.field private n:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

.field private o:Llx;

.field private p:Llx;

.field private q:Ljava/io/InputStream;

.field private r:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

.field private s:Ljava/net/CacheRequest;


# direct methods
.method public constructor <init>(Lcom/squareup/okhttp/v_1_5_1/h;Lcom/squareup/okhttp/v_1_5_1/internal/http/n;ZLcom/squareup/okhttp/v_1_5_1/c;Lcom/squareup/okhttp/v_1_5_1/internal/http/s;Lcom/squareup/okhttp/v_1_5_1/internal/http/r;)V
    .locals 2

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->b:J

    .line 136
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    .line 137
    iput-object p2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->i:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    .line 138
    iput-object p2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    .line 139
    iput-boolean p3, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->c:Z

    .line 140
    iput-object p4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    .line 141
    iput-object p5, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->e:Lcom/squareup/okhttp/v_1_5_1/internal/http/s;

    .line 142
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Lcom/squareup/okhttp/v_1_5_1/c;->b()Lcom/squareup/okhttp/v_1_5_1/j;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->f:Lcom/squareup/okhttp/v_1_5_1/j;

    .line 143
    iput-object p6, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->k:Llw;

    .line 144
    return-void

    .line 142
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/squareup/okhttp/v_1_5_1/internal/http/s;)Lcom/squareup/okhttp/v_1_5_1/c;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 214
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/s;->a(Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/c;

    move-result-object v0

    .line 216
    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/c;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 217
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v1}, Lcom/squareup/okhttp/v_1_5_1/h;->a()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v2}, Lcom/squareup/okhttp/v_1_5_1/h;->b()I

    move-result v2

    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->w()Lcom/squareup/okhttp/v_1_5_1/l;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/okhttp/v_1_5_1/c;->a(IILcom/squareup/okhttp/v_1_5_1/l;)V

    .line 218
    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/c;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v1}, Lcom/squareup/okhttp/v_1_5_1/h;->j()Lcom/squareup/okhttp/v_1_5_1/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/okhttp/v_1_5_1/d;->b(Lcom/squareup/okhttp/v_1_5_1/c;)V

    .line 219
    :cond_0
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v1}, Lcom/squareup/okhttp/v_1_5_1/h;->l()Lcom/squareup/okhttp/v_1_5_1/k;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/c;->b()Lcom/squareup/okhttp/v_1_5_1/j;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/okhttp/v_1_5_1/k;->b(Lcom/squareup/okhttp/v_1_5_1/j;)V

    .line 224
    :cond_1
    :goto_0
    return-object v0

    .line 220
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/c;->j()Z

    move-result v1

    if-nez v1, :cond_1

    .line 221
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v1}, Lcom/squareup/okhttp/v_1_5_1/h;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/c;->b(I)V

    goto :goto_0
.end method

.method private static a(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 655
    new-instance v2, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    invoke-direct {v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;-><init>()V

    .line 657
    invoke-virtual {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->g()Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    move-result-object v3

    move v0, v1

    .line 658
    :goto_0
    invoke-virtual {v3}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d;->a()I

    move-result v4

    if-ge v0, v4, :cond_3

    .line 659
    invoke-virtual {v3, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d;->a(I)Ljava/lang/String;

    move-result-object v4

    .line 660
    invoke-virtual {v3, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d;->b(I)Ljava/lang/String;

    move-result-object v5

    .line 661
    const-string/jumbo v6, "Warning"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string/jumbo v6, "1"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 658
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 664
    :cond_1
    invoke-static {v4}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {p1, v4}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_0

    .line 665
    :cond_2
    invoke-virtual {v2, v4, v5}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    goto :goto_1

    .line 669
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->g()Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    move-result-object v0

    .line 670
    :goto_2
    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d;->a()I

    move-result v3

    if-ge v1, v3, :cond_5

    .line 671
    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 672
    invoke-static {v3}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 673
    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    .line 670
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 677
    :cond_5
    invoke-virtual {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->i()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;

    move-result-object v0

    invoke-virtual {v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;->a()Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/d;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->a()Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/net/URL;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 564
    invoke-static {p0}, Llh;->a(Ljava/net/URL;)I

    move-result v0

    invoke-virtual {p0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Llh;->a(Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/net/URL;->getPort()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Llx;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 471
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->o:Llx;

    .line 472
    iget-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->h:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "gzip"

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->n:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    const-string/jumbo v2, "Content-Encoding"

    invoke-virtual {v1, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 473
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->n:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->i()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;

    move-result-object v0

    const-string/jumbo v1, "Content-Encoding"

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->b(Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;

    move-result-object v0

    const-string/jumbo v1, "Content-Length"

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->b(Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->a()Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->n:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    .line 477
    new-instance v0, Llo;

    invoke-direct {v0, p1}, Llo;-><init>(Llx;)V

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->p:Llx;

    .line 481
    :goto_0
    return-void

    .line 479
    :cond_0
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->p:Llx;

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 685
    const-string/jumbo v0, "Connection"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Keep-Alive"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Proxy-Authenticate"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Proxy-Authorization"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "TE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Trailers"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Transfer-Encoding"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Upgrade"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/io/IOException;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 377
    instance-of v0, p1, Ljavax/net/ssl/SSLHandshakeException;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Ljava/security/cert/CertificateException;

    if-eqz v0, :cond_0

    move v0, v1

    .line 379
    :goto_0
    instance-of v3, p1, Ljava/net/ProtocolException;

    .line 380
    if-nez v0, :cond_1

    if-nez v3, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 377
    goto :goto_0

    :cond_1
    move v1, v2

    .line 380
    goto :goto_1
.end method

.method public static p()Ljava/lang/String;
    .locals 2

    .prologue
    .line 559
    const-string/jumbo v0, "http.agent"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 560
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Java"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "java.version"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private r()Lcom/squareup/okhttp/v_1_5_1/internal/http/p;
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->n:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->i()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->a()Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    move-result-object v0

    return-object v0
.end method

.method private s()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 229
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->e:Lcom/squareup/okhttp/v_1_5_1/internal/http/s;

    if-nez v0, :cond_1

    .line 233
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->t()Lcom/squareup/okhttp/v_1_5_1/a;

    move-result-object v1

    .line 234
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/s;

    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->b()Ljava/net/URI;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v3}, Lcom/squareup/okhttp/v_1_5_1/h;->d()Ljava/net/ProxySelector;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v4}, Lcom/squareup/okhttp/v_1_5_1/h;->j()Lcom/squareup/okhttp/v_1_5_1/d;

    move-result-object v4

    sget-object v5, Lle;->a:Lle;

    iget-object v6, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v6}, Lcom/squareup/okhttp/v_1_5_1/h;->l()Lcom/squareup/okhttp/v_1_5_1/k;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/squareup/okhttp/v_1_5_1/internal/http/s;-><init>(Lcom/squareup/okhttp/v_1_5_1/a;Ljava/net/URI;Ljava/net/ProxySelector;Lcom/squareup/okhttp/v_1_5_1/d;Lle;Lcom/squareup/okhttp/v_1_5_1/k;)V

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->e:Lcom/squareup/okhttp/v_1_5_1/internal/http/s;

    .line 240
    :goto_0
    invoke-virtual {v1}, Lcom/squareup/okhttp/v_1_5_1/a;->d()Ljava/util/List;

    move-result-object v0

    sget-object v2, Lcom/squareup/okhttp/v_1_5_1/Protocol;->b:Lcom/squareup/okhttp/v_1_5_1/Protocol;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 241
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/h;->a(Lcom/squareup/okhttp/v_1_5_1/a;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 243
    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->e:Lcom/squareup/okhttp/v_1_5_1/internal/http/s;

    invoke-direct {p0, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/s;)Lcom/squareup/okhttp/v_1_5_1/c;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 245
    :try_start_1
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/h;->b(Lcom/squareup/okhttp/v_1_5_1/a;)V

    .line 247
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 252
    :goto_1
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/c;->b()Lcom/squareup/okhttp/v_1_5_1/j;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->f:Lcom/squareup/okhttp/v_1_5_1/j;

    .line 253
    return-void

    .line 237
    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->e:Lcom/squareup/okhttp/v_1_5_1/internal/http/s;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/s;->a()Lcom/squareup/okhttp/v_1_5_1/a;

    move-result-object v1

    goto :goto_0

    .line 245
    :catchall_0
    move-exception v0

    :try_start_2
    iget-object v3, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v3, v1}, Lcom/squareup/okhttp/v_1_5_1/h;->b(Lcom/squareup/okhttp/v_1_5_1/a;)V

    throw v0

    .line 247
    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 249
    :cond_2
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->e:Lcom/squareup/okhttp/v_1_5_1/internal/http/s;

    invoke-direct {p0, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/s;)Lcom/squareup/okhttp/v_1_5_1/c;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    goto :goto_1
.end method

.method private t()Lcom/squareup/okhttp/v_1_5_1/a;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 261
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->a()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    .line 262
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->a()Ljava/net/URL;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 263
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 264
    :cond_0
    new-instance v0, Ljava/net/UnknownHostException;

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->a()Ljava/net/URL;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 268
    :cond_1
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->l()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 269
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v2}, Lcom/squareup/okhttp/v_1_5_1/h;->g()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v3

    .line 270
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v2}, Lcom/squareup/okhttp/v_1_5_1/h;->h()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v4

    .line 272
    :goto_0
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->a()Ljava/net/URL;

    move-result-object v2

    invoke-static {v2}, Llh;->a(Ljava/net/URL;)I

    move-result v2

    .line 275
    iget-object v5, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v5}, Lcom/squareup/okhttp/v_1_5_1/h;->m()Ljava/util/List;

    move-result-object v5

    sget-object v6, Lcom/squareup/okhttp/v_1_5_1/Protocol;->b:Lcom/squareup/okhttp/v_1_5_1/Protocol;

    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 276
    iget-object v5, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    new-instance v6, Lcom/squareup/okhttp/v_1_5_1/internal/http/m;

    invoke-direct {v6, v0, v1, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/m;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v5, v6}, Lcom/squareup/okhttp/v_1_5_1/h;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/m;)Lcom/squareup/okhttp/v_1_5_1/internal/http/m;

    move-result-object v0

    .line 277
    if-eqz v0, :cond_2

    .line 278
    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/m;->a()Ljava/lang/String;

    move-result-object v1

    .line 279
    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/m;->b()I

    move-result v2

    .line 282
    :cond_2
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/a;

    iget-object v5, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v5}, Lcom/squareup/okhttp/v_1_5_1/h;->i()Lcom/squareup/okhttp/v_1_5_1/g;

    move-result-object v5

    iget-object v6, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v6}, Lcom/squareup/okhttp/v_1_5_1/h;->c()Ljava/net/Proxy;

    move-result-object v6

    iget-object v7, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v7}, Lcom/squareup/okhttp/v_1_5_1/h;->m()Ljava/util/List;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/squareup/okhttp/v_1_5_1/a;-><init>(Ljava/lang/String;ILjavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;Lcom/squareup/okhttp/v_1_5_1/g;Ljava/net/Proxy;Ljava/util/List;)V

    return-object v0

    :cond_3
    move-object v3, v4

    goto :goto_0
.end method

.method private u()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 392
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/h;->f()Lcom/squareup/okhttp/v_1_5_1/i;

    move-result-object v0

    .line 393
    if-nez v0, :cond_0

    .line 403
    :goto_0
    return-void

    .line 396
    :cond_0
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->n:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-static {v1, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/a;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 397
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-interface {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/i;->b(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Z

    goto :goto_0

    .line 402
    :cond_1
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->r()Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/i;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)Ljava/net/CacheRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->s:Ljava/net/CacheRequest;

    goto :goto_0
.end method

.method private v()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 518
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->g()Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;

    move-result-object v0

    .line 520
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->i()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 521
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->a(Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;

    .line 524
    :cond_0
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    const-string/jumbo v2, "Host"

    invoke-virtual {v1, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 525
    const-string/jumbo v1, "Host"

    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->a()Ljava/net/URL;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;

    .line 528
    :cond_1
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    invoke-virtual {v1}, Lcom/squareup/okhttp/v_1_5_1/c;->k()I

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    const-string/jumbo v2, "Connection"

    invoke-virtual {v1, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    .line 530
    const-string/jumbo v1, "Connection"

    const-string/jumbo v2, "Keep-Alive"

    invoke-virtual {v0, v1, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;

    .line 533
    :cond_3
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    const-string/jumbo v2, "Accept-Encoding"

    invoke-virtual {v1, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    .line 534
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->h:Z

    .line 535
    const-string/jumbo v1, "Accept-Encoding"

    const-string/jumbo v2, "gzip"

    invoke-virtual {v0, v1, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;

    .line 538
    :cond_4
    invoke-virtual {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->c()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    const-string/jumbo v2, "Content-Type"

    invoke-virtual {v1, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    .line 539
    const-string/jumbo v1, "Content-Type"

    const-string/jumbo v2, "application/x-www-form-urlencoded"

    invoke-virtual {v0, v1, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;

    .line 542
    :cond_5
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v1}, Lcom/squareup/okhttp/v_1_5_1/h;->e()Ljava/net/CookieHandler;

    move-result-object v1

    .line 543
    if-eqz v1, :cond_6

    .line 547
    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->a()Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->e()Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/squareup/okhttp/v_1_5_1/internal/http/l;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/d;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    .line 549
    iget-object v3, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v3}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->b()Ljava/net/URI;

    move-result-object v3

    invoke-virtual {v1, v3, v2}, Ljava/net/CookieHandler;->get(Ljava/net/URI;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    .line 552
    invoke-static {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/l;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;Ljava/util/Map;)V

    .line 555
    :cond_6
    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->a()Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    .line 556
    return-void
.end method

.method private w()Lcom/squareup/okhttp/v_1_5_1/l;
    .locals 7

    .prologue
    .line 696
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->l()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 712
    :goto_0
    return-object v0

    .line 698
    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->i()Ljava/lang/String;

    move-result-object v0

    .line 699
    if-nez v0, :cond_1

    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->p()Ljava/lang/String;

    move-result-object v0

    .line 701
    :cond_1
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->a()Ljava/net/URL;

    move-result-object v2

    .line 704
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v1}, Lcom/squareup/okhttp/v_1_5_1/h;->m()Ljava/util/List;

    move-result-object v1

    sget-object v3, Lcom/squareup/okhttp/v_1_5_1/Protocol;->b:Lcom/squareup/okhttp/v_1_5_1/Protocol;

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 705
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    new-instance v3, Lcom/squareup/okhttp/v_1_5_1/internal/http/m;

    invoke-virtual {v2}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2}, Llh;->a(Ljava/net/URL;)I

    move-result v6

    invoke-direct {v3, v4, v5, v6}, Lcom/squareup/okhttp/v_1_5_1/internal/http/m;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v1, v3}, Lcom/squareup/okhttp/v_1_5_1/h;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/m;)Lcom/squareup/okhttp/v_1_5_1/internal/http/m;

    move-result-object v3

    .line 707
    if-eqz v3, :cond_2

    .line 708
    new-instance v1, Lcom/squareup/okhttp/v_1_5_1/l;

    invoke-virtual {v3}, Lcom/squareup/okhttp/v_1_5_1/internal/http/m;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lcom/squareup/okhttp/v_1_5_1/internal/http/m;->b()I

    move-result v3

    iget-object v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v4}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->j()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/squareup/okhttp/v_1_5_1/l;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    .line 712
    :cond_2
    new-instance v1, Lcom/squareup/okhttp/v_1_5_1/l;

    invoke-virtual {v2}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2}, Llh;->a(Ljava/net/URL;)I

    move-result v2

    iget-object v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v4}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->j()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v2, v0, v4}, Lcom/squareup/okhttp/v_1_5_1/l;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/io/IOException;)Lcom/squareup/okhttp/v_1_5_1/internal/http/h;
    .locals 7

    .prologue
    .line 355
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->e:Lcom/squareup/okhttp/v_1_5_1/internal/http/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->e:Lcom/squareup/okhttp/v_1_5_1/internal/http/s;

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/s;->a(Lcom/squareup/okhttp/v_1_5_1/c;Ljava/io/IOException;)V

    .line 359
    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->k:Llw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->k:Llw;

    instance-of v0, v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/r;

    if-eqz v0, :cond_5

    :cond_1
    const/4 v0, 0x1

    .line 360
    :goto_0
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->e:Lcom/squareup/okhttp/v_1_5_1/internal/http/s;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    if-eqz v1, :cond_4

    :cond_2
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->e:Lcom/squareup/okhttp/v_1_5_1/internal/http/s;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->e:Lcom/squareup/okhttp/v_1_5_1/internal/http/s;

    invoke-virtual {v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/s;->b()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    invoke-direct {p0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->b(Ljava/io/IOException;)Z

    move-result v1

    if-eqz v1, :cond_4

    if-nez v0, :cond_6

    .line 364
    :cond_4
    const/4 v0, 0x0

    .line 370
    :goto_1
    return-object v0

    .line 359
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 367
    :cond_6
    invoke-virtual {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->n()Lcom/squareup/okhttp/v_1_5_1/c;

    move-result-object v4

    .line 370
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->i:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    iget-boolean v3, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->c:Z

    iget-object v5, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->e:Lcom/squareup/okhttp/v_1_5_1/internal/http/s;

    iget-object v6, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->k:Llw;

    check-cast v6, Lcom/squareup/okhttp/v_1_5_1/internal/http/r;

    invoke-direct/range {v0 .. v6}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;-><init>(Lcom/squareup/okhttp/v_1_5_1/h;Lcom/squareup/okhttp/v_1_5_1/internal/http/n;ZLcom/squareup/okhttp/v_1_5_1/c;Lcom/squareup/okhttp/v_1_5_1/internal/http/s;Lcom/squareup/okhttp/v_1_5_1/internal/http/r;)V

    goto :goto_1
.end method

.method public final a()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 152
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->m:Lcom/squareup/okhttp/v_1_5_1/ResponseSource;

    if-eqz v0, :cond_1

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->g:Lcom/squareup/okhttp/v_1_5_1/internal/http/v;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 155
    :cond_2
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->v()V

    .line 156
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/h;->f()Lcom/squareup/okhttp/v_1_5_1/i;

    move-result-object v2

    .line 158
    if-eqz v2, :cond_7

    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-interface {v2, v0}, Lcom/squareup/okhttp/v_1_5_1/i;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    move-result-object v0

    .line 161
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 162
    new-instance v3, Lcom/squareup/okhttp/v_1_5_1/internal/http/a$a;

    iget-object v6, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-direct {v3, v4, v5, v6, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/a$a;-><init>(JLcom/squareup/okhttp/v_1_5_1/internal/http/n;Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)V

    invoke-virtual {v3}, Lcom/squareup/okhttp/v_1_5_1/internal/http/a$a;->a()Lcom/squareup/okhttp/v_1_5_1/internal/http/a;

    move-result-object v3

    .line 163
    iget-object v4, v3, Lcom/squareup/okhttp/v_1_5_1/internal/http/a;->c:Lcom/squareup/okhttp/v_1_5_1/ResponseSource;

    iput-object v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->m:Lcom/squareup/okhttp/v_1_5_1/ResponseSource;

    .line 164
    iget-object v4, v3, Lcom/squareup/okhttp/v_1_5_1/internal/http/a;->a:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    iput-object v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    .line 166
    if-eqz v2, :cond_3

    .line 167
    iget-object v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->m:Lcom/squareup/okhttp/v_1_5_1/ResponseSource;

    invoke-interface {v2, v4}, Lcom/squareup/okhttp/v_1_5_1/i;->a(Lcom/squareup/okhttp/v_1_5_1/ResponseSource;)V

    .line 170
    :cond_3
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->m:Lcom/squareup/okhttp/v_1_5_1/ResponseSource;

    sget-object v4, Lcom/squareup/okhttp/v_1_5_1/ResponseSource;->c:Lcom/squareup/okhttp/v_1_5_1/ResponseSource;

    if-eq v2, v4, :cond_4

    .line 171
    iget-object v2, v3, Lcom/squareup/okhttp/v_1_5_1/internal/http/a;->b:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    iput-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->r:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    .line 174
    :cond_4
    if-eqz v0, :cond_5

    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->m:Lcom/squareup/okhttp/v_1_5_1/ResponseSource;

    invoke-virtual {v2}, Lcom/squareup/okhttp/v_1_5_1/ResponseSource;->b()Z

    move-result v2

    if-nez v2, :cond_5

    .line 175
    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->h()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;

    move-result-object v0

    invoke-static {v0}, Llh;->a(Ljava/io/Closeable;)V

    .line 178
    :cond_5
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->m:Lcom/squareup/okhttp/v_1_5_1/ResponseSource;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/ResponseSource;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 180
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    if-nez v0, :cond_6

    .line 181
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->s()V

    .line 184
    :cond_6
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    invoke-virtual {v0, p0}, Lcom/squareup/okhttp/v_1_5_1/c;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/h;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/v;

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->g:Lcom/squareup/okhttp/v_1_5_1/internal/http/v;

    .line 188
    invoke-virtual {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->k:Llw;

    if-nez v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->g:Lcom/squareup/okhttp/v_1_5_1/internal/http/v;

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-interface {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/v;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Llw;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->k:Llw;

    goto/16 :goto_0

    :cond_7
    move-object v0, v1

    .line 158
    goto :goto_1

    .line 194
    :cond_8
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    if-eqz v0, :cond_9

    .line 195
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/h;->j()Lcom/squareup/okhttp/v_1_5_1/d;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    invoke-virtual {v0, v2}, Lcom/squareup/okhttp/v_1_5_1/d;->a(Lcom/squareup/okhttp/v_1_5_1/c;)V

    .line 196
    iput-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    .line 200
    :cond_9
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->r:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->n:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    .line 201
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->r:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->h()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->r:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->h()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;->b()Llx;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a(Llx;)V

    goto/16 :goto_0
.end method

.method public a(Lcom/squareup/okhttp/v_1_5_1/internal/http/d;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 717
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/h;->e()Ljava/net/CookieHandler;

    move-result-object v0

    .line 718
    if-eqz v0, :cond_0

    .line 719
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->b()Ljava/net/URI;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p1, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/l;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/d;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/net/CookieHandler;->put(Ljava/net/URI;Ljava/util/Map;)V

    .line 721
    :cond_0
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    .line 291
    iget-wide v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->b:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 292
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->b:J

    .line 293
    return-void
.end method

.method c()Z
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/i;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final d()Llw;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->m:Lcom/squareup/okhttp/v_1_5_1/ResponseSource;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->k:Llw;

    return-object v0
.end method

.method public final e()Llj;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->l:Llj;

    .line 307
    if-eqz v0, :cond_0

    .line 309
    :goto_0
    return-object v0

    .line 308
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d()Llw;

    move-result-object v0

    .line 309
    if-eqz v0, :cond_1

    invoke-static {v0}, Llr;->a(Llw;)Llj;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->l:Llj;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->n:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Lcom/squareup/okhttp/v_1_5_1/internal/http/n;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    return-object v0
.end method

.method public final h()Lcom/squareup/okhttp/v_1_5_1/internal/http/p;
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->n:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 330
    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->n:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    return-object v0
.end method

.method public final i()Llx;
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->n:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->p:Llx;

    return-object v0
.end method

.method public final j()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->q:Ljava/io/InputStream;

    .line 340
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->i()Llx;

    move-result-object v0

    invoke-static {v0}, Llr;->a(Llx;)Llk;

    move-result-object v0

    invoke-interface {v0}, Llk;->k()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->q:Ljava/io/InputStream;

    goto :goto_0
.end method

.method public final k()Lcom/squareup/okhttp/v_1_5_1/c;
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    return-object v0
.end method

.method public l()Lcom/squareup/okhttp/v_1_5_1/j;
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->f:Lcom/squareup/okhttp/v_1_5_1/j;

    return-object v0
.end method

.method public final m()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 411
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->g:Lcom/squareup/okhttp/v_1_5_1/internal/http/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    if-eqz v0, :cond_0

    .line 412
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->g:Lcom/squareup/okhttp/v_1_5_1/internal/http/v;

    invoke-interface {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/v;->c()V

    .line 414
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    .line 415
    return-void
.end method

.method public final n()Lcom/squareup/okhttp/v_1_5_1/c;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 422
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->l:Llj;

    if-eqz v1, :cond_1

    .line 424
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->l:Llj;

    invoke-static {v1}, Llh;->a(Ljava/io/Closeable;)V

    .line 430
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->p:Llx;

    if-nez v1, :cond_2

    .line 431
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    invoke-static {v1}, Llh;->a(Ljava/io/Closeable;)V

    .line 432
    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    .line 451
    :goto_1
    return-object v0

    .line 425
    :cond_1
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->k:Llw;

    if-eqz v1, :cond_0

    .line 426
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->k:Llw;

    invoke-static {v1}, Llh;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 437
    :cond_2
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->p:Llx;

    invoke-static {v1}, Llh;->a(Ljava/io/Closeable;)V

    .line 440
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->q:Ljava/io/InputStream;

    invoke-static {v1}, Llh;->a(Ljava/io/Closeable;)V

    .line 443
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->g:Lcom/squareup/okhttp/v_1_5_1/internal/http/v;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->g:Lcom/squareup/okhttp/v_1_5_1/internal/http/v;

    invoke-interface {v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/v;->d()Z

    move-result v1

    if-nez v1, :cond_3

    .line 444
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    invoke-static {v1}, Llh;->a(Ljava/io/Closeable;)V

    .line 445
    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    goto :goto_1

    .line 449
    :cond_3
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    .line 450
    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    move-object v0, v1

    .line 451
    goto :goto_1
.end method

.method public final o()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 489
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->d()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "HEAD"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 508
    :cond_0
    :goto_0
    return v0

    .line 493
    :cond_1
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->n:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    invoke-virtual {v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->c()I

    move-result v2

    .line 494
    const/16 v3, 0x64

    if-lt v2, v3, :cond_2

    const/16 v3, 0xc8

    if-lt v2, v3, :cond_3

    :cond_2
    const/16 v3, 0xcc

    if-eq v2, v3, :cond_3

    const/16 v3, 0x130

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 497
    goto :goto_0

    .line 503
    :cond_3
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->n:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    invoke-static {v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/l;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    const-string/jumbo v2, "chunked"

    iget-object v3, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->n:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    const-string/jumbo v4, "Transfer-Encoding"

    invoke-virtual {v3, v4}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    .line 505
    goto :goto_0
.end method

.method public final q()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, -0x1

    .line 574
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->n:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    if-eqz v0, :cond_1

    .line 648
    :cond_0
    :goto_0
    return-void

    .line 575
    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->m:Lcom/squareup/okhttp/v_1_5_1/ResponseSource;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "call sendRequest() first!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 576
    :cond_2
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->m:Lcom/squareup/okhttp/v_1_5_1/ResponseSource;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/ResponseSource;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 579
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->l:Llj;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->l:Llj;

    invoke-interface {v0}, Llj;->b()Llq;

    move-result-object v0

    invoke-virtual {v0}, Llq;->l()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    .line 580
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->l:Llj;

    invoke-interface {v0}, Llj;->a()V

    .line 583
    :cond_3
    iget-wide v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->b:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_5

    .line 584
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/l;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->k:Llw;

    instance-of v0, v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/r;

    if-eqz v0, :cond_4

    .line 586
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->k:Llw;

    check-cast v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/r;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/r;->b()J

    move-result-wide v0

    .line 587
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->g()Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;

    move-result-object v2

    const-string/jumbo v3, "Content-Length"

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->a()Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    .line 591
    :cond_4
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->g:Lcom/squareup/okhttp/v_1_5_1/internal/http/v;

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-interface {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/v;->b(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)V

    .line 594
    :cond_5
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->k:Llw;

    if-eqz v0, :cond_6

    .line 595
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->l:Llj;

    if-eqz v0, :cond_7

    .line 597
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->l:Llj;

    invoke-interface {v0}, Llj;->close()V

    .line 601
    :goto_1
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->k:Llw;

    instance-of v0, v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/r;

    if-eqz v0, :cond_6

    .line 602
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->g:Lcom/squareup/okhttp/v_1_5_1/internal/http/v;

    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->k:Llw;

    check-cast v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/r;

    invoke-interface {v1, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/v;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/r;)V

    .line 606
    :cond_6
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->g:Lcom/squareup/okhttp/v_1_5_1/internal/http/v;

    invoke-interface {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/v;->a()V

    .line 608
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->g:Lcom/squareup/okhttp/v_1_5_1/internal/http/v;

    invoke-interface {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/v;->b()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->j:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    invoke-virtual {v1}, Lcom/squareup/okhttp/v_1_5_1/c;->i()Lcom/squareup/okhttp/v_1_5_1/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->a(Lcom/squareup/okhttp/v_1_5_1/e;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;

    move-result-object v0

    sget-object v1, Lcom/squareup/okhttp/v_1_5_1/internal/http/l;->b:Ljava/lang/String;

    iget-wide v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;

    move-result-object v0

    sget-object v1, Lcom/squareup/okhttp/v_1_5_1/internal/http/l;->c:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->m:Lcom/squareup/okhttp/v_1_5_1/ResponseSource;

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->a(Lcom/squareup/okhttp/v_1_5_1/ResponseSource;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->a()Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->n:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    .line 615
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->d:Lcom/squareup/okhttp/v_1_5_1/c;

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->n:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    invoke-virtual {v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/c;->a(I)V

    .line 616
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->n:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->g()Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/d;)V

    .line 618
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->m:Lcom/squareup/okhttp/v_1_5_1/ResponseSource;

    sget-object v1, Lcom/squareup/okhttp/v_1_5_1/ResponseSource;->b:Lcom/squareup/okhttp/v_1_5_1/ResponseSource;

    if-ne v0, v1, :cond_9

    .line 619
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->r:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->n:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 620
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->g:Lcom/squareup/okhttp/v_1_5_1/internal/http/v;

    invoke-interface {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/v;->e()V

    .line 621
    invoke-virtual {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->m()V

    .line 622
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->r:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->n:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    invoke-static {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->n:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    .line 626
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a:Lcom/squareup/okhttp/v_1_5_1/h;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/h;->f()Lcom/squareup/okhttp/v_1_5_1/i;

    move-result-object v0

    .line 627
    invoke-interface {v0}, Lcom/squareup/okhttp/v_1_5_1/i;->b()V

    .line 628
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->r:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->r()Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/okhttp/v_1_5_1/i;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)V

    .line 630
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->r:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->h()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 631
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->r:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->h()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;->b()Llx;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a(Llx;)V

    goto/16 :goto_0

    .line 599
    :cond_7
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->k:Llw;

    invoke-interface {v0}, Llw;->close()V

    goto/16 :goto_1

    .line 635
    :cond_8
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->r:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->h()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;

    move-result-object v0

    invoke-static {v0}, Llh;->a(Ljava/io/Closeable;)V

    .line 639
    :cond_9
    invoke-virtual {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->o()Z

    move-result v0

    if-nez v0, :cond_a

    .line 641
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->g:Lcom/squareup/okhttp/v_1_5_1/internal/http/v;

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->s:Ljava/net/CacheRequest;

    invoke-interface {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/v;->a(Ljava/net/CacheRequest;)Llx;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->o:Llx;

    .line 642
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->o:Llx;

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->p:Llx;

    goto/16 :goto_0

    .line 646
    :cond_a
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->u()V

    .line 647
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->g:Lcom/squareup/okhttp/v_1_5_1/internal/http/v;

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->s:Ljava/net/CacheRequest;

    invoke-interface {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/v;->a(Ljava/net/CacheRequest;)Llx;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/h;->a(Llx;)V

    goto/16 :goto_0
.end method
