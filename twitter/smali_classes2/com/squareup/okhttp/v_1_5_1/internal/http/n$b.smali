.class public Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/okhttp/v_1_5_1/internal/http/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field private a:Ljava/net/URL;

.field private b:Ljava/lang/String;

.field private c:Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

.field private d:Lcom/squareup/okhttp/v_1_5_1/internal/http/n$a;

.field private e:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 242
    const-string/jumbo v0, "GET"

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->b:Ljava/lang/String;

    .line 243
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    invoke-direct {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    .line 244
    return-void
.end method

.method private constructor <init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)V
    .locals 1

    .prologue
    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Ljava/net/URL;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->a:Ljava/net/URL;

    .line 248
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->b(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->b:Ljava/lang/String;

    .line 249
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->c(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Lcom/squareup/okhttp/v_1_5_1/internal/http/n$a;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/n$a;

    .line 250
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->d(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->e:Ljava/lang/Object;

    .line 251
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->e(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d;->b()Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    .line 252
    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;Lcom/squareup/okhttp/v_1_5_1/internal/http/n$1;)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;)Ljava/net/URL;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->a:Ljava/net/URL;

    return-object v0
.end method

.method static synthetic b(Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;)Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    return-object v0
.end method

.method static synthetic d(Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;)Lcom/squareup/okhttp/v_1_5_1/internal/http/n$a;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/n$a;

    return-object v0
.end method

.method static synthetic e(Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->e:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;
    .locals 1

    .prologue
    .line 298
    const-string/jumbo v0, "User-Agent"

    invoke-virtual {p0, v0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Lcom/squareup/okhttp/v_1_5_1/internal/http/n$a;)Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;
    .locals 2

    .prologue
    .line 318
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 319
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "method == null || method.length() == 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 321
    :cond_1
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->b:Ljava/lang/String;

    .line 322
    iput-object p2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->d:Lcom/squareup/okhttp/v_1_5_1/internal/http/n$a;

    .line 323
    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    .line 274
    return-object p0
.end method

.method public a(Ljava/net/URL;)Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;
    .locals 2

    .prologue
    .line 263
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "url == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 264
    :cond_0
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->a:Ljava/net/URL;

    .line 265
    return-object p0
.end method

.method public a()Lcom/squareup/okhttp/v_1_5_1/internal/http/n;
    .locals 2

    .prologue
    .line 337
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->a:Ljava/net/URL;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "url == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 338
    :cond_0
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;Lcom/squareup/okhttp/v_1_5_1/internal/http/n$1;)V

    return-object v0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/n$b;->c:Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/v_1_5_1/internal/http/d$a;

    .line 283
    return-object p0
.end method
