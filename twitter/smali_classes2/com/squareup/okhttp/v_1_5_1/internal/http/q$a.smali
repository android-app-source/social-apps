.class final Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;
.super Ljava/net/HttpURLConnection;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/okhttp/v_1_5_1/internal/http/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

.field private final b:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;


# direct methods
.method public constructor <init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 249
    invoke-virtual {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->a()Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->a()Ljava/net/URL;

    move-result-object v1

    invoke-direct {p0, v1}, Ljava/net/HttpURLConnection;-><init>(Ljava/net/URL;)V

    .line 250
    invoke-virtual {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->a()Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;->a:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    .line 251
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;->b:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    .line 254
    iput-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;->connected:Z

    .line 255
    invoke-virtual {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->h()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;->doOutput:Z

    .line 258
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;->a:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;->method:Ljava/lang/String;

    .line 259
    return-void

    .line 255
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;->b:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    return-object v0
.end method


# virtual methods
.method public addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 282
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->a()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public connect()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 265
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->a()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public disconnect()V
    .locals 1

    .prologue
    .line 270
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->a()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public getAllowUserInteraction()Z
    .locals 1

    .prologue
    .line 464
    const/4 v0, 0x0

    return v0
.end method

.method public getConnectTimeout()I
    .locals 1

    .prologue
    .line 403
    const/4 v0, 0x0

    return v0
.end method

.method public getContent()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 419
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->d()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public getContent([Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 424
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->d()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public getDefaultUseCaches()Z
    .locals 1

    .prologue
    .line 489
    invoke-super {p0}, Ljava/net/HttpURLConnection;->getDefaultUseCaches()Z

    move-result v0

    return v0
.end method

.method public getDoInput()Z
    .locals 1

    .prologue
    .line 444
    const/4 v0, 0x1

    return v0
.end method

.method public getDoOutput()Z
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;->a:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->f()Lcom/squareup/okhttp/v_1_5_1/internal/http/n$a;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getErrorStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 381
    const/4 v0, 0x0

    return-object v0
.end method

.method public getHeaderField(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 350
    if-gez p1, :cond_0

    .line 351
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid header index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 353
    :cond_0
    if-nez p1, :cond_1

    .line 354
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;->b:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->b()Ljava/lang/String;

    move-result-object v0

    .line 356
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;->b:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->g()Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    move-result-object v0

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getHeaderField(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 361
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;->b:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;->b:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->g()Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getHeaderFieldKey(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 338
    if-gez p1, :cond_0

    .line 339
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid header index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 341
    :cond_0
    if-nez p1, :cond_1

    .line 342
    const/4 v0, 0x0

    .line 344
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;->b:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->g()Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    move-result-object v0

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/d;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getHeaderFields()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 366
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;->b:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->g()Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;->b:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    invoke-virtual {v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/l;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/d;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getIfModifiedSince()J
    .locals 2

    .prologue
    .line 484
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 429
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->d()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public getInstanceFollowRedirects()Z
    .locals 1

    .prologue
    .line 320
    invoke-super {p0}, Ljava/net/HttpURLConnection;->getInstanceFollowRedirects()Z

    move-result v0

    return v0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 434
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->a()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public getReadTimeout()I
    .locals 1

    .prologue
    .line 414
    const/4 v0, 0x0

    return v0
.end method

.method public getRequestMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;->a:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRequestProperties()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 294
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->c()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public getRequestProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;->a:Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResponseCode()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 371
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;->b:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->c()I

    move-result v0

    return v0
.end method

.method public getResponseMessage()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 376
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/q$a;->b:Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUseCaches()Z
    .locals 1

    .prologue
    .line 474
    invoke-super {p0}, Ljava/net/HttpURLConnection;->getUseCaches()Z

    move-result v0

    return v0
.end method

.method public setAllowUserInteraction(Z)V
    .locals 1

    .prologue
    .line 459
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->a()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setChunkedStreamingMode(I)V
    .locals 1

    .prologue
    .line 309
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->a()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setConnectTimeout(I)V
    .locals 1

    .prologue
    .line 397
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->a()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setDefaultUseCaches(Z)V
    .locals 0

    .prologue
    .line 494
    invoke-super {p0, p1}, Ljava/net/HttpURLConnection;->setDefaultUseCaches(Z)V

    .line 495
    return-void
.end method

.method public setDoInput(Z)V
    .locals 1

    .prologue
    .line 439
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->a()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setDoOutput(Z)V
    .locals 1

    .prologue
    .line 449
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->a()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setFixedLengthStreamingMode(I)V
    .locals 1

    .prologue
    .line 299
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->a()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setFixedLengthStreamingMode(J)V
    .locals 1

    .prologue
    .line 304
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->a()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setIfModifiedSince(J)V
    .locals 1

    .prologue
    .line 479
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->a()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setInstanceFollowRedirects(Z)V
    .locals 1

    .prologue
    .line 314
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->a()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setReadTimeout(I)V
    .locals 1

    .prologue
    .line 408
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->a()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setRequestMethod(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/ProtocolException;
        }
    .end annotation

    .prologue
    .line 325
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->a()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 277
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->a()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setUseCaches(Z)V
    .locals 1

    .prologue
    .line 469
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;->a()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public usingProxy()Z
    .locals 1

    .prologue
    .line 390
    const/4 v0, 0x0

    return v0
.end method
