.class final Lcom/squareup/okhttp/v_1_5_1/internal/http/f$d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Llw;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/okhttp/v_1_5_1/internal/http/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

.field private b:Z

.field private c:J


# direct methods
.method private constructor <init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;J)V
    .locals 0

    .prologue
    .line 242
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$d;->a:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 243
    iput-wide p2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$d;->c:J

    .line 244
    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;JLcom/squareup/okhttp/v_1_5_1/internal/http/f$1;)V
    .locals 0

    .prologue
    .line 238
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$d;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;J)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 262
    iget-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$d;->b:Z

    if-eqz v0, :cond_0

    .line 264
    :goto_0
    return-void

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$d;->a:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;)Llj;

    move-result-object v0

    invoke-interface {v0}, Llj;->a()V

    goto :goto_0
.end method

.method public a(Llq;J)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 251
    iget-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$d;->b:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 252
    :cond_0
    invoke-virtual {p1}, Llq;->l()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Llh;->a(JJJ)V

    .line 253
    iget-wide v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$d;->c:J

    cmp-long v0, p2, v0

    if-lez v0, :cond_1

    .line 254
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "expected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$d;->c:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " bytes but received "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 257
    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$d;->a:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;)Llj;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Llj;->a(Llq;J)V

    .line 258
    iget-wide v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$d;->c:J

    sub-long/2addr v0, p2

    iput-wide v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$d;->c:J

    .line 259
    return-void
.end method

.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 267
    iget-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$d;->b:Z

    if-eqz v0, :cond_0

    .line 271
    :goto_0
    return-void

    .line 268
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$d;->b:Z

    .line 269
    iget-wide v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$d;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    new-instance v0, Ljava/net/ProtocolException;

    const-string/jumbo v1, "unexpected end of stream"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 270
    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f$d;->a:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/f;I)I

    goto :goto_0
.end method
