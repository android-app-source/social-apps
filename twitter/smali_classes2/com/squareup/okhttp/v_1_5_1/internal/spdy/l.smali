.class public final Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$b;,
        Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;
    }
.end annotation


# static fields
.field static final synthetic k:Z

.field private static final l:Ljava/util/concurrent/ExecutorService;


# instance fields
.field final a:Lcom/squareup/okhttp/v_1_5_1/Protocol;

.field final b:Z

.field c:J

.field d:J

.field final e:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/j;

.field final f:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/j;

.field final g:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/a;

.field final h:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;

.field final i:J

.field final j:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$b;

.field private final m:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/f;

.field private final n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/okhttp/v_1_5_1/internal/spdy/m;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Ljava/lang/String;

.field private p:I

.field private q:I

.field private r:J

.field private volatile s:Z

.field private t:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/okhttp/v_1_5_1/internal/spdy/h;",
            ">;"
        }
    .end annotation
.end field

.field private final u:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/i;

.field private v:I

.field private w:Z

.field private final x:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 53
    const-class v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v8

    :goto_0
    sput-boolean v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->k:Z

    .line 67
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const v3, 0x7fffffff

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v7}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    const-string/jumbo v0, "OkHttp SpdyConnection"

    invoke-static {v0, v8}, Llh;->a(Ljava/lang/String;Z)Ljava/util/concurrent/ThreadFactory;

    move-result-object v8

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    sput-object v1, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->l:Ljava/util/concurrent/ExecutorService;

    return-void

    :cond_0
    move v0, v2

    .line 53
    goto :goto_0
.end method

.method private constructor <init>(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->n:Ljava/util/Map;

    .line 86
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->r:J

    .line 100
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->c:J

    .line 111
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/j;

    invoke-direct {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/j;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->e:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/j;

    .line 116
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/j;

    invoke-direct {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/j;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->f:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/j;

    .line 118
    iput-boolean v3, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->w:Z

    .line 767
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->x:Ljava/util/Set;

    .line 127
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->a(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;)Lcom/squareup/okhttp/v_1_5_1/Protocol;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->a:Lcom/squareup/okhttp/v_1_5_1/Protocol;

    .line 128
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->b(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;)Lcom/squareup/okhttp/v_1_5_1/internal/spdy/i;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->u:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/i;

    .line 129
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->c(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->b:Z

    .line 130
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->d(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;)Lcom/squareup/okhttp/v_1_5_1/internal/spdy/f;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->m:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/f;

    .line 131
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->c(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->q:I

    .line 132
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->c(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_1
    iput v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->v:I

    .line 138
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->c(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->e:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/j;

    const/4 v1, 0x7

    const/high16 v2, 0x1000000

    invoke-virtual {v0, v1, v3, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/j;->a(III)Lcom/squareup/okhttp/v_1_5_1/internal/spdy/j;

    .line 142
    :cond_0
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->e(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->o:Ljava/lang/String;

    .line 145
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->a:Lcom/squareup/okhttp/v_1_5_1/Protocol;

    sget-object v1, Lcom/squareup/okhttp/v_1_5_1/Protocol;->a:Lcom/squareup/okhttp/v_1_5_1/Protocol;

    if-ne v0, v1, :cond_3

    .line 146
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e;

    invoke-direct {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e;-><init>()V

    .line 152
    :goto_2
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->f:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/j;

    const/high16 v2, 0x10000

    invoke-virtual {v1, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/j;->d(I)I

    move-result v1

    int-to-long v2, v1

    iput-wide v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->d:J

    .line 153
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->f(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;)Llk;

    move-result-object v1

    iget-boolean v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->b:Z

    invoke-interface {v0, v1, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/n;->a(Llk;Z)Lcom/squareup/okhttp/v_1_5_1/internal/spdy/a;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->g:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/a;

    .line 154
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->g(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;)Llj;

    move-result-object v1

    iget-boolean v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->b:Z

    invoke-interface {v0, v1, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/n;->a(Llj;Z)Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->h:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;

    .line 155
    invoke-interface {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/n;->a()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->i:J

    .line 157
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$b;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$1;)V

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->j:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$b;

    .line 158
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->j:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$b;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 159
    return-void

    :cond_1
    move v0, v2

    .line 131
    goto :goto_0

    :cond_2
    move v1, v2

    .line 132
    goto :goto_1

    .line 147
    :cond_3
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->a:Lcom/squareup/okhttp/v_1_5_1/Protocol;

    sget-object v1, Lcom/squareup/okhttp/v_1_5_1/Protocol;->b:Lcom/squareup/okhttp/v_1_5_1/Protocol;

    if-ne v0, v1, :cond_4

    .line 148
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/k;

    invoke-direct {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/k;-><init>()V

    goto :goto_2

    .line 150
    :cond_4
    new-instance v0, Ljava/lang/AssertionError;

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->a:Lcom/squareup/okhttp/v_1_5_1/Protocol;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method synthetic constructor <init>(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$1;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;)V

    return-void
.end method

.method private a(ILjava/util/List;ZZ)Lcom/squareup/okhttp/v_1_5_1/internal/spdy/m;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;",
            ">;ZZ)",
            "Lcom/squareup/okhttp/v_1_5_1/internal/spdy/m;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 233
    if-nez p3, :cond_0

    const/4 v5, 0x1

    .line 234
    :goto_0
    if-nez p4, :cond_1

    const/4 v6, 0x1

    .line 235
    :goto_1
    const/4 v7, -0x1

    .line 236
    const/4 v14, 0x0

    .line 240
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->h:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;

    move-object/from16 v16, v0

    monitor-enter v16

    .line 241
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 242
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->s:Z

    if-eqz v2, :cond_2

    .line 243
    new-instance v2, Ljava/io/IOException;

    const-string/jumbo v3, "shutdown"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 252
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v2

    .line 261
    :catchall_1
    move-exception v2

    monitor-exit v16
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v2

    .line 233
    :cond_0
    const/4 v5, 0x0

    goto :goto_0

    .line 234
    :cond_1
    const/4 v6, 0x0

    goto :goto_1

    .line 245
    :cond_2
    :try_start_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->q:I

    .line 246
    move-object/from16 v0, p0

    iget v2, v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->q:I

    add-int/lit8 v2, v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->q:I

    .line 247
    new-instance v2, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/m;

    move-object/from16 v4, p0

    move-object/from16 v8, p2

    invoke-direct/range {v2 .. v8}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/m;-><init>(ILcom/squareup/okhttp/v_1_5_1/internal/spdy/l;ZZILjava/util/List;)V

    .line 248
    invoke-virtual {v2}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/m;->a()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 249
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->n:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v4, v8, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->a(Z)V

    .line 252
    :cond_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 253
    if-nez p1, :cond_5

    .line 254
    :try_start_4
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->h:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;

    move v9, v5

    move v10, v6

    move v11, v3

    move/from16 v12, p1

    move v13, v7

    move-object/from16 v15, p2

    invoke-interface/range {v8 .. v15}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;->a(ZZIIIILjava/util/List;)V

    .line 261
    :goto_2
    monitor-exit v16
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 263
    if-nez p3, :cond_4

    .line 264
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->h:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;

    invoke-interface {v3}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;->c()V

    .line 267
    :cond_4
    return-object v2

    .line 256
    :cond_5
    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->b:Z

    if-eqz v4, :cond_6

    .line 257
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "client streams shouldn\'t have associated stream IDs"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 259
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->h:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;

    move/from16 v0, p1

    move-object/from16 v1, p2

    invoke-interface {v4, v0, v3, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;->a(IILjava/util/List;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_2
.end method

.method static synthetic a(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->o:Ljava/lang/String;

    return-object v0
.end method

.method private a(ILjava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 770
    monitor-enter p0

    .line 771
    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->x:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 772
    sget-object v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;->b:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->a(ILcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;)V

    .line 773
    monitor-exit p0

    .line 791
    :goto_0
    return-void

    .line 775
    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->x:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 776
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 777
    sget-object v6, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$4;

    const-string/jumbo v2, "OkHttp %s Push Request[%s]"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->o:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$4;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;Ljava/lang/String;[Ljava/lang/Object;ILjava/util/List;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 776
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(ILjava/util/List;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 795
    sget-object v7, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$5;

    const-string/jumbo v2, "OkHttp %s Push Headers[%s]"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->o:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$5;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;Ljava/lang/String;[Ljava/lang/Object;ILjava/util/List;Z)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 809
    return-void
.end method

.method private a(ILlk;IZ)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 817
    new-instance v5, Llq;

    invoke-direct {v5}, Llq;-><init>()V

    .line 818
    int-to-long v0, p3

    invoke-interface {p2, v0, v1}, Llk;->a(J)V

    .line 819
    int-to-long v0, p3

    invoke-interface {p2, v5, v0, v1}, Llk;->b(Llq;J)J

    .line 820
    invoke-virtual {v5}, Llq;->l()J

    move-result-wide v0

    int-to-long v2, p3

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Llq;->l()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " != "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 821
    :cond_0
    sget-object v8, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$6;

    const-string/jumbo v2, "OkHttp %s Push Data[%s]"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->o:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$6;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;Ljava/lang/String;[Ljava/lang/Object;ILlq;IZ)V

    invoke-interface {v8, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 835
    return-void
.end method

.method private a(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;Lcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 432
    sget-boolean v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->k:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 435
    :cond_0
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->a(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v2

    .line 442
    :goto_0
    monitor-enter p0

    .line 443
    :try_start_1
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 444
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->n:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    new-array v4, v4, [Lcom/squareup/okhttp/v_1_5_1/internal/spdy/m;

    invoke-interface {v0, v4}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/okhttp/v_1_5_1/internal/spdy/m;

    .line 445
    iget-object v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->n:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    .line 446
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->a(Z)V

    move-object v5, v0

    .line 448
    :goto_1
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->t:Ljava/util/Map;

    if-eqz v0, :cond_7

    .line 449
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->t:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->t:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    new-array v2, v2, [Lcom/squareup/okhttp/v_1_5_1/internal/spdy/h;

    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/okhttp/v_1_5_1/internal/spdy/h;

    .line 450
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->t:Ljava/util/Map;

    move-object v4, v0

    .line 452
    :goto_2
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 454
    if-eqz v5, :cond_3

    .line 455
    array-length v6, v5

    move v2, v3

    move-object v0, v1

    :goto_3
    if-ge v2, v6, :cond_2

    aget-object v1, v5, v2

    .line 457
    :try_start_2
    invoke-virtual {v1, p2}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/m;->a(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 455
    :cond_1
    :goto_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 436
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 437
    goto :goto_0

    .line 452
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 458
    :catch_1
    move-exception v1

    .line 459
    if-eqz v0, :cond_1

    move-object v0, v1

    goto :goto_4

    :cond_2
    move-object v1, v0

    .line 464
    :cond_3
    if-eqz v4, :cond_4

    .line 465
    array-length v2, v4

    move v0, v3

    :goto_5
    if-ge v0, v2, :cond_4

    aget-object v3, v4, v0

    .line 466
    invoke-virtual {v3}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/h;->c()V

    .line 465
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 471
    :cond_4
    :try_start_4
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->g:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/a;

    invoke-interface {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/a;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 476
    :goto_6
    :try_start_5
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->h:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;

    invoke-interface {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    move-object v0, v1

    .line 481
    :cond_5
    :goto_7
    if-eqz v0, :cond_6

    throw v0

    .line 472
    :catch_2
    move-exception v0

    move-object v1, v0

    .line 473
    goto :goto_6

    .line 477
    :catch_3
    move-exception v0

    .line 478
    if-eqz v1, :cond_5

    move-object v0, v1

    goto :goto_7

    .line 482
    :cond_6
    return-void

    :cond_7
    move-object v4, v2

    goto :goto_2

    :cond_8
    move-object v5, v2

    goto :goto_1
.end method

.method static synthetic a(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;ILcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->c(ILcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;ILjava/util/List;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->a(ILjava/util/List;)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;ILjava/util/List;Z)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->a(ILjava/util/List;Z)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;ILlk;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->a(ILlk;IZ)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;Lcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;Lcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->a(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;Lcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;ZIILcom/squareup/okhttp/v_1_5_1/internal/spdy/h;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->b(ZIILcom/squareup/okhttp/v_1_5_1/internal/spdy/h;)V

    return-void
.end method

.method private declared-synchronized a(Z)V
    .locals 2

    .prologue
    .line 187
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    :goto_0
    iput-wide v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->r:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    monitor-exit p0

    return-void

    .line 187
    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(ZIILcom/squareup/okhttp/v_1_5_1/internal/spdy/h;)V
    .locals 9

    .prologue
    .line 374
    sget-object v8, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$3;

    const-string/jumbo v2, "OkHttp %s ping %08x%08x"

    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->o:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move v5, p2

    move v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$3;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;Ljava/lang/String;[Ljava/lang/Object;ZIILcom/squareup/okhttp/v_1_5_1/internal/spdy/h;)V

    invoke-interface {v8, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 383
    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;I)Z
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->d(I)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;Z)Z
    .locals 0

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->w:Z

    return p1
.end method

.method static synthetic b(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;I)I
    .locals 0

    .prologue
    .line 53
    iput p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->p:I

    return p1
.end method

.method static synthetic b(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;ZIILcom/squareup/okhttp/v_1_5_1/internal/spdy/h;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->a(ZIILcom/squareup/okhttp/v_1_5_1/internal/spdy/h;)V

    return-void
.end method

.method private b(ZIILcom/squareup/okhttp/v_1_5_1/internal/spdy/h;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 386
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->h:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;

    monitor-enter v1

    .line 388
    if-eqz p4, :cond_0

    :try_start_0
    invoke-virtual {p4}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/h;->a()V

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->h:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;->a(ZII)V

    .line 390
    monitor-exit v1

    .line 391
    return-void

    .line 390
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic b(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;)Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->s:Z

    return v0
.end method

.method static synthetic b(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;Z)Z
    .locals 0

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->s:Z

    return p1
.end method

.method static synthetic c(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;)I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->p:I

    return v0
.end method

.method private declared-synchronized c(I)Lcom/squareup/okhttp/v_1_5_1/internal/spdy/h;
    .locals 2

    .prologue
    .line 394
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->t:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->t:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic c(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;I)Lcom/squareup/okhttp/v_1_5_1/internal/spdy/h;
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->c(I)Lcom/squareup/okhttp/v_1_5_1/internal/spdy/h;

    move-result-object v0

    return-object v0
.end method

.method private c(ILcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;)V
    .locals 7

    .prologue
    .line 838
    sget-object v6, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$7;

    const-string/jumbo v2, "OkHttp %s Push Reset[%s]"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->o:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$7;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;Ljava/lang/String;[Ljava/lang/Object;ILcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 846
    return-void
.end method

.method static synthetic d(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;)I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->q:I

    return v0
.end method

.method private d(I)Z
    .locals 2

    .prologue
    .line 763
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->a:Lcom/squareup/okhttp/v_1_5_1/Protocol;

    sget-object v1, Lcom/squareup/okhttp/v_1_5_1/Protocol;->a:Lcom/squareup/okhttp/v_1_5_1/Protocol;

    if-ne v0, v1, :cond_0

    if-eqz p1, :cond_0

    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->n:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic f(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;)Lcom/squareup/okhttp/v_1_5_1/internal/spdy/f;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->m:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/f;

    return-object v0
.end method

.method static synthetic g()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->l:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic g(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;)Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->w:Z

    return v0
.end method

.method static synthetic h(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;)Lcom/squareup/okhttp/v_1_5_1/internal/spdy/i;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->u:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/i;

    return-object v0
.end method

.method static synthetic i(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->x:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/squareup/okhttp/v_1_5_1/Protocol;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->a:Lcom/squareup/okhttp/v_1_5_1/Protocol;

    return-object v0
.end method

.method declared-synchronized a(I)Lcom/squareup/okhttp/v_1_5_1/internal/spdy/m;
    .locals 2

    .prologue
    .line 175
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->n:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/m;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/util/List;ZZ)Lcom/squareup/okhttp/v_1_5_1/internal/spdy/m;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;",
            ">;ZZ)",
            "Lcom/squareup/okhttp/v_1_5_1/internal/spdy/m;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 228
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->a(ILjava/util/List;ZZ)Lcom/squareup/okhttp/v_1_5_1/internal/spdy/m;

    move-result-object v0

    return-object v0
.end method

.method a(IJ)V
    .locals 8

    .prologue
    .line 342
    sget-object v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$2;

    const-string/jumbo v3, "OkHttp Window Update %s stream %d"

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v5, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->o:Ljava/lang/String;

    aput-object v5, v4, v2

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    move-object v2, p0

    move v5, p1

    move-wide v6, p2

    invoke-direct/range {v1 .. v7}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$2;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;Ljava/lang/String;[Ljava/lang/Object;IJ)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 350
    return-void
.end method

.method a(ILcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;)V
    .locals 7

    .prologue
    .line 327
    sget-object v6, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$1;

    const-string/jumbo v2, "OkHttp %s stream %d"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->o:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$1;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;Ljava/lang/String;[Ljava/lang/Object;ILcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 335
    return-void
.end method

.method public a(IZLlq;J)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const-wide/16 v8, 0x0

    .line 292
    cmp-long v0, p4, v8

    if-nez v0, :cond_2

    .line 293
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->h:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;

    invoke-interface {v0, p2, p1, p3, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;->a(ZILlq;I)V

    .line 315
    :cond_0
    return-void

    .line 308
    :cond_1
    :try_start_0
    iget-wide v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->d:J

    invoke-static {p4, p5, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    iget-wide v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->i:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v2, v2

    .line 309
    iget-wide v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->d:J

    int-to-long v6, v2

    sub-long/2addr v4, v6

    iput-wide v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->d:J

    .line 310
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 312
    int-to-long v4, v2

    sub-long/2addr p4, v4

    .line 313
    iget-object v3, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->h:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;

    if-eqz p2, :cond_3

    cmp-long v0, p4, v8

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v3, v0, p1, p3, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;->a(ZILlq;I)V

    .line 297
    :cond_2
    cmp-long v0, p4, v8

    if-lez v0, :cond_0

    .line 299
    monitor-enter p0

    .line 301
    :goto_1
    :try_start_1
    iget-wide v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->d:J

    cmp-long v0, v2, v8

    if-gtz v0, :cond_1

    .line 302
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 304
    :catch_0
    move-exception v0

    .line 305
    :try_start_2
    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0

    .line 310
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_3
    move v0, v1

    .line 313
    goto :goto_0
.end method

.method a(J)V
    .locals 3

    .prologue
    .line 322
    iget-wide v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->d:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->d:J

    .line 323
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 324
    :cond_0
    return-void
.end method

.method public a(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 408
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->h:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;

    monitor-enter v1

    .line 410
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 411
    :try_start_1
    iget-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->s:Z

    if-eqz v0, :cond_0

    .line 412
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 420
    :goto_0
    return-void

    .line 414
    :cond_0
    const/4 v0, 0x1

    :try_start_3
    iput-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->s:Z

    .line 415
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->p:I

    .line 416
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 418
    :try_start_4
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->h:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;

    sget-object v3, Llh;->a:[B

    invoke-interface {v2, v0, p1, v3}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;->a(ILcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;[B)V

    .line 419
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 416
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method declared-synchronized b(I)Lcom/squareup/okhttp/v_1_5_1/internal/spdy/m;
    .locals 2

    .prologue
    .line 179
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->n:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/m;

    .line 180
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->n:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 181
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    :cond_0
    monitor-exit p0

    return-object v0

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method b(ILcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 338
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->h:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;

    invoke-interface {v0, p1, p2}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;->a(ILcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;)V

    .line 339
    return-void
.end method

.method public declared-synchronized b()Z
    .locals 4

    .prologue
    .line 192
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->r:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()J
    .locals 2

    .prologue
    .line 200
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->r:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 428
    sget-object v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;->a:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;

    sget-object v1, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;->l:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;

    invoke-direct {p0, v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->a(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;Lcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;)V

    .line 429
    return-void
.end method

.method public d()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 398
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->h:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;

    invoke-interface {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;->c()V

    .line 399
    return-void
.end method

.method public e()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 489
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->h:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;

    invoke-interface {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;->a()V

    .line 490
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->h:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->e:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/j;

    invoke-interface {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;->a(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/j;)V

    .line 491
    return-void
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 494
    iget-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->s:Z

    return v0
.end method
