.class Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$6;
.super Llf;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->a(ILlk;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Llq;

.field final synthetic c:I

.field final synthetic d:Z

.field final synthetic e:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;


# direct methods
.method varargs constructor <init>(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;Ljava/lang/String;[Ljava/lang/Object;ILlq;IZ)V
    .locals 0

    .prologue
    .line 821
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$6;->e:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;

    iput p4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$6;->a:I

    iput-object p5, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$6;->b:Llq;

    iput p6, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$6;->c:I

    iput-boolean p7, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$6;->d:Z

    invoke-direct {p0, p2, p3}, Llf;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 824
    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$6;->e:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->h(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;)Lcom/squareup/okhttp/v_1_5_1/internal/spdy/i;

    move-result-object v0

    iget v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$6;->a:I

    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$6;->b:Llq;

    iget v3, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$6;->c:I

    iget-boolean v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$6;->d:Z

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/i;->a(ILlk;IZ)Z

    move-result v0

    .line 825
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$6;->e:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;

    iget-object v1, v1, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->h:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;

    iget v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$6;->a:I

    sget-object v3, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;->l:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;

    invoke-interface {v1, v2, v3}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;->a(ILcom/squareup/okhttp/v_1_5_1/internal/spdy/ErrorCode;)V

    .line 826
    :cond_0
    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$6;->d:Z

    if-eqz v0, :cond_2

    .line 827
    :cond_1
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$6;->e:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 828
    :try_start_1
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$6;->e:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->i(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;)Ljava/util/Set;

    move-result-object v0

    iget v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$6;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 829
    monitor-exit v1

    .line 833
    :cond_2
    :goto_0
    return-void

    .line 829
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 831
    :catch_0
    move-exception v0

    goto :goto_0
.end method
