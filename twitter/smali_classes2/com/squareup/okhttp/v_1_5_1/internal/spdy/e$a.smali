.class final Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Llx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field a:I

.field b:B

.field c:I

.field d:I

.field private final e:Llk;


# direct methods
.method public constructor <init>(Llk;)V
    .locals 0

    .prologue
    .line 508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 509
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$a;->e:Llk;

    .line 510
    return-void
.end method

.method private a()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 534
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$a;->c:I

    .line 535
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$a;->e:Llk;

    invoke-interface {v1}, Llk;->i()I

    move-result v1

    .line 536
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$a;->e:Llk;

    invoke-interface {v2}, Llk;->i()I

    move-result v2

    .line 537
    const/high16 v3, 0x3fff0000    # 1.9921875f

    and-int/2addr v3, v1

    shr-int/lit8 v3, v3, 0x10

    int-to-short v3, v3

    iput v3, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$a;->d:I

    iput v3, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$a;->a:I

    .line 538
    const v3, 0xff00

    and-int/2addr v3, v1

    shr-int/lit8 v3, v3, 0x8

    int-to-byte v3, v3

    .line 539
    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    iput-byte v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$a;->b:B

    .line 540
    const v1, 0x7fffffff

    and-int/2addr v1, v2

    iput v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$a;->c:I

    .line 541
    const/16 v1, 0xa

    if-eq v3, v1, :cond_0

    const-string/jumbo v0, "%s != TYPE_CONTINUATION"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 542
    :cond_0
    iget v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$a;->c:I

    if-eq v1, v0, :cond_1

    const-string/jumbo v0, "TYPE_CONTINUATION streamId changed"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 543
    :cond_1
    return-void
.end method


# virtual methods
.method public b(Llq;J)J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v0, -0x1

    .line 513
    :goto_0
    iget v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$a;->d:I

    if-nez v2, :cond_2

    .line 514
    iget-byte v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$a;->b:B

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_1

    .line 522
    :cond_0
    :goto_1
    return-wide v0

    .line 515
    :cond_1
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$a;->a()V

    goto :goto_0

    .line 519
    :cond_2
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$a;->e:Llk;

    iget v3, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$a;->d:I

    int-to-long v4, v3

    invoke-static {p2, p3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-interface {v2, p1, v4, v5}, Llk;->b(Llq;J)J

    move-result-wide v2

    .line 520
    cmp-long v4, v2, v0

    if-eqz v4, :cond_0

    .line 521
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$a;->d:I

    int-to-long v0, v0

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$a;->d:I

    move-wide v0, v2

    .line 522
    goto :goto_1
.end method

.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 531
    return-void
.end method
