.class public Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Llk;

.field private c:Llj;

.field private d:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/f;

.field private e:Lcom/squareup/okhttp/v_1_5_1/Protocol;

.field private f:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/i;

.field private g:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLlk;Llj;)V
    .locals 1

    .prologue
    .line 515
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 501
    sget-object v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/f;->a:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/f;

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->d:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/f;

    .line 502
    sget-object v0, Lcom/squareup/okhttp/v_1_5_1/Protocol;->b:Lcom/squareup/okhttp/v_1_5_1/Protocol;

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->e:Lcom/squareup/okhttp/v_1_5_1/Protocol;

    .line 503
    sget-object v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/i;->a:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/i;

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->f:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/i;

    .line 516
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->a:Ljava/lang/String;

    .line 517
    iput-boolean p2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->g:Z

    .line 518
    iput-object p3, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->b:Llk;

    .line 519
    iput-object p4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->c:Llj;

    .line 520
    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;)Lcom/squareup/okhttp/v_1_5_1/Protocol;
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->e:Lcom/squareup/okhttp/v_1_5_1/Protocol;

    return-object v0
.end method

.method static synthetic b(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;)Lcom/squareup/okhttp/v_1_5_1/internal/spdy/i;
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->f:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/i;

    return-object v0
.end method

.method static synthetic c(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;)Z
    .locals 1

    .prologue
    .line 497
    iget-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->g:Z

    return v0
.end method

.method static synthetic d(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;)Lcom/squareup/okhttp/v_1_5_1/internal/spdy/f;
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->d:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/f;

    return-object v0
.end method

.method static synthetic e(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;)Llk;
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->b:Llk;

    return-object v0
.end method

.method static synthetic g(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;)Llj;
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->c:Llj;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/squareup/okhttp/v_1_5_1/Protocol;)Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;
    .locals 0

    .prologue
    .line 528
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->e:Lcom/squareup/okhttp/v_1_5_1/Protocol;

    .line 529
    return-object p0
.end method

.method public a()Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;
    .locals 2

    .prologue
    .line 538
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$1;)V

    return-object v0
.end method
