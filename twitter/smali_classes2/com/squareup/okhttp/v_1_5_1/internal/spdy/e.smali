.class public final Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/squareup/okhttp/v_1_5_1/internal/spdy/n;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$a;,
        Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$c;,
        Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$b;
    }
.end annotation


# static fields
.field private static final a:Lll;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-string/jumbo v0, "PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n"

    invoke-static {v0}, Lll;->a(Ljava/lang/String;)Lll;

    move-result-object v0

    sput-object v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e;->a:Lll;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 499
    return-void
.end method

.method static synthetic a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33
    invoke-static {p0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;
    .locals 1

    .prologue
    .line 33
    invoke-static {p0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b()Lll;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e;->a:Lll;

    return-object v0
.end method

.method private static varargs c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;
    .locals 2

    .prologue
    .line 487
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static varargs d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 491
    new-instance v0, Ljava/io/IOException;

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 69
    const/16 v0, 0x3fff

    return v0
.end method

.method public a(Llk;Z)Lcom/squareup/okhttp/v_1_5_1/internal/spdy/a;
    .locals 2

    .prologue
    .line 61
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$b;

    const/16 v1, 0x1000

    invoke-direct {v0, p1, v1, p2}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$b;-><init>(Llk;IZ)V

    return-object v0
.end method

.method public a(Llj;Z)Lcom/squareup/okhttp/v_1_5_1/internal/spdy/b;
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$c;

    invoke-direct {v0, p1, p2}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/e$c;-><init>(Llj;Z)V

    return-object v0
.end method
