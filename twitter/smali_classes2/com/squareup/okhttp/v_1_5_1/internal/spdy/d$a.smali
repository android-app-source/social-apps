.class final Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

.field b:I

.field c:I

.field d:Llc;

.field e:J

.field f:I

.field private final g:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/Huffman$Codec;

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Llk;

.field private j:I


# direct methods
.method constructor <init>(ZILlx;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->h:Ljava/util/List;

    .line 107
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    .line 109
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->b:I

    .line 110
    iput v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->c:I

    .line 116
    new-instance v0, Llc$a;

    invoke-direct {v0}, Llc$a;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->d:Llc;

    .line 123
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->e:J

    .line 124
    iput v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->f:I

    .line 127
    if-eqz p1, :cond_0

    sget-object v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/Huffman$Codec;->b:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/Huffman$Codec;

    :goto_0
    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->g:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/Huffman$Codec;

    .line 128
    iput p2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->j:I

    .line 129
    invoke-static {p3}, Llr;->a(Llx;)Llk;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->i:Llk;

    .line 130
    return-void

    .line 127
    :cond_0
    sget-object v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/Huffman$Codec;->a:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/Huffman$Codec;

    goto :goto_0
.end method

.method private a(ILcom/squareup/okhttp/v_1_5_1/internal/spdy/c;)V
    .locals 6

    .prologue
    const/4 v3, -0x1

    .line 295
    iget v0, p2, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->j:I

    .line 296
    if-eq p1, v3, :cond_4

    .line 297
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->d(I)I

    move-result v2

    aget-object v1, v1, v2

    iget v1, v1, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->j:I

    sub-int/2addr v0, v1

    move v1, v0

    .line 301
    :goto_0
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->j:I

    if-le v1, v0, :cond_0

    .line 302
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->d()V

    .line 304
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->h:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 333
    :goto_1
    return-void

    .line 309
    :cond_0
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->f:I

    add-int/2addr v0, v1

    iget v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->j:I

    sub-int/2addr v0, v2

    .line 310
    invoke-direct {p0, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->b(I)I

    move-result v0

    .line 312
    if-ne p1, v3, :cond_3

    .line 313
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->c:I

    add-int/lit8 v0, v0, 0x1

    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    array-length v2, v2

    if-le v0, v2, :cond_2

    .line 314
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    new-array v2, v0, [Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    .line 315
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    array-length v4, v4

    iget-object v5, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    array-length v5, v5

    invoke-static {v0, v3, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 316
    array-length v0, v2

    const/16 v3, 0x40

    if-ne v0, v3, :cond_1

    .line 317
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->d:Llc;

    check-cast v0, Llc$a;

    invoke-virtual {v0}, Llc$a;->b()Llc;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->d:Llc;

    .line 319
    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->d:Llc;

    iget-object v3, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    array-length v3, v3

    invoke-interface {v0, v3}, Llc;->d(I)V

    .line 320
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->b:I

    .line 321
    iput-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    .line 323
    :cond_2
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->b:I

    add-int/lit8 v2, v0, -0x1

    iput v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->b:I

    .line 324
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->d:Llc;

    invoke-interface {v2, v0}, Llc;->a(I)V

    .line 325
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    aput-object p2, v2, v0

    .line 326
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->c:I

    .line 332
    :goto_2
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->f:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->f:I

    goto :goto_1

    .line 328
    :cond_3
    invoke-direct {p0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->d(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, p1

    .line 329
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->d:Llc;

    invoke-interface {v2, v0}, Llc;->a(I)V

    .line 330
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    aput-object p2, v2, v0

    goto :goto_2

    :cond_4
    move v1, v0

    goto/16 :goto_0
.end method

.method private b(I)I
    .locals 6

    .prologue
    .line 162
    const/4 v1, 0x0

    .line 163
    if-lez p1, :cond_1

    .line 165
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    iget v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->b:I

    if-lt v0, v2, :cond_0

    if-lez p1, :cond_0

    .line 166
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    aget-object v2, v2, v0

    iget v2, v2, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->j:I

    sub-int/2addr p1, v2

    .line 167
    iget v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->f:I

    iget-object v3, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->j:I

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->f:I

    .line 168
    iget v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->c:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->c:I

    .line 169
    add-int/lit8 v1, v1, 0x1

    .line 165
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->d:Llc;

    invoke-interface {v0, v1}, Llc;->d(I)V

    .line 172
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    iget v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->b:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    iget v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->b:I

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v4, v1

    iget v5, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->c:I

    invoke-static {v0, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 174
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->b:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->b:I

    .line 176
    :cond_1
    return v1
.end method

.method private c(I)V
    .locals 5

    .prologue
    .line 239
    invoke-direct {p0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->h(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 240
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->j:I

    if-nez v0, :cond_0

    .line 241
    iget-wide v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->e:J

    const-wide/16 v2, 0x1

    iget v4, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->c:I

    sub-int v4, p1, v4

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->e:J

    .line 249
    :goto_0
    return-void

    .line 243
    :cond_0
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d;->a()[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    move-result-object v0

    iget v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->c:I

    sub-int v1, p1, v1

    aget-object v0, v0, v1

    .line 244
    const/4 v1, -0x1

    invoke-direct {p0, v1, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a(ILcom/squareup/okhttp/v_1_5_1/internal/spdy/c;)V

    goto :goto_0

    .line 247
    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->d:Llc;

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->d(I)I

    move-result v1

    invoke-interface {v0, v1}, Llc;->b(I)V

    goto :goto_0
.end method

.method private d(I)I
    .locals 1

    .prologue
    .line 253
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->b:I

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v0, p1

    return v0
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 153
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->e()V

    .line 154
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 155
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->b:I

    .line 156
    iput v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->c:I

    .line 157
    iput v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->f:I

    .line 158
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 211
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->e:J

    .line 212
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->d:Llc;

    invoke-interface {v0}, Llc;->a()V

    .line 213
    return-void
.end method

.method private e(I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 257
    invoke-direct {p0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->g(I)Lll;

    move-result-object v0

    .line 258
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a(Z)Lll;

    move-result-object v1

    .line 259
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->h:Ljava/util/List;

    new-instance v3, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    invoke-direct {v3, v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;-><init>(Lll;Lll;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 260
    return-void
.end method

.method private f()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 263
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a(Z)Lll;

    move-result-object v0

    .line 264
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a(Z)Lll;

    move-result-object v1

    .line 265
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->h:Ljava/util/List;

    new-instance v3, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    invoke-direct {v3, v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;-><init>(Lll;Lll;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 266
    return-void
.end method

.method private f(I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 270
    invoke-direct {p0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->g(I)Lll;

    move-result-object v0

    .line 271
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a(Z)Lll;

    move-result-object v1

    .line 272
    const/4 v2, -0x1

    new-instance v3, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    invoke-direct {v3, v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;-><init>(Lll;Lll;)V

    invoke-direct {p0, v2, v3}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a(ILcom/squareup/okhttp/v_1_5_1/internal/spdy/c;)V

    .line 273
    return-void
.end method

.method private g(I)Lll;
    .locals 2

    .prologue
    .line 282
    invoke-direct {p0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->h(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d;->a()[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    move-result-object v0

    iget v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->c:I

    sub-int v1, p1, v1

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->h:Lll;

    .line 285
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->d(I)I

    move-result v1

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->h:Lll;

    goto :goto_0
.end method

.method private g()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 276
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a(Z)Lll;

    move-result-object v0

    .line 277
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a(Z)Lll;

    move-result-object v1

    .line 278
    const/4 v2, -0x1

    new-instance v3, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    invoke-direct {v3, v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;-><init>(Lll;Lll;)V

    invoke-direct {p0, v2, v3}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a(ILcom/squareup/okhttp/v_1_5_1/internal/spdy/c;)V

    .line 279
    return-void
.end method

.method private h()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 336
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->i:Llk;

    invoke-interface {v0}, Llk;->f()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method private h(I)Z
    .locals 1

    .prologue
    .line 290
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->c:I

    if-lt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(II)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 340
    and-int v0, p1, p2

    .line 341
    if-ge v0, p2, :cond_0

    .line 358
    :goto_0
    return v0

    .line 347
    :cond_0
    const/4 v0, 0x0

    .line 349
    :goto_1
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->h()I

    move-result v1

    .line 350
    and-int/lit16 v2, v1, 0x80

    if-eqz v2, :cond_1

    .line 351
    and-int/lit8 v1, v1, 0x7f

    shl-int/2addr v1, v0

    add-int/2addr p2, v1

    .line 352
    add-int/lit8 v0, v0, 0x7

    goto :goto_1

    .line 354
    :cond_1
    shl-int v0, v1, v0

    add-int/2addr v0, p2

    .line 355
    goto :goto_0
.end method

.method a(Z)Lll;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 366
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->h()I

    move-result v1

    .line 367
    and-int/lit16 v0, v1, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    .line 368
    :goto_0
    const/16 v2, 0x7f

    invoke-virtual {p0, v1, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a(II)I

    move-result v1

    .line 370
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->i:Llk;

    int-to-long v4, v1

    invoke-interface {v2, v4, v5}, Llk;->c(J)Lll;

    move-result-object v1

    .line 372
    if-eqz v0, :cond_2

    .line 373
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->g:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/Huffman$Codec;

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/Huffman$Codec;->a(Lll;)Lll;

    move-result-object v0

    .line 376
    :goto_1
    if-eqz p1, :cond_0

    .line 377
    invoke-virtual {v0}, Lll;->d()Lll;

    move-result-object v0

    .line 380
    :cond_0
    return-object v0

    .line 367
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method a()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x80

    const/16 v3, 0x40

    const/16 v2, 0x3f

    .line 184
    :goto_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->i:Llk;

    invoke-interface {v0}, Llk;->e()Z

    move-result v0

    if-nez v0, :cond_6

    .line 185
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->i:Llk;

    invoke-interface {v0}, Llk;->f()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 186
    if-ne v0, v4, :cond_0

    .line 187
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->e()V

    goto :goto_0

    .line 188
    :cond_0
    and-int/lit16 v1, v0, 0x80

    if-ne v1, v4, :cond_1

    .line 189
    const/16 v1, 0x7f

    invoke-virtual {p0, v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a(II)I

    move-result v0

    .line 190
    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->c(I)V

    goto :goto_0

    .line 192
    :cond_1
    if-ne v0, v3, :cond_2

    .line 193
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->f()V

    goto :goto_0

    .line 194
    :cond_2
    and-int/lit8 v1, v0, 0x40

    if-ne v1, v3, :cond_3

    .line 195
    invoke-virtual {p0, v0, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a(II)I

    move-result v0

    .line 196
    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->e(I)V

    goto :goto_0

    .line 197
    :cond_3
    if-nez v0, :cond_4

    .line 198
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->g()V

    goto :goto_0

    .line 199
    :cond_4
    and-int/lit16 v1, v0, 0xc0

    if-nez v1, :cond_5

    .line 200
    invoke-virtual {p0, v0, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a(II)I

    move-result v0

    .line 201
    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->f(I)V

    goto :goto_0

    .line 204
    :cond_5
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unhandled byte: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 208
    :cond_6
    return-void
.end method

.method a(I)V
    .locals 2

    .prologue
    .line 142
    iput p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->j:I

    .line 143
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->j:I

    iget v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->f:I

    if-ge v0, v1, :cond_0

    .line 144
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->j:I

    if-nez v0, :cond_1

    .line 145
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->d()V

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->f:I

    iget v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->j:I

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->b(I)I

    goto :goto_0
.end method

.method b()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 216
    const/4 v0, 0x0

    :goto_0
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d;->a()[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 217
    iget-wide v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->e:J

    shr-long/2addr v2, v0

    and-long/2addr v2, v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 218
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->h:Ljava/util/List;

    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d;->a()[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    move-result-object v2

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 221
    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    iget v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->b:I

    if-eq v0, v1, :cond_3

    .line 222
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->d:Llc;

    invoke-interface {v1, v0}, Llc;->c(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 223
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->h:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->a:[Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 226
    :cond_3
    return-void
.end method

.method c()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 233
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->h:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 234
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$a;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 235
    return-object v0
.end method
