.class Lcom/squareup/okhttp/v_1_5_1/internal/spdy/g$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Llx;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/okhttp/v_1_5_1/internal/spdy/g;-><init>(Llk;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Llk;

.field final synthetic b:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/g;


# direct methods
.method constructor <init>(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/g;Llk;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/g$1;->b:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/g;

    iput-object p2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/g$1;->a:Llk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public b(Llq;J)J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v0, -0x1

    .line 43
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/g$1;->b:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/g;

    invoke-static {v2}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/g;->a(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/g;)I

    move-result v2

    if-nez v2, :cond_1

    .line 47
    :cond_0
    :goto_0
    return-wide v0

    .line 44
    :cond_1
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/g$1;->a:Llk;

    iget-object v3, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/g$1;->b:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/g;

    invoke-static {v3}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/g;->a(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/g;)I

    move-result v3

    int-to-long v4, v3

    invoke-static {p2, p3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-interface {v2, p1, v4, v5}, Llk;->b(Llq;J)J

    move-result-wide v2

    .line 45
    cmp-long v4, v2, v0

    if-eqz v4, :cond_0

    .line 46
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/g$1;->b:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/g;

    invoke-static {v0, v2, v3}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/g;->a(Lcom/squareup/okhttp/v_1_5_1/internal/spdy/g;J)I

    move-wide v0, v2

    .line 47
    goto :goto_0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/g$1;->a:Llk;

    invoke-interface {v0}, Llk;->close()V

    .line 52
    return-void
.end method
