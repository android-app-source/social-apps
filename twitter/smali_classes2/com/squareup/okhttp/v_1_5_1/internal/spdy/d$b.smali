.class final Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$b;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation


# instance fields
.field private final a:Llq;


# direct methods
.method constructor <init>(Llq;)V
    .locals 0

    .prologue
    .line 400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 401
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$b;->a:Llq;

    .line 402
    return-void
.end method


# virtual methods
.method a(III)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 424
    if-ge p1, p2, :cond_0

    .line 425
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$b;->a:Llq;

    or-int v1, p3, p1

    invoke-virtual {v0, v1}, Llq;->c(I)Llq;

    .line 440
    :goto_0
    return-void

    .line 430
    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$b;->a:Llq;

    or-int v1, p3, p2

    invoke-virtual {v0, v1}, Llq;->c(I)Llq;

    .line 431
    sub-int v0, p1, p2

    .line 434
    :goto_1
    const/16 v1, 0x80

    if-lt v0, v1, :cond_1

    .line 435
    and-int/lit8 v1, v0, 0x7f

    .line 436
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$b;->a:Llq;

    or-int/lit16 v1, v1, 0x80

    invoke-virtual {v2, v1}, Llq;->c(I)Llq;

    .line 437
    ushr-int/lit8 v0, v0, 0x7

    .line 438
    goto :goto_1

    .line 439
    :cond_1
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$b;->a:Llq;

    invoke-virtual {v1, v0}, Llq;->c(I)Llq;

    goto :goto_0
.end method

.method a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x40

    .line 406
    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 407
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    iget-object v3, v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->h:Lll;

    .line 408
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 409
    if-eqz v0, :cond_0

    .line 411
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    const/16 v3, 0x3f

    invoke-virtual {p0, v0, v3, v4}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$b;->a(III)V

    .line 412
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    iget-object v0, v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->i:Lll;

    invoke-virtual {p0, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$b;->a(Lll;)V

    .line 406
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 414
    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$b;->a:Llq;

    invoke-virtual {v0, v4}, Llq;->c(I)Llq;

    .line 415
    invoke-virtual {p0, v3}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$b;->a(Lll;)V

    .line 416
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    iget-object v0, v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->i:Lll;

    invoke-virtual {p0, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$b;->a(Lll;)V

    goto :goto_1

    .line 419
    :cond_1
    return-void
.end method

.method a(Lll;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 443
    invoke-virtual {p1}, Lll;->e()I

    move-result v0

    const/16 v1, 0x7f

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$b;->a(III)V

    .line 444
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/d$b;->a:Llq;

    invoke-virtual {v0, p1}, Llq;->b(Lll;)Llq;

    .line 445
    return-void
.end method
