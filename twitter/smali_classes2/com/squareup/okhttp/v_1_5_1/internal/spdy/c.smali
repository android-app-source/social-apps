.class public final Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Lll;

.field public static final b:Lll;

.field public static final c:Lll;

.field public static final d:Lll;

.field public static final e:Lll;

.field public static final f:Lll;

.field public static final g:Lll;


# instance fields
.field public final h:Lll;

.field public final i:Lll;

.field final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-string/jumbo v0, ":status"

    invoke-static {v0}, Lll;->a(Ljava/lang/String;)Lll;

    move-result-object v0

    sput-object v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->a:Lll;

    .line 9
    const-string/jumbo v0, ":method"

    invoke-static {v0}, Lll;->a(Ljava/lang/String;)Lll;

    move-result-object v0

    sput-object v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->b:Lll;

    .line 10
    const-string/jumbo v0, ":path"

    invoke-static {v0}, Lll;->a(Ljava/lang/String;)Lll;

    move-result-object v0

    sput-object v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->c:Lll;

    .line 11
    const-string/jumbo v0, ":scheme"

    invoke-static {v0}, Lll;->a(Ljava/lang/String;)Lll;

    move-result-object v0

    sput-object v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->d:Lll;

    .line 12
    const-string/jumbo v0, ":authority"

    invoke-static {v0}, Lll;->a(Ljava/lang/String;)Lll;

    move-result-object v0

    sput-object v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->e:Lll;

    .line 13
    const-string/jumbo v0, ":host"

    invoke-static {v0}, Lll;->a(Ljava/lang/String;)Lll;

    move-result-object v0

    sput-object v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->f:Lll;

    .line 14
    const-string/jumbo v0, ":version"

    invoke-static {v0}, Lll;->a(Ljava/lang/String;)Lll;

    move-result-object v0

    sput-object v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->g:Lll;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 24
    invoke-static {p1}, Lll;->a(Ljava/lang/String;)Lll;

    move-result-object v0

    invoke-static {p2}, Lll;->a(Ljava/lang/String;)Lll;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;-><init>(Lll;Lll;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Lll;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 28
    invoke-static {p2}, Lll;->a(Ljava/lang/String;)Lll;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;-><init>(Lll;Lll;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Lll;Lll;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->h:Lll;

    .line 33
    iput-object p2, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->i:Lll;

    .line 34
    invoke-virtual {p1}, Lll;->e()I

    move-result v0

    add-int/lit8 v0, v0, 0x20

    invoke-virtual {p2}, Lll;->e()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->j:I

    .line 35
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 38
    instance-of v1, p1, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    if-eqz v1, :cond_0

    .line 39
    check-cast p1, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;

    .line 40
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->h:Lll;

    iget-object v2, p1, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->h:Lll;

    invoke-virtual {v1, v2}, Lll;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->i:Lll;

    iget-object v2, p1, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->i:Lll;

    invoke-virtual {v1, v2}, Lll;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 43
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 47
    .line 48
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->h:Lll;

    invoke-virtual {v0}, Lll;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 49
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->i:Lll;

    invoke-virtual {v1}, Lll;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 50
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 54
    const-string/jumbo v0, "%s: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->h:Lll;

    invoke-virtual {v3}, Lll;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/c;->i:Lll;

    invoke-virtual {v3}, Lll;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
