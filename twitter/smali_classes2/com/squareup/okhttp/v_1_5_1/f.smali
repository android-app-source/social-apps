.class public final Lcom/squareup/okhttp/v_1_5_1/f;
.super Ljava/net/ResponseCache;
.source "Twttr"

# interfaces
.implements Lcom/squareup/okhttp/v_1_5_1/i;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/okhttp/v_1_5_1/f$1;,
        Lcom/squareup/okhttp/v_1_5_1/f$b;,
        Lcom/squareup/okhttp/v_1_5_1/f$c;,
        Lcom/squareup/okhttp/v_1_5_1/f$a;
    }
.end annotation


# instance fields
.field private final a:Lld;

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(Ljava/io/File;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/net/ResponseCache;-><init>()V

    .line 129
    const v0, 0x31191

    const/4 v1, 0x2

    invoke-static {p1, v0, v1, p2, p3}, Lld;->a(Ljava/io/File;IIJ)Lld;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/f;->a:Lld;

    .line 130
    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/v_1_5_1/f;)I
    .locals 2

    .prologue
    .line 112
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/f;->b:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/squareup/okhttp/v_1_5_1/f;->b:I

    return v0
.end method

.method static synthetic a(Llk;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    invoke-static {p0}, Lcom/squareup/okhttp/v_1_5_1/f;->b(Llk;)I

    move-result v0

    return v0
.end method

.method private a(Lld$a;)V
    .locals 1

    .prologue
    .line 232
    if-eqz p1, :cond_0

    .line 233
    :try_start_0
    invoke-virtual {p1}, Lld$a;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 237
    :cond_0
    :goto_0
    return-void

    .line 235
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/squareup/okhttp/v_1_5_1/f;)I
    .locals 2

    .prologue
    .line 112
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/f;->c:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/squareup/okhttp/v_1_5_1/f;->c:I

    return v0
.end method

.method private static b(Llk;)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 544
    const/4 v0, 0x1

    invoke-interface {p0, v0}, Llk;->a(Z)Ljava/lang/String;

    move-result-object v0

    .line 546
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 547
    :catch_0
    move-exception v1

    .line 548
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Expected an integer but was \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static c(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Llh;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 146
    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/f;->c(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Ljava/lang/String;

    move-result-object v1

    .line 150
    :try_start_0
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/f;->a:Lld;

    invoke-virtual {v2, v1}, Lld;->a(Ljava/lang/String;)Lld$c;

    move-result-object v1

    .line 151
    if-nez v1, :cond_0

    .line 167
    :goto_0
    return-object v0

    .line 154
    :cond_0
    new-instance v2, Lcom/squareup/okhttp/v_1_5_1/f$c;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lld$c;->a(I)Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/okhttp/v_1_5_1/f$c;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    invoke-virtual {v2, p1, v1}, Lcom/squareup/okhttp/v_1_5_1/f$c;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;Lld$c;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    move-result-object v1

    .line 162
    invoke-virtual {v2, p1, v1}, Lcom/squareup/okhttp/v_1_5_1/f$c;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 163
    invoke-virtual {v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->h()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;

    move-result-object v1

    invoke-static {v1}, Llh;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 167
    goto :goto_0

    .line 155
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public a(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)Ljava/net/CacheRequest;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 171
    invoke-virtual {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->a()Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->d()Ljava/lang/String;

    move-result-object v1

    .line 173
    invoke-virtual {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->a()Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/squareup/okhttp/v_1_5_1/f;->b(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-object v0

    .line 176
    :cond_1
    const-string/jumbo v2, "GET"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 183
    invoke-virtual {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->k()Z

    move-result v1

    if-nez v1, :cond_0

    .line 187
    new-instance v1, Lcom/squareup/okhttp/v_1_5_1/f$c;

    invoke-direct {v1, p1}, Lcom/squareup/okhttp/v_1_5_1/f$c;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)V

    .line 190
    :try_start_0
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/f;->a:Lld;

    invoke-virtual {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->a()Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/okhttp/v_1_5_1/f;->c(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lld;->b(Ljava/lang/String;)Lld$a;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 191
    if-eqz v2, :cond_0

    .line 194
    :try_start_1
    invoke-virtual {v1, v2}, Lcom/squareup/okhttp/v_1_5_1/f$c;->a(Lld$a;)V

    .line 195
    new-instance v1, Lcom/squareup/okhttp/v_1_5_1/f$a;

    invoke-direct {v1, p0, v2}, Lcom/squareup/okhttp/v_1_5_1/f$a;-><init>(Lcom/squareup/okhttp/v_1_5_1/f;Lld$a;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    goto :goto_0

    .line 196
    :catch_0
    move-exception v1

    move-object v1, v0

    .line 197
    :goto_1
    invoke-direct {p0, v1}, Lcom/squareup/okhttp/v_1_5_1/f;->a(Lld$a;)V

    goto :goto_0

    .line 196
    :catch_1
    move-exception v1

    move-object v1, v2

    goto :goto_1
.end method

.method public a()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 269
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/f;->a:Lld;

    invoke-virtual {v0}, Lld;->close()V

    .line 270
    return-void
.end method

.method public declared-synchronized a(Lcom/squareup/okhttp/v_1_5_1/ResponseSource;)V
    .locals 2

    .prologue
    .line 281
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/f;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/f;->f:I

    .line 283
    sget-object v0, Lcom/squareup/okhttp/v_1_5_1/f$1;->a:[I

    invoke-virtual {p1}, Lcom/squareup/okhttp/v_1_5_1/ResponseSource;->ordinal()I

    move-result v1

    aget v0, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v0, :pswitch_data_0

    .line 292
    :goto_0
    monitor-exit p0

    return-void

    .line 285
    :pswitch_0
    :try_start_1
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/f;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/f;->e:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 281
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 289
    :pswitch_1
    :try_start_2
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/f;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/f;->d:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 283
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)V
    .locals 3

    .prologue
    .line 215
    new-instance v1, Lcom/squareup/okhttp/v_1_5_1/f$c;

    invoke-direct {v1, p2}, Lcom/squareup/okhttp/v_1_5_1/f$c;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/p;)V

    .line 216
    invoke-virtual {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->h()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$a;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/v_1_5_1/f$b;

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/f$b;->a(Lcom/squareup/okhttp/v_1_5_1/f$b;)Lld$c;

    move-result-object v2

    .line 217
    const/4 v0, 0x0

    .line 219
    :try_start_0
    invoke-virtual {v2}, Lld$c;->a()Lld$a;

    move-result-object v0

    .line 220
    if-eqz v0, :cond_0

    .line 221
    invoke-virtual {v1, v0}, Lcom/squareup/okhttp/v_1_5_1/f$c;->a(Lld$a;)V

    .line 222
    invoke-virtual {v0}, Lld$a;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    :cond_0
    :goto_0
    return-void

    .line 224
    :catch_0
    move-exception v1

    .line 225
    invoke-direct {p0, v0}, Lcom/squareup/okhttp/v_1_5_1/f;->a(Lld$a;)V

    goto :goto_0
.end method

.method public declared-synchronized b()V
    .locals 1

    .prologue
    .line 295
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/f;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/f;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296
    monitor-exit p0

    return-void

    .line 295
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Z
    .locals 2

    .prologue
    .line 203
    invoke-virtual {p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/i;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/f;->a:Lld;

    invoke-static {p1}, Lcom/squareup/okhttp/v_1_5_1/f;->c(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lld;->c(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    :goto_0
    const/4 v0, 0x1

    .line 211
    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 206
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public get(Ljava/net/URI;Ljava/lang/String;Ljava/util/Map;)Ljava/net/CacheResponse;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URI;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)",
            "Ljava/net/CacheResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 134
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "This is not a general purpose response cache."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public put(Ljava/net/URI;Ljava/net/URLConnection;)Ljava/net/CacheRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "This is not a general purpose response cache."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
