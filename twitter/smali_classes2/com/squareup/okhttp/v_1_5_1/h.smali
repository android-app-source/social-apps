.class public final Lcom/squareup/okhttp/v_1_5_1/h;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Ljava/net/URLStreamHandlerFactory;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/squareup/okhttp/v_1_5_1/a;",
            "Lcom/squareup/okhttp/v_1_5_1/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/squareup/okhttp/v_1_5_1/k;

.field private c:Ljava/net/Proxy;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/squareup/okhttp/v_1_5_1/Protocol;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/net/ProxySelector;

.field private f:Ljava/net/CookieHandler;

.field private g:Lcom/squareup/okhttp/v_1_5_1/i;

.field private h:Ljavax/net/ssl/SSLSocketFactory;

.field private i:Ljavax/net/ssl/HostnameVerifier;

.field private j:Lcom/squareup/okhttp/v_1_5_1/g;

.field private k:Lcom/squareup/okhttp/v_1_5_1/d;

.field private l:Z

.field private m:I

.field private n:I

.field private volatile o:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/squareup/okhttp/v_1_5_1/internal/http/m;",
            "Lcom/squareup/okhttp/v_1_5_1/internal/http/m;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->l:Z

    .line 78
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->a:Ljava/util/Map;

    .line 79
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/k;

    invoke-direct {v0}, Lcom/squareup/okhttp/v_1_5_1/k;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->b:Lcom/squareup/okhttp/v_1_5_1/k;

    .line 80
    sget-object v0, Ljava/util/Collections;->EMPTY_MAP:Ljava/util/Map;

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->o:Ljava/util/Map;

    .line 81
    return-void
.end method

.method private b(Ljava/net/ResponseCache;)Lcom/squareup/okhttp/v_1_5_1/i;
    .locals 1

    .prologue
    .line 485
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/squareup/okhttp/v_1_5_1/i;

    if-eqz v0, :cond_1

    :cond_0
    check-cast p1, Lcom/squareup/okhttp/v_1_5_1/i;

    :goto_0
    return-object p1

    :cond_1
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;

    invoke-direct {v0, p1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/q;-><init>(Ljava/net/ResponseCache;)V

    move-object p1, v0

    goto :goto_0
.end method

.method private declared-synchronized p()Ljavax/net/ssl/SSLSocketFactory;
    .locals 4

    .prologue
    .line 463
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->h:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 465
    :try_start_1
    const-string/jumbo v0, "TLS"

    invoke-static {v0}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v0

    .line 466
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 467
    invoke-virtual {v0}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->h:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 472
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->h:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    .line 468
    :catch_0
    move-exception v0

    .line 469
    :try_start_3
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 463
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->m:I

    return v0
.end method

.method public a(Lcom/squareup/okhttp/v_1_5_1/i;)Lcom/squareup/okhttp/v_1_5_1/h;
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/h;->g:Lcom/squareup/okhttp/v_1_5_1/i;

    .line 216
    return-object p0
.end method

.method public a(Ljava/net/CookieHandler;)Lcom/squareup/okhttp/v_1_5_1/h;
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/h;->f:Ljava/net/CookieHandler;

    .line 194
    return-object p0
.end method

.method public a(Ljava/net/Proxy;)Lcom/squareup/okhttp/v_1_5_1/h;
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/h;->c:Ljava/net/Proxy;

    .line 160
    return-object p0
.end method

.method public a(Ljava/net/ResponseCache;)Lcom/squareup/okhttp/v_1_5_1/h;
    .locals 1

    .prologue
    .line 205
    invoke-direct {p0, p1}, Lcom/squareup/okhttp/v_1_5_1/h;->b(Ljava/net/ResponseCache;)Lcom/squareup/okhttp/v_1_5_1/i;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/okhttp/v_1_5_1/h;->a(Lcom/squareup/okhttp/v_1_5_1/i;)Lcom/squareup/okhttp/v_1_5_1/h;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/List;)Lcom/squareup/okhttp/v_1_5_1/h;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/okhttp/v_1_5_1/Protocol;",
            ">;)",
            "Lcom/squareup/okhttp/v_1_5_1/h;"
        }
    .end annotation

    .prologue
    .line 352
    invoke-static {p1}, Llh;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 353
    sget-object v1, Lcom/squareup/okhttp/v_1_5_1/Protocol;->c:Lcom/squareup/okhttp/v_1_5_1/Protocol;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 354
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "protocols doesn\'t contain http/1.1: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 356
    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 357
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "protocols must not contain null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359
    :cond_1
    invoke-static {v0}, Llh;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->d:Ljava/util/List;

    .line 360
    return-object p0
.end method

.method public a(Ljava/util/Map;)Lcom/squareup/okhttp/v_1_5_1/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/squareup/okhttp/v_1_5_1/internal/http/m;",
            "Lcom/squareup/okhttp/v_1_5_1/internal/http/m;",
            ">;)",
            "Lcom/squareup/okhttp/v_1_5_1/h;"
        }
    .end annotation

    .prologue
    .line 390
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->o:Ljava/util/Map;

    .line 391
    return-object p0
.end method

.method public a(Ljavax/net/ssl/HostnameVerifier;)Lcom/squareup/okhttp/v_1_5_1/h;
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/h;->i:Ljavax/net/ssl/HostnameVerifier;

    .line 247
    return-object p0
.end method

.method public a(Ljavax/net/ssl/SSLSocketFactory;)Lcom/squareup/okhttp/v_1_5_1/h;
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/h;->h:Ljavax/net/ssl/SSLSocketFactory;

    .line 230
    return-object p0
.end method

.method public a(Lcom/squareup/okhttp/v_1_5_1/internal/http/m;)Lcom/squareup/okhttp/v_1_5_1/internal/http/m;
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->o:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/m;

    return-object v0
.end method

.method public declared-synchronized a(Lcom/squareup/okhttp/v_1_5_1/a;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 88
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/v_1_5_1/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    if-eqz v0, :cond_0

    .line 93
    :goto_0
    monitor-exit p0

    return-object v0

    .line 92
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, p1

    .line 93
    goto :goto_0

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/net/URL;)Ljava/net/HttpURLConnection;
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->c:Ljava/net/Proxy;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/okhttp/v_1_5_1/h;->a(Ljava/net/URL;Ljava/net/Proxy;)Ljava/net/HttpURLConnection;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/net/URL;Ljava/net/Proxy;)Ljava/net/HttpURLConnection;
    .locals 4

    .prologue
    .line 409
    invoke-virtual {p1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    .line 410
    invoke-virtual {p0}, Lcom/squareup/okhttp/v_1_5_1/h;->n()Lcom/squareup/okhttp/v_1_5_1/h;

    move-result-object v1

    .line 411
    iput-object p2, v1, Lcom/squareup/okhttp/v_1_5_1/h;->c:Ljava/net/Proxy;

    .line 413
    const-string/jumbo v2, "http"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/HttpURLConnectionImpl;

    invoke-direct {v0, p1, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/HttpURLConnectionImpl;-><init>(Ljava/net/URL;Lcom/squareup/okhttp/v_1_5_1/h;)V

    .line 414
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v2, "https"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/k;

    invoke-direct {v0, p1, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/k;-><init>(Ljava/net/URL;Lcom/squareup/okhttp/v_1_5_1/h;)V

    goto :goto_0

    .line 415
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unexpected protocol: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public a(JLjava/util/concurrent/TimeUnit;)V
    .locals 5

    .prologue
    .line 110
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 111
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "timeout < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 113
    :cond_0
    if-nez p3, :cond_1

    .line 114
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "unit == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 116
    :cond_1
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 117
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 118
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Timeout too large."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :cond_2
    long-to-int v0, v0

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->m:I

    .line 121
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->n:I

    return v0
.end method

.method public b(JLjava/util/concurrent/TimeUnit;)V
    .locals 5

    .prologue
    .line 134
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 135
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "timeout < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :cond_0
    if-nez p3, :cond_1

    .line 138
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "unit == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :cond_1
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 141
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 142
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Timeout too large."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 144
    :cond_2
    long-to-int v0, v0

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->n:I

    .line 145
    return-void
.end method

.method public declared-synchronized b(Lcom/squareup/okhttp/v_1_5_1/a;)V
    .locals 1

    .prologue
    .line 101
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    monitor-exit p0

    return-void

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Ljava/net/Proxy;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->c:Ljava/net/Proxy;

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/squareup/okhttp/v_1_5_1/h;->o()Lcom/squareup/okhttp/v_1_5_1/h;

    move-result-object v0

    return-object v0
.end method

.method public createURLStreamHandler(Ljava/lang/String;)Ljava/net/URLStreamHandler;
    .locals 1

    .prologue
    .line 501
    const-string/jumbo v0, "http"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "https"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 503
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/h$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/okhttp/v_1_5_1/h$1;-><init>(Lcom/squareup/okhttp/v_1_5_1/h;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public d()Ljava/net/ProxySelector;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->e:Ljava/net/ProxySelector;

    return-object v0
.end method

.method public e()Ljava/net/CookieHandler;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->f:Ljava/net/CookieHandler;

    return-object v0
.end method

.method public f()Lcom/squareup/okhttp/v_1_5_1/i;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->g:Lcom/squareup/okhttp/v_1_5_1/i;

    return-object v0
.end method

.method public g()Ljavax/net/ssl/SSLSocketFactory;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->h:Ljavax/net/ssl/SSLSocketFactory;

    return-object v0
.end method

.method public h()Ljavax/net/ssl/HostnameVerifier;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->i:Ljavax/net/ssl/HostnameVerifier;

    return-object v0
.end method

.method public i()Lcom/squareup/okhttp/v_1_5_1/g;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->j:Lcom/squareup/okhttp/v_1_5_1/g;

    return-object v0
.end method

.method public j()Lcom/squareup/okhttp/v_1_5_1/d;
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->k:Lcom/squareup/okhttp/v_1_5_1/d;

    return-object v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 298
    iget-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->l:Z

    return v0
.end method

.method public l()Lcom/squareup/okhttp/v_1_5_1/k;
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->b:Lcom/squareup/okhttp/v_1_5_1/k;

    return-object v0
.end method

.method public m()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/squareup/okhttp/v_1_5_1/Protocol;",
            ">;"
        }
    .end annotation

    .prologue
    .line 378
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/h;->d:Ljava/util/List;

    return-object v0
.end method

.method n()Lcom/squareup/okhttp/v_1_5_1/h;
    .locals 2

    .prologue
    .line 423
    invoke-virtual {p0}, Lcom/squareup/okhttp/v_1_5_1/h;->o()Lcom/squareup/okhttp/v_1_5_1/h;

    move-result-object v0

    .line 424
    iget-object v1, v0, Lcom/squareup/okhttp/v_1_5_1/h;->e:Ljava/net/ProxySelector;

    if-nez v1, :cond_0

    .line 425
    invoke-static {}, Ljava/net/ProxySelector;->getDefault()Ljava/net/ProxySelector;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/okhttp/v_1_5_1/h;->e:Ljava/net/ProxySelector;

    .line 427
    :cond_0
    iget-object v1, v0, Lcom/squareup/okhttp/v_1_5_1/h;->f:Ljava/net/CookieHandler;

    if-nez v1, :cond_1

    .line 428
    invoke-static {}, Ljava/net/CookieHandler;->getDefault()Ljava/net/CookieHandler;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/okhttp/v_1_5_1/h;->f:Ljava/net/CookieHandler;

    .line 430
    :cond_1
    iget-object v1, v0, Lcom/squareup/okhttp/v_1_5_1/h;->g:Lcom/squareup/okhttp/v_1_5_1/i;

    if-nez v1, :cond_2

    .line 431
    invoke-static {}, Ljava/net/ResponseCache;->getDefault()Ljava/net/ResponseCache;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/okhttp/v_1_5_1/h;->b(Ljava/net/ResponseCache;)Lcom/squareup/okhttp/v_1_5_1/i;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/okhttp/v_1_5_1/h;->g:Lcom/squareup/okhttp/v_1_5_1/i;

    .line 433
    :cond_2
    iget-object v1, v0, Lcom/squareup/okhttp/v_1_5_1/h;->h:Ljavax/net/ssl/SSLSocketFactory;

    if-nez v1, :cond_3

    .line 434
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/h;->p()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/okhttp/v_1_5_1/h;->h:Ljavax/net/ssl/SSLSocketFactory;

    .line 436
    :cond_3
    iget-object v1, v0, Lcom/squareup/okhttp/v_1_5_1/h;->i:Ljavax/net/ssl/HostnameVerifier;

    if-nez v1, :cond_4

    .line 437
    sget-object v1, Lma;->a:Lma;

    iput-object v1, v0, Lcom/squareup/okhttp/v_1_5_1/h;->i:Ljavax/net/ssl/HostnameVerifier;

    .line 439
    :cond_4
    iget-object v1, v0, Lcom/squareup/okhttp/v_1_5_1/h;->j:Lcom/squareup/okhttp/v_1_5_1/g;

    if-nez v1, :cond_5

    .line 440
    sget-object v1, Lcom/squareup/okhttp/v_1_5_1/internal/http/e;->a:Lcom/squareup/okhttp/v_1_5_1/g;

    iput-object v1, v0, Lcom/squareup/okhttp/v_1_5_1/h;->j:Lcom/squareup/okhttp/v_1_5_1/g;

    .line 442
    :cond_5
    iget-object v1, v0, Lcom/squareup/okhttp/v_1_5_1/h;->k:Lcom/squareup/okhttp/v_1_5_1/d;

    if-nez v1, :cond_6

    .line 443
    invoke-static {}, Lcom/squareup/okhttp/v_1_5_1/d;->a()Lcom/squareup/okhttp/v_1_5_1/d;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/okhttp/v_1_5_1/h;->k:Lcom/squareup/okhttp/v_1_5_1/d;

    .line 445
    :cond_6
    iget-object v1, v0, Lcom/squareup/okhttp/v_1_5_1/h;->d:Ljava/util/List;

    if-nez v1, :cond_7

    .line 446
    sget-object v1, Llh;->f:Ljava/util/List;

    iput-object v1, v0, Lcom/squareup/okhttp/v_1_5_1/h;->d:Ljava/util/List;

    .line 448
    :cond_7
    return-object v0
.end method

.method public o()Lcom/squareup/okhttp/v_1_5_1/h;
    .locals 1

    .prologue
    .line 478
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/v_1_5_1/h;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 479
    :catch_0
    move-exception v0

    .line 480
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method
