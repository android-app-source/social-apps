.class final Lcom/squareup/okhttp/v_1_5_1/f$a;
.super Ljava/net/CacheRequest;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/okhttp/v_1_5_1/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/squareup/okhttp/v_1_5_1/f;

.field private final b:Lld$a;

.field private c:Ljava/io/OutputStream;

.field private d:Z

.field private e:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Lcom/squareup/okhttp/v_1_5_1/f;Lld$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 316
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/f$a;->a:Lcom/squareup/okhttp/v_1_5_1/f;

    invoke-direct {p0}, Ljava/net/CacheRequest;-><init>()V

    .line 317
    iput-object p2, p0, Lcom/squareup/okhttp/v_1_5_1/f$a;->b:Lld$a;

    .line 318
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lld$a;->a(I)Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/f$a;->c:Ljava/io/OutputStream;

    .line 319
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/f$a$1;

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/f$a;->c:Ljava/io/OutputStream;

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/squareup/okhttp/v_1_5_1/f$a$1;-><init>(Lcom/squareup/okhttp/v_1_5_1/f$a;Ljava/io/OutputStream;Lcom/squareup/okhttp/v_1_5_1/f;Lld$a;)V

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/f$a;->e:Ljava/io/OutputStream;

    .line 338
    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/v_1_5_1/f$a;)Z
    .locals 1

    .prologue
    .line 310
    iget-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/f$a;->d:Z

    return v0
.end method

.method static synthetic a(Lcom/squareup/okhttp/v_1_5_1/f$a;Z)Z
    .locals 0

    .prologue
    .line 310
    iput-boolean p1, p0, Lcom/squareup/okhttp/v_1_5_1/f$a;->d:Z

    return p1
.end method


# virtual methods
.method public abort()V
    .locals 2

    .prologue
    .line 341
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/f$a;->a:Lcom/squareup/okhttp/v_1_5_1/f;

    monitor-enter v1

    .line 342
    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/f$a;->d:Z

    if-eqz v0, :cond_0

    .line 343
    monitor-exit v1

    .line 353
    :goto_0
    return-void

    .line 345
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/f$a;->d:Z

    .line 346
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/f$a;->a:Lcom/squareup/okhttp/v_1_5_1/f;

    invoke-static {v0}, Lcom/squareup/okhttp/v_1_5_1/f;->b(Lcom/squareup/okhttp/v_1_5_1/f;)I

    .line 347
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 348
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/f$a;->c:Ljava/io/OutputStream;

    invoke-static {v0}, Llh;->a(Ljava/io/Closeable;)V

    .line 350
    :try_start_1
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/f$a;->b:Lld$a;

    invoke-virtual {v0}, Lld$a;->b()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 351
    :catch_0
    move-exception v0

    goto :goto_0

    .line 347
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public getBody()Ljava/io/OutputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 356
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/f$a;->e:Ljava/io/OutputStream;

    return-object v0
.end method
