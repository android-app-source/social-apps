.class public final Lcom/squareup/okhttp/v_1_5_1/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final a:Lcom/squareup/okhttp/v_1_5_1/d;

.field private final b:Lcom/squareup/okhttp/v_1_5_1/j;

.field private c:Ljava/net/Socket;

.field private d:Ljava/io/InputStream;

.field private e:Ljava/io/OutputStream;

.field private f:Llk;

.field private g:Llj;

.field private h:Z

.field private i:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

.field private j:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;

.field private k:I

.field private l:J

.field private m:Lcom/squareup/okhttp/v_1_5_1/e;

.field private n:I


# direct methods
.method public constructor <init>(Lcom/squareup/okhttp/v_1_5_1/d;Lcom/squareup/okhttp/v_1_5_1/j;)V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->h:Z

    .line 84
    const/4 v0, 0x1

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->k:I

    .line 90
    iput-object p1, p0, Lcom/squareup/okhttp/v_1_5_1/c;->a:Lcom/squareup/okhttp/v_1_5_1/d;

    .line 91
    iput-object p2, p0, Lcom/squareup/okhttp/v_1_5_1/c;->b:Lcom/squareup/okhttp/v_1_5_1/j;

    .line 92
    return-void
.end method

.method private a(Lcom/squareup/okhttp/v_1_5_1/l;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 118
    invoke-static {}, Llg;->a()Llg;

    move-result-object v1

    .line 121
    invoke-virtual {p0}, Lcom/squareup/okhttp/v_1_5_1/c;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    invoke-direct {p0, p1}, Lcom/squareup/okhttp/v_1_5_1/c;->b(Lcom/squareup/okhttp/v_1_5_1/l;)V

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->b:Lcom/squareup/okhttp/v_1_5_1/j;

    iget-object v0, v0, Lcom/squareup/okhttp/v_1_5_1/j;->a:Lcom/squareup/okhttp/v_1_5_1/a;

    iget-object v0, v0, Lcom/squareup/okhttp/v_1_5_1/a;->d:Ljavax/net/ssl/SSLSocketFactory;

    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/c;->c:Ljava/net/Socket;

    iget-object v3, p0, Lcom/squareup/okhttp/v_1_5_1/c;->b:Lcom/squareup/okhttp/v_1_5_1/j;

    iget-object v3, v3, Lcom/squareup/okhttp/v_1_5_1/j;->a:Lcom/squareup/okhttp/v_1_5_1/a;

    iget-object v3, v3, Lcom/squareup/okhttp/v_1_5_1/a;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/okhttp/v_1_5_1/c;->b:Lcom/squareup/okhttp/v_1_5_1/j;

    iget-object v4, v4, Lcom/squareup/okhttp/v_1_5_1/j;->a:Lcom/squareup/okhttp/v_1_5_1/a;

    iget v4, v4, Lcom/squareup/okhttp/v_1_5_1/a;->c:I

    invoke-virtual {v0, v2, v3, v4, v5}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->c:Ljava/net/Socket;

    .line 128
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->c:Ljava/net/Socket;

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    .line 129
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/c;->b:Lcom/squareup/okhttp/v_1_5_1/j;

    iget-boolean v2, v2, Lcom/squareup/okhttp/v_1_5_1/j;->d:Z

    if-eqz v2, :cond_1

    .line 130
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/c;->b:Lcom/squareup/okhttp/v_1_5_1/j;

    iget-object v2, v2, Lcom/squareup/okhttp/v_1_5_1/j;->a:Lcom/squareup/okhttp/v_1_5_1/a;

    iget-object v2, v2, Lcom/squareup/okhttp/v_1_5_1/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Llg;->a(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;)V

    .line 139
    :goto_0
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/c;->b:Lcom/squareup/okhttp/v_1_5_1/j;

    iget-object v1, v1, Lcom/squareup/okhttp/v_1_5_1/j;->a:Lcom/squareup/okhttp/v_1_5_1/a;

    iget-object v1, v1, Lcom/squareup/okhttp/v_1_5_1/a;->g:Ljava/util/List;

    sget-object v2, Lcom/squareup/okhttp/v_1_5_1/Protocol;->b:Lcom/squareup/okhttp/v_1_5_1/Protocol;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    .line 153
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->startHandshake()V

    .line 156
    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/c;->b:Lcom/squareup/okhttp/v_1_5_1/j;

    iget-object v1, v1, Lcom/squareup/okhttp/v_1_5_1/j;->a:Lcom/squareup/okhttp/v_1_5_1/a;

    iget-object v1, v1, Lcom/squareup/okhttp/v_1_5_1/a;->e:Ljavax/net/ssl/HostnameVerifier;

    iget-object v3, p0, Lcom/squareup/okhttp/v_1_5_1/c;->b:Lcom/squareup/okhttp/v_1_5_1/j;

    iget-object v3, v3, Lcom/squareup/okhttp/v_1_5_1/j;->a:Lcom/squareup/okhttp/v_1_5_1/a;

    iget-object v3, v3, Lcom/squareup/okhttp/v_1_5_1/a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljavax/net/ssl/HostnameVerifier;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSession;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 157
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Hostname \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/c;->b:Lcom/squareup/okhttp/v_1_5_1/j;

    iget-object v2, v2, Lcom/squareup/okhttp/v_1_5_1/j;->a:Lcom/squareup/okhttp/v_1_5_1/a;

    iget-object v2, v2, Lcom/squareup/okhttp/v_1_5_1/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\' was not verified"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 132
    :cond_1
    invoke-virtual {v1, v0}, Llg;->a(Ljavax/net/ssl/SSLSocket;)V

    goto :goto_0

    .line 160
    :cond_2
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/c;->e:Ljava/io/OutputStream;

    .line 161
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/c;->d:Ljava/io/InputStream;

    .line 162
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/okhttp/v_1_5_1/e;->a(Ljavax/net/ssl/SSLSession;)Lcom/squareup/okhttp/v_1_5_1/e;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/c;->m:Lcom/squareup/okhttp/v_1_5_1/e;

    .line 163
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/c;->o()V

    .line 166
    sget-object v1, Lcom/squareup/okhttp/v_1_5_1/Protocol;->c:Lcom/squareup/okhttp/v_1_5_1/Protocol;

    .line 169
    if-eqz v2, :cond_3

    .line 170
    sget-object v1, Lcom/squareup/okhttp/v_1_5_1/Protocol;->b:Lcom/squareup/okhttp/v_1_5_1/Protocol;

    .line 173
    :cond_3
    iget-boolean v2, v1, Lcom/squareup/okhttp/v_1_5_1/Protocol;->spdyVariant:Z

    if-eqz v2, :cond_4

    .line 174
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V

    .line 175
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;

    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/c;->b:Lcom/squareup/okhttp/v_1_5_1/j;

    iget-object v2, v2, Lcom/squareup/okhttp/v_1_5_1/j;->a:Lcom/squareup/okhttp/v_1_5_1/a;

    invoke-virtual {v2}, Lcom/squareup/okhttp/v_1_5_1/a;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/okhttp/v_1_5_1/c;->f:Llk;

    iget-object v4, p0, Lcom/squareup/okhttp/v_1_5_1/c;->g:Llj;

    invoke-direct {v0, v2, v5, v3, v4}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;-><init>(Ljava/lang/String;ZLlk;Llj;)V

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->a(Lcom/squareup/okhttp/v_1_5_1/Protocol;)Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l$a;->a()Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->j:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;

    .line 177
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->j:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->e()V

    .line 181
    :goto_1
    return-void

    .line 179
    :cond_4
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/c;->a:Lcom/squareup/okhttp/v_1_5_1/d;

    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/c;->f:Llk;

    iget-object v3, p0, Lcom/squareup/okhttp/v_1_5_1/c;->g:Llj;

    invoke-direct {v0, v1, p0, v2, v3}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;-><init>(Lcom/squareup/okhttp/v_1_5_1/d;Lcom/squareup/okhttp/v_1_5_1/c;Llk;Llj;)V

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->i:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    goto :goto_1
.end method

.method private b(Lcom/squareup/okhttp/v_1_5_1/l;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 331
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->d:Ljava/io/InputStream;

    invoke-static {v0}, Llr;->a(Ljava/io/InputStream;)Llx;

    move-result-object v0

    invoke-static {v0}, Llr;->a(Llx;)Llk;

    move-result-object v1

    .line 332
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->e:Ljava/io/OutputStream;

    invoke-static {v0}, Llr;->a(Ljava/io/OutputStream;)Llw;

    move-result-object v0

    invoke-static {v0}, Llr;->a(Llw;)Llj;

    move-result-object v0

    .line 333
    new-instance v2, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    iget-object v3, p0, Lcom/squareup/okhttp/v_1_5_1/c;->a:Lcom/squareup/okhttp/v_1_5_1/d;

    invoke-direct {v2, v3, p0, v1, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;-><init>(Lcom/squareup/okhttp/v_1_5_1/d;Lcom/squareup/okhttp/v_1_5_1/c;Llk;Llj;)V

    .line 334
    invoke-virtual {p1}, Lcom/squareup/okhttp/v_1_5_1/l;->b()Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    move-result-object v0

    .line 335
    invoke-virtual {p1}, Lcom/squareup/okhttp/v_1_5_1/l;->a()Ljava/lang/String;

    move-result-object v3

    .line 337
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/n;->e()Lcom/squareup/okhttp/v_1_5_1/internal/http/d;

    move-result-object v4

    invoke-virtual {v2, v4, v3}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/d;Ljava/lang/String;)V

    .line 338
    invoke-virtual {v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->d()V

    .line 339
    invoke-virtual {v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->e()Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->a(Lcom/squareup/okhttp/v_1_5_1/internal/http/n;)Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p$b;->a()Lcom/squareup/okhttp/v_1_5_1/internal/http/p;

    move-result-object v0

    .line 340
    invoke-virtual {v2}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;->g()V

    .line 342
    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->c()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 358
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unexpected response code for CONNECT: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/http/p;->c()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 346
    :sswitch_0
    invoke-interface {v1}, Llk;->b()Llq;

    move-result-object v0

    invoke-virtual {v0}, Llq;->l()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 347
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "TLS tunnel buffered too many bytes!"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 352
    :sswitch_1
    iget-object v4, p0, Lcom/squareup/okhttp/v_1_5_1/c;->b:Lcom/squareup/okhttp/v_1_5_1/j;

    iget-object v4, v4, Lcom/squareup/okhttp/v_1_5_1/j;->a:Lcom/squareup/okhttp/v_1_5_1/a;

    iget-object v4, v4, Lcom/squareup/okhttp/v_1_5_1/a;->f:Lcom/squareup/okhttp/v_1_5_1/g;

    iget-object v5, p0, Lcom/squareup/okhttp/v_1_5_1/c;->b:Lcom/squareup/okhttp/v_1_5_1/j;

    iget-object v5, v5, Lcom/squareup/okhttp/v_1_5_1/j;->b:Ljava/net/Proxy;

    invoke-static {v4, v0, v5}, Lcom/squareup/okhttp/v_1_5_1/internal/http/e;->a(Lcom/squareup/okhttp/v_1_5_1/g;Lcom/squareup/okhttp/v_1_5_1/internal/http/p;Ljava/net/Proxy;)Lcom/squareup/okhttp/v_1_5_1/internal/http/n;

    move-result-object v0

    .line 354
    if-nez v0, :cond_0

    .line 355
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "Failed to authenticate with proxy"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 349
    :cond_1
    return-void

    .line 342
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x197 -> :sswitch_1
    .end sparse-switch
.end method

.method private o()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 365
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->d:Ljava/io/InputStream;

    invoke-static {v0}, Llr;->a(Ljava/io/InputStream;)Llx;

    move-result-object v0

    invoke-static {v0}, Llr;->a(Llx;)Llk;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->f:Llk;

    .line 366
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->e:Ljava/io/OutputStream;

    invoke-static {v0}, Llr;->a(Ljava/io/OutputStream;)Llw;

    move-result-object v0

    invoke-static {v0}, Llr;->a(Llw;)Llj;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->g:Llj;

    .line 367
    return-void
.end method


# virtual methods
.method public a(Lcom/squareup/okhttp/v_1_5_1/internal/http/h;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 273
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->j:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/t;

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/c;->j:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;

    invoke-direct {v0, p1, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/t;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/h;Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/j;

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/c;->i:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    invoke-direct {v0, p1, v1}, Lcom/squareup/okhttp/v_1_5_1/internal/http/j;-><init>(Lcom/squareup/okhttp/v_1_5_1/internal/http/h;Lcom/squareup/okhttp/v_1_5_1/internal/http/f;)V

    goto :goto_0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 296
    iput p1, p0, Lcom/squareup/okhttp/v_1_5_1/c;->k:I

    .line 297
    return-void
.end method

.method public a(IILcom/squareup/okhttp/v_1_5_1/l;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->h:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "already connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->b:Lcom/squareup/okhttp/v_1_5_1/j;

    iget-object v0, v0, Lcom/squareup/okhttp/v_1_5_1/j;->b:Ljava/net/Proxy;

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/net/Socket;

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/c;->b:Lcom/squareup/okhttp/v_1_5_1/j;

    iget-object v1, v1, Lcom/squareup/okhttp/v_1_5_1/j;->b:Ljava/net/Proxy;

    invoke-direct {v0, v1}, Ljava/net/Socket;-><init>(Ljava/net/Proxy;)V

    :goto_0
    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->c:Ljava/net/Socket;

    .line 99
    invoke-static {}, Llg;->a()Llg;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/c;->c:Ljava/net/Socket;

    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/c;->b:Lcom/squareup/okhttp/v_1_5_1/j;

    iget-object v2, v2, Lcom/squareup/okhttp/v_1_5_1/j;->c:Ljava/net/InetSocketAddress;

    invoke-virtual {v0, v1, v2, p1}, Llg;->a(Ljava/net/Socket;Ljava/net/InetSocketAddress;I)V

    .line 100
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->c:Ljava/net/Socket;

    invoke-virtual {v0, p2}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 101
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->d:Ljava/io/InputStream;

    .line 102
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->e:Ljava/io/OutputStream;

    .line 104
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->b:Lcom/squareup/okhttp/v_1_5_1/j;

    iget-object v0, v0, Lcom/squareup/okhttp/v_1_5_1/j;->a:Lcom/squareup/okhttp/v_1_5_1/a;

    iget-object v0, v0, Lcom/squareup/okhttp/v_1_5_1/a;->d:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v0, :cond_2

    .line 105
    invoke-direct {p0, p3}, Lcom/squareup/okhttp/v_1_5_1/c;->a(Lcom/squareup/okhttp/v_1_5_1/l;)V

    .line 110
    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->h:Z

    .line 111
    return-void

    .line 98
    :cond_1
    new-instance v0, Ljava/net/Socket;

    invoke-direct {v0}, Ljava/net/Socket;-><init>()V

    goto :goto_0

    .line 107
    :cond_2
    invoke-direct {p0}, Lcom/squareup/okhttp/v_1_5_1/c;->o()V

    .line 108
    new-instance v0, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    iget-object v1, p0, Lcom/squareup/okhttp/v_1_5_1/c;->a:Lcom/squareup/okhttp/v_1_5_1/d;

    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/c;->f:Llk;

    iget-object v3, p0, Lcom/squareup/okhttp/v_1_5_1/c;->g:Llj;

    invoke-direct {v0, v1, p0, v2, v3}, Lcom/squareup/okhttp/v_1_5_1/internal/http/f;-><init>(Lcom/squareup/okhttp/v_1_5_1/d;Lcom/squareup/okhttp/v_1_5_1/c;Llk;Llj;)V

    iput-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->i:Lcom/squareup/okhttp/v_1_5_1/internal/http/f;

    goto :goto_1
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 185
    iget-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->h:Z

    return v0
.end method

.method public a(J)Z
    .locals 5

    .prologue
    .line 256
    invoke-virtual {p0}, Lcom/squareup/okhttp/v_1_5_1/c;->h()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    sub-long/2addr v2, p1

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lcom/squareup/okhttp/v_1_5_1/j;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->b:Lcom/squareup/okhttp/v_1_5_1/j;

    return-object v0
.end method

.method public b(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 309
    iget-boolean v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->h:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "updateReadTimeout - not connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 310
    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->c:Ljava/net/Socket;

    invoke-virtual {v0, p1}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 311
    return-void
.end method

.method public c()Ljava/net/Socket;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->c:Ljava/net/Socket;

    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 189
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    .line 190
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isInputShutdown()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isOutputShutdown()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/okhttp/v_1_5_1/c;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->j:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->f()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 217
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/c;->f:Llk;

    if-nez v2, :cond_1

    .line 237
    :cond_0
    :goto_0
    return v0

    .line 220
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/okhttp/v_1_5_1/c;->j()Z

    move-result v2

    if-nez v2, :cond_0

    .line 224
    :try_start_0
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/c;->c:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getSoTimeout()I
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    .line 226
    :try_start_1
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/c;->c:Ljava/net/Socket;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 227
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/c;->f:Llk;

    invoke-interface {v2}, Llk;->e()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-eqz v2, :cond_2

    .line 232
    :try_start_2
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/c;->c:Ljava/net/Socket;

    invoke-virtual {v2, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/squareup/okhttp/v_1_5_1/c;->c:Ljava/net/Socket;

    invoke-virtual {v2, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    goto :goto_0

    .line 234
    :catch_0
    move-exception v1

    goto :goto_0

    .line 232
    :catchall_0
    move-exception v2

    iget-object v4, p0, Lcom/squareup/okhttp/v_1_5_1/c;->c:Ljava/net/Socket;

    invoke-virtual {v4, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    throw v2
    :try_end_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 236
    :catch_1
    move-exception v0

    move v0, v1

    .line 237
    goto :goto_0
.end method

.method public f()V
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->j:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "spdyConnection != null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 243
    :cond_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->l:J

    .line 244
    return-void
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->j:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->j:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()J
    .locals 2

    .prologue
    .line 264
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->j:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->l:J

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->j:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;

    invoke-virtual {v0}, Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;->c()J

    move-result-wide v0

    goto :goto_0
.end method

.method public i()Lcom/squareup/okhttp/v_1_5_1/e;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->m:Lcom/squareup/okhttp/v_1_5_1/e;

    return-object v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->j:Lcom/squareup/okhttp/v_1_5_1/internal/spdy/l;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 292
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->k:I

    return v0
.end method

.method public l()Z
    .locals 2

    .prologue
    .line 305
    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->b:Lcom/squareup/okhttp/v_1_5_1/j;

    iget-object v0, v0, Lcom/squareup/okhttp/v_1_5_1/j;->a:Lcom/squareup/okhttp/v_1_5_1/a;

    iget-object v0, v0, Lcom/squareup/okhttp/v_1_5_1/a;->d:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->b:Lcom/squareup/okhttp/v_1_5_1/j;

    iget-object v0, v0, Lcom/squareup/okhttp/v_1_5_1/j;->b:Ljava/net/Proxy;

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()V
    .locals 1

    .prologue
    .line 314
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->n:I

    .line 315
    return-void
.end method

.method public n()I
    .locals 1

    .prologue
    .line 322
    iget v0, p0, Lcom/squareup/okhttp/v_1_5_1/c;->n:I

    return v0
.end method
