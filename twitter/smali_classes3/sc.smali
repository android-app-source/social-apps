.class public Lsc;
.super Lse;
.source "Twttr"


# instance fields
.field private a:Z

.field private b:Lcom/twitter/android/composer/o;

.field private c:Lcom/twitter/library/client/Session;

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/twitter/android/composer/mediarail/view/MediaRailView;Landroid/support/v4/app/LoaderManager;ILcom/twitter/android/composer/o;Lcom/twitter/library/client/Session;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Lse;-><init>(Lcom/twitter/android/composer/mediarail/view/MediaRailView;Landroid/support/v4/app/LoaderManager;I)V

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsc;->a:Z

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsc;->d:Z

    .line 29
    iput-object p4, p0, Lsc;->b:Lcom/twitter/android/composer/o;

    .line 30
    iput-object p5, p0, Lsc;->c:Lcom/twitter/library/client/Session;

    .line 31
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 99
    invoke-super {p0, p1}, Lse;->a(Landroid/os/Bundle;)V

    .line 100
    if-eqz p1, :cond_0

    .line 101
    const-string/jumbo v0, "should_reappear"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsc;->a:Z

    .line 103
    :cond_0
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-super {p0, p1, p2}, Lse;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    .line 51
    invoke-virtual {p0}, Lsc;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lsc;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsc;->c:Lcom/twitter/library/client/Session;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lsc;->b:Lcom/twitter/android/composer/o;

    iget-object v1, p0, Lsc;->c:Lcom/twitter/library/client/Session;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/o;->a(Lcom/twitter/library/client/Session;)V

    .line 54
    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lsc;->c:Lcom/twitter/library/client/Session;

    .line 58
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 45
    iput-boolean p1, p0, Lsc;->a:Z

    .line 46
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lsc;->a:Z

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsc;->a:Z

    .line 63
    invoke-super {p0}, Lse;->b()V

    .line 64
    return-void
.end method

.method public b(Z)V
    .locals 4

    .prologue
    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsc;->a:Z

    .line 75
    if-nez p1, :cond_0

    .line 78
    invoke-virtual {p0}, Lsc;->f()Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    move-result-object v0

    new-instance v1, Lsc$1;

    invoke-direct {v1, p0}, Lsc$1;-><init>(Lsc;)V

    const-wide/16 v2, 0x15e

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 87
    :goto_0
    return-void

    .line 85
    :cond_0
    invoke-virtual {p0}, Lsc;->b()V

    goto :goto_0
.end method

.method public c()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 92
    invoke-super {p0}, Lse;->c()Landroid/os/Bundle;

    move-result-object v0

    .line 93
    const-string/jumbo v1, "should_reappear"

    iget-boolean v2, p0, Lsc;->a:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 94
    return-object v0
.end method

.method protected d()Z
    .locals 1

    .prologue
    .line 107
    invoke-super {p0}, Lse;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    invoke-static {}, Lbpo;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 107
    :goto_0
    return v0

    .line 108
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected e()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 113
    invoke-super {p0}, Lse;->e()V

    .line 115
    invoke-virtual {p0}, Lsc;->f()Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 116
    invoke-static {}, Lbpo;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 117
    new-instance v1, Lsf;

    const v2, 0x7f0a0524

    .line 118
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f02080e

    const/4 v4, 0x0

    const v5, 0x7f130054

    invoke-direct {v1, v2, v3, v4, v5}, Lsf;-><init>(Ljava/lang/String;III)V

    .line 120
    invoke-virtual {p0}, Lsc;->f()Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->a(Lsg;)V

    .line 122
    :cond_0
    invoke-static {}, Lbpo;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 123
    new-instance v1, Lsf;

    const v2, 0x7f0a0525

    .line 124
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f020811

    const v4, 0x7f130055

    invoke-direct {v1, v2, v3, v6, v4}, Lsf;-><init>(Ljava/lang/String;III)V

    .line 126
    invoke-virtual {p0}, Lsc;->f()Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->a(Lsg;)V

    .line 128
    :cond_1
    invoke-static {}, Lbpo;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 129
    iput-boolean v6, p0, Lsc;->d:Z

    .line 130
    new-instance v1, Lsf;

    const v2, 0x7f0a0523

    .line 131
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f020810

    const/4 v4, 0x2

    const v5, 0x7f130053

    invoke-direct {v1, v2, v3, v4, v5}, Lsf;-><init>(Ljava/lang/String;III)V

    .line 133
    invoke-virtual {p0}, Lsc;->f()Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->a(Lsg;)V

    .line 136
    :cond_2
    new-instance v1, Lsf;

    const v2, 0x7f0a0522

    .line 137
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f020839

    const/4 v3, 0x3

    const v4, 0x7f130052

    invoke-direct {v1, v0, v2, v3, v4}, Lsf;-><init>(Ljava/lang/String;III)V

    .line 139
    invoke-virtual {p0}, Lsc;->f()Lcom/twitter/android/composer/mediarail/view/MediaRailView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/mediarail/view/MediaRailView;->b(Lsg;)V

    .line 140
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lsc;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method
