.class public Lcub;
.super Lorg/spongycastle/asn1/k;
.source "Twttr"


# instance fields
.field private a:Lorg/spongycastle/asn1/m;

.field private b:Lcuf;

.field private c:Lorg/spongycastle/asn1/s;


# direct methods
.method public constructor <init>(Lcuf;Lorg/spongycastle/asn1/d;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcub;-><init>(Lcuf;Lorg/spongycastle/asn1/d;Lorg/spongycastle/asn1/s;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Lcuf;Lorg/spongycastle/asn1/d;Lorg/spongycastle/asn1/s;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 65
    new-instance v0, Lorg/spongycastle/asn1/au;

    invoke-interface {p2}, Lorg/spongycastle/asn1/d;->c()Lorg/spongycastle/asn1/p;

    move-result-object v1

    const-string/jumbo v2, "DER"

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/p;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/au;-><init>([B)V

    iput-object v0, p0, Lcub;->a:Lorg/spongycastle/asn1/m;

    .line 66
    iput-object p1, p0, Lcub;->b:Lcuf;

    .line 67
    iput-object p3, p0, Lcub;->c:Lorg/spongycastle/asn1/s;

    .line 68
    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/q;)V
    .locals 2

    .prologue
    .line 76
    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    .line 77
    invoke-virtual {p1}, Lorg/spongycastle/asn1/q;->e()Ljava/util/Enumeration;

    move-result-object v1

    .line 79
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->d()Ljava/math/BigInteger;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "wrong version for private key info"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcuf;->a(Ljava/lang/Object;)Lcuf;

    move-result-object v0

    iput-object v0, p0, Lcub;->b:Lcuf;

    .line 86
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/m;->a(Ljava/lang/Object;)Lorg/spongycastle/asn1/m;

    move-result-object v0

    iput-object v0, p0, Lcub;->a:Lorg/spongycastle/asn1/m;

    .line 88
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/v;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/spongycastle/asn1/s;->a(Lorg/spongycastle/asn1/v;Z)Lorg/spongycastle/asn1/s;

    move-result-object v0

    iput-object v0, p0, Lcub;->c:Lorg/spongycastle/asn1/s;

    .line 92
    :cond_1
    return-void
.end method

.method public static a(Ljava/lang/Object;)Lcub;
    .locals 2

    .prologue
    .line 39
    instance-of v0, p0, Lcub;

    if-eqz v0, :cond_0

    .line 41
    check-cast p0, Lcub;

    .line 48
    :goto_0
    return-object p0

    .line 43
    :cond_0
    if-eqz p0, :cond_1

    .line 45
    new-instance v0, Lcub;

    invoke-static {p0}, Lorg/spongycastle/asn1/q;->a(Ljava/lang/Object;)Lorg/spongycastle/asn1/q;

    move-result-object v1

    invoke-direct {v0, v1}, Lcub;-><init>(Lorg/spongycastle/asn1/q;)V

    move-object p0, v0

    goto :goto_0

    .line 48
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Lcuf;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcub;->b:Lcuf;

    return-object v0
.end method

.method public b()Lorg/spongycastle/asn1/d;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lcub;->a:Lorg/spongycastle/asn1/m;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m;->e()[B

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/p;->b([B)Lorg/spongycastle/asn1/p;

    move-result-object v0

    return-object v0
.end method

.method public c()Lorg/spongycastle/asn1/p;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 151
    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    .line 153
    new-instance v1, Lorg/spongycastle/asn1/i;

    const-wide/16 v2, 0x0

    invoke-direct {v1, v2, v3}, Lorg/spongycastle/asn1/i;-><init>(J)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 154
    iget-object v1, p0, Lcub;->b:Lcuf;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 155
    iget-object v1, p0, Lcub;->a:Lorg/spongycastle/asn1/m;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 157
    iget-object v1, p0, Lcub;->c:Lorg/spongycastle/asn1/s;

    if-eqz v1, :cond_0

    .line 159
    new-instance v1, Lorg/spongycastle/asn1/bd;

    iget-object v2, p0, Lcub;->c:Lorg/spongycastle/asn1/s;

    invoke-direct {v1, v4, v4, v2}, Lorg/spongycastle/asn1/bd;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    .line 162
    :cond_0
    new-instance v1, Lorg/spongycastle/asn1/ay;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/ay;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method
