.class public Lso;
.super Lsl;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lso$a;
    }
.end annotation


# instance fields
.field h:Lcom/twitter/android/media/stickers/StickerMediaView;

.field private final i:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;ILsp;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            "I",
            "Lsp;",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/media/ui/image/MediaImageView;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct/range {p0 .. p6}, Lsl;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;ILsp;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/util/List;)V

    .line 51
    iput-object p1, p0, Lso;->i:Landroid/content/Context;

    .line 52
    iget-object v0, p0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    invoke-virtual {v0, p7}, Lcom/twitter/android/media/stickers/StickerMediaView;->setShouldShowStickerVisualHashtags(Z)V

    .line 55
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 74
    iget-object v0, p0, Lso;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    const v0, 0x7f040284

    invoke-virtual {p1, v0, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    .line 79
    :goto_0
    invoke-static {}, Lbpt;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 80
    new-instance v1, Lcom/twitter/android/media/stickers/StickerMediaView;

    invoke-direct {v1, p2}, Lcom/twitter/android/media/stickers/StickerMediaView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    .line 81
    iget-object v1, p0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->addView(Landroid/view/View;)V

    .line 83
    :cond_0
    return-object v0

    .line 77
    :cond_1
    iget-object v0, p0, Lso;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    goto :goto_0
.end method

.method a(Lsn;)V
    .locals 10

    .prologue
    .line 139
    iget-object v0, p0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    if-eqz v0, :cond_1

    .line 140
    iget-object v2, p1, Lsn;->b:Lcom/twitter/model/core/MediaEntity;

    .line 141
    if-eqz v2, :cond_1

    iget-object v0, v2, Lcom/twitter/model/core/MediaEntity;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v2}, Lcom/twitter/model/util/c;->f(Lcom/twitter/model/core/MediaEntity;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 142
    invoke-virtual {p0}, Lso;->f()Landroid/widget/ImageView;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/MultiTouchImageView;

    .line 144
    invoke-virtual {p1}, Lsn;->a()Lcom/twitter/model/core/Tweet;

    move-result-object v3

    .line 146
    if-eqz v3, :cond_2

    .line 147
    iget-object v1, p0, Lso;->i:Landroid/content/Context;

    check-cast v1, Lcom/twitter/android/GalleryActivity;

    .line 148
    invoke-virtual {v1, v3}, Lcom/twitter/android/GalleryActivity;->a(Lcom/twitter/model/core/Tweet;)Ljava/util/Map;

    move-result-object v1

    .line 149
    iget-wide v4, v2, Lcom/twitter/model/core/MediaEntity;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbrm;

    .line 153
    :goto_0
    if-eqz v1, :cond_1

    .line 154
    invoke-virtual {v0}, Lcom/twitter/ui/widget/MultiTouchImageView;->getActiveRect()Landroid/graphics/RectF;

    move-result-object v4

    .line 155
    iget-object v5, p0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    iget v6, v4, Landroid/graphics/RectF;->left:F

    .line 156
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iget v7, v4, Landroid/graphics/RectF;->top:F

    .line 157
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 158
    invoke-virtual {v0}, Lcom/twitter/ui/widget/MultiTouchImageView;->getRight()I

    move-result v8

    int-to-float v8, v8

    iget v9, v4, Landroid/graphics/RectF;->right:F

    sub-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    .line 159
    invoke-virtual {v0}, Lcom/twitter/ui/widget/MultiTouchImageView;->getBottom()I

    move-result v9

    int-to-float v9, v9

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    sub-float v4, v9, v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 155
    invoke-virtual {v5, v6, v7, v8, v4}, Lcom/twitter/android/media/stickers/StickerMediaView;->setPadding(IIII)V

    .line 160
    iget-object v4, p0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    iget-object v2, v2, Lcom/twitter/model/core/MediaEntity;->s:Ljava/util/List;

    invoke-virtual {v4, v2, v1, v0}, Lcom/twitter/android/media/stickers/StickerMediaView;->a(Ljava/util/List;Lbrm;Lcom/twitter/ui/widget/MultiTouchImageView;)V

    .line 162
    if-nez v3, :cond_0

    .line 163
    iget-object v1, p0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/twitter/android/media/stickers/StickerMediaView;->a(Z)V

    .line 165
    :cond_0
    iget-object v1, p0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    invoke-virtual {v1}, Lcom/twitter/android/media/stickers/StickerMediaView;->requestLayout()V

    .line 166
    new-instance v1, Lso$3;

    invoke-direct {v1, p0, v0}, Lso$3;-><init>(Lso;Lcom/twitter/ui/widget/MultiTouchImageView;)V

    invoke-virtual {v0, v1}, Lcom/twitter/ui/widget/MultiTouchImageView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 188
    :cond_1
    return-void

    .line 151
    :cond_2
    iget-object v1, p0, Lso;->i:Landroid/content/Context;

    check-cast v1, Lcom/twitter/android/GalleryActivity;

    invoke-virtual {v1, v2}, Lcom/twitter/android/GalleryActivity;->a(Lcom/twitter/model/core/MediaEntity;)Lbrm;

    move-result-object v1

    goto :goto_0
.end method

.method public a(Lsn;Lcom/twitter/ui/anim/c$a;Lsj$a;)V
    .locals 6

    .prologue
    .line 90
    instance-of v0, p1, Lss;

    if-nez v0, :cond_0

    .line 91
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "A photo item is required!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 93
    :cond_0
    iput-object p1, p0, Lso;->f:Lsn;

    .line 94
    invoke-virtual {p0}, Lso;->a()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    .line 96
    iget-object v1, p0, Lso;->f:Lsn;

    iget-object v1, v1, Lsn;->c:Lcom/twitter/media/request/a$a;

    new-instance v2, Lso$1;

    invoke-direct {v2, p0, p3}, Lso$1;-><init>(Lso;Lsj$a;)V

    invoke-virtual {v1, v2}, Lcom/twitter/media/request/a$a;->a(Lcom/twitter/media/request/b$b;)Lcom/twitter/media/request/b$a;

    .line 110
    iget-object v1, p0, Lso;->f:Lsn;

    iget-object v1, v1, Lsn;->c:Lcom/twitter/media/request/a$a;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 111
    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->getImageView()Landroid/widget/ImageView;

    move-result-object v1

    check-cast v1, Lcom/twitter/ui/widget/MultiTouchImageView;

    .line 112
    if-eqz p2, :cond_1

    .line 113
    new-instance v2, Lso$a;

    iget-object v3, p0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    invoke-direct {v2, v3, v0, p2}, Lso$a;-><init>(Lcom/twitter/android/media/stickers/StickerMediaView;Landroid/view/View;Lcom/twitter/ui/anim/c$a;)V

    invoke-virtual {v1, v2}, Lcom/twitter/ui/widget/MultiTouchImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 117
    :cond_1
    new-instance v0, Lso$2;

    invoke-direct {v0, p0, v1}, Lso$2;-><init>(Lso;Lcom/twitter/ui/widget/MultiTouchImageView;)V

    invoke-virtual {v1, v0}, Lcom/twitter/ui/widget/MultiTouchImageView;->setMultiTouchListener(Lcom/twitter/ui/widget/MultiTouchImageView$a;)V

    .line 134
    invoke-virtual {p0}, Lso;->f()Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lso;->i:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a095d

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p1, Lsn;->d:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 136
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 210
    if-eqz p1, :cond_1

    .line 211
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lso;->b(Z)V

    .line 215
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    iget-object v0, p0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/StickerMediaView;->b()V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lso;->f()Landroid/widget/ImageView;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/MultiTouchImageView;

    invoke-virtual {v0}, Lcom/twitter/ui/widget/MultiTouchImageView;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 226
    iget-object v0, p0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/media/stickers/StickerMediaView;->a(Z)V

    .line 228
    :cond_0
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/StickerMediaView;->b()V

    .line 205
    :cond_0
    invoke-virtual {p0}, Lso;->a()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 206
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/StickerMediaView;->b()V

    .line 194
    iget-object v0, p0, Lso;->b:Landroid/view/ViewGroup;

    iget-object v1, p0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 195
    const/4 v0, 0x0

    iput-object v0, p0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    .line 197
    :cond_0
    iget-object v1, p0, Lso;->g:Ljava/util/List;

    iget-object v0, p0, Lso;->b:Landroid/view/ViewGroup;

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lso;->h:Lcom/twitter/android/media/stickers/StickerMediaView;

    invoke-virtual {v0}, Lcom/twitter/android/media/stickers/StickerMediaView;->a()V

    .line 222
    :cond_0
    return-void
.end method

.method public f()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 231
    invoke-virtual {p0}, Lso;->a()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0}, Lcom/twitter/media/ui/image/MediaImageView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method
