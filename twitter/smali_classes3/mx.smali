.class public Lmx;
.super Lne;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lne",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:J

.field private d:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/dms/q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lnd;J)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 61
    invoke-static {}, Lcom/twitter/android/client/w;->g()I

    move-result v3

    move-object v0, p0

    move-object v1, p1

    move v4, v2

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lne;-><init>(Landroid/content/Context;IIZLnd;)V

    .line 63
    iput-wide p3, p0, Lmx;->c:J

    .line 64
    return-void
.end method

.method static a(Ljava/lang/String;Lcom/twitter/library/provider/w;Ljava/lang/Iterable;JILandroid/content/Context;)Lcbi;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/twitter/library/provider/w;",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/dms/q;",
            ">;JI",
            "Landroid/content/Context;",
            ")",
            "Lcbi",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    invoke-static {p0}, Lmx;->b(Ljava/lang/String;)Z

    move-result v4

    .line 84
    invoke-static {p0}, Lmx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 85
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v6

    move-object v0, p2

    move-wide v2, p3

    move-object v5, p6

    .line 87
    invoke-static/range {v0 .. v5}, Lmx;->a(Ljava/lang/Iterable;Ljava/lang/String;JZLandroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    .line 89
    invoke-static {v6}, Lmx;->a(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/MutableSet;->a(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v2

    .line 92
    invoke-virtual {v6}, Lcom/twitter/util/collection/h;->i()I

    move-result v0

    if-ge v0, p5, :cond_1

    .line 93
    const/16 v0, 0x8

    .line 94
    invoke-virtual {v6}, Lcom/twitter/util/collection/h;->i()I

    move-result v3

    sub-int v3, p5, v3

    .line 93
    invoke-interface {p1, v1, v0, v3}, Lcom/twitter/library/provider/w;->a(Ljava/lang/String;II)Ljava/util/List;

    move-result-object v0

    .line 95
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 96
    iget-wide v4, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v0}, Lcom/twitter/library/dm/e;->a(Lcom/twitter/model/core/TwitterUser;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 97
    invoke-virtual {v6, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 98
    iget-wide v4, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 104
    :cond_1
    invoke-static {v1}, Lcom/twitter/android/provider/SuggestionsProvider;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 106
    if-eqz v0, :cond_3

    .line 107
    new-instance v3, Lcom/twitter/android/provider/b;

    invoke-direct {v3}, Lcom/twitter/android/provider/b;-><init>()V

    .line 108
    invoke-static {v0, v3}, Lcpt;->a(Ljava/lang/Iterable;Lcpv;)Ljava/lang/Iterable;

    move-result-object v0

    .line 109
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/search/TwitterTypeAhead;

    .line 110
    invoke-virtual {v6}, Lcom/twitter/util/collection/h;->i()I

    move-result v4

    if-ge v4, p5, :cond_2

    .line 111
    iget-object v0, v0, Lcom/twitter/library/api/search/TwitterTypeAhead;->e:Lcom/twitter/model/core/TwitterUser;

    .line 112
    iget-wide v4, v0, Lcom/twitter/model/core/TwitterUser;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {v0}, Lcom/twitter/library/dm/e;->a(Lcom/twitter/model/core/TwitterUser;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 113
    invoke-virtual {v6, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_1

    .line 119
    :cond_3
    invoke-virtual {v6}, Lcom/twitter/util/collection/h;->i()I

    move-result v0

    if-ge p5, v0, :cond_5

    .line 120
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    invoke-static {v6, p5}, Lcpt;->a(Ljava/lang/Iterable;I)Ljava/lang/Iterable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    move-result-object v0

    .line 124
    :goto_2
    invoke-static {v1}, Lcom/twitter/library/util/af;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 125
    invoke-static {v1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v2, Lcom/twitter/model/util/g;->b:Ljava/util/regex/Pattern;

    .line 126
    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 127
    invoke-virtual {v0, v1}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 130
    :cond_4
    new-instance v1, Lcbl;

    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-direct {v1, v0}, Lcbl;-><init>(Ljava/lang/Iterable;)V

    return-object v1

    :cond_5
    move-object v0, v6

    .line 120
    goto :goto_2
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p1, :cond_0

    const/16 v0, 0x30

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/16 v0, 0x31

    goto :goto_0
.end method

.method private static a(Ljava/lang/Iterable;Ljava/lang/String;JZLandroid/content/Context;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/twitter/model/dms/q;",
            ">;",
            "Ljava/lang/String;",
            "JZ",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    invoke-static {p1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 137
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v1

    .line 139
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/q;

    .line 140
    iget-object v3, v0, Lcom/twitter/model/dms/q;->i:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 144
    iget-boolean v3, v0, Lcom/twitter/model/dms/q;->h:Z

    if-eqz v3, :cond_1

    .line 145
    if-eqz p4, :cond_0

    .line 146
    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 149
    :cond_1
    iget-object v0, v0, Lcom/twitter/model/dms/q;->i:Ljava/util/List;

    .line 150
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 151
    invoke-static {v0}, Lcom/twitter/library/dm/e;->a(Lcom/twitter/model/core/TwitterUser;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 152
    invoke-virtual {v1, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_0

    .line 157
    :cond_2
    invoke-virtual {v1}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 198
    :goto_1
    return-object v0

    .line 159
    :cond_3
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v3

    .line 160
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v4

    .line 162
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "(?i:.*\\b"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/twitter/library/util/af;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".*)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    .line 163
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/dms/q;

    .line 164
    iget-object v1, v0, Lcom/twitter/model/dms/q;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 168
    iget-boolean v1, v0, Lcom/twitter/model/dms/q;->h:Z

    if-eqz v1, :cond_7

    new-instance v1, Lcom/twitter/library/dm/b;

    invoke-direct {v1, v0, p5, p2, p3}, Lcom/twitter/library/dm/b;-><init>(Lcom/twitter/model/dms/q;Landroid/content/Context;J)V

    .line 169
    invoke-virtual {v1}, Lcom/twitter/library/dm/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 170
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v2, 0x1

    .line 172
    :goto_3
    if-nez v2, :cond_a

    .line 174
    iget-object v1, v0, Lcom/twitter/model/dms/q;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/model/core/TwitterUser;

    .line 175
    invoke-virtual {v1}, Lcom/twitter/model/core/TwitterUser;->a()J

    move-result-wide v8

    cmp-long v8, v8, p2

    if-eqz v8, :cond_5

    iget-object v8, v1, Lcom/twitter/model/core/TwitterUser;->c:Ljava/lang/String;

    .line 176
    invoke-virtual {v5, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/regex/Matcher;->matches()Z

    move-result v8

    if-nez v8, :cond_6

    iget-object v1, v1, Lcom/twitter/model/core/TwitterUser;->j:Ljava/lang/String;

    .line 177
    invoke-virtual {v5, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 178
    :cond_6
    const/4 v1, 0x1

    .line 184
    :goto_4
    if-eqz v1, :cond_4

    .line 185
    iget-boolean v1, v0, Lcom/twitter/model/dms/q;->h:Z

    if-eqz v1, :cond_8

    .line 186
    if-eqz p4, :cond_4

    .line 187
    invoke-virtual {v4, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto :goto_2

    .line 170
    :cond_7
    const/4 v2, 0x0

    goto :goto_3

    .line 190
    :cond_8
    iget-object v0, v0, Lcom/twitter/model/dms/q;->i:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/TwitterUser;

    .line 191
    invoke-static {v0}, Lcom/twitter/library/dm/e;->a(Lcom/twitter/model/core/TwitterUser;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 192
    invoke-virtual {v3, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    goto/16 :goto_2

    .line 198
    :cond_9
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Iterable;)Lcom/twitter/util/collection/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    goto/16 :goto_1

    :cond_a
    move v1, v2

    goto :goto_4
.end method

.method private static a(Ljava/lang/Iterable;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 204
    new-instance v0, Lmx$1;

    invoke-direct {v0}, Lmx$1;-><init>()V

    .line 205
    invoke-static {p0, v0}, Lcpt;->a(Ljava/lang/Iterable;Lcpp;)Ljava/lang/Iterable;

    move-result-object v0

    .line 204
    invoke-static {v0}, Lcom/twitter/util/collection/o;->a(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized a(Lbta;)V
    .locals 2

    .prologue
    .line 227
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmx;->d:Ljava/lang/Iterable;

    if-nez v0, :cond_0

    .line 228
    iget-wide v0, p0, Lmx;->c:J

    invoke-virtual {p1, v0, v1}, Lbta;->a(J)Ljava/lang/Iterable;

    move-result-object v0

    iput-object v0, p0, Lmx;->d:Ljava/lang/Iterable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230
    :cond_0
    monitor-exit p0

    return-void

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x31

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected synthetic a(Ljava/lang/Object;Z)Lcbi;
    .locals 1

    .prologue
    .line 38
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lmx;->b(Ljava/lang/String;Z)Lcbi;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Lcom/twitter/library/api/search/TwitterTypeAheadGroup;)V
    .locals 0

    .prologue
    .line 38
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lmx;->a(Ljava/lang/String;Lcom/twitter/library/api/search/TwitterTypeAheadGroup;)V

    return-void
.end method

.method protected a(Ljava/lang/String;Lcom/twitter/library/api/search/TwitterTypeAheadGroup;)V
    .locals 2

    .prologue
    .line 223
    invoke-virtual {p0, p1}, Lmx;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/api/search/TwitterTypeAheadGroup;->a:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 224
    return-void
.end method

.method protected synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 38
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lmx;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected b(Ljava/lang/String;Z)Lcbi;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lcbi",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    iget-wide v0, p0, Lmx;->c:J

    invoke-static {v0, v1}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v2

    .line 75
    new-instance v0, Lbta;

    invoke-virtual {v2}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v1

    invoke-direct {v0, v1}, Lbta;-><init>(Lcom/twitter/database/model/i;)V

    invoke-direct {p0, v0}, Lmx;->a(Lbta;)V

    .line 77
    iget-object v3, p0, Lmx;->d:Ljava/lang/Iterable;

    iget-wide v4, p0, Lmx;->c:J

    iget v6, p0, Lmx;->b:I

    iget-object v7, p0, Lmx;->a:Landroid/content/Context;

    move-object v1, p1

    invoke-static/range {v1 .. v7}, Lmx;->a(Ljava/lang/String;Lcom/twitter/library/provider/w;Ljava/lang/Iterable;JILandroid/content/Context;)Lcbi;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic b(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lmx;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 68
    invoke-static {p1}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method protected d(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 218
    invoke-static {p1}, Lmx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
