.class public Ldbs;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Ltv/periscope/android/session/a;

.field private final b:Ltv/periscope/android/api/ApiService;

.field private final c:Lrx/f;

.field private final d:Lrx/f;


# direct methods
.method public constructor <init>(Ltv/periscope/android/session/a;Ltv/periscope/android/api/ApiService;)V
    .locals 2

    .prologue
    .line 28
    invoke-static {}, Lcws;->d()Lrx/f;

    move-result-object v0

    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-direct {p0, p1, p2, v0, v1}, Ldbs;-><init>(Ltv/periscope/android/session/a;Ltv/periscope/android/api/ApiService;Lrx/f;Lrx/f;)V

    .line 29
    return-void
.end method

.method constructor <init>(Ltv/periscope/android/session/a;Ltv/periscope/android/api/ApiService;Lrx/f;Lrx/f;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Ldbs;->a:Ltv/periscope/android/session/a;

    .line 35
    iput-object p2, p0, Ldbs;->b:Ltv/periscope/android/api/ApiService;

    .line 36
    iput-object p3, p0, Ldbs;->c:Lrx/f;

    .line 37
    iput-object p4, p0, Ldbs;->d:Lrx/f;

    .line 38
    return-void
.end method

.method static synthetic a(Ldbs;)Ltv/periscope/android/api/ApiService;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Ldbs;->b:Ltv/periscope/android/api/ApiService;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/c",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 98
    new-instance v0, Ltv/periscope/android/api/GetFollowingRequest;

    invoke-direct {v0}, Ltv/periscope/android/api/GetFollowingRequest;-><init>()V

    .line 99
    iget-object v1, p0, Ldbs;->a:Ltv/periscope/android/session/a;

    invoke-interface {v1}, Ltv/periscope/android/session/a;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Ltv/periscope/android/api/GetFollowingRequest;->cookie:Ljava/lang/String;

    .line 100
    iput-object p1, v0, Ltv/periscope/android/api/GetFollowingRequest;->userId:Ljava/lang/String;

    .line 101
    const/4 v1, 0x1

    iput-boolean v1, v0, Ltv/periscope/android/api/GetFollowingRequest;->onlyIds:Z

    .line 103
    invoke-static {v0}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Ldbs;->c:Lrx/f;

    .line 104
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Ldbs$1;

    invoke-direct {v1, p0}, Ldbs$1;-><init>(Ldbs;)V

    .line 105
    invoke-virtual {v0, v1}, Lrx/c;->f(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Ldbs;->d:Lrx/f;

    .line 116
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 103
    return-object v0
.end method
