.class public interface abstract Lcui;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Lorg/spongycastle/asn1/l;

.field public static final b:Lorg/spongycastle/asn1/l;

.field public static final c:Lorg/spongycastle/asn1/l;

.field public static final d:Lorg/spongycastle/asn1/l;

.field public static final e:Lorg/spongycastle/asn1/l;

.field public static final f:Lorg/spongycastle/asn1/l;

.field public static final g:Lorg/spongycastle/asn1/l;

.field public static final h:Lorg/spongycastle/asn1/l;

.field public static final i:Lorg/spongycastle/asn1/l;

.field public static final j:Lorg/spongycastle/asn1/l;

.field public static final k:Lorg/spongycastle/asn1/l;

.field public static final l:Lorg/spongycastle/asn1/l;

.field public static final m:Lorg/spongycastle/asn1/l;

.field public static final n:Lorg/spongycastle/asn1/l;

.field public static final o:Lorg/spongycastle/asn1/l;

.field public static final p:Lorg/spongycastle/asn1/l;

.field public static final q:Lorg/spongycastle/asn1/l;

.field public static final r:Lorg/spongycastle/asn1/l;

.field public static final s:Lorg/spongycastle/asn1/l;

.field public static final t:Lorg/spongycastle/asn1/l;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2.5.4.3"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcui;->a:Lorg/spongycastle/asn1/l;

    .line 11
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2.5.4.6"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcui;->b:Lorg/spongycastle/asn1/l;

    .line 13
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2.5.4.7"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcui;->c:Lorg/spongycastle/asn1/l;

    .line 15
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2.5.4.8"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcui;->d:Lorg/spongycastle/asn1/l;

    .line 17
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2.5.4.10"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcui;->e:Lorg/spongycastle/asn1/l;

    .line 19
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2.5.4.11"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcui;->f:Lorg/spongycastle/asn1/l;

    .line 22
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2.5.4.20"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcui;->g:Lorg/spongycastle/asn1/l;

    .line 24
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2.5.4.41"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcui;->h:Lorg/spongycastle/asn1/l;

    .line 32
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1.3.14.3.2.26"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcui;->i:Lorg/spongycastle/asn1/l;

    .line 40
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1.3.36.3.2.1"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcui;->j:Lorg/spongycastle/asn1/l;

    .line 48
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1.3.36.3.3.1.2"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcui;->k:Lorg/spongycastle/asn1/l;

    .line 52
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2.5.8.1.1"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcui;->l:Lorg/spongycastle/asn1/l;

    .line 56
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1.3.6.1.5.5.7"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcui;->m:Lorg/spongycastle/asn1/l;

    .line 61
    sget-object v0, Lcui;->m:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lcui;->n:Lorg/spongycastle/asn1/l;

    .line 68
    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2.5.29"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcui;->o:Lorg/spongycastle/asn1/l;

    .line 71
    sget-object v0, Lcui;->m:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "48"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lcui;->p:Lorg/spongycastle/asn1/l;

    .line 73
    sget-object v0, Lcui;->p:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lcui;->q:Lorg/spongycastle/asn1/l;

    .line 75
    sget-object v0, Lcui;->p:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Lcui;->r:Lorg/spongycastle/asn1/l;

    .line 78
    sget-object v0, Lcui;->r:Lorg/spongycastle/asn1/l;

    sput-object v0, Lcui;->s:Lorg/spongycastle/asn1/l;

    .line 80
    sget-object v0, Lcui;->q:Lorg/spongycastle/asn1/l;

    sput-object v0, Lcui;->t:Lorg/spongycastle/asn1/l;

    return-void
.end method
