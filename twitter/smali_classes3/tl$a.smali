.class Ltl$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Ltl;

.field private final b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:Z

.field private e:J

.field private f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ltl;Landroid/view/View;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 90
    iput-object p1, p0, Ltl$a;->a:Ltl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ltl$a;->f:Ljava/lang/ref/WeakReference;

    .line 92
    iput-object p3, p0, Ltl$a;->b:Ljava/lang/Object;

    .line 93
    return-void
.end method

.method private a(J)V
    .locals 5

    .prologue
    .line 126
    iget-object v0, p0, Ltl$a;->a:Ltl;

    invoke-static {v0}, Ltl;->b(Ltl;)Lti$a;

    move-result-object v0

    iget-object v1, p0, Ltl$a;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lti$a;->b(Ljava/lang/Object;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    .line 127
    iget-wide v2, p0, Ltl$a;->e:J

    iput-wide v2, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->D:J

    .line 128
    iput-wide p1, v0, Lcom/twitter/analytics/feature/model/TwitterScribeItem;->E:J

    .line 129
    iget-object v1, p0, Ltl$a;->a:Ltl;

    invoke-static {v1}, Ltl;->c(Ltl;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    return-void
.end method

.method private a()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 102
    iget-object v0, p0, Ltl$a;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 103
    if-eqz v0, :cond_0

    iget-object v2, p0, Ltl$a;->a:Ltl;

    invoke-static {v2}, Ltl;->a(Ltl;)Landroid/view/ViewGroup;

    move-result-object v2

    if-nez v2, :cond_1

    .line 122
    :cond_0
    :goto_0
    return v1

    .line 107
    :cond_1
    const v2, 0x7f13004c

    invoke-virtual {v0, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 108
    if-eqz v2, :cond_0

    iget-object v3, p0, Ltl$a;->a:Ltl;

    invoke-static {v3}, Ltl;->b(Ltl;)Lti$a;

    move-result-object v3

    invoke-interface {v3, v2}, Lti$a;->a(Ljava/lang/Object;)J

    move-result-wide v2

    iget-object v4, p0, Ltl$a;->a:Ltl;

    invoke-static {v4}, Ltl;->b(Ltl;)Lti$a;

    move-result-object v4

    iget-object v5, p0, Ltl$a;->b:Ljava/lang/Object;

    invoke-interface {v4, v5}, Lti$a;->a(Ljava/lang/Object;)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 112
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    .line 113
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 114
    if-ge v2, v0, :cond_0

    .line 118
    add-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    .line 119
    iget-object v2, p0, Ltl$a;->a:Ltl;

    invoke-static {v2}, Ltl;->a(Ltl;)Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getTop()I

    move-result v2

    .line 120
    iget-object v3, p0, Ltl$a;->a:Ltl;

    invoke-static {v3}, Ltl;->a(Ltl;)Landroid/view/ViewGroup;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getBottom()I

    move-result v3

    .line 122
    if-lt v0, v2, :cond_2

    if-gt v0, v3, :cond_2

    const/4 v0, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method a(JZ)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 133
    if-nez p3, :cond_2

    invoke-direct {p0}, Ltl$a;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 136
    iget-boolean v0, p0, Ltl$a;->c:Z

    if-nez v0, :cond_1

    .line 137
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v0

    iput-wide v0, p0, Ltl$a;->e:J

    .line 138
    iput-boolean v4, p0, Ltl$a;->c:Z

    .line 139
    iput-boolean v5, p0, Ltl$a;->d:Z

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 142
    :cond_1
    iget-boolean v0, p0, Ltl$a;->d:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Ltl$a;->e:J

    sub-long v0, p1, v0

    iget-object v2, p0, Ltl$a;->a:Ltl;

    invoke-static {v2}, Ltl;->d(Ltl;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 143
    iget-wide v0, p0, Ltl$a;->e:J

    iget-object v2, p0, Ltl$a;->a:Ltl;

    invoke-static {v2}, Ltl;->d(Ltl;)J

    move-result-wide v2

    add-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Ltl$a;->a(J)V

    .line 144
    iput-boolean v4, p0, Ltl$a;->d:Z

    goto :goto_0

    .line 150
    :cond_2
    iget-boolean v0, p0, Ltl$a;->c:Z

    if-eqz v0, :cond_3

    .line 151
    iget-wide v0, p0, Ltl$a;->e:J

    sub-long v0, p1, v0

    .line 152
    iget-boolean v2, p0, Ltl$a;->d:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Ltl$a;->a:Ltl;

    invoke-static {v2}, Ltl;->e(Ltl;)J

    move-result-wide v2

    cmp-long v2, v0, v2

    if-lez v2, :cond_3

    .line 153
    iget-object v2, p0, Ltl$a;->a:Ltl;

    invoke-static {v2}, Ltl;->d(Ltl;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_4

    .line 154
    invoke-direct {p0, p1, p2}, Ltl$a;->a(J)V

    .line 159
    :goto_1
    iput-boolean v4, p0, Ltl$a;->d:Z

    .line 164
    :cond_3
    iput-boolean v5, p0, Ltl$a;->c:Z

    goto :goto_0

    .line 157
    :cond_4
    iget-wide v0, p0, Ltl$a;->e:J

    iget-object v2, p0, Ltl$a;->a:Ltl;

    invoke-static {v2}, Ltl;->d(Ltl;)J

    move-result-wide v2

    add-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Ltl$a;->a(J)V

    goto :goto_1
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Ltl$a;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p1, :cond_0

    .line 97
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ltl$a;->f:Ljava/lang/ref/WeakReference;

    .line 99
    :cond_0
    return-void
.end method
