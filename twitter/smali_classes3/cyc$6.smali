.class Lcyc$6;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcyc;->z()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ldcd;

.field final synthetic b:Lcxp;

.field final synthetic c:Lcyc;


# direct methods
.method constructor <init>(Lcyc;Ldcd;Lcxp;)V
    .locals 0

    .prologue
    .line 722
    iput-object p1, p0, Lcyc$6;->c:Lcyc;

    iput-object p2, p0, Lcyc$6;->a:Ldcd;

    iput-object p3, p0, Lcyc$6;->b:Lcxp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 725
    iget-object v0, p0, Lcyc$6;->c:Lcyc;

    invoke-static {v0}, Lcyc;->d(Lcyc;)Ltv/periscope/android/ui/broadcaster/BroadcasterView;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcaster/BroadcasterView;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 726
    iget-object v0, p0, Lcyc$6;->c:Lcyc;

    invoke-static {v0}, Lcyc;->f(Lcyc;)Ltv/periscope/android/ui/broadcast/ChatRoomView;

    move-result-object v0

    .line 727
    invoke-static {}, Ltv/periscope/model/chat/Message;->P()Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    sget-object v2, Ltv/periscope/model/chat/MessageType;->E:Ltv/periscope/model/chat/MessageType;

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->a(Ltv/periscope/model/chat/MessageType;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    iget-object v2, p0, Lcyc$6;->a:Ldcd;

    .line 728
    invoke-virtual {v2}, Ldcd;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->c(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    iget-object v2, p0, Lcyc$6;->c:Lcyc;

    .line 729
    invoke-static {v2}, Lcyc;->e(Lcyc;)Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcyc$6;->a:Ldcd;

    invoke-virtual {v3}, Ldcd;->c()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ltv/periscope/model/chat/Message$a;->h(Ljava/lang/String;)Ltv/periscope/model/chat/Message$a;

    move-result-object v1

    .line 730
    invoke-virtual {v1}, Ltv/periscope/model/chat/Message$a;->a()Ltv/periscope/model/chat/Message;

    move-result-object v1

    .line 727
    invoke-virtual {v0, v1}, Ltv/periscope/android/ui/broadcast/ChatRoomView;->a(Ltv/periscope/model/chat/Message;)V

    .line 732
    iget-object v0, p0, Lcyc$6;->c:Lcyc;

    invoke-static {v0}, Lcyc;->g(Lcyc;)Ltv/periscope/android/analytics/summary/a;

    move-result-object v0

    iget-object v1, p0, Lcyc$6;->a:Ldcd;

    invoke-virtual {v1}, Ldcd;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltv/periscope/android/analytics/summary/a;->h(Ljava/lang/String;)V

    .line 735
    iget-object v0, p0, Lcyc$6;->b:Lcxp;

    iget-object v1, p0, Lcyc$6;->a:Ldcd;

    invoke-virtual {v1}, Ldcd;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcxp;->a(Ljava/lang/String;)V

    .line 737
    :cond_0
    return-void
.end method
