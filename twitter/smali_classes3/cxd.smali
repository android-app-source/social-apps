.class public Lcxd;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcxd$c;,
        Lcxd$b;,
        Lcxd$d;,
        Lcxd$a;
    }
.end annotation


# instance fields
.field final a:Ltv/periscope/android/api/ApiManager;

.field final b:Ltv/periscope/android/ui/broadcast/ac;

.field c:Ljava/lang/String;

.field d:Ltv/periscope/android/player/PlayMode;

.field e:Ltv/periscope/android/ui/chat/p;

.field f:Z
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private final g:Lcxd$c;

.field private h:Lcxd$b;

.field private i:Lcxd$a;

.field private final j:Z

.field private final k:Z

.field private final l:J

.field private m:Z

.field private n:Z


# direct methods
.method public constructor <init>(Ltv/periscope/android/api/ApiManager;Ltv/periscope/android/ui/broadcast/ac;Ltv/periscope/android/player/PlayMode;Lcxd$a;Ltv/periscope/android/ui/chat/p;ZJZ)V
    .locals 11

    .prologue
    .line 80
    new-instance v4, Lcxd$c;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {v4, v0, v1}, Lcxd$c;-><init>(Landroid/os/Looper;Lcxd$b;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move-wide/from16 v8, p7

    move/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcxd;-><init>(Ltv/periscope/android/api/ApiManager;Ltv/periscope/android/ui/broadcast/ac;Ltv/periscope/android/player/PlayMode;Lcxd$c;Lcxd$a;Ltv/periscope/android/ui/chat/p;ZJZ)V

    .line 82
    return-void
.end method

.method private constructor <init>(Ltv/periscope/android/api/ApiManager;Ltv/periscope/android/ui/broadcast/ac;Ltv/periscope/android/player/PlayMode;Lcxd$c;Lcxd$a;Ltv/periscope/android/ui/chat/p;ZJZ)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-object p1, p0, Lcxd;->a:Ltv/periscope/android/api/ApiManager;

    .line 94
    iput-object p2, p0, Lcxd;->b:Ltv/periscope/android/ui/broadcast/ac;

    .line 95
    iput-object p3, p0, Lcxd;->d:Ltv/periscope/android/player/PlayMode;

    .line 96
    iput-object p5, p0, Lcxd;->i:Lcxd$a;

    .line 97
    iput-object p4, p0, Lcxd;->g:Lcxd$c;

    .line 98
    iput-object p6, p0, Lcxd;->e:Ltv/periscope/android/ui/chat/p;

    .line 99
    iput-wide p8, p0, Lcxd;->l:J

    .line 100
    iput-boolean p7, p0, Lcxd;->j:Z

    .line 101
    iput-boolean p10, p0, Lcxd;->k:Z

    .line 102
    return-void
.end method

.method static synthetic a(Lcxd;)Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcxd;->k:Z

    return v0
.end method

.method static synthetic b(Lcxd;)Lcxd$b;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcxd;->h:Lcxd$b;

    return-object v0
.end method

.method private d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 142
    new-instance v0, Lcxd$b;

    invoke-direct {v0, p0}, Lcxd$b;-><init>(Lcxd;)V

    iput-object v0, p0, Lcxd;->h:Lcxd$b;

    .line 143
    iget-object v0, p0, Lcxd;->h:Lcxd$b;

    iput-object p1, v0, Lcxd$b;->a:Ljava/lang/String;

    .line 144
    iget-object v0, p0, Lcxd;->g:Lcxd$c;

    iget-object v1, p0, Lcxd;->h:Lcxd$b;

    invoke-virtual {v0, v1}, Lcxd$c;->a(Lcxd$b;)V

    .line 145
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcxd;->g:Lcxd$c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcxd$c;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 106
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 312
    invoke-static {p1}, Lcwz;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 313
    iget-boolean v0, p0, Lcxd;->f:Z

    if-eqz v0, :cond_1

    .line 314
    iget-object v0, p0, Lcxd;->i:Lcxd$a;

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Lcxd;->i:Lcxd$a;

    invoke-interface {v0}, Lcxd$a;->a()V

    .line 317
    :cond_0
    invoke-virtual {p0}, Lcxd;->j()V

    .line 333
    :cond_1
    :goto_0
    return-void

    .line 327
    :cond_2
    iget-object v0, p0, Lcxd;->i:Lcxd$a;

    if-eqz v0, :cond_1

    .line 328
    iget-object v0, p0, Lcxd;->i:Lcxd$a;

    invoke-interface {v0}, Lcxd$a;->b()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcxd;->h:Lcxd$b;

    if-nez v0, :cond_0

    .line 134
    invoke-direct {p0, p1}, Lcxd;->d(Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcxd;->a:Ltv/periscope/android/api/ApiManager;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Ltv/periscope/android/api/ApiManager;->getAccessVideo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 139
    :goto_0
    return-void

    .line 137
    :cond_0
    iget-object v0, p0, Lcxd;->a:Ltv/periscope/android/api/ApiManager;

    iget-object v1, p0, Lcxd;->h:Lcxd$b;

    iget-object v1, v1, Lcxd$b;->e:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ltv/periscope/android/api/ApiManager;->getAccessVideo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/16 v1, 0x7b

    .line 208
    iget-object v0, p0, Lcxd;->h:Lcxd$b;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcxd;->h:Lcxd$b;

    invoke-virtual {v0, p1, p2}, Lcxd$b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    :cond_0
    iget-object v0, p0, Lcxd;->g:Lcxd$c;

    invoke-virtual {v0, v1}, Lcxd$c;->removeMessages(I)V

    .line 212
    iget-object v0, p0, Lcxd;->g:Lcxd$c;

    sget-wide v2, Ltv/periscope/android/api/Constants;->API_PING_INTERVAL_MILLIS:J

    invoke-virtual {v0, v1, v2, v3}, Lcxd$c;->a(IJ)Z

    .line 213
    return-void
.end method

.method public a(Ltv/periscope/android/ui/chat/p;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcxd;->e:Ltv/periscope/android/ui/chat/p;

    .line 110
    return-void
.end method

.method public a(Ltv/periscope/model/af;Ltv/periscope/model/p;Z)V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcxd;->h:Lcxd$b;

    if-nez v0, :cond_0

    .line 156
    invoke-virtual {p2}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcxd;->d(Ljava/lang/String;)V

    .line 160
    :cond_0
    iget-boolean v0, p0, Lcxd;->j:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcxd;->h:Lcxd$b;

    iget-object v0, v0, Lcxd$b;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcxd;->h:Lcxd$b;

    iget-object v0, v0, Lcxd$b;->f:Ljava/lang/String;

    .line 161
    invoke-virtual {p1}, Ltv/periscope/model/af;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 162
    :cond_1
    iget-object v0, p0, Lcxd;->h:Lcxd$b;

    invoke-virtual {p1}, Ltv/periscope/model/af;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcxd$b;->f:Ljava/lang/String;

    .line 163
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcxd;->a(Z)V

    .line 166
    :cond_2
    invoke-virtual {p1}, Ltv/periscope/model/af;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcxd;->h:Lcxd$b;

    iget-object v1, v1, Lcxd$b;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 180
    :cond_3
    :goto_0
    iget-object v0, p0, Lcxd;->h:Lcxd$b;

    iput-object p1, v0, Lcxd$b;->d:Ltv/periscope/model/af;

    .line 181
    return-void

    .line 168
    :cond_4
    iget-object v0, p0, Lcxd;->h:Lcxd$b;

    iget-object v0, v0, Lcxd$b;->e:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 169
    iget-object v0, p0, Lcxd;->h:Lcxd$b;

    invoke-virtual {p1}, Ltv/periscope/model/af;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcxd$b;->e:Ljava/lang/String;

    .line 170
    if-eqz p3, :cond_3

    .line 172
    invoke-virtual {p0}, Lcxd;->d()V

    goto :goto_0

    .line 176
    :cond_5
    iget-object v0, p0, Lcxd;->h:Lcxd$b;

    invoke-virtual {p1}, Ltv/periscope/model/af;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcxd$b;->e:Ljava/lang/String;

    .line 177
    invoke-virtual {p0}, Lcxd;->d()V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcxd;->h:Lcxd$b;

    invoke-virtual {v0, p1}, Lcxd$b;->a(Z)V

    .line 150
    return-void
.end method

.method public b()Ltv/periscope/model/af;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcxd;->h:Lcxd$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcxd;->h:Lcxd$b;

    iget-object v0, v0, Lcxd$b;->d:Ltv/periscope/model/af;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 217
    iget-object v0, p0, Lcxd;->h:Lcxd$b;

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcxd;->h:Lcxd$b;

    invoke-virtual {v0, p1}, Lcxd$b;->b(Ljava/lang/String;)V

    .line 220
    :cond_0
    return-void
.end method

.method public c()Ltv/periscope/model/StreamType;
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0}, Lcxd;->b()Ltv/periscope/model/af;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcxd;->b()Ltv/periscope/model/af;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/model/af;->k()Ltv/periscope/model/StreamType;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ltv/periscope/model/StreamType;->a:Ltv/periscope/model/StreamType;

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcxd;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 4

    .prologue
    .line 192
    iget-object v0, p0, Lcxd;->h:Lcxd$b;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcxd;->g:Lcxd$c;

    const/16 v1, 0x7e

    iget-wide v2, p0, Lcxd;->l:J

    invoke-virtual {v0, v1, v2, v3}, Lcxd$c;->a(IJ)Z

    .line 195
    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 198
    invoke-virtual {p0}, Lcxd;->a()V

    .line 199
    iget-object v0, p0, Lcxd;->h:Lcxd$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcxd;->h:Lcxd$b;

    invoke-virtual {v0}, Lcxd$b;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcxd;->h:Lcxd$b;

    invoke-virtual {v0}, Lcxd$b;->b()V

    .line 204
    :goto_0
    return-void

    .line 202
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcxd;->m:Z

    goto :goto_0
.end method

.method public f()V
    .locals 2

    .prologue
    .line 272
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcxd;->n:Z

    .line 273
    iget-object v0, p0, Lcxd;->g:Lcxd$c;

    const/16 v1, 0x7c

    invoke-virtual {v0, v1}, Lcxd$c;->removeMessages(I)V

    .line 274
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 277
    iget-boolean v0, p0, Lcxd;->f:Z

    if-eqz v0, :cond_0

    .line 278
    invoke-virtual {p0}, Lcxd;->j()V

    .line 282
    :goto_0
    return-void

    .line 280
    :cond_0
    invoke-virtual {p0}, Lcxd;->f()V

    goto :goto_0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 285
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcxd;->f:Z

    .line 286
    invoke-virtual {p0}, Lcxd;->j()V

    .line 287
    return-void
.end method

.method public i()V
    .locals 2

    .prologue
    .line 290
    iget-boolean v0, p0, Lcxd;->f:Z

    if-eqz v0, :cond_0

    .line 293
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcxd;->f:Z

    .line 294
    iget-object v0, p0, Lcxd;->g:Lcxd$c;

    const/16 v1, 0x7c

    invoke-virtual {v0, v1}, Lcxd$c;->removeMessages(I)V

    .line 296
    :cond_0
    return-void
.end method

.method j()V
    .locals 6

    .prologue
    const/16 v0, 0x7c

    .line 299
    iget-object v1, p0, Lcxd;->g:Lcxd$c;

    invoke-virtual {v1, v0}, Lcxd$c;->hasMessages(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 300
    invoke-static {}, Lcwz;->a()J

    move-result-wide v2

    .line 301
    const-string/jumbo v1, "BLCM"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Repeating /accessChat in "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Ltv/periscope/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    iget-boolean v1, p0, Lcxd;->n:Z

    if-eqz v1, :cond_1

    .line 305
    :goto_0
    iget-object v1, p0, Lcxd;->g:Lcxd$c;

    invoke-virtual {v1, v0, v2, v3}, Lcxd$c;->a(IJ)Z

    .line 306
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcxd;->n:Z

    .line 308
    :cond_0
    return-void

    .line 304
    :cond_1
    const/16 v0, 0x7d

    goto :goto_0
.end method

.method public onEventMainThread(Ltv/periscope/android/event/ApiEvent;)V
    .locals 3

    .prologue
    .line 227
    sget-object v0, Lcxd$1;->a:[I

    iget-object v1, p1, Ltv/periscope/android/event/ApiEvent;->a:Ltv/periscope/android/event/ApiEvent$Type;

    invoke-virtual {v1}, Ltv/periscope/android/event/ApiEvent$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 229
    :pswitch_0
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 230
    iget-boolean v0, p0, Lcxd;->m:Z

    if-eqz v0, :cond_2

    .line 231
    iget-object v0, p0, Lcxd;->h:Lcxd$b;

    if-eqz v0, :cond_1

    .line 232
    iget-object v1, p0, Lcxd;->h:Lcxd$b;

    iget-object v2, p1, Ltv/periscope/android/event/ApiEvent;->b:Ljava/lang/String;

    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v0, Ltv/periscope/android/api/StartWatchingResponse;

    iget-object v0, v0, Ltv/periscope/android/api/StartWatchingResponse;->session:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcxd$b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    :cond_1
    invoke-virtual {p0}, Lcxd;->e()V

    goto :goto_0

    .line 236
    :cond_2
    iget-object v1, p1, Ltv/periscope/android/event/ApiEvent;->b:Ljava/lang/String;

    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->d:Ljava/lang/Object;

    check-cast v0, Ltv/periscope/android/api/StartWatchingResponse;

    iget-object v0, v0, Ltv/periscope/android/api/StartWatchingResponse;->session:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lcxd;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 242
    :pswitch_1
    invoke-virtual {p1}, Ltv/periscope/android/event/ApiEvent;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p1, Ltv/periscope/android/event/ApiEvent;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcxd;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 227
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 255
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 257
    const-string/jumbo v1, "{ BroadcastLifeCyleManager:\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "{ mCurrentLifeCycle:\n"

    .line 258
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    iget-object v1, p0, Lcxd;->h:Lcxd$b;

    if-nez v1, :cond_0

    .line 260
    const-string/jumbo v1, " null \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    :goto_0
    const-string/jumbo v1, " } \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " } \n"

    .line 265
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    .line 266
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 262
    :cond_0
    iget-object v1, p0, Lcxd;->h:Lcxd$b;

    invoke-virtual {v1}, Lcxd$b;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
