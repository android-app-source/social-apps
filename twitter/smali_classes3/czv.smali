.class public Lczv;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public a:[[Lczu;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x2

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    filled-new-array {v0, v0}, [I

    move-result-object v0

    const-class v1, Lczu;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lczu;

    iput-object v0, p0, Lczv;->a:[[Lczu;

    .line 11
    return-void
.end method


# virtual methods
.method public a()Lczv;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 32
    new-instance v0, Lczv;

    invoke-direct {v0}, Lczv;-><init>()V

    .line 33
    iget-object v1, v0, Lczv;->a:[[Lczu;

    aget-object v1, v1, v3

    iget-object v2, p0, Lczv;->a:[[Lczu;

    aget-object v2, v2, v3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lczu;->a()Lczu;

    move-result-object v2

    aput-object v2, v1, v3

    .line 34
    iget-object v1, v0, Lczv;->a:[[Lczu;

    aget-object v1, v1, v4

    iget-object v2, p0, Lczv;->a:[[Lczu;

    aget-object v2, v2, v3

    aget-object v2, v2, v4

    invoke-virtual {v2}, Lczu;->a()Lczu;

    move-result-object v2

    aput-object v2, v1, v3

    .line 35
    iget-object v1, v0, Lczv;->a:[[Lczu;

    aget-object v1, v1, v3

    iget-object v2, p0, Lczv;->a:[[Lczu;

    aget-object v2, v2, v4

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lczu;->a()Lczu;

    move-result-object v2

    aput-object v2, v1, v4

    .line 36
    iget-object v1, v0, Lczv;->a:[[Lczu;

    aget-object v1, v1, v4

    iget-object v2, p0, Lczv;->a:[[Lczu;

    aget-object v2, v2, v4

    aget-object v2, v2, v4

    invoke-virtual {v2}, Lczu;->a()Lczu;

    move-result-object v2

    aput-object v2, v1, v4

    .line 37
    return-object v0
.end method

.method public a(Lczv;)Lczv;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x0

    .line 22
    new-instance v3, Lczv;

    invoke-direct {v3}, Lczv;-><init>()V

    move v2, v1

    .line 23
    :goto_0
    if-ge v2, v7, :cond_1

    move v0, v1

    .line 24
    :goto_1
    if-ge v0, v7, :cond_0

    .line 25
    iget-object v4, v3, Lczv;->a:[[Lczu;

    aget-object v4, v4, v2

    iget-object v5, p0, Lczv;->a:[[Lczu;

    aget-object v5, v5, v2

    aget-object v5, v5, v0

    iget-object v6, p1, Lczv;->a:[[Lczu;

    aget-object v6, v6, v2

    aget-object v6, v6, v0

    invoke-virtual {v5, v6}, Lczu;->b(Lczu;)Lczu;

    move-result-object v5

    aput-object v5, v4, v0

    .line 24
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 23
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 28
    :cond_1
    return-object v3
.end method

.method public a(F)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x0

    .line 14
    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    move v0, v1

    .line 15
    :goto_1
    if-ge v0, v5, :cond_0

    .line 16
    iget-object v3, p0, Lczv;->a:[[Lczu;

    aget-object v3, v3, v2

    new-instance v4, Lczu;

    invoke-direct {v4, p1}, Lczu;-><init>(F)V

    aput-object v4, v3, v0

    .line 15
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 14
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 19
    :cond_1
    return-void
.end method

.method public b(Lczv;)Lczv;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v1, 0x0

    .line 41
    new-instance v5, Lczv;

    invoke-direct {v5}, Lczv;-><init>()V

    move v4, v1

    .line 42
    :goto_0
    if-ge v4, v8, :cond_2

    move v3, v1

    .line 43
    :goto_1
    if-ge v3, v8, :cond_1

    .line 44
    new-instance v0, Lczu;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lczu;-><init>(F)V

    move-object v2, v0

    move v0, v1

    .line 45
    :goto_2
    if-ge v0, v8, :cond_0

    .line 46
    iget-object v6, p0, Lczv;->a:[[Lczu;

    aget-object v6, v6, v0

    aget-object v6, v6, v3

    iget-object v7, p1, Lczv;->a:[[Lczu;

    aget-object v7, v7, v4

    aget-object v7, v7, v0

    invoke-virtual {v6, v7}, Lczu;->a(Lczu;)Lczu;

    move-result-object v6

    invoke-virtual {v2, v6}, Lczu;->b(Lczu;)Lczu;

    move-result-object v2

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 48
    :cond_0
    iget-object v0, v5, Lczv;->a:[[Lczu;

    aget-object v0, v0, v4

    aput-object v2, v0, v3

    .line 43
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 42
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 51
    :cond_2
    return-object v5
.end method
