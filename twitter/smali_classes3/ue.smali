.class public final Lue;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lvb;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lue$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/provider/t;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcqt;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ltt;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ltx;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lua;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/scribe/n;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/timeline/br;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lub;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/analytics/model/ScribeItem;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcsd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcsd",
            "<",
            "Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lue;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lue;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lue$a;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    sget-boolean v0, Lue;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 60
    :cond_0
    invoke-direct {p0, p1}, Lue;->a(Lue$a;)V

    .line 61
    return-void
.end method

.method synthetic constructor <init>(Lue$a;Lue$1;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lue;-><init>(Lue$a;)V

    return-void
.end method

.method public static a()Lue$a;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Lue$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lue$a;-><init>(Lue$1;)V

    return-object v0
.end method

.method private a(Lue$a;)V
    .locals 5

    .prologue
    .line 70
    .line 73
    invoke-static {p1}, Lue$a;->a(Lue$a;)Lvc;

    move-result-object v0

    .line 72
    invoke-static {v0}, Lvd;->a(Lvc;)Ldagger/internal/c;

    move-result-object v0

    .line 71
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lue;->b:Lcta;

    .line 75
    new-instance v0, Lue$1;

    invoke-direct {v0, p0, p1}, Lue$1;-><init>(Lue;Lue$a;)V

    iput-object v0, p0, Lue;->c:Lcta;

    .line 88
    new-instance v0, Lue$2;

    invoke-direct {v0, p0, p1}, Lue$2;-><init>(Lue;Lue$a;)V

    iput-object v0, p0, Lue;->d:Lcta;

    .line 101
    new-instance v0, Lue$3;

    invoke-direct {v0, p0, p1}, Lue$3;-><init>(Lue;Lue$a;)V

    iput-object v0, p0, Lue;->e:Lcta;

    .line 114
    iget-object v0, p0, Lue;->d:Lcta;

    iget-object v1, p0, Lue;->e:Lcta;

    .line 116
    invoke-static {v0, v1}, Ltu;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 115
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lue;->f:Lcta;

    .line 119
    iget-object v0, p0, Lue;->f:Lcta;

    .line 120
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lue;->g:Lcta;

    .line 123
    invoke-static {}, Lbdi;->c()Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lue;->h:Lcta;

    .line 128
    invoke-static {p1}, Lue$a;->a(Lue$a;)Lvc;

    move-result-object v0

    iget-object v1, p0, Lue;->b:Lcta;

    iget-object v2, p0, Lue;->c:Lcta;

    iget-object v3, p0, Lue;->g:Lcta;

    iget-object v4, p0, Lue;->h:Lcta;

    .line 127
    invoke-static {v0, v1, v2, v3, v4}, Lvh;->a(Lvc;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 126
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lue;->i:Lcta;

    .line 137
    invoke-static {p1}, Lue$a;->a(Lue$a;)Lvc;

    move-result-object v0

    .line 136
    invoke-static {v0}, Lvi;->a(Lvc;)Ldagger/internal/c;

    move-result-object v0

    .line 135
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lue;->j:Lcta;

    .line 142
    invoke-static {p1}, Lue$a;->a(Lue$a;)Lvc;

    move-result-object v0

    .line 141
    invoke-static {v0}, Lvf;->a(Lvc;)Ldagger/internal/c;

    move-result-object v0

    .line 140
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lue;->k:Lcta;

    .line 145
    invoke-static {}, Lvg;->c()Ldagger/internal/c;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lue;->l:Lcta;

    .line 150
    invoke-static {p1}, Lue$a;->a(Lue$a;)Lvc;

    move-result-object v0

    .line 149
    invoke-static {v0}, Lve;->a(Lvc;)Ldagger/internal/c;

    move-result-object v0

    .line 148
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lue;->m:Lcta;

    .line 152
    iget-object v0, p0, Lue;->i:Lcta;

    iget-object v1, p0, Lue;->j:Lcta;

    iget-object v2, p0, Lue;->k:Lcta;

    iget-object v3, p0, Lue;->l:Lcta;

    iget-object v4, p0, Lue;->m:Lcta;

    .line 153
    invoke-static {v0, v1, v2, v3, v4}, Lcom/twitter/android/livevideo/landing/g;->a(Lcta;Lcta;Lcta;Lcta;Lcta;)Lcsd;

    move-result-object v0

    iput-object v0, p0, Lue;->n:Lcsd;

    .line 159
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;)V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lue;->n:Lcsd;

    invoke-interface {v0, p1}, Lcsd;->a(Ljava/lang/Object;)V

    .line 164
    return-void
.end method
