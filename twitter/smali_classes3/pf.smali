.class abstract Lpf;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field protected final b:Lcom/twitter/library/av/playback/AVPlayer;

.field protected c:J

.field d:Z
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/twitter/library/av/playback/AVPlayer;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lpf;->b:Lcom/twitter/library/av/playback/AVPlayer;

    .line 23
    return-void
.end method


# virtual methods
.method protected abstract a(Lcom/twitter/library/av/playback/AVPlayer$PlayerStartType;)V
.end method

.method protected abstract a(Lcom/twitter/library/av/playback/aa;Lcom/twitter/library/av/m;)V
.end method

.method protected abstract a()Z
.end method

.method protected abstract b()Ljava/lang/String;
.end method

.method final b(Lcom/twitter/library/av/playback/aa;Lcom/twitter/library/av/m;)V
    .locals 3

    .prologue
    .line 26
    iget-boolean v0, p0, Lpf;->d:Z

    if-nez v0, :cond_0

    .line 27
    invoke-virtual {p0, p1, p2}, Lpf;->a(Lcom/twitter/library/av/playback/aa;Lcom/twitter/library/av/m;)V

    .line 29
    invoke-virtual {p0}, Lpf;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lpf;->d:Z

    .line 31
    iget-object v0, p0, Lpf;->b:Lcom/twitter/library/av/playback/AVPlayer;

    invoke-static {p2}, Lcom/twitter/library/av/m$a;->a(Lcom/twitter/library/av/m;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    invoke-virtual {p0}, Lpf;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/av/m$a;->a(Ljava/lang/String;)Lcom/twitter/library/av/m$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/av/m$a;->a()Lcom/twitter/library/av/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/av/playback/AVPlayer;->a(Lcom/twitter/library/av/m;)V

    .line 34
    :cond_0
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 45
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lpf;->c:J

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lpf;->d:Z

    .line 47
    return-void
.end method
