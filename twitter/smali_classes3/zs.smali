.class public Lzs;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lzu;

.field private final b:Lcom/twitter/analytics/feature/model/MomentScribeDetails;


# direct methods
.method constructor <init>(Lzu;Lcom/twitter/analytics/feature/model/MomentScribeDetails;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lzs;->a:Lzu;

    .line 25
    iput-object p2, p0, Lzs;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    .line 26
    return-void
.end method

.method public static a(J)Lzs;
    .locals 4

    .prologue
    .line 18
    new-instance v1, Lzs;

    invoke-static {}, Lzu;->a()Lzu;

    move-result-object v2

    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;-><init>()V

    .line 19
    invoke-virtual {v0, p0, p1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    invoke-direct {v1, v2, v0}, Lzs;-><init>(Lzu;Lcom/twitter/analytics/feature/model/MomentScribeDetails;)V

    .line 18
    return-object v1
.end method

.method private b()V
    .locals 6

    .prologue
    .line 53
    iget-object v0, p0, Lzs;->a:Lzu;

    iget-object v1, p0, Lzs;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:recommended_tweets:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "likes"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "navigate"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 54
    return-void
.end method

.method private c()V
    .locals 6

    .prologue
    .line 57
    iget-object v0, p0, Lzs;->a:Lzu;

    iget-object v1, p0, Lzs;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:recommended_tweets:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "tweets"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "navigate"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 58
    return-void
.end method

.method private d()V
    .locals 6

    .prologue
    .line 61
    iget-object v0, p0, Lzs;->a:Lzu;

    iget-object v1, p0, Lzs;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:recommended_tweets:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "search"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "navigate"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 62
    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 49
    iget-object v0, p0, Lzs;->a:Lzu;

    iget-object v1, p0, Lzs;->b:Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    const-string/jumbo v2, "moments:maker:recommended_tweets:%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "add"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "click"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lzu;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;Ljava/lang/String;[Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method public a(Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;)V
    .locals 2

    .prologue
    .line 29
    sget-object v0, Lzs$1;->a:[I

    invoke-virtual {p1}, Lcom/twitter/android/moments/ui/maker/AddTweetsCategory;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 46
    :goto_0
    return-void

    .line 31
    :pswitch_0
    invoke-direct {p0}, Lzs;->c()V

    goto :goto_0

    .line 35
    :pswitch_1
    invoke-direct {p0}, Lzs;->b()V

    goto :goto_0

    .line 39
    :pswitch_2
    invoke-direct {p0}, Lzs;->d()V

    goto :goto_0

    .line 29
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
