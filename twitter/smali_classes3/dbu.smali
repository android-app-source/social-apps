.class public Ldbu;
.super Ldbx;
.source "Twttr"


# instance fields
.field private final b:Ltv/periscope/android/ui/user/g;


# direct methods
.method public constructor <init>(Ltv/periscope/android/view/aj;Ltv/periscope/android/ui/user/g;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Ldbx;-><init>(Ltv/periscope/android/view/aj;)V

    .line 17
    iput-object p2, p0, Ldbu;->b:Ltv/periscope/android/ui/user/g;

    .line 18
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    sget v0, Ltv/periscope/android/library/f$l;->ps__profile_sheet_more_options_block:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ltv/periscope/android/api/PsUser;)Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 47
    iget-object v0, p0, Ldbu;->b:Ltv/periscope/android/ui/user/g;

    iget-object v1, p1, Ltv/periscope/android/api/PsUser;->id:Ljava/lang/String;

    iget-object v2, p1, Ltv/periscope/android/api/PsUser;->twitterId:Ljava/lang/String;

    iget-object v3, p1, Ltv/periscope/android/api/PsUser;->username:Ljava/lang/String;

    move-object v5, v4

    invoke-interface/range {v0 .. v5}, Ltv/periscope/android/ui/user/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ltv/periscope/model/chat/Message;)V

    .line 48
    const/4 v0, 0x1

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x1

    return v0
.end method
