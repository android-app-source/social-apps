.class public Lsp;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsp$a;
    }
.end annotation


# instance fields
.field private final a:Lsp$a;

.field private b:Z

.field private c:Lcom/twitter/model/core/Tweet;

.field private d:I


# direct methods
.method public constructor <init>(Lsp$a;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lsp;->a:Lsp$a;

    .line 34
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lsp;->a:Lsp$a;

    invoke-interface {v0, p1}, Lsp$a;->b(I)V

    .line 52
    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 55
    iput-boolean p1, p0, Lsp;->b:Z

    .line 56
    iget-object v0, p0, Lsp;->a:Lsp$a;

    iget-boolean v1, p0, Lsp;->b:Z

    invoke-interface {v0, v1}, Lsp$a;->a(Z)V

    .line 57
    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/android/av/GalleryVideoChromeView;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lsp;->a:Lsp$a;

    invoke-interface {v0}, Lsp$a;->c()Lcom/twitter/android/av/GalleryVideoChromeView;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/model/core/Tweet;)V
    .locals 1

    .prologue
    .line 46
    iput-object p1, p0, Lsp;->c:Lcom/twitter/model/core/Tweet;

    .line 47
    iget v0, p0, Lsp;->d:I

    invoke-direct {p0, v0}, Lsp;->a(I)V

    .line 48
    return-void
.end method

.method public a(Lsk;)V
    .locals 1

    .prologue
    .line 37
    iget v0, p1, Lsk;->b:I

    iput v0, p0, Lsp;->d:I

    .line 38
    iget-boolean v0, p1, Lsk;->a:Z

    invoke-direct {p0, v0}, Lsp;->a(Z)V

    .line 39
    iget v0, p0, Lsp;->d:I

    invoke-direct {p0, v0}, Lsp;->a(I)V

    .line 40
    return-void
.end method
