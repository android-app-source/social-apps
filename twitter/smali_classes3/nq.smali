.class public Lnq;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field static a:Lnq;


# instance fields
.field private final b:Lnp;

.field private final c:Lnp;


# direct methods
.method constructor <init>(Lno;Lnn;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lnq;->b:Lnp;

    .line 54
    iput-object p2, p0, Lnq;->c:Lnp;

    .line 55
    return-void
.end method

.method public static a(Landroid/content/Context;)Lnq;
    .locals 3

    .prologue
    .line 30
    sget-object v0, Lnq;->a:Lnq;

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Lnq;

    new-instance v1, Lno;

    invoke-direct {v1, p0}, Lno;-><init>(Landroid/content/Context;)V

    new-instance v2, Lnn;

    invoke-direct {v2}, Lnn;-><init>()V

    invoke-direct {v0, v1, v2}, Lnq;-><init>(Lno;Lnn;)V

    sput-object v0, Lnq;->a:Lnq;

    .line 33
    const-class v0, Lnq;

    invoke-static {v0}, Lcru;->a(Ljava/lang/Class;)V

    .line 36
    :cond_0
    sget-object v0, Lnq;->a:Lnq;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/model/av/AVMedia;)Lnp;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lnq;->b(Lcom/twitter/model/av/AVMedia;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lnq;->b:Lnp;

    .line 67
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lnq;->c:Lnp;

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lnq;->b:Lnp;

    invoke-interface {v0}, Lnp;->a()V

    .line 85
    iget-object v0, p0, Lnq;->c:Lnp;

    invoke-interface {v0}, Lnp;->a()V

    .line 86
    return-void
.end method

.method b(Lcom/twitter/model/av/AVMedia;)Z
    .locals 2

    .prologue
    .line 76
    const-string/jumbo v0, "audio"

    invoke-interface {p1}, Lcom/twitter/model/av/AVMedia;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "audio_configurations_macawnymizer_beaconing_enabled"

    .line 77
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 76
    :goto_0
    return v0

    .line 77
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
