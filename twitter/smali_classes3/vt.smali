.class public Lvt;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lvu$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lvt$a;
    }
.end annotation


# instance fields
.field private final b:Landroid/support/v4/view/ViewPager;

.field private final c:Lcom/twitter/internal/android/widget/HorizontalListView;

.field private final d:Landroid/support/v4/app/FragmentActivity;

.field private final e:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject",
            "<",
            "Lcom/twitter/model/livevideo/b;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/twitter/android/at;

.field private g:Lrx/j;

.field private h:Lvt$a;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lvt;->e:Lrx/subjects/PublishSubject;

    .line 43
    new-instance v0, Lcom/twitter/android/at;

    .line 44
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/at;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lvt;->f:Lcom/twitter/android/at;

    .line 55
    iput-object p1, p0, Lvt;->d:Landroid/support/v4/app/FragmentActivity;

    .line 56
    const v0, 0x7f13044e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lvt;->b:Landroid/support/v4/view/ViewPager;

    .line 57
    const v0, 0x7f13044d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/HorizontalListView;

    iput-object v0, p0, Lvt;->c:Lcom/twitter/internal/android/widget/HorizontalListView;

    .line 58
    iget-object v0, p0, Lvt;->c:Lcom/twitter/internal/android/widget/HorizontalListView;

    new-instance v1, Lvt$1;

    invoke-direct {v1, p0}, Lvt$1;-><init>(Lvt;)V

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 64
    return-void
.end method

.method static synthetic a(Lvt;)Lrx/subjects/PublishSubject;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lvt;->e:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lvt;->f:Lcom/twitter/android/at;

    invoke-virtual {v0}, Lcom/twitter/android/at;->a()I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 164
    iget-object v0, p0, Lvt;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 165
    iget-object v0, p0, Lvt;->f:Lcom/twitter/android/at;

    invoke-virtual {v0, p1}, Lcom/twitter/android/at;->a(I)V

    .line 167
    :cond_0
    return-void
.end method

.method private a(Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;)V
    .locals 2

    .prologue
    .line 138
    invoke-virtual {p1}, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;->aC_()Lrx/c;

    move-result-object v0

    .line 139
    invoke-static {}, Lcre;->d()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 140
    invoke-virtual {v0}, Lrx/c;->j()Lrx/c;

    move-result-object v0

    new-instance v1, Lvt$3;

    invoke-direct {v1, p0}, Lvt$3;-><init>(Lvt;)V

    .line 141
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/d;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lvt;->g:Lrx/j;

    .line 160
    return-void
.end method

.method private a(Lcom/twitter/library/client/m;)V
    .locals 3

    .prologue
    .line 121
    iget-object v0, p0, Lvt;->d:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/library/client/m;->a(Landroid/support/v4/app/FragmentManager;)Lcom/twitter/app/common/base/BaseFragment;

    move-result-object v0

    .line 124
    invoke-direct {p0}, Lvt;->c()V

    .line 127
    instance-of v1, v0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;

    if-eqz v1, :cond_0

    .line 128
    check-cast v0, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;

    invoke-direct {p0, v0}, Lvt;->a(Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;)V

    .line 131
    :cond_0
    iget-object v0, p0, Lvt;->h:Lvt$a;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/twitter/library/client/m;->a()Lcom/twitter/app/common/base/b;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/android/ai;

    if-eqz v0, :cond_1

    .line 132
    invoke-virtual {p1}, Lcom/twitter/library/client/m;->a()Lcom/twitter/app/common/base/b;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ai;

    .line 133
    iget-object v1, p0, Lvt;->h:Lvt$a;

    invoke-virtual {v0}, Lcom/twitter/android/ai;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/twitter/android/ai;->f()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lvt$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    :cond_1
    return-void
.end method

.method static synthetic a(Lvt;I)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lvt;->a(I)V

    return-void
.end method

.method static synthetic a(Lvt;Lcom/twitter/library/client/m;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lvt;->a(Lcom/twitter/library/client/m;)V

    return-void
.end method

.method private b(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 95
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v0, :cond_0

    .line 96
    :goto_0
    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Lvt;->c:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setVisibility(I)V

    .line 98
    new-instance v0, Lcom/twitter/android/at;

    invoke-direct {v0, p1}, Lcom/twitter/android/at;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lvt;->f:Lcom/twitter/android/at;

    .line 99
    iget-object v0, p0, Lvt;->c:Lcom/twitter/internal/android/widget/HorizontalListView;

    iget-object v1, p0, Lvt;->f:Lcom/twitter/android/at;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 100
    iget-object v0, p0, Lvt;->f:Lcom/twitter/android/at;

    invoke-virtual {v0}, Lcom/twitter/android/at;->notifyDataSetChanged()V

    .line 104
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 95
    goto :goto_0

    .line 102
    :cond_1
    iget-object v0, p0, Lvt;->c:Lcom/twitter/internal/android/widget/HorizontalListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setVisibility(I)V

    goto :goto_1
.end method

.method private c()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lvt;->g:Lrx/j;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lvt;->g:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 92
    :cond_0
    return-void
.end method

.method private c(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 107
    new-instance v0, Lvs;

    iget-object v1, p0, Lvt;->d:Landroid/support/v4/app/FragmentActivity;

    iget-object v3, p0, Lvt;->b:Landroid/support/v4/view/ViewPager;

    iget-object v4, p0, Lvt;->c:Lcom/twitter/internal/android/widget/HorizontalListView;

    iget-object v5, p0, Lvt;->f:Lcom/twitter/android/at;

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lvs;-><init>(Landroid/support/v4/app/FragmentActivity;Ljava/util/List;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/HorizontalListView;Lcom/twitter/android/at;)V

    .line 108
    iget-object v1, p0, Lvt;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 109
    invoke-virtual {v0}, Lvs;->notifyDataSetChanged()V

    .line 110
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lvs;->a(I)Lcom/twitter/library/client/m;

    move-result-object v1

    invoke-direct {p0, v1}, Lvt;->a(Lcom/twitter/library/client/m;)V

    .line 111
    new-instance v1, Lvt$2;

    invoke-direct {v1, p0, v0}, Lvt$2;-><init>(Lvt;Lvs;)V

    invoke-virtual {v0, v1}, Lvs;->a(Lcom/twitter/android/AbsPagesAdapter$a;)V

    .line 117
    return-void
.end method


# virtual methods
.method public a()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/livevideo/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lvt;->e:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lvt;->b(Ljava/util/List;)V

    .line 73
    invoke-direct {p0, p1}, Lvt;->c(Ljava/util/List;)V

    .line 74
    return-void
.end method

.method public a(Lvt$a;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lvt;->h:Lvt$a;

    .line 68
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0}, Lvt;->c()V

    .line 85
    iget-object v0, p0, Lvt;->e:Lrx/subjects/PublishSubject;

    invoke-virtual {v0}, Lrx/subjects/PublishSubject;->by_()V

    .line 86
    return-void
.end method
