.class public Lrb;
.super Lcom/twitter/library/card/z;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/card/ak$a;
.implements Lcom/twitter/library/card/p$a;
.implements Lcom/twitter/library/card/q$a;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/android/card/d;

.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

.field private e:Landroid/widget/LinearLayout;

.field private f:Landroid/widget/TextView;

.field private g:J

.field private h:J

.field private i:Ljava/lang/Long;

.field private j:Lcom/twitter/library/card/CardContext;

.field private k:Lcom/twitter/model/core/TwitterUser;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private final n:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;)V
    .locals 1

    .prologue
    .line 106
    new-instance v0, Lcom/twitter/android/card/f;

    invoke-direct {v0, p1}, Lcom/twitter/android/card/f;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, p2, v0}, Lrb;-><init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;)V

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/library/widget/renderablecontent/DisplayMode;Lcom/twitter/android/card/d;)V
    .locals 7

    .prologue
    const v6, 0x7f0205a6

    const/4 v5, -0x1

    const/4 v4, -0x2

    const/4 v3, 0x0

    .line 75
    invoke-direct {p0}, Lcom/twitter/library/card/z;-><init>()V

    .line 76
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lrb;->c:Ljava/lang/ref/WeakReference;

    .line 77
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lrb;->a:Landroid/content/Context;

    .line 78
    iput-object p3, p0, Lrb;->b:Lcom/twitter/android/card/d;

    .line 79
    iput-object p2, p0, Lrb;->d:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    .line 80
    invoke-static {}, Lcom/twitter/library/dm/d;->s()Z

    move-result v0

    iput-boolean v0, p0, Lrb;->n:Z

    .line 83
    sget-object v0, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne p2, v0, :cond_0

    .line 84
    const v0, 0x7f04004b

    .line 88
    :goto_0
    iget-object v1, p0, Lrb;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lrb;->e:Landroid/widget/LinearLayout;

    .line 89
    iget-object v0, p0, Lrb;->e:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v5, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 91
    iget-object v0, p0, Lrb;->e:Landroid/widget/LinearLayout;

    const v1, 0x7f1301ad

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/ui/widget/TwitterButton;

    iput-object v0, p0, Lrb;->f:Landroid/widget/TextView;

    .line 93
    invoke-static {}, Lcom/twitter/util/s;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 94
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 96
    iget-object v1, p0, Lrb;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 97
    iget-object v0, p0, Lrb;->e:Landroid/widget/LinearLayout;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 98
    iget-object v0, p0, Lrb;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v3, v3, v6, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 103
    :goto_1
    return-void

    .line 86
    :cond_0
    const v0, 0x7f04004a

    goto :goto_0

    .line 100
    :cond_1
    iget-object v0, p0, Lrb;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v6, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 101
    iget-object v0, p0, Lrb;->f:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_1
.end method

.method static synthetic a(Lrb;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lrb;->f()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/twitter/library/card/CardContext;Lcom/twitter/model/core/TwitterUser;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 171
    if-nez p1, :cond_1

    .line 234
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    invoke-virtual {p1}, Lcom/twitter/library/card/CardContext;->e()J

    move-result-wide v0

    .line 177
    iget-object v2, p0, Lrb;->i:Ljava/lang/Long;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lrb;->i:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v2, v0

    if-eqz v0, :cond_3

    .line 178
    :cond_2
    iget-object v0, p0, Lrb;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040049

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lrb;->e:Landroid/widget/LinearLayout;

    .line 179
    iput-object v4, p0, Lrb;->f:Landroid/widget/TextView;

    .line 182
    :cond_3
    iget-object v0, p0, Lrb;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 184
    iget-object v0, p0, Lrb;->d:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    sget-object v1, Lcom/twitter/library/widget/renderablecontent/DisplayMode;->a:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    if-ne v0, v1, :cond_4

    .line 185
    iget-object v0, p0, Lrb;->b:Lcom/twitter/android/card/d;

    const-string/jumbo v1, "message_me_card_show"

    invoke-virtual {p0}, Lrb;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2, v4}, Lcom/twitter/android/card/d;->d(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/analytics/feature/model/NativeCardUserAction;)V

    .line 188
    :cond_4
    iget-object v0, p0, Lrb;->f:Landroid/widget/TextView;

    new-instance v1, Lrb$1;

    invoke-direct {v1, p0, p2}, Lrb$1;-><init>(Lrb;Lcom/twitter/model/core/TwitterUser;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method

.method static synthetic b(Lrb;)Lcom/twitter/android/card/d;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lrb;->b:Lcom/twitter/android/card/d;

    return-object v0
.end method

.method static synthetic c(Lrb;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lrb;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lrb;)Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lrb;->n:Z

    return v0
.end method

.method static synthetic e(Lrb;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lrb;->l:Ljava/lang/String;

    return-object v0
.end method

.method private f()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lrb;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic f(Lrb;)Lcom/twitter/library/card/CardContext;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lrb;->j:Lcom/twitter/library/card/CardContext;

    return-object v0
.end method

.method static synthetic g(Lrb;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lrb;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic h(Lrb;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lrb;->f:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 126
    invoke-static {}, Lcom/twitter/library/card/p;->a()Lcom/twitter/library/card/p;

    move-result-object v0

    .line 127
    iget-wide v2, p0, Lrb;->g:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/p;->b(JLjava/lang/Object;)V

    .line 128
    invoke-static {}, Lcom/twitter/library/card/q;->a()Lcom/twitter/library/card/q;

    move-result-object v0

    .line 129
    iget-wide v2, p0, Lrb;->h:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/q;->b(JLjava/lang/Object;)V

    .line 130
    iget-object v0, p0, Lrb;->i:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 131
    invoke-static {}, Lcom/twitter/library/card/ak;->a()Lcom/twitter/library/card/ak;

    move-result-object v0

    iget-object v1, p0, Lrb;->i:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/ak;->b(JLjava/lang/Object;)V

    .line 133
    :cond_0
    return-void
.end method

.method public a(JLcar;)V
    .locals 4

    .prologue
    .line 137
    iget-object v0, p0, Lrb;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 138
    const-string/jumbo v0, "cta"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    .line 139
    invoke-direct {p0}, Lrb;->f()Landroid/app/Activity;

    move-result-object v1

    .line 140
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 141
    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 142
    const-string/jumbo v3, "string"

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v3, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 143
    if-eqz v0, :cond_2

    .line 144
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 145
    iget-object v1, p0, Lrb;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    :cond_0
    :goto_0
    const-string/jumbo v0, "default_composer_text"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lrb;->l:Ljava/lang/String;

    .line 152
    const-string/jumbo v0, "welcome_message_id"

    invoke-static {v0, p3}, Lcom/twitter/library/card/ah;->a(Ljava/lang/String;Lcar;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lrb;->m:Ljava/lang/String;

    .line 154
    :cond_1
    return-void

    .line 148
    :cond_2
    iget-object v0, p0, Lrb;->f:Landroid/widget/TextView;

    const v1, 0x7f0a0544

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public a(JLcom/twitter/library/card/CardContext;)V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lrb;->b:Lcom/twitter/android/card/d;

    invoke-interface {v0, p3}, Lcom/twitter/android/card/d;->a(Lcom/twitter/library/card/CardContext;)V

    .line 165
    iput-object p3, p0, Lrb;->j:Lcom/twitter/library/card/CardContext;

    .line 166
    iget-object v0, p0, Lrb;->k:Lcom/twitter/model/core/TwitterUser;

    invoke-direct {p0, p3, v0}, Lrb;->a(Lcom/twitter/library/card/CardContext;Lcom/twitter/model/core/TwitterUser;)V

    .line 167
    return-void
.end method

.method public a(JLcom/twitter/model/core/TwitterUser;)V
    .locals 1

    .prologue
    .line 158
    iput-object p3, p0, Lrb;->k:Lcom/twitter/model/core/TwitterUser;

    .line 159
    iget-object v0, p0, Lrb;->j:Lcom/twitter/library/card/CardContext;

    invoke-direct {p0, v0, p3}, Lrb;->a(Lcom/twitter/library/card/CardContext;Lcom/twitter/model/core/TwitterUser;)V

    .line 160
    return-void
.end method

.method public a(Lcom/twitter/library/card/z$a;)V
    .locals 4

    .prologue
    .line 111
    iget-wide v0, p1, Lcom/twitter/library/card/z$a;->a:J

    iput-wide v0, p0, Lrb;->g:J

    .line 112
    iget-wide v0, p1, Lcom/twitter/library/card/z$a;->b:J

    iput-wide v0, p0, Lrb;->h:J

    .line 113
    const-string/jumbo v0, "recipient"

    iget-object v1, p1, Lcom/twitter/library/card/z$a;->c:Lcar;

    invoke-static {v0, v1}, Lcom/twitter/library/card/y;->a(Ljava/lang/String;Lcar;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lrb;->i:Ljava/lang/Long;

    .line 115
    invoke-static {}, Lcom/twitter/library/card/p;->a()Lcom/twitter/library/card/p;

    move-result-object v0

    .line 116
    iget-wide v2, p1, Lcom/twitter/library/card/z$a;->a:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/p;->a(JLjava/lang/Object;)V

    .line 117
    invoke-static {}, Lcom/twitter/library/card/q;->a()Lcom/twitter/library/card/q;

    move-result-object v0

    .line 118
    iget-wide v2, p0, Lrb;->h:J

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/q;->a(JLjava/lang/Object;)V

    .line 119
    iget-object v0, p0, Lrb;->i:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 120
    invoke-static {}, Lcom/twitter/library/card/ak;->a()Lcom/twitter/library/card/ak;

    move-result-object v0

    iget-object v1, p0, Lrb;->i:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p0}, Lcom/twitter/library/card/ak;->a(JLjava/lang/Object;)V

    .line 122
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 51
    check-cast p1, Lcom/twitter/library/card/z$a;

    invoke-virtual {p0, p1}, Lrb;->a(Lcom/twitter/library/card/z$a;)V

    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 238
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lrb;->d:Lcom/twitter/library/widget/renderablecontent/DisplayMode;

    invoke-static {v0}, Lcom/twitter/android/card/g;->a(Lcom/twitter/library/widget/renderablecontent/DisplayMode;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Landroid/view/View;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lrb;->e:Landroid/widget/LinearLayout;

    return-object v0
.end method
