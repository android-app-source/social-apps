.class public Lcqp;
.super Lcql;
.source "Twttr"

# interfaces
.implements Lcqr;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcql;-><init>()V

    .line 27
    iput-object p1, p0, Lcqp;->a:Landroid/content/Context;

    .line 28
    return-void
.end method


# virtual methods
.method protected a()Ljava/io/File;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 41
    :try_start_0
    iget-object v0, p0, Lcqp;->a:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 42
    if-eqz v0, :cond_0

    .line 48
    :goto_0
    return-object v0

    .line 42
    :cond_0
    iget-object v0, p0, Lcqp;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 45
    :catch_0
    move-exception v0

    .line 46
    invoke-static {v0}, Lcpd;->c(Ljava/lang/Throwable;)V

    :goto_1
    move-object v0, v1

    .line 48
    goto :goto_0

    .line 43
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public a(Ljava/util/List;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;)",
            "Lrx/g",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    new-instance v0, Lcqp$2;

    invoke-direct {v0, p0, p1}, Lcqp$2;-><init>(Lcqp;Ljava/util/List;)V

    invoke-static {v0}, Lcre;->a(Ljava/util/concurrent/Callable;)Lrx/g;

    move-result-object v0

    .line 98
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    goto :goto_0
.end method

.method public c(Ljava/io/File;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Lrx/g",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcqp;->a(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    new-instance v0, Lcqp$1;

    invoke-direct {v0, p0, p1}, Lcqp$1;-><init>(Lcqp;Ljava/io/File;)V

    invoke-static {v0}, Lcre;->a(Ljava/util/concurrent/Callable;)Lrx/g;

    move-result-object v0

    .line 77
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lrx/g;->a(Ljava/lang/Object;)Lrx/g;

    move-result-object v0

    goto :goto_0
.end method
