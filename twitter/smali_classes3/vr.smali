.class public final Lvr;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lvq;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/ai;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lbdh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lvr;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lvr;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lcom/twitter/android/ai;",
            ">;",
            "Lcta",
            "<",
            "Lbdh;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    sget-boolean v0, Lvr;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 22
    :cond_0
    iput-object p1, p0, Lvr;->b:Lcta;

    .line 23
    sget-boolean v0, Lvr;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 24
    :cond_1
    iput-object p2, p0, Lvr;->c:Lcta;

    .line 25
    return-void
.end method

.method public static a(Lcta;Lcta;)Ldagger/internal/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lcom/twitter/android/ai;",
            ">;",
            "Lcta",
            "<",
            "Lbdh;",
            ">;)",
            "Ldagger/internal/c",
            "<",
            "Lvq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    new-instance v0, Lvr;

    invoke-direct {v0, p0, p1}, Lvr;-><init>(Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lvq;
    .locals 3

    .prologue
    .line 29
    new-instance v2, Lvq;

    iget-object v0, p0, Lvr;->b:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ai;

    iget-object v1, p0, Lvr;->c:Lcta;

    invoke-interface {v1}, Lcta;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbdh;

    invoke-direct {v2, v0, v1}, Lvq;-><init>(Lcom/twitter/android/ai;Lbdh;)V

    return-object v2
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Lvr;->a()Lvq;

    move-result-object v0

    return-object v0
.end method
