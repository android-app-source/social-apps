.class public Lcyo;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcyn;


# instance fields
.field protected final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ltv/periscope/model/p;",
            ">;"
        }
    .end annotation
.end field

.field protected final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ltv/periscope/model/q;",
            ">;"
        }
    .end annotation
.end field

.field protected final c:Lde/greenrobot/event/c;


# direct methods
.method public constructor <init>(Lde/greenrobot/event/c;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lde/greenrobot/event/c;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ltv/periscope/model/p;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcyo;->b:Ljava/util/Map;

    .line 26
    iput-object p1, p0, Lcyo;->c:Lde/greenrobot/event/c;

    .line 27
    iput-object p2, p0, Lcyo;->a:Ljava/util/Map;

    .line 28
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ltv/periscope/model/p;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcyo;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/p;

    return-object v0
.end method

.method public a()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 134
    iget-object v0, p0, Lcyo;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 135
    iget-object v0, p0, Lcyo;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 136
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 19

    .prologue
    .line 48
    move-object/from16 v0, p0

    iget-object v0, v0, Lcyo;->b:Ljava/util/Map;

    move-object/from16 v18, v0

    const-wide/16 v2, 0x0

    move/from16 v0, p2

    int-to-long v4, v0

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    const-wide/16 v16, 0x0

    invoke-static/range {v2 .. v17}, Ltv/periscope/model/q;->a(JJJJJJJJ)Ltv/periscope/model/q;

    move-result-object v2

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    return-void
.end method

.method public a(Ljava/lang/String;Ltv/periscope/model/p;)V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcyo;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    return-void
.end method

.method public a(Ljava/lang/String;Ltv/periscope/model/q;)V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcyo;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    return-void
.end method

.method public a(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 82
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 83
    iget-object v2, p0, Lcyo;->a:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/p;

    .line 84
    if-eqz v0, :cond_0

    .line 85
    sget-object v2, Ltv/periscope/model/BroadcastState;->e:Ltv/periscope/model/BroadcastState;

    invoke-virtual {v0, v2}, Ltv/periscope/model/p;->a(Ltv/periscope/model/BroadcastState;)V

    .line 86
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ltv/periscope/model/p;->a(J)V

    .line 87
    invoke-virtual {p0, v0}, Lcyo;->a(Ltv/periscope/model/p;)V

    goto :goto_0

    .line 90
    :cond_1
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/p;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 65
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/p;

    .line 67
    invoke-virtual {p0, v0}, Lcyo;->a(Ltv/periscope/model/p;)V

    .line 68
    invoke-virtual {v0, v2, v3}, Ltv/periscope/model/p;->b(J)V

    .line 69
    invoke-virtual {p0, v0}, Lcyo;->b(Ltv/periscope/model/p;)V

    goto :goto_0

    .line 72
    :cond_0
    iget-object v0, p0, Lcyo;->c:Lde/greenrobot/event/c;

    sget-object v1, Ltv/periscope/android/event/CacheEvent;->n:Ltv/periscope/android/event/CacheEvent;

    invoke-virtual {v0, v1}, Lde/greenrobot/event/c;->d(Ljava/lang/Object;)V

    .line 73
    return-void
.end method

.method protected a(Ltv/periscope/model/p;)V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcyo;->a:Ljava/util/Map;

    invoke-virtual {p1}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/p;

    .line 94
    if-eqz v0, :cond_1

    .line 95
    invoke-virtual {v0}, Ltv/periscope/model/p;->K()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ltv/periscope/model/p;->M()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Ltv/periscope/model/p;->K()Z

    move-result v0

    if-nez v0, :cond_1

    .line 99
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ltv/periscope/model/p;->a(Z)V

    .line 101
    :cond_1
    return-void
.end method

.method public b(Ljava/lang/String;)Ltv/periscope/model/q;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcyo;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/q;

    return-object v0
.end method

.method protected b(Ltv/periscope/model/p;)V
    .locals 3

    .prologue
    .line 111
    invoke-virtual {p1}, Ltv/periscope/model/p;->c()Ljava/lang/String;

    move-result-object v1

    .line 112
    iget-object v0, p0, Lcyo;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/p;

    .line 113
    if-eqz v0, :cond_4

    .line 114
    invoke-virtual {p1}, Ltv/periscope/model/p;->P()Ljava/lang/Long;

    move-result-object v2

    if-nez v2, :cond_0

    .line 115
    invoke-virtual {v0}, Ltv/periscope/model/p;->P()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v2}, Ltv/periscope/model/p;->a(Ljava/lang/Long;)V

    .line 117
    :cond_0
    invoke-virtual {p1}, Ltv/periscope/model/p;->Q()Ljava/lang/Long;

    move-result-object v2

    if-nez v2, :cond_1

    .line 118
    invoke-virtual {v0}, Ltv/periscope/model/p;->Q()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v2}, Ltv/periscope/model/p;->b(Ljava/lang/Long;)V

    .line 120
    :cond_1
    invoke-virtual {p1}, Ltv/periscope/model/p;->R()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    .line 121
    invoke-virtual {v0}, Ltv/periscope/model/p;->R()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ltv/periscope/model/p;->a(Ljava/lang/String;)V

    .line 123
    :cond_2
    invoke-virtual {p1}, Ltv/periscope/model/p;->S()Ljava/lang/Long;

    move-result-object v2

    if-nez v2, :cond_3

    .line 124
    invoke-virtual {v0}, Ltv/periscope/model/p;->S()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v2}, Ltv/periscope/model/p;->c(Ljava/lang/Long;)V

    .line 126
    :cond_3
    invoke-virtual {v0}, Ltv/periscope/model/p;->O()Z

    move-result v0

    invoke-virtual {p1, v0}, Ltv/periscope/model/p;->c(Z)V

    .line 128
    :cond_4
    iget-object v0, p0, Lcyo;->a:Ljava/util/Map;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    return-void
.end method

.method public synthetic c(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcyo;->a(Ljava/lang/String;)Ltv/periscope/model/p;

    move-result-object v0

    return-object v0
.end method
