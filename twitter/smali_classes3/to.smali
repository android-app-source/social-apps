.class public Lto;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/livevideo/landing/i$a;


# instance fields
.field a:Lcom/twitter/model/core/Tweet;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private final b:Landroid/app/Activity;

.field private final c:Landroid/content/res/Resources;

.field private final d:Ltp;

.field private e:Lbrc;

.field private final f:Lcom/twitter/android/av/video/e$b;

.field private final g:Lbre;

.field private final h:Lcom/twitter/android/livevideo/landing/i;

.field private i:Lcom/twitter/android/av/video/e;

.field private j:Landroid/view/View$OnClickListener;

.field private k:Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView$a;

.field private l:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ltp;Lbre;)V
    .locals 1

    .prologue
    .line 73
    new-instance v0, Lcom/twitter/android/av/video/e$b;

    invoke-direct {v0}, Lcom/twitter/android/av/video/e$b;-><init>()V

    invoke-direct {p0, p1, p2, v0, p3}, Lto;-><init>(Landroid/app/Activity;Ltp;Lcom/twitter/android/av/video/e$b;Lbre;)V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Ltp;Lcom/twitter/android/av/video/e$b;Lbre;)V
    .locals 6

    .prologue
    .line 82
    new-instance v5, Lcom/twitter/android/livevideo/landing/i;

    .line 83
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {}, Lcuz;->a()Lrx/f;

    move-result-object v1

    invoke-direct {v5, v0, v1}, Lcom/twitter/android/livevideo/landing/i;-><init>(Landroid/content/res/Resources;Lrx/f;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 82
    invoke-direct/range {v0 .. v5}, Lto;-><init>(Landroid/app/Activity;Ltp;Lcom/twitter/android/av/video/e$b;Lbre;Lcom/twitter/android/livevideo/landing/i;)V

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Ltp;Lcom/twitter/android/av/video/e$b;Lbre;Lcom/twitter/android/livevideo/landing/i;)V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Lto$1;

    invoke-direct {v0, p0}, Lto$1;-><init>(Lto;)V

    iput-object v0, p0, Lto;->j:Landroid/view/View$OnClickListener;

    .line 92
    iput-object p1, p0, Lto;->b:Landroid/app/Activity;

    .line 93
    iput-object p3, p0, Lto;->f:Lcom/twitter/android/av/video/e$b;

    .line 94
    iput-object p2, p0, Lto;->d:Ltp;

    .line 95
    iput-object p4, p0, Lto;->g:Lbre;

    .line 97
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lto;->c:Landroid/content/res/Resources;

    .line 99
    iput-object p5, p0, Lto;->h:Lcom/twitter/android/livevideo/landing/i;

    .line 100
    iget-object v0, p0, Lto;->h:Lcom/twitter/android/livevideo/landing/i;

    invoke-virtual {v0, p0}, Lcom/twitter/android/livevideo/landing/i;->a(Lcom/twitter/android/livevideo/landing/i$a;)V

    .line 101
    return-void
.end method

.method private g()V
    .locals 4

    .prologue
    .line 132
    iget-object v0, p0, Lto;->e:Lbrc;

    if-nez v0, :cond_0

    .line 155
    :goto_0
    return-void

    .line 135
    :cond_0
    iget-object v0, p0, Lto;->e:Lbrc;

    invoke-virtual {v0}, Lbrc;->e()Lcas;

    move-result-object v0

    .line 136
    iget-object v1, p0, Lto;->e:Lbrc;

    invoke-virtual {v1}, Lbrc;->d()Ljava/lang/String;

    move-result-object v1

    .line 137
    if-eqz v0, :cond_1

    .line 138
    iget-object v0, p0, Lto;->d:Ltp;

    iget-object v2, p0, Lto;->e:Lbrc;

    invoke-virtual {v0, v2}, Ltp;->a(Lbrc;)V

    .line 141
    :cond_1
    iget-object v0, p0, Lto;->g:Lbre;

    iget-object v2, p0, Lto;->e:Lbrc;

    invoke-virtual {v2}, Lbrc;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lbre;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lto;->e:Lbrc;

    .line 142
    invoke-virtual {v0}, Lbrc;->f()Lcom/twitter/model/livevideo/BroadcastState;

    move-result-object v0

    sget-object v2, Lcom/twitter/model/livevideo/BroadcastState;->b:Lcom/twitter/model/livevideo/BroadcastState;

    if-ne v0, v2, :cond_2

    .line 143
    invoke-static {v1}, Lcom/twitter/util/y;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 144
    invoke-direct {p0}, Lto;->i()V

    goto :goto_0

    .line 146
    :cond_2
    iget-object v0, p0, Lto;->e:Lbrc;

    invoke-virtual {v0}, Lbrc;->f()Lcom/twitter/model/livevideo/BroadcastState;

    move-result-object v0

    sget-object v1, Lcom/twitter/model/livevideo/BroadcastState;->c:Lcom/twitter/model/livevideo/BroadcastState;

    if-ne v0, v1, :cond_3

    .line 147
    iget-object v0, p0, Lto;->d:Ltp;

    iget-object v1, p0, Lto;->c:Landroid/content/res/Resources;

    const v2, 0x7f0a04a9

    .line 148
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 147
    invoke-virtual {v0, v1}, Ltp;->a(Ljava/lang/String;)V

    .line 153
    :goto_1
    invoke-direct {p0}, Lto;->h()V

    goto :goto_0

    .line 150
    :cond_3
    iget-object v0, p0, Lto;->h:Lcom/twitter/android/livevideo/landing/i;

    iget-object v1, p0, Lto;->e:Lbrc;

    invoke-virtual {v1}, Lbrc;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/livevideo/landing/i;->d(J)V

    goto :goto_1
.end method

.method private h()V
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lto;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lto;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->a()V

    .line 169
    const/4 v0, 0x0

    iput-object v0, p0, Lto;->i:Lcom/twitter/android/av/video/e;

    .line 171
    :cond_0
    return-void
.end method

.method private i()V
    .locals 6

    .prologue
    .line 210
    iget-object v0, p0, Lto;->e:Lbrc;

    if-nez v0, :cond_1

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    iget-object v0, p0, Lto;->e:Lbrc;

    invoke-virtual {v0}, Lbrc;->p()Lcom/twitter/model/livevideo/b;

    move-result-object v1

    .line 214
    if-eqz v1, :cond_0

    iget-object v0, p0, Lto;->l:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lto;->e:Lbrc;

    invoke-virtual {v0}, Lbrc;->o()Ljava/lang/String;

    move-result-object v0

    .line 218
    const-string/jumbo v2, "live_video_reuse_card_player"

    invoke-static {v2}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 226
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Card"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lto;->e:Lbrc;

    invoke-virtual {v2}, Lbrc;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 228
    :cond_2
    new-instance v4, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;

    iget-object v2, p0, Lto;->a:Lcom/twitter/model/core/Tweet;

    invoke-direct {v4, v0, v1, v2}, Lcom/twitter/library/av/playback/livevideo/LiveVideoDataSource;-><init>(Ljava/lang/String;Lcom/twitter/model/livevideo/b;Lcom/twitter/model/core/Tweet;)V

    .line 229
    iget-object v0, p0, Lto;->i:Lcom/twitter/android/av/video/e;

    if-nez v0, :cond_3

    .line 230
    iget-object v0, p0, Lto;->d:Ltp;

    invoke-virtual {v0}, Ltp;->d()V

    .line 231
    iget-object v0, p0, Lto;->f:Lcom/twitter/android/av/video/e$b;

    iget-object v1, p0, Lto;->b:Landroid/app/Activity;

    iget-object v2, p0, Lto;->d:Ltp;

    .line 232
    invoke-virtual {v2}, Ltp;->c()Landroid/view/ViewGroup;

    move-result-object v2

    iget-object v3, p0, Lto;->l:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v5, p0, Lto;->j:Landroid/view/View$OnClickListener;

    .line 231
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/av/video/e$b;->a(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/av/playback/AVDataSource;Landroid/view/View$OnClickListener;)Lcom/twitter/android/av/video/e;

    move-result-object v0

    iput-object v0, p0, Lto;->i:Lcom/twitter/android/av/video/e;

    .line 234
    iget-object v0, p0, Lto;->i:Lcom/twitter/android/av/video/e;

    sget-object v1, Lbyo;->c:Lbyf;

    sget-object v2, Lcom/twitter/library/av/VideoPlayerView$Mode;->m:Lcom/twitter/library/av/VideoPlayerView$Mode;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/av/video/e;->a(Lbyf;Lcom/twitter/library/av/VideoPlayerView$Mode;)V

    .line 235
    iget-object v0, p0, Lto;->d:Ltp;

    invoke-virtual {v0}, Ltp;->e()Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;

    move-result-object v0

    .line 236
    iget-object v1, p0, Lto;->k:Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView$a;

    invoke-virtual {v0, v1}, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;->setOnErrorListener(Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView$a;)V

    .line 237
    iget-object v1, p0, Lto;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v1, v0}, Lcom/twitter/android/av/video/e;->a(Lcom/twitter/library/av/control/e;)V

    .line 238
    iget-object v0, p0, Lto;->d:Ltp;

    iget-object v1, p0, Lto;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v1}, Lcom/twitter/android/av/video/e;->l()Lcom/twitter/library/av/playback/AVPlayerAttachment;

    move-result-object v1

    invoke-virtual {v0, v1}, Ltp;->a(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V

    .line 239
    iget-object v0, p0, Lto;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->e()V

    .line 241
    :cond_3
    iget-object v0, p0, Lto;->d:Ltp;

    invoke-virtual {v0}, Ltp;->b()V

    goto/16 :goto_0
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lto;->d:Ltp;

    invoke-virtual {v0}, Ltp;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 179
    iput-object p1, p0, Lto;->j:Landroid/view/View$OnClickListener;

    .line 180
    iget-object v0, p0, Lto;->d:Ltp;

    invoke-virtual {v0}, Ltp;->a()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lto;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 181
    return-void
.end method

.method public a(Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lbrc;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lto;->l:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 115
    iput-object p2, p0, Lto;->e:Lbrc;

    .line 116
    invoke-direct {p0}, Lto;->g()V

    .line 117
    return-void
.end method

.method public a(Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView$a;)V
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, Lto;->k:Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView$a;

    .line 190
    return-void
.end method

.method public a(Lcom/twitter/model/core/Tweet;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lto;->a:Lcom/twitter/model/core/Tweet;

    .line 110
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lto;->d:Ltp;

    invoke-virtual {v0, p1}, Ltp;->a(Ljava/lang/String;)V

    .line 200
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lto;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lto;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->d()V

    .line 123
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lto;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lto;->i:Lcom/twitter/android/av/video/e;

    invoke-virtual {v0}, Lcom/twitter/android/av/video/e;->b()V

    .line 129
    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x0

    iput-object v0, p0, Lto;->e:Lbrc;

    .line 162
    iget-object v0, p0, Lto;->h:Lcom/twitter/android/livevideo/landing/i;

    invoke-virtual {v0}, Lcom/twitter/android/livevideo/landing/i;->a()V

    .line 163
    invoke-direct {p0}, Lto;->h()V

    .line 164
    return-void
.end method

.method public e()Lcom/twitter/library/widget/a;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lto;->i:Lcom/twitter/android/av/video/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lto;->i:Lcom/twitter/android/av/video/e;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/library/widget/a;->j:Lcom/twitter/library/widget/a;

    goto :goto_0
.end method

.method public f()V
    .locals 0

    .prologue
    .line 204
    return-void
.end method
