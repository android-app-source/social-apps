.class public Ldaw;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/view/a;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ltv/periscope/android/ui/broadcast/ao;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/ao;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Ldaw;->a:Ljava/lang/String;

    .line 16
    iput-object p2, p0, Ldaw;->b:Ltv/periscope/android/ui/broadcast/ao;

    .line 17
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 21
    sget v0, Ltv/periscope/android/library/f$f;->ps__ic_as_copylink:I

    return v0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget v0, Ltv/periscope/android/library/f$l;->ps__share_broadcast_copy_link:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 26
    sget v0, Ltv/periscope/android/library/f$d;->ps__light_grey:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 36
    sget v0, Ltv/periscope/android/library/f$d;->ps__primary_text:I

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Ldaw;->b:Ltv/periscope/android/ui/broadcast/ao;

    iget-object v1, p0, Ldaw;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/broadcast/ao;->k(Ljava/lang/String;)V

    .line 47
    const/4 v0, 0x0

    return v0
.end method

.method public f()Ltv/periscope/android/view/c;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Ltv/periscope/android/view/c;->c:Ltv/periscope/android/view/c;

    return-object v0
.end method
