.class public Ltt;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltx;


# instance fields
.field private final a:Lcom/twitter/database/model/i;

.field private final b:Lcqt;


# direct methods
.method public constructor <init>(Lcom/twitter/library/provider/t;Lcqt;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    invoke-virtual {p1}, Lcom/twitter/library/provider/t;->d()Lcom/twitter/database/schema/TwitterSchema;

    move-result-object v0

    iput-object v0, p0, Ltt;->a:Lcom/twitter/database/model/i;

    .line 40
    iput-object p2, p0, Ltt;->b:Lcqt;

    .line 41
    return-void
.end method

.method static synthetic a(Ltt;Lcom/twitter/model/livevideo/b;)J
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0, p1}, Ltt;->b(Lcom/twitter/model/livevideo/b;)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(Laxt$a;)Lcom/twitter/model/livevideo/b;
    .locals 4

    .prologue
    .line 116
    new-instance v0, Lcom/twitter/model/livevideo/b$a;

    invoke-direct {v0}, Lcom/twitter/model/livevideo/b$a;-><init>()V

    .line 117
    invoke-interface {p1}, Laxt$a;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/livevideo/b$a;->a(J)Lcom/twitter/model/livevideo/b$a;

    move-result-object v0

    .line 118
    invoke-interface {p1}, Laxt$a;->c()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/livevideo/b$a;->b(J)Lcom/twitter/model/livevideo/b$a;

    move-result-object v0

    .line 119
    invoke-interface {p1}, Laxt$a;->d()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/livevideo/b$a;->c(J)Lcom/twitter/model/livevideo/b$a;

    move-result-object v0

    .line 120
    invoke-interface {p1}, Laxt$a;->e()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/model/livevideo/b$a;->d(J)Lcom/twitter/model/livevideo/b$a;

    move-result-object v0

    .line 121
    invoke-interface {p1}, Laxt$a;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/livevideo/b$a;->b(Ljava/lang/String;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v0

    .line 122
    invoke-interface {p1}, Laxt$a;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/livevideo/b$a;->c(Ljava/lang/String;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v0

    .line 123
    invoke-interface {p1}, Laxt$a;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/livevideo/b$a;->a(Ljava/lang/String;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v0

    .line 124
    invoke-interface {p1}, Laxt$a;->i()Lcom/twitter/model/livevideo/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/livevideo/b$a;->a(Lcom/twitter/model/livevideo/a;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v0

    .line 125
    invoke-interface {p1}, Laxt$a;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/livevideo/b$a;->d(Ljava/lang/String;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v0

    .line 126
    invoke-interface {p1}, Laxt$a;->k()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/livevideo/b$a;->a(Ljava/util/List;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v0

    .line 127
    invoke-interface {p1}, Laxt$a;->l()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/livevideo/b$a;->b(Ljava/util/List;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v0

    .line 128
    invoke-interface {p1}, Laxt$a;->m()Lcom/twitter/model/livevideo/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/livevideo/b$a;->a(Lcom/twitter/model/livevideo/d;)Lcom/twitter/model/livevideo/b$a;

    move-result-object v0

    .line 129
    invoke-interface {p1}, Laxt$a;->n()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/livevideo/b$a;->a(Z)Lcom/twitter/model/livevideo/b$a;

    move-result-object v0

    .line 130
    invoke-virtual {v0}, Lcom/twitter/model/livevideo/b$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/livevideo/b;

    .line 116
    return-object v0
.end method

.method static synthetic a(Ltt;J)Lcom/twitter/model/livevideo/b;
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ltt;->b(J)Lcom/twitter/model/livevideo/b;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/twitter/model/livevideo/b;)J
    .locals 4

    .prologue
    .line 89
    iget-object v0, p1, Lcom/twitter/model/livevideo/b;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/twitter/model/livevideo/b;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/twitter/model/livevideo/b;->i:Lcom/twitter/model/livevideo/a;

    if-nez v0, :cond_1

    .line 90
    :cond_0
    const-wide/16 v0, -0x1

    .line 110
    :goto_0
    return-wide v0

    .line 93
    :cond_1
    iget-object v0, p0, Ltt;->a:Lcom/twitter/database/model/i;

    const-class v1, Laxu;

    invoke-interface {v0, v1}, Lcom/twitter/database/model/i;->c(Ljava/lang/Class;)Lcom/twitter/database/model/m;

    move-result-object v0

    .line 94
    invoke-interface {v0}, Lcom/twitter/database/model/m;->b()Lcom/twitter/database/model/h;

    move-result-object v1

    .line 95
    iget-object v0, v1, Lcom/twitter/database/model/h;->d:Ljava/lang/Object;

    check-cast v0, Laxu$a;

    iget-wide v2, p1, Lcom/twitter/model/livevideo/b;->b:J

    .line 96
    invoke-interface {v0, v2, v3}, Laxu$a;->a(J)Laxu$a;

    move-result-object v0

    iget-wide v2, p1, Lcom/twitter/model/livevideo/b;->c:J

    .line 97
    invoke-interface {v0, v2, v3}, Laxu$a;->b(J)Laxu$a;

    move-result-object v0

    iget-wide v2, p1, Lcom/twitter/model/livevideo/b;->d:J

    .line 98
    invoke-interface {v0, v2, v3}, Laxu$a;->c(J)Laxu$a;

    move-result-object v0

    iget-wide v2, p1, Lcom/twitter/model/livevideo/b;->e:J

    .line 99
    invoke-interface {v0, v2, v3}, Laxu$a;->d(J)Laxu$a;

    move-result-object v0

    iget-object v2, p1, Lcom/twitter/model/livevideo/b;->f:Ljava/lang/String;

    .line 100
    invoke-interface {v0, v2}, Laxu$a;->a(Ljava/lang/String;)Laxu$a;

    move-result-object v0

    iget-object v2, p1, Lcom/twitter/model/livevideo/b;->g:Ljava/lang/String;

    .line 101
    invoke-interface {v0, v2}, Laxu$a;->b(Ljava/lang/String;)Laxu$a;

    move-result-object v0

    iget-object v2, p1, Lcom/twitter/model/livevideo/b;->h:Ljava/lang/String;

    .line 102
    invoke-interface {v0, v2}, Laxu$a;->c(Ljava/lang/String;)Laxu$a;

    move-result-object v0

    iget-object v2, p1, Lcom/twitter/model/livevideo/b;->i:Lcom/twitter/model/livevideo/a;

    .line 103
    invoke-interface {v0, v2}, Laxu$a;->a(Lcom/twitter/model/livevideo/a;)Laxu$a;

    move-result-object v0

    iget-object v2, p0, Ltt;->b:Lcqt;

    .line 104
    invoke-interface {v2}, Lcqt;->a()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Laxu$a;->e(J)Laxu$a;

    move-result-object v0

    iget-object v2, p1, Lcom/twitter/model/livevideo/b;->j:Ljava/lang/String;

    .line 105
    invoke-interface {v0, v2}, Laxu$a;->d(Ljava/lang/String;)Laxu$a;

    move-result-object v0

    iget-object v2, p1, Lcom/twitter/model/livevideo/b;->k:Ljava/util/List;

    .line 106
    invoke-interface {v0, v2}, Laxu$a;->a(Ljava/util/List;)Laxu$a;

    move-result-object v0

    iget-object v2, p1, Lcom/twitter/model/livevideo/b;->l:Ljava/util/List;

    .line 107
    invoke-interface {v0, v2}, Laxu$a;->b(Ljava/util/List;)Laxu$a;

    move-result-object v0

    iget-object v2, p1, Lcom/twitter/model/livevideo/b;->m:Lcom/twitter/model/livevideo/d;

    .line 108
    invoke-interface {v0, v2}, Laxu$a;->a(Lcom/twitter/model/livevideo/d;)Laxu$a;

    move-result-object v0

    iget-boolean v2, p1, Lcom/twitter/model/livevideo/b;->n:Z

    .line 109
    invoke-interface {v0, v2}, Laxu$a;->a(Z)Laxu$a;

    .line 110
    invoke-virtual {v1}, Lcom/twitter/database/model/h;->b()J

    move-result-wide v0

    goto :goto_0
.end method

.method private b(J)Lcom/twitter/model/livevideo/b;
    .locals 5

    .prologue
    .line 69
    iget-object v0, p0, Ltt;->a:Lcom/twitter/database/model/i;

    const-class v1, Laxt;

    invoke-interface {v0, v1}, Lcom/twitter/database/model/i;->a(Ljava/lang/Class;)Lcom/twitter/database/model/k;

    move-result-object v0

    check-cast v0, Laxt;

    invoke-interface {v0}, Laxt;->f()Lcom/twitter/database/model/l;

    move-result-object v0

    .line 70
    const-string/jumbo v1, "event_id"

    .line 71
    invoke-static {v1}, Laux;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/twitter/database/model/l;->a(Ljava/lang/String;[Ljava/lang/Object;)Lcom/twitter/database/model/g;

    move-result-object v1

    .line 73
    :try_start_0
    invoke-virtual {v1}, Lcom/twitter/database/model/g;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, v1, Lcom/twitter/database/model/g;->a:Ljava/lang/Object;

    check-cast v0, Laxt$a;

    invoke-direct {p0, v0}, Ltt;->a(Laxt$a;)Lcom/twitter/model/livevideo/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 79
    invoke-virtual {v1}, Lcom/twitter/database/model/g;->close()V

    .line 76
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    .line 79
    invoke-virtual {v1}, Lcom/twitter/database/model/g;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/twitter/database/model/g;->close()V

    throw v0
.end method


# virtual methods
.method public a(J)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/livevideo/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    new-instance v0, Ltt$2;

    invoke-direct {v0, p0, p1, p2}, Ltt$2;-><init>(Ltt;J)V

    invoke-static {v0}, Lrx/c;->a(Ljava/util/concurrent/Callable;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/model/livevideo/b;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/livevideo/b;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/livevideo/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    new-instance v0, Ltt$1;

    invoke-direct {v0, p0, p1}, Ltt$1;-><init>(Ltt;Lcom/twitter/model/livevideo/b;)V

    invoke-static {v0}, Lrx/c;->a(Ljava/util/concurrent/Callable;)Lrx/c;

    move-result-object v0

    return-object v0
.end method
