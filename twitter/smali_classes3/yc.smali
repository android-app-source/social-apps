.class public final Lyc;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ldagger/internal/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/c",
        "<",
        "Lyb;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lzj;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxx;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lyh;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/api/moments/maker/r;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lbsb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lyc;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lyc;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcta;Lcta;Lcta;Lcta;Lcta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lzj;",
            ">;",
            "Lcta",
            "<",
            "Lxx;",
            ">;",
            "Lcta",
            "<",
            "Lyh;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/library/api/moments/maker/r;",
            ">;",
            "Lcta",
            "<",
            "Lbsb;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    sget-boolean v0, Lyc;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 35
    :cond_0
    iput-object p1, p0, Lyc;->b:Lcta;

    .line 36
    sget-boolean v0, Lyc;->a:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 37
    :cond_1
    iput-object p2, p0, Lyc;->c:Lcta;

    .line 39
    sget-boolean v0, Lyc;->a:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 40
    :cond_2
    iput-object p3, p0, Lyc;->d:Lcta;

    .line 41
    sget-boolean v0, Lyc;->a:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 42
    :cond_3
    iput-object p4, p0, Lyc;->e:Lcta;

    .line 43
    sget-boolean v0, Lyc;->a:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 44
    :cond_4
    iput-object p5, p0, Lyc;->f:Lcta;

    .line 45
    return-void
.end method

.method public static a(Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcta",
            "<",
            "Lzj;",
            ">;",
            "Lcta",
            "<",
            "Lxx;",
            ">;",
            "Lcta",
            "<",
            "Lyh;",
            ">;",
            "Lcta",
            "<",
            "Lcom/twitter/library/api/moments/maker/r;",
            ">;",
            "Lcta",
            "<",
            "Lbsb;",
            ">;)",
            "Ldagger/internal/c",
            "<",
            "Lyb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    new-instance v0, Lyc;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lyc;-><init>(Lcta;Lcta;Lcta;Lcta;Lcta;)V

    return-object v0
.end method


# virtual methods
.method public a()Lyb;
    .locals 6

    .prologue
    .line 49
    new-instance v0, Lyb;

    iget-object v1, p0, Lyc;->b:Lcta;

    .line 50
    invoke-interface {v1}, Lcta;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lzj;

    iget-object v2, p0, Lyc;->c:Lcta;

    .line 51
    invoke-interface {v2}, Lcta;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lxx;

    iget-object v3, p0, Lyc;->d:Lcta;

    .line 52
    invoke-interface {v3}, Lcta;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lyh;

    iget-object v4, p0, Lyc;->e:Lcta;

    .line 53
    invoke-interface {v4}, Lcta;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/library/api/moments/maker/r;

    iget-object v5, p0, Lyc;->f:Lcta;

    .line 54
    invoke-interface {v5}, Lcta;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lbsb;

    invoke-direct/range {v0 .. v5}, Lyb;-><init>(Lzj;Lxx;Lyh;Lcom/twitter/library/api/moments/maker/r;Lbsb;)V

    .line 49
    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lyc;->a()Lyb;

    move-result-object v0

    return-object v0
.end method
