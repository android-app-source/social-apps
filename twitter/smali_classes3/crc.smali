.class public Lcrc;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcrc$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/support/v4/content/LocalBroadcastManager;


# direct methods
.method public constructor <init>(Landroid/support/v4/content/LocalBroadcastManager;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcrc;->a:Landroid/support/v4/content/LocalBroadcastManager;

    .line 30
    return-void
.end method


# virtual methods
.method public a(Landroid/content/IntentFilter;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/IntentFilter;",
            ")",
            "Lrx/c",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    new-instance v0, Lcrc$a;

    iget-object v1, p0, Lcrc;->a:Landroid/support/v4/content/LocalBroadcastManager;

    invoke-direct {v0, v1, p1}, Lcrc$a;-><init>(Landroid/support/v4/content/LocalBroadcastManager;Landroid/content/IntentFilter;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcrc;->a:Landroid/support/v4/content/LocalBroadcastManager;

    invoke-virtual {v0, p1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 39
    return-void
.end method
