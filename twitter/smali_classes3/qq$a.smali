.class public final Lqq$a;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lqq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private a:Lano;

.field private b:Lamu;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lqq$1;)V
    .locals 0

    .prologue
    .line 284
    invoke-direct {p0}, Lqq$a;-><init>()V

    return-void
.end method

.method static synthetic a(Lqq$a;)Lano;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lqq$a;->a:Lano;

    return-object v0
.end method

.method static synthetic b(Lqq$a;)Lamu;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lqq$a;->b:Lamu;

    return-object v0
.end method


# virtual methods
.method public a(Lamu;)Lqq$a;
    .locals 1

    .prologue
    .line 308
    invoke-static {p1}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamu;

    iput-object v0, p0, Lqq$a;->b:Lamu;

    .line 309
    return-object p0
.end method

.method public a(Lano;)Lqq$a;
    .locals 1

    .prologue
    .line 303
    invoke-static {p1}, Ldagger/internal/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lano;

    iput-object v0, p0, Lqq$a;->a:Lano;

    .line 304
    return-object p0
.end method

.method public a()Lqr;
    .locals 3

    .prologue
    .line 292
    iget-object v0, p0, Lqq$a;->a:Lano;

    if-nez v0, :cond_0

    .line 293
    new-instance v0, Lano;

    invoke-direct {v0}, Lano;-><init>()V

    iput-object v0, p0, Lqq$a;->a:Lano;

    .line 295
    :cond_0
    iget-object v0, p0, Lqq$a;->b:Lamu;

    if-nez v0, :cond_1

    .line 296
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lamu;

    .line 297
    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 299
    :cond_1
    new-instance v0, Lqq;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lqq;-><init>(Lqq$a;Lqq$1;)V

    return-object v0
.end method
