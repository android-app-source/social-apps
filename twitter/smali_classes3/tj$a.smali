.class Ltj$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lti$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lti$a",
        "<",
        "Lcom/twitter/model/core/Tweet;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final c:Lcom/twitter/library/scribe/n;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/scribe/n;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Ltj$a;->a:Landroid/content/Context;

    .line 50
    iput-object p2, p0, Ltj$a;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 51
    iput-object p3, p0, Ltj$a;->c:Lcom/twitter/library/scribe/n;

    .line 52
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/model/core/Tweet;)J
    .locals 2

    .prologue
    .line 56
    iget-wide v0, p1, Lcom/twitter/model/core/Tweet;->O:J

    return-wide v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)J
    .locals 2

    .prologue
    .line 40
    check-cast p1, Lcom/twitter/model/core/Tweet;

    invoke-virtual {p0, p1}, Ltj$a;->a(Lcom/twitter/model/core/Tweet;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 62
    iget-object v0, p0, Ltj$a;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ltj$a;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->a()Ljava/lang/String;

    move-result-object v0

    .line 63
    :goto_0
    iget-object v2, p0, Ltj$a;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    if-eqz v2, :cond_0

    iget-object v1, p0, Ltj$a;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    invoke-virtual {v1}, Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;->b()Ljava/lang/String;

    move-result-object v1

    .line 64
    :cond_0
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string/jumbo v1, "stream:linger:results"

    aput-object v1, v2, v0

    invoke-static {v2}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v0, v1

    .line 62
    goto :goto_0
.end method

.method public b(Lcom/twitter/model/core/Tweet;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 4

    .prologue
    .line 70
    iget-object v0, p0, Ltj$a;->c:Lcom/twitter/library/scribe/n;

    iget-object v1, p0, Ltj$a;->a:Landroid/content/Context;

    iget-object v2, p0, Ltj$a;->b:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    const/4 v3, 0x0

    invoke-interface {v0, v1, p1, v2, v3}, Lcom/twitter/library/scribe/n;->a(Landroid/content/Context;Lcom/twitter/model/core/Tweet;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/lang/String;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic b(Ljava/lang/Object;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;
    .locals 1

    .prologue
    .line 40
    check-cast p1, Lcom/twitter/model/core/Tweet;

    invoke-virtual {p0, p1}, Ltj$a;->b(Lcom/twitter/model/core/Tweet;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v0

    return-object v0
.end method
