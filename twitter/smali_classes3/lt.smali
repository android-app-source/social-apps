.class final Llt;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Llk;


# instance fields
.field public final a:Llq;

.field public final b:Llx;

.field private c:Z


# direct methods
.method public constructor <init>(Llx;)V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Llq;

    invoke-direct {v0}, Llq;-><init>()V

    invoke-direct {p0, p1, v0}, Llt;-><init>(Llx;Llq;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Llx;Llq;)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_0
    iput-object p2, p0, Llt;->a:Llq;

    .line 30
    iput-object p1, p0, Llt;->b:Llx;

    .line 31
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 213
    iget-boolean v0, p0, Llt;->c:Z

    if-eqz v0, :cond_0

    .line 214
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 216
    :cond_0
    return-void
.end method

.method static synthetic a(Llt;)Z
    .locals 1

    .prologue
    .line 22
    iget-boolean v0, p0, Llt;->c:Z

    return v0
.end method


# virtual methods
.method public a(B)J
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, -0x1

    .line 140
    invoke-direct {p0}, Llt;->a()V

    .line 141
    const-wide/16 v0, 0x0

    .line 143
    :cond_0
    iget-object v2, p0, Llt;->a:Llq;

    invoke-virtual {v2, p1, v0, v1}, Llq;->a(BJ)J

    move-result-wide v0

    cmp-long v2, v0, v6

    if-nez v2, :cond_1

    .line 144
    iget-object v0, p0, Llt;->a:Llq;

    iget-wide v0, v0, Llq;->b:J

    .line 145
    iget-object v2, p0, Llt;->b:Llx;

    iget-object v3, p0, Llt;->a:Llq;

    const-wide/16 v4, 0x800

    invoke-interface {v2, v3, v4, v5}, Llx;->b(Llq;J)J

    move-result-wide v2

    cmp-long v2, v2, v6

    if-nez v2, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 147
    :cond_1
    return-wide v0
.end method

.method public a(Z)Ljava/lang/String;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v10, -0x1

    const-wide/16 v8, 0x1

    const-wide/16 v2, 0x0

    .line 82
    invoke-direct {p0}, Llt;->a()V

    move-wide v0, v2

    .line 85
    :cond_0
    iget-object v4, p0, Llt;->a:Llq;

    const/16 v5, 0xa

    invoke-virtual {v4, v5, v0, v1}, Llq;->a(BJ)J

    move-result-wide v0

    cmp-long v4, v0, v10

    if-nez v4, :cond_3

    .line 86
    iget-object v0, p0, Llt;->a:Llq;

    iget-wide v0, v0, Llq;->b:J

    .line 87
    iget-object v4, p0, Llt;->b:Llx;

    iget-object v5, p0, Llt;->a:Llq;

    const-wide/16 v6, 0x800

    invoke-interface {v4, v5, v6, v7}, Llx;->b(Llq;J)J

    move-result-wide v4

    cmp-long v4, v4, v10

    if-nez v4, :cond_0

    .line 88
    if-eqz p1, :cond_1

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 89
    :cond_1
    iget-object v0, p0, Llt;->a:Llq;

    iget-wide v0, v0, Llq;->b:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    iget-object v0, p0, Llt;->a:Llq;

    iget-wide v0, v0, Llq;->b:J

    invoke-virtual {p0, v0, v1}, Llt;->d(J)Ljava/lang/String;

    move-result-object v0

    .line 103
    :goto_0
    return-object v0

    .line 89
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 93
    :cond_3
    cmp-long v2, v0, v2

    if-lez v2, :cond_4

    iget-object v2, p0, Llt;->a:Llq;

    sub-long v4, v0, v8

    invoke-virtual {v2, v4, v5}, Llq;->d(J)B

    move-result v2

    const/16 v3, 0xd

    if-ne v2, v3, :cond_4

    .line 95
    sub-long/2addr v0, v8

    invoke-virtual {p0, v0, v1}, Llt;->d(J)Ljava/lang/String;

    move-result-object v0

    .line 96
    const-wide/16 v2, 0x2

    invoke-virtual {p0, v2, v3}, Llt;->b(J)V

    goto :goto_0

    .line 101
    :cond_4
    invoke-virtual {p0, v0, v1}, Llt;->d(J)Ljava/lang/String;

    move-result-object v0

    .line 102
    invoke-virtual {p0, v8, v9}, Llt;->b(J)V

    goto :goto_0
.end method

.method public a(J)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0}, Llt;->a()V

    .line 61
    :cond_0
    iget-object v0, p0, Llt;->a:Llq;

    iget-wide v0, v0, Llq;->b:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_1

    .line 62
    iget-object v0, p0, Llt;->b:Llx;

    iget-object v1, p0, Llt;->a:Llq;

    const-wide/16 v2, 0x800

    invoke-interface {v0, v1, v2, v3}, Llx;->b(Llq;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 64
    :cond_1
    return-void
.end method

.method public b(Llq;J)J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    const-wide/16 v0, -0x1

    .line 42
    cmp-long v2, p2, v4

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_0
    invoke-direct {p0}, Llt;->a()V

    .line 45
    iget-object v2, p0, Llt;->a:Llq;

    iget-wide v2, v2, Llq;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 46
    iget-object v2, p0, Llt;->b:Llx;

    iget-object v3, p0, Llt;->a:Llq;

    const-wide/16 v4, 0x800

    invoke-interface {v2, v3, v4, v5}, Llx;->b(Llq;J)J

    move-result-wide v2

    .line 47
    cmp-long v2, v2, v0

    if-nez v2, :cond_1

    .line 51
    :goto_0
    return-wide v0

    .line 50
    :cond_1
    iget-object v0, p0, Llt;->a:Llq;

    iget-wide v0, v0, Llq;->b:J

    invoke-static {p2, p3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 51
    iget-object v2, p0, Llt;->a:Llq;

    invoke-virtual {v2, p1, v0, v1}, Llq;->b(Llq;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public b()Llq;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Llt;->a:Llq;

    return-object v0
.end method

.method public b(J)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 128
    invoke-direct {p0}, Llt;->a()V

    .line 129
    :goto_0
    cmp-long v0, p1, v4

    if-lez v0, :cond_1

    .line 130
    iget-object v0, p0, Llt;->a:Llq;

    iget-wide v0, v0, Llq;->b:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    iget-object v0, p0, Llt;->b:Llx;

    iget-object v1, p0, Llt;->a:Llq;

    const-wide/16 v2, 0x800

    invoke-interface {v0, v1, v2, v3}, Llx;->b(Llq;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 131
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 133
    :cond_0
    iget-object v0, p0, Llt;->a:Llq;

    invoke-virtual {v0}, Llq;->l()J

    move-result-wide v0

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 134
    iget-object v2, p0, Llt;->a:Llq;

    invoke-virtual {v2, v0, v1}, Llq;->b(J)V

    .line 135
    sub-long/2addr p1, v0

    .line 136
    goto :goto_0

    .line 137
    :cond_1
    return-void
.end method

.method public c(J)Lll;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    invoke-virtual {p0, p1, p2}, Llt;->a(J)V

    .line 73
    iget-object v0, p0, Llt;->a:Llq;

    invoke-virtual {v0, p1, p2}, Llq;->c(J)Lll;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 202
    iget-boolean v0, p0, Llt;->c:Z

    if-eqz v0, :cond_0

    .line 206
    :goto_0
    return-void

    .line 203
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Llt;->c:Z

    .line 204
    iget-object v0, p0, Llt;->b:Llx;

    invoke-interface {v0}, Llx;->close()V

    .line 205
    iget-object v0, p0, Llt;->a:Llq;

    invoke-virtual {v0}, Llq;->o()V

    goto :goto_0
.end method

.method public d(J)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    invoke-virtual {p0, p1, p2}, Llt;->a(J)V

    .line 78
    iget-object v0, p0, Llt;->a:Llq;

    invoke-virtual {v0, p1, p2}, Llq;->e(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0}, Llt;->a()V

    .line 56
    iget-object v0, p0, Llt;->a:Llq;

    invoke-virtual {v0}, Llq;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Llt;->b:Llx;

    iget-object v1, p0, Llt;->a:Llq;

    const-wide/16 v2, 0x800

    invoke-interface {v0, v1, v2, v3}, Llx;->b(Llq;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    const-wide/16 v0, 0x1

    invoke-virtual {p0, v0, v1}, Llt;->a(J)V

    .line 68
    iget-object v0, p0, Llt;->a:Llq;

    invoke-virtual {v0}, Llq;->f()B

    move-result v0

    return v0
.end method

.method public g()S
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    const-wide/16 v0, 0x2

    invoke-virtual {p0, v0, v1}, Llt;->a(J)V

    .line 109
    iget-object v0, p0, Llt;->a:Llq;

    invoke-virtual {v0}, Llq;->g()S

    move-result v0

    return v0
.end method

.method public h()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    const-wide/16 v0, 0x2

    invoke-virtual {p0, v0, v1}, Llt;->a(J)V

    .line 114
    iget-object v0, p0, Llt;->a:Llq;

    invoke-virtual {v0}, Llq;->h()I

    move-result v0

    return v0
.end method

.method public i()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 118
    const-wide/16 v0, 0x4

    invoke-virtual {p0, v0, v1}, Llt;->a(J)V

    .line 119
    iget-object v0, p0, Llt;->a:Llq;

    invoke-virtual {v0}, Llq;->i()I

    move-result v0

    return v0
.end method

.method public j()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    const-wide/16 v0, 0x4

    invoke-virtual {p0, v0, v1}, Llt;->a(J)V

    .line 124
    iget-object v0, p0, Llt;->a:Llq;

    invoke-virtual {v0}, Llq;->j()I

    move-result v0

    return v0
.end method

.method public k()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 151
    new-instance v0, Llt$1;

    invoke-direct {v0, p0}, Llt$1;-><init>(Llt;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 209
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "buffer("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Llt;->b:Llx;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
