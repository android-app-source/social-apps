.class public Lub;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lrx/f;

.field private c:J

.field private d:Lrx/j;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-static {}, Lcws;->c()Lrx/f;

    move-result-object v0

    invoke-direct {p0, v0}, Lub;-><init>(Lrx/f;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Lrx/f;)V
    .locals 4

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lub;->a:Lrx/subjects/PublishSubject;

    .line 37
    iput-object p1, p0, Lub;->b:Lrx/f;

    .line 38
    invoke-virtual {p0}, Lub;->a()J

    move-result-wide v0

    invoke-virtual {p0}, Lub;->b()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 39
    invoke-direct {p0, v0, v1}, Lub;->c(J)Lrx/c;

    move-result-object v0

    .line 40
    iget-object v1, p0, Lub;->a:Lrx/subjects/PublishSubject;

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/d;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lub;->d:Lrx/j;

    .line 41
    return-void
.end method

.method private c(J)Lrx/c;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/c",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 118
    cmp-long v0, p1, v2

    if-lez v0, :cond_0

    .line 119
    iput-wide p1, p0, Lub;->c:J

    .line 120
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Lub;->b:Lrx/f;

    invoke-static {p1, p2, v0, v1}, Lrx/c;->a(JLjava/util/concurrent/TimeUnit;Lrx/f;)Lrx/c;

    move-result-object v0

    .line 123
    :goto_0
    return-object v0

    .line 122
    :cond_0
    iput-wide v2, p0, Lub;->c:J

    .line 123
    invoke-static {}, Lrx/c;->e()Lrx/c;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method a()J
    .locals 4

    .prologue
    .line 49
    const-string/jumbo v0, "live_video_timeline_minimum_refresh_rate_interval_seconds"

    const-wide/16 v2, 0x0

    .line 50
    invoke-static {v0, v2, v3}, Lcoj;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 49
    return-wide v0
.end method

.method public a(J)V
    .locals 5

    .prologue
    .line 76
    invoke-virtual {p0, p1, p2}, Lub;->b(J)J

    move-result-wide v0

    .line 77
    iget-wide v2, p0, Lub;->c:J

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 83
    :goto_0
    return-void

    .line 81
    :cond_0
    iget-object v2, p0, Lub;->d:Lrx/j;

    invoke-interface {v2}, Lrx/j;->B_()V

    .line 82
    invoke-direct {p0, v0, v1}, Lub;->c(J)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lub;->a:Lrx/subjects/PublishSubject;

    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/d;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lub;->d:Lrx/j;

    goto :goto_0
.end method

.method b()J
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 56
    :try_start_0
    const-string/jumbo v0, "live_video_timeline_polling_interval"

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcoj;->a(Ljava/lang/String;J)J
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 59
    :goto_0
    return-wide v0

    .line 57
    :catch_0
    move-exception v0

    .line 59
    const-string/jumbo v0, "live_video_timeline_polling_interval"

    .line 60
    invoke-static {v0}, Lcoj;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 59
    invoke-static {v0, v4, v5}, Lcom/twitter/util/y;->a(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method b(J)J
    .locals 5

    .prologue
    .line 108
    invoke-virtual {p0}, Lub;->a()J

    move-result-wide v0

    .line 109
    invoke-virtual {p0}, Lub;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 110
    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 112
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Lub;->b()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method c()Z
    .locals 1

    .prologue
    .line 66
    const-string/jumbo v0, "live_video_timeline_server_controlled_refresh_rate_enabled"

    .line 67
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    .line 66
    return v0
.end method

.method public d()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lub;->a:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lub;->a:Lrx/subjects/PublishSubject;

    invoke-virtual {v0}, Lrx/subjects/PublishSubject;->by_()V

    .line 98
    iget-object v0, p0, Lub;->d:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 99
    return-void
.end method
