.class Lcpt$5$1;
.super Lcpn;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcpt$5;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcpn",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcpt$5;

.field private final b:Lcpu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcpu",
            "<+TT;>;"
        }
    .end annotation
.end field

.field private c:Z


# direct methods
.method constructor <init>(Lcpt$5;)V
    .locals 2

    .prologue
    .line 82
    iput-object p1, p0, Lcpt$5$1;->a:Lcpt$5;

    invoke-direct {p0}, Lcpn;-><init>()V

    .line 83
    new-instance v0, Lcpu;

    iget-object v1, p0, Lcpt$5$1;->a:Lcpt$5;

    iget-object v1, v1, Lcpt$5;->a:Ljava/lang/Iterable;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, v1}, Lcpu;-><init>(Ljava/util/Iterator;)V

    iput-object v0, p0, Lcpt$5$1;->b:Lcpu;

    return-void
.end method


# virtual methods
.method protected a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 100
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcpt$5$1;->c:Z

    .line 101
    iget-object v0, p0, Lcpt$5$1;->b:Lcpu;

    invoke-virtual {v0}, Lcpu;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public hasNext()Z
    .locals 2

    .prologue
    .line 88
    iget-boolean v0, p0, Lcpt$5$1;->c:Z

    if-nez v0, :cond_1

    .line 89
    :goto_0
    iget-object v0, p0, Lcpt$5$1;->b:Lcpu;

    invoke-virtual {v0}, Lcpu;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcpt$5$1;->a:Lcpt$5;

    iget-object v0, v0, Lcpt$5;->b:Lcpv;

    iget-object v1, p0, Lcpt$5$1;->b:Lcpu;

    invoke-virtual {v1}, Lcpu;->c()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcpv;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    iget-object v0, p0, Lcpt$5$1;->b:Lcpu;

    invoke-virtual {v0}, Lcpu;->next()Ljava/lang/Object;

    goto :goto_0

    .line 92
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcpt$5$1;->c:Z

    .line 94
    :cond_1
    iget-object v0, p0, Lcpt$5$1;->b:Lcpu;

    invoke-virtual {v0}, Lcpu;->hasNext()Z

    move-result v0

    return v0
.end method
