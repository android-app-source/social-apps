.class public Lsm;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lsn;Landroid/view/ViewGroup;ILsp;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/util/List;Z)Lsl;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lsn;",
            "Landroid/view/ViewGroup;",
            "I",
            "Lsp;",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/media/ui/image/MediaImageView;",
            ">;Z)",
            "Lsl;"
        }
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p2}, Lsn;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 35
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Gallery item type not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 22
    :pswitch_0
    new-instance v0, Lsq;

    move-object v1, p1

    move-object v2, p3

    move v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lsq;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;ILsp;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/util/List;)V

    .line 31
    :goto_0
    return-object v0

    .line 26
    :pswitch_1
    new-instance v0, Lso;

    move-object v1, p1

    move-object v2, p3

    move v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    move/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lso;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;ILsp;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/util/List;Z)V

    goto :goto_0

    .line 31
    :pswitch_2
    new-instance v0, Lsr;

    move-object v1, p1

    move-object v2, p3

    move v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lsr;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;ILsp;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/util/List;)V

    goto :goto_0

    .line 19
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
