.class public Ltp;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field final a:Landroid/view/ViewGroup;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field final b:Lcom/twitter/media/ui/image/MediaImageView;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field final c:Lcom/twitter/android/moments/ui/VideoFillCropFrameLayout;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field final d:Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field final e:Landroid/view/View;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field final f:Lcom/twitter/library/widget/TightTextView;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-direct {p0, p1}, Ltp;->a(Landroid/content/Context;)Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Ltp;->a:Landroid/view/ViewGroup;

    .line 46
    iget-object v0, p0, Ltp;->a:Landroid/view/ViewGroup;

    const v1, 0x7f13059b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/ui/image/MediaImageView;

    iput-object v0, p0, Ltp;->b:Lcom/twitter/media/ui/image/MediaImageView;

    .line 47
    iget-object v0, p0, Ltp;->a:Landroid/view/ViewGroup;

    const v1, 0x7f13051e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/ui/VideoFillCropFrameLayout;

    iput-object v0, p0, Ltp;->c:Lcom/twitter/android/moments/ui/VideoFillCropFrameLayout;

    .line 48
    iget-object v0, p0, Ltp;->a:Landroid/view/ViewGroup;

    const v1, 0x7f13059c

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;

    iput-object v0, p0, Ltp;->d:Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;

    .line 49
    iget-object v0, p0, Ltp;->a:Landroid/view/ViewGroup;

    const v1, 0x7f13059d

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltp;->e:Landroid/view/View;

    .line 50
    iget-object v0, p0, Ltp;->a:Landroid/view/ViewGroup;

    const v1, 0x7f13059e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/TightTextView;

    iput-object v0, p0, Ltp;->f:Lcom/twitter/library/widget/TightTextView;

    .line 51
    return-void
.end method

.method private a(Landroid/content/Context;)Landroid/view/ViewGroup;
    .locals 4

    .prologue
    .line 93
    .line 94
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040245

    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Ltp;->a:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public a(Lbrc;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 59
    invoke-virtual {p1}, Lbrc;->e()Lcas;

    move-result-object v0

    .line 60
    if-eqz v0, :cond_0

    .line 61
    iget-object v1, p0, Ltp;->b:Lcom/twitter/media/ui/image/MediaImageView;

    new-instance v2, Lcom/twitter/media/request/a$a;

    iget-object v0, v0, Lcas;->a:Ljava/lang/String;

    invoke-direct {v2, v0}, Lcom/twitter/media/request/a$a;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/twitter/media/ui/image/MediaImageView;->b(Lcom/twitter/media/request/a$a;)Z

    .line 63
    :cond_0
    iget-object v0, p0, Ltp;->b:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, v3}, Lcom/twitter/media/ui/image/MediaImageView;->setVisibility(I)V

    .line 64
    iget-object v0, p0, Ltp;->e:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 65
    iget-object v0, p0, Ltp;->c:Lcom/twitter/android/moments/ui/VideoFillCropFrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/VideoFillCropFrameLayout;->setVisibility(I)V

    .line 66
    return-void
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Ltp;->c:Lcom/twitter/android/moments/ui/VideoFillCropFrameLayout;

    invoke-virtual {v0, p1}, Lcom/twitter/android/moments/ui/VideoFillCropFrameLayout;->setAVPlayerAttachment(Lcom/twitter/library/av/playback/AVPlayerAttachment;)V

    .line 85
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ltp;->f:Lcom/twitter/library/widget/TightTextView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/TightTextView;->setText(Ljava/lang/String;)V

    .line 89
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 69
    iget-object v0, p0, Ltp;->b:Lcom/twitter/media/ui/image/MediaImageView;

    invoke-virtual {v0, v1}, Lcom/twitter/media/ui/image/MediaImageView;->setVisibility(I)V

    .line 70
    iget-object v0, p0, Ltp;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 71
    iget-object v0, p0, Ltp;->c:Lcom/twitter/android/moments/ui/VideoFillCropFrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/moments/ui/VideoFillCropFrameLayout;->setVisibility(I)V

    .line 72
    return-void
.end method

.method public c()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Ltp;->c:Lcom/twitter/android/moments/ui/VideoFillCropFrameLayout;

    return-object v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Ltp;->c:Lcom/twitter/android/moments/ui/VideoFillCropFrameLayout;

    invoke-virtual {v0}, Lcom/twitter/android/moments/ui/VideoFillCropFrameLayout;->removeAllViews()V

    .line 81
    return-void
.end method

.method public e()Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Ltp;->d:Lcom/twitter/android/av/AutoPlayVideoPlayerChromeView;

    return-object v0
.end method
