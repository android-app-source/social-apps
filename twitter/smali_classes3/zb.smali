.class public final Lzb;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lzf;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lzb$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/api/moments/maker/f",
            "<",
            "Lcom/twitter/library/api/moments/maker/b;",
            "Lcfm;",
            "Lcom/twitter/model/core/z;",
            ">;>;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/api/moments/maker/f",
            "<",
            "Lcom/twitter/library/api/moments/maker/h;",
            "Lcfm;",
            "Lcom/twitter/model/core/z;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/api/moments/maker/f",
            "<",
            "Lcom/twitter/library/api/moments/maker/i;",
            "Lcfm;",
            "Lcom/twitter/model/core/z;",
            ">;>;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/api/moments/maker/f",
            "<",
            "Lcom/twitter/library/api/moments/maker/s;",
            "Lcfm;",
            "Lcom/twitter/model/core/z;",
            ">;>;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/api/upload/g;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/api/upload/m$a;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/api/upload/m;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/api/moments/maker/n;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/provider/t;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcnz;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lbsb;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lawb;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/Long;",
            "Lcfe;",
            ">;>;"
        }
    .end annotation
.end field

.field private q:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/api/moments/maker/j;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/support/v4/content/LocalBroadcastManager;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcrc;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcif;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcii;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/api/moments/maker/l;",
            ">;"
        }
    .end annotation
.end field

.field private w:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lzb;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lzb;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lzb$a;)V
    .locals 1

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    sget-boolean v0, Lzb;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 116
    :cond_0
    invoke-direct {p0, p1}, Lzb;->a(Lzb$a;)V

    .line 117
    return-void
.end method

.method synthetic constructor <init>(Lzb$a;Lzb$1;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lzb;-><init>(Lzb$a;)V

    return-void
.end method

.method public static a()Lzb$a;
    .locals 2

    .prologue
    .line 120
    new-instance v0, Lzb$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lzb$a;-><init>(Lzb$1;)V

    return-object v0
.end method

.method private a(Lzb$a;)V
    .locals 5

    .prologue
    .line 126
    new-instance v0, Lzb$1;

    invoke-direct {v0, p0, p1}, Lzb$1;-><init>(Lzb;Lzb$a;)V

    iput-object v0, p0, Lzb;->b:Lcta;

    .line 139
    new-instance v0, Lzb$2;

    invoke-direct {v0, p0, p1}, Lzb$2;-><init>(Lzb;Lzb$a;)V

    iput-object v0, p0, Lzb;->c:Lcta;

    .line 158
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lzb;->b:Lcta;

    iget-object v2, p0, Lzb;->c:Lcta;

    .line 154
    invoke-static {v0, v1, v2}, Lcom/twitter/library/api/moments/maker/g;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 153
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzb;->d:Lcta;

    .line 168
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lzb;->b:Lcta;

    iget-object v2, p0, Lzb;->c:Lcta;

    .line 164
    invoke-static {v0, v1, v2}, Lcom/twitter/library/api/moments/maker/g;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 163
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzb;->e:Lcta;

    .line 179
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lzb;->b:Lcta;

    iget-object v2, p0, Lzb;->c:Lcta;

    .line 174
    invoke-static {v0, v1, v2}, Lcom/twitter/library/api/moments/maker/g;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 173
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzb;->f:Lcta;

    .line 190
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lzb;->b:Lcta;

    iget-object v2, p0, Lzb;->c:Lcta;

    .line 185
    invoke-static {v0, v1, v2}, Lcom/twitter/library/api/moments/maker/g;->a(Lcsd;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 184
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzb;->g:Lcta;

    .line 194
    iget-object v0, p0, Lzb;->b:Lcta;

    iget-object v1, p0, Lzb;->c:Lcta;

    .line 195
    invoke-static {v0, v1}, Lcom/twitter/library/api/upload/h;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lzb;->h:Lcta;

    .line 197
    iget-object v0, p0, Lzb;->b:Lcta;

    .line 198
    invoke-static {v0}, Lcom/twitter/library/api/upload/o;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lzb;->i:Lcta;

    .line 200
    iget-object v0, p0, Lzb;->h:Lcta;

    iget-object v1, p0, Lzb;->i:Lcta;

    .line 201
    invoke-static {v0, v1}, Lcom/twitter/library/api/upload/n;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lzb;->j:Lcta;

    .line 203
    iget-object v0, p0, Lzb;->d:Lcta;

    iget-object v1, p0, Lzb;->e:Lcta;

    iget-object v2, p0, Lzb;->f:Lcta;

    iget-object v3, p0, Lzb;->g:Lcta;

    iget-object v4, p0, Lzb;->j:Lcta;

    .line 205
    invoke-static {v0, v1, v2, v3, v4}, Lcom/twitter/library/api/moments/maker/o;->a(Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 204
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzb;->k:Lcta;

    .line 212
    new-instance v0, Lzb$3;

    invoke-direct {v0, p0, p1}, Lzb$3;-><init>(Lzb;Lzb$a;)V

    iput-object v0, p0, Lzb;->l:Lcta;

    .line 225
    new-instance v0, Lzb$4;

    invoke-direct {v0, p0, p1}, Lzb$4;-><init>(Lzb;Lzb$a;)V

    iput-object v0, p0, Lzb;->m:Lcta;

    .line 238
    iget-object v0, p0, Lzb;->b:Lcta;

    iget-object v1, p0, Lzb;->l:Lcta;

    iget-object v2, p0, Lzb;->m:Lcta;

    .line 240
    invoke-static {v0, v1, v2}, Lzi;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 239
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzb;->n:Lcta;

    .line 245
    new-instance v0, Lzb$5;

    invoke-direct {v0, p0, p1}, Lzb$5;-><init>(Lzb;Lzb$a;)V

    iput-object v0, p0, Lzb;->o:Lcta;

    .line 258
    iget-object v0, p0, Lzb;->o:Lcta;

    .line 260
    invoke-static {v0}, Lyv;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 259
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzb;->p:Lcta;

    .line 263
    iget-object v0, p0, Lzb;->p:Lcta;

    .line 265
    invoke-static {v0}, Lcom/twitter/library/api/moments/maker/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 264
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzb;->q:Lcta;

    .line 268
    new-instance v0, Lzb$6;

    invoke-direct {v0, p0, p1}, Lzb$6;-><init>(Lzb;Lzb$a;)V

    iput-object v0, p0, Lzb;->r:Lcta;

    .line 281
    iget-object v0, p0, Lzb;->r:Lcta;

    .line 282
    invoke-static {v0}, Lcrd;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lzb;->s:Lcta;

    .line 284
    iget-object v0, p0, Lzb;->s:Lcta;

    .line 286
    invoke-static {v0}, Lcig;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 285
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzb;->t:Lcta;

    .line 288
    iget-object v0, p0, Lzb;->s:Lcta;

    .line 289
    invoke-static {v0}, Lcij;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lzb;->u:Lcta;

    .line 291
    iget-object v0, p0, Lzb;->k:Lcta;

    iget-object v1, p0, Lzb;->n:Lcta;

    iget-object v2, p0, Lzb;->q:Lcta;

    iget-object v3, p0, Lzb;->t:Lcta;

    iget-object v4, p0, Lzb;->u:Lcta;

    .line 293
    invoke-static {v0, v1, v2, v3, v4}, Lcom/twitter/library/api/moments/maker/m;->a(Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 292
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzb;->v:Lcta;

    .line 300
    iget-object v0, p0, Lzb;->v:Lcta;

    .line 302
    invoke-static {v0}, Lxd;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 301
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lzb;->w:Lcta;

    .line 303
    return-void
.end method


# virtual methods
.method public b()Lxc;
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lzb;->w:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxc;

    return-object v0
.end method
