.class public final Lyr;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lyt;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lyr$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/abs/j;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lawb;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/Long;",
            "Lcfe;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/util/m;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcqu;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxe;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/support/v4/content/LocalBroadcastManager;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcrc;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcif;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lyr;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lyr;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lyr$a;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    sget-boolean v0, Lyr;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 63
    :cond_0
    invoke-direct {p0, p1}, Lyr;->a(Lyr$a;)V

    .line 64
    return-void
.end method

.method synthetic constructor <init>(Lyr$a;Lyr$1;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lyr;-><init>(Lyr$a;)V

    return-void
.end method

.method public static a()Lyr$a;
    .locals 2

    .prologue
    .line 67
    new-instance v0, Lyr$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lyr$a;-><init>(Lyr$1;)V

    return-object v0
.end method

.method private a(Lyr$a;)V
    .locals 3

    .prologue
    .line 73
    new-instance v0, Lyr$1;

    invoke-direct {v0, p0, p1}, Lyr$1;-><init>(Lyr;Lyr$a;)V

    iput-object v0, p0, Lyr;->b:Lcta;

    .line 86
    iget-object v0, p0, Lyr;->b:Lcta;

    .line 88
    invoke-static {v0}, Lcom/twitter/app/common/abs/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 87
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyr;->c:Lcta;

    .line 90
    new-instance v0, Lyr$2;

    invoke-direct {v0, p0, p1}, Lyr$2;-><init>(Lyr;Lyr$a;)V

    iput-object v0, p0, Lyr;->d:Lcta;

    .line 103
    iget-object v0, p0, Lyr;->d:Lcta;

    .line 105
    invoke-static {v0}, Lyv;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 104
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyr;->e:Lcta;

    .line 110
    invoke-static {}, Lyy;->c()Ldagger/internal/c;

    move-result-object v0

    .line 109
    invoke-static {v0}, Lcom/twitter/library/util/n;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lyr;->f:Lcta;

    .line 114
    invoke-static {}, Lyx;->c()Ldagger/internal/c;

    move-result-object v0

    .line 113
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyr;->g:Lcta;

    .line 116
    iget-object v0, p0, Lyr;->f:Lcta;

    .line 119
    invoke-static {}, Lxg;->c()Ldagger/internal/c;

    move-result-object v1

    iget-object v2, p0, Lyr;->g:Lcta;

    .line 117
    invoke-static {v0, v1, v2}, Lxf;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lyr;->h:Lcta;

    .line 122
    new-instance v0, Lyr$3;

    invoke-direct {v0, p0, p1}, Lyr$3;-><init>(Lyr;Lyr$a;)V

    iput-object v0, p0, Lyr;->i:Lcta;

    .line 135
    iget-object v0, p0, Lyr;->i:Lcta;

    .line 136
    invoke-static {v0}, Lcrd;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lyr;->j:Lcta;

    .line 138
    iget-object v0, p0, Lyr;->j:Lcta;

    .line 140
    invoke-static {v0}, Lcig;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 139
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyr;->k:Lcta;

    .line 142
    iget-object v0, p0, Lyr;->e:Lcta;

    iget-object v1, p0, Lyr;->h:Lcta;

    iget-object v2, p0, Lyr;->k:Lcta;

    .line 144
    invoke-static {v0, v1, v2}, Lxi;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 143
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyr;->l:Lcta;

    .line 148
    return-void
.end method


# virtual methods
.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    invoke-static {}, Ldagger/internal/f;->a()Ldagger/internal/c;

    move-result-object v0

    invoke-interface {v0}, Ldagger/internal/c;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public c()Lcom/twitter/app/common/abs/j;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lyr;->c:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/j;

    return-object v0
.end method

.method public d()Lxh;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lyr;->l:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxh;

    return-object v0
.end method
