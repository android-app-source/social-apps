.class public Lfm;
.super Lfn;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lfn",
        "<",
        "Lcom/github/mikephil/charting/charts/PieChart;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/github/mikephil/charting/charts/PieChart;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lfn;-><init>(Lcom/github/mikephil/charting/charts/PieRadarChartBase;)V

    .line 14
    return-void
.end method


# virtual methods
.method protected a(IFF)Lfj;
    .locals 7

    .prologue
    .line 19
    iget-object v0, p0, Lfm;->a:Lcom/github/mikephil/charting/charts/PieRadarChartBase;

    check-cast v0, Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/PieChart;->getData()Lcom/github/mikephil/charting/data/h;

    move-result-object v0

    check-cast v0, Lcom/github/mikephil/charting/data/m;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/m;->a()Lgg;

    move-result-object v3

    .line 21
    invoke-interface {v3, p1}, Lgg;->f(I)Lcom/github/mikephil/charting/data/Entry;

    move-result-object v2

    .line 23
    new-instance v0, Lfj;

    int-to-float v1, p1

    invoke-virtual {v2}, Lcom/github/mikephil/charting/data/Entry;->b()F

    move-result v2

    const/4 v5, 0x0

    invoke-interface {v3}, Lgg;->q()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    move-result-object v6

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v6}, Lfj;-><init>(FFFFILcom/github/mikephil/charting/components/YAxis$AxisDependency;)V

    return-object v0
.end method
