.class public abstract Lsx;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsx$b;,
        Lsx$a;
    }
.end annotation


# static fields
.field private static final f:Lcom/twitter/util/serialization/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/serialization/l",
            "<",
            "Lsx;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:J

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsx;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 32
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/twitter/util/serialization/j;

    const/4 v1, 0x0

    const-class v2, Ltg;

    new-instance v3, Ltg$b;

    invoke-direct {v3}, Ltg$b;-><init>()V

    .line 33
    invoke-static {v2, v3}, Lcom/twitter/util/serialization/j;->a(Ljava/lang/Class;Lcom/twitter/util/serialization/l;)Lcom/twitter/util/serialization/j;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/twitter/util/serialization/f;->a([Lcom/twitter/util/serialization/j;)Lcom/twitter/util/serialization/l;

    move-result-object v0

    sput-object v0, Lsx;->f:Lcom/twitter/util/serialization/l;

    .line 32
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;JJLjava/util/List;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "JJ",
            "Ljava/util/List",
            "<",
            "Lsx;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lsx;->a:Ljava/lang/String;

    .line 48
    iput-wide p2, p0, Lsx;->b:J

    .line 49
    iput-wide p4, p0, Lsx;->c:J

    .line 50
    invoke-static {p6}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lsx;->d:Ljava/util/List;

    .line 51
    iput-object p7, p0, Lsx;->e:Ljava/lang/String;

    .line 52
    return-void
.end method

.method protected constructor <init>(Lsx$a;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iget-object v0, p1, Lsx$a;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lsx;->a:Ljava/lang/String;

    .line 56
    iget-wide v0, p1, Lsx$a;->b:J

    iput-wide v0, p0, Lsx;->b:J

    .line 57
    iget-wide v0, p1, Lsx$a;->c:J

    iput-wide v0, p0, Lsx;->c:J

    .line 58
    iget-object v0, p1, Lsx$a;->d:Ljava/util/List;

    invoke-static {v0}, Lcom/twitter/util/collection/h;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lsx;->d:Ljava/util/List;

    .line 59
    iget-object v0, p1, Lsx$a;->e:Ljava/lang/String;

    iput-object v0, p0, Lsx;->e:Ljava/lang/String;

    .line 60
    return-void
.end method

.method static synthetic a()Lcom/twitter/util/serialization/l;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lsx;->f:Lcom/twitter/util/serialization/l;

    return-object v0
.end method


# virtual methods
.method public a(Lsx;)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lsx;->d:Ljava/util/List;

    invoke-static {v0, p1}, Lsw;->a(Ljava/util/List;Lsx;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lsx;->d:Ljava/util/List;

    .line 64
    return-void
.end method
