.class public abstract Lsx$a;
.super Lcom/twitter/util/object/i;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40c
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lsx;",
        "B:",
        "Lsx$a",
        "<TT;TB;>;>",
        "Lcom/twitter/util/object/i",
        "<TT;>;"
    }
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field b:J

.field c:J

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsx;",
            ">;"
        }
    .end annotation
.end field

.field e:Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 66
    invoke-direct {p0}, Lcom/twitter/util/object/i;-><init>()V

    .line 69
    iput-wide v0, p0, Lsx$a;->b:J

    .line 70
    iput-wide v0, p0, Lsx$a;->c:J

    return-void
.end method


# virtual methods
.method public a(J)Lsx$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TB;"
        }
    .end annotation

    .prologue
    .line 82
    iput-wide p1, p0, Lsx$a;->b:J

    .line 83
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsx$a;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lsx$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 76
    iput-object p1, p0, Lsx$a;->a:Ljava/lang/String;

    .line 77
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsx$a;

    return-object v0
.end method

.method public a(Ljava/util/List;)Lsx$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsx;",
            ">;)TB;"
        }
    .end annotation

    .prologue
    .line 94
    iput-object p1, p0, Lsx$a;->d:Ljava/util/List;

    .line 95
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsx$a;

    return-object v0
.end method

.method public b(J)Lsx$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TB;"
        }
    .end annotation

    .prologue
    .line 88
    iput-wide p1, p0, Lsx$a;->c:J

    .line 89
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsx$a;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lsx$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TB;"
        }
    .end annotation

    .prologue
    .line 100
    iput-object p1, p0, Lsx$a;->e:Ljava/lang/String;

    .line 101
    invoke-static {p0}, Lcom/twitter/util/object/ObjectUtils;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsx$a;

    return-object v0
.end method
