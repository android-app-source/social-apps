.class public Lcyz;
.super Lcyu;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcyu",
        "<",
        "Ltv/periscope/model/user/UserItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ltv/periscope/model/user/UserItem$a;

.field private final d:Ltv/periscope/model/user/UserItem$a;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/user/UserItem;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcyt;

.field private g:Ltv/periscope/model/user/UserType;

.field private h:Ltv/periscope/model/ChannelType;

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/user/UserItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcyw;Lcyt;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcyu;-><init>(Lcyw;)V

    .line 31
    iput-object p2, p0, Lcyz;->f:Lcyt;

    .line 33
    sget-object v0, Ltv/periscope/model/user/UserType;->a:Ltv/periscope/model/user/UserType;

    iput-object v0, p0, Lcyz;->g:Ltv/periscope/model/user/UserType;

    .line 35
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcyz;->b:Ljava/lang/String;

    .line 36
    new-instance v0, Ltv/periscope/model/user/UserItem$a;

    iget-object v1, p0, Lcyz;->g:Ltv/periscope/model/user/UserType;

    invoke-direct {v0, v1}, Ltv/periscope/model/user/UserItem$a;-><init>(Ltv/periscope/model/user/UserType;)V

    iput-object v0, p0, Lcyz;->c:Ltv/periscope/model/user/UserItem$a;

    .line 37
    new-instance v0, Ltv/periscope/model/user/UserItem$a;

    sget-object v1, Ltv/periscope/model/user/UserType;->n:Ltv/periscope/model/user/UserType;

    invoke-direct {v0, v1}, Ltv/periscope/model/user/UserItem$a;-><init>(Ltv/periscope/model/user/UserType;)V

    iput-object v0, p0, Lcyz;->d:Ltv/periscope/model/user/UserItem$a;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcyz;->e:Ljava/util/List;

    .line 39
    invoke-virtual {p0}, Lcyz;->b()V

    .line 40
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcyz;->e:Ljava/util/List;

    iget-object v1, p0, Lcyz;->i:Ljava/util/List;

    invoke-static {v0, v1}, Ltv/periscope/android/util/i;->a(Ljava/util/Collection;Ljava/util/Collection;)V

    .line 58
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcyz;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcyz;->b(I)Ltv/periscope/model/user/UserItem;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ltv/periscope/model/user/UserItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 52
    iput-object p1, p0, Lcyz;->i:Ljava/util/List;

    .line 53
    invoke-direct {p0}, Lcyz;->c()V

    .line 54
    return-void
.end method

.method public a(Ltv/periscope/model/user/UserType;)V
    .locals 1

    .prologue
    .line 43
    iput-object p1, p0, Lcyz;->g:Ltv/periscope/model/user/UserType;

    .line 44
    iget-object v0, p0, Lcyz;->c:Ltv/periscope/model/user/UserItem$a;

    iput-object p1, v0, Ltv/periscope/model/user/UserItem$a;->a:Ltv/periscope/model/user/UserType;

    .line 45
    return-void
.end method

.method public b(I)Ltv/periscope/model/user/UserItem;
    .locals 3

    .prologue
    .line 80
    iget-object v0, p0, Lcyz;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/user/UserItem;

    .line 82
    invoke-interface {v0}, Ltv/periscope/model/user/UserItem;->type()Ltv/periscope/model/user/UserItem$Type;

    move-result-object v1

    sget-object v2, Ltv/periscope/model/user/UserItem$Type;->c:Ltv/periscope/model/user/UserItem$Type;

    if-ne v1, v2, :cond_1

    .line 83
    iget-object v1, p0, Lcyz;->a:Lcyw;

    check-cast v0, Ltv/periscope/model/user/e;

    invoke-virtual {v0}, Ltv/periscope/model/user/e;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcyw;->c(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/model/user/UserItem;

    .line 88
    :cond_0
    :goto_0
    return-object v0

    .line 84
    :cond_1
    invoke-interface {v0}, Ltv/periscope/model/user/UserItem;->type()Ltv/periscope/model/user/UserItem$Type;

    move-result-object v1

    sget-object v2, Ltv/periscope/model/user/UserItem$Type;->i:Ltv/periscope/model/user/UserItem$Type;

    if-ne v1, v2, :cond_0

    .line 85
    iget-object v1, p0, Lcyz;->f:Lcyt;

    check-cast v0, Ltv/periscope/model/user/d;

    invoke-virtual {v0}, Ltv/periscope/model/user/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcyt;->c(Ljava/lang/String;)Ltv/periscope/model/r;

    move-result-object v0

    goto :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    .line 61
    iget-object v0, p0, Lcyz;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 63
    const/4 v0, 0x0

    .line 64
    iget-object v1, p0, Lcyz;->h:Ltv/periscope/model/ChannelType;

    sget-object v2, Ltv/periscope/model/ChannelType;->b:Ltv/periscope/model/ChannelType;

    if-ne v1, v2, :cond_0

    .line 65
    iget-object v0, p0, Lcyz;->f:Lcyt;

    invoke-virtual {v0}, Lcyt;->a()Ljava/util/List;

    move-result-object v0

    .line 68
    :cond_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 69
    iget-object v1, p0, Lcyz;->e:Ljava/util/List;

    iget-object v2, p0, Lcyz;->d:Ltv/periscope/model/user/UserItem$a;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    iget-object v1, p0, Lcyz;->e:Ljava/util/List;

    invoke-static {v0}, Ltv/periscope/model/user/d;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 73
    :cond_1
    iget-object v0, p0, Lcyz;->e:Ljava/util/List;

    iget-object v1, p0, Lcyz;->c:Ltv/periscope/model/user/UserItem$a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    iget-object v0, p0, Lcyz;->e:Ljava/util/List;

    iget-object v1, p0, Lcyz;->a:Lcyw;

    iget-object v2, p0, Lcyz;->b:Ljava/lang/String;

    iget-object v3, p0, Lcyz;->g:Ltv/periscope/model/user/UserType;

    invoke-interface {v1, v2, v3}, Lcyw;->a(Ljava/lang/String;Ltv/periscope/model/user/UserType;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ltv/periscope/model/user/e;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 75
    invoke-direct {p0}, Lcyz;->c()V

    .line 76
    return-void
.end method
