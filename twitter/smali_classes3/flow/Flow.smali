.class public final Lflow/Flow;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lflow/Flow$b;,
        Lflow/Flow$TraversalState;,
        Lflow/Flow$a;,
        Lflow/Flow$c;,
        Lflow/Flow$d;,
        Lflow/Flow$Direction;
    }
.end annotation


# instance fields
.field private a:Lflow/b;

.field private b:Lflow/Flow$a;

.field private c:Lflow/Flow$b;


# direct methods
.method public constructor <init>(Lflow/b;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object p1, p0, Lflow/Flow;->a:Lflow/b;

    .line 88
    return-void
.end method

.method static synthetic a(Lflow/Flow;Lflow/Flow$b;)Lflow/Flow$b;
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lflow/Flow;->c:Lflow/Flow$b;

    return-object p1
.end method

.method public static a(Landroid/content/Context;)Lflow/Flow;
    .locals 1

    .prologue
    .line 36
    const-string/jumbo v0, "flow.Flow.FLOW_SERVICE"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    return-object v0
.end method

.method public static a(Landroid/view/View;)Lflow/Flow;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lflow/Flow;->a(Landroid/content/Context;)Lflow/Flow;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lflow/Flow;)Lflow/b;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lflow/Flow;->a:Lflow/b;

    return-object v0
.end method

.method static synthetic a(Lflow/Flow;Lflow/b;)Lflow/b;
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lflow/Flow;->a:Lflow/b;

    return-object p1
.end method

.method static synthetic a(Lflow/b;Lflow/b;)Lflow/b;
    .locals 1

    .prologue
    .line 27
    invoke-static {p0, p1}, Lflow/Flow;->b(Lflow/b;Lflow/b;)Lflow/b;

    move-result-object v0

    return-object v0
.end method

.method private a(Lflow/Flow$b;)V
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lflow/Flow;->c:Lflow/Flow$b;

    if-nez v0, :cond_1

    .line 230
    iput-object p1, p0, Lflow/Flow;->c:Lflow/Flow$b;

    .line 232
    iget-object v0, p0, Lflow/Flow;->b:Lflow/Flow$a;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lflow/Flow$b;->c()V

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 234
    :cond_1
    iget-object v0, p0, Lflow/Flow;->c:Lflow/Flow$b;

    invoke-virtual {v0, p1}, Lflow/Flow$b;->a(Lflow/Flow$b;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 40
    const-string/jumbo v0, "flow.Flow.FLOW_SERVICE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lflow/Flow;)Lflow/Flow$a;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lflow/Flow;->b:Lflow/Flow$a;

    return-object v0
.end method

.method private static b(Lflow/b;Lflow/b;)Lflow/b;
    .locals 6

    .prologue
    .line 239
    invoke-virtual {p0}, Lflow/b;->b()Ljava/util/Iterator;

    move-result-object v0

    .line 240
    invoke-virtual {p1}, Lflow/b;->b()Ljava/util/Iterator;

    move-result-object v1

    .line 242
    invoke-virtual {p0}, Lflow/b;->f()Lflow/b$a;

    move-result-object v2

    invoke-virtual {v2}, Lflow/b$a;->a()Lflow/b$a;

    move-result-object v2

    .line 244
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 245
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 246
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 247
    invoke-virtual {v2, v3}, Lflow/b$a;->a(Ljava/lang/Object;)Lflow/b$a;

    .line 259
    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 260
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Lflow/b$a;->a(Ljava/lang/Object;)Lflow/b$a;

    goto :goto_1

    .line 250
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 251
    invoke-virtual {v4, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 252
    invoke-virtual {v2, v4}, Lflow/b$a;->a(Ljava/lang/Object;)Lflow/b$a;

    goto :goto_0

    .line 254
    :cond_2
    invoke-virtual {v2, v3}, Lflow/b$a;->a(Ljava/lang/Object;)Lflow/b$a;

    goto :goto_1

    .line 262
    :cond_3
    invoke-virtual {v2}, Lflow/b$a;->e()Lflow/b;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lflow/Flow;)Lflow/Flow$b;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lflow/Flow;->c:Lflow/Flow$b;

    return-object v0
.end method


# virtual methods
.method public a()Lflow/b;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lflow/Flow;->a:Lflow/b;

    return-object v0
.end method

.method public a(Lflow/Flow$a;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 100
    const-string/jumbo v0, "dispatcher"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p1, v0, v1}, Lflow/d;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow$a;

    iput-object v0, p0, Lflow/Flow;->b:Lflow/Flow$a;

    .line 102
    iget-object v0, p0, Lflow/Flow;->c:Lflow/Flow$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflow/Flow;->c:Lflow/Flow$b;

    iget-object v0, v0, Lflow/Flow$b;->d:Lflow/Flow$TraversalState;

    sget-object v1, Lflow/Flow$TraversalState;->b:Lflow/Flow$TraversalState;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lflow/Flow;->c:Lflow/Flow$b;

    iget-object v0, v0, Lflow/Flow$b;->e:Lflow/Flow$b;

    if-nez v0, :cond_2

    .line 107
    :cond_0
    iget-object v0, p0, Lflow/Flow;->a:Lflow/b;

    sget-object v1, Lflow/Flow$Direction;->c:Lflow/Flow$Direction;

    invoke-virtual {p0, v0, v1}, Lflow/Flow;->a(Lflow/b;Lflow/Flow$Direction;)V

    .line 121
    :cond_1
    :goto_0
    return-void

    .line 111
    :cond_2
    iget-object v0, p0, Lflow/Flow;->c:Lflow/Flow$b;

    iget-object v0, v0, Lflow/Flow$b;->d:Lflow/Flow$TraversalState;

    sget-object v1, Lflow/Flow$TraversalState;->a:Lflow/Flow$TraversalState;

    if-ne v0, v1, :cond_3

    .line 113
    iget-object v0, p0, Lflow/Flow;->c:Lflow/Flow$b;

    invoke-virtual {v0}, Lflow/Flow$b;->c()V

    goto :goto_0

    .line 117
    :cond_3
    iget-object v0, p0, Lflow/Flow;->c:Lflow/Flow$b;

    iget-object v0, v0, Lflow/Flow$b;->d:Lflow/Flow$TraversalState;

    sget-object v1, Lflow/Flow$TraversalState;->b:Lflow/Flow$TraversalState;

    if-eq v0, v1, :cond_1

    .line 118
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Hanging traversal in unexpected state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lflow/Flow;->c:Lflow/Flow$b;

    iget-object v2, v2, Lflow/Flow$b;->d:Lflow/Flow$TraversalState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public a(Lflow/b;Lflow/Flow$Direction;)V
    .locals 1

    .prologue
    .line 139
    new-instance v0, Lflow/Flow$1;

    invoke-direct {v0, p0, p1, p2}, Lflow/Flow$1;-><init>(Lflow/Flow;Lflow/b;Lflow/Flow$Direction;)V

    invoke-direct {p0, v0}, Lflow/Flow;->a(Lflow/Flow$b;)V

    .line 144
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 162
    new-instance v0, Lflow/Flow$2;

    invoke-direct {v0, p0, p1}, Lflow/Flow$2;-><init>(Lflow/Flow;Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lflow/Flow;->a(Lflow/Flow$b;)V

    .line 201
    return-void
.end method

.method public b(Lflow/Flow$a;)V
    .locals 3

    .prologue
    .line 132
    iget-object v0, p0, Lflow/Flow;->b:Lflow/Flow$a;

    const-string/jumbo v1, "dispatcher"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1, v1, v2}, Lflow/d;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lflow/Flow;->b:Lflow/Flow$a;

    .line 133
    :cond_0
    return-void
.end method

.method public b()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 209
    iget-object v1, p0, Lflow/Flow;->a:Lflow/b;

    invoke-virtual {v1}, Lflow/b;->c()I

    move-result v1

    if-gt v1, v0, :cond_0

    iget-object v1, p0, Lflow/Flow;->c:Lflow/Flow$b;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lflow/Flow;->c:Lflow/Flow$b;

    iget-object v1, v1, Lflow/Flow$b;->d:Lflow/Flow$TraversalState;

    sget-object v2, Lflow/Flow$TraversalState;->c:Lflow/Flow$TraversalState;

    if-eq v1, v2, :cond_1

    .line 211
    :cond_0
    :goto_0
    new-instance v1, Lflow/Flow$3;

    invoke-direct {v1, p0}, Lflow/Flow$3;-><init>(Lflow/Flow;)V

    invoke-direct {p0, v1}, Lflow/Flow;->a(Lflow/Flow$b;)V

    .line 225
    return v0

    .line 209
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
