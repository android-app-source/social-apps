.class public Lmu;
.super Lmq;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmq",
        "<",
        "Ljava/lang/String;",
        "Lcom/twitter/android/provider/f;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:Lmt;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 25
    const v0, 0x7f040424

    invoke-direct {p0, p1, v0}, Lmu;-><init>(Landroid/content/Context;I)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lmq;-><init>(Landroid/content/Context;)V

    .line 22
    new-instance v0, Lmt;

    invoke-direct {v0}, Lmt;-><init>()V

    iput-object v0, p0, Lmu;->b:Lmt;

    .line 30
    iput p2, p0, Lmu;->a:I

    .line 31
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lcom/twitter/android/provider/f;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 40
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 41
    iget v1, p0, Lmu;->a:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 42
    new-instance v1, Lms;

    invoke-direct {v1, v0}, Lms;-><init>(Landroid/view/View;)V

    .line 43
    invoke-virtual {v1}, Lms;->a()Lcom/twitter/media/ui/image/UserImageView;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02008a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/media/ui/image/UserImageView;->setDefaultDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 44
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 45
    return-object v0
.end method

.method public bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 20
    check-cast p2, Lcom/twitter/android/provider/f;

    invoke-virtual {p0, p1, p2, p3}, Lmu;->a(Landroid/content/Context;Lcom/twitter/android/provider/f;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/provider/f;)V
    .locals 3

    .prologue
    .line 50
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lms;

    .line 52
    iget-object v1, p3, Lcom/twitter/android/provider/f;->d:Ljava/lang/String;

    .line 53
    invoke-virtual {v0}, Lms;->a()Lcom/twitter/media/ui/image/UserImageView;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/twitter/media/ui/image/UserImageView;->a(Ljava/lang/String;)Z

    .line 55
    invoke-virtual {v0}, Lms;->b()Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p3, Lcom/twitter/android/provider/f;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    iget-object v1, p0, Lmu;->b:Lmt;

    invoke-virtual {v1, v0, p3}, Lmt;->a(Lmt$a;Lcom/twitter/android/provider/f;)V

    .line 59
    invoke-virtual {v0}, Lms;->c()Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {p3}, Lcom/twitter/android/provider/f;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 61
    invoke-virtual {v0}, Lms;->d()Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p3, Lcom/twitter/android/provider/f;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    return-void

    .line 59
    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method

.method public bridge synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 20
    check-cast p3, Lcom/twitter/android/provider/f;

    invoke-virtual {p0, p1, p2, p3}, Lmu;->a(Landroid/view/View;Landroid/content/Context;Lcom/twitter/android/provider/f;)V

    return-void
.end method

.method public a(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lmu;->b:Lmt;

    invoke-virtual {v0, p1}, Lmt;->a(Ljava/util/Collection;)V

    .line 35
    return-void
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lmu;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/provider/f;

    .line 67
    if-eqz v0, :cond_0

    iget-wide v0, v0, Lcom/twitter/android/provider/f;->a:J

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method
