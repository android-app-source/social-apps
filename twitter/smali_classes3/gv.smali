.class public Lgv;
.super Lgp;
.source "Twttr"


# instance fields
.field protected a:Lcom/github/mikephil/charting/charts/PieChart;

.field protected b:Landroid/graphics/Paint;

.field protected c:Landroid/graphics/Paint;

.field protected d:Landroid/graphics/Paint;

.field protected e:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field protected f:Landroid/graphics/Canvas;

.field protected l:Landroid/graphics/Path;

.field protected m:Landroid/graphics/RectF;

.field private n:Landroid/text/TextPaint;

.field private p:Landroid/graphics/Paint;

.field private q:Landroid/text/StaticLayout;

.field private r:Ljava/lang/CharSequence;

.field private s:Landroid/graphics/RectF;

.field private t:[Landroid/graphics/RectF;

.field private u:Landroid/graphics/Path;

.field private v:Landroid/graphics/RectF;

.field private w:Landroid/graphics/Path;


# direct methods
.method public constructor <init>(Lcom/github/mikephil/charting/charts/PieChart;Lew;Lhp;)V
    .locals 6

    .prologue
    const/high16 v5, 0x41500000    # 13.0f

    const/4 v4, -0x1

    const/4 v3, 0x1

    .line 72
    invoke-direct {p0, p2, p3}, Lgp;-><init>(Lew;Lhp;)V

    .line 60
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lgv;->s:Landroid/graphics/RectF;

    .line 61
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/graphics/RectF;

    const/4 v1, 0x0

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    aput-object v2, v0, v1

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    aput-object v1, v0, v3

    const/4 v1, 0x2

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    aput-object v2, v0, v1

    iput-object v0, p0, Lgv;->t:[Landroid/graphics/RectF;

    .line 151
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lgv;->u:Landroid/graphics/Path;

    .line 152
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lgv;->v:Landroid/graphics/RectF;

    .line 615
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lgv;->w:Landroid/graphics/Path;

    .line 658
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lgv;->l:Landroid/graphics/Path;

    .line 731
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lgv;->m:Landroid/graphics/RectF;

    .line 73
    iput-object p1, p0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    .line 75
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lgv;->b:Landroid/graphics/Paint;

    .line 76
    iget-object v0, p0, Lgv;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 77
    iget-object v0, p0, Lgv;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 79
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lgv;->c:Landroid/graphics/Paint;

    .line 80
    iget-object v0, p0, Lgv;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 81
    iget-object v0, p0, Lgv;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 82
    iget-object v0, p0, Lgv;->c:Landroid/graphics/Paint;

    const/16 v1, 0x69

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 84
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0, v3}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lgv;->n:Landroid/text/TextPaint;

    .line 85
    iget-object v0, p0, Lgv;->n:Landroid/text/TextPaint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 86
    iget-object v0, p0, Lgv;->n:Landroid/text/TextPaint;

    const/high16 v1, 0x41400000    # 12.0f

    invoke-static {v1}, Lho;->a(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 88
    iget-object v0, p0, Lgv;->k:Landroid/graphics/Paint;

    invoke-static {v5}, Lho;->a(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 89
    iget-object v0, p0, Lgv;->k:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 90
    iget-object v0, p0, Lgv;->k:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 92
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lgv;->p:Landroid/graphics/Paint;

    .line 93
    iget-object v0, p0, Lgv;->p:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 94
    iget-object v0, p0, Lgv;->p:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 95
    iget-object v0, p0, Lgv;->p:Landroid/graphics/Paint;

    invoke-static {v5}, Lho;->a(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 97
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lgv;->d:Landroid/graphics/Paint;

    .line 98
    iget-object v0, p0, Lgv;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 99
    return-void
.end method


# virtual methods
.method protected a(Lgg;)F
    .locals 3

    .prologue
    .line 202
    invoke-interface {p1}, Lgg;->a()F

    move-result v0

    iget-object v1, p0, Lgv;->o:Lhp;

    invoke-virtual {v1}, Lhp;->o()F

    move-result v1

    div-float v1, v0, v1

    .line 203
    invoke-interface {p1}, Lgg;->v()F

    move-result v2

    iget-object v0, p0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/PieChart;->getData()Lcom/github/mikephil/charting/data/h;

    move-result-object v0

    check-cast v0, Lcom/github/mikephil/charting/data/m;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/m;->l()F

    move-result v0

    div-float v0, v2, v0

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v0, v2

    .line 205
    cmpl-float v0, v1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    .line 207
    :goto_0
    return v0

    .line 205
    :cond_0
    invoke-interface {p1}, Lgg;->a()F

    move-result v0

    goto :goto_0
.end method

.method protected a(Lhk;FFFFFF)F
    .locals 10

    .prologue
    .line 162
    const/high16 v0, 0x40000000    # 2.0f

    div-float v0, p7, v0

    add-float v0, v0, p6

    .line 165
    iget v1, p1, Lhk;->a:F

    add-float v2, p6, p7

    const v3, 0x3c8efa35

    mul-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    double-to-float v2, v2

    mul-float/2addr v2, p2

    add-float/2addr v1, v2

    .line 166
    iget v2, p1, Lhk;->b:F

    add-float v3, p6, p7

    const v4, 0x3c8efa35

    mul-float/2addr v3, v4

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float/2addr v3, p2

    add-float/2addr v2, v3

    .line 169
    iget v3, p1, Lhk;->a:F

    const v4, 0x3c8efa35

    mul-float/2addr v4, v0

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v4, p2

    add-float/2addr v3, v4

    .line 170
    iget v4, p1, Lhk;->b:F

    const v5, 0x3c8efa35

    mul-float/2addr v0, v5

    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    double-to-float v0, v6

    mul-float/2addr v0, p2

    add-float/2addr v0, v4

    .line 173
    sub-float v4, v1, p4

    float-to-double v4, v4

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 174
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    sub-float v6, v2, p5

    float-to-double v6, v6

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    .line 175
    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    add-double/2addr v4, v6

    .line 173
    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    .line 180
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v6

    const-wide v6, 0x4066800000000000L    # 180.0

    float-to-double v8, p3

    sub-double/2addr v6, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double/2addr v6, v8

    const-wide v8, 0x3f91df46a2529d39L    # 0.017453292519943295

    mul-double/2addr v6, v8

    .line 181
    invoke-static {v6, v7}, Ljava/lang/Math;->tan(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-float v4, v4

    .line 184
    sub-float v4, p2, v4

    .line 187
    float-to-double v4, v4

    add-float/2addr v1, p4

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v1, v6

    sub-float v1, v3, v1

    float-to-double v6, v1

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    .line 188
    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    add-float v1, v2, p5

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-double v0, v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    .line 189
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    add-double/2addr v0, v6

    .line 187
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    sub-double v0, v4, v0

    double-to-float v0, v0

    .line 191
    return v0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 120
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 125
    iget-object v0, p0, Lgv;->o:Lhp;

    invoke-virtual {v0}, Lhp;->n()F

    move-result v0

    float-to-int v1, v0

    .line 126
    iget-object v0, p0, Lgv;->o:Lhp;

    invoke-virtual {v0}, Lhp;->m()F

    move-result v0

    float-to-int v2, v0

    .line 128
    iget-object v0, p0, Lgv;->e:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgv;->e:Ljava/lang/ref/WeakReference;

    .line 129
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lgv;->e:Ljava/lang/ref/WeakReference;

    .line 130
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-eq v0, v2, :cond_1

    .line 132
    :cond_0
    if-lez v1, :cond_3

    if-lez v2, :cond_3

    .line 134
    new-instance v0, Ljava/lang/ref/WeakReference;

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lgv;->e:Ljava/lang/ref/WeakReference;

    .line 135
    new-instance v1, Landroid/graphics/Canvas;

    iget-object v0, p0, Lgv;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lgv;->f:Landroid/graphics/Canvas;

    .line 140
    :cond_1
    iget-object v0, p0, Lgv;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 142
    iget-object v0, p0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/PieChart;->getData()Lcom/github/mikephil/charting/data/h;

    move-result-object v0

    check-cast v0, Lcom/github/mikephil/charting/data/m;

    .line 144
    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/m;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgg;

    .line 146
    invoke-interface {v0}, Lgg;->p()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Lgg;->s()I

    move-result v2

    if-lez v2, :cond_2

    .line 147
    invoke-virtual {p0, p1, v0}, Lgv;->a(Landroid/graphics/Canvas;Lgg;)V

    goto :goto_0

    .line 149
    :cond_3
    return-void
.end method

.method protected a(Landroid/graphics/Canvas;Lgg;)V
    .locals 29

    .prologue
    .line 212
    const/4 v7, 0x0

    .line 213
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/charts/PieChart;->getRotationAngle()F

    move-result v18

    .line 215
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->g:Lew;

    invoke-virtual {v4}, Lew;->b()F

    move-result v19

    .line 216
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->g:Lew;

    invoke-virtual {v4}, Lew;->a()F

    move-result v20

    .line 218
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/charts/PieChart;->getCircleBox()Landroid/graphics/RectF;

    move-result-object v21

    .line 220
    invoke-interface/range {p2 .. p2}, Lgg;->s()I

    move-result v22

    .line 221
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/charts/PieChart;->getDrawAngles()[F

    move-result-object v23

    .line 222
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/charts/PieChart;->getCenterCircleBox()Lhk;

    move-result-object v5

    .line 223
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/charts/PieChart;->getRadius()F

    move-result v6

    .line 224
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/charts/PieChart;->d()Z

    move-result v4

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/charts/PieChart;->c()Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x1

    move/from16 v17, v4

    .line 225
    :goto_0
    if-eqz v17, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    .line 226
    invoke-virtual {v4}, Lcom/github/mikephil/charting/charts/PieChart;->getHoleRadius()F

    move-result v4

    const/high16 v8, 0x42c80000    # 100.0f

    div-float/2addr v4, v8

    mul-float/2addr v4, v6

    move v12, v4

    .line 229
    :goto_1
    const/4 v13, 0x0

    .line 230
    const/4 v4, 0x0

    move v8, v4

    :goto_2
    move/from16 v0, v22

    if-ge v8, v0, :cond_2

    .line 232
    move-object/from16 v0, p2

    invoke-interface {v0, v8}, Lgg;->f(I)Lcom/github/mikephil/charting/data/Entry;

    move-result-object v4

    check-cast v4, Lcom/github/mikephil/charting/data/PieEntry;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/data/PieEntry;->b()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    sget v9, Lho;->b:F

    cmpl-float v4, v4, v9

    if-lez v4, :cond_14

    .line 233
    add-int/lit8 v4, v13, 0x1

    .line 230
    :goto_3
    add-int/lit8 v8, v8, 0x1

    move v13, v4

    goto :goto_2

    .line 224
    :cond_0
    const/4 v4, 0x0

    move/from16 v17, v4

    goto :goto_0

    .line 226
    :cond_1
    const/4 v4, 0x0

    move v12, v4

    goto :goto_1

    .line 237
    :cond_2
    const/4 v4, 0x1

    if-gt v13, v4, :cond_a

    const/4 v4, 0x0

    move v14, v4

    .line 239
    :goto_4
    const/4 v4, 0x0

    move v15, v4

    move/from16 v16, v7

    :goto_5
    move/from16 v0, v22

    if-ge v15, v0, :cond_12

    .line 241
    aget v24, v23, v15

    .line 244
    move-object/from16 v0, p2

    invoke-interface {v0, v15}, Lgg;->f(I)Lcom/github/mikephil/charting/data/Entry;

    move-result-object v4

    .line 247
    invoke-virtual {v4}, Lcom/github/mikephil/charting/data/Entry;->b()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    sget v7, Lho;->b:F

    cmpl-float v4, v4, v7

    if-lez v4, :cond_9

    .line 249
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v4, v15}, Lcom/github/mikephil/charting/charts/PieChart;->a(I)Z

    move-result v4

    if-nez v4, :cond_9

    .line 251
    const/4 v4, 0x0

    cmpl-float v4, v14, v4

    if-lez v4, :cond_b

    const/high16 v4, 0x43340000    # 180.0f

    cmpg-float v4, v24, v4

    if-gtz v4, :cond_b

    const/4 v4, 0x1

    .line 253
    :goto_6
    move-object/from16 v0, p0

    iget-object v7, v0, Lgv;->h:Landroid/graphics/Paint;

    move-object/from16 v0, p2

    invoke-interface {v0, v15}, Lgg;->b(I)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 255
    const/4 v7, 0x1

    if-ne v13, v7, :cond_c

    const/4 v7, 0x0

    .line 258
    :goto_7
    const/high16 v8, 0x40000000    # 2.0f

    div-float v8, v7, v8

    add-float v8, v8, v16

    mul-float v8, v8, v20

    add-float v10, v18, v8

    .line 259
    sub-float v7, v24, v7

    mul-float v11, v7, v20

    .line 260
    const/4 v7, 0x0

    cmpg-float v7, v11, v7

    if-gez v7, :cond_3

    .line 261
    const/4 v11, 0x0

    .line 264
    :cond_3
    move-object/from16 v0, p0

    iget-object v7, v0, Lgv;->u:Landroid/graphics/Path;

    invoke-virtual {v7}, Landroid/graphics/Path;->reset()V

    .line 266
    iget v7, v5, Lhk;->a:F

    const v8, 0x3c8efa35

    mul-float/2addr v8, v10

    float-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    double-to-float v8, v8

    mul-float/2addr v8, v6

    add-float/2addr v8, v7

    .line 267
    iget v7, v5, Lhk;->b:F

    const v9, 0x3c8efa35

    mul-float/2addr v9, v10

    float-to-double v0, v9

    move-wide/from16 v26, v0

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->sin(D)D

    move-result-wide v26

    move-wide/from16 v0, v26

    double-to-float v9, v0

    mul-float/2addr v9, v6

    add-float/2addr v9, v7

    .line 269
    const/high16 v7, 0x43b40000    # 360.0f

    rem-float v7, v11, v7

    sget v25, Lho;->b:F

    cmpg-float v7, v7, v25

    if-gtz v7, :cond_d

    .line 271
    move-object/from16 v0, p0

    iget-object v7, v0, Lgv;->u:Landroid/graphics/Path;

    iget v0, v5, Lhk;->a:F

    move/from16 v25, v0

    iget v0, v5, Lhk;->b:F

    move/from16 v26, v0

    sget-object v27, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    move/from16 v0, v25

    move/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v7, v0, v1, v6, v2}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 284
    :goto_8
    move-object/from16 v0, p0

    iget-object v7, v0, Lgv;->v:Landroid/graphics/RectF;

    iget v0, v5, Lhk;->a:F

    move/from16 v25, v0

    sub-float v25, v25, v12

    iget v0, v5, Lhk;->b:F

    move/from16 v26, v0

    sub-float v26, v26, v12

    iget v0, v5, Lhk;->a:F

    move/from16 v27, v0

    add-float v27, v27, v12

    iget v0, v5, Lhk;->b:F

    move/from16 v28, v0

    add-float v28, v28, v12

    move/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v28

    invoke-virtual {v7, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 290
    if-eqz v17, :cond_10

    const/4 v7, 0x0

    cmpl-float v7, v12, v7

    if-gtz v7, :cond_4

    if-eqz v4, :cond_10

    .line 293
    :cond_4
    if-eqz v4, :cond_13

    .line 294
    mul-float v7, v24, v20

    move-object/from16 v4, p0

    .line 295
    invoke-virtual/range {v4 .. v11}, Lgv;->a(Lhk;FFFFFF)F

    move-result v4

    .line 302
    const/4 v7, 0x0

    cmpg-float v7, v4, v7

    if-gez v7, :cond_5

    .line 303
    neg-float v4, v4

    .line 305
    :cond_5
    invoke-static {v12, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    move v7, v4

    .line 308
    :goto_9
    const/4 v4, 0x1

    if-eq v13, v4, :cond_6

    const/4 v4, 0x0

    cmpl-float v4, v7, v4

    if-nez v4, :cond_e

    :cond_6
    const/4 v4, 0x0

    .line 311
    :goto_a
    const/high16 v8, 0x40000000    # 2.0f

    div-float v8, v4, v8

    add-float v8, v8, v16

    mul-float v8, v8, v20

    add-float v8, v8, v18

    .line 312
    sub-float v4, v24, v4

    mul-float v4, v4, v20

    .line 313
    const/4 v9, 0x0

    cmpg-float v9, v4, v9

    if-gez v9, :cond_7

    .line 314
    const/4 v4, 0x0

    .line 316
    :cond_7
    add-float/2addr v8, v4

    .line 318
    const/high16 v9, 0x43b40000    # 360.0f

    rem-float v9, v11, v9

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-nez v9, :cond_f

    .line 320
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->u:Landroid/graphics/Path;

    iget v8, v5, Lhk;->a:F

    iget v9, v5, Lhk;->b:F

    sget-object v10, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual {v4, v8, v9, v7, v10}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 368
    :cond_8
    :goto_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->u:Landroid/graphics/Path;

    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 370
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->f:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v7, v0, Lgv;->u:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v8, v0, Lgv;->h:Landroid/graphics/Paint;

    invoke-virtual {v4, v7, v8}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 374
    :cond_9
    mul-float v4, v24, v19

    add-float v7, v16, v4

    .line 239
    add-int/lit8 v4, v15, 0x1

    move v15, v4

    move/from16 v16, v7

    goto/16 :goto_5

    .line 237
    :cond_a
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lgv;->a(Lgg;)F

    move-result v4

    move v14, v4

    goto/16 :goto_4

    .line 251
    :cond_b
    const/4 v4, 0x0

    goto/16 :goto_6

    .line 255
    :cond_c
    const v7, 0x3c8efa35

    mul-float/2addr v7, v6

    div-float v7, v14, v7

    goto/16 :goto_7

    .line 274
    :cond_d
    move-object/from16 v0, p0

    iget-object v7, v0, Lgv;->u:Landroid/graphics/Path;

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 276
    move-object/from16 v0, p0

    iget-object v7, v0, Lgv;->u:Landroid/graphics/Path;

    move-object/from16 v0, v21

    invoke-virtual {v7, v0, v10, v11}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    goto/16 :goto_8

    .line 308
    :cond_e
    const v4, 0x3c8efa35

    mul-float/2addr v4, v7

    div-float v4, v14, v4

    goto :goto_a

    .line 323
    :cond_f
    move-object/from16 v0, p0

    iget-object v9, v0, Lgv;->u:Landroid/graphics/Path;

    iget v10, v5, Lhk;->a:F

    const v11, 0x3c8efa35

    mul-float/2addr v11, v8

    float-to-double v0, v11

    move-wide/from16 v26, v0

    .line 324
    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->cos(D)D

    move-result-wide v26

    move-wide/from16 v0, v26

    double-to-float v11, v0

    mul-float/2addr v11, v7

    add-float/2addr v10, v11

    iget v11, v5, Lhk;->b:F

    const v25, 0x3c8efa35

    mul-float v25, v25, v8

    move/from16 v0, v25

    float-to-double v0, v0

    move-wide/from16 v26, v0

    .line 325
    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->sin(D)D

    move-result-wide v26

    move-wide/from16 v0, v26

    double-to-float v0, v0

    move/from16 v25, v0

    mul-float v7, v7, v25

    add-float/2addr v7, v11

    .line 323
    invoke-virtual {v9, v10, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 327
    move-object/from16 v0, p0

    iget-object v7, v0, Lgv;->u:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v9, v0, Lgv;->v:Landroid/graphics/RectF;

    neg-float v4, v4

    invoke-virtual {v7, v9, v8, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    goto/16 :goto_b

    .line 335
    :cond_10
    const/high16 v7, 0x43b40000    # 360.0f

    rem-float v7, v11, v7

    const/16 v25, 0x0

    cmpl-float v7, v7, v25

    if-eqz v7, :cond_8

    .line 336
    if-eqz v4, :cond_11

    .line 338
    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v11, v4

    add-float v25, v10, v4

    .line 340
    mul-float v7, v24, v20

    move-object/from16 v4, p0

    .line 341
    invoke-virtual/range {v4 .. v11}, Lgv;->a(Lhk;FFFFFF)F

    move-result v4

    .line 350
    iget v7, v5, Lhk;->a:F

    const v8, 0x3c8efa35

    mul-float v8, v8, v25

    float-to-double v8, v8

    .line 351
    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    double-to-float v8, v8

    mul-float/2addr v8, v4

    add-float/2addr v7, v8

    .line 352
    iget v8, v5, Lhk;->b:F

    const v9, 0x3c8efa35

    mul-float v9, v9, v25

    float-to-double v10, v9

    .line 353
    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    double-to-float v9, v10

    mul-float/2addr v4, v9

    add-float/2addr v4, v8

    .line 355
    move-object/from16 v0, p0

    iget-object v8, v0, Lgv;->u:Landroid/graphics/Path;

    invoke-virtual {v8, v7, v4}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_b

    .line 360
    :cond_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->u:Landroid/graphics/Path;

    iget v7, v5, Lhk;->a:F

    iget v8, v5, Lhk;->b:F

    invoke-virtual {v4, v7, v8}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_b

    .line 377
    :cond_12
    invoke-static {v5}, Lhk;->a(Lhk;)V

    .line 378
    return-void

    :cond_13
    move v7, v12

    goto/16 :goto_9

    :cond_14
    move v4, v13

    goto/16 :goto_3
.end method

.method protected a(Landroid/graphics/Canvas;Ljava/lang/String;FF)V
    .locals 1

    .prologue
    .line 604
    iget-object v0, p0, Lgv;->p:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, p3, p4, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 605
    return-void
.end method

.method public a(Landroid/graphics/Canvas;[Lfj;)V
    .locals 28

    .prologue
    .line 735
    move-object/from16 v0, p0

    iget-object v2, v0, Lgv;->g:Lew;

    invoke-virtual {v2}, Lew;->b()F

    move-result v17

    .line 736
    move-object/from16 v0, p0

    iget-object v2, v0, Lgv;->g:Lew;

    invoke-virtual {v2}, Lew;->a()F

    move-result v18

    .line 739
    move-object/from16 v0, p0

    iget-object v2, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/charts/PieChart;->getRotationAngle()F

    move-result v19

    .line 741
    move-object/from16 v0, p0

    iget-object v2, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/charts/PieChart;->getDrawAngles()[F

    move-result-object v20

    .line 742
    move-object/from16 v0, p0

    iget-object v2, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/charts/PieChart;->getAbsoluteAngles()[F

    move-result-object v21

    .line 743
    move-object/from16 v0, p0

    iget-object v2, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/charts/PieChart;->getCenterCircleBox()Lhk;

    move-result-object v3

    .line 744
    move-object/from16 v0, p0

    iget-object v2, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/charts/PieChart;->getRadius()F

    move-result v4

    .line 745
    move-object/from16 v0, p0

    iget-object v2, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/charts/PieChart;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/charts/PieChart;->c()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    move/from16 v16, v2

    .line 746
    :goto_0
    if-eqz v16, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    .line 747
    invoke-virtual {v2}, Lcom/github/mikephil/charting/charts/PieChart;->getHoleRadius()F

    move-result v2

    const/high16 v5, 0x42c80000    # 100.0f

    div-float/2addr v2, v5

    mul-float/2addr v2, v4

    move v10, v2

    .line 750
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lgv;->m:Landroid/graphics/RectF;

    move-object/from16 v22, v0

    .line 751
    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 753
    const/4 v2, 0x0

    move v11, v2

    :goto_2
    move-object/from16 v0, p2

    array-length v2, v0

    if-ge v11, v2, :cond_17

    .line 756
    aget-object v2, p2, v11

    invoke-virtual {v2}, Lfj;->a()F

    move-result v2

    float-to-int v6, v2

    .line 758
    move-object/from16 v0, v20

    array-length v2, v0

    if-lt v6, v2, :cond_3

    .line 753
    :cond_0
    :goto_3
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    goto :goto_2

    .line 745
    :cond_1
    const/4 v2, 0x0

    move/from16 v16, v2

    goto :goto_0

    .line 747
    :cond_2
    const/4 v2, 0x0

    move v10, v2

    goto :goto_1

    .line 761
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/charts/PieChart;->getData()Lcom/github/mikephil/charting/data/h;

    move-result-object v2

    check-cast v2, Lcom/github/mikephil/charting/data/m;

    aget-object v5, p2, v11

    .line 763
    invoke-virtual {v5}, Lfj;->f()I

    move-result v5

    .line 762
    invoke-virtual {v2, v5}, Lcom/github/mikephil/charting/data/m;->c(I)Lgg;

    move-result-object v7

    .line 765
    if-eqz v7, :cond_0

    invoke-interface {v7}, Lgg;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 768
    invoke-interface {v7}, Lgg;->s()I

    move-result v8

    .line 769
    const/4 v12, 0x0

    .line 770
    const/4 v2, 0x0

    move v5, v2

    :goto_4
    if-ge v5, v8, :cond_4

    .line 772
    invoke-interface {v7, v5}, Lgg;->f(I)Lcom/github/mikephil/charting/data/Entry;

    move-result-object v2

    check-cast v2, Lcom/github/mikephil/charting/data/PieEntry;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/data/PieEntry;->b()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    sget v9, Lho;->b:F

    cmpl-float v2, v2, v9

    if-lez v2, :cond_19

    .line 773
    add-int/lit8 v2, v12, 0x1

    .line 770
    :goto_5
    add-int/lit8 v5, v5, 0x1

    move v12, v2

    goto :goto_4

    .line 777
    :cond_4
    if-nez v6, :cond_d

    .line 778
    const/4 v2, 0x0

    move v15, v2

    .line 782
    :goto_6
    const/4 v2, 0x1

    if-gt v12, v2, :cond_e

    const/4 v2, 0x0

    move v13, v2

    .line 784
    :goto_7
    aget v23, v20, v6

    .line 787
    invoke-interface {v7}, Lgg;->z()F

    move-result v2

    .line 788
    add-float v24, v4, v2

    .line 789
    move-object/from16 v0, p0

    iget-object v5, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v5}, Lcom/github/mikephil/charting/charts/PieChart;->getCircleBox()Landroid/graphics/RectF;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 790
    neg-float v5, v2

    neg-float v2, v2

    move-object/from16 v0, v22

    invoke-virtual {v0, v5, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 792
    const/4 v2, 0x0

    cmpl-float v2, v13, v2

    if-lez v2, :cond_f

    const/high16 v2, 0x43340000    # 180.0f

    cmpg-float v2, v23, v2

    if-gtz v2, :cond_f

    const/4 v2, 0x1

    move v14, v2

    .line 794
    :goto_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lgv;->h:Landroid/graphics/Paint;

    invoke-interface {v7, v6}, Lgg;->b(I)I

    move-result v5

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 796
    const/4 v2, 0x1

    if-ne v12, v2, :cond_10

    const/4 v2, 0x0

    move v5, v2

    .line 800
    :goto_9
    const/4 v2, 0x1

    if-ne v12, v2, :cond_11

    const/4 v2, 0x0

    .line 804
    :goto_a
    const/high16 v6, 0x40000000    # 2.0f

    div-float v6, v5, v6

    add-float/2addr v6, v15

    mul-float v6, v6, v18

    add-float v8, v19, v6

    .line 805
    sub-float v5, v23, v5

    mul-float v9, v5, v18

    .line 806
    const/4 v5, 0x0

    cmpg-float v5, v9, v5

    if-gez v5, :cond_5

    .line 807
    const/4 v9, 0x0

    .line 810
    :cond_5
    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v2, v5

    add-float/2addr v5, v15

    mul-float v5, v5, v18

    add-float v5, v5, v19

    .line 811
    sub-float v2, v23, v2

    mul-float v2, v2, v18

    .line 812
    const/4 v6, 0x0

    cmpg-float v6, v2, v6

    if-gez v6, :cond_6

    .line 813
    const/4 v2, 0x0

    .line 816
    :cond_6
    move-object/from16 v0, p0

    iget-object v6, v0, Lgv;->u:Landroid/graphics/Path;

    invoke-virtual {v6}, Landroid/graphics/Path;->reset()V

    .line 818
    const/high16 v6, 0x43b40000    # 360.0f

    rem-float v6, v9, v6

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-nez v6, :cond_12

    .line 820
    move-object/from16 v0, p0

    iget-object v2, v0, Lgv;->u:Landroid/graphics/Path;

    iget v5, v3, Lhk;->a:F

    iget v6, v3, Lhk;->b:F

    sget-object v7, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    move/from16 v0, v24

    invoke-virtual {v2, v5, v6, v0, v7}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 834
    :goto_b
    const/4 v2, 0x0

    .line 835
    if-eqz v14, :cond_7

    .line 836
    mul-float v5, v23, v18

    iget v2, v3, Lhk;->a:F

    const v6, 0x3c8efa35

    mul-float/2addr v6, v8

    float-to-double v6, v6

    .line 840
    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    double-to-float v6, v6

    mul-float/2addr v6, v4

    add-float/2addr v6, v2

    iget v2, v3, Lhk;->b:F

    const v7, 0x3c8efa35

    mul-float/2addr v7, v8

    float-to-double v0, v7

    move-wide/from16 v24, v0

    .line 841
    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sin(D)D

    move-result-wide v24

    move-wide/from16 v0, v24

    double-to-float v7, v0

    mul-float/2addr v7, v4

    add-float/2addr v7, v2

    move-object/from16 v2, p0

    .line 837
    invoke-virtual/range {v2 .. v9}, Lgv;->a(Lhk;FFFFFF)F

    move-result v2

    .line 847
    :cond_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lgv;->v:Landroid/graphics/RectF;

    iget v6, v3, Lhk;->a:F

    sub-float/2addr v6, v10

    iget v7, v3, Lhk;->b:F

    sub-float/2addr v7, v10

    iget v0, v3, Lhk;->a:F

    move/from16 v24, v0

    add-float v24, v24, v10

    iget v0, v3, Lhk;->b:F

    move/from16 v25, v0

    add-float v25, v25, v10

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v5, v6, v7, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 853
    if-eqz v16, :cond_15

    const/4 v5, 0x0

    cmpl-float v5, v10, v5

    if-gtz v5, :cond_8

    if-eqz v14, :cond_15

    .line 856
    :cond_8
    if-eqz v14, :cond_18

    .line 859
    const/4 v5, 0x0

    cmpg-float v5, v2, v5

    if-gez v5, :cond_9

    .line 860
    neg-float v2, v2

    .line 862
    :cond_9
    invoke-static {v10, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    move v5, v2

    .line 865
    :goto_c
    const/4 v2, 0x1

    if-eq v12, v2, :cond_a

    const/4 v2, 0x0

    cmpl-float v2, v5, v2

    if-nez v2, :cond_13

    :cond_a
    const/4 v2, 0x0

    .line 868
    :goto_d
    const/high16 v6, 0x40000000    # 2.0f

    div-float v6, v2, v6

    add-float/2addr v6, v15

    mul-float v6, v6, v18

    add-float v6, v6, v19

    .line 869
    sub-float v2, v23, v2

    mul-float v2, v2, v18

    .line 870
    const/4 v7, 0x0

    cmpg-float v7, v2, v7

    if-gez v7, :cond_b

    .line 871
    const/4 v2, 0x0

    .line 873
    :cond_b
    add-float/2addr v6, v2

    .line 875
    const/high16 v7, 0x43b40000    # 360.0f

    rem-float v7, v9, v7

    const/4 v8, 0x0

    cmpl-float v7, v7, v8

    if-nez v7, :cond_14

    .line 877
    move-object/from16 v0, p0

    iget-object v2, v0, Lgv;->u:Landroid/graphics/Path;

    iget v6, v3, Lhk;->a:F

    iget v7, v3, Lhk;->b:F

    sget-object v8, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual {v2, v6, v7, v5, v8}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 917
    :cond_c
    :goto_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lgv;->u:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 919
    move-object/from16 v0, p0

    iget-object v2, v0, Lgv;->f:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v5, v0, Lgv;->u:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v6, v0, Lgv;->h:Landroid/graphics/Paint;

    invoke-virtual {v2, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 780
    :cond_d
    add-int/lit8 v2, v6, -0x1

    aget v2, v21, v2

    mul-float v2, v2, v17

    move v15, v2

    goto/16 :goto_6

    .line 782
    :cond_e
    invoke-interface {v7}, Lgg;->a()F

    move-result v2

    move v13, v2

    goto/16 :goto_7

    .line 792
    :cond_f
    const/4 v2, 0x0

    move v14, v2

    goto/16 :goto_8

    .line 796
    :cond_10
    const v2, 0x3c8efa35

    mul-float/2addr v2, v4

    div-float v2, v13, v2

    move v5, v2

    goto/16 :goto_9

    .line 800
    :cond_11
    const v2, 0x3c8efa35

    mul-float v2, v2, v24

    div-float v2, v13, v2

    goto/16 :goto_a

    .line 823
    :cond_12
    move-object/from16 v0, p0

    iget-object v6, v0, Lgv;->u:Landroid/graphics/Path;

    iget v7, v3, Lhk;->a:F

    const v25, 0x3c8efa35

    mul-float v25, v25, v5

    move/from16 v0, v25

    float-to-double v0, v0

    move-wide/from16 v26, v0

    .line 824
    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->cos(D)D

    move-result-wide v26

    move-wide/from16 v0, v26

    double-to-float v0, v0

    move/from16 v25, v0

    mul-float v25, v25, v24

    add-float v7, v7, v25

    iget v0, v3, Lhk;->b:F

    move/from16 v25, v0

    const v26, 0x3c8efa35

    mul-float v26, v26, v5

    move/from16 v0, v26

    float-to-double v0, v0

    move-wide/from16 v26, v0

    .line 825
    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->sin(D)D

    move-result-wide v26

    move-wide/from16 v0, v26

    double-to-float v0, v0

    move/from16 v26, v0

    mul-float v24, v24, v26

    add-float v24, v24, v25

    .line 823
    move/from16 v0, v24

    invoke-virtual {v6, v7, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 827
    move-object/from16 v0, p0

    iget-object v6, v0, Lgv;->u:Landroid/graphics/Path;

    move-object/from16 v0, v22

    invoke-virtual {v6, v0, v5, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    goto/16 :goto_b

    .line 865
    :cond_13
    const v2, 0x3c8efa35

    mul-float/2addr v2, v5

    div-float v2, v13, v2

    goto/16 :goto_d

    .line 880
    :cond_14
    move-object/from16 v0, p0

    iget-object v7, v0, Lgv;->u:Landroid/graphics/Path;

    iget v8, v3, Lhk;->a:F

    const v9, 0x3c8efa35

    mul-float/2addr v9, v6

    float-to-double v12, v9

    .line 881
    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    double-to-float v9, v12

    mul-float/2addr v9, v5

    add-float/2addr v8, v9

    iget v9, v3, Lhk;->b:F

    const v12, 0x3c8efa35

    mul-float/2addr v12, v6

    float-to-double v12, v12

    .line 882
    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    double-to-float v12, v12

    mul-float/2addr v5, v12

    add-float/2addr v5, v9

    .line 880
    invoke-virtual {v7, v8, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 884
    move-object/from16 v0, p0

    iget-object v5, v0, Lgv;->u:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v7, v0, Lgv;->v:Landroid/graphics/RectF;

    neg-float v2, v2

    invoke-virtual {v5, v7, v6, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    goto/16 :goto_e

    .line 892
    :cond_15
    const/high16 v5, 0x43b40000    # 360.0f

    rem-float v5, v9, v5

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-eqz v5, :cond_c

    .line 894
    if-eqz v14, :cond_16

    .line 895
    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v9, v5

    add-float/2addr v5, v8

    .line 897
    iget v6, v3, Lhk;->a:F

    const v7, 0x3c8efa35

    mul-float/2addr v7, v5

    float-to-double v8, v7

    .line 898
    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    double-to-float v7, v8

    mul-float/2addr v7, v2

    add-float/2addr v6, v7

    .line 899
    iget v7, v3, Lhk;->b:F

    const v8, 0x3c8efa35

    mul-float/2addr v5, v8

    float-to-double v8, v5

    .line 900
    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    double-to-float v5, v8

    mul-float/2addr v2, v5

    add-float/2addr v2, v7

    .line 902
    move-object/from16 v0, p0

    iget-object v5, v0, Lgv;->u:Landroid/graphics/Path;

    invoke-virtual {v5, v6, v2}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_e

    .line 908
    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lgv;->u:Landroid/graphics/Path;

    iget v5, v3, Lhk;->a:F

    iget v6, v3, Lhk;->b:F

    invoke-virtual {v2, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_e

    .line 922
    :cond_17
    invoke-static {v3}, Lhk;->a(Lhk;)V

    .line 923
    return-void

    :cond_18
    move v5, v10

    goto/16 :goto_c

    :cond_19
    move v2, v12

    goto/16 :goto_5
.end method

.method public b()Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lgv;->b:Landroid/graphics/Paint;

    return-object v0
.end method

.method public b(Landroid/graphics/Canvas;)V
    .locals 50

    .prologue
    .line 383
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/charts/PieChart;->getCenterCircleBox()Lhk;

    move-result-object v26

    .line 386
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/charts/PieChart;->getRadius()F

    move-result v27

    .line 387
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/charts/PieChart;->getRotationAngle()F

    move-result v28

    .line 388
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/charts/PieChart;->getDrawAngles()[F

    move-result-object v29

    .line 389
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/charts/PieChart;->getAbsoluteAngles()[F

    move-result-object v30

    .line 391
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->g:Lew;

    invoke-virtual {v4}, Lew;->b()F

    move-result v31

    .line 392
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->g:Lew;

    invoke-virtual {v4}, Lew;->a()F

    move-result v32

    .line 394
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/charts/PieChart;->getHoleRadius()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    div-float v33, v4, v5

    .line 395
    const/high16 v4, 0x41200000    # 10.0f

    div-float v4, v27, v4

    const v5, 0x40666666    # 3.6f

    mul-float/2addr v4, v5

    .line 397
    move-object/from16 v0, p0

    iget-object v5, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v5}, Lcom/github/mikephil/charting/charts/PieChart;->d()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 398
    mul-float v4, v27, v33

    sub-float v4, v27, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    .line 401
    :cond_0
    sub-float v34, v27, v4

    .line 403
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/charts/PieChart;->getData()Lcom/github/mikephil/charting/data/h;

    move-result-object v4

    move-object v13, v4

    check-cast v13, Lcom/github/mikephil/charting/data/m;

    .line 404
    invoke-virtual {v13}, Lcom/github/mikephil/charting/data/m;->i()Ljava/util/List;

    move-result-object v35

    .line 406
    invoke-virtual {v13}, Lcom/github/mikephil/charting/data/m;->l()F

    move-result v36

    .line 408
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/charts/PieChart;->f()Z

    move-result v37

    .line 411
    const/4 v5, 0x0

    .line 413
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 415
    const/high16 v4, 0x40a00000    # 5.0f

    invoke-static {v4}, Lho;->a(F)F

    move-result v38

    .line 417
    const/4 v4, 0x0

    move/from16 v16, v4

    :goto_0
    invoke-interface/range {v35 .. v35}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v16

    if-ge v0, v4, :cond_16

    .line 419
    move-object/from16 v0, v35

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object v14, v4

    check-cast v14, Lgg;

    .line 421
    invoke-interface {v14}, Lgg;->o()Z

    move-result v39

    .line 423
    if-nez v39, :cond_1

    if-nez v37, :cond_1

    .line 417
    :goto_1
    add-int/lit8 v4, v16, 0x1

    move/from16 v16, v4

    goto :goto_0

    .line 426
    :cond_1
    invoke-interface {v14}, Lgg;->A()Lcom/github/mikephil/charting/data/PieDataSet$ValuePosition;

    move-result-object v40

    .line 427
    invoke-interface {v14}, Lgg;->B()Lcom/github/mikephil/charting/data/PieDataSet$ValuePosition;

    move-result-object v41

    .line 430
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lgv;->b(Lgc;)V

    .line 432
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->k:Landroid/graphics/Paint;

    const-string/jumbo v6, "Q"

    invoke-static {v4, v6}, Lho;->b(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v4

    int-to-float v4, v4

    const/high16 v6, 0x40800000    # 4.0f

    .line 433
    invoke-static {v6}, Lho;->a(F)F

    move-result v6

    add-float v42, v4, v6

    .line 435
    invoke-interface {v14}, Lgg;->g()Lff;

    move-result-object v43

    .line 437
    invoke-interface {v14}, Lgg;->s()I

    move-result v44

    .line 439
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->d:Landroid/graphics/Paint;

    invoke-interface {v14}, Lgg;->C()I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 440
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->d:Landroid/graphics/Paint;

    invoke-interface {v14}, Lgg;->D()F

    move-result v6

    invoke-static {v6}, Lho;->a(F)F

    move-result v6

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 442
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lgv;->a(Lgg;)F

    move-result v45

    .line 444
    const/4 v4, 0x0

    move/from16 v25, v4

    move/from16 v20, v5

    :goto_2
    move/from16 v0, v25

    move/from16 v1, v44

    if-ge v0, v1, :cond_17

    .line 446
    move/from16 v0, v25

    invoke-interface {v14, v0}, Lgg;->f(I)Lcom/github/mikephil/charting/data/Entry;

    move-result-object v4

    move-object v15, v4

    check-cast v15, Lcom/github/mikephil/charting/data/PieEntry;

    .line 448
    if-nez v20, :cond_8

    .line 449
    const/4 v4, 0x0

    .line 453
    :goto_3
    aget v5, v29, v20

    .line 454
    const v6, 0x3c8efa35

    mul-float v6, v6, v34

    div-float v6, v45, v6

    .line 457
    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    sub-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    .line 459
    add-float/2addr v4, v5

    .line 461
    mul-float v4, v4, v32

    add-float v10, v28, v4

    .line 463
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/charts/PieChart;->g()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-virtual {v15}, Lcom/github/mikephil/charting/data/PieEntry;->b()F

    move-result v4

    div-float v4, v4, v36

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    move/from16 v17, v4

    .line 466
    :goto_4
    const v4, 0x3c8efa35

    mul-float/2addr v4, v10

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v46, v0

    .line 467
    const v4, 0x3c8efa35

    mul-float/2addr v4, v10

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v47, v0

    .line 469
    if-eqz v37, :cond_a

    sget-object v4, Lcom/github/mikephil/charting/data/PieDataSet$ValuePosition;->b:Lcom/github/mikephil/charting/data/PieDataSet$ValuePosition;

    move-object/from16 v0, v40

    if-ne v0, v4, :cond_a

    const/4 v4, 0x1

    move/from16 v24, v4

    .line 471
    :goto_5
    if-eqz v39, :cond_b

    sget-object v4, Lcom/github/mikephil/charting/data/PieDataSet$ValuePosition;->b:Lcom/github/mikephil/charting/data/PieDataSet$ValuePosition;

    move-object/from16 v0, v41

    if-ne v0, v4, :cond_b

    const/4 v4, 0x1

    move/from16 v23, v4

    .line 473
    :goto_6
    if-eqz v37, :cond_c

    sget-object v4, Lcom/github/mikephil/charting/data/PieDataSet$ValuePosition;->a:Lcom/github/mikephil/charting/data/PieDataSet$ValuePosition;

    move-object/from16 v0, v40

    if-ne v0, v4, :cond_c

    const/4 v4, 0x1

    move/from16 v22, v4

    .line 475
    :goto_7
    if-eqz v39, :cond_d

    sget-object v4, Lcom/github/mikephil/charting/data/PieDataSet$ValuePosition;->a:Lcom/github/mikephil/charting/data/PieDataSet$ValuePosition;

    move-object/from16 v0, v41

    if-ne v0, v4, :cond_d

    const/4 v4, 0x1

    move/from16 v21, v4

    .line 478
    :goto_8
    if-nez v24, :cond_2

    if-eqz v23, :cond_5

    .line 480
    :cond_2
    invoke-interface {v14}, Lgg;->F()F

    move-result v8

    .line 481
    invoke-interface {v14}, Lgg;->G()F

    move-result v5

    .line 482
    invoke-interface {v14}, Lgg;->E()F

    move-result v4

    const/high16 v6, 0x42c80000    # 100.0f

    div-float/2addr v4, v6

    .line 489
    move-object/from16 v0, p0

    iget-object v6, v0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v6}, Lcom/github/mikephil/charting/charts/PieChart;->d()Z

    move-result v6

    if-eqz v6, :cond_e

    .line 490
    mul-float v6, v27, v33

    sub-float v6, v27, v6

    mul-float/2addr v4, v6

    mul-float v6, v27, v33

    add-float/2addr v4, v6

    .line 496
    :goto_9
    invoke-interface {v14}, Lgg;->H()Z

    move-result v6

    if-eqz v6, :cond_f

    mul-float v5, v5, v34

    const v6, 0x3c8efa35

    mul-float/2addr v6, v10

    float-to-double v6, v6

    .line 497
    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    double-to-float v6, v6

    mul-float/2addr v5, v6

    move v9, v5

    .line 501
    :goto_a
    mul-float v5, v4, v46

    move-object/from16 v0, v26

    iget v6, v0, Lhk;->a:F

    add-float/2addr v5, v6

    .line 502
    mul-float v4, v4, v47

    move-object/from16 v0, v26

    iget v6, v0, Lhk;->b:F

    add-float/2addr v6, v4

    .line 504
    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v4, v8

    mul-float v4, v4, v34

    mul-float v4, v4, v46

    move-object/from16 v0, v26

    iget v7, v0, Lhk;->a:F

    add-float/2addr v7, v4

    .line 505
    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v4, v8

    mul-float v4, v4, v34

    mul-float v4, v4, v47

    move-object/from16 v0, v26

    iget v8, v0, Lhk;->b:F

    add-float/2addr v8, v4

    .line 507
    float-to-double v0, v10

    move-wide/from16 v18, v0

    const-wide v48, 0x4076800000000000L    # 360.0

    rem-double v18, v18, v48

    const-wide v48, 0x4056800000000000L    # 90.0

    cmpl-double v4, v18, v48

    if-ltz v4, :cond_10

    float-to-double v10, v10

    const-wide v18, 0x4076800000000000L    # 360.0

    rem-double v10, v10, v18

    const-wide v18, 0x4070e00000000000L    # 270.0

    cmpg-double v4, v10, v18

    if-gtz v4, :cond_10

    .line 508
    sub-float v9, v7, v9

    .line 511
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->k:Landroid/graphics/Paint;

    sget-object v10, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v10}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 513
    if-eqz v24, :cond_3

    .line 514
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->p:Landroid/graphics/Paint;

    sget-object v10, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v10}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 516
    :cond_3
    sub-float v4, v9, v38

    move/from16 v18, v8

    move/from16 v19, v4

    move v10, v8

    move v12, v9

    .line 530
    :goto_b
    invoke-interface {v14}, Lgg;->C()I

    move-result v4

    const v9, 0x112233

    if-eq v4, v9, :cond_4

    .line 531
    move-object/from16 v0, p0

    iget-object v9, v0, Lgv;->d:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 532
    move-object/from16 v0, p0

    iget-object v11, v0, Lgv;->d:Landroid/graphics/Paint;

    move-object/from16 v6, p1

    move v9, v12

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 536
    :cond_4
    if-eqz v24, :cond_12

    if-eqz v23, :cond_12

    .line 538
    const/4 v9, 0x0

    .line 545
    move/from16 v0, v25

    invoke-interface {v14, v0}, Lgg;->e(I)I

    move-result v12

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, v43

    move/from16 v7, v17

    move-object v8, v15

    move/from16 v10, v19

    move/from16 v11, v18

    .line 538
    invoke-virtual/range {v4 .. v12}, Lgv;->a(Landroid/graphics/Canvas;Lff;FLcom/github/mikephil/charting/data/Entry;IFFI)V

    .line 547
    invoke-virtual {v13}, Lcom/github/mikephil/charting/data/m;->j()I

    move-result v4

    move/from16 v0, v25

    if-ge v0, v4, :cond_5

    invoke-virtual {v15}, Lcom/github/mikephil/charting/data/PieEntry;->a()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 548
    invoke-virtual {v15}, Lcom/github/mikephil/charting/data/PieEntry;->a()Ljava/lang/String;

    move-result-object v4

    add-float v5, v18, v42

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v19

    invoke-virtual {v0, v1, v4, v2, v5}, Lgv;->a(Landroid/graphics/Canvas;Ljava/lang/String;FF)V

    .line 562
    :cond_5
    :goto_c
    if-nez v22, :cond_6

    if-eqz v21, :cond_7

    .line 564
    :cond_6
    mul-float v4, v34, v46

    move-object/from16 v0, v26

    iget v5, v0, Lhk;->a:F

    add-float v10, v4, v5

    .line 565
    mul-float v4, v34, v47

    move-object/from16 v0, v26

    iget v5, v0, Lhk;->b:F

    add-float v11, v4, v5

    .line 567
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->k:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 570
    if-eqz v22, :cond_14

    if-eqz v21, :cond_14

    .line 572
    const/4 v9, 0x0

    move/from16 v0, v25

    invoke-interface {v14, v0}, Lgg;->e(I)I

    move-result v12

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, v43

    move/from16 v7, v17

    move-object v8, v15

    invoke-virtual/range {v4 .. v12}, Lgv;->a(Landroid/graphics/Canvas;Lff;FLcom/github/mikephil/charting/data/Entry;IFFI)V

    .line 574
    invoke-virtual {v13}, Lcom/github/mikephil/charting/data/m;->j()I

    move-result v4

    move/from16 v0, v25

    if-ge v0, v4, :cond_7

    invoke-virtual {v15}, Lcom/github/mikephil/charting/data/PieEntry;->a()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 575
    invoke-virtual {v15}, Lcom/github/mikephil/charting/data/PieEntry;->a()Ljava/lang/String;

    move-result-object v4

    add-float v5, v11, v42

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v4, v10, v5}, Lgv;->a(Landroid/graphics/Canvas;Ljava/lang/String;FF)V

    .line 588
    :cond_7
    :goto_d
    add-int/lit8 v20, v20, 0x1

    .line 444
    add-int/lit8 v4, v25, 0x1

    move/from16 v25, v4

    goto/16 :goto_2

    .line 451
    :cond_8
    add-int/lit8 v4, v20, -0x1

    aget v4, v30, v4

    mul-float v4, v4, v31

    goto/16 :goto_3

    .line 464
    :cond_9
    invoke-virtual {v15}, Lcom/github/mikephil/charting/data/PieEntry;->b()F

    move-result v4

    move/from16 v17, v4

    goto/16 :goto_4

    .line 469
    :cond_a
    const/4 v4, 0x0

    move/from16 v24, v4

    goto/16 :goto_5

    .line 471
    :cond_b
    const/4 v4, 0x0

    move/from16 v23, v4

    goto/16 :goto_6

    .line 473
    :cond_c
    const/4 v4, 0x0

    move/from16 v22, v4

    goto/16 :goto_7

    .line 475
    :cond_d
    const/4 v4, 0x0

    move/from16 v21, v4

    goto/16 :goto_8

    .line 494
    :cond_e
    mul-float v4, v4, v27

    goto/16 :goto_9

    .line 497
    :cond_f
    mul-float v5, v5, v34

    move v9, v5

    goto/16 :goto_a

    .line 519
    :cond_10
    add-float/2addr v9, v7

    .line 521
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->k:Landroid/graphics/Paint;

    sget-object v10, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v10}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 523
    if-eqz v24, :cond_11

    .line 524
    move-object/from16 v0, p0

    iget-object v4, v0, Lgv;->p:Landroid/graphics/Paint;

    sget-object v10, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v10}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 526
    :cond_11
    add-float v4, v9, v38

    move/from16 v18, v8

    move/from16 v19, v4

    move v10, v8

    move v12, v9

    .line 527
    goto/16 :goto_b

    .line 551
    :cond_12
    if-eqz v24, :cond_13

    .line 552
    invoke-virtual {v13}, Lcom/github/mikephil/charting/data/m;->j()I

    move-result v4

    move/from16 v0, v25

    if-ge v0, v4, :cond_5

    invoke-virtual {v15}, Lcom/github/mikephil/charting/data/PieEntry;->a()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 553
    invoke-virtual {v15}, Lcom/github/mikephil/charting/data/PieEntry;->a()Ljava/lang/String;

    move-result-object v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v42, v5

    add-float v5, v5, v18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v19

    invoke-virtual {v0, v1, v4, v2, v5}, Lgv;->a(Landroid/graphics/Canvas;Ljava/lang/String;FF)V

    goto/16 :goto_c

    .line 555
    :cond_13
    if-eqz v23, :cond_5

    .line 557
    const/4 v9, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v42, v4

    add-float v11, v18, v4

    .line 558
    move/from16 v0, v25

    invoke-interface {v14, v0}, Lgg;->e(I)I

    move-result v12

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, v43

    move/from16 v7, v17

    move-object v8, v15

    move/from16 v10, v19

    .line 557
    invoke-virtual/range {v4 .. v12}, Lgv;->a(Landroid/graphics/Canvas;Lff;FLcom/github/mikephil/charting/data/Entry;IFFI)V

    goto/16 :goto_c

    .line 578
    :cond_14
    if-eqz v22, :cond_15

    .line 579
    invoke-virtual {v13}, Lcom/github/mikephil/charting/data/m;->j()I

    move-result v4

    move/from16 v0, v25

    if-ge v0, v4, :cond_7

    invoke-virtual {v15}, Lcom/github/mikephil/charting/data/PieEntry;->a()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 580
    invoke-virtual {v15}, Lcom/github/mikephil/charting/data/PieEntry;->a()Ljava/lang/String;

    move-result-object v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v42, v5

    add-float/2addr v5, v11

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v4, v10, v5}, Lgv;->a(Landroid/graphics/Canvas;Ljava/lang/String;FF)V

    goto/16 :goto_d

    .line 582
    :cond_15
    if-eqz v21, :cond_7

    .line 584
    const/4 v9, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v42, v4

    add-float/2addr v11, v4

    move/from16 v0, v25

    invoke-interface {v14, v0}, Lgg;->e(I)I

    move-result v12

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, v43

    move/from16 v7, v17

    move-object v8, v15

    invoke-virtual/range {v4 .. v12}, Lgv;->a(Landroid/graphics/Canvas;Lff;FLcom/github/mikephil/charting/data/Entry;IFFI)V

    goto/16 :goto_d

    .line 591
    :cond_16
    invoke-static/range {v26 .. v26}, Lhk;->a(Lhk;)V

    .line 592
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 593
    return-void

    :cond_17
    move/from16 v5, v20

    goto/16 :goto_1
.end method

.method public c()Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lgv;->c:Landroid/graphics/Paint;

    return-object v0
.end method

.method public c(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 610
    invoke-virtual {p0, p1}, Lgv;->d(Landroid/graphics/Canvas;)V

    .line 611
    iget-object v0, p0, Lgv;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 612
    invoke-virtual {p0, p1}, Lgv;->e(Landroid/graphics/Canvas;)V

    .line 613
    return-void
.end method

.method public d()Landroid/text/TextPaint;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lgv;->n:Landroid/text/TextPaint;

    return-object v0
.end method

.method protected d(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/high16 v7, 0x42c80000    # 100.0f

    .line 623
    iget-object v0, p0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/PieChart;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgv;->f:Landroid/graphics/Canvas;

    if-eqz v0, :cond_2

    .line 625
    iget-object v0, p0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/PieChart;->getRadius()F

    move-result v0

    .line 626
    iget-object v1, p0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/charts/PieChart;->getHoleRadius()F

    move-result v1

    div-float/2addr v1, v7

    mul-float/2addr v1, v0

    .line 627
    iget-object v2, p0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/charts/PieChart;->getCenterCircleBox()Lhk;

    move-result-object v2

    .line 629
    iget-object v3, p0, Lgv;->b:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getColor()I

    move-result v3

    invoke-static {v3}, Landroid/graphics/Color;->alpha(I)I

    move-result v3

    if-lez v3, :cond_0

    .line 631
    iget-object v3, p0, Lgv;->f:Landroid/graphics/Canvas;

    iget v4, v2, Lhk;->a:F

    iget v5, v2, Lhk;->b:F

    iget-object v6, p0, Lgv;->b:Landroid/graphics/Paint;

    invoke-virtual {v3, v4, v5, v1, v6}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 637
    :cond_0
    iget-object v3, p0, Lgv;->c:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getColor()I

    move-result v3

    invoke-static {v3}, Landroid/graphics/Color;->alpha(I)I

    move-result v3

    if-lez v3, :cond_1

    iget-object v3, p0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    .line 638
    invoke-virtual {v3}, Lcom/github/mikephil/charting/charts/PieChart;->getTransparentCircleRadius()F

    move-result v3

    iget-object v4, p0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/charts/PieChart;->getHoleRadius()F

    move-result v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    .line 640
    iget-object v3, p0, Lgv;->c:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getAlpha()I

    move-result v3

    .line 641
    iget-object v4, p0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/charts/PieChart;->getTransparentCircleRadius()F

    move-result v4

    div-float/2addr v4, v7

    mul-float/2addr v0, v4

    .line 643
    iget-object v4, p0, Lgv;->c:Landroid/graphics/Paint;

    int-to-float v5, v3

    iget-object v6, p0, Lgv;->g:Lew;

    invoke-virtual {v6}, Lew;->b()F

    move-result v6

    mul-float/2addr v5, v6

    iget-object v6, p0, Lgv;->g:Lew;

    invoke-virtual {v6}, Lew;->a()F

    move-result v6

    mul-float/2addr v5, v6

    float-to-int v5, v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 646
    iget-object v4, p0, Lgv;->w:Landroid/graphics/Path;

    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    .line 647
    iget-object v4, p0, Lgv;->w:Landroid/graphics/Path;

    iget v5, v2, Lhk;->a:F

    iget v6, v2, Lhk;->b:F

    sget-object v7, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v4, v5, v6, v0, v7}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 648
    iget-object v0, p0, Lgv;->w:Landroid/graphics/Path;

    iget v4, v2, Lhk;->a:F

    iget v5, v2, Lhk;->b:F

    sget-object v6, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v4, v5, v1, v6}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 649
    iget-object v0, p0, Lgv;->f:Landroid/graphics/Canvas;

    iget-object v1, p0, Lgv;->w:Landroid/graphics/Path;

    iget-object v4, p0, Lgv;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 652
    iget-object v0, p0, Lgv;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 654
    :cond_1
    invoke-static {v2}, Lhk;->a(Lhk;)V

    .line 656
    :cond_2
    return-void
.end method

.method public e()Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lgv;->p:Landroid/graphics/Paint;

    return-object v0
.end method

.method protected e(Landroid/graphics/Canvas;)V
    .locals 14

    .prologue
    .line 665
    iget-object v0, p0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/PieChart;->getCenterText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 667
    iget-object v0, p0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/PieChart;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    if-eqz v1, :cond_4

    .line 669
    iget-object v0, p0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/PieChart;->getCenterCircleBox()Lhk;

    move-result-object v10

    .line 670
    iget-object v0, p0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/PieChart;->getCenterTextOffset()Lhk;

    move-result-object v11

    .line 672
    iget v0, v10, Lhk;->a:F

    iget v2, v11, Lhk;->a:F

    add-float/2addr v2, v0

    .line 673
    iget v0, v10, Lhk;->b:F

    iget v3, v11, Lhk;->b:F

    add-float/2addr v3, v0

    .line 675
    iget-object v0, p0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/PieChart;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/PieChart;->c()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    .line 676
    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/PieChart;->getRadius()F

    move-result v0

    iget-object v4, p0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v4}, Lcom/github/mikephil/charting/charts/PieChart;->getHoleRadius()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    div-float/2addr v4, v5

    mul-float/2addr v0, v4

    .line 679
    :goto_0
    iget-object v4, p0, Lgv;->t:[Landroid/graphics/RectF;

    const/4 v5, 0x0

    aget-object v12, v4, v5

    .line 680
    sub-float v4, v2, v0

    iput v4, v12, Landroid/graphics/RectF;->left:F

    .line 681
    sub-float v4, v3, v0

    iput v4, v12, Landroid/graphics/RectF;->top:F

    .line 682
    add-float/2addr v2, v0

    iput v2, v12, Landroid/graphics/RectF;->right:F

    .line 683
    add-float/2addr v0, v3

    iput v0, v12, Landroid/graphics/RectF;->bottom:F

    .line 684
    iget-object v0, p0, Lgv;->t:[Landroid/graphics/RectF;

    const/4 v2, 0x1

    aget-object v13, v0, v2

    .line 685
    invoke-virtual {v13, v12}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 687
    iget-object v0, p0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/PieChart;->getCenterTextRadiusPercent()F

    move-result v0

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v0, v2

    .line 688
    float-to-double v2, v0

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    .line 690
    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {v13}, Landroid/graphics/RectF;->width()F

    move-result v3

    mul-float/2addr v3, v0

    sub-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    .line 691
    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v4

    mul-float/2addr v0, v4

    sub-float v0, v3, v0

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v0, v3

    .line 689
    invoke-virtual {v13, v2, v0}, Landroid/graphics/RectF;->inset(FF)V

    .line 695
    :cond_0
    iget-object v0, p0, Lgv;->r:Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgv;->s:Landroid/graphics/RectF;

    invoke-virtual {v13, v0}, Landroid/graphics/RectF;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 698
    :cond_1
    iget-object v0, p0, Lgv;->s:Landroid/graphics/RectF;

    invoke-virtual {v0, v13}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 699
    iput-object v1, p0, Lgv;->r:Ljava/lang/CharSequence;

    .line 701
    iget-object v0, p0, Lgv;->s:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v5

    .line 704
    new-instance v0, Landroid/text/StaticLayout;

    const/4 v2, 0x0

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    iget-object v4, p0, Lgv;->n:Landroid/text/TextPaint;

    float-to-double v6, v5

    .line 706
    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    double-to-int v5, v6

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lgv;->q:Landroid/text/StaticLayout;

    .line 711
    :cond_2
    iget-object v0, p0, Lgv;->q:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    int-to-float v0, v0

    .line 713
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 714
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_3

    .line 715
    iget-object v1, p0, Lgv;->l:Landroid/graphics/Path;

    .line 716
    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    .line 717
    sget-object v2, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v1, v12, v2}, Landroid/graphics/Path;->addOval(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 718
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 721
    :cond_3
    iget v1, v13, Landroid/graphics/RectF;->left:F

    iget v2, v13, Landroid/graphics/RectF;->top:F

    invoke-virtual {v13}, Landroid/graphics/RectF;->height()F

    move-result v3

    sub-float v0, v3, v0

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v0, v3

    add-float/2addr v0, v2

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 722
    iget-object v0, p0, Lgv;->q:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 724
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 726
    invoke-static {v10}, Lhk;->a(Lhk;)V

    .line 727
    invoke-static {v11}, Lhk;->a(Lhk;)V

    .line 729
    :cond_4
    return-void

    .line 676
    :cond_5
    iget-object v0, p0, Lgv;->a:Lcom/github/mikephil/charting/charts/PieChart;

    .line 677
    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/PieChart;->getRadius()F

    move-result v0

    goto/16 :goto_0
.end method

.method public f()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 981
    iget-object v0, p0, Lgv;->f:Landroid/graphics/Canvas;

    if-eqz v0, :cond_0

    .line 982
    iget-object v0, p0, Lgv;->f:Landroid/graphics/Canvas;

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 983
    iput-object v1, p0, Lgv;->f:Landroid/graphics/Canvas;

    .line 985
    :cond_0
    iget-object v0, p0, Lgv;->e:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    .line 986
    iget-object v0, p0, Lgv;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 987
    iget-object v0, p0, Lgv;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    .line 988
    iput-object v1, p0, Lgv;->e:Ljava/lang/ref/WeakReference;

    .line 990
    :cond_1
    return-void
.end method
