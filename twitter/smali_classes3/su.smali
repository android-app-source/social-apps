.class public Lsu;
.super Laum;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laum",
        "<",
        "Ljava/lang/Void;",
        "Lcbi",
        "<",
        "Lcdi;",
        ">;",
        "Lbcs;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/app/Application;

.field private final b:Lcom/twitter/library/client/Session;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/twitter/library/client/Session;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Laum;-><init>()V

    .line 29
    iput-object p1, p0, Lsu;->a:Landroid/app/Application;

    .line 30
    iput-object p2, p0, Lsu;->b:Lcom/twitter/library/client/Session;

    .line 31
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/Void;)Lbcs;
    .locals 3

    .prologue
    .line 48
    new-instance v0, Lbcs;

    iget-object v1, p0, Lsu;->a:Landroid/app/Application;

    iget-object v2, p0, Lsu;->b:Lcom/twitter/library/client/Session;

    invoke-direct {v0, v1, v2}, Lbcs;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    return-object v0
.end method

.method protected a(Lbcs;)Lcbi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbcs;",
            ")",
            "Lcbi",
            "<",
            "Lcdi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    invoke-virtual {p1}, Lbcs;->g()Lcdj;

    move-result-object v0

    .line 37
    new-instance v1, Lcbl;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {v1, v0}, Lcbl;-><init>(Ljava/lang/Iterable;)V

    return-object v1

    :cond_0
    iget-object v0, v0, Lcdj;->b:Ljava/util/List;

    goto :goto_0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Lcom/twitter/library/service/s;
    .locals 1

    .prologue
    .line 20
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lsu;->a(Ljava/lang/Void;)Lbcs;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Lcom/twitter/library/service/s;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    check-cast p1, Lbcs;

    invoke-virtual {p0, p1}, Lsu;->a(Lbcs;)Lcbi;

    move-result-object v0

    return-object v0
.end method

.method protected a()Z
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x1

    return v0
.end method
