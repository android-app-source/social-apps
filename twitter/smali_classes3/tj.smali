.class public Ltj;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltj$a;
    }
.end annotation


# direct methods
.method public static a(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;)Lti;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            ")",
            "Lti",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 22
    new-instance v0, Lcom/twitter/library/scribe/a;

    invoke-direct {v0}, Lcom/twitter/library/scribe/a;-><init>()V

    invoke-static {p0, p1, v0}, Ltj;->a(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/scribe/n;)Lti;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/scribe/n;)Lti;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;",
            "Lcom/twitter/library/scribe/n;",
            ")",
            "Lti",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    invoke-static {}, Lth;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    new-instance v1, Ltj$a;

    invoke-direct {v1, p0, p1, p2}, Ltj$a;-><init>(Landroid/content/Context;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lcom/twitter/library/scribe/n;)V

    .line 32
    new-instance v0, Ltk;

    .line 33
    invoke-static {}, Lth;->b()J

    move-result-wide v2

    .line 34
    invoke-static {}, Lth;->c()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Ltk;-><init>(Lti$a;JJ)V

    .line 36
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ltm;->b()Lti;

    move-result-object v0

    goto :goto_0
.end method
