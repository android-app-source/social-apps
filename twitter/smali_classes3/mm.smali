.class public Lmm;
.super Lmp;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmm$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lmp",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final b:I

.field private final c:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/autocomplete/a;)V
    .locals 2

    .prologue
    .line 34
    const v0, 0x7f0400c7

    invoke-direct {p0, p1, p2, v0}, Lmp;-><init>(Landroid/content/Context;Lcom/twitter/android/autocomplete/a;I)V

    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e02fc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lmm;->b:I

    .line 36
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lmm;->c:J

    .line 37
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation

    .prologue
    .line 41
    instance-of v0, p1, Lcom/twitter/model/core/TwitterUser;

    if-eqz v0, :cond_0

    .line 42
    const/4 v0, 0x0

    .line 46
    :goto_0
    return v0

    .line 43
    :cond_0
    instance-of v0, p1, Lcom/twitter/model/dms/q;

    if-eqz v0, :cond_1

    .line 44
    const/4 v0, 0x1

    goto :goto_0

    .line 46
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TT;",
            "Landroid/view/ViewGroup;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 58
    invoke-virtual {p0, p2}, Lmm;->a(Ljava/lang/Object;)I

    move-result v0

    .line 59
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 60
    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 61
    const v0, 0x7f0400d0

    invoke-virtual {v1, v0, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 62
    const v0, 0x7f13011b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 63
    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    .line 82
    :goto_0
    return-object v0

    .line 65
    :cond_0
    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 66
    const v0, 0x7f0400c6

    invoke-virtual {v1, v0, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 68
    new-instance v1, Lmm$a;

    invoke-direct {v1, v0}, Lmm$a;-><init>(Landroid/view/View;)V

    .line 69
    iget-object v2, v1, Lmm$a;->a:Lcom/twitter/app/dm/widget/DMAvatar;

    iget v3, p0, Lmm;->b:I

    invoke-virtual {v2, v3}, Lcom/twitter/app/dm/widget/DMAvatar;->setSize(I)V

    .line 70
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 72
    :cond_1
    if-nez v0, :cond_3

    .line 73
    invoke-super {p0, p1, p2, p3}, Lmp;->a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 74
    if-eqz v1, :cond_2

    .line 75
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lms;

    .line 76
    invoke-virtual {v0}, Lms;->a()Lcom/twitter/media/ui/image/UserImageView;

    move-result-object v0

    .line 77
    sget-object v2, Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;->c:Lcom/twitter/media/ui/image/config/CommonRoundingStrategy;

    invoke-virtual {v0, v2}, Lcom/twitter/media/ui/image/UserImageView;->setRoundingStrategy(Lcom/twitter/media/ui/image/config/g;)V

    .line 78
    iget v2, p0, Lmm;->b:I

    iget v3, p0, Lmm;->b:I

    invoke-virtual {v0, v2, v3}, Lcom/twitter/media/ui/image/UserImageView;->a(II)V

    :cond_2
    move-object v0, v1

    .line 80
    goto :goto_0

    .line 82
    :cond_3
    invoke-super {p0, p1, p2, p3}, Lmp;->a(Landroid/content/Context;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/content/Context;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 88
    invoke-virtual {p0, p3}, Lmm;->a(Ljava/lang/Object;)I

    move-result v0

    .line 89
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 90
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 91
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    :goto_0
    return-void

    .line 92
    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 93
    check-cast p3, Lcom/twitter/model/dms/q;

    .line 94
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmm$a;

    .line 95
    iget-object v1, v0, Lmm$a;->a:Lcom/twitter/app/dm/widget/DMAvatar;

    invoke-virtual {v1, p3}, Lcom/twitter/app/dm/widget/DMAvatar;->setConversation(Lcom/twitter/model/dms/q;)V

    .line 96
    iget-object v0, v0, Lmm$a;->b:Landroid/widget/TextView;

    new-instance v1, Lcom/twitter/library/dm/b;

    invoke-virtual {p0}, Lmm;->j()Landroid/content/Context;

    move-result-object v2

    iget-wide v4, p0, Lmm;->c:J

    invoke-direct {v1, p3, v2, v4, v5}, Lcom/twitter/library/dm/b;-><init>(Lcom/twitter/model/dms/q;Landroid/content/Context;J)V

    invoke-virtual {v1}, Lcom/twitter/library/dm/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 98
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lmp;->a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x3

    return v0
.end method
