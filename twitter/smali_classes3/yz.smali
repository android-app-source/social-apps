.class public final Lyz;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lzd;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lyz$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private A:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxn;",
            ">;"
        }
    .end annotation
.end field

.field private B:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private C:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;>;"
        }
    .end annotation
.end field

.field private D:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;"
        }
    .end annotation
.end field

.field private E:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/abs/j;",
            ">;"
        }
    .end annotation
.end field

.field private F:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lanr;",
            ">;"
        }
    .end annotation
.end field

.field private G:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lzj;",
            ">;"
        }
    .end annotation
.end field

.field private H:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/api/moments/maker/r;",
            ">;"
        }
    .end annotation
.end field

.field private I:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lyb;",
            ">;"
        }
    .end annotation
.end field

.field private J:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lya;",
            ">;"
        }
    .end annotation
.end field

.field private K:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/support/v4/content/LocalBroadcastManager;",
            ">;"
        }
    .end annotation
.end field

.field private L:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcrc;",
            ">;"
        }
    .end annotation
.end field

.field private M:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcif;",
            ">;"
        }
    .end annotation
.end field

.field private N:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxp;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lyf;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/database/schema/TwitterSchema;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lwo;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcnz;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/moments/data/c",
            "<",
            "Lapb;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxv;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/provider/t;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lbsb;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/util/object/d",
            "<",
            "Ljava/lang/Long;",
            "Lbdk;",
            ">;>;"
        }
    .end annotation
.end field

.field private n:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lwq$a;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lwq;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxr;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lawb;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/Long;",
            "Lcfe;",
            ">;>;"
        }
    .end annotation
.end field

.field private s:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;>;"
        }
    .end annotation
.end field

.field private t:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxx;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lyd;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxt;",
            ">;"
        }
    .end annotation
.end field

.field private w:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laun",
            "<",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private x:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lyh;",
            ">;"
        }
    .end annotation
.end field

.field private y:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lyg;",
            ">;"
        }
    .end annotation
.end field

.field private z:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    const-class v0, Lyz;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lyz;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lyz$a;)V
    .locals 1

    .prologue
    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    sget-boolean v0, Lyz;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 175
    :cond_0
    invoke-direct {p0, p1}, Lyz;->a(Lyz$a;)V

    .line 176
    return-void
.end method

.method synthetic constructor <init>(Lyz$a;Lyz$1;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lyz;-><init>(Lyz$a;)V

    return-void
.end method

.method public static a()Lyz$a;
    .locals 2

    .prologue
    .line 179
    new-instance v0, Lyz$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lyz$a;-><init>(Lyz$1;)V

    return-object v0
.end method

.method private a(Lyz$a;)V
    .locals 5

    .prologue
    .line 185
    new-instance v0, Lyz$1;

    invoke-direct {v0, p0, p1}, Lyz$1;-><init>(Lyz;Lyz$a;)V

    iput-object v0, p0, Lyz;->b:Lcta;

    .line 198
    new-instance v0, Lyz$2;

    invoke-direct {v0, p0, p1}, Lyz$2;-><init>(Lyz;Lyz$a;)V

    iput-object v0, p0, Lyz;->c:Lcta;

    .line 211
    iget-object v0, p0, Lyz;->b:Lcta;

    iget-object v1, p0, Lyz;->c:Lcta;

    .line 213
    invoke-static {v0, v1}, Lyp;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 212
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->d:Lcta;

    .line 216
    iget-object v0, p0, Lyz;->d:Lcta;

    .line 217
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->e:Lcta;

    .line 219
    new-instance v0, Lyz$3;

    invoke-direct {v0, p0, p1}, Lyz$3;-><init>(Lyz;Lyz$a;)V

    iput-object v0, p0, Lyz;->f:Lcta;

    .line 232
    iget-object v0, p0, Lyz;->f:Lcta;

    .line 234
    invoke-static {v0}, Lyl;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 233
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->g:Lcta;

    .line 237
    new-instance v0, Lyz$4;

    invoke-direct {v0, p0, p1}, Lyz$4;-><init>(Lyz;Lyz$a;)V

    iput-object v0, p0, Lyz;->h:Lcta;

    .line 250
    iget-object v0, p0, Lyz;->b:Lcta;

    .line 252
    invoke-static {v0}, Lyn;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 251
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->i:Lcta;

    .line 255
    iget-object v0, p0, Lyz;->h:Lcta;

    iget-object v1, p0, Lyz;->i:Lcta;

    .line 257
    invoke-static {v0, v1}, Lxw;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 256
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->j:Lcta;

    .line 260
    new-instance v0, Lyz$5;

    invoke-direct {v0, p0, p1}, Lyz$5;-><init>(Lyz;Lyz$a;)V

    iput-object v0, p0, Lyz;->k:Lcta;

    .line 273
    iget-object v0, p0, Lyz;->b:Lcta;

    iget-object v1, p0, Lyz;->k:Lcta;

    iget-object v2, p0, Lyz;->h:Lcta;

    .line 275
    invoke-static {v0, v1, v2}, Lbsc;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 274
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->l:Lcta;

    .line 280
    iget-object v0, p0, Lyz;->b:Lcta;

    iget-object v1, p0, Lyz;->c:Lcta;

    iget-object v2, p0, Lyz;->l:Lcta;

    .line 282
    invoke-static {v0, v1, v2}, Lym;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 281
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->m:Lcta;

    .line 288
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lyz;->m:Lcta;

    .line 287
    invoke-static {v0, v1}, Lws;->a(Lcsd;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 286
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->n:Lcta;

    .line 291
    iget-object v0, p0, Lyz;->n:Lcta;

    iget-object v1, p0, Lyz;->j:Lcta;

    .line 293
    invoke-static {v0, v1}, Lwr;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 292
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->o:Lcta;

    .line 296
    iget-object v0, p0, Lyz;->g:Lcta;

    iget-object v1, p0, Lyz;->j:Lcta;

    iget-object v2, p0, Lyz;->o:Lcta;

    .line 298
    invoke-static {v0, v1, v2}, Lxs;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 297
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->p:Lcta;

    .line 303
    new-instance v0, Lyz$6;

    invoke-direct {v0, p0, p1}, Lyz$6;-><init>(Lyz;Lyz$a;)V

    iput-object v0, p0, Lyz;->q:Lcta;

    .line 316
    iget-object v0, p0, Lyz;->q:Lcta;

    .line 318
    invoke-static {v0}, Lyv;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 317
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->r:Lcta;

    .line 321
    iget-object v0, p0, Lyz;->q:Lcta;

    .line 323
    invoke-static {v0}, Lyo;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 322
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->s:Lcta;

    .line 326
    iget-object v0, p0, Lyz;->s:Lcta;

    .line 328
    invoke-static {v0}, Lxy;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 327
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->t:Lcta;

    .line 331
    iget-object v0, p0, Lyz;->d:Lcta;

    .line 333
    invoke-static {v0}, Lye;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 332
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->u:Lcta;

    .line 335
    iget-object v0, p0, Lyz;->t:Lcta;

    iget-object v1, p0, Lyz;->u:Lcta;

    .line 337
    invoke-static {v0, v1}, Lxu;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 336
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->v:Lcta;

    .line 341
    iget-object v0, p0, Lyz;->v:Lcta;

    .line 342
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->w:Lcta;

    .line 344
    iget-object v0, p0, Lyz;->w:Lcta;

    .line 348
    invoke-static {}, Lcom/twitter/android/moments/data/u;->c()Ldagger/internal/c;

    move-result-object v1

    iget-object v2, p0, Lyz;->d:Lcta;

    .line 346
    invoke-static {v0, v1, v2}, Lyi;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 345
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->x:Lcta;

    .line 351
    iget-object v0, p0, Lyz;->x:Lcta;

    .line 352
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->y:Lcta;

    .line 354
    iget-object v0, p0, Lyz;->y:Lcta;

    .line 356
    invoke-static {v0}, Lxl;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 355
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->z:Lcta;

    .line 358
    iget-object v0, p0, Lyz;->p:Lcta;

    iget-object v1, p0, Lyz;->r:Lcta;

    iget-object v2, p0, Lyz;->z:Lcta;

    .line 360
    invoke-static {v0, v1, v2}, Lxo;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 359
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->A:Lcta;

    .line 365
    iget-object v0, p0, Lyz;->A:Lcta;

    .line 366
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->B:Lcta;

    .line 368
    const/4 v0, 0x2

    const/4 v1, 0x0

    .line 369
    invoke-static {v0, v1}, Ldagger/internal/f;->a(II)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lyz;->e:Lcta;

    .line 370
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lyz;->B:Lcta;

    .line 371
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    .line 372
    invoke-virtual {v0}, Ldagger/internal/f$a;->a()Ldagger/internal/f;

    move-result-object v0

    iput-object v0, p0, Lyz;->C:Lcta;

    .line 374
    new-instance v0, Lyz$7;

    invoke-direct {v0, p0, p1}, Lyz$7;-><init>(Lyz;Lyz$a;)V

    iput-object v0, p0, Lyz;->D:Lcta;

    .line 387
    iget-object v0, p0, Lyz;->D:Lcta;

    .line 389
    invoke-static {v0}, Lcom/twitter/app/common/abs/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 388
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->E:Lcta;

    .line 394
    invoke-static {p1}, Lyz$a;->b(Lyz$a;)Lano;

    move-result-object v0

    .line 393
    invoke-static {v0}, Lanp;->a(Lano;)Ldagger/internal/c;

    move-result-object v0

    .line 392
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->F:Lcta;

    .line 396
    iget-object v0, p0, Lyz;->F:Lcta;

    .line 397
    invoke-static {v0}, Lzl;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lyz;->G:Lcta;

    .line 400
    iget-object v0, p0, Lyz;->b:Lcta;

    iget-object v1, p0, Lyz;->c:Lcta;

    iget-object v2, p0, Lyz;->G:Lcta;

    .line 402
    invoke-static {v0, v1, v2}, Lzm;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 401
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->H:Lcta;

    .line 407
    iget-object v0, p0, Lyz;->G:Lcta;

    iget-object v1, p0, Lyz;->t:Lcta;

    iget-object v2, p0, Lyz;->x:Lcta;

    iget-object v3, p0, Lyz;->H:Lcta;

    iget-object v4, p0, Lyz;->l:Lcta;

    .line 409
    invoke-static {v0, v1, v2, v3, v4}, Lyc;->a(Lcta;Lcta;Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 408
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->I:Lcta;

    .line 416
    iget-object v0, p0, Lyz;->I:Lcta;

    .line 417
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->J:Lcta;

    .line 419
    new-instance v0, Lyz$8;

    invoke-direct {v0, p0, p1}, Lyz$8;-><init>(Lyz;Lyz$a;)V

    iput-object v0, p0, Lyz;->K:Lcta;

    .line 432
    iget-object v0, p0, Lyz;->K:Lcta;

    .line 433
    invoke-static {v0}, Lcrd;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lyz;->L:Lcta;

    .line 435
    iget-object v0, p0, Lyz;->L:Lcta;

    .line 437
    invoke-static {v0}, Lcig;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 436
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->M:Lcta;

    .line 439
    iget-object v0, p0, Lyz;->A:Lcta;

    iget-object v1, p0, Lyz;->M:Lcta;

    .line 441
    invoke-static {v0, v1}, Lxq;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 440
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyz;->N:Lcta;

    .line 443
    return-void
.end method


# virtual methods
.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 447
    iget-object v0, p0, Lyz;->C:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public c()Lcom/twitter/app/common/abs/j;
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lyz;->E:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/j;

    return-object v0
.end method

.method public d()Lyf;
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Lyz;->d:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyf;

    return-object v0
.end method

.method public e()Lya;
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, Lyz;->J:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lya;

    return-object v0
.end method

.method public f()Lxp;
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lyz;->N:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxp;

    return-object v0
.end method
