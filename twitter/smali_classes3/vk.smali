.class public Lvk;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lvj;


# instance fields
.field private final a:Lcom/twitter/android/media/selection/a;

.field private final b:Lcom/twitter/android/media/selection/c;

.field private final c:Lcom/twitter/library/client/Session;

.field private final d:Lvm;

.field private final e:Landroid/content/Context;

.field private final f:Lcom/twitter/library/client/p;

.field private final g:Lakr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lakr",
            "<",
            "Lcom/twitter/android/composer/a;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/twitter/model/timeline/ap;

.field private final i:Lcom/twitter/analytics/model/ScribeItem;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lvm;Lcom/twitter/android/media/selection/c;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/p;Lakr;Lcom/twitter/model/timeline/ap;Lcom/twitter/analytics/feature/model/TwitterScribeItem;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lvm;",
            "Lcom/twitter/android/media/selection/c;",
            "Lcom/twitter/library/client/Session;",
            "Lcom/twitter/library/client/p;",
            "Lakr",
            "<",
            "Lcom/twitter/android/composer/a;",
            ">;",
            "Lcom/twitter/model/timeline/ap;",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ")V"
        }
    .end annotation

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Lvk;->e:Landroid/content/Context;

    .line 80
    iput-object p2, p0, Lvk;->d:Lvm;

    .line 81
    iput-object p3, p0, Lvk;->b:Lcom/twitter/android/media/selection/c;

    .line 82
    iput-object p6, p0, Lvk;->g:Lakr;

    .line 83
    invoke-interface {p2}, Lvm;->e()Lcom/twitter/android/media/selection/a;

    move-result-object v0

    iput-object v0, p0, Lvk;->a:Lcom/twitter/android/media/selection/a;

    .line 84
    iput-object p4, p0, Lvk;->c:Lcom/twitter/library/client/Session;

    .line 85
    iput-object p5, p0, Lvk;->f:Lcom/twitter/library/client/p;

    .line 86
    iput-object p7, p0, Lvk;->h:Lcom/twitter/model/timeline/ap;

    .line 87
    iput-object p8, p0, Lvk;->i:Lcom/twitter/analytics/model/ScribeItem;

    .line 88
    return-void
.end method

.method private a(Lcom/twitter/model/drafts/a;)V
    .locals 7

    .prologue
    .line 129
    iget-object v0, p0, Lvk;->c:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    .line 131
    new-instance v1, Lcom/twitter/analytics/feature/model/ClientEventLog;

    invoke-direct {v1, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 132
    invoke-static {v1}, Lcom/twitter/library/scribe/c;->a(Lcom/twitter/analytics/feature/model/ClientEventLog;)V

    .line 133
    iget-object v0, p0, Lvk;->i:Lcom/twitter/analytics/model/ScribeItem;

    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "tweet:composition:::send_reply"

    aput-object v6, v4, v5

    .line 134
    invoke-virtual {v0, v4}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 135
    invoke-static {v1}, Lcpm;->a(Lcpk;)V

    .line 136
    sget-object v0, Lcom/twitter/android/composer/ComposerType;->b:Lcom/twitter/android/composer/ComposerType;

    iget-object v1, p1, Lcom/twitter/model/drafts/a;->d:Ljava/util/List;

    invoke-static {v2, v3, v0, v1}, Lcom/twitter/android/composer/p;->a(JLcom/twitter/android/composer/ComposerType;Ljava/util/List;)V

    .line 137
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 170
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 92
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 93
    invoke-virtual {p0}, Lvk;->j()V

    .line 94
    iget-object v0, p0, Lvk;->d:Lvm;

    invoke-interface {v0}, Lvm;->a()V

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    const/4 v0, -0x2

    if-ne p1, v0, :cond_0

    .line 96
    invoke-virtual {p0}, Lvk;->k()V

    .line 97
    iget-object v0, p0, Lvk;->d:Lvm;

    invoke-interface {v0}, Lvm;->a()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/media/selection/MediaAttachment;)V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lvk;->b:Lcom/twitter/android/media/selection/c;

    iget-object v1, p0, Lvk;->a:Lcom/twitter/android/media/selection/a;

    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/media/selection/c;->a(Lcom/twitter/android/media/selection/MediaAttachment;Lcom/twitter/android/media/selection/a;)V

    .line 117
    return-void
.end method

.method public a([JLjava/util/List;JJJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;JJJ)V"
        }
    .end annotation

    .prologue
    .line 207
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 174
    invoke-virtual {p0}, Lvk;->h()V

    .line 175
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 179
    invoke-virtual {p0}, Lvk;->l()V

    .line 180
    return-void
.end method

.method public d()V
    .locals 4

    .prologue
    .line 185
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lvk;->c:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 186
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, ":composition::add_photo:click"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 187
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 188
    return-void
.end method

.method public e()V
    .locals 4

    .prologue
    .line 193
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lvk;->c:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 194
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, ":composition::remove_photo:click"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 195
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 196
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lvk;->d:Lvm;

    invoke-interface {v0}, Lvm;->aH_()V

    .line 201
    return-void
.end method

.method public g()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 103
    iget-object v1, p0, Lvk;->d:Lvm;

    invoke-interface {v1}, Lvm;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 111
    :goto_0
    return v0

    .line 107
    :cond_0
    iget-object v1, p0, Lvk;->d:Lvm;

    invoke-interface {v1}, Lvm;->aK_()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 108
    iget-object v1, p0, Lvk;->d:Lvm;

    invoke-interface {v1}, Lvm;->aG_()V

    goto :goto_0

    .line 111
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method h()V
    .locals 3

    .prologue
    .line 120
    iget-object v0, p0, Lvk;->d:Lvm;

    invoke-interface {v0}, Lvm;->aJ_()V

    .line 121
    invoke-virtual {p0}, Lvk;->i()Lcom/twitter/model/drafts/a;

    move-result-object v0

    .line 122
    iget-object v1, p0, Lvk;->e:Landroid/content/Context;

    iget-object v2, p0, Lvk;->c:Lcom/twitter/library/client/Session;

    invoke-static {v1, v2, v0}, Lcom/twitter/android/client/x;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/drafts/a;)Ljava/lang/String;

    .line 123
    invoke-direct {p0, v0}, Lvk;->a(Lcom/twitter/model/drafts/a;)V

    .line 124
    iget-object v0, p0, Lvk;->d:Lvm;

    invoke-interface {v0}, Lvm;->j()V

    .line 125
    return-void
.end method

.method i()Lcom/twitter/model/drafts/a;
    .locals 2

    .prologue
    .line 141
    new-instance v0, Lcom/twitter/model/drafts/a$a;

    invoke-direct {v0}, Lcom/twitter/model/drafts/a$a;-><init>()V

    iget-object v1, p0, Lvk;->d:Lvm;

    .line 142
    invoke-interface {v1}, Lvm;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->a(Ljava/lang/String;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    iget-object v1, p0, Lvk;->d:Lvm;

    .line 143
    invoke-interface {v1}, Lvm;->h()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->a(Ljava/util/List;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    iget-object v1, p0, Lvk;->d:Lvm;

    .line 144
    invoke-interface {v1}, Lvm;->aI_()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->b(Ljava/util/List;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    iget-object v1, p0, Lvk;->h:Lcom/twitter/model/timeline/ap;

    .line 145
    invoke-virtual {v0, v1}, Lcom/twitter/model/drafts/a$a;->a(Lcom/twitter/model/timeline/ap;)Lcom/twitter/model/drafts/a$a;

    move-result-object v0

    .line 146
    invoke-virtual {v0}, Lcom/twitter/model/drafts/a$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/drafts/a;

    .line 141
    return-object v0
.end method

.method j()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 151
    invoke-virtual {p0}, Lvk;->i()Lcom/twitter/model/drafts/a;

    move-result-object v0

    .line 152
    new-instance v1, Lcom/twitter/android/composer/y;

    iget-object v2, p0, Lvk;->e:Landroid/content/Context;

    iget-object v3, p0, Lvk;->c:Lcom/twitter/library/client/Session;

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/twitter/android/composer/y;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/model/drafts/a;Z)V

    .line 153
    sget-object v0, Lcom/twitter/async/service/AsyncOperation$ExecutionClass;->g:Lcom/twitter/async/service/AsyncOperation$ExecutionClass;

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/y;->a(Lcom/twitter/async/service/AsyncOperation$ExecutionClass;)V

    .line 154
    iget-object v0, p0, Lvk;->f:Lcom/twitter/library/client/p;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/p;->a(Lcom/twitter/async/service/AsyncOperation;)Ljava/lang/String;

    .line 155
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lvk;->c:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 156
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "tweet"

    aput-object v2, v1, v4

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "cancel_reply_sheet"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "save_draft"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 157
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 158
    return-void
.end method

.method k()V
    .locals 4

    .prologue
    .line 162
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    iget-object v1, p0, Lvk;->c:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>(J)V

    .line 163
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "tweet"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "cancel_reply_sheet"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "dont_save"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->b([Ljava/lang/String;)Lcom/twitter/analytics/model/ScribeLog;

    .line 164
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 165
    return-void
.end method

.method l()V
    .locals 2

    .prologue
    .line 210
    invoke-virtual {p0}, Lvk;->m()Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 211
    iget-object v1, p0, Lvk;->d:Lvm;

    invoke-interface {v1}, Lvm;->j()V

    .line 212
    iget-object v1, p0, Lvk;->g:Lakr;

    invoke-virtual {v1, v0}, Lakr;->a(Lako;)V

    .line 213
    return-void
.end method

.method m()Lcom/twitter/android/composer/a;
    .locals 2

    .prologue
    .line 217
    invoke-static {}, Lcom/twitter/android/composer/a;->a()Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 218
    invoke-virtual {p0}, Lvk;->i()Lcom/twitter/model/drafts/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->a(Lcom/twitter/model/drafts/a;)Lcom/twitter/android/composer/a;

    move-result-object v0

    iget-object v1, p0, Lvk;->d:Lvm;

    .line 219
    invoke-interface {v1}, Lvm;->aK_()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->a(Z)Lcom/twitter/android/composer/a;

    move-result-object v0

    iget-object v1, p0, Lvk;->c:Lcom/twitter/library/client/Session;

    .line 220
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->b(Ljava/lang/String;)Lcom/twitter/android/composer/a;

    move-result-object v0

    iget-object v1, p0, Lvk;->d:Lvm;

    .line 221
    invoke-interface {v1}, Lvm;->l()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/a;->a([I)Lcom/twitter/android/composer/a;

    move-result-object v0

    .line 217
    return-object v0
.end method
