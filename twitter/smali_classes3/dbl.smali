.class public Ldbl;
.super Ldbh;
.source "Twttr"


# instance fields
.field private final e:Ltv/periscope/android/ui/broadcast/moderator/c;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/moderator/d;Ltv/periscope/android/ui/broadcast/moderator/g;Ltv/periscope/android/ui/broadcast/moderator/b;Ltv/periscope/android/ui/broadcast/moderator/c;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3, p4}, Ldbh;-><init>(Ljava/lang/String;Ltv/periscope/android/ui/broadcast/moderator/d;Ltv/periscope/android/ui/broadcast/moderator/g;Ltv/periscope/android/ui/broadcast/moderator/b;)V

    .line 25
    iput-object p5, p0, Ldbl;->e:Ltv/periscope/android/ui/broadcast/moderator/c;

    .line 26
    return-void
.end method

.method private d(Ltv/periscope/model/chat/Message;)Z
    .locals 2

    .prologue
    .line 62
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->A()Ltv/periscope/model/chat/MessageType$ReportType;

    move-result-object v0

    .line 63
    if-eqz v0, :cond_0

    sget-object v1, Ltv/periscope/model/chat/MessageType$ReportType;->a:Ltv/periscope/model/chat/MessageType$ReportType;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(Ltv/periscope/model/chat/Message;)Z
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0, p1}, Ldbl;->f(Ltv/periscope/model/chat/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Ldbl;->g(Ltv/periscope/model/chat/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Ldbl;->h(Ltv/periscope/model/chat/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(Ltv/periscope/model/chat/Message;)Z
    .locals 2

    .prologue
    .line 71
    sget-object v0, Ltv/periscope/model/chat/MessageType;->A:Ltv/periscope/model/chat/MessageType;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->b()Ltv/periscope/model/chat/MessageType;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g(Ltv/periscope/model/chat/Message;)Z
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->C()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private h(Ltv/periscope/model/chat/Message;)Z
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ldcq;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private i(Ltv/periscope/model/chat/Message;)Z
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->G()Ljava/lang/Integer;

    move-result-object v0

    .line 84
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 31
    const-string/jumbo v0, "WaitForModerationRequest"

    return-object v0
.end method

.method public a(Ltv/periscope/model/chat/Message;)Z
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Ldbl;->b:Ltv/periscope/android/ui/broadcast/moderator/d;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->B()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/broadcast/moderator/d;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 37
    invoke-direct {p0, p1}, Ldbl;->e(Ltv/periscope/model/chat/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbl;->e:Ltv/periscope/android/ui/broadcast/moderator/c;

    .line 38
    invoke-interface {v0}, Ltv/periscope/android/ui/broadcast/moderator/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbl;->b:Ltv/periscope/android/ui/broadcast/moderator/d;

    .line 39
    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/broadcast/moderator/d;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    invoke-direct {p0, p1}, Ldbl;->i(Ltv/periscope/model/chat/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    invoke-direct {p0, p1}, Ldbl;->d(Ltv/periscope/model/chat/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 36
    :goto_0
    return v0

    .line 41
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ltv/periscope/model/chat/Message;)V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Ldbl;->b:Ltv/periscope/android/ui/broadcast/moderator/d;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/broadcast/moderator/d;->a(Ltv/periscope/model/chat/Message;)V

    .line 47
    iget-object v0, p0, Ldbl;->c:Ltv/periscope/android/ui/broadcast/moderator/g;

    invoke-virtual {p1}, Ltv/periscope/model/chat/Message;->B()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ltv/periscope/android/ui/broadcast/moderator/g;->a(Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Ldbl;->c:Ltv/periscope/android/ui/broadcast/moderator/g;

    invoke-interface {v0, p1}, Ltv/periscope/android/ui/broadcast/moderator/g;->a(Ltv/periscope/model/chat/Message;)V

    .line 49
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Ldbl;->d:Ltv/periscope/android/ui/broadcast/moderator/b;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/moderator/b;->d()V

    .line 54
    iget-object v0, p0, Ldbl;->d:Ltv/periscope/android/ui/broadcast/moderator/b;

    invoke-virtual {v0}, Ltv/periscope/android/ui/broadcast/moderator/b;->b()Ltv/periscope/android/ui/broadcast/moderator/a;

    move-result-object v0

    .line 55
    if-nez v0, :cond_0

    .line 59
    :goto_0
    return-void

    .line 58
    :cond_0
    iget-object v1, p0, Ldbl;->c:Ltv/periscope/android/ui/broadcast/moderator/g;

    invoke-interface {v1, v0}, Ltv/periscope/android/ui/broadcast/moderator/g;->a(Ltv/periscope/android/ui/broadcast/moderator/a;)V

    goto :goto_0
.end method
