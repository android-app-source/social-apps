.class public Lpv;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpv$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/av/h;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lpu$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lpu$a;

    invoke-direct {v0, p1}, Lpu$a;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0}, Lpv;-><init>(Lpu$a;)V

    .line 43
    return-void
.end method

.method constructor <init>(Lpu$a;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lpv;->a:Ljava/util/Map;

    .line 49
    iput-object p1, p0, Lpv;->b:Lpu$a;

    .line 50
    return-void
.end method

.method private a(Lcom/twitter/library/client/Session;Ljava/lang/Long;)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/client/Session;",
            "Ljava/lang/Long;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/av/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lpv;->b:Lpu$a;

    .line 95
    invoke-virtual {v0, p1}, Lpu$a;->a(Lcom/twitter/library/client/Session;)Lpu;

    move-result-object v0

    invoke-virtual {v0, p2}, Lpu;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    new-instance v1, Lpv$a;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lpv$a;-><init>(Lpv$1;)V

    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 94
    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;)Lrx/c;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/client/Session;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/av/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    .line 63
    iget-object v2, p0, Lpv;->a:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 64
    iget-object v2, p0, Lpv;->a:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v4, 0x0

    .line 65
    invoke-direct {p0, p1, v4}, Lpv;->a(Lcom/twitter/library/client/Session;Ljava/lang/Long;)Lrx/c;

    move-result-object v4

    invoke-virtual {v4}, Lrx/c;->g()Lrx/c;

    move-result-object v4

    .line 64
    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    :cond_0
    iget-object v2, p0, Lpv;->a:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/c;

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;J)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/client/Session;",
            "J)",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/av/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lpv;->a(Lcom/twitter/library/client/Session;Ljava/lang/Long;)Lrx/c;

    move-result-object v0

    invoke-virtual {v0}, Lrx/c;->g()Lrx/c;

    move-result-object v0

    return-object v0
.end method
