.class public Lwy;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lxh;

.field private final b:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject",
            "<",
            "Lcfd;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/util/object/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/moments/viewmodels/a;",
            "Lcom/twitter/android/moments/viewmodels/b;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject",
            "<",
            "Lcom/twitter/model/moments/viewmodels/a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:J

.field private final f:Lrx/j;

.field private g:Lcom/twitter/android/moments/viewmodels/b;


# direct methods
.method public constructor <init>(Lxh;Lcom/twitter/util/object/d;Lxp;Lcom/twitter/model/moments/viewmodels/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lxh;",
            "Lcom/twitter/util/object/d",
            "<",
            "Lcom/twitter/model/moments/viewmodels/a;",
            "Lcom/twitter/android/moments/viewmodels/b;",
            ">;",
            "Lxp;",
            "Lcom/twitter/model/moments/viewmodels/a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lwy;->a:Lxh;

    .line 46
    iput-object p2, p0, Lwy;->c:Lcom/twitter/util/object/d;

    .line 47
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lwy;->b:Lrx/subjects/PublishSubject;

    .line 48
    invoke-static {}, Lrx/subjects/PublishSubject;->r()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lwy;->d:Lrx/subjects/PublishSubject;

    .line 49
    invoke-virtual {p4}, Lcom/twitter/model/moments/viewmodels/a;->a()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    iget-wide v0, v0, Lcom/twitter/model/moments/Moment;->b:J

    invoke-virtual {p3, v0, v1}, Lxp;->a(J)Lrx/c;

    move-result-object v0

    new-instance v1, Lwy$2;

    invoke-direct {v1, p0}, Lwy$2;-><init>(Lwy;)V

    .line 50
    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Lwy$1;

    invoke-direct {v1, p0}, Lwy$1;-><init>(Lwy;)V

    .line 57
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lwy;->d:Lrx/subjects/PublishSubject;

    .line 65
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/d;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lwy;->f:Lrx/j;

    .line 66
    iget-object v0, p0, Lwy;->c:Lcom/twitter/util/object/d;

    invoke-interface {v0, p4}, Lcom/twitter/util/object/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/moments/viewmodels/b;

    iput-object v0, p0, Lwy;->g:Lcom/twitter/android/moments/viewmodels/b;

    .line 67
    invoke-virtual {p4}, Lcom/twitter/model/moments/viewmodels/a;->a()Lcom/twitter/model/moments/Moment;

    move-result-object v0

    iget-wide v0, v0, Lcom/twitter/model/moments/Moment;->b:J

    iput-wide v0, p0, Lwy;->e:J

    .line 68
    return-void
.end method

.method static synthetic a(Lwy;)Lcom/twitter/android/moments/viewmodels/b;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lwy;->g:Lcom/twitter/android/moments/viewmodels/b;

    return-object v0
.end method

.method static synthetic a(Lwy;Lcom/twitter/android/moments/viewmodels/b;)Lcom/twitter/android/moments/viewmodels/b;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lwy;->g:Lcom/twitter/android/moments/viewmodels/b;

    return-object p1
.end method

.method static synthetic b(Lwy;)Lcom/twitter/util/object/d;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lwy;->c:Lcom/twitter/util/object/d;

    return-object v0
.end method


# virtual methods
.method public a()Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/moments/viewmodels/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lwy;->b:Lrx/subjects/PublishSubject;

    new-instance v1, Lwy$3;

    invoke-direct {v1, p0}, Lwy$3;-><init>(Lwy;)V

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lwy;->g:Lcom/twitter/android/moments/viewmodels/b;

    .line 99
    invoke-virtual {v1}, Lcom/twitter/android/moments/viewmodels/b;->a()Lcom/twitter/model/moments/viewmodels/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->d(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lwy;->d:Lrx/subjects/PublishSubject;

    invoke-virtual {v0, v1}, Lrx/c;->g(Lrx/c;)Lrx/c;

    move-result-object v0

    .line 94
    return-object v0
.end method

.method public a(Lcfd;)V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lwy;->a(Lcfd;Lcom/twitter/model/moments/viewmodels/g;)V

    .line 75
    return-void
.end method

.method public a(Lcfd;Lcom/twitter/model/moments/viewmodels/g;)V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lwy;->g:Lcom/twitter/android/moments/viewmodels/b;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/moments/viewmodels/b;->a(Lcfd;Lcom/twitter/model/moments/viewmodels/g;)Lcom/twitter/android/moments/viewmodels/b;

    .line 85
    iget-object v0, p0, Lwy;->b:Lrx/subjects/PublishSubject;

    invoke-virtual {v0, p1}, Lrx/subjects/PublishSubject;->a(Ljava/lang/Object;)V

    .line 86
    return-void
.end method

.method public b()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<",
            "Lcfd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lwy;->b:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method public c()Lrx/g;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<",
            "Lwy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Lwy;->a:Lxh;

    iget-wide v2, p0, Lwy;->e:J

    iget-object v1, p0, Lwy;->g:Lcom/twitter/android/moments/viewmodels/b;

    invoke-virtual {v1}, Lcom/twitter/android/moments/viewmodels/b;->b()Ljava/lang/Iterable;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Lxh;->a(JLjava/lang/Iterable;)Lrx/g;

    move-result-object v0

    new-instance v1, Lwy$4;

    invoke-direct {v1, p0}, Lwy$4;-><init>(Lwy;)V

    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/b;)Lrx/g;

    move-result-object v0

    .line 123
    invoke-static {p0}, Lcre;->a(Ljava/lang/Object;)Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/g;->c(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    .line 116
    return-object v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lwy;->f:Lrx/j;

    invoke-static {v0}, Lcrj;->a(Lrx/j;)V

    .line 128
    return-void
.end method
