.class public final Lyq;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lys;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lyq$a;
    }
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private A:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxn;",
            ">;"
        }
    .end annotation
.end field

.field private B:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private C:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;>;"
        }
    .end annotation
.end field

.field private D:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/p;",
            ">;"
        }
    .end annotation
.end field

.field private E:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/app/common/abs/j;",
            ">;"
        }
    .end annotation
.end field

.field private F:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/support/v4/content/LocalBroadcastManager;",
            ">;"
        }
    .end annotation
.end field

.field private G:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcrc;",
            ">;"
        }
    .end annotation
.end field

.field private H:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcif;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/client/Session;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lyf;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/database/schema/TwitterSchema;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lwo;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcnz;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/android/moments/data/c",
            "<",
            "Lapb;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private j:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxv;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/library/provider/t;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lbsb;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/util/object/d",
            "<",
            "Ljava/lang/Long;",
            "Lbdk;",
            ">;>;"
        }
    .end annotation
.end field

.field private n:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lwq$a;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lwq;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxr;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lawb;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/Long;",
            "Lcfe;",
            ">;>;"
        }
    .end annotation
.end field

.field private s:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lcom/twitter/database/lru/l",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;>;"
        }
    .end annotation
.end field

.field private t:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxx;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lyd;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxt;",
            ">;"
        }
    .end annotation
.end field

.field private w:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Laun",
            "<",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcep;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private x:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lyh;",
            ">;"
        }
    .end annotation
.end field

.field private y:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lyg;",
            ">;"
        }
    .end annotation
.end field

.field private z:Lcta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcta",
            "<",
            "Lxk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-class v0, Lyq;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lyq;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lyq$a;)V
    .locals 1

    .prologue
    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    sget-boolean v0, Lyq;->a:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 148
    :cond_0
    invoke-direct {p0, p1}, Lyq;->a(Lyq$a;)V

    .line 149
    return-void
.end method

.method synthetic constructor <init>(Lyq$a;Lyq$1;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lyq;-><init>(Lyq$a;)V

    return-void
.end method

.method public static a()Lyq$a;
    .locals 2

    .prologue
    .line 152
    new-instance v0, Lyq$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lyq$a;-><init>(Lyq$1;)V

    return-object v0
.end method

.method private a(Lyq$a;)V
    .locals 3

    .prologue
    .line 158
    new-instance v0, Lyq$1;

    invoke-direct {v0, p0, p1}, Lyq$1;-><init>(Lyq;Lyq$a;)V

    iput-object v0, p0, Lyq;->b:Lcta;

    .line 171
    new-instance v0, Lyq$2;

    invoke-direct {v0, p0, p1}, Lyq$2;-><init>(Lyq;Lyq$a;)V

    iput-object v0, p0, Lyq;->c:Lcta;

    .line 184
    iget-object v0, p0, Lyq;->b:Lcta;

    iget-object v1, p0, Lyq;->c:Lcta;

    .line 186
    invoke-static {v0, v1}, Lyp;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 185
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyq;->d:Lcta;

    .line 189
    iget-object v0, p0, Lyq;->d:Lcta;

    .line 190
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyq;->e:Lcta;

    .line 192
    new-instance v0, Lyq$3;

    invoke-direct {v0, p0, p1}, Lyq$3;-><init>(Lyq;Lyq$a;)V

    iput-object v0, p0, Lyq;->f:Lcta;

    .line 205
    iget-object v0, p0, Lyq;->f:Lcta;

    .line 207
    invoke-static {v0}, Lyl;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 206
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyq;->g:Lcta;

    .line 210
    new-instance v0, Lyq$4;

    invoke-direct {v0, p0, p1}, Lyq$4;-><init>(Lyq;Lyq$a;)V

    iput-object v0, p0, Lyq;->h:Lcta;

    .line 223
    iget-object v0, p0, Lyq;->b:Lcta;

    .line 225
    invoke-static {v0}, Lyn;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 224
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyq;->i:Lcta;

    .line 228
    iget-object v0, p0, Lyq;->h:Lcta;

    iget-object v1, p0, Lyq;->i:Lcta;

    .line 230
    invoke-static {v0, v1}, Lxw;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 229
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyq;->j:Lcta;

    .line 233
    new-instance v0, Lyq$5;

    invoke-direct {v0, p0, p1}, Lyq$5;-><init>(Lyq;Lyq$a;)V

    iput-object v0, p0, Lyq;->k:Lcta;

    .line 246
    iget-object v0, p0, Lyq;->b:Lcta;

    iget-object v1, p0, Lyq;->k:Lcta;

    iget-object v2, p0, Lyq;->h:Lcta;

    .line 248
    invoke-static {v0, v1, v2}, Lbsc;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 247
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyq;->l:Lcta;

    .line 253
    iget-object v0, p0, Lyq;->b:Lcta;

    iget-object v1, p0, Lyq;->c:Lcta;

    iget-object v2, p0, Lyq;->l:Lcta;

    .line 255
    invoke-static {v0, v1, v2}, Lym;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 254
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyq;->m:Lcta;

    .line 261
    invoke-static {}, Ldagger/internal/MembersInjectors;->a()Lcsd;

    move-result-object v0

    iget-object v1, p0, Lyq;->m:Lcta;

    .line 260
    invoke-static {v0, v1}, Lws;->a(Lcsd;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 259
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyq;->n:Lcta;

    .line 264
    iget-object v0, p0, Lyq;->n:Lcta;

    iget-object v1, p0, Lyq;->j:Lcta;

    .line 266
    invoke-static {v0, v1}, Lwr;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 265
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyq;->o:Lcta;

    .line 269
    iget-object v0, p0, Lyq;->g:Lcta;

    iget-object v1, p0, Lyq;->j:Lcta;

    iget-object v2, p0, Lyq;->o:Lcta;

    .line 271
    invoke-static {v0, v1, v2}, Lxs;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 270
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyq;->p:Lcta;

    .line 276
    new-instance v0, Lyq$6;

    invoke-direct {v0, p0, p1}, Lyq$6;-><init>(Lyq;Lyq$a;)V

    iput-object v0, p0, Lyq;->q:Lcta;

    .line 289
    iget-object v0, p0, Lyq;->q:Lcta;

    .line 291
    invoke-static {v0}, Lyv;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 290
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyq;->r:Lcta;

    .line 294
    iget-object v0, p0, Lyq;->q:Lcta;

    .line 296
    invoke-static {v0}, Lyo;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 295
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyq;->s:Lcta;

    .line 299
    iget-object v0, p0, Lyq;->s:Lcta;

    .line 301
    invoke-static {v0}, Lxy;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 300
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyq;->t:Lcta;

    .line 304
    iget-object v0, p0, Lyq;->d:Lcta;

    .line 306
    invoke-static {v0}, Lye;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 305
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyq;->u:Lcta;

    .line 308
    iget-object v0, p0, Lyq;->t:Lcta;

    iget-object v1, p0, Lyq;->u:Lcta;

    .line 310
    invoke-static {v0, v1}, Lxu;->a(Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 309
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyq;->v:Lcta;

    .line 314
    iget-object v0, p0, Lyq;->v:Lcta;

    .line 315
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyq;->w:Lcta;

    .line 317
    iget-object v0, p0, Lyq;->w:Lcta;

    .line 321
    invoke-static {}, Lcom/twitter/android/moments/data/u;->c()Ldagger/internal/c;

    move-result-object v1

    iget-object v2, p0, Lyq;->d:Lcta;

    .line 319
    invoke-static {v0, v1, v2}, Lyi;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 318
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyq;->x:Lcta;

    .line 324
    iget-object v0, p0, Lyq;->x:Lcta;

    .line 325
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyq;->y:Lcta;

    .line 327
    iget-object v0, p0, Lyq;->y:Lcta;

    .line 329
    invoke-static {v0}, Lxl;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 328
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyq;->z:Lcta;

    .line 331
    iget-object v0, p0, Lyq;->p:Lcta;

    iget-object v1, p0, Lyq;->r:Lcta;

    iget-object v2, p0, Lyq;->z:Lcta;

    .line 333
    invoke-static {v0, v1, v2}, Lxo;->a(Lcta;Lcta;Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 332
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyq;->A:Lcta;

    .line 338
    iget-object v0, p0, Lyq;->A:Lcta;

    .line 339
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyq;->B:Lcta;

    .line 341
    const/4 v0, 0x2

    const/4 v1, 0x0

    .line 342
    invoke-static {v0, v1}, Ldagger/internal/f;->a(II)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lyq;->e:Lcta;

    .line 343
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    iget-object v1, p0, Lyq;->B:Lcta;

    .line 344
    invoke-virtual {v0, v1}, Ldagger/internal/f$a;->a(Lcta;)Ldagger/internal/f$a;

    move-result-object v0

    .line 345
    invoke-virtual {v0}, Ldagger/internal/f$a;->a()Ldagger/internal/f;

    move-result-object v0

    iput-object v0, p0, Lyq;->C:Lcta;

    .line 347
    new-instance v0, Lyq$7;

    invoke-direct {v0, p0, p1}, Lyq$7;-><init>(Lyq;Lyq$a;)V

    iput-object v0, p0, Lyq;->D:Lcta;

    .line 360
    iget-object v0, p0, Lyq;->D:Lcta;

    .line 362
    invoke-static {v0}, Lcom/twitter/app/common/abs/k;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 361
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyq;->E:Lcta;

    .line 364
    new-instance v0, Lyq$8;

    invoke-direct {v0, p0, p1}, Lyq$8;-><init>(Lyq;Lyq$a;)V

    iput-object v0, p0, Lyq;->F:Lcta;

    .line 377
    iget-object v0, p0, Lyq;->F:Lcta;

    .line 378
    invoke-static {v0}, Lcrd;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    iput-object v0, p0, Lyq;->G:Lcta;

    .line 380
    iget-object v0, p0, Lyq;->G:Lcta;

    .line 382
    invoke-static {v0}, Lcig;->a(Lcta;)Ldagger/internal/c;

    move-result-object v0

    .line 381
    invoke-static {v0}, Ldagger/internal/b;->a(Lcta;)Lcta;

    move-result-object v0

    iput-object v0, p0, Lyq;->H:Lcta;

    .line 383
    return-void
.end method


# virtual methods
.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lala;",
            ">;"
        }
    .end annotation

    .prologue
    .line 387
    iget-object v0, p0, Lyq;->C:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public c()Lcom/twitter/app/common/abs/j;
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lyq;->E:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/app/common/abs/j;

    return-object v0
.end method

.method public d()Lxn;
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lyq;->A:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxn;

    return-object v0
.end method

.method public e()Lyf;
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Lyq;->d:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyf;

    return-object v0
.end method

.method public f()Lwq;
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lyq;->o:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwq;

    return-object v0
.end method

.method public g()Lcif;
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lyq;->H:Lcta;

    invoke-interface {v0}, Lcta;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcif;

    return-object v0
.end method
