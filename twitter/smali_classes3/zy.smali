.class public Lzy;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/moments/viewmodels/a;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;
    .locals 10

    .prologue
    const/4 v1, 0x2

    .line 17
    new-instance v4, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;

    invoke-direct {v4}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;-><init>()V

    .line 19
    const-wide/16 v2, -0x1

    .line 20
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    move-result-object v0

    sget-object v5, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->c:Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    if-ne v0, v5, :cond_2

    move-object v0, p0

    .line 21
    check-cast v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;

    .line 22
    sget-object v5, Lzy$1;->a:[I

    iget-object v6, v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->a:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    invoke-virtual {v6}, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 41
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Unknown video type: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v0, v0, Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage;->a:Lcom/twitter/model/moments/viewmodels/MomentTweetStreamingVideoPage$VideoType;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lcpd;->c(Ljava/lang/Throwable;)V

    move v0, v1

    :goto_0
    move-wide v8, v2

    move v2, v0

    move-wide v0, v8

    .line 54
    :goto_1
    invoke-virtual {v4, v2}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;->a(I)Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;

    .line 55
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 56
    invoke-virtual {v4, v0, v1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;->a(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;

    .line 58
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/a;->b()I

    move-result v0

    if-lez v0, :cond_1

    .line 59
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/a;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 60
    if-ltz v0, :cond_1

    .line 61
    invoke-virtual {v4, v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;->b(I)Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;

    .line 62
    invoke-virtual {p1}, Lcom/twitter/model/moments/viewmodels/a;->b()I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;->c(I)Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;

    .line 65
    :cond_1
    invoke-virtual {v4}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;

    return-object v0

    .line 24
    :pswitch_0
    const/4 v0, 0x6

    .line 25
    goto :goto_0

    .line 28
    :pswitch_1
    const/4 v0, 0x0

    .line 29
    goto :goto_0

    .line 33
    :pswitch_2
    const/4 v0, 0x3

    .line 34
    goto :goto_0

    .line 37
    :pswitch_3
    const/4 v0, 0x4

    .line 38
    goto :goto_0

    .line 46
    :cond_2
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    move-result-object v0

    sget-object v5, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->b:Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    if-ne v0, v5, :cond_3

    .line 47
    const/4 v2, 0x1

    move-object v0, p0

    .line 48
    check-cast v0, Lcom/twitter/model/moments/viewmodels/o;

    iget-wide v0, v0, Lcom/twitter/model/moments/viewmodels/o;->a:J

    goto :goto_1

    .line 49
    :cond_3
    invoke-virtual {p0}, Lcom/twitter/model/moments/viewmodels/MomentPage;->e()Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    move-result-object v0

    sget-object v5, Lcom/twitter/model/moments/viewmodels/MomentPage$Type;->d:Lcom/twitter/model/moments/viewmodels/MomentPage$Type;

    if-ne v0, v5, :cond_4

    .line 50
    const/4 v0, 0x5

    move-wide v8, v2

    move v2, v0

    move-wide v0, v8

    goto :goto_1

    :cond_4
    move-wide v8, v2

    move v2, v1

    move-wide v0, v8

    .line 52
    goto :goto_1

    .line 22
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
