.class Lcyg$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/graphics/b$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcyg;->a(Ltv/periscope/android/graphics/GLRenderView;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ltv/periscope/android/graphics/GLRenderView;

.field final synthetic b:Lcyg;


# direct methods
.method constructor <init>(Lcyg;Ltv/periscope/android/graphics/GLRenderView;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcyg$1;->b:Lcyg;

    iput-object p2, p0, Lcyg$1;->a:Ltv/periscope/android/graphics/GLRenderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 176
    iget-object v0, p0, Lcyg$1;->b:Lcyg;

    new-instance v1, Ltv/periscope/android/graphics/o;

    iget-object v2, p0, Lcyg$1;->a:Ltv/periscope/android/graphics/GLRenderView;

    invoke-virtual {v2}, Ltv/periscope/android/graphics/GLRenderView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Ltv/periscope/android/graphics/o;-><init>(Landroid/content/Context;)V

    iput-object v1, v0, Lcyg;->j:Ltv/periscope/android/graphics/o;

    .line 177
    iget-object v0, p0, Lcyg$1;->b:Lcyg;

    iget-object v0, v0, Lcyg;->j:Ltv/periscope/android/graphics/o;

    const-string/jumbo v1, "Encoder"

    invoke-virtual {v0, v1}, Ltv/periscope/android/graphics/o;->a(Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcyg$1;->b:Lcyg;

    new-instance v1, Ltv/periscope/android/graphics/l;

    iget-object v2, p0, Lcyg$1;->a:Ltv/periscope/android/graphics/GLRenderView;

    invoke-virtual {v2}, Ltv/periscope/android/graphics/GLRenderView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Ltv/periscope/android/graphics/l;-><init>(Landroid/content/Context;)V

    iput-object v1, v0, Lcyg;->k:Ltv/periscope/android/graphics/o;

    .line 179
    iget-object v0, p0, Lcyg$1;->b:Lcyg;

    iget-object v0, v0, Lcyg;->k:Ltv/periscope/android/graphics/o;

    const-string/jumbo v1, "Preview"

    invoke-virtual {v0, v1}, Ltv/periscope/android/graphics/o;->a(Ljava/lang/String;)V

    .line 180
    iget-object v0, p0, Lcyg$1;->b:Lcyg;

    new-instance v1, Ltv/periscope/android/graphics/i;

    invoke-direct {v1}, Ltv/periscope/android/graphics/i;-><init>()V

    iput-object v1, v0, Lcyg;->h:Ltv/periscope/android/graphics/i;

    .line 181
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 185
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Unable to acquire video context"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
