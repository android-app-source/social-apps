.class public Lorg/spongycastle/asn1/bg;
.super Lorg/spongycastle/asn1/p;
.source "Twttr"


# instance fields
.field private a:[B


# direct methods
.method constructor <init>([B)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Lorg/spongycastle/asn1/p;-><init>()V

    .line 82
    iput-object p1, p0, Lorg/spongycastle/asn1/bg;->a:[B

    .line 83
    return-void
.end method


# virtual methods
.method a(Lorg/spongycastle/asn1/o;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    const/16 v0, 0x1a

    iget-object v1, p0, Lorg/spongycastle/asn1/bg;->a:[B

    invoke-virtual {p1, v0, v1}, Lorg/spongycastle/asn1/o;->a(I[B)V

    .line 124
    return-void
.end method

.method a()Z
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return v0
.end method

.method a(Lorg/spongycastle/asn1/p;)Z
    .locals 2

    .prologue
    .line 129
    instance-of v0, p1, Lorg/spongycastle/asn1/bg;

    if-nez v0, :cond_0

    .line 131
    const/4 v0, 0x0

    .line 134
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/spongycastle/asn1/bg;->a:[B

    check-cast p1, Lorg/spongycastle/asn1/bg;

    iget-object v1, p1, Lorg/spongycastle/asn1/bg;->a:[B

    invoke-static {v0, v1}, Lorg/spongycastle/util/a;->a([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method b()I
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lorg/spongycastle/asn1/bg;->a:[B

    array-length v0, v0

    invoke-static {v0}, Lorg/spongycastle/asn1/bs;->a(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lorg/spongycastle/asn1/bg;->a:[B

    array-length v1, v1

    add-int/2addr v0, v1

    return v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lorg/spongycastle/asn1/bg;->a:[B

    invoke-static {v0}, Lorg/spongycastle/util/d;->b([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lorg/spongycastle/asn1/bg;->a:[B

    invoke-static {v0}, Lorg/spongycastle/util/a;->a([B)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0}, Lorg/spongycastle/asn1/bg;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
