.class Lorg/spongycastle/asn1/ap;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field static final a:Lorg/spongycastle/asn1/q;

.field static final b:Lorg/spongycastle/asn1/s;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5
    new-instance v0, Lorg/spongycastle/asn1/ay;

    invoke-direct {v0}, Lorg/spongycastle/asn1/ay;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/ap;->a:Lorg/spongycastle/asn1/q;

    .line 6
    new-instance v0, Lorg/spongycastle/asn1/ba;

    invoke-direct {v0}, Lorg/spongycastle/asn1/ba;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/ap;->b:Lorg/spongycastle/asn1/s;

    return-void
.end method

.method static a(Lorg/spongycastle/asn1/e;)Lorg/spongycastle/asn1/q;
    .locals 2

    .prologue
    .line 10
    invoke-virtual {p0}, Lorg/spongycastle/asn1/e;->a()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    sget-object v0, Lorg/spongycastle/asn1/ap;->a:Lorg/spongycastle/asn1/q;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/spongycastle/asn1/bi;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/bi;-><init>(Lorg/spongycastle/asn1/e;)V

    goto :goto_0
.end method

.method static b(Lorg/spongycastle/asn1/e;)Lorg/spongycastle/asn1/s;
    .locals 2

    .prologue
    .line 15
    invoke-virtual {p0}, Lorg/spongycastle/asn1/e;->a()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    sget-object v0, Lorg/spongycastle/asn1/ap;->b:Lorg/spongycastle/asn1/s;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/spongycastle/asn1/bj;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/bj;-><init>(Lorg/spongycastle/asn1/e;)V

    goto :goto_0
.end method
