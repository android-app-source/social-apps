.class Lcyg$f;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ltv/periscope/android/graphics/GLRenderView$k;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcyg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "f"
.end annotation


# instance fields
.field final synthetic a:Lcyg;


# direct methods
.method private constructor <init>(Lcyg;)V
    .locals 0

    .prologue
    .line 1078
    iput-object p1, p0, Lcyg$f;->a:Lcyg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcyg;Lcyg$1;)V
    .locals 0

    .prologue
    .line 1078
    invoke-direct {p0, p1}, Lcyg$f;-><init>(Lcyg;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1096
    iget-object v1, p0, Lcyg$f;->a:Lcyg;

    iget-object v1, v1, Lcyg;->z:Ljava/lang/Object;

    monitor-enter v1

    .line 1097
    :try_start_0
    iget-object v2, p0, Lcyg$f;->a:Lcyg;

    iget-object v2, v2, Lcyg;->k:Ltv/periscope/android/graphics/o;

    iget-object v3, p0, Lcyg$f;->a:Lcyg;

    iget v3, v3, Lcyg;->s:I

    if-ne v3, v0, :cond_1

    :goto_0
    invoke-virtual {v2, v0}, Ltv/periscope/android/graphics/o;->a(Z)V

    .line 1098
    iget-object v0, p0, Lcyg$f;->a:Lcyg;

    iget-object v0, v0, Lcyg;->k:Ltv/periscope/android/graphics/o;

    iget-object v2, p0, Lcyg$f;->a:Lcyg;

    iget v2, v2, Lcyg;->s:I

    invoke-virtual {v0, v2}, Ltv/periscope/android/graphics/o;->c(I)V

    .line 1099
    iget-object v0, p0, Lcyg$f;->a:Lcyg;

    iget-object v0, v0, Lcyg;->k:Ltv/periscope/android/graphics/o;

    iget-object v2, p0, Lcyg$f;->a:Lcyg;

    iget v2, v2, Lcyg;->u:I

    invoke-virtual {v0, v2}, Ltv/periscope/android/graphics/o;->b(I)V

    .line 1100
    iget-object v0, p0, Lcyg$f;->a:Lcyg;

    iget-object v0, v0, Lcyg;->h:Ltv/periscope/android/graphics/i;

    .line 1101
    if-eqz v0, :cond_0

    .line 1102
    iget-object v2, p0, Lcyg$f;->a:Lcyg;

    iget-object v2, v2, Lcyg;->k:Ltv/periscope/android/graphics/o;

    invoke-virtual {v2, v0}, Ltv/periscope/android/graphics/o;->a(Ltv/periscope/android/graphics/i;)V

    .line 1104
    :cond_0
    monitor-exit v1

    .line 1105
    return-void

    .line 1097
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1104
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(II)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1091
    invoke-static {v0, v0, p1, p2}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 1092
    return-void
.end method

.method public a(Landroid/opengl/EGLConfig;)V
    .locals 2

    .prologue
    .line 1081
    iget-object v0, p0, Lcyg$f;->a:Lcyg;

    iget-object v0, v0, Lcyg;->g:Ltv/periscope/android/graphics/GLRenderView;

    new-instance v1, Lcyg$f$1;

    invoke-direct {v1, p0}, Lcyg$f$1;-><init>(Lcyg$f;)V

    invoke-virtual {v0, v1}, Ltv/periscope/android/graphics/GLRenderView;->post(Ljava/lang/Runnable;)Z

    .line 1087
    return-void
.end method
