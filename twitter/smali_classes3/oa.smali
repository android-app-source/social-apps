.class public Loa;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lbkn;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/av/playback/AVPlayer;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/av/playback/AVPlayer;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbiy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19
    new-instance v0, Loc;

    invoke-direct {v0, p1}, Loc;-><init>(Lcom/twitter/library/av/playback/AVPlayer;)V

    const/4 v1, 0x4

    new-array v1, v1, [Lbiy;

    const/4 v2, 0x0

    new-instance v3, Loj;

    invoke-direct {v3, p1}, Loj;-><init>(Lcom/twitter/library/av/playback/AVPlayer;)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Loh;

    invoke-direct {v3, p1}, Loh;-><init>(Lcom/twitter/library/av/playback/AVPlayer;)V

    aput-object v3, v1, v2

    const/4 v2, 0x2

    new-instance v3, Loi;

    invoke-direct {v3, p1}, Loi;-><init>(Lcom/twitter/library/av/playback/AVPlayer;)V

    aput-object v3, v1, v2

    const/4 v2, 0x3

    new-instance v3, Lob;

    invoke-direct {v3}, Lob;-><init>()V

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/library/av/playback/AVPlayer;",
            "Lcom/twitter/model/av/AVMedia;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbiy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    new-instance v0, Lof;

    invoke-direct {v0, p1, p2}, Lof;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V

    const/4 v1, 0x3

    new-array v1, v1, [Lbiy;

    const/4 v2, 0x0

    new-instance v3, Lnw;

    invoke-direct {v3, p1, p2}, Lnw;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Log;

    invoke-direct {v3, p1, p2}, Log;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V

    aput-object v3, v1, v2

    const/4 v2, 0x2

    new-instance v3, Lnr;

    invoke-direct {v3, p1, p2}, Lnr;-><init>(Lcom/twitter/library/av/playback/AVPlayer;Lcom/twitter/model/av/AVMedia;)V

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
