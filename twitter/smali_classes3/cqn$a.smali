.class Lcqn$a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcqs$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcqn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/content/SharedPreferences$Editor;


# direct methods
.method constructor <init>(Landroid/content/SharedPreferences$Editor;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    iput-object p1, p0, Lcqn$a;->a:Landroid/content/SharedPreferences$Editor;

    .line 112
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcqs$b;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcqn$a;->a:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 160
    return-object p0
.end method

.method public a(Ljava/lang/String;I)Lcqs$b;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcqn$a;->a:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 132
    return-object p0
.end method

.method public a(Ljava/lang/String;J)Lcqs$b;
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcqn$a;->a:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 139
    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lcqs$b;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcqn$a;->a:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 118
    return-object p0
.end method

.method public a(Ljava/lang/String;Z)Lcqs$b;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcqn$a;->a:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 153
    return-object p0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcqn$a;->a:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 173
    return-void
.end method
