.class public Lry;
.super Lru;
.source "Twttr"


# static fields
.field private static final c:Lcom/twitter/util/collection/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Lcom/twitter/util/collection/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final a:Lcom/twitter/util/concurrent/ObservablePromise;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/concurrent/ObservablePromise",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field b:Lcom/twitter/library/api/upload/g$a;

.field private final e:Ljava/lang/Object;

.field private f:Lcom/twitter/android/client/tweetuploadmanager/c;

.field private g:Lcom/twitter/util/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/library/api/progress/ProgressUpdatedEvent;",
            ">;"
        }
    .end annotation
.end field

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 51
    const-string/jumbo v0, "X-Media-Type"

    const-string/jumbo v1, "video/mp4"

    invoke-static {v0, v1}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    sput-object v0, Lry;->c:Lcom/twitter/util/collection/Pair;

    .line 52
    const-string/jumbo v0, "X-Media-Cropping"

    const-string/jumbo v1, "center"

    invoke-static {v0, v1}, Lcom/twitter/util/collection/Pair;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/Pair;

    move-result-object v0

    sput-object v0, Lry;->d:Lcom/twitter/util/collection/Pair;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lru;-><init>()V

    .line 54
    new-instance v0, Lcom/twitter/util/concurrent/ObservablePromise;

    invoke-direct {v0}, Lcom/twitter/util/concurrent/ObservablePromise;-><init>()V

    iput-object v0, p0, Lry;->a:Lcom/twitter/util/concurrent/ObservablePromise;

    .line 57
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lry;->e:Ljava/lang/Object;

    return-void
.end method

.method private static a(Lcom/twitter/model/media/EditableMedia;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/media/EditableMedia;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/util/collection/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 224
    sget-object v0, Lry$4;->a:[I

    invoke-virtual {p0}, Lcom/twitter/model/media/EditableMedia;->f()Lcom/twitter/media/model/MediaType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/media/model/MediaType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 238
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    .line 226
    :pswitch_0
    sget-object v0, Lry;->c:Lcom/twitter/util/collection/Pair;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 229
    :pswitch_1
    check-cast p0, Lcom/twitter/model/media/EditableSegmentedVideo;

    invoke-virtual {p0}, Lcom/twitter/model/media/EditableSegmentedVideo;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 231
    sget-object v1, Lry;->c:Lcom/twitter/util/collection/Pair;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    sget-object v1, Lry;->d:Lcom/twitter/util/collection/Pair;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 235
    :cond_0
    sget-object v0, Lry;->c:Lcom/twitter/util/collection/Pair;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 224
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(Lcom/twitter/android/client/tweetuploadmanager/c;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 184
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->g()Lcom/twitter/model/drafts/a;

    move-result-object v0

    .line 185
    if-nez v0, :cond_0

    move v0, v1

    .line 193
    :goto_0
    return v0

    .line 188
    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->o()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/upload/x;

    .line 189
    invoke-virtual {v0}, Lcom/twitter/library/api/upload/x;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 190
    goto :goto_0

    .line 193
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static c(Lcom/twitter/android/client/tweetuploadmanager/c;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 205
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->g()Lcom/twitter/model/drafts/a;

    move-result-object v0

    .line 206
    if-nez v0, :cond_0

    move v0, v1

    .line 219
    :goto_0
    return v0

    .line 212
    :cond_0
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    .line 213
    invoke-virtual {p0}, Lcom/twitter/android/client/tweetuploadmanager/c;->o()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/upload/x;

    .line 214
    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/api/upload/x;->a(J)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 215
    goto :goto_0

    .line 219
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private d(Lcom/twitter/android/client/tweetuploadmanager/c;)V
    .locals 7

    .prologue
    .line 98
    iget-object v0, p0, Lry;->f:Lcom/twitter/android/client/tweetuploadmanager/c;

    invoke-virtual {v0}, Lcom/twitter/android/client/tweetuploadmanager/c;->o()Ljava/util/List;

    move-result-object v0

    .line 100
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    iget v2, p0, Lry;->h:I

    if-le v1, v2, :cond_2

    iget-object v1, p0, Lry;->a:Lcom/twitter/util/concurrent/ObservablePromise;

    invoke-virtual {v1}, Lcom/twitter/util/concurrent/ObservablePromise;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 101
    iget v1, p0, Lry;->h:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lry;->h:I

    .line 102
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/upload/x;

    .line 103
    iget-object v1, p0, Lry;->f:Lcom/twitter/android/client/tweetuploadmanager/c;

    invoke-virtual {v1}, Lcom/twitter/android/client/tweetuploadmanager/c;->e()Landroid/content/Context;

    move-result-object v1

    .line 104
    iget-object v2, p0, Lry;->f:Lcom/twitter/android/client/tweetuploadmanager/c;

    invoke-virtual {v2}, Lcom/twitter/android/client/tweetuploadmanager/c;->l()Lcom/twitter/library/client/Session;

    move-result-object v2

    .line 106
    new-instance v3, Lcom/twitter/library/api/upload/g;

    new-instance v4, Lcom/twitter/library/service/v;

    invoke-direct {v4, v2}, Lcom/twitter/library/service/v;-><init>(Lcom/twitter/library/client/Session;)V

    invoke-direct {v3, v1, v4}, Lcom/twitter/library/api/upload/g;-><init>(Landroid/content/Context;Lcom/twitter/library/service/v;)V

    .line 107
    iget-object v2, p0, Lry;->e:Ljava/lang/Object;

    monitor-enter v2

    .line 108
    :try_start_0
    iget-object v1, p0, Lry;->b:Lcom/twitter/library/api/upload/g$a;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lry;->b:Lcom/twitter/library/api/upload/g$a;

    invoke-virtual {v1}, Lcom/twitter/library/api/upload/g$a;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 109
    monitor-exit v2

    .line 149
    :goto_0
    return-void

    .line 111
    :cond_0
    invoke-virtual {v0}, Lcom/twitter/library/api/upload/x;->b()Lcom/twitter/model/drafts/DraftAttachment;

    move-result-object v1

    .line 112
    iget-object v4, v1, Lcom/twitter/model/drafts/DraftAttachment;->f:Landroid/net/Uri;

    invoke-static {v4}, Lcom/twitter/util/ac;->e(Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, v1, Lcom/twitter/model/drafts/DraftAttachment;->f:Landroid/net/Uri;

    iget-object v1, v1, Lcom/twitter/model/drafts/DraftAttachment;->g:Lcom/twitter/media/model/MediaType;

    sget-object v5, Lcom/twitter/library/api/upload/MediaUsage;->a:Lcom/twitter/library/api/upload/MediaUsage;

    iget-object v6, p0, Lry;->g:Lcom/twitter/util/q;

    .line 113
    invoke-virtual {v3, v4, v1, v5, v6}, Lcom/twitter/library/api/upload/g;->a(Landroid/net/Uri;Lcom/twitter/media/model/MediaType;Lcom/twitter/library/api/upload/MediaUsage;Lcom/twitter/util/q;)Lcom/twitter/library/api/upload/g$a;

    move-result-object v1

    .line 118
    :goto_1
    iput-object v1, p0, Lry;->b:Lcom/twitter/library/api/upload/g$a;

    .line 124
    iget-object v1, p0, Lry;->b:Lcom/twitter/library/api/upload/g$a;

    new-instance v3, Lry$1;

    invoke-direct {v3, p0}, Lry$1;-><init>(Lry;)V

    invoke-virtual {v1, v3}, Lcom/twitter/library/api/upload/g$a;->d(Lcom/twitter/util/concurrent/d;)Lcom/twitter/util/concurrent/g;

    .line 133
    iget-object v1, p0, Lry;->b:Lcom/twitter/library/api/upload/g$a;

    new-instance v3, Lry$2;

    invoke-direct {v3, p0, p1, v0}, Lry$2;-><init>(Lry;Lcom/twitter/android/client/tweetuploadmanager/c;Lcom/twitter/library/api/upload/x;)V

    invoke-virtual {v1, v3}, Lcom/twitter/library/api/upload/g$a;->c(Lcom/twitter/util/concurrent/d;)Lcom/twitter/util/concurrent/g;

    .line 139
    iget-object v1, p0, Lry;->b:Lcom/twitter/library/api/upload/g$a;

    new-instance v3, Lry$3;

    invoke-direct {v3, p0, p1, v0}, Lry$3;-><init>(Lry;Lcom/twitter/android/client/tweetuploadmanager/c;Lcom/twitter/library/api/upload/x;)V

    invoke-virtual {v1, v3}, Lcom/twitter/library/api/upload/g$a;->b(Lcom/twitter/util/concurrent/d;)Lcom/twitter/util/concurrent/g;

    .line 145
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 119
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Lcom/twitter/library/api/upload/x;->e()Lcom/twitter/media/model/MediaFile;

    move-result-object v4

    const/4 v5, 0x2

    .line 120
    invoke-virtual {v1, v5}, Lcom/twitter/model/drafts/DraftAttachment;->a(I)Lcom/twitter/model/media/EditableMedia;

    move-result-object v1

    invoke-static {v1}, Lry;->a(Lcom/twitter/model/media/EditableMedia;)Ljava/util/List;

    move-result-object v1

    iget-object v5, p0, Lry;->g:Lcom/twitter/util/q;

    sget-object v6, Lcom/twitter/library/api/upload/MediaUsage;->a:Lcom/twitter/library/api/upload/MediaUsage;

    .line 118
    invoke-virtual {v3, v4, v1, v5, v6}, Lcom/twitter/library/api/upload/g;->a(Lcom/twitter/media/model/MediaFile;Ljava/util/List;Lcom/twitter/util/q;Lcom/twitter/library/api/upload/MediaUsage;)Lcom/twitter/library/api/upload/g$a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_1

    .line 147
    :cond_2
    iget-object v0, p0, Lry;->a:Lcom/twitter/util/concurrent/ObservablePromise;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/util/concurrent/ObservablePromise;->set(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/android/client/tweetuploadmanager/c;Lcom/twitter/util/q;)Lcom/twitter/util/concurrent/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/android/client/tweetuploadmanager/c;",
            "Lcom/twitter/util/q",
            "<",
            "Lcom/twitter/library/api/progress/ProgressUpdatedEvent;",
            ">;)",
            "Lcom/twitter/util/concurrent/g",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    iput-object p1, p0, Lry;->f:Lcom/twitter/android/client/tweetuploadmanager/c;

    .line 84
    iput-object p2, p0, Lry;->g:Lcom/twitter/util/q;

    .line 86
    const/4 v0, 0x0

    iput v0, p0, Lry;->h:I

    .line 87
    invoke-direct {p0, p1}, Lry;->d(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    .line 89
    iget-object v0, p0, Lry;->a:Lcom/twitter/util/concurrent/ObservablePromise;

    return-object v0
.end method

.method a(Lcom/twitter/android/client/tweetuploadmanager/c;Lcom/twitter/library/api/upload/x;Lcom/twitter/library/api/upload/f;)V
    .locals 4

    .prologue
    .line 165
    invoke-virtual {p3}, Lcom/twitter/library/api/upload/f;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 166
    invoke-virtual {p3}, Lcom/twitter/library/api/upload/f;->c()Ljava/lang/Exception;

    move-result-object v0

    .line 167
    if-nez v0, :cond_0

    .line 168
    iget-object v0, p0, Lry;->a:Lcom/twitter/util/concurrent/ObservablePromise;

    new-instance v1, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Media upload failed with an error code of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 169
    invoke-virtual {p3}, Lcom/twitter/library/api/upload/f;->d()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Lcom/twitter/android/client/tweetuploadmanager/TweetUploadException;-><init>(Lcom/twitter/android/client/tweetuploadmanager/c;Ljava/lang/String;)V

    .line 168
    invoke-virtual {v0, v1}, Lcom/twitter/util/concurrent/ObservablePromise;->setException(Ljava/lang/Throwable;)V

    .line 181
    :goto_0
    return-void

    .line 171
    :cond_0
    iget-object v1, p0, Lry;->a:Lcom/twitter/util/concurrent/ObservablePromise;

    invoke-virtual {v1, v0}, Lcom/twitter/util/concurrent/ObservablePromise;->setException(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 179
    :cond_1
    iget-wide v0, p3, Lcom/twitter/library/api/upload/f;->a:J

    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/twitter/library/api/upload/x;->a(JJ)V

    .line 180
    invoke-direct {p0, p1}, Lry;->d(Lcom/twitter/android/client/tweetuploadmanager/c;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/client/tweetuploadmanager/c;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 64
    iget-object v1, p0, Lry;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 65
    :try_start_0
    iget-object v0, p0, Lry;->a:Lcom/twitter/util/concurrent/ObservablePromise;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/twitter/util/concurrent/ObservablePromise;->cancel(Z)Z

    .line 66
    iget-object v0, p0, Lry;->b:Lcom/twitter/library/api/upload/g$a;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lry;->b:Lcom/twitter/library/api/upload/g$a;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/twitter/library/api/upload/g$a;->cancel(Z)Z

    .line 69
    :cond_0
    monitor-exit v1

    .line 70
    return v3

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
