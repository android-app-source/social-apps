.class Lcre$a;
.super Lcqw;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcre;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcqw",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/util/concurrent/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/util/concurrent/d",
            "<-TT;>;"
        }
    .end annotation
.end field

.field private volatile b:Lrx/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/i",
            "<-TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lrx/i;Lcom/twitter/util/concurrent/d;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/i",
            "<-TT;>;",
            "Lcom/twitter/util/concurrent/d",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 315
    invoke-direct {p0}, Lcqw;-><init>()V

    .line 316
    iput-object p1, p0, Lcre$a;->b:Lrx/i;

    .line 317
    new-instance v0, Lcre$a$1;

    invoke-direct {v0, p0}, Lcre$a$1;-><init>(Lcre$a;)V

    invoke-static {v0}, Lcwy;->a(Lrx/functions/a;)Lrx/j;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/i;->a(Lrx/j;)V

    .line 325
    iput-object p2, p0, Lcre$a;->a:Lcom/twitter/util/concurrent/d;

    .line 326
    return-void
.end method

.method static synthetic a(Lcre$a;Lrx/i;)Lrx/i;
    .locals 0

    .prologue
    .line 308
    iput-object p1, p0, Lcre$a;->b:Lrx/i;

    return-object p1
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 365
    iget-object v0, p0, Lcre$a;->b:Lrx/i;

    if-nez v0, :cond_1

    .line 366
    monitor-enter p0

    .line 367
    :try_start_0
    iget-object v0, p0, Lcre$a;->b:Lrx/i;

    if-nez v0, :cond_0

    .line 368
    iget-object v0, p0, Lcre$a;->a:Lcom/twitter/util/concurrent/d;

    invoke-interface {v0, p1}, Lcom/twitter/util/concurrent/d;->a(Ljava/lang/Object;)V

    .line 369
    invoke-virtual {p0}, Lcre$a;->B_()V

    .line 370
    monitor-exit p0

    .line 377
    :goto_0
    return-void

    .line 372
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 375
    :cond_1
    iget-object v0, p0, Lcre$a;->b:Lrx/i;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/i;

    .line 376
    invoke-virtual {v0, p1}, Lrx/i;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 372
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcre$a;->b:Lrx/i;

    if-nez v0, :cond_1

    .line 348
    monitor-enter p0

    .line 349
    :try_start_0
    iget-object v0, p0, Lcre$a;->b:Lrx/i;

    if-nez v0, :cond_0

    .line 350
    invoke-static {p1}, Lcpd;->c(Ljava/lang/Throwable;)V

    .line 351
    invoke-virtual {p0}, Lcre$a;->B_()V

    .line 352
    monitor-exit p0

    .line 361
    :goto_0
    return-void

    .line 354
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 357
    :cond_1
    iget-object v0, p0, Lcre$a;->b:Lrx/i;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/i;

    .line 358
    invoke-virtual {v0, p1}, Lrx/i;->a(Ljava/lang/Throwable;)V

    .line 360
    invoke-virtual {v0, p0}, Lrx/i;->a(Lrx/j;)V

    goto :goto_0

    .line 354
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public by_()V
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcre$a;->b:Lrx/i;

    if-nez v0, :cond_1

    .line 331
    monitor-enter p0

    .line 332
    :try_start_0
    iget-object v0, p0, Lcre$a;->b:Lrx/i;

    if-nez v0, :cond_0

    .line 333
    invoke-virtual {p0}, Lcre$a;->B_()V

    .line 334
    monitor-exit p0

    .line 343
    :goto_0
    return-void

    .line 336
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 339
    :cond_1
    iget-object v0, p0, Lcre$a;->b:Lrx/i;

    invoke-static {v0}, Lcom/twitter/util/object/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/i;

    .line 340
    invoke-virtual {v0}, Lrx/i;->by_()V

    .line 342
    invoke-virtual {v0, p0}, Lrx/i;->a(Lrx/j;)V

    goto :goto_0

    .line 336
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
