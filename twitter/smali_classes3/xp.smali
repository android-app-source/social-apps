.class public Lxp;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lxm;


# instance fields
.field private final a:Lxm;

.field private final b:Lcif;


# direct methods
.method public constructor <init>(Lxn;Lcif;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lxp;->a:Lxm;

    .line 34
    iput-object p2, p0, Lxp;->b:Lcif;

    .line 35
    return-void
.end method

.method static synthetic a(Lxp;)Lxm;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lxp;->a:Lxm;

    return-object v0
.end method


# virtual methods
.method public a(J)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/c",
            "<",
            "Lcom/twitter/util/collection/m",
            "<",
            "Lcom/twitter/model/moments/viewmodels/b;",
            "Lcom/twitter/model/moments/b;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lxp;->b:Lcif;

    invoke-virtual {v0, p1, p2}, Lcif;->d(J)Lrx/c;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->d(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    new-instance v1, Lxp$1;

    invoke-direct {v1, p0}, Lxp$1;-><init>(Lxp;)V

    invoke-virtual {v0, v1}, Lrx/c;->f(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lxp;->a:Lxm;

    invoke-static {v0}, Lcqc;->a(Ljava/io/Closeable;)V

    .line 56
    return-void
.end method
