.class public Lst;
.super Lsn;
.source "Twttr"


# direct methods
.method public constructor <init>(Lcom/twitter/model/core/Tweet;Lcom/twitter/media/request/a$a;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11
    invoke-direct {p0, p1, v0, p2, v0}, Lsn;-><init>(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/MediaEntity;Lcom/twitter/media/request/a$a;Ljava/lang/String;)V

    .line 12
    return-void
.end method


# virtual methods
.method public b()I
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lst;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->aj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 18
    const/4 v0, 0x3

    .line 23
    :goto_0
    return v0

    .line 19
    :cond_0
    iget-object v0, p0, Lst;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ag()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 20
    const/4 v0, 0x4

    goto :goto_0

    .line 23
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lst;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ag()Z

    move-result v0

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lst;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->L()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lst;->a:Lcom/twitter/model/core/Tweet;

    invoke-virtual {v0}, Lcom/twitter/model/core/Tweet;->ag()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
