.class public Ldby;
.super Ldbx;
.source "Twttr"


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ltv/periscope/android/ui/user/g;


# direct methods
.method public constructor <init>(Ltv/periscope/android/view/aj;Landroid/content/Context;Ltv/periscope/android/ui/user/g;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Ldbx;-><init>(Ltv/periscope/android/view/aj;)V

    .line 21
    iput-object p2, p0, Ldby;->b:Landroid/content/Context;

    .line 22
    iput-object p3, p0, Ldby;->c:Ltv/periscope/android/ui/user/g;

    .line 23
    return-void
.end method

.method public static a(Landroid/content/Context;Ltv/periscope/android/api/PsUser;Ltv/periscope/android/ui/user/g;)Landroid/support/v7/app/AlertDialog;
    .locals 3

    .prologue
    .line 59
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Ltv/periscope/android/library/f$l;->ps__unblock_dialog_title:I

    .line 60
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$l;->ps__block_unblock_dialog_btn_cancel:I

    const/4 v2, 0x0

    .line 61
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Ltv/periscope/android/library/f$l;->ps__unblock_dialog_btn_confirm:I

    new-instance v2, Ldby$1;

    invoke-direct {v2, p2, p1}, Ldby$1;-><init>(Ltv/periscope/android/ui/user/g;Ltv/periscope/android/api/PsUser;)V

    .line 62
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    .line 59
    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    sget v0, Ltv/periscope/android/library/f$l;->ps__profile_sheet_more_options_unblock:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ltv/periscope/android/api/PsUser;)Z
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Ldby;->b:Landroid/content/Context;

    iget-object v1, p0, Ldby;->c:Ltv/periscope/android/ui/user/g;

    invoke-static {v0, p1, v1}, Ldby;->a(Landroid/content/Context;Ltv/periscope/android/api/PsUser;Ltv/periscope/android/ui/user/g;)Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 53
    const/4 v0, 0x1

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x1

    return v0
.end method
