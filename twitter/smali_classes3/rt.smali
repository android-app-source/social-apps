.class public Lrt;
.super Lcom/twitter/library/api/r;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/twitter/library/api/r",
        "<",
        "Lcom/twitter/library/api/j",
        "<",
        "Ljava/lang/String;",
        "Lcom/twitter/model/core/z;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final b:Lrr;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lrr;)V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lrt;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/r;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    .line 34
    iput-object p3, p0, Lrt;->b:Lrr;

    .line 35
    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/service/d;
    .locals 6

    .prologue
    .line 40
    invoke-virtual {p0}, Lrt;->J()Lcom/twitter/library/service/d$a;

    move-result-object v0

    const-string/jumbo v1, "analytics.twitter.com"

    .line 41
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->b(Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    sget-object v1, Lcom/twitter/network/HttpOperation$RequestMethod;->a:Lcom/twitter/network/HttpOperation$RequestMethod;

    .line 42
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Lcom/twitter/network/HttpOperation$RequestMethod;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x0

    .line 43
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "mob_idsync_generate"

    aput-object v3, v1, v2

    .line 44
    invoke-virtual {v0, v1}, Lcom/twitter/library/service/d$a;->a([Ljava/lang/Object;)Lcom/twitter/library/service/d$a;

    move-result-object v0

    .line 46
    invoke-virtual {p0}, Lrt;->M()Lcom/twitter/library/service/v;

    move-result-object v1

    .line 47
    if-eqz v1, :cond_0

    .line 48
    const-string/jumbo v2, "user_id"

    iget-wide v4, v1, Lcom/twitter/library/service/v;->c:J

    invoke-virtual {v0, v2, v4, v5}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;J)Lcom/twitter/library/service/d$a;

    .line 51
    :cond_0
    sget-object v1, Lcom/twitter/library/client/b;->a:Lcom/twitter/library/client/b;

    invoke-virtual {v1}, Lcom/twitter/library/client/b;->a()Lcom/twitter/library/api/c;

    move-result-object v1

    .line 52
    if-eqz v1, :cond_1

    .line 53
    const-string/jumbo v2, "ad_id"

    invoke-virtual {v1}, Lcom/twitter/library/api/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/twitter/library/service/d$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/d$a;

    .line 56
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/library/service/d$a;->a()Lcom/twitter/library/service/d;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/j;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/network/HttpOperation;",
            "Lcom/twitter/library/service/u;",
            "Lcom/twitter/library/api/j",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 68
    invoke-virtual {p2}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    invoke-virtual {p3}, Lcom/twitter/library/api/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 70
    invoke-static {v0}, Lcom/twitter/util/collection/CollectionUtils;->b(Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 71
    iget-object v1, p0, Lrt;->b:Lrr;

    invoke-virtual {v1, v0}, Lrr;->a(Ljava/util/List;)V

    .line 74
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/service/c;)V
    .locals 0

    .prologue
    .line 22
    check-cast p3, Lcom/twitter/library/api/j;

    invoke-virtual {p0, p1, p2, p3}, Lrt;->a(Lcom/twitter/network/HttpOperation;Lcom/twitter/library/service/u;Lcom/twitter/library/api/j;)V

    return-void
.end method

.method protected b()Lcom/twitter/library/api/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/twitter/library/api/j",
            "<",
            "Ljava/lang/String;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    const-class v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/api/j;->a(Ljava/lang/Class;)Lcom/twitter/library/api/j;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    return-object v0
.end method

.method protected synthetic f()Lcom/twitter/library/service/c;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lrt;->b()Lcom/twitter/library/api/j;

    move-result-object v0

    return-object v0
.end method
