.class public Lcxv;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:J

.field private static final b:J


# instance fields
.field private final c:J

.field private final d:J

.field private final e:J

.field private f:J

.field private g:I

.field private h:J

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 16
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcxv;->a:J

    .line 17
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcxv;->b:J

    return-void
.end method

.method public constructor <init>(J)V
    .locals 9

    .prologue
    .line 29
    const/4 v3, 0x5

    sget-wide v4, Lcxv;->a:J

    sget-wide v6, Lcxv;->b:J

    move-object v0, p0

    move-wide v1, p1

    invoke-direct/range {v0 .. v7}, Lcxv;-><init>(JIJJ)V

    .line 30
    return-void
.end method

.method public constructor <init>(JIJJ)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcxv;->f:J

    .line 24
    const/4 v0, 0x5

    iput v0, p0, Lcxv;->g:I

    .line 33
    iput-wide p1, p0, Lcxv;->c:J

    .line 34
    iput p3, p0, Lcxv;->g:I

    .line 35
    iput-wide p4, p0, Lcxv;->d:J

    .line 36
    iput-wide p6, p0, Lcxv;->e:J

    .line 37
    return-void
.end method


# virtual methods
.method public a(JJ)V
    .locals 5

    .prologue
    .line 40
    iget-wide v0, p0, Lcxv;->f:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcxv;->i:Z

    if-eqz v0, :cond_1

    .line 55
    :cond_0
    :goto_0
    return-void

    .line 44
    :cond_1
    iget-wide v0, p0, Lcxv;->c:J

    sub-long/2addr v0, p1

    .line 45
    add-long v2, p3, v0

    iput-wide v2, p0, Lcxv;->h:J

    .line 47
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    iget-wide v2, p0, Lcxv;->d:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 48
    iget-wide v0, p0, Lcxv;->h:J

    iget-wide v2, p0, Lcxv;->e:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcxv;->h:J

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcxv;->i:Z

    .line 51
    :cond_2
    iget-wide v0, p0, Lcxv;->h:J

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcxv;->h:J

    .line 53
    iput-wide p1, p0, Lcxv;->f:J

    .line 54
    iget v0, p0, Lcxv;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcxv;->g:I

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcxv;->i:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcxv;->g:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lcxv;->h:J

    return-wide v0
.end method
