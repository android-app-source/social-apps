.class public Lwz;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/library/api/moments/maker/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/twitter/library/api/moments/maker/f",
            "<",
            "Lcom/twitter/library/api/moments/maker/a;",
            "Lcom/twitter/model/json/moments/maker/JsonCreateMomentResponse;",
            "Lcom/twitter/model/core/z;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lbsb;

.field private final c:Lxj;

.field private final d:Lcif;


# direct methods
.method public constructor <init>(Lbsb;Lcom/twitter/library/api/moments/maker/f;Lxj;Lcif;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbsb;",
            "Lcom/twitter/library/api/moments/maker/f",
            "<",
            "Lcom/twitter/library/api/moments/maker/a;",
            "Lcom/twitter/model/json/moments/maker/JsonCreateMomentResponse;",
            "Lcom/twitter/model/core/z;",
            ">;",
            "Lxj;",
            "Lcif;",
            ")V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p2, p0, Lwz;->a:Lcom/twitter/library/api/moments/maker/f;

    .line 57
    iput-object p1, p0, Lwz;->b:Lbsb;

    .line 58
    iput-object p3, p0, Lwz;->c:Lxj;

    .line 59
    iput-object p4, p0, Lwz;->d:Lcif;

    .line 60
    return-void
.end method

.method static synthetic a(Lwz;)Lbsb;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lwz;->b:Lbsb;

    return-object v0
.end method

.method private a()Lrx/functions/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/b",
            "<",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 103
    new-instance v0, Lwz$5;

    invoke-direct {v0, p0}, Lwz$5;-><init>(Lwz;)V

    return-object v0
.end method

.method public static a(Landroid/app/Activity;)Lwz;
    .locals 6

    .prologue
    .line 40
    invoke-static {}, Lcom/twitter/library/client/v;->a()Lcom/twitter/library/client/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/v;->c()Lcom/twitter/library/client/Session;

    move-result-object v0

    .line 41
    new-instance v1, Lbsb;

    .line 42
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/twitter/library/provider/t;->a(J)Lcom/twitter/library/provider/t;

    move-result-object v2

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v1, p0, v2, v4, v5}, Lbsb;-><init>(Landroid/content/Context;Lcom/twitter/library/provider/t;J)V

    .line 43
    new-instance v2, Lcif;

    new-instance v3, Lcrc;

    .line 44
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v4

    invoke-direct {v3, v4}, Lcrc;-><init>(Landroid/support/v4/content/LocalBroadcastManager;)V

    invoke-direct {v2, v3}, Lcif;-><init>(Lcrc;)V

    .line 45
    new-instance v3, Lwz;

    new-instance v4, Lcom/twitter/library/api/moments/maker/f;

    invoke-direct {v4, p0, v0}, Lcom/twitter/library/api/moments/maker/f;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    .line 47
    invoke-static {}, Lxj;->a()Lxj;

    move-result-object v0

    invoke-direct {v3, v1, v4, v0, v2}, Lwz;-><init>(Lbsb;Lcom/twitter/library/api/moments/maker/f;Lxj;Lcif;)V

    .line 45
    return-object v3
.end method

.method static synthetic b(Lwz;)Lcif;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lwz;->d:Lcif;

    return-object v0
.end method


# virtual methods
.method public a(Lcev;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcev;",
            ")",
            "Lrx/c",
            "<",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lwz;->a:Lcom/twitter/library/api/moments/maker/f;

    new-instance v1, Lcom/twitter/library/api/moments/maker/a;

    invoke-direct {v1, p1}, Lcom/twitter/library/api/moments/maker/a;-><init>(Lcev;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/moments/maker/f;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    .line 69
    invoke-static {}, Lcre;->e()Lrx/functions/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Lwz$4;

    invoke-direct {v1, p0}, Lwz$4;-><init>(Lwz;)V

    .line 70
    invoke-virtual {v0, v1}, Lrx/c;->d(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    new-instance v1, Lwz$3;

    invoke-direct {v1, p0}, Lwz$3;-><init>(Lwz;)V

    .line 77
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lwz;->c:Lxj;

    iget-object v1, v1, Lxj;->a:Lrx/f;

    .line 83
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    new-instance v1, Lwz$2;

    invoke-direct {v1, p0}, Lwz$2;-><init>(Lwz;)V

    .line 84
    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/functions/b;)Lrx/c;

    move-result-object v0

    new-instance v1, Lwz$1;

    invoke-direct {v1, p0}, Lwz$1;-><init>(Lwz;)V

    .line 90
    invoke-virtual {v0, v1}, Lrx/c;->h(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lwz;->c:Lxj;

    iget-object v1, v1, Lxj;->b:Lrx/f;

    .line 96
    invoke-virtual {v0, v1}, Lrx/c;->a(Lrx/f;)Lrx/c;

    move-result-object v0

    .line 97
    invoke-static {}, Lcom/twitter/util/collection/k;->a()Lcom/twitter/util/collection/k;

    move-result-object v1

    invoke-static {v1}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->f(Lrx/c;)Lrx/c;

    move-result-object v0

    .line 98
    invoke-direct {p0}, Lwz;->a()Lrx/functions/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/functions/b;)Lrx/c;

    move-result-object v0

    .line 68
    return-object v0
.end method
