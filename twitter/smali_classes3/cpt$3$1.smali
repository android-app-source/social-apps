.class Lcpt$3$1;
.super Lcpn;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcpt$3;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcpn",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcpt$3;

.field private final b:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<+",
            "Ljava/lang/Iterable",
            "<+TT;>;>;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcpt$3;)V
    .locals 1

    .prologue
    .line 469
    iput-object p1, p0, Lcpt$3$1;->a:Lcpt$3;

    invoke-direct {p0}, Lcpn;-><init>()V

    .line 470
    iget-object v0, p0, Lcpt$3$1;->a:Lcpt$3;

    iget-object v0, v0, Lcpt$3;->a:Ljava/lang/Iterable;

    .line 471
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcpt$3$1;->b:Ljava/util/Iterator;

    .line 472
    iget-object v0, p0, Lcpt$3$1;->b:Ljava/util/Iterator;

    .line 473
    invoke-direct {p0, v0}, Lcpt$3$1;->a(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcpt$3$1;->c:Ljava/util/Iterator;

    .line 472
    return-void
.end method

.method private a(Ljava/util/Iterator;)Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<+",
            "Ljava/lang/Iterable",
            "<+TT;>;>;)",
            "Ljava/util/Iterator",
            "<+TT;>;"
        }
    .end annotation

    .prologue
    .line 492
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 493
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 494
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 496
    :goto_0
    return-object v0

    .line 494
    :cond_0
    invoke-static {}, Lcpr;->c()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0

    .line 496
    :cond_1
    invoke-static {}, Lcpr;->c()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 486
    iget-object v0, p0, Lcpt$3$1;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 477
    :goto_0
    iget-object v0, p0, Lcpt$3$1;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcpt$3$1;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, p0, Lcpt$3$1;->b:Ljava/util/Iterator;

    invoke-direct {p0, v0}, Lcpt$3$1;->a(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcpt$3$1;->c:Ljava/util/Iterator;

    goto :goto_0

    .line 480
    :cond_0
    iget-object v0, p0, Lcpt$3$1;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method
