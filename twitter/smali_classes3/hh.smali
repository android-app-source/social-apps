.class public final Lhh;
.super Lhl$a;
.source "Twttr"


# static fields
.field private static c:Lhl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lhl",
            "<",
            "Lhh;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:F

.field public b:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 20
    const/16 v0, 0x100

    new-instance v1, Lhh;

    invoke-direct {v1, v2, v2}, Lhh;-><init>(FF)V

    invoke-static {v0, v1}, Lhl;->a(ILhl$a;)Lhl;

    move-result-object v0

    sput-object v0, Lhh;->c:Lhl;

    .line 21
    sget-object v0, Lhh;->c:Lhl;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Lhl;->a(F)V

    .line 22
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lhl$a;-><init>()V

    .line 45
    return-void
.end method

.method public constructor <init>(FF)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lhl$a;-><init>()V

    .line 48
    iput p1, p0, Lhh;->a:F

    .line 49
    iput p2, p0, Lhh;->b:F

    .line 50
    return-void
.end method

.method public static a(FF)Lhh;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lhh;->c:Lhl;

    invoke-virtual {v0}, Lhl;->a()Lhl$a;

    move-result-object v0

    check-cast v0, Lhh;

    .line 31
    iput p0, v0, Lhh;->a:F

    .line 32
    iput p1, v0, Lhh;->b:F

    .line 33
    return-object v0
.end method

.method public static a(Lhh;)V
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lhh;->c:Lhl;

    invoke-virtual {v0, p0}, Lhl;->a(Lhl$a;)V

    .line 38
    return-void
.end method


# virtual methods
.method protected a()Lhl$a;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 26
    new-instance v0, Lhh;

    invoke-direct {v0, v1, v1}, Lhh;-><init>(FF)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 54
    if-nez p1, :cond_1

    .line 64
    :cond_0
    :goto_0
    return v1

    .line 57
    :cond_1
    if-ne p0, p1, :cond_2

    move v1, v0

    .line 58
    goto :goto_0

    .line 60
    :cond_2
    instance-of v2, p1, Lhh;

    if-eqz v2, :cond_0

    .line 61
    check-cast p1, Lhh;

    .line 62
    iget v2, p0, Lhh;->a:F

    iget v3, p1, Lhh;->a:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lhh;->b:F

    iget v3, p1, Lhh;->b:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 77
    iget v0, p0, Lhh;->a:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    iget v1, p0, Lhh;->b:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lhh;->a:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lhh;->b:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
