.class public Lyf;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lala;
.implements Ljava/io/Closeable;


# instance fields
.field private final a:Lauj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lauj",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Lauj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lauj",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Lauj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lauj",
            "<",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/Tweet;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lauj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lauj",
            "<",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/Tweet;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lauj;Lauj;Lauj;Lauj;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lauj",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;>;",
            "Lauj",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;>;",
            "Lauj",
            "<",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/Tweet;",
            ">;>;",
            "Lauj",
            "<",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/Tweet;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lyf;->a:Lauj;

    .line 44
    iput-object p2, p0, Lyf;->b:Lauj;

    .line 45
    iput-object p3, p0, Lyf;->c:Lauj;

    .line 46
    iput-object p4, p0, Lyf;->d:Lauj;

    .line 47
    return-void
.end method

.method static synthetic a(Lyf;)Lauj;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lyf;->c:Lauj;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Lyf;
    .locals 6

    .prologue
    .line 51
    new-instance v0, Laes;

    new-instance v1, Lauh;

    .line 52
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, v2}, Lauh;-><init>(Landroid/content/ContentResolver;)V

    invoke-direct {v0, v1, p1}, Laes;-><init>(Lauj;Lcom/twitter/library/client/Session;)V

    .line 53
    new-instance v1, Lwu;

    new-instance v2, Lwv;

    invoke-direct {v2, p0, p1}, Lwv;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-direct {v1, v2, v0}, Lwu;-><init>(Lwv;Laes;)V

    .line 55
    new-instance v2, Lyf;

    new-instance v3, Laug;

    new-instance v4, Lwt;

    invoke-direct {v4, v1}, Lwt;-><init>(Lauj;)V

    invoke-direct {v3, v4}, Laug;-><init>(Lauj;)V

    new-instance v4, Laug;

    new-instance v5, Lwt;

    invoke-direct {v5, v0}, Lwt;-><init>(Lauj;)V

    invoke-direct {v4, v5}, Laug;-><init>(Lauj;)V

    invoke-direct {v2, v3, v4, v1, v0}, Lyf;-><init>(Lauj;Lauj;Lauj;Lauj;)V

    return-object v2
.end method


# virtual methods
.method public a(J)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/c",
            "<",
            "Lcom/twitter/util/collection/k",
            "<",
            "Lcom/twitter/model/core/Tweet;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lyf;->b:Lauj;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Lauj;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lrx/c;->k()Lrx/c;

    move-result-object v0

    iget-object v1, p0, Lyf;->a:Lauj;

    .line 69
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Lauj;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/c;->e(Lrx/c;)Lrx/c;

    move-result-object v0

    new-instance v1, Lyf$1;

    invoke-direct {v1, p0}, Lyf$1;-><init>(Lyf;)V

    .line 70
    invoke-virtual {v0, v1}, Lrx/c;->k(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 67
    return-object v0
.end method

.method public a(Ljava/lang/Iterable;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lrx/c",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/Tweet;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lyf;->d:Lauj;

    invoke-interface {v0, p1}, Lauj;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    invoke-virtual {v0}, Lrx/c;->k()Lrx/c;

    move-result-object v0

    new-instance v1, Lyf$2;

    invoke-direct {v1, p0, p1}, Lyf$2;-><init>(Lyf;Ljava/lang/Iterable;)V

    invoke-virtual {v0, v1}, Lrx/c;->f(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/app/common/di/scope/InjectionScope;)V
    .locals 1

    .prologue
    .line 103
    sget-object v0, Lcom/twitter/app/common/di/scope/InjectionScope;->c:Lcom/twitter/app/common/di/scope/InjectionScope;

    if-ne p1, v0, :cond_0

    .line 104
    invoke-static {p0}, Ltv/periscope/android/util/l;->a(Ljava/io/Closeable;)V

    .line 106
    :cond_0
    return-void
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lyf;->a:Lauj;

    invoke-interface {v0}, Lauj;->close()V

    .line 98
    iget-object v0, p0, Lyf;->b:Lauj;

    invoke-interface {v0}, Lauj;->close()V

    .line 99
    return-void
.end method
