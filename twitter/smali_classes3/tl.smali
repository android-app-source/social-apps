.class Ltl;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltl$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ltl",
            "<TT;>.a;>;"
        }
    .end annotation
.end field

.field private final c:Lti$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lti$a",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final d:J

.field private final e:J

.field private f:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Lti$a;JJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lti$a",
            "<TT;>;JJ)V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-static {}, Lcom/twitter/util/collection/MutableList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ltl;->a:Ljava/util/List;

    .line 28
    invoke-static {}, Lcom/twitter/util/collection/MutableMap;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Ltl;->b:Ljava/util/Map;

    .line 39
    iput-object p1, p0, Ltl;->c:Lti$a;

    .line 40
    iput-wide p2, p0, Ltl;->d:J

    .line 41
    iput-wide p4, p0, Ltl;->e:J

    .line 42
    return-void
.end method

.method static synthetic a(Ltl;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Ltl;->f:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic b(Ltl;)Lti$a;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Ltl;->c:Lti$a;

    return-object v0
.end method

.method static synthetic c(Ltl;)Ljava/util/List;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Ltl;->a:Ljava/util/List;

    return-object v0
.end method

.method static synthetic d(Ltl;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Ltl;->e:J

    return-wide v0
.end method

.method static synthetic e(Ltl;)J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Ltl;->d:J

    return-wide v0
.end method


# virtual methods
.method public a(JZ)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZ)",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/analytics/feature/model/TwitterScribeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Ltl;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltl$a;

    .line 76
    invoke-virtual {v0, p1, p2, p3}, Ltl$a;->a(JZ)V

    goto :goto_0

    .line 78
    :cond_0
    iget-object v0, p0, Ltl;->a:Ljava/util/List;

    return-object v0
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 49
    const v0, 0x7f13004c

    invoke-virtual {p1, v0, p2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 50
    iget-object v0, p0, Ltl;->c:Lti$a;

    invoke-interface {v0, p2}, Lti$a;->a(Ljava/lang/Object;)J

    move-result-wide v2

    .line 51
    iget-object v0, p0, Ltl;->b:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltl$a;

    .line 52
    if-nez v0, :cond_0

    .line 53
    new-instance v0, Ltl$a;

    invoke-direct {v0, p0, p1, p2}, Ltl$a;-><init>(Ltl;Landroid/view/View;Ljava/lang/Object;)V

    .line 54
    iget-object v1, p0, Ltl;->b:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    :goto_0
    invoke-static {}, Lcom/twitter/util/aa;->b()J

    move-result-wide v2

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v3, v1}, Ltl$a;->a(JZ)V

    .line 59
    return-void

    .line 56
    :cond_0
    invoke-virtual {v0, p1}, Ltl$a;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Ltl;->f:Landroid/view/ViewGroup;

    .line 46
    return-void
.end method
