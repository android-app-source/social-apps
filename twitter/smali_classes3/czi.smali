.class public Lczi;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/google/android/exoplayer/ExoPlayer$Listener;
.implements Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer$EventListener;
.implements Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$EventListener;
.implements Lcom/google/android/exoplayer/chunk/ChunkSampleSource$EventListener;
.implements Lcom/google/android/exoplayer/dash/DashChunkSource$EventListener;
.implements Lcom/google/android/exoplayer/drm/StreamingDrmSessionManager$EventListener;
.implements Lcom/google/android/exoplayer/hls/HlsSampleSource$EventListener;
.implements Lcom/google/android/exoplayer/metadata/MetadataTrackRenderer$MetadataRenderer;
.implements Lcom/google/android/exoplayer/text/TextRenderer;
.implements Lcom/google/android/exoplayer/upstream/BandwidthMeter$EventListener;
.implements Lcom/google/android/exoplayer/util/DebugTextViewHelper$Provider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lczi$b;,
        Lczi$a;,
        Lczi$c;,
        Lczi$d;,
        Lczi$e;,
        Lczi$f;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/exoplayer/ExoPlayer$Listener;",
        "Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer$EventListener;",
        "Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$EventListener;",
        "Lcom/google/android/exoplayer/chunk/ChunkSampleSource$EventListener;",
        "Lcom/google/android/exoplayer/dash/DashChunkSource$EventListener;",
        "Lcom/google/android/exoplayer/drm/StreamingDrmSessionManager$EventListener;",
        "Lcom/google/android/exoplayer/hls/HlsSampleSource$EventListener;",
        "Lcom/google/android/exoplayer/metadata/MetadataTrackRenderer$MetadataRenderer",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/exoplayer/metadata/id3/Id3Frame;",
        ">;>;",
        "Lcom/google/android/exoplayer/text/TextRenderer;",
        "Lcom/google/android/exoplayer/upstream/BandwidthMeter$EventListener;",
        "Lcom/google/android/exoplayer/util/DebugTextViewHelper$Provider;"
    }
.end annotation


# instance fields
.field private final a:Lczi$f;

.field private final b:Ldct;

.field private final c:Lcom/google/android/exoplayer/ExoPlayer;

.field private final d:Lcom/google/android/exoplayer/util/PlayerControl;

.field private final e:Landroid/os/Handler;

.field private final f:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lczi$e;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:I

.field private i:Z

.field private j:F

.field private k:Landroid/view/Surface;

.field private l:Lcom/google/android/exoplayer/TrackRenderer;

.field private m:Lcom/google/android/exoplayer/TrackRenderer;

.field private n:Lcom/google/android/exoplayer/CodecCounters;

.field private o:Lcom/google/android/exoplayer/chunk/Format;

.field private p:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

.field private q:Lczi$a;

.field private r:Lczi$b;

.field private s:Lczi$d;

.field private t:Lczi$c;


# direct methods
.method public constructor <init>(Lczi$f;)V
    .locals 1

    .prologue
    .line 216
    new-instance v0, Ldct;

    invoke-direct {v0}, Ldct;-><init>()V

    invoke-direct {p0, p1, v0}, Lczi;-><init>(Lczi$f;Ldct;)V

    .line 217
    return-void
.end method

.method public constructor <init>(Lczi$f;Ldct;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 220
    iput-object p1, p0, Lczi;->a:Lczi$f;

    .line 221
    iput-object p2, p0, Lczi;->b:Ldct;

    .line 222
    const/4 v0, 0x4

    const/16 v1, 0x3e8

    const/16 v2, 0x1388

    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer/ExoPlayer$Factory;->newInstance(III)Lcom/google/android/exoplayer/ExoPlayer;

    move-result-object v0

    iput-object v0, p0, Lczi;->c:Lcom/google/android/exoplayer/ExoPlayer;

    .line 223
    iget-object v0, p0, Lczi;->c:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v0, p0}, Lcom/google/android/exoplayer/ExoPlayer;->addListener(Lcom/google/android/exoplayer/ExoPlayer$Listener;)V

    .line 224
    new-instance v0, Lcom/google/android/exoplayer/util/PlayerControl;

    iget-object v1, p0, Lczi;->c:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/util/PlayerControl;-><init>(Lcom/google/android/exoplayer/ExoPlayer;)V

    iput-object v0, p0, Lczi;->d:Lcom/google/android/exoplayer/util/PlayerControl;

    .line 225
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lczi;->e:Landroid/os/Handler;

    .line 226
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lczi;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 227
    iput v3, p0, Lczi;->h:I

    .line 228
    iput v3, p0, Lczi;->g:I

    .line 230
    iget-object v0, p0, Lczi;->c:Lcom/google/android/exoplayer/ExoPlayer;

    const/4 v1, 0x2

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/exoplayer/ExoPlayer;->setSelectedTrack(II)V

    .line 231
    return-void
.end method

.method private b(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 644
    iget-object v0, p0, Lczi;->l:Lcom/google/android/exoplayer/TrackRenderer;

    if-nez v0, :cond_0

    .line 655
    :goto_0
    return-void

    .line 648
    :cond_0
    if-eqz p1, :cond_1

    .line 649
    iget-object v0, p0, Lczi;->c:Lcom/google/android/exoplayer/ExoPlayer;

    iget-object v1, p0, Lczi;->l:Lcom/google/android/exoplayer/TrackRenderer;

    iget-object v2, p0, Lczi;->k:Landroid/view/Surface;

    invoke-interface {v0, v1, v3, v2}, Lcom/google/android/exoplayer/ExoPlayer;->blockingSendMessage(Lcom/google/android/exoplayer/ExoPlayer$ExoPlayerComponent;ILjava/lang/Object;)V

    goto :goto_0

    .line 652
    :cond_1
    iget-object v0, p0, Lczi;->c:Lcom/google/android/exoplayer/ExoPlayer;

    iget-object v1, p0, Lczi;->l:Lcom/google/android/exoplayer/TrackRenderer;

    iget-object v2, p0, Lczi;->k:Landroid/view/Surface;

    invoke-interface {v0, v1, v3, v2}, Lcom/google/android/exoplayer/ExoPlayer;->sendMessage(Lcom/google/android/exoplayer/ExoPlayer$ExoPlayerComponent;ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private j()V
    .locals 4

    .prologue
    .line 632
    iget-object v0, p0, Lczi;->c:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v0}, Lcom/google/android/exoplayer/ExoPlayer;->getPlayWhenReady()Z

    move-result v1

    .line 633
    invoke-virtual {p0}, Lczi;->f()I

    move-result v2

    .line 634
    iget-boolean v0, p0, Lczi;->i:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lczi;->h:I

    if-eq v0, v2, :cond_2

    .line 635
    :cond_0
    iget-object v0, p0, Lczi;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lczi$e;

    .line 636
    invoke-interface {v0, v1, v2}, Lczi$e;->a(ZI)V

    goto :goto_0

    .line 638
    :cond_1
    iput-boolean v1, p0, Lczi;->i:Z

    .line 639
    iput v2, p0, Lczi;->h:I

    .line 641
    :cond_2
    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lczi;->c:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer/ExoPlayer;->getSelectedTrack(I)I

    move-result v0

    return v0
.end method

.method a()Ldct;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lczi;->b:Ldct;

    return-object v0
.end method

.method public a(F)V
    .locals 4

    .prologue
    .line 658
    iget-object v0, p0, Lczi;->m:Lcom/google/android/exoplayer/TrackRenderer;

    if-eqz v0, :cond_0

    .line 659
    iget-object v0, p0, Lczi;->c:Lcom/google/android/exoplayer/ExoPlayer;

    iget-object v1, p0, Lczi;->m:Lcom/google/android/exoplayer/TrackRenderer;

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/exoplayer/ExoPlayer;->sendMessage(Lcom/google/android/exoplayer/ExoPlayer$ExoPlayerComponent;ILjava/lang/Object;)V

    .line 661
    :cond_0
    iput p1, p0, Lczi;->j:F

    .line 662
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lczi;->c:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v0, p1, p2}, Lcom/google/android/exoplayer/ExoPlayer;->seekTo(J)V

    .line 390
    return-void
.end method

.method public a(Landroid/view/Surface;)V
    .locals 1

    .prologue
    .line 270
    iput-object p1, p0, Lczi;->k:Landroid/view/Surface;

    .line 271
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lczi;->b(Z)V

    .line 272
    return-void
.end method

.method public a(Lczi$a;)V
    .locals 0

    .prologue
    .line 262
    iput-object p1, p0, Lczi;->q:Lczi$a;

    .line 263
    return-void
.end method

.method public a(Lczi$b;)V
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lczi;->r:Lczi$b;

    .line 267
    return-void
.end method

.method public a(Lczi$c;)V
    .locals 0

    .prologue
    .line 258
    iput-object p1, p0, Lczi;->t:Lczi$c;

    .line 259
    return-void
.end method

.method public a(Lczi$d;)V
    .locals 0

    .prologue
    .line 254
    iput-object p1, p0, Lczi;->s:Lczi$d;

    .line 255
    return-void
.end method

.method public a(Lczi$e;)V
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lczi;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 247
    return-void
.end method

.method a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 374
    iget-object v0, p0, Lczi;->s:Lczi$d;

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Lczi;->s:Lczi$d;

    invoke-interface {v0, p1}, Lczi$d;->b(Ljava/lang/Exception;)V

    .line 377
    :cond_0
    iget-object v0, p0, Lczi;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lczi$e;

    .line 378
    invoke-interface {v0, p1}, Lczi$e;->a(Ljava/lang/Exception;)V

    goto :goto_0

    .line 380
    :cond_1
    const/4 v0, 0x1

    iput v0, p0, Lczi;->g:I

    .line 381
    invoke-direct {p0}, Lczi;->j()V

    .line 382
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer/metadata/id3/Id3Frame;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 579
    iget-object v0, p0, Lczi;->r:Lczi$b;

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lczi;->a(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 580
    iget-object v0, p0, Lczi;->r:Lczi$b;

    invoke-interface {v0, p1}, Lczi$b;->a(Ljava/util/List;)V

    .line 582
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lczi;->c:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer/ExoPlayer;->setPlayWhenReady(Z)V

    .line 386
    return-void
.end method

.method a([Lcom/google/android/exoplayer/TrackRenderer;Lcom/google/android/exoplayer/upstream/BandwidthMeter;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 345
    move v0, v1

    :goto_0
    const/4 v2, 0x4

    if-ge v0, v2, :cond_1

    .line 346
    aget-object v2, p1, v0

    if-nez v2, :cond_0

    .line 348
    new-instance v2, Lcom/google/android/exoplayer/DummyTrackRenderer;

    invoke-direct {v2}, Lcom/google/android/exoplayer/DummyTrackRenderer;-><init>()V

    aput-object v2, p1, v0

    .line 345
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 352
    :cond_1
    aget-object v0, p1, v1

    iput-object v0, p0, Lczi;->l:Lcom/google/android/exoplayer/TrackRenderer;

    .line 353
    aget-object v0, p1, v3

    iput-object v0, p0, Lczi;->m:Lcom/google/android/exoplayer/TrackRenderer;

    .line 354
    iget-object v0, p0, Lczi;->l:Lcom/google/android/exoplayer/TrackRenderer;

    instance-of v0, v0, Lcom/google/android/exoplayer/MediaCodecTrackRenderer;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lczi;->l:Lcom/google/android/exoplayer/TrackRenderer;

    check-cast v0, Lcom/google/android/exoplayer/MediaCodecTrackRenderer;

    iget-object v0, v0, Lcom/google/android/exoplayer/MediaCodecTrackRenderer;->codecCounters:Lcom/google/android/exoplayer/CodecCounters;

    :goto_1
    iput-object v0, p0, Lczi;->n:Lcom/google/android/exoplayer/CodecCounters;

    .line 358
    iput-object p2, p0, Lczi;->p:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

    .line 359
    invoke-direct {p0, v1}, Lczi;->b(Z)V

    .line 360
    iget-object v0, p0, Lczi;->c:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer/ExoPlayer;->prepare([Lcom/google/android/exoplayer/TrackRenderer;)V

    .line 361
    const/4 v0, 0x3

    iput v0, p0, Lczi;->g:I

    .line 363
    iget v0, p0, Lczi;->j:F

    cmpl-float v0, v0, v4

    if-nez v0, :cond_2

    .line 364
    invoke-virtual {p0, v4}, Lczi;->a(F)V

    .line 366
    :cond_2
    return-void

    .line 354
    :cond_3
    aget-object v0, p1, v3

    instance-of v0, v0, Lcom/google/android/exoplayer/MediaCodecTrackRenderer;

    if-eqz v0, :cond_4

    aget-object v0, p1, v3

    check-cast v0, Lcom/google/android/exoplayer/MediaCodecTrackRenderer;

    iget-object v0, v0, Lcom/google/android/exoplayer/MediaCodecTrackRenderer;->codecCounters:Lcom/google/android/exoplayer/CodecCounters;

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public b()Lcom/google/android/exoplayer/ExoPlayer;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lczi;->c:Lcom/google/android/exoplayer/ExoPlayer;

    return-object v0
.end method

.method public b(Lczi$e;)V
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lczi;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 251
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 321
    iget v0, p0, Lczi;->g:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 322
    iget-object v0, p0, Lczi;->c:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v0}, Lcom/google/android/exoplayer/ExoPlayer;->stop()V

    .line 324
    :cond_0
    iget-object v0, p0, Lczi;->a:Lczi$f;

    invoke-interface {v0}, Lczi$f;->a()V

    .line 325
    iput-object v2, p0, Lczi;->o:Lcom/google/android/exoplayer/chunk/Format;

    .line 326
    iput-object v2, p0, Lczi;->l:Lcom/google/android/exoplayer/TrackRenderer;

    .line 327
    iput-object v2, p0, Lczi;->m:Lcom/google/android/exoplayer/TrackRenderer;

    .line 328
    const/4 v0, 0x2

    iput v0, p0, Lczi;->g:I

    .line 329
    invoke-direct {p0}, Lczi;->j()V

    .line 330
    iget-object v0, p0, Lczi;->a:Lczi$f;

    invoke-interface {v0, p0}, Lczi$f;->a(Lczi;)V

    .line 331
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lczi;->c:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v0}, Lcom/google/android/exoplayer/ExoPlayer;->stop()V

    .line 335
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lczi;->a:Lczi$f;

    invoke-interface {v0}, Lczi$f;->a()V

    .line 394
    const/4 v0, 0x1

    iput v0, p0, Lczi;->g:I

    .line 395
    const/4 v0, 0x0

    iput-object v0, p0, Lczi;->k:Landroid/view/Surface;

    .line 396
    iget-object v0, p0, Lczi;->c:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v0}, Lcom/google/android/exoplayer/ExoPlayer;->release()V

    .line 397
    return-void
.end method

.method public f()I
    .locals 4

    .prologue
    const/4 v0, 0x2

    .line 400
    iget v1, p0, Lczi;->g:I

    if-ne v1, v0, :cond_1

    .line 409
    :cond_0
    :goto_0
    return v0

    .line 403
    :cond_1
    iget-object v1, p0, Lczi;->c:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v1}, Lcom/google/android/exoplayer/ExoPlayer;->getPlaybackState()I

    move-result v1

    .line 404
    iget v2, p0, Lczi;->g:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    :cond_2
    move v0, v1

    .line 409
    goto :goto_0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 433
    iget-object v0, p0, Lczi;->c:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v0}, Lcom/google/android/exoplayer/ExoPlayer;->getDuration()J

    move-result-wide v0

    return-wide v0
.end method

.method public getBandwidthMeter()Lcom/google/android/exoplayer/upstream/BandwidthMeter;
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lczi;->p:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

    return-object v0
.end method

.method public getCodecCounters()Lcom/google/android/exoplayer/CodecCounters;
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lczi;->n:Lcom/google/android/exoplayer/CodecCounters;

    return-object v0
.end method

.method public getCurrentPosition()J
    .locals 2

    .prologue
    .line 429
    iget-object v0, p0, Lczi;->c:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v0}, Lcom/google/android/exoplayer/ExoPlayer;->getCurrentPosition()J

    move-result-wide v0

    return-wide v0
.end method

.method public getFormat()Lcom/google/android/exoplayer/chunk/Format;
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lczi;->o:Lcom/google/android/exoplayer/chunk/Format;

    return-object v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lczi;->c:Lcom/google/android/exoplayer/ExoPlayer;

    invoke-interface {v0}, Lcom/google/android/exoplayer/ExoPlayer;->getPlayWhenReady()Z

    move-result v0

    return v0
.end method

.method i()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lczi;->e:Landroid/os/Handler;

    return-object v0
.end method

.method public onAudioTrackInitializationError(Lcom/google/android/exoplayer/audio/AudioTrack$InitializationException;)V
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lczi;->s:Lczi$d;

    if-eqz v0, :cond_0

    .line 530
    iget-object v0, p0, Lczi;->s:Lczi$d;

    invoke-interface {v0, p1}, Lczi$d;->a(Lcom/google/android/exoplayer/audio/AudioTrack$InitializationException;)V

    .line 532
    :cond_0
    return-void
.end method

.method public onAudioTrackUnderrun(IJJ)V
    .locals 6

    .prologue
    .line 543
    iget-object v0, p0, Lczi;->s:Lczi$d;

    if-eqz v0, :cond_0

    .line 544
    iget-object v0, p0, Lczi;->s:Lczi$d;

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lczi$d;->b(IJJ)V

    .line 546
    :cond_0
    return-void
.end method

.method public onAudioTrackWriteError(Lcom/google/android/exoplayer/audio/AudioTrack$WriteException;)V
    .locals 1

    .prologue
    .line 536
    iget-object v0, p0, Lczi;->s:Lczi$d;

    if-eqz v0, :cond_0

    .line 537
    iget-object v0, p0, Lczi;->s:Lczi$d;

    invoke-interface {v0, p1}, Lczi$d;->a(Lcom/google/android/exoplayer/audio/AudioTrack$WriteException;)V

    .line 539
    :cond_0
    return-void
.end method

.method public onAvailableRangeChanged(ILcom/google/android/exoplayer/TimeRange;)V
    .locals 1

    .prologue
    .line 586
    iget-object v0, p0, Lczi;->t:Lczi$c;

    if-eqz v0, :cond_0

    .line 587
    iget-object v0, p0, Lczi;->t:Lczi$c;

    invoke-interface {v0, p1, p2}, Lczi$c;->a(ILcom/google/android/exoplayer/TimeRange;)V

    .line 589
    :cond_0
    return-void
.end method

.method public onBandwidthSample(IJJ)V
    .locals 6

    .prologue
    .line 489
    iget-object v0, p0, Lczi;->t:Lczi$c;

    if-eqz v0, :cond_0

    .line 490
    iget-object v0, p0, Lczi;->t:Lczi$c;

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lczi$c;->a(IJJ)V

    .line 492
    :cond_0
    return-void
.end method

.method public onCryptoError(Landroid/media/MediaCodec$CryptoException;)V
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, Lczi;->s:Lczi$d;

    if-eqz v0, :cond_0

    .line 551
    iget-object v0, p0, Lczi;->s:Lczi$d;

    invoke-interface {v0, p1}, Lczi$d;->a(Landroid/media/MediaCodec$CryptoException;)V

    .line 553
    :cond_0
    return-void
.end method

.method public onCues(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer/text/Cue;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 572
    iget-object v0, p0, Lczi;->q:Lczi$a;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lczi;->a(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 573
    iget-object v0, p0, Lczi;->q:Lczi$a;

    invoke-interface {v0, p1}, Lczi$a;->b(Ljava/util/List;)V

    .line 575
    :cond_0
    return-void
.end method

.method public onDecoderInitializationError(Lcom/google/android/exoplayer/MediaCodecTrackRenderer$DecoderInitializationException;)V
    .locals 1

    .prologue
    .line 522
    iget-object v0, p0, Lczi;->s:Lczi$d;

    if-eqz v0, :cond_0

    .line 523
    iget-object v0, p0, Lczi;->s:Lczi$d;

    invoke-interface {v0, p1}, Lczi$d;->a(Lcom/google/android/exoplayer/MediaCodecTrackRenderer$DecoderInitializationException;)V

    .line 525
    :cond_0
    return-void
.end method

.method public onDecoderInitialized(Ljava/lang/String;JJ)V
    .locals 6

    .prologue
    .line 558
    iget-object v0, p0, Lczi;->t:Lczi$c;

    if-eqz v0, :cond_0

    .line 559
    iget-object v0, p0, Lczi;->t:Lczi$c;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lczi$c;->a(Ljava/lang/String;JJ)V

    .line 561
    :cond_0
    return-void
.end method

.method public onDownstreamFormatChanged(ILcom/google/android/exoplayer/chunk/Format;IJ)V
    .locals 2

    .prologue
    .line 497
    iget-object v0, p0, Lczi;->t:Lczi$c;

    if-nez v0, :cond_1

    .line 506
    :cond_0
    :goto_0
    return-void

    .line 500
    :cond_1
    if-nez p1, :cond_2

    .line 501
    iput-object p2, p0, Lczi;->o:Lcom/google/android/exoplayer/chunk/Format;

    .line 502
    iget-object v0, p0, Lczi;->t:Lczi$c;

    invoke-interface {v0, p2, p3, p4, p5}, Lczi$c;->a(Lcom/google/android/exoplayer/chunk/Format;IJ)V

    goto :goto_0

    .line 503
    :cond_2
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 504
    iget-object v0, p0, Lczi;->t:Lczi$c;

    invoke-interface {v0, p2, p3, p4, p5}, Lczi$c;->b(Lcom/google/android/exoplayer/chunk/Format;IJ)V

    goto :goto_0
.end method

.method public onDrawnToSurface(Landroid/view/Surface;)V
    .locals 1

    .prologue
    .line 598
    iget-object v0, p0, Lczi;->t:Lczi$c;

    if-eqz v0, :cond_0

    .line 599
    iget-object v0, p0, Lczi;->t:Lczi$c;

    invoke-interface {v0, p1}, Lczi$c;->a(Landroid/view/Surface;)V

    .line 601
    :cond_0
    return-void
.end method

.method public onDrmKeysLoaded()V
    .locals 0

    .prologue
    .line 511
    return-void
.end method

.method public onDrmSessionManagerError(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 515
    iget-object v0, p0, Lczi;->s:Lczi$d;

    if-eqz v0, :cond_0

    .line 516
    iget-object v0, p0, Lczi;->s:Lczi$d;

    invoke-interface {v0, p1}, Lczi$d;->c(Ljava/lang/Exception;)V

    .line 518
    :cond_0
    return-void
.end method

.method public onDroppedFrames(IJ)V
    .locals 2

    .prologue
    .line 482
    iget-object v0, p0, Lczi;->t:Lczi$c;

    if-eqz v0, :cond_0

    .line 483
    iget-object v0, p0, Lczi;->t:Lczi$c;

    invoke-interface {v0, p1, p2, p3}, Lczi$c;->a(IJ)V

    .line 485
    :cond_0
    return-void
.end method

.method public onLoadCanceled(IJ)V
    .locals 0

    .prologue
    .line 624
    return-void
.end method

.method public onLoadCompleted(IJIILcom/google/android/exoplayer/chunk/Format;JJJJ)V
    .locals 19

    .prologue
    .line 615
    move-object/from16 v0, p0

    iget-object v2, v0, Lczi;->t:Lczi$c;

    if-eqz v2, :cond_0

    .line 616
    move-object/from16 v0, p0

    iget-object v3, v0, Lczi;->t:Lczi$c;

    move/from16 v4, p1

    move-wide/from16 v5, p2

    move/from16 v7, p4

    move/from16 v8, p5

    move-object/from16 v9, p6

    move-wide/from16 v10, p7

    move-wide/from16 v12, p9

    move-wide/from16 v14, p11

    move-wide/from16 v16, p13

    invoke-interface/range {v3 .. v17}, Lczi$c;->a(IJIILcom/google/android/exoplayer/chunk/Format;JJJJ)V

    .line 619
    :cond_0
    return-void
.end method

.method public onLoadError(ILjava/io/IOException;)V
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lczi;->s:Lczi$d;

    if-eqz v0, :cond_0

    .line 566
    iget-object v0, p0, Lczi;->s:Lczi$d;

    invoke-interface {v0, p1, p2}, Lczi$d;->a(ILjava/io/IOException;)V

    .line 568
    :cond_0
    return-void
.end method

.method public onLoadStarted(IJIILcom/google/android/exoplayer/chunk/Format;JJ)V
    .locals 13

    .prologue
    .line 606
    iget-object v0, p0, Lczi;->t:Lczi$c;

    if-eqz v0, :cond_0

    .line 607
    iget-object v1, p0, Lczi;->t:Lczi$c;

    move v2, p1

    move-wide v3, p2

    move/from16 v5, p4

    move/from16 v6, p5

    move-object/from16 v7, p6

    move-wide/from16 v8, p7

    move-wide/from16 v10, p9

    invoke-interface/range {v1 .. v11}, Lczi$c;->a(IJIILcom/google/android/exoplayer/chunk/Format;JJ)V

    .line 610
    :cond_0
    return-void
.end method

.method public synthetic onMetadata(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 61
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lczi;->a(Ljava/util/List;)V

    return-void
.end method

.method public onPlayWhenReadyCommitted()V
    .locals 0

    .prologue
    .line 594
    return-void
.end method

.method public onPlayerError(Lcom/google/android/exoplayer/ExoPlaybackException;)V
    .locals 2

    .prologue
    .line 459
    const/4 v0, 0x1

    iput v0, p0, Lczi;->g:I

    .line 460
    iget-object v0, p0, Lczi;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lczi$e;

    .line 461
    invoke-interface {v0, p1}, Lczi$e;->a(Ljava/lang/Exception;)V

    goto :goto_0

    .line 463
    :cond_0
    return-void
.end method

.method public onPlayerSeekComplete()V
    .locals 2

    .prologue
    .line 467
    iget-object v0, p0, Lczi;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lczi$e;

    .line 468
    invoke-interface {v0}, Lczi$e;->c()V

    goto :goto_0

    .line 470
    :cond_0
    return-void
.end method

.method public onPlayerStateChanged(ZI)V
    .locals 0

    .prologue
    .line 454
    invoke-direct {p0}, Lczi;->j()V

    .line 455
    return-void
.end method

.method public onUpstreamDiscarded(IJJ)V
    .locals 0

    .prologue
    .line 629
    return-void
.end method

.method public onVideoSizeChanged(IIIF)V
    .locals 2

    .prologue
    .line 475
    iget-object v0, p0, Lczi;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lczi$e;

    .line 476
    invoke-interface {v0, p1, p2, p3, p4}, Lczi$e;->a(IIIF)V

    goto :goto_0

    .line 478
    :cond_0
    return-void
.end method
