.class public Lczh;
.super Lczg;
.source "Twttr"


# static fields
.field private static final c:[B


# instance fields
.field private d:Lczk$a;

.field private e:I

.field private f:I

.field private g:I

.field private h:[B

.field private i:[B

.field private j:Lcom/google/android/exoplayer/MediaFormat;

.field private final k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lczh;->c:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data
.end method

.method public constructor <init>(I[B[BIILczk$a;Lczg$a;)V
    .locals 1

    .prologue
    .line 34
    const-string/jumbo v0, "video/avc"

    invoke-direct {p0, p1, v0, p7}, Lczg;-><init>(ILjava/lang/String;Lczg$a;)V

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lczh;->g:I

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lczh;->j:Lcom/google/android/exoplayer/MediaFormat;

    .line 30
    const/4 v0, 0x4

    iput v0, p0, Lczh;->k:I

    .line 35
    iput-object p6, p0, Lczh;->d:Lczk$a;

    .line 36
    iput p4, p0, Lczh;->e:I

    .line 37
    iput p5, p0, Lczh;->f:I

    .line 38
    iput-object p2, p0, Lczh;->h:[B

    .line 39
    iput-object p3, p0, Lczh;->i:[B

    .line 40
    return-void
.end method

.method public static a([BII)J
    .locals 6

    .prologue
    .line 122
    const-wide/16 v2, 0x0

    .line 123
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    .line 125
    const/16 v1, 0x8

    shl-long/2addr v2, v1

    add-int v1, p1, v0

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    int-to-long v4, v1

    or-long/2addr v2, v4

    .line 123
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 127
    :cond_0
    return-wide v2
.end method


# virtual methods
.method public b()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lczh;->g:I

    return v0
.end method

.method public readData(IJLcom/google/android/exoplayer/MediaFormatHolder;Lcom/google/android/exoplayer/SampleHolder;)I
    .locals 9

    .prologue
    .line 53
    iget-object v0, p0, Lczh;->j:Lcom/google/android/exoplayer/MediaFormat;

    if-nez v0, :cond_0

    .line 55
    iget-object v0, p0, Lczh;->h:[B

    array-length v0, v0

    sget-object v1, Lczh;->c:[B

    array-length v1, v1

    add-int/2addr v0, v1

    new-array v0, v0, [B

    .line 56
    sget-object v1, Lczh;->c:[B

    const/4 v2, 0x0

    const/4 v3, 0x0

    sget-object v4, Lczh;->c:[B

    array-length v4, v4

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 57
    iget-object v1, p0, Lczh;->h:[B

    const/4 v2, 0x0

    sget-object v3, Lczh;->c:[B

    array-length v3, v3

    iget-object v4, p0, Lczh;->h:[B

    array-length v4, v4

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 58
    iget-object v1, p0, Lczh;->i:[B

    array-length v1, v1

    sget-object v2, Lczh;->c:[B

    array-length v2, v2

    add-int/2addr v1, v2

    new-array v1, v1, [B

    .line 59
    sget-object v2, Lczh;->c:[B

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Lczh;->c:[B

    array-length v5, v5

    invoke-static {v2, v3, v1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 60
    iget-object v2, p0, Lczh;->i:[B

    const/4 v3, 0x0

    sget-object v4, Lczh;->c:[B

    array-length v4, v4

    iget-object v5, p0, Lczh;->i:[B

    array-length v5, v5

    invoke-static {v2, v3, v1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 62
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 63
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    iget v0, p0, Lczh;->b:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "video/avc"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-wide/16 v4, -0x1

    iget v6, p0, Lczh;->e:I

    iget v7, p0, Lczh;->f:I

    invoke-static/range {v0 .. v8}, Lcom/google/android/exoplayer/MediaFormat;->createVideoFormat(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;)Lcom/google/android/exoplayer/MediaFormat;

    move-result-object v0

    iput-object v0, p0, Lczh;->j:Lcom/google/android/exoplayer/MediaFormat;

    .line 68
    iget-object v0, p0, Lczh;->j:Lcom/google/android/exoplayer/MediaFormat;

    iput-object v0, p4, Lcom/google/android/exoplayer/MediaFormatHolder;->format:Lcom/google/android/exoplayer/MediaFormat;

    .line 69
    const/4 v0, -0x4

    .line 118
    :goto_0
    return v0

    .line 72
    :cond_0
    invoke-virtual {p0}, Lczh;->a()Lczg$b;

    move-result-object v2

    .line 73
    if-nez v2, :cond_1

    .line 75
    const/4 v0, -0x2

    goto :goto_0

    .line 77
    :cond_1
    invoke-virtual {v2}, Lczg$b;->b()[B

    move-result-object v3

    .line 79
    iget-object v0, p5, Lcom/google/android/exoplayer/SampleHolder;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    array-length v1, v3

    if-ge v0, v1, :cond_2

    .line 81
    array-length v0, v3

    invoke-virtual {p5, v0}, Lcom/google/android/exoplayer/SampleHolder;->ensureSpaceForWrite(I)V

    .line 84
    :cond_2
    const/4 v1, 0x0

    .line 85
    const/4 v0, 0x0

    .line 86
    :cond_3
    :goto_1
    array-length v4, v3

    add-int/lit8 v4, v4, -0x4

    if-ge v0, v4, :cond_4

    .line 88
    const/4 v4, 0x4

    invoke-static {v3, v0, v4}, Lczh;->a([BII)J

    move-result-wide v4

    long-to-int v4, v4

    .line 89
    add-int/lit8 v0, v0, 0x4

    .line 90
    add-int v5, v0, v4

    array-length v6, v3

    if-gt v5, v6, :cond_3

    .line 91
    aget-byte v5, v3, v0

    and-int/lit8 v5, v5, 0x1f

    .line 92
    packed-switch v5, :pswitch_data_0

    .line 103
    :goto_2
    iget-object v5, p5, Lcom/google/android/exoplayer/SampleHolder;->data:Ljava/nio/ByteBuffer;

    sget-object v6, Lczh;->c:[B

    const/4 v7, 0x0

    sget-object v8, Lczh;->c:[B

    array-length v8, v8

    invoke-virtual {v5, v6, v7, v8}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 104
    iget-object v5, p5, Lcom/google/android/exoplayer/SampleHolder;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v3, v0, v4}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 105
    add-int/2addr v0, v4

    goto :goto_1

    .line 95
    :pswitch_0
    const/4 v1, 0x1

    .line 96
    goto :goto_2

    .line 108
    :cond_4
    if-eqz v1, :cond_5

    .line 110
    monitor-enter p0

    .line 112
    :try_start_0
    iget v0, p0, Lczh;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lczh;->g:I

    .line 113
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    :cond_5
    if-eqz v1, :cond_6

    const/4 v0, 0x1

    :goto_3
    iput v0, p5, Lcom/google/android/exoplayer/SampleHolder;->flags:I

    .line 116
    array-length v0, v3

    iput v0, p5, Lcom/google/android/exoplayer/SampleHolder;->size:I

    .line 117
    invoke-virtual {v2}, Lczg$b;->a()D

    move-result-wide v0

    const-wide v2, 0x408f400000000000L    # 1000.0

    mul-double/2addr v0, v2

    const-wide v2, 0x408f400000000000L    # 1000.0

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p5, Lcom/google/android/exoplayer/SampleHolder;->timeUs:J

    .line 118
    const/4 v0, -0x3

    goto :goto_0

    .line 113
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 115
    :cond_6
    const/4 v0, 0x0

    goto :goto_3

    .line 92
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method

.method public readDiscontinuity(I)J
    .locals 2

    .prologue
    .line 48
    const-wide/high16 v0, -0x8000000000000000L

    return-wide v0
.end method
