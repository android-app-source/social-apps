.class public Lczf;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lczg$a;
.implements Lczi$f;
.implements Ltv/periscope/android/video/lhls/HTTPRequest$HTTPRequestDelegate;
.implements Ltv/periscope/android/video/lhls/LHLSPlayer$LHLSPlayerListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lczf$a;
    }
.end annotation


# instance fields
.field private A:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private B:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ltv/periscope/android/video/lhls/HTTPRequest;",
            ">;"
        }
    .end annotation
.end field

.field private volatile C:Z

.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private c:Lczi;

.field private d:Lczk$a;

.field private e:Ljava/lang/String;

.field private f:Ljava/util/Timer;

.field private g:Z

.field private h:J

.field private i:J

.field private j:J

.field private k:J

.field private l:J

.field private m:J

.field private n:J

.field private o:J

.field private p:J

.field private q:Ltv/periscope/android/video/lhls/LHLSPlayer;

.field private r:Lczh;

.field private s:Lcze;

.field private t:Ltv/periscope/android/video/rtmp/i;

.field private u:Ltv/periscope/android/video/rtmp/i;

.field private v:Ltv/periscope/android/video/rtmp/i;

.field private w:Ltv/periscope/android/video/rtmp/i;

.field private x:Ltv/periscope/android/video/rtmp/i;

.field private y:J

.field private z:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lczk$a;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-boolean v1, p0, Lczf;->g:Z

    .line 53
    iput-wide v2, p0, Lczf;->i:J

    .line 54
    iput-wide v2, p0, Lczf;->j:J

    .line 55
    iput-wide v2, p0, Lczf;->k:J

    .line 56
    iput-wide v2, p0, Lczf;->l:J

    .line 57
    iput-wide v2, p0, Lczf;->m:J

    .line 58
    iput-wide v2, p0, Lczf;->n:J

    .line 68
    iput-wide v2, p0, Lczf;->o:J

    .line 69
    iput-wide v2, p0, Lczf;->p:J

    .line 71
    iput-object v0, p0, Lczf;->q:Ltv/periscope/android/video/lhls/LHLSPlayer;

    .line 72
    iput-object v0, p0, Lczf;->r:Lczh;

    .line 73
    iput-object v0, p0, Lczf;->s:Lcze;

    .line 75
    new-instance v0, Ltv/periscope/android/video/rtmp/i;

    invoke-direct {v0}, Ltv/periscope/android/video/rtmp/i;-><init>()V

    iput-object v0, p0, Lczf;->t:Ltv/periscope/android/video/rtmp/i;

    .line 76
    new-instance v0, Ltv/periscope/android/video/rtmp/i;

    invoke-direct {v0}, Ltv/periscope/android/video/rtmp/i;-><init>()V

    iput-object v0, p0, Lczf;->u:Ltv/periscope/android/video/rtmp/i;

    .line 77
    new-instance v0, Ltv/periscope/android/video/rtmp/i;

    invoke-direct {v0}, Ltv/periscope/android/video/rtmp/i;-><init>()V

    iput-object v0, p0, Lczf;->v:Ltv/periscope/android/video/rtmp/i;

    .line 78
    new-instance v0, Ltv/periscope/android/video/rtmp/i;

    invoke-direct {v0}, Ltv/periscope/android/video/rtmp/i;-><init>()V

    iput-object v0, p0, Lczf;->w:Ltv/periscope/android/video/rtmp/i;

    .line 79
    new-instance v0, Ltv/periscope/android/video/rtmp/i;

    invoke-direct {v0}, Ltv/periscope/android/video/rtmp/i;-><init>()V

    iput-object v0, p0, Lczf;->x:Ltv/periscope/android/video/rtmp/i;

    .line 80
    iput-wide v2, p0, Lczf;->y:J

    .line 81
    iput-wide v2, p0, Lczf;->z:J

    .line 82
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lczf;->A:Ljava/util/HashMap;

    .line 83
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lczf;->B:Ljava/util/Vector;

    .line 84
    iput-boolean v1, p0, Lczf;->C:Z

    .line 126
    iput-object p1, p0, Lczf;->a:Landroid/content/Context;

    .line 127
    iput-object p3, p0, Lczf;->e:Ljava/lang/String;

    .line 128
    iput-object p4, p0, Lczf;->d:Lczk$a;

    .line 129
    iput-object p2, p0, Lczf;->b:Ljava/lang/String;

    .line 130
    return-void
.end method

.method static synthetic a(Lczf;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lczf;->j()V

    return-void
.end method

.method static synthetic a(Lczf;J)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lczf;->b(J)V

    return-void
.end method

.method private declared-synchronized a(Z)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 317
    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    iput-wide v0, p0, Lczf;->h:J

    .line 318
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lczf;->m:J

    .line 319
    iget-wide v0, p0, Lczf;->j:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 321
    iget-wide v0, p0, Lczf;->m:J

    iput-wide v0, p0, Lczf;->j:J

    .line 323
    :cond_0
    if-eqz p1, :cond_2

    .line 325
    iget-wide v0, p0, Lczf;->y:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 327
    iget-wide v0, p0, Lczf;->m:J

    iget-wide v2, p0, Lczf;->y:J

    sub-long/2addr v0, v2

    long-to-double v0, v0

    .line 328
    iget-object v2, p0, Lczf;->v:Ltv/periscope/android/video/rtmp/i;

    invoke-virtual {v2, v0, v1}, Ltv/periscope/android/video/rtmp/i;->a(D)V

    .line 330
    :cond_1
    iget-wide v0, p0, Lczf;->m:J

    iput-wide v0, p0, Lczf;->y:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 341
    :goto_0
    monitor-exit p0

    return-void

    .line 334
    :cond_2
    :try_start_1
    iget-wide v0, p0, Lczf;->z:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    .line 336
    iget-wide v0, p0, Lczf;->m:J

    iget-wide v2, p0, Lczf;->z:J

    sub-long/2addr v0, v2

    long-to-double v0, v0

    .line 337
    iget-object v2, p0, Lczf;->u:Ltv/periscope/android/video/rtmp/i;

    invoke-virtual {v2, v0, v1}, Ltv/periscope/android/video/rtmp/i;->a(D)V

    .line 339
    :cond_3
    iget-wide v0, p0, Lczf;->m:J

    iput-wide v0, p0, Lczf;->z:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 317
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(J)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 438
    monitor-enter p0

    .line 440
    :try_start_0
    iget-wide v0, p0, Lczf;->o:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lczf;->o:J

    .line 441
    iget-wide v0, p0, Lczf;->l:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 443
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lczf;->l:J

    .line 445
    const-string/jumbo v0, "LHLS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Start to first packet: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lczf;->j:J

    iget-wide v4, p0, Lczf;->i:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    const-string/jumbo v0, "LHLS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Start to first frame: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lczf;->l:J

    iget-wide v4, p0, Lczf;->i:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    iget-object v0, p0, Lczf;->A:Ljava/util/HashMap;

    const-string/jumbo v1, "RtmpConnectSuccess"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 450
    invoke-direct {p0}, Lczf;->l()V

    .line 452
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lczf;->p:J

    .line 453
    monitor-exit p0

    .line 454
    return-void

    .line 453
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private e()V
    .locals 4

    .prologue
    .line 133
    iget-object v0, p0, Lczf;->A:Ljava/util/HashMap;

    const-string/jumbo v1, "RtmpConnectSuccess"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    iget-object v0, p0, Lczf;->A:Ljava/util/HashMap;

    const-string/jumbo v1, "RtmpConnectTime"

    const-wide/16 v2, -0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 164
    monitor-enter p0

    .line 166
    :try_start_0
    iget-boolean v0, p0, Lczf;->g:Z

    if-eqz v0, :cond_0

    .line 168
    monitor-exit p0

    .line 174
    :goto_0
    return-void

    .line 170
    :cond_0
    const-string/jumbo v0, "LHLS"

    const-string/jumbo v1, "Requesting reconnect"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const/4 v0, 0x1

    iput-boolean v0, p0, Lczf;->g:Z

    .line 172
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    iget-object v0, p0, Lczf;->d:Lczk$a;

    invoke-interface {v0}, Lczk$a;->b()V

    goto :goto_0

    .line 172
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private g()V
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    .line 284
    iget-object v1, p0, Lczf;->r:Lczh;

    if-nez v1, :cond_0

    .line 287
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Stream with no video encountered: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lczf;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lf;->a(Ljava/lang/String;)V

    move-object v1, v0

    .line 302
    :goto_0
    iget-object v2, p0, Lczf;->s:Lcze;

    if-nez v2, :cond_1

    .line 304
    new-instance v2, Ljava/lang/Exception;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Stream with no audio encountered: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lczf;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lf;->a(Ljava/lang/Throwable;)V

    .line 309
    :goto_1
    const/4 v2, 0x4

    new-array v2, v2, [Lcom/google/android/exoplayer/TrackRenderer;

    .line 310
    const/4 v3, 0x0

    aput-object v1, v2, v3

    .line 311
    aput-object v0, v2, v5

    .line 312
    iget-object v0, p0, Lczf;->c:Lczi;

    new-instance v1, Lcom/google/android/exoplayer/upstream/DefaultBandwidthMeter;

    invoke-direct {v1}, Lcom/google/android/exoplayer/upstream/DefaultBandwidthMeter;-><init>()V

    invoke-virtual {v0, v2, v1}, Lczi;->a([Lcom/google/android/exoplayer/TrackRenderer;Lcom/google/android/exoplayer/upstream/BandwidthMeter;)V

    .line 313
    return-void

    .line 291
    :cond_0
    new-instance v1, Lczf$a;

    iget-object v3, p0, Lczf;->a:Landroid/content/Context;

    iget-object v4, p0, Lczf;->r:Lczh;

    const-wide/16 v6, 0x1388

    iget-object v2, p0, Lczf;->c:Lczi;

    .line 296
    invoke-virtual {v2}, Lczi;->i()Landroid/os/Handler;

    move-result-object v8

    iget-object v9, p0, Lczf;->c:Lczi;

    const/16 v10, 0x32

    move-object v2, p0

    invoke-direct/range {v1 .. v10}, Lczf$a;-><init>(Lczf;Landroid/content/Context;Lcom/google/android/exoplayer/SampleSource;IJLandroid/os/Handler;Lcom/google/android/exoplayer/MediaCodecVideoTrackRenderer$EventListener;I)V

    goto :goto_0

    .line 306
    :cond_1
    new-instance v0, Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer;

    iget-object v2, p0, Lczf;->s:Lcze;

    sget-object v3, Lcom/google/android/exoplayer/MediaCodecSelector;->DEFAULT:Lcom/google/android/exoplayer/MediaCodecSelector;

    invoke-direct {v0, v2, v3}, Lcom/google/android/exoplayer/MediaCodecAudioTrackRenderer;-><init>(Lcom/google/android/exoplayer/SampleSource;Lcom/google/android/exoplayer/MediaCodecSelector;)V

    goto :goto_1
.end method

.method private h()V
    .locals 6

    .prologue
    const-wide/16 v2, 0xbb8

    .line 344
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lczf;->f:Ljava/util/Timer;

    .line 345
    iget-object v0, p0, Lczf;->f:Ljava/util/Timer;

    new-instance v1, Lczf$1;

    invoke-direct {v1, p0}, Lczf$1;-><init>(Lczf;)V

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 353
    return-void
.end method

.method private i()V
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lczf;->f:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lczf;->f:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 360
    iget-object v0, p0, Lczf;->f:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 361
    const/4 v0, 0x0

    iput-object v0, p0, Lczf;->f:Ljava/util/Timer;

    .line 363
    :cond_0
    return-void
.end method

.method private j()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x2328

    const/4 v0, 0x1

    const-wide/16 v6, 0x0

    .line 367
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 368
    const/4 v1, 0x0

    .line 369
    monitor-enter p0

    .line 371
    :try_start_0
    iget-wide v4, p0, Lczf;->h:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    .line 373
    iget-wide v4, p0, Lczf;->h:J

    sub-long/2addr v2, v4

    cmp-long v2, v2, v8

    if-lez v2, :cond_0

    .line 375
    const-string/jumbo v1, "LHLS"

    const-string/jumbo v2, "Connect timeout"

    invoke-static {v1, v2}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lczf;->h:J

    .line 377
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lczf;->m:J

    move v1, v0

    .line 390
    :cond_0
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    invoke-direct {p0}, Lczf;->k()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 395
    :goto_1
    if-eqz v0, :cond_1

    .line 397
    invoke-direct {p0}, Lczf;->f()V

    .line 399
    :cond_1
    return-void

    .line 381
    :cond_2
    :try_start_1
    iget-wide v4, p0, Lczf;->m:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    .line 383
    iget-wide v4, p0, Lczf;->m:J

    sub-long/2addr v2, v4

    cmp-long v2, v2, v8

    if-lez v2, :cond_0

    .line 385
    const-string/jumbo v1, "LHLS"

    const-string/jumbo v2, "No data timeout"

    invoke-static {v1, v2}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lczf;->m:J

    move v1, v0

    goto :goto_0

    .line 390
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private k()Z
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 403
    const/4 v0, 0x0

    .line 404
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    const-wide/32 v4, 0xf4240

    div-long/2addr v2, v4

    .line 405
    monitor-enter p0

    .line 407
    :try_start_0
    iget-wide v4, p0, Lczf;->n:J

    cmp-long v1, v4, v8

    if-lez v1, :cond_0

    iget-wide v4, p0, Lczf;->n:J

    cmp-long v1, v4, v2

    if-eqz v1, :cond_0

    .line 409
    iget-wide v4, p0, Lczf;->o:J

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    iget-wide v6, p0, Lczf;->n:J

    sub-long v6, v2, v6

    div-long/2addr v4, v6

    long-to-double v4, v4

    .line 410
    iget-object v1, p0, Lczf;->t:Ltv/periscope/android/video/rtmp/i;

    invoke-virtual {v1, v4, v5}, Ltv/periscope/android/video/rtmp/i;->a(D)V

    .line 411
    const-string/jumbo v1, "LHLS"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "FPS: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    iget-wide v4, p0, Lczf;->o:J

    cmp-long v1, v4, v8

    if-nez v1, :cond_0

    .line 415
    iget-wide v4, p0, Lczf;->p:J

    cmp-long v1, v4, v8

    if-nez v1, :cond_1

    .line 417
    iput-wide v2, p0, Lczf;->p:J

    .line 430
    :cond_0
    :goto_0
    iput-wide v2, p0, Lczf;->n:J

    .line 431
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lczf;->o:J

    .line 432
    monitor-exit p0

    .line 433
    return v0

    .line 421
    :cond_1
    iget-wide v4, p0, Lczf;->p:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0x2328

    cmp-long v1, v4, v6

    if-lez v1, :cond_0

    .line 423
    const-string/jumbo v0, "LHLS"

    const-string/jumbo v1, "No video timeout"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    const/4 v0, 0x1

    goto :goto_0

    .line 432
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private l()V
    .locals 4

    .prologue
    .line 468
    iget-object v0, p0, Lczf;->A:Ljava/util/HashMap;

    const-string/jumbo v1, "RtmpConnectTime"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 469
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lczf;->h:J

    sub-long/2addr v0, v2

    .line 470
    iget-object v2, p0, Lczf;->A:Ljava/util/HashMap;

    const-string/jumbo v3, "RtmpConnectTime"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 472
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 156
    const-string/jumbo v0, "LHLS"

    const-string/jumbo v1, "LHLS Playback cancel"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    invoke-virtual {p0}, Lczf;->d()V

    .line 160
    return-void
.end method

.method public a(J)V
    .locals 13

    .prologue
    const-wide/16 v2, 0x0

    const-wide v10, 0x408f400000000000L    # 1000.0

    const-wide/16 v8, 0x3e8

    .line 502
    monitor-enter p0

    .line 504
    cmp-long v0, p1, v2

    if-nez v0, :cond_0

    :try_start_0
    iget-wide v0, p0, Lczf;->k:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 506
    iget-wide p1, p0, Lczf;->k:J

    .line 508
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 509
    iget-object v0, p0, Lczf;->s:Lcze;

    if-eqz v0, :cond_1

    .line 511
    iget-object v0, p0, Lczf;->s:Lcze;

    invoke-virtual {v0}, Lcze;->getBufferedPositionUs()J

    move-result-wide v0

    .line 512
    div-long v2, v0, v8

    sub-long/2addr v2, p1

    long-to-double v2, v2

    .line 513
    const-string/jumbo v4, "LHLS"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Audio queue: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    double-to-long v6, v2

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "ms ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    div-long/2addr v0, v8

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    iget-object v0, p0, Lczf;->w:Ltv/periscope/android/video/rtmp/i;

    div-double/2addr v2, v10

    invoke-virtual {v0, v2, v3}, Ltv/periscope/android/video/rtmp/i;->a(D)V

    .line 516
    :cond_1
    iget-object v0, p0, Lczf;->r:Lczh;

    if-eqz v0, :cond_2

    .line 518
    iget-object v0, p0, Lczf;->r:Lczh;

    invoke-virtual {v0}, Lczh;->getBufferedPositionUs()J

    move-result-wide v0

    .line 519
    div-long v2, v0, v8

    sub-long/2addr v2, p1

    long-to-double v2, v2

    .line 520
    const-string/jumbo v4, "LHLS"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Video queue: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    double-to-long v6, v2

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "ms ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    div-long/2addr v0, v8

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    iget-object v0, p0, Lczf;->x:Ltv/periscope/android/video/rtmp/i;

    div-double/2addr v2, v10

    invoke-virtual {v0, v2, v3}, Ltv/periscope/android/video/rtmp/i;->a(D)V

    .line 523
    :cond_2
    return-void

    .line 508
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lczi;)V
    .locals 2

    .prologue
    .line 140
    const-string/jumbo v0, "LHLS"

    const-string/jumbo v1, "LHLS Playback starting"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    iput-object p1, p0, Lczf;->c:Lczi;

    .line 142
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lczf;->h:J

    .line 143
    iget-wide v0, p0, Lczf;->h:J

    iput-wide v0, p0, Lczf;->i:J

    .line 144
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lczf;->j:J

    .line 145
    const/4 v0, 0x0

    iput-boolean v0, p0, Lczf;->C:Z

    .line 147
    invoke-direct {p0}, Lczf;->e()V

    .line 148
    invoke-direct {p0}, Lczf;->h()V

    .line 150
    new-instance v0, Ltv/periscope/android/video/lhls/LHLSPlayer;

    invoke-direct {v0, p0}, Ltv/periscope/android/video/lhls/LHLSPlayer;-><init>(Ltv/periscope/android/video/lhls/LHLSPlayer$LHLSPlayerListener;)V

    iput-object v0, p0, Lczf;->q:Ltv/periscope/android/video/lhls/LHLSPlayer;

    .line 151
    iget-object v0, p0, Lczf;->q:Ltv/periscope/android/video/lhls/LHLSPlayer;

    iget-object v1, p0, Lczf;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ltv/periscope/android/video/lhls/LHLSPlayer;->StartPlayback(Ljava/lang/String;)Z

    .line 152
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lczf;->r:Lczh;

    if-eqz v0, :cond_0

    .line 460
    iget-object v0, p0, Lczf;->r:Lczh;

    invoke-virtual {v0}, Lczh;->b()I

    move-result v0

    .line 462
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide v6, 0x408f400000000000L    # 1000.0

    const-wide/16 v4, 0x0

    .line 475
    iget-object v0, p0, Lczf;->A:Ljava/util/HashMap;

    const-string/jumbo v1, "Protocol"

    const-string/jumbo v2, "LHLS"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 476
    iget-object v0, p0, Lczf;->t:Ltv/periscope/android/video/rtmp/i;

    iget-object v1, p0, Lczf;->A:Ljava/util/HashMap;

    const-string/jumbo v2, "FrameRate"

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/video/rtmp/i;->a(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 478
    iget-wide v0, p0, Lczf;->i:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    .line 480
    iget-wide v0, p0, Lczf;->j:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    .line 482
    iget-wide v0, p0, Lczf;->j:J

    iget-wide v2, p0, Lczf;->i:J

    sub-long/2addr v0, v2

    long-to-double v0, v0

    .line 483
    iget-object v2, p0, Lczf;->A:Ljava/util/HashMap;

    const-string/jumbo v3, "StartToFirstPacket"

    div-double/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 484
    iget-wide v0, p0, Lczf;->l:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    .line 486
    iget-wide v0, p0, Lczf;->l:J

    iget-wide v2, p0, Lczf;->i:J

    sub-long/2addr v0, v2

    long-to-double v0, v0

    .line 487
    iget-object v2, p0, Lczf;->A:Ljava/util/HashMap;

    const-string/jumbo v3, "StartToFirstFrame"

    div-double/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 492
    :cond_0
    iget-object v0, p0, Lczf;->u:Ltv/periscope/android/video/rtmp/i;

    iget-object v1, p0, Lczf;->A:Ljava/util/HashMap;

    const-string/jumbo v2, "AudioJitter"

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/video/rtmp/i;->a(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 493
    iget-object v0, p0, Lczf;->v:Ltv/periscope/android/video/rtmp/i;

    iget-object v1, p0, Lczf;->A:Ljava/util/HashMap;

    const-string/jumbo v2, "VideoJitter"

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/video/rtmp/i;->a(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 494
    iget-object v0, p0, Lczf;->w:Ltv/periscope/android/video/rtmp/i;

    iget-object v1, p0, Lczf;->A:Ljava/util/HashMap;

    const-string/jumbo v2, "AudioQueue"

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/video/rtmp/i;->a(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 495
    iget-object v0, p0, Lczf;->x:Ltv/periscope/android/video/rtmp/i;

    iget-object v1, p0, Lczf;->A:Ljava/util/HashMap;

    const-string/jumbo v2, "VideoQueue"

    invoke-virtual {v0, v1, v2}, Ltv/periscope/android/video/rtmp/i;->a(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 497
    iget-object v0, p0, Lczf;->A:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 5

    .prologue
    .line 527
    const-string/jumbo v0, "LHLS"

    const-string/jumbo v1, "LHLS playback shutdown"

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    const/4 v0, 0x1

    iput-boolean v0, p0, Lczf;->C:Z

    .line 529
    iget-object v0, p0, Lczf;->q:Ltv/periscope/android/video/lhls/LHLSPlayer;

    if-eqz v0, :cond_0

    .line 530
    iget-object v0, p0, Lczf;->q:Ltv/periscope/android/video/lhls/LHLSPlayer;

    invoke-virtual {v0}, Ltv/periscope/android/video/lhls/LHLSPlayer;->StopPlayback()V

    .line 532
    :cond_0
    invoke-direct {p0}, Lczf;->i()V

    .line 535
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lczf;->B:Ljava/util/Vector;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 536
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv/periscope/android/video/lhls/HTTPRequest;

    .line 538
    const-string/jumbo v2, "LHLS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Cancelling orphaned request "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ltv/periscope/android/video/lhls/HTTPRequest;->getURL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    invoke-virtual {v0}, Ltv/periscope/android/video/lhls/HTTPRequest;->cancelRequest()V

    goto :goto_0

    .line 541
    :cond_1
    return-void
.end method

.method public makeNetRequest(Ljava/lang/String;J)Ltv/periscope/android/video/lhls/HTTPRequest;
    .locals 8

    .prologue
    .line 262
    iget-boolean v0, p0, Lczf;->C:Z

    if-eqz v0, :cond_0

    .line 264
    const/4 v0, 0x0

    .line 270
    :goto_0
    return-object v0

    .line 268
    :cond_0
    new-instance v0, Ltv/periscope/android/video/lhls/HTTPRequest;

    iget-object v1, p0, Lczf;->c:Lczi;

    invoke-virtual {v1}, Lczi;->a()Ldct;

    move-result-object v4

    iget-object v5, p0, Lczf;->b:Ljava/lang/String;

    move-object v1, p1

    move-wide v2, p2

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Ltv/periscope/android/video/lhls/HTTPRequest;-><init>(Ljava/lang/String;JLdct;Ljava/lang/String;Ltv/periscope/android/video/lhls/HTTPRequest$HTTPRequestDelegate;)V

    .line 269
    iget-object v1, p0, Lczf;->B:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onAudio([BDD)V
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lczf;->s:Lcze;

    if-eqz v0, :cond_0

    .line 227
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lczf;->a(Z)V

    .line 228
    iget-object v0, p0, Lczf;->s:Lcze;

    invoke-virtual {v0, p1, p2, p3}, Lcze;->a([BD)V

    .line 230
    :cond_0
    return-void
.end method

.method public onAudioFormat(II)V
    .locals 2

    .prologue
    .line 209
    monitor-enter p0

    .line 211
    :try_start_0
    iget-object v0, p0, Lczf;->s:Lcze;

    if-nez v0, :cond_0

    .line 213
    new-instance v0, Lcze;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p1, p2, p0}, Lcze;-><init>(IIILczg$a;)V

    iput-object v0, p0, Lczf;->s:Lcze;

    .line 214
    iget-object v0, p0, Lczf;->r:Lczh;

    if-eqz v0, :cond_0

    .line 216
    invoke-direct {p0}, Lczf;->g()V

    .line 219
    :cond_0
    monitor-exit p0

    .line 220
    return-void

    .line 219
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onDiscontinuity()V
    .locals 4

    .prologue
    .line 248
    iget-wide v0, p0, Lczf;->j:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, Lczf;->d:Lczk$a;

    invoke-interface {v0}, Lczk$a;->a()V

    .line 252
    :cond_0
    return-void
.end method

.method public onEOS()V
    .locals 0

    .prologue
    .line 257
    return-void
.end method

.method public onMetadata([BD)V
    .locals 4

    .prologue
    .line 234
    invoke-static {p1}, Ltv/periscope/android/video/rtmp/a;->a([B)[Ljava/lang/Object;

    move-result-object v0

    .line 235
    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 237
    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, Ljava/util/Map;

    .line 238
    iget-object v1, p0, Lczf;->d:Lczk$a;

    const-wide v2, 0x408f400000000000L    # 1000.0

    mul-double/2addr v2, p2

    double-to-long v2, v2

    invoke-interface {v1, v0, v2, v3}, Lczk$a;->a(Ljava/util/Map;J)V

    .line 241
    :cond_0
    return-void
.end method

.method public onRequestComplete(Ltv/periscope/android/video/lhls/HTTPRequest;)V
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lczf;->B:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    .line 278
    return-void
.end method

.method public onVideo([BDD)V
    .locals 4

    .prologue
    .line 193
    monitor-enter p0

    .line 195
    :try_start_0
    iget-wide v0, p0, Lczf;->k:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 197
    const-wide v0, 0x408f400000000000L    # 1000.0

    mul-double/2addr v0, p2

    double-to-long v0, v0

    iput-wide v0, p0, Lczf;->k:J

    .line 199
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    iget-object v0, p0, Lczf;->r:Lczh;

    if-eqz v0, :cond_1

    .line 202
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lczf;->a(Z)V

    .line 203
    iget-object v0, p0, Lczf;->r:Lczh;

    invoke-virtual {v0, p1, p2, p3}, Lczh;->a([BD)V

    .line 205
    :cond_1
    return-void

    .line 199
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onVideoFormat([B[BII)V
    .locals 8

    .prologue
    .line 178
    monitor-enter p0

    .line 180
    :try_start_0
    iget-object v0, p0, Lczf;->r:Lczh;

    if-nez v0, :cond_0

    .line 182
    new-instance v0, Lczh;

    const/4 v1, 0x0

    iget-object v6, p0, Lczf;->d:Lczk$a;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-object v7, p0

    invoke-direct/range {v0 .. v7}, Lczh;-><init>(I[B[BIILczk$a;Lczg$a;)V

    iput-object v0, p0, Lczf;->r:Lczh;

    .line 183
    iget-object v0, p0, Lczf;->s:Lcze;

    if-eqz v0, :cond_0

    .line 185
    invoke-direct {p0}, Lczf;->g()V

    .line 188
    :cond_0
    monitor-exit p0

    .line 189
    return-void

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
