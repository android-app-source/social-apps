.class public abstract Lgl;
.super Lgp;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgl$a;
    }
.end annotation


# instance fields
.field protected f:Lgl$a;


# direct methods
.method public constructor <init>(Lew;Lhp;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lgp;-><init>(Lew;Lhp;)V

    .line 19
    new-instance v0, Lgl$a;

    invoke-direct {v0, p0}, Lgl$a;-><init>(Lgl;)V

    iput-object v0, p0, Lgl;->f:Lgl$a;

    .line 23
    return-void
.end method


# virtual methods
.method protected a(Lcom/github/mikephil/charting/data/Entry;Lfz;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 44
    if-nez p1, :cond_1

    .line 52
    :cond_0
    :goto_0
    return v0

    .line 47
    :cond_1
    invoke-interface {p2, p1}, Lfz;->e(Lcom/github/mikephil/charting/data/Entry;)I

    move-result v1

    int-to-float v1, v1

    .line 49
    if-eqz p1, :cond_0

    invoke-interface {p2}, Lfz;->s()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lgl;->g:Lew;

    invoke-virtual {v3}, Lew;->b()F

    move-result v3

    mul-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-gez v1, :cond_0

    .line 52
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected a(Lgc;)Z
    .locals 1

    .prologue
    .line 32
    invoke-interface {p1}, Lgc;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lgc;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
