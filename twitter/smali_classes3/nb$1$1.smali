.class Lnb$1$1;
.super Lcom/twitter/library/service/t;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnb$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lnb$1;


# direct methods
.method constructor <init>(Lnb$1;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lnb$1$1;->a:Lnb$1;

    invoke-direct {p0}, Lcom/twitter/library/service/t;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/async/service/AsyncOperation;)V
    .locals 0

    .prologue
    .line 50
    check-cast p1, Lcom/twitter/library/service/s;

    invoke-virtual {p0, p1}, Lnb$1$1;->a(Lcom/twitter/library/service/s;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/s;)V
    .locals 3

    .prologue
    .line 53
    invoke-virtual {p1}, Lcom/twitter/library/service/s;->l()Lcom/twitter/async/service/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/async/service/j;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/u;

    invoke-virtual {v0}, Lcom/twitter/library/service/u;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    check-cast p1, Lcom/twitter/library/api/search/f;

    .line 56
    iget-object v0, p0, Lnb$1$1;->a:Lnb$1;

    iget-object v0, v0, Lnb$1;->a:Landroid/content/Context;

    iget-object v1, p0, Lnb$1$1;->a:Lnb$1;

    iget-object v1, v1, Lnb$1;->b:Lcom/twitter/library/client/Session;

    invoke-static {v0, v1, p1}, Lcom/twitter/android/trends/f;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/api/search/f;)V

    .line 58
    iget-object v0, p0, Lnb$1$1;->a:Lnb$1;

    iget-object v0, v0, Lnb$1;->c:Lnb$a;

    .line 59
    invoke-virtual {p1}, Lcom/twitter/library/api/search/f;->e()Ljava/util/List;

    move-result-object v1

    .line 58
    invoke-interface {v0, v1}, Lnb$a;->a(Ljava/util/List;)V

    .line 67
    :goto_0
    return-void

    .line 62
    :cond_0
    iget-object v0, p0, Lnb$1$1;->a:Lnb$1;

    iget-object v0, v0, Lnb$1;->c:Lnb$a;

    .line 63
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v1

    .line 62
    invoke-interface {v0, v1}, Lnb$a;->a(Ljava/util/List;)V

    .line 64
    iget-object v0, p0, Lnb$1$1;->a:Lnb$1;

    iget-object v0, v0, Lnb$1;->a:Landroid/content/Context;

    const v1, 0x7f0a0973

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
