.class Lcyb$b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcyb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcyb;


# direct methods
.method private constructor <init>(Lcyb;)V
    .locals 0

    .prologue
    .line 478
    iput-object p1, p0, Lcyb$b;->a:Lcyb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcyb;Lcyb$1;)V
    .locals 0

    .prologue
    .line 478
    invoke-direct {p0, p1}, Lcyb$b;-><init>(Lcyb;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 482
    iget-object v0, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v0}, Lcyb;->b(Lcyb;)Lcyf;

    move-result-object v0

    if-nez v0, :cond_0

    .line 567
    :goto_0
    return-void

    .line 485
    :cond_0
    iget-object v0, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v0}, Lcyb;->c(Lcyb;)Ltv/periscope/android/video/RTMPPublisher;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/video/RTMPPublisher;->g()J

    move-result-wide v2

    .line 486
    iget-object v0, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v0}, Lcyb;->c(Lcyb;)Ltv/periscope/android/video/RTMPPublisher;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/video/RTMPPublisher;->h()J

    move-result-wide v4

    .line 487
    iget-object v0, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v0}, Lcyb;->c(Lcyb;)Ltv/periscope/android/video/RTMPPublisher;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/video/RTMPPublisher;->i()Ljava/util/Date;

    move-result-object v1

    .line 489
    if-nez v1, :cond_1

    .line 491
    iget-object v0, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v0}, Lcyb;->d(Lcyb;)Lcyb$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcyb$a;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 496
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 497
    const-wide/16 v8, 0x0

    cmp-long v0, v2, v8

    if-nez v0, :cond_9

    .line 499
    iget-object v0, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v0}, Lcyb;->e(Lcyb;)J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v0, v8, v10

    if-lez v0, :cond_2

    .line 500
    iget-object v0, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v0}, Lcyb;->e(Lcyb;)J

    move-result-wide v8

    sub-long v8, v6, v8

    const-wide/16 v10, 0x2ee0

    cmp-long v0, v8, v10

    if-lez v0, :cond_2

    .line 501
    iget-object v0, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v0}, Lcyb;->c(Lcyb;)Ltv/periscope/android/video/RTMPPublisher;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 502
    const-string/jumbo v0, "BroadcasterVideoController"

    const-string/jumbo v8, "Reconnect on zero-send timeout"

    invoke-static {v0, v8}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    iget-object v0, p0, Lcyb$b;->a:Lcyb;

    const-wide/16 v8, 0x0

    invoke-static {v0, v8, v9}, Lcyb;->a(Lcyb;J)J

    .line 504
    iget-object v0, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v0}, Lcyb;->c(Lcyb;)Ltv/periscope/android/video/RTMPPublisher;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/video/RTMPPublisher;->j()V

    .line 513
    :cond_2
    :goto_1
    const/4 v0, 0x0

    .line 514
    monitor-enter p0

    .line 515
    :try_start_0
    iget-object v8, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v8}, Lcyb;->f(Lcyb;)J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-lez v8, :cond_3

    .line 516
    iget-object v8, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v8}, Lcyb;->f(Lcyb;)J

    move-result-wide v8

    sub-long/2addr v6, v8

    const-wide/16 v8, 0xbb8

    cmp-long v6, v6, v8

    if-lez v6, :cond_3

    .line 517
    const-string/jumbo v0, "BroadcasterVideoController"

    const-string/jumbo v6, "Restart encoder on video output timeout"

    invoke-static {v0, v6}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    iget-object v0, p0, Lcyb$b;->a:Lcyb;

    const-wide/16 v6, 0x0

    invoke-static {v0, v6, v7}, Lcyb;->b(Lcyb;J)J

    .line 519
    const/4 v0, 0x1

    .line 522
    :cond_3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 523
    if-eqz v0, :cond_4

    .line 524
    iget-object v0, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v0}, Lcyb;->b(Lcyb;)Lcyf;

    move-result-object v0

    invoke-interface {v0}, Lcyf;->g()V

    .line 525
    iget-object v0, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v0}, Lcyb;->c(Lcyb;)Ltv/periscope/android/video/RTMPPublisher;

    move-result-object v0

    invoke-virtual {v0}, Ltv/periscope/android/video/RTMPPublisher;->e()V

    .line 530
    :cond_4
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 531
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    sub-long v0, v6, v0

    long-to-double v0, v0

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double v6, v0, v6

    .line 532
    const-wide/16 v0, 0x0

    .line 533
    iget-object v8, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v8}, Lcyb;->g(Lcyb;)J

    move-result-wide v8

    cmp-long v8, v4, v8

    if-lez v8, :cond_5

    .line 534
    iget-object v0, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v0}, Lcyb;->g(Lcyb;)J

    move-result-wide v0

    sub-long v0, v4, v0

    .line 538
    :cond_5
    const-wide/16 v8, 0x0

    cmpl-double v8, v6, v8

    if-lez v8, :cond_6

    .line 539
    long-to-double v2, v2

    div-double/2addr v2, v6

    double-to-long v2, v2

    .line 540
    long-to-double v0, v0

    div-double/2addr v0, v6

    double-to-long v0, v0

    .line 542
    :cond_6
    iget-object v6, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v6}, Lcyb;->h(Lcyb;)Ltv/periscope/android/video/b;

    move-result-object v6

    long-to-int v7, v2

    long-to-int v8, v0

    invoke-virtual {v6, v7, v8}, Ltv/periscope/android/video/b;->b(II)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 543
    const-string/jumbo v6, "BroadcasterVideoController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Change video rate: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v8}, Lcyb;->h(Lcyb;)Ltv/periscope/android/video/b;

    move-result-object v8

    invoke-virtual {v8}, Ltv/periscope/android/video/b;->c()I

    move-result v8

    mul-int/lit8 v8, v8, 0x8

    div-int/lit16 v8, v8, 0x400

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    iget-object v6, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v6}, Lcyb;->b(Lcyb;)Lcyf;

    move-result-object v6

    iget-object v7, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v7}, Lcyb;->h(Lcyb;)Ltv/periscope/android/video/b;

    move-result-object v7

    invoke-virtual {v7}, Ltv/periscope/android/video/b;->c()I

    move-result v7

    mul-int/lit8 v7, v7, 0x8

    invoke-interface {v6, v7}, Lcyf;->b(I)V

    .line 546
    :cond_7
    iget-object v6, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v6, v4, v5}, Lcyb;->c(Lcyb;J)J

    .line 552
    iget-object v4, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v4}, Lcyb;->h(Lcyb;)Ltv/periscope/android/video/b;

    move-result-object v4

    invoke-virtual {v4}, Ltv/periscope/android/video/b;->b()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 553
    iget-object v4, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v4}, Lcyb;->i(Lcyb;)Ljava/util/HashMap;

    move-result-object v4

    const-string/jumbo v5, "BadConnection"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 555
    :cond_8
    iget-object v4, p0, Lcyb$b;->a:Lcyb;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcyb;->a(Lcyb;Z)V

    .line 558
    long-to-int v2, v2

    mul-int/lit8 v2, v2, 0x8

    div-int/lit16 v2, v2, 0x400

    .line 559
    long-to-int v0, v0

    mul-int/lit8 v0, v0, 0x8

    div-int/lit16 v0, v0, 0x400

    .line 560
    iget-object v1, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v1}, Lcyb;->b(Lcyb;)Lcyf;

    move-result-object v1

    invoke-interface {v1}, Lcyf;->e()I

    move-result v1

    div-int/lit16 v1, v1, 0x400

    .line 562
    new-instance v3, Ljava/text/DecimalFormat;

    const-string/jumbo v4, "#.#"

    invoke-direct {v3, v4}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 563
    const-string/jumbo v4, "BroadcasterVideoController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Rate/Sent/Not Sent "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v5, "/"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " kbits/s fps req/actual:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcyb$b;->a:Lcyb;

    .line 564
    invoke-static {v1}, Lcyb;->h(Lcyb;)Ltv/periscope/android/video/b;

    move-result-object v1

    invoke-virtual {v1}, Ltv/periscope/android/video/b;->d()D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v1}, Lcyb;->h(Lcyb;)Ltv/periscope/android/video/b;

    move-result-object v1

    invoke-virtual {v1}, Ltv/periscope/android/video/b;->e()D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 563
    invoke-static {v4, v0}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    const-string/jumbo v0, "BroadcasterVideoController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Audio kbits/s: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v2}, Lcyb;->h(Lcyb;)Ltv/periscope/android/video/b;

    move-result-object v2

    invoke-virtual {v2}, Ltv/periscope/android/video/b;->f()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    iget-object v0, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v0}, Lcyb;->d(Lcyb;)Lcyb$a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcyb$a;->a(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 509
    :cond_9
    iget-object v0, p0, Lcyb$b;->a:Lcyb;

    invoke-static {v0, v6, v7}, Lcyb;->a(Lcyb;J)J

    goto/16 :goto_1

    .line 522
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
