.class public Lhk;
.super Lhl$a;
.source "Twttr"


# static fields
.field public static final c:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lhk;",
            ">;"
        }
    .end annotation
.end field

.field private static f:Lhl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lhl",
            "<",
            "Lhk;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:F

.field public b:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 19
    const/16 v0, 0x20

    new-instance v1, Lhk;

    invoke-direct {v1, v2, v2}, Lhk;-><init>(FF)V

    invoke-static {v0, v1}, Lhl;->a(ILhl$a;)Lhl;

    move-result-object v0

    sput-object v0, Lhk;->f:Lhl;

    .line 20
    sget-object v0, Lhk;->f:Lhl;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Lhl;->a(F)V

    .line 46
    new-instance v0, Lhk$1;

    invoke-direct {v0}, Lhk$1;-><init>()V

    sput-object v0, Lhk;->c:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lhl$a;-><init>()V

    .line 24
    return-void
.end method

.method public constructor <init>(FF)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lhl$a;-><init>()V

    .line 27
    iput p1, p0, Lhk;->a:F

    .line 28
    iput p2, p0, Lhk;->b:F

    .line 29
    return-void
.end method

.method public static a(FF)Lhk;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lhk;->f:Lhl;

    invoke-virtual {v0}, Lhl;->a()Lhl$a;

    move-result-object v0

    check-cast v0, Lhk;

    .line 33
    iput p0, v0, Lhk;->a:F

    .line 34
    iput p1, v0, Lhk;->b:F

    .line 35
    return-object v0
.end method

.method public static a(Lhk;)V
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lhk;->f:Lhl;

    invoke-virtual {v0, p0}, Lhl;->a(Lhl$a;)V

    .line 40
    return-void
.end method


# virtual methods
.method protected a()Lhl$a;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 86
    new-instance v0, Lhk;

    invoke-direct {v0, v1, v1}, Lhk;-><init>(FF)V

    return-object v0
.end method

.method public a(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lhk;->a:F

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lhk;->b:F

    .line 74
    return-void
.end method
