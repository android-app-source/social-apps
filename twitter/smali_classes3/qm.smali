.class public Lqm;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcbi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcbi",
            "<",
            "Lcom/twitter/library/av/playback/AVDataSource;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/twitter/library/av/p;


# direct methods
.method public constructor <init>(Lcbi;Ljava/util/Map;Lcom/twitter/library/av/p;)V
    .locals 0
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcbi",
            "<",
            "Lcom/twitter/library/av/playback/AVDataSource;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/twitter/library/av/p;",
            ")V"
        }
    .end annotation

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-object p1, p0, Lqm;->a:Lcbi;

    .line 98
    iput-object p2, p0, Lqm;->b:Ljava/util/Map;

    .line 99
    iput-object p3, p0, Lqm;->c:Lcom/twitter/library/av/p;

    .line 100
    return-void
.end method

.method public static a(Lcom/twitter/library/av/playback/AVDataSource;)Lqm;
    .locals 5

    .prologue
    .line 46
    new-instance v0, Lcbl$a;

    invoke-direct {v0}, Lcbl$a;-><init>()V

    .line 47
    invoke-virtual {v0, p0}, Lcbl$a;->a(Ljava/lang/Object;)Lcbl$a;

    .line 48
    new-instance v1, Lqm;

    invoke-virtual {v0}, Lcbl$a;->a()Lcbl;

    move-result-object v0

    invoke-static {}, Lcom/twitter/util/collection/i;->f()Ljava/util/Map;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/av/s;

    .line 49
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/twitter/library/av/s;-><init>(Ljava/util/List;)V

    invoke-direct {v1, v0, v2, v3}, Lqm;-><init>(Lcbi;Ljava/util/Map;Lcom/twitter/library/av/p;)V

    .line 48
    return-object v1
.end method

.method public static a(Lqe;Lcom/twitter/library/av/playback/AVDataSource;Lcbi;)Lqm;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lqe;",
            "Lcom/twitter/library/av/playback/AVDataSource;",
            "Lcbi",
            "<",
            "Lcom/twitter/model/av/i;",
            ">;)",
            "Lqm;"
        }
    .end annotation

    .prologue
    .line 58
    new-instance v2, Lcbl$a;

    invoke-direct {v2}, Lcbl$a;-><init>()V

    .line 59
    invoke-static {}, Lcom/twitter/util/collection/i;->e()Lcom/twitter/util/collection/i;

    move-result-object v3

    .line 61
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v4

    .line 63
    invoke-virtual {v2, p1}, Lcbl$a;->a(Ljava/lang/Object;)Lcbl$a;

    .line 66
    const/4 v0, 0x1

    .line 68
    invoke-virtual {p2}, Lcbi;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/av/i;

    .line 69
    invoke-virtual {v0}, Lcom/twitter/model/av/i;->a()Ljava/lang/String;

    move-result-object v6

    .line 70
    invoke-static {v6}, Lcom/twitter/util/y;->b(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 71
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v7, v6}, Lcom/twitter/util/collection/i;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/twitter/util/collection/i;

    .line 73
    :cond_1
    invoke-virtual {v0}, Lcom/twitter/model/av/i;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/core/ac;

    .line 74
    invoke-virtual {p0, v0}, Lqe;->a(Lcom/twitter/model/core/ac;)Lcom/twitter/library/av/playback/TweetAVDataSource;

    move-result-object v0

    .line 76
    invoke-virtual {v2, v0}, Lcbl$a;->a(Ljava/lang/Object;)Lcbl$a;

    .line 77
    new-instance v7, Lqm$1;

    invoke-direct {v7, v0}, Lqm$1;-><init>(Lcom/twitter/library/av/playback/TweetAVDataSource;)V

    invoke-virtual {v4, v7}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    .line 85
    add-int/lit8 v1, v1, 0x1

    .line 86
    goto :goto_0

    .line 89
    :cond_2
    new-instance v5, Lqm;

    invoke-virtual {v2}, Lcbl$a;->a()Lcbl;

    move-result-object v2

    invoke-virtual {v3}, Lcom/twitter/util/collection/i;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    new-instance v3, Lcom/twitter/library/av/s;

    .line 90
    invoke-virtual {v4}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-direct {v3, v1}, Lcom/twitter/library/av/s;-><init>(Ljava/util/List;)V

    invoke-direct {v5, v2, v0, v3}, Lqm;-><init>(Lcbi;Ljava/util/Map;Lcom/twitter/library/av/p;)V

    .line 89
    return-object v5
.end method


# virtual methods
.method public a()Lcbi;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcbi",
            "<",
            "Lcom/twitter/library/av/playback/AVDataSource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lqm;->a:Lcbi;

    return-object v0
.end method

.method public b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lqm;->b:Ljava/util/Map;

    return-object v0
.end method

.method public c()Lcom/twitter/library/av/p;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lqm;->c:Lcom/twitter/library/av/p;

    return-object v0
.end method
