.class public final Lhr;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhr$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/google/android/gms/common/api/a$g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/a$g",
            "<",
            "Lcom/google/android/gms/auth/api/credentials/internal/c;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/google/android/gms/common/api/a$g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/a$g",
            "<",
            "Lcom/google/android/gms/internal/bh;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Lcom/google/android/gms/common/api/a$g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/a$g",
            "<",
            "Lcom/google/android/gms/auth/api/signin/internal/d;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lcom/google/android/gms/common/api/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/a",
            "<",
            "Lht;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:Lcom/google/android/gms/common/api/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/a",
            "<",
            "Lhr$a;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:Lcom/google/android/gms/common/api/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/a",
            "<",
            "Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:Lcom/google/android/gms/common/api/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/a",
            "<",
            "Lcom/google/android/gms/common/api/a$a$b;",
            ">;"
        }
    .end annotation
.end field

.field public static final h:Lcom/google/android/gms/auth/api/proxy/a;

.field public static final i:Lcom/google/android/gms/auth/api/credentials/a;

.field public static final j:Lcom/google/android/gms/internal/bf;

.field public static final k:Lcom/google/android/gms/auth/api/signin/a;

.field private static final l:Lcom/google/android/gms/common/api/a$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/a$b",
            "<",
            "Lcom/google/android/gms/auth/api/credentials/internal/c;",
            "Lhr$a;",
            ">;"
        }
    .end annotation
.end field

.field private static final m:Lcom/google/android/gms/common/api/a$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/a$b",
            "<",
            "Lcom/google/android/gms/internal/bh;",
            "Lcom/google/android/gms/common/api/a$a$b;",
            ">;"
        }
    .end annotation
.end field

.field private static final n:Lcom/google/android/gms/common/api/a$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/a$b",
            "<",
            "Lcom/google/android/gms/auth/api/signin/internal/d;",
            "Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/google/android/gms/common/api/a$g;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/a$g;-><init>()V

    sput-object v0, Lhr;->a:Lcom/google/android/gms/common/api/a$g;

    new-instance v0, Lcom/google/android/gms/common/api/a$g;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/a$g;-><init>()V

    sput-object v0, Lhr;->b:Lcom/google/android/gms/common/api/a$g;

    new-instance v0, Lcom/google/android/gms/common/api/a$g;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/a$g;-><init>()V

    sput-object v0, Lhr;->c:Lcom/google/android/gms/common/api/a$g;

    new-instance v0, Lhr$1;

    invoke-direct {v0}, Lhr$1;-><init>()V

    sput-object v0, Lhr;->l:Lcom/google/android/gms/common/api/a$b;

    new-instance v0, Lhr$2;

    invoke-direct {v0}, Lhr$2;-><init>()V

    sput-object v0, Lhr;->m:Lcom/google/android/gms/common/api/a$b;

    new-instance v0, Lhr$3;

    invoke-direct {v0}, Lhr$3;-><init>()V

    sput-object v0, Lhr;->n:Lcom/google/android/gms/common/api/a$b;

    sget-object v0, Lhs;->b:Lcom/google/android/gms/common/api/a;

    sput-object v0, Lhr;->d:Lcom/google/android/gms/common/api/a;

    new-instance v0, Lcom/google/android/gms/common/api/a;

    const-string/jumbo v1, "Auth.CREDENTIALS_API"

    sget-object v2, Lhr;->l:Lcom/google/android/gms/common/api/a$b;

    sget-object v3, Lhr;->a:Lcom/google/android/gms/common/api/a$g;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/a;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/api/a$b;Lcom/google/android/gms/common/api/a$g;)V

    sput-object v0, Lhr;->e:Lcom/google/android/gms/common/api/a;

    new-instance v0, Lcom/google/android/gms/common/api/a;

    const-string/jumbo v1, "Auth.GOOGLE_SIGN_IN_API"

    sget-object v2, Lhr;->n:Lcom/google/android/gms/common/api/a$b;

    sget-object v3, Lhr;->c:Lcom/google/android/gms/common/api/a$g;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/a;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/api/a$b;Lcom/google/android/gms/common/api/a$g;)V

    sput-object v0, Lhr;->f:Lcom/google/android/gms/common/api/a;

    new-instance v0, Lcom/google/android/gms/common/api/a;

    const-string/jumbo v1, "Auth.ACCOUNT_STATUS_API"

    sget-object v2, Lhr;->m:Lcom/google/android/gms/common/api/a$b;

    sget-object v3, Lhr;->b:Lcom/google/android/gms/common/api/a$g;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/a;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/api/a$b;Lcom/google/android/gms/common/api/a$g;)V

    sput-object v0, Lhr;->g:Lcom/google/android/gms/common/api/a;

    new-instance v0, Lcom/google/android/gms/internal/bn;

    invoke-direct {v0}, Lcom/google/android/gms/internal/bn;-><init>()V

    sput-object v0, Lhr;->h:Lcom/google/android/gms/auth/api/proxy/a;

    new-instance v0, Lcom/google/android/gms/auth/api/credentials/internal/b;

    invoke-direct {v0}, Lcom/google/android/gms/auth/api/credentials/internal/b;-><init>()V

    sput-object v0, Lhr;->i:Lcom/google/android/gms/auth/api/credentials/a;

    new-instance v0, Lcom/google/android/gms/internal/bg;

    invoke-direct {v0}, Lcom/google/android/gms/internal/bg;-><init>()V

    sput-object v0, Lhr;->j:Lcom/google/android/gms/internal/bf;

    new-instance v0, Lcom/google/android/gms/auth/api/signin/internal/c;

    invoke-direct {v0}, Lcom/google/android/gms/auth/api/signin/internal/c;-><init>()V

    sput-object v0, Lhr;->k:Lcom/google/android/gms/auth/api/signin/a;

    return-void
.end method
