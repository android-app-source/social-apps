.class public Lczt;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:F

.field public final b:F

.field public final c:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput v0, p0, Lczt;->a:F

    .line 10
    iput v0, p0, Lczt;->b:F

    .line 11
    iput v0, p0, Lczt;->c:F

    .line 12
    return-void
.end method

.method public constructor <init>(F)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput p1, p0, Lczt;->a:F

    .line 22
    iput p1, p0, Lczt;->b:F

    .line 23
    iput p1, p0, Lczt;->c:F

    .line 24
    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput p1, p0, Lczt;->a:F

    .line 16
    iput p2, p0, Lczt;->b:F

    .line 17
    iput p3, p0, Lczt;->c:F

    .line 18
    return-void
.end method


# virtual methods
.method public a()F
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0, p0}, Lczt;->c(Lczt;)F

    move-result v0

    return v0
.end method

.method public a(F)Lczt;
    .locals 4

    .prologue
    .line 47
    new-instance v0, Lczt;

    iget v1, p0, Lczt;->a:F

    mul-float/2addr v1, p1

    iget v2, p0, Lczt;->b:F

    mul-float/2addr v2, p1

    iget v3, p0, Lczt;->c:F

    mul-float/2addr v3, p1

    invoke-direct {v0, v1, v2, v3}, Lczt;-><init>(FFF)V

    return-object v0
.end method

.method public a(Lczt;)Lczt;
    .locals 5

    .prologue
    .line 27
    new-instance v0, Lczt;

    iget v1, p0, Lczt;->a:F

    iget v2, p1, Lczt;->a:F

    add-float/2addr v1, v2

    iget v2, p0, Lczt;->b:F

    iget v3, p1, Lczt;->b:F

    add-float/2addr v2, v3

    iget v3, p0, Lczt;->c:F

    iget v4, p1, Lczt;->c:F

    add-float/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Lczt;-><init>(FFF)V

    return-object v0
.end method

.method public b()F
    .locals 2

    .prologue
    .line 59
    invoke-virtual {p0}, Lczt;->a()F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public b(Lczt;)Lczt;
    .locals 5

    .prologue
    .line 35
    new-instance v0, Lczt;

    iget v1, p0, Lczt;->a:F

    iget v2, p1, Lczt;->a:F

    sub-float/2addr v1, v2

    iget v2, p0, Lczt;->b:F

    iget v3, p1, Lczt;->b:F

    sub-float/2addr v2, v3

    iget v3, p0, Lczt;->c:F

    iget v4, p1, Lczt;->c:F

    sub-float/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Lczt;-><init>(FFF)V

    return-object v0
.end method

.method public c(Lczt;)F
    .locals 3

    .prologue
    .line 51
    iget v0, p0, Lczt;->a:F

    iget v1, p1, Lczt;->a:F

    mul-float/2addr v0, v1

    iget v1, p0, Lczt;->b:F

    iget v2, p1, Lczt;->b:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lczt;->c:F

    iget v2, p1, Lczt;->c:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public c()Lczt;
    .locals 2

    .prologue
    .line 63
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lczt;->b()F

    move-result v1

    div-float/2addr v0, v1

    invoke-virtual {p0, v0}, Lczt;->a(F)Lczt;

    move-result-object v0

    return-object v0
.end method

.method public d(Lczt;)Lczt;
    .locals 6

    .prologue
    .line 67
    new-instance v0, Lczt;

    iget v1, p0, Lczt;->b:F

    iget v2, p1, Lczt;->c:F

    mul-float/2addr v1, v2

    iget v2, p0, Lczt;->c:F

    iget v3, p1, Lczt;->b:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget v2, p0, Lczt;->c:F

    iget v3, p1, Lczt;->a:F

    mul-float/2addr v2, v3

    iget v3, p0, Lczt;->a:F

    iget v4, p1, Lczt;->c:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, Lczt;->a:F

    iget v4, p1, Lczt;->b:F

    mul-float/2addr v3, v4

    iget v4, p0, Lczt;->b:F

    iget v5, p1, Lczt;->a:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Lczt;-><init>(FFF)V

    return-object v0
.end method
