.class public Lwu;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lauj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lauj",
        "<",
        "Ljava/lang/Iterable",
        "<",
        "Ljava/lang/Long;",
        ">;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/Long;",
        "Lcom/twitter/model/core/Tweet;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lwv;

.field private final b:Laes;


# direct methods
.method public constructor <init>(Lwv;Laes;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lwu;->a:Lwv;

    .line 29
    iput-object p2, p0, Lwu;->b:Laes;

    .line 30
    return-void
.end method

.method static synthetic a(Lwu;)Laes;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lwu;->b:Laes;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Iterable;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lrx/c",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/Tweet;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lwu;->a:Lwv;

    invoke-static {p1}, Lcom/twitter/util/collection/h;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lwv;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    new-instance v1, Lwu$1;

    invoke-direct {v1, p0}, Lwu$1;-><init>(Lwu;)V

    .line 36
    invoke-virtual {v0, v1}, Lrx/c;->f(Lrx/functions/d;)Lrx/c;

    move-result-object v0

    .line 43
    invoke-virtual {v0}, Lrx/c;->k()Lrx/c;

    move-result-object v0

    .line 35
    return-object v0
.end method

.method public synthetic a_(Ljava/lang/Object;)Lrx/c;
    .locals 1

    .prologue
    .line 22
    check-cast p1, Ljava/lang/Iterable;

    invoke-virtual {p0, p1}, Lwu;->a(Ljava/lang/Iterable;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lwu;->a:Lwv;

    invoke-virtual {v0}, Lwv;->close()V

    .line 49
    iget-object v0, p0, Lwu;->b:Laes;

    invoke-virtual {v0}, Laes;->close()V

    .line 50
    return-void
.end method
