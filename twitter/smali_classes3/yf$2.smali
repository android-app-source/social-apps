.class Lyf$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/functions/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lyf;->a(Ljava/lang/Iterable;)Lrx/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/d",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/Long;",
        "Lcom/twitter/model/core/Tweet;",
        ">;",
        "Lrx/c",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/Long;",
        "Lcom/twitter/model/core/Tweet;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/Iterable;

.field final synthetic b:Lyf;


# direct methods
.method constructor <init>(Lyf;Ljava/lang/Iterable;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lyf$2;->b:Lyf;

    iput-object p2, p0, Lyf$2;->a:Ljava/lang/Iterable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 81
    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lyf$2;->a(Ljava/util/Map;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/Map;)Lrx/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/Tweet;",
            ">;)",
            "Lrx/c",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/twitter/model/core/Tweet;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lyf$2;->a:Ljava/lang/Iterable;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/util/collection/CollectionUtils;->a(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    .line 85
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 86
    invoke-static {p1}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    .line 88
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lyf$2;->b:Lyf;

    invoke-static {v1}, Lyf;->a(Lyf;)Lauj;

    move-result-object v1

    invoke-interface {v1, v0}, Lauj;->a_(Ljava/lang/Object;)Lrx/c;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Lrx/c;->k()Lrx/c;

    move-result-object v0

    .line 90
    invoke-static {p1}, Lrx/c;->b(Ljava/lang/Object;)Lrx/c;

    move-result-object v1

    invoke-static {}, Lcrb;->a()Lrx/functions/e;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lrx/c;->a(Lrx/c;Lrx/functions/e;)Lrx/c;

    move-result-object v0

    goto :goto_0
.end method
