.class public Lsj;
.super Landroid/support/v4/view/PagerAdapter;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsj$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/Session;

.field private final c:Lsj$a;

.field private final d:Lsm;

.field private final e:Lsp;

.field private final f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

.field private final g:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lsl;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/twitter/media/ui/image/MediaImageView;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Z

.field private j:Lsl;

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsn;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcom/twitter/ui/anim/c$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lsp;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Lsm;Lsj$a;Z)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 51
    iput-object p1, p0, Lsj;->a:Landroid/content/Context;

    .line 52
    iput-object p2, p0, Lsj;->b:Lcom/twitter/library/client/Session;

    .line 53
    iput-object p3, p0, Lsj;->e:Lsp;

    .line 54
    iput-object p4, p0, Lsj;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    .line 55
    iput-object p5, p0, Lsj;->d:Lsm;

    .line 56
    iput-object p6, p0, Lsj;->c:Lsj$a;

    .line 57
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lsj;->g:Landroid/util/SparseArray;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsj;->h:Ljava/util/List;

    .line 59
    iput-boolean p7, p0, Lsj;->i:Z

    .line 60
    return-void
.end method

.method private a(Lsn;Lsl;)V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lsj;->l:Lcom/twitter/ui/anim/c$a;

    iget-object v1, p0, Lsj;->c:Lsj$a;

    invoke-virtual {p2, p1, v0, v1}, Lsl;->a(Lsn;Lcom/twitter/ui/anim/c$a;Lsj$a;)V

    .line 183
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lsn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lsj;->k:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsj;->k:Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/twitter/util/collection/h;->g()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I)Lsn;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lsj;->k:Ljava/util/List;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lsj;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 126
    :cond_0
    const/4 v0, 0x0

    .line 128
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lsj;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsn;

    goto :goto_0
.end method

.method public a(Lcom/twitter/model/core/MediaEntity;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 96
    if-eqz p2, :cond_0

    new-instance v0, Lcom/twitter/library/network/t;

    iget-object v2, p0, Lsj;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->h()Lcom/twitter/model/account/OAuthToken;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/twitter/library/network/t;-><init>(Lcom/twitter/model/account/OAuthToken;)V

    .line 97
    :goto_0
    new-instance v2, Lss;

    .line 98
    invoke-static {p1}, Lcom/twitter/media/util/k;->a(Lcom/twitter/model/core/MediaEntity;)Lcom/twitter/media/request/a$a;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/twitter/media/request/a$a;->a(Ljava/lang/Object;)Lcom/twitter/media/request/b$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/media/request/a$a;

    iget-object v3, p1, Lcom/twitter/model/core/MediaEntity;->y:Ljava/lang/String;

    invoke-direct {v2, v1, p1, v0, v3}, Lss;-><init>(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/MediaEntity;Lcom/twitter/media/request/a$a;Ljava/lang/String;)V

    .line 99
    invoke-static {v2}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 100
    invoke-virtual {p0, v0}, Lsj;->a(Ljava/util/List;)V

    .line 101
    return-void

    :cond_0
    move-object v0, v1

    .line 96
    goto :goto_0
.end method

.method public a(Lcom/twitter/ui/anim/c$a;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lsj;->l:Lcom/twitter/ui/anim/c$a;

    .line 69
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsn;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 76
    iput-object p1, p0, Lsj;->k:Ljava/util/List;

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lsj;->j:Lsl;

    .line 78
    invoke-virtual {p0}, Lsj;->notifyDataSetChanged()V

    .line 79
    return-void
.end method

.method public b(I)Lsl;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lsj;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsl;

    return-object v0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 217
    iget-object v2, p0, Lsj;->g:Landroid/util/SparseArray;

    .line 218
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 219
    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsl;

    invoke-virtual {v0}, Lsl;->c()V

    .line 218
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 221
    :cond_0
    return-void
.end method

.method public b(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/card/property/ImageSpec;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 108
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 109
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/card/property/ImageSpec;

    .line 110
    iget-object v3, v0, Lcom/twitter/model/card/property/ImageSpec;->c:Lcom/twitter/model/card/property/Vector2F;

    .line 111
    new-instance v4, Lss;

    iget-object v5, v0, Lcom/twitter/model/card/property/ImageSpec;->b:Ljava/lang/String;

    iget v6, v3, Lcom/twitter/model/card/property/Vector2F;->x:F

    iget v3, v3, Lcom/twitter/model/card/property/Vector2F;->y:F

    .line 112
    invoke-static {v6, v3}, Lcom/twitter/util/math/Size;->a(FF)Lcom/twitter/util/math/Size;

    move-result-object v3

    invoke-static {v5, v3}, Lcom/twitter/media/request/a;->a(Ljava/lang/String;Lcom/twitter/util/math/Size;)Lcom/twitter/media/request/a$a;

    move-result-object v3

    iget-object v0, v0, Lcom/twitter/model/card/property/ImageSpec;->d:Ljava/lang/String;

    invoke-direct {v4, v7, v7, v3, v0}, Lss;-><init>(Lcom/twitter/model/core/Tweet;Lcom/twitter/model/core/MediaEntity;Lcom/twitter/media/request/a$a;Ljava/lang/String;)V

    .line 111
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 114
    :cond_0
    invoke-virtual {p0, v1}, Lsj;->a(Ljava/util/List;)V

    .line 115
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    .line 224
    iget-object v2, p0, Lsj;->g:Landroid/util/SparseArray;

    .line 225
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 226
    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsl;

    .line 227
    iget v3, v0, Lsl;->a:I

    invoke-virtual {p0, v3}, Lsj;->a(I)Lsn;

    move-result-object v3

    .line 228
    if-eqz v3, :cond_0

    .line 229
    invoke-direct {p0, v3, v0}, Lsj;->a(Lsn;Lsl;)V

    .line 225
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 232
    :cond_1
    return-void
.end method

.method public d()V
    .locals 3

    .prologue
    .line 239
    iget-object v2, p0, Lsj;->g:Landroid/util/SparseArray;

    .line 240
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 241
    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsl;

    invoke-virtual {v0}, Lsl;->d()V

    .line 240
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 243
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lsj;->j:Lsl;

    .line 244
    return-void
.end method

.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 187
    check-cast p3, Landroid/view/View;

    .line 188
    iget-object v0, p0, Lsj;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsl;

    .line 189
    if-eqz v0, :cond_0

    .line 190
    invoke-virtual {v0}, Lsl;->d()V

    .line 191
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 192
    iget-object v0, p0, Lsj;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->remove(I)V

    .line 194
    :cond_0
    return-void
.end method

.method public finishUpdate(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lsj;->j:Lsl;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lsj;->j:Lsl;

    iget v0, v0, Lsl;->a:I

    invoke-virtual {p0, v0}, Lsj;->a(I)Lsn;

    move-result-object v0

    .line 147
    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lsn;->e:Z

    if-eqz v1, :cond_0

    .line 148
    iget-object v1, p0, Lsj;->j:Lsl;

    invoke-direct {p0, v0, v1}, Lsj;->a(Lsn;Lsl;)V

    .line 151
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lsj;->k:Ljava/util/List;

    if-nez v0, :cond_0

    .line 199
    const/4 v0, 0x0

    .line 201
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lsj;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 211
    check-cast p1, Landroid/view/View;

    .line 212
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsl;

    .line 213
    iget v0, v0, Lsl;->a:I

    invoke-virtual {p0}, Lsj;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x2

    goto :goto_0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 166
    iget-object v0, p0, Lsj;->k:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsn;

    .line 167
    if-nez v2, :cond_0

    .line 168
    const/4 v0, 0x0

    .line 177
    :goto_0
    return-object v0

    .line 171
    :cond_0
    iget-object v0, p0, Lsj;->d:Lsm;

    iget-object v1, p0, Lsj;->a:Landroid/content/Context;

    iget-object v5, p0, Lsj;->e:Lsp;

    iget-object v6, p0, Lsj;->f:Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;

    iget-object v7, p0, Lsj;->h:Ljava/util/List;

    iget-boolean v8, p0, Lsj;->i:Z

    move-object v3, p1

    move v4, p2

    invoke-virtual/range {v0 .. v8}, Lsm;->a(Landroid/content/Context;Lsn;Landroid/view/ViewGroup;ILsp;Lcom/twitter/analytics/feature/model/TwitterScribeAssociation;Ljava/util/List;Z)Lsl;

    move-result-object v1

    .line 173
    invoke-direct {p0, v2, v1}, Lsj;->a(Lsn;Lsl;)V

    .line 174
    invoke-virtual {v1}, Lsl;->a()Landroid/view/ViewGroup;

    move-result-object v0

    .line 175
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 176
    iget-object v2, p0, Lsj;->g:Landroid/util/SparseArray;

    invoke-virtual {v2, p2, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 206
    if-ne p2, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 155
    if-nez p3, :cond_0

    .line 156
    iput-object v1, p0, Lsj;->j:Lsl;

    .line 162
    :goto_0
    return-void

    .line 158
    :cond_0
    iget-object v0, p0, Lsj;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsl;

    .line 159
    if-eqz v0, :cond_1

    iget v2, v0, Lsl;->a:I

    .line 160
    invoke-virtual {p0}, Lsj;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_1

    :goto_1
    iput-object v0, p0, Lsj;->j:Lsl;

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method
