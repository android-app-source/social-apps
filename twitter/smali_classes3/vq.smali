.class public Lvq;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/ai;

.field private final b:Lbdh;


# direct methods
.method public constructor <init>(Lcom/twitter/android/ai;Lbdh;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lvq;->a:Lcom/twitter/android/ai;

    .line 45
    iput-object p2, p0, Lvq;->b:Lbdh;

    .line 46
    return-void
.end method

.method private a(JLcom/twitter/model/livevideo/e;I)Lcom/twitter/library/client/m;
    .locals 5

    .prologue
    .line 91
    iget-object v0, p0, Lvq;->b:Lbdh;

    iget-object v1, p3, Lcom/twitter/model/livevideo/e;->b:Ljava/lang/String;

    invoke-virtual {v0, p1, p2, v1}, Lbdh;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 93
    iget-object v1, p0, Lvq;->a:Lcom/twitter/android/ai;

    invoke-virtual {v1}, Lcom/twitter/android/ai;->g()Lcom/twitter/android/ai$a;

    move-result-object v1

    .line 94
    invoke-virtual {v1, v0}, Lcom/twitter/android/ai$a;->c(Ljava/lang/String;)Lcom/twitter/app/common/timeline/c$a;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ai$a;

    iget-object v1, p3, Lcom/twitter/model/livevideo/e;->b:Ljava/lang/String;

    .line 95
    invoke-virtual {v0, v1}, Lcom/twitter/android/ai$a;->a(Ljava/lang/String;)Lcom/twitter/android/ai$a;

    move-result-object v0

    iget-object v1, p3, Lcom/twitter/model/livevideo/e;->d:Ljava/lang/String;

    .line 96
    invoke-virtual {v0, v1}, Lcom/twitter/android/ai$a;->b(Ljava/lang/String;)Lcom/twitter/android/ai$a;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Lcom/twitter/android/ai$a;->a()Lcom/twitter/android/ai;

    move-result-object v0

    .line 99
    new-instance v1, Lcom/twitter/library/client/m$a;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "twitter://live/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p3, Lcom/twitter/model/livevideo/e;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 100
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-class v3, Lcom/twitter/android/livevideo/landing/LiveVideoTimelineFragment;

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/client/m$a;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    .line 102
    invoke-virtual {v1, p4}, Lcom/twitter/library/client/m$a;->b(I)Lcom/twitter/library/client/m$a;

    move-result-object v1

    iget-object v2, p3, Lcom/twitter/model/livevideo/e;->c:Ljava/lang/String;

    .line 103
    invoke-virtual {v1, v2}, Lcom/twitter/library/client/m$a;->a(Ljava/lang/CharSequence;)Lcom/twitter/library/client/m$a;

    move-result-object v1

    .line 104
    invoke-virtual {v1, v0}, Lcom/twitter/library/client/m$a;->a(Lcom/twitter/app/common/base/b;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    iget-object v1, p3, Lcom/twitter/model/livevideo/e;->b:Ljava/lang/String;

    .line 105
    invoke-virtual {v0, v1}, Lcom/twitter/library/client/m$a;->a(Ljava/lang/String;)Lcom/twitter/library/client/m$a;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Lcom/twitter/library/client/m$a;->a()Lcom/twitter/library/client/m;

    move-result-object v0

    .line 99
    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/livevideo/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    new-instance v0, Lcom/twitter/model/livevideo/e$a;

    invoke-direct {v0}, Lcom/twitter/model/livevideo/e$a;-><init>()V

    const-string/jumbo v1, ""

    .line 81
    invoke-virtual {v0, v1}, Lcom/twitter/model/livevideo/e$a;->a(Ljava/lang/String;)Lcom/twitter/model/livevideo/e$a;

    move-result-object v1

    const-string/jumbo v0, "Tweets"

    .line 82
    invoke-static {p1, v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/twitter/model/livevideo/e$a;->b(Ljava/lang/String;)Lcom/twitter/model/livevideo/e$a;

    move-result-object v1

    const-string/jumbo v0, ""

    .line 83
    invoke-static {p2, v0}, Lcom/twitter/util/object/h;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/twitter/model/livevideo/e$a;->c(Ljava/lang/String;)Lcom/twitter/model/livevideo/e$a;

    move-result-object v0

    .line 84
    invoke-virtual {v0}, Lcom/twitter/model/livevideo/e$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/livevideo/e;

    .line 86
    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/twitter/model/livevideo/b;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/livevideo/b;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/model/livevideo/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    const-string/jumbo v0, "live_video_multitimeline_enabled"

    .line 68
    invoke-static {v0}, Lcoj;->a(Ljava/lang/String;)Z

    move-result v0

    .line 70
    iget-object v1, p1, Lcom/twitter/model/livevideo/b;->l:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    iget-object v0, p1, Lcom/twitter/model/livevideo/b;->f:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/model/livevideo/b;->h:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lvq;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 74
    :goto_0
    return-object v0

    :cond_0
    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/twitter/model/livevideo/b;->l:Ljava/util/List;

    goto :goto_0

    :cond_1
    iget-object v0, p1, Lcom/twitter/model/livevideo/b;->l:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/util/collection/h;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/model/livevideo/b;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/livevideo/b;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/twitter/library/client/m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    invoke-static {}, Lcom/twitter/util/collection/h;->e()Lcom/twitter/util/collection/h;

    move-result-object v3

    .line 55
    invoke-direct {p0, p1}, Lvq;->b(Lcom/twitter/model/livevideo/b;)Ljava/util/List;

    move-result-object v1

    .line 56
    const/4 v0, 0x0

    .line 57
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/model/livevideo/e;

    .line 58
    iget-wide v6, p1, Lcom/twitter/model/livevideo/b;->b:J

    add-int/lit8 v2, v1, 0x1

    invoke-direct {p0, v6, v7, v0, v1}, Lvq;->a(JLcom/twitter/model/livevideo/e;I)Lcom/twitter/library/client/m;

    move-result-object v0

    .line 59
    invoke-virtual {v3, v0}, Lcom/twitter/util/collection/h;->c(Ljava/lang/Object;)Lcom/twitter/util/collection/h;

    move v1, v2

    .line 60
    goto :goto_0

    .line 61
    :cond_0
    invoke-virtual {v3}, Lcom/twitter/util/collection/h;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method
