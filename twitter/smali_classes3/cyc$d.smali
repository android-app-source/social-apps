.class Lcyc$d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/google/android/gms/location/f;
.implements Lczq$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcyc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcyc;


# direct methods
.method constructor <init>(Lcyc;)V
    .locals 0

    .prologue
    .line 1297
    iput-object p1, p0, Lcyc$d;->a:Lcyc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/location/Location;)V
    .locals 3

    .prologue
    .line 1301
    const-string/jumbo v0, "PeriscopeBroadcaster"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onLocationChanged "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ltv/periscope/android/util/t;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1302
    iget-object v0, p0, Lcyc$d;->a:Lcyc;

    invoke-static {v0, p1}, Lcyc;->a(Lcyc;Landroid/location/Location;)Landroid/location/Location;

    .line 1303
    iget-object v0, p0, Lcyc$d;->a:Lcyc;

    invoke-static {v0}, Lcyc;->a(Lcyc;)Lcxy;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcxy;->a(Landroid/location/Location;)V

    .line 1304
    return-void
.end method

.method public a(Lcom/google/android/gms/location/LocationSettingsResult;)V
    .locals 3

    .prologue
    .line 1317
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/location/LocationSettingsResult;->b()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    iget-object v1, p0, Lcyc$d;->a:Lcyc;

    invoke-static {v1}, Lcyc;->l(Lcyc;)Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/api/Status;->a(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1321
    :goto_0
    return-void

    .line 1318
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a([Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1308
    iget-object v0, p0, Lcyc$d;->a:Lcyc;

    invoke-static {v0}, Lcyc;->l(Lcyc;)Landroid/app/Activity;

    move-result-object v0

    const/16 v1, 0x64

    invoke-static {v0, p1, v1}, Ltv/periscope/android/permissions/b;->a(Landroid/app/Activity;[Ljava/lang/String;I)V

    .line 1309
    return-void
.end method
