.class public Lzn;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Lcom/twitter/android/moments/ui/fullscreen/s;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lzn$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/twitter/model/moments/viewmodels/a;

.field private final b:Lbse;

.field private final c:Lrx/j;

.field private d:Lcom/twitter/model/moments/Moment;

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(Lcom/twitter/model/moments/Moment;Lrx/c;Lcom/twitter/model/moments/viewmodels/a;Lbse;Landroid/os/Bundle;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/twitter/model/moments/Moment;",
            "Lrx/c",
            "<",
            "Lcom/twitter/model/moments/Moment;",
            ">;",
            "Lcom/twitter/model/moments/viewmodels/a;",
            "Lbse;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    const/high16 v0, -0x80000000

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput v0, p0, Lzn;->e:I

    .line 60
    iput v0, p0, Lzn;->f:I

    .line 65
    iput-object p3, p0, Lzn;->a:Lcom/twitter/model/moments/viewmodels/a;

    .line 66
    iput-object p1, p0, Lzn;->d:Lcom/twitter/model/moments/Moment;

    .line 67
    iput-object p4, p0, Lzn;->b:Lbse;

    .line 69
    if-eqz p5, :cond_0

    .line 70
    const-string/jumbo v0, "state_previous_selected_page"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lzn;->e:I

    .line 71
    const-string/jumbo v0, "state_selected_page"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lzn;->f:I

    .line 74
    :cond_0
    new-instance v0, Lzn$1;

    invoke-direct {v0, p0}, Lzn$1;-><init>(Lzn;)V

    invoke-virtual {p2, v0}, Lrx/c;->b(Lrx/i;)Lrx/j;

    move-result-object v0

    iput-object v0, p0, Lzn;->c:Lrx/j;

    .line 82
    return-void
.end method

.method static synthetic a(Lzn;)Lcom/twitter/model/moments/Moment;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lzn;->d:Lcom/twitter/model/moments/Moment;

    return-object v0
.end method

.method static synthetic a(Lzn;Lcom/twitter/model/moments/Moment;)Lcom/twitter/model/moments/Moment;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lzn;->d:Lcom/twitter/model/moments/Moment;

    return-object p1
.end method

.method private a(Ljava/lang/String;Lcom/twitter/analytics/feature/model/MomentScribeDetails;)V
    .locals 3

    .prologue
    .line 153
    new-instance v0, Lcom/twitter/analytics/feature/model/ClientEventLog;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-direct {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;-><init>([Ljava/lang/String;)V

    .line 154
    invoke-static {p2}, Lcom/twitter/library/scribe/b;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails;)Lcom/twitter/analytics/feature/model/TwitterScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/analytics/feature/model/ClientEventLog;->a(Lcom/twitter/analytics/model/ScribeItem;)Lcom/twitter/analytics/model/ScribeLog;

    move-result-object v0

    .line 153
    invoke-static {v0}, Lcpm;->a(Lcpk;)V

    .line 155
    return-void
.end method

.method static synthetic a(Lzn;Ljava/lang/String;Lcom/twitter/analytics/feature/model/MomentScribeDetails;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lzn;->a(Ljava/lang/String;Lcom/twitter/analytics/feature/model/MomentScribeDetails;)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 145
    invoke-direct {p0}, Lzn;->i()Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v1

    .line 146
    invoke-direct {p0}, Lzn;->k()Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;

    move-result-object v0

    .line 147
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;

    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    .line 148
    if-eqz p1, :cond_0

    const-string/jumbo v1, "moments:capsule::navigate:forward"

    .line 149
    :goto_0
    invoke-direct {p0, v1, v0}, Lzn;->a(Ljava/lang/String;Lcom/twitter/analytics/feature/model/MomentScribeDetails;)V

    .line 150
    return-void

    .line 148
    :cond_0
    const-string/jumbo v1, "moments:capsule::navigate:back"

    goto :goto_0
.end method

.method private i()Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;
    .locals 4

    .prologue
    .line 159
    new-instance v1, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-direct {v1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;-><init>()V

    .line 160
    iget-object v0, p0, Lzn;->d:Lcom/twitter/model/moments/Moment;

    iget-wide v2, v0, Lcom/twitter/model/moments/Moment;->b:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    iget-object v2, p0, Lzn;->d:Lcom/twitter/model/moments/Moment;

    iget-boolean v2, v2, Lcom/twitter/model/moments/Moment;->k:Z

    .line 161
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(Ljava/lang/Boolean;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    .line 162
    iget-object v0, p0, Lzn;->a:Lcom/twitter/model/moments/viewmodels/a;

    iget v2, p0, Lzn;->f:I

    invoke-virtual {v0, v2}, Lcom/twitter/model/moments/viewmodels/a;->c(I)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 163
    instance-of v2, v0, Lcom/twitter/model/moments/viewmodels/n;

    if-eqz v2, :cond_0

    .line 164
    check-cast v0, Lcom/twitter/model/moments/viewmodels/n;

    .line 165
    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/n;->t()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->b(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    .line 166
    iget-object v2, p0, Lzn;->a:Lcom/twitter/model/moments/viewmodels/a;

    invoke-static {v0, v2}, Lzy;->a(Lcom/twitter/model/moments/viewmodels/MomentPage;Lcom/twitter/model/moments/viewmodels/a;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails$Metadata;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    .line 168
    :cond_0
    return-object v1
.end method

.method private j()Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 173
    new-instance v3, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;

    invoke-direct {v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;-><init>()V

    .line 174
    iget v0, p0, Lzn;->f:I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->a(Z)Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;

    .line 175
    iget v0, p0, Lzn;->f:I

    add-int/lit8 v0, v0, 0x1

    iget-object v4, p0, Lzn;->a:Lcom/twitter/model/moments/viewmodels/a;

    invoke-virtual {v4}, Lcom/twitter/model/moments/viewmodels/a;->b()I

    move-result v4

    if-ne v0, v4, :cond_1

    :goto_1
    invoke-virtual {v3, v1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->b(Z)Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;

    .line 176
    return-object v3

    :cond_0
    move v0, v2

    .line 174
    goto :goto_0

    :cond_1
    move v1, v2

    .line 175
    goto :goto_1
.end method

.method private k()Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;
    .locals 4

    .prologue
    .line 181
    invoke-direct {p0}, Lzn;->j()Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;

    move-result-object v1

    .line 182
    iget v0, p0, Lzn;->e:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_0

    .line 183
    iget-object v0, p0, Lzn;->a:Lcom/twitter/model/moments/viewmodels/a;

    iget v2, p0, Lzn;->e:I

    invoke-virtual {v0, v2}, Lcom/twitter/model/moments/viewmodels/a;->c(I)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 184
    instance-of v2, v0, Lcom/twitter/model/moments/viewmodels/n;

    if-eqz v2, :cond_0

    .line 185
    check-cast v0, Lcom/twitter/model/moments/viewmodels/n;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/n;->t()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->a(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;

    .line 188
    :cond_0
    iget v0, p0, Lzn;->f:I

    if-ltz v0, :cond_1

    .line 189
    iget-object v0, p0, Lzn;->a:Lcom/twitter/model/moments/viewmodels/a;

    iget v2, p0, Lzn;->f:I

    invoke-virtual {v0, v2}, Lcom/twitter/model/moments/viewmodels/a;->c(I)Lcom/twitter/model/moments/viewmodels/MomentPage;

    move-result-object v0

    .line 190
    instance-of v2, v0, Lcom/twitter/model/moments/viewmodels/n;

    if-eqz v2, :cond_1

    .line 191
    check-cast v0, Lcom/twitter/model/moments/viewmodels/n;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/n;->t()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->b(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;

    .line 194
    :cond_1
    return-object v1
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 85
    invoke-direct {p0}, Lzn;->i()Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v1

    .line 86
    invoke-direct {p0}, Lzn;->j()Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;

    invoke-virtual {v1, v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails$Transition;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    .line 88
    const-string/jumbo v1, "moments:capsule::capsule_page:impression"

    invoke-direct {p0, v1, v0}, Lzn;->a(Ljava/lang/String;Lcom/twitter/analytics/feature/model/MomentScribeDetails;)V

    .line 89
    return-void
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 108
    const-string/jumbo v1, "moments:capsule:::user_action"

    invoke-direct {p0}, Lzn;->i()Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    new-instance v2, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;

    invoke-direct {v2, p1}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;-><init>(I)V

    invoke-virtual {v0, v2}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(Lcom/twitter/analytics/feature/model/MomentScribeDetails$Engagement;)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    invoke-direct {p0, v1, v0}, Lzn;->a(Ljava/lang/String;Lcom/twitter/analytics/feature/model/MomentScribeDetails;)V

    .line 109
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 206
    const-string/jumbo v0, "state_previous_selected_page"

    iget v1, p0, Lzn;->e:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 207
    return-void
.end method

.method public a(Lcom/twitter/model/moments/Moment;)V
    .locals 4

    .prologue
    .line 112
    iget-object v0, p0, Lzn;->b:Lbse;

    iget-object v1, p0, Lzn;->d:Lcom/twitter/model/moments/Moment;

    iget-wide v2, v1, Lcom/twitter/model/moments/Moment;->b:J

    invoke-virtual {v0, v2, v3}, Lbse;->a(J)Lrx/c;

    move-result-object v0

    new-instance v1, Lzn$2;

    invoke-direct {v1, p0, p1}, Lzn$2;-><init>(Lzn;Lcom/twitter/model/moments/Moment;)V

    invoke-virtual {v0, v1}, Lrx/c;->b(Lrx/i;)Lrx/j;

    .line 129
    return-void
.end method

.method a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 100
    const-string/jumbo v0, "moments:capsule:%s:moment:follow"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lzn;->i()Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    invoke-direct {p0, v1, v0}, Lzn;->a(Ljava/lang/String;Lcom/twitter/analytics/feature/model/MomentScribeDetails;)V

    .line 101
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lzn;->a(Z)V

    .line 93
    return-void
.end method

.method b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 104
    const-string/jumbo v0, "moments:capsule:%s:moment:unfollow"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lzn;->i()Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    invoke-direct {p0, v1, v0}, Lzn;->a(Ljava/lang/String;Lcom/twitter/analytics/feature/model/MomentScribeDetails;)V

    .line 105
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lzn;->a(Z)V

    .line 97
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 133
    const-string/jumbo v1, "moments:capsule::cta:click"

    invoke-direct {p0}, Lzn;->i()Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    invoke-direct {p0, v1, v0}, Lzn;->a(Ljava/lang/String;Lcom/twitter/analytics/feature/model/MomentScribeDetails;)V

    .line 134
    return-void
.end method

.method e()V
    .locals 4
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 138
    new-instance v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    invoke-direct {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;-><init>()V

    iget-object v1, p0, Lzn;->d:Lcom/twitter/model/moments/Moment;

    iget-wide v2, v1, Lcom/twitter/model/moments/Moment;->b:J

    .line 139
    invoke-virtual {v0, v2, v3}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->a(J)Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;

    move-result-object v0

    .line 140
    invoke-virtual {v0}, Lcom/twitter/analytics/feature/model/MomentScribeDetails$a;->q()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/analytics/feature/model/MomentScribeDetails;

    .line 141
    const-string/jumbo v1, "moments:capsule:::open"

    invoke-direct {p0, v1, v0}, Lzn;->a(Ljava/lang/String;Lcom/twitter/analytics/feature/model/MomentScribeDetails;)V

    .line 142
    return-void
.end method

.method public f()Lzq;
    .locals 2

    .prologue
    .line 198
    new-instance v0, Lzn$a;

    const-string/jumbo v1, "start"

    invoke-direct {v0, p0, v1}, Lzn$a;-><init>(Lzn;Ljava/lang/String;)V

    return-object v0
.end method

.method public g()Lzq;
    .locals 2

    .prologue
    .line 202
    new-instance v0, Lzn$a;

    const-string/jumbo v1, "end"

    invoke-direct {v0, p0, v1}, Lzn$a;-><init>(Lzn;Ljava/lang/String;)V

    return-object v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lzn;->c:Lrx/j;

    invoke-interface {v0}, Lrx/j;->B_()V

    .line 261
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0

    .prologue
    .line 257
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0

    .prologue
    .line 230
    return-void
.end method

.method public onPageSelected(I)V
    .locals 2

    .prologue
    .line 234
    iget v0, p0, Lzn;->f:I

    if-eq p1, v0, :cond_1

    .line 235
    iget v0, p0, Lzn;->f:I

    iput v0, p0, Lzn;->e:I

    .line 236
    iput p1, p0, Lzn;->f:I

    .line 237
    if-ltz p1, :cond_1

    iget-object v0, p0, Lzn;->a:Lcom/twitter/model/moments/viewmodels/a;

    invoke-virtual {v0}, Lcom/twitter/model/moments/viewmodels/a;->b()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 238
    invoke-virtual {p0}, Lzn;->a()V

    .line 239
    iget v0, p0, Lzn;->e:I

    if-ltz v0, :cond_0

    iget v0, p0, Lzn;->e:I

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_2

    .line 243
    :cond_0
    invoke-virtual {p0}, Lzn;->e()V

    .line 253
    :cond_1
    :goto_0
    return-void

    .line 245
    :cond_2
    iget v0, p0, Lzn;->e:I

    iget v1, p0, Lzn;->f:I

    if-le v0, v1, :cond_3

    .line 246
    invoke-virtual {p0}, Lzn;->b()V

    goto :goto_0

    .line 248
    :cond_3
    invoke-virtual {p0}, Lzn;->c()V

    goto :goto_0
.end method
