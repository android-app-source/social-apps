.class public Lrx/g;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lrx/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field final a:Lrx/g$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/g$a",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lrx/g$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/g$a",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    invoke-static {p1}, Lcwl;->a(Lrx/g$a;)Lrx/g$a;

    move-result-object v0

    iput-object v0, p0, Lrx/g;->a:Lrx/g$a;

    .line 69
    return-void
.end method

.method public static a(Lrx/g;Lrx/g;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/g",
            "<+TT;>;",
            "Lrx/g",
            "<+TT;>;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 231
    invoke-static {p0}, Lrx/g;->b(Lrx/g;)Lrx/c;

    move-result-object v0

    invoke-static {p1}, Lrx/g;->b(Lrx/g;)Lrx/c;

    move-result-object v1

    invoke-static {v0, v1}, Lrx/c;->b(Lrx/c;Lrx/c;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lrx/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 636
    invoke-static {p0}, Lrx/internal/util/i;->b(Ljava/lang/Object;)Lrx/internal/util/i;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Callable;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<+TT;>;)",
            "Lrx/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 573
    new-instance v0, Lrx/internal/operators/av;

    invoke-direct {v0, p0}, Lrx/internal/operators/av;-><init>(Ljava/util/concurrent/Callable;)V

    invoke-static {v0}, Lrx/g;->a(Lrx/g$a;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lrx/g$a;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/g$a",
            "<TT;>;)",
            "Lrx/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 115
    new-instance v0, Lrx/g;

    invoke-direct {v0, p0}, Lrx/g;-><init>(Lrx/g$a;)V

    return-object v0
.end method

.method public static a(Lrx/g;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/g",
            "<+",
            "Lrx/g",
            "<+TT;>;>;)",
            "Lrx/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 659
    instance-of v0, p0, Lrx/internal/util/i;

    if-eqz v0, :cond_0

    .line 660
    check-cast p0, Lrx/internal/util/i;

    invoke-static {}, Lrx/internal/util/UtilityFunctions;->b()Lrx/functions/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lrx/internal/util/i;->e(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    .line 662
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lrx/g$5;

    invoke-direct {v0, p0}, Lrx/g$5;-><init>(Lrx/g;)V

    invoke-static {v0}, Lrx/g;->a(Lrx/g$a;)Lrx/g;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lrx/g;Lrx/g;Lrx/functions/e;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/g",
            "<+TT1;>;",
            "Lrx/g",
            "<+TT2;>;",
            "Lrx/functions/e",
            "<-TT1;-TT2;+TR;>;)",
            "Lrx/g",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 958
    const/4 v0, 0x2

    new-array v0, v0, [Lrx/g;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    new-instance v1, Lrx/g$6;

    invoke-direct {v1, p2}, Lrx/g$6;-><init>(Lrx/functions/e;)V

    invoke-static {v0, v1}, Lrx/internal/operators/bb;->a([Lrx/g;Lrx/functions/h;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method private a(Lrx/i;Z)Lrx/j;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/i",
            "<-TT;>;Z)",
            "Lrx/j;"
        }
    .end annotation

    .prologue
    .line 1730
    if-eqz p2, :cond_0

    .line 1732
    :try_start_0
    invoke-virtual {p1}, Lrx/i;->c()V

    .line 1734
    :cond_0
    iget-object v0, p0, Lrx/g;->a:Lrx/g$a;

    invoke-static {p0, v0}, Lcwl;->a(Lrx/g;Lrx/g$a;)Lrx/g$a;

    move-result-object v0

    invoke-static {p1}, Lrx/internal/operators/ax;->a(Lrx/i;)Lrx/h;

    move-result-object v1

    invoke-interface {v0, v1}, Lrx/g$a;->call(Ljava/lang/Object;)V

    .line 1735
    invoke-static {p1}, Lcwl;->b(Lrx/j;)Lrx/j;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1752
    :goto_0
    return-object v0

    .line 1736
    :catch_0
    move-exception v0

    .line 1738
    invoke-static {v0}, Lrx/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 1741
    :try_start_1
    invoke-static {v0}, Lcwl;->d(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {p1, v1}, Lrx/i;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 1752
    invoke-static {}, Lcwy;->b()Lrx/j;

    move-result-object v0

    goto :goto_0

    .line 1742
    :catch_1
    move-exception v1

    .line 1743
    invoke-static {v1}, Lrx/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 1746
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Error occurred attempting to subscribe ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, "] and then again while trying to pass to onError."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1748
    invoke-static {v2}, Lcwl;->d(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 1750
    throw v2
.end method

.method private static b(Lrx/g;)Lrx/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/g",
            "<TT;>;)",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 205
    new-instance v0, Lrx/internal/operators/bc;

    iget-object v1, p0, Lrx/g;->a:Lrx/g$a;

    invoke-direct {v0, v1}, Lrx/internal/operators/bc;-><init>(Lrx/g$a;)V

    invoke-static {v0}, Lrx/c;->a(Lrx/c$a;)Lrx/c;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1367
    invoke-virtual {p0}, Lrx/g;->c()Lrx/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lrx/c;->a(I)Lrx/c;

    move-result-object v0

    invoke-virtual {v0}, Lrx/c;->b()Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lrx/c$b;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/c$b",
            "<+TR;-TT;>;)",
            "Lrx/g",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 151
    new-instance v0, Lrx/internal/operators/ax;

    iget-object v1, p0, Lrx/g;->a:Lrx/g$a;

    invoke-direct {v0, v1, p1}, Lrx/internal/operators/ax;-><init>(Lrx/g$a;Lrx/c$b;)V

    invoke-static {v0}, Lrx/g;->a(Lrx/g$a;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lrx/f;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/f;",
            ")",
            "Lrx/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1516
    instance-of v0, p0, Lrx/internal/util/i;

    if-eqz v0, :cond_0

    .line 1517
    check-cast p0, Lrx/internal/util/i;

    invoke-virtual {p0, p1}, Lrx/internal/util/i;->c(Lrx/f;)Lrx/g;

    move-result-object v0

    .line 1522
    :goto_0
    return-object v0

    .line 1519
    :cond_0
    if-nez p1, :cond_1

    .line 1520
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "scheduler is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1522
    :cond_1
    new-instance v0, Lrx/internal/operators/ay;

    iget-object v1, p0, Lrx/g;->a:Lrx/g$a;

    invoke-direct {v0, v1, p1}, Lrx/internal/operators/ay;-><init>(Lrx/g$a;Lrx/f;)V

    invoke-static {v0}, Lrx/g;->a(Lrx/g$a;)Lrx/g;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lrx/functions/a;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/a;",
            ")",
            "Lrx/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2325
    new-instance v0, Lrx/internal/operators/au;

    iget-object v1, p0, Lrx/g;->a:Lrx/g$a;

    invoke-direct {v0, v1, p1}, Lrx/internal/operators/au;-><init>(Lrx/g$a;Lrx/functions/a;)V

    invoke-static {v0}, Lrx/g;->a(Lrx/g$a;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lrx/functions/d;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/d",
            "<-TT;+",
            "Lrx/g",
            "<+TR;>;>;)",
            "Lrx/g",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 1407
    instance-of v0, p0, Lrx/internal/util/i;

    if-eqz v0, :cond_0

    .line 1408
    check-cast p0, Lrx/internal/util/i;

    invoke-virtual {p0, p1}, Lrx/internal/util/i;->e(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    .line 1410
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lrx/g;->c(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    invoke-static {v0}, Lrx/g;->a(Lrx/g;)Lrx/g;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lrx/g;Lrx/functions/e;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T2:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/g",
            "<+TT2;>;",
            "Lrx/functions/e",
            "<-TT;-TT2;+TR;>;)",
            "Lrx/g",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 2211
    invoke-static {p0, p1, p2}, Lrx/g;->a(Lrx/g;Lrx/g;Lrx/functions/e;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lrx/d;)Lrx/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/d",
            "<-TT;>;)",
            "Lrx/j;"
        }
    .end annotation

    .prologue
    .line 1764
    if-nez p1, :cond_0

    .line 1765
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "observer is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1767
    :cond_0
    new-instance v0, Lrx/g$2;

    invoke-direct {v0, p0, p1}, Lrx/g$2;-><init>(Lrx/g;Lrx/d;)V

    invoke-virtual {p0, v0}, Lrx/g;->a(Lrx/h;)Lrx/j;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lrx/functions/b;)Lrx/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/b",
            "<-TT;>;)",
            "Lrx/j;"
        }
    .end annotation

    .prologue
    .line 1656
    invoke-static {}, Lrx/functions/Actions;->b()Lrx/functions/b;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lrx/g;->a(Lrx/functions/b;Lrx/functions/b;)Lrx/j;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lrx/functions/b;Lrx/functions/b;)Lrx/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/b",
            "<-TT;>;",
            "Lrx/functions/b",
            "<",
            "Ljava/lang/Throwable;",
            ">;)",
            "Lrx/j;"
        }
    .end annotation

    .prologue
    .line 1679
    if-nez p1, :cond_0

    .line 1680
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "onSuccess can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1682
    :cond_0
    if-nez p2, :cond_1

    .line 1683
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "onError can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1686
    :cond_1
    new-instance v0, Lrx/g$1;

    invoke-direct {v0, p0, p2, p1}, Lrx/g$1;-><init>(Lrx/g;Lrx/functions/b;Lrx/functions/b;)V

    invoke-virtual {p0, v0}, Lrx/g;->a(Lrx/h;)Lrx/j;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lrx/h;)Lrx/j;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/h",
            "<-TT;>;)",
            "Lrx/j;"
        }
    .end annotation

    .prologue
    .line 1872
    if-nez p1, :cond_0

    .line 1873
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "te is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1876
    :cond_0
    :try_start_0
    iget-object v0, p0, Lrx/g;->a:Lrx/g$a;

    invoke-static {p0, v0}, Lcwl;->a(Lrx/g;Lrx/g$a;)Lrx/g$a;

    move-result-object v0

    invoke-interface {v0, p1}, Lrx/g$a;->call(Ljava/lang/Object;)V

    .line 1877
    invoke-static {p1}, Lcwl;->b(Lrx/j;)Lrx/j;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1893
    :goto_0
    return-object v0

    .line 1878
    :catch_0
    move-exception v0

    .line 1879
    invoke-static {v0}, Lrx/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 1882
    :try_start_1
    invoke-static {v0}, Lcwl;->d(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {p1, v1}, Lrx/h;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 1893
    invoke-static {}, Lcwy;->a()Lrx/j;

    move-result-object v0

    goto :goto_0

    .line 1883
    :catch_1
    move-exception v1

    .line 1884
    invoke-static {v1}, Lrx/exceptions/a;->b(Ljava/lang/Throwable;)V

    .line 1887
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Error occurred attempting to subscribe ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, "] and then again while trying to pass to onError."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1889
    invoke-static {v2}, Lcwl;->d(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 1891
    throw v2
.end method

.method public final a(Lrx/i;)Lrx/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/i",
            "<-TT;>;)",
            "Lrx/j;"
        }
    .end annotation

    .prologue
    .line 1817
    if-nez p1, :cond_0

    .line 1818
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "observer can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1822
    :cond_0
    invoke-virtual {p1}, Lrx/i;->c()V

    .line 1829
    instance-of v0, p1, Lcwf;

    if-nez v0, :cond_1

    .line 1831
    new-instance v0, Lcwf;

    invoke-direct {v0, p1}, Lcwf;-><init>(Lrx/i;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lrx/g;->a(Lrx/i;Z)Lrx/j;

    move-result-object v0

    .line 1833
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lrx/g;->a(Lrx/i;Z)Lrx/j;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lrx/functions/d;)Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/d",
            "<-TT;+",
            "Lrx/c",
            "<+TR;>;>;)",
            "Lrx/c",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 1431
    invoke-virtual {p0, p1}, Lrx/g;->c(Lrx/functions/d;)Lrx/g;

    move-result-object v0

    invoke-static {v0}, Lrx/g;->b(Lrx/g;)Lrx/c;

    move-result-object v0

    invoke-static {v0}, Lrx/c;->b(Lrx/c;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lrx/f;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/f;",
            ")",
            "Lrx/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1914
    instance-of v0, p0, Lrx/internal/util/i;

    if-eqz v0, :cond_0

    .line 1915
    check-cast p0, Lrx/internal/util/i;

    invoke-virtual {p0, p1}, Lrx/internal/util/i;->c(Lrx/f;)Lrx/g;

    move-result-object v0

    .line 1917
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lrx/g$3;

    invoke-direct {v0, p0, p1}, Lrx/g$3;-><init>(Lrx/g;Lrx/f;)V

    invoke-static {v0}, Lrx/g;->a(Lrx/g$a;)Lrx/g;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lrx/functions/b;)Lrx/g;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/b",
            "<",
            "Ljava/lang/Throwable;",
            ">;)",
            "Lrx/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2234
    if-nez p1, :cond_0

    .line 2235
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "onError is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2238
    :cond_0
    new-instance v0, Lrx/internal/operators/at;

    invoke-static {}, Lrx/functions/Actions;->a()Lrx/functions/Actions$b;

    move-result-object v1

    new-instance v2, Lrx/g$4;

    invoke-direct {v2, p0, p1}, Lrx/g$4;-><init>(Lrx/g;Lrx/functions/b;)V

    invoke-direct {v0, p0, v1, v2}, Lrx/internal/operators/at;-><init>(Lrx/g;Lrx/functions/b;Lrx/functions/b;)V

    invoke-static {v0}, Lrx/g;->a(Lrx/g$a;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lrx/j;
    .locals 2

    .prologue
    .line 1636
    invoke-static {}, Lrx/functions/Actions;->a()Lrx/functions/Actions$b;

    move-result-object v0

    invoke-static {}, Lrx/functions/Actions;->b()Lrx/functions/b;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lrx/g;->a(Lrx/functions/b;Lrx/functions/b;)Lrx/j;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lrx/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2043
    invoke-static {p0}, Lrx/g;->b(Lrx/g;)Lrx/c;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lrx/functions/b;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/b",
            "<-TT;>;)",
            "Lrx/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2297
    if-nez p1, :cond_0

    .line 2298
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "onSuccess is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2301
    :cond_0
    invoke-static {}, Lrx/functions/Actions;->a()Lrx/functions/Actions$b;

    move-result-object v0

    .line 2302
    new-instance v1, Lrx/internal/operators/at;

    invoke-direct {v1, p0, p1, v0}, Lrx/internal/operators/at;-><init>(Lrx/g;Lrx/functions/b;Lrx/functions/b;)V

    invoke-static {v1}, Lrx/g;->a(Lrx/g$a;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lrx/functions/d;)Lrx/g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/d",
            "<-TT;+TR;>;)",
            "Lrx/g",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 1473
    new-instance v0, Lrx/internal/operators/ba;

    invoke-direct {v0, p0, p1}, Lrx/internal/operators/ba;-><init>(Lrx/g;Lrx/functions/d;)V

    invoke-static {v0}, Lrx/g;->a(Lrx/g$a;)Lrx/g;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcwt;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcwt",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2183
    invoke-static {p0}, Lcwt;->a(Lrx/g;)Lcwt;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lrx/functions/d;)Lrx/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/d",
            "<",
            "Ljava/lang/Throwable;",
            "+TT;>;)",
            "Lrx/g",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1552
    new-instance v0, Lrx/internal/operators/az;

    iget-object v1, p0, Lrx/g;->a:Lrx/g$a;

    invoke-direct {v0, v1, p1}, Lrx/internal/operators/az;-><init>(Lrx/g$a;Lrx/functions/d;)V

    invoke-static {v0}, Lrx/g;->a(Lrx/g$a;)Lrx/g;

    move-result-object v0

    return-object v0
.end method
