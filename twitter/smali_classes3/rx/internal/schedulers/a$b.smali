.class final Lrx/internal/schedulers/a$b;
.super Lrx/f$a;
.source "Twttr"

# interfaces
.implements Lrx/functions/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lrx/internal/schedulers/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation


# instance fields
.field final a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final b:Lcwv;

.field private final c:Lrx/internal/schedulers/a$a;

.field private final d:Lrx/internal/schedulers/a$c;


# direct methods
.method constructor <init>(Lrx/internal/schedulers/a$a;)V
    .locals 1

    .prologue
    .line 185
    invoke-direct {p0}, Lrx/f$a;-><init>()V

    .line 180
    new-instance v0, Lcwv;

    invoke-direct {v0}, Lcwv;-><init>()V

    iput-object v0, p0, Lrx/internal/schedulers/a$b;->b:Lcwv;

    .line 186
    iput-object p1, p0, Lrx/internal/schedulers/a$b;->c:Lrx/internal/schedulers/a$a;

    .line 187
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lrx/internal/schedulers/a$b;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 188
    invoke-virtual {p1}, Lrx/internal/schedulers/a$a;->a()Lrx/internal/schedulers/a$c;

    move-result-object v0

    iput-object v0, p0, Lrx/internal/schedulers/a$b;->d:Lrx/internal/schedulers/a$c;

    .line 189
    return-void
.end method


# virtual methods
.method public B_()V
    .locals 3

    .prologue
    .line 193
    iget-object v0, p0, Lrx/internal/schedulers/a$b;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lrx/internal/schedulers/a$b;->d:Lrx/internal/schedulers/a$c;

    invoke-virtual {v0, p0}, Lrx/internal/schedulers/a$c;->a(Lrx/functions/a;)Lrx/j;

    .line 199
    :cond_0
    iget-object v0, p0, Lrx/internal/schedulers/a$b;->b:Lcwv;

    invoke-virtual {v0}, Lcwv;->B_()V

    .line 200
    return-void
.end method

.method public a(Lrx/functions/a;)Lrx/j;
    .locals 3

    .prologue
    .line 214
    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v1, v2}, Lrx/internal/schedulers/a$b;->a(Lrx/functions/a;JLjava/util/concurrent/TimeUnit;)Lrx/j;

    move-result-object v0

    return-object v0
.end method

.method public a(Lrx/functions/a;JLjava/util/concurrent/TimeUnit;)Lrx/j;
    .locals 2

    .prologue
    .line 219
    iget-object v0, p0, Lrx/internal/schedulers/a$b;->b:Lcwv;

    invoke-virtual {v0}, Lcwv;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    invoke-static {}, Lcwy;->b()Lrx/j;

    move-result-object v0

    .line 235
    :goto_0
    return-object v0

    .line 224
    :cond_0
    iget-object v0, p0, Lrx/internal/schedulers/a$b;->d:Lrx/internal/schedulers/a$c;

    new-instance v1, Lrx/internal/schedulers/a$b$1;

    invoke-direct {v1, p0, p1}, Lrx/internal/schedulers/a$b$1;-><init>(Lrx/internal/schedulers/a$b;Lrx/functions/a;)V

    invoke-virtual {v0, v1, p2, p3, p4}, Lrx/internal/schedulers/a$c;->b(Lrx/functions/a;JLjava/util/concurrent/TimeUnit;)Lrx/internal/schedulers/ScheduledAction;

    move-result-object v0

    .line 233
    iget-object v1, p0, Lrx/internal/schedulers/a$b;->b:Lcwv;

    invoke-virtual {v1, v0}, Lcwv;->a(Lrx/j;)V

    .line 234
    iget-object v1, p0, Lrx/internal/schedulers/a$b;->b:Lcwv;

    invoke-virtual {v0, v1}, Lrx/internal/schedulers/ScheduledAction;->a(Lcwv;)V

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lrx/internal/schedulers/a$b;->c:Lrx/internal/schedulers/a$a;

    iget-object v1, p0, Lrx/internal/schedulers/a$b;->d:Lrx/internal/schedulers/a$c;

    invoke-virtual {v0, v1}, Lrx/internal/schedulers/a$a;->a(Lrx/internal/schedulers/a$c;)V

    .line 205
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lrx/internal/schedulers/a$b;->b:Lcwv;

    invoke-virtual {v0}, Lcwv;->b()Z

    move-result v0

    return v0
.end method
