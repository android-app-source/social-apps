.class public final Lrx/internal/operators/as;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/c$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lrx/internal/operators/as$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/c$b",
        "<",
        "Ljava/util/List",
        "<TT;>;TT;>;"
    }
.end annotation


# static fields
.field private static final c:Ljava/util/Comparator;


# instance fields
.field final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<-TT;>;"
        }
    .end annotation
.end field

.field final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lrx/internal/operators/as$a;

    invoke-direct {v0}, Lrx/internal/operators/as$a;-><init>()V

    sput-object v0, Lrx/internal/operators/as;->c:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Lrx/functions/e;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/e",
            "<-TT;-TT;",
            "Ljava/lang/Integer;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput p2, p0, Lrx/internal/operators/as;->b:I

    .line 51
    new-instance v0, Lrx/internal/operators/as$1;

    invoke-direct {v0, p0, p1}, Lrx/internal/operators/as$1;-><init>(Lrx/internal/operators/as;Lrx/functions/e;)V

    iput-object v0, p0, Lrx/internal/operators/as;->a:Ljava/util/Comparator;

    .line 57
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    check-cast p1, Lrx/i;

    invoke-virtual {p0, p1}, Lrx/internal/operators/as;->a(Lrx/i;)Lrx/i;

    move-result-object v0

    return-object v0
.end method

.method public a(Lrx/i;)Lrx/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/i",
            "<-",
            "Ljava/util/List",
            "<TT;>;>;)",
            "Lrx/i",
            "<-TT;>;"
        }
    .end annotation

    .prologue
    .line 61
    new-instance v0, Lrx/internal/producers/SingleDelayedProducer;

    invoke-direct {v0, p1}, Lrx/internal/producers/SingleDelayedProducer;-><init>(Lrx/i;)V

    .line 62
    new-instance v1, Lrx/internal/operators/as$2;

    invoke-direct {v1, p0, v0, p1}, Lrx/internal/operators/as$2;-><init>(Lrx/internal/operators/as;Lrx/internal/producers/SingleDelayedProducer;Lrx/i;)V

    .line 102
    invoke-virtual {p1, v1}, Lrx/i;->a(Lrx/j;)V

    .line 103
    invoke-virtual {p1, v0}, Lrx/i;->a(Lrx/e;)V

    .line 104
    return-object v1
.end method
