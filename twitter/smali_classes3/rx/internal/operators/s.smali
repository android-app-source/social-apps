.class public final Lrx/internal/operators/s;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lrx/c$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/c$a",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field final a:J

.field final b:J

.field final c:Ljava/util/concurrent/TimeUnit;

.field final d:Lrx/f;


# direct methods
.method public constructor <init>(JJLjava/util/concurrent/TimeUnit;Lrx/f;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-wide p1, p0, Lrx/internal/operators/s;->a:J

    .line 38
    iput-wide p3, p0, Lrx/internal/operators/s;->b:J

    .line 39
    iput-object p5, p0, Lrx/internal/operators/s;->c:Ljava/util/concurrent/TimeUnit;

    .line 40
    iput-object p6, p0, Lrx/internal/operators/s;->d:Lrx/f;

    .line 41
    return-void
.end method


# virtual methods
.method public a(Lrx/i;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/i",
            "<-",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lrx/internal/operators/s;->d:Lrx/f;

    invoke-virtual {v0}, Lrx/f;->a()Lrx/f$a;

    move-result-object v0

    .line 46
    invoke-virtual {p1, v0}, Lrx/i;->a(Lrx/j;)V

    .line 47
    new-instance v1, Lrx/internal/operators/s$1;

    invoke-direct {v1, p0, p1, v0}, Lrx/internal/operators/s$1;-><init>(Lrx/internal/operators/s;Lrx/i;Lrx/f$a;)V

    iget-wide v2, p0, Lrx/internal/operators/s;->a:J

    iget-wide v4, p0, Lrx/internal/operators/s;->b:J

    iget-object v6, p0, Lrx/internal/operators/s;->c:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v0 .. v6}, Lrx/f$a;->a(Lrx/functions/a;JJLjava/util/concurrent/TimeUnit;)Lrx/j;

    .line 63
    return-void
.end method

.method public synthetic call(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lrx/i;

    invoke-virtual {p0, p1}, Lrx/internal/operators/s;->a(Lrx/i;)V

    return-void
.end method
